c**************************************************************************
c
c	Parameters used by MadGraphII in many different subroutines
c
c**************************************************************************
      double precision zero    , one    
      parameter       (zero=0d0, one=1d0)
c**************************************************************************
c
c     Careful if you want to change the maximum number of external particles
c     you also have to change several other parameters:
c           maxtops is probably the most important. Also look at some
c           of the other QCD color related parameters.
c
c**************************************************************************
      integer    maxlines  
      parameter (maxlines=11)  !This is the number of external lines+1
c**************************************************************************
c
c     for nexteral = 7, 8, 9, 10  -> maxtops = 2485, 34300, 559405, 10525900
c
c**************************************************************************
      integer    maxtops       ,maxnodes           ,maxgraphs
      parameter (maxtops=1999   ,maxnodes=maxlines-3,maxgraphs=9999)
      integer    max_fermi
      parameter (max_fermi=120)  !Used in fermi4. Should be (maxlines/2)!
c**************************************************************************
c
c     Below are some QCD color parameters
c
c**************************************************************************
      integer    Nc
      parameter (Nc=3)        !Number of colors SU(3)
      integer    maxfactors
      parameter (maxfactors=9)!Max number of color factors T(a,b,c)T(c,d,e)...
      integer    maxterms
      parameter (maxterms=250)!Max number of terms from expanding color
      integer    maxflows
      parameter (maxflows=250)!Max number of color flows (string configs)
      integer    maxcolors
      parameter (maxcolors=250)!Max size of reduced color matrix
c**************************************************************************
c
c     Below are some General size parameters
c
c**************************************************************************
      integer    max_coup     
      parameter (max_coup=5)  !Max number of coupling types QCD/QED/QFD ...
      integer    max_particles  !Need to change V3 and V4 if change here
      parameter (max_particles=2**7-1) !Max num of particles+antiparticle
      integer    isubfile
      parameter (isubfile=28)  !Unit number for file
      integer    max_string
      parameter (max_string=120) !Largest line of ouput call iovxxx(.......)
      integer    max_coup_num
      parameter (max_coup_num=1000)  ! Max. number of distinct couplings/masses
      integer    coup_len
      parameter (coup_len=30)  ! String length for couplings
c**************************************************************************
c
c     Below are parameters related to crossings, and pp->Wjjj 
c
c**************************************************************************
      integer    npartons      !Used for crossings
      parameter (npartons=11)  !Number of partons in proton u,d,s,c,b,t,g,...
      integer    nparts      !Used for crossings
      parameter (nparts=11)  !Number of partons in proton u,d,s,c,b,t,g,...
      integer    maxcross
      parameter (maxcross=200)!Max number of crossings for a process
      integer    maxrows  ,maxcols
c      parameter (maxrows=6,maxcols=720) !Permutations in genpp,drawfeyn
c      parameter (maxrows=7,maxcols=5040) !Permutations in genpp,drawfeyn
      parameter (maxrows=8,maxcols=40320) !Permutations in genpp,drawfeyn
      integer    maxproc
      parameter (maxproc=2)     !Largest number of subprocess for pp->jj
                                !Careful these are all in memory for optimiz.
c**************************************************************************
c
c     Postscript Feynman diagram parameters
c
c**************************************************************************
      integer    graphs_per_row  , scale   , rows_per_page
      parameter (graphs_per_row=3, scale=10, rows_per_page=5) !For Diagrams
      integer    graphs_per_page
      parameter (graphs_per_page=rows_per_page*graphs_per_row)
c**************************************************************************
c
c     Parameters for reading in subprocesses/Models
c
c**************************************************************************
      integer    fermion  
      parameter (fermion = 1)   !Used to identify fermions for fermi crossing 
      integer    max_words   , max_chars   , maxline    !For reading in model
      parameter (max_words=20, max_chars=80, maxline=80)
      character*1 null
      parameter  (null=' ')
      integer   maxv3     , maxv4     
      parameter(maxv3=8192, maxv4=2048) !Maximum number of 3/4-vertices in sort
      integer    max_blocks            !Maximim number of blocks of couplings
      parameter (max_blocks=50)        !which may be selectively written out
