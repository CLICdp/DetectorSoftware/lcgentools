!-----------------------
! madgraph - a Feynman Diagram package by Tim Stelzer and Bill Long 
! (c) 1993
!
! Filename: particles.f
!-----------------------
c       Subroutine AddSMParticles
c c***********************************************************************
c c     Add standard Model Particles, quarks, leptons, bosons
c c     Variable info contains the following
c c        info(1) = color factors  either 1,3 or 8
c c        info(2) = nhelicities    either 1,2 or 3
c c        info(3) = type           1==fermion 0=boson   
c c        info(4) = linetype       1==photon  2=fermion 3=scalar 4=gluon
c c        req     = models needed  1==QCD     2=QED     3=QFD    4=BRS
c c***********************************************************************
c       implicit none

c ! Local
c       integer info(5),i
c       character*80 buffi

c c-----
c c  Begin Code
c c-----      
c       print*,'Using default Standard Model particles'

c c
c c     This must be the first one, it is the unknown particle
c       buffi  = ' '
c       do i=1,4
c          info(i)=0
c       enddo
c       call addparticle('?? ',buffi,buffi,info,'?? ','??','??','??')
c c
c c     Add Standard Model Particles  
c c            ALL      d,u,c,s,b,t,  d~,u~,c~,s~,b~,t~
c c            QED      e+,e-,mu+,mu-,ta+,ta-,A
c c            QFD      W+,W-,Z,ve,vm,vt,ve~,vm~,vt~
c c            QCD      g
c c
c c     format for adding Dirac Fermion+antiFermion to particle list
c c     call addFermi('name','antiname','mass','code',color,nhel,line_type,req)
c c     req: 1->QCD 2->QED 3->QFD

c c    SPECIAL  PROTON, JET
c       call addFermion('p ','p~ ','ZERO ','ZERO ','p',3,2,2,0)
c       call addFermion('j ','j~ ','ZERO ','ZERO ','j',3,2,2,0)

c c    QUARKS
c       call addFermion('d ','d~ ','ZERO ','ZERO ','d',3,2,2,0)
c c      call addFermion('u ','u~ ','UMASS','UWIDTH','u',3,2,2,0)
c       call addFermion('u ','u~ ','ZERO ','ZERO ','u',3,2,2,0)
c       call addFermion('s ','s~ ','ZERO ','ZERO ','s',3,2,2,0)
c c      call addFermion('c ','c~ ','CMASS','CWIDTH','c',3,2,2,0)
c       call addFermion('c ','c~ ','ZERO ','ZERO ','c',3,2,2,0)
c c      call addFermion('b ','b~ ','ZERO ','ZERO ','b',3,2,2,0)
c       call addFermion('b ','b~ ','BMASS','BWIDTH','b',3,2,2,0)
c       call addFermion('t ','t~ ','TMASS','TWIDTH','t',3,2,2,0)

c       call addFermion('r ','r ','fmass(1)','zero ','r',8,2,2,1)

c c    LEPTONS
c c      call addFermion('e- ','e+ ','ZERO ','ZERO ','e',1,2,2,2)
c       call addFermion('e- ','e+ ','FMASS(1)','ZERO ','e',1,2,2,2)
c       call addFermion('mu- ','mu+ ','ZERO ','ZERO ','mu',1,2,2,2)
c       call addFermion('ta- ','ta+ ','ZERO ','ZERO ','ta',1,2,2,2)
c       call addFermion('ve ','ve~ ','ZERO ','ZERO ','ve',1,2,2,3)
c       call addFermion('vm ','vm~ ','ZERO ','ZERO ','vm',1,2,2,3)
c       call addFermion('vt ','vt~ ','ZERO ','ZERO ','vt',1,2,2,3)

c c
c c     format for adding vector bosons to particle list
c c     call addboson('name','antiname','mass','code',color,nhel,line_type,req)
c c     req: 1->QCD 2->QED 3->QFD 4->GHOST

c c    Bosons
c       call addboson('g ','g ','ZERO ','ZERO ',' ',8,2,4,1)
c       call addboson('a ','a ','ZERO ','ZERO ','A',1,2,1,2)
c       call addboson('z ','z ','ZMASS','ZWIDTH','Z',1,3,1,3)
c c      call addboson('w+ ','w- ','WMASS','WWIDTH','W',1,3,1,3)
c       call addboson('w- ','w+ ','WMASS','WWIDTH','W',1,3,1,3)
c       call addscalar('h ','h ','HMASS','HWIDTH','h',1,1,3,3)
c c      call addscalar('x+ ','x- ','WMASS','WWIDTH','X+',1,1,3,4)
c c      call addscalar('x- ','x+ ','WMASS','WWIDTH','X-',1,1,3,4)
c c      call addscalar('x3 ','x3 ','ZMASS','ZWIDTH','X3',1,1,3,4)
c       end


c added (WK):
      block data init_major
      logical         major_exist
      common/to_major/major_exist
      data  major_exist/.false./
      end


      subroutine addFermion(code,anticode,mass,width,name,
     $     icolor,ihel,iline,ireq,pdg)
c***************************************************************************
c     Adds Dirac Fermion both the particle and the antiparticle to the
c     list of active particles.
c***************************************************************************
      implicit none
c
c     Constant
c
      include 'params.inc'
      character*(*) buffp_tail
      parameter (   buffp_tail=',NHEL(????),+1*IC(????),W(1,????))')
      character*(*) buffa_tail
      parameter (   buffa_tail=',NHEL(????),-1*IC(????),W(1,????))')
      character*(*) buffi_head               
      parameter (   buffi_head='IXXXXX(P(0,????),')
      character*(*) buffo_head                
      parameter (   buffo_head='OXXXXX(P(0,????),')
c
c     Arguments
c
      character*(*) code,anticode,mass,width,name
      integer icolor,ihel,iline,ireq,pdg
c
c     Local
c
      character*(max_string) siwave,sowave
      integer info(5),npart,jline(1),kline,np
c
c     Global
c
      character*(4*max_particles) particle(4)
      integer                               charge_c(max_particles)
      integer iparticle(0:max_particles,0:4),inverse(max_particles)
      common/to_model/iparticle,  particle,  inverse, charge_c

      logical         major_exist
      common/to_major/major_exist

c      data  major_exist/.false./
c-----
c  Begin Code
c-----
      info(1)= icolor
      info(2)= ihel
      info(3)= 1
      info(4)= iline
      info(5)= ireq
      siwave = buffi_head//mass//buffp_tail
      sowave = buffo_head//mass//buffp_tail
      call addparticle 
     &     (code,siwave,sowave,info,anticode,mass,width,name,pdg)
      siwave = buffo_head//mass//buffa_tail
      sowave = buffi_head//mass//buffa_tail
c
c     Check for Majorona type fermion
c
      call getparticle(code,jline(1),npart)
      if (npart .eq. 1) then
c         call getparticle(anticode,kline,np)
         if (code .eq. anticode) then   !Majorana
            if (.not. major_exist) then
               write(*,*) 'Majorana particles exist',
     $              ' code will run slower.'
               major_exist=.true.
            endif
            info(2) = -2
c            write(*,*) 'Warning Majorana particles may need ',
c     &           'CC checked.',code,anticode
            call addparticle(' ',siwave,sowave,info,code,
     $           mass,width,name,pdg)
c            write(*,*) 'Majorona Results:',jline(1),inverse(jline(1)),
c     &           charge_c(jline(1)),inverse(inverse(jline(1))),
c     &           charge_c(inverse(jline(1)))
            
         else                                  !Dirac
            call addparticle(anticode,siwave,sowave,info,code,
     $           mass,width,name,-pdg)
c
c     Now add charge conjugate fields for Dirac Fermions
c     uses the fact that particle numbers are added sequentially to add
c     the charge-conjugate field. I am defining this field to be a spinor
c     with the same quantum numbers, but with 'ficticious flow' in the opposite
c     direction. See Denner, Eck, Hahn, Kublbeck CERN-TH.6549/92
c
            if (inverse(jline(1)) .ne. jline(1)+1) then
               write(*,*) 'Warning from addparticle:'
               write(*,*) 'Particles not added sequentially',
     &              jline(1),inverse(jline(1)),code,anticode
            endif
            siwave = buffo_head//mass//buffa_tail
            sowave = buffi_head//mass//buffa_tail

            call addparticle(' ',siwave,sowave,info,' ',
     $           mass,width,name,-pdg)

            siwave = buffi_head//mass//buffp_tail
            sowave = buffo_head//mass//buffp_tail
            call addparticle(' ',siwave,sowave,info,' ',
     $           mass,width,name,pdg)

c            charge_c(jline(1))   = jline(1)+3        !Hagiwara
c            charge_c(jline(1)+3) = jline(1)
c            charge_c(jline(1)+1) = jline(1)+2
c            charge_c(jline(1)+2) = jline(1)+1
c            inverse(jline(1)+2)  = jline(1)+3
c            inverse(jline(1)+3)  = jline(1)+2            

            charge_c(jline(1))   = jline(1)+2       !Tim's method
            charge_c(jline(1)+2) = jline(1)
            charge_c(jline(1)+1) = jline(1)+3
            charge_c(jline(1)+3) = jline(1)+1
            inverse(jline(1)+2)  = jline(1)+3
            inverse(jline(1)+3)  = jline(1)+2            
         endif
      else
         print*,'Error Fermion not entered properly',npart,code
      endif
      end

      subroutine addBoson(code,anticode,mass,width,name,
     $     icolor,ihel,iline,ireq,pdg)
c***************************************************************************
c     Adds vector boson to the
c     list of active particles.
c***************************************************************************
      implicit none
c
c     Constant
c
      include 'params.inc'
      character*(*) buff_head               
      parameter (   buff_head='VXXXXX(P(0,????),')
      character*(*) buffi_tail
      parameter (   buffi_tail=',NHEL(????),-1*IC(????),W(1,????))')
      character*(*) buffo_tail
      parameter (   buffo_tail=',NHEL(????),+1*IC(????),W(1,????))')
c      integer    max_string
c      parameter (max_string=120)
c
c     Arguments
c
      character*(*) code,anticode,mass,width,name
      integer icolor,ihel,iline,ireq,pdg
c
c     Local
c
      character*(max_string) siwave,sowave
      integer info(5),npart,jline(1)
c
c     Global
c
      character*(4*max_particles) particle(4)
      integer                               charge_c(max_particles)
      integer iparticle(0:max_particles,0:4),inverse(max_particles)
      common/to_model/iparticle,  particle,  inverse, charge_c
c-----
c  Begin Code
c-----
      info(1)= icolor
      info(2)= ihel
      info(3)= 0   
      info(4)= iline
      info(5)= ireq
      siwave = buff_head//mass//buffi_tail
      sowave = buff_head//mass//buffo_tail
      call addparticle
     &     (code,siwave,sowave,info,anticode,mass,width,name,pdg)
      call getparticle(code,jline(1),npart)
      if (npart .eq. 1) then
         if (inverse(jline(1)) .eq. jline(1)) then   !Own anti-particle
         else                                  
            call addparticle(anticode,siwave,sowave,info,code,
     $           mass,width,name,-pdg)
         endif
      else
         print*,'Error Boson not entered properly',npart,code
      endif
      end

      subroutine addscalar(code,anticode,mass,width,name,
     $     icolor,ihel,iline,ireq,pdg)
c***************************************************************************
c     Adds vector boson to the
c     list of active particles.
c***************************************************************************
      implicit none
c
c     Constant
c
      include 'params.inc'
      character*(*) buff_head               
      parameter (   buff_head='SXXXXX(P(0,????)')
      character*(*) buffi_tail
      parameter (   buffi_tail=',-1*IC(????),W(1,????))')
      character*(*) buffo_tail
      parameter (   buffo_tail=',+1*IC(????),W(1,????))')
c
c     Arguments
c
      character*(*) code,anticode,mass,width,name
      integer icolor,ihel,iline,ireq,pdg
c
c     Local
c
      character*(max_string) siwave,sowave
      integer info(5),jline(1),npart
c
c     Global
c
      character*(4*max_particles) particle(4)
      integer                               charge_c(max_particles)
      integer iparticle(0:max_particles,0:4),inverse(max_particles)
      common/to_model/iparticle,  particle,  inverse, charge_c
c-----
c  Begin Code
c-----
      info(1)= icolor
      info(2)= ihel
      info(3)= 0   
      info(4)= iline
      info(5)= ireq
      siwave = buff_head//buffi_tail
      sowave = buff_head//buffo_tail
      call addparticle
     &     (code,siwave,sowave,info,anticode,mass,width,name,pdg)
      call getparticle(code,jline(1),npart)
      if (npart .eq. 1) then
         if (inverse(jline(1)) .eq. jline(1)) then   !Own anti-particle
         else                                  !Dirac
            call addparticle(anticode,siwave,sowave,info,code,
     $           mass,width,name,-pdg)
         endif
      else
         print*,'Error Scalar not entered properly',npart,code
      endif
      end

      subroutine addparticle(code,siwave,sowave,info,anticode,
     $     mass,width,name,pdg)
c***************************************************************************
c     subroutine to add another type of particle to MadGraph
c     siwave and sowave must be very similar, such that the positions
c     of the wavefunctions are identical.  ixxxxxx(....) and oxxxxx(...) 
c     work fine.
c     written 1/30/94
c***************************************************************************
      implicit none

! Constants

      include 'params.inc'

! Arguments

      character*(*) code,anticode
      character*(*) name,mass,width
      character*(*) siwave,sowave
      integer info(5),pdg

! Local

      integer i,n,npart,ipart,n1,in
      character*(5) buff,inbuff

! Global
      character*(max_string) iwave(max_particles),owave(max_particles)
      integer iposx(3,max_particles)
      integer info_p(5,max_particles)
      character*(8) str(3,max_particles)

      common/to_external/iwave,owave,iposx,info_p,str

      character*(4*max_particles) particle(4)
      integer                               charge_c(max_particles)
      integer iparticle(0:max_particles,0:4),inverse(max_particles)
      common/to_model/iparticle,  particle,  inverse, charge_c

      integer pdg_code(0:max_particles)
      common/to_pdg/pdg_code

!-----
!  Begin Code
!-----      
!
! Convert to lowercase characters
!
      buff   = code
      inbuff = anticode
      call lower_case(buff)
      call lower_case(inbuff)

!
! Make sure code isn't already in use
!
      n = index(buff,null)-1
      if (n .gt. 0) then
         if (mod(index(particle(n),buff(1:n))-1,n) .eq. 0 .and.
     $        index(particle(n),buff(1:n))-1 .ge. 0) then
            print*,'Error particle code already in use',n,' ',buff(1:n)
            return
         endif
      endif
!
! Add to list of particles
!
      if (iparticle(0,n) .eq. max_particles) then
         write(*,*) 'Exceeded Maximum number of particles',max_particles
         return
      endif
         iparticle(0,n)=iparticle(0,n)+1
         npart = iparticle(0,n)
         ipart = iparticle(0,1)+iparticle(0,2)+iparticle(0,3)
     &        +iparticle(0,4)+iparticle(0,0)
         iparticle(npart,n) = ipart
         if (n .gt. 0) then
            particle(n)((npart-1)*n+1:npart*n)=buff(1:n)
         endif
         charge_c(ipart) = ipart  !Default to being own charge_c
!
! Check inverse, make charge_c default to inverse
!
      n1 = index(inbuff,null)-1
      if (n1 .gt. 0) then
         in = index(particle(n1),inbuff(1:n1))-1
         if (mod(in,n1) .eq. 0 .and. in .ge. 0) then
            inverse(iparticle(npart,n))    = iparticle(in/n1+1,n1)
            inverse(iparticle(in/n1+1,n1)) = iparticle(npart,n) 
            charge_c(iparticle(npart,n))   = iparticle(in/n1+1,n1)
            charge_c(iparticle(in/n1+1,n1))= iparticle(npart,n) 
c         write(*,*) 'Setting up inverse ',buff,inbuff,
c     &        iparticle(npart,n),iparticle(in/n1+1,n1)
         endif
      endif
!
! Find Position for P,ihel,W
!
      iwave(ipart)      = siwave
      owave(ipart)      = sowave
      do i=1,5
         info_p(i,ipart)=info(i)
      enddo
      str(1,ipart) = name
      str(2,ipart) = mass
      str(3,ipart) = width
c      print*,'Added particle ',str(1,ipart),info_p(5,ipart),ipart
c      print*,' ',n,n1,buff(1:n),inbuff(1:n1)
c      print*,'added particle ',buff(1:n),str(1,ipart)
      iposx(1,ipart) = index(siwave,'P(0,????')+4
      iposx(2,ipart) = index(siwave,'NHEL(????)')+5
      iposx(3,ipart) = index(siwave,'W(1,????')+4
c      write(*,'(i4,a)') ipart,iwave(ipart)
c      write(*,'(i4,a)') ipart,owave(ipart)
      if (iposx(1,ipart) .ne. index(sowave,'P(0,????')+4) then
         print*,'Error adding external particle, P??? '
         print*,siwave
         print*,sowave
      endif
      if (iposx(2,ipart) .ne. index(sowave,'NHEL(????)')+5) then
         print*,'Error adding external particle, NHEL(????) '
         print*,siwave
         print*,sowave
      endif
      if (iposx(3,ipart) .ne. index(sowave,'W(1,????')+4) then
         print*,'Error adding external particle, W??? '
         print*,siwave
         print*,sowave
      endif

! WK: Enter PDG code
      pdg_code(0) = ipart
      pdg_code(ipart) = pdg

      end

      subroutine getparticle(xbuff,iline,nparticles)
c***************************************************************************
c     Parses buff to find what particles are contained and returns
c     those in iline
c***************************************************************************
      implicit none

! Constants
      include 'params.inc'
      integer    char_A,              char_Z
c      parameter (char_A = ichar('A'), char_Z = ichar('Z'))
      integer    lcshift
c      parameter (lcshift = ichar('a') - ichar('A'))

! Arguments

      character*(*) xbuff
      integer iline(*),nparticles

! Local

      integer i,in1,in2,in3,in4
      character*(max_string) buff


! Global

      character*(4*max_particles) particle(4)
      integer                               charge_c(max_particles)
      integer iparticle(0:max_particles,0:4),inverse(max_particles)
      common/to_model/iparticle,  particle,  inverse, charge_c

!-----
!  Begin Code
!-----      
      char_A = ichar('A')
      char_Z = ichar('Z')
      lcshift = ichar('a') - ichar('A')
      buff   = xbuff
      do i=1,len(xbuff)
         if (ichar(buff(i:i)) .ge. char_A .and.
     &       ichar(buff(i:i)) .le. char_Z) then
            buff(i:i)=char(ichar(buff(i:i))+lcshift)
         endif
      enddo
      nparticles=0
      i=1
      do while(i .le. len(xbuff))
         in4 = index(particle(4),buff(i:i+3))-1
         in3 = index(particle(3),buff(i:i+2))-1
         in2 = index(particle(2),buff(i:i+1))-1
         in1 = index(particle(1),buff(i:i+0))-1
         if (mod(in4,4) .eq. 0) then
            nparticles=nparticles+1
            iline(nparticles)= iparticle(in4/4+1,4)
c            print*,'Got one',nparticles,' ',buff(i:i+3)
            i=i+4
         elseif (mod(in3,3) .eq. 0) then
            nparticles=nparticles+1
            iline(nparticles)= iparticle(in3/3+1,3)
c            print*,'Got one',nparticles,' ',buff(i:i+2)
            i=i+3
         elseif (mod(in2,2) .eq. 0) then
            nparticles=nparticles+1
            iline(nparticles)= iparticle(in2/2+1,2)
c            print*,'Got one',nparticles,' ',xbuff(i:i+1)
            i=i+2
         elseif (mod(in1,1) .eq. 0 .and. in1 .ge. 0) then
            nparticles=nparticles+1
            iline(nparticles)= iparticle(in1/1+1,1)
c            print*,'Got one',nparticles,' ',xbuff(i:i+0)
            i=i+1
         else
            i=i+1
         endif
c         if (nparticles .gt. maxlines) then
c            write(*,*) 'Error too many particles',nparticles
c            i=len(xbuff)+5
c         endif
      enddo

      end



      subroutine write_external(iline)
c************************************************************************
c     Write External Wave Functions
c************************************************************************
      implicit none
! Constants
      include 'params.inc'
c      integer    max_particles       
c      parameter (max_particles=2**7-1)
c      integer    max_string
c      parameter (max_string=120)

! Arguments
      integer iline(0:10)

! Local
      integer i,ipart
      character*4 cnum
      character*(max_string) buff
     
! Global
      character*(max_string) iwave(max_particles),owave(max_particles)
      character*(8) str(3,max_particles)
      integer iposx(3,max_particles)
      integer info_p(5,max_particles)

      common/to_external/iwave,owave,iposx,info_p,str
      integer         nincoming
      common/to_proc2/nincoming

      character*(4*max_particles) particle(4)
      integer                               charge_c(max_particles)
      integer iparticle(0:max_particles,0:4),inverse(max_particles)
      common/to_model/iparticle,  particle,  inverse, charge_c


!-----
!  Begin Code
!-----      
      do i=1,iline(0)
         if (i .le. nincoming) then
            ipart=inverse(iline(i))
            buff = iwave(ipart)
         else
            ipart = iline(i)
            buff = owave(ipart)
         endif
         call makestringamp(i,cnum)
         buff(iposx(1,ipart):iposx(1,ipart)+3)=cnum
         buff(iposx(3,ipart)-10:iposx(3,ipart)-7)=cnum
         if (iposx(2,ipart) .ge. 6) then
            buff(iposx(2,ipart):iposx(2,ipart)+3)=cnum
         else
c            print*,'Sorry no helicity here',iposx(2,ipart)
         endif
         call optcheck(buff,i,iposx(3,ipart))
      enddo
      end


      subroutine part_string(i,str,nlen)
c************************************************************************
c     Given particle i, returns the appropriate string for it
c     and its length
c************************************************************************
      implicit none

! Constants

      include 'params.inc'
c      integer    max_particles
c      parameter (max_particles=2**7-1)
      
! Arguments

      character*4 str
      integer     i,nlen

! Local
      
      integer npos

! Global

      character*(4*max_particles) particle(4)
      integer                               charge_c(max_particles)
      integer iparticle(0:max_particles,0:4),inverse(max_particles)
      common/to_model/iparticle,  particle,  inverse, charge_c

!-----
!  Begin Code
!-----      

      nlen=1
      npos=1
      do while (iparticle(npos,nlen) .ne. i .and. nlen .le. 4)
         npos=npos+1
         if (npos .gt. iparticle(0,nlen)) then
            npos = 1
            nlen = nlen+1
         endif
      enddo
      if (nlen .gt. 4) then
         nlen=4
         str='????'
      else
         str = particle(nlen)(nlen*(npos-1)+1:nlen*(npos))
      endif
c      print*,'found string',i,str(1:nlen)
      end

      Logical function Majarana(ipart)
c***********************************************************************
c     Returns .true. if particle is majarana, false if
c     it is not.
c***********************************************************************
      implicit none

! Constants

      include 'params.inc'
      
! Arguments

      integer ipart

! Global

      character*(4*max_particles) particle(4)
      integer                               charge_c(max_particles)
      integer iparticle(0:max_particles,0:4),inverse(max_particles)
      common/to_model/iparticle,  particle,  inverse, charge_c

      character*(8) str(3,max_particles)
      character*(max_string) iwave(max_particles),owave(max_particles)
      integer info_p(5,max_particles),iposx(3,max_particles)
      common/to_external/iwave,owave,iposx,info_p,str

!-----
!  Begin Code
!-----
      majarana = .false.
      if (info_p(3,ipart) .eq. 1)   then           !Fermion
         if (info_p(2,ipart)          .eq. -2 .or.
     $       info_p(2,inverse(ipart)) .eq. -2)      then
            majarana=.true.
         endif
      endif
      end
