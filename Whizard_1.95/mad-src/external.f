!-----------------------
! madgraph - a Feynman Diagram package by Tim Stelzer and Bill Long 
! (c) 1993,1994
!
! Filename: external.f
!-----------------------

      Subroutine writeincoming(i)
!****************************************************************************
!     Writes incoming External line
!****************************************************************************
      implicit none

! Constants

      integer   maxcount,   from,  type,  flav,  calc,  dir,  yes
      parameter(maxcount=40,from=1,type=2,flav=3,calc=5,dir=4,yes=1)
      integer   maxlines
      parameter(maxlines=20)

! Arguments

      integer i

! Local Variables

      character*3 s
      character*41 buff
      character*41 buff_i
      character*36 buff_v(3)
      character*20 buff_s

! Global Variables

      integer iv3(maxcount,6),iv4(maxcount,6),iline(maxcount,5),nlines
      Common/tohelas/iv3,iv4,iline,nlines

! Table for calls to IXXXXX

      data buff_i/'IXXXXX(P   ,FMASS(   ),NHEL(   ), 1,W   )'/

! Table for calls to VXXXXX

      data buff_v/'VXXXXX(P   , ZERO,NHEL(   ),-1,W   )',
     &            'VXXXXX(P   ,ZMASS,NHEL(   ),-1,W   )',
     &            'VXXXXX(P   ,WMASS,NHEL(   ),-1,W   )'/

! Table for calls to SXXXXX

      data buff_s/'SXXXXX(P   ,-1,W   )'/

!-----------
! Begin Code
!-----------
      call makestringamp(i,s)
      if     (abs(iline(i,type)) .eq. 1) then  ! Photon, Z, or W+-
         buff = buff_v(abs(iline(i,flav)))
         buff( 9:11) = s
         buff(24:26) = s
         buff(33:35) = s
         call optcheck(buff,i,32)
         iline(i,calc) = yes                   !wavefunction has been computed

      elseif (abs(iline(i,type)) .eq. 2) then  ! Gluon
         buff = buff_v(1)
         buff( 9:11) = s
         buff(24:26) = s
         buff(33:35) = s
         call optcheck(buff,i,32)
         iline(i,calc) = yes                   !wavefunction has been computed

      elseif      (iline(i,type) .eq. 3) then  !Fermion
         buff = buff_i
         call makestringamp(abs(iline(i,flav)),buff(19:21)) !Flavor
         if (iline(i,flav) .lt. 0) buff(34:34) = '-'        !Anti-particle
         buff( 9:11) = s
         buff(29:31) = s
         buff(38:40) = s
         call optcheck(buff,i,37)
         iline(i,calc) = yes                    !wavefunction has been computed
         
      elseif (abs(iline(i,type)) .eq. 4)  then  !Scalar
         buff = buff_s
         buff( 9:11) = s
         buff(17:19) = s
         call optcheck(buff,i,16)
         iline(i,calc) = yes                    !wavefunction has been computed

      else
         print*,'Incoming Particle ',iline(i,type),iline(i,flav),
     &        ' is not currently supported'
      endif
      end
      
      Subroutine writeoutgoing(i)
!****************************************************************************
!     Writes outgoing External line
!****************************************************************************
      implicit none

! Constants

      integer   maxcount,   from,  type,  flav,  calc,  dir,  yes
      parameter(maxcount=40,from=1,type=2,flav=3,calc=5,dir=4,yes=1)
      integer   maxlines
      parameter(maxlines=20)

! Arguments

      integer i

! Local Variables

      character*3 s
      character*41 buff
      character*41 buff_i
      character*36 buff_v(3)
      character*20 buff_s

! Global Variables

      integer iv3(maxcount,6),iv4(maxcount,6),iline(maxcount,5),nlines
      Common/tohelas/iv3,iv4,iline,nlines


! Table for calls to OXXXXX

      data buff_i/'OXXXXX(P   ,FMASS(   ),NHEL(   ), 1,W   )'/

! Table for calls to VXXXXX

      data buff_v/'VXXXXX(P   , ZERO,NHEL(   ), 1,W   )',
     &            'VXXXXX(P   ,ZMASS,NHEL(   ), 1,W   )',
     &            'VXXXXX(P   ,WMASS,NHEL(   ), 1,W   )'/

! Table for calls to SXXXXX

      data buff_s/'SXXXXX(P   , 1,W   )'/

!-----------
! Begin Code
!-----------
      call makestringamp(i,s)
      if     (abs(iline(i,type)) .eq. 1) then  ! Photon, Z, or W+-
         buff = buff_v(abs(iline(i,flav)))
         buff( 9:11) = s
         buff(24:26) = s
         buff(33:35) = s
         call optcheck(buff,i,32)
         iline(i,calc) = yes                   !wavefunction has been computed

      elseif (abs(iline(i,type)) .eq. 2) then  ! Gluon
         buff = buff_v(1)
         buff( 9:11) = s
         buff(24:26) = s
         buff(33:35) = s
         call optcheck(buff,i,32)
         iline(i,calc) = yes                   !wavefunction has been computed

      elseif      (iline(i,type) .eq. 3) then  !Fermion
         buff = buff_i
         call makestringamp(abs(iline(i,flav)),buff(19:21)) !Flavor
         if (iline(i,flav) .lt. 0) buff(34:34) = '-'        !Anti-particle
         buff( 9:11) = s
         buff(29:31) = s
         buff(38:40) = s
         call optcheck(buff,i,37)
         iline(i,calc) = yes                    !wavefunction has been computed
         
      elseif (abs(iline(i,type)) .eq. 4)  then  !Scalar
         buff = buff_s
         buff( 9:11) = s
         buff(17:19) = s
         call optcheck(buff,i,16)
         iline(i,calc) = yes                    !wavefunction has been computed

      else
         print*,'Outgoinging Particle ',iline(i,type),iline(i,flav),i,
     &        ' is not currently supported'
      endif
      end

