!-----------------------
! madgraph - a Feynman Diagram package by Tim Stelzer and Bill Long 
! (c) 1993
!
! Filename: newinfo.f
!-----------------------
 
      Subroutine WriteGraphs(tops,graphs,next)
!***************************************************************************
!     This routine loads information from tops and iline into iv3 and iv4
!     so that it may be used to generate the HELAS code.
!***************************************************************************
      implicit none

! Constants

      integer   maxcount,   from,  type,  flav,  calc,  dir,  yes
      parameter(maxcount=40,from=1,type=2,flav=3,calc=5,dir=4,yes=1)
      integer    maxtops,      maxnodes,  maxgraphs
      parameter (maxtops=2500,maxnodes=5,maxgraphs=999)
      integer    maxlines
      parameter (maxlines=8)

! Arguments

      integer tops(0:4,0:maxnodes,0:maxtops)
      integer graphs(0:maxgraphs,0:maxlines)
      integer next

! External Function

      integer inverse

! Local Variables

      integer gtype(-30:30),gflav(-30:30)
      integer igraph,ntop,inode
      integer n3vert,n4vert,i,j,nint,nv4(8)

! Global Variables

      integer iv3(maxcount,6),iv4(maxcount,6),iline(maxcount,5),nlines
      Common/tohelas/iv3,iv4,iline,nlines
      integer tline(-maxlines:maxlines)
      Common/to_proc/tline
      integer order(2,0:maxcount)
      common/newfix/order
      logical done
      integer number
      common /total/done,number
      integer           nincoming
      common /to_proc2/ nincoming

!-----------
! Begin Code
!-----------
      call setgtype(gtype)
      call setgflav(gflav)

      do igraph=1,graphs(0,0)
         call initial
         n3vert=0
         n4vert=0
         ntop = graphs(igraph,0)

!  Convert node information from topology format to NewConvert format

         order(1,0)=tops(1,0,ntop)
         do inode=1,tops(1,0,ntop)
            if (tops(0,inode,ntop) .eq. 3) then
               n3vert=n3vert+1
               order(2,inode)=n3vert
               order(1,inode)=3
               do j=1,3
                  iv3(n3vert,j) = tops(j,inode,ntop)
               enddo
               iv3(n3vert,calc)=-yes
            elseif(tops(0,inode,ntop) .eq. 4) then
               n4vert=n4vert+1
               order(2,inode)=n4vert
               order(1,inode)=4
               do j=1,4
                  iv4(n4vert,j) = tops(j,inode,ntop)
               enddo
               iv4(n4vert,calc)=-yes
               nv4(n4vert) = graphs(igraph,maxlines-n4vert)
            else
               print*,'Error Vertex not 3 or 4',tops(0,inode,ntop)
               stop
            endif
         enddo

!     Convert line information from topology format to NewConvert format

         do j=1,next
            iline(j,flav) = gflav(tline(j))
            iline(j,calc) = -yes
            iline(j,type) = gtype(tline(j))
            iline(j,from) = 2
            iline(j,dir)  = -abs(iline(j,flav))/iline(j,flav)
            if (j .le. nincoming ) then
               iline(j,from)=1  !incoming line
               iline(j,flav) = gflav(inverse(tline(j)))
            endif
         enddo
         nint = next-3-n4vert
         do j=1,nint
            iline(j+next,flav) = gflav(graphs(igraph,j))
            iline(j+next,calc) = -yes
            iline(j+next,type) = gtype(graphs(igraph,j))
            iline(j+next,from) = 3
            iline(j+next,dir)  = -abs(iline(j+next,flav))
     &           /iline(j+next,flav)
         enddo

         nlines=next+nint

!     Converts internal Line number from negative to positive format

         do i=1,n3vert
            do j=1,3
               if (iv3(i,j) .lt. 0) iv3(i,j)=next-iv3(i,j)
            enddo
         enddo
         do i=1,n4vert
            do j=1,4
               if (iv4(i,j) .lt. 0) iv4(i,j)=next-iv4(i,j)
            enddo
            if (iline(iv4(i,nv4(i)),type) .eq. 2) then
               iline(iv4(i,nv4(i)),type) = -2 !4 gluon vertex
            endif
         enddo
         call sortvertexhelas
         number=igraph
         if (igraph .eq. 1) then
            call writeexternal
         else
            call markexternal
         endif
         call vertex
         call getFermiFact
      enddo
      close(4)
      end

!---------------------------------
      Subroutine Initial
      implicit none

! Constants

      integer   maxcount,   from,  type,  flav,  calc,  dir,  yes
      parameter(maxcount=40,from=1,type=2,flav=3,calc=5,dir=4,yes=1)

! Local Variables

      integer i,j

! Global Variables

      integer iv3(maxcount,6),iv4(maxcount,6),iline(maxcount,5),nlines
      Common/tohelas/iv3,iv4,iline,nlines

!-----------
! Begin Code
!-----------
      do i=1,maxcount
         do j=1,3
            iv3(i,j)=-1
         enddo
         iv3(i,calc)=yes
         do j=1,4
            iv4(i,j)=-1
         enddo
         iv4(i,calc)=yes
         iline(i,calc)=-yes
      enddo
      end


      Subroutine setgflav(gflav)
!**************************************************************************
!     Routine to convert flavors from Particle data book to MadGraph
!     number scheme
!**************************************************************************
      implicit none

! Constants

      integer    maxparticles
      parameter (maxparticles=30)

! Arguments

      integer gflav(-maxparticles:maxparticles)

! Local Variables

      integer i

!-----------
! Begin Code
!-----------
      do i=-maxparticles,maxparticles
         gflav(i)=0
      enddo

!     QCD particles

      gflav(1) = 4
      gflav(2) = 3
      gflav(3) = 8
      gflav(4) = 7
      gflav(5) = 12
      gflav(6) = 11
      gflav(9) = 2

!     QFD particles

      gflav(11)= 1
      gflav(12)= 2
      gflav(13)= 5
      gflav(14)= 6
      gflav(15)= 9
      gflav(16)= 10
      gflav(22)= 1            !photon
      gflav(23)= 2            !Z
      gflav(24)=-3            !W+
      gflav(25)= 1
      do i=1,maxparticles
         gflav(-i)=-gflav(i)
      enddo
      end

      subroutine setgtype(gtype)
!************************************************************************
!     Routine to convert from Particle Data book codes to MadGraph
!     type codes
!************************************************************************
      implicit none

! Constants

      integer    maxparticles
      parameter (maxparticles=30)

! Arguments

      integer gtype(-maxparticles:maxparticles)

! Local Variables

      integer i

!-----------
! Begin Code
!-----------
      do i=-maxparticles,maxparticles
         gtype(i)=0
      enddo
      do i=1,6
         gtype(i)=3         !fermions
         gtype(-i)=3
      enddo
      do i=11,16
         gtype(i)=3
         gtype(-i)=3
      enddo
      gtype(9) =2
      gtype(21)=2
      gtype(22)=1          !photon
      gtype(23)=1          !Z
      gtype(24)=1          !W+
      gtype(-24)=1
      gtype(25)=4          !Higgs
      end
