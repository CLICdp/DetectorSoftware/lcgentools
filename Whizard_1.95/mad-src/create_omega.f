!-----------------------
! madgraph - a Feynman Diagram package by Tim Stelzer and Bill Long 
! (c) 1993
!
! Filename: create.f
!-----------------------

      Subroutine CreateColor(tops,graphs,ngraph,color)
!**************************************************************************
!     Given the topologies, and the lines, fill in the appropriate
!     color factors into the color variable
!**************************************************************************
      implicit none

! Constants

      integer   maxlines,  maxfactors,  maxterms
      parameter(maxlines=8,maxfactors=9,maxterms=250)
      integer    maxtops,     maxnodes,  maxgraphs
      parameter (maxtops=2500,maxnodes=5,maxgraphs=999)

! Arguments

      integer tops(0:4,0:maxnodes,0:maxtops)
      integer graphs(0:maxgraphs,0:maxlines)  !graphs, topology internal lines
      integer ngraph
      integer color(0:maxlines,0:maxfactors,0:maxterms)

! Local Variables

      integer ng1,ng2,ng3,ng4,fo,fi,i,ln,n
      integer ntop,nvert,n4glue

! External Functions

      integer Inverse

! Global Variables

      integer        iline(-maxlines:maxlines)
      common/to_proc/iline

!-----------
! Begin Code
!-----------
      n4glue=0
      do i=1,maxlines
         iline(-i)=graphs(ngraph,i)           !set up values of internal lines
      enddo
      ntop=graphs(ngraph,0)                   !topology for graph ngraph
      do nvert=1,tops(1,0,ntop)               !loop over all nodes
         ng1=0
         ng2=0
         ng3=0
         ng4=0
         fo =0
         fi =0
         do ln=1,tops(0,nvert,ntop)           !# lines in vertex
            if (tops(ln,nvert,ntop) .lt. 0) then   !invert
               iline(tops(ln,nvert,ntop))=
     &              inverse(iline(tops(ln,nvert,ntop)))
            endif
            if ( iline(tops(ln,nvert,ntop)) .eq. 9) then
               if (ng1 .eq. 0) then
                  ng1=tops(ln,nvert,ntop)
               elseif(ng2 .eq. 0) then
                  ng2=tops(ln,nvert,ntop)
               elseif(ng3 .eq. 0) then
                  ng3=tops(ln,nvert,ntop)
               elseif(ng4 .eq. 0) then
                  ng4=tops(ln,nvert,ntop)
               else
                  print*,'Error too many gluons in Create Color'
               endif
            elseif (iline(tops(ln,nvert,ntop)) .eq. 22) then !photon
            elseif (iline(tops(ln,nvert,ntop)) .eq. 23) then !Z
            elseif (iline(tops(ln,nvert,ntop)) .eq. 24) then !W+
            elseif (iline(tops(ln,nvert,ntop)) .eq.-24) then !W-
            elseif (iline(tops(ln,nvert,ntop)) .eq. 25) then !H
            elseif (iline(tops(ln,nvert,ntop)) .lt. 0)  then
               fi = tops(ln,nvert,ntop)
            elseif (iline(tops(ln,nvert,ntop)) .gt. 0) then
               fo = tops(ln,nvert,ntop)
            endif
         enddo
         if (ng4 .ne. 0) then
            n4glue=n4glue+1
            n = iline(-maxlines+n4glue)
            if (n .eq. 1) then
               call addF4factor(color,ng1,ng2,ng3,ng4)
            elseif (n .eq. 2) then
               call addF4factor(color,ng3,ng1,ng2,ng4)
            elseif (n .eq. 3) then
               call addF4factor(color,ng2,ng3,ng1,ng4)
            else
               print*,'Error in 4 glue n .ne. 1,2,or 3',n
               stop
            endif
         elseif (ng3 .ne. 0) then
            call addFfactor(color,ng1,ng3,ng2)
         elseif (ng2 .ne. 0) then
            call addFfactorP(color,ng1,ng2)
         elseif (fi .ne. 0 .or. fo .ne. 0) then
            if (fi*fo .eq. 0) then
               print*,'Error in quark line',fi,fo,ng1
               print*,'lines',(tops(ln,nvert,ntop),ln=1,4)
               print*,'iline',iline(-1),iline(4),iline(3)
               call printgraph(graphs,tops,ngraph)
               stop
            endif
            if (ng1 .ne. 0) then
               call addTfactor(color,fo,fi,ng1)
            else
               if (abs(iline(fi)) .le. 6) then !Quark
                  call addTdel(color,fo,fi)
               endif
            endif
         endif
      enddo
      end

      Subroutine AddTdel(color,fo,fi)
!***************************************************************************
!     Add the T factor t__{fo,fi} to all terms (delta function)
!***************************************************************************
      implicit none

! Constants

      integer   maxlines,  maxfactors,  maxterms
      parameter(maxlines=8,maxfactors=9,maxterms=250)

! Arguments

      integer color(0:maxlines,0:maxfactors,0:maxterms)
      integer fo,fi

! Local Variables

      integer nterms,nfactors
      integer iterm
!------------
!  Begin code
!------------
      nterms=color(0,0,0)
      do iterm=1,nterms
         color(0,0,iterm)=color(0,0,iterm)+1
         nfactors=color(0,0,iterm)
         color(0,nfactors,iterm)=-2         !One gluon, two quark lines T mat
         color(1,nfactors,iterm)=fo         !Outgoing quark line
         color(2,nfactors,iterm)=fi         !Outgoing quark line
      enddo
      end

      Subroutine AddTFactor(color,fo,fi,ng)
!***************************************************************************
!     Add the T factor t^ng__{fo,fi} to all terms
!***************************************************************************
      implicit none

! Constants

      integer   maxlines,  maxfactors,  maxterms
      parameter(maxlines=8,maxfactors=9,maxterms=250)

! Arguments

      integer color(0:maxlines,0:maxfactors,0:maxterms)
      integer ng,fo,fi

! Local Varibales

      integer nterms,nfactors
      integer iterm

!------------
!  Begin code
!------------
      nterms=color(0,0,0)
      do iterm=1,nterms
         color(0,0,iterm)=color(0,0,iterm)+1
         nfactors=color(0,0,iterm)
         color(0,nfactors,iterm)=-3         !One gluon, two quark lines T mat
         color(1,nfactors,iterm)=fo         !Outgoing quark line
         color(2,nfactors,iterm)=fi         !Outgoing quark line
         color(3,nfactors,iterm)=ng         !Gluon color line
      enddo
      end

      Subroutine AddFFactorP(color,ng1,ng2)
!***************************************************************************
!     Add the T factor t^ng__{fo,fi} to all terms
!***************************************************************************
      implicit none

! Constants

      integer   maxlines,  maxfactors,  maxterms
      parameter(maxlines=8,maxfactors=9,maxterms=250)

! Arguments

      integer color(0:maxlines,0:maxfactors,0:maxterms)
      integer ng1,ng2

! Local Variables

      integer nterms,nfactors
      integer iterm

!------------
!  Begin Code
!------------
      nterms=color(0,0,0)
      do iterm=1,nterms
         color(0,0,iterm)=color(0,0,iterm)+1
         nfactors=color(0,0,iterm)
         color(0,nfactors,iterm)=2           !One gluon, two quark lines T mat
         color(1,nfactors,iterm)=ng1         !Outgoing quark line
         color(2,nfactors,iterm)=ng2         !Outgoing quark line
         color(2,1,iterm)=color(2,1,iterm)*2 !F{a,b}=.5*delta{a,b}
      enddo
      end

      Subroutine AddFFactor(color,ng1,ng2,ng3)
!***************************************************************************
!     Add the T factor t^ng__{fo,fi} to all terms
!***************************************************************************
      implicit none

! Constants

      integer   maxlines,  maxfactors,  maxterms
      parameter(maxlines=8,maxfactors=9,maxterms=250)

! Arguments

      integer color(0:maxlines,0:maxfactors,0:maxterms)
      integer ng1,ng2,ng3

! Local Variables

      integer nterms,nfactors
      integer iterm,nflows

!------------
!  Begin code
!------------
      nterms=color(0,0,0)
      nflows=color(4,1,nterms)                  !How many flows exist
      do iterm=1,nterms
         color(1,1,iterm)=-color(1,1,iterm)
         call copyterm(color,iterm,-999,-999)    !Add term for two flows

         color(0,0,iterm)=color(0,0,iterm)+1
         nfactors=color(0,0,iterm)
         color(0,nfactors,iterm)=3          !3 Gluons F matrix
         color(1,nfactors,iterm)=ng1        !Gluon 1
         color(2,nfactors,iterm)=ng2        !Gluon 2
         color(3,nfactors,iterm)=ng3        !Gluon 3 color line
         color(1,1,iterm)=color(1,1,iterm)*2

         color(0,0,iterm+nterms)=color(0,0,iterm+nterms)+1
         nfactors=color(0,0,iterm+nterms)
         color(0,nfactors,iterm+nterms)=3          !3 Gluons F matrix
         color(1,nfactors,iterm+nterms)=ng3        !Gluon 1
         color(2,nfactors,iterm+nterms)=ng2        !Gluon 2
         color(3,nfactors,iterm+nterms)=ng1        !Gluon 3 color line
         color(1,1,iterm+nterms)=color(1,1,iterm+nterms)*2
         color(2,1,iterm+nterms)=-color(2,1,iterm+nterms) !Subtract second
      enddo
      if(color(0,0,0) .ne. 2*nterms) then
         print*,'Warning wrong number of terms created in addF',
     &        color(0,0,0),nterms
      endif
      end

      Subroutine AddF4Factor(color,ng1,ng2,ng3,ng4)
!***************************************************************************
!     Add the T factor t^ng__{fo,fi} to all terms
!***************************************************************************
      implicit none

! Constants

      integer   maxlines,  maxfactors,  maxterms
      parameter(maxlines=8,maxfactors=9,maxterms=250)
      integer   Nc
      parameter(Nc=3)

! Arguments

      integer color(0:maxlines,0:maxfactors,0:maxterms)
      integer ng1,ng2,ng3,ng4

! Local Variables

      integer nterms,nfactors
      integer iterm,fterm,nflows,fflow

!------------
!  Begin code
!------------
      nterms=color(0,0,0)
      nflows=color(4,1,nterms)                   !How many flows exist
      do iterm=1,nterms
         color(1,1,iterm) = -2*color(1,1,iterm)
         call copyterm(color,iterm,-999,-999)    !Add term for flow 2
         call copyterm(color,iterm,-999,-999)    !Add term for flow 3
         call copyterm(color,iterm,-999,-999)    !Add term for flow 4

         color(0,0,iterm)=color(0,0,iterm)+1     !Add Factor  Term 1
         nfactors=color(0,0,iterm)
         color(0,nfactors,iterm)=4               !4 Gluons F matrix
         color(1,nfactors,iterm)=ng1             !Gluon 1
         color(2,nfactors,iterm)=ng2             !Gluon 2
         color(3,nfactors,iterm)=ng3             !Gluon 3 color line
         color(4,nfactors,iterm)=ng4             !Gluon 4 color line
         
         fterm = nterms+3*(iterm-1)+1
         fflow=1
         color(0,0,fterm)=color(0,0,fterm)+1     !Term 2 1 factors
         nfactors=color(0,0,fterm)
         color(0,nfactors,fterm)=4               !4 Gluons F matrix
         color(1,nfactors,fterm)=ng2             !Gluon 1
         color(2,nfactors,fterm)=ng1             !Gluon 2
         color(3,nfactors,fterm)=ng4             !Gluon 3 color line
         color(4,nfactors,fterm)=ng3             !Gluon 4 color line
         color(3,1,fterm)= fflow
         

         fterm = nterms+3*(iterm-1)+2
         color(0,0,fterm)=color(0,0,fterm)+1     !Term 2
         nfactors=color(0,0,fterm)
         color(0,nfactors,fterm)=4               !4 Gluons F matrix
         color(1,nfactors,fterm)=ng2             !Gluon 1
         color(2,nfactors,fterm)=ng1             !Gluon 2
         color(3,nfactors,fterm)=ng3             !Gluon 3 color line
         color(4,nfactors,fterm)=ng4             !Gluon 4 color line
         color(2,1,fterm)=-color(2,1,fterm)
         color(3,1,fterm)= fflow

         fterm = nterms+3*(iterm-1)+3
         color(0,0,fterm)=color(0,0,fterm)+1     !Term 2
         nfactors=color(0,0,fterm)
         color(0,nfactors,fterm)=4               !4 Gluons F matrix
         color(1,nfactors,fterm)=ng1             !Gluon 1
         color(2,nfactors,fterm)=ng2             !Gluon 2
         color(3,nfactors,fterm)=ng4             !Gluon 3 color line
         color(4,nfactors,fterm)=ng3             !Gluon 4 color line
         color(2,1,fterm)=-color(2,1,fterm)
         color(3,1,fterm)= fflow
      enddo
      if(color(0,0,0) .ne. 4*nterms) then
         print*,'Warning wrong number of terms created in addF4',
     &        color(0,0,0),nterms
      endif
      end
