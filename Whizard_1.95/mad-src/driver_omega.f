!-----------------------
! madgraph - a Feynman Diagram package by Tim Stelzer and Bill Long 
! (c) 1993
!
! Filename: driver.f
!-----------------------

      program MADGRAPH
!*************************************************************************
!     Main program for MadGraph
!*************************************************************************
      implicit none

! Constants

      integer    maxtops,     maxnodes,  maxgraphs
      parameter (maxtops=2500,maxnodes=5,maxgraphs=999)
      integer    maxlines,  maxfactors,  maxterms,    maxflows
      parameter (maxlines=8,maxfactors=9,maxterms=250,maxflows=200)
      integer    graphs_per_row,   scale,    rows_per_page
      parameter (graphs_per_row=3, scale=10, rows_per_page=5)


! Local Variables

      integer tops1(0:4,0:maxnodes,0:maxtops)
      integer tops2(0:4,0:maxnodes,0:maxtops)
      integer color(0:maxlines,0:maxfactors,0:maxterms)
      integer flows(0:maxlines,0:maxfactors,0:8,0:maxflows)
      integer graphs(0:maxgraphs,0:maxlines) !graphs, topology internal lines
      integer graphcolor(0:2,0:maxflows,0:maxgraphs)
      integer i,j,next,xplotsize,yplotsize,npages
      double precision sflows(maxflows,maxflows)
      logical more
      integer fortran

! External Functions

      integer foundflow

! Global Variables

      integer        iline(-maxlines:maxlines)
      common/to_proc/iline
      integer         noqcd,noqed,nqfd
      common/to_order/noqcd,noqed,nqfd
      character*15 name
      integer iname
      common/to_name/iname,name
      integer wnum(999),wcount
      common/toopt/wcount,wnum

! Data Table

! If you want the output function written using real*8 and complex*16
! style declarations, remove the comment character from the following
! line.

!      data fortran /77/

! If you want the output function written using Fortran 90 style
! type declarations, remove the comment character from the following
! line.

      data fortran /90/

!-----------
! Begin Code
!-----------
      print*,' '
      call writelogo
      print*,' '
      
      more = .true.
      call readproc(more)
      do while (more)
      wcount=0
      print*,' '
      next=iline(0)
      if (next .lt. 3) then
         print*,'Sorry thats too easy, I need at least 1 order'
         stop
      elseif (next .gt. 7) then
         print*,'Sorry. Program configured to maximum',
     &         ' of 7 external lines'
         stop
      else
         print*,'Generating diagrams for',next,' external legs'
      endif
      if (mod(next,2) .eq. 1) then
         call tops3(tops1)
      else
         call tops3(tops2)
         call addexternal(tops2,tops1)             !now I have 4
      endif
      do i=1,(next+mod(next,2))/2 - 2       !keep adding tops 2 at a time
         call addexternal(tops1,tops2)
         call addexternal(tops2,tops1)
      enddo
      call SortVertex(tops1)
      call InsertFields(tops1,graphs)
      if (graphs(0,0) .eq. 0) then
         print*,'Sorry there are no graphs for this process.'
         if (noqcd .ne. 0) write(*,*) 'Order of QCD=',noqcd
         if (noqed .ne. 0) write(*,*) 'Order of QFD=',noqed
         if (nqfd .ne. 0) write(*,*) 'ElectroWeak sector was included'
         write(*,'(a)') ' You may want to consider a different order'
      else
      graphcolor(0,0,0)=graphs(0,0)        !Number of graphs
      print*,'There are ',graphs(0,0),' graphs.'
      flows(0,0,0,0)=0
      print*,'Writing Feynman graphs in file ',name(1:iname)//'.ps'
      open(unit=4,file=name(1:iname)//'.ps',status='unknown')
      xplotsize = (graphs_per_row*15)*scale
      yplotsize = (rows_per_page*15)*scale
      npages    = int((graphs(0,0)-1)/(graphs_per_row*rows_per_page))
      npages    = npages +1
      call startit(xplotsize,yplotsize,npages)

      do i=1,graphs(0,0)

!         print*,'Working on graph',i
         call setcolors(color)
         call createcolor(tops1,graphs,i,color)
         call simpcolors(color,-1)                 !sum internal lines
         call orderF(color)
         call addterms(color)

         graphcolor(0,0,i) = color(0,0,0)   !Number of terms for graph i
         do j=1,color(0,0,0)                !check all terms
            graphcolor(0,j,i)=foundflow(color,flows,j)  !flow for term
            graphcolor(1,j,i)=color(1,1,j)          !numerator
            graphcolor(2,j,i)=color(2,1,j)          !Denominator
         enddo
         call PositionVerts(graphs,tops1,i,next)
      enddo
      write(4,'(a)') 'showpage'
      write(4,'(a)') '%%Trailer'
      close(4)
      call square(flows,sflows)
      open(unit=99,status='scratch')
      open(unit=91,status='scratch')
      call writegraphs(tops1,graphs,next)
      call matrixcolor(graphcolor,sflows)
      rewind 99
      rewind 91
      if (fortran .eq. 90) then
         call writesub_f90(graphs(0,0),next,noqed,noqcd)
      else
         call writesub(graphs(0,0),next,noqed,noqcd)
      end if
      close(99,status='delete')
      close(91,status='delete')
      endif
      write(*,*) 
      write(*,*) 
      call readproc(more)
      enddo
      write(*,*)
      write(*,*)
      write(*,*) 'Thank you for using MadGraph'
      write(*,*)

!************* These are for color flow information only ******************
!      call writecolor(graphcolor,sflows,goodflow)
!      call writeflows(flows,goodflow)

      end

!--------------------------------
      Integer Function Inverse(i)
      implicit none
      integer i
      if (i .eq. 0) then
         inverse = 1
      else if (i .eq. 9  .or. i .eq. 22 .or. i .eq. 23
     &    .or. i .eq. 25 .or. i .eq. 26) then
         inverse = i
      else
         inverse = -i
      end if
      end
      
      
      subroutine square(flows,sflows)
!**************************************************************************
!     Square terms and simplify to get color factors for flows
!**************************************************************************
      implicit none

! Constants

      integer    maxlines,  maxfactors,  maxterms,    maxflows
      parameter (maxlines=8,maxfactors=9,maxterms=250,maxflows=200)
      integer    maxgraphs
      parameter (maxgraphs=999)

! Arguments

      integer flows(0:maxlines,0:maxfactors,0:8,0:maxflows)
      double precision  sflows(maxflows,maxflows)

! Local Variables

      integer color(0:maxlines,0:maxfactors,0:maxterms)
      integer nflows,nfactors,nelements
      integer iflow,ifact,ielement,iterm,cterm,fterm
      integer cflow,cfact,cfactors

!-----------
! Begin Code
!-----------
      nflows=flows(0,0,0,0)
      do iflow=1,nflows
         do cflow=iflow,nflows
            do iterm=1,flows(0,0,0,iflow)
               nfactors=flows(0,0,iterm,iflow)
               do ifact=0,nfactors
                  nelements=abs(flows(0,ifact,iterm,iflow))
                  do ielement=0,nelements
                     color(ielement,ifact,iterm)=
     &                    flows(ielement,ifact,iterm,iflow)
                  enddo
               enddo
            enddo                    !have all terms copied
            color(0,0,0)=flows(0,0,0,iflow)
            do cterm=2,flows(0,0,0,cflow)
               do iterm=1,flows(0,0,0,iflow)
                  call copyterm(color,iterm,-999,-999)
               enddo
            enddo              !now we have enough set for all square terms
            do cterm=1,flows(0,0,0,cflow)
               cfactors=flows(0,0,cterm,cflow)
               do iterm=1,flows(0,0,0,iflow)
                  nfactors=flows(0,0,iterm,iflow)
                  fterm = flows(0,0,0,iflow)*(cterm-1)+iterm
                  color(0,0,fterm) = nfactors+cfactors-1
                  color(0,1,fterm) = 2 !num and denom
                  color(1,1,fterm) = color(1,1,fterm)*
     &                 flows(1,1,cterm,cflow)
                  color(2,1,fterm) = color(2,1,fterm)*
     &                 flows(2,1,cterm,cflow)
                  do cfact=2,cfactors
                     nelements=abs(flows(0,cfact,cterm,cflow))
                     color(0,nfactors+cfact-1,fterm)=
     &                    flows(0,cfact,cterm,cflow)
                     if (flows(0,cfact,cterm,cflow) .lt. 0) then !T Matrix
                        color(1,cfact+nfactors-1,fterm) =
     &                       flows(2,cfact,cterm,cflow)
                        color(2,cfact+nfactors-1,fterm) =
     &                          flows(1,cfact,cterm,cflow)
                        do ielement=3,nelements
                           color(ielement,cfact+nfactors-1,fterm)=
     &                          flows(nelements-ielement+3,cfact,
     &                          cterm,cflow)
                        enddo
                     else
                        do ielement=1,nelements
                           color(ielement,cfact+nfactors-1,fterm)=
     &                          flows(nelements-ielement+1,cfact,
     &                          cterm,cflow)
                        enddo
                     endif
                  enddo
               enddo
            enddo
            call simpcolors(color,1)
            call addterms(color)
            if (color(0,0,0) .ge. 1) then
               sflows(iflow,cflow)=dble(color(1,1,1))/
     &              dble(color(2,1,1))
               sflows(cflow,iflow)=dble(color(1,1,1))/
     &              dble(color(2,1,1))
               if (color(0,0,0) .gt. 1) then
                  print*,'Error More than one term',color(0,0,1)
                  call printcolors(color)
               elseif (color(0,0,1) .gt. 1) then !one factor
                  print*,'One term but many factors',color(0,0,1),
     &                 iflow,cflow,color(1,1,1)
                  call printcolors(color)
               endif
            else
               sflows(iflow,cflow)=0.d0
               sflows(cflow,iflow)=0.d0
            endif
         enddo
      enddo
      end

               



