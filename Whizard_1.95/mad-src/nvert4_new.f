!-----------------------
! madgraph - a Feynman Diagram package by Tim Stelzer and Bill Long 
! (c) 1993
!
! Filename: nvert4.f
!-----------------------
 
      subroutine addGGGG1(ipart,coup1,coup2,info,icoup)
c**************************************************************************
c     Sets up the GGGG vertex with all the permutations
c     of the color structure
c     info is the color info for the W3W3 configuration
c     coup1 is coupling of W w/ boson 1
c     coup2 is coupling of W w/ boson 2
c     Order of particles should be W- A W+ A
c     icoup is the coupling constant
c*************************************************************************
      implicit none

! Constants

      include 'params.inc'
      character*(*) VVVV_head                          
      parameter    (VVVV_head='GGGGXX(W(1,1???),W(1,2???),'//
     &                               'W(1,3???),W(1,4???),')
      character*(*) VVVV_head1                          
      parameter    (VVVV_head1='GGGGXX(W(1,3???),W(1,1???),'//
     &                               'W(1,2???),W(1,4???),')
      character*(*) VVVV_head2                          
      parameter    (VVVV_head2='GGGGXX(W(1,2???),W(1,3???),'//
     &                               'W(1,1???),W(1,4???),')
      character*(*) VVVV_tail                          
      parameter    (VVVV_tail=',AMP(????))')
      character*(*) JVVV1_head 
      parameter    (JVVV1_head='JGGGXX(W(1,1???),W(1,2???),W(1,3???),')
      character*(*) JVVV1_tail 
      parameter    (JVVV1_tail=',W(1,4???))')
      character*(*) JVVV2_head 
      parameter    (JVVV2_head='JGGGXX(W(1,3???),W(1,1???),W(1,2???),')
      character*(*) JVVV2_tail 
      parameter    (JVVV2_tail=',W(1,4???))')
      character*(*) JVVV3_head 
      parameter    (JVVV3_head='JGGGXX(W(1,2???),W(1,3???),W(1,1???),')
      character*(*) JVVV3_tail 
      parameter    (JVVV3_tail=',W(1,4???))')

! Arguments

      character*(*) coup1,coup2
      integer info(0:4),icoup(0:max_coup),ipart(0:5)

! Local
      
      integer ipart2(0:5),i,info2(0:4)
      character*(8) vmass1,vwidth1,vmass2,vwidth2
      character*(max_string) buff


! Global

      character*(4*max_particles) particle(4)
      integer                               charge_c(max_particles)
      integer iparticle(0:max_particles,0:4),inverse(max_particles)
      common/to_model/iparticle,  particle,  inverse, charge_c

      character*(max_string) iwave(max_particles),owave(max_particles)
      character*(8) str(3,max_particles)
      integer info_p(5,max_particles),iposx(3,max_particles)
      common/to_external/iwave,owave,iposx,info_p,str

c-----
c  Begin Code
c-----
      vmass1  = str(2,ipart(2))
      vwidth1 = str(3,ipart(2))
      vmass2  = str(2,ipart(4))
      vwidth2 = str(3,ipart(4))
      if (vmass1(1:4) .ne. 'ZERO' .and. vmass1(1:3) .ne. '0D0') then
         print*,'Warning nonzero mass in 4 g sector',vmass1
         print*,'skipping interaction'
      else
      do i=0,5
         ipart2(i)=ipart(i)
      enddo
      buff = VVVV_head//coup1//VVVV_tail
      call add4vertex(ipart2,buff,info,icoup)  !Send the W3W3 version

      do i=0,5
         ipart2(i)=ipart(i)
      enddo
      ipart2(4)=1
      ipart2(5)=inverse(ipart(4))

      buff = JVVV1_head//coup1//JVVV1_tail
      call add4vertex(ipart2,buff,info,icoup)

      info2(0) = info(0)
      info2(1) = 2
      info2(2) = 3
      info2(3) = 1
      info2(4) = 4
      do i=0,5
         ipart2(i)=ipart(i)
      enddo
      ipart2(4) = 1
      ipart2(5) = inverse(ipart(4))
      buff = JVVV2_head//coup1//JVVV2_tail
      call add4vertex(ipart2,buff,info2,icoup)

      do i=1,5
         ipart2(i)=ipart(i)
      enddo
      buff = VVVV_head1//coup1//VVVV_tail
      call add4vertex(ipart2,buff,info2,icoup)  !Send the W3W3 version

      info2(1) = 3
      info2(2) = 1
      info2(3) = 2
      info2(4) = 4
      do i=0,5
         ipart2(i)=ipart(i)
      enddo
      ipart2(4) = 1
      ipart2(5) = inverse(ipart(4))
      buff = JVVV3_head//coup1//JVVV3_tail
      call add4vertex(ipart2,buff,info2,icoup)

      do i=1,5
         ipart2(i)=ipart(i)
      enddo
      buff = VVVV_head2//coup1//VVVV_tail
      call add4vertex(ipart2,buff,info2,icoup)  !Send the W3W3 version

      endif
      end

      subroutine addWWWW1(ipart,coup1,coup2,info,icoup)
c**************************************************************************
c     Sets up the WWWW vertex with all the permutations
c     info is the color info for the WWWW configuration
c     coup1 is coupling of W w/ boson 1
c     coup2 is coupling of W w/ boson 2
c     Order of particles should be W- A W+ A
c     icoup is the coupling constant
c*************************************************************************
      implicit none

! Constants

      include 'params.inc'
      character*(*) VVVV_head                          
      parameter    (VVVV_head='WWWWXX(W(1,1???),W(1,2???),'//
     &                               'W(1,3???),W(1,4???),')
      character*(*) VVVV_tail                          
      parameter    (VVVV_tail=',AMP(????))')
      character*(*) JVVV1_head 
      parameter    (JVVV1_head='JWWWXX(W(1,1???),W(1,2???),W(1,3???),')
      character*(*) JVVV1_tail 
      parameter    (JVVV1_tail=',W(1,4???))')
      character*(*) JVVV2_head 
      parameter    (JVVV2_head='JWWWXX(W(1,2???),W(1,1???),W(1,4???),')
      character*(*) JVVV2_tail 
      parameter    (JVVV2_tail=',W(1,3???))')
      character*(*) JVVV3_head 
      parameter    (JVVV3_head='JWWWXX(W(1,1???),W(1,4???),W(1,3???),')
      character*(*) JVVV3_tail 
      parameter    (JVVV3_tail=',W(1,2???))')
      character*(*) JVVV4_head 
      parameter    (JVVV4_head='JWWWXX(W(1,2???),W(1,3???),W(1,4???),')
      character*(*) JVVV4_tail 
      parameter    (JVVV4_tail=',W(1,1???))')

c      character*(*) WMASS        , WWIDTH
c      parameter    (WMASS='WMASS', WWIDTH='WWIDTH')
      
! Arguments

      character*(*) coup1,coup2
      integer info(0:4),icoup(0:max_coup),ipart(0:5)

! Local
      
      integer ipart2(0:5),i
      character*(8) vmass1,vwidth1,vmass2,vwidth2,wmass3,wwidth3
      character*(max_string) buff


! Global

      character*(4*max_particles) particle(4)
      integer                               charge_c(max_particles)
      integer iparticle(0:max_particles,0:4),inverse(max_particles)
      common/to_model/iparticle,  particle,  inverse, charge_c

      character*(max_string) iwave(max_particles),owave(max_particles)
      character*(8) str(3,max_particles)
      integer info_p(5,max_particles),iposx(3,max_particles)
      common/to_external/iwave,owave,iposx,info_p,str

c-----
c  Begin Code
c-----
      if (ipart(0) .eq. 4) then
         wmass3  = str(2,ipart(1))
         wwidth3 = str(3,ipart(1))
         vmass1  = str(2,ipart(2))
         vwidth1 = str(3,ipart(2))
         vmass2  = str(2,ipart(4))
         vwidth2 = str(3,ipart(4))
         do i=0,5
            ipart2(i)=ipart(i)
         enddo
         buff = VVVV_head//coup1//','//coup2//VVVV_tail
         call add4vertex(ipart2,buff,info,icoup) !Send the WWWW version

      do i=0,5
         ipart2(i)=ipart(i)
      enddo
      ipart2(4)=1
      ipart2(5)=inverse(ipart(4))

      buff = JVVV1_head//coup1//','//coup2//','
     $        //vmass2//','//vwidth2//JVVV1_tail
      call add4vertex(ipart2,buff,info,icoup)

      if (ipart(3) .ne. ipart(4)) then       
         do i=0,5
            ipart2(i)=ipart(i)
         enddo
         ipart2(3) = 1
         ipart2(5) = inverse(ipart(3))
         buff = JVVV2_head//coup2//','//coup1//','
     $        //wmass3//','//wwidth3//JVVV2_tail
         call add4vertex(ipart2,buff,info,icoup)
      endif

      if (ipart(2) .ne. ipart(3) .and. ipart(2) .ne. ipart(4)) then       
         do i=0,5
            ipart2(i)=ipart(i)
         enddo
         ipart2(2) = 1
         ipart2(5) = inverse(ipart(2))
         buff = JVVV3_head//coup2//','//coup1//','//vmass1//','
     $        //vwidth1//JVVV3_tail
         call add4vertex(ipart2,buff,info,icoup)
      endif

      if ( ipart(1) .ne. ipart(3) .and.
     $     ipart(1) .ne. ipart(3) .and. ipart(1) .ne. ipart(4)) then       
         do i=0,5
            ipart2(i)=ipart(i)
         enddo
         ipart2(1) = 1
         ipart2(5) = inverse(ipart(1))
         buff = JVVV4_head//coup1//','//coup2//','
     $        //wmass3//','//wwidth3//JVVV4_tail
         call add4vertex(ipart2,buff,info,icoup)
      endif

      else
         print*,'Sorry must have 4 particles in W3W3',(ipart(i),i=0,4)
      endif
      end

      subroutine addW3W31(ipart,coup1,coup2,info,icoup)
c**************************************************************************
c     Sets up the W3W3 vertex with all the permutations
c     info is the color info for the W3W3 configuration
c     coup1 is coupling of W w/ boson 1
c     coup2 is coupling of W w/ boson 2
c     Order of particles should be W- A W+ A
c     icoup is the coupling constant
c*************************************************************************
      implicit none

! Constants

      include 'params.inc'
c      integer    max_string
c      parameter (max_string=120)
c      integer    max_coup
c      parameter (max_coup=5)
c      integer    max_particles       
c      parameter (max_particles=2**7-1)
      character*(*) VVVV_head                          
      parameter    (VVVV_head='W3W3XX(W(1,1???),W(1,2???),'//
     &                               'W(1,3???),W(1,4???),')
      character*(*) VVVV_tail                          
      parameter    (VVVV_tail=',AMP(????))')
      character*(*) JVVV1_head 
      parameter    (JVVV1_head='JW3WXX(W(1,1???),W(1,2???),W(1,3???),')
      character*(*) JVVV1_tail 
      parameter    (JVVV1_tail=',W(1,4???))')
      character*(*) JVVV2_head 
      parameter    (JVVV2_head='JW3WXX(W(1,2???),W(1,1???),W(1,4???),')
      character*(*) JVVV2_tail 
      parameter    (JVVV2_tail=',W(1,3???))')
      character*(*) JVVV3_head 
      parameter    (JVVV3_head='JW3WXX(W(1,1???),W(1,4???),W(1,3???),')
      character*(*) JVVV3_tail 
      parameter    (JVVV3_tail=',W(1,2???))')
      character*(*) JVVV4_head 
      parameter    (JVVV4_head='JW3WXX(W(1,2???),W(1,3???),W(1,4???),')
      character*(*) JVVV4_tail 
      parameter    (JVVV4_tail=',W(1,1???))')

c      character*(*) WMASS        , WWIDTH
c      parameter    (WMASS='WMASS', WWIDTH='WWIDTH')
      
! Arguments

      character*(*) coup1,coup2
      integer info(0:4),icoup(0:max_coup),ipart(0:5)

! Local
      
      integer ipart2(0:5),i
      character*(8) vmass1,vwidth1,vmass2,vwidth2,wmass3,wwidth3
      character*(max_string) buff


! Global

      character*(4*max_particles) particle(4)
      integer                               charge_c(max_particles)
      integer iparticle(0:max_particles,0:4),inverse(max_particles)
      common/to_model/iparticle,  particle,  inverse, charge_c

      character*(max_string) iwave(max_particles),owave(max_particles)
      character*(8) str(3,max_particles)
      integer info_p(5,max_particles),iposx(3,max_particles)
      common/to_external/iwave,owave,iposx,info_p,str

c-----
c  Begin Code
c-----
      if (ipart(0) .eq. 4) then
      vmass1  = str(2,ipart(2))
      vwidth1 = str(3,ipart(2))
      vmass2  = str(2,ipart(4))
      vwidth2 = str(3,ipart(4))
      wmass3  = str(2,ipart(3))
      wwidth3 = str(3,ipart(3))
      do i=0,5
         ipart2(i)=ipart(i)
      enddo
      buff = VVVV_head//coup1//','//coup2//VVVV_tail
      call add4vertex(ipart2,buff,info,icoup)  !Send the W3W3 version

      do i=0,5
         ipart2(i)=ipart(i)
      enddo
      ipart2(4)=1
      ipart2(5)=inverse(ipart(4))

      buff = JVVV1_head//coup1//','//coup2//','
     $       //vmass2//','//vwidth2//JVVV1_tail

c      write(*,*) 'Adding',buff

      call add4vertex(ipart2,buff,info,icoup)

      if (ipart(3) .ne. ipart(4)) then       
         do i=0,5
            ipart2(i)=ipart(i)
         enddo
         ipart2(3) = 1
         ipart2(5) = inverse(ipart(3))
         buff = JVVV2_head//coup2//','//coup1//','
     $        //wmass3//','//wwidth3//JVVV2_tail
         call add4vertex(ipart2,buff,info,icoup)
      endif

      if (ipart(2) .ne. ipart(3) .and. ipart(2) .ne. ipart(4)) then       
         do i=0,5
            ipart2(i)=ipart(i)
         enddo
         ipart2(2) = 1
         ipart2(5) = inverse(ipart(2))
         buff = JVVV3_head//coup2//','//coup1//','
     $        //vmass1//','//vwidth1//JVVV3_tail
         call add4vertex(ipart2,buff,info,icoup)
      endif

      if ( ipart(1) .ne. ipart(3) .and.
     $     ipart(1) .ne. ipart(3) .and. ipart(1) .ne. ipart(4)) then       
         do i=0,5
            ipart2(i)=ipart(i)
         enddo
         ipart2(1) = 1
         ipart2(5) = inverse(ipart(1))
         buff = JVVV4_head//coup1//','//coup2//','
     $        //wmass3//','//wwidth3//JVVV4_tail
         call add4vertex(ipart2,buff,info,icoup)
      endif

      else
         print*,'Sorry must have 4 particles in W3W3',(ipart(i),i=0,4)
      endif
      end

      subroutine addVVSS1(ipart,coup,info,icoup)
c**************************************************************************
c     Sets up the VVSS vertex with all the permutations
c     info is the color info for the VVSS configuration
c     coup is coupling of W w/ boson 1
c     Order of particles should be VVSS
c     icoup is the coupling constant
c*************************************************************************
      implicit none

! Constants

      include 'params.inc'
      character*(*) VVSS_head                          
      parameter    (VVSS_head='VVSSXX(W(1,1???),W(1,2???),'//
     &                               'W(1,3???),W(1,4???),')
      character*(*) VVSS_tail                          
      parameter    (VVSS_tail=',AMP(????))')
      character*(*) HVVS1_head 
      parameter    (HVVS1_head='HVVSXX(W(1,1???),W(1,2???),W(1,3???),')
      character*(*) HVVS1_tail 
      parameter    (HVVS1_tail=',W(1,4???))')
      character*(*) HVVS2_head 
      parameter    (HVVS2_head='HVVSXX(W(1,2???),W(1,1???),W(1,4???),')
      character*(*) HVVS2_tail 
      parameter    (HVVS2_tail=',W(1,3???))')
      character*(*) JVSS1_head 
      parameter    (JVSS1_head='JVSSXX(W(1,1???),W(1,4???),W(1,3???),')
      character*(*) JVSS1_tail 
      parameter    (JVSS1_tail=',W(1,2???))')
      character*(*) JVSS2_head 
      parameter    (JVSS2_head='JVSSXX(W(1,2???),W(1,3???),W(1,4???),')
      character*(*) JVSS2_tail 
      parameter    (JVSS2_tail=',W(1,1???))')

      
! Arguments

      character*(*) coup
      integer info(0:4),icoup(0:max_coup),ipart(0:5)

! Local
      
      integer ipart2(0:5),i
      character*(8) vmass,vwidth,smass,swidth
      character*(max_string) buff


! Global

      character*(4*max_particles) particle(4)
      integer                               charge_c(max_particles)
      integer iparticle(0:max_particles,0:4),inverse(max_particles)
      common/to_model/iparticle,  particle,  inverse, charge_c

      character*(max_string) iwave(max_particles),owave(max_particles)
      character*(8) str(3,max_particles)
      integer info_p(5,max_particles),iposx(3,max_particles)
      common/to_external/iwave,owave,iposx,info_p,str

c-----
c  Begin Code
c-----
      if (ipart(0) .eq. 4) then
      do i=0,5
         ipart2(i)=ipart(i)
      enddo
      buff = VVSS_head//coup//VVSS_tail
      call add4vertex(ipart2,buff,info,icoup)  !Send the VVSS version

      do i=0,5
         ipart2(i)=ipart(i)
      enddo
      ipart2(4)=1
      ipart2(5)=inverse(ipart(4))
      smass  = str(2,ipart(4))
      swidth = str(3,ipart(4))

      buff = HVVS1_head//coup//','//smass//','//swidth//HVVS1_tail
      call add4vertex(ipart2,buff,info,icoup)

      if (ipart(3) .ne. ipart(4)) then       
         do i=0,5
            ipart2(i)=ipart(i)
         enddo
         smass  = str(2,ipart(4))
         swidth = str(3,ipart(4))
         ipart2(3) = 1
         ipart2(5) = inverse(ipart(3))
         buff=HVVS2_head//coup//','//smass//','//swidth//HVVS2_tail
         call add4vertex(ipart2,buff,info,icoup)
      endif

      do i=0,5
         ipart2(i)=ipart(i)
      enddo
      ipart2(2) = 1
      ipart2(5) = inverse(ipart(2))
      vmass  = str(2,ipart(2))
      vwidth = str(3,ipart(2))
      buff = JVSS1_head//coup//','//vmass//','//vwidth//JVSS1_tail
      call add4vertex(ipart2,buff,info,icoup)

      if (ipart(1) .ne. ipart(2)) then       
         do i=0,5
            ipart2(i)=ipart(i)
         enddo
         vmass  = str(2,ipart(1))
         vwidth = str(3,ipart(1))
         ipart2(1) = 1
         ipart2(5) = inverse(ipart(1))
         buff=JVSS2_head//coup//','//vmass//','//vwidth//JVSS2_tail
         call add4vertex(ipart2,buff,info,icoup)
      endif

      else
         print*,'Sorry must have 4 particles in W3W3',(ipart(i),i=0,4)
      endif
      end
