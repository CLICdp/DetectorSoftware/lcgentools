!-----------------------
! madgraph - a Feynman Diagram package by Tim Stelzer and Bill Long 
! (c) 1993
!
! Filename: logo.f
!-----------------------

      subroutine writelogo
      implicit none

! Local Variables

      character*40 left(16)
      character*35 right(16)
      integer i

! Data Tables

      data left/
     & '                                        ',
     & '  "$o   o$"              "$    o""""$   ',
     & '   $"o o"$     ooo    ooo $   $     "   ',
     & '   $ "o" $    "   $  $   "$   $         ',
     & '   $  "  $   o""""$  $    $   $   ""$"  ',
     & '  o$o   o$o  "ooo"$o "ooo"$o   "oooo"   ',
     & '                                        ',
     & '                                        ',
     & '    o                        o          ',
     & '     ""o                  o""           ',
     & '        ""oo          oo""           o  ',
     & '           o""""""""""o            oo$oo',
     & '        o""            ""o           $  ',
     & '     oo"                  "oo           ',
     & '    "                        "          ',
     & '                                        '/

      data right/
     & '                                   ',
     & '                          "$       ',
     & 'oo ooo     ooo   oo ooo    $ ooo   ',
     & ' $"  "    "   $   $"   $   $"   $  ',
     & ' $       o""""$   $    $   $    $  ',
     & 'o$oo     "ooo"$o  $"ooo"  o$o  o$o ',
     & '                  $                ',
     & '                 """               ',
     & '     oooooooooooooooooooooooooo    ',
     & '                  $                ',
     & '                  $                ',
     & '                  $                ',
     & '                  $                ',
     & '                  $                ',
     & '     """"""""""""""""""""""""""    ',
     & '                                   '/

!-----------
! Begin Code
!-----------
      write(*,'(a40,a35)')  (left(i),right(i),i=1,16)
      end
