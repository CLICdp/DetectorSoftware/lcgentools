!-----------------------
! madgraph - a Feynman Diagram package by Tim Stelzer and Bill Long 
! (c) 1993
!
! Filename: drawfeyn.f
!-----------------------

!*************************************************************************
!     This file contains routines for generating ps files for the Feynman
!     diagrams.
!*************************************************************************


      Subroutine PositionVerts(graphs,tops,igraph,next)
!*************************************************************************
!     For graph i determine an asthetic placement of the vertices and
!     external lines.  Basic idea is to minimize Sum(length^2) of all lines
!*************************************************************************
      implicit none

! Constants

      integer    maxtops,     maxnodes,  maxgraphs
      parameter (maxtops=2500,maxnodes=5,maxgraphs=999)
      integer    maxlines,  maxfactors,  maxterms
      parameter (maxlines=8,maxfactors=9,maxterms=250)
      integer    maxrows,  maxcols
      parameter (maxrows=6,maxcols=720)
      integer    graphs_per_row,   scale,    rows_per_page
      parameter (graphs_per_row=3, scale=10, rows_per_page=5)
      integer    graphs_per_page
      parameter (graphs_per_page=rows_per_page*graphs_per_row)
! Arguments

      integer graphs(0:maxgraphs,0:maxlines) 
      integer tops(0:4,0:maxnodes,0:maxtops),next
      integer igraph
      
! Local Variables

      integer nverts,ntop
      integer i,iy,xoff,yoff,nden
      integer jline(-maxlines:maxlines,2)
      integer c(-maxlines:maxnodes,maxrows)
      integer lnum,ivert,nlines
      real xshift,yshift,x1,x2,y1,y2,ypos(maxlines)
      logical done,changed
      real pverts(-maxlines:maxlines,2),goodverts(-maxlines:maxlines,2)
      integer perms(maxrows,maxcols),k(maxrows),count,iter
      real mysum,minsum
      integer rows,irow,icol,pgraph,npage

! External Functions

      integer inverse

! Global Variables

      integer        iline(-maxlines:maxlines)
      common/to_proc/iline

!-----------
! Begin Code
!-----------

! Initialize things

      pgraph = mod(igraph-1,graphs_per_page)+1
      if (pgraph .eq. 1) then
         npage = (igraph-1)/graphs_per_page + 1
         xoff = graphs_per_row*15*scale
         yoff = (rows_per_page*15)*scale
         if (npage .gt. 1) write(4,'(a)') 'showpage'
         write(4,'(a,2I8)') '%%Page:',npage,npage
         write(4,'(a)') '%%PageBoundingBox:+40 +20 540 810'
         write(4,'(a)') '%%PageFonts: Helvetica'
         write(4,'(a)') '/Helvetica findfont 10 scalefont setfont'
         write(4,*) xoff/2+15,yoff+20,' moveto'
         write(4,'(a)') '(Diagrams by MadGraph) show'
      endif
      icol = mod(pgraph-1,graphs_per_row)
      irow = rows_per_page-int((pgraph-1)/graphs_per_row)-1
      xoff = icol*15*scale+100
      yoff = irow*15*scale+50
      pverts(-1,1) = 0
      pverts(-1,2) = 10
      pverts(-2,1) = 0
      pverts(-2,2) = 0
      iy = 10
      do i=3,next
         ypos(i-2)=iy
         if (next .eq. 3) then
            ypos(1) = 5
         else
            iy = iy - 10/(next-3)
         endif
      enddo
      ntop   = graphs(igraph,0)
      nverts = tops(1,0,ntop)

! Determine which vertices are connected set up C(iline,ivert)

      do i=-maxlines,maxlines
         jline(i,1)=0
         jline(i,2)=0
      enddo
      do i=1,next
         jline(i,1) =-i
         jline(i,2) =-i
      enddo
      do ivert = 1,nverts
         nlines = tops(0,ivert,ntop)
         do i = 1,nlines
            lnum=tops(i,ivert,ntop)
            if (jline(lnum,1) .eq. 0) then
               jline(lnum,1) = i
               jline(lnum,2) = ivert
            elseif (jline(lnum,1) .lt. 0) then
               C(i,ivert) = jline(lnum,2)      != -lnum
               jline(lnum,1) = ivert
            else
               C(i,ivert) = jline(lnum,2)
               C(jline(lnum,1),jline(lnum,2))= ivert
               jline(lnum,1) = ivert
            endif
         enddo
      enddo

! Loop over all configurations of final legs orders

      do i=1,next-2
         k(i)=i
      enddo
      count=0
      if (next .eq. 3) then
         rows=1
         count=1
         perms(1,1)=1
      elseif (next .eq. 4) then
         rows = 2
         call perm2(k,perms,rows,count)
      elseif (next .eq. 5) then
         rows = 3
         call perm3(k,perms,rows,count)
      elseif (next .eq. 6) then
         rows = 4
         call perm4(k,perms,rows,count)
      elseif (next .eq. 7) then
         rows = 5
         call perm5(k,perms,rows,count)
      endif

      minsum = 99999
      do iter=1,count
         do i=3,next
            if (next .le. 4) then
               pverts(-i,1)=10
            elseif(next .eq. 5) then
               pverts(-i,1)=9
            else
               pverts(-i,1)=8
            endif
            pverts(-i,2)=ypos(perms(i-2,iter))
         enddo

! Guess initial values for vertices, neg = external

         do ivert=1,nverts
            pverts(i,1)=0.
            pverts(i,2)=0.
         enddo
         do ivert=1,nverts
            xshift = 0
            yshift = 0
            nlines = tops(0,ivert,ntop)
            nden=0
            do i=1,nlines
               if (c(i,ivert) .lt. 0) then            !This is external 
                  xshift = xshift + pverts(c(i,ivert),1)
                  yshift = yshift + pverts(c(i,ivert),2)
                  nden=nden+1
               endif
            enddo
            if (nden .gt. 0) then
               pverts(ivert,1)=xshift/nden
               pverts(ivert,2)=yshift/nden
            else
               pverts(ivert,1)=ivert
               pverts(ivert,2)=ivert
            endif
         enddo


! Now try to order

      done=.false.
      changed = .true.
      do while (.not. done)
         done=.true.
         do ivert=1,nverts
            xshift = 0
            yshift = 0
            nlines = tops(0,ivert,ntop)
            do i=1,nlines
               xshift = xshift + pverts(c(i,ivert),1)
               yshift = yshift + pverts(c(i,ivert),2)
            enddo
            if (abs(pverts(ivert,1)-xshift/nlines) .gt. .1) done=.false.
            if (abs(pverts(ivert,2)-yshift/nlines) .gt. .1) done=.false.
            pverts(ivert,1)=xshift/nlines
            pverts(ivert,2)=yshift/nlines
         enddo
      enddo

      mysum = 0
      do i=1,next
         mysum=mysum + (pverts(jline(i,1),1)-pverts(jline(i,2),1))**2
         mysum=mysum + (pverts(jline(i,1),2)-pverts(jline(i,2),2))**2
      enddo
      do i=1,nverts-1
         mysum=mysum + (pverts(jline(-i,1),1)-pverts(jline(-i,2),1))**2
         mysum=mysum + (pverts(jline(-i,1),2)-pverts(jline(-i,2),2))**2
      enddo
      if (mysum .ge. 99999) then
         print*,'Warning mysum is large',mysum
      endif
      if (mysum .lt. minsum+.1) then
         minsum = mysum
         do i=-maxlines,maxlines
            goodverts(i,1) = pverts(i,1)
            goodverts(i,2) = pverts(i,2)
         enddo
      endif

      enddo          !iteration

      do i=-maxlines,maxlines
         pverts(i,1) = goodverts(i,1)
         pverts(i,2) = goodverts(i,2)
      enddo

      if (minsum .eq. 99999) then
         print*,'Warning graph not drawn',igraph
         return
      endif
      do i=3,next
         pverts(-i,1) = 10
      enddo
      do i=1,next
         x1 = 10.*pverts(jline(i,1),1)+xoff
         y1 = 10.*pverts(jline(i,1),2)+yoff
         x2 = 10.*pverts(jline(i,2),1)+xoff
         y2 = 10.*pverts(jline(i,2),2)+yoff
         if (i .le. 2) then
            call drawline(x2,y2,x1,y1,inverse(iline(i)))
         else
            call drawline(x1,y1,x2,y2,iline(i))
         endif
      enddo
      do i=1,nverts-1
         x1 = 10.*pverts(jline(-i,1),1)+xoff
         y1 = 10.*pverts(jline(-i,1),2)+yoff
         x2 = 10.*pverts(jline(-i,2),1)+xoff
         y2 = 10.*pverts(jline(-i,2),2)+yoff
         call drawline(x1,y1,x2,y2,iline(-i))
      enddo
      write(4,*) xoff+40,yoff-10,'  moveto'
      write(4,'(a,i4,a)') '(graph ',igraph,')   show'
      write(4,*) xoff-10,yoff+98,'  moveto'
      write(4,*) '(1)   show'
      write(4,*) xoff-10,yoff-2,'  moveto'
      write(4,*) '(2)   show'
      do i=3,next
         write(4,*) xoff+100,yoff+10*pverts(-i,2),'  moveto'
         write(4,'(a,i3,a)') '(',i,') show'
      enddo
      end


      Subroutine drawline(x1,y1,x2,y2,itype)
!***************************************************************************
!     Routine to draw postscript for feynman diagram line
!     from x1,y1 to x2,y2 with appropriate label
!***************************************************************************
      implicit none
      
! Arguments

      real x1,y1,x2,y2,dx,dy,d
      integer itype

! Local Variables

      integer i
      character*2 names(-30:30)
      data (names(i),i=1,6)     /'d ','u ','s ','c ','b ','t '/
      data (names(i),i=-6,-1)   /'t ','b ','c ','s ','u ','d '/
      data names(9) /'  '/
      data (names(i),i=11,16)   /'e ','ve','mu','vu','ta','vt'/
      data (names(i),i=-16,-11) /'vt','ta','vu','mu','ve','e '/
      data (names(i),i=22,25)  /'A ','Z ','W ','  '/
      data names(-24) /'W '/
!-----------
! Begin Code
!-----------
      d  = sqrt((x1-x2)**2+(y1-y2)**2)
      dx = (x1-x2)/d
      dy = (y1-y2)/d

      if (dy .lt. 0) then
         dy=-dy
         dx=-dx
      endif

      if (itype .eq. 9) then
         write(4,*) x1,y1,x2,y2,' 0  Fgluon'
      elseif (abs(itype) .lt. 20) then
         if (itype .gt. 0) then
            write(4,*) x1,y1,x2,y2,'  Ffermion'
         else
            write(4,*) x2,y2,x1,y1,'  Ffermion'
         endif
      elseif(itype .eq. 25) then
         write(4,*) x1,y1,x2,y2,'  Fhiggs'
      else
         write(4,*) x1,y1,x2,y2,' 0  Fphoton'
      endif
      write(4,*) (x1+x2)/2.+5.*dy, (y1+y2)/2.-5.*dx-4., '  moveto'
      write(4,'(3a)') '(',names(itype),')   show'
      end
