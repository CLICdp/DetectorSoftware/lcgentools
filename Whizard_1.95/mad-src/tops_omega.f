
      Subroutine tops3(tops)
!*************************************************************************
!     Initialize the smallest graph possible, 3 external lines
!     which can only have one node
!*************************************************************************
      implicit none

! Constants

      integer    maxtops,      maxnodes
      parameter (maxtops=2500,maxnodes=5)

! Arguments

      integer tops(0:4,0:maxnodes,0:maxtops)

!-----------
! Begin Code
!-----------

      tops(0,0,0)=1                !number of tops
      tops(1,0,0)=3                !number of external lines
      tops(0,0,1)=0                !number of internal lines graph 1
      tops(1,0,1)=1                !number of vertices for graph 1
      tops(2,0,1)=1                !number of 3 vertices for graph 1
      tops(3,0,1)=0                !number of 4 vertices for graph 1
      tops(0,1,1)=3                !first vertex has 3 lines
      tops(1,1,1)=1                !first lines in vertex1 is line1
      tops(2,1,1)=2                !second lines in vertex1 is line2
      tops(3,1,1)=3                !third lines in vertex1 is line3
      tops(4,1,1)=0                !this is meaningless but set to zero
      end

      Subroutine addExternal(oldtops,newtops)
!*************************************************************************
!     Add one external leg to oldgraphs topologies and store in
!     newgraphs topologies
!*************************************************************************
      implicit none

! Constants

      integer    maxtops,     maxnodes
      parameter (maxtops=2500,maxnodes=5)

! Arguments

      integer oldtops(0:4,0:maxnodes,0:maxtops)
      integer newtops(0:4,0:maxnodes,0:maxtops)

! Local Variables

      integer oldg,oldvert,oldline

!-----------
! Begin Code
!-----------
      newtops(0,0,0)=0                    !no new topologies
      newtops(1,0,0)=oldtops(1,0,0)+1     !adding one external line

      do oldg=1,oldtops(0,0,0)            !loop over each of the old topolog
         do oldline = 1,oldtops(1,0,0)
            call copytops(oldtops,newtops,oldg)
            call splitline(newtops,oldline)
         enddo
         do oldline = -1,oldtops(0,0,oldg),-1   !internal lines are negative
            call copytops(oldtops,newtops,oldg)
            call splitline(newtops,oldline)
         enddo
         do oldvert=1,oldtops(1,0,oldg)   !check each old vertex
            if (oldtops(0,oldvert,oldg) .eq. 3) then    !3 vertex can add
               call copytops(oldtops,newtops,oldg)
               call splitvertex(newtops,oldvert)
            endif
         enddo
      enddo
      end

      Subroutine copyTops(oldtops,newtops,oldg)
!*************************************************************************
!     copy topology from oldgraphs to newgraphs
!*************************************************************************
      implicit none

! Constants

      integer    maxtops,      maxnodes
      parameter (maxtops=2500,maxnodes=5)

! Arguments

      integer oldtops(0:4,0:maxnodes,0:maxtops)
      integer newtops(0:4,0:maxnodes,0:maxtops)
      integer oldg

! Local Variables

      integer ivert,iline,ntops,i

!-----------
! Begin Code
!-----------
      newtops(0,0,0) = newtops(0,0,0)+1            !adding one new topology
      ntops=newtops(0,0,0)
      do i=0,4
         newtops(i,0,ntops)=oldtops(i,0,oldg)      !Basic topology info
      enddo
      do ivert=1,oldtops(1,0,oldg)                 !loop over all vertices
         do iline=0,4
            newtops(iline,ivert,ntops)=oldtops(iline,ivert,oldg)
         enddo
      enddo
      end

      Subroutine splitvertex(newtops,nvert)
!*************************************************************************
!     copy topology from oldgraphs to newgraphs
!*************************************************************************
      implicit none

! Constants

      integer    maxtops,     maxnodes
      parameter (maxtops=2500,maxnodes=5)

! Arguments

      integer newtops(0:4,0:maxnodes,0:maxtops),nvert

! Local Variables

      integer ntops

!-----------
! Begin Code
!-----------
      ntops=newtops(0,0,0)                !always working on the newest top.
      if (newtops(0,nvert,ntops) .ne. 3) then
         print*,'Error splitting vertex of non-3vertex',newtops
         stop
      endif
      newtops(0,nvert,ntops) = 4                   !now its a 4 vertex
      newtops(4,nvert,ntops) = newtops(1,0,0)      !we are adding the newest Ext
      newtops(2,0,ntops) = newtops(2,0,ntops) -1   !one less 3 vertex
      newtops(3,0,ntops) = newtops(3,0,ntops) +1   !one more 4 vertex
      end

      Subroutine splitline(newtops,nline)

!*************************************************************************
!     copy topology from oldgraphs to newgraphs
!*************************************************************************
      implicit none

! Constants
      integer    maxtops,      maxnodes
      parameter (maxtops=2500,maxnodes=5)

! Arguments

      integer newtops(0:4,0:maxnodes,0:maxtops),nline

! Local

      integer nvert,ninternal,ntops
      integer nfound,convert(2),conline(2),i,j

!-----------
! Begin Code
!-----------
      ntops=newtops(0,0,0)                !always working on the newest top.
      nfound=0
      do i=1,newtops(1,0,ntops)           !look through all vertices
         do j=1,newtops(0,i,ntops)        !either 3 or 4 lines to check
            if (newtops(j,i,ntops) .eq. nline) then
               nfound=nfound+1
               convert(nfound)=i
               conline(nfound)=j
            endif
         enddo
      enddo
      if (nline .lt. 0 .and. nfound .ne. 2) then
         print*,'Splitting internal line, but not enough nodes',nfound
         print*,'Graph number',ntops,convert(1),convert(2),nline
         stop
      elseif(nline .gt. 0 .and.  nfound .ne. 1) then
         print*,'Splitting external line, but not enough nodes',nfound
         print*,'Graph number',ntops,convert(1),convert(2),nline
      endif
      newtops(0,0,ntops) = newtops(0,0,ntops) -1     !one more internal line (-)
      newtops(1,0,ntops) = newtops(1,0,ntops) +1     !one more vertex
      newtops(2,0,ntops) = newtops(1,0,ntops) +1     !one more 3 vertex
      nvert=newtops(1,0,ntops)                       !number of new vertex
      ninternal=newtops(0,0,ntops)                   !number of new internal line
      newtops(0,nvert,ntops) = 3                     !Created 3 vertex
      newtops(1,nvert,ntops) = newtops(1,0,0)        !New External line
      newtops(2,nvert,ntops) = nline                 !original split line
      newtops(3,nvert,ntops) = ninternal             !new internal line
      newtops(4,nvert,ntops) = 0                     !Not Used set to zero
      newtops(conline(1),convert(1),ntops)=ninternal !change other node of int
      end


      Subroutine SortVertex(tops)
!*************************************************************************
!     Put vertices for each topology in an intelligent order
!     So that after ones before it are evaluated, each has only 1 unknown
!     wave function (line), and put that line at the end
!*************************************************************************
      implicit none
      
! Constants

      integer    maxtops,     maxnodes
      parameter (maxtops=2500,maxnodes=5)
      integer   maxlines
      parameter(maxlines=8)

! Arguments

      integer  tops(0:4,0:maxnodes,0:maxtops)

! Local

      integer itemp
      integer ntop,iorder(maxnodes),nvert,iline(-maxlines:maxlines),ngot
      integer i,ndone
      logical done

!-----------
! Begin Code
!-----------
      do ntop=1,tops(0,0,0)       !sum over each topology
         done=.false.
         ndone=0
         do i=1,maxlines
            iline(i)  = 1         !external lines are positive and known
            iline(-i) = 0         !internal lines neg and not known
         enddo
         nvert = 0
         do while (.not. done)
            nvert=nvert+1
            if (nvert .gt. tops(1,0,ntop)) nvert=1
            ngot=0
            do i=1,tops(0,nvert,ntop)
               if (iline(tops(i,nvert,ntop)) .ne. 0) ngot=ngot+1
            enddo
            if (tops(0,nvert,ntop) - ngot .eq. 1) then
               ndone=ndone+1
               iorder(ndone) = nvert
               do i=1,tops(0,nvert,ntop)
                  if (iline(tops(i,nvert,ntop)) .eq. 0) then !this is uncalced 
                     iline(tops(i,nvert,ntop)) = 1
                     itemp=tops(i,nvert,ntop)                !move to last position
                     tops(i,nvert,ntop) =
     &                    tops(tops(0,nvert,ntop),nvert,ntop)
                     tops(tops(0,nvert,ntop),nvert,ntop) = itemp
                  endif
               enddo
            endif
            if (tops(0,nvert,ntop) - ngot .eq. 0) then   !have them all
               if (ndone .eq. tops(1,0,ntop)-1)   then   !last one
                  done=.true.
                 do i=1,ndone
                     if (iorder(i) .eq. nvert) then
                        done=.false.
                     endif
                  enddo
                  if (done) then
                     ndone=ndone+1
                     iorder(ndone)=nvert
                  endif
               endif
            endif
         enddo
         call orderverts(tops,iorder,ntop)
      enddo
      end

      Subroutine orderverts(tops,iorder,ntop)
!************************************************************************
!     Places vertices in the order specified by iorder which
!     should assure all lines except one are known at each node
!************************************************************************
      implicit none

! Constants

      integer    maxtops,     maxnodes
      parameter (maxtops=2500,maxnodes=5)

! Arguments

      integer tops(0:4,0:maxnodes,0:maxtops)
      integer ntop,iorder(maxnodes)

! Local

      integer iline,vtemp(0:4,0:maxnodes)
      integer i

!-----------
! Begin Code
!-----------
      do i=1,tops(1,0,ntop)
         do iline=0,4
            vtemp(iline,i) = tops(iline,i,ntop)
         enddo
      enddo
      do i=1,tops(1,0,ntop)
         do iline=0,4
            tops(iline,i,ntop) = vtemp(iline,iorder(i))
         enddo
      enddo
      end

