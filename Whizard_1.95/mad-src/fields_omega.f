!-----------------------
! madgraph - a Feynman Diagram package by Tim Stelzer and Bill Long 
! (c) 1993
!
! Filename: fields.f
!-----------------------
               
      Subroutine InsertFields(tops,graphs)
!****************************************************************************
!     Given a topology list, and the external particles, check which
!     graphs work, and which don't
!****************************************************************************
      implicit none

! Constants

      integer    maxtops,     maxnodes,  maxgraphs
      parameter (maxtops=2500,maxnodes=5,maxgraphs=999)
      integer    maxlines,  maxfactors,  maxterms
      parameter (maxlines=8,maxfactors=9,maxterms=250)

! Arguments

      integer tops(0:4,0:maxnodes,0:maxtops)
      integer graphs(0:maxgraphs,0:maxlines) !graphs, topology internal lines

! Local Variables

      integer i,nvert,n(maxnodes),nextchange,ntop
      integer ngraphs,n4glue,nqcd,nqed,nbadgraphs
      logical done,fail,oneit,more

! Global Variables

      integer        iline(-maxlines:maxlines)
      common/to_proc/iline
      integer         noqcd,noqed,nqfd
      common/to_order/noqcd,noqed,nqfd
      integer        ibad(maxgraphs)
      common/to_bad/ ibad
      character*15 name
      integer iname
      common/to_name/iname,name

!-----------
! Begin Code
!-----------
      do i=1,maxgraphs
         ibad(i) = 0
      enddo
      i=0
      open(unit=22, file='.'//name(1:iname),status='old',err=11)
      do while (i .lt. maxgraphs)
         i=i+1
         read(22,*,end=11) ibad(i)
      enddo
 11   if (i .gt. 0) then
         write(*,*) 'Deleting graphs in file .',name(1:iname)
      endif
      ngraphs=0
      nbadgraphs=1
      graphs(0,0)=0
      do ntop=1,tops(0,0,0)
         done=.false.
         do i=1,maxnodes
            n(i)=1
         enddo
         do while (.not. done)
            do i=1,maxlines
               iline(-i) = 0
            enddo
            nvert=0
            n4glue=0                          !# of 4 glue vert found in graph
            oneit=.true.
            nextchange=0
            do while (oneit)
               nvert=nvert+1
               call checkvert(tops(0,nvert,ntop),
     &              iline(tops(1,nvert,ntop)),
     &              iline(tops(2,nvert,ntop)),
     &              iline(tops(3,nvert,ntop)),
     &              iline(tops(4,nvert,ntop)),
     &              n(nvert),fail, more)
               if (.not. fail .and. tops(0,nvert,ntop) .eq. 4) then 
                     n4glue=n4glue+1
                     iline(-maxlines+n4glue) = n(nvert)
               endif
               if (more) nextchange=nvert
               if (fail) oneit=.false.
               if (nvert .eq. tops(1,0,ntop) .and. .not. fail) then
                  oneit=.false.                     !this iteration finished
                  nqed=0
                  nqcd=0
                  do i=1,tops(1,0,ntop)          !check all vertices
                     if (tops(0,i,ntop) .eq. 3) then         !3 vertex
                        if (iline(tops(1,i,ntop)) .eq. 9 .or.
     &                      iline(tops(2,i,ntop)) .eq. 9 .or.
     &                      iline(tops(3,i,ntop)) .eq. 9) then
                           nqcd=nqcd+1
                        elseif (iline(tops(1,i,ntop)) .eq. 22 .or.
     &                      iline(tops(2,i,ntop)) .eq. 22 .or.
     &                      iline(tops(3,i,ntop)) .eq. 22) then
                           nqed=nqed+1
                        elseif (iline(tops(1,i,ntop)) .eq. 23 .or.
     &                      iline(tops(2,i,ntop)) .eq. 23 .or.
     &                      iline(tops(3,i,ntop)) .eq. 23) then
                           nqed=nqed+1
                        elseif (abs(iline(tops(1,i,ntop))) .eq. 24 .or.
     &                      abs(iline(tops(2,i,ntop))) .eq. 24 .or.
     &                      abs(iline(tops(3,i,ntop))) .eq. 24) then
                           nqed=nqed+1
                        elseif (abs(iline(tops(1,i,ntop))) .eq. 25 .or.
     &                      abs(iline(tops(2,i,ntop))) .eq. 25 .or.
     &                      abs(iline(tops(3,i,ntop))) .eq. 25) then
                           nqed=nqed+1
                        else
                           print*,'Error could not find type!',
     &                          iline(tops(1,i,ntop)),
     &                          iline(tops(2,i,ntop)),
     &                          iline(tops(3,i,ntop))
                        endif
                     elseif(tops(0,i,ntop) .eq. 4) then         !4 vertex
                        if (iline(tops(1,i,ntop)) .eq. 9 .or.
     &                      iline(tops(2,i,ntop)) .eq. 9 .or.
     &                      iline(tops(3,i,ntop)) .eq. 9) then
                           nqcd=nqcd+2
                        else
                           nqed=nqed+2
                        endif
                     endif
                  enddo
                  if (nqed .ne. noqed .and. .not. fail) then
                     fail =.true.
                  endif
                  if (nqcd .ne. noqcd .and. .not. fail) then
                     fail =.true.
                  endif
c
c     This is check to see if we should throw this graph out
c
                  if (.not. fail) then
                     if (ngraphs+nbadgraphs .eq. ibad(nbadgraphs)) then
                        nbadgraphs=nbadgraphs+1
                        fail = .true.
                     endif
                  endif
                  if (.not. fail) then
                     ngraphs=ngraphs+1
                     if (ngraphs .le. maxgraphs) then
                        graphs(0,0)=ngraphs
                        graphs(ngraphs,0)=ntop
                        do i=1,maxlines
                           graphs(ngraphs,i) = iline(-i)
                        enddo
                     elseif (ngraphs .eq. maxgraphs+1) then
                        print*,'Sorry Maxgraphs is set to ',maxgraphs
                     endif
                  endif
               endif
            enddo
            if (nextchange .ne. 0) then
               n(nextchange) = n(nextchange)+1
               do i=nextchange+1,maxnodes
                  n(i)=1
               enddo
            else
               done=.true.
            endif
            nextchange=0
         enddo
      enddo
      end

      Subroutine checkvert(nlines,l1,l2,l3,l4,n,fail,more)
!***************************************************************************
!     Checks to see if the vertex is allowed, and how many different ways
!     it can be allowed if only two particles are given
!***************************************************************************
      implicit none

! Arguments

      integer l1,l2,l3,l4,n,nlines
      logical fail,more

! Local Variables

      integer itest
      logical vertex_ok

! External Functions

      integer v3,v4

! Global Variables

      integer         noqcd,noqed,nqfd
      common/to_order/noqcd,noqed,nqfd


!-----------
! Begin Code
!-----------

      if (nlines .eq. 3) then
         itest = v3(l1,l2,l3,n)
         vertex_ok  = itest .ne. 0
         more = vertex_ok .and. v3(l1,l2,l3,n+1) .ne. 0
         if (vertex_ok) then
            if (l1 .eq. 0) l1 = itest
            if (l2 .eq. 0) l2 = itest
            if (l3 .eq. 0) l3 = itest
         endif
      elseif (nlines .eq. 4) then
         itest=v4(l1,l2,l3,l4,n)
         vertex_ok = itest .ne. 0
         more = vertex_ok .and. v4(l1,l2,l3,l4,n+1) .ne. 0
         if (vertex_ok) then
            if (l1 .eq. 0) l1 = itest
            if (l2 .eq. 0) l2 = itest
            if (l3 .eq. 0) l3 = itest
            if (l4 .eq. 0) l4 = itest
         endif
      else
         more =      .false.
         vertex_ok = .false.
      endif
      fail = .not. vertex_ok
      end

!----------------------------------------------------               
      integer function v4(l1,l2,l3,l4,n)
      implicit none

! Constants

      integer(8) select(5)
      integer(8) mask(5)
      integer   maxv4
      parameter(maxv4=64)

! Arguments

      integer l1,l2,l3,l4,n

! Local Variables

      integer i,j,nfound,vertex,shift
      logical used(maxv4)

! External Functions

      integer packv4,inverse,iand,ior

! Global Variables

      integer           nv4,v4_table(maxv4)
      common /v4_block/ nv4,v4_table

! Tables for bit masks

      data    mask  /z'00ffffff',z'ff00ffff',z'ffff00ff',z'ffffff00',0/
      data    select/z'ff000000',z'00ff0000',z'0000ff00',z'000000ff',0/

!-----------
! Begin Code
!-----------
      vertex = packv4(l1,l2,l3,l4)
      nfound = 0
      if (iand(vertex,select(1)) .ne. 0) then    ! all 4 lines supplied
         do i=1,nv4
            if (vertex .eq. v4_table(i)) then
               nfound = nfound + 1
               if ( nfound .eq. n) then
                  v4 = 1
                  return
               end if
            end if
         end do
      else               ! one line is missing     
         do i=1,nv4
            used(i) = .false.
         end do
         shift = 256*256*256
         do j=1,4
            do i=1,nv4
               if (vertex .eq. iand(v4_table(i),mask(j))) then
                  if (.not. used(i)) then
                     nfound = nfound + 1
                     used(i) = .true.
                  end if
                  if (nfound .eq. n) then
                     v4 = iand(v4_table(i),select(j))/shift
                     if (v4 .gt. 100) v4 = 100-v4
                     v4 = inverse(v4)
                     return
                  end if
               end if
            end do
            vertex = ior(iand(vertex,select(j+1))*256,
     &                   iand(vertex,  mask(j+1)))
            shift = shift/256
         end do
      end if
      v4 = 0
      end

!----------------------------------------------------

      Subroutine PrintGraph(graphs,tops,ngraph)
      implicit none

! Constants

      integer    maxtops,     maxnodes,  maxgraphs
      parameter (maxtops=2500,maxnodes=5,maxgraphs=999)
      integer    maxlines
      parameter (maxlines=8)

! Arguments

      integer graphs(0:maxgraphs,0:maxlines)  !graphs, topology internal lines
      integer tops(0:4,0:maxnodes,0:maxtops)
      integer ngraph

! Local Variables

      character*5 name(-10:10)
      integer nvert1,nvert2,n
      integer i,nvert,j,k,n4glue

! Global Variables
      integer        iline(-maxlines:maxlines)
      common/to_proc/iline

!-----------
! Begin Code
!-----------
      name(9) ='G[1]'
      name(-9)='G[-1]'
      name(2) ='F[2]'
      name(-2)='F[-2]'
      name(3) ='F[3]'
      name(-3)='F[-3]'
      name(0)='NA[0]'
      n4glue=0

      do j=1,tops(1,0,graphs(ngraph,0))       !search all nodes
         if (tops(0,j,graphs(ngraph,0)) .eq. 4 .and.   !4 Vertex
     &        graphs(ngraph,tops(1,j,graphs(ngraph,1))) .eq. 1 .and.
     &        graphs(ngraph,tops(2,j,graphs(ngraph,1))) .eq. 1 ) then
            print*,'Found 4 gluon vertex',n4glue
            n = iline(-maxlines+n4glue)
            n4glue=n4glue+1
         endif
      enddo

      do i=1,tops(1,0,0)                      !External lines
         do j=1,tops(1,0,ngraph)
            do k=1,tops(0,j,graphs(ngraph,0)) !search all lines in node
               if (tops(k,j,graphs(ngraph,0)) .eq. i) then
                  nvert=j
               endif
            enddo
         enddo
         write(6,10) i,tops(0,nvert,graphs(ngraph,0)),
     &        nvert,name(iline(i))
      enddo
      do i=-1,tops(0,0,graphs(ngraph,0)),-1                 !Internal lines
         nvert1=0
         nvert2=0
         do j=1,tops(1,0,graphs(ngraph,0))         !Search all nodes
            do k=1,tops(0,j,graphs(ngraph,0))      !search all lines in node
               if (tops(k,j,graphs(ngraph,0)) .eq. i) then
                  if (nvert1 .eq. 0) then
                     nvert1=j
                  else
                     nvert2=j
                  endif
               endif
            enddo
         enddo
         write(6,20) tops(0,nvert1,graphs(ngraph,0)),nvert1,
     &        tops(0,nvert2,graphs(ngraph,0)),
     &        nvert2,name(graphs(ngraph,-i))
      enddo
 10   format(' Propagator[out][e[',i3,'], v',
     &     i1,'[',i3,'], ',a5,']',3i5)
 20   format(' Propagator[in][v',i1,'[',i3,'], v',
     &     i1,'[',i3,'], ',a5,']',3i5)
      return
      end
