!-----------------------
! madgraph - a Feynman Diagram package by Tim Stelzer and Bill Long 
! (c) 1993
!
! Filename: colors.f
!-----------------------

      subroutine simpcolors(color,i)
!**************************************************************************
!     A set of routines to handle color matrix algebra
!     color(0,0,0)       = number of terms
!     color(0,0,term)    = number of factors for term
!     color(1,1,term)    = numerator coeficient of term
!     color(2,1,term)    = denominator coeficient of term
!     color(3,1,term)    = Color flow # for this term
!     color(4,1,term)    = # of flows for this graph
!     color(0,1,term)    = 2 because have numerator and denominator
!     color(0,fact,term) = number of colors in factor
!     color(i,fact,term) = color line of ith element in factor,term
!**************************************************************************
      implicit none

! Constants

      integer   maxlines,  maxfactors,  maxterms
      parameter(maxlines=8,maxfactors=9,maxterms=250)
      
! Arguments

      integer color(0:maxlines,0:maxfactors,0:maxterms),i

! Local Variables

      integer iline

!-----------
! Begin Code
!-----------
      do iline=1,maxlines                    !Sum over internal lines
         call sumgluons(color,i*Iline)
         call ClearZeros(color)
      enddo
      end


      Subroutine Sumgluons(color,tcolor)
!**************************************************************************
!     Find Identical gluons and sum over them
!**************************************************************************
      implicit none

! Constants

      integer   maxlines,  maxfactors,  maxterms
      parameter(maxlines=8,maxfactors=9,maxterms=250)

! Arguments

      integer color(0:maxlines,0:maxfactors,0:maxterms),tcolor

! Local Variables

      integer nterms,nfactors,nelements,summe(2,3)
      integer iterm,ifact,ielement,nfound,nt,ntime

!-----------
! Begin Code
!-----------
      nterms=color(0,0,0)
      iterm =1
      ntime=0
      do while (iterm .le. nterms)
         nfactors=color(0,0,iterm)
         nfound=0
         nt=0
         do ifact=2,nfactors
            nelements=color(0,ifact,iterm)
            do ielement=1,abs(nelements)
               if (color(ielement,ifact,iterm) .eq. tcolor) then  !found one
                  if (nfound .gt. 2) then
                     print*,'Warning more that two colors',tcolor
                     stop
                  endif
                  nfound=nfound+1
                  summe(nfound,1)=iterm
                  summe(nfound,2)=ifact
                  summe(nfound,3)=ielement
                  if(color(0,ifact,iterm) .lt. 0) then
                     nt=nt+1                                 !number of T matrices
                  endif
               endif
            enddo
         enddo
         if (nfound .ne. 2) then
            iterm=iterm+1
         else
            if (nt .eq. 2 ) then
               if (summe(1,2) .ne. summe(2,2)) then
                  if (summe(1,3) .lt. summe(2,3) ) then       !put in order
                     call ttsum(color,summe(1,1),summe(1,2),summe(1,3),
     &                    summe(2,2),summe(2,3))
                  else
                     call ttsum(color,summe(2,1),summe(2,2),summe(2,3),
     &                    summe(1,2),summe(1,3))
                  endif
               else
                  call tsum(color,summe(1,1),summe(1,2),summe(1,3),
     &                 summe(2,2),summe(2,3))
               endif
            else if (nt .eq. 1 ) then
               if (color(0,summe(1,2),summe(1,1)) .gt. 0) then
                  call ftsum(color,summe(1,1),summe(1,2),summe(1,3),
     &                 summe(2,2),summe(2,3))
               else
                  call ftsum(color,summe(2,1),summe(2,2),summe(2,3),
     &                 summe(1,2),summe(1,3))
               endif
            else if (nt .eq. 0 ) then
               if (summe(1,2) .ne. summe(2,2)) then
                  call ffsum(color,summe(1,1),summe(1,2),summe(1,3),
     &                 summe(2,2),summe(2,3))
               else
                  call fsum(color,summe(1,1),summe(1,2),summe(1,3),
     &                 summe(2,2),summe(2,3))
               endif
            endif
         endif
         nterms=color(0,0,0)
      enddo
      end
      
      
      Subroutine SumQuarks(color)
!**************************************************************************
!     A set of routines to handle color matrix algebra
!**************************************************************************
      implicit none

! Constants

      integer   maxlines,  maxfactors,  maxterms
      parameter(maxlines=8,maxfactors=9,maxterms=250)

! Arguments

      integer color(0:maxlines,0:maxfactors,0:maxterms)

! Local Variables

      integer nterms,nfactors,nelements
      integer iterm,ifact,i,j

!-----------
! Begin Code
!-----------
      nterms=color(0,0,0)
      do iterm=1,nterms
         nfactors=color(0,0,iterm)
         ifact=1
         do while(ifact .lt. nfactors)
            ifact=ifact+1
            nelements=color(0,ifact,iterm)
            if (nelements .lt. 0) then                   !This is a T matrix
               if (color(1,ifact,iterm) .eq. color(2,ifact,iterm)) then
                  if (nelements .eq. -2) then !T matrix no glue
                     color(1,1,iterm)=3*color(1,1,iterm) !T(i,i) = 3
                     color(0,0,iterm)=color(0,0,iterm)-1
                     nfactors=color(0,0,iterm)
                     do i=ifact,nfactors
                        color(0,i,iterm)=color(0,i+1,iterm)
                        do j=1,abs(color(0,i,iterm))
                           color(j,i,iterm)=color(j,i+1,iterm)
                        enddo
                     enddo
                     ifact=ifact-1           !set factor back since 1 deleted
                  elseif (nelements .eq. -3)  then        !T matrix 1 glue
                     color(1,1,iterm) = 0                 !tr[T(a)]=0
                  elseif (nelements .lt. -3) then
                     color(0,ifact,iterm) = -color(0,ifact,iterm)-2
                     do i=1,-nelements-2
                        color(i,ifact,iterm)=color(i+2,ifact,iterm)
                     enddo
                  else
                     print*,'Error T matrix w/ wrong # elements',
     &                    nelements
                     stop
                  endif
               endif
            endif     !Identical quark sums
         enddo        !factors
      enddo           !Terms
      end


      Subroutine ClearZeros(color)
!**************************************************************************
!     Looks for terms with zero's in constant factor, and removes them
!     Also removes factors of 1
!     And tries to reduce numerator and denominator
!**************************************************************************
      implicit none

! Constants

      integer   maxlines,  maxfactors,  maxterms
      parameter(maxlines=8,maxfactors=9,maxterms=250)
      integer   Nc
      parameter(Nc=3)

! Arguments

      integer color(0:maxlines,0:maxfactors,0:maxterms)

! Local Variables

      integer nterms,nfactors,nelements
      integer iterm,ifact,ielement,i,j,m,n,r
      logical reduce

!-----------
! Begin Code
!-----------
      nterms=color(0,0,0)              !Eliminate F[a] because is zero
      do iterm=1,nterms
         nfactors=color(0,0,iterm)
         do ifact=2,nfactors
            if (color(0,ifact,iterm) .eq. 1)  then    !F[a] = 0
               color(1,1,iterm)=0
            endif
         enddo
      enddo
      
      nterms=color(0,0,0)
      iterm=0
      do while (iterm .lt. nterms)
         iterm=iterm+1
         if (color(1,1,iterm) .eq. 0.) then            !numerator is zero
            color(0,0,0)=color(0,0,0)-1                !deleteing term
            do i=iterm,color(0,0,0)
               color(0,0,i)=color(0,0,i+1)
               do ifact=1,color(0,0,i+1)
                  color(0,ifact,i)=color(0,ifact,i+1)
                  do ielement=1,abs(color(0,ifact,i+1))
                     color(ielement,ifact,i)=color(ielement,ifact,i+1)
                  enddo
               enddo
            enddo
            nterms=color(0,0,0)                       !reset number of terms
            iterm =iterm-1                            !reset term working on
         endif
      enddo

      nterms=color(0,0,0)            !Clear factors with zero elements
      do iterm=1,nterms
         nfactors=color(0,0,iterm)
         ifact=0
         do while (ifact .lt. nfactors)
            ifact=ifact+1
            nelements=color(0,ifact,iterm)
            if (nelements .eq. 0)   then           !This is f w/ no glue = Nc
               color(1,1,iterm)=color(1,1,iterm)*Nc
               do i=ifact,nfactors-1
                  ielement=color(0,i+1,iterm)
                  do j=0,ielement
                     color(j,i,iterm)=color(j,i+1,iterm)
                  enddo
               enddo
               ifact=ifact-1
               color(0,0,iterm)=color(0,0,iterm)-1
               nfactors=color(0,0,iterm)
            elseif (color(0,ifact,iterm) .eq. 1) then   !F matrix w/ 1 glue
               color(1,1,iterm) = 0             !This is zero
            endif
         enddo
      enddo

      nterms=color(0,0,0)
      do iterm=1,nterms
         reduce=.true.
         m=color(1,1,iterm)
         n=color(2,1,iterm)         !denominator
         if (n .ne. 0) then 
         do while (reduce)
            r=mod(m,n)
            if (r .eq. 0) then
               reduce=.false.
               color(1,1,iterm)=color(1,1,iterm)/n
               color(2,1,iterm)=color(2,1,iterm)/n
            endif
            m=n
            n=r
         enddo
         endif
      enddo
      end



      Subroutine PrintColors(color)
!**************************************************************************
!     Print out T,F matrices
!**************************************************************************
      implicit none

! Constants

      integer   maxlines,  maxfactors,  maxterms
      parameter(maxlines=8,maxfactors=9,maxterms=250)

! Arguments

      integer color(0:maxlines,0:maxfactors,0:maxterms)

! Local Variables

      integer nterms,nfactors,nelements
      integer iterm,ifact,i
      real sum

!-----------
! Begin Code
!-----------
      nterms=color(0,0,0)
      sum=0
      do iterm=1,nterms
         nfactors=color(0,0,iterm)
         print*,'term',iterm,' Coefficient',
     &        isign(color(1,1,iterm),color(2,1,iterm)),
     &        '/',abs(color(2,1,iterm)),color(3,1,iterm)
         sum=sum+real(color(1,1,iterm))/real(color(2,1,iterm))
         do ifact=2,nfactors
            nelements=color(0,ifact,iterm)
            if (nelements .gt. 0) then
               write(*,*) 'F[',(color(i,ifact,iterm),i=1,nelements),']'
            elseif (nelements .lt. 0) then
               write(*,*) 'T[',(color(i,ifact,iterm),i=1,-nelements),']'
            else
            endif
         enddo
      enddo
      end

      subroutine setcolors(color)
!**************************************************************************
!     A set of routines to handle color matrix algebra
!     color(0,0,0)       = number of terms
!     color(0,0,term)    = number of factors for term
!     color(1,1,term)    = numerator coeficient of term
!     color(2,1,term)    = denominator coeficient of term
!     color(2,1,term)    = Color flow of this term
!     color(4,1,term)    = # of Color flows for graph
!     color(0,1,term)    = 4 because have numerator and denominator
!     color(0,fact,term) = number of colors in factor
!     color(i,fact,term) = color line of ith element in factor,term
!**************************************************************************
      implicit none

! Constants

      integer   maxlines,  maxfactors,  maxterms
      parameter(maxlines=8,maxfactors=9,maxterms=250)

! Arguments

      integer color(0:maxlines,0:maxfactors,0:maxterms)

!-----------
! Begin Code
!-----------
      color(0,0,0) = 1                       !1 term

      color(0,0,1) = 1                       !1 factors
      
      color(0,1,1) = 4                       !2 parts + num and denom
      color(1,1,1) = 1                       !numerator
      color(2,1,1) = 1                       !denominator
      color(3,1,1) = 1                       !1st flow
      color(4,1,1) = 1                       !1 flow to start

      end
      

      Subroutine copyterm(color,iterm,ifact1,ifact2)
!**************************************************************************
!     Copies iterm to end of color matrix, w/o includeing ifact1,ifact2
!**************************************************************************
      implicit none

! Constants

      integer   maxlines,  maxfactors,  maxterms
      parameter(maxlines=8,maxfactors=9,maxterms=250)

! Arguments

      integer color(0:maxlines,0:maxfactors,0:maxterms)
      integer ifact1,ifact2,iterm

! Local Variables

      integer nterms,nfactors,nelements,nfound
      integer ifactor,ielement

!-----------
! Begin Code
!-----------
      color(0,0,0) = color(0,0,0) +1
      nterms=color(0,0,0)
      nfactors=color(0,0,iterm)
      nfound = 0
      do ifactor=1,nfactors
         if (ifactor .ne. ifact1 .and. ifactor .ne. ifact2) then
            nelements=color(0,ifactor,iterm)
            color(0,ifactor-nfound,nterms)=color(0,ifactor,iterm)
            do ielement=1,abs(nelements)
               color(ielement,ifactor-nfound,nterms)
     &              =color(ielement,ifactor,iterm)
            enddo
         else
            nfound=nfound+1
         endif
      enddo
      color(0,0,nterms)=nfactors-nfound
      end
      

      integer function AddFlow(color,flows,iterm)
!***************************************************************************
!     Store a flow to so other flows can be compared to it
!***************************************************************************
      implicit none

! Constants

      integer   maxlines,  maxfactors,  maxterms,    maxflows
      parameter(maxlines=8,maxfactors=9,maxterms=250,maxflows=200)

! Arguments

      integer color(0:maxlines,0:maxfactors,0:maxterms)
      integer flows(0:maxlines,0:maxfactors,0:8,0:maxflows)
      integer iterm

! Local Variables

      integer nterms,nfactors
      integer ifactor,ielement,nflows

!-----------
! Begin Code
!-----------
      flows(0,0,0,0)=flows(0,0,0,0)+1              !one more flow added
      nflows=flows(0,0,0,0)
      nterms=0
      flows(0,0,0,nflows)=0              !no terms so far
      nterms=nterms+1
      flows(0,0,0,nflows)=flows(0,0,0,nflows)+1 !1 more term
      nfactors=color(0,0,iterm)
      flows(0,0,nterms,nflows)=nfactors
      do ifactor=1,nfactors
         flows(0,ifactor,nterms,nflows)=color(0,ifactor,iterm) !#elements
         do ielement=1,abs(color(0,ifactor,iterm))
            flows(ielement,ifactor,nterms,nflows)=
     &           color(ielement,ifactor,iterm)
         enddo
         flows(1,1,nterms,nflows)=1
         flows(2,1,nterms,nflows)=1 !Set fraction to 1
      enddo
      addflow=nflows     
      end

      
      Integer Function foundflow(color,flows,jterm)
!***************************************************************************
!     Looks to see if the flow already exists if flows, if so, return true
!***************************************************************************
      implicit none

! Constants

      integer   maxlines,  maxfactors,  maxterms,    maxflows
      parameter(maxlines=8,maxfactors=9,maxterms=250,maxflows=200)

! Arguments

      integer color(0:maxlines,0:maxfactors,0:maxterms)
      integer flows(0:maxlines,0:maxfactors,0:8,0:maxflows)
      integer jterm

! Local Variables

      integer nterms,nfacts,nelements
      integer iterm,ifact,ielement
      logical lfactor
      integer fflow,fterm,nfterms,nflows,nffacts,ffact
      integer igterm,goodterms(10),addflow

!-----------
! Begin Code
!-----------
      nflows=flows(0,0,0,0)
      lfactor=.false.
      fflow=0
      nterms=1
      goodterms(nterms)=jterm
      do while(fflow .lt. nflows .and. .not. lfactor)         
         fflow=fflow+1
         lfactor=.true.
         igterm=0
         nfterms=flows(0,0,0,fflow)
         lfactor = (nfterms .eq. nterms)
         do while(igterm .lt. nterms .and. lfactor)
            igterm=igterm+1
            iterm=goodterms(igterm)
            nfacts=color(0,0,iterm)
            lfactor=.false.
            fterm=0
            do while(fterm .lt. nfterms .and. .not. lfactor)
               fterm=fterm+1
               lfactor=.true.
               ifact   = 1       !ignore constant factors
               nffacts = abs(flows(0,0,fterm,fflow))
               do while(ifact .lt. nfacts .and. lfactor)
                  ifact=ifact+1
                  ffact=1       !ignore constant factors
                  lfactor=.false.
                  do while(ffact .lt. nffacts .and. .not. lfactor)
                     ffact=ffact+1
                     ielement=-1 !Check the 0 component too
                     nelements=abs(color(0,ifact,iterm))
                     lfactor=.true.
                     do while (ielement .lt. nelements .and. lfactor)
                        ielement=ielement+1
                        lfactor=(color(ielement,ifact,iterm) .eq.
     &                       (flows(ielement,ffact,fterm,fflow)))
                     enddo
                  enddo
               enddo
            enddo
         enddo
      enddo
      if (lfactor) then
         foundflow=fflow
      else
         foundflow=addflow(color,flows,jterm)
      endif
      end
            
      
      Subroutine AddTerms(color)
!***************************************************************************
!     Looks for idential color factor terms, and if they exist
!     adds them together to make 1 term
!***************************************************************************
      implicit none

! Constants

      integer   maxlines,  maxfactors,  maxterms,    maxflows
      parameter(maxlines=8,maxfactors=9,maxterms=250,maxflows=200)

! Arguments

      integer color(0:maxlines,0:maxfactors,0:maxterms)

! Local Variables

      integer nterms,nfactors,nelements
      integer iterm,ifact,ielement
      integer cterm,cfact,cfactors
      logical lgood

!-----------
! Begin Code
!-----------
      cterm=0
      do while (cterm .lt. color(0,0,0))
         cterm=cterm+1
         iterm=cterm
         lgood=.false.
         nterms=color(0,0,0)
         cfactors=color(0,0,cterm)
         do while (.not. lgood .and. iterm .lt. nterms)
            iterm=iterm+1
            lgood=.true.
            cfact = 1
            nfactors=color(0,0,iterm)
            do while (lgood .and. cfact .lt. cfactors)
               cfact=cfact+1
               lgood=.false.
               ifact=1
               do while (.not. lgood .and. ifact .lt. nfactors)
                  ielement=-1
                  ifact=ifact+1
                  lgood=.true.
                  nelements=abs(color(0,ifact,iterm))
                  do while ( lgood .and. ielement .lt. nelements)
                     ielement=ielement+1
                     lgood = (color(ielement,ifact,iterm) .eq.
     &                    color(ielement,cfact,cterm)) 
                  enddo         !if lgood after enddo have good factor
               enddo            !nothing here, just sum over terms
            enddo               !if lgood after enddo have good term

            if (lgood .and. cterm .ne. iterm) then !add two terms
               color(1,1,cterm)=color(1,1,cterm)*color(2,1,iterm)
     &              +color(1,1,iterm)*color(2,1,cterm)
               color(2,1,cterm)=color(2,1,cterm)*color(2,1,iterm)
               color(1,1,iterm)=0
               call ClearZeros(color)
               cterm=cterm-1
            else
            endif
         enddo
      enddo
      end
      
                     
      subroutine orderF(color)
!***************************************************************************
!     Uses fact F(a,b,c)=F(c,a,b) to order 
!***************************************************************************
      implicit none

! Constants

      integer   maxlines,  maxfactors,  maxterms,    maxflows
      parameter(maxlines=8,maxfactors=9,maxterms=250,maxflows=200)

! Arguments

      integer color(0:maxlines,0:maxfactors,0:maxterms)

! Local Variables

      integer nterms,nfactors,nelements
      integer iterm,ifact,ielement
      integer i,j,ismall,itemp(maxlines)

!-----------
! Begin Code
!-----------
      nterms=color(0,0,0)
      do iterm=1,nterms
         nfactors=color(0,0,iterm)
         do ifact=2,nfactors                  !first one is just constant
            nelements=color(0,ifact,iterm)    !positive if F, then order
            ismall=1
            do ielement=1,nelements
               if (color(ielement,ifact,iterm) .lt.
     &               color(ismall,ifact,iterm)) ismall=ielement
            enddo
            if (ismall .ne. 1) then           !rearrange the order
               do i=1,nelements
                  itemp(i)=color(i,ifact,iterm)
               enddo
               do i=1,nelements
                  j=i-1+ismall
                  if (j .gt. nelements) j=j-nelements
                  color(i,ifact,iterm) = itemp(j)
               enddo
            endif
         enddo
      enddo
      end
      
               

                  
