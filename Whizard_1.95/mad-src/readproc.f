!-----------------------
! madgraph - a Feynman Diagram package by Tim Stelzer and Bill Long 
! (c) 1993
!
! Filename: readproc.f
!-----------------------
 
      subroutine readproc(more)
!************************************************************************
!     Reads character string and converts to the appropriate process
!************************************************************************
      implicit none

! Constants

      include 'params.inc'
c      integer    maxlines
c      parameter (maxlines=8)
c      integer    max_particles
c      parameter (max_particles=2**7-1)
c      integer    max_coup
c      parameter (max_coup=5)
c      integer    max_string
c      parameter (max_string=120)

! Arguments
      
      logical more

! Local Variables

      character*80 process
      character*80 attempt
      character*10 snum
      character*4  str1
      integer i,nparticles,j,imin
      integer nchar,length
      logical jetloop

! Global Variables

      integer        iline(-maxlines:maxlines),idir(-maxlines:maxlines)
      integer        this_coup(max_coup) ,goal_coup(max_coup)
      common/to_proc/iline,idir,this_coup,goal_coup
      integer         nincoming
      common/to_proc2/nincoming
      character*15 name
      integer iname
      common/to_name/iname,name
      character*60 proc
      integer iproc
      common/to_write/iproc,proc

      character*(4*max_particles) particle(4)
      integer                               charge_c(max_particles)
      integer iparticle(0:max_particles,0:4),inverse(max_particles)
      common/to_model/iparticle,  particle,  inverse, charge_c

      character*(max_string) iwave(max_particles),owave(max_particles)
      character*(8) str2(3,max_particles)
      integer info_p(5,max_particles),iposx(3,max_particles)
      common/to_external/iwave,owave,iposx,info_p,str2

      integer          req_part(0:maxlines),  exc_part(0:maxlines)
      common/to_filter/req_part,              exc_part

      data snum /'0123456789'/
      data jetloop /.false./
!-----------
! Begin Code
!-----------
      iname=0
      if (.not. jetloop) then
c      call resetV
      print*,'Standard Model particles include:'
      print*,'    Quarks:   d u s c b t d~ u~ s~ c~ b~ t~'
      print*,'    Leptons:  e- mu- ta- e+ mu+ ta+ ve vm vt ve~ vm~ vt~'
      print*,'    Bosons:   g a z w+ w- h'
      print*,' '
      print*, 'Enter process you would like calculated ',
     &     'in the form  e+ e- -> a.'
      print*, '(<return> to exit MadGraph.)'
      read '(a)',process
      if (process .eq. ' ') then
         more = .false.
         return
      else
         more = .true.
      end if
!
! Parse string for particles
!
         i = index(process,'>')
         call getparticle(process(1:i),iline(1:),nincoming)
c
c     Lines below allow user to specify intermdiate particles
c     and excluded particles   uu~ > tt~ > w+w-bb~  / a
c
         j = index(process(i+1:),'>')+i       !Break for required particles
         req_part(0)=0
         if (j .gt. i) then
            call getparticle(process(i+1:j),req_part(1:),req_part(0))
            i = j
c            write(*,*) 'Requiring particles',req_part(0)
         endif

         j = index(process(i+1:),'/')+i       !Break for eexcluded particles
         exc_part(0) = 0
         if (j.gt. i) then
            call getparticle(process(j+1:),exc_part(1:),exc_part(0))
            if (exc_part(0) .gt. maxlines) then
               write(*,*) 'Too many particles to exclude'
            endif
c            write(*,*) 'Excluding particles',exc_part(0)
         endif
c
c     Now  get final state
c
         if (j .eq. i) then
         call getparticle(process(i+1:),iline(nincoming+1:),nparticles)
         else
         call getparticle(process(i+1:j),iline(nincoming+1:),nparticles)
         endif
         nparticles = nparticles+nincoming
         iline(0) = nparticles
         call set_jet(iline(0),jetloop)
         j = 21
         attempt = 'Attempting Process: '
         do i=1,iline(0)
            call part_string(iline(i),attempt(j:j+3),nchar)
            j = j+nchar+1
            if (i .eq. nincoming) then
               attempt(j:j+2) = '-> '
               j = j+3
            endif
         enddo
         write (*,'(1x/a)') attempt(1:j)
         iproc = j-18
         proc = attempt(19:j)

         if (nparticles .le. 0) then
            print*,'No process specified.  Exiting MadGraph.'
            more = .false.
            return
         endif
         if (iline(0) .lt. 3) then
            write(*,'(a)') 'Sorry you must enter at least 3 particles.'
            write(*,'(a,i2,a)') 'Only',iline(0),' particles found.'
            write(*,'(a)') 'Please try again. '
            more = .false.
            return
         elseif(iline(0) .gt. maxlines-1) then
            write(*,'(2a,i2,a)')
     &           'Sorry this version of MadGraph configured',
     &           ' to a maximum of ',maxlines-1, ' external particles.'
            write(*,'(i2,a)') iline(0),' particles found.'
            write(*,'(a)') 'Please try again. '
            more = .false.
            return
         endif
!
! Determine Order in QCD, QED, QFD and Ghosts  
!
         call get_order

      else
         call inc_jet(iline(0),jetloop)
         j = 21
         attempt = 'Attempting Process: '
         do i=1,iline(0)
            call part_string(iline(i),attempt(j:j+3),nchar)
            j = j+nchar+1
            if (i .eq. nincoming) then
               attempt(j:j+2) = '-> '
               j = j+3
            endif
         enddo
         write (*,'(1x/a)') attempt(1:j)
         iproc = j-18
         proc = attempt(19:j)
      endif

!
! make name for process
!
      iname = 0
      do i=1,iline(0)
         if (iname .gt. 14) then
            print*,'name too large, truncating: ',name
            iname=14
c            stop
         endif
         if (i .eq. nincoming+1 .and. i .gt. 1) then
            if (iname .lt. 14) then
               iname=iname+1
               name(iname:iname)='_'
            endif
         endif
         call part_string(iline(i),str1,length)
         if (iname+length .gt. 14) then
            write(*,*) 'Truncating name.'
            length=14-iname
         endif
         if (length .gt. 0) name(iname+1:iname+length) = str1(1:length)
         iname = iname+length
         if (i .le. nincoming) then
            iline(i)=inverse(iline(i))
c           if (info_p(2,iline(i)) .eq. -2) iline(i)=inverse(iline(i))!Majorana
         endif
      enddo
      do i=1,iname
         if(name(i:i) .eq. '~') name(i:i) = 'b'
         if(name(i:i) .eq. '+') name(i:i) = 'p'
         if(name(i:i) .eq. '-') name(i:i) = 'm'
      enddo
!
! Query name for process
!
      if (.not. jetloop) then
         write(*,'(a,a,a)') 'Enter a name to identify process (',
     &        name(1:iname),'): '
         read(*,'(a)') process
         i=1
         do while (i .lt. 75 .and. (ichar(process(i:i)) .lt. 65 .or.
     &        ichar(process(i:i)) .gt. 122 .or.
     &        (ichar(process(i:i)) .gt. 90 .and.
     &        ichar(process(i:i)) .lt. 97)))
            i=i+1
         enddo
         imin = i
         do while (i .lt. 75 .and. process(i:i) .ne.' ')
            i=i+1
         enddo
         if (imin .ne. i) then
            if (i-imin .gt. 14) then
               print*,'Truncating name'
               i=imin+14
            endif
            iname=i-imin
            name(1:iname)=process(imin:i)
         endif
      else
         do i=1,iline(0)
            if (i .le. nincoming) then
               iline(i)=inverse(iline(i))
c               if (info_p(2,iline(i)).eq.-2) iline(i)=inverse(iline(i))
            endif
         enddo
      endif
      end

      Subroutine set_jet(iline,jetloop)
c***********************************************************************
c     Looks to see if any of the partons are labeled 'jet' or 'proton'
c     in which case it will be necessary to loop over all the different
c     partons.
c***********************************************************************
      implicit none
!Constants
      include 'params.inc'

! Arguments

      integer iline(0:10)
      logical jetloop

! Local
      integer i,pcode,jcode
      logical first_time

! Global
      integer         icode(npartons),njet,ijet(10),icount(10)
      common /to_jets/icode,          njet,ijet,    icount
      logical         lwp,lwm,lz,decay,cross
      common/to_decay/lwp,lwm,lz,decay,cross
! Data
      Data first_time/.true./
      save pcode,jcode

!-----------
! Begin Code
!-----------
      njet = 0
c       if (first_time) then
c          first_time=.false.
c          call getparticle('P ',pcode,i)
c          if (i .ne. 1) then
c             print*,'Warning no particle code for P, proton',i
c             pcode=-999
c          endif
c          call getparticle('J ',jcode,i)
c          if (i .ne. 1) then
c             print*,'Warning no particle code for J, jet',i
c             jcode=-999
c          endif
c       endif
      pcode = -999
      jcode = -999
      call getparticle('d u s c b d~ u~ s~ c~ b~ g',icode(1:),i)
c      call getparticle('dsucbd~s~u~c~b~g',icode,i)
      if (i .ne. npartons) then
         print*,'Warning not enough jet particle codes',i
      endif
      jetloop = .false.
      do i=1,iline(0)
         if (iline(i) .eq. pcode .or. iline(i) .eq. jcode) then
            jetloop      = .true.
            njet         = njet+1
            iline(i)     = icode(1)
            ijet(njet)   = i
            icount(njet) = 1
         endif
      enddo
      if (ijet(1) .eq. 1 .and. ijet(2) .eq. 2) then
         cross=.true.
      else
         cross=.false.
      endif
      end

      Subroutine inc_jet(iline,jetloop)
c***********************************************************************
c     Loops over the different possible partons in protons and jets
c***********************************************************************
      implicit none
!Constants
      include 'params.inc'
c      integer    npartons
c      parameter (npartons=11)

! Arguments

      integer iline(0:10)
      logical jetloop

! Local
      integer i,j

! Global
      integer         icode(npartons),njet,ijet(10),icount(10)
      common /to_jets/icode,          njet,ijet,    icount
! Data

!-----------
! Begin Code
!-----------
      jetloop=.false.
      i = njet
      do while(icount(i) .eq. npartons .and. i .gt. 1)
         i=i-1
      enddo
      icount(i)=icount(i)+1
      do j=i+1,njet
         icount(j)=icount(i)
      enddo
      do j=1,njet
         if (icount(j) .lt. npartons) then
            jetloop=.true.
         endif
         iline(ijet(j)) = icode(icount(j))
      enddo
      end

      Subroutine get_order
c***********************************************************************
c     Determines the appropriate order of the different coupling
c     constants based on information about the external particles
c     and also based on user input.
c***********************************************************************
      implicit none
! Constants

      include 'params.inc'

! Local
      integer i

! Global Variables

      integer        iline(-maxlines:maxlines),idir(-maxlines:maxlines)
      integer        this_coup(max_coup) ,goal_coup(max_coup)
      common/to_proc/iline,idir,this_coup,goal_coup

      character*(10)            coup_name(max_coup)
      integer             ncoups
      common/to_couplings/ncoups,coup_name
!-----------
! Begin Code
!-----------
      do i=1,ncoups
         write(*,'(a,a)',advance='no') 
     &        'Enter maximum number of vertices for ',
     &        coup_name(i)
         read(*,*) goal_coup(i)
         if (goal_coup(i) .lt. 0) goal_coup(i)=0
      enddo
      end









      Subroutine get_order_old
c***********************************************************************
c     Determines the appropriate order of the different coupling
c     constants based on information about the external particles
c     and also based on user input.
c     Input is from common block iline and
c***********************************************************************
      implicit none
! Constants

      include 'params.inc'
c      integer    maxlines  , max_coup
c      parameter (maxlines=8, max_coup=5)
c      integer    max_string    , max_particles
c      parameter (max_string=120, max_particles=2**7-1)

! Local

      integer i,nqfd,in1,wpart(0:3)
      integer reqorder(max_coup),imin,imax
      character*25 input
      character*10 snum
      logical done

! Global Variables

      integer        iline(-maxlines:maxlines),idir(-maxlines:maxlines)
      integer        this_coup(max_coup) ,goal_coup(max_coup)
      common/to_proc/iline,idir,this_coup,goal_coup

      character*(max_string) iwave(max_particles),owave(max_particles)
      character*(8) str2(3,max_particles)
      integer info_p(5,max_particles),iposx(3,max_particles)
      common/to_external/iwave,owave,iposx,info_p,str2

      logical         lwp,lwm,lz,decay,cross
      common/to_decay/lwp,lwm,lz,decay,cross


      data snum /'0123456789'/
      data wpart/0,0,0,0/
!-----------
! Begin Code
!-----------
      do i=1,max_coup
         reqorder(i)=0
      enddo
      do i=1,iline(0)
         if (info_p(5,iline(i)) .ne. 0) then
            if (info_p(5,iline(i)) .le. max_coup) then
               reqorder(info_p(5,iline(i)))=
     &              reqorder(info_p(5,iline(i)))+1
            else
               print*,'Order of particle too big in readproc'
            endif
         endif
      enddo
      imin = 0
      reqorder(2)=reqorder(2)+reqorder(3)+reqorder(4)
      do i=1,2
         if (reqorder(i) .gt. iline(0)-2) reqorder(i)=iline(0)-2
         imin=imin+reqorder(i)
      enddo

      if (imin .gt. iline(0)-2) then
         print*,'Sorry this process is not possible at tree level'
         print*,'Well try our best'
         reqorder(2)=iline(0)-2-reqorder(1)
c         stop
      endif

      imin=reqorder(1)
      imax=iline(0)-2-reqorder(2)-reqorder(5)
      write(*,'(a,i1,a,i1,a,i1,a)')
     &     'Enter the number of QCD vertices between ',
     &     imin,' and ',imax,' (',imax,'): '
      read(*,'(a)') input
      i=0
      done=.false.
      do while(i .lt. 25 .and. .not. done)
         i=i+1
         in1 = index(snum(imin+1:imax+1),input(i:i))
         if (in1 .ne. 0) then
            goal_coup(1) = imin+in1-1
            done=.true.
         else
            goal_coup(1) = imax
         endif
      enddo
      goal_coup(2) = iline(0)-2-goal_coup(1)-reqorder(4)
      goal_coup(3) = reqorder(3)
      goal_coup(4) = reqorder(4)


      imin = reqorder(2)
      imax = 9
      write(*,'(a,i1,a,i1,a,i1,a)')
     &     'Enter the number of QFD vertices between ',
     &     imin,' and ',imax,' (',imin,'): '
      read(*,'(a)') input
      i=0
      done=.false.
      do while(i .lt. 25 .and. .not. done)
         i=i+1
         in1 = index(snum(imin+1:imax+1),input(i:i))
         if (in1 .ne. 0) then
            goal_coup(2) = imin+in1-1
            done=.true.
         else
            goal_coup(2) = imin
         endif
      enddo

      imin = reqorder(4)
      imax = 9
      write(*,'(a,i1,a,i1,a,i1,a)')
     &     'Enter the number of BRS vertices between ',
     &     imin,' and ',imax,' (',imin,'): '
      read(*,'(a)') input
      i=0
      done=.false.
      do while(i .lt. 25 .and. .not. done)
         i=i+1
         in1 = index(snum(imin+1:imax+1),input(i:i))
         if (in1 .ne. 0) then
            goal_coup(4) = imin+in1-1
            done=.true.
         else
            goal_coup(4) = imin
         endif
      enddo


      write(*,'(a,i1)') 'The number of QFD vertices is ',goal_coup(2)
      if (goal_coup(3) .eq. 0 .and. goal_coup(2) .gt. 0) then
         write(*,'(a)') 'Would you like to include the Weak sector (n)?'
         read(*,'(a)') input
         nqfd=index(input,'y')
         if (nqfd .eq. 0) nqfd = index(input,'Y')
         if (nqfd .ne. 0) nqfd = 1
      elseif (goal_coup(3) .gt. 0) then
         write(*,'(a)') 'QFD required for this process ok?: '
         read(*,'(a)') input
      else
         write(*,'(a)') 'No QFD possible all QCD ok?: '
         read(*,'(a)') input
      endif
      if (nqfd .eq. 1) then
         goal_coup(3)=goal_coup(3)+goal_coup(2)
      endif
c      goal_coup(2)=9

      write(*,'(a,10i4)') 'Max Couplings',(goal_coup(i),i=1,max_coup)

c
c     Check if need to decay final particle
c
      if (wpart(0) .eq. 0) then
         call getparticle('w+w-z',wpart(1:),wpart(0))
         if (wpart(0) .ne. 3) then
            print*,'Warning couldnt find WZ boson codes'
         endif
      endif
      decay = .false.
      do i=1,wpart(0)
         if (iline(iline(0)) .eq. wpart(i)) decay=.true.
      enddo
      if (decay) then
         write(*,'(a)') 'Would you like to decay the final boson (n)?'
         read(*,'(a)') input
         i=index(input,'y')
         if (i .eq. 0) i = index(input,'Y')
         if (i .eq. 0) decay=.false.
      else
         write(*,'(a)') 'Last particle can not be decayed. ok?:'
         read(*,'(a)') input
      endif
c      call loadmodel(goal_coup(1),goal_coup(2),goal_coup(3))
c      if (goal_coup(1).gt. 0) call addQCD
c      if (goal_coup(2) +goal_coup(3)+goal_coup(4).gt. 0) call addQED
c      if (nqfd .gt. 0 .or. goal_coup(3)+goal_coup(4).gt. 0) call addQFD

      end
