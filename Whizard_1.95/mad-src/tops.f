
c       subroutine test_tops
c c************************************************************************
c c     Test if the topology generator works properly
c c************************************************************************
c       implicit none

c !  Constants

c       include 'params.inc'

c !  Local

c       integer tops(0:4,0:maxnodes,0:1)
c       integer tid(maxlines),next,icount,i
c       logical done
c c-----
c c  Begin Code
c c-----
c       done = .false.
c       icount=0
c       read(*,*) next
c       do i=4,next          !This is the first topology which we get for free
c          tid(i)=1
c       enddo
c       do while (.not. done) 
c          call next_top(tid,tops,next,done)
c c         write(*,'(10i4)') (tid(i),i=4,next)
c          icount=icount+1
c       enddo
c       write(*,*) next,icount
c       end

      subroutine next_top(tid,top1,next,done)
c************************************************************************
c     Given tid and the number of external particles, gets the next
c     topology and returns it in tid. Sets done=true if there are no more
c     topologies.
c************************************************************************
      implicit none

!  Constants

      include 'params.inc'
!  Arguments

      integer tid(maxlines),next
      logical done
      integer top1(0:4,0:maxnodes,0:maxgraphs)
!  Local

      integer i,i_inc,icount
      logical failed
c-----
c  Begin Code
c-----
      i_inc=next
      if (tid(i_inc) .eq. i_inc-1) then    !Done with external
         tid(i_inc)=-1
      elseif (tid(i_inc) .gt. 0) then      !Another external or vertex
         tid(i_inc)=tid(i_inc)+1
      else                                 !Another internal
         tid(i_inc)=tid(i_inc)-1
      endif
      done = .false.
      failed = .true.
      do while (failed)
         call gen1top(next,top1,tid,failed)
         if (failed) then
            if (tid(i_inc) .eq. i_inc-1) then !Done with external  
               tid(i_inc)=-1
            elseif (tid(i_inc) .lt. 0)  then !Done with internal
               tid(i_inc)=i_inc
            else                !Done with vertex/everything
               tid(i_inc)=1                     
               i_inc = i_inc-1
               if (tid(i_inc) .eq. i_inc-1) then !Done with external
                  tid(i_inc)=-1
               elseif (tid(i_inc) .gt. 0) then !Another external or vertex
                  tid(i_inc)=tid(i_inc)+1
               else             !Another internal
                  tid(i_inc)=tid(i_inc)-1
               endif
               if (i_inc .le. 3) done=.true.
            endif
         endif
      enddo
      if (.not. done) call sortvertex(top1)
      end

      subroutine gen1top(next,top1,tid,failed)
c*************************************************************************
c     Given the number of external particles, returns in tops
c     the topologies for all possible graphs assuming 3 and 4 point
c     interactions
c*************************************************************************
      implicit none

!  Constants
      
      include 'params.inc'

!  Arguments

      integer next, top1(0:4,0:maxnodes,0:maxgraphs),tid(maxlines)
      logical failed

!  Local

      integer i,nvert,n3vert

c-----
c  Begin Code
c-----
      failed=.true.
      call tops3(top1)             !Start with 3 external line state
      if (next .eq. 3) then
         if (tid(3) .eq. 1) then
            tid(3) = tid(3)+1      ! WK: should be tid(3) instead of tid(4)
            failed = .false.
            return
         else
            return
         endif
      endif
      do i=4,next
         top1(1,0,0)=i             !Number of external lines after adding 1
         if (tid(i) .lt. i) then
            if (tid(i) .eq. 0) return  !There is no line 0
            if (tid(i) .lt. -top1(1,0,1)+1) return !Not this many internal
            call splitline(top1,tid(i))
         else
            nvert=1
            n3vert=0
c            write(*,*) 'Looking for 3 vert',n3vert,tid(i)-i
            do while(n3vert .lt. tid(i)-i+1.and.nvert .le. top1(1,0,1))
               if (top1(0,nvert,1) .eq. 3)  n3vert=n3vert+1
               if (n3vert .eq. tid(i)-i+1) then
                  call splitvertex(top1,nvert)
c                  write(*,*) 'Split vertex ',nvert
               else
c                  write(*,*) 'Still looking for 3 vert',n3vert,tid(i)-i
               endif
               nvert=nvert+1
            enddo
            if (n3vert .ne. tid(i)-i+1) return !Sorry didn't work
c            write(*,*) 'Passed:',n3vert,tid(i)-i
         endif
      enddo
      failed=.false.
      end

      subroutine write_topology(tops,ntop)
!*************************************************************************
!     Initialize the smallest graph possible, 3 external lines
!     which can only have one node
!*************************************************************************
      implicit none
! Constants

      include 'params.inc'

! Arguments

      integer tops(0:4,0:maxnodes,0:maxtops),ntop

! Local
      
      integer i,j

!-----------
! Begin Code
!-----------
      do i=1,tops(1,0,ntop)
         write(*,'(10i6)') i,(tops(j,i,ntop),j=1,tops(0,i,ntop))
      enddo
      end

      Subroutine tops3(tops)
!*************************************************************************
!     Initialize the smallest graph possible, 3 external lines
!     which can only have one node
!*************************************************************************
      implicit none

! Constants

      include 'params.inc'

! Arguments

      integer tops(0:4,0:maxnodes,0:maxtops)

!-----------
! Begin Code
!-----------

      tops(0,0,0)=1                !number of tops
      tops(1,0,0)=3                !number of external lines
      tops(0,0,1)=0                !number of internal lines graph 1
      tops(1,0,1)=1                !number of vertices for graph 1
      tops(2,0,1)=1                !number of 3 vertices for graph 1
      tops(3,0,1)=0                !number of 4 vertices for graph 1
      tops(0,1,1)=3                !first vertex has 3 lines
      tops(1,1,1)=1                !first lines in vertex1 is line1
      tops(2,1,1)=2                !second lines in vertex1 is line2
      tops(3,1,1)=3                !third lines in vertex1 is line3
      tops(4,1,1)=0                !this is meaningless but set to zero
      end

      Subroutine addExternal(oldtops,newtops)
!*************************************************************************
!     Add one external leg to oldgraphs topologies and store in
!     newgraphs topologies
!*************************************************************************
      implicit none

! Constants

      include 'params.inc'

! Arguments

      integer oldtops(0:4,0:maxnodes,0:maxtops)
      integer newtops(0:4,0:maxnodes,0:maxtops)

! Local Variables

      integer oldg,oldvert,oldline

!-----------
! Begin Code
!-----------
      newtops(0,0,0)=0                    !no new topologies
      newtops(1,0,0)=oldtops(1,0,0)+1     !adding one external line

      do oldg=1,oldtops(0,0,0)            !loop over each of the old topolog
         do oldline = 1,oldtops(1,0,0)
            call copytops(oldtops,newtops,oldg)
            call splitline(newtops,oldline)
         enddo
         do oldline = -1,oldtops(0,0,oldg),-1   !internal lines are negative
            call copytops(oldtops,newtops,oldg)
            call splitline(newtops,oldline)
         enddo
         do oldvert=1,oldtops(1,0,oldg)   !check each old vertex
            if (oldtops(0,oldvert,oldg) .eq. 3) then    !3 vertex can add
               call copytops(oldtops,newtops,oldg)
               call splitvertex(newtops,oldvert)
            endif
         enddo
      enddo
      end

      Subroutine copyTops(oldtops,newtops,oldg)
!*************************************************************************
!     copy topology from oldgraphs to newgraphs
!*************************************************************************
      implicit none

! Constants

      include 'params.inc'
c      integer    maxtops,      maxnodes
c      parameter (maxtops=2500,maxnodes=5)

! Arguments

      integer oldtops(0:4,0:maxnodes,0:maxtops)
      integer newtops(0:4,0:maxnodes,0:maxtops)
      integer oldg

! Local Variables

      integer ivert,iline,ntops,i

!-----------
! Begin Code
!-----------
      newtops(0,0,0) = newtops(0,0,0)+1            !adding one new topology
c      write(*,*) 'Adding topology ',newtops(0,0,0)
      if (newtops(0,0,0) .gt. maxtops) then
         write(*,*) 'Sorry exceeded maximum number of topologies',
     &        maxtops
         stop
      endif
      ntops=newtops(0,0,0)
      do i=0,4
         newtops(i,0,ntops)=oldtops(i,0,oldg)      !Basic topology info
      enddo
      do ivert=1,oldtops(1,0,oldg)                 !loop over all vertices
         do iline=0,4
            newtops(iline,ivert,ntops)=oldtops(iline,ivert,oldg)
         enddo
      enddo
      end

      Subroutine splitvertex(newtops,nvert)
!*************************************************************************
!     copy topology from oldgraphs to newgraphs
!*************************************************************************
      implicit none

! Constants

      include 'params.inc'
c      integer    maxtops,     maxnodes
c      parameter (maxtops=2500,maxnodes=5)

! Arguments

      integer newtops(0:4,0:maxnodes,0:maxtops),nvert

! Local Variables

      integer ntops

!-----------
! Begin Code
!-----------
      ntops=newtops(0,0,0)                !always working on the newest top.
      if (newtops(0,nvert,ntops) .ne. 3) then
         print*,'Error splitting vertex of non-3vertex',newtops
         stop
      endif
      newtops(0,nvert,ntops) = 4                  !now its a 4 vertex
      newtops(4,nvert,ntops) = newtops(1,0,0)     !we are adding the newest Ext
      newtops(2,0,ntops) = newtops(2,0,ntops) -1  !one less 3 vertex
      newtops(3,0,ntops) = newtops(3,0,ntops) +1  !one more 4 vertex
      end

      Subroutine splitline(newtops,nline)
!*************************************************************************
!     copy topology from oldgraphs to newgraphs
!*************************************************************************
      implicit none

! Constants

      include 'params.inc'
c      integer    maxtops,      maxnodes
c      parameter (maxtops=2500,maxnodes=5)

! Arguments

      integer newtops(0:4,0:maxnodes,0:maxtops),nline

! Local

      integer nvert,ninternal,ntops
      integer nfound,convert(2),conline(2),i,j

!-----------
! Begin Code
!-----------
      ntops=newtops(0,0,0)                !always working on the newest top.
      nfound=0
      do i=1,newtops(1,0,ntops)           !look through all vertices
         do j=1,newtops(0,i,ntops)        !either 3 or 4 lines to check
            if (newtops(j,i,ntops) .eq. nline) then
               nfound=nfound+1
               convert(nfound)=i
               conline(nfound)=j
            endif
         enddo
      enddo
      if (nline .lt. 0 .and. nfound .ne. 2) then
         print*,'Splitting internal line, but not enough nodes',nfound
         print*,'Graph number',ntops,convert(1),convert(2),nline
         stop
      elseif(nline .gt. 0 .and.  nfound .ne. 1) then
         print*,'Splitting external line, but not enough nodes',nfound
         print*,'Graph number',ntops,convert(1),convert(2),nline
      endif
      newtops(0,0,ntops) = newtops(0,0,ntops) -1     !one more internal line (-)
      newtops(1,0,ntops) = newtops(1,0,ntops) +1     !one more vertex
      newtops(2,0,ntops) = newtops(1,0,ntops) +1     !one more 3 vertex
      nvert=newtops(1,0,ntops)                       !number of new vertex
      ninternal=newtops(0,0,ntops)                 !number of new internal line
      newtops(0,nvert,ntops) = 3                     !Created 3 vertex
      newtops(1,nvert,ntops) = newtops(1,0,0)        !New External line
      newtops(2,nvert,ntops) = nline                 !original split line
      newtops(3,nvert,ntops) = ninternal             !new internal line
      newtops(4,nvert,ntops) = 0                     !Not Used set to zero
      newtops(conline(1),convert(1),ntops)=ninternal !change other node of int
      end


      Subroutine SortVertex(tops)
!*************************************************************************
!     Put vertices for each topology in an intelligent order
!     So that after ones before it are evaluated, each has only 1 unknown
!     wave function (line), and put that line at the end
!*************************************************************************
      implicit none
      
! Constants

      include 'params.inc'
c      integer    maxtops,     maxnodes
c      parameter (maxtops=2500,maxnodes=5)
c      integer   maxlines
c      parameter(maxlines=8)

! Arguments

      integer  tops(0:4,0:maxnodes,0:maxtops)

! Local

      integer itemp
      integer ntop,iorder(maxnodes),nvert,iline(-maxlines:maxlines),ngot
      integer i,ndone
      logical done

!-----------
! Begin Code
!-----------
      do ntop=1,tops(0,0,0)       !sum over each topology
         done=.false.
         ndone=0
         do i=1,maxlines
            iline(i)  = 1         !external lines are positive and known
            iline(-i) = 0         !internal lines neg and not known
         enddo
         nvert = 0
         do while (.not. done)
            nvert=nvert+1
            if (nvert .gt. tops(1,0,ntop)) nvert=1
            ngot=0
            do i=1,tops(0,nvert,ntop)
               if (iline(tops(i,nvert,ntop)) .ne. 0) ngot=ngot+1
            enddo
            if (tops(0,nvert,ntop) - ngot .eq. 1) then
               ndone=ndone+1
               iorder(ndone) = nvert
               do i=1,tops(0,nvert,ntop)
                  if (iline(tops(i,nvert,ntop)) .eq. 0) then !this is uncalced 
                     iline(tops(i,nvert,ntop)) = 1
                     itemp=tops(i,nvert,ntop)                !move to last position
                     tops(i,nvert,ntop) =
     &                    tops(tops(0,nvert,ntop),nvert,ntop)
                     tops(tops(0,nvert,ntop),nvert,ntop) = itemp
                  endif
               enddo
            endif
            if (tops(0,nvert,ntop) - ngot .eq. 0) then   !have them all
               if (ndone .eq. tops(1,0,ntop)-1)   then   !last one
                  done=.true.
                 do i=1,ndone
                     if (iorder(i) .eq. nvert) then
                        done=.false.
                     endif
                  enddo
                  if (done) then
                     ndone=ndone+1
                     iorder(ndone)=nvert
                  endif
               endif
            endif
         enddo
         call orderverts(tops,iorder,ntop)
      enddo
      end

      Subroutine orderverts(tops,iorder,ntop)
!************************************************************************
!     Places vertices in the order specified by iorder which
!     should assure all lines except one are known at each node
!************************************************************************
      implicit none

! Constants

      include 'params.inc'
c      integer    maxtops,     maxnodes
c      parameter (maxtops=2500,maxnodes=5)

! Arguments

      integer tops(0:4,0:maxnodes,0:maxtops)
      integer ntop,iorder(maxnodes)

! Local

      integer iline,vtemp(0:4,0:maxnodes)
      integer i

!-----------
! Begin Code
!-----------
      do i=1,tops(1,0,ntop)
         do iline=0,4
            vtemp(iline,i) = tops(iline,i,ntop)
         enddo
      enddo
      do i=1,tops(1,0,ntop)
         do iline=0,4
            tops(iline,i,ntop) = vtemp(iline,iorder(i))
         enddo
      enddo
      end

