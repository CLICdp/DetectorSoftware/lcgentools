!-----------------------
! madgraph - a Feynman Diagram package by Tim Stelzer and Bill Long 
! (c) 1993
!
! Filename: genhelas.f
!-----------------------
 
      Subroutine writeexternal
!**************************************************************************
!     Writes code to generate all of the external wave functions
!
!**************************************************************************
      implicit none

! Constants

      integer   maxcount,   from,  type,  flav,  calc,  dir,  yes
      parameter(maxcount=40,from=1,type=2,flav=3,calc=5,dir=4,yes=1)
      integer   kind
      parameter(kind=6)

! Global Variables

      integer iv3(maxcount,6),iv4(maxcount,6),iline(maxcount,5),nlines
      Common/tohelas/iv3,iv4,iline,nlines

! Local Variables

      integer i

!-----------
! Begin Code
!-----------
      do i = 1,nlines
         if(iline(i,from) .le. 2) then   !External Particle
            if (iline(i,type) .eq. 3) then  !Fermion
               if (iline(i,dir) .eq. 1) then
                  call writeincoming(i)
               elseif (iline(i,dir) .eq. -1) then
                  call writeoutgoing(i)
               else
                  print*,'Line ',i,'has no direction',iline(i,type)
               endif
            elseif (abs(iline(i,type)) .le. 2) then !Then Gluon,Photon,Z
               if (iline(i,from) .eq. 1) then
                  call writeincoming(i)
               else
                  call writeoutgoing(i)
               endif
            elseif (abs(iline(i,type)) .le. 4) then    !Higgs
               if (iline(i,from) .eq. 1) then
                  call writeincoming(i)
               else
                  call writeoutgoing(i)
               endif
            else
               print*,'Error in Write external particle not known'
            endif
         endif
      enddo
      end

      Subroutine markexternal
!**************************************************************************
!     Marks external lines as calculated
!
!**************************************************************************
      implicit none

! Constants

      integer   maxcount,   from,  type,  flav,  calc,  dir,  yes
      parameter(maxcount=40,from=1,type=2,flav=3,calc=5,dir=4,yes=1)

! Global Variables

      integer iv3(maxcount,6),iv4(maxcount,6),iline(maxcount,5),nlines
      Common/tohelas/iv3,iv4,iline,nlines

! Local Variables

      integer i

!-----------
! Begin Code
!-----------
      do i = 1,nlines
         if (iline(i,from) .eq. 1) then
            iline(i,calc) = yes
         elseif (iline(i,from) .eq. 2) then
            iline(i,calc) = yes
         elseif (iline(i,from) .eq. 3) then
            iline(i,calc) = -yes
         endif
      enddo
      return
      end

      Subroutine Vertex
!*************************************************************************
!     Writes code to do the vertex functions
!     First tries to find external fermion line to follow, then looks for
!     any functions which haven't been calculated
!*************************************************************************
      implicit none

! Constants

      integer    fermion,  gluon
      parameter (fermion=3,gluon=2)
      integer   maxcount,   from,  type,  flav,  calc,  dir,  yes
      parameter(maxcount=40,from=1,type=2,flav=3,calc=5,dir=4,yes=1)
      integer   kind
      parameter(kind=6)
      integer   IOV,   IOS,   VVV,   VVS,   VSS,   SSS,   WWWW
      parameter(IOV=1, IOS=2, VVV=3, VVS=4, VSS=5, SSS=6, WWWW=7)
      integer   W3W3,   VVSS,   SSSS,    IOG,    GGG,    GGGG
      parameter(W3W3=8, VVSS=9, SSSS=10, IOG=11, GGG=12, GGGG=13)

! Local Variables

      integer i,j
      logical done

! Global Variables

      integer order(2,0:maxcount)
      common /newfix/order
      integer iv3(maxcount,6),iv4(maxcount,6),iline(maxcount,5),nlines
      Common/tohelas/iv3,iv4,iline,nlines

!-----------
! Begin Code
!-----------
      done=.false.
      do j=1,order(1,0)
         i=order(2,j)
         if (order(1,j) .eq. 3) then !this is 3 vertex
            if     (iv3(i,kind) .eq. IOV) then
               call writeFFV(i)
            elseif (iv3(i,kind) .eq. IOG) then
               call writeFFG(i)
            elseif (iv3(i,kind) .eq. GGG) then
               call writeGGG(i)
            elseif (iv3(i,kind) .eq. VVV) then
               call writeVVV(i)
            elseif (iv3(i,kind) .eq. IOS) then
               call writeIOS(i)
            elseif (iv3(i,kind) .eq. VVS) then
               call writeVVS(i)
            elseif (iv3(i,kind) .eq. SSS) then
               call writeSSS(i)
            else
               print*,'3 Vertex ',iv3(i,kind),'Not implemented yet'
               stop
            endif
            done=.false.
         endif
         if (order(1,j) .eq. 4) then
            if (iv4(i,kind) .eq. GGGG) then
               call writeGGGG(i)
            elseif (iv4(i,kind) .eq. SSSS) then
               call writeSSSS(i)
            elseif (iv4(i,kind) .eq. VVSS) then
               call writeVVSS(i)
            elseif (iv4(i,kind) .eq. WWWW) then
               call writeWWWW(i)
            elseif (iv4(i,kind) .eq. W3W3) then
               call writeW3W3(i)
            else
               print*,'4 Vertex ',iv4(i,kind),'Not implemented yet'
               stop
            endif
            done=.false.
         endif
      enddo
      end


      Subroutine SortVertexHelas
!****************************************************************************
!     Stores what each vertex is IOV,IOS,VVV,VVS,VSS,SSS,WWWW,W3W3,VVSS,SSSS
!                                IOG,GGG,G3G3
!****************************************************************************
      implicit none

! Constants

      integer   IOV,   IOS,   VVV,   VVS,   VSS,   SSS,   WWWW
      parameter(IOV=1, IOS=2, VVV=3, VVS=4, VSS=5, SSS=6, WWWW=7)
      integer   W3W3,   VVSS,   SSSS,    IOG,    GGG,    GGGG
      parameter(W3W3=8, VVSS=9, SSSS=10, IOG=11, GGG=12, GGGG=13)
      integer    fermion,  gluon,  W,  higgs
      parameter (fermion=3,gluon=2,W=1,higgs=4)
      integer   maxcount,   from,  type,  flav,  calc,  dir,  yes
      parameter(maxcount=40,from=1,type=2,flav=3,calc=5,dir=4,yes=1)
      integer   kind,  empty
      parameter(kind=6,empty=-1)

! Local Variables

      integer j,nvert
      integer icount(-1:4)

! Global Variables

      integer iv3(maxcount,6),iv4(maxcount,6),iline(maxcount,5),nlines
      Common/tohelas/iv3,iv4,iline,nlines

!-----------
! Begin Code
!-----------
      do nvert=1,maxcount
         if (iv3(nvert,calc) .eq. -yes) then
            do j=-1,4
               icount(j)=0
            enddo
            do j=1,3
               if (abs(iline(iv3(nvert,j),type)) .eq. gluon) then
                  icount(gluon)=icount(gluon)+1
               else if (abs(iline(iv3(nvert,j),type)) .eq. w) then
                  icount(w)=icount(w)+1
               else if (abs(iline(iv3(nvert,j),type)) .eq. higgs) then
                  icount(higgs)=icount(higgs)+1
               else
                  icount(iline(iv3(nvert,j),type)) =
     &                 icount(iline(iv3(nvert,j),type)) +1
               endif
            enddo
            if (icount(fermion) .eq. 2) then
               if (icount(w) .eq. 1) then
                  iv3(nvert,kind) = IOV
               elseif (icount(higgs) .eq. 1) then
                  iv3(nvert,kind) = IOS
               elseif (icount(gluon) .eq. 1) then
                  iv3(nvert,kind) = IOG
               else
                  print*,'Error 2 fermions, but no glue,higgs, or A'
                  stop
               endif
            elseif (icount(w) .eq. 1)  then
               if (icount(higgs) .eq. 2) then
                  iv3(nvert,kind) = VSS
               else
                  print*,'Error 1 Vector, not 2 scalers'
                  stop
               endif
            elseif (icount(w) .eq. 2)  then
               if (icount(higgs) .eq. 1) then
                  iv3(nvert,kind) = VVS
               else
                  print*,'Error 2 Vector, no scaler',icount(w),
     &                 icount(higgs),icount(fermion),icount(gluon)
                  stop
               endif
            elseif (icount(w) .eq. 3) then
               iv3(nvert,kind) = VVV
            elseif (icount(gluon) .eq. 3) then
               iv3(nvert,kind) = GGG
            elseif (icount(higgs) .eq. 3) then
               iv3(nvert,kind) = SSS
            elseif (icount(empty) .ne. 3) then
               print*,'Unknown type ',(icount(j),j=-1,3)
               write(*,*) (iv3(nvert,j),j=1,5)
               write(*,*) (iline(iv3(nvert,j),type),j=1,3)
               stop
            endif
         endif
!***************************** 4 particle vertex below ***********************
         if (iv4(nvert,calc) .eq. -yes) then
            do j=-1,4
               icount(j)=0
            enddo
            do j=1,4
               if (abs(iline(iv4(nvert,j),type)) .eq. gluon) then
                  icount(gluon) = icount(gluon)+1
               else if (abs(iline(iv4(nvert,j),type)) .eq. w) then
                  icount(w) = icount(w)+1
               else
                  icount(iline(iv4(nvert,j),type)) =
     &                 icount(iline(iv4(nvert,j),type)) +1
                  print*,'Got something wierd',iline(iv4(nvert,j),type),
     &                 iline(iv4(nvert,j),flav),iv4(nvert,j),j,nvert
               endif
            enddo
            if (icount(w) .eq. 4)  then
               iv4(nvert,kind) = WWWW
            elseif (icount(w) .eq. 2) then
               if (icount(higgs) .eq. 2) then
                  iv4(nvert,kind) = VVSS
               else
                  print*,'Error in 4 vector vertex',(icount(j),j=-1,3)
                  print*,'Two Ws but not two scalers',nvert
                  stop
               endif
            elseif(icount(higgs) .eq. 4) then
               iv4(nvert,kind) = SSSS
            elseif(icount(gluon) .eq. 4) then
               iv4(nvert,kind) = GGGG
            elseif (icount(empty) .ne. 4) then
               print*,'Error in 4 vector vertex',(icount(j),j=-1,3)
               stop
            endif
            if (iv4(nvert,kind) .eq. WWWW) then    !See if any Z or A
               do j=1,4
                  if (abs(iline(iv4(nvert,j),flav)) .ne. 3) then
                     iv4(nvert,kind) = W3W3
                  endif
               enddo
            endif
         endif
      enddo
      end

