      Subroutine crossing(ncross,jpart,imatch)
c************************************************************************
c     Determines all the different crossings for the particles
c     and which crossing are identical.  
c     ncross = number of crossings found
c     jpart  = particles put in, and all permutations
c     npart  = number of particles to all permuting
c     imatch = matching since some crossings will have same amplitude
c              keeps from double calculating things (just for speed)
c************************************************************************
      implicit none
c
c Constants
c
      include 'params.inc'
c
c     Arguments
c
      integer ncross,jpart(maxlines,maxcross),imatch(0:maxcross)
c
c     Local
c
      integer i,j,k,l,ipart(maxlines,maxcross),nt,ntb
      integer idiff(6,maxcross),n1,n2,ng,nfound,ndiff,p,nb1,nb2
      integer nu,nch,nub,ncb,nq,t1,t2,nbb,nb,npart
      integer kjet
      logical good

! Global Variables

      integer        iline(-maxlines:maxlines),idir(-maxlines:maxlines)
      integer        this_coup(max_coup) ,goal_coup(max_coup)
      common/to_proc/iline,idir,this_coup,goal_coup

      character*(4*max_particles) particle(4)
      integer                               charge_c(max_particles)
      integer iparticle(0:max_particles,0:4),inverse(max_particles)
      common/to_model/iparticle,  particle,  inverse, charge_c

      logical         lwp,lwm,lz,decay,cross
      common/to_decay/lwp,lwm,lz,decay,cross

      integer         icode(npartons),njet,ijet(10),icount(10)
      common /to_jets/icode,          njet,ijet,    icount
c----
c Begin Code
c----
      ng = icode(11)
      nu = icode(2)
      nch = icode(4)
      nub= icode(7)
      ncb= icode(9)
      nb = icode(5)
      nbb= icode(10)
c *WK
      nt = 0
      ntb = 0
c      call getparticle('t',nt,nfound)
c      if (nfound .ne. 1) print*,'Warning couldnt find t',nt,nfound
c      call getparticle('t~',ntb,nfound)
c      if (nfound .ne. 1) print*,'Warning couldnt find t~',ntb,nfound
c      call getparticle('g',ng,nfound)
c      if (nfound .ne. 1) print*,'Warning couldnt find g',ng,nfound
c      call getparticle('u',nu,nfound)
c      if (nfound .ne. 1) print*,'Warning couldnt find u',nu,nfound
c      call getparticle('c',nc,nfound)
c      if (nfound .ne. 1) print*,'Warning couldnt find c',nc,nfound
c      call getparticle('u~',nub,nfound)
c      if (nfound .ne. 1) print*,'Warning couldnt find u~',nub,nfound
c      call getparticle('c~',ncb,nfound)
c      if (nfound .ne. 1) print*,'Warning couldnt find c~',ncb,nfound
c      call getparticle('b~',nbb,nfound)
c      if (nfound .ne. 1) print*,'Warning couldnt find b~',nbb,nfound
c      call getparticle('b',nb,nfound)
c      if (nfound .ne. 1) print*,'Warning couldnt find b',nb,nfound
c
c     Count number of partons
c
      nq    = 0
      do i=1,iline(0)
         do j=1,11
            if (iline(i) .eq. icode(j)) nq=nq+1
         enddo
      enddo

      ndiff = 0
      ncross= 1
      npart = nq
      do i=1,npart
         do j=1,npart
            ipart(1,ncross) = iline(i)
            ipart(2,ncross) = iline(j)
            jpart(1,ncross) = i
            jpart(2,ncross) = j
            if (i .ne. j) then
               k=3
               do l=1,npart
                  if (l .ne. i .and. l .ne. j) then
                     ipart(k,ncross)=iline(l)
                     jpart(k,ncross)=l
                     k = k+1
                  endif
               enddo
               good = .true.
               do l=1,ncross-1
                  if ( ipart(1,ncross) .eq. ipart(1,l) .and.
     $                 ipart(2,ncross) .eq. ipart(2,l)) good=.false.
               enddo
c
c     Make sure don't cross anything which wasn't a jet or proton
c
               write(*,'(a,8i5)') 'iline',(iline(k),k=1,npart)
               write(*,'(a,8i5)') 'ipart',
     &                            (ipart(k,ncross),k=1,npart)
               write(*,'(a,8i5)') 'ijet',(ijet(k),k=1,njet)
               do k=1,npart
                  kjet = 1
                  if (ipart(k,ncross) .ne. iline(k)) then
                     do while(ijet(kjet) .lt. k 
     &                         .and. kjet .lt. njet)
                        kjet=kjet+1
                     enddo
                     if (ijet(kjet) .ne. k) good=.false.
c                     write(*,*) k,kjet,ijet(kjet),iline(k)
                  endif
               enddo
c
c     Make sure no b's in initial state
c
               if (ipart(1,ncross) .eq. nb) good=.false.
               if (ipart(1,ncross) .eq. nbb) good=.false.
               if (ipart(2,ncross) .eq. nb) good=.false.
               if (ipart(2,ncross) .eq. nbb) good=.false.
c
c     Make sure no t's in initial state
c
               if (ipart(1,ncross) .eq. nt) good=.false.
               if (ipart(1,ncross) .eq. ntb) good=.false.
               if (ipart(2,ncross) .eq. nt) good=.false.
               if (ipart(2,ncross) .eq. ntb) good=.false.
               if (good) then
                  do k=npart+1,iline(0)
                     ipart(k,ncross)=iline(k)
                     jpart(k,ncross)=k
                  enddo
c                  write(*,3)ncross,(ipart(k,ncross),k=1,iline(0))
 3                format( 10i4)
                  if (ncross .gt. maxcross) then
                     print*,'Warning too many crossings',ncross,maxcross
                     ncross=maxcross
                  endif
c
c Check if similar to previous pattern by seeing how many times
c       initial particle is found in the final state, but
c       in case of gluons, mark special with -1
c
                  n1=0
                  n2=0
                  nb1=0
                  nb2=0
c
c     First see how many times the parton appears on the other side
c     and   see how many times inverse appears on the other side.
c     also take note to if its a particle or antiparticle, and up
c     or down type.
c     
                  do p=3,npart
                     if (ipart(1,ncross) .eq. ipart(p,ncross)) n1=n1+1
                     if (ipart(2,ncross) .eq. ipart(p,ncross)) n2=n2+1
                     if (ipart(1,ncross) .eq.
     &                    inverse(ipart(p,ncross))) nb1=nb1+1
                     if (ipart(2,ncross) .eq.
     &                    inverse(ipart(p,ncross))) nb2=nb2+1
                  enddo
c
c     For Z production up and down quarks are different, store up = 1 down=2
c
                  t1=2
                  t2=2
                  if (lz) then
                     if ( ipart(1,ncross) .eq. nu  .or.
     &                    ipart(1,ncross) .eq. nub .or.
     &                    ipart(1,ncross) .eq. nc  .or.
     &                    ipart(1,ncross) .eq. ncb )     t1=1
                     if ( ipart(2,ncross) .eq. nu  .or.
     &                    ipart(2,ncross) .eq. nub .or.
     &                    ipart(2,ncross) .eq. nch  .or.
     &                    ipart(2,ncross) .eq. ncb )     t2=1
                     if (ipart(1,ncross) .eq. ng) t1=0
                     if (ipart(2,ncross) .eq. ng) t2=0
                  endif
c
c     Distinguish particle from antiparticle by multiplying by -1
c
                  if (ipart(1,ncross) .lt.
     &                 inverse(ipart(1,ncross))) then
                     t1=-t1
                  endif
                  if (ipart(2,ncross) .lt.
     &                 inverse(ipart(2,ncross))) then
                     t2=-t2
                  endif
c
c     Check for match
c
                  p=1
c
c     Use this one for Z production where Charge Conjugation is used
c
                  if (lz) then
                  do while ((n1 .ne. idiff(1,p) .or. n2 .ne. idiff(2,p)
     &                .or.   nb1.ne. idiff(3,p) .or. nb2.ne. idiff(4,p)
     &                .or. ((t1 .ne. idiff(5,p) .or. t2 .ne. idiff(6,p))
     &               .and.  (t1 .ne. -idiff(5,p)
     &                                      .or. t2 .ne. -idiff(6,p))))
     &                      .and. (p .le. ndiff))
                     p=p+1
                  enddo

                  elseif(goal_coup(2) .eq. 0 .and.
     &                 goal_coup(3) .eq. 0) then        !QCD use c,p
                  do while ((n1 .ne. idiff(1,p) .or. n2 .ne. idiff(2,p)
     &                .or.   nb1.ne. idiff(3,p) .or. nb2.ne. idiff(4,p))
     &                      .and. (p .le. ndiff))
                     p=p+1
                  enddo

                  else        !W production

                  do while ((n1 .ne. idiff(1,p) .or. n2 .ne. idiff(2,p)
     &                .or.   nb1.ne. idiff(3,p) .or. nb2.ne. idiff(4,p)
     &                .or.   t1 .ne. idiff(5,p) .or. t2 .ne. idiff(6,p))
     &                      .and. (p .le. ndiff))
                     p=p+1
                  enddo
                  endif
c
c     Remove Optimization
c
                  
                  p=ndiff+1

                  if (p .gt. ndiff) then
                     ndiff=ndiff+1
                     idiff(1,ndiff)=n1
                     idiff(2,ndiff)=n2
                     idiff(3,ndiff)=nb1
                     idiff(4,ndiff)=nb2
                     idiff(5,ndiff)=t1
                     idiff(6,ndiff)=t2
                  endif
                  imatch(ncross)=p
                  imatch(0) = ndiff

                  ncross=ncross+1
               endif
            endif
         enddo
      enddo
      ncross=ncross-1
      print*,'Crossing ',ndiff,ncross
      end

      subroutine getdenom(isym,initc,ihel,step,ipart)
c*************************************************************************
c     Given the initial and final state particles in iline
c     This routine gives the different factors for
c     isym    = 1/n! for identical final state particles
c     initc   = 1/Nch for averaging color of initial state particles
c     ihel    = 1/nhels for averaging over initial spin states
c************************************************************************
      implicit none
c
c     Constants
c
      include 'params.inc'
c
c     Arguments
c
      integer ihel,initc,isym,step(maxlines),ipart(maxlines)
c
c     Local
c     
      integer j,identpart(max_particles),nexternal,i

! Global Variables

      integer        iline(-maxlines:maxlines),idir(-maxlines:maxlines)
      integer        this_coup(max_coup) ,goal_coup(max_coup)
      common/to_proc/iline,idir,this_coup,goal_coup

      integer         nincoming
      common/to_proc2/nincoming

      integer info_p(5,max_particles),iposx(3,max_particles)
      character*(max_string) iwave(max_particles),owave(max_particles)
      character*(8) str(3,max_particles)
      common/to_external/iwave,owave,iposx,info_p,str
c-----
c Begin Code
c-----
      nexternal=iline(0)
      do j=1,max_particles
         identpart(j)=0                   !Number of identical final parts
      enddo
      initc=1
      isym=1
      do i=1,nexternal
         if (i .le. nincoming) then                   
            initc = initc*(info_p(1,iline(ipart(i)))) !Average colors
         else
            identpart(iline(ipart(i))) = identpart(iline(ipart(i)))+1
            isym=isym*identpart(iline(ipart(i))) ! n! for identical final
         endif
      enddo

      ihel=1
      do i=1,nexternal
         if (info_p(2,iline(ipart(i))) .eq. 1) then
            step(i) = 3                        !Only 1 Helicity (scalar)
         elseif (abs(info_p(2,iline(ipart(i)))) .eq. 2) then
            step(i) = 2                        !2 Helicity states -1,1
            if (i .le. nincoming) ihel=ihel*2 
         elseif (info_p(2,iline(ipart(i))) .eq. 3) then
            step(i) = 1                        !3 Helicity state -1,0,1 W,Z
            if (i .le. nincoming) ihel=ihel*3 
         else
            print*,'Unknown Helicity',info_p(2,iline(ipart(i))),
     &           iline(ipart(i)),ipart(i)
         endif
      enddo
c      print*,'Denominators',initc,ihel,isym,nexternal
      end

