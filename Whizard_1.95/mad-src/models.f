!-----------------------
! madgraph - a Feynman Diagram package by Tim Stelzer and Bill Long 
! (c) 1993, 1994
!
! Filename: models.f
!-----------------------
 
      Subroutine SetQED
!**************************************************************************
!     Sets up QED 3 vertex Couplings
!     Quarks  1 - 6 d u s c b t
!     Leptons 11-16 e v mu v tau v
!     Boson   22-24 photon, Z, W+
!**************************************************************************
      implicit none

! Local Variables

      integer j,k,jj
      integer photon(10)
      data photon/1,2,3,4,5,6,11,13,15,24/

!-----------
! Begin Code
!-----------
      do jj=1,10
         k=-photon(jj)
         j= photon(jj)
         call add3vert(22,j,k)     !photon
      enddo
      end

      Subroutine SetQFD
!**************************************************************************
!     Sets up QED 3 vertex Couplings
!     Quarks  1 - 6 d u s c b t
!     Leptons 11-16 e v mu v tau v
!     Boson   22-24 photon, Z, W+
!**************************************************************************
      implicit none

! Local Variables

      integer j,k,jj
      integer Z(13),W1(6),W2(6)
      data Z/1,2,3,4,5,6,11,12,13,14,15,16,24/
      data W1/1,3,5,11,13,15/
      data W2/2,4,6,12,14,16/

!-----------
! Begin Code
!-----------
      call add3vert(22,-24,24)       !photon W-W+
      do jj=1,13
         k=-Z(jj)
         j= Z(jj)
         call add3vert(23,j,k)       !Z
      enddo
      do jj=1,6
         j=-W1(jj)
         k= W2(jj)
         call add3vert(-24,j,k)       !W-
         j= -j
         k= -k
         call add3vert(24,j,k)        !W+
      enddo
      call add3vert(25,5,-5)          !Higgs BB
      call add3vert(25,6,-6)          !Higgs TT
      call add3vert(25,24,-24)        !Higgs WW
      call add3vert(25,23,23)         !Higgs ZZ
      call add4vert(24,-24,24,-24)    !W+W-W+W-
      call add4vert(24,-24,23,23)
      call add4vert(24,-24,22,22)
      call add4vert(24,-24,22,23)
      end

!---------------------------

      Subroutine SetQCD
      implicit none

! Local Variables

      integer j,k

!-----------
!  Begin code
!-----------
      call add3vert(9,9,9)     ! ggg
      do j=1,6
         k=-j
         call add3vert(9,j,k)  ! gqq
      enddo
      call add4vert(9,9,9,9)    !gggg  - need 3 copies
      call add4vert(9,9,9,9)    !gggg    for color
      call add4vert(9,9,9,9)    !gggg    flows
      end


      subroutine add4vert(i,j,k,l)
!****************************************************************************
!     Adds the vertex to the 4 vertex function
!****************************************************************************
      implicit none

! Constants

      integer   maxv4
      parameter(maxv4=64)

! Arguments

      integer i,j,k,l

! Local Variables

      integer vertex,n,gluecount,gluon

! External Functions

      integer packv4

! Global Variables

      integer           nv4,v4_table(maxv4)
      common /v4_block/ nv4,v4_table

!-----------
! Begin Code
!-----------
      if (nv4 .eq. maxv4) then
         print*,' V4 vertex table size exceeded '
         stop
      end if
      vertex = packv4(i,j,k,l)
      gluon =  packv4(9,9,9,9)
      gluecount = 0
      do n=1,nv4
         if (vertex .eq. v4_table(n)) then
            if (vertex .eq. gluon .and. gluecount .lt. 3) then
               gluecount =gluecount + 1
            else
               return
            end if
         end if
      end do
      nv4 = nv4 + 1
      v4_table(nv4) = vertex
      end

!-------------------------------------
      integer function packv4(i,j,k,l)
      implicit none

! Arguments

      integer i,j,k,l

! Local Variables

      integer ii,jj,kk,ll,temp

!-----------
!  Begin code
!-----------
      if (i .lt. 0) then
         ii = 100-i
      else
         ii = i
      end if
      if (j .lt. 0) then
         jj = 100-j
      else
         jj = j
      end if
      if (k .lt. 0) then
         kk = 100-k
      else
         kk = k
      end if
      if (l .lt. 0) then
         ll = 100-l
      else
         ll = l
      end if

      if (ii .gt. jj) then
         temp = ii
         ii = jj
         jj = temp
      end if
      if (jj .gt. kk) then
         temp = jj
         jj = kk
         kk = temp
      end if
      if (kk .gt. ll) then
         temp = kk
         kk = ll
         ll = temp
      end if
      if (ii .gt. jj) then
         temp = ii
         ii = jj
         jj = temp
      end if
      if (jj .gt. kk) then
         temp = jj
         jj = kk
         kk = temp
      end if
      if (ii .gt. jj) then
         temp = ii
         ii = jj
         jj = temp
      end if

      packv4 = ((ii*256 + jj)*256 + kk)*256 + ll
      end

!----------------------------------
      subroutine add3vert(i,j,k)
      implicit none

! Constants

      integer   maxv3
      parameter(maxv3=256)

! Arguments

      integer i,j,k

! Local Variables

      integer vertex,n

! External Functions

      integer packv3

! Global Variables

      integer           nv3,v3_table(maxv3)
      common /v3_block/ nv3,v3_table

!-----------
! Begin Code
!-----------
      if (nv3 .eq. maxv3) then
         print*,' V3 vertex table size exceeded '
         stop
      end if
      vertex = packv3(i,j,k)
      do n=1,nv3
         if (vertex .eq. v3_table(n)) return
      end do
      nv3 = nv3 + 1
      v3_table(nv3) = vertex
      end

!---------------------------
      integer function packv3(i,j,k)
      implicit none

! Arguments

      integer i,j,k

! Local Variables

      integer ii,jj,kk,temp

!-----------
!  Begin code
!-----------

      if (i .lt. 0) then
         ii = 100-i
      else
         ii = i
      end if
      if (j .lt. 0) then
         jj = 100-j
      else
         jj = j
      end if
      if (k .lt. 0) then
         kk = 100-k
      else
         kk = k
      end if

      if (ii .gt. jj) then
         temp = ii
         ii = jj
         jj = temp
      end if
      if (jj .gt. kk) then
         temp = jj
         jj = kk
         kk = temp
      end if
      if (ii .gt. jj) then
         temp = ii
         ii = jj
         jj = temp
      end if

      packv3 = (ii*256 + jj)*256 + kk
      end

!------------------------
      Subroutine resetV
      implicit none

! Constants

      integer   maxv3
      parameter(maxv3=256)
      integer   maxv4
      parameter(maxv4=64)

! Global Variables

      integer           nv3,v3_table(maxv3)
      common /v3_block/ nv3,v3_table
      integer           nv4,v4_table(maxv4)
      common /v4_block/ nv4,v4_table

!-----------
! Begin Code
!-----------
      nv3 = 0
      nv4 = 0
      end

!------------------------------------
      integer function v3(l1,l2,l3,n)
      implicit none

! Constants

      integer select(4)
      integer mask(4)
      integer   maxv3
      parameter(maxv3=256)

! Arguments

      integer l1,l2,l3,n

! Local Variables

      integer i,j,nfound,vertex,shift
      logical used(maxv3)

! External Functions

      integer packv3,inverse,iand,ior

! Global Variables

      integer           nv3,v3_table(maxv3)
      common /v3_block/ nv3,v3_table

! Data Tables

      data    select/z'ff0000',z'00ff00',z'0000ff',0/
      data    mask  /z'00ffff',z'ff00ff',z'ffff00',0/

!-----------
!  Begin code
!-----------
      vertex = packv3(l1,l2,l3)
      nfound = 0
      if (iand(vertex,select(1)) .ne. 0) then    ! all 3 lines supplied
         do i=1,nv3
            if (vertex .eq. v3_table(i)) then
               nfound = nfound + 1
               if ( nfound .eq. n) then
                  v3 = 1
                  return
               end if
            end if
         end do
      else               ! one line is missing
         do i=1,nv3
            used(i) = .false.
         end do
         shift = 256*256
         do j=1,3
            do i=1,nv3
               if (vertex .eq. iand(v3_table(i),mask(j))) then
                  if (.not.used(i)) then
                     nfound = nfound + 1
                     used(i) = .true.
                  end if
                  if (nfound .eq. n) then
                     v3 = iand(v3_table(i),select(j))/shift
                     if (v3 .gt. 100) v3 = 100-v3
                     v3 = inverse(v3)
                     return
                  end if
               end if
            end do
            vertex = ior(iand(vertex,select(j+1))*256,
     &                   iand(vertex,  mask(j+1)))
            shift = shift/256
         end do
      end if
      v3 = 0
      end
