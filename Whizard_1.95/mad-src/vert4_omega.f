!-----------------------
! madgraph - a Feynman Diagram package by Tim Stelzer and Bill Long 
! (c) 1993
!
! Filename: vert4.f
!-----------------------

      subroutine writeSSSS(i)
!************************************************************************
!     Routine to write fermion 4 vector vertex
!     currently only does gluons, not A,W,Z
!************************************************************************
      implicit none

! Constants

      integer   maxcount,   from,  type,  flav,  calc,  dir,  yes
      parameter(maxcount=40,from=1,type=2,flav=3,calc=5,dir=4,yes=1)

! Arguments

      integer i

! Local Variables

      integer j,ncalced
      integer missing,lv(4)
      character*72 buff
      character*42 buff_hsss
      character*42 buff_ssss

! Global Variables

      integer iv3(maxcount,6),iv4(maxcount,6),iline(maxcount,5),nlines
      Common/tohelas/iv3,iv4,iline,nlines
      logical alldone
      integer number
      common/total/alldone,number

! Data Tables

      data buff_ssss /'SSSSXX(W   ,W   ,W   ,W   ,GHHHH,AMP(   ))'/
      data buff_hsss /'HSSSXX(W   ,W   ,W   ,GHHHH,HMASS,HWIDTH,W'/

!-----------
! Begin Code
!-----------
      ncalced=0
      do j=1,4
         if (iline(iv4(i,j),calc) .eq. yes) then
            ncalced=ncalced+1
            lv(ncalced) = iv4(i,j)
         else
            missing=iv4(i,j)
         endif
      enddo
      if (ncalced .le. 2) then
         return
      elseif (ncalced .eq. 3) then
         buff=buff_hsss
         call makestring(lv(1),buff( 9:11))
         call makestring(lv(2),buff(14:16))
         call makestring(lv(3),buff(19:21))
         call optcheck(buff,missing,42)
         iline(missing,calc) = yes
         iv4(i,calc) = yes
      else if (ncalced .eq. 4) then            !all lines known, gives answer
         buff = buff_ssss
         call makestring(lv(1),buff( 9:11))
         call makestring(lv(2),buff(14:16))
         call makestring(lv(3),buff(19:21))
         call makestring(lv(4),buff(24:26))
         call makestringamp(number,buff(38:40))
         call writebuff(buff(1:42))
         iv4(i,calc) = yes
      else
         print*,'Error more than 4 lines in HHHH vertex',ncalced
         stop
      endif
      end

      subroutine writeVVSS(i)
!************************************************************************
!     Routine to write VVSS vertex
!************************************************************************
      implicit none

! Constants

      integer   maxcount,   from,  type,  flav,  calc,  dir,  yes
      parameter(maxcount=40,from=1,type=2,flav=3,calc=5,dir=4,yes=1)

! Arguments

      integer i

! Local Variables

      integer j,ncalced
      integer missing,lv(4),w(4)
      character*72 buff
      character*41 buff_hvvs,buff_jvss,buff_vvss
      character*1 S1,S2
      integer pid(4),pack
      integer hvvs(2),jvss(3),vvss(2)

! Global Variables

      integer iv3(maxcount,6),iv4(maxcount,6),iline(maxcount,5),nlines
      Common/tohelas/iv3,iv4,iline,nlines
      logical alldone
      integer number
      common/total/alldone,number

! Data Tables

      data buff_hvvs /'HVVSXX(W   ,W   ,W   ,G  H,HMASS,HWIDTH,W'/
      data buff_jvss /'JVSSXX(W   ,W   ,W   ,G  H, MASS, WIDTH,W'/
      data buff_vvss /'VVSSXX(W   ,W   ,W   ,W   ,G  H,AMP(   ))'/
      data hvvs / 134, 122/
      data jvss / 112, 113, 114/
      data vvss / 1122, 1134/

!-----------
! Begin Code
!-----------
      ncalced=0
      ncalced=0
      do j=1,4
         if (iline(iv4(i,j),calc) .eq. yes) then
            ncalced=ncalced+1
            lv(ncalced) = iv4(i,j)
            if (abs(iline(iv4(i,j),flav)) .eq. 3) then
               if (iline(iv4(i,j),dir) .eq. 1) then
                  pid(ncalced) = 4
               else
                  pid(ncalced) = 3
               end if
            else
               pid(ncalced)= iline(iv4(i,j),flav)
            end if 
          else
            missing=iv4(i,j)
         endif
      enddo
      call  sort2(pid,lv,ncalced)
      pack = 0
      do j=1,ncalced
         pack = pack*10 + pid(j)
      end do
      if (ncalced .le. 2) then
         return
      elseif (ncalced .eq. 3) then
         if (pack .eq. hvvs(1)) then
            w(1)=lv(3)
            w(2)=lv(2)
            w(3)=lv(1)
            S1='W'
            S2='W'
            buff = buff_hvvs
         elseif(pack .eq. hvvs(2)) then
            w(1)=lv(3)
            w(2)=lv(2)
            w(3)=lv(1)
            S1='Z'
            S2='Z'
            buff = buff_hvvs
         elseif(pack .eq. jvss(1)) then
            w(1)=lv(3)
            w(2)=lv(2)
            w(3)=lv(1)
            S1='Z'
            S2='Z'
            buff = buff_jvss
            buff(28:28)='Z'
            buff(34:34)='Z'
         elseif(pack .eq. jvss(2)) then
            w(1)=lv(3)
            w(2)=lv(2)
            w(3)=lv(1)
            S1='W'
            S2='W'
            buff = buff_jvss
            buff(28:28)='W'
            buff(34:34)='W'
            iline(missing,dir) = -1  
         elseif(pack .eq. jvss(3)) then
            w(1)=lv(3)
            w(2)=lv(2)
            w(3)=lv(1)
            S1='W'
            S2='W'
            buff = buff_jvss
            buff(28:28)='W'
            buff(34:34)='W'
            iline(missing,dir) = +1 
         else
            Print*,'Error in VVSS routine unknown vertex',pack
         endif
         call makestring(w(1),buff( 9:11))
         call makestring(w(2),buff(14:16))
         call makestring(w(3),buff(19:21))
         buff(24:24) = s1
         buff(25:25) = s2
         call optcheck(buff,missing,41)
         iline(missing,calc) = yes
         iv4(i,calc) = yes
      else if (ncalced .eq. 4) then            !all lines known, gives answer
         buff=buff_vvss
         call makestring(lv(4),buff( 9:11))
         call makestring(lv(3),buff(14:16))
         call makestring(lv(2),buff(19:21))
         call makestring(lv(1),buff(24:26))
         call makestringamp(number,buff(37:39))
         iv4(i,calc) = yes
         if (pack .eq. vvss(1)) then
            buff(29:30)='ZZ'
         elseif (pack .eq. vvss(2)) then
            buff(29:30)='WW'
         else
            print*,'Error in VVSS unknown vertex',pack
         endif
         call writebuff(buff(1:41))
      else
         print*,'Error more than 4 lines in HHHH vertex',ncalced
         stop
      endif
      end

