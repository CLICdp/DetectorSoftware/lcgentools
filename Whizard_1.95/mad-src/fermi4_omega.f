!-----------------------
! madgraph - a Feynman Diagram package by Tim Stelzer and Bill Long 
! (c) 1993
!
! Filename: fermi4.f
!-----------------------

      subroutine GetFermiFact
!****************************************************************************
!     Driver for finding fermi factors for antisymmetric graphs with
!     at least 4 identical fermions
!     Input:
!           common/tohelas/iv3.....
!     Output:
!           fact = +-1 for each graph
!****************************************************************************
      implicit none

! Constants

      integer    maxgraphs
      parameter (maxgraphs=999)

! Local Variables

      integer ifac,nline1,nline2,order(12),i
      integer nline(6),nfound

! Global Variables

      logical done
      integer number
      common /total/done,number
      integer symfact(maxgraphs)
      common /tosymmetry/symfact

!-----------
! Begin Code
!-----------
      ifac=1
      call findstart(nline,nfound)
      do i=1,nfound
         nline1=nline(i)
         call followfermi(nline1,nline2)
         order(2*i-1) = nline1
         order(2*i)   = nline2
      enddo
      call isort(order,ifac,nfound)
      symfact(number)=ifac
      end

      
      Subroutine FindStart(nline,nfound)
!*****************************************************************************
!     Output:
!           nline(6)   line number of external particle flowing out
!           nfound     number of external outflowing fermions found
!*****************************************************************************
      implicit none      

! Constants

      integer   fermion
      parameter(fermion=3)
      integer   maxcount,   from,  type,  flav,  calc,  dir,  yes
      parameter(maxcount=40,from=1,type=2,flav=3,calc=5,dir=4,yes=1)

! Arguments

      integer nline(6),nfound

! Local Variables

      integer i

! Global Variables

      integer iv3(maxcount,6),iv4(maxcount,6),iline(maxcount,5),nlines
      Common/tohelas/iv3,iv4,iline,nlines

!-----------
! Begin Code
!-----------
      nfound = 0
      do i=1,nlines
         if (iline(i,from) .le. 2) then               !External Partical
            if (iline(i,type) .eq. fermion) then      !Fermion
               if (iline(i,dir) .eq. -1) then         !Follow trail backward
                  nfound=nfound+1
                  nline(nfound) = i
               endif
            endif
         endif
      enddo
      end


      Subroutine FollowFermi(nline1,nline2)
!*****************************************************************************
!     Input:
!           nline1   line of external outgoing fermion
!     Output:
!           nline2   line number of external particle flowing out
!*****************************************************************************
      implicit none      

! Constants

      integer   fermion
      parameter(fermion=3)
      integer   maxcount,   from,  type,  flav,  calc,  dir,  yes
      parameter(maxcount=40,from=1,type=2,flav=3,calc=5,dir=4,yes=1)

! Arguments

      integer nline1,nline2

! Local Variables

      integer i,j,nvert,oldvert,lnum
      logical finished,foundit

! Global Variables

      integer iv3(maxcount,6),iv4(maxcount,6),iline(maxcount,5),nlines
      Common/tohelas/iv3,iv4,iline,nlines

!-----------
! Begin Code
!-----------
      nline2 = nline1
      finished = .false.
      oldvert= 0
      do while (.not. finished)
         foundit= .false.
         nvert  = 0
         do while (.not. foundit .and. nvert .le. maxcount)
            nvert=nvert+1
            if (nvert .eq. oldvert) nvert=nvert+1
            do j=1,3
               if (iv3(nvert,j) .eq. nline2) then
                  foundit = .true.
                  lnum = j
               endif
            enddo
         enddo
         if (.not. foundit)
     &        print*,'Error vertex for line not found',nline2
         foundit=.false.
         i = 0
         do while (i .le. 3 .and. .not. foundit)
            i=i+1
            if (i .ne. lnum .and.
     &      iline(iv3(nvert,i),type) .eq. fermion) then
               nline2=iv3(nvert,i)
               oldvert=nvert
               foundit = .true.
            endif
         enddo
         if (.not. foundit)
     &        print*,'Error did not find 2nd fermion in iv3=',nvert
         if (iline(nline2,from) .le. 2) then !External
            finished = .true.
         endif
      enddo
      end
      
      Subroutine isort(order,ifac,nfound)
!****************************************************************************
!     Sorts the line numbers, keeping track of the number of permutations
!     Input:
!           order(12)       12 lines in order found
!     Output:
!           ifac            -1**(number of permutations to put in order
!****************************************************************************
      implicit none

! Arguments

      integer ifac,order(12),nfound

! Local Variables

      integer i,j,itemp

!-----------
! Begin Code
!-----------
      ifac = 1
      do i=1,2*nfound-1
         do j=1,2*nfound-1
            if (order(j) .ge. order(j+1)) then
               itemp=order(j)
               order(j)=order(j+1)
               order(j+1) = itemp
               ifac=-ifac
            endif
         enddo
      enddo
      do i=1,2*nfound-1
         if (order(i) .ge. order(i+1)) then
            print*,'Error order not correct',i,order(i),order(i+1)
         endif
      enddo
      end
