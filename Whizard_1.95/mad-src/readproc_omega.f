!-----------------------
! madgraph - a Feynman Diagram package by Tim Stelzer and Bill Long 
! (c) 1993
!
! Filename: readproc.f
!-----------------------
 
      subroutine readproc(more)
!************************************************************************
!     Reads character string and converts to the appropriate process
!************************************************************************
      implicit none

! Constants

      integer    maxlines,   n1,    n2,    n3
      parameter (maxlines=8, n1=10, n2=13, n3=7)

! Arguments
      
      logical more

! Local Variables

      character*80 process
      character*80 attempt
      character*(1*n1) match1
      character*(2*n2) match2
      character*(3*n3) match3
      character*(2*n2) name2
      character*(2*n3) name3
      character*10 snum
      integer icode1(n1),icode2(n2),icode3(n3)
      integer i,nparticles,iqcdmin,iqcdmax,j
      integer in1,in2,in3,imin
      logical incoming,done

! External Functions

      integer inverse

! Global Variables

      integer        iline(-maxlines:maxlines)
      common/to_proc/iline
      integer         nincoming
      common/to_proc2/nincoming
      integer         noqcd,noqed,nqfd
      common/to_order/noqcd,noqed,nqfd
      character*15 name
      integer iname
      common/to_name/iname,name
      character*60 proc
      integer iproc
      common/to_write/iproc,proc

      data snum /'0123456789'/
      data match1/'duscbtgazh'/
      data icode1/1,2,3,4,5,6,9,22,23,25/
      data match2/'d~u~s~c~b~t~e-e+w-w+vevmvt'/
      data name2/'dbubsbcbbbtbemepwmwpvevmvt'/
      data icode2/-1,-2,-3,-4,-5,-6,11,-11,-24,24,12,14,16/
      data match3/'ve~vm~vt~mu-mu+ta-ta+'/
      data name3/'vevmvtmumutata'/
      data icode3/-12,-14,-16,13,-13,15,-15/

!-----------
! Begin Code
!-----------
      iname=0
      noqcd=0
      noqed=0
      nqfd =0
      more =.true.
      incoming = .true.
      print*,'Standard Model particles include:'
      print*,'    Quarks:   d u s c b t d~ u~ s~ c~ b~ t~'
      print*,'    Leptons:  e- mu- ta- e+ mu+ ta+ ve vm vt ve~ vm~ vt~'
      print*,'    Bosons:   g a z w+ w- h'
      print*,' '
      print*, 'Enter process you would like calculated ',
     &     'in the form  e+ e- -> a.'
      print*, '(<return> to exit MadGraph.)'
      read '(a)',process
      if (process .eq. ' ') then
         more = .false.
         return
      else
         more = .true.
      end if
!
! Convert to lowercase characters
!
      call lower_case(process)
!
! Parse string for particles
!
      nparticles=0
      i = 1
      j = 21
      attempt = 'Attempting Process: '
      do while(i .lt. 75)
         in3 = index(match3,process(i:i+2))-1
         in2 = index(match2,process(i:i+1))-1
         in1 = index(match1,process(i:i+0))-1
         if (mod(in3,3) .eq. 0) then
            nparticles=nparticles+1
            iline(nparticles)= icode3(in3/3+1)
            if (incoming) iline(nparticles)=inverse(iline(nparticles))
            attempt(j:j+2) = process(i:i+2)
            j=j+4
            i=i+3
         elseif (mod(in2,2) .eq. 0) then
            nparticles=nparticles+1
            iline(nparticles)= icode2(in2/2+1)
            if (incoming) iline(nparticles)=inverse(iline(nparticles))
            attempt(j:j+1) = process(i:i+1)
            j=j+3
            i=i+2
         elseif (mod(in1,1) .eq. 0 .and. in1 .ge. 0) then
            nparticles=nparticles+1
            iline(nparticles)= icode1(in1/1+1)
            if (incoming) iline(nparticles)=inverse(iline(nparticles))
            attempt(j:j) =  process(i:i)
            j=j+2
            i=i+1
         elseif(process(i:i) .eq. '>') then
            attempt(j:j+3) =  ' -> '
            j=j+4
            i=i+1
            incoming=.false.
            nincoming = nparticles
         else
            i=i+1
         endif
      enddo
      write (*,'(1x/a)') attempt(1:j)
      iproc = j-18
      proc = attempt(19:j)
!
! Determine Order in QCD and QFD
!
      iline(0)=nparticles
      if (nparticles .le. 0) then
         print*,'No process specified.  Exiting MadGraph.'
         more = .false.
         return
      endif
      print*,' '
      do i=1,nparticles
         if (iline(i) .eq. 9) then
            noqcd=noqcd+1
         elseif (abs(iline(i)) .gt. 9) then
            noqed=noqed+1
            if (abs(iline(i)) .ne. 22 .and. abs(iline(i)) .ne. 11 .and.
     &          abs(iline(i)) .ne. 13 .and. abs(iline(i)) .ne. 15) then
               nqfd=1
            endif
         endif
      enddo
      if (noqcd .gt. nparticles-2) noqcd=nparticles-2
      if (noqed .gt. nparticles-2) noqed=nparticles-2
      iqcdmin =noqcd
      iqcdmax =nparticles-2-noqed
      noqcd= iqcdmax
      if (noqed+noqcd .gt. nparticles-2) then
         print*,'Sorry this process is not possible at tree level'
         stop
      endif
      write(*,'(a,i1,a,i1,a,i1,a)')
     &     'Enter the number of QCD vertices between ',
     &     iqcdmin,' and ',iqcdmax,' (',noqcd,'): '
      read(*,'(a)') process
      i=0
      done=.false.
      do while(i .lt. 25 .and. .not. done)
         i=i+1
         in1 = index(snum(iqcdmin+1:iqcdmax+1),process(i:i))
         if (in1 .ne. 0) then
            noqcd = iqcdmin+in1-1
            done=.true.
         endif
      enddo
      if (noqcd .gt. nparticles-2) then
         print*,'Warning Maximum order for the process is',nparticles-2
         stop
      endif
      noqed = nparticles-2-noqcd
      write(*,'(a,i1)') 'The number of QFD vertices is ',noqed
      if (nqfd .eq. 0 .and. noqed .gt. 0) then
         write(*,'(a)') 'Would you like to include the Weak sector (n)?'
         read(*,'(a)') process
         nqfd=index(process,'y')
         if (nqfd .eq. 0) nqfd = index(process,'Y')
         if (nqfd .ne. 0) nqfd = 1
      elseif (noqed .gt. 0) then
         write(*,'(a)') 'QFD required for this process ok?: '
         read(*,'(a)') process
      else
         write(*,'(a)') 'No QFD possible all QCD ok?: '
         read(*,'(a)') process
      endif
!
! make name for process
!
      incoming=.true.
      do i=1,iline(0)
         if (i .le. nincoming) iline(i)=inverse(iline(i))
         if (iname .gt. 14) then
            print*,'name too large',name
            stop
         endif
         if (i .eq. nincoming+1) then
            iname=iname+1
            name(iname:iname)='_'
            incoming=.false.
         endif
         do j=1,n1
            if (iline(i) .eq. icode1(j)) then
               iname=iname+1
               name(iname:iname)=match1(j:j)
            endif
         enddo
         do j=1,n2
            if (iline(i) .eq. icode2(j)) then
               iname=iname+2
               name(iname-1:iname)=name2(2*j-1:2*j)
            endif
         enddo
         do j=1,n3
            if (iline(i) .eq. icode3(j)) then
               iname=iname+2
               name(iname-1:iname)=name3(2*j-1:2*j)
            endif
         enddo
         if (i .le. nincoming) iline(i)=inverse(iline(i))
      enddo
!
! Query name for process
!
      write(*,'(a,a,a)') 'Enter a name to identify process (',
     &     name(1:iname),'): '
      read(*,'(a)') process
      i=1
      do while (i .lt. 75 .and. (ichar(process(i:i)) .lt. 65 .or.
     &     ichar(process(i:i)) .gt. 122 .or.
     &     (ichar(process(i:i)) .gt. 90 .and.
     &     ichar(process(i:i)) .lt. 97)))
         i=i+1
      enddo
      imin = i
      do while (i .lt. 75 .and. process(i:i) .ne.' ')
         i=i+1
      enddo
      if (imin .ne. i) then
         if (i-imin .gt. 14) then
            print*,'Truncating name'
            i=imin+14
         endif
         iname=i-imin
         name(1:iname)=process(imin:i)
      endif

! Set up the Coupling Vertices
      call resetv
      if (noqcd .gt. 0) call setQCD
      if (noqed .gt. 0) call setQED
      if (noqed .gt. 0 .and. nqfd .gt. 0) call setQFD
      end

