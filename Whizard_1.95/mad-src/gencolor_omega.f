!-----------------------
! madgraph - a Feynman Diagram package by Tim Stelzer and Bill Long 
! (c) 1993
!
! Filename: gencolor.f
!-----------------------
 
      Subroutine MatrixColor(graphcolor,sflows)
!**************************************************************************
!     write out all color factors and interference terms
!**************************************************************************
      implicit none

! Constants

      integer    maxtops,     maxnodes,  maxgraphs
      parameter (maxtops=2500,maxnodes=5,maxgraphs=999)
      integer    maxlines,  maxfactors,  maxterms,    maxflows
      parameter (maxlines=8,maxfactors=9,maxterms=250,maxflows=200)
      integer    maxcolors
      parameter (maxcolors=250)

! Arguments

      integer graphcolor(0:2,0:maxflows,0:maxgraphs)
      double precision  sflows(maxflows,maxflows)

! Local Variables

      integer igraph,jgraph,iterm,jterm,j
      integer identpart(-30:30)
      integer ngraphs,iplace(maxgraphs),nrows,irow,ikill
      double precision  sum,den
      double precision  color_mat(maxgraphs)
      double precision  red_color(maxcolors,maxgraphs),denom(maxgraphs)
      double precision  eigen_val(maxcolors)

! Global Variables

      integer        iline(-maxlines:maxlines)
      common/to_proc/iline
      integer         nincoming
      common/to_proc2/nincoming

!-----------
! Begin Code
!-----------
      do j=-30,30
         identpart(j)=0                   !Number of identical final parts
      enddo
      den=1d0
      do j=1,nincoming
         if (abs(iline(j)) .eq. 9) then        !gluon
            den=den*8d0
         elseif (abs(iline(j)) .le. 6) then    !fermion quark
            den=den*3d0
         elseif (abs(iline(j)) .le. 16 .and.
     &           abs(iline(j)) .ge. 11) then   !fermion not quark
            den=den*1d0
         endif
      enddo
      do j=nincoming+1,iline(0)                !over all final state lines
         identpart(iline(j)) = identpart(iline(j))+1
         den=den*identpart(iline(j))           ! n! for identical final particle
      enddo
      ngraphs=graphcolor(0,0,0)
      nrows = 0
      do igraph=1,ngraphs
         do jgraph=1,ngraphs
            sum=0
            do iterm=1,graphcolor(0,0,igraph)
               do jterm=1,graphcolor(0,0,jgraph)
                  sum=sum+sflows(graphcolor(0,iterm,igraph),
     &                 graphcolor(0,jterm,jgraph))*
     &                 dble(graphcolor(1,iterm,igraph))*
     &                 dble(graphcolor(1,jterm,jgraph))/
     &                 dble(graphcolor(2,iterm,igraph))/
     &                 dble(graphcolor(2,jterm,jgraph))
               enddo
            enddo
            if (abs(sum) .lt. 1e-8) sum=0
            color_mat(jgraph)=sum/den
         enddo
         call compresscolor(igraph,ngraphs,color_mat,red_color,
     &       nrows,iplace,denom)
      enddo
      print*,'Reduced color matrix',nrows,ngraphs
      jgraph=0
      ikill =0
      do irow=1,nrows
         jgraph=jgraph+1
         do while(jgraph .le. ngraphs .and. iplace(jgraph) .ne. irow)
            call killcolumn(red_color,jgraph-ikill,nrows,ngraphs)
            ikill=ikill+1
            jgraph=jgraph+1
         enddo
         if (jgraph .gt. ngraphs) then
            print*,'Error did not find all rows',jgraph,ngraphs,irow
            stop
         endif
      enddo

      call eigensys(red_color,eigen_val,nrows)
      call gencode(red_color,eigen_val,ngraphs,nrows,iplace,denom)

      end

      Subroutine gencode(eigen_vec,eigen_val,ngraphs,nrows,iplace,denom)
!***************************************************************************
!     This routine writes out the fortran code to do color factors
!     for all of the graphs.
!     Input:
!          Eigen_vec,Eigenval  -> eigen vectors and values of color matrix
!          ngraphs             -> number of graphs
!
!***************************************************************************
      implicit none
! Constants

      integer   maxgraphs,    maxcolors
      parameter(maxgraphs=999,maxcolors=250)
      double precision zero
      parameter(zero=0d0)

! Arguments

      double precision Eigen_vec(maxcolors,maxgraphs)
      double precision Eigen_val(maxcolors)
      double precision denom(maxgraphs)
      integer ngraphs,nrows,iplace(maxgraphs)

! Local Variables

      integer i,j
      character*3 str1,str2
      character*40 buff
      double precision temp_vec(maxgraphs),sum

! Global Variables

      integer            symfact(maxgraphs)
      common /tosymmetry/symfact
      integer          neig
      common /to_color/neig

!-----------
! Begin Code
!-----------
      neig=0
      do i=1,nrows
         if (abs(eigen_val(i)) .gt. 1d-9) then
            neig=neig+1
            call makestringamp(neig,str1)
            buff = '      DATA EIGEN_VAL('//str1//')/'
            sum=0d0
            do j=1,ngraphs
               temp_vec(j)=eigen_vec(iplace(j),i)/denom(i)/denom(j)
               sum = sum+temp_vec(j)**2
            enddo
            sum=sqrt(sum)
c            print*,'In writecode i,sum',i,sum,denom(i)
            do j=1,ngraphs
               temp_vec(j)=temp_vec(j)/sum
            enddo
            write(99,20) buff,eigen_val(i)*sum*sum,'/'
            do j=1,ngraphs
               call makestringamp(j,str2)
               buff = '      DATA EIGEN_VEC('//str2//','//str1//')/'
               if (abs(temp_vec(j)) .gt. 1d-12) then
                  write(99,20) buff,temp_vec(j)*dble(symfact(j)),'/'
               else
                  write(99,20) buff,zero,'/'
               endif
            enddo
         endif
      enddo
 20   format (a31,1pd24.16,a2)
      end


      Subroutine CompressColor(irow,ngraphs,new,red_color,
     &     nrows,iplace,den)
!***********************************************************************
!  Compresses color matrix down to reduce size
!***********************************************************************

! Constants

      integer    maxtops,     maxnodes,  maxgraphs
      parameter (maxtops=2500,maxnodes=5,maxgraphs=999)
      integer    maxlines,  maxfactors,  maxterms,    maxflows
      parameter (maxlines=8,maxfactors=9,maxterms=250,maxflows=200)
      integer    maxcolors
      parameter (maxcolors=250)

! Arguments

      double precision  red_color(maxcolors,maxgraphs),den(maxgraphs)
      double precision  new(maxgraphs)
      integer ngraphs,irow,iplace(maxgraphs),nrows

! Local 

      logical found,gotfac
      integer i

!  External

      logical equal

!-----------
! Begin Code
!-----------

      sum=0d0
      do i=irow,ngraphs
         den(i)=1d0
      enddo
      found=.false.
      i = 0
      do while (.not. found .and. i .lt. nrows)
         found = .true.
         gotfac = .false.
         i = i+1
         j = 0
         do while (found .and. j .lt. ngraphs)
            j=j+1
            if (.not. gotfac) then
               if (new(j) .ne. 0d0) then
                  den(irow)=red_color(i,j)/new(j)
                  gotfac = .true.
               endif
            endif
            found = equal(new(j)*den(irow),red_color(i,j))
         enddo
      enddo
      if (.not. found) then
         if (nrows .gt. maxcolors) then
            print*,'Error too many color combination, reset maxcolors',
     &           maxcolors
         endif
         nrows=nrows+1
         iplace(irow) = nrows
         den(irow) = 1d0 
         do i=1,ngraphs
            red_color(nrows,i)=new(i)
         enddo
c         print*,'Added new one',nrows,irow,den(irow)
      else
         iplace(irow) = i
      endif
      end

      logical function equal(x,y)
!************************************************************************
!     Determines if two double precision numbers are equal to tol
!************************************************************************
      implicit none

!  Constants
      
      double precision tol
      parameter       (tol = 1e-10)

!  Arguments

      double precision x,y

!-----------
! Begin Code
!-----------
      if (x .eq. 0d0 .and. y .eq. 0d0) then
         equal = .true.
      elseif(x+y .eq. 0) then
         equal=.false.
      else
         equal = (abs(x-y)/abs(x+y) .lt. tol)
      endif
      end

      subroutine killcolumn(red_color,jcol,nrows,ngraphs)
!***********************************************************************
!  Compresses color matrix down to reduce size
!***********************************************************************
      implicit none

! Constants

      integer    maxtops,     maxnodes,  maxgraphs
      parameter (maxtops=2500,maxnodes=5,maxgraphs=999)
      integer    maxcolors
      parameter (maxcolors=250)

! Arguments

      double precision  red_color(maxcolors,maxgraphs)
      integer jcol,nrows,ngraphs

! Local

      integer icol,irow

!---------
! Begin Code
!---------

      do icol=jcol,ngraphs
         do irow=1,nrows
            red_color(irow,icol)=red_color(irow,icol+1)
         enddo
      enddo
      end


