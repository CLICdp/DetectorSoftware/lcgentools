!-----------------------
! madgraph - a Feynman Diagram package by Tim Stelzer and Bill Long 
! (c) 1993,1994
!
! Filename: vertex.f
!-----------------------

      subroutine writeW3W3(i)
!************************************************************************
!     writeW3W3 writes calls to JW3WXX and W3W3XX
!    
!************************************************************************

      implicit none

! Constants

      integer   maxcount,   from,  type,  flav,  calc,  dir,  yes
      parameter(maxcount=40,from=1,type=2,flav=3,calc=5,dir=4,yes=1)

! Arguments

      integer i

! Local Variables

      integer j,k,bptr,ncalced,missing
      integer lv(4),w(4),pid(4),pack
      character*72 buff

      integer           jw3w(8),tptr_jw3w(8),pptr_jw3w(8),perm_jw3w(3,3)
      integer                    dir_jw3w(8)
      character*37 tail_jw3w(7)
      character*22 head_jw3w
      integer           w3w3(3),tptr_w3w3(3),pptr_w3w3(3),perm_w3w3(4,2)
      character*32 tail_w3w3(3)
      character*27 head_w3w3

! Global Variables

      integer iv3(maxcount,6),iv4(maxcount,6),iline(maxcount,5),nlines
      Common/tohelas/iv3,iv4,iline,nlines
      integer number
      logical alldone
      common/total/alldone,number


!  Lookup table scheme for reordering lines for Helas Calls.
!
!  Particle codes:  1 = A (photon)
!                   2 = Z
!                   3 = W-
!                  -3 = W+ :: Changed to 4 for table lookup purposes only
!
!
!  Tables for calls to W3W3XX (See p. 91, Helas Manual)
!
!  Lookup WM W31 WP W32     Correct    Input   Perm3  Perm3  Buff_tail
!  Index                     Order     Order          Index  Index
!
!     1    W-  Z  W+  Z     3 2 4 2    2 2 3 4  3142    1      1
!     2    W-  Z  W+  A     3 2 4 1    1 2 3 4  3241    2      2
!     3    W-  A  W+  A     3 1 4 1    1 1 3 4  3142    1      3
!      
      data w3w3/      2234,1234,1134/
      data pptr_w3w3/    1,   2,   1/
      data tptr_w3w3/    1,   2,   3/
      data perm_w3w3/3,1,4,2,  ! 1
     &               3,2,4,1/  ! 2
      data head_w3w3/'W3W3XX(W   ,W   ,W   ,W   ,'/
      data tail_w3w3/'GWWZ,GWWZ,WMASS,WWIDTH,AMP(   ))',   ! W-  Z  W+  Z    1
     &               'GWWZ,GWWA,WMASS,WWIDTH,AMP(   ))',   ! W-  Z  W+  A    2
     &               'GWWA,GWWA,WMASS,WWIDTH,AMP(   ))'/   ! W-  A  W+  A    3   
!
!
!  Tables for calls to JW3WXX (See p. 94, Helas Manual)
!
!  Lookup W1 W2 W3      Correct    Input   Perm3  Perm3  Buff_tail Missing W
!  Index                 Order     Order          Index  Index       dir
!
!     1    W- Z  W+      3 2 4     2 3 4    213     1     1,2         1
!     2    W- A  W+      3 1 4     1 3 4    213     1     3,4         1
!     3    Z  W- Z       2 3 2     2 2 3    132     2     5          -1
!     4    Z  W+ Z       2 4 2     2 2 4    132     2     5           1
!     5    Z  W- A       2 3 1     1 2 3    231     3     6          -1
!     6    Z  W+ A       2 4 1     1 2 4    231     3     6           1
!     7    A  W- A       1 3 1     1 1 3    132     2     7          -1
!     8    A  W+ A       1 4 1     1 1 4    132     2     7           1
!
      data jw3w/     234,134,223,224,123,124,113,114/
      data pptr_jw3w/  1,  1,  2,  2,  3,  3,  2,  2/
      data tptr_jw3w/ -1, -3,  5,  5,  6,  6,  7,  7/
      data  dir_jw3w/  1,  1, -1,  1, -1,  1, -1,  1/
      data perm_jw3w/2,1,3,  ! 1
     &               1,3,2,  ! 2
     &               2,3,1/  ! 3
      data head_jw3w/'JW3WXX(W   ,W   ,W   ,'/
      data tail_jw3w/'GWWZ,GWWZ,WMASS,WWIDTH,ZMASS,ZWIDTH,W',   ! W- Z W+ : Z    1
     &               'GWWZ,GWWA,WMASS,WWIDTH,AMASS,AWIDTH,W',   ! W- Z W+ : A    2
     &               'GWWA,GWWZ,WMASS,WWIDTH,ZMASS,ZWIDTH,W',   ! W- A W+ : Z    3
     &               'GWWA,GWWA,WMASS,WWIDTH,AMASS,AWIDTH,W',   ! W- A W+ : A    4
     &               'GWWZ,GWWZ,WMASS,WWIDTH,WMASS,WWIDTH,W',   ! Z W+- Z : W-+  5
     &               'GWWZ,GWWA,WMASS,WWIDTH,WMASS,WWIDTH,W',   ! Z W+- A : W-+  6
     &               'GWWA,GWWA,WMASS,WWIDTH,WMASS,WWIDTH,W'/   ! A W+- A : W-+  7
!-----------
! Begin Code
!-----------

      ncalced = 0
      do j=1,4
         if (iline(iv4(i,j),calc) .eq. yes) then
            ncalced = ncalced+1
            lv(ncalced) = iv4(i,j)
            if (abs(iline(iv4(i,j),flav)) .eq. 3) then
               if ( iline(iv4(i,j),dir) .eq. 1) then
                  pid(ncalced) = 4
               else
                  pid(ncalced) = 3
               end if
            else
               pid(ncalced)= iline(iv4(i,j),flav)
            end if               
         else
            missing = iv4(i,j)
         end if
      end do
      call sort2(pid,lv,ncalced)
      pack = 0
      do j=1,ncalced
         pack = pack*10 + pid(j)
      end do
      
      if (ncalced .eq. 3) then                     ! Prepare output string for JW3WXX call
         j = 1
         do while(j .le. 8 .and. pack .ne. jw3w(j))
            j = j + 1
         end do
         if (j .eq. 9) then
            print*,'Illegal vertex combination in Writing JW3W'
         else
            do k=1,3
               w(k) = lv(perm_jw3w(k,pptr_jw3w(j)))
            end do
            if (tptr_jw3w(j) .lt. 0) then
               if (iline(missing,flav) .eq. 1) then      ! A
                  bptr = -tptr_jw3w(j)+1
               else
                  bptr = -tptr_jw3w(j)                   ! Z
               end if
            else
               bptr = tptr_jw3w(j)                       ! W+-
            end if
            buff(1:22) =  head_jw3w
            call makestring(w(1),buff( 9:11))
            call makestring(w(2),buff(14:16))
            call makestring(w(3),buff(19:21))
            buff(23:59) = tail_jw3w(bptr)
            call optcheck(buff,missing,59)
            iline(missing,calc) = yes
            iline(missing,dir ) = dir_jw3w(j)
            iv4(i,calc) = yes
         end if
      else if (ncalced .eq. 4) then ! Prepare output string for W3W3XXOB call
         j = 1
         do while(j .le. 3 .and. pack .ne. w3w3(j))
            j = j + 1
         end do
         if (j .eq. 4) then
            print*,'Illegal vertex combination in Writing W3W3'
         else
            do k=1,4
               w(k) = lv(perm_w3w3(k,pptr_w3w3(j)))
            end do
            bptr = tptr_w3w3(j) 
            buff(1:27) =  head_w3w3
            call makestring(w(1),buff( 9:11))
            call makestring(w(2),buff(14:16))
            call makestring(w(3),buff(19:21))
            call makestring(w(4),buff(24:26))
            buff(28:59) = tail_w3w3(bptr)
            call makestringamp(number,buff(55:57))
            call writebuff(buff(1:59))
            iv4(i,calc) = yes
         end if
      else
         print*,' Illegal value for ncalced in WriteW3W3 ',ncalced
      end if
      end

      subroutine writeWWWW(i)
!************************************************************************
!     writeWWWW writes calls to JWWWXX and WWWWXX
!     
!************************************************************************

      implicit none

! Constants

      integer   maxcount,   from,  type,  flav,  calc,  dir,  yes
      parameter(maxcount=40,from=1,type=2,flav=3,calc=5,dir=4,yes=1)

! Arguments

      integer i

! Local Variables

      integer j,ncalced,missing
      integer lv(4),w(4),pid(4),pack
      character*72 buff

      integer           jwww(2),dir_jwww(2)
      character*59 buff_jwww
      integer           wwww
      character*59 buff_wwww

! Global Variables

      integer iv3(maxcount,6),iv4(maxcount,6),iline(maxcount,5),nlines
      Common/tohelas/iv3,iv4,iline,nlines
      integer number
      logical alldone
      common/total/alldone,number


!  Lookup table scheme for reordering lines for Helas Calls.
!
!  Particle codes:  1 = A (photon)
!                   2 = Z
!                   3 = W-
!                  -3 = W+ :: Changed to 4 for table lookup purposes only
!
!
!  Tables for calls to WWWWXX (See p. 86, Helas Manual)
!
!  Lookup WM1 WP1 WM2 WP2   Correct    Input    Perm
!  Index                     Order     Order  
!
!     1   W-  W+  W-  W+    3 4 3 4    3 3 4 4  1324
!      
      data wwww/      3344/
      data buff_wwww
     & /'WWWWXX(W   ,W   ,W   ,W   ,GWWA,GWWZ,ZMASS,ZWIDTH,AMP(   ))'/
!
!
!  Tables for calls to JWWWXX (See p. 89, Helas Manual)
!
!  Lookup W1 W2 W3      Correct    Input   Perm  Missing W
!  Index                 Order     Order            dir
!
!     1    W- W+ W-      3 4 3     3 3 4    132     -1
!     2    W+ W- W+      4 3 4     3 4 4    213      1
!
      data jwww/     334,344/
      data dir_jwww/  -1,  1/
      data buff_jwww
     & /'JWWWXX(W   ,W   ,W   ,GWWA,GWWZ,ZMASS,ZWIDTH,WMASS,WWIDTH,W'/

!-----------
! Begin Code
!-----------

      ncalced = 0
      do j=1,4
         if (iline(iv4(i,j),calc) .eq. yes) then
            ncalced = ncalced+1
            lv(ncalced) = iv4(i,j)
            if (abs(iline(iv4(i,j),flav)) .eq. 3) then
               if ( iline(iv4(i,j),dir) .eq. 1) then
                  pid(ncalced) = 4
               else
                  pid(ncalced) = 3
               end if
            else
               pid(ncalced)= iline(iv4(i,j),flav)
            end if               
         else
            missing = iv4(i,j)
         end if
      end do
      call sort2(pid,lv,ncalced)
      pack = 0
      do j=1,ncalced
         pack = pack*10 + pid(j)
      end do
      
      if (ncalced .eq. 3) then                     ! Prepare output string for JWWWXX call
         if (pack .eq. jwww(1)) then
            j = 1
            w(1) = lv(1)
            w(2) = lv(3)
            w(3) = lv(2)
         else if (pack .eq. jwww(2)) then
            j = 2
            w(1) = lv(2)
            w(2) = lv(1)
            w(3) = lv(3)
         else
            print*,'Illegal vertex combination in Writing JWWW'
         end if
         buff =  buff_jwww
         call makestring(w(1),buff( 9:11))
         call makestring(w(2),buff(14:16))
         call makestring(w(3),buff(19:21))
         call optcheck(buff,missing,59)
         iline(missing,calc) = yes
         iline(missing,dir ) = dir_jwww(j)
         iv4(i,calc) = yes
      else if (ncalced .eq. 4) then                ! Prepare output string for W3W3XX call
         if (pack .eq. wwww) then
            w(1) = lv(1)
            w(2) = lv(3)
            w(3) = lv(2)
            w(4) = lv(4)
         else
            print*,'Illegal vertex combination in Writing WWWW'
         end if
         buff =  buff_wwww
         call makestring(w(1),buff( 9:11))
         call makestring(w(2),buff(14:16))
         call makestring(w(3),buff(19:21))
         call makestring(w(4),buff(24:26))
         call makestringamp(number,buff(55:57))
         call writebuff(buff(1:59))
         iv4(i,calc) = yes
      else
         print*,' Illegal value for ncalced in WriteWWWW ',ncalced
      end if
      end

      
      Subroutine writeggg(i)
!************************************************************************
!     Routine to write 3 gluon vertex
!************************************************************************
      implicit none

! Constants

      integer   maxcount,   from,  type,  flav,  calc,  dir,  yes
      parameter(maxcount=40,from=1,type=2,flav=3,calc=5,dir=4,yes=1)

! Arguments

      integer i

! Local Variables

      integer j,ncalced,missing,lv(3)
      character*72 buff
      character*20 buff_jgg
      character*33 buff_ggg
      
! Global Variables

      integer iv3(maxcount,6),iv4(maxcount,6),iline(maxcount,5),nlines
      Common/tohelas/iv3,iv4,iline,nlines
      logical alldone
      integer number
      common/total/alldone,number

!  Tables for calls to JGGXXX (See p. 42, Helas manual. JGGXXX is a customized JVVXXX.)

      data buff_jgg/'JGGXXX(W   ,W   ,G,W'/

!  Tables for calls to GGGXXX (See p. 42, Helas manual. GGGXXX is a customized VVVXXX.)

      data buff_ggg/'GGGXXX(W   ,W   ,W   ,G,AMP(   ))'/

!-----------
! Begin Code
!-----------
      ncalced=0
      do j=1,3
         if (iline(iv3(i,j),calc) .eq. yes) then
            ncalced=ncalced+1
            lv(ncalced) = iv3(i,j)
         else
            missing=iv3(i,j)
         endif
      enddo
      if (ncalced .eq. 2) then
         buff = buff_jgg
         call makestring(lv(1),buff( 9:11))
         call makestring(lv(2),buff(14:16))
         call optcheck(buff,missing,20)
         iline(missing,calc) = yes
         iv3(i,calc) = yes
      elseif (ncalced .eq. 3) then
         buff = buff_ggg
         call makestring(lv(1),buff( 9:11))
         call makestring(lv(2),buff(14:16))
         call makestring(lv(3),buff(19:21))
         call makestringamp(number,buff(29:31))
         call writebuff(buff(:33))
         iv3(i,calc) = yes
      endif
      end


      Subroutine writeSSS(i)
!************************************************************************
!     Routine to write 3 Higgs vertex
!     Used for 3 Higgs Vertex 
!************************************************************************
      implicit none

! Constants

      integer   maxcount,   from,  type,  flav,  calc,  dir,  yes
      parameter(maxcount=40,from=1,type=2,flav=3,calc=5,dir=4,yes=1)

! Arguments

      integer i

! Local Variables

      integer j,ncalced,missing,lv(3)
      character*40 buff
      character*36 buff_hss
      character*36 buff_sss
      
! Global Variables

      integer iv3(maxcount,6),iv4(maxcount,6),iline(maxcount,5),nlines
      Common/tohelas/iv3,iv4,iline,nlines
      logical alldone
      integer number
      common/total/alldone,number

!  Tables for calls to HSSXXX (See p. 83, Helas manual)

      data buff_hss/'HSSXXX(W   ,W   ,GHHH,HMASS,HWIDTH,W'/

!  Tables for calls to SSSXXX (See p. 82, Helas manual)

      data buff_sss/'SSSXXX(W   ,W   ,W   ,GHHH,AMP(   ))'/
!-----------
! Begin Code
!-----------
      ncalced = 0
      do j = 1,3
         if (iline(iv3(i,j),calc) .eq. yes) then
            ncalced = ncalced+1
            lv(ncalced) = iv3(i,j)
         else
            missing = iv3(i,j)
         endif
      enddo
      if (ncalced .eq. 2) then
         buff = buff_hss
         call makestring(lv(1),buff( 9:11))
         call makestring(lv(2),buff(14:16))
         call optcheck(buff,missing,36)
         iline(missing,calc) = yes
         iv3(i,calc) = yes
      elseif (ncalced .eq. 3) then
         buff = buff_sss
         call makestring(lv(1),buff( 9:11))
         call makestring(lv(2),buff(14:16))
         call makestring(lv(3),buff(19:21))
         call makestringamp(number,buff(32:34))
         call writebuff(buff(:36))
         iv3(i,calc) = yes
      endif
      end

      subroutine writeVVV(i)
!************************************************************************
!     writeVVV writes calls to JVVXXX and VVVXXX
!    
!************************************************************************

      implicit none

! Constants

      integer   maxcount,   from,  type,  flav,  calc,  dir,  yes
      parameter(maxcount=40,from=1,type=2,flav=3,calc=5,dir=4,yes=1)

! Arguments

      integer i

! Local Variables

      integer j,k,bptr,ncalced,missing
      integer lv(4),w(4),pid(4),pack
      character*40 buff

      integer           jvv(5),tptr_jvv(5),pptr_jvv(5),perm_jvv(2,2)
      integer                    dir_jvv(5)
      character*19 tail_jvv(4)
      character*17 head_jvv
      integer           vvv(2),perm_vvv(3)
      character*36 buff_vvv
      character*3 coup_vvv(2)

! Global Variables

      integer iv3(maxcount,6),iv4(maxcount,6),iline(maxcount,5),nlines
      Common/tohelas/iv3,iv4,iline,nlines
      integer number
      logical alldone
      common/total/alldone,number


!  Lookup table scheme for reordering lines for Helas Calls.
!
!  Particle codes:  1 = A (photon)
!                   2 = Z
!                   3 = W-
!                  -3 = W+ :: Changed to 4 for table lookup purposes only
!
!
!  Tables for calls to VVVXXX (See p. 70, Helas Manual)
!
!  Lookup WM  WP  W3   Correct    Input Perm  Perm 
!  Index                Order     Order       Index
!
!     1    W- W+  A     3 4 1    1 3 4   231    1 
!     2    W- W+  Z     3 4 2    2 3 4   231    1 
!      
      data vvv/       134, 234/
      data perm_vvv/  2,3,1/
      data buff_vvv/'VVVXXX(W   ,W   ,W   ,G   ,AMP(   ))'/
      data coup_vvv/'WWA','WWZ'/
!
!
!  Tables for calls to JVVXXX (See p. 73, Helas Manual)
!
!  Lookup  V1 V2 : JVV  Correct  Input Perm  Perm   Buff_tail Missing W
!  Index                 Order   Order       Index  Index       dir
!
!     1    W- W+ : A,Z   3 4     3 4    12     1    -1         1
!     2    A  W- : W+    1 3     1 3    12     1     3        -1
!     3    Z  W- : W+    2 3     2 3    12     1     4        -1
!     4    W+ A  : W-    4 1     1 4    21     2     3         1
!     5    W+ Z  : W-    4 2     2 4    21     2     4         1
!
      data jvv/      34,13,23,14,24/
      data pptr_jvv/  1, 1, 1, 2, 2/
      data tptr_jvv/ -1, 3, 4, 3, 4/
      data  dir_jvv/  1,-1,-1, 1, 1/
      data perm_jvv/ 1,2,
     &               2,1/
      data head_jvv/'JVVXXX(W   ,W   ,'/
      data tail_jvv/'GWWA,AMASS,AWIDTH,W',
     &              'GWWZ,ZMASS,ZWIDTH,W',
     &              'GWWA,WMASS,WWIDTH,W',
     &              'GWWZ,WMASS,WWIDTH,W'/

!-----------
! Begin Code
!-----------

      ncalced = 0
      do j=1,3
         if (iline(iv3(i,j),calc) .eq. yes) then
            ncalced = ncalced+1
            lv(ncalced) = iv3(i,j)
            if (abs(iline(iv3(i,j),flav)) .eq. 3) then
               if ( iline(iv3(i,j),dir) .eq. 1) then
                  pid(ncalced) = 4
               else
                  pid(ncalced) = 3
               end if
            else
               pid(ncalced)= iline(iv3(i,j),flav)
            end if               
         else
            missing = iv3(i,j)
         end if
      end do
      call sort2(pid,lv,ncalced)
      pack = 0
      do j=1,ncalced
         pack = pack*10 + pid(j)
      end do
      
      if (ncalced .eq. 2) then         ! Prepare output string for JVVXXX call
         j = 1
         do while(j .le. 5 .and. pack .ne. jvv(j))
            j = j + 1
         end do
         if (j .eq. 6) then
            print*,'Illegal vertex combination in Writing JVV',pack
         else
            do k=1,2
               w(k) = lv(perm_jvv(k,pptr_jvv(j)))
            end do
            if (tptr_jvv(j) .lt. 0) then
               if (iline(missing,flav) .eq. 1) then      ! A
                  bptr = -tptr_jvv(j)
               else
                  bptr = -tptr_jvv(j)+1                  ! Z
               end if
            else
               bptr = tptr_jvv(j)                        ! W+-
            end if
            buff(1:17) =  head_jvv
            call makestring(w(1),buff( 9:11))
            call makestring(w(2),buff(14:16))
            buff(18:36) = tail_jvv(bptr)
            call optcheck(buff,missing,36)
            iline(missing,calc) = yes
            iline(missing,dir ) = dir_jvv(j)
            iv3(i,calc) = yes
         end if
      else if (ncalced .eq. 3) then                ! Prepare output string for VVVXXX call
         j = 1
         do while(j .le. 2 .and. pack .ne. vvv(j))
            j = j + 1
         end do
         if (j .eq. 3) then
            print*,'Illegal vertex combination in Writing VVV',pack
            stop
         else
            do k=1,3
               w(k) = lv(perm_vvv(k))
            end do
            buff = buff_vvv
            buff(24:26)=coup_vvv(j)
            call makestring(w(1),buff( 9:11))
            call makestring(w(2),buff(14:16))
            call makestring(w(3),buff(19:21))
            call makestringamp(number,buff(32:34))
            call writebuff(buff(:36))
            iv3(i,calc) = yes
         end if
      end if
      end

                  
      Subroutine writeGGGG(i)
!************************************************************************
!     Routine to write 4 gluon vertex
!     
!************************************************************************
      implicit none

! Constants

      integer   maxcount,   from,  type,  flav,  calc,  dir,  yes
      parameter(maxcount=40,from=1,type=2,flav=3,calc=5,dir=4,yes=1)
      integer   gluon
      parameter(gluon=2)

! Arguments

      integer i

! Local Variables

      integer ncalced,missing,j,lv(4),w(4)
      integer perm(3,3),pptr
      character*42 buff
      character*38 buff_gggg
      character*25 buff_jggg

! Global Variables

      integer iv3(maxcount,6),iv4(maxcount,6),iline(maxcount,5),nlines
      Common/tohelas/iv3,iv4,iline,nlines
      logical alldone
      integer number
      common/total/alldone,number

!  Table for calls to GGGGXX (See p. 42, Helas Manual. GGGGXX is a customized W3W3XX.)

      data buff_gggg/'GGGGXX(W   ,W   ,W   ,W   ,G,AMP(   ))'/

!  Table for calls to JGGGXX (See p. 42, Helas Manual. JGGGXX is a customized JW3WvXX.)

      data buff_jggg/'JGGGXX(W   ,W   ,W   ,G,W'/
      data perm/1,2,3,
     &          3,1,2,
     &          2,3,1/

!-----------
! Begin Code
!-----------

      ncalced=0
      pptr = 0
      do j=1,4
         if (iline(iv4(i,j),calc) .eq. yes ) then
            ncalced=ncalced+1
            lv(ncalced) = iv4(i,j)
            if (iline(iv4(i,j),type) .eq. -gluon) pptr = ncalced
         else
            missing=iv4(i,j)
         endif
      enddo
      if (pptr .le. 0 .or. pptr .gt. 3) then
         print*,'Error not one of first three gluons is negative'
      else
         do j=1,3
            w(j) = lv(perm(j,pptr))
         end do
      end if
      if (ncalced .eq. 3) then 
         buff = buff_jggg
         call makestring(w(1), buff( 9:11))
         call makestring(w(2), buff(14:16))
         call makestring(w(3), buff(19:21))
         call optcheck(buff,missing,25)
         iline(missing,calc) = yes
         iv4(i,calc) = yes
      elseif (ncalced .eq. 4) then
         buff = buff_gggg
         call makestring(w(1), buff( 9:11))
         call makestring(w(2), buff(14:16))
         call makestring(w(3), buff(19:21))
         call makestring(lv(4),buff(24:26))
         call makestringamp(number,buff(34:36))
         call writebuff(buff(:38))
         iv4(i,calc) = yes
      endif
      end

      
      Subroutine writeFFV(i)
!************************************************************************
!     Routine to write fermion fermion V vertex
!     NOTE:  For photon quark vertex need factor of quark fractional charge!
!     
!************************************************************************
      implicit none

! Constants

      integer   maxcount,   from,  type,  flav,  calc,  dir,  yes
      parameter(maxcount=40,from=1,type=2,flav=3,calc=5,dir=4,yes=1)

! Arguments

      integer i

! Local Variables

      integer j,ncalced
      integer missing,lif,lof,other
      character*49 buff
      character*35 jio,iov
      character*45 fvo,fvi
      character*3 str
      character*1 s,boson_labels(3),sf,fermion_labels(0:3)

! Global Variables

      integer iv3(maxcount,6),iv4(maxcount,6),iline(maxcount,5),nlines
      Common/tohelas/iv3,iv4,iline,nlines
      logical alldone
      integer number
      common/total/alldone,number

!  Data Tables

      data   boson_labels/'A','Z','W'/
      data fermion_labels/'L','N','U','D'/
      data jio/'JIOXXX(W   ,W   ,G  , MASS, WIDTH,W'/
      data fvo/'FVOXXX(W   ,W   ,G  ,FMASS(   ),FWIDTH(   ),W'/
      data fvi/'FVIXXX(W   ,W   ,G  ,FMASS(   ),FWIDTH(   ),W'/
      data iov/'IOVXXX(W   ,W   ,W   ,G  ,AMP(   ))'/

!-----------
! Begin Code
!-----------
      ncalced=0
      lif=0
      lof=0
      other=0
      do j=1,3
         if (iline(iv3(i,j),type) .eq. 3) then    !Fermion
            if (iline(iv3(i,j),calc) .eq. yes) then        !only if calculated
               if (iline(iv3(i,j),dir) .eq. 1) lif = j
               if (iline(iv3(i,j),dir) .eq.-1) lof = j
            endif
         else
            other = j
         endif
         if (iline(iv3(i,j),calc) .eq. yes) then
            ncalced=ncalced+1
         else
            missing=j
         endif
      enddo
      if (ncalced .ge. 2
     &.and. (lif .eq. other .or. lof .eq. other .or. lif .eq. lof)) then
         print*,'Warning lif=lof',iv3(i,1),iv3(i,2),iv3(i,3)
         print*,'lif,lof,other',lif,lof,other
         print*,'types ',iline(iv3(i,1),type),iline(iv3(i,2),type),
     &        iline(iv3(i,3),type)
      endif

      if (lof .eq. 0) lof=missing
      if (lif .eq. 0) lif=missing
      if (iline(iv3(i,other),type) .eq. 1)  then !W,Z,Photon
         s = boson_labels(abs(iline(iv3(i,other),flav)))
      else
         Print*,'Error FFV subroutine wrong element'
      endif
      if (s .eq. 'W') then
         sf = 'F'
      else
         sf = fermion_labels(mod(abs(iline(iv3(i,lif),flav))-1,4))
      end if
      if (ncalced .eq. 2) then       !We have two of the three WF
         if (missing .eq. other) then    !Photon,Glue,W,Z is missing
            buff = jio
            call makestring(iv3(i,lif),buff( 9:11))
            call makestring(iv3(i,lof),buff(14:16))
            buff(19:19) = s
            buff(20:20) = sf
            buff(22:22) = s
            buff(28:28) = s
            call optcheck(buff,iv3(i,other),35)
            iline(iv3(i,missing),calc) = yes

         elseif (missing .eq. lif) then    !Incoming Fermion is missing
            buff = fvo
            call makestring(iv3(i,lof),  buff( 9:11))
            call makestring(iv3(i,other),buff(14:16))               
            call makestringamp(abs(iline(iv3(i,lif),flav)),str)
            buff(19:19) = s
            buff(20:20) = sf
            buff(28:30) = str
            buff(40:42) = str
            call optcheck(buff,iv3(i,lif),45)
            iline(iv3(i,missing),dir) = -1
            iline(iv3(i,missing),calc) = yes

         elseif (missing .eq. lof) then   !Outgoing Fermion Missing
            buff = fvi
            call makestring(iv3(i,lif),  buff( 9:11))
            call makestring(iv3(i,other),buff(14:16))
            call makestringamp(abs(iline(iv3(i,lof),flav)),str)
            buff(19:19) = s
            buff(20:20) = sf
            buff(28:30) = str
            buff(40:42) = str
            call optcheck(buff,iv3(i,lof),45)
            iline(iv3(i,missing),dir) = 1
            iline(iv3(i,missing),calc) = yes
         endif
         iv3(i,calc) = yes

      elseif(ncalced .eq. 3) then
         buff = iov
         call makestring(iv3(i,lif),  buff( 9:11))
         call makestring(iv3(i,lof),  buff(14:16))
         call makestring(iv3(i,other),buff(19:21))
         call makestringamp(number,buff(31:33))
         buff(24:24) = s
         buff(25:25) = sf
         call writebuff(buff(:35))
         iv3(i,calc) = yes
      endif
      end

      Subroutine writeIOS(i)
!************************************************************************
!     Routine to write fermion fermion Scalar vertex
!     
!************************************************************************
      implicit none

! Constants

      integer   maxcount,   from,  type,  flav,  calc,  dir,  yes
      parameter(maxcount=40,from=1,type=2,flav=3,calc=5,dir=4,yes=1)

! Arguments

      integer i

! Local Variables

      integer j,ncalced
      integer missing,lif,lof,other
      character*55 buff
      character*41 hio,ios
      character*51 fso,fsi
      character*3 str

! Global Variables

      integer iv3(maxcount,6),iv4(maxcount,6),iline(maxcount,5),nlines
      Common/tohelas/iv3,iv4,iline,nlines
      logical alldone
      integer number
      common/total/alldone,number

!  Data Tables

      data hio/'HIOXXX(W   ,W   ,GH(1,   ),HMASS,HWIDTH,W'/
      data fso/'FSOXXX(W   ,W   ,GH(1,   ),FMASS(   ),FWIDTH(   ),W'/
      data fsi/'FSIXXX(W   ,W   ,GH(1,   ),FMASS(   ),FWIDTH(   ),W'/
      data ios/'IOSXXX(W   ,W   ,W   ,GH(1,   ),AMP(   ))'/

!-----------
! Begin Code
!-----------
      ncalced=0
      lif=0
      lof=0
      other=0
      do j=1,3
         if (iline(iv3(i,j),type) .eq. 3) then    !Fermion
            if (iline(iv3(i,j),calc) .eq. yes) then        !only if calculated
               if (iline(iv3(i,j),dir) .eq. 1) lif = j
               if (iline(iv3(i,j),dir) .eq.-1) lof = j
            endif
         else
            other = j
         endif
         if (iline(iv3(i,j),calc) .eq. yes) then
            ncalced=ncalced+1
         else
            missing=j
         endif
      enddo
      if (ncalced .ge. 2
     &.and. (lif .eq. other .or. lof .eq. other .or. lif .eq. lof)) then
         print*,'Warning lif=lof',iv3(i,1),iv3(i,2),iv3(i,3)
         print*,'lif,lof,other',lif,lof,other
         print*,'types ',iline(iv3(i,1),type),iline(iv3(i,2),type),
     &        iline(iv3(i,3),type)
      endif

      if (lof .eq. 0) lof=missing
      if (lif .eq. 0) lif=missing
      if (iline(iv3(i,other),type) .eq. 4)  then !Higgs
         call makestringamp(abs(iline(iv3(i,lof),flav)),str)
      else
         Print*,'Error FFS subroutine wrong element'
      endif
      if (ncalced .eq. 2) then       !We have two of the three WF
         if (missing .eq. other) then    !Higgs is missing
            buff = hio
            call makestring(iv3(i,lif),buff( 9:11))
            call makestring(iv3(i,lof),buff(14:16))
            buff(23:25) = str
            call optcheck(buff,iv3(i,other),41)
            iline(iv3(i,missing),calc) = yes

         elseif (missing .eq. lif) then    !Incoming Fermion is missing
            buff = fso
            call makestring(iv3(i,lof),  buff( 9:11))
            call makestring(iv3(i,other),buff(14:16))               
            buff(23:25) = str
            buff(34:36) = str
            buff(46:48) = str
            call optcheck(buff,iv3(i,lif),51)
            iline(iv3(i,missing),dir) = -1
            iline(iv3(i,missing),calc) = yes

         elseif (missing .eq. lof) then   !Outgoing Fermion Missing
            buff = fsi
            call makestring(iv3(i,lif),  buff( 9:11))
            call makestring(iv3(i,other),buff(14:16))
            buff(23:25) = str
            buff(34:36) = str
            buff(46:48) = str
            call optcheck(buff,iv3(i,lof),51)
            iline(iv3(i,missing),dir) = 1
            iline(iv3(i,missing),calc) = yes
         endif
         iv3(i,calc) = yes

      elseif(ncalced .eq. 3) then
         buff = ios
         call makestring(iv3(i,lif),  buff( 9:11))
         call makestring(iv3(i,lof),  buff(14:16))
         call makestring(iv3(i,other),buff(19:21))
         call makestringamp(number,buff(37:39))
         buff(28:30) = str
         call writebuff(buff(:41))
         iv3(i,calc) = yes
      endif
      end


      Subroutine writeVVS(i)
!************************************************************************
!     Routine to write fermion fermion Scalar vertex
!     
!************************************************************************
      implicit none

! Constants

      integer   maxcount,   from,  type,  flav,  calc,  dir,  yes
      parameter(maxcount=40,from=1,type=2,flav=3,calc=5,dir=4,yes=1)

! Arguments

      integer i

! Local Variables

      integer j,ncalced
      integer missing,lif,lof,other
      character*40 buff
      character*36 hvv,jvs,vvs
      character*2 sv,boson_labels(3)

! Global Variables

      integer iv3(maxcount,6),iv4(maxcount,6),iline(maxcount,5),nlines
      Common/tohelas/iv3,iv4,iline,nlines
      logical alldone
      integer number
      common/total/alldone,number

! Data Tables

      data boson_labels/'AA','ZZ','WW'/
      data hvv/'HVVXXX(W   ,W   ,G  H,HMASS,HWIDTH,W'/
      data jvs/'JVSXXX(W   ,W   ,G  H, MASS, WIDTH,W'/
      data vvs/'VVSXXX(W   ,W   ,W   ,G  H,AMP(   ))'/

!-----------
! Begin Code
!-----------
      ncalced=0
      lif=0
      lof=0
      other=0
      do j=1,3
         if (iline(iv3(i,j),type) .eq. 1) then    !Photon W or Z
            if (iline(iv3(i,j),calc) .eq. yes) then        !only if calculated
               if (lif .eq. 0) then
                  lif = j
               else
                  lof = j
               endif
            endif
         else
            other = j
         endif
         if (iline(iv3(i,j),calc) .eq. yes) then
            ncalced=ncalced+1
         else
            missing=j
         endif
      enddo
      if (ncalced .ge. 2
     &.and.(lif .eq. other .or. lof .eq. other .or. lif .eq. lof)) then
            print*,'Warning lif=lof',iv3(i,1),iv3(i,2),iv3(i,3)
            print*,'lif,lof,other',lif,lof,other
            print*,'types ',iline(iv3(i,1),type),iline(iv3(i,2),type),
     &           iline(iv3(i,3),type)
      endif
      if (lof .eq. 0) lof=missing
      if (lif .eq. 0) lif=missing
      if (iline(iv3(i,other),type) .ne. 4)  then !not Higgs
         Print*,'Error FFS subroutine wrong element'
      endif
      if (iline(iv3(i,lif),type) .eq. 1)  then !W,Z,Photon
         sv = boson_labels(abs(iline(iv3(i,lif),flav)))
      else
         Print*,'Error VVS subroutine wrong element line type is',
     &        iline(iv3(i,other),type)
      endif
      if (ncalced .eq. 2) then       !We have two of the three WF
         if (missing .eq. other) then    !Higgs is missing
            buff = hvv
            call makestring(iv3(i,lif),buff( 9:11))
            call makestring(iv3(i,lof),buff(14:16))
            buff(19:20) = sv
            call optcheck(buff,iv3(i,other),36)
            iline(iv3(i,missing),calc) = yes

         elseif (missing .eq. lif) then
            buff = jvs
            call makestring(iv3(i,lof),  buff( 9:11))
            call makestring(iv3(i,other),buff(14:16))
            buff(19:20) = sv
            buff(23:23) = sv(1:1)
            buff(29:29) = sv(1:1)
            call optcheck(buff,iv3(i,lif),36)
            iline(iv3(i,missing),dir) = iline(iv3(i,lof),dir)
            iline(iv3(i,missing),calc) = yes

         elseif (missing .eq. lof) then
            buff = jvs
            call makestring(iv3(i,lif),  buff( 9:11))
            call makestring(iv3(i,other),buff(14:16))
            buff(19:20) = sv
            buff(23:23) = sv(1:1)
            buff(29:29) = sv(1:1)
            call optcheck(buff,iv3(i,lof),36)
            iline(iv3(i,missing),dir) = iline(iv3(i,lif),dir)
            iline(iv3(i,missing),calc) = yes
         endif
         iv3(i,calc) = yes

      elseif(ncalced .eq. 3) then
         buff = vvs
         call makestring(iv3(i,lif),  buff( 9:11))
         call makestring(iv3(i,lof),  buff(14:16))
         call makestring(iv3(i,other),buff(19:21))
         buff(24:25) = sv
         call makestringamp(number,buff(32:34))
         call writebuff(buff(:36))
         iv3(i,calc) = yes
      endif
      end


      Subroutine writeFFG(i)
!************************************************************************
!     Routine to write fermion fermion GLUON vertex
!     
!************************************************************************
      implicit none

! Constants

      integer   maxcount,   from,  type,  flav,  calc,  dir,  yes
      parameter(maxcount=40,from=1,type=2,flav=3,calc=5,dir=4,yes=1)

! Arguments

      integer i

! Local Variables

      integer j,ncalced
      integer missing,lif,lof,other
      character*48 buff
      character*31 jio
      character*34 iov
      character*44 fvo,fvi
      character*3 str

! Global Variables

      integer iv3(maxcount,6),iv4(maxcount,6),iline(maxcount,5),nlines
      Common/tohelas/iv3,iv4,iline,nlines
      logical alldone
      integer number
      common/total/alldone,number

! Data Tables

      data jio/'JIOXXX(W   ,W   ,GG,ZERO,ZERO,W'/
      data fvo/'FVOXXX(W   ,W   ,GG,FMASS(   ),FWIDTH(   ),W'/
      data fvi/'FVIXXX(W   ,W   ,GG,FMASS(   ),FWIDTH(   ),W'/
      data iov/'IOVXXX(W   ,W   ,W   ,GG,AMP(   ))'/

!-----------
! Begin Code
!-----------     
      ncalced=0
      lif=0
      lof=0
      other=0
      do j=1,3
         if (iline(iv3(i,j),type) .eq. 3) then    !Fermion
            if (iline(iv3(i,j),calc) .eq. yes) then        !only if calculated
               if (iline(iv3(i,j),dir) .eq. 1) lif = j
               if (iline(iv3(i,j),dir) .eq.-1) lof = j
            endif
         else
            other = j
         endif
         if (iline(iv3(i,j),calc) .eq. yes) then
            ncalced=ncalced+1
         else
            missing=j
         endif
      enddo
      if (ncalced .ge. 2
     &.and.(lif .eq. other .or. lof .eq. other .or. lif .eq. lof)) then
            print*,'Warning lif=lof',iv3(i,1),iv3(i,2),iv3(i,3)
            print*,'lif,lof,other',lif,lof,other
            print*,'types ',iline(iv3(i,1),type),iline(iv3(i,2),type),
     &           iline(iv3(i,3),type)
      endif
      if (lof .eq. 0) lof=missing
      if (lif .eq. 0) lif=missing

      if (ncalced .eq. 2) then       !We have two of the three WF
         if (missing .eq. other) then    !Photon,Glue,W,Z is missing
            buff = jio
            call makestring(iv3(i,lif),buff( 9:11))
            call makestring(iv3(i,lof),buff(14:16))
            call optcheck(buff,iv3(i,other),31)
            iline(iv3(i,missing),calc) = yes

         elseif (missing .eq. lif) then    !Incomming Fermion is missing
            buff = fvo
            call makestring(iv3(i,lof),  buff( 9:11))
            call makestring(iv3(i,other),buff(14:16))               
            call makestringamp(abs(iline(iv3(i,lof),flav)),str)
            buff(27:29) = str
            buff(39:41) = str
            call optcheck(buff,iv3(i,lif),44)
            iline(iv3(i,missing),dir) = -1
            iline(iv3(i,missing),calc) = yes

         elseif (missing .eq. lof) then   !Outgoing Fermion Missing
            buff = fvi
            call makestring(iv3(i,lif),  buff( 9:11))
            call makestring(iv3(i,other),buff(14:16))
            call makestringamp(abs(iline(iv3(i,lof),flav)),str)
            buff(27:29) = str
            buff(39:41) = str
            call optcheck(buff,iv3(i,lof),44)
            iline(iv3(i,missing),dir) = 1
            iline(iv3(i,missing),calc) = yes
         endif
         iv3(i,calc) = yes

      elseif(ncalced .eq. 3) then
         buff = iov
         call makestring(iv3(i,lif),  buff( 9:11))
         call makestring(iv3(i,lof),  buff(14:16))
         call makestring(iv3(i,other),buff(19:21))
         call makestringamp(number,buff(30:32))
         call writebuff(buff(:34))
         iv3(i,calc) = yes
      endif
      end

      
      Subroutine makestring(j,str)
!************************************************************************
!     Returns in string form the number i=wnum(j)
!
!************************************************************************
      implicit none

! Arguments

      integer j
      character*3 str

! Global Variables

      integer wnum(999),wcount
      common/toopt/wcount,wnum

!-----------
! Begin Code
!-----------     
      if (j .gt. 999 .or. j .le. 0) then
         print*,'Error j outside [1:999] in makestring',j
      else
         call makestringamp(wnum(j),str)
      endif
      end
      
!------------------------------------------
      subroutine makestringamp(i,str)
      implicit none

!  Constants

      integer z
      parameter (z = ichar('0'))

!  Arguments

      integer i
      character*3 str
!-----------
! Begin Code
!-----------
      if (i .gt. 999) then
         print*,' i too big'
      else if (i .gt. 99) then
         str=char(i/100+z)//char(mod(i/10,10)+z)//char(mod(i,10)+z)
      else if (i .gt. 9) then
         str=char(i/10+z)//char(mod(i,10)+z)
      else if (i .gt. 0) then
         str=char(i+z)
      else
         print*,' i too small'
         str='000'
      end if
      end


      subroutine optcheck(buff,line,num)
!****************************************************************************
!     This routine looks to see if the wave function has been calculated
!     if so returns found=.true. and sets the wavenumber for the graph
!     Else returns false and a new wave number is assigned and the graph
!     is written
!****************************************************************************
      implicit none

! Constants

      integer   maxgraphs,    maxlines
      parameter(maxgraphs=999,maxlines=40)

! Arguments

      character*(*) buff
      integer line,num

! Local Variables

      character*72 wstrings(maxgraphs*maxlines)
      integer i
      logical found

! Global Variables

      integer wnum(999),wcount
      common/toopt/wcount,wnum

      save wstrings

!-----------
! Begin Code
!-----------     
      found=.false.
      do i=1,wcount                        !number of wavefunctions calculated
         if (buff(1:num) .eq. wstrings(i)(1:num)) then
            found=.true.
            wnum(line) = i
         endif
      enddo
      if (.not. found) then
         wcount=wcount+1
         wnum(line) = wcount
         call makestring(line,buff(num+1:num+3))
         buff(num+4:num+4) = ')'
         call writebuff(buff(:num+4))
         wstrings(wcount) = buff(:num+4)
      endif
      end


!--------------------------
      subroutine writebuff(buff)

      implicit none

! Constants

      integer   lun
      parameter(lun = 91)

! Arguments

      character*(*) buff

! Local Variables

      integer length,i

!-----------
! Begin Code
!-----------
      length = len(buff)
      if (length .le. 60) then
         write (lun,100) buff
      else
         i = length
         do while (buff(i:i) .ne. ',')
            i = i - 1
         end do
         write (lun,100) buff(:i)
         write (lun,200) buff(i+1:)
      end if
 100  format ('      CALL ',a)
 200  format ('     &     ',a)
      end


!-----------------------------------

      subroutine sort2(array,aux1,n)
      implicit none

! Arguments

      integer n
      integer array(n),aux1(n)

!  Local Variables

      integer i,k,temp
      logical done

!-----------
! Begin Code
!-----------
      do i=n-1,1,-1
         done = .true.
         do k=1,i
            if (array(k) .gt. array(k+1)) then
               temp = array(k)
               array(k) = array(k+1)
               array(k+1) = temp
               temp = aux1(k)
               aux1(k) = aux1(k+1)
               aux1(k+1) = temp
               done = .false.
            end if
         end do
         if (done) return
      end do
      end 

