*###########################################
*#          Service procedures             #
*#   contents:                             #
*#    CLRSCR                               #
*###########################################
*#  Conversion routines                    #
*#  DTOCHR, ITOCHR, LENINT, ONN, CH2CAT    #
*###########################################



********************************************
*     Clear screen                         *
********************************************
      SUBROUTINE CLRSCR
      IK = 24
12    CONTINUE
      WRITE(*,*)
      IF (IK .GT. 0) THEN
         IK = IK-1
         GO TO 12
      ENDIF
      RETURN
      END

*************************************************************
*          Conversion of DOUBLE PRECISION into CHARACTER              *
*************************************************************
*    Input:  DCHR  - output string,                         *
*            XXVAL - DOUBLE PRECISION number to convert,              *
*            LENSTR - number of converted valued digits.    *
*    Output: DTOCHR - character string of converted input   *
*                     DOUBLE PRECISION number with LENSTR valued      *
*                     digits in this string.                *
*    Format of the output:     (G LENSTR+7 . LENSTR)        *
*        so declare DCHR at least as CHARCATER*(7+LENSTR)   *
*************************************************************
      SUBROUTINE DTOCHR(DCHR,XXVAL,LENSTR)
      IMPLICIT DOUBLE PRECISION(A-H,O-Z),INTEGER(I-N)
      CHARACTER DCHR*(*)
      CHARACTER XFMT*10,XFMT1*10,XCC*2

      DCHR=' '
      WRITE(XCC,FMT='(I2)') LENSTR
      XFMT=XCC//')'
      WRITE(XCC,FMT='(I2)') LENSTR+7
      XFMT1='(G'//XCC//'.'//XFMT
      WRITE(DCHR,FMT=XFMT1,ERR=900) XXVAL
      GOTO 1000
900   WRITE(*,*) 'Error in DTOCHR.'
      READ(*,*)

1000  RETURN
      END


*******************************************************
*                    FUNCTION                         *
* Conversion of integer number into character string  *
*******************************************************
*   Input:  ICHR - output string,                     *
*           JJ - integer number under the conversion; *
*   Output: ITOCHR - the character string,            *
*             declare ICHR at least as                *
*                CHARACTER*LENINT(JJ)                 *
*******************************************************
      SUBROUTINE ITOCHR(ICHR,JJ)
      IMPLICIT DOUBLE PRECISION(A-H,O-Z),INTEGER(I-N)
      CHARACTER ICHR*(*)
      CHARACTER XFMT*10,XCC

      ICHR=' '
      WRITE(XCC,FMT='(I1)') LENINT(JJ)
      XFMT='(I'//XCC//')'
      WRITE(ICHR,FMT=XFMT,err=100) JJ

      goto 900
100   write(*,*) 'Error in ITOCHR.'
      read(*,*)

900   RETURN
      END

********************************************
*           FUNCTION                       *
*     Length of the integer number         *
********************************************
      INTEGER FUNCTION LENINT(JJ)
      IMPLICIT DOUBLE PRECISION(A-H,O-Z),INTEGER(I-N)
      KK=1
10    CONTINUE
      IF (JJ .LT. 10**KK) THEN
         LLL=KK
         GOTO 20
      ENDIF
      KK=KK+1
      GOTO 10

20    LENINT=LLL
      RETURN
      END


***********************************************************
*   convert logical var to the character ON or OFF        *
***********************************************************
      FUNCTION ONN(ARG)
      LOGICAL ARG
      CHARACTER*4 ONN
      
      IF (ARG) THEN
        ONN=' ON '
      ELSE
        ONN=' OFF'
      ENDIF
      RETURN
      END

*************************************************
*   Concatenation of two strings                *
*                Warning!                       *
*    sizeof(SCH) has to be equal or more then   *
*         sizeof(CH1)+sizeof(CH2)               *
*    for the concatenation without lossing of   *
*          part of CH2                          *
*************************************************
      SUBROUTINE CH2CAT(SCH,CH1,CH2)
      CHARACTER SCH*(*),CH1*(*),CH2*(*)

      L1=0
      DO 1 I=1,LEN(CH1)
        IF(CH1(I:I).EQ.' ')  GOTO 2 
1           L1=L1+1
2     CONTINUE

      L2=0
      DO 3 I=1,LEN(CH2)
        IF(CH2(I:I).EQ.' ')  GOTO 4
3           L2=L2+1
4        CONTINUE

      IF(L1.GE.LEN(SCH)) THEN
         SCH=CH1
         RETURN
      ENDIF

      IF( L1+L2.GT.LEN(SCH) ) L2=LEN(SCH)-L1
   
      DO 10 I=1,L1
10     SCH(I:I)=CH1(I:I)

      DO 11 I=1,L2
11     SCH(I+L1:I+L1)=CH2(I:I)

      DO 12 I=L2+L1+1,LEN(SCH)
12       SCH(I:I)=' '

       RETURN
      END

