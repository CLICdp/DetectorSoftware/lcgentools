*********************************************************
*   View results or protocol files
*********************************************************
      SUBROUTINE  VIEWRP(F_NAME)
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      CHARACTER F_NAME *(*)
      LOGICAL       XFILE,FEND
      CHARACTER     XFMT*20,XSTR*79,DOLL*2,S*1
      COMMON /DEVICE/  IPRT,ISES,IBAT,ISTR(2),IEVENT,ITMP
      COMMON /DOLL/    DOLL
      SAVE

      NLPAGE=23
      NPG=0
      XSTR=' '
      FEND=.FALSE.
      XFMT='(1X,A60'//DOLL//')'
      
      INQUIRE(FILE=F_NAME,EXIST=XFILE)
      IF (.NOT.XFILE) THEN        
        WRITE(*,*) 'File ', F_NAME, ' is absent'
        WRITE(*,*) 'Press ENTER to continue'
        READ(*,*)
        RETURN
      ENDIF

      OPEN(ITMP,FILE=F_NAME,STATUS='UNKNOWN')
      REWIND(ITMP)

6     CALL CLRSCR
      
      FEND=.FALSE.
      NPG=NPG+1
      DO 7 K=1,NLPAGE
        IF ( FEND ) THEN 
           WRITE(*,*)
        ElSE
           XSTR=' '
           READ(ITMP,FMT='(A)',END=66,ERR=66) XSTR
           WRITE(*,FMT='(1x,A)') XSTR
           GOTO 7
66         WRITE(*,*)
           FEND=.TRUE.
        ENDIF
7     CONTINUE

      IF (FEND) THEN
         WRITE(*,FMT=XFMT) '>>> End of file.   '//
     &    'x - quit; b - previous page :  '
      ELSE
        WRITE(*,FMT=XFMT) '>>>> x - quit; b - previous page; '//
     & 'Enter, Space - next page :  '
      ENDIF

77    READ(*,FMT='(A1)',ERR=77 ) S
        
      IF (S.EQ.'x'.OR.S.EQ.'X') THEN
          CLOSE(ITMP)
          RETURN
      ELSE IF (S.EQ.' ')  THEN
         IF (FEND)  GOTO 77
         GOTO 6
      ELSE IF (S.EQ.'b'.OR.S.EQ.'B') THEN
         REWIND(ITMP)
         NPG=NPG-2
         DO 88 K=1,NPG*NLPAGE
88       READ(ITMP,FMT='(A)') XSTR
         GOTO 6
      ENDIF          
           
      GOTO 77

1001  RETURN
      END
