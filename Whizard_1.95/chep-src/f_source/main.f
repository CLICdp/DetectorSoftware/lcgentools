*#########################################
*#  Main program and menu of 1st level   #
*#       file:      main.f               #
*#                                       #
*#   contents:                           #
*#         MAIN program (main menu)      #
*#########################################

*********************************************
*   M A I N   P R O G R A M                 *
*       The KinComp program                 *
*   (CompHEP-Vegas) interface        *
*********************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z), INTEGER (I-N)
      LOGICAL       XFILE
      CHARACTER     PROCES*30,VERS*20,DCH*10
      CHARACTER*500     STRMEN,XSTRM1,XSTRM2,XSTRM3

      COMMON /DEVICE/  IPRT,ISES,IBAT,ISTR(2),IEVENT,ITMP
      CHARACTER*12  PRTNAM,SESNAM,BATNAM
      COMMON /DEVNAM/  PRTNAM,SESNAM,BATNAM

      COMMON /NSESS/   NSESS
      COMMON /PROCES/  NSUB,PROCES
      COMMON /VERS/    VERS
      COMMON /MBACK/   MBACK
      CHARACTER*1 CH      
      COMMON /LOGG/ L(1)
      CHARACTER*78 XXSTR
      LOGICAL USRMEN
      SAVE
  
      EXTERNAL S_TCUT,INTCUT,CHTCUT,DLTCUT
      EXTERNAL  VIEWRG,NEWREG,CHNREG,DELREG
      EXTERNAL   VIEWCT ,NEWCUT,CHNCUT,DELCUT
***  initialization of the session
      MBACK=0
      CALL TSKINI

***  start batch calculations
      INQUIRE(FILE=BATNAM,EXIST=XFILE)
      IF (XFILE) THEN
         WRITE(*,*)' Would you like to use the BATCH mode?  Y/N'
         READ(*,FMT='(A1)') CH
         IF ( (CH.EQ.'y').OR.(CH.EQ.'Y')) THEN
            OPEN(IBAT,FILE=BATNAM,STATUS='UNKNOWN')
5           READ(IBAT,FMT='(A)',END=7,ERR=7) XXSTR
            NSESS_=NSESS
            CALL  R_SESS(IBAT)
            NSESS=NSESS_
            CALL RUNBAS(1)
            GOTO 5
7           CALL W_SESS(ISES)
            CLOSE(IBAT)
            STOP 'End of batch calculation.'
         ENDIF
      ENDIF


10    CALL CLRSCR
      CALL ITOCHR(DCH,NSESS)

      IF (USRMEN(0)) THEN
         XSTRM3='!QCD SCALE !View results!Batch!User menu !!!'
      ELSE
         XSTRM3='!QCD SCALE !View results!Batch!!!'
      ENDIF

      XSTRM2='Regularization!Widths'//XSTRM3   
      XSTRM1='Monte-Carlo simulation!Subprocess!IN state'//
     &'!Model parameters!Kinematics!Invariant cuts!E,Pt,A,J cuts!'//
     &XSTRM2

      STRMEN = '%M A I N  M E N U!'//
     &         'CompHEP numerical module    '//VERS//
     &'! VEGAS, MRS, CTEQ inside !'//
     &         PROCES//'                  Session # '//DCH//'!%'//
     & XSTRM1


      MODE=MENU(STRMEN,'f_')
      IF (MODE .EQ. 0) THEN
          IF (MBACK.EQ.1) THEN
             MBACK=0
             GOTO 10
          ELSE
             CALL W_SESS(ISES)
             STOP 'End of CompHEP MC integration session' 
          ENDIF
      ELSE IF (MODE .EQ. 1) THEN
           CALL RUNBAS(0)
      ELSE IF (MODE.EQ.2) THEN 
           CALL SUB_MEN
      ELSE IF (MODE .EQ. 3) THEN
           IF (NIN().EQ.2)   CALL IN_SET
      ELSE IF (MODE .EQ. 4) THEN
           CALL MDLSET
      ELSE IF (MODE .EQ. 5) THEN
           CALL ENTKIN
      ELSE IF (MODE .EQ. 6) THEN
            CALL NCDMEN( VIEWCT ,NEWCUT,CHNCUT,DELCUT,'f_6') 
      ELSE IF (MODE .EQ. 7) THEN
            CALL NCDMEN(S_TCUT,INTCUT,CHTCUT,DLTCUT,'f_7')
      ELSE IF (MODE .EQ. 8) THEN
            CALL NCDMEN(VIEWRG,NEWREG,CHNREG,DELREG,'f_8')
      ELSE IF (MODE .EQ. 9) THEN
            CALL W_MEN
      ELSE IF (MODE .EQ. 10) THEN
           CALL QCDMEN
      ELSE IF (MODE .EQ. 11) THEN
           CALL VIEWF
      ELSE IF (MODE .EQ. 12) THEN
         CALL B_ADD
      ELSE
         IF(USRMEN(1)) CONTINUE
      ENDIF
      GO TO 10

      END


      SUBROUTINE W_MEN
      LOGICAL GWIDTH,RWIDTH
      COMMON /WDTH/ GWIDTH,RWIDTH
      CHARACTER*100 STRMEN
      CHARACTER*4 ONN
      SAVE
1     CALL CLRSCR
      STRMEN = '% WIDTH MENU !%'//
     & 'Gauge invariance '// ONN(GWIDTH)//'!S-dependence '//
     & ONN(RWIDTH)//'!!!'

       KEY=MENU(STRMEN,'f_9')
       IF (KEY.EQ.0) RETURN    
       IF (KEY.EQ.1)    GWIDTH=.NOT.GWIDTH
       IF (KEY.EQ.2)    RWIDTH=.NOT.RWIDTH
       GOTO 1
      END
