      SUBROUTINE P_CTQ(P_NAME,RES)      
C* Check particle name   
      IMPLICIT REAL*8 (A-H,O-Z)
      CHARACTER*60 NAME
      CHARACTER*6 P_NAME,Q_NAME,PINF
      DIMENSION INHADR(2),NQUARK(2)
      LOGICAL RES
      LOGICAL FIRST
      CHARACTER*60 PATH 
      CHARACTER*1 D_SLAH,F_SLAH
      CHARACTER*100 FNAME,PDFPTH

 
      CHARACTER*25 CTQMEN(2)
      CHARACTER*200 STRMEN
      CHARACTER*30 PROCES
      COMMON/PROCES/NSUB,PROCES
      COMMON /DEVICE/  IPRT,ISES,IBAT,ISTR(2),IEVENT,ITMP

      DATA INHADR/1,1/
      DATA CTQMEN/'CTEQ3m (  Proton )',
     &            'CTEQ3m ( AntiProton )'/
      DATA FIRST/.TRUE./



      SAVE

      RES=.FALSE.
      DO 1 K=-6,6
1     IF (P_NAME.EQ.Q_NAME(K))  RES=.TRUE.      
      RETURN
      
      ENTRY N_CTQ(I,NAME)
C* Return NAME of function with paramiters
      MODE=1 +  (1-INHADR(I))/2  
      NAME= CTQMEN(MODE) 
      RETURN

      ENTRY R_CTQ(I,NAME,BE,RES)
      BE=1
      DO 2 MODE=1,2
      IF ( NAME.EQ.CTQMEN(MODE))  THEN
        INHADR(I) = 1- 2*(MODE-1)             
        DO 11 K=-6,6
          IF (PINF(NSUB,I).EQ.Q_NAME(K))  THEN
            NQUARK(I)=K
            RES=.TRUE.
            RETURN
          ENDIF
11      CONTINUE
        RES=.FALSE.                             
        RETURN
      ENDIF
2     CONTINUE
      RES=.FALSE.
      RETURN

      ENTRY M_CTQ(I)
   
      CALL CLRSCR


      STRMEN = '% CTQ menu!%'//
     & CTQMEN(1)//'!'//CTQMEN(2)//'!'//
     & '!!!'
      
      
      MODE=MENU(STRMEN,'SF_CTQ')
      IF(MODE.EQ.0) THEN
         RETURN
      ELSEIF ((MODE.GE.1).OR.(MODE.LE.4)) THEN
        INHADR(I) = 1 - 2*(MODE-1) 
      ELSE
         CONTINUE
      ENDIF
      RETURN

      ENTRY C_CTQ(I,X,STRF)
      

      IF(FIRST) THEN
C*          write(*,*) 'first'
        FIRST=.FALSE.
        CALL CPTH(PATH,D_SLAH,F_SLAH)
        CALL CH2CAT(PDFPTH,PATH,F_SLAH)
        CALL CH2CAT(FNAME,PDFPTH,'cteq3m.ini  ')


        OPEN(ISTR(I),FILE=fname,STATUS='UNKNOWN',ERR=100) 
        CALL cteqpdfset (ISTR(I),1.0e-5,1.0e8)
        CLOSE(ISTR(I))
      ENDIF
 
      IF(X.Lt.0.00001)  x=0.00001
      CALL SCALE(DSCALE)     
      call cteqstm(x,dscale,upv,dnv,usea,dsea,str,chm,bot,top,gl)
        NABS=ABS(NQUARK(I))
        IF (NABS .EQ. 0) THEN
          STRF = GL
        ELSE IF (NABS .EQ. 1) THEN
          STRF=USEA
          IF (NQUARK(I)*INHADR(I).GT.0) STRF=STRF+UPV
        ELSE IF (NABS .EQ. 2) THEN
          STRF = DSEA
          IF (NQUARK(I)*INHADR(I).GT.0) STRF=STRF+DNV
        ELSE IF (NABS .EQ. 3) THEN
          STRF = STR
        ELSE IF (NABS .EQ. 4) THEN
          STRF = CHM
        ELSE IF (NABS .EQ. 5) THEN
          STRF = BOT
        ELSE IF (NABS .EQ. 6) THEN
          STRF = top
        ENDIF
        STRF = STRF/X
        RETURN
100     STOP ' Can not open cteq3m.ini' 
      END  
