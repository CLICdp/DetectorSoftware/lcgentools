**********************************************************
*  Equivalent electron approximation structure function. *
*                                                        *
*    XIN  is a (small) mass of virtual 'electron',       *
*    XOUT is the summary mass of rest out-particles.     *
**********************************************************

      SUBROUTINE P_EEA(P_NAME,RES)      
C* Check particle name   
      IMPLICIT REAL*8 (A-H,O-Z)
      CHARACTER*60 NAME
      CHARACTER*6 P_NAME
      DIMENSION XIN(2),XOUT(2)
      LOGICAL RES 
      CHARACTER*20 HEADER
      CHARACTER*10 BUFF1,BUFF2
      CHARACTER*500 STRMEN
      CHARACTER*15 CHM1,CHM2
      COMMON /SQS/ SQRTS
C*      ALFA =1./137.
      DATA ALFA/0.0072992701/
      DATA PI/3.1415927/
      DATA XIN/0.005,0.005/, XOUT/100,100/


      SAVE
     
      IF ((P_NAME.EQ.'E1').OR.(P_NAME.EQ.'e1')) THEN 
         RES=.TRUE.
      ELSE
         RES=.FALSE.
      ENDIF

      RETURN

      ENTRY N_EEA(I,NAME)
C* Return NAME of function with paramiters 
        WRITE(NAME,FMT='(A20,A6,F10.4,A2)')
     &'Equiv.Electron Appr.',' ( mX=',XOUT(I),' )'  
      RETURN

      ENTRY R_EEA(I,NAME,BE,RES)
      BE=1
      READ(NAME,FMT='(A20,A6,F10.4)',ERR=10)
     &HEADER,BUFF2,XOUT_
      IF (HEADER.NE.'Equiv.Electron Appr.') GOTO 10
      IF (XOUT_.LE.0) GOTO 10
      RES=.TRUE.
      XOUT(I)=XOUT_
      RETURN
10    RES=.FALSE.
      RETURN

      ENTRY M_EEA(I)
   
20    CALL CLRSCR

      WRITE(CHM2,FMT='(G12.3)') XOUT(I) 
     
      STRMEN = '%Equiv. Electron Appr. menu!%'//
     & 'Produced mass minimum=' //CHM2 //
     & '!!!'
            
      MODE=MENU(STRMEN,'SF_EEA')
      IF(MODE.EQ.0) THEN
         RETURN
      ELSEIF (MODE.EQ.1) THEN
         WRITE(*,FMT='(''New value = '',$)')
         READ(*,*) XX         
         IF(XX.GT.0) XOUT(I)=XX 
         RETURN
      ELSE
         CONTINUE
      ENDIF
      GOTO 20

      ENTRY C_EEA(I,X,STRF)

      XMIN=(XOUT(I)/SQRTS)**2
      XMAX=1d0
      DELT=4*(XIN(I)/XOUT(I))**2

      IF((X.LT.XMIN).OR.(X.GT.XMAX)) THEN
         STRF=0
      ELSE
         STRF=(ALFA/PI)*(LOG((1-X)/DELT)*(0.5D0-X+X**2)+X*(1-X))
      ENDIF

      END  
     
