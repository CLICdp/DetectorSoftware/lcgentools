
*************************************************************************
*   This function calculates running ALPHA_STRONG up to third order     *
*     as a function of Lambda_QCD.                                      *
*   Ref: PDG-94   (Phys. Rev. D50, 1994, p.1297).                       *
*                                                                       *
*   The value of ALPHA_S is matched at the thresholds DSCALE = Mq,      *
*     where Mq is a mass of the corresponding quark. Mq (s,c,b and      *
*     t quarks) are initialized when ALF_S is call first time.          *
*                                                                       *
*   Lambda_QCD is matched:                                              *
*     to 2,3 and 4 flavours by the PDG-94 formula and                   *
*     to 6 flavours by 1st order terms of this formula.                 *
*   So, basic parameter is Lambda_QCD of 5 flavours and                 *
*     its value is taken 0.135 GeV (deep-inelastic data).               *
*                                                                       *
*   Input:   DSCALE   = QCD scale in GeV  (see function SCALE)          *
*************************************************************************
c      DOUBLE PRECISION FUNCTIONALPHAS2(DSCALE,nl)
      DOUBLE PRECISION FUNCTION ALPHAS2(DSCALE,NNNF,QCDL5 )
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
*  Lambda_QCD of different number of flavours 
      DIMENSION QCDLF(6)
*  beta-function coefficients 
      COMMON /BETA/ BETA0(6),BETA1(6),BETA2(6)
      LOGICAL FIRST
      DATA FIRST/.TRUE./
      DATA QCDLF(5)/ 0.135d0/
      SAVE

*** initialization
      IF ((FIRST).OR. (QCDL5.NE. QCDLF(5)))  THEN
         QCDLF(5)= QCDL5
         PI4=16.0D0*ATAN(1.d0)
* empty (nonused)
         QCDLF(1) = 0.d0
         BETA0(1) = 0.d0
         BETA1(1) = 0.d0
         BETA2(1) = 0.d0
         QCDLF(2) = 0.d0
         BETA0(2) = 0.d0
         BETA1(2) = 0.d0
         BETA2(2) = 0.d0
* BETA-function coefficients
         DO 10 NFF=3,6
c         DO 10 NFF=2,6
           BETA0(NFF)= 11.d0 - 2.d0/3.d0*NFF
           BETA1(NFF)= 51.d0 -19.d0/3.d0*NFF
           BETA2(NFF)= 2857.d0-5033.d0/9.d0*NFF-325.d0/27.d0*NFF**2
10       CONTINUE
* this value was extracted from deep-inelastic data (PDG-94)
c         QCDLF(5) = 0.200d0         (PDG-94 average value)
c         QCDLF(5) = 0.135d0
c         write(*,*) 'nf=5 qcdl=',qcdlf(5)
* matching t-quark threshold (PDG-94 1st order formula)
         XMTOP = 170.d0
         QCDLF(6) = XMTOP*(QCDLF(5)/XMTOP)**(BETA0(5)/BETA0(6))
c         write(*,*) 'nf=6 qcdl=',qcdlf(6)
* matching b-quark threshold 
c         XMB = 4.75d0
         XMB = 4.3d0
         QCDLF(4)=TONF_1(QCDLF(5),XMB,5)
c         QCDLF(4)=TONF_1(QCDLF(5),XMB,5,nl)
* matching c-quark threshold 
c         XMC = 1.5d0
         XMC = 1.3d0
         QCDLF(3)=TONF_1(QCDLF(4),XMC,4)
c         QCDLF(3)=TONF_1(QCDLF(4),XMC,4,nl)
* matching s-quark threshold 
c         XMS = 0.3d0
c         QCDLF(2)=TONF_1(QCDLF(3),XMS,3,nl)
* end of the initialization
         FIRST=.FALSE.
      ENDIF

      IF     (DSCALE .GT. XMTOP) THEN
          NF = 6
      ELSEIF (DSCALE .GT. XMB) THEN
          NF = 5
      ELSEIF (DSCALE .GT. XMC) THEN
          NF = 4
      ELSE
c      ELSEIF (DSCALE .GT. XMS) THEN
          NF = 3
c      ELSE
c          NF = 2
      ENDIF

      B0 = BETA0(NF)
      B1 = BETA1(NF)
      B2 = BETA1(NF)
      RL = 2.d0*LOG(DSCALE/QCDLF(NF))
      ALPHAS2 = PI4/(B0*RL)*( 1.d0 -2.d0*B1/B0**2*LOG(RL)/RL )

c      if(nl.eq.3) then
* PDG-94 says that these extra terms (of 3rd order) are smaller then exp_error
      alf_s = alf_s + PI4/(B0*RL)*(
     &                     +4.d0*B1**2/(B0**4*RL**2)
     &                         *((LOG(RL)-0.5d0)**2
     &                            +8.d0*B2*B0/B1**2 
     &                            -5.d0/4.d0)
     &                            )
c      endif

* warning: if DSCALE<QCDLF then LOG(RL) will fail.

      RETURN
      END

****************************************************
*  Transformation of Lambda_QCD from NF to NF-1    *      
*     (formula from PDG-94)                        *
****************************************************
c      DOUBLE PRECISION FUNCTION TONF_1(QCDL,XMQ,NF,nl)
      DOUBLE PRECISION FUNCTION TONF_1(QCDL,XMQ,NF)
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      COMMON /BETA/ B0(6),B1(6),B2(6)
    
      RL = LOG(XMQ/QCDL)
* warning: if XMQ<QCDL then LOG(RL) will fail. This is a case for
*          Ms=0.3GeV threshold if QCDLF(5)=0.2GeV. Here QCDLF(3)=0.33GeV.
      B10N  = B1(NF)/B0(NF)
      B10N1 = B1(NF-1)/B0(NF-1)

c      TONF = QCDL*EXP(-1/B0(NF-1)
c     &   *( (B0(NF)-B0(NF-1))*RL
c     &     +(B10N-B10N1)*LOG(2.d0*RL)
c     &     -B10N1*LOG(B0(NF)/B0(NF-1)) ))

c      if (nl.eq.3) then
* really values of matching Lambda_QCD don't depend on extra terms
        TONF_1 = QCDL*EXP(-1/B0(NF-1)
     &   *( (B0(NF)-B0(NF-1))*RL
     &     +(B10N-B10N1)*LOG(2.d0*RL)
     &     -B10N1*LOG(B0(NF)/B0(NF-1)) 
     &     +( B10N*(B10N-B10N1)*LOG(2.d0*RL)
     &       +B10N**2-B10N1**2
     &       -B2(NF)/2.d0/B0(NF)+B2(NF-1)/2.d0/B0(NF-1)-7.d0/18.d0
     &      )/(B0(NF)*RL)
     &    )              )
c      else
c        tonf_1 = tonf
c      endif

c      write(*,*) 'nf =',nf-1,' qcdl_2=',tonf,' qcdl3=',tonf_1

      RETURN
      END

****************************************************************************
* Flavour number matching inALPHAS2:
*
*        NF              5        6        4        3       2
*    Lambda_QCD (GeV)    0.135    0.0683   0.1985   0.2408  0.2293
*                        0.200    0.1052   0.28275  0.33078 failed
*
*    Comment: for NF=2 and Lambda_QCD(5)=QCDLF(5)=0.2GeV matching is failed 
* because of the term with LOG(LOG(Ms/QCDLF(3))) where Ms<QCDLF(3).
****************************************************************************

      SUBROUTINE QCDMEN
      IMPLICIT REAL*8 (A-H,O-Z)
      CHARACTER     PROCES*30
      COMMON /PROCES/  NSUB,PROCES
      COMMON/QCDGG/ QCDL,NF
      COMMON /NSCALE/ RVAL, LIST(10)
      CHARACTER*25 CHTMP,CHALPH
      CHARACTER*12 CHNF,CHQCDL
      CHARACTER*3 CHMOM 
      CHARACTER*500 STRMEN     
      SAVE

*      NF=5
*      QCDL=0.135
*      RVAL=91.187

10    CALL CLRSCR
*      CALL ITOCHR(CHNF,NF)
      WRITE(CHNF,FMT='(I2)') NF
      WRITE(CHQCDL,FMT='(G12.3)') QCDL             
      IF(LIST(1).EQ.0) THEN
         WRITE(CHTMP,FMT='(A1,1PE10.3,A7)') '(',RVAL,'GeV)**2'
      ELSE 
         CHTMP='('
         N=1         
21       CHTMP(3*N-1:3*N+1)= CHMOM(LIST(N)) 
         N=N+1
         IF(LIST(N).NE.0) GOTO 21
         CHTMP(3*N-1:3*N+2)=')**2'
      ENDIF

      IF (LIST(1).EQ.0) THEN
        WRITE(CHALPH,FMT='(A6,G12.3)') 
     &      'ALPHA=', ALPHAS2(RVAL,NF,QCDL)
      ELSE
          CHALPH=' QCD ALPHA not defined '
      ENDIF 

  
       STRMEN = '% ALPHA QCD  menu!'//PROCES//'!'//CHALPH//     
     &    '!%QCD Lambda = '//CHQCDL//
     &    '!Q2         =  '//CHTMP  //'!!!'

      MODE=MENU(STRMEN,'f_A')
      IF (MODE.EQ.0) THEN
        RETURN
      ELSEIF (MODE.EQ.1) THEN 
        WRITE(*,FMT='('' QCD Lambda = '',$)') 
           READ(*,*)  QCDL
      ELSEIF (MODE.EQ.2) THEN
22       WRITE(*,*) 
     &     'possible input is  N<number> or L<invariant>'
         READ(*,FMT='(A20)') CHTMP
         IF ((CHTMP(1:1).EQ.'N').or.(CHTMP(1:1).EQ.'n')) THEN
            READ( CHTMP(2:20)  ,FMT='(F19.0)',ERR=29)  RVAL
            RVAL=ABS(RVAL)        
            LIST(1) = 0 
         ELSE 
            DO 23 LL=1,10
23          LIST(LL)=0
            LL=2
            DO 24 LL=2,20
24             IF(CHTMP(LL:LL).NE.' ') GOTO 25
            GOTO 29
25          READ(CHTMP(LL:20),FMT='(10I1)',ERR=29) LIST
            CALL ORDINV(LIST)
            CALL CONINV(LIST)
29          IF(LIST(1).EQ.0) THEN 
               WRITE(*,*) 'input error '
               GOTO 22
            ENDIF             
         ENDIF
      ELSE
        CONTINUE
      ENDIF
      GOTO 10

      END 
     

***************************************************
*   Write user parameters in the file SESSION.DAT *
***************************************************
      SUBROUTINE W_QCD(MODE)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      CHARACTER XXX*80
      COMMON /NSCALE/ RVAL, LIST(10)
      COMMON /QCDGG/QCDL,NF
      SAVE      
* write first (prompt) line
      WRITE(MODE,100) XXX
      WRITE(MODE,104) 'Q**2                :   ',RVAL,LIST
      WRITE(MODE,101) 'Number of flavours  ;   ',NF
      WRITE(MODE,102) 'Lambda QCD          :   ',QCDL
      RETURN
      
*********************************************
*   Initialization of user parameters       *
*********************************************
      ENTRY I_QCD
      XXX='========  QCD  parameters ==========='
      NF=5
      QCDL=0.135
      RVAL=91.187
      DO 11 I=1,10 
11      LIST(I)=0
      RETURN

****************************************************
*   Read user parameters from the file SESSION.DAT *
****************************************************
      ENTRY R_QCD(MODE)
* read first (prompt) line
      READ(MODE,100)  XXX
      READ(MODE,104) XXX,RVAL,LIST  
      READ(MODE,101) XXX,NF
      READ(MODE,102) XXX,QCDL
      RETURN
100   FORMAT(1X,A78)
101   FORMAT(1X,A24,i2)
102   FORMAT(1x,A24,g8.2)
104   FORMAT(1x,A24,E10.3,1x,10I2)
      END


      FUNCTION CHMOM(I)
      CHARACTER*3 CHMOM
      IF (I.LT.0) THEN 
          CHMOM(1:1)='-'
      ELSE
          CHMOM(1:1)='+'
      ENDIF
      CHMOM(2:2)='p'
      WRITE(CHMOM(3:3),FMT='(I1)') ABS(I)
      RETURN
      END






**********************************************************************
*    This subroutine is called during phase space integration        *
*      (during BASES and SPRING run)                                 *
*      for each phase space point after calculation particles        *
*      momenta and just before calculation of structure functions    *
*      and squared matrix element.                                   *
*                                                                    *
*    It can be used, for example, to provide QCD running strong      *
*      coupling constant. In the program there is used the notation  *
*                           GG                                       *
*      for QCD coupling constant.                                    *
******************************************************************
      SUBROUTINE ALF
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      COMMON/QCDGG/ QCDL,NF
      CHARACTER NAME*6,XNAME*6

******* search for QCD coupling constant among process parameters
      NAME='GG'
      XNAME=' '
      DO 10 K=1,NVAR()
        CALL VINF(K,XNAME,XVAL)
        IF (NAME.EQ.XNAME) GOTO 20
10    CONTINUE
      GOTO 100
20    DSCALE=1.D0
*** evaluate current transfered momentum scale
      CALL SCALE(DSCALE)
*** evaluate running alpha-s (ALPHAS2 is the function from CERN PDF library)
      ALFA=ALPHAS2(DSCALE,NF,QCDL)      
*   from evlp1.f ( cteq) 
*      CALL ALFSET (SS, AL)
*** calculate new value of GG
      XVAL = SQRT(16*ATAN(1.d0)*ALFA)
*** assign new value of GG
      CALL ASGN(K,XVAL)
100   RETURN
      END


**************************************************************
*   This subroutine user can call to evaluate the transfered *
*     momentum scale of the process - could be necessary for *
*     structure function interface.                          *
*                                                            *
*   In the example from STRFUN.F this subroutine is called   *
*     for the evaluation of transfered momentum scale.       *
*   NSCALE = 0 for t-type, and =1 for s-type of              *
*     transfered momentum scale.                             *
**************************************************************
      SUBROUTINE SCALE(DSCALE)
      IMPLICIT REAL*8 (A-H,O-Z)
      COMMON /NSCALE/ RVAL, LIST(10)
*
*     Example: for deep inelastic scattering (t-type of DSCALE), 
*            for instance    e-(p1),q(p2) -> e-(p3),q(p4),
*              dscale = sqrt(-t13), t13=(p3-p1)**2;
*              for s-channel resonance production (s-type of DSCALE),
*            for instance  W-production in pp-collisions,  
*              dscale = sqrt(shat), shat=(p1+p2)^2 where p1 and p2 are 
*              partons momenta.
*

      IF (LIST(1).EQ.0) THEN
         DSCALE=RVAL
      ELSE          
         NVBUFF=NIN()+NOUT()+3
         CALL LVTONV(LIST,NVBUFF)
         DSCALE= SQRT(ABS(VDOT4(NVBUFF,NVBUFF)))
      ENDIF

      IF (DSCALE.LT.0.3) DSCALE=0.3

      RETURN
      END
