      FUNCTION Q_NAME(I)
      CHARACTER*6 CPRTN(-6:6),Q_NAME      
      DATA CPRTN/'T','B','C','S','D','U','G','u','d','s','c','b','t'/
      SAVE
      IF ((I.LT.-6).OR.(I.GT.6) ) THEN
          Q_NAME=' '
      ELSE
          Q_NAME=CPRTN(I)
      ENDIF
      END

      SUBROUTINE P_MRS(P_NAME,RES)      
C* Check particle name   
      IMPLICIT REAL*8 (A-H,O-Z)
      CHARACTER*60 NAME
      CHARACTER*6 P_NAME,Q_NAME,PINF
      DIMENSION INHADR(2),NMRS(2),NQUARK(2)
      LOGICAL RES
      LOGICAL FIRST(2)
      CHARACTER*60 PATH 
      CHARACTER*1 D_SLAH,F_SLAH
      CHARACTER*100 FNAME,PDFPTH

 
      CHARACTER*25 MRSMEN(4)
      CHARACTER*200 STRMEN
      CHARACTER*30 PROCES
      COMMON/PROCES/NSUB,PROCES
      COMMON /DEVICE/  IPRT,ISES,IBAT,ISTR(2),IEVENT,ITMP

      DATA INHADR/1,1/,NMRS/1,1/
      DATA FIRST/.TRUE.,.TRUE./
      DATA MRSMEN/'MRS ( A''  , Proton )',
     &            'MRS ( G   , Proton )',
     &            'MRS ( A''  , AntiProton)',
     &            'MRS ( G   , AntiProton)'/      


      SAVE

      RES=.FALSE.
      DO 1 K=-6,6
1     IF (P_NAME.EQ.Q_NAME(K))  RES=.TRUE.      
      RETURN
      
      ENTRY N_MRS(I,NAME)
C* Return NAME of function with paramiters
      MODE=( 1-INHADR(I) ) + NMRS(I)
      NAME= MRSMEN(MODE) 
      RETURN

      ENTRY R_MRS(I,NAME,BE,RES)
      BE=1
      RES=.FALSE.
      DO 2 MODE=1,4
      IF ( NAME.EQ.MRSMEN(MODE))  THEN
        RES=.TRUE.      
        INHADR(I) = 1 - 2*((MODE-1)/2) 
        NMRS(I)= MODE+INHADR(I)-1
       
        DO 11 K=-6,6
          IF (PINF(NSUB,I).EQ.Q_NAME(K)) THEN
             NQUARK(I)=K      
             RES=.TRUE.
             RETURN
          ENDIF  
11      CONTINUE
        RES=.FALSE.                  
        RETURN
      ENDIF      
2     CONTINUE
      RES=.FALSE.
      RETURN

      ENTRY M_MRS(I)
   
      CALL CLRSCR


      STRMEN = '% MRS menu!%'//
     & MRSMEN(1)//'!'//MRSMEN(2)//'!'//
     & MRSMEN(3)//'!'//MRSMEN(4)//'!!!'
      
      
      MODE=MENU(STRMEN,'SF_MRS')
      IF(MODE.EQ.0) THEN
         RETURN
      ELSEIF ((MODE.GE.1).OR.(MODE.LE.4)) THEN
        INHADR(I) = 1 - 2*((MODE-1)/2) 
        NMRS(I)= MODE+INHADR(I)-1         
      ELSE
         CONTINUE
      ENDIF
      RETURN

      ENTRY C_MRS(I,X,STRF)
      

      IF(FIRST(NMRS(I))) THEN
        FIRST(NMRS(I))=.FALSE.
        CALL CPTH(PATH,D_SLAH,F_SLAH)
        CALL CH2CAT(PDFPTH,PATH,F_SLAH)
        IF (NMRS(I).EQ.1) THEN
          CALL CH2CAT(FNAME,PDFPTH,'ftn30  ')
        ELSE
          CALL CH2CAT(FNAME,PDFPTH,'ftn31  ')
        ENDIF
        OPEN(ISTR(I),FILE=FNAME,STATUS='UNKNOWN')
      ENDIF
 
      IF(X.Lt.0.00001)  x=0.00001
      CALL SCALE(DSCALE)  
      IF(DSCALE**2.LT.5) DSCALE=2.25
      CALL MRSEB(ISTR(I),X,DSCALE,NMRS(I),
     &                    UPV,DNV,USEA,DSEA,STR,CHM,BOT,GL)        
        NABS=ABS(NQUARK(I))
        IF (NABS .EQ. 0) THEN
          STRF = GL
        ELSE IF (NABS .EQ. 1) THEN
          STRF=USEA
          IF (NQUARK(I)*INHADR(I).GT.0) STRF=STRF+UPV
        ELSE IF (NABS .EQ. 2) THEN
          STRF = DSEA
          IF (NQUARK(I)*INHADR(I).GT.0) STRF=STRF+DNV
        ELSE IF (NABS .EQ. 3) THEN
          STRF = STR
        ELSE IF (NABS .EQ. 4) THEN
          STRF = CHM
        ELSE IF (NABS .EQ. 5) THEN
          STRF = BOT
        ELSE IF (NABS .EQ. 6) THEN
          STRF = 0
        ENDIF
        STRF = STRF/X
      END  
