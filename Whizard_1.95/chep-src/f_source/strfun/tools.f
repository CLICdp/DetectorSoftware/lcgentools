      FUNCTION DINTER(x,N,XI,YI)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION XI(500),YI(500)

      IF(x.LE.XI(2)) THEN
      x1=XI(1)
      x2=XI(2)
      x3=XI(3)
      DINTER=
     & YI(1)*       (x-x2)*(x-x3)/(        (x1-x2)*(x1-x3))
     &+YI(2)*(x-x1)*       (x-x3)/((x2-x1)*        (x2-x3))
     &+YI(3)*(x-x1)*(x-x2)       /((x3-x1)*(x3-x2)        )
      ELSE IF (x.GT.XI(N-1)) THEN
      x1=XI(N-2)
      x2=XI(N-1)
      x3=XI(N)
      DINTER=
     & YI(N-2)*       (x-x2)*(x-x3)/(        (x1-x2)*(x1-x3))        
     &+YI(N-1)*(x-x1)*       (x-x3)/((x2-x1)*        (x2-x3)) 
     &+YI(N  )*(x-x1)*(x-x2)       /((x3-x1)*(x3-x2)        )      
      ELSE
        I=2
10      I=I+1
        IF(x.GT.XI(I)) GOTO 10
        x1=XI(I-1)
        x2=XI(I)
        x3=XI(I+1)
        x4=XI(I+2)
        DINTER=
     & YI(I-1)*     (x-x2)*(x-x3)*(x-x4)/(      (x1-x2)*(x1-x3)*(x1-x4))
     &+YI(I  )*(x-x1)*     (x-x3)*(x-x4)/((x2-x1)*      (x2-x3)*(x2-x4))
     &+YI(I+1)*(x-x1)*(x-x2)*     (x-x4)/((x3-x1)*(x3-x2)*      (x3-x4))
     &+YI(I+2)*(x-x1)*(x-x2)*(x-x3)     /((x4-x1)*(x4-x2)*(x4-x3)      )
      ENDIF
      RETURN 
      END

      FUNCTION GAMMAI(N,A)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      COMMON/GAMM/NGAM
      LOGICAL  ISCALK(100)
      DIMENSION GMEM(100)

      EXTERNAL GAMMA0 

      DATA AA/-1/

      SAVE

      IF(A.NE.AA) THEN
        AA=A
        EA=EXP(-A)
        DO 5 I=1,100
5       ISCALK(I)=.FALSE.       
      ENDIF
 
      IF((N.LE.100).AND.ISCALK(N)) THEN
        GAMMAI=GMEM(N)
        RETURN
*      ELSE
*         write(*,*) 'GAMMA(',N,')'
      ENDIF
   
      
      EA=EXP(-A)
      GAMMAI=1- EA

      ISCALK(1)=.TRUE.
      GMEM(1)=GAMMAI
      ERR=0
      DO 10 I=1,N-1
      EA=A*EA
      GAMMAI=I*GAMMAI - EA
      ISCALK(I+1)=.TRUE.
      GMEM(I+1)=GAMMAI
      ERR=I*ERR+ EA*1.E-14
      IF (ERR.GT.1.D-8*GAMMAI) THEN
        NGAM=N
        EPS=1.D-8
        GAMMAI=SIMPS0(GAMMA0,0.D0,A,EPS)
        IF(N.LE.100)  ISCALK(N)=.TRUE.
        IF(N.LE.100)  GMEM(N)=GAMMAI 
        RETURN
      ENDIF
10    CONTINUE
      
      RETURN
      END

      FUNCTION GAMMA0(Z)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      COMMON/GAMM/NGAM
      GAMMA0=Z**(NGAM-1)*EXP(-Z)      
      RETURN
      END


      FUNCTION DGAMMA(R)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL FIRST
      DIMENSION XI(23),YI(23)

      DATA FIRST/.TRUE./
      DATA XI/0.45D0,0.50D0,0.55D0,0.60D0,0.65D0,0.70D0,0.75D0,0.80D0,    
     &0.85D0,0.90D0,0.95D0,1.00D0,1.05D0,1.10D0,1.15D0,1.20D0,1.25D0,    
     &1.30D0,1.35D0,1.40D0,1.45D0,1.50D0,1.55D0/    
      DATA YI/
     &0.5080948656271617D0, 0.5641895835477563D0,0.6187642988518796D0,    
     &0.6715049724437373D0, 0.7221284929050429D0,0.7703831838841270D0,    
     &0.8160489391375885D0, 0.8589370192976601D0,0.8988895450003129D0,    
     &0.9357787210546802D0, 0.9695058259259547D0,1.000000000000000D0,    
     &1.027216865140965D0,  1.051137005952489D0, 1.071764342307242D0,    
     &1.089124420965782D0,  1.103262651267671D0, 1.114242508521903D0,    
     &1.122143726457711D0,  1.127060497983235D0, 1.129099701393207D0,    
     &1.128379167095513D0,  1.125025997912024/    
      SAVE
      
      DGAMMA=1
      RR=R
*       write(*,*) 'RR=',RR
10    IF( ABS(RR-1.D0).GT.0.5D0) THEN
        IF (RR.GT.1) THEN
          RR=RR-1
          DGAMMA=DGAMMA*RR
        ELSE
          DGAMMA=DGAMMA/RR
          RR=RR+1
        ENDIF
        GOTO 10
      ENDIF  
      
      DGAMMA=DGAMMA/DINTER(RR,23,XI,YI)
      RETURN
      END




      FUNCTION SIMPS0(FUNC,A,B,EPS)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      EXTERNAL FUNC

      H2=(B-A)/2
      H3=(B-A)/3
      N=2
      SODD=0.5D0*(FUNC(A)+FUNC(B))
      SEVEN= FUNC(H2)
      SIMPS= H3*(SODD + 2*SEVEN)
      ERR=SIMPS  
  
10    SIMPS_=SIMPS
      SODD=SODD+SEVEN
      SEVEN=0
      X=A+ H2/2
      DO 20 I=1,N   
        SEVEN=SEVEN+ FUNC(X)
20      X=X+H2

      H2=H2/2
      H3=H3/2
      N=N*2
      SIMPS=H3*(SODD + 2*SEVEN)
      ERR_=ERR
      ERR= ABS(SIMPS-SIMPS_)
      ERR0=ABS(SIMPS)*EPS
*      write(*,*)'S0',N, ERR,ERR0
      IF( (ERR.LT.ERR0).AND.(ERR_/16.LT.ERR0).AND.(N.GT.4) )THEN
        SIMPS0=SIMPS
        RETURN
      ENDIF                 
      IF (N.GT.100000) THEN
         EPS=ERR/ABS(SIMPS)
         SIMPS0=SIMPS 
         RETURN
      ENDIF
      GOTO 10    
      END

      FUNCTION SIMPS1(FUNC,A,B,EPS)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      EXTERNAL FUNC

      H2=(B-A)/2
      H3=(B-A)/3
      N=2
      SODD=0.5D0*(FUNC(A)+FUNC(B))
      SEVEN= FUNC(H2)
      SIMPS= H3*(SODD + 2*SEVEN)
      ERR=SIMPS  
  
10    SIMPS_=SIMPS
      SODD=SODD+SEVEN
      SEVEN=0
      X=A+ H2/2
      DO 20 I=1,N   
        SEVEN=SEVEN+ FUNC(X)
20      X=X+H2

      H2=H2/2
      H3=H3/2
      N=N*2
      SIMPS=H3*(SODD + 2*SEVEN)
      ERR_=ERR
      ERR= ABS(SIMPS-SIMPS_) 
      ERR0=ABS(SIMPS)*EPS
*      write(*,*)'S1', N, ERR,ERR0
      IF( (ERR.LT.ERR0).AND.(ERR_/16.LT.ERR0).AND.(N.GT.4) )THEN
        SIMPS1=SIMPS
        RETURN
      ENDIF                 
      IF (N.GT.100000) THEN
         EPS=ERR/ABS(SIMPS)
         SIMPS1=SIMPS 
         RETURN
      ENDIF
      GOTO 10    
      END


      FUNCTION CONVOL(F1,F2,B1,B2,X,EPS)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      EXTERNAL F1,F2
      LOGICAL FIRST
      FF1(U)=F1(U)*(X-U)**(B2-1)*F2(X-U)/B1
      FF2(U)=F2(U)*(X-U)**(B1-1)*F1(X-U)/B2

*      write(*,*) 'convol x=',x
      IF(X.EQ.0.D0) THEN
        CONVOL=F1(X)*F2(X)*DGAMMA(B1)*DGAMMA(B2)/DGAMMA(B1+B2)
*        write(*,*) 'convol ok'
        RETURN
      ENDIF 
      FIRST=.TRUE.
      CONVOL=0
      A=0
5     CONTINUE
      IF (FIRST) THEN
        B=(X/2)**B1
      ELSE
        B=(X/2)**B2
      ENDIF 
      
      H2=(B-A)/2
      H3=(B-A)/3
      N=2
      IF(FIRST) THEN
         SODD=0.5D0*(FF1(A)+FF1(X/2))
         SEVEN= FF1(H2**(1/B1))
      ELSE
         SODD=0.5D0*(FF2(A)+FF2(X/2))
         SEVEN= FF2(H2**(1/B2))
      ENDIF

      SIMPS= H3*(SODD + 2*SEVEN)
      ERR=SIMPS 

  
10    SIMPS_=SIMPS
      SODD=SODD+SEVEN
      SEVEN=0
      XX=A+ H2/2
      DO 20 I=1,N   
      IF(FIRST) THEN
        SEVEN=SEVEN+ FF1(XX**(1/B1))
      ELSE
        SEVEN=SEVEN+ FF2(XX**(1/B2))
      ENDIF
      
20    XX=XX+H2

      H2=H2/2
      H3=H3/2
      N=N*2
      SIMPS=H3*(SODD + 2*SEVEN)
      ERR_=ERR
      ERR= ABS(SIMPS-SIMPS_) 
      ERR0=ABS(SIMPS)*EPS
*      write(*,*) ERR
      IF( (ERR.LE.ERR0).AND.(ERR_/16.LE.ERR0).AND.(N.GT.4) )THEN
        CONVOL=CONVOL+SIMPS
        GOTO 30
      ENDIF                 
      IF (N.GT.100000) THEN
        EPS=ERR/ABS(SIMPS)
        CONVOL=CONVOL+SIMPS 
        GOTO 30
      ENDIF
      GOTO 10    
30    IF(FIRST) THEN
         FIRST=.FALSE.
         GOTO 5
      ENDIF
      CONVOL=CONVOL*X**(1-B1-B2)
      RETURN
      END
