      FUNCTION AMHATF(I)
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      COMMON / CWZPRM / ALAM(0:9), AMHAT(0:9), AMN, NHQ
      COMMON / QCDPAR / AL, NF, NORDER, SET
      COMMON / IOUNIT / NIN, NOUT, NWRT
      LOGICAL SET
      IF (.NOT.SET) CALL LAMCWZ
      IF ((I.LE.0).OR.(I.GT.9)) THEN
         WRITE (NOUT,*) 'I IS OUT OF RANGE IN AMHATF'
         AMHATF = 0
      ELSE
         AMHATF = AMHAT(I)
      ENDIF
      RETURN
      END
      FUNCTION DXDZ (Z)
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      PARAMETER (D0=0D0, D1=1D0, D2=2D0, D3=3D0, D4=4D0, D10=1D1)
      COMMON / IOUNIT / NIN, NOUT, NWRT
      DATA HUGE, IWRN / 1.E20, 0 /
      ZZ = Z
      X = XFRMZ (ZZ)
      TEM = DZDX (X)
      IF     (TEM .NE. D0) THEN
        TMP = D1 / TEM
      Else
      CALL WARNR(IWRN, NWRT, 'DXDZ is singular in DXDZ; DXDZ set=HUGE',
     >             'Z', Z, D0, D1, 0)
        TMP = HUGE
      EndIf
      DXDZ = TMP
      RETURN
      END
      SUBROUTINE EVLSET (NAME, VALUE, IRET)
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      LOGICAL LSTX
      CHARACTER*(*) NAME
      PARAMETER (MXX = 105, MXQ = 25, MXF = 6)
      PARAMETER (MXPN = MXF * 2 + 2)
      PARAMETER (MXQX= MXQ * MXX,   MXPQX = MXQX * MXPN)
      COMMON / IOUNIT / NIN, NOUT, NWRT
      COMMON / XXARAY / XCR, XMIN, XV(0:MXX), LSTX, NX
      COMMON / QARAY1 / QINI,QMAX, QV(0:MXQ),TV(0:MXQ), NT,JT,NG
      COMMON / EVLPAC / AL, IKNL, IPD0, IHDN, KF
      IRET = 1
      IF     (NAME .EQ. 'QINI')  THEN
          IF (VALUE .LE. 0) GOTO 12
          QINI = VALUE
      ElseIF (NAME .EQ. 'IPD0')  THEN
          ITEM = NINT(VALUE)
          ITM = MOD (ITEM, 20)
          IF (ITM .LT. 0 .OR. ITM .GT. 9) GOTO 12
          IPD0 = ITEM
      ElseIF (NAME .EQ. 'IHDN') THEN
          ITEM = NINT(VALUE)
          IF (ITEM .LT. -1 .OR. ITEM .GT. 5) GOTO 12
          IHDN = ITEM
      ElseIF (NAME .EQ. 'QMAX')  THEN
          IF (VALUE .LE. QINI) GOTO 12
          QMAX = VALUE
      ElseIF (NAME .EQ. 'IKNL') THEN
          ITMP = NINT(VALUE)
          ITEM = ABS(ITMP)
          IF (ITEM .NE. 1 .AND. ITEM .NE. 2) GOTO 12
          IKNL = ITMP
      ElseIF (NAME .EQ. 'XCR') THEN
          IF (VALUE .LT. XMIN .OR. VALUE .GT. 10.) GOTO 12
          XCR = VALUE
          LSTX = .FALSE.
      ElseIF (NAME .EQ. 'XMIN') THEN
          IF (VALUE .LT. 1D-7 .OR. VALUE .GT. 1D0) GOTO 12
          XMIN = VALUE
          LSTX = .FALSE.
      ElseIF (NAME .EQ. 'NX') THEN
          ITEM = NINT(VALUE)
          IF (ITEM .LT. 10 .OR. ITEM .GT. MXX-1) GOTO 12
          NX = ITEM
          LSTX = .FALSE.
      ElseIF (NAME .EQ. 'NT') THEN
          ITEM = NINT(VALUE)
          IF (ITEM .LT. 2 .OR. ITEM .GT. MXQ) GOTO 12
          NT = ITEM
      ElseIF (NAME .EQ. 'JT') THEN
          ITEM = NINT(VALUE)
          IF (ITEM .LT. 1 .OR. ITEM .GT. 5) GOTO 12
          JT = ITEM
      ElseIF (NAME .EQ. 'KF') THEN
          ITEM = NINT(VALUE)
          IF (ITEM .LT. 1 .OR. ITEM .GT. MXPN) GOTO 12
          KF = ITEM
      Else
          IRET = 0
      EndIf
      RETURN
   12 IRET = 2
      RETURN
      END
      SUBROUTINE EVLGET (NAME, VALUE, IRET)
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      LOGICAL LSTX
      CHARACTER*(*) NAME
      PARAMETER (MXX = 105, MXQ = 25, MXF = 6)
      PARAMETER (MXPN = MXF * 2 + 2)
      PARAMETER (MXQX= MXQ * MXX,   MXPQX = MXQX * MXPN)
      COMMON / IOUNIT / NIN, NOUT, NWRT
      COMMON / XXARAY / XCR, XMIN, XV(0:MXX), LSTX, NX
      COMMON / QARAY1 / QINI,QMAX, QV(0:MXQ),TV(0:MXQ), NT,JT,NG
      COMMON / EVLPAC / AL, IKNL, IPD0, IHDN, KF
      IRET = 1
      IF     (NAME .EQ. 'QINI')  THEN
          VALUE = QINI
      ElseIF (NAME .EQ. 'IPD0')  THEN
          VALUE = IPD0
      ElseIF (NAME .EQ. 'IHDN') THEN
          VALUE = IHDN
      ElseIF (NAME .EQ. 'QMAX')  THEN
          VALUE = QMAX
      ElseIF (NAME .EQ. 'IKNL') THEN
          VALUE = IKNL
      ElseIF (NAME .EQ. 'XCR') THEN
          VALUE = XCR
      ElseIF (NAME .EQ. 'XMIN') THEN
          VALUE = XMIN
      ElseIF (NAME .EQ. 'NX') THEN
          VALUE = NX
      ElseIF (NAME .EQ. 'NT') THEN
          VALUE = NT
      ElseIF (NAME .EQ. 'JT') THEN
          VALUE = JT
      ElseIF (NAME .EQ. 'KF') THEN
          VALUE = KF
      Else
          IRET = 0
      EndIf
      RETURN
      ENTRY EVLOUT (NOUUT)
c      WRITE (NOUUT, 131) QINI, IPD0, IHDN, QMAX, IKNL,
c     >      XMIN, XCR, NX, NT, JT, KF
  131 FORMAT ( /
     >' Current parameters and values are: '//
     >' Initiation parameters: Qini, Ipd0, Ihdn = ', F8.2, 2I8 //
     >' Maximum Q, Order of Alpha:   Qmax, IKNL = ', 1PE10.2, I6//
     >' X- mesh parameters   : Xmin, Xcr,  Nx   = ', 2(1PE10.2), I8//
     >' LnQ-mesh parameters  : Nt,   Jt         = ', 2I8       //
     >' # of parton flavors  : Kf (2 * Nf + 1)  = ',  I8       /)
      RETURN
      END
      SUBROUTINE EVLGT1 (NAME, VALUE, IRET)
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      LOGICAL LSTX
      CHARACTER*(*) NAME
      PARAMETER (MXX = 105, MXQ = 25, MXF = 6)
      PARAMETER (MXPN = MXF * 2 + 2)
      PARAMETER (MXQX= MXQ * MXX,   MXPQX = MXQX * MXPN)
      COMMON / IOUNIT / NIN, NOUT, NWRT
      COMMON / X1RRAY / XCR, XMIN, XV(0:MXX), LSTX, NX
      COMMON / Q1RAY1 / QINI,QMAX, QV(0:MXQ),TV(0:MXQ), NT,JT,NG
      COMMON / E1LPAR / AL, IKNL, IPD0, IHDN, KF
      IRET = 1
      IF     (NAME .EQ. 'QINI')  THEN
          VALUE = QINI
      ElseIF (NAME .EQ. 'IPD0')  THEN
          VALUE = IPD0
      ElseIF (NAME .EQ. 'IHDN') THEN
          VALUE = IHDN
      ElseIF (NAME .EQ. 'QMAX')  THEN
          VALUE = QMAX
      ElseIF (NAME .EQ. 'IKNL') THEN
          VALUE = IKNL
      ElseIF (NAME .EQ. 'XCR') THEN
          VALUE = XCR
      ElseIF (NAME .EQ. 'XMIN') THEN
          VALUE = XMIN
      ElseIF (NAME .EQ. 'NX') THEN
          VALUE = NX
      ElseIF (NAME .EQ. 'NT') THEN
          VALUE = NT
      ElseIF (NAME .EQ. 'JT') THEN
          VALUE = JT
      ElseIF (NAME .EQ. 'KF') THEN
          VALUE = KF
      Else
          IRET = 0
      EndIf
      RETURN
      ENTRY EVLOT1 (NOUUT)
c      WRITE (NOUUT, 131) QINI, IPD0, IHDN, QMAX, IKNL,
c     >      XMIN, XCR, NX, NT, JT, KF
  131 FORMAT ( /
     >' Current parameters and values are: '//
     >' Initiation parameters: Qini, Ipd0, Ihdn = ', F8.2, 2I8 //
     >' Maximum Q, Kernel ID : Qmax, IKNL       = ', 1PE10.2, I6//
     >' X- mesh parameters   : Xmin, Xcr,  Nx   = ', 2(1PE10.2), I8//
     >' LnQ-mesh parameters  : Nt,   Jt         = ', 2I8       //
     >' # of parton flavors  : Kf (2 * Nf + 1)  = ',  I8       /)
      RETURN
      END
      SUBROUTINE EVLIN
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      COMMON / IOUNIT / NIN, NOUT, NWRT
      CALL EVLGET ('QINI', V1, IR1)
      CALL EVLGET ('IPD0', V2, IR2)
      CALL EVLGET ('IHDN', V3, IR3)
    1 WRITE  (NOUT, 101) V1, V2, V3
  101 FORMAT (/' Current values of parameters QINI, IPD0, IHDN: ',
     >        1PE10.2, 2I6 / '$Type new values: ' )
      READ(NIN, *, ERR=302) V1, V2, V3
      CALL EVLSET ('QINI', V1, IRET1)
      CALL EVLSET ('IPD0', V2, IRET2)
      CALL EVLSET ('IHDN', V3, IRET3)
      goto 301
 302  write(nout,120)
      goto 1
 301  IF ((IRET1.NE.1) .OR. (IRET2.NE.1) .OR. (IRET3.NE.3)) THEN
   20   WRITE (NOUT, 120)
        GOTO 1
      EndIf
      CALL EVLGET ('QMAX', V1, IR1)
      CALL EVLGET ('NT',   V2, IR2)
      CALL EVLGET ('JT',   V3, IR3)
    2 WRITE  (NOUT, 102)  V1, V2, V3
  102 FORMAT (/' Current values of parameters QMAX, NT, JT: ',
     >        1PE10.2, I6, I6 / '$Type new values: ' )
      READ(NIN, *, ERR=304) V1, V2, V3
      CALL EVLSET ('QMAX', V1, IRET1)
      CALL EVLSET ('NT',   V2, IRET2)
      CALL EVLSET ('JT',   V3, IRET3)
      goto 303
 304  write(nout,120)
      goto 2
 303  IF ((IRET1.NE.1) .OR. (IRET2.NE.1) .OR. (IRET3.NE.1)) THEN
   22   WRITE (NOUT, 120)
        GOTO 2
      EndIf
      CALL EVLGET ('XMIN', V1, IR1)
      CALL EVLGET ('XCR',  V2, IR2)
      CALL EVLGET ('NX',   V3, IR3)
      CALL EVLGET ('KF',   V4, IR4)
      CALL EVLGET ('IKNL', V5, IR5)
    3 WRITE  (NOUT, 103)  V1, V2, V3, V4, V5
  103 FORMAT(/' Current values of parameters XMIN, XCR, NX, KF, IKNL: ',
     >  2(1PE12.3), 3I6 / '$Type new values: ' )
      READ(NIN, *, ERR=22) V1, V2, V3, V4, V5
      CALL EVLSET ('XMIN', V1, IRET1)
      CALL EVLSET ('XCR',  V2, IRET2)
      CALL EVLSET ('NX',   V3, IRET3)
      CALL EVLSET ('KF',   V4, IRET4)
      CALL EVLSET ('IKNL', V5, IRET5)
      IF ( (IRET1 .NE. 1) .OR. (IRET2 .NE. 1) .OR. (IRET3 .NE. 1)
     > .OR. (IRET4 .NE. 1) .OR. (IRET5 .NE. 1) ) THEN
   23   WRITE (NOUT, 120)
        GOTO 3
      EndIf
  120 FORMAT(' Bad values, Try again!' /)
      RETURN
      END


      SUBROUTINE cteqpdfset(IU1,xmin,q2max)

      REAL xmin,q2max,qmax
C*      CHARACTER fname*(*)

C*      CHARACTER cteqdir*54,flin1*65 

      double precision dqmax,qmss(3),dxmin,dq2max
      INTEGER nx,nq,lpr,len1,ierr,len2 

      
      dQ2max = q2max
      qmax=sqrt(q2max)
      dqmax=qmax

C                       Number of grid points in LnLnQ variable
      Nq   = 10
C                       Min. value of X needed. (Xmax is, of course, 1.0)
      dXmin = xmin
C                       Number of grid points in X
      Nx   = 80
C                       Print level control switch (for details to beprinted)
      Lpr  = 0
C                       "heavy quark" mass thresholds
      qmss(1) = 1.6d0
      qmss(2) = 5.0d0
      qmss(3) = 180.d0

C*      iu1 = 99
C*      OPEN (IU1, FILE=fname, STATUS='UNKNOWN',err=901)
      CALL Evlini(Iu1, dXmin,dQmax,qmss, Nx,Nq,Lpr)
C*      CLOSE(IU1)
      RETURN
  901 IERR = 1
      RETURN

C                        ****************************
      END



      SUBROUTINE cteqstm(sx,sscale,upv,dnv,usea,dsea,str,chm,bot,top,gl)
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)

C*      REAL sx,sscale,upv,dnv,usea,dsea,str,chm,bot,top,gl
C*      double precision x,scale,pardis

C     Iparton = (6, 5, 4, 3, 2, 1, 0, -1, ......, -6)
C           for (t, b, c, s, d, u, g, u_bar, ..., t_bar)


      scale=sscale
      x=sx

      upv=x*ParDis(1,x,scale)-x*pardis(-1,x,scale)
      dnv=x*ParDis(2,x,scale)-x*pardis(-2,x,scale)
      usea=x*ParDis(-1,x,scale)
      dsea=x*ParDis(-2,x,scale)
      str=x*ParDis(3,x,scale)
      chm=x*ParDis(4,x,scale)
      bot=x*ParDis(5,x,scale)
      top=x*ParDis(6,x,scale)
      gl=x*ParDis(0,x,scale)

      END


      SUBROUTINE Evlini (Nini, Xmin,Qmax,Qmss, Nx,Nq,Lpr)

C !! Warning:
C !! The input nnn.ini file must be open and associated with Unit# = Nini

C   Explanations:

C * READ in nnn.ini file (Unit # = Nini) for input evolu parameters from fit nnn
C * Do evolution in the range (Xmin, 1) and (Qini, Qmax)
C                                           (Qini is given in .ini)
C * Qmss(4:6) are heavy quark masses to be used for charm, bottom, and top.
C * Nx and Nq are # of grids in (x,Q) - tradeoff between accuracy/efficiency
C * Set up Pdf(Iset, Ihadn, Iparton, x, Q, Irt)
C   Pdf's   to be used as Iset = 10
C * Lpr : print level -- for diagnostics and debugging

C To simplify the logistics, the total # of flavors will be set always = 6
C To generate PDF's with less than 6 flavors, simply decouple the unwanted
C heavy flavors by setting their masses beyond Qmax.

      IMPLICIT DOUBLE PRECISION (A-H, O-Z)

      PARAMETER (NF0 = 4, nshp = 4,NEX = nshp+2)

      CHARACTER head*78

      PARAMETER (MXQ = 25, MxQrk = 6)

      PARAMETER (MXX = 105, MXF = 6)

      EXTERNAL FitIni
C
      DIMENSION Qmss(3)

      COMMON /FitIpt/ BP(0:NEX, -NF0:2)
      COMMON /InpFin/ A(Nshp), B(0:NEX, -NF0:2), Ifun

      DATA D0, Ahdn / 0.0, 1.0 /
C                                          ----------------------------
C                                             Initialize pdf & Qcd parameters
      Dumm = SetPdf()
      Dumm = SetQcd()

C                                              Nini is the input Xnnn.ini file
      CALL iniread(Nini, qini, anfl, ordn, ordi, blam, Qm4, Qm5)

      IF (Lpr .GE. 2) PRINT '(A/6F9.3, I5)',
     >' Nini, qini, anfl, ordn, ordi, blam, ifun=',
     >  float(Nini), qini, anfl, ordn, ordi, blam, ifun
C                                          ----------------------------
C                                           Set up evolution parameters
C                                  Will always generate 6 total flavors
      Tnfl = 6.0
      CALL PARQCD(1, 'NFL', Tnfl, JR)
      CALL PARPDF(1, 'IHDN', Ahdn, JR)
C                                          From subroutine arguments
      Anx = Nx
      Ant = Nq

      IF(Qmss(1).NE.Qm4 .OR. Qmss(2).NE.Qm5) THEN
        PRINT  *,'WARNING:M4,M5 not quite match'
        WRITE (*,'(A4,2f10.4)')'M4:',Qmss(1),Qm4
        WRITE (*,'(A4,2f10.4)')'M5:',Qmss(2),Qm5
      ENDIF

      CALL parqcd(1, 'M4', Qmss(1), jr)
      CALL parqcd(1, 'M5', Qmss(2), jr)
      CALL parqcd(1, 'M6', Qmss(3), jr)

      CALL parpdf(1, 'QMAX', qmax, jr)
      CALL parpdf(1, 'XMIN', xmin, jr)
      CALL parpdf(1, 'NX', anx, jr)
      CALL parpdf(1, 'NT', ant, jr)
C                                           From .ini
      CALL PARQCD(1, 'ORDR', ORDN, JR)

      CALL PARPDF(1, 'IKNL', ORDI, JR)
      CALL PARPDF(1, 'QINI', QINI, IR)
C                                     Set Lambda for Anfl effective flavors
C                                  without affecting total number of flavors
      Neff = Nint (Anfl)
      IordEvl = Nint (Ordi)
      CALL SetLam (Neff, Blam, IordEvl)

c             Fill B() matrix (doing the equivalent job of Inptn in fitpac.)

      DO 10 IFL = -NF0, 2
        DO 10 IEX =  0, NEX
          B(IEX, IFL) = BP(IEX, IFL)
   10 CONTINUE

c      PRINT '(A/A)', 'Initial distribution parameters:'
c     >  ,'    N         A1         A2         A3         A4'
c      DO 11 ifl = 2,-nf0,-1
c   11 PRINT '(1X, 5(1pE11.3))',  (b(iex,ifl),iex=0,4)
C
c      PRINT *, 'Start evolution ....'
C                                      ----------------------------
      Anout = 6
      CALL ParPdf (4, 'DUM', ANout, Jr)
      CALL Evolve (FitIni, Irt)

      RETURN
C                        ****************************
      END

      SUBROUTINE iniread(Nini,qini,anfl,ordn,ordi,blam,Qm4,Qm5)

      IMPLICIT double precision (a-h,o-z)

      CHARACTER*80 lin80

      PARAMETER(nf0=4,nshp=4,nex=nshp+2)

      COMMON /InpFin/ A(Nshp), B(0:NEX, -NF0:2), Ifun
      COMMON /FitIpt/ BP(0:NEX, -NF0:2)

      READ(Nini,'(a80)')lin80
      READ(Nini,'(a80)')lin80
      READ(Nini,*)ordn,ordi,anfl,blam,qini,afun,Qm4,Qm5

      ifun = nint(afun)

      icn=0

    5 READ(Nini,'(a80)')lin80
      IF(lin80(1:5).EQ.' Coef')icn=1
      IF(icn.EQ.0) GOTO 5

      READ(Nini,'(a80)')lin80
      DO 10 ifl=2,-4,-1
   10 READ(Nini,'(6f12.3)')(bp(iex,ifl),iex=0,5)

      RETURN
      END

      SUBROUTINE ALFSET (QS, ALFS)
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      EXTERNAL RTALF
      COMMON / RTALFC / ALFST, JORD, NEFF
      DATA ALAM, BLAM, ERR / 0.01, 10.0, 0.02 /
      QST   = QS
      ALFST = ALFS
      CALL PARQCD (2, 'ORDR', ORDR, IR1)
      JORD  = ORDR
      NEFF = NFL(QS)
      EFLLN  = QZBRNT (RTALF, ALAM, BLAM, ERR, IR2)
      EFFLAM = QS / EXP (EFLLN)
      CALL SETL1 (NEFF, EFFLAM)
      END
      FUNCTION ALAMF(N)
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      COMMON / CWZPRM / ALAM(0:9), AMHAT(0:9), AMN, NHQ
      COMMON / QCDPAR / AL, NF, NORDER, SET
      COMMON / IOUNIT / NIN, NOUT, NWRT
      LOGICAL SET
      IF (.NOT.SET) CALL LAMCWZ
      IF ((N.LT.0) .OR. (N.GT.9)) THEN
         WRITE (NOUT, *) ' N IS OUT OF RANGE IN ALAMF'
         ALAMF=0.
      ELSE
         ALAMF = ALAM(MAX(N, NF-NHQ))
      ENDIF
      RETURN
      END

      FUNCTION ALPI (AMU)
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      COMMON / IOUNIT / NIN, NOUT, NWRT
      COMMON / CWZPRM / ALAM(0:9), AMHAT(0:9), AMN, NHQ
      COMMON / QCDPAR / AL, NF, NORDER, SET
      LOGICAL SET
      PARAMETER (D0 = 0.D0, D1 = 1.D0, BIG = 1.0D15)
      DATA IW1, IW2 / 2*0 /
      IF(.NOT.SET) CALL LAMCWZ
      NEFF = NFL(AMU)
      ALM  = ALAM(NEFF)
      ALPI = ALPQCD (NORDER, NEFF, AMU/ALM, IRT)
      IF (IRT .EQ. 1) THEN
         CALL QWARN (IW1, NWRT, 'AMU < ALAM in ALPI', 'MU', AMU,
     >              ALM, BIG, 1)
         WRITE (NWRT, '(A,I4,F15.3)') 'NEFF, LAMDA = ', NEFF, ALM
      ELSEIF (IRT .EQ. 2) THEN
         CALL QWARN (IW2, NWRT, 'ALPI > 1; Be aware!', 'ALPI', ALPI,
     >             D0, D1, 0)
         WRITE (NWRT, '(A,I4,2F15.3)') 'NF, LAM, MU= ', NEFF, ALM, AMU
      ENDIF
      RETURN
      END
      SUBROUTINE CNVL1 (IRDR, JRDR, NF, VLAM)
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      EXTERNAL ZCNVLM
      COMMON / LAMCNV / AMU, ULAM, NFL, IRD, JRD
      COMMON / IOUNIT / NIN, NOUT, NWRT
      DATA ALM, BLM, ERR, AMUMIN / 0.001, 2.0, 0.02, 1.5 /
      IRD = IRDR
      JRD = JRDR
      ULAM = VLAM
      CALL PARQCD(2, 'NFL', ANF, IRT)
      NTL = NFLTOT()
      IF (NF .GT. NTL) THEN
c         WRITE (NOUT, *) ' NF .GT. NTOTAL in CNVL1; set NF = NTOTAL'
c         WRITE (NOUT, *) ' NF, NTOTAL = ', NF, NTL
         NF = NTL
      ENDIF
      NFL = NF
      AMU = AMHATF(NF)
      AMU = MAX (AMU, AMUMIN)
      VLM1 = QZBRNT (ZCNVLM, ALM, BLM, ERR, IR1)
      IF (NF .LT. NTL) THEN
        AMU = AMHATF(NF+1)
        AMU = MAX (AMU, AMUMIN)
        VLM2 = QZBRNT(ZCNVLM, ALM, BLM, ERR, IR2)
      ELSE
        VLM2 = VLM1
      ENDIF
      VLAM = (VLM1 + VLM2) / 2
      RETURN
      END
      SUBROUTINE SETL1  (NEF, VLAM)
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      LOGICAL SET
      COMMON / CWZPRM / ALAM(0:9), AMHAT(0:9), AMN, NHQ
      COMMON / QCDPAR / AL, NF, NORDER, SET
      COMMON / COMQMS / QMS(9)
      COMMON / IOUNIT / NIN, NOUT, NWRT
      IF (NEF .LT. 0 .OR. NEF .GT. NF) THEN
c        WRITE(NOUT,*)'NEF out of range in SETL1, NEF, NF =',NEF,NF
        STOP
      ENDIF
      AMHAT(0) = 0.
      DO 5 N = 1, NF
         AMHAT(N) = QMS(N)
    5    CONTINUE
      ALAM(NEF) = VLAM
      DO 10 N = NEF, 1, -1
         CALL TRNLAM(NORDER, N, -1, IR1)
   10    CONTINUE
      DO 20 N = NEF, NF-1
         CALL TRNLAM(NORDER, N, 1, IR1)
   20    CONTINUE
      DO 30, N = NF, 1, -1
         IF ((ALAM(N) .GE. 0.7 * AMHAT(N))
     >       .OR. (ALAM(N-1) .GE. 0.7 * AMHAT(N)))THEN
            NHQ = NF - N
            GOTO 40
            ENDIF
   30    CONTINUE
      NHQ = NF
   40 CONTINUE
      DO 50, N = NF-NHQ, 1, -1
         AMHAT(N) = 0
         ALAM(N-1) = ALAM(N)
   50    CONTINUE
      AMN = ALAM(NF)
      DO 60, N = 0, NF-1
         IF (ALAM(N) .GT. AMN)  AMN = ALAM(N)
   60    CONTINUE
      AMN = AMN * 1.0001
      AL = ALAM(NF)
      SET = .TRUE.
      RETURN
      END
      SUBROUTINE LAMCWZ
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      COMMON / QCDPAR / AL, NF, NORDER, SET
      LOGICAL SET
      CALL SETL1 (NF, AL)
      END
      FUNCTION ALPQCD (IRDR, NF, RML, IRT)
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      PARAMETER (D0 = 0.D0, D1 = 1.D0, BIG = 1.0D15)
      PARAMETER (CG = 3.0, TR = 0.5, CF = 4.0/3.0)
      COMMON / IOUNIT / NIN, NOUT, NWRT
      DATA IW1 / 0/
      IRT = 0
      IF (IRDR .LT. 1 .OR. IRDR .GT. 2) THEN
c        WRITE(NOUT, *) 'Order parameter out of range in ALPQCD;
c     >  IRDR = ', IRDR
        IRT = 3
        STOP
      ENDIF
      B0 = (11.* CG  - 2.* NF) / 3.
      B1 = (34.* CG**2 - 10.* CG * NF - 6.* CF * NF) / 3.
      RM2 = RML ** 2
      IF (RM2 .LE. 1.) THEN
         IRT = 1
         ALPQCD = 99
         RETURN
      ENDIF
      ALN = LOG (RM2)
      AL = 4./ B0 / ALN
      IF (IRDR .GE. 2) AL = AL * (1.- B1 * LOG(ALN) / ALN / B0**2)
      IF (AL .GE. 1.) THEN
         IRT = 2
      ENDIF
      ALPQCD = AL
      RETURN
      END
      Function SetPdf ()
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      External DatPDF
      SetPDF = 0.
      Return
      END
      SUBROUTINE EVOLVE (FINI, IRET)
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      LOGICAL LSTX
      CHARACTER*(*) HEADER
      PARAMETER (MXX = 105, MXQ = 25, MXF = 6)
      PARAMETER (MXPN = MXF * 2 + 2)
      PARAMETER (MXQX= MXQ * MXX,   MXPQX = MXQX * MXPN)
      PARAMETER (M1=-3, M2=3, NDG=3, NDH=NDG+1, L1=M1-1, L2=M2+NDG-2)
      COMMON / IOUNIT / NIN, NOUT, NWRT
      COMMON / XXARAY / XCR, XMIN, XV(0:MXX), LSTX, NX
      COMMON / QARAY1 / QINI,QMAX, QV(0:MXQ),TV(0:MXQ), NT,JT,NG
      COMMON / QARAY2 / TLN(MXF), DTN(MXF), NTL(MXF), NTN(MXF)
      COMMON / EVLPAC / AL, IKNL, IPD0, IHDN, KF
      COMMON / PEVLDT / UPD(MXPQX)
      COMMON / VARIBX / XA(MXX, L1:L2), ELY(MXX), DXTZ(MXX)
      COMMON / VARBAB / GB(NDG, NDH, MXX), H(NDH, MXX, M1:M2)
      DIMENSION QRKP(MXF)
      DIMENSION JI(-MXF : MXF+1)
      EXTERNAL NSRHSP, NSRHSM, FINI
      DATA D0 / 0.0 /
   11 IRET = 0
      IF (IHDN .LE. 4) THEN
        MXVAL = 2
      ElseIF (IHDN .LE. 6) THEN
        MXVAL = 3
      EndIf
      IF (.NOT. LSTX) CALL XARRAY
      DLX = 1D0 / NX
      J10 = NX / 10
      CALL PARPDF (2, 'ALAM', AL, IR)
      CALL QARRAY (NINI, NFMX)
      NFSN = NFMX + 1
      DO 101 IFLV = -NFMX, NFMX+1
        JFL = NFMX + IFLV
        JI(IFLV) = JFL * (NT+1) * (NX+1)
  101 CONTINUE
    3 DO 31 IZ = 1, NX
        UPD(JI(0)+IZ+1) = FINI (0, XV(IZ))
        UPD(JI(NFSN)+IZ+1) = 0
        IF (NFMX .EQ. 0) GOTO 31
        DO 331 IFLV = 1, NINI
          A = FINI ( IFLV, XV(IZ))
          B = FINI (-IFLV, XV(IZ))
          QRKP (IFLV) = A + B
          UPD(JI(NFSN)+IZ+1) = UPD(JI(NFSN)+IZ+1) + QRKP (IFLV)
          UPD(JI(-IFLV)+IZ+1) = A - B
  331   CONTINUE
        DO 332 IFLV = 1, NINI
           UPD(JI( IFLV)+IZ+1) = QRKP(IFLV) - UPD(JI(NFSN)+IZ+1)/NINI
  332   CONTINUE
   31 CONTINUE
      DO 21 NEFF = NINI, NFMX
          IF (IKNL .EQ. 2) CALL STUPKL (NEFF)
          ICNT = NEFF - NINI + 1
          IF (NTN(ICNT) .EQ. 0) GOTO 21
          NITR = NTN (ICNT)
          DT   = DTN (ICNT)
          TIN  = TLN (ICNT)
          CALL SNEVL (IKNL, NX, NITR, JT, DT, TIN, NEFF,
     >    UPD(JI(NFSN)+2), UPD(JI(0)+2), UPD(JI(NFSN)+1), UPD(JI(0)+1))
          IF (NEFF .EQ. 0) GOTO 88
    5     DO 333 IFLV = 1, NEFF
           CALL NSEVL (NSRHSP, IKNL, NX, NITR, JT, DT, TIN, NEFF,
     >                 UPD(JI( IFLV)+2), UPD(JI( IFLV)+1))
           IF (IFLV .LE. MXVAL)
     >     CALL NSEVL (NSRHSM, IKNL, NX, NITR, JT, DT, TIN, NEFF,
     >                 UPD(JI(-IFLV)+2), UPD(JI(-IFLV)+1))
           DO 55 IS = 0, NITR
           DO 56 IX = 0, NX
             TP = UPD (IS*(NX+1) + IX + 1 + JI( IFLV))
             TS = UPD (IS*(NX+1) + IX + 1 + JI( NFSN)) / NEFF
             TP = TP + TS
             IF (IKNL .GT. 0) TP = MAX (TP, D0)
             IF (IFLV .LE. MXVAL) THEN
                TM = UPD (IS*(NX+1) + IX + 1 + JI(-IFLV))
                IF (IKNL .GT. 0) THEN
                  TM = MAX (TM, D0)
                  TP = MAX (TP, TM)
                EndIf
             Else
                TM = 0.
             EndIf
             UPD (JI( IFLV) + IS*(NX+1) + IX + 1) = (TP + TM)/2.
             UPD (JI(-IFLV) + IS*(NX+1) + IX + 1) = (TP - TM)/2.
   56      CONTINUE
   55      CONTINUE
333      CONTINUE
        DO 334 IFLV = NEFF + 1, NFMX
          DO 57 IS = 0, NITR
          DO 58 IX = 0, NX
            UPD(JI( IFLV) + IS*(NX+1) + IX + 1) = 0
            UPD(JI(-IFLV) + IS*(NX+1) + IX + 1) = 0
   58     CONTINUE
   57     CONTINUE
  334   CONTINUE
   88   CONTINUE
        IF (NFMX .EQ. NEFF) GOTO 21
        DO 335 IFLV = -NFMX, NFMX+1
           JI(IFLV) = JI(IFLV) + NITR * (NX+1)
  335   CONTINUE
        CALL HQRK (NX, TT, NEFF+1, UPD(JI(0)+2), UPD(JI(NEFF+1)+2))
        DO 32 IZ = 1, NX
         QRKP (NEFF+1) = 2. * UPD(JI( NEFF+1) + IZ + 1)
         UPD (JI(NFSN)+IZ+1) = UPD (JI(NFSN)+IZ+1)  + QRKP (NEFF+1)
         VS00 =  UPD (JI(NFSN)+IZ+1) / (NEFF+1)
         UPD(JI( NEFF+1) + IZ + 1) = QRKP(NEFF+1) - VS00
         DO 321 IFL = 1, NEFF
           A = UPD(JI( IFL)+IZ+1)
           B = UPD(JI(-IFL)+IZ+1)
           QRKP(IFL) = A + B
           UPD(JI( IFL)+IZ+1) = QRKP(IFL) - VS00
           IF (IFL .LE. MXVAL)  UPD(JI(-IFL)+IZ+1) = A - B
  321    CONTINUE
   32   CONTINUE
   21 CONTINUE
      GOTO 900
      ENTRY EVLWT (NU, HEADER, IRR)
      CALL PARPDF (2, 'ALAM', AL, IR)
      CALL PARPDF (2, 'NFL',  FL, IR)
      CALL PARPDF (2, 'ORDER', DR, IR)
      CALL PARPDF (2, 'M1', AM1, IR)
      CALL PARPDF (2, 'M2', AM2, IR)
      CALL PARPDF (2, 'M3', AM3, IR)
      CALL PARPDF (2, 'M4', AM4, IR)
      CALL PARPDF (2, 'M5', AM5, IR)
      CALL PARPDF (2, 'M6  ', AM6, IR)
      WRITE (NU, IOSTAT=IR1) 
     >     HEADER, AL, FL, DR, AM1, AM2, AM3, AM4, AM5, AM6,
     >     IPD0, IHDN, IKNL, KF, QINI, QMAX, NT, JT, NG, XMIN, XCR, NX,
     >     (TLN(I), NTL(I), DTN(I), NTN(I), I =1, NG), NTL(NG+1),
     >     (XV(I), I =1, NX), (QV(I), I =0, NT), (TV(I), I =0, NT)
      CALL WTUPD (UPD, (NX+1)*(NT+1)*KF+1, NU, IR2)
      IRR = IR1 + IR2
c      IF (IRR .NE. 0) PRINT *, 'Write error in EVLWT'
      RETURN
      ENTRY EVLRD (NU, HEADER, IRR)
      READ (NU, IOSTAT=IR1)
     >     HEADER, AL, FL, DR, AM1, AM2, AM3, AM4, AM5, AM6,
     >     IPD0, IHDN, IKNL, KF, QINI, QMAX, NT, JT, NG, XMIN, XCR, NX,
     >     (TLN(I), NTL(I), DTN(I), NTN(I), I =1, NG), NTL(NG+1),
     >     (XV(I), I =1, NX), (QV(I), I =0, NT), (TV(I), I =0, NT)
      CALL RDUPD (UPD, (NX+1)*(NT+1)*KF+1, NU, IR2)
      IRR = IR1 + IR2
      CLOSE (NU)
c      IF (IRR .NE. 0) PRINT *, 'Read error in EVLRD'
      CALL PARPDF (1, 'ALAM', AL, IR)
      CALL PARPDF (1, 'NFL',  FL, IR)
      CALL PARPDF (1, 'ORDER', DR, IR)
      CALL PARPDF (1, 'M1', AM1, IR)
      CALL PARPDF (1, 'M2', AM2, IR)
      CALL PARPDF (1, 'M3', AM3, IR)
      CALL PARPDF (1, 'M4', AM4, IR)
      CALL PARPDF (1, 'M5', AM5, IR)
      CALL PARPDF (1, 'M6  ', AM6, IR)
c      PRINT '(/A/1X,A/)', ' Parameters from this data file are:',HEADER
      CALL PARPDF(4, 'ALL', DBLE(NOUT), IR)
  900 CONTINUE
      RETURN
      END
      SUBROUTINE QWARN (IWRN, NWRT1, MSG, NMVAR, VARIAB,
     >                  VMIN, VMAX, IACT)
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      COMMON /IOUNIT/ NIN, NOUT, NWRT
      PARAMETER (D0=0D0, D1=1D0, D2=2D0, D3=3D0, D4=4D0, D10=1D1)
      CHARACTER*(*) MSG, NMVAR
      IW = IWRN
      VR = VARIAB
      WRITE (NWRT1,'(I5, 3X,A/ 1X,A,'' = '',1PD16.7)') IW, MSG,
     >                  NMVAR, VR
      IF  (IW .EQ. 0) THEN
c         WRITE (NOUT, '(1X, A/1X, A,'' = '', 1PD16.7/A,I4)')
c     >      MSG, NMVAR, VR,
c     >      ' Complete set of warning messages on file unit #', NWRT1
         IF (IACT .EQ. 1) THEN
c         WRITE (NOUT,'(1X,A/2(1PD15.3))')'The limits are: ', VMIN,VMAX
         WRITE (NWRT1,'(1X,A/2(1PD15.3))')'The limits are: ', VMIN,VMAX
         ENDIF
      ENDIF
      IWRN = IW + 1
      RETURN
      END
      FUNCTION NAMQCD(NNAME)
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      CHARACTER NNAME*(*), NAME*8
      COMMON /IOUNIT/ NIN, NOUT, NWRT
      COMMON /QCDPAR/ AL, NF, NORDER, SET
      LOGICAL SET
      CHARACTER ONECH*(1)
      ONECH = '0'
      IASC0 = ICHAR(ONECH)
      NAME = NNAME
      NAMQCD=0
      IF ( (NAME .EQ. 'ALAM') .OR. (NAME .EQ. 'LAMB') .OR.
     1        (NAME .EQ. 'LAM') .OR. (NAME .EQ. 'LAMBDA') )
     2             NAMQCD=1
      IF ( (NAME .EQ. 'NFL') .OR. (NAME(1:3) .EQ. '#FL') .OR.
     1        (NAME .EQ. '# FL') )
     2             NAMQCD=2
      DO 10 I=1, 9
         IF (NAME .EQ. 'M'//CHAR(I+IASC0))
     1             NAMQCD=I+2
10       CONTINUE
      DO 20 I= 0, NF
         IF (NAME .EQ. 'LAM'//CHAR(I+IASC0))
     1             NAMQCD=I+13
20       CONTINUE
      IF (NAME(:3).EQ.'ORD' .OR. NAME(:3).EQ.'NRD') NAMQCD = 24
      RETURN
      END
      FUNCTION NFLTOT()
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      COMMON /QCDPAR/ AL, NF, NORDER, SET
      LOGICAL SET
      NFLTOT=NF
      RETURN
      END
      FUNCTION ZCNVLM (VLAM)
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      COMMON / LAMCNV / AMU, ULAM, NFL, IRD, JRD
      ZCNVLM= ALPQCD (IRD,NFL,AMU/ULAM,I) - ALPQCD (JRD,NFL,AMU/VLAM,I)
      END
      FUNCTION QZBRNT(FUNC, X1, X2, TOLIN, IRT)
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      COMMON /IOUNIT/ NIN, NOUT, NWRT
      PARAMETER (ITMAX = 1000, EPS = 3.E-12)
      external func
      TOL = ABS(TOLIN)
      A=X1
      B=X2
      FA=FUNC(A)
      FB=FUNC(B)
      IF(FB*FA.GT.0.)  THEN
        WRITE (NOUT, *) 'Root must be bracketed for QZBRNT.'
        IRT = 1
      ENDIF
      FC=FB
      DO 11 ITER=1,ITMAX
        IF(FB*FC.GT.0.) THEN
          C=A
          FC=FA
          D=B-A
          E=D
        ENDIF
        IF(ABS(FC).LT.ABS(FB)) THEN
          A=B
          B=C
          C=A
          FA=FB
          FB=FC
          FC=FA
        ENDIF
        TOL1=2.*EPS*ABS(B)+0.5*TOL
        XM=.5*(C-B)
        IF(ABS(XM).LE.TOL1 .OR. FB.EQ.0.)THEN
          QZBRNT=B
          RETURN
        ENDIF
        IF(ABS(E).GE.TOL1 .AND. ABS(FA).GT.ABS(FB)) THEN
          S=FB/FA
          IF(A.EQ.C) THEN
            P=2.*XM*S
            Q=1.-S
          ELSE
            Q=FA/FC
            R=FB/FC
            P=S*(2.*XM*Q*(Q-R)-(B-A)*(R-1.))
            Q=(Q-1.)*(R-1.)*(S-1.)
          ENDIF
          IF(P.GT.0.) Q=-Q
          P=ABS(P)
          IF(2.*P .LT. MIN(3.*XM*Q-ABS(TOL1*Q),ABS(E*Q))) THEN
            E=D
            D=P/Q
          ELSE
            D=XM
            E=D
          ENDIF
        ELSE
          D=XM
          E=D
        ENDIF
        A=B
        FA=FB
        IF(ABS(D) .GT. TOL1) THEN
          B=B+D
        ELSE
          B=B+SIGN(TOL1,XM)
        ENDIF
        FB=FUNC(B)
11    CONTINUE
      WRITE (NOUT, *) 'QZBRNT exceeding maximum iterations.'
      IRT = 2
      QZBRNT=B
      RETURN
      END
      SUBROUTINE TRNLAM (IRDR, NF, IACT, IRT)
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      COMMON / IOUNIT / NIN, NOUT, NWRT
      COMMON / CWZPRM / ALAM(0:9), AMHAT(0:9), AMN, NHQ
      COMMON / TRNCOM / VMULM, JRDR, N, N1
      EXTERNAL ZBRLAM
      DATA ALM0, BLM0, RERR / 0.01, 10.0, 0.0001 /
      DATA IW1, IW2, IW3, IW4, IR1, SML / 4*0, 0, 1.E-5 /
      IRT = 0
      N = NF
      JRDR = IRDR
      JACT = IACT
      VLAM = ALAM(N)
      IF (JACT .GT. 0) THEN
         N1 = N + 1
         THMS = AMHAT(N1)
         ALM = LOG (THMS/VLAM)
         BLM = BLM0
      ELSE
         N1 = N -1
         THMS = AMHAT(N)
         ALM = ALM0
         THMS = MAX (THMS, SML)
         BLM = LOG (THMS/VLAM)
      ENDIF
      IF (VLAM .GE. 0.7 * THMS) THEN
         IF (JACT . EQ. 1) THEN
            AMHAT(N1) = 0
         ELSE
            AMHAT(N) = 0
         ENDIF
         IRT = 4
         ALAM(N1) = VLAM
         RETURN
      ENDIF
      IF (ALM .GE. BLM) THEN
         WRITE (NOUT, *) 'TRNLAM has ALM >= BLM: ', ALM, BLM
         WRITE (NOUT, *) 'I do not know how to continue'
         STOP
         ENDIF
      VMULM = THMS/VLAM
      ERR = RERR * LOG (VMULM)
      WLLN = QZBRNT (ZBRLAM, ALM, BLM, ERR, IR1)
      ALAM(N1) = THMS / EXP (WLLN)
      IF (IR1 .NE. 0) THEN
         WRITE (NOUT, *) 'QZBRNT failed to find VLAM in TRNLAM; ',
     >        'NF, VLAM =', NF, VLAM
         WRITE (NOUT, *) 'I do not know how to continue'
        STOP
      ENDIF
      RETURN
      END
      FUNCTION RTALF (EFLLN)
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      PARAMETER (PI = 3.141592653589)
      COMMON / RTALFC / ALFST, JORD, NEFF
      EFMULM = EXP (EFLLN)
      TEM1 = PI / ALFST
      TEM2 = 1. / ALPQCD (JORD, NEFF, EFMULM, I)
      RTALF = TEM1 - TEM2
      END
      FUNCTION ZBRLAM (WLLN)
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      COMMON / TRNCOM / VMULM, JRDR, N, N1
      WMULM = EXP (WLLN)
      TEM1 = 1./ ALPQCD(JRDR, N1, WMULM, I)
      TEM2 = 1./ ALPQCD(JRDR, N,  VMULM, I)
      ZBRLAM = TEM1 - TEM2
      END
      SUBROUTINE EVLPAR (IACT, NAME, VALUE, IRET)
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      CHARACTER*(*) NAME
      COMMON / IOUNIT / NIN, NOUT, NWRT
      IRET = 1
      IF     (IACT .EQ. 0) THEN
              WRITE ( NINT (VALUE) , 101)
  101         FORMAT (/ ' Initiation parameters:   Qini, Ipd0, Ihdn ' /
     >                  ' Maximum Q, Order of Alpha:     Qmax, IKNL ' /
     >                  ' X- mesh parameters   :   Xmin, Xcr,   Nx   ' /
     >                  ' LnQ-mesh parameters  :         Nt,   Jt   ' /
     >                  ' # of parton flavors  :         Kf         ' /)
              IRET = 4
      ElseIF (IACT .EQ. 1) THEN
              CALL EVLSET (NAME, VALUE, IRET)
      ElseIF (IACT .EQ. 2) THEN
              CALL EVLGET (NAME, VALUE, IRET)
      ElseIF (IACT .EQ. 5) THEN
              CALL EVLGT1 (NAME, VALUE, IRET)
      ElseIF (IACT .EQ. 3) THEN
              CALL EVLIN
              IRET = 4
      ElseIF (IACT .EQ. 4) THEN
              CALL EVLOUT ( NINT (VALUE) )
              IRET = 4
      ElseIF (IACT .EQ. 6) THEN
              CALL EVLOT1 ( NINT (VALUE) )
              IRET = 4
      Else
              IRET = 3
      EndIf
      RETURN
      END
      SUBROUTINE XARRAY
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      LOGICAL LSTX
      PARAMETER (D0 = 0.0, D10=10.0)
      PARAMETER (MXX = 105, MXQ = 25, MXF = 6)
      PARAMETER (MXPN = MXF * 2 + 2)
      PARAMETER (MXQX= MXQ * MXX,   MXPQX = MXQX * MXPN)
      PARAMETER (M1=-3, M2=3, NDG=3, NDH=NDG+1, L1=M1-1, L2=M2+NDG-2)
      COMMON / IOUNIT / NIN, NOUT, NWRT
      COMMON / VARIBX / XA(MXX, L1:L2), ELY(MXX), DXTZ(MXX)
      COMMON / VARBAB / GB(NDG, NDH, MXX), H(NDH, MXX, M1:M2)
      COMMON / HINTEC / GH(NDG, MXX)
      COMMON / XXARAY / XCR, XMIN, XV(0:MXX), LSTX, NX
      COMMON / XYARAY / ZZ(MXX, MXX)
      DIMENSION G1(NDG,NDH), G2(NDG,NDH), A(NDG)
      DATA F12, F22, F32 / 1D0, 1D0, 1D0 /
      DATA (G1(I,NDH), G2(I,1), I=1,NDG) / 0.0,0.0,0.0,0.0,0.0,0.0 /
      DATA NA, IA, PUNY / 3, 5, 1D-30 /
      XV(0) = 0D0
      DZ = 1D0 / (NX-1)
      DO 10 I = 1, NX - 1
         Z = DZ * (I-1)
         X = XFRMZ (Z)
         DXTZ(I) = DXDZ(Z) / X
         XV (I)  = X
         XA(I, 1) = X
         XA(I, 0) = LOG (X)
         DO 20 L = L1, L2
          IF (L .NE. 0 .AND. L .NE. 1)  XA(I, L) = X ** L
   20    CONTINUE
   10 CONTINUE
         XV(NX) = 1D0
         DXTZ(NX) = DXDZ(1.D0)
         DO 21 L = L1, L2
            XA (NX, L) = 1D0
   21    CONTINUE
         XA (NX, 0) = 0D0
      DO 11 I = 1, NX-1
         ELY(I) = LOG(1D0 - XV(I))
   11 CONTINUE
       ELY(NX) = 3D0* ELY(NX-1) - 3D0* ELY(NX-2) + ELY(NX-3)
      DO 17 IX = 1, NX
      ZZ (IX, IX) = 1.
      DO 17 IY = IX+1, NX
         XY = XV(IX) / XV(IY)
         ZZ (IX, IY) = ZFRMX (XY)
         ZZ (NX-IX+1, NX-IY+1) = XY
   17 CONTINUE
      DO 30 I = 1, NX-1
      IF (I .NE. NX-1) THEN
        F11 = 1D0/XV(I)
        F21 = 1D0/XV(I+1)
        F31 = 1D0/XV(I+2)
        F13 = XV(I)
        F23 = XV(I+1)
        F33 = XV(I+2)
        DET = F11*F22*F33 + F21*F32*F13 + F31*F12*F23
     >      - F31*F22*F13 - F21*F12*F33 - F11*F32*F23
        IF (ABS(DET) .LT. PUNY) THEN
        CALL WARNR(IWRN, NWRT, 'Determinant close to zero;
     > will be arbitrarily set to:', 'DET', PUNY, D0, D0, 0)
        DET = PUNY
        EndIf
        G2(1,2) = (F22*F33 - F23*F32) / DET
        G2(1,3) = (F32*F13 - F33*F12) / DET
        G2(1,4) = (F12*F23 - F13*F22) / DET
        G2(2,2) = (F23*F31 - F21*F33) / DET
        G2(2,3) = (F33*F11 - F31*F13) / DET
        G2(2,4) = (F13*F21 - F11*F23) / DET
        G2(3,2) = (F21*F32 - F22*F31) / DET
        G2(3,3) = (F31*F12 - F32*F11) / DET
        G2(3,4) = (F11*F22 - F12*F21) / DET
        B2 = LOG (XV(I+2)/XV(I))
        B3 = XV(I) * (B2 - 1.) + XV(I+2)
        GH (1,I) = B2 * G2 (2,2) + B3 * G2 (3,2)
        GH (2,I) = B2 * G2 (2,3) + B3 * G2 (3,3)
        GH (3,I) = B2 * G2 (2,4) + B3 * G2 (3,4)
      EndIf
        DO 51 J = 1, NDH
           DO 52 L = 1, NDG
              IF     (I .EQ. 1) THEN
                 GB(L,J,I) = G2(L,J)
              ElseIF (I .EQ. NX-1) THEN
                 GB(L,J,I) = G1(L,J)
              Else
                 GB(L,J,I) = (G1(L,J) + G2(L,J)) / 2D0
              EndIf
   52      CONTINUE
   51   CONTINUE
        DO 35 MM = M1, M2
           DO 40 K = 1, NDG
             KK = K + MM - 2
             IF (KK .EQ. 0) THEN
               A(K) = XA(I+1, 0) - XA(I, 0)
             Else
               A(K) = (XA(I+1, KK) - XA(I, KK)) / DBLE(KK)
             EndIf
   40      CONTINUE
           DO 41 J = 1, NDH
             TEM = 0
             DO 43 L = 1, NDG
               TEM = TEM + A(L) * GB(L,J,I)
   43        CONTINUE
             H(J,I,MM) = TEM
   41      CONTINUE
   35   CONTINUE
      DO 42 J = 1, NDG
        DO 44 L = 1, NDG
           G1(L,J) = G2(L,J+1)
   44 CONTINUE
   42 CONTINUE
   30 CONTINUE
      LSTX = .TRUE.
      RETURN
      END
      SUBROUTINE QARRAY (NINI, NFMX)
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      PARAMETER (MXX = 105, MXQ = 25, MXF = 6)
      PARAMETER (MXPN = MXF * 2 + 2)
      PARAMETER (MXQX= MXQ * MXX,   MXPQX = MXQX * MXPN)
      COMMON / QARAY1 / QINI,QMAX, QV(0:MXQ),TV(0:MXQ), NT,JT,NG
      COMMON / QARAY2 / TLN(MXF), DTN(MXF), NTL(MXF), NTN(MXF)
      COMMON / EVLPAC / AL, IKNL, IPD0, IHDN, KF
      NCNT = 0
      IF (NT .GE. mxq) NT = mxq - 1
      S = LOG(QINI/AL)
      TINI = LOG(S)
      S = LOG(QMAX/AL)
      TMAX = LOG(S)
    1 DT0 = (TMAX - TINI) / float(NT)
      NINI = NFL(QINI)
      NFMX = NFL(QMAX)
      KF = 2 * NFMX + 2
      NG = NFMX - NINI + 1
      QIN  = QINI
      QOUT = QINI
      S = LOG(QIN/AL)
      TIN  = LOG(S)
      TLN(1) = TIN
      NTL(1)  = 0
      QV(0) = QINI
      TV(0) = Tin
      DO 20 NEFF = NINI, NFMX
        ICNT = NEFF - NINI + 1
        IF (NEFF .LT. NFMX) THEN
          THRN = AMHATF (NEFF + 1)
          QOUN = MIN (QMAX, THRN)
        Else
          QOUN = QMAX
        EndIf
        IF (QOUN-QOUT .LE. 0.0001) THEN
          DT   = 0
          NITR = 0
        Else
          QOUT = QOUN
          S = LOG(QOUT/AL)
          TOUT = LOG(S)
          TEM = TOUT - TIN
          NITR = INT (TEM / DT0) + 1
          DT  = TEM / NITR
        EndIf
        DTN (ICNT) = DT
        NTN (ICNT) = NITR
        TLN (ICNT) = TIN
        NTL (ICNT+1) = NTL(ICNT) + NITR
        IF (NITR .NE. 0) THEN
        DO 205 I = 1, NITR
           TV (NTL(ICNT)+I) = TIN + DT * I
           S = EXP (TV(NTL(ICNT)+I))
           QV (NTL(ICNT)+I) = AL * EXP (S)
  205   CONTINUE
        EndIf
        QIN = QOUT
        TIN = TOUT
   20 CONTINUE
      NCNT = NCNT + 1
      NTP = NTL (NG + 1)
      ND  = NTP - NT
      IF (NTP .GE. MXQ) THEN
         NT = MXQ - ND - NCNT
         GOTO 1
      EndIf
      NT = NTP
      RETURN
      END
      SUBROUTINE STUPKL (NFL)
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      LOGICAL LSTX
      PARAMETER (D0=0D0, D1=1D0, D2=2D0, D3=3D0, D4=4D0, D10=1D1)
      PARAMETER (MXX = 105, MXQ = 25, MXF = 6)
      PARAMETER (MX = 3)
      PARAMETER (M1=-3, M2=3, NDG=3, NDH=NDG+1, L1=M1-1, L2=M2+NDG-2)
      COMMON / IOUNIT / NIN, NOUT, NWRT
      COMMON / XXARAY / XCR, XMIN, XV(0:MXX), LSTX, NX
      COMMON / XYARAY / ZZ (MXX, MXX)
      COMMON / VARIBX / XA(MXX, L1:L2), ELY(MXX), DXTZ(MXX)
      COMMON / KRN1ST / FF1(0:MXX), FG1(0:MXX), GF1(0:MXX), GG1(0:MXX),
     >                  FF2(0:MXX), FG2(0:MXX), GF2(0:MXX), GG2(0:MXX),
     >                  PNSP(0:MXX), PNSM(0:MXX)
      COMMON / KRN2ND / FFG(MXX, MXX), GGF(MXX, MXX), PNS(MXX, MXX)
      COMMON / KRNL00 / DZ, XL(MX), NNX
      COMMON / KRNL01 / AFF1 (MXX), AFF2 (MXX), AGG1 (MXX), AGG2 (MXX),
     >                  AFG2, AGF2, ANSP (MXX), ANSM (MXX), AQQB
      EXTERNAL PFF1, RGG1, RFF2, RGG2, RGF2, RFG2, TFF1, RFF1, VFF1
      EXTERNAL FNSP, FNSM
      SAVE
      DATA AERR, RERR / 0.0, 0.02 /
      NNX = NX
      DZ = 1./ (NX - 1)
      DO 5 I0 = 1, MX
        XL(I0) = XV(I0)
    5 CONTINUE
      DO 10 I = 1, NX-1
        XZ = XV(I)
      CALL KERNEL (XZ, FF1(I), GF1(I), FG1(I), GG1(I), PNSP(I),
     >          PNSM(I), FF2(I), GF2(I), FG2(I), GG2(I), NFL, IRT)
   10 CONTINUE
      FF1(0) = FF1(1) * 3. - FF1(2) * 3. + FF1(3)
      FG1(0) = FG1(1) * 3. - FG1(2) * 3. + FG1(3)
      GF1(0) = GF1(1) * 3. - GF1(2) * 3. + GF1(3)
      GG1(0) = GG1(1) * 3. - GG1(2) * 3. + GG1(3)
      PNSP(0) = PNSP(1) * 3. - PNSP(2) * 3. + PNSP(3)
      PNSM(0) = PNSM(1) * 3. - PNSM(2) * 3. + PNSM(3)
      FF2(0) = FF2(1) * 3. - FF2(2) * 3. + FF2(3)
      FG2(0) = FG2(1) * 3. - FG2(2) * 3. + FG2(3)
      GF2(0) = GF2(1) * 3. - GF2(2) * 3. + GF2(3)
      GG2(0) = GG2(1) * 3. - GG2(2) * 3. + GG2(3)
      FF1(NX) = FF1(NX-1) * 3. - FF1(NX-2) * 3. + FF1(NX-3)
      FG1(NX) = FG1(NX-1) * 3. - FG1(NX-2) * 3. + FG1(NX-3)
      GF1(NX) = GF1(NX-1) * 3. - GF1(NX-2) * 3. + GF1(NX-3)
      GG1(NX) = GG1(NX-1) * 3. - GG1(NX-2) * 3. + GG1(NX-3)
      PNSM(NX) = PNSM(NX-1) * 3. - PNSM(NX-2) * 3. + PNSM(NX-3)
      PNSP(NX) = PNSP(NX-1) * 3. - PNSP(NX-2) * 3. + PNSP(NX-3)
      FF2(NX) = FF2(NX-1) * 3. - FF2(NX-2) * 3. + FF2(NX-3)
      FG2(NX) = FG2(NX-1) * 3. - FG2(NX-2) * 3. + FG2(NX-3)
      GF2(NX) = GF2(NX-1) * 3. - GF2(NX-2) * 3. + GF2(NX-3)
      GG2(NX) = GG2(NX-1) * 3. - GG2(NX-2) * 3. + GG2(NX-3)
         RER = RERR * 4.
         AFF1 (1) = GAUSS(PFF1, D0, XV(1), AERR, RERR, ER1, IRT)
         DGG1     = NFL / 3.
         TMPG     = GAUSS(RGG1, D0, XV(1), AERR, RERR, ER3, IRT)
         AGG1 (1) = TMPG + DGG1
       ANSM (1) = GAUSS(FNSM, D0, XV(1), AERR, RER, ER2, IRT)
       ANSP (1) = GAUSS(FNSP, D0, XV(1), AERR, RER, ER2, IRT)
         AER = AFF1(1) * RER
         AFF2 (1) = GAUSS(RFF2, D0, XV(1),  AER, RER, ER2, IRT)
         AGF2     = GAUSS(RGF2, D0, XV(1),  AER, RER, ER4, IRT)
         AER = AGG1(1) * RER
         AGG2 (1) = GAUSS(RGG2, D0, XV(1),  AER, RER, ER4, IRT)
         AFG2     = GAUSS(RFG2, D0, XV(1),  AER, RER, ER4, IRT)
      DO 20 I2 = 2, NX-1
      TEM =GAUSS(PFF1,XV(I2-1),XV(I2),AERR,RERR,ER1,IRT)
      AFF1(I2) = TEM + AFF1(I2-1)
      AER = ABS(TEM * RER)
      AGF2    =GAUSS(RGF2,XV(I2-1),XV(I2),AER,RER,ER2,IRT)+AGF2
      AFF2(I2)=GAUSS(RFF2,XV(I2-1),XV(I2),AER,RER,ER2,IRT)+AFF2(I2-1)
      TEM      = GAUSS(RGG1,XV(I2-1),XV(I2),AERR,RERR,ER3,IRT)
      TMPG     = TMPG + TEM
      AGG1(I2) = TMPG + DGG1
      AER = ABS(TEM * RER)
      AFG2    =GAUSS(RFG2,XV(I2-1),XV(I2),AER,RER,ER4,IRT)+AFG2
      AGG2(I2)=GAUSS(RGG2,XV(I2-1),XV(I2),AER,RER,ER4,IRT)+AGG2(I2-1)
      ANSP(I2)=GAUSS(FNSP,XV(I2-1),XV(I2),AERR,RER,ER4,IRT)+ANSP(I2-1)
      ANSM(I2)=GAUSS(FNSM,XV(I2-1),XV(I2),AERR,RER,ER4,IRT)+ANSM(I2-1)
   20 CONTINUE
      ANSP(NX)=GAUSS(FNSP,XV(NX-1),D1,AERR,RER,ERR, IRT) + ANSP(NX-1)
      ANSM(NX)=GAUSS(FNSM,XV(NX-1),D1,AERR,RER,ERR, IRT) + ANSM(NX-1)
      AQQB = ANSP(NX) - ANSM(NX)
      AGF2 = GAUSS(RGF2, XV(NX-1), D1, AER, RER, ERR, IRT) + AGF2
      AFG2 = GAUSS(RFG2, XV(NX-1), D1, AER, RER, ERR, IRT) + AFG2
      DO 21 IX = 1, NX-1
        X = XV(IX)
        NP = NX - IX + 1
        IS = NP
        XG2 = (LOG(1./(1.-X)) + 1.) ** 2
        FFG (IS, IS) = FG2(NX) * DXTZ(I) * XG2
      GGF (IS, IS) = GF2(NX) * DXTZ(I) * XG2
      PNS (IS, IS) =PNSM(NX) * DXTZ(I)
        DO 31 KZ = 2, NP
          IY = IX + KZ - 1
          IT = NX - IY + 1
          XY = X / XV(IY)
          XM1 = 1.- XY
          XG2 = (LOG(1./XM1) + 1.) ** 2
          Z  = ZZ (IX, IY)
          TZ = (Z + DZ) / DZ
          IZ = TZ
          IZ = MAX (IZ, 0)
          IZ = MIN (IZ, NX-1)
          DT = TZ - IZ
          TEM = (FF2(IZ) * (1.- DT) + FF2(IZ+1) * DT) / XM1 / XY
          FFG (IX, IY) = TEM * DXTZ(IY)
          TEM = (FG2(IZ) * (1.- DT) + FG2(IZ+1) * DT) * XG2 / XY
          FFG (IS, IT) = TEM * DXTZ(IY)
          TEM = (GF2(IZ) * (1.- DT) + GF2(IZ+1) * DT) * XG2 / XY
        GGF (IS, IT) = TEM * DXTZ(IY)
          TEM = (GG2(IZ) * (1.- DT) + GG2(IZ+1) * DT) / XM1 / XY
        GGF (IX, IY) = TEM * DXTZ(IY)
        TEM = (PNSP(IZ) * (1.- DT) + PNSP(IZ+1) * DT) / XM1
        PNS (IX, IY) = TEM * DXTZ(IY)
        TEM = (PNSM(IZ) * (1.- DT) + PNSM(IZ+1) * DT) / XM1
        PNS (IS, IT) = TEM * DXTZ(IY)
   31   CONTINUE
   21 CONTINUE
      RETURN
      END
      FUNCTION FINTRP (FF,  X0, DX, NX,  XV,  ERR, IR)
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      PARAMETER (D0=0D0, D1=1D0, D2=2D0, D3=3D0, D4=4D0, D10=1D1)
      PARAMETER (MX = 3)
      DIMENSION FF (0:NX), XX(MX)
      COMMON / IOUNIT / NIN, NOUT, NWRT
      DATA SML, HUGE, XX / 1.D-5, 1.D30, 0., 1.0, 2.0 /
      DATA  IW1, IW2, IW3, IW4, IW5, IW6, IW7 / 7 * 0 /
      IR = 0
      X = XV
      ERR = 0.
      ANX = NX
      FINTRP = 0.
      IF (NX .LT. 1) THEN
         CALL WARNI(IW1, NWRT, 'Nx < 1, error in FINTRP.',
     >              'NX', NX, 1, 256, 1)
         IR = 1
         RETURN
      ELSE
         MNX = MIN(NX+1, MX)
      ENDIF
      IF (DX .LE. 0) THEN
         CALL WARNR(IW3, NWRT, 'DX < 0, error in FINTRP.',
     >              'DX', DX, D0, D1, 1)
         IR = 2
         RETURN
      ENDIF
      XM = X0 + DX * NX
      IF (X .LT. X0-SML .OR. X .GT. XM+SML) THEN
        CALL WARNR(IW5,NWRT,'X out of range in FINTRP,
     >      Extrapolation used.','X',X,X0,XM,1)
      IR = 3
      ENDIF
      TX = (X - X0) / DX
      IF (TX .LE. 1.) THEN
        IX = 0
      ELSEIF (TX .GE. ANX-1.) THEN
        IX = NX - 2
      ELSE
        IX = TX
      ENDIF
      DDX = TX - IX
      CALL RATINT (XX, FF(IX), MNX, DDX, TEM, ERR)
      FINTRP = TEM
      RETURN
      END
      FUNCTION FitIni (LP, XX)
      Implicit Double Precision (A-H, O-Z)
      Parameter (D0=0D0, D1=1D0, D2=2D0, D3=3D0, D4=4D0, D10=1D1)
      Parameter (NF0 = 4, Nshp = 4, NCP = 4, NEX = Nshp+2)
      Common 
     >  /InpFin/ A(Nshp), B(0:NEX, -NF0:2), Ifun
      DATA D1M, SML / 0.99999999, 1.0D-10 /
      Fa(X,L) = X**(B(1,L)-1.) *(1.-X)**B(2,L)
      F1(X,L) = 1.+ (Exp(B(3,L)) - 1.) * x**B(4,L)
      F2(x,L) = (LOG(1.+ 1./x)) **B(3,L)
      F3(x,L) = x**(B(1,L) - 1.) *((1.-x)**B(2,L))
     $         *(1. + B(3,L)*(x**.5) + B(4,L)*x)*B(0,L)
      F5(x,L) = 1. + B(3,L)*(x**.5) + B(4,L)*x
      IF (LP .LT. -NF0 .OR. LP .GT. NF0) Then
        FitIni = 0.
        Return
      EndIf
      X = XX
      IF (x .LE. SML .OR. x .GE. D1M) Then
        FitIni = 0.
        Return
      EndIf
      if (lp.eq.1.or.lp.eq.2) then
      if     (ifun .eq. 1.or. ifun.eq.3 .or. ifun.eq.4) then
            FV = Fa(x, LP) * F1(x, LP)
         ElseIf (Ifun .Eq. 2) Then
            FV = Fa(x, LP) * F2(x, LP)
         ElseIf (Ifun .Eq. 5) Then
            FV = Fa(x, LP) * F5(x, LP)
         Else
c            Print *, 'Ifun = 1...5 in FitIni! Not ', Ifun
            Stop
         Endif
      else
         fv = 0.
      endif
      IF (LP .LE. 0) Then
        VL = 0.
      ElseIf (LP .LE. 2) Then
        VL = FV * B(0, LP)
      Else
        VL = 0.
      EndIf
      KP = -ABS (LP)
      If (Ifun .Eq. 1) Then
         FS = Fa(x, KP) * F1(x, KP)* B(0, KP)
      ElseIf (Ifun .Eq. 2) Then
         FS = Fa(x, KP) * F2(x, KP)* B(0, KP)
      ElseIf (Ifun.eq.3) then
	 if(kp.eq.-1 .or. kp.eq.-2) then
           FS = .5D0 * fa(x,-2) * f1(x, -2) * b(0,-2)
     >        + ( -kp -1.5D0) * F3(x,-1) 
	 Else
	   FS = Fa(x, KP) * F1(x, KP)* B(0, KP)
         endif
      ElseIf (Ifun.eq.4) then
	 if(kp.eq.-1 .or. kp.eq.-2) then
           FS = .5D0 * fa(x,-2) * f1(x, -2) * b(0,-2)
     >        + ( -kp -1.5D0) * F3(x,-1) 
	 Elseif( kp.eq.0) then
	   FS = F3(x,kp)
	 Else
	   FS = Fa(x, KP) * F1(x, KP)* B(0, KP)
         endif
      ElseIf (Ifun.eq.5) then
	 If ( kp.eq.-1) then
	    FS = F3(x,-2)*(1.D0-B(0,-4))*0.2D0-.5D0*F3(x,-1)
	 ElseIf ( kp.eq.-2) then
	    FS = F3(x,-2)*(1.D0-B(0,-4))*0.2D0+.5D0*F3(x,-1)
	 ElseIf ( kp.eq.-3) then
	    FS = F3(x,kp)*(1.D0-B(0,-4))*0.1D0
	 ElseIf ( kp.eq.-4) then
	    FS = F3(x,kp)* B(0,-3) * 0.5D0
	 Elseif( kp.eq.0) then
	    FS = F3(x,kp)
         endif
      Endif
      SEA = FS 
      FitIni = VL + SEA
      Return
      End
      BLOCK DATA DATPDF
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      CHARACTER*10 NAMPDF, NMHDRN, NAMQRK
      CHARACTER*12 MRSFLN
      LOGICAL LSTX
      PARAMETER (Z = 1D -10, ZZ = 1D -20)
      PARAMETER (MXX = 105, MXQ = 25, MXF = 6, MXHDRN = 6)
      PARAMETER (MXPN = MXF*2+2)
      PARAMETER (MXQX= MXQ * MXX,   MXPQX = MXQX * MXPN)
      PARAMETER (MXPDF = 20)
      COMMON / IOUNIT / NIN, NOUT, NWRT
      COMMON / XXARAY / XCR, XMIN, XV(0:MXX), LSTX, NX
      COMMON / QARAY1 / QINI,QMAX, QV(0:MXQ),TV(0:MXQ), NT,JT,NG
      COMMON / EVLPAC / AL, IKNL, IPD0, IHDN, KF
      COMMON / PEVLDT / UPD(MXPQX)
      COMMON / PARPRZ / XMNPRZ(9), QMNPRZ(9), ALF(9),
     >                  NFLPRZ(9), NFAL(9), MORD(9)
      COMMON / INITIA / AN(-MXF : MXF), AI(-MXF : MXF),
     >                  BI(-MXF : MXF), CI(-MXF : MXF)
      COMMON / NAMPDF / NAMPDF(MXPDF), NMHDRN(-1 : MXHDRN),
     >                  NAMQRK(-MXF : MXF)
      COMMON / MRSDAT / MRSFLN (3)
      DATA QINI, QMAX, XMIN, XCR / 1.9, 1.001D2, 0.999D-3, 1.5 /
      DATA KF, IKNL, IPD0, IHDN / 10, 1, 1, 1 /
      DATA NX, NT, JT, LSTX / 40, 6, 1, .FALSE. /
      DATA (NFLPRZ(I), I=1,9) / 6,4,5,5,5,6,6,6,6/
      DATA (XMNPRZ(I), I=1,9) / 3*1.d-4,6*1.D-5/
      DATA (QMNPRZ(I), I=1,9) / 2.25, 2.0,3.2, 2*2.25, 4*2.0  /
      DATA (MORD(I), I=1,9) / 2*1, 7*2/
      DATA (ALF(I),  I=1,9) / 0.2, 0.2, 0.3, 0.19, 0.215,
     $     0.22, 0.225,0.2,0.2/
      DATA (NFAL(I), I=1,9) / 3*4, 6*5 /
      DATA MRSFLN / 'prmz:hmrse', 'prmz:hmrsb', 'GRID3' /
      DATA (NAMPDF(I),I=1,11) / 'Ehlq_1    ','Dk-Ow 1   ', 'DFLM_NLLA',
     >'KMRS B0  ','MRS92_D0  ','MT90_S1  ','CTEQ1M   ', '          ',
     >'         ', 'QCD_EVL_1','QCD_EVL_2' /
      DATA (NMHDRN(I), I=-1,5)/ 'AntiProton', 'Dummy', 'Proton',
     > 'Neutron', 'Pi_Plus', 'Pi_Minus', 'K_Plus' /
      DATA (NAMQRK(I), I=-6,6)  / '-t_Quark', '-b_Quark', '-c_Quark',
     >    '-s_Quark', '-d_Quark', '-u_Quark',    'Gluon',  'u_Quark',
     >     'd_Quark',  's_Quark', 'c_Quark',   'b_Quark',  't_Quark' /
      DATA AI / 7 * Z,                      0.5,  0.4,  4 * Z /
      DATA BI / 6 * Z,                3.5,  2 * 1.51,   4 * 1./
      DATA CI / 3 * 5, 3 * 8.54,      5.9,  3.5,  4.5,  4 * 5./
      DATA AN / 3 *ZZ, .081, 2 *.182, 2.62, 1.78, 0.67, 4 *ZZ /
      END
      BLOCK DATA DATQCD
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      COMMON /COMQCH/ VALQCH(9)
      COMMON /COMQMS/ VALQMS(9)
      COMMON /QCDPAR/ AL, NF, NORDER, SET
      COMMON /COMALP/ ALPHA
      LOGICAL SET
      DATA AL, NF, NORDER, SET / .20, 5, 2, .FALSE. /
      DATA VALQCH/ 0.66666667, -0.33333333,
     >  -0.33333333, 0.66666667,
     >  -0.33333333, 0.66666667,
     >  3*0./
      DATA VALQMS/  2*0., .2, 1.6, 5., 180., 3*0./
      DATA ALPHA/  7.29927E-3 /
      END

      FUNCTION GAUSS(F,XL,XR,AERR,RERR,ERR,IRT)
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
        DIMENSION XLIMS(100), R(93), W(93)
        INTEGER PTR(4),NORD(4),NIN,NOUT,NWRT
        external f
        COMMON/IOUNIT/NIN,NOUT,NWRT
        DATA PTR,NORD/4,10,22,46,  6,12,24,48/
        DATA R/.2386191860,.6612093865,.9324695142,
     1 .1252334085,.3678314990,.5873179543,.7699026742,.9041172563,
     1 .9815606342,.0640568929,.1911188675,.3150426797,.4337935076,
     1 .5454214714,.6480936519,.7401241916,.8200019860,.8864155270,
     1 .9382745520,.9747285560,.9951872200,.0323801710,.0970046992,
     1 .1612223561,.2247637903,.2873624873,.3487558863,.4086864820,
     1 .4669029048,.5231609747,.5772247261,.6288673968,.6778723796,
     1 .7240341309,.7671590325,.8070662040,.8435882616,.8765720203,
     1 .9058791367,.9313866907,.9529877032,.9705915925,.9841245837,
     1 .9935301723,.9987710073,.0162767488,.0488129851,.0812974955,
     1 .1136958501,.1459737146,.1780968824,.2100313105,.2417431561,
     1 .2731988126,.3043649444,.3352085229,.3656968614,.3957976498,
     1 .4254789884,.4547094222,.4834579739,.5116941772,.5393881083,
     1 .5665104186,.5930323648,.6189258401,.6441634037,.6687183100,
     1 .6925645366,.7156768123,.7380306437,.7596023411,.7803690438,
     1 .8003087441,.8194003107,.8376235112,.8549590334,.8713885059,
     1 .8868945174,.9014606353,.9150714231,.9277124567,.9393703398,
     1 .9500327178,.9596882914,.9683268285,.9759391746,.9825172636,
     1 .9880541263,.9925439003,.9959818430,.9983643759,.9996895039/
        DATA W/.4679139346,.3607615730,.1713244924,
     1 .2491470458,.2334925365,.2031674267,.1600783285,.1069393260,
     1 .0471753364,.1279381953,.1258374563,.1216704729,.1155056681,
     1 .1074442701,.0976186521,.0861901615,.0733464814,.0592985849,
     1 .0442774388,.0285313886,.0123412298,.0647376968,.0644661644,
     1 .0639242386,.0631141923,.0620394232,.0607044392,.0591148397,
     1 .0572772921,.0551995037,.0528901894,.0503590356,.0476166585,
     1 .0446745609,.0415450829,.0382413511,.0347772226,.0311672278,
     1 .0274265097,.0235707608,.0196161605,.0155793157,.0114772346,
     1 .0073275539,.0031533461,.0325506145,.0325161187,.0324471637,
     1 .0323438226,.0322062048,.0320344562,.0318287589,.0315893308,
     1 .0313164256,.0310103326,.0306713761,.0302999154,.0298963441,
     1 .0294610900,.0289946142,.0284974111,.0279700076,.0274129627,
     1 .0268268667,.0262123407,.0255700360,.0249006332,.0242048418,
     1 .0234833991,.0227370697,.0219666444,.0211729399,.0203567972,
     1 .0195190811,.0186606796,.0177825023,.0168854799,.0159705629,
     1 .0150387210,.0140909418,.0131282296,.0121516047,.0111621020,
     1 .0101607705,.0091486712,.0081268769,.0070964708,.0060585455,
     1 .0050142027,.0039645543,.0029107318,.0018539608,.0007967921/
        DATA TOLABS,TOLREL,NMAX/1.E-35,5.E-4,100/
        TOLABS=AERR
        TOLREL=RERR
        GAUSS=0.
        NLIMS=2
        XLIMS(1)=XL
        XLIMS(2)=XR
10      AA=(XLIMS(NLIMS)-XLIMS(NLIMS-1))/2D0
        BB=(XLIMS(NLIMS)+XLIMS(NLIMS-1))/2D0
        TVAL=0.
        DO 15 I=1,3
15      TVAL=TVAL+W(I)*(F(BB+AA*R(I))+F(BB-AA*R(I)))
        TVAL=TVAL*AA
        DO 25 J=1,4
        VAL=0.
        DO 20 I=PTR(J),PTR(J)-1+NORD(J)
20      VAL=VAL+W(I)*(F(BB+AA*R(I))+F(BB-AA*R(I)))
        VAL=VAL*AA
        TOL=MAX(TOLABS,TOLREL*ABS(VAL))
        IF (ABS(TVAL-VAL).LT.TOL) THEN
                GAUSS=GAUSS+VAL
                NLIMS=NLIMS-2
                IF (NLIMS.NE.0) GO TO 10
                RETURN
                END IF
25      TVAL=VAL
        IF (NMAX.EQ.2) THEN
                GAUSS=VAL
                RETURN
                END IF
        IF (NLIMS.GT.(NMAX-2)) THEN
                WRITE(NOUT,50) GAUSS,NMAX,BB-AA,BB+AA
                RETURN
                END IF
        XLIMS(NLIMS+1)=BB
        XLIMS(NLIMS+2)=BB+AA
        XLIMS(NLIMS)=BB
        NLIMS=NLIMS+2
        GO TO 10
50      FORMAT (' GAUSS FAILS, GAUSS,NMAX,XL,XR=',G15.7,I5,2G15.7)
        END
      SUBROUTINE HQRK (NX, TT, NQRK, Y, F)
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      PARAMETER (MXX = 105, MXQ = 25, MXF = 6)
      DIMENSION Y(NX), F(NX)
      IF (NX .GT. 1) GOTO 11
   11 CONTINUE
      DO 230 IZ = 1, NX
        IF (NX .GT. 1) THEN
        F(IZ) = 0
        GOTO 230
        EndIf
  230 CONTINUE
      RETURN
      END
      SUBROUTINE WTUPD (UPD, NTL, NDAT, IRET)
      DOUBLE PRECISION UPD
      DIMENSION UPD (NTL)
      WRITE (NDAT, IOSTAT=IRET) UPD
      RETURN
      ENTRY RDUPD (UPD, NTL, NDAT, IRET)
      READ (NDAT, IOSTAT=IRET) UPD
      RETURN
      END
      SUBROUTINE POLINT (XA,YA,N,X,Y,DY)
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      PARAMETER (NMAX=10)
      DIMENSION XA(N),YA(N),C(NMAX),D(NMAX)
      NS=1
      DIF=ABS(X-XA(1))
      DO 11 I=1,N
        DIFT=ABS(X-XA(I))
        IF (DIFT.LT.DIF) THEN
          NS=I
          DIF=DIFT
        ENDIF
        C(I)=YA(I)
        D(I)=YA(I)
11    CONTINUE
      Y=YA(NS)
      NS=NS-1
      DO 13 M=1,N-1
        DO 12 I=1,N-M
          HO=XA(I)-X
          HP=XA(I+M)-X
          W=C(I+1)-D(I)
          DEN=HO-HP
          IF(DEN.EQ.0.)PAUSE
          DEN=W/DEN
          D(I)=HP*DEN
          C(I)=HO*DEN
12      CONTINUE
        IF (2*NS.LT.N-M)THEN
          DY=C(NS+1)
        ELSE
          DY=D(NS)
          NS=NS-1
        ENDIF
        Y=Y+DY
13    CONTINUE
      RETURN
      END
      SUBROUTINE RATINT(XA,YA,N,X,Y,DY)
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      PARAMETER (NMAX=10,TINY=1.E-25)
      DIMENSION XA(N),YA(N),C(NMAX),D(NMAX)
      NS=1
      HH=ABS(X-XA(1))
      DO 11 I=1,N
        H=ABS(X-XA(I))
        IF (H.EQ.0.)THEN
          Y=YA(I)
          DY=0.0
          RETURN
        ELSE IF (H.LT.HH) THEN
          NS=I
          HH=H
        ENDIF
        C(I)=YA(I)
        D(I)=YA(I)+TINY
11    CONTINUE
      Y=YA(NS)
      NS=NS-1
      DO 13 M=1,N-1
        DO 12 I=1,N-M
          W=C(I+1)-D(I)
          H=XA(I+M)-X
          T=(XA(I)-X)*D(I)/H
          DD=T-C(I+1)
          IF(DD.EQ.0.)PAUSE
          DD=W/DD
          D(I)=C(I+1)*DD
          C(I)=T*DD
12      CONTINUE
        IF (2*NS.LT.N-M)THEN
          DY=C(NS+1)
        ELSE
          DY=D(NS)
          NS=NS-1
        ENDIF
        Y=Y+DY
13    CONTINUE
      RETURN
      END
      SUBROUTINE KERNEL
     >(XX, FF1, FG1, GF1, GG1, PNSP, PNSM, FF2, FG2, GF2, GG2, NFL, IRT)
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      PARAMETER (PI = 3.141592653589793, PI2 = PI ** 2)
      PARAMETER (D0 = 0.0, D1 = 1.0)
      COMMON / IOUNIT / NIN, NOUT, NWRT
      DATA CF, CG, TR, IWRN / 1.333333333333, 3.0, 0.5, 0 /
      IRT = 0
      TRNF = TR * NFL
      X = XX
      IF (X .LE. 0. .OR. X .GE. 1.) THEN
        CALL WARNR(IWRN, NWRT, 'X out of range in KERNEL', 'X', X,
     >             D0, D1, 1)
        IRT = 1
        RETURN
      EndIf
      XI = 1./ X
      X2 = X ** 2
      XM1I = 1./ (1.- X)
      XP1I = 1./ (1.+ X)
      XLN = LOG (X)
      XLN2 = XLN ** 2
      XLN1M = LOG (1.- X)
      SPEN2 = SPENC2 (X)
      FFP = (1.+ X2) * XM1I
      FGP = (2.- 2.* X + X2) / X
      GFP = 1. - 2.* X + 2.* X2
      GGP = XM1I + XI - 2. + X - X2
      FFM = (1.+ X2) * XP1I
      FGM = - (2.+ 2.* X + X2) / X
      GFM = 1. + 2.* X + 2.* X2
      GGM = XP1I - XI - 2. - X - X2
      FF1 = CF * FFP * (1.- X)
      FG1 = CF * FGP * X
      GF1 = 2.* TRNF * GFP
      GG1 = 2.* CG * GGP * X * (1.-X)
      PCF2 = -2.* FFP *XLN*XLN1M - (3.*XM1I + 2.*X)*XLN
     >     - (1.+X)/2.*XLN2 - 5.*(1.-X)
      PCFG = FFP * (XLN2 + 11.*XLN/3.+ 67./9.- PI**2 / 3.)
     >     + 2.*(1.+X) * XLN + 40.* (1.-X) / 3.
      PCFT = (FFP * (- XLN - 5./3.) - 2.*(1.-X)) * 2./ 3.
      PQQB = 2.* FFM * SPEN2 + 2.*(1.+X)*XLN + 4.*(1.-X)
      PQQB = (CF**2-CF*CG/2.) * PQQB
      PQQ2 = CF**2 * PCF2 + CF*CG * PCFG / 2. + CF*TRNF * PCFT
      PNSP = (PQQ2 + PQQB) * (1.-X)
      PNSM = (PQQ2 - PQQB) * (1.-X)
      FFCF2 = - 1. + X + (1.- 3.*X) * XLN / 2. - (1.+ X) * XLN2 / 2.
     >      - FFP * (3.* XLN / 2. + 2.* XLN * XLN1M)
     >      + FFM * 2.* SPEN2
      FFCFG = 14./3.* (1.-X)
     >      + FFP * (11./6.* XLN + XLN2 / 2. + 67./18. - PI2 / 6.)
     >      - FFM * SPEN2
      FFCFT = - 16./3. + 40./3.* X + (10.* X + 16./3.* X2 + 2.) * XLN
     >                 - 112./9.* X2 + 40./9./X - 2.* (1.+ X) * XLN2
     >      - FFP * (10./9. + 2./3. * XLN)
      FGCF2 = - 5./2.- 7./2.* X + (2.+ 7./2.* X) * XLN + (X/2.-1.)*XLN2
     >               - 2.* X * XLN1M
     >      - FGP * (3.* XLN1M + XLN1M ** 2)
      FGCFG = 28./9. + 65./18.* X + 44./9. * X2 - (12.+ 5.*X + 8./3.*X2)
     >                      * XLN + (4.+ X) * XLN2 + 2.* X * XLN1M
     >      + FGP * (-2.*XLN*XLN1M + XLN2/2. + 11./3.*XLN1M + XLN1M**2
     >               - PI2/6. + 0.5)
     >      + FGM * SPEN2
      FGCFT = -4./3.* X - FGP * (20./9.+ 4./3.*XLN1M)
      GFCFT = 4.- 9.*X + (-1.+ 4.*X)*XLN + (-1.+ 2.*X)*XLN2 + 4.*XLN1M
     >      + GFP * (-4.*XLN*XLN1M + 4.*XLN + 2.*XLN2 - 4.*XLN1M
     >               + 2.*XLN1M**2 - 2./3.* PI2 + 10.)
      GFCGT = 182./9.+ 14./9.*X + 40./9./X + (136./3.*X - 38./3.)*XLN
     >               - 4.*XLN1M - (2.+ 8.*X)*XLN2
     >      + GFP * (-XLN2 + 44./3.*XLN - 2.*XLN1M**2 + 4.*XLN1M
     >               + PI2/3. - 218./9.)
     >      + GFM * 2. * SPEN2
      GGCFT = -16.+ 8.*X + 20./3.*X2 + 4./3./X + (-6.-10.*X)*XLN
     >        - 2.* (1.+ X) * XLN2
      GGCGT = 2.- 2.*X + 26./9.*X2 - 26./9./X - 4./3.*(1.+X)*XLN
     >      - GGP * 20./9.
      GGCG2 = 27./2.*(1.-X) + 67./9.*(X2-XI) + 4.*(1.+X)*XLN2
     >              + (-25.+ 11.*X - 44.*X2)/3.*XLN
     >      + GGP * (67./9.- 4.*XLN*XLN1M + XLN2 - PI2/3.)
     >      + GGM * 2.* SPEN2
      FF2 = CF * TRNF * FFCFT + CF ** 2 * FFCF2 + CF * CG   * FFCFG
      FG2 = CF * TRNF * FGCFT + CF ** 2 * FGCF2 + CF * CG   * FGCFG
      GF2 = CF * TRNF * GFCFT                   + CG * TRNF * GFCGT
      GG2 = CF * TRNF * GGCFT + CG ** 2 * GGCG2 + CG * TRNF * GGCGT
      XLG = (LOG(1./(1.-X)) + 1.)
      XG2 = XLG ** 2
      FF2 = FF2 * X * (1.- X)
      FG2 = FG2 * X / XG2
      GF2 = GF2 * X / XG2
      GG2 = GG2 * X * (1.- X)
      RETURN
      END
      FUNCTION PFF1 (XX)
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      LOGICAL LA, LB, LSTX
      PARAMETER (D0=0D0, D1=1D0, D2=2D0, D3=3D0, D4=4D0, D10=1D1)
      PARAMETER (MXX = 105, MXQ = 25, MXF = 6)
      PARAMETER (M1=-3, M2=3, NDG=3, NDH=NDG+1, L1=M1-1, L2=M2+NDG-2)
      PARAMETER (MX = 3)
      COMMON / IOUNIT / NIN, NOUT, NWRT
      COMMON / XXARAY / XCR, XMIN, XV(0:MXX), LSTX, NX
      COMMON / KRNL00 / DZ, XL(MX), NNX
      COMMON / VARIBX / XA(MXX, L1:L2), ELY(MXX), DXTZ(MXX)
      COMMON / KRN1ST / FF1(0:MXX), FG1(0:MXX), GF1(0:MXX), GG1(0:MXX),
     >                  FF2(0:MXX), FG2(0:MXX), GF2(0:MXX), GG2(0:MXX),
     >                  PNSP(0:MXX), PNSM(0:MXX)
      SAVE
      DATA LA, LB, TINY / 2 * .FALSE., 1.E-15 /
      LB = .TRUE.
      ENTRY TFF1(ZZ)
      LA = .TRUE.
      GOTO 2
      ENTRY QFF1 (XX)
      LB = .TRUE.
      ENTRY RFF1 (XX)
    2 IF (LA .AND. .NOT.LB) THEN
        Z = ZZ
        X = XFRMZ (Z)
      Else
        X = XX
      EndIf
      IF (X .GE. D1) THEN
        PFF1 = 0
        RETURN
      ElseIF (X .GE. XMIN) THEN
        Z = ZFRMX (X)
        TEM = FINTRP (FF1,  -DZ, DZ, NX,  Z,  ERR, IRT)
      Else
        CALL POLINT (XL, FF1(1), MX, X, TEM, ERR)
      EndIf
      IF (LA) THEN
         IF (LB) THEN
            PFF1 = TEM / (1.-X)
            LB   =.FALSE.
         Else
            TFF1 = TEM / (1.-X) * DXDZ(Z)
         EndIf
         LA   =.FALSE.
      Else
         IF (LB) THEN
            QFF1 = TEM
            LB   =.FALSE.
         Else
            RFF1 = TEM * X / (1.-X)
         EndIf
      EndIf
      RETURN
      ENTRY FNSP (XX)
      X = XX
      IF (X .GE. D1) THEN
        FNSP = 0.
        RETURN
      ElseIF (X .GE. XMIN) THEN
        Z = ZFRMX (X)
        TEM = FINTRP (PNSP,  -DZ, DZ, NX,  Z,  ERR, IRT)
      Else
        CALL POLINT (XL, PNSP(1), MX, X, TEM, ERR)
      EndIf
      FNSP = TEM / (1.- X)
      RETURN
      ENTRY FNSM (XX)
      X = XX
      IF (X .GE. D1) THEN
        FNSM = 0.
        RETURN
      ElseIF (X .GE. XMIN) THEN
        Z = ZFRMX (X)
        TEM = FINTRP (PNSM,  -DZ, DZ, NX,  Z,  ERR, IRT)
      Else
        CALL POLINT (XL, PNSM(1), MX, X, TEM, ERR)
      EndIf
      FNSM = TEM / (1.- X)
      RETURN
      ENTRY PFG1 (XX)
      LA = .TRUE.
      ENTRY QFG1 (XX)
      LB = .TRUE.
      ENTRY RFG1 (XX)
      X = XX
      IF (X .GE. D1) THEN
         PFG1 = 0
         RETURN
      ElseIF (X .GE. XMIN) THEN
        Z = ZFRMX (X)
        TEM = FINTRP (FG1,  -DZ, DZ, NX,  Z,  ERR, IRT)
      Else
        CALL POLINT (XL, FG1(1), MX, X, TEM, ERR)
      EndIf
      IF (LA) THEN
         PFG1 = TEM
         LA   =.FALSE.
      Else
         IF (LB) THEN
            QFG1 = TEM * (1.- X)
            LB   =.FALSE.
         Else
            RFG1 = TEM * X
         EndIf
      EndIf
      RETURN
      ENTRY PGF1 (XX)
      LA = .TRUE.
      ENTRY QGF1 (XX)
      LB = .TRUE.
      ENTRY RGF1 (XX)
      X = XX
      IF (X .GE. D1) THEN
        PGF1= 0
        RETURN
      ElseIF (X .GE. XMIN) THEN
        Z = ZFRMX (X)
        TEM = FINTRP (GF1,  -DZ, DZ, NX,  Z,  ERR, IRT)
      Else
        CALL POLINT (XL, GF1(1), MX, X, TEM, ERR)
      EndIf
      IF (LA) THEN
         PGF1 = TEM / X
         LA   =.FALSE.
      Else
         IF (LB) THEN
            QGF1 = TEM * (1.- X) / X
            LB   =.FALSE.
         Else
            RGF1 = TEM
         EndIf
      EndIf
      RETURN
      ENTRY PGG1 (XX)
      LA = .TRUE.
      ENTRY QGG1 (XX)
      LB = .TRUE.
      ENTRY RGG1 (XX)
      X = XX
      IF (X .GE. D1) THEN
        PGG1= 0
        RETURN
      ElseIF (X .GE. XMIN) THEN
        Z = ZFRMX (X)
        TEM = FINTRP (GG1,  -DZ, DZ, NX,  Z,  ERR, IRT)
      Else
        CALL POLINT (XL, GG1(1), MX, X, TEM, ERR)
      EndIf
      IF (LA) THEN
         PGG1 = TEM / X / (1.-X)
         LA   =.FALSE.
      Else
         IF (LB) THEN
            QGG1 = TEM / X
            LB   =.FALSE.
         Else
            RGG1 = TEM / (1.-X)
         EndIf
      EndIf
      RETURN
      ENTRY PFF2 (XX)
      LA = .TRUE.
      ENTRY QFF2 (XX)
      LB = .TRUE.
      ENTRY RFF2 (XX)
      X = XX
      IF (X .GE. D1) THEN
        PFF2 = 0
        RETURN
      ElseIF (X .GE. XMIN) THEN
        Z = ZFRMX (X)
        TEM = FINTRP (FF2,  -DZ, DZ, NX,  Z,  ERR, IRT)
      Else
        CALL POLINT (XL, FF2(1), MX, X, TEM, ERR)
      EndIf
      IF (LA) THEN
         PFF2 = TEM / X / (1.-X)
         LA   =.FALSE.
      Else
         IF (LB) THEN
            QFF2 = TEM / X
            LB   =.FALSE.
         Else
            RFF2 = TEM / (1.-X)
         EndIf
      EndIf
      RETURN
      ENTRY PFG2 (XX)
      LA = .TRUE.
      ENTRY QFG2 (XX)
      LB = .TRUE.
      ENTRY RFG2 (XX)
      X = XX
      XM1 = 1.- X
      XLG = (LOG(1./XM1) + 1.)**2
      IF (X .GE. D1) THEN
        PFG2 = 0
        RETURN
      ElseIF (X .GE. XMIN) THEN
        Z = ZFRMX (X)
        TEM = FINTRP (FG2,  -DZ, DZ, NX,  Z,  ERR, IRT)
      Else
        CALL POLINT (XL, FG2(1), MX, X, TEM, ERR)
      EndIf
      IF (LA) THEN
         PFG2 = TEM / X * XLG
         LA   =.FALSE.
      Else
         IF (LB) THEN
            QFG2 = TEM / X * XLG * XM1
            LB   =.FALSE.
         Else
            RFG2 = TEM * XLG
         EndIf
      EndIf
      RETURN
      ENTRY PGF2 (XX)
      LA = .TRUE.
      ENTRY QGF2 (XX)
      LB = .TRUE.
      ENTRY RGF2 (XX)
      X = XX
      XM1 = 1.- X
      XLG = (LOG(1./XM1) + 1.) ** 2
      IF (X .GE. D1) THEN
        PGF2 = 0
        RETURN
      ElseIF (X .GE. XMIN) THEN
        Z = ZFRMX (X)
        TEM = FINTRP (GF2,  -DZ, DZ, NX,  Z,  ERR, IRT)
      Else
        CALL POLINT (XL, GF2(1), MX, X, TEM, ERR)
      EndIf
      IF (LA) THEN
         PGF2 = TEM / X * XLG
         LA   =.FALSE.
      Else
         IF (LB) THEN
            QGF2 = TEM / X * XLG * XM1
            LB   =.FALSE.
         Else
            RGF2 = TEM * XLG
         EndIf
      EndIf
      RETURN
      ENTRY PGG2 (XX)
      LA = .TRUE.
      ENTRY QGG2 (XX)
      LB = .TRUE.
      ENTRY RGG2 (XX)
      X = XX
      IF (X .GE. D1) THEN
        PGG2 = 0
        RETURN
      ElseIF (X .GE. XMIN) THEN
        Z = ZFRMX (X)
        TEM = FINTRP (GG2,  -DZ, DZ, NX,  Z,  ERR, IRT)
      Else
        CALL POLINT (XL, GG2(1), MX, X, TEM, ERR)
      EndIf
      IF (LA) THEN
         PGG2 = TEM / X / (1.-X)
         LA   =.FALSE.
      Else
         IF (LB) THEN
            QGG2 = TEM / X
            LB   =.FALSE.
         Else
            RGG2 = TEM / (1.-X)
         EndIf
      EndIf
      RETURN
      ENTRY VFF1 (XX)
      X = XX
      TEM = (1.+ X**2) / (1.- X)
      VFF1 = TEM *4./3.
      RETURN
      END
      SUBROUTINE NSEVL (RHS, IKNL,NX,NT,JT,DT,TIN,NEFF,U0,UN)
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      PARAMETER (MXX = 105, MXQ = 25, MXF = 6)
      PARAMETER (MXPN = MXF * 2 + 2)
      PARAMETER (MXQX= MXQ * MXX,   MXPQX = MXQX * MXPN)
      PARAMETER (M1=-3, M2=3, NDG=3, NDH=NDG+1, L1=M1-1, L2=M2+NDG-2)
      COMMON / IOUNIT / NIN, NOUT, NWRT
      COMMON / ANOMS1 / GG, GF, FG, FF, GPL, GMI
      COMMON / ANOMS2 / SGG, SGF, SFG, SFF, TGG, TGF, TFG, TFF
      COMMON / VARIBX / XA(MXX, L1:L2), ELY(MXX), DXTZ(MXX)
      DIMENSION  U0(NX), UN(0:NX, 0:NT)
      DIMENSION  Y0(MXX), Y1(MXX), YP(MXX), F0(MXX), F1(MXX), FP(MXX)
      external rhs
      ESF(T) = 4./ (33.- 2.*NEFF) * LOG((T - TLAM)/(TIN - TLAM))
      N5 = (NX - 1) / 5
      DDT = DT / JT
      IF (NX .GT. MXX) THEN
      WRITE (NOUT,*) 'Nx =', NX, ' greater than Max # of pts in NSEVL.'
      STOP 'Program stopped in NSEVL'
      EndIf
      TMD = TIN + DT * NT / 2.
      AMU = EXP(TMD)
      TEM = 6./ (33.- 2.* NEFF) / ALPI(AMU)
      TLAM = TMD - TEM
      DO 9 IX = 1, NX
      UN(IX, 0)  = U0(IX)
    9 CONTINUE
      UN(0, 0) = 3D0*U0(1) - 3D0*U0(2) - U0(1)
      TT = TIN
      DO 10 IZ = 1, NX
      Y0(IZ)   = U0(IZ)
   10 CONTINUE
      DO 20 IS = 1, NT
         DO 202 JS = 1, JT
            IRND = (IS-1) * JT + JS
            IF (IRND .EQ. 1) THEN
                CALL RHS (TT, Neff, Y0, F0)
                DO 250 IZ = 1, NX
                   Y0(IZ) = Y0(IZ) + DDT * F0(IZ)
  250           CONTINUE
                TT = TT + DDT
                CALL RHS (TT, NEFF, Y0, F1)
                DO 251 IZ = 1, NX
                   Y1(IZ) = U0(IZ) + DDT * (F0(IZ) + F1(IZ)) / 2D0
  251           CONTINUE
            Else
                CALL RHS (TT, NEFF, Y1, F1)
                DO 252 IZ = 1, NX
                   YP(IZ) = Y1(IZ) + DDT * (3D0 * F1(IZ) - F0(IZ)) / 2D0
  252           CONTINUE
                TT = TT + DDT
                CALL RHS (TT, NEFF, YP, FP)
                DO 253 IZ = 1, NX
                   Y1(IZ) = Y1(IZ) + DDT * (FP(IZ) + F1(IZ)) / 2D0
                   F0(IZ) = F1(IZ)
  253           CONTINUE
            EndIf
  202    CONTINUE
         DO 260 IZ = 1, NX
            UN (IZ, IS) = Y1(IZ)
  260    CONTINUE
         UN(0, IS) = 3D0*Y1(1) - 3D0*Y1(2) + Y1(3)
   20 CONTINUE
      RETURN
      END
      FUNCTION PARDIS (IPRTN, XX, QQ)
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      LOGICAL LSTX
      PARAMETER (MXX = 105, MXQ = 25, MXF = 6)
      PARAMETER (MXPN = MXF * 2 + 2)
      PARAMETER (MXQX= MXQ * MXX,   MXPQX = MXQX * MXPN)
      COMMON / IOUNIT / NIN, NOUT, NWRT
      COMMON / XXARAY / XCR, XMIN, XV(0:MXX), LSTX, NX
      COMMON / QARAY1 / QINI,QMAX, QV(0:MXQ),TV(0:MXQ), NT,JT,NG
      COMMON / QARAY2 / TLN(MXF), DTN(MXF), NTL(MXF), NTN(MXF)
      COMMON / EVLPAC / AL, IKNL, IPD0, IHDN, KF
      COMMON / PEVLDT / UPD(MXPQX)
      Dimension Fq(5), Df(5), QL(5)
      DATA SMLL, M, II / 1.D-7, 2, 0 /
      X = XX
      Q = QQ
      Md = M / 2
      Amd= Md
      IF (X .LT. -SMLL .OR. X .GT. 1D0+SMLL) THEN
c       PRINT *, 'X = ',X, 'Out of range in the PARDIS function call!'
       STOP
      EndIf
      IF (Q .LT. QINI) THEN
      CALL WARNR (IWRN1, NWRT, 'Q less than QINI in PARDIS call; Q SET
     >= QINI.', 'Q', Q, QINI, QMAX, 1)
       Q = QINI
      ElseIF (Q .GT. QMAX) THEN
      CALL WARNR(IWRN2,NWRT,'Q greater than QMAX in PARDIS call;
     >Extrapolation will be used.', 'Q', Q, QINI, QMAX, 1)
      EndIf
      JL = -1
      JU = Nx+1
 11   If (JU-JL .GT. 1) Then
         JM = (JU+JL) / 2
         If (X .GT. XV(JM)) Then
            JL = JM
         Else
            JU = JM
         Endif
         Goto 11
      Endif
      Jx = JL - (M-1)/2
      If     (Jx .LT. 0) Then
         Jx = 0
      Elseif (Jx .GT. Nx-M) Then
         Jx = Nx - M
      Endif
      JL = -1
      JU = NT+1
 12   If (JU-JL .GT. 1) Then
         JM = (JU+JL) / 2
         If (Q .GT. QV(JM)) Then
            JL = JM
         Else
            JU = JM
         Endif
         Goto 12
       Endif
      Jq = JL - (M-1)/2
      If     (Jq .LT. 0) Then
         Jq = 0
      Elseif (Jq .GT. Nt-M) Then
         Jq = Nt - M
      Endif
      SS  = LOG (Q/AL)
      TT  = LOG (SS)
      JFL = IPRTN + (KF - 2) / 2
      J0  = (JFL * (NT+1) + Jq) * (NX+1) + Jx
      Do 21 Iq = 1, M+1
      J1 = J0 + (Nx+1)*(Iq-1) + 1
      If     (II .EQ. 0) Then
        Call Polint (XV(Jx), Upd(J1), M+1, X, Fq(Iq), Df(Iq))
      Elseif (II .EQ. 1) Then
        Call RatInt (XV(Jx), Upd(J1), M+1, X, Fq(Iq), Df(Iq))
      Else
c         Print *, 'II out of range in Pardis; II = ', II
      Endif
 21   Continue
      Call Polint (TV(Jq), Fq(1), M+1, TT, Ftmp, Ddf)
      PARDIS = Ftmp
      RETURN
      END
      SUBROUTINE PARPDF (IACT, NAME, VALUE, IRET)
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      CHARACTER NAME*(*), Uname*10
      LOGICAL START1
      COMMON / IOUNIT / NIN, NOUT, NWRT
      DATA ILEVEL, LRET / 1, 1 /
      JRET = IRET
      CALL UPC (NAME, Ln, Uname)
      IF (IACT .EQ. 0 .OR. IACT .EQ. 4)
     >    IVALUE = NINT (VALUE)
      START1 = (IACT .NE. 1) .AND. (IACT .NE. 2)
      IF (START1)  ILEVEL = 1
      GOTO (1, 2), ILEVEL
    1 START1 = .TRUE.
      ILEVEL = 0
      CALL PARQCD (IACT, Uname(1:Ln), VALUE, JRET)
              IF (JRET .EQ. 1)  GOTO 11
              IF (JRET .EQ. 2)  GOTO 12
              IF (JRET .EQ. 3)  GOTO 13
              IF (JRET .GT. 4)  GOTO 15
              ILEVEL =  ILEVEL + 1
    2 CALL EVLPAR (IACT, Uname(1:Ln), VALUE, JRET)
              IF (JRET .EQ. 1)  GOTO 11
              IF (JRET .EQ. 2)  GOTO 12
              IF (JRET .EQ. 3)  GOTO 13
              IF (JRET .GT. 4)  GOTO 15
              ILEVEL =  ILEVEL + 1
      IF (.NOT. START1) GOTO 1
      IF (JRET .EQ. 0)  GOTO 10
    9 CONTINUE
      GOTO 14
   10 CONTINUE
   11 CONTINUE
   12 CONTINUE
   13 CONTINUE
   14 CONTINUE
   15 CONTINUE
      IF (JRET .NE. 4) LRET = JRET
c      IF (LRET.EQ.0 .OR. LRET.EQ.2 .OR. LRET.EQ.3) THEN
c        PRINT *, 'Error in PARPDF: IRET, IACT, NAME, VALUE =',
c     >  LRET, IACT, NAME, VALUE
c        PAUSE
c      EndIf
      IRET= JRET
      RETURN
  100 FORMAT (/)
      END
      SUBROUTINE PARQCD(IACT,NAME,VALUE,IRET)
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      INTEGER IACT,IRET
      CHARACTER*(*) NAME
      IRET=1
      IF (IACT.EQ.0) THEN
         WRITE (NINT(VALUE), *)  'LAM(BDA), NFL, ORD(ER), Mi, ',
     >               '(i in 1 to 9), LAMi (i in 1 to NFL)'
         IRET=4
      ELSEIF (IACT.EQ.1) THEN
         CALL QCDSET (NAME,VALUE,IRET)
      ELSEIF (IACT.EQ.2) THEN
         CALL QCDGET (NAME,VALUE,IRET)
      ELSEIF (IACT.EQ.3) THEN
         CALL QCDIN
         IRET=4
      ELSEIF (IACT.EQ.4) THEN
         CALL QCDOUT(NINT(VALUE))
         IRET=4
      ELSE
         IRET=3
      ENDIF
      RETURN
      END
      SUBROUTINE QCDSET (NAME,VALUE,IRET)
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      CHARACTER*(*) NAME
      COMMON / COMQMS / VALMAS(9)
      COMMON / QCDPAR / AL, NF, NORDER, SET
      LOGICAL SET
      PARAMETER (PI=3.1415927, EULER=0.57721566)
      IVALUE = NINT(VALUE)
      ICODE  = NAMQCD(NAME)
      IF (ICODE .EQ. 0) THEN
         IRET=0
      ELSE
         IRET = 1
         SET = .FALSE.
         IF (ICODE .EQ. 1) THEN
            IF (VALUE.LE.0) GOTO 12
            AL=VALUE
         ELSEIF (ICODE .EQ. 2) THEN
            IF ( (IVALUE .LT. 0) .OR. (IVALUE .GT. 9)) GOTO 12
            NF = IVALUE
         ELSEIF ((ICODE .GE. 3) .AND. (ICODE .LE. 11))  THEN
            IF (VALUE .LT. 0) GOTO 12
            Scle = Min (Value , VALMAS(ICODE - 2))
            AlfScle = Alpi(Scle) * Pi
            VALMAS(ICODE - 2) = VALUE
            Call AlfSet (Scle, AlfScle)
         ELSEIF ((ICODE .GE. 13) .AND. (ICODE .LE. 13+NF))  THEN
            IF (VALUE .LE. 0) GOTO 12
            CALL SETL1 (ICODE-13, VALUE)
         ELSEIF (ICODE .EQ. 24)  THEN
            IF ((IVALUE .LT. 1) .OR. (IVALUE .GT. 2)) GOTO 12
            NORDER = IVALUE
         ENDIF
         IF (.NOT. SET) CALL LAMCWZ
      ENDIF
      RETURN
 12   IRET=2
      RETURN
      END
      SUBROUTINE QCDGET(NAME,VALUE,IRET)
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      CHARACTER*(*) NAME
      COMMON / CWZPRM / ALAM(0:9), AMHAT(0:9), AMN, NHQ
      COMMON / QCDPAR / AL, NF, NORDER, SET
      COMMON / COMQMS / VALQMS(9)
      LOGICAL SET
      PARAMETER (PI=3.1415927, EULER=0.57721566)
      ICODE = NAMQCD(NAME)
      IRET = 1
      IF (ICODE .EQ. 1) THEN
         VALUE = AL
      ELSEIF (ICODE .EQ. 2) THEN
         VALUE = NF
      ELSEIF ((ICODE .GE. 3) .AND. (ICODE .LE. 12))  THEN
         VALUE = VALQMS(ICODE - 2)
      ELSEIF ((ICODE .GE. 13) .AND. (ICODE .LE. 13+NF))  THEN
         VALUE = ALAM(ICODE - 13)
      ELSEIF (ICODE .EQ. 24) THEN
         VALUE = NORDER
      ELSE
         IRET=0
      ENDIF
      END
      SUBROUTINE QCDIN
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      COMMON /IOUNIT/ NIN, NOUT, NWRT
      DIMENSION VALMAS(9)
      CHARACTER ONECH*(1)
      ONECH = '0'
      IASC0 = ICHAR(ONECH)
      CALL QCDGET ('LAM',ALAM,IRET1)
      CALL QCDGET ('NFL',ANF,IRET2)
      CALL QCDGET ('ORDER',ORDER,IRET3)
      NF = NINT(ANF)
      NORDER = NINT(ORDER)
 1    WRITE (NOUT, *) 'LambdaMSBAR, # Flavors, loop order ?'
      READ (NIN,*, IOSTAT = IRET) ALAM, NF, NORDER
      ORDER = NORDER
      ANF = NF
      IF (IRET .LT. 0) GOTO 22
      IF (IRET .EQ. 0) THEN
         CALL QCDSET ('LAM',ALAM,IRET1)
         CALL QCDSET ('NFL',ANF,IRET2)
         CALL QCDSET ('ORDER',ORDER,IRET3)
         ENDIF
      IF ((IRET.NE.0) .OR. (IRET1.NE.1) .OR. (IRET2.NE.1)
     >     .OR. (IRET3.NE.1)) THEN
         WRITE (NOUT, *) 'Bad value(s), try again.'
         GOTO 1
         ENDIF
      DO 20, I = 1, NF
         CALL QCDGET('M'//CHAR(I+IASC0),VALMAS(I),IRET1)
 10      WRITE (NOUT, '(1X,A,I2,A)') 'Mass of Quark', I, '?'
         READ (NIN,*, IOSTAT=IRET) VALMAS(I)
         IF (IRET .LT. 0) GOTO 22
         IF (IRET .EQ. 0)
     >      CALL QCDSET('M'//CHAR(I+IASC0),VALMAS(I),IRET1)
         IF ((IRET .NE. 0) .OR. (IRET1 .NE. 1)) THEN
            WRITE (NOUT, *) 'Bad value, try again.'
            GOTO 10
            ENDIF
 20      CONTINUE
      RETURN
 22   WRITE (NOUT, *) 'END OF FILE ON INPUT'
      WRITE (NOUT, *)
      RETURN
      END
      SUBROUTINE QCDOUT(NOUT)
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      COMMON /QCDPAR/ AL, NF, NORDER, SET
      COMMON / CWZPRM / ALAM(0:9), AMHAT(0:9), AMN, NHQ
      COMMON /COMQMS/ VALQMS(9)
      LOGICAL SET
      IF (.NOT. SET) CALL LAMCWZ
c      WRITE (NOUT,110) AL, NF, NORDER
 110   FORMAT(
     1 ' Lambda (MSBAR) =',G13.5,', NFL (total # of Flavors) =',I3,
     2 ', Order (loops) =', I2)
c      WRITE (NOUT,120) (I,VALQMS(I),I=1,NF)
 120   FORMAT (3(' M', I1, '=', G13.5, :, ','))
c      IF (NHQ .GT. 0)
c     1   WRITE (NOUT,130) (I, ALAMF(I), I = NF-NHQ, NF)
 130   FORMAT (: ' ! Effective lambda given number of light quarks:'/
     >    (2(' ! ', I1, ' quarks => lambda = ', G13.5 : '; ')) )
      RETURN
      END
      SUBROUTINE SETLAM (NEF, WLAM, IRDR)
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      COMMON / IOUNIT / NIN, NOUT, NWRT
      COMMON / QCDPAR / AL, NF, NORDER, SET
      LOGICAL SET
      IF ((NEF .LT. 0) .OR. (NEF .GT. NF)) THEN
         WRITE(NOUT,*)'NEF out of range in SETLAM, NEF, NF=', NEF,NF
         STOP
      ENDIF
      VLAM = WLAM
      IF (IRDR .NE. NORDER) CALL CNVL1 (IRDR, NORDER, NEF, VLAM)
      CALL SETL1 (NEF, VLAM)
      END
      FUNCTION NFL(AMU)
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      COMMON / CWZPRM / ALAM(0:9), AMHAT(0:9), AMN, NHQ
      COMMON /QCDPAR/ AL, NF, NORDER, SET
      LOGICAL SET
      IF (.NOT. SET) CALL LAMCWZ
      NFL = NF - NHQ
      IF ((NFL .EQ. NF) .OR. (AMU .LE. AMN)) GOTO 20
      DO 10 I = NF - NHQ + 1, NF
         IF (AMU .GE. AMHAT(I)) THEN
            NFL = I
         ELSE
            GOTO 20
         ENDIF
10       CONTINUE
20    RETURN
      END
      Function SetQCD ()
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      External DatQCD
      SetQCD = 0.
      Return
      END
      SUBROUTINE WARNI (IWRN, NWRT, MSG, NMVAR, IVAB,
     >                  IMIN, IMAX, IACT)
      CHARACTER*(*) MSG, NMVAR
      Data Nmax / 1000 /
      IW = IWRN
      IV = IVAB
      IF  (IW .EQ. 0) THEN
c         PRINT '(1X,A/1X, 2A,I10 /A,I4)', MSG, NMVAR, ' = ', IV,
c     >         ' For all warning messages, check file unit #', NWRT
         IF (IACT .EQ. 1) THEN
c         PRINT       '(A/2I10)', ' The limits are: ', IMIN, IMAX
         WRITE (NWRT,'(A/2I10)') ' The limits are: ', IMIN, IMAX
         ENDIF
      ENDIF
      If (Iw .LT. Nmax) Then
         WRITE (NWRT,'(1X,A/1X,2A, I10)') MSG, NMVAR, ' = ', IV
      Elseif (Iw .Eq. Nmax) Then
c         Print '(/A/)', '!!! Severe Warning, Too many errors !!!'
c         Print '(/A/)', '    !!! Check The Error File !!!'
         Write (Nwrt, '(//A//)')
     >     'Too many warnings, Message suppressed !!'
      Endif
      IWRN = IW + 1
      RETURN
      END
      SUBROUTINE UpC (A, La, UpA)
      CHARACTER A*(*), UpA*(*), C*(1)
      INTEGER I, La, Ld
      La = Len(A)
      Lb = Len(UpA)
      If (Lb .Lt. La) Stop 'UpCase conversion length mismatch!'
      Ld = ICHAR('A')-ICHAR('a')
      DO 1 I = 1, Lb
        If (I .Le. La) Then
         c = A(I:I)
         IF ( LGE(C, 'a') .AND. LLE(C, 'z') ) THEN
           UpA (I:I) = CHAR(Ichar(c) + ld)
         Else
           UpA (I:I) = C
         ENDIF
        Else
         UpA (I:I) = ' '
        Endif
 1    CONTINUE
      RETURN
      END
      FUNCTION SMPNOL (NX, DX, FN, ERR)
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      DIMENSION FN(NX)
      MS = MOD(NX, 2)
      IF (NX .LE. 1 .OR. NX .GT. 1000) THEN
c         PRINT *, 'NX =', NX, ' OUT OF RANGE IN SMPNOL!'
         STOP
      ELSEIF (NX .EQ. 2) THEN
         TEM = DX * FN(2)
      ELSEIF (NX .EQ. 3) THEN
         TEM = DX * FN(2) * 2.
      ELSE
         IF (MS .EQ. 0) THEN
            TEM = DX * (23.* FN(2) - 16.* FN(3) + 5.* FN(4)) / 12.
            TMP = DX * (3.* FN(2) - FN(3)) / 2.
            ERR = ABS(TEM - TMP)
            TEM = TEM + SMPSNA (NX-1, DX, FN(2), ER1)
            ERR = ABS(ER1) + ERR
         ELSE
            TEM = DX * (8.* FN(2) - 4.* FN(3) + 8.* FN(4)) / 3.
            TMP = DX * (3.* FN(2) + 2.* FN(3) + 3.* FN(4)) / 2.
            ERR = ABS(TEM - TMP)
            TEM = TEM + SMPSNA (NX-4, DX, FN(5), ER1)
            ERR = ABS(ER1) + ERR
         ENDIF
      ENDIF
      SMPNOL = TEM
      RETURN
      END
      FUNCTION ZBRNT(FUNC, X1, X2, TOL, IRT)
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      PARAMETER (ITMAX = 1000, EPS = 3.E-12)
      external func
      IRT = 0
      TOL = ABS(TOL)
      A=X1
      B=X2
      FA=FUNC(A)
      FB=FUNC(B)
      IF(FB*FA.GT.0.)  THEN
c        PRINT *, 'Root must be bracketed for ZBRNT.'
        IRT = 1
        RETURN
      ENDIF
      FC=FB
      DO 11 ITER=1,ITMAX
        IF(FB*FC.GT.0.) THEN
          C=A
          FC=FA
          D=B-A
          E=D
        ENDIF
        IF(ABS(FC).LT.ABS(FB)) THEN
          A=B
          B=C
          C=A
          FA=FB
          FB=FC
          FC=FA
        ENDIF
        TOL1=2.*EPS*ABS(B)+0.5*TOL
        XM=.5*(C-B)
        IF(ABS(XM).LE.TOL1 .OR. FB.EQ.0.)THEN
          ZBRNT=B
          RETURN
        ENDIF
        IF(ABS(E).GE.TOL1 .AND. ABS(FA).GT.ABS(FB)) THEN
          S=FB/FA
          IF(A.EQ.C) THEN
            P=2.*XM*S
            Q=1.-S
          ELSE
            Q=FA/FC
            R=FB/FC
            P=S*(2.*XM*Q*(Q-R)-(B-A)*(R-1.))
            Q=(Q-1.)*(R-1.)*(S-1.)
          ENDIF
          IF(P.GT.0.) Q=-Q
          P=ABS(P)
          IF(2.*P .LT. MIN(3.*XM*Q-ABS(TOL1*Q),ABS(E*Q))) THEN
            E=D
            D=P/Q
          ELSE
            D=XM
            E=D
          ENDIF
        ELSE
          D=XM
          E=D
        ENDIF
        A=B
        FA=FB
        IF(ABS(D) .GT. TOL1) THEN
          B=B+D
        ELSE
          B=B+SIGN(TOL1,XM)
        ENDIF
        FB=FUNC(B)
11    CONTINUE
c      PRINT *, 'ZBRNT exceeding maximum iterations.'
      IRT = 2
      ZBRNT=B
      RETURN
      END
      FUNCTION SMPSNA (NX, DX, F, ERR)
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      PARAMETER (D0=0D0, D1=1D0, D2=2D0, D3=3D0, D4=4D0, D10=1D1)
      PARAMETER (MAXX = 1000)
      COMMON / IOUNIT / NIN, NOUT, NWRT
      DIMENSION F(NX)
      DATA IW1, IW2, TINY / 2*0, 1.E-35 /
      IF (DX .LE. 0.) THEN
        CALL WARNR(IW2,NWRT,'DX cannot be < 0. in SMPSNA', 'DX', DX,
     >               D0, D1, 0)
        SMPSNA = 0.
        RETURN
      ENDIF
      IF (NX .LE. 0 .OR. NX .GT. MAXX) THEN
        CALL WARNI(IW1, NWRT, 'NX out of range in SMPSNA', 'NX', NX,
     >               1, MAXX, 1)
        SIMP = 0.
      ELSEIF (NX .EQ. 1) THEN
        SIMP = 0.
      ELSEIF (NX .EQ. 2) THEN
        SIMP = (F(1) + F(2)) / 2.
        ERRD = (F(1) - F(2)) / 2.
      ELSE
        MS = MOD(NX, 2)
        IF (MS .EQ. 0) THEN
          ADD = (9.*F(NX) + 19.*F(NX-1) - 5.*F(NX-2) + F(NX-3)) / 24.
          NZ = NX - 1
        ELSE
          ADD = 0.
          NZ = NX
        ENDIF
        IF (NZ .EQ. 3) THEN
          SIMP = (F(1) + 4.* F(2) + F(3)) / 3.
          TRPZ = (F(1) + 2.* F(2) + F(3)) / 2.
        ELSE
          SE = F(2)
          SO = 0
          NM1 = NZ - 1
          DO 60 I = 4, NM1, 2
            IM1 = I - 1
            SE = SE + F(I)
            SO = SO + F(IM1)
   60     CONTINUE
          SIMP = (F(1) + 4.* SE + 2.* SO + F(NZ)) / 3.
          TRPZ = (F(1) + 2.* (SE + SO) + F(NZ)) / 2.
        ENDIF
        ERRD = TRPZ - SIMP 
        SIMP = SIMP + ADD
      ENDIF
      SMPSNA = SIMP * DX
      IF (ABS(SIMP) .GT. TINY) THEN
        ERR = ERRD / SIMP
      ELSE
        ERR = 0.
      ENDIF
      RETURN
      END
      SUBROUTINE SNEVL(IKNL, NX, NT, JT, DT, TIN, NEFF, UI, GI, US, GS)
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      PARAMETER (MXX = 105, MXQ = 25, MXF = 6)
      PARAMETER (MXQX= MXQ * MXX)
      PARAMETER (M1=-3, M2=3, NDG=3, NDH=NDG+1, L1=M1-1, L2=M2+NDG-2)
      COMMON / IOUNIT / NIN, NOUT, NWRT
      COMMON / ANOMS1 / GG, GF, FG, FF, GPL, GMI
      COMMON / ANOMS2 / SGG, SGF, SFG, SFF, TGG, TGF, TFG, TFF
      COMMON / VARIBX / XA(MXX, L1:L2), ELY(MXX), DXTZ(MXX)
      DIMENSION UI(NX), US(0:NX, 0:NT)
      DIMENSION GI(NX), GS(0:NX, 0:NT)
      DIMENSION Y0(MXX), Y1(MXX), YP(MXX), F0(MXX), F1(MXX), FP(MXX)
      DIMENSION Z0(MXX), Z1(MXX), ZP(MXX), G0(MXX), G1(MXX), GP(MXX)
      DATA D0 / 0.0 /
      ESF(T) = 4./ (33.- 2.*NEFF) * LOG((T - TLAM)/(TIN - TLAM))
      N5 = (NX - 1) / 5
      JTT = 2 * JT
      DDT = DT / JTT
      IF (NX .GT. MXX) THEN
      WRITE (NOUT,*) 'Nx =', NX, ' greater than Max # of pts in SNEVL.'
      STOP 'Program stopped in SNEVL'
      EndIf
      TMD = TIN + DT * NT / 2.
      AMU = EXP(TMD)
      TEM = 6./ (33.- 2.* NEFF) / ALPI(AMU)
      TLAM = TMD - TEM
      DO 9 IX = 1, NX
      US (IX, 0) = UI(IX)
      GS (IX, 0) = GI(IX)
    9 CONTINUE
      US ( 0, 0) = (UI(1) - UI(2))* 3D0 + UI(3)
      GS ( 0, 0) = (GI(1) - GI(2))* 3D0 + GI(3)
      TT = TIN
      DO 10 IZ = 1, NX
      Y0(IZ) = UI(IZ)
      Z0(IZ) = GI(IZ)
   10 CONTINUE
      DO 20 IS = 1, NT
         DO 202 JS = 1, JTT
            IRND = (IS-1) * JTT + JS
            IF (IRND .EQ. 1) THEN
                CALL SNRHS (TT, NEFF, Y0, Z0,  F0, G0)
                DO 250 IZ = 1, NX
                   Y0(IZ) = Y0(IZ) + DDT * F0(IZ)
                   Z0(IZ) = Z0(IZ) + DDT * G0(IZ)
  250           CONTINUE
                TT = TT + DDT
                CALL SNRHS (TT, NEFF, Y0, Z0,  F1, G1)
                DO 251 IZ = 1, NX
                   Y1(IZ) = UI(IZ) + DDT * (F0(IZ) + F1(IZ)) / 2D0
                   Z1(IZ) = GI(IZ) + DDT * (G0(IZ) + G1(IZ)) / 2D0
  251           CONTINUE
            Else
                CALL SNRHS (TT, NEFF, Y1, Z1,  F1, G1)
                DO 252 IZ = 1, NX
                   YP(IZ) = Y1(IZ) + DDT * (3D0 * F1(IZ) - F0(IZ)) / 2D0
                   ZP(IZ) = Z1(IZ) + DDT * (3D0 * G1(IZ) - G0(IZ)) / 2D0
  252           CONTINUE
                TT = TT + DDT
                CALL SNRHS (TT, NEFF, YP, ZP,  FP, GP)
                DO 253 IZ = 1, NX
                   Y1(IZ) = Y1(IZ) + DDT * (FP(IZ) + F1(IZ)) / 2D0
                   Z1(IZ) = Z1(IZ) + DDT * (GP(IZ) + G1(IZ)) / 2D0
                   F0(IZ) = F1(IZ)
                   G0(IZ) = G1(IZ)
  253           CONTINUE
            EndIf
  202    CONTINUE
         DO 260 IX = 1, NX
           IF (IKNL .GT. 0) THEN
            US (IX, IS) = MAX(Y1(IX), D0)
            GS (IX, IS) = MAX(Z1(IX), D0)
           Else
            US (IX, IS) = Y1(IX)
            GS (IX, IS) = Z1(IX)
           EndIf
  260    CONTINUE
         US(0, IS) = 3D0*Y1(1) - 3D0*Y1(2) + Y1(3)
         GS(0, IS) = 3D0*Z1(1) - 3D0*Z1(2) + Z1(3)
   20 CONTINUE
      RETURN
      END
      SUBROUTINE NSRHSP (TT, NEFF, FI, FO)
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      LOGICAL LSTX
      PARAMETER (MXX = 105, MXQ = 25, MXF = 6)
      PARAMETER (M1=-3, M2=3, NDG=3, NDH=NDG+1, L1=M1-1, L2=M2+NDG-2)
      COMMON / VARIBX / XA(MXX, L1:L2), ELY(MXX), DXTZ(MXX)
      COMMON / XXARAY / XCR, XMIN, XV(0:MXX), LSTX, NX
      COMMON / XYARAY / ZZ (MXX, MXX)
      COMMON / KRNL01 / AFF1 (MXX), AFF2 (MXX), AGG1 (MXX), AGG2 (MXX),
     >                  AFG2, AGF2, ANSP (MXX), ANSM (MXX), AQQB
      COMMON / KRN2ND / FFG(MXX, MXX), GGF(MXX, MXX), PNS(MXX, MXX)
      COMMON / EVLPAC / AL, IKNL, IPD0, IHDN, KF
      DIMENSION G1(MXX), FI(NX), FO(NX)
      DIMENSION W0(MXX), W1(MXX), WH(MXX)
      DATA IRUN / 0 /
      S = EXP(TT)
      Q = AL * EXP (S)
      CPL = ALPI(Q)
      CPL2= CPL ** 2 / 2. * S
      CPL = CPL * S
      CALL INTEGR (NX, 0, FI, W0, IR1)
      CALL INTEGR (NX, 1, FI, W1, IR2)
      CALL HINTEG (NX,    FI, WH)
      DO 230 IZ = 1, NX
      FO(IZ) = 2.* FI(IZ) + 4./3.* ( 2.* WH(IZ) - W0(IZ) - W1(IZ))
      FO(IZ) = CPL * FO(IZ)
  230 CONTINUE
      IF (IKNL .EQ. 2) THEN
      DZ = 1./ (NX - 1)
      DO 21 IX = 1, NX-1
        X = XV(IX)
        NP = NX - IX + 1
        DO 31 KZ = 2, NP
          IY = IX + KZ - 1
          XY = ZZ (NX-IX+1, NX-IY+1)
          G1(KZ) = PNS (IX,IY) * (FI(IY) - XY * FI(IX))
   31   CONTINUE
        TEM1 = SMPNOL (NP, DZ, G1, ERR)
        TMP2 = (TEM1 + FI(IX) * (-ANSP(IX) + AQQB)) * CPL2
        FO(IX) = FO(IX) + TMP2
   21 CONTINUE
      EndIf
      RETURN
      END
      SUBROUTINE NSRHSM (TT, NEFF, FI, FO)
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      LOGICAL LSTX
      PARAMETER (MXX = 105, MXQ = 25, MXF = 6)
      PARAMETER (M1=-3, M2=3, NDG=3, NDH=NDG+1, L1=M1-1, L2=M2+NDG-2)
      COMMON / VARIBX / XA(MXX, L1:L2), ELY(MXX), DXTZ(MXX)
      COMMON / XXARAY / XCR, XMIN, XV(0:MXX), LSTX, NX
      COMMON / XYARAY / ZZ (MXX, MXX)
      COMMON / KRNL01 / AFF1 (MXX), AFF2 (MXX), AGG1 (MXX), AGG2 (MXX),
     >                  AFG2, AGF2, ANSP (MXX), ANSM (MXX), AQQB
      COMMON / KRN2ND / FFG(MXX, MXX), GGF(MXX, MXX), PNS(MXX, MXX)
      COMMON / EVLPAC / AL, IKNL, IPD0, IHDN, KF
      DIMENSION G1(MXX), FI(NX), FO(NX)
      DIMENSION W0(MXX), W1(MXX), WH(MXX)
      DATA IRUN / 0 /
      S = EXP(TT)
      Q = AL * EXP (S)
      CPL = ALPI(Q)
      CPL2= CPL ** 2 / 2. * S
      CPL = CPL * S
      CALL INTEGR (NX, 0, FI, W0, IR1)
      CALL INTEGR (NX, 1, FI, W1, IR2)
      CALL HINTEG (NX,    FI, WH)
      DO 230 IZ = 1, NX
      FO(IZ) = 2.* FI(IZ) + 4./3.* ( 2.* WH(IZ) - W0(IZ) - W1(IZ))
      FO(IZ) = CPL * FO(IZ)
  230 CONTINUE
      IF (IKNL .EQ. 2) THEN
      DZ = 1./ (NX - 1)
      DO 21 IX = 1, NX-1
        X = XV(IX)
        NP = NX - IX + 1
        IS = NP
        DO 31 KZ = 2, NP
          IY = IX + KZ - 1
          IT = NX - IY + 1
          XY = ZZ (IS, IT)
          G1(KZ) = PNS (IS,IT) * (FI(IY) - XY * FI(IX))
   31   CONTINUE
        TEM1 = SMPNOL (NP, DZ, G1, ERR)
        TMP2 = (TEM1 - FI(IX) * ANSM(IX)) * CPL2
        FO(IX) = FO(IX) + TMP2
   21 CONTINUE
      EndIf
      RETURN
      END
      SUBROUTINE SNRHS (TT, NEFF, FI, GI,  FO, GO)
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      LOGICAL LSTX
      PARAMETER (MXX = 105, MXQ = 25, MXF = 6)
      PARAMETER (M1=-3, M2=3, NDG=3, NDH=NDG+1, L1=M1-1, L2=M2+NDG-2)
      COMMON / VARIBX / XA(MXX, L1:L2), ELY(MXX), DXTZ(MXX)
      COMMON / XXARAY / XCR, XMIN, XV(0:MXX), LSTX, NX
      COMMON / XYARAY / ZZ (MXX, MXX)
      COMMON / KRNL01 / AFF1 (MXX), AFF2 (MXX), AGG1 (MXX), AGG2 (MXX),
     >                  AFG2, AGF2, ANSP (MXX), ANSM (MXX), AQQB
      COMMON / KRN2ND / FFG(MXX, MXX), GGF(MXX, MXX), PNS(MXX, MXX)
      COMMON / EVLPAC / AL, IKNL, IPD0, IHDN, KF
      DIMENSION GI(NX), GO(NX), G1(MXX), G2(MXX), G3(MXX), G4(MXX)
      DIMENSION FI(NX), FO(NX), W0(MXX), W1(MXX), WH(MXX), WM(MXX)
      DIMENSION R0(MXX), R1(MXX), R2(MXX), RH(MXX), RM(MXX)
      S = EXP(TT)
      Q = AL * EXP (S)
      CPL = ALPI(Q)
      CPL2= CPL ** 2 / 2. * S
      CPL = CPL * S
      CALL INTEGR (NX,-1, FI, WM, IR1)
      CALL INTEGR (NX, 0, FI, W0, IR2)
      CALL INTEGR (NX, 1, FI, W1, IR3)
      CALL INTEGR (NX,-1, GI, RM, IR4)
      CALL INTEGR (NX, 0, GI, R0, IR5)
      CALL INTEGR (NX, 1, GI, R1, IR6)
      CALL INTEGR (NX, 2, GI, R2, IR7)
      CALL HINTEG (NX,    FI, WH)
      CALL HINTEG (NX,    GI, RH)
      IF (IKNL .GT. 0) THEN
      DO 230 IZ = 1, NX
      FO(IZ) = ( 2D0 * FI(IZ)
     >      + 4D0 / 3D0 * ( 2D0 * WH(IZ) - W0(IZ) - W1(IZ) ))
     >      + NEFF * ( R0(IZ) - 2D0 * R1(IZ) + 2D0 * R2(IZ) )
      FO(IZ) = FO(IZ) * CPL
      GO(IZ) = 4D0 / 3D0 * ( 2D0 * WM(IZ) - 2D0 * W0(IZ)  + W1(IZ) )
     >      + (33D0 - 2D0 * NEFF) / 6D0 * GI(IZ)
     >      + 6D0 * (RH(IZ) + RM(IZ) - 2D0 * R0(IZ) + R1(IZ) - R2(IZ))
      GO(IZ) = GO(IZ) * CPL
  230 CONTINUE
      Else
      DO 240 IZ = 1, NX
      FO(IZ) = NEFF * (-R0(IZ) + 2.* R1(IZ) )
     > + 2.* FI(IZ) + 4./ 3.* ( 2.* WH(IZ) - W0(IZ) - W1(IZ) )
      FO(IZ) = FO(IZ) * CPL
      GO(IZ) = 4./ 3.* ( 2.* W0(IZ) - W1(IZ) )
     >+ (33.- 2.* NEFF) / 6.* GI(IZ) + 6.*(RH(IZ) + R0(IZ) - 2.* R1(IZ))
      GO(IZ) = GO(IZ) * CPL
  240 CONTINUE
      EndIf
      IF (IKNL .EQ. 2) THEN
      DZ = 1./(NX - 1)
      DO 21 I = 1, NX-1
        X = XV(I)
        NP = NX - I + 1
        IS = NP
        G2(1) = FFG(IS, IS) * GI(I)
        G3(1) = GGF(IS, IS) * FI(I)
        DO 31 KZ = 2, NP
          IY = I + KZ - 1
          IT = NX - IY + 1
          XY = ZZ (IS, IT)
          G1(KZ) = FFG(I, IY) * (FI(IY) - XY**2 *FI(I))
          G4(KZ) = GGF(I, IY) * (GI(IY) - XY**2 *GI(I))
          G2(KZ) = FFG(IS, IT) * GI(IY)
          G3(KZ) = GGF(IS, IT) * FI(IY)
   31   CONTINUE
        TEM1 = SMPNOL (NP, DZ, G1, ERR)
        TEM2 = SMPSNA (NP, DZ, G2, ERR)
        TEM3 = SMPSNA (NP, DZ, G3, ERR)
        TEM4 = SMPNOL (NP, DZ, G4, ERR)
        TEM1 = TEM1 - FI(I) * (AFF2(I) + AGF2)
        TEM4 = TEM4 - GI(I) * (AGG2(I) + AFG2)
        TMF = TEM1 + TEM2
        TMG = TEM3 + TEM4
        FO(I) = FO(I) + TMF * CPL2
        GO(I) = GO(I) + TMG * CPL2
   21 CONTINUE
      EndIf
      RETURN
      END
      SUBROUTINE INTEGR (NX, M, F,   G, IR)
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      CHARACTER MSG*80
      PARAMETER (MXX = 105, MXQ = 25, MXF = 6)
      PARAMETER (MXPN = MXF * 2 + 2)
      PARAMETER (MXQX= MXQ * MXX,   MXPQX = MXQX * MXPN)
      PARAMETER (M1=-3, M2=3, NDG=3, NDH=NDG+1, L1=M1-1, L2=M2+NDG-2)
      COMMON / IOUNIT / NIN, NOUT, NWRT
      COMMON / VARIBX / XA(MXX, L1:L2), ELY(MXX), DXTZ(MXX)
      COMMON / VARBAB / GB(NDG, NDH, MXX), H(NDH, MXX, M1:M2)
      DIMENSION   F(NX), G(NX)
      DATA IWRN1, IWRN2 / 0, 0 /
      IRR = 0
      IF (NX .LT. 1 .OR. XA(NX-1,1) .EQ. 0D0) THEN
        MSG = 'NX out of range in INTEGR call'
        CALL WARNI (IWRN1, NWRT, MSG, 'NX', NX, 0, MXX, 0)
        IRR = 1
      EndIf
      IF (M .LT. M1 .OR. M .GT. M2) THEN
        MSG ='Exponent M out of range in INTEGR'
        CALL WARNI (IWRN2, NWRT, MSG, 'M', M, M1, M2, 1)
        IRR = 2
      EndIf
      G(NX) = 0D0
      TEM = H(1, NX-1, -M) * F(NX-2) + H(2, NX-1, -M) * F(NX-1)
     >    + H(3, NX-1, -M) * F(NX)
      IF (M .EQ. 0) THEN
         G(NX-1) = TEM
      Else
         G(NX-1) = TEM * XA(NX-1, M)
      EndIf
      DO 10 I = NX-2, 2, -1
         TEM = TEM + H(1,I,-M)*F(I-1) + H(2,I,-M)*F(I)
     >             + H(3,I,-M)*F(I+1) + H(4,I,-M)*F(I+2)
         IF (M .EQ. 0) THEN
            G(I) = TEM
         Else
            G(I) = TEM * XA(I, M)
         EndIf
   10 CONTINUE
      TEM = TEM + H(2,1,-M)*F(1) + H(3,1,-M)*F(2) + H(4,1,-M)*F(3)
      IF (M .EQ. 0) THEN
         G(1) = TEM
      Else
         G(1) = TEM * XA(1, M)
      EndIf
      IR = IRR
      RETURN
      END
      SUBROUTINE HINTEG (NX, F, H)
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      PARAMETER (MXX = 105, MXQ = 25, MXF = 6)
      PARAMETER (MXPN = MXF * 2 + 2)
      PARAMETER (MXQX= MXQ * MXX,   MXPQX = MXQX * MXPN)
      PARAMETER (M1=-3, M2=3, NDG=3, NDH=NDG+1, L1=M1-1, L2=M2+NDG-2)
      COMMON / IOUNIT / NIN, NOUT, NWRT
      COMMON / HINTEC / GH(NDG, MXX)
      COMMON / VARIBX / XA(MXX, L1:L2), ELY(MXX), DXTZ(MXX)
      DIMENSION F(NX), H(NX), G(MXX)
      DZ = 1D0 / (NX-1)
      DO 20 I = 1, NX-2
         NP = NX - I + 1
         TEM = GH(1,I)*F(I) + GH(2,I)*F(I+1) + GH(3,I)*F(I+2)
         DO 30 KZ = 3, NP
            IY = I + KZ - 1
            W = XA(I,1) / XA(IY,1)
            G(KZ) = DXTZ(IY)*(F(IY)-W*F(I))/(1.-W)
   30    CONTINUE
         HTEM = SMPSNA (NP-2, DZ, G(3), ERR)
         TEM1 = F(I) * ELY(I)
         H(I) = TEM + HTEM + TEM1
   20 CONTINUE
      H(NX-1) = F(NX) - F(NX-1) + F(NX-1) * (ELY(NX-1) - XA(NX-1,0))
      H(NX)   = 0
      RETURN
      END
      FUNCTION ZFXL (XL)
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      COMMON / INVERT / ZZ
      X = EXP(XL)
      TT = ZFRMX (X) - ZZ
      ZFXL = TT
      RETURN
      END
      FUNCTION SPENCE (X)
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      EXTERNAL SPNINT, SPN2IN
      COMMON / SPENCC / XX
      DATA U1, AERR, RERR / 1.D0, 1.E-7, 5.E-3 /
      XX = X
      TEM = GAUSS(SPNINT, XX, U1, AERR, RERR, ERR, IRT)
      SPENCE = TEM
      RETURN
      ENTRY SPENC2 (X)
      XX = X
      TEM = GAUSS (SPN2IN, XX, U1, AERR, RERR, ERR, IRT)
      SPENC2 = TEM + LOG (XX) ** 2 / 2.
      RETURN
      END
      FUNCTION SPNINT (ZZ)
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      COMMON / SPENCC / X
      Z = ZZ
      TEM = LOG (1.- Z) / Z
      SPNINT = TEM
      RETURN
      ENTRY SPN2IN (ZZ)
      Z = ZZ
      TEM = LOG (1.+ X - Z) / Z
      SPN2IN = TEM
      RETURN
      END
      SUBROUTINE WARNR (IWRN, NWRT, MSG, NMVAR, VARIAB,
     >                  VMIN, VMAX, IACT)
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      PARAMETER (D0=0D0, D1=1D0, D2=2D0, D3=3D0, D4=4D0, D10=1D1)
      CHARACTER*(*) MSG, NMVAR
      Data Nmax / 1000 /
      IW = IWRN
      VR = VARIAB
      IF  (IW .EQ. 0) THEN
c         PRINT '(1X, A/1X,2A,1PD16.7/A,I4)', MSG, NMVAR, ' = ', VR,
c     >         ' For all warning messages, check file unit #', NWRT
         IF (IACT .EQ. 1) THEN
c         PRINT       '(A/2(1PE15.4))', ' The limits are: ', VMIN, VMAX
         WRITE (NWRT,'(A/2(1PE15.4))') ' The limits are: ', VMIN, VMAX
         ENDIF
      ENDIF
      If (Iw .LT. Nmax) Then
         WRITE (NWRT,'(I5, 2A/1X,2A,1PD16.7)') IW, '   ', MSG,
     >                  NMVAR, ' = ', VR
      Elseif (Iw .Eq. Nmax) Then
c         Print '(/A/)', '!!! Severe Warning, Too many errors !!!'
c         Print '(/A/)', '    !!! Check The Error File !!!'
         Write (Nwrt, '(//A//)')
     >     '!! Too many warnings, Message suppressed from now on !!'
      Endif
      IWRN = IW + 1
      RETURN
      END
      FUNCTION XFRMZ (Z)
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      LOGICAL LSTX
      PARAMETER (D0=0D0, D1=1D0, D2=2D0, D3=3D0, D4=4D0, D10=1D1)
      PARAMETER (MXX = 105)
      COMMON / IOUNIT / NIN, NOUT, NWRT
      COMMON / XXARAY / XCR, XMIN, XV(0:MXX), LSTX, NX
      COMMON / INVERT / ZZ
      EXTERNAL ZFXL
      DATA SML, IRUN, TEM, RER / 1E-5, 0, D1, 1E-3 /
      DATA E1, ZLOW, ZHIGH, IWRN1, IWRN2 / 1.00002, -10.0,1.00002, 2*0 /
    7 EPS = TEM * RER
      ZZ = Z
      IF (Z .LE. ZHIGH .AND. Z .GT. ZLOW) THEN
          XLA = LOG (XMIN) * 1.5
          XLB = 0.00001
          TEM = ZBRNT (ZFXL, XLA, XLB, EPS, IRT)
      Else
        CALL WARNR (IWRN2, NWRT, 'Z out of range in XFRMZ, X set=0.',
     >              'Z', Z, ZLOW, ZHIGH, 1)
        TEM = 0
      EndIf
      XFRMZ = EXP(TEM)
      RETURN
      END
      FUNCTION ZFRMX (XX)
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      LOGICAL LSTX
      PARAMETER (D0=0D0, D1=1D0, D2=2D0, D3=3D0, D4=4D0, D10=1D1)
      PARAMETER (MXX = 105)
      COMMON / IOUNIT / NIN, NOUT, NWRT
      COMMON / XXARAY / XCR, XMIN, XV(0:MXX), LSTX, NX
      DATA IWRN1,IWRN2,IWRN3,IWRN4, HUGE, TINY / 4*0, 1.E35, 1.E-35 /
      F(X) = (XCR-XMIN) * LOG (X/XMIN) + LOG (XCR/XMIN) * (X-XMIN)
      D(X) = (XCR-XMIN) / X          + LOG (XCR/XMIN)
      X = XX
      IF (X .GE. XMIN) THEN
         TEM = F(X) / F(D1)
      ElseIF (X .GE. D0) THEN
         X = MAX (X, TINY)
         TEM = F(X) / F(D1)
      Else
         CALL WARNR(IWRN1, NWRT, 'X out of range in ZFRMX, Z set to 99.'
     >             , 'X', X, TINY, HUGE, 1)
         TEM = 99.
         STOP
      EndIf
      ZFRMX = TEM
      RETURN
      ENTRY DZDX (XX)
      X = XX
      IF (X .GE. XMIN) THEN
         TEM = D(X) / F(D1)
      ElseIF (X .GE. D0) THEN
         X = MAX (X, TINY)
         TEM = D(X) / F(D1)
      Else
         CALL WARNR(IWRN1, NWRT, 'X out of range in DZDX, Z set to 99.'
     >             , 'X', X, TINY, HUGE, 1)
         TEM = 99.
         STOP
      EndIf
      DZDX = TEM
      RETURN
      END
