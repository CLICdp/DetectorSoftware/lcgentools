      BLOCK DATA BEAM
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      COMMON/isrcom/ BETA_,B_Ncl_,B_ips_,COEF,ITYPE_
      COMMON/beamst/BNcl,BIPS,NX
      DATA NX/0/ 

      DATA ITYPE_/-1/,BETA_/-1/,B_Ncl_/-1/,B_ips_/-1/

      END


      FUNCTION BS_H(ETAX,BNcl)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
*      write(*,*)ETAX,BNcl

      G1_3=           1.d0/2.678938534707740      
      G2_3= ETAX**(1.d0/3)/1.354117939426404D0/2
      G3_3= ETAX**(2.d0/3)/6


      S0=G1_3*GAMMAI(2,BNcl)+ G2_3*GAMMAI(3,BNcl)+G3_3*GAMMAI(4,BNcl)
      N=3

1     N=N+1
      G1_3= 3*G1_3*ETAX/(N*(N-1)*(N-2)*(N-3))
      DS=G1_3*GAMMAI(N+1,BNcl)      
      N=N+1
      G2_3= 3*G2_3*ETAX/(N*(N-1)*(N-2)*(N-3))
      DS=DS+G2_3*GAMMAI(N+1,BNcl)
      N=N+1
      G3_3= 3*G3_3*ETAX/(N*(N-1)*(N-2)*(N-3))
      DS=DS+G3_3*GAMMAI(N+1,BNcl)
  
      S0=S0+DS
      IF(DS.GT.1.E-8*S0) GOTO 1
      BS_H=S0
*        write(*,*) 'BS_H=',BS_H
      RETURN
      END


      FUNCTION BS_INT(Y)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      COMMON/beamst/BNcl,BIPS,NX 
          
      BS_INT=0
      X=1-Y**3
      IF(X.LE.0.D0) RETURN
      ETAX= 2/(3*BIPS)*(1/X-1)
      IF(ETAX.GT.100) return
      IF(NX.EQ.0) THEN
        BS_INT=3*(2/(3*BIPS))**(1.d0/3)*EXP(-ETAX)/(X**(4.d0/3 ))
     &      *BS_H(ETAX,BNcl)
      ELSE
        BS_INT=3*(2/(3*BIPS*x))**(1.d0/3)*EXP(-ETAX)*BS_H(ETAX,BNcl)
      ENDIF
*      write(*,*) y,bs_int
      RETURN
      END

      FUNCTION  CFbeam(Y)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      COMMON/isrcom/ BETA,B_Ncl,B_ips,COEF,ITYPE
      x=EXP(-Y)
*      write(*,*)'x=',x,BETA,B_Ncl,B_ips,COEF,ITYPE
      CFbeam=0
      IF(X.LE.0.D0) RETURN
      ETAX= 2/(3*B_IPS)*(1/X-1)
      IF(ETAX.GT.50) then
*       write(*,*)  'CFbeam(',x  ,')=', CFbeam

       return
      endif
      CFbeam =(2/(3*B_IPS))**(1.d0/3)*EXP(-ETAX)/(X**(4.d0/3 ))
     &      *BS_H(ETAX,B_Ncl)

      CFbeam=CFbeam*DIVY(Y)**(1.D0/3 -1)
*      write(*,*)  'CFbeam(',x  ,')=', CFbeam
      RETURN
      END



      FUNCTION  CFisr(Y)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      COMMON/isrcom/ BETA,B_Ncl,B_ips,COEF,ITYPE

      X=EXP(-Y)


      IF (ITYPE.EQ.2) THEN
        STRF=COEF
     &   *((1+x**2)-BETA*(log(x)*(1+3*x**2)/2+(1-x)**2)/2)/2
      ELSE       
         IF (X.GT.0.999999) THEN
           DlnXdX=(x-3)/2
           Dln1_x=log(0.0000001)
         ELSE
           DlnXdX=log(x)/(1-x)
           Dln1_x=log(1-x)
         ENDIF
      STRF=COEF -(1-x)**(1-beta)*((1+x)/2 +
     &beta*( 2*(1+x)*Dln1_x+2*DlnXdX-(3*(1+x)*log(x)-5-x)/2)/4)

      ENDIF

      CFisr=STRF*DIVY(Y)**(BETA-1)

*      write(*,*) 'CFisr(',x,')=', CFisr
      RETURN
      END





      SUBROUTINE INITISR(ITYPE,BETA,B_NCL,B_ipsilon, XI,YI,NNN)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION XI(500),YI(500)
      COMMON/isrcom/ BETA_,B_Ncl_,B_ips_,COEF,ITYPE_
      EXTERNAL CFisr,CFbeam
      DATA  NNN_/-1/
      DATA EGAM/0.5772156649d0/
      SAVE isrcom
      IF( (ITYPE.EQ.ITYPE_).AND.(BETA.EQ.BETA_).AND.(B_Ncl.EQ.B_Ncl_)
     & .AND.(B_ipsilon.EQ.B_ips_).AND.(NNN.EQ.NNN_) ) RETURN
      
      ITYPE_ =ITYPE
       BETA_ =BETA
      B_Ncl_ =B_Ncl
      B_ips_ =B_ipsilon
        NNN_ =NNN
      COEF=EXP(BETA*(0.75d0-EGAM))/DGAMMA(1+BETA)

      DO 100 I=1,NNN
        XI(I)= Dfloat(I-1)/NNN
        x=1-XI(I)**3 
        IF(ITYPE.EQ.1) THEN
           YI(I)=COEF -(1-x)**(1-beta)*((1+x)/2 +
     &beta*(2*(1+x)*log(1-x)+2*log(x)/(1-x)-(3*(1+x)*log(x)-5-x)/2)/4)
        ELSE
           YI(I)=COEF
     &   *((1+x**2)-BETA*(log(x)*(1+3*x**2)/2+(1-x)**2)/2)/2
        ENDIF 
              
        IF(B_Ncl.NE.0) THEN
        EPS=1.E-6
        B2=1.d0/3
*        write(*,*)x,-LOG(X)
        YI(I)=( YI(I)*(1-EXP(-B_Ncl)) + 
     &           (1-x)**B2*DIVY(-LOG(X))**(1-BETA-B2)*
     &           CONVOL(CFisr,CFbeam,BETA,B2,-LOG(X),EPS)
     &        )/B_Ncl
        
        ENDIF

*        write(*,*) x,yi(i),eps
100   CONTINUE

*        call testint(beta,NNN,XI,YI) 

      END


      FUNCTION FINT(X)
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION XI(500),YI(500),XX(500),YY(500)
*       write(*,*)x,beta_

      FINT=DINTER(x**(1/(3*beta_)),NNN_,XX,YY)
*       write(*,*) 
      return
      entry fint_(beta,NNN,XI,YI)
      NNN_=NNN
      beta_=beta
      DO 10 I=1,NNN
        XX(I)=XI(I)
        YY(I)=YI(I)
10    CONTINUE  
      fint_=0
      return
      end

      subroutine  testint(beta,NNN,XI,YI)
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION XI(500),YI(500)
      external fint,fint_
      qq=fint_(beta,NNN,XI,YI)
      eps=1.E-6
      qq=simps1(fint,0.d0,1.d0,eps)
      write(*,*) 'integral=',qq
      return
      end


