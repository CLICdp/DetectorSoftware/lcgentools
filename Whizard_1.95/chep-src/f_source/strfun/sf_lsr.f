      SUBROUTINE P_LSR(P_NAME,RES)      
C* Check particle name   
      IMPLICIT REAL*8 (A-H,O-Z)
      CHARACTER*60 NAME
      CHARACTER*6 P_NAME

      LOGICAL RES 
      CHARACTER*20 HEADER
      CHARACTER*10 BUFF1,BUFF2
      CHARACTER*500 STRMEN
      CHARACTER*15 CHM1,CHM2

      LOGICAL FIRST
      DATA FIRST /.TRUE./
      DATA X0/4.82D0/
      DATA XMAX/0.82817869415807d0/



      SAVE

      IF (P_NAME.EQ.'A') THEN 
         RES=.TRUE.
      ELSE
         RES=.FALSE.
      ENDIF

      RETURN

      ENTRY N_LSR(I,NAME)
C* Return NAME of function with paramiters 
        NAME= ' Real photon beam'  
      RETURN

      ENTRY R_LSR(I,NAME,BE,RES)
      BE=1
      IF ( NAME.NE.' Real photon beam'  ) GOTO 10
      RES=.TRUE.
      RETURN
10    RES=.FALSE.
      RETURN

      ENTRY M_LSR(I)
      RETURN

      ENTRY C_LSR(I,X,STRF)

      IF(FIRST) THEN
         FIRST=.FALSE.
         RNORMA=(1D0-4D0/X0-8D0/X0**2)*DLOG(1D0+X0)
     &          +0.5d0+8D0/X0-1D0/(2D0*(1D0+X0)**2)
      ENDIF
      IF (X.GT.XMAX) THEN
         STRF=0
      ELSE

         STRF=((1D0-X)+1D0/(1D0-X)*(1D0-4D0*X/X0
     &      *(1D0-X/(X0*(1D0-X)))))/RNORMA
      ENDIF
      RETURN
      END  



