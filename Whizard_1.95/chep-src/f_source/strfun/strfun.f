      SUBROUTINE SF_MENU(I)
      IMPLICIT REAL*8 (A-H,O-Z)
      CHARACTER*1000 STRMEN
      CHARACTER*6 P_NAME,PINF
      CHARACTER*60 NAME
      LOGICAL RES
      CHARACTER*60 SF_TXT(2)
      COMMON/SF_TXT/SF_TXT
      DIMENSION NFUN(15) 
      CHARACTER*30 PROCES
      COMMON/PROCES/NSUB,PROCES
      SAVE
     
      P_NAME=PINF(NSUB,I)  
      K=1
      STRMEN='% STRUCTURE FUNCTION MENU !%OFF '

      CALL P_EPA(P_NAME,RES)
      IF (RES) THEN
        CALL N_EPA(I,NAME)
        STRMEN(60*k:60*(K+1)) ='!'//NAME
        NFUN(K)=1
        K=K+1
      ENDIF

      CALL P_LSR(P_NAME,RES)
      IF (RES) THEN
        CALL N_LSR(I,NAME)
        STRMEN(60*k:60*(K+1)) ='!'//NAME
        NFUN(K)=2
        K=K+1
      ENDIF
  
      CALL P_EEA(P_NAME,RES)
      IF (RES) THEN
        CALL N_EEA(I,NAME)
        STRMEN(60*k:60*(K+1)) ='!'//NAME
        NFUN(K)=3
        K=K+1
      ENDIF

      CALL P_MRS(P_NAME,RES)
      IF (RES) THEN
        CALL N_MRS(I,NAME)
        STRMEN(60*k:60*(K+1)) ='!'//NAME
        NFUN(K)=4
        K=K+1
      ENDIF

      CALL P_CTQ(P_NAME,RES)
      IF (RES) THEN
        CALL N_CTQ(I,NAME)
        STRMEN(60*k:60*(K+1)) ='!'//NAME
        NFUN(K)=5
        K=K+1
      ENDIF

      CALL P_ISR(P_NAME,RES)
      IF (RES) THEN
        CALL N_ISR(I,NAME)
        STRMEN(60*k:60*(K+1)) ='!'//NAME
        NFUN(K)=6
        K=K+1
      ENDIF

      CALL P_PRV(P_NAME,RES)
      IF (RES) THEN
        CALL N_PRV(I,NAME)
        STRMEN(60*k:60*(K+1)) ='!'//NAME
        NFUN(K)=7
        K=K+1
      ENDIF


      
      STRMEN(60*k:60*K+3) ='!!!'

      CALL CLRSCR      
      MODE=MENU(STRMEN,'f_31')
      IF( MODE.EQ.0)  THEN
         RETURN
      ELSEIF ( MODE.EQ.1) THEN
         SF_TXT(I)='OFF'
      ELSE 
         GOTO (51,52,53,54,55,56,57 ), NFUN(MODE-1)
51       CALL M_EPA(I)
         CALL N_EPA(I,SF_TXT(I))
         GOTO 70
52       CALL M_LSR(I)
         CALL N_LSR(I,SF_TXT(I))
         GOTO 70
53       CALL M_EEA(I)
         CALL N_EEA(I,SF_TXT(I))
         GOTO 70
54       CALL M_MRS(I)
         CALL N_MRS(I,SF_TXT(I))
         GOTO 70
55       CALL M_CTQ(I)
         CALL N_CTQ(I,SF_TXT(I))
         GOTO 70
56       CALL M_ISR(I)
         CALL N_ISR(I,SF_TXT(I))
C*         CALL N_ISR(2,SF_TXT(2))
         GOTO 70
57       CALL M_PRV(I)
         CALL N_PRV(I,SF_TXT(I))
         GOTO 70
      ENDIF

70    RETURN    

      
      END
 
      
      FUNCTION  STRFUN(I,X)
      IMPLICIT REAL*8 (A-H,O-Z)
      LOGICAL STRFON      
      COMMON /STRFST/ XPARTN(2),BE(2),STRFON(2)
      CHARACTER*60 SF_TXT(2)      
      COMMON/SF_TXT/SF_TXT
      LOGICAL RES
      DIMENSION NFUN(2)
      SAVE

      IF(I.EQ.0) THEN
        DO 10 J=1,2
          STRFON(J)=.TRUE.
          IF (SF_TXT(J).EQ.'OFF') THEN
            STRFON(J)=.FALSE.
          ELSE
            CALL R_EPA(J,SF_TXT(J),BE(J),RES)
            IF (RES) THEN
              NFUN(J)=1
              GOTO 10
            ENDIF
            CALL R_LSR(J,SF_TXT(J),BE(J),RES)
            IF (RES) THEN
              NFUN(J)=2
              GOTO 10
            ENDIF
            CALL R_EEA(J,SF_TXT(J),BE(J),RES)
            IF (RES) THEN
              NFUN(J)=3
              GOTO 10
            ENDIF
            CALL R_MRS(J,SF_TXT(J),BE(J),RES)
            IF (RES) THEN
              NFUN(J)=4
              GOTO 10
            ENDIF
            CALL R_CTQ(J,SF_TXT(J),BE(J),RES)
            IF (RES) THEN
              NFUN(J)=5
              GOTO 10
            ENDIF
            CALL R_ISR(J,SF_TXT(J),BE(J),RES)
            IF (RES) THEN
              NFUN(J)=6
              GOTO 10
            ENDIF

            CALL R_PRV(J,SF_TXT(J),BE(J),RES)
            IF (RES) THEN
              NFUN(J)=7
              GOTO 10
            ENDIF
            
            WRITE(*,90) J
90          FORMAT('Alien  structure function for ',I1,' particle')
            SF_TXT(J)='OFF'
            STRFON(J)=.FALSE.
            WRITE(*,*) 'Stucture function is switched OFF'  
            WRITE(*,*) 'Press ENTER to continue'
            READ(*,*)
            NFUN(J)=0            
          ENDIF
10        CONTINUE
        STRFUN=0
      ELSE
        GOTO (51,52,53,54,55,56,57),NFUN(I)
51      CALL C_EPA(I,X,F)
        GOTO 100
52      CALL C_LSR(I,X,F)
        GOTO 100
53      CALL C_EEA(I,X,F)
        GOTO 100 
54      CALL C_MRS(I,X,F)
        GOTO 100 
55      CALL C_CTQ(I,X,F)
        GOTO 100 
56      CALL C_ISR(I,X,F)
        GOTO 100
57      CALL C_PRV(I,X,F)
        GOTO 100 
      ENDIF
100   STRFUN=F
      END

      SUBROUTINE INI_SF(MODE)
      IMPLICIT REAL*8 (A-H,O-Z)
      CHARACTER*60 SF_TXT(2)      
      COMMON/SF_TXT/SF_TXT

      SF_TXT(1)='OFF'
      SF_TXT(2)='OFF'
      RETURN

      ENTRY RD_SF(MODE)
       READ(MODE,FMT='(1x,A60)') SF_TXT(1)
       READ(MODE,FMT='(1x,A60)') SF_TXT(2)
       DUMMY=STRFUN(0,1.D0)
      RETURN
      ENTRY WRT_SF(MODE)
       WRITE(MODE,FMT='(1x,A60)') SF_TXT(1)
       WRITE(MODE,FMT='(1x,A60)') SF_TXT(2)

      RETURN
      END
