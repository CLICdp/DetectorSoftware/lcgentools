
**********************************************************
*  Equivalent photon approximation structure function.   *
*     Improved Weizsaecker-Williams formula              *
*      C.F.Weizsaecker, Z.Phys. 88 (1934) 612            *
*      E.J.Williams,    Phys.Rev. 45 (1934) 729          *
*                                                        *
*   V.M.Budnev et al., Phys.Rep. 15C (1975) 181          *
**********************************************************

      SUBROUTINE P_EPA(P_NAME,RES)      
C* Check particle name   
      IMPLICIT REAL*8 (A-H,O-Z)
      CHARACTER*60 NAME
      CHARACTER*6 P_NAME
      DIMENSION XIN(2),Qmax(2)
      LOGICAL RES 
      CHARACTER*20 HEADER
      CHARACTER*10 BUFF1,BUFF2
      CHARACTER*500 STRMEN
      CHARACTER*15 CHM1,CHM2
      COMMON /SQS/ SQRTS
C*      ALFA =1./137.
      DATA ALFA/0.0072992701/
      DATA PI/3.1415927/
      DATA XIN/0.000511,0.000511/, Qmax/100,100/

      SAVE

      IF (P_NAME.EQ.'A') THEN 
         RES=.TRUE.
      ELSE
         RES=.FALSE.
      ENDIF

      RETURN

      ENTRY N_EPA(I,NAME)
C* Return NAME of function with paramiters 
        WRITE(NAME,FMT='(A20,A8,F10.6,A8,F10.4,A2)')
     &'Equiv. Photon Appr. ', ' (  mIn=',XIN(I),' |Q|max=',Qmax(I),' )'  
      RETURN

      ENTRY R_EPA(I,NAME,BE,RES)
      BE=1
      READ(NAME,FMT='(A20,A8,F10.6,A8,F10.4)',ERR=10)
     &HEADER,BUFF1,XIN_,BUFF2,Qmax_
      IF ( HEADER.NE.'Equiv. Photon Appr. ') GOTO 10
      IF (XIN_ .LE.0) GOTO 10
      IF (Qmax_.LE.0) GOTO 10
      RES=.TRUE.
      XIN(I)=XIN_
      Qmax(I)=Qmax_
      RETURN
10    RES=.FALSE.
      RETURN

      ENTRY M_EPA(I)
   
20    CALL CLRSCR
      WRITE(CHM1,FMT='(G12.3)') XIN(I)
      WRITE(CHM2,FMT='(G12.3)') Qmax(I) 
     

      STRMEN = '%Equiv. Photon Appr. menu!%'//
     & 'Incoming lepton mass = '//CHM1//
     & '!|Q|max  =' //CHM2 //
     & '!!!'
      
      
      MODE=MENU(STRMEN,'SF_EPA')
      IF(MODE.EQ.0) THEN
         RETURN
      ELSEIF (MODE.EQ.1) THEN 
         WRITE(*,FMT='(''New value = '',$)')
         READ(*,*) XX
         IF (XX.GT.0) XIN(I)=XX
      ELSEIF (MODE.EQ.2) THEN
         WRITE(*,FMT='(''New value = '',$)')
         READ(*,*) XX         
         IF(XX.GT.0) Qmax(I)=XX 
      ELSE
         CONTINUE
      ENDIF
      GOTO 20

      ENTRY C_EPA(I,X,STRF)

         DELT=(XIN(I)/Qmax(I))**2
         STRF=(ALFA/(2*PI))*(LOG((1-X)/(X**2*DELT))*(1+(1-X)**2)/X
     &        -2*(1-X-DELT*X**2)/X)

      RETURN
      END  




     
     
