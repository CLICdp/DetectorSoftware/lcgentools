* E.A.Kuraev,V.S.Fadin:Sov.J.Nucl.Phys.41(1985)466
* S.Jadach,B.F.L.Ward:Comp.Phys.Commun.56(1990)351
      SUBROUTINE P_ISR(P_NAME,RES)      
C* Check particle name   
      IMPLICIT REAL*8 (A-H,O-Z)
      CHARACTER*60 NAME
      CHARACTER*6 P_NAME
      DIMENSION XI(500),YI(500)
      LOGICAL RES 
      CHARACTER*20 HEADER
      CHARACTER*12 BUFF1
      CHARACTER*500 STRMEN
      CHARACTER*15 CHM1,CHM2,CHM3
      COMMON /SQS/ SQRTS
      DATA PI/3.1415927/
      DATA SCALE/100/
      DATA ITYPE/2/
      DATA B_Ncl/2.0/
      DATA B_ipsilon/0.03/


      SAVE

      IF ((P_NAME.EQ.'e1').OR.(P_NAME.EQ.'E1')) THEN 
         RES=.TRUE.
      ELSE
         RES=.FALSE.
      ENDIF

      RETURN

      ENTRY N_ISR(I,NAME)
C* Return NAME of function with paramiters 
      WRITE(NAME,FMT='(A17,A12,3('','',G8.2),A1)')
     &'ISR&Beamstrahlung','(Parameters=',SCALE,
     & B_ncl,B_ipsilon,')'
 
      RETURN

      ENTRY R_ISR(I,NAME,BE,RES)
      READ(NAME,FMT='(A17,A12,3(1x,G8.2))',ERR=10)
     &HEADER,BUFF1,SCALE,B_ncl,B_ipsilon
      IF (HEADER.NE.'ISR&Beamstrahlung') GOTO 10
      RES=.TRUE.

      PI=4.d0*atan(1.d0)
*** mass of electron (in GeV)
      EM=0.51099906d-03
*** fine structure constant
      ALPHA=1.d0/137.0359895d0
      BETA   = ALPHA/PI*(2.d0*LOG(SCALE/EM)-1.d0)

*** Euler constant and Riemann numbers
      EGAM   = 0.5772156649d0
      RKSI2  = PI**2/6.d0
      RKSI3  = 1.2020569031d0
      RKSI4  = PI**4/90.d0
      RKSI5  = 1.0369277551d0
*** Euler functon GAMMA(1+beta/2) in its Teylor expansion
      BE=BETA
      GAMMA = EXP(-EGAM*BE
     &            +RKSI2/2.D0*BE**2
     &            -RKSI3/3.D0*BE**3
     &            +RKSI4/4.D0*BE**4
     &            -RKSI5/5.D0*BE**5)
*      GAMMA=DGAMMA(1D0+BE)

      COEF=EXP(BETA*(0.75d0-EGAM))/(GAMMA)
*         write(*,*) BETA
      RETURN
10    RES=.FALSE.
      RETURN

      ENTRY M_ISR(I)
   
20    CALL CLRSCR
      WRITE(CHM1,FMT='(G10.3)') SCALE
      WRITE(CHM2,FMT='(G10.3)') B_Ncl
      WRITE(CHM3,FMT='(G10.3)') B_ipsilon
      STRMEN = '%Initial State Radiation menu!%ISR scale = '//CHM1//
     &'!Beamstralung Ncl='//CHM2//'!Beamstralung ipsilon='//CHM3//'!!!'
            
      MODE=MENU(STRMEN,'SF_ISR')
      IF(MODE.EQ.0) THEN
         RETURN
      ELSE 
         WRITE(*,FMT='(''New value = '',$)')
         READ(*,*) XX
         IF (MODE.EQ.1) SCALE=XX
         IF (MODE.EQ.2) B_Ncl=xx
         IF (Mode.EQ.3) B_ipsilon=xx
      ENDIF
      GOTO 20

      ENTRY C_ISR(I,X,STRF)
       CALL INITISR(ITYPE,BETA,B_NCL,B_ipsilon, XI,YI,200)
       xx=(1-X)**(1.d0/3)
       STRF=DINTER(xx,200,XI,YI)
      RETURN
      END  
