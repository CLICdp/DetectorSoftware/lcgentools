      SUBROUTINE REGFUN(ITYPE,NSING,SINGAR,XMIN,XMAX,XX,XOUT,FACTOR)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION SINGAR(3,100),ARINT(0:100)
      COMMON/RGNORM/RNORM(0:200)
      LOGICAL FAONLY
      DATA DELT0/1.E-13/

*      write(*,*) 'regfun',ITYPE,NSING,XMIN,XMAX,XX


      IF (NSING.EQ.0) THEN
         XOUT= XMIN + XX*(XMAX-XMIN)
         FACTOR=(XMAX-XMIN)
         RETURN
      ENDIF    
      FAONLY=.FALSE.
      GOTO 1
      
      ENTRY REGFCT(ITYPE,NSING,SINGAR,XMIN,XMAX,XOUT,FACTOR)
      FAONLY=.TRUE.
      IF (NSING.EQ.0) THEN
         FACTOR=(XMAX-XMIN)
         RETURN
      ENDIF    

1     CONTINUE

      IF (ITYPE.GT.0)  THEN
         ITYP=ITYPE 
         ARINT(0)=1
         RNORM(0)=1/(XMAX-XMIN)
      ELSE
         ITYP=-ITYPE
         ARINT(0)=0
         RNORM(0)=0
      ENDIF




      DO 5  I=1,NSING

*      write(*,*) SINGAR(1,I),SINGAR(2,I)

      IF((ITYP.EQ.1).OR.((ITYP.EQ.2).AND.(SINGAR(2,I).EQ.0)))THEN
        IF ((SINGAR(1,I).GE.xmin).AND.(SINGAR(1,I).LE.xmax))THEN 
          DELT=DELT0*(ABS(XMIN)+ABS(XMAX))    
          IF   ((SINGAR(1,I)-DELT).LE.xmin) THEN 
               SINGAR(1,I)=xmin-DELT 
          ELSE IF  ((SINGAR(1,I)+DELT).GE.xmax) THEN
               SINGAR(1,I)=xmax+DELT 
          ELSE
*               write(*,*)  xmin, '< x <', xmax
*               write(*,*) 'Type=',itype ,' Bad reg=',I 
*               do 199 ll=1,NSING 
*199                write(*,*) ll, singar(1,ll),singar(2,ll),singar(3,ll)
*          ELSE
               write(*,*)  'ERROR. Regularization number ',I 
               write(*,*) 'presents pole inside of phase space ',
     &  ' or very closed to its boundary'

               STOP 
          ENDIF
        ENDIF
      ENDIF
5     CONTINUE
      
*      ARINT(0)=RNORM(0)*(XMAX-XMIN)
*      TINT=ARINT(0)
*      DO 10  I=1,NSING
*         ARINT(I)= RNORM(I)*(SINGI(ITYP,SINGAR(1,I),SINGAR(2,I),XMAX)
*     & -SINGI(ITYP,SINGAR(1,I),SINGAR(2,I),XMIN))
*         TINT=TINT+ARINT(I)          
*10    CONTINUE

      TINT=ARINT(0)
      DO 10  I=1,NSING
         ARINT(I)=1
         RNORM(I)= 1/( (SINGI(ITYP,SINGAR(1,I),SINGAR(2,I),
     &  SINGAR(3,I),XMAX)
     & -SINGI(ITYP,SINGAR(1,I),SINGAR(2,I),SINGAR(3,I),XMIN)) )
         TINT=TINT+ARINT(I)          
10    CONTINUE

      IF (FAONLY) GOTO 13
      TINTXX=TINT*XX
     
      IF (ARINT(0).GT.TINTXX) THEN
         XOUT=XMIN+TINTXX/RNORM(0)
      ELSE 
         TINTXX=TINTXX-ARINT(0)
         NN=1
11       IF (TINTXX.GT.ARINT(NN) ) THEN
           TINTXX=TINTXX-ARINT(NN)
           NN=NN+1
           GOTO 11
         ENDIF
      
         XOUT = SINGI1(ITYP,SINGAR(1,NN),SINGAR(2,NN),SINGAR(3,NN),
     &          TINTXX/RNORM(NN)+
     &          SINGI(ITYP,SINGAR(1,NN),SINGAR(2,NN),SINGAR(3,NN),
     &          XMIN),XMIN)

      ENDIF

13    CONTINUE
            
      FACTOR=RNORM(0)

      DO 20 I=1,NSING
20    FACTOR=FACTOR+RNORM(I)*SING(ITYP,SINGAR(1,I),SINGAR(2,I),
     & SINGAR(3,I),XOUT)

*      write(*,*) NSING,FACTOR      
       FACTOR=TINT/FACTOR

*      write(*,*) 'end of regfun', xout,factor 
      RETURN
      END



      FUNCTION SING(ITYP,POLE,WIDTH,DEG,X)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
     
      IF (ITYP.EQ.1) THEN
1       IF (DEG.GT.1.5) THEN 
          SING=1/(POLE-X)**2
*           write(*,*)"SING"
        ELSE
          SING=1/ABS(POLE-X)
        ENDIF
      ELSEIF (ITYP.EQ.2) THEN
        IF (WIDTH.EQ.0.0D0) GOTO 1
        SING=1/((POLE-X)**2 +WIDTH**2)           
      ENDIF
      RETURN
      END


      FUNCTION SINGI(ITYP,POLE,WIDTH,DEG,X)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
          
      IF (ITYP.EQ.1) THEN
1       IF (DEG.GT.1.5) THEN
          SINGI= -1/(X-POLE)
*           write(*,*)"SINGI"
        ELSE  
          IF (POLE.LT.X) THEN
            SINGI= LOG(X-POLE)
          ELSE
            SINGI= -LOG(POLE-X)
          ENDIF
        ENDIF          
      ELSEIF (ITYP.EQ.2) THEN
         IF (WIDTH.EQ.0.0D0) GOTO 1
         SINGI=ATAN((X-POLE)/WIDTH)/WIDTH 
      ENDIF 

      RETURN
      END


      FUNCTION SINGI1(ITYP,POLE,WIDTH,DEG,X,xmin)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)

      IF (ITYP.EQ.1) THEN
1       IF (DEG.GT.1.5) THEN
          SINGI1=POLE-1/X
*          write(*,*)"SINGI1"
        ELSE        
          IF (POLE.LT.Xmin) THEN
            SINGI1= EXP(X) +POLE
          ELSE
            SINGI1= POLE-EXP(-X)
          ENDIF
        ENDIF           
      ELSEIF (ITYP.EQ.2) THEN
         IF (WIDTH.EQ.0.0D0) GOTO 1
         SINGI1= POLE+WIDTH*TAN(X*WIDTH)  
      ENDIF 

      RETURN
      END
