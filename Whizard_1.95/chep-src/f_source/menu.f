*###################################################
*#            Menu's                               #
*#   contents:                                     #
*#      IN_SET(2), MDLSET(3), MC_SET(6)            #
*#      BATCH(8), VIEWF(9), EVNMEN(11) #
*###################################################

*********************************************
*    IN states menu                         *
*********************************************
      SUBROUTINE IN_SET
      IMPLICIT DOUBLE PRECISION (A-H,O-Z),INTEGER (I-N)
      CHARACTER PROCES*30
      CHARACTER STRMEN*300,DOLL*2
      CHARACTER DCH*12,RCH*12,XFMT*10,DCD*10
      COMMON /DOLL/   DOLL
      COMMON /PROCES/ NSUB,PROCES
      COMMON /SQS/    SQRTS,RAPID
      COMMON /NSESS/  NSESS
      CHARACTER*60 SF_TXT(2)
      COMMON /SF_TXT/SF_TXT
      SAVE

      CALL ITOCHR(DCD,NSESS)
***   menu loop
10    CALL CLRSCR  
      NS=1
      CALL DTOCHR(DCH,SQRTS,4)
      CALL DTOCHR(RCH,RAPID,4)
      STRMEN = '%IN states menu! !'//
     &         PROCES//'                  Session # '//DCD//'!%'//
     &         'S.F.1= '//SF_TXT(1)//'!'//
     &         'S.F.2= '//SF_TXT(2)//'!'//
     &         'SQRTS (GeV) = '//DCH//'!'//
     &         'C.M. rapidity = '//RCH//'!'// 
     &         '!!'

      MODE=MENU(STRMEN,'f_3')
      IF (MODE .EQ. 0) THEN
         GOTO 1000
      ELSE IF (MODE .LE. 2) THEN 
          CALL SF_MENU(MODE)
      ELSE IF (MODE .EQ. 3) THEN
          XFMT='(1X,A35'//DOLL//')'
12        WRITE(*,FMT=XFMT) 'Enter new SQRTS (in GeV): '
          READ(*,FMT='(G12.0)',ERR=12) SQRTS
      ELSE IF (MODE .EQ. 4) THEN
          XFMT='(1X,A35'//DOLL//')'
13        WRITE(*,FMT=XFMT) 'Enter new value for rapidity : '
          READ(*,FMT='(G12.0)',ERR=13) RAPID
      ENDIF
      GO TO 10

1000  CONTINUE
      RETURN
      END

**************************************************
* Physics model parameters menu                  *
**************************************************
      SUBROUTINE MDLSET
      IMPLICIT DOUBLE PRECISION (A-H,O-Z), INTEGER (I-N)
      CHARACTER        NAME*6
      CHARACTER        STRMEN*5000,XSTRM*5000,DOLL*2
      CHARACTER        DCH1*13,XFMT*40,DCH*10
      COMMON /DOLL/    DOLL
      CHARACTER*30 PROCES
      COMMON /PROCES/  NSUB,PROCES
      COMMON /NSESS/   NSESS
      SAVE

      CALL ITOCHR(DCH,NSESS)

***  menu loop
10    CALL CLRSCR  
      XSTRM='!!'
      DO 20 K=0,NVAR()-1
        CALL VINF(NVAR()-K,NAME,VALUE)
        CALL DTOCHR(DCH1,VALUE,6)
        STRMEN=NAME//'= '//DCH1//'!'//XSTRM
        XSTRM=STRMEN
20    CONTINUE
      STRMEN = ' !'//PROCES//'            Session # '//DCH//'!%'//XSTRM
      XSTRM=STRMEN
      STRMEN = '%Physical model parameters menu!'//XSTRM

      K=MENU(STRMEN,'f_4')
      IF (K .EQ. 0) GOTO 1000
      IF (K .LE. NVAR()) THEN
             CALL VINF(K,NAME,VAL)
             XFMT='(1X,A17'//DOLL//')'
30           WRITE(*,FMT=XFMT) 'Enter new value: '
             READ(*,FMT='(G15.0)',ERR=30) VAL
             CALL ASGN(K,VAL)
             IF (INAME .EQ. K) THEN
                 TVAL1 = VAL
                 IF (NPOINT .EQ. 1) TVAL2 = VAL
                 IF (TVAL2 .LT. VAL) TVAL2 = VAL
                 TVALUE = TVAL1
             ENDIF
      ENDIF
      GOTO 10

1000  CONTINUE

      RETURN
      END

*********************************************
*    View files menu                        *
*********************************************
      SUBROUTINE VIEWF
      CHARACTER DOLL*2
      COMMON /DOLL/    DOLL
      COMMON /NSESS/   NSESS
      CHARACTER*30 F_NAME,SESSTR
      SAVE

10    CALL CLRSCR
      IF( NSESS.EQ.1) THEN
         WRITE(*,*) ' Results are not produced  ,press ENTER '
         READ(*,*)
         RETURN
      ELSE
        WRITE(*,FMT='(1x,'' Enter a session number ( 1 - '','//
     &'I3,'') to view   or 0 to exit'')') NSESS-1 
        READ(*,FMT='(I3)', ERR=10) INSESS
        IF (INSESS.EQ.0 ) RETURN
        CALL ITOCHR(SESSTR,INSESS)
         CALL CH2CAT(F_NAME,'prt_',SESSTR)                    
         CALL VIEWRP(F_NAME)
        GOTO 10
      ENDIF
      END
