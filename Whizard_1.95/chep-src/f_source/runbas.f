*#################################################
*#            BASES call procedures              #
*#       file:      runbas.f                     #
*#   contents:                                   #
*#  RUNBAS, USERIN, FUNC, SQME2, USEROUT, EVNGEN #
*#-----------------------------------------------#
*# The scheme of calls                           #
*#                                               #
*#    RUNBAS :                                   #
*#        BSMAIN(FUNC) :                         #
*#                   BSUSRI :                    #
*#                       USERIN :                #
*#                            BHINIT             #
*#                            XHINIT             #
*#                            DHINIT             #
*#                   BASES(FUNC) :               #
*#                            FUNC :             #
*#                                KIN*           #
*#                                SQME2          #
*#                                XHFILL         #
*#                                DHFILL         #
*#                   USROUT (if NPRINT<=0)       #
*#        USROUT                                 #
*#################################################


*******************************************
*   Start BASES                           *
*    NMODE = 1  batch work                *
*    NMODE <> 1 interactive mode          *
*******************************************
      SUBROUTINE RUNBAS(NMODE)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL      APPEND

      CHARACTER    DOLL*2

      COMMON /NSESS/   NSESS
      COMMON /DOLL/    DOLL
      COMMON /SQS/     SQRTS

      COMMON /DEVICE/  IPRT,ISES,IBAT,ISTR(2),IEVENT,ITMP
      CHARACTER*12  PRTNAM,SESNAM,BATNAM
      COMMON /DEVNAM/  PRTNAM,SESNAM,BATNAM

      COMMON /APPEND/  APPEND

      COMMON /RGNORM/ RNORM(0:200)
      COMMON /FUNMOD/ MODE_F  

      LOGICAL STRFON
      COMMON /STRFST/ XPARTN(2),BE(2),STRFON(2)
      CHARACTER*500  STRMEN
      LOGICAL FILEON      
      COMMON/MCINTR/ NCALL0,ITMX0,FILEON,MDS0
      LOGICAL WRTF
      COMMON /BATCH/PMIN,PMAX,NSIZE,NUMPAR,NCALL1,IT1,NCALL2,IT2,WRTF

      LOGICAL CLSTAT,FIRST,XFILE
      CHARACTER*4 ONN
      CHARACTER*1 CH
      CHARACTER*10 CNCALL,CITMX
      CHARACTER*3 DCH
      CHARACTER*20 MESS,DCH2,TABF
      DIMENSION RNORM2(0:200)
      COMMON /PVECT/ P(0:3,100)
      CHARACTER        VERS*20
      COMMON /VERS/    VERS
      


      LOGICAL SCREEN,QUADRA 
      SAVE
      
      EXTERNAL FUNC

      IF (NIN().EQ.2) THEN
        MESS=' Cross section [pb] '
      ELSE
        MESS='  Width     [Gev]   ' 
      ENDIF


      IF(NMODE.EQ.1) THEN
        SCREEN=.FALSE.
        NTAB=1
        IF(NSIZE.GT.1) THEN
           CALL ITOCHR(DCH,NSESS + NSIZE-1)
           DCH2='_'//DCH
           CALL ITOCHR(DCH,NSESS)           
           CALL CH2CAT(TABF,DCH,DCH2)
           OPEN(ITMP,FILE='tab_'//TABF,STATUS='UNKNOWN')
           PTMP=PMIN
           IF(NUMPAR.EQ.0) THEN
              SQRTS=PMIN
              TABF='SQRTS'
           ELSE 
              CALL ASGN(NUMPAR,PTMP)
              CALL VINF(NUMPAR,TABF,DUMMY)
           ENDIF

           WRITE(ITMP,FMT='(3x,A6,2x,A20,A8,2x,A9)')  
     &     TABF, MESS, ' Error ' , 'CHI**2/IT'          
        ENDIF 
      ELSE
        SCREEN=.TRUE.
      ENDIF

***** SCREEN is TRUE for ANY CASE
      SCREEN=.TRUE.

15    NBATCH=1
*** save current session parameters
      CALL W_SESS(ISES)
*** open protocol and resulting files
C*      CALL SESFIL
      CALL ITOCHR(DCH,NSESS)
      OPEN(IPRT,FILE='prt_'//DCH,STATUS='UNKNOWN')
      WRITE(IPRT,FMT='(10x,''CompHEP kinematics module  '',A20)') VERS
      WRITE(IPRT,*) ' The session parameters:'
      CALL W_SESS(IPRT)
      WRITE(IPRT,FMT='(1x,78(''=''))' )
***  initkinematics
      CALL VEGINI
      DUMMY=STRFUN(0,0.D0)


      QUADRA= .NOT. (LENR().EQ.8)
      CALL IMKMOM(QUADRA,NDIM,IERR)
      IF(IERR.EQ.1) THEN
         WRITE(*   ,*) " Too small energy!"
         WRITE(IPRT,*) " Too small energy!"
         IF(NMODE.EQ.0) THEN
            READ(*,*)
            GOTO 103
         ELSE
            GOTO 102
         ENDIF
      ENDIF


****  weights for singularitys are calculating 
C      DO 3 KREG=0,NNREG()
C         MODE_F=KREG      
C         RNORM(KREG)=1 

C         call vegas(ndim,func,avgi,sd,chi2a)

C         RNORM(KREG)=0 
C         RNORM2(KREG)=1/AVGI
C3    CONTINUE
        
      MODE_F=-1
      DO 34 KREG=0,NNREG()
34    RNORM(KREG)=1
****=RNORM2(KREG)


****     Main cycle

      CLSTAT=.TRUE.
      FILEON=.FALSE.
      FIRST=.TRUE.
      CALL ITOCHR(DCH,NSESS)
      
20    IF ( NMODE.EQ.1 )  THEN
         IF (NBATCH.EQ.1) THEN
            NCALL0=NCALL1
            ITMX0=IT1
            IF (WRTF.AND.(IT2*NCALL2.EQ.0)) THEN
               FILEON=.TRUE.
            ELSE
               FILEON=.FALSE.
            ENDIF
            NBATCH=2
         ELSE IF (NBATCH.EQ.2) THEN
            IF (IT2*NCALL2.EQ.0) GOTO 19
            NCALL0=NCALL2
            ITMX0=IT2
            FILEON=WRTF         
            NBATCH=3
         ELSE IF (NBATCH.EQ.3) THEN
19          IF(NSIZE.GT.1)WRITE(ITMP,FMT='(2G12.4,3x,2G12.2)')
     &            PTMP,AVGI,SD,CHI2A
            IF(NTAB.GE.NSIZE) THEN
               IF(NSIZE.GT.1) CLOSE(ITMP)
               CLOSE(IPRT)
               NSESS=NSESS+1
               RETURN
            ENDIF
            PTMP=PMIN+NTAB*(PMAX-PMIN)/(NSIZE-1)    
            IF (NUMPAR.EQ.0) THEN
              SQRTS=PTMP
            ELSE
              CALL ASGN(NUMPAR,PTMP)
            ENDIF
            NSESS=NSESS+1
            NTAB=NTAB+1
            GOTO 15
         ENDIF
      ELSE  
        CALL CLRSCR
        CALL ITOCHR(CNCALL,NCALL0)
        CALL ITOCHR(CITMX,ITMX0)

        STRMEN= 'Start integration!Clear statistic '//ONN(CLSTAT)
     &   //'!Ncall='//CNCALL//'!Itmx='//CITMX
     &   //'!Stratified Sampling '//ONN(MDS0.NE.0) 
     &   //'!Writing to file '//ONN(FILEON)//'!!!'

        MODE=MENU(STRMEN,'f_1')

************************* EXIT
        IF (MODE.EQ.0) GOTO 102
        IF (MODE.EQ.1) GOTO 101    
************************** Clear statistic  ON/OFF     
        IF (MODE.EQ.2) CLSTAT=.NOT.CLSTAT

************************** Set NCALL
        IF (MODE.EQ.3) THEN
           WRITE(*,FMT='(1X,A20'//DOLL//')' ) 'Enter new value: '
           READ(*,FMT='(I7)',ERR=21) NCALL0
21         CONTINUE
        ENDIF

************************** Set ITXM 
        IF (MODE.EQ.4) THEN
           WRITE(*,FMT='(1X,A20'//DOLL//')' ) 'Enter new value: '
           READ(*,FMT='(I4)',ERR=22) ITMX0
22         CONTINUE
        ENDIF
*************************  Writing on file ON/OFF
        IF (MODE.EQ.5) THEN
           IF (MDS0.NE.0) THEN
              MDS0=0
           ELSE
              MDS0=1
           ENDIF 
        ENDIF
        IF (MODE.EQ.6) FILEON=.NOT.FILEON
        GOTO 20
      ENDIF
      
101   CONTINUE
************************ Start integration
         IF(FILEON) THEN
            OPEN(IEVENT,FILE='event_'//DCH,
     &         FORM='UNFORMATTED',STATUS='UNKNOWN')
            CALL SVCOMP(IEVENT,FIRST)
            CALL SVGRID(IEVENT)
            CALL INIBUF
            WRITE(IPRT,*) ' Writing events to file ! '
         ENDIF

      IF (NMODE.EQ.0) THEN
         WRITE(*,100) NCALL0, MESS,MESS
 100     FORMAT(1X,'INTEGRATION BY VEGAS     NCALL=',I7,/,
     &   '  IT      CURRENT RESULTS          ',
     &       '      ACCUMULATED RESULTS        CHI**2' ,
     &   /,6x,A20,' Error %',3x,A20,' Error %'/,1X,78('-'))
      ENDIF
           CALL VEGNNN(NCALL0,MDS0)
           WRITE(IPRT,100)NCALL0,MESS,MESS          
           IF (FIRST) THEN       
                call vegas(ndim,func,avgi,sd,chi2A)
                CALL VEGPRN(IPRT,SCREEN,AVGI,SD,CHI2A)
                FIRST=.FALSE.
           ELSE
               IF (CLSTAT) THEN
                  call vegas1(ndim,func,avgi,sd,chi2A)
                  CALL VEGPRN(IPRT,SCREEN,AVGI,SD,CHI2A)
               ELSE
                  call VEGAS2(NDIM,FUNC,AVGI,SD,CHI2A)
                  CALL VEGPRN(IPRT,SCREEN,AVGI,SD,CHI2A)
               ENDIF
           ENDIF

           DO 23 I=2,ITMX0
             CALL  VEGAS3(NDIM,FUNC,AVGI,SD,CHI2A)
23           CALL  VEGPRN(IPRT,SCREEN,AVGI,SD,CHI2A)

           IF (ITMX0.LT.2) CHI2A=0

           IF (CLSTAT) THEN
              NN=NIN()
              CALL VEGOUT(IPRT,SCREEN,AVGI,SD,CHI2A,NN)
           ENDIF
 
           IF(FILEON)  THEN
              CALL FLSBUF
              WRITE(IEVENT)   ( (P(I,J),I=0,3),J=1,NIN()+NOUT())

c              write(IEVENT) AVGI
c              rewind (IEVENT)

              CLOSE(IEVENT)
*** For a test only
*            close(35)
              FILEON=.FALSE.
           ENDIF

           IF (NMODE.EQ.0) THEN
              WRITE(*,*) 'Press ENTER to continue'
              READ(*,*)                  
           ENDIF

       GOTO 20

102   CONTINUE    
      IF (NMODE.EQ.0) THEN
         WRITE(*,FMT='(1X,''Save results ? Y/N  '''//DOLL//')')
         READ(*,FMT='(A1)') CH
         IF ((CH.EQ.'Y').OR.(CH.EQ.'y')) THEN 
            NSESS=NSESS+1
            CLOSE(IPRT)
            RETURN
         ELSE IF((CH.EQ.'N').OR.(CH.EQ.'n')) THEN
103        INQUIRE(FILE='event_'//DCH ,EXIST=XFILE)
           IF (XFILE) THEN
              OPEN(IEVENT,FILE='event_'//DCH,STATUS='UNKNOWN')
              CLOSE(IEVENT,STATUS='DELETE')
           ENDIF
           OPEN(IPRT,FILE='prt_'//DCH,STATUS='UNKNOWN')
           CLOSE(IPRT,STATUS='DELETE')
           RETURN
         ELSE 
            GOTO 20
         ENDIF
      ELSE
         NSESS=NSESS+1 
      ENDIF

      RETURN 

      END



***************************************************
*     kinematics vars for current phase space point
***************************************************
      DOUBLE PRECISION FUNCTION FUNC(X,WGT)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION X(25)
      LOGICAL          LPHSP,STRFON
      COMMON /PHSP/    LPHSP
      COMMON /FACTOR/  FACTOR
      COMMON /STRFST/  XPARTN(2),BE(2),STRFON(2)
      LOGICAL TSTPDF
      COMMON /TSTPDF/ TSTPDF
      COMMON /FUNMOD/ MODE_F
      COMMON /PVECT/ P(0:3,100)
      LOGICAL FILEON,QUADRA
      COMMON/MCINTR/ NCALL0,ITMX0,FILEON,MDS0

      CHARACTER   PROCES*30
      COMMON /PROCES/ NSUB,PROCES

      SAVE

      FUNC=0.D0    
** number of the subprocess
*** call kinematics preparation of scalar products
      CALL MKMOM(X,FACTOR) 

      IF (FACTOR .EQ. 0.D0) GOTO 20
      QUADRA=.NOT.(LENR().EQ.8)
      IF (QUADRA) THEN
         CALL QSPROD
      ELSE
         CALL SPROD
      ENDIF
      CALL LORROT

*** call for 'running strong coupling constant'
      CALL ALF 
***  structure function  multiplication
      IF (NIN().EQ.2) THEN      
         IF (TSTPDF) FACTOR=1
         IF (STRFON(1)) FACTOR=FACTOR*STRFUN(1,XPARTN(1))
         IF (STRFON(2)) FACTOR=FACTOR*STRFUN(2,XPARTN(2))
		
         IF (TSTPDF) THEN
            FUNC=FACTOR
            GOTO 20
         ENDIF
      ENDIF

*** user cut function
      FACTOR=FACTOR*T_CUT()
      IF (FACTOR.EQ.0.D0)   GOTO 20

      FACTOR=FACTOR*CALCUT()
      IF (FACTOR.EQ.0.D0)   GOTO 20


      FACTOR=FACTOR*USRCUT()
      IF (FACTOR .EQ. 0.D0) GOTO 20
      
*** squared matrix element
      IF (MODE_F.GE.0) THEN
         FUNC=FACTOR*SNGFUN(MODE_F)
      ELSE  
         FUNC=FACTOR*SQME(NSUB)
      ENDIF
20    CONTINUE
      
      IF (( MODE_F.LT.0 ).AND.FILEON) THEN
         CALL WRTBUF(FUNC)
      ENDIF
      RETURN
      END

      SUBROUTINE WRTBUF(SI)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL FILEON
      COMMON /MCINTR/ NCALL0,ITMX0,FILEON,MDS0
      INTEGER NBUF
      REAL  * 4 BUFF(500)
      COMMON /DEVICE/  IPRT,ISES,IBAT,ISTR(2),IEVENT,ITMP
      SAVE
         IF (FILEON) THEN
            NBUF=NBUF+1
            BUFF(NBUF)=SI
            SI= BUFF(NBUF)
***            write(*,*) BUFF(NBUF)
            IF (NBUF.EQ.500) THEN
               WRITE(IEVENT) BUFF
               NBUF=0
            ENDIF
         ENDIF
         RETURN

      ENTRY FLSBUF
         IF (NBUF.EQ.0) RETURN
         DO 10 I=NBUF+1,500
10         BUFF(I)=0
         WRITE(IEVENT) BUFF
         NBUF=0
         RETURN

      ENTRY INIBUF
        NBUF=0
        RETURN
      END
