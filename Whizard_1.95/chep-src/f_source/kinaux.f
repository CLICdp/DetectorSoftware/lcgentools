C      SUBROUTINE LVTONV(LV,NV)
C      LOGICAL FUNCTION EQVECT(LV1,LV2)
C      FUNCTION NEXTNN(N)
C      SUBROUTINE ORDINV(LV)
C      SUBROUTINE CONINV(LV)
C      LOGICAL FUNCTION SPOLE(LV)
C      SUBROUTINE LVCOPY(LVFROM,VLTO)
C      SUBROUTINE LVCONC(LV1,LV2,LVRES)
C      FUNCTION LVLEN(LV)


      SUBROUTINE LVTONV(LV,NV)
      DIMENSION  LV(10)      
      CALL VNULL4(NV)
      I=1        
1     N=IABS(LV(I)) 
      IF ( N.EQ.0)  RETURN
      CALL VSUM4(NV,N,NV,ISIGN(1,LV(I)))
      I=I+1
      GOTO 1
      END
      
      SUBROUTINE LVCOPY(LVFROM,LVTO)
      DIMENSION LVFROM(10),LVTO(10)
      DO 5 I=1,10
5     LVTO(I)=0
      I=1
10    LVTO(I)=LVFROM(I)
      IF (LVFROM(I).EQ.0) RETURN
      I=I+1
      GOTO 10
      END
      

      SUBROUTINE ORDINV(LV)
      DIMENSION LV(10)
      I=1
      N=0
10    IF (LV(I).NE.0) THEN
        IF ( (IABS(LV(I)).LE.NIN()).AND.(LV(I).GT.0) ) LV(I)=-LV(I)
         N=N+1
         I=I+1
         GOTO 10
      ENDIF
      IF(N.EQ.0) RETURN
     
      J=1
20    IF (LV(J+1).EQ.0) RETURN
      IF (IABS(LV(J+1)).EQ.IABS(LV(J)) ) THEN 
         LV(1)=0
         RETURN
      ENDIF   
      IF (IABS(LV(J+1)).GT.IABS(LV(J)) ) THEN
         J=J+1
      ELSE
C*         IM=IABS(LV(J+1))
         IM=LV(J+1)
         LV(J+1)=LV(J)
         LV(J)=IM
         IF (J.EQ.1) THEN 
            J=J+1
         ELSE
            J=J-1 
         ENDIF
      ENDIF          
      
      GOTO 20                
      END
      
      SUBROUTINE LVMIRR(LV)
      DIMENSION LV(10),LV2(10)

      N=0
10    IF (LV(N+1).NE.0) THEN
         N=N+1
         GOTO 10
      ENDIF


      DO 20 I=1,N+1
20    LV2(I)=LV(I)     
      K=1
      DO 40 I=1,NIN()+NOUT()
      DO 30 J=1,N
30    IF ( IABS(LV2(J)).EQ.I ) GOTO 40
      IF (I.LE.NIN()) THEN 
         LV(NEXTNN(K))=-I      
      ELSE
         LV(NEXTNN(K))=I
      ENDIF
40    CONTINUE
      LV(K)=0
      RETURN
      
      END
     
      SUBROUTINE CONINV(LV)
      DIMENSION LV(10)      
      CALL ORDINV(LV)
      IF (LV(1).EQ.0 ) RETURN

      I=1
      N=0
10    IF (LV(I).NE.0) THEN
         N=N+1
         I=I+1
         GOTO 10
      ENDIF
             
      IF(   (NIN()+NOUT().GT.2*N).OR.
     &    ( (NIN()+NOUT().EQ.2*N).AND.(LV(1).EQ.-1)) ) RETURN

      CALL LVMIRR(LV)
      RETURN

      
      END
      
      LOGICAL FUNCTION EQVECT(LV1,LV2)
      DIMENSION LV1(10), LV2(10)
      DIMENSION LVBUF1(10),LVBUF2(10)

      CALL LVCOPY(LV1,LVBUF1)
      CALL LVCOPY(LV2,LVBUF2)
      CALL CONINV(LVBUF1)
      CALL CONINV(LVBUF2)

      I=1
      EQVECT=.FALSE.

1     IF(LVBUF1(I).NE.LVBUF2(I)) GOTO 10 
      IF(LVBUF1(I).EQ.0) THEN
        EQVECT=.TRUE.
        RETURN
      ELSE
        I=I+1
        GOTO 1
      ENDIF            
10    RETURN
      END

      FUNCTION NEXTNN(N)
         NEXTNN=N
         N=N+1
      RETURN
      END      


      
      LOGICAL FUNCTION SPOLE(LV)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)      
      DIMENSION LV(10)
      SPOLE=(NIN().EQ.1).OR.(LV(1).GT.0).OR.(LV(2).LT.0)       
      RETURN
      END 


      SUBROUTINE LVCONC(LV1,LV2,LVRES)
      DIMENSION LV1(10),LV2(10),LVRES(10)
      DO 5 I=1,10
5     LVRES(I)=0
      I=1
10    LVRES(I)=LV1(I)
      IF (LV1(I).EQ.0) GOTO 20
      I=I+1
      GOTO 10
20    J=1
30    LVRES(I)=LV2(J)
      IF (LV2(J).EQ.0) RETURN
      I=I+1
      J=J+1 
      GOTO 30
      END


      SUBROUTINE LVDIFF(LV1,LV2,LVRES,IERR)
      DIMENSION LV1(10),LV2(10),LVRES(10)
      
      NI=0
      N2=0
10    IF (LV2(N2+1).NE.0) THEN 
        N2=N2+1
        N1=0
20      IF(LV1(N1+1).NE.0) THEN
          N1=N1+1
          IF (IABS(LV1(N1)).EQ.IABS(LV2(N2))) THEN
             NI=NI+1 
             GOTO 10
          ENDIF
          GOTO 20
        ENDIF           
        GOTO 10
      ENDIF

      
      IF (NI.EQ.0) THEN 
        CALL LVCONC(LV1,LV2,LVRES) 
        CALL LVMIRR(LVRES)         
        IERR=0
        RETURN
      ELSE
         IF (NI.LT.N2) THEN
             IERR=1
             RETURN
         ENDIF 
      ENDIF

      IERR=0
      NRES=1
      N1=0
30    N1=N1+1
      DO 40 I2=1,N2 
         IF (IABS(LV1(N1)).EQ.IABS(LV2(I2))) GOTO 30 
40    CONTINUE
      LVRES(NRES)=LV1(N1)
      IF (LV1(N1).EQ.0) THEN
         CALL ORDINV(LVRES)
         RETURN
      ENDIF
      NRES=NRES+1
      GOTO 30    
      
      END



      subroutine  printlv(lv)
      dimension lv(10)
      write(*,*) (lv(k),k=1,10) 
      read(*,*)
      return
      end

      FUNCTION LVLEN(LV)
      DIMENSION LV(10)
      I=1
1     IF((I.GT.10).OR.(LV(I).EQ.0)) THEN
        LVLEN=I-1
        RETURN
      ENDIF
      I=I+1
      GOTO 1 
      END  

       
      SUBROUTINE  SNGPOS(LV,NDEC,NCLUST,LVAUX )
      IMPLICIT DOUBLE PRECISION (A-H,O-Z) 
      LOGICAL STRFON,ISKNOW
c      LOGICAL spole
      COMMON /STRFST/XPARTN(2),BE(2),STRFON(2)
      COMMON /KINMTC/ LVIN(10,10), LVOUT(10,10,2)
      SAVE /KINMTC/,/STRFST/
      DIMENSION LV(10),LVAUX(10),LVAUX2(10,2),IERR(2),LN(2)
      
*      singularity for total energy
 
      IF ( (LV(1).EQ.-1).AND.(LV(2).EQ.-2).AND.(LV(3).EQ.0)
     & .AND.(NIN().EQ.2)) THEN
         LVAUX(1)=0
         NDEC=0
         NCLUST=0
         RETURN
      ENDIF


      DO 20 I=1,NOUT()-1

      DO 10 K=1,2
      CALL LVDIFF(LV,LVOUT(1,I,K),LVAUX2(1,K),IERR(K))
      IF ( (IERR(K).EQ.0).AND.(.NOT.ISKNOW(LVAUX2(1,K),I)) ) IERR(K)=-1
      IF (IERR(K).EQ.0) THEN 
        LN(K)=LVLEN(LVOUT(1,I,K))+LVLEN(LVAUX2(1,K))
      ELSE 
         LN(K)=100
      ENDIF
10    CONTINUE

      IF ((IERR(1).EQ.0).OR.(IERR(2).EQ.0)) THEN
         NDEC=I
         K=1
         IF( LN(1).GT.LN(2)) K=2
         NCLUST=K
         CALL LVCOPY(LVAUX2(1,K),LVAUX)
         RETURN
      ENDIF 

20    CONTINUE
      RETURN
      END      

      LOGICAL FUNCTION ISKNOW(LV,I)
      COMMON /KINMTC/ LVIN(10,10),LVOUT(10,10,2)
      DIMENSION LV(10),ICMP(10)
      COMMON /XY/ NUMBX,NUMBY
      LOGICAL EQVECT

      IF (EQVECT(LV,LVIN(1,I))) THEN
         ISKNOW=.FALSE.
         RETURN
      ENDIF

      LEN=0
25    IF ( LV(LEN+1).NE.0 ) THEN
          LEN=LEN+1
          GOTO 25
      ENDIF      


      DO 28 J=1,10
        ICMP(J)=IABS(LV(J))
28    CONTINUE
      DO 30 K=1,2
      DO 30 J=1,I-1
      DO 30 N=1,10
       IF ((LVOUT(2,J,K).EQ.0).AND.(LVOUT(1,J,K).EQ.ICMP(N))) THEN
        ICMP(N)=0
       ENDIF
30    CONTINUE
      DO 31 K=1,2
      DO 31 J=1,10
       IF ((LVIN(K,1).EQ.ICMP(J)).OR.
     & (ICMP(J).EQ.NUMBX)) ICMP(J)=0
CCC     & (ICMP(J).EQ.NUMBX).OR.(ICMP(J).EQ.NUMBY)) ICMP(J)=0
31    CONTINUE

      ISKNOW=.FALSE.
      DO 40 J=1,LEN
40      IF(ICMP(J).NE.0) RETURN

50    ISKNOW=.TRUE.
      RETURN
      END

      SUBROUTINE WRTKIN(NCHAN,NDEC)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      COMMON /KINMTC/ LVIN(10,10), LVOUT(10,10,2)      
      COMMON /XY/ NUMBX,NUMBY
      SAVE /KINMTC/,/XY/
      CHARACTER*1 IN(10),OUT1(10),OUT2(10) 
      CHARACTER*2 PIN(10),P1(10),P2(10)
*      CHARACTER*1 POLE(10)
*      CHARACTER*2 PPOLE(10)
      CHARACTER*50 TXT1,TXT2,TXT3
      CHARACTER*10 TXT4,TXT5
      NUMBX=NIN()+NOUT()+1
      NUMBY=NUMBX+1
      
      TXT1='===================== Kinematical scheme ('
      TXT2=') -> ('
      TXT3=') ========================'
      WRITE(NCHAN,10) TXT1,NIN(),TXT2,NOUT(),TXT3
10    FORMAT(1X,A42,I2,A6,I2,A26)

      DO 50 I=1,NDEC
      TXT1='Decay '
      TXT2=' : ('
      TXT3=') -> ('
      TXT4=') , ('
      TXT5=')'
      
         DO 19 K=1,10
         IN(K)=' '
19       PIN(K)='  '           

         DO 20 K=1,NIN()+NOUT()+1 
            IF (LVIN(K,I).NE.0) THEN
                IN(K)=CHAR(48+IABS(LVIN(K,I)))
                PIN(K)='+p'
            ELSE
                IN(K)=' '
                PIN(K)='  '
            ENDIF 
20       CONTINUE                   
         DO 25 K=1,10
            IF (LVOUT(K,I,1).NE.0) THEN
                OUT1(K)=CHAR(48+IABS(LVOUT(K,I,1)))
                P1(K)='+p'
            ELSE
                OUT1(K)=' '
                P1(K)='  '
            ENDIF 
25       CONTINUE                   
         DO 28 K=1,10
            IF (LVOUT(K,I,2).NE.0) THEN
                OUT2(K)=CHAR(48+IABS(LVOUT(K,I,2)))
                P2(K)='+p'
            ELSE
                OUT2(K)=' '
                P2(K)='  '
            ENDIF 
28       CONTINUE   

      WRITE(NCHAN,70)  TXT1,I,TXT2, (PIN(K),IN(K), K=1,6), TXT3,
     &(P1(K),OUT1(K),K=1,6), TXT4,(P2(K),OUT2(K),K=1,6), TXT5

     
C*      WRITE(*,*)
50    CONTINUE

      IF (NCHAN.EQ.6) THEN
      WRITE(6,FMT='(A78)')' ==================================='//
     &          '==========================================='
      WRITE(6,*)
      WRITE(6,*)
      ENDIF

      RETURN

70    FORMAT(1X,A6,I2,A4,6(A2,A1),A6,6(A2,A1),A5,
     & 6(A2,A1),A1)
                                                                        
      ENTRY RDRKIN(NCHAN)

      NUMBX=NIN()+NOUT()+1
      NUMBY=NUMBX+1
      NLENG1=6
      NLENG2=9
      DO 80 I=1,10
       DO 80 J=1,10
        LVIN(J,I)=0
        DO 80 L=1,2
         LVOUT(J,I,L)=0
80    CONTINUE
      
      READ(NCHAN, 10) TXT1,N,TXT2,NTXT3 
      DO 105 I=1,NOUT()-1
        READ(NCHAN,70)   TXT1,N,TXT2, (PIN(K),IN(K), K=1,6), TXT3,
     & (P1(J),OUT1(J),J=1,6), TXT4, (P2(L),OUT2(L),L=1,6), TXT5
        DO 82 K=1,NLENG1
          IF (IN(K).EQ.' ') THEN 
            LVIN(K,I)=0
          ELSE
             LVIN(K,I)=ICHAR(IN(K))-48
         ENDIF        
82      CONTINUE  
        DO 87 K=1,NLENG1
          IF (OUT1(K).EQ.' ') THEN 
            LVOUT(K,I,1)=0
          ELSE
             LVOUT(K,I,1)=ICHAR(OUT1(K))-48
         ENDIF        
87      CONTINUE  
        DO 92 K=1,NLENG1
          IF (OUT2(K).EQ.' ') THEN 
            LVOUT(K,I,2)=0
          ELSE
             LVOUT(K,I,2)=ICHAR(OUT2(K))-48
         ENDIF        
92      CONTINUE 
 

105   CONTINUE
      
      RETURN
      END
