      SUBROUTINE NCDMEN(WRTCUT,INCUT,CHCUT,DLCUT,HMENU)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      character ch(20),ch1*1, proces*30
      CHARACTER HMENU*(*)
        CHARACTER PATH*60,D_SLASH*1,F_SLASH*1
        CHARACTER*100 F_NAME,BEG
        LOGICAL XFILE
      COMMON /PROCES/ NSUB,PROCES
      EXTERNAL  WRTCUT,INCUT,CHCUT,DLCUT
            
99    CALL CLRSCR()
      CALL WRTCUT(6)
      WRITE(*,FMT='(1x,62(''*''))')
      WRITE(*,*) '*  Process:  '//PROCES//'                  *'
      WRITE(*,FMT='(1x,''*  N - new, D?[ ?] - delete,'','//
     &' '' C? - change, H - help, X - quit *'' )')
      WRITE(*,FMT='(1x,62(''*''))') 
      WRITE(*,FMT='(1x,''Your choice?  '',$)')
      DO 10 I=1,10 
10      ch(i)=' ' 
      READ(*,FMT='(20A1)') ch

c      write(*,FMT='(1x,20A1)') ch
c      read(*,*) 
       
      IF( (CH(1).EQ.'X').OR.(CH(1).EQ.'x') ) THEN
         RETURN 
      ELSEIF ( (CH(1).EQ.'N').OR.(CH(1).EQ.'n') ) THEN
         CALL INCUT()
      ELSEIF ( (CH(1).EQ.'D').OR.(CH(1).EQ.'d') ) THEN

      i=2
17    if (ch(i).eq.' ') goto 18
      if ((ichar(ch(i)).ge.58).or.(ichar(ch(i)).lt.48)) goto 99
      i=i+1
      goto 17


18    I=3
20    if (ch(I).EQ.' ') goto 30
      IF (ch(i-1).LT.ch(i)) THEN
        ch1=ch(i-1)
        ch(i-1)=ch(i)
        ch(i)=ch1
        if(i.EQ.3) THEN
             i=i+1
        ELSE
             i=i-1
        ENDIF
      ELSE
        i=i+1
      ENDIF        
      GOTO 20

30    CONTINUE

         I=2
40       IF (CH(I).EQ.' ') GOTO 99
c         if (CH(I).GE.'A') THEN
c             NCUT=ICHAR(CH(I))-ICHAR('A')+10
c         ELSE
             NCUT=ICHAR(CH(I))-ICHAR('0')
c         ENDIF
         CALL DLCUT(NCUT)
         I=I+1
         GOTO 40
       ELSEIF ( (CH(1).EQ.'C').OR.(CH(1).EQ.'c') ) THEN
         IF (CH(2).EQ.' ') GOTO 99
         if ((ichar(ch(2)).ge.58).or.(ichar(ch(2)).lt.48)) goto 99
c         if (CH(2).GE.'A') THEN
c             NCUT=ICHAR(CH(2))-ICHAR('A')+10
c         ELSE
             NCUT=ICHAR(CH(2))-ICHAR('0')
c         ENDIF
         CALL CHCUT(NCUT)
       ELSEIF ( (CH(1).EQ.'H').OR.(CH(1).EQ.'h') ) THEN
         CALL CPTH(PATH,D_SLASH,F_SLASH)
         CALL CH2CAT(F_NAME,PATH,D_SLASH//'help'//F_SLASH)
         CALL CH2CAT(BEG,F_NAME,HMENU)
         CALL CH2CAT(F_NAME,BEG,'.txt')
         INQUIRE(FILE=F_NAME,EXIST=XFILE)
         IF (XFILE) CALL VIEWRP(F_NAME )
       ENDIF 
         GOTO 99                 
       END

