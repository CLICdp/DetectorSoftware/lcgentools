
********************************************
*     Add session in batch                 *
********************************************
      SUBROUTINE B_ADD
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL XFILE
      CHARACTER*78  STRX

      COMMON /DEVICE/  IPRT,ISES,IBAT,ISTR(2),IEVENT,ITMP
      CHARACTER*12  PRTNAM,SESNAM,BATNAM
      COMMON /DEVNAM/  PRTNAM,SESNAM,BATNAM

      COMMON /NSESS/   NSESS
      LOGICAL WRTF
      COMMON/BATCH/PMIN,PMAX,NSIZE,NUMPAR,NCALL1,IT1,NCALL2,IT2,WRTF 
      CHARACTER*300 STRMEN
      CHARACTER*4 ONN
      CHARACTER*6 VNAME

      SAVE
1     CALL CLRSCR
      IF (NSIZE.LE.1) THEN
        WRITE(STRMEN, 100) NCALL1,IT1,NCALL2,IT2,ONN(WRTF)
      ELSE 
         IF (NUMPAR.EQ.0) THEN 
            VNAME='SQRTS'
         ELSE
            CALL VINF(NUMPAR,VNAME,DUMMY)
         ENDIF
         WRITE(STRMEN, 110) NCALL1,IT1,NCALL2,IT2,ONN(WRTF),
     &   NSIZE,VNAME,PMIN,PMAX
      ENDIF  

      MODE=MENU(STRMEN,'f_C')
      IF (MODE.EQ.0) THEN
          RETURN
      ELSE IF (MODE.EQ.1) THEN
         INQUIRE(FILE=BATNAM,EXIST=XFILE)
         IF (XFILE) THEN
            OPEN(IBAT,FILE=BATNAM,STATUS='UNKNOWN')
10          READ(IBAT,FMT='(A)',END=11,ERR=11) STRX
            GOTO 10
11          CONTINUE
         ELSE
            OPEN(IBAT,FILE=BATNAM,STATUS='NEW')
         ENDIF

         WRITE(IBAT,*) '|||||||| next session ||||||||'
         CALL W_SESS(IBAT)

         ENDFILE IBAT
         CLOSE(IBAT)
         RETURN           
      ELSE IF (MODE.EQ.2) THEN
        WRITE(*,101) 
        READ(*,FMT='(I6)',ERR=1 )  NCALL1
      ELSE IF (MODE.EQ.3) THEN
        WRITE(*,101) 
        READ(*,FMT='(I3)',ERR=1 )  IT1
      ELSE IF (MODE.EQ.4) THEN
        WRITE(*,101) 
        READ(*,FMT='(I6)',ERR=1)   NCALL2   
      ELSE IF (MODE.EQ.5) THEN
        WRITE(*,101) 
        READ(*,FMT='(I6)',ERR=1)   IT2
      ELSE IF (MODE.EQ.6) THEN
        WRTF=.NOT.WRTF
      ELSE IF (MODE.EQ.7) THEN
        WRITE(*,101)
        READ(*,FMT='(I6)',ERR=1)  NSIZE
        IF((NSIZE.GT.1).AND.(NIN().EQ.1).AND.(NVAR().EQ.0))NSIZE=0
      ELSE IF (MODE.EQ.8) THEN
        IF(NIN().EQ.1) THEN 
            STRMEN='% Select parameter !%'
        ELSE 
            STRMEN='% Select parameter !%SQRTS !'
        ENDIF
                 
        DO 20 J=1,NVAR()
           CALL VINF(J,VNAME,DUMMY)
           JJ=J + NIN() -2
           STRMEN(22+JJ*7:28+JJ*7) =VNAME//'!'
20      CONTINUE  
        JJ=NVAR() + NIN() -2       
        STRMEN(29+JJ*7:31+JJ*7)='!!!'
        NPAR=MENU(STRMEN,'f_D8')
        IF (NPAR.NE.0) THEN
           NUMPAR=NPAR
           IF(NIN().EQ.2) NUMPAR=NUMPAR-1
        ENDIF        
      ELSE IF (MODE.EQ.9) THEN
        WRITE(*,101)               
        READ(*,FMT='(G12.0)',ERR=1) PMIN
      ELSE IF (MODE.EQ.10) THEN
        WRITE(*,101)               
        READ(*,FMT='(G12.0)',ERR=1) PMAX
      ENDIF
      
      GOTO 1    


100   FORMAT('% BATCH calculation menu !%Add to batch file !Ncall1 = ',
     &I6,'!Itmx1 = ',I3 ,'!Ncall2 = ',I6, '!Itmx2 = ',I3,
     & '!Write to file = ',A4,'!Table size =  0 !!!' )

110   FORMAT('% BATCH calculation menu !%Add to batch file !Ncall1 = ',
     &I6,'!Itmx1 = ',I3 ,'!Ncall2 = ',I6,'!Itmx2 = ',I3,
     & '!Write to file = ',A4,'!Table size = ',I2, 
     & '!Tab. param.= ',A6,'!Min. val=',G12.6,
     & '!Max. val=',G12.6,'!!!' )



101   FORMAT('Enter new value ',$)
      END


      SUBROUTINE B_INI
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)

      LOGICAL WRTF
      COMMON/BATCH/PMIN,PMAX,NSIZE,NUMPAR,NCALL1,IT1,NCALL2,IT2,WRTF 
 
      SAVE
      NCALL1=10000
      NCALL2=10000
      IT1=5
      IT2=5
      WRTF=.TRUE.
      PMIN=0
      PMAX=100
      NSIZE=0
      NUMPAR=2-NIN()
      RETURN
      
      ENTRY B_WRT(MODE)
      WRITE(MODE,*) ' ===== BATCH PARAMETERS ====='
      WRITE(MODE,FMT='(1x,4I6,L4)') NCALL1,IT1,NCALL2,IT2,WRTF
      WRITE(MODE,FMT='(1x,2I4,2G12.4)') NSIZE,NUMPAR,PMIN,PMAX

      RETURN

      ENTRY B_RD(MODE)

      READ(MODE,*)
      READ(MODE,FMT='(1x,4I6,L4)')  NCALL1,IT1,NCALL2,IT2,WRTF
      READ(MODE,FMT='(1x,2I4,2G12.4)') NSIZE,NUMPAR,PMIN,PMAX

      RETURN

      END      
       
