*********************************************
*   scalar products for in- and out-momenta *
*********************************************
      SUBROUTINE SPROD
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      COMMON /SCLR/    DP(15)
      SAVE
      NTOT=NIN()+NOUT()
      DO 10 I=1,NTOT-1
      DO 10 J=I+1,NTOT
10    DP(INDX(I,J))=VDOT4(I,J)      
      RETURN
      END

*******************************************
*  Lorentz rotation to moving C.M. system *
*******************************************
      SUBROUTINE LORROT
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      COMMON /PVECT/P(0:3,100)
      COMMON /SQS/ SQRTS,RAPID
      DOUBLE PRECISION RAPID_, SH,CH
      DATA RAPID_/0.D0/,SH/0.D0/,CH/1.D0/
      SAVE

      IF (RAPID.NE.RAPID_) THEN
         RAPID_=RAPID
         SH=SINH(RAPID)
         CH=SQRT(1+SH**2)
      ENDIF
      
      IF( RAPID.NE.0) THEN
         DO 20 I=1,NIN()+NOUT()
         EE     =   P(0,I)
         PP     =   P(3,I)
         P(0,I) =   EE*CH + PP*SH
         P(3,I) =   EE*SH + PP*CH   
20       CONTINUE
      ENDIF
      END
*********************************
* Improved SQRT function        *
*********************************
      DOUBLE PRECISION FUNCTION VSQRT(A)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)

      IF (A .LT. 0) THEN
         VSQRT = 0 
         RETURN
      ENDIF

      VSQRT=SQRT(A)
      RETURN
      END

*********************************
* Energy of 1st decayed cluster *
*********************************
      DOUBLE PRECISION FUNCTION FEE(S,D1M2,D2M2)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)

      FEE = VSQRT(S)/2+(D1M2-D2M2)/(2*VSQRT(S))
      RETURN
      END


*******************************************
*    Scalar product of two 4-vectors:     *
*******************************************
      DOUBLE PRECISION FUNCTION VDOT4(I,J)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z),INTEGER (I-N)
      COMMON /PVECT/ P(400)
      SAVE
      IX = 4*I-3
      JX = 4*J-3
       VDOT4 = P(IX)*P(JX)-P(IX+1)*P(JX+1)-P(IX+2)*P(JX+2)
     &  -P(IX+3)*P(JX+3) 
      RETURN
      END


********************************************************
*       SUM or Difference of two 4-vectors:            *
* ISG=1  sum   and  ISG=-1   difference                *
*             P(I) + ISG*P(J)=>P(K)                    *
********************************************************
      SUBROUTINE VSUM4(I,J,K,ISG)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z),INTEGER (I-N)
      COMMON /PVECT/ P(400)
      SAVE
      IX = 4*I-3
      JX = 4*J-3
      KX = 4*K-3
      IF (ISG .EQ. 1) THEN
           P(KX) = P(IX) + P(JX)
           P(KX+1) = P(IX+1) + P(JX+1)
           P(KX+2) = P(IX+2) + P(JX+2)
           P(KX+3) = P(IX+3) + P(JX+3)
      ELSE
           P(KX) = P(IX) - P(JX)
           P(KX+1) = P(IX+1) - P(JX+1)
           P(KX+2) = P(IX+2) - P(JX+2)
           P(KX+3) = P(IX+3) - P(JX+3)
      ENDIF
      RETURN
      END 


*******************************************
* NULLification of a 4-vector:  P(I) => 0 *
*******************************************
      SUBROUTINE VNULL4(I)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z),INTEGER (I-N)
      COMMON /PVECT/ P(400)
      SAVE
      IX=4*I-3
      P(IX) = 0
      P(IX+1) = 0
      P(IX+2) = 0
      P(IX+3) = 0
      RETURN
      END 

      
      SUBROUTINE EPS4(N1,N2,N3,N4)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      COMMON /PVECT/ P(0:3,100)
      SAVE /PVECT/ 
      A10=P(0,N1)
      A11=P(1,N1)
      A12=P(2,N1)
      A13=P(3,N1)

      A20=P(0,N2)
      A21=P(1,N2)
      A22=P(2,N2)
      A23=P(3,N2)

      A30=P(0,N3)
      A31=P(1,N3)
      A32=P(2,N3)
      A33=P(3,N3)
      
C                               A10  A20  A30  X0
C                               A11  A21  A31  X1
C                               A12  A22  A32  X2
C                               A13  A23  A33  X3


      
      D1021=A10*A21-A20*A11
      D1022=A10*A22-A20*A12
      D1023=A10*A23-A20*A13
      D1122=A11*A22-A21*A12
      D1123=A11*A23-A21*A13
      D1223=A12*A23-A22*A13
      
      P(0,N4)=+(            A31*D1223 - A32*D1123 + A33*D1122)
      P(1,N4)=  A30*D1223 -             A32*D1023 + A33*D1022
      P(2,N4)=-(A30*D1123 - A31*D1023 +             A33*D1021)
      P(3,N4)=  A30*D1122 - A31*D1022 + A32*D1021

      
      
      RETURN
      END
      
