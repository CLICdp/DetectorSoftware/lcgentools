C      SUBROUTINE WRT(MES,LVV)
C      SUBROUTINE WRTKIN(NCHAN,NDEC)
C      ENTRY RDRKIN(NCHAN,NDEC)
C      FUNCTION FILL(LVAR,I)
C      FUNCTION LVEQAL(LV1,LV2)
C      SUBROUTINE USKIN
C      SUBROUTINE ENTKIN

C     ENTKIN ==> USKIN,WRT,FILL,LVEQAL   




      SUBROUTINE WRT(MES,LVV)
      COMMON /XY/ NUMBX,NUMBY
      SAVE /XY/
      DIMENSION LVV(10)
      CHARACTER*16 MES
      CHARACTER*2 YCHAR,XCHAR
      character*2 doll
      common /doll/ doll
      
      YCHAR=' Y'
      XCHAR=' X'


      WRITE(*,FMT='(1X,A16'//doll//')') MES
      IF (LVV(1).EQ.0)  THEN
         WRITE(*,*)
         RETURN
      ENDIF
      I=1
1     IF ((LVV(I).NE.NUMBX).AND.(LVV(I).NE.NUMBY)) THEN
        WRITE(*,FMT='(I4'//doll//')') LVV(I)
      ELSE 
        IF (LVV(I).EQ.NUMBY) THEN
          WRITE(*,FMT='(A4'//doll//')') YCHAR  
        ELSE
          WRITE(*,FMT='(A4'//doll//')') XCHAR  
        ENDIF
      ENDIF     
      I=I+1
      IF ( LVV(I).NE.0) GO TO 1       
      WRITE(*,*)
      RETURN
      END



      FUNCTION FILL(LVAR,I)
      LOGICAL FILL
      COMMON /XY/ NUMBX,NUMBY
      SAVE /XY/
      DIMENSION LVAR(10,10)
      CHARACTER*1 RDAR(50),CC


      READ(*,FMT='(50A1)') RDAR
      
      DO 5 J=1,10
        LVAR(J,I)=0
5     CONTINUE

      FILL=.FALSE.

      NCR=1
      DO 10 J=1,50
        CC=RDAR(J)
        IF(NCR.EQ.1) THEN
           IF ((CC.EQ.'X').OR.(CC.EQ.'x')) THEN 
             LVAR(NCR,I)=NUMBX 
             RETURN
           ELSE   IF ((CC.EQ.'R').OR.(CC.EQ.'r')) THEN            
             FILL=.TRUE.
             RETURN 
           ENDIF         
        ENDIF
        
        NN=ICHAR(CC)-ICHAR('0')
        IF ((NN.GT.0).AND.(NN.LT.10)) THEN
           LVAR(NCR,I)=NN
           NCR=NCR+1        
           IF (NCR.EQ.10) RETURN
        ENDIF 
10    CONTINUE  
           

      RETURN
      END  


      FUNCTION LVEQAL(LV1,LV2)
      LOGICAL LVEQAL
      DIMENSION LV1(10),LV2(10),ICMP(10),ICMP2(10)

      DO 5 J=1,10
        ICMP(J)=LV1(J)
        ICMP2(J)=LV2(J)
5     CONTINUE
      DO 10 J=1,10
      DO 10 K=1,10
        IF (ICMP2(K).EQ.ICMP(J)) THEN
          ICMP(J)=0
          ICMP2(K)=0
        ENDIF
10    CONTINUE

      DO 20 J=1,10
        IF ((ICMP(J).NE.0).OR.(ICMP2(J).NE.0)) THEN
          LVEQAL=.FALSE.
          GO TO 25
        ENDIF
20    CONTINUE        
      LVEQAL=.TRUE.

25    CONTINUE
      RETURN
      END      



      SUBROUTINE USKIN
      character*2 doll
      common /doll/ doll
      COMMON /INOUT/ NNIN,NNOUT
      COMMON /KINMTC/ LVIN(10,10), LVOUT(10,10,2)
      COMMON /XY/ NUMBX,NUMBY
      DIMENSION ICURRN(10,2),ICURR2(10,2),ICMP(10),ISHIFT(10)
      LOGICAL FILL

      COMMON /DEVICE/  IPRT,ISES,IBAT,ISTR(2),IEVENT,ITMP

      SAVE /DOLL/,/INOUT/,/KINMTC/,/XY/,/DEVICE/
      NCR=1
      NCR2=1
      I=1
1     CONTINUE            

      DO 2 J=1,10
      DO 2 K=I,10
      LVIN(J,K)=0
      DO 2 L=1,2
      LVOUT(J,K,L)=0
2     CONTINUE

      IF (I.NE.1) THEN
       DO 3 J=1,I-1
       write(*,*)
       WRITE(*,FMT='(1X,''-------- Decay '',I2,'' ---------'')') J
       CALL WRT('IN state:       ',LVIN(1,J))
       CALL WRT('1st cluster:    ',LVOUT(1,J,1))
       CALL WRT('2nd cluster:    ',LVOUT(1,J,2))
3      CONTINUE       
      ENDIF

** Fill the vector of the in-particles for this decay      
      IF (I.EQ.1) THEN
        LVIN(1,1)=1
        IF (NIN().EQ.2) LVIN(2,1)=2
      ELSE       
        DO 4 J=1,10 
          LVIN(J,I)=LVOUT(J,ICURRN(NCR-1,1),ICURRN(NCR-1,2))
4       CONTINUE
        NCR=NCR-1
      ENDIF 

*** Fill the vector of the out-particles of the 1-st cluster 
*** and check with in-cluster     
5     write(*,*)
      write(*,*)
      WRITE(*,FMT='(1X,''-------- Input of Decay '',I2,'
     &//''' ---------'')') I
      CALL WRT('IN state:       ',LVIN(1,I))
      WRITE(*,FMT='(5X,''Enter particle(s) in 1st cluster: '''
     &//doll//')')
      
      IF ( (I.GT.1).AND.(LVIN(3,I).EQ.0) ) THEN
         WRITE(*,*) LVIN(1,I),' (autoinput)'
         LVOUT(1,I,1)= LVIN(1,I)
         LVOUT(2,I,1)=0
         GOTO  201
      ENDIF
      
      IF ( (I.EQ.1).AND.(NOUT().EQ.2)) THEN
         WRITE(*,*) NIN()+1 ,' (autoinput)' 
         LVOUT(1,I,1)= NIN()+1
         LVOUT(2,I,1)=0
         GOTO  201
      ENDIF
                  
          
      IF (FILL(LVOUT,I)) THEN
       IF (I.GT.1) THEN
        I=I-1
        NCR=NCR+1-ISHIFT(NCR2-1)
        ICURRN(NCR,1)=ICURR2(NCR2-1,1)
        ICURRN(NCR,2)=ICURR2(NCR2-1,2)
        NCR2=NCR2-1
        NCR=NCR+1
        GO TO 1
       ELSE
        call r_sess(ises)
        GO TO 101
       ENDIF 
      ENDIF
      
201   IF (LVOUT(1,I,1).EQ.0) THEN 
        WRITE(*,*) 'ERROR: 1st cluster is empty!'
        write(*,*)
        GO TO 5
      ENDIF 
      DO 6 J=1,10
        ICMP(J)=LVOUT(J,I,1)
6     CONTINUE 
      IF (I.EQ.1) THEN
        DO 8 J=1,10
         IF ((ICMP(J).GT.NIN()).AND.(ICMP(J).LE.(NIN()+NOUT()))) THEN
           ICMP(J)=0
         ENDIF
8       CONTINUE        
        DO 10 J=1,10
          IF (ICMP(J).NE.0) THEN
          WRITE(*,*) 'ERROR: particle(s) have to be from out state!'
        write(*,*)
          GO TO 5
          ENDIF
10      CONTINUE 
      ENDIF
      DO 11 K=1,10
      DO 11 J=1,10
       IF (LVIN(K,I).EQ.ICMP(J)) ICMP(J)=0                             
11    CONTINUE
      DO 12 J=1,10
        IF (ICMP(J).NE.0) THEN
      WRITE(*,*) 'ERROR: cluster particle(s) have to be from IN state'//
     & ' of this decay !'
        write(*,*)
          GO TO 5
        ENDIF
12    CONTINUE        

*** Fill the vector of the out-particles of the 2-nd cluster      
      DO 15 J=1,10
        ICMP(J)=LVIN(J,I)
15    CONTINUE
      IF (I.EQ.1) THEN
        DO 16 J=1,10
          IF (J.LE.(NOUT())) THEN
            ICMP(J)=NIN()+J
          ELSE 
           ICMP(J)=0
          ENDIF  
16      CONTINUE        
      ENDIF
      DO 17 K=1,10
      DO 17 J=1,10
       IF (LVOUT(K,I,1).EQ.ICMP(J)) ICMP(J)=0
17    CONTINUE
      K=1
      DO 18 J=1,10
        IF (ICMP(J).NE.0) THEN
         LVOUT(K,I,2)=ICMP(J)
         K=K+1
        ENDIF
18    CONTINUE        
              
      CALL WRT('1st cluster:    ',LVOUT(1,I,1))
      CALL WRT('2nd cluster:    ',LVOUT(1,I,2))


C***  Fill the ATH vector 

*** Fill the POLE vector and check one




*** Calculate the current number of the next decayed cluster
      IF (I.NE.1) THEN 
         ICURR2(NCR2,1)=ICURRN(NCR,1)
         ICURR2(NCR2,2)=ICURRN(NCR,2)
         ISHIFT(NCR2)=0           
         NCR2=NCR2+1
      ENDIF 


      J=1
87    IF ((J.LT.10).AND.(LVOUT(J,I,2).NE.0)) THEN
        J=J+1
        GO TO 87
      ENDIF
      IF (J.EQ.1) THEN
       WRITE(*,*) 'ERROR: 2nd cluster is empty!'
        write(*,*)
       GO TO 5
      ELSE 
      IF (J.NE.2) THEN
        IF (I.NE.1) ISHIFT(NCR2-1)=ISHIFT(NCR2-1)+1
        ICURRN(NCR,1)=I
        ICURRN(NCR,2)=2
        NCR=NCR+1
      ENDIF
      ENDIF
        
      J=1
91    IF ((J.LT.10).AND.(LVOUT(J,I,1).NE.0)) THEN
        J=J+1
        GO TO 91
      ENDIF 
      IF (J.NE.2) THEN
        IF (I.NE.1) ISHIFT(NCR2-1)=ISHIFT(NCR2-1)+1
        ICURRN(NCR,1)=I
        ICURRN(NCR,2)=1
        NCR=NCR+1
      ENDIF
     

100   CONTINUE
      IF (I.LT.(NOUT()-1)) THEN
       I=I+1
       GO TO 1
      ENDIF       

101   RETURN
      END


      SUBROUTINE ENTKIN
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      CHARACTER STRMEN*400,DOLL*2,PROCES*30,DCH*10
      COMMON /PROCES/ NSUB,PROCES
      COMMON /DOLL/ DOLL
      COMMON /INOUT/ NNIN,NNOUT
      COMMON /KINMTC/ LVIN(10,10),LVOUT(10,10,2)
      COMMON /DEVICE/  IPRT,ISES,IBAT,ISTR(2),IEVENT,ITMP

      COMMON /NSESS/ NSESS
      SAVE /PROCES/,/DOLL/,/INOUT/,/KINMTC/,/DEVICE/,/NSESS/

      CALL ITOCHR(DCH,NSESS)

10    CALL CLRSCR
      CALL WRTKIN(6,NOUT()-1)

      STRMEN='%Kinematics menu!'//
     &       PROCES//'                Session # '//DCH//'!%'//
     &       'Input new kinematics!!!'

      NKN=MENU(STRMEN,'f_5')
      IF (NKN.EQ.0) THEN
         GOTO 1000
      ELSEIF (NKN.EQ.1) THEN
         call w_sess(ises)
         CALL USKIN
      ENDIF
      GOTO 10

1000  RETURN
      END      


      
      SUBROUTINE STDKIN
      COMMON /KINMTC/ LVIN(10,10), LVOUT(10,10,2)
      SAVE /KINMTC/ 
      NVPOSX=NIN()+NOUT()+1
      NVPOSY=NVPOSX+1
      DO 1 J=1,10
      DO 1 I=1,10
      LVIN(J,I)=0
      DO 1 K=1,2
      LVOUT(J,I,K)=0
1     CONTINUE
        
      DO 10 I=1,NOUT()-1
         LVOUT(1,I,1)=I+NIN()
         DO 5 J=1,NOUT()-I
5           LVOUT(J,I,2)=I+NIN()+J         
10    CONTINUE   


   
      LVIN(1,1)=1      
      IF (NIN().EQ.2)   LVIN(2,1)=2       
      DO 20 I=2,NOUT()-1
         LVIN(1,I)=I+NIN()
         DO 25 J=1,NOUT()-I
            LVIN(J+1,I)=I+NIN()+J
25    CONTINUE
20    CONTINUE   

            
      END

