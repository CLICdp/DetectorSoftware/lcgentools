*#########################################################
*#  Procedures to read-write session or batch files      #
*#   contents:                                           #
*#    W_SESS, INIPAR, R_SESS                              #
*#########################################################

*********************************************
*      Write parameters to the files        *
*                                           *
*  MODE=ISES in session parameters file     *
*  MODE=IBAT in batch file                  *
*  else in the protocol file                *
*********************************************
      SUBROUTINE W_SESS(MODE)
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)

      COMMON /DEVICE/  IPRT,ISES,IBAT,ISTR(2),IEVENT,ITMP
      CHARACTER*12  PRTNAM,SESNAM,BATNAM
      COMMON /DEVNAM/  PRTNAM,SESNAM,BATNAM

      SAVE

      IF (MODE .EQ. ISES) THEN
        OPEN(ISES, FILE=SESNAM,STATUS='UNKNOWN')
      ENDIF
      CALL W_PRC(MODE)
      CALL WNSESS(MODE)
      CALL W_MC(MODE)
      CALL W_IN(MODE)
      CALL W_MDL(MODE)
      CALL WRTKIN(MODE,NOUT()-1)
      CALL WRTCUT(MODE)
      CALL W_TCUT(MODE)
      CALL WRTREG(MODE)
      CALL W_QCD(MODE)
      CALL W_WDTH(MODE)
      CALL B_WRT(MODE)
      CALL W_USR(MODE)
      IF (MODE .EQ. ISES) THEN
        ENDFILE(ISES)
        CLOSE(ISES)
      ENDIF
      RETURN

************************************************
*      Read parameters from file               *
*                                              *
*   MODE = ISES   from session parameters file *
*   MODE = IBAT   from batch file              *
************************************************
      ENTRY R_SESS(MODE)

      IF (MODE .EQ. ISES) THEN
          OPEN(ISES,FILE=SESNAM,STATUS='UNKNOWN')
      ENDIF

      ICC=0
      CALL R_PRC(MODE,ICC)
      IF ((MODE.EQ.ISES).AND.(ICC.EQ.1)) THEN
          CLOSE(ISES)
          CALL INIPAR
          GOTO 1000
      ENDIF

      CALL RNSESS(MODE)
      CALL R_MC(MODE)
      CALL R_IN(MODE)
      CALL R_MDL(MODE)
      CALL RDRKIN(MODE)
      CALL RDRCUT(MODE)
      CALL R_TCUT(MODE)
      CALL RDRREG(MODE)
      CALL R_QCD(MODE)
      CALL R_WDTH(MODE)
      CALL B_RD(MODE)
      CALL R_USR(MODE)

      IF (MODE.EQ.ISES) THEN
        CLOSE(ISES)
      ENDIF

1000  CONTINUE
      RETURN
      END

**********************
      SUBROUTINE INIPAR
        CALL I_PRC
        CALL VINI
C                     from comphep output
        CALL INSESS
        CALL I_MC
        CALL I_IN
        CALL I_MDL
        CALL INICUT
        CALL I_TCUT
        CALL INIREG
        CALL STDKIN
        CALL I_QCD
        CALL I_WDTH
        CALL B_INI
        CALL I_USR
    
      RETURN
      END
