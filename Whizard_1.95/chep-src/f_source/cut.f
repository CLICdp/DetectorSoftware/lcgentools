      DOUBLE PRECISION FUNCTION CALCUT()
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL MINON,MAXON
      COMMON/INVCUT/ LVINVC(10,60),MINON(60),MAXON(60),
     &CVMIN(60),CVMAX(60)
      SAVE /INVCUT/
      CALCUT=1
      I=1
1     IF (LVINVC(1,I).EQ.0) RETURN
      NVBUFF=NIN()+NOUT()+3
      CALL LVTONV(LVINVC(1,I),NVBUFF)
      VINV=VDOT4(NVBUFF,NVBUFF)
      IF (MINON(I).AND.(VINV.LE.CVMIN(I)) ) THEN
         CALCUT=0
         RETURN
      ELSE
        IF (MAXON(I).AND.(VINV.GE.CVMAX(I)) ) THEN
           CALCUT=0
           RETURN
        ENDIF
      ENDIF
      I=I+1
      GOTO 1
      END





      SUBROUTINE INICUT
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL MINON,MAXON 
      COMMON/INVCUT/ LVINVC(10,60),MINON(60),MAXON(60),
     &CVMIN(60),CVMAX(60)
      SAVE /INVCUT/
      LVINVC(1,1)=0
      RETURN
      END
    

      SUBROUTINE DELCUT(NCUT)      
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL MINON,MAXON 
      COMMON/INVCUT/ LVINVC(10,60),MINON(60),MAXON(60),
     &CVMIN(60),CVMAX(60)
      SAVE /INVCUT/
      IF(NCUT.LE.0) GOTO 40       
      DO 10 I=1,NCUT    
10    IF (LVINVC(1,I).EQ.0) RETURN
      I=NCUT
20    I1=I+1
      J=1 
30    NP=LVINVC(J,I1)
      LVINVC(J,I)=NP
      IF (NP.NE.0) THEN
         J=J+1
         GOTO 30           
      ENDIF
      MINON(I)=MINON(I1)
      MAXON(I)=MAXON(I1)
      CVMIN(I)=CVMIN(I1)
      CVMAX(I)=CVMAX(I1)
      IF (LVINVC(1,I).EQ.0) RETURN
      I=I+1
      GOTO 20
      
40    RETURN
      END 
      
      SUBROUTINE CHNCUT(NCUT)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)      
      LOGICAL MINONW,MAXONW
      LOGICAL MINON,MAXON  
      COMMON/INVCUT/ LVINVC(10,60),MINON(60),MAXON(60),
     &CVMIN(60),CVMAX(60)
      SAVE /INVCUT/
      IF(NCUT.LE.0)RETURN
      DO 10 I=1,NCUT    
10    IF (LVINVC(1,I).EQ.0) RETURN

      CALL VALCUT(MINONW,MAXONW,CVMINW,CVMAXW)

      IF ( MINONW.AND.MAXONW.AND.(CVMINW.GE.CVMAXW)) RETURN
      IF (.NOT.(MINONW.OR.MAXONW) ) RETURN
      MINON(NCUT)=MINONW
      MAXON(NCUT)=MAXONW
      CVMIN(NCUT)=CVMINW
      CVMAX(NCUT)=CVMAXW 
      RETURN
      END
      
      
      SUBROUTINE ADDCUT(LV,MINONW,MAXONW,CVMINW,CVMAXW)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)            
      LOGICAL MINON,MAXON        
      COMMON/INVCUT/ LVINVC(10,60),MINON(60),MAXON(60),
     &CVMIN(60),CVMAX(60)
      SAVE /INVCUT/

      LOGICAL MINONW,MAXONW,EQVECT
      DIMENSION LV(10)
       
      IF ( MINONW.AND.MAXONW.AND.(CVMINW.GE.CVMAXW)) RETURN
      IF (.NOT.(MINONW.OR.MAXONW) ) RETURN
      NCUT=1
10    IF((LVINVC(1,NCUT).NE.0).AND.(.NOT.EQVECT(LV,LVINVC(1,NCUT))))
     &THEN
         NCUT=NCUT+1
         GOTO 10
      ENDIF
      IF (NCUT.GE.60) RETURN
      J=1 
30    NP=LV(J)
      LVINVC(J,NCUT)=NP
      IF (NP.NE.0) THEN
         J=J+1
         GOTO 30           
      ENDIF
      
      MINON(NCUT)=MINONW
      MAXON(NCUT)=MAXONW
      CVMIN(NCUT)=CVMINW
      CVMAX(NCUT)=CVMAXW 
       

      RETURN
      END
      
      SUBROUTINE VIEWCT
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      CHARACTER PTXT(20),doll*2
      common /doll/ doll
      CHARACTER*25 STATUS
      CHARACTER*1 MOM(5)
      DIMENSION LAUX(10)
      LOGICAL MINON,MAXON  
      COMMON/INVCUT/ LVINVC(10,60),MINON(60),MAXON(60),
     &CVMIN(60),CVMAX(60)
      CHARACTER*1 CH_MIN,CH_MAX
      SAVE /INVCUT/
      IF (LVINVC(1,1).EQ.0) THEN
        WRITE(*,*) '***********  No invariant cuts *********** '
        write(*,*)
        RETURN
      ENDIF   
      WRITE(*,1001)
1001  FORMAT(1X,30('-'),' C U T S ',38('-') )
      WRITE(*,1002)
1002  FORMAT(1X,'|  N |    MIN VALUE    | INVARIANT            |',
     &'   MAX VALUE     |  POSITION  |')

      WRITE(*,1003)
1003  FORMAT(1X,'|    |    [GeV**2]     |                      |',
     &'    [GeV**2]     |            |')

      WRITE(*,1004)
1004  FORMAT(1X, 78('-') )     
      I=1
10    IF (LVINVC(1,I).EQ.0) THEN
      WRITE(*,1005)
1005  FORMAT(1X,78('-')) 
      write(*,*)

      RETURN
      ENDIF
      
      J=1
      K=2
      PTXT(1)='(' 
      NP= LVINVC(J,I) 
15    IF (NP.LT.0) THEN
         PTXT(NEXTNN(K))='-'
         NP=-NP
      ELSE 
         PTXT(NEXTNN(K))='+'
      ENDIF  
      PTXT(NEXTNN(K))='p'
      PTXT(NEXTNN(K))=CHAR(NP+48)      
      J=J+1
      NP= LVINVC(J,I)
      IF (NP .NE.0) GOTO 15 
      
      PTXT(NEXTNN(K))=')'
      PTXT(NEXTNN(K))='*'
      PTXT(NEXTNN(K))='*'
      PTXT(NEXTNN(K))='2'
      
      DO 20 J=K,20
20    PTXT(J)=' '


      CALL SNGPOS(LVINVC(1,I),NDEC,NCLUST,LAUX )
      J=1
210    IF (LAUX(J).NE.0) THEN
         WRITE(MOM(J),FMT='(I1)') ABS(LAUX(J))
         J=J+1
         GOTO 210
      ENDIF
      J1=J
      DO 220 J=J1,5
220    MOM(J)=' '
      WRITE(STATUS,1010) NDEC,NCLUST,(MOM(kk),kk=1,5)
1010  FORMAT('S',I1,',L',I1,',P', 5(A1) )

      
      IF(MINON(I)) THEN
         IF (CVMIN(I).EQ.0) THEN
            CH_MIN=' '
            VAL_MIN=0
         ELSE IF ( CVMIN(I).GT.0) THEN
            CH_MIN=' '
            VAL_MIN= SQRT(CVMIN(I))
         ELSE
            CH_MIN='-'
            VAL_MIN= SQRT(-CVMIN(I))
         ENDIF
      ENDIF

      IF(MAXON(I)) THEN
         IF (CVMAX(I).EQ.0) THEN
            CH_MAX=' '
            VAL_MAX=0
         ELSE IF ( CVMAX(I).GT.0) THEN
            CH_MAX=' '
            VAL_MAX= SQRT(CVMAX(I))
         ELSE
            CH_MAX='-'
            VAL_MAX= SQRT(-CVMAX(I))
         ENDIF
      ENDIF

      IF (MINON(I).AND.MAXON(I)) THEN      
      WRITE(*,1006)  I,CH_MIN,VAL_MIN,PTXT,CH_MAX,VAL_MAX,STATUS
1006  FORMAT(1X,'|',I3,' |',A1,'(',1PE10.3,')**2 < ',20A1,' <',
     & A1,'(',E10.3,')**2 |',A12,'|') 
      ELSE
      IF (MINON(I)) THEN 
      WRITE(*,1007)I,CH_MIN,VAL_MIN ,PTXT,STATUS
1007  FORMAT(1X,'|',I3,' |',A1,'(',1PE10.3,')**2 < ',20A1,' |',16X,
     &' |', A12,'|') 
      ELSE
      WRITE(*,1008)I,PTXT,CH_MAX,VAL_MAX,STATUS
1008  FORMAT(1X,'|',I3,' |',16X,' | ',20A1,' <',A1,'(',1PE10.3,
     &')**2 |', A12,'|') 
      ENDIF
      ENDIF 
      
      
25    CONTINUE
      I=I+1
      IF (MOD(I,17).EQ.16) THEN
         WRITE(*,FMT='(1x,''Press ENTER to continue'''//doll//')')    
         READ(*,*)         
      ENDIF
      GOTO 10
      
      END 


      SUBROUTINE VALCUT(MINW,MAXW,VMIN,VMAX)
      DOUBLE PRECISION VMIN,VMAX
      LOGICAL MINW,MAXW
      CHARACTER doll*2
      common /doll/ doll


         MINW=.TRUE.
         WRITE(*,FMT='(1X,''Enter MIN value for Inv/SQRT(ABS(Inv))'//
     & '(in GeV): '''//doll//')') 
         READ(*,FMT='(G15.0)',ERR=30 ) VMIN
         VMIN=VMIN*ABS(VMIN)
         GOTO 31
30       MINW=.FALSE.
31       CONTINUE

         MAXW=.TRUE.
         WRITE(*,FMT='(1X,''Enter MAX value for Inv/SQRT(ABS(Inv))'// 
     & ' (in GeV): '''//doll//')') 
         READ(*,FMT='(G15.0)',ERR=40 ) VMAX
         VMAX=VMAX*ABS(VMAX) 
         GOTO 41
40       MAXW=.FALSE.
41       CONTINUE

      RETURN
      END


      SUBROUTINE NEWCUT
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL MINW,MAXW
      CHARACTER PLIST(20),DOLL*2
      COMMON /DOLL/ DOLL
      CHARACTER PROCES*30
      COMMON /PROCES/ NSUB,PROCES
      DIMENSION LV(21)
      LOGICAL MINON,MAXON  
      COMMON/INVCUT/ LVINVC(10,60),MINON(60),MAXON(60),
     &CVMIN(60),CVMAX(60)
      COMMON /NSESS/ NSESS
      SAVE /DOLL/,/PROCES/,/INVCUT/,/NSESS/ 


         WRITE(*,
     &   FMT='(1X,''Enter particle number(s) for an invariant: '''
     & // doll//')')
         READ(*,FMT='(20A1)') PLIST
         LPOS=1
         DO 10 K=1,20
           NCH=ICHAR(PLIST(K))-ICHAR('0')
           IF ( (1.LE.NCH).AND.(NCH.LE.NIN()+NOUT()) ) 
     &        LV(NEXTNN(LPOS))=NCH
10       CONTINUE 
         LV(LPOS)=0
         CALL ORDINV(LV)
         CALL CONINV(LV)         
         IF (LV(1).EQ.0) THEN
           WRITE(*,*) 'Incorrect input!'
         ELSE
           CALL VALCUT(MINW,MAXW,VMIN,VMAX)            
           CALL ADDCUT(LV,MINW,MAXW,VMIN,VMAX)
         ENDIF           
         RETURN     
      END

      SUBROUTINE WRTCUT(NCHAN)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL MINON,MAXON  
      COMMON/INVCUT/ LVINVC(10,60),MINON(60),MAXON(60),
     &CVMIN(60),CVMAX(60)
      SAVE /INVCUT/
      LOGICAL ZERO
      CHARACTER*1 P(6),S(6),NP(6) 
      CHARACTER*50 TXT1,TXT2

      DATA CMAX/1.D30/
      N=1
10    IF (LVINVC(1,NEXTNN(N)).NE.0) GOTO 10   
      N=N-2
      TXT1='=================== There are introduced '
      TXT2=' invariant cuts ==================='
      WRITE(NCHAN,110) TXT1,N,TXT2 
      TXT1=' <  ('
      TXT2= ')**2  < '
      DO 50 I=1,N
         ZERO=.FALSE.
         DO 20 K=1,6
         IF (ZERO) THEN
            P(K)=' '
            S(K)=' '
            NP(K)=' '            
         ELSE 
            P(K)='p'
            IF (LVINVC(K,I).GT.0) THEN
                S(K)='+'
            ELSE
                S(K)='-'
            ENDIF        
            NP(K)=CHAR(48+IABS(LVINVC(K,I)))
         
            ZERO=(LVINVC(K+1,I).EQ.0)
         ENDIF
20       CONTINUE
         IF (.NOT.MINON(I)) CVMIN(I)=-CMAX
         IF (.NOT.MAXON(I)) CVMAX(I)= CMAX
50       WRITE(NCHAN,120) CVMIN(I),  TXT1 ,
     &    (S(K),P(K),NP(K), K=1,6),  TXT2, CVMAX(I)
      RETURN
      

110   FORMAT(1X,A41,I2,A35)
120   FORMAT(1X,1PE17.10,A5,6(A1,A1,A1),A8,E17.10)


      ENTRY RDRCUT(NCHAN)
      READ(NCHAN, 110) TXT1,N,TXT2 
      DO 60 I=1,N
        READ(NCHAN,120) CVMIN(I),TXT1,
     &    (S(K),P(K),NP(K),K=1,6),TXT2,CVMAX(I)
        DO 65 K=1,6
          IF (NP(K).EQ.' ') THEN 
            LVINVC(K,I)=0
          ELSE
             LVINVC(K,I)=ICHAR(NP(K))-48
             IF(S(K).EQ.'-')  LVINVC(K,I)=-LVINVC(K,I)
         ENDIF        
65      CONTINUE  
        IF (CVMIN(I).LE. -0.999*CMAX) THEN 
            MINON(I)=.FALSE.
        ELSE
           MINON(I)=.TRUE.   
        ENDIF
        IF (CVMAX(I).GE. 0.999*CMAX) THEN
           MAXON(I)=.FALSE.         
        ELSE
           MAXON(I)=.TRUE.
        ENDIF

60    CONTINUE
      LVINVC(1,N+1)=0           
      
      RETURN
      END
      
      FUNCTION NCUT(LV)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)            
      LOGICAL MINON,MAXON        
      COMMON/INVCUT/ LVINVC(10,60),MINON(60),MAXON(60),
     &CVMIN(60),CVMAX(60)
      SAVE /INVCUT/
      LOGICAL EQVECT
      DIMENSION LV(10)
       
      CALL CONINV(LV)
      NCUT=1
10    IF((LVINVC(1,NCUT).NE.0).AND.(.NOT.EQVECT(LV,LVINVC(1,NCUT))))
     &THEN
         NCUT=NCUT+1
         GOTO 10
      ENDIF
      IF (LVINVC(1,NCUT).EQ.0) NCUT=0 
      RETURN
      END
      
      SUBROUTINE RANCOR(VMIN,VMAX,SHIFT,FMULT,N)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)            
      LOGICAL MINON,MAXON        
      COMMON/INVCUT/ LVINVC(10,60),MINON(60),MAXON(60),
     &CVMIN(60),CVMAX(60)
      SAVE /INVCUT/
      IF (N.EQ.0) RETURN

      IF (MINON(N)) THEN
         VNEW=(CVMIN(N)-SHIFT)*FMULT
         IF (FMULT.GT.0) THEN
            VMIN=DMAX1(VMIN,VNEW)
         ELSE
            VMAX=DMIN1(VMAX,VNEW)
         ENDIF   
      ENDIF
      
      IF (MAXON(N)) THEN
         VNEW=(CVMAX(N)-SHIFT)*FMULT
         IF (FMULT.GT.0) THEN
            VMAX=DMIN1(VMAX,VNEW)
         ELSE
            VMIN=DMAX1(VMIN,VNEW)           
         ENDIF   
      ENDIF
      
      RETURN
      END
