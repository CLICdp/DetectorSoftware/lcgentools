*************************************************************
*                       Menu function                       *
*************************************************************
*  Input:                                                   *
*                                                           *
*    STRMEN - menu string, list of records                  *
*             (each record has to finished by the symbol    *
*                             '!' );                        *
*             menu string has to be finished by three       *
*             symbols '!' immediately after last record;    *
*             a number of records in the beginning of the   *
*             menu string can be included between two       *
*             symbols '%' - in this case these records      *
*             are treated as a menu header.                 *
*                                                           *
*    Example:                                               *
*         STRMEN= '% menu1 ! comment !% POS1 ! POS2 !!!'    *
*************************************************************
*  Output: menu return code -   pressed key                 *
*************************************************************
*     There are possible two menu modes:                    *
* single column mode  - all position records in one column, *
* double column mode  - position records are displayed in   *
*                        two columns.                       *
*     These two modes are choosen  automatically.           *
*                                                           *
*     Max number of displayed position records:             *
*               18       in single column mode,             *
*               36       in double column mode.             *
*                                                           *
*     Max number of displayed symbols in one record:        *
*        in header record -                        66,      *
*        in menu position record (single mode) -   66,      *
*        in menu position record (double mode) -   28.      *
*************************************************************
*    Menu border is initialized by default.                 *
*    Reset menu border - call SETBRD(BRDSTR) with parameter *
*       BRDSTR='vh123456', where symbols used as            *
*              1hhhhhhhhhh2                                 *
*              v          v                                 *
*              3hhhhhhhhhh4                                 *
*              v          v                                 *
*              v          v                                 *
*              5hhhhhhhhhh6                                 *
*    Return default border - call DEFBRD without parameters *
*************************************************************
      INTEGER FUNCTION MENU(STRMEN,HMENU)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z),INTEGER (I-N)
c      CHARACTER*5000 STRMEN
      CHARACTER STRMEN*(*), HMENU*(*)
      CHARACTER CHBORD*8,CCODE*4,XFMT*10,DOLL*2,DCH*2
      COMMON /DOLL/ DOLL
      COMMON /MBACK/ MBACK
      COMMON /NBRD/  NBRD,CHBORD

         CHARACTER PATH*60,D_SLASH*1,F_SLASH*1
         CHARACTER*100 F_NAME,BEG
         LOGICAL XFILE


      SAVE
      
*********************
**  DEFAULT BORDER **
      IF (NBRD.EQ.0) THEN
          NBDR=1
          CHBORD='********'
      ENDIF
      
************************************************
***  Max header record                      ****
************************************************
       MAXHDR=0
       HDR0=1
       LLX0=0
       IF (STRMEN(1:1).EQ.'%') THEN
         DO 30 K=2,2000
           IF (STRMEN(K:K).EQ.'!') THEN
             IF(MAXHDR.LT.(K-HDR0)) THEN
                  MAXHDR=(K-HDR0)
             ENDIF     
             HDR0=K
           ENDIF
           IF(STRMEN(K:K).EQ.'%') THEN
              LLX0=K
              GOTO 40
           ENDIF
30       CONTINUE           
       ENDIF
************************************************
***  Max position record                    ****
************************************************
40     MAXLL=0
       LLX=LLX0
       DO 50 I=LLX0+1,2000
         IF(STRMEN(I:I).EQ.'!') THEN
           IF(MAXLL.LT.(I-LLX)) THEN 
             MAXLL=(I-LLX)
           ENDIF
           LLX=I
           IF(STRMEN(I+1:I+1).EQ.'!') GOTO 70
         ENDIF
50     CONTINUE

*********************************************
*** Max width of menu and records         ***
*********************************************
70    MAXLEN=MAXHDR+10

      IF (2*MAXLL.LE.58) GOTO 80
      IF (MAXLEN.LT.(MAXLL+10)) MAXLEN=MAXLL+10
      GOTO 90
80    IF (MAXLEN .LT. (2*MAXLL+20)) MAXLEN=2*MAXLL+20

90    IF (MAXLEN.GE.76) MAXLEN=76
      IF (MAXLEN.LT.40) MAXLEN=40

*** length of header record
      LREC1=MAXLEN-10
*** length of position record      
      LREC2=(MAXLEN-20)/2

*** 1- or 2-column menu
      IF (MAXLL.GT.LREC2) THEN
            NCOL=1
      ELSE
            NCOL=2
      ENDIF

***********************************************
***  loop for return code (MENU)
***********************************************
95    NPOS=0
      IF (MBACK.EQ.1) THEN
           MENU=0
           GOTO 110
      ENDIF
1     CONTINUE
C*      CALL CLRSCR
      LREC10=LREC1

      CALL MENU1(MAXLEN,NCOL,LREC10,LREC2,STRMEN,NPOS,HMENU)
***    read menu return code
      XFMT='(A49'//DOLL//')'
      WRITE(*,FMT=XFMT)
     & '   Type number of menu position and press ENTER: '
      READ(*,FMT='(A3)',ERR=95) CCODE
      IF ( CCODE.EQ.'    ') GOTO 95
      CCODE(4:4)=' '


      IF ((CCODE.EQ.'x') .OR. (CCODE.EQ.'X')) THEN
         MENU=0
         GOTO 110
      ENDIF
      IF ((CCODE(1:1).EQ.'H').OR.(CCODE(1:1).EQ.'h')) THEN
         MENU=99
         IF (HMENU.EQ.' ') THEN
C*            MENU=0
C*            GOTO 110
              GOTO 1
         ENDIF
         NHPOS=0
         DCH='  '
         IF (CCODE(2:2).EQ.' ') GOTO 105
         NHPOS=1
         DCH(1:1)=CCODE(2:2)
         IF (CCODE(3:3).EQ.' ') GOTO 105
         NHPOS=2
         DCH(2:2)=CCODE(3:3)
105      CONTINUE
         IF (NHPOS.EQ.1) THEN
              NHPOS=ICHAR(DCH(1:1))-48
              GOTO 107
         ENDIF
         IF (NHPOS.EQ.2) THEN
             NHPOS=10*(ICHAR(DCH(1:1))-48)+(ICHAR(DCH(2:2))-48)
         ENDIF
107      IF ((NHPOS.LT.1).OR.(NHPOS.GT.NPOS)) NHPOS=0

         CALL CPTH(PATH,D_SLASH,F_SLASH)
         CALL CH2CAT(F_NAME,PATH,D_SLASH//'help'//F_SLASH)
         CALL CH2CAT(BEG,F_NAME,HMENU)
         IF( (NHPOS.GT.0).AND.(NHPOS.LE.32) ) THEN
            IF (NHPOS.LE.9) THEN
                ND=ICHAR('0')
            ELSE
                ND=ICHAR('A')-10
            ENDIF
            CALL CH2CAT(F_NAME,BEG,CHAR(ND+NHPOS)//'.txt') 
            INQUIRE(FILE=F_NAME,EXIST=XFILE)
            IF (XFILE) THEN
                CALL VIEWRP(F_NAME )
            ELSE
               GOTO 108
            ENDIF
         ELSE
108        CALL CH2CAT(F_NAME,BEG,'.txt')
           CALL VIEWRP(F_NAME )
         ENDIF
C*         CALL CLRSCR
C*         GOTO 110
           GOTO 1
      ENDIF
      IF ((CCODE.EQ.'m') .OR. (CCODE.EQ.'M')) THEN
        MENU=0
        MBACK=1
        GOTO 110
      ENDIF
      ICODE=0
      K=0
1001  K=K+1
      IF (CCODE(K:K).EQ.' ') GOTO 1001 
1002  N=ICHAR(CCODE(K:K)) - ICHAR('0')
      K=K+1
      IF( (N.GE.0).AND.(N.LE.9) ) THEN
         ICODE=ICODE*10+N
         GOTO 1002
      ENDIF
    
***     check range for menu return code (NPOS - number of menu positions)
      IF ((ICODE .GE. 0) .AND. (ICODE .LE. NPOS)) THEN
          MENU=ICODE
      ELSE
          GOTO 95
      ENDIF

110   CONTINUE
      RETURN
      END

*************************************************************
*                    Menu image                             *
*************************************************************
*  Input:                                                   *
*    MAXLEN - width of menu (has to be >40 and <78),        *
*             for 1 column records (header ones and         *
*               position records in the case of NCOL=1)     *
*               the length of a record will be              *
*                  (MAXLEN-10)                              *
*             for 2 column records (position records        *
*               in the case of NCOL=1) the length of a      *
*               record will be                              *
*                  (MAXLEN-20)/2                            *
*    NCOL   - one or two column formation:                  *
*               NCOL=1  menu positions are in 1 column,     *
*               NCOL=2    in 2 column.                      *
*    LREC1  - length of header record displayed.            *
*    LREC2  - length of position record displayed.          *
*    CHBORD - string used for the border of menu:           *
*        CHBORD='vh123456' - where                          *
*         'v' - symbol for vertical line,                   *
*         'h' - symbol for horisontal line,                 *
*         '1' - symbol for top-left corner,                 *
*         '2' - symbol for top-right corner,                *
*         '3' - symbol for bottom-right corner,             *
*         '4' - symbol for bottom-left corner.              *
*         '5' - symbol for left end of middle line,         *
*         '6' - symbol for right end of middle line.        *
*    NPOS - OUTPUT parameter is number of menu positions.   *
*************************************************************
*     Width of records:                                     *
*        in header  - (MAXLEN-11)  (max 66)                 *
*        in menu position (NCOL=1) - (MAXLEN-11)  (max 66)  *
*        in menu position (NCOL=2) - (MAXLEN-20)/2 (max 28) *
*************************************************************
      SUBROUTINE MENU1(MAXLEN,NCOL,LREC1,LREC2,STRMEN,NPOS,HMENU)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z),INTEGER (I-N)
      LOGICAL ENDMEN,HEADL,RL,ODDLOG,HEADOFF
c      CHARACTER*5000 STRMEN
      CHARACTER STRMEN*(*), HMENU*(*)
      CHARACTER*80 TMPSTR,XSTR, STR1,STR2
      CHARACTER CHB(8),CHBORD*8
      CHARACTER*3 CHH1
      CHARACTER  ICH*2,ICH1*2,ILEFT*2,BLN*4
      CHARACTER*100 XFCHR,FCHR2,FCHR3,FCHR4,FCHR5,FCHR6,FCHR7
      CHARACTER*100 FCHRT,FCHRM,FCHRB
      COMMON /NBRD/ NBRD,CHBORD
      SAVE

***************************************
*** menu initialization parameters ****
***************************************
      I0=0
      NCOM=0
      HEADL=.FALSE.
      HEADOFF=.FALSE.
      RL = .TRUE.
      ODDLOG=.FALSE.
      ENDMEN=.FALSE.
      TMPSTR=' '
      XSTR=' '
      STR1=' '
      STR2=' '
      DO 5 K=1,8
        CHB(K)=CHBORD(K:K)
5     CONTINUE

***********************************
** left skip for menu centering ***
***********************************
      CALL ITOCHR(ILEFT,(80-MAXLEN)/2+1)

*******************************
*** FORMATs for menu image ****
*******************************
      CHH1=''''//CHB(1)//''''
**       top horisontal line
      FCHRT=' ' 
      FCHRT(1:1)=CHB(3)
      FCHRT(MAXLEN:MAXLEN)=CHB(4)
      DO 10 I=2,MAXLEN-1
       FCHRT(I:I)=CHB(2)
10    CONTINUE
      FCHRT(MAXLEN+1:MAXLEN+1)=''''
      FCHRT(MAXLEN+2:MAXLEN+2)=','
      FCHRT(MAXLEN+3:MAXLEN+3)='A'
      FCHRT(MAXLEN+4:MAXLEN+4)='1'
      FCHRT(MAXLEN+5:MAXLEN+5)=')'
      XFCHR=FCHRT
      FCHRT='('//ILEFT//'X,'''//XFCHR
      xfchr=' '
**       middle line
      FCHRM(1:1)=CHB(5)
      FCHRM(MAXLEN:MAXLEN)=CHB(6)
      DO 12 I=2,MAXLEN-1
       FCHRM(I:I)=CHB(2)
12    CONTINUE
      FCHRM(MAXLEN+1:MAXLEN+1)=''''
      FCHRM(MAXLEN+2:MAXLEN+2)=','
      FCHRM(MAXLEN+3:MAXLEN+3)='A'
      FCHRM(MAXLEN+4:MAXLEN+4)='1'
      FCHRM(MAXLEN+5:MAXLEN+5)=')'
      XFCHR=FCHRM
      FCHRM='('//ILEFT//'X,'''//XFCHR
**       bottom horisontal line
      FCHRB(1:1)=CHB(7)
      FCHRB(MAXLEN:MAXLEN)=CHB(8)
      DO 13 I=2,MAXLEN-1
       FCHRB(I:I)=CHB(2)
13    CONTINUE
      FCHRB(MAXLEN+1:MAXLEN+1)=''''
      FCHRB(MAXLEN+2:MAXLEN+2)=','
      FCHRB(MAXLEN+3:MAXLEN+3)='A'
      FCHRB(MAXLEN+4:MAXLEN+4)='1'
      FCHRB(MAXLEN+5:MAXLEN+5)=')'
      XFCHR=FCHRB
      FCHRB='('//ILEFT//'X,'''//XFCHR
**       header record
      CALL ITOCHR(ICH,MAXLEN-10)
      FCHR2= '('//ILEFT//'X,'//CHH1//',4X,A'//ICH//',4X,'//CHH1//')'
**       Prompt line
      NLS=(MAXLEN-40)/2
      NRS=MAXLEN-40-NLS
      CALL ITOCHR(ICH,NRS+1)
      CALL ITOCHR(ICH1,NLS+1)
      IF (HMENU.EQ.'f_') THEN
           FCHR3='('//ILEFT//'X,'//CHH1//','//ICH1//
     &           'X,''        x: Exit      h: Help        '','//
     &           ICH//'X,'//CHH1//')'
      ELSE
           FCHR3='('//ILEFT//'X,'//CHH1//','//ICH1//
     &           'X,''x: Exit     h: Help     m: MAIN menu'','//
     &           ICH//'X,'//CHH1//')'
      ENDIF
**       empty line
      CALL ITOCHR(ICH,MAXLEN-2)
      FCHR4='('//ILEFT//'X,'//CHH1//','//ICH//'X,'//CHH1//')'

      IF ((MAXLEN-20).NE.(2*LREC2)) THEN
        BLN=',3X,'
        CALL ITOCHR(ICH,LREC2+12)
      ELSE
        BLN=',2X,'
        CALL ITOCHR(ICH,LREC2+11)
      ENDIF
**       line with only one 1/2 position record (in NCOL=2 case)
      FCHR5=','//ICH//'X,'//CHH1//')'
      CALL ITOCHR(ICH,LREC2)
      XFCHR=FCHR5
      FCHR5='('//ILEFT//'X,'//CHH1//',3X,I2,'': '',A'//ICH//XFCHR

**       position record line in the case of NCOL=1
      CALL ITOCHR(ICH,LREC1)
      FCHR6='('//ILEFT//'X,'//CHH1//',1X,I2,'': '',A'//ICH//
     &        ',3X,'//CHH1//')'
**       pair position records line (NCOL=2)
      CALL ITOCHR(ICH,LREC2)
      FCHR7='('//ILEFT//'X,'//CHH1//',3X,I2,'': '',A'//ICH//',5X,'//
     &        'I2,'': '',A'//ICH//BLN//CHH1//')'

************************************
***   if menu head is present    ***
************************************
      IF (STRMEN(1:1).EQ.'%') THEN
***       top line for menu head
         WRITE(*,FMT=FCHRT) 
         HEADL=.TRUE.
         I0=1
      ENDIF

****************************************
*** loop over records in menu string ***
****************************************
      NHEAD=0
      DO 130  N=1,100
        IF (HEADL) THEN
          NHEAD=NHEAD+1
***       skip left-right column for menu positions
        ELSE
            RL=.NOT. RL
        ENDIF
        IF (NCOL.EQ.1) THEN
            IF (N.GE.20) GOTO 1000
        ELSE
            IF ((N+NHEAD).GE.40) GOTO 1000
        ENDIF
***   loop over characters in current record
        DO 100 I=1,LREC1
            IF ((STRMEN(I0+I:I0+I).NE.'!') .AND. (I.EQ.LREC1)) THEN
              DO 15 IK=1,2000-I0-I
                IF(STRMEN(I0+I+IK:I0+I+IK).EQ.'!') THEN
                  I0=I0+IK
                  GOTO 16
                ENDIF
15            CONTINUE
              WRITE(*,*)  'Too long record in the menu string'//
     &           ' or triple !!! is absent in the end of menu string.'
              STOP
16            TMPSTR(I:I)='!'
            ELSE
              TMPSTR(I:I)=STRMEN(I0+I:I0+I)
            ENDIF
*** if header is ended by '%' without previous '!'
            IF ((HEADL).AND.(STRMEN(I0+I:I0+I).EQ.'%')) THEN
              TMPSTR(I:I)='!'
              HEADOFF=.TRUE.
            ENDIF
***  if end of current record
            IF (TMPSTR(I:I) .EQ. '!') THEN
               IF (.NOT.HEADL) THEN
***  if not head line then add position line by blanks
                  DO 20 J=I,LREC1
                     TMPSTR(J:J)=' '
20                CONTINUE
               ELSE
***  if head line then center it
                  N0=(LREC1-I)/2
                  IF (N0.LT.0) N0=0
                  DO 30 J=1,N0
                   XSTR(J:J)=' '
30                CONTINUE
                  DO 40 J=N0+1,N0+I-1
                   XSTR(J:J)=TMPSTR(J-N0:J-N0)
40                CONTINUE
                  DO 50 J=N0+I,LREC1
                   XSTR(J:J)=' '
50                CONTINUE
                  NCOM=NCOM+1
               ENDIF
***  if end of menu string
               IF (STRMEN(I0+1+I:I0+1+I).EQ.'!') THEN
                 NPOS = N-NCOM
                 IF (RL) THEN
                      ENDMEN=.TRUE.
                 ELSE
***  if odd number of menu positions
                      ODDLOG=.TRUE.
                      NCOM=NCOM+1
                 ENDIF
               ENDIF
               I0=I0+I
               GOTO 120
            ENDIF
***  end of loop over character in the current menu record
100     CONTINUE
***  write menu lines
120     IF (HEADL) THEN
           WRITE(*,FMT=FCHR2) XSTR
***  if end of head records in menu string
           IF (STRMEN(I0+1:I0+1).EQ.'%') THEN
                HEADOFF=.TRUE.
                I0=I0+1
           ENDIF
           IF (HEADOFF) THEN
              HEADL=.FALSE.
              DO 125 I=1,LREC1
                 TMPSTR(I:I)=' '
125           CONTINUE
              IF (NCOL.EQ.2) LREC1=LREC2
           ENDIF
        ELSE
***         if first line with menu positions
           IF ((N-NCOM) .EQ. 1) THEN
              WRITE(*,FMT=FCHRM)
              WRITE(*,FMT=FCHR3) 
              WRITE(*,FMT=FCHR4) 
           ENDIF
***         write line with menu positions
           IF (RL) THEN
               STR2=TMPSTR
               IF (ODDLOG) THEN
                  IF (NCOL.EQ.1) THEN
                      WRITE(*,FMT=FCHR6) N-NCOM,STR1
                  ELSE
                   WRITE(*,FMT=FCHR5) N-NCOM,STR1
                  ENDIF
               ELSE
                  IF (NCOL.EQ.1) THEN
                      WRITE(*,FMT=FCHR6) N-NCOM-1,STR1
                      WRITE(*,FMT=FCHR6) N-NCOM,STR2
                  ELSE
                   WRITE(*,FMT=FCHR7) N-NCOM-1,STR1,N-NCOM,STR2
                  ENDIF
               ENDIF
           ELSE
***              continue to create empty STR2
               STR1=TMPSTR
           ENDIF
        ENDIF
***       if end of menu string then go out of loop over records
        IF (ENDMEN) GOTO 1000
130   CONTINUE

***     after menu construction
1000  CONTINUE

***     bottom menu line
      WRITE(*,FMT=FCHRB)

      RETURN
      END

*********************************************
*   SET  menu border                       **
*********************************************
      SUBROUTINE SETBRD(CHBORD)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z),INTEGER (I-N)
      CHARACTER CHBORD*8,XCHBRD*8
      COMMON /NBRD/  NBRD,XCHBRD
      SAVE

      XCHBRD=CHBORD
      NBRD=1
      RETURN

      ENTRY DEFBRD
      NBRD=0
      RETURN

      END
