
*  SUBROUTINE W_TCUT(MODE)  - write down cuts
*       ENTRY R_TCUT(MODE)  - read cuts
*   SUBROUTINE INTCUT()     - new PT cut
*   SUBROUTINE CHTCUT(NCUT) - change PT cut
*   SUBROUTINE DLTCUT(NCUT) - remove PT cut
*   FUNCTION  T_CUT() - return 1 or 0

      SUBROUTINE I_TCUT()
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      PARAMETER (MC=32)
      CHARACTER*1 CNAME(MC) 
      COMMON /TCUT/PTMIN(MC),NUMP1(MC),NUMP2(MC),NTCUT,CNAME
      SAVE /TCUT/
      NTCUT=0 
      RETURN
      END
      
      SUBROUTINE W_TCUT(MODE) 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      PARAMETER (MC=32)
      CHARACTER*1 CNAME(MC) 
      COMMON /TCUT/  PTMIN(MC),NUMP1(MC),NUMP2(MC),NTCUT,CNAME
      CHARACTER buff16*16,C1*1
      SAVE /TCUT/

      buff16 = '===== There are '
      WRITE(MODE,101) buff16 ,NTCUT,' cuts for PT,E,Angle ====='
      DO 10 I=1,NTCUT
10    WRITE(MODE,103) CNAME(I),NUMP1(I),NUMP2(I),'>',PTMIN(I)
       
      RETURN   
101   FORMAT(1x,A16,I2,A27)
103   FORMAT(1x,A1,I1,I1,1x,A1,1x, 1PE12.4)     
      
      ENTRY R_TCUT(MODE)
      READ(MODE,101)  buff16,NTCUT
      DO 20 I=1,NTCUT 
        READ(MODE,103) CNAME(I),NUMP1(I),NUMP2(I),C1,PTMIN(I)
20    CONTINUE             
              
      RETURN
      END


      
      FUNCTION HLPSTR(I)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      CHARACTER*40 HLPSTR
      CHARACTER*40 BUF
      PARAMETER (MC=32)
      CHARACTER*1 CNAME(MC) 
      COMMON /TCUT/  PTMIN(MC),NUMP1(MC),NUMP2(MC),NTCUT,CNAME
      CHARACTER  CNUM*1,CNAME1*3, CUNIT*3
      SAVE /TCUT/

      IF (I.LT.10) THEN 
         CNUM=CHAR( I + ICHAR('0') )
      ELSE 
         CNUM=CHAR( I + ICHAR('A') -10)
      ENDIF

      IF (CNAME(I).EQ.'A') THEN
         CNAME1='A'// CHAR(NUMP1(I)+ICHAR('0'))
     &             // CHAR(NUMP2(I)+ICHAR('0'))
         CUNIT ='Deg'
      ELSEIF (CNAME(I).EQ.'E') THEN
         CNAME1='E'// CHAR(NUMP2(I)+ICHAR('0'))
         CUNIT='Gev'
      ELSEIF (CNAME(I).EQ.'T') THEN
         CNAME1='T'// CHAR(NUMP2(I)+ICHAR('0'))
         CUNIT='Gev'
      ELSEIF (CNAME(I).EQ.'J') THEN
        CNAME1='J'// CHAR(NUMP1(I)+ICHAR('0'))
     &             // CHAR(NUMP2(I)+ICHAR('0'))
         CUNIT =' '
      ENDIF    
      WRITE(BUF,100) CNUM,CNAME1,PTMIN(I),CUNIT
100   FORMAT('| ',A1,' | ',A3,' > ',1PE12.4,1x,A3,' |')
       HLPSTR=BUF
      RETURN
      END

      SUBROUTINE S_TCUT 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      PARAMETER (MC=32)
      CHARACTER*1 CNAME(MC) 
      COMMON /TCUT/  PTMIN(MC),NUMP1(MC),NUMP2(MC),NTCUT,CNAME
      CHARACTER*40 HLPSTR ,BUFF1,BUFF2
      SAVE /TCUT/

      IF (NTCUT.EQ.0) THEN 
          WRITE(*,*) ' Cuts for PT, E,Angles and jet J are absent '
          RETURN
      ENDIF
 
      WRITE(*,101)
      DO 10 I=1,(NTCUT+1)/2
      
      I2= (NTCUT+1)/2 +I

      BUFF1=HLPSTR(I)
      IF (I2.LE.NTCUT) THEN
        BUFF2=HLPSTR(I2)        
        WRITE(*,102) BUFF1,BUFF2
      ELSE
        WRITE(*,102) BUFF1,'|                            |'
      ENDIF   

10    CONTINUE
      WRITE(*,103)            
101   FORMAT(1x,20x,'Cuts for PT,E,Angles',/,1x,60('-') )
102   FORMAT(1x,A30,A30)    
103   FORMAT(1x,60('-'))       
      RETURN
      END



            
      SUBROUTINE DLTCUT(NDEL)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      PARAMETER (MC=32)
      CHARACTER*1 CNAME(MC) 
      COMMON /TCUT/  PTMIN(MC),NUMP1(MC),NUMP2(MC),NTCUT,CNAME
      
      IF( (NDEL.LT.0).OR.(NDEL.GT.NTCUT) ) GOTO 99
      NTCUT=NTCUT-1
      DO 10 I=NDEL,NTCUT
         CNAME(I)=CNAME(I+1)
         NUMP1(I)=NUMP1(I+1)
         NUMP2(I)=NUMP2(I+1)
         PTMIN(I)=PTMIN(I+1)
10    CONTINUE      
99    RETURN
      END
      
      SUBROUTINE CHTCUT(NCUT)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      PARAMETER (MC=32)
      CHARACTER*1 CNAME(MC) 
      COMMON /TCUT/  PTMIN(MC),NUMP1(MC),NUMP2(MC),NTCUT,CNAME
      IF((NCUT.LE.0).OR.(NCUT.GT.NTCUT)) THEN
         WRITE(*,*) 'Incorrect input'
         READ(*,*)
         RETURN
      ENDIF
77    WRITE(*,FMT='(1x, ''Enter new  bound '',$)')
      READ(*, FMT='(G15.0)', ERR=77 ) PTMIN(NCUT)

99    RETURN
      END
      
      SUBROUTINE INTCUT()
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      PARAMETER (MC=32)
      CHARACTER*1 CNAME(MC)
      COMMON /TCUT/  PTMIN(MC),NUMP1(MC),NUMP2(MC),NTCUT,CNAME     
      CHARACTER*3 C3     

      IF(NTCUT.EQ.MC) THEN
         WRITE(*,*) ' Too many cuts !'
         READ(*,*)
         RETURN
      ENDIF              
      WRITE(*,FMT='(7x,''Enter a cut specification :  '','//
     & '/,''Ei for i particle energy,              '','//
     & '''Ti for i particle transverse momentum, '','//
     & '/,''Aij  for i-j partiles angle,           '','//
     & '''Jij  for i-j jet separator'' )')
      READ(*,FMT='(A3)',ERR=99) C3

      IF ((C3(1:1).EQ.'A').OR.( C3(1:1).EQ.'a')) THEN
         C3(1:1)='A'
         N1= ICHAR(C3(2:2))-ICHAR('0')
         N2= ICHAR(C3(3:3))-ICHAR('0')

         IF (N1.GT.N2) THEN
            NN=N1
            N1=N2
            N2=NN
         ENDIF

         IF( (N1.LE.0).OR.(N2.LE.NIN()).OR.
     & (N2.GT.NIN()+NOUT()).OR.(N1.EQ.N2 )  )  GOTO 99

      ELSEIF ((C3(1:1).EQ.'T').OR.( C3(1:1).EQ.'t')) THEN
         IF(NIN().EQ.1) GOTO 99
         C3(1:1)='T'
         N1=0
         N2= ICHAR(C3(2:2))-ICHAR('0')
         IF( (N2.LE.NIN()).OR.(N2.GT.NIN()+NOUT()) ) GOTO 99
      ELSEIF ((C3(1:1).EQ.'E').OR.( C3(1:1).EQ.'e')) THEN
         C3(1:1)='E'
         N1= 0
         N2= ICHAR(C3(2:2))-ICHAR('0')
         IF( (N2.LE.NIN()).OR.(N2.GT.NIN()+NOUT()) ) GOTO 99
      ELSEIF ((C3(1:1).EQ.'J').OR.( C3(1:1).EQ.'j')) THEN
         C3(1:1)='J'
         N1= ICHAR(C3(2:2))-ICHAR('0')
         N2= ICHAR(C3(3:3))-ICHAR('0')

         IF (N1.GT.N2) THEN
            NN=N1
            N1=N2
            N2=NN
         ENDIF
         IF( (N1.LE.0).OR. (N2.GT.NIN()+NOUT()).OR.(N1.EQ.N2 ))
     &    GOTO 99        
      ELSE
         GOTO 99
      ENDIF

      DO 20 I=1,NTCUT
      IF((CNAME(I).EQ. C3(1:1)).AND.(NUMP1(I).EQ.N1).
     & AND. NUMP2(I).EQ.N2) THEN
         WRITE(*,*) ' This cut already exist ' 
         READ(*,*)         
         RETURN
      ENDIF       
20    CONTINUE 

      NTCUT=NTCUT+1
      NUMP1(NTCUT)=N1
      NUMP2(NTCUT)=N2
      CNAME(NTCUT)=C3(1:1)


77    WRITE(*,FMT='( ''Enter bound '', $)') 
      READ(*,FMT='(G15.0)', ERR=77 ) PTMIN(NTCUT)
      IF(PTMIN(NTCUT).LT.0) PTMIN(NTCUT)=0

      RETURN

99    WRITE(*,FMT='(1x,  '' Wrong cut specification !  '', $)') 
      READ(*,* ) 
     
      END
      
      
      FUNCTION  T_CUT()
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      PARAMETER (MC=32)
      CHARACTER*1 CNAME(MC)
      COMMON /TCUT/  PTMIN(MC),NUMP1(MC),NUMP2(MC),NTCUT,CNAME
      COMMON /PVECT/P(0:3,100)
      FPI=ACOS(-1.D0)/180
      DO 10 I=1,NTCUT
         NP1=NUMP1(I)
         NP2=NUMP2(I)

         IF (CNAME(I).EQ.'E') THEN
           EN=P(0,NP2)
           IF (PTMIN(I).GT.EN) THEN
             T_CUT=0
             RETURN
           ENDIF

         ELSEIF (CNAME(I).EQ.'T') THEN         
           PT=SQRT(P(1,NP2)**2 + P(2,NP2)**2)
           IF(PTMIN(I).GT.PT)  THEN
             T_CUT=0
             RETURN
           ENDIF

         ELSEIF (CNAME(I).EQ.'A') THEN 
           CS=(P(1,NP1)*P(1,NP2)+P(2,NP1)*P(2,NP2)+P(3,NP1)*P(3,NP2))/
     *     SQRT( (P(1,NP1)**2 + P(2,NP1)**2 + P(3,NP1)**2)*
     *           (P(1,NP2)**2 + P(2,NP2)**2 + P(3,NP2)**2)
     *         )
           IF(COS(PTMIN(I)*FPI).LT.CS)  THEN
             T_CUT=0
             RETURN         
           ENDIF
         ELSE
* (CNAME(I).EQ.'J' )
           P1=SQRT(P(1,NP1)**2 + P(2,NP1)**2 + P(3,NP1)**2)
           P2=SQRT(P(1,NP2)**2 + P(2,NP2)**2 + P(3,NP2)**2)
           PT1=SQRT(P(1,NP1)**2 + P(2,NP1)**2)
           PT2=SQRT(P(1,NP2)**2 + P(2,NP2)**2)   
           CS=(P(1,NP1)*P(1,NP2)+P(2,NP1)*P(2,NP2))/(PT1*PT2)
           DL= (P1+P(3,NP1))*(P2-P(3,NP2))/(P1-P(3,NP1))/(P2+P(3,NP2))
           IF ( (0.5*LOG(DL))**2 + ACOS(CS)**2 .LT.PTMIN(I)**2) THEN
             T_CUT=0
             RETURN
           ENDIF
         ENDIF
10    CONTINUE
      T_CUT=1      
      RETURN
      END
      

      
