

      SUBROUTINE QDEC0(NVIN,AMM1,AMM2,FACTOR)

      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DOUBLE PRECISION HDIF,HSUM,DPARCOS,DPARFI,FACTOR
      DIMENSION HSUM(2)
      COMMON /QPVECT/ P(0:3,100)
      
      DOUBLE PRECISION AM1,AM2,VV, VP,VA,PP,PA,AA,V0,E0V,OO,VO,PO,
     #P0,P1,E1P,E1V,A0,A1,A2,E2A,E2P,E2V,POUT,E1OUT,E2OUT      
      INTEGER N0,N1,N2 

      LOGICAL FIRST
     
      SAVE

      DATA FIRST/.TRUE./
      
      IF (FIRST) THEN
        C_PI  =  4*(4*DASIN(1.D0))**2
        FIRST =  .FALSE.
      ENDIF

      AM1=AMM1
      AM2=AMM2
      N0=NVIN

      VV=QVDOT4(NVIN,  NVIN)

      V0 = SQRT(VV)
      E0V= 1/V0

      POUT=SQRT( (V0**2-(AM1+AM2)**2)*(V0**2-(AM1-AM2)**2) )/(2*V0)
      E1OUT=SQRT(AM1**2+POUT**2)
      E2OUT=SQRT(AM2**2+POUT**2)         
      FACTOR=POUT/(C_PI*(E1OUT+E2OUT))

      RETURN

      ENTRY QDEC1(NVPOLE,HSUM,HDIF)

      N1=NVPOLE

      VP=QVDOT4(N0,  NVPOLE)
      PP=QVDOT4(NVPOLE,NVPOLE)
      
      P0 = E0V*VP

      P1 = SQRT(P0**2-PP)

      HDIF=2*P1*POUT            
      HSUM(1)=PP+AM1**2+2*P0*E1OUT
      HSUM(2)=PP+AM2**2+2*P0*E2OUT      

      RETURN

      ENTRY QDEC2(NVOUT1,DPARCOS)

      OO=   QVDOT4(NVOUT1,NVOUT1)
      VO=   QVDOT4(N0,    NVOUT1)
      PO=   QVDOT4(N1,    NVOUT1)

      DPARCOS=(PO*VV-VP*VO)/SQRT((PP*VV-VP**2)*(OO*VV-VO**2))

      RETURN

      ENTRY  QDEC3(NVATH,DPARCOS,DPARFI,NVOUT1,NVOUT2)

**   v = NVIN; p = NVPOLE; a = NVATH;         
**  scalar products:

      VA=QVDOT4(N0,    NVATH)
      PA=QVDOT4(N1,    NVATH)
      AA=QVDOT4(NVATH, NVATH)
              
** bases: e0..e3 : v=V0*e0  ;  p=P0*e0 + P1*e1; a =A0*e0 + A1*e1 + A2*e2 
**                 e0=E0V*v ; e1=E1V*v + E1P*p; e2=E2V*v + E2P*p + E2A*a 
**   out1= E1OUT*e0 + POUT*( PARCOS*e1 + PARSIN*(e2*COSFI+ e3*SINFI)) 
**       = E1OUT*E0V*v + POUT*( PARCOS*(E1V*v + E1P*p ) 
**       + PARSIN*(COSFI*(E2V*v + E2P*p + E2A*a )  + SINFI*e3 ))     

**       =(E1OUT*E0V + POUT*( PARCOS*E1V +PARSIN*COSFI*E2V))*v
**       +POUT*(PARCOS*E1P+PARSIN*COSFI*E2P)*p
**       +POUT*PARSIN*COSFI*E2A*a
**       +POUT*PARSIN*SINFI*e3

      E1P= 1/P1
      E1V=-E1P*P0*E0V
      
      A0 = E0V*VA
      A1 =-(E1V*VA+E1P*PA)
      A2 = SQRT(A0**2-A1**2-AA)
      E2A= 1/A2
      E2P=-E2A*A1*E1P
      E2V=-E2A*(A0*E0V+A1*E1V)
               
***** out state *****         
         
      PARCOS=DPARCOS
      PARSIN=SQRT((1-PARCOS)*(1+PARCOS))
      PARFI=DPARFI
      SINFI=SIN(PARFI)
      COSFI=COS(PARFI)

      N3=NVOUT1
      N2=NVATH

      CALL QEPS4(N0,N1,N2,N3)
      
      CINV =E1OUT*E0V 
      CINP= POUT*( PARCOS*E1V +PARSIN*COSFI*E2V)

      CPOL=POUT*(PARCOS*E1P+PARSIN*COSFI*E2P)
      CATH=POUT*PARSIN*COSFI*E2A
      CEPS=POUT*PARSIN*SINFI/SQRT(-QVDOT4(N3,N3))
      
      DO 10 I=0,3
10    P(I,N3)=CINP*P(I,N0)+CPOL*P(I,N1)+CATH*P(I,N2)+CEPS*P(I,N3)
      
      DO 20 I=0,3
      P(I,NVOUT2)=E2OUT*E0V*P(I,N0)-P(I,N3)
20    P(I,NVOUT1)=E1OUT*E0V*P(I,N0)+P(I,N3)      
      RETURN      
      END      
