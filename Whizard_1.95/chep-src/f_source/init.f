*#########################################
*#    Initialization procedure           #
*#########################################

*********************************************************
*   Task initialization
*********************************************************
      SUBROUTINE TSKINI
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      LOGICAL       XFILE

      COMMON /DEVICE/  IPRT,ISES,IBAT,ISTR(2),IEVENT,ITMP
      CHARACTER*12  PRTNAM,SESNAM,BATNAM
      COMMON /DEVNAM/  PRTNAM,SESNAM,BATNAM

      CHARACTER     VERS*20,CHBORD*8,DOLL*2
      COMMON /VERS/    VERS
      COMMON /NBRD/    NBRD,CHBORD
      COMMON /DOLL/    DOLL
      SAVE

*** number of the KinComp version
      VERS = ' v. 3.2 ( 19.01.96)'
*** out device and name for protocol file
      IPRT = 10
      PRTNAM = 'prt_'
*** out device and name for resulting file
      ISES = 11
      SESNAM = 'SESSION.DAT'
*** out device and name for batch file
      IBAT = 12
      BATNAM = 'BATCH.DAT'
      
      ISTR(1) = 13
      ISTR(2) = 14
      IEVENT= 15
      ITMP  = 16 
*** create process string
      CALL WRTPRC

*** read parameters from file 'SESSION.DAT':
      CALL INIPAR
      INQUIRE(FILE=SESNAM,EXIST=XFILE)
      IF (XFILE) THEN
         OPEN(ISES,FILE=SESNAM,STATUS='UNKNOWN')
         CALL R_SESS(ISES)
      ELSE
         CALL W_SESS(ISES)
      ENDIF

*** dollar symbol in the end of READ lines
      DOLL=',$'

*** initialization of menu parameters
      NBRD=1
      CHBORD='********'
      
      RETURN
      END
