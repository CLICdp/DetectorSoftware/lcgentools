      BLOCK DATA NSTEP_
       COMMON/STEP/NSTEP
       DATA NSTEP/0/
      END

      SUBROUTINE ISNAN(MESS,R) 
      CHARACTER *(*) MESS 
      DOUBLE PRECISION R
      CHARACTER*20 STR
      COMMON/STEP/NSTEP
      SAVE
      WRITE(STR,FMT='(E20.10)') R
      DO 10 K=1,20
      IF(STR(K:K).EQ.'n') THEN
        WRITE(*,*) 'NAN ON STEP=',NSTEP,'MESS=',MESS
        STOP
      ENDIF
10    CONTINUE
      RETURN
      END

      FUNCTION DIVY(Y)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)            
      IF (ABS(Y).GT.0.01) THEN
         DIVY = (1-EXP(-Y))/Y
      ELSE  
         DIVY = 1-Y*(1+Y*(1-Y*(1+Y/5)/4)/3)/2
      ENDIF
      END

      SUBROUTINE  MKMOM(X,TFACT)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)      
      DOUBLE PRECISION QAMASS,QPM,QSS,QP,QMASS
      DOUBLE PRECISION DMASS
      EQUIVALENCE (DMASS,QMASS)
      
      DIMENSION QAMASS(10,2),QPM(10)
      
      DIMENSION  X(10),AMASS(10,2),
     &           NVIN(10),NVOUT(10,2),
     &           LNKBAB(10),LNKLEG(10)

      DIMENSION  SUMMAS(10,2),PM(10)
      
      DIMENSION  NMSREG(10,2),NMSCUT(10,2),NCSREG(10,2,100),
     & NCSCUT(10,2,100), ITYPEP(10,100)

      DIMENSION HSUM(2),SINGAR(3,100)
      LOGICAL SPOLE

      LOGICAL QUADRA,QUADR_    
      COMMON /KINMTC/ LVIN(10,10), LVOUT(10,10,2)
      DIMENSION  LVPOLE(10,10,120)
     

      DIMENSION LVBUF(10)
            
      LOGICAL STRFON,EQVECT
      COMMON /STRFST/ XPARTN(2),BE(2),STRFON(2)
      
      COMMON /QPVECT/ QP(0:3,100)
      COMMON /PVECT/P(0:3,100)
      COMMON /SQS/ SQRTS
      CHARACTER*30 PROCES
      COMMON /PROCES/NSUB,PROCES
      INTEGER NVPOLE,NVATH,NVPOS0,NSS,NOUT1
      DOUBLE PRECISION TFACT0,E1CM,E2CM,SSMIN,SSMAX
      REAL RAN2      
      COMMON/INVREG/LVINVR(10,200),RGMASS(200),RGWDTH(200),NEXTRG(200),
     & NDEG(200)
      COMMON/INVCUT/ LVINVC(10,60),MINON(60),MAXON(60),
     &CVMIN(60),CVMAX(60)

      DIMENSION NSPH(10)
      LOGICAL ORIG(200)
      DIMENSION  SPH_WE(10,200)
      COMMON/STEP/NSTEP
      CHARACTER * 100 MESS
      DATA   NDEBUG/0/
      SAVE 
    
*     moment in the cms
      CMFUN(E,PM1,PM2)=
     & SQRT((E**2-(PM1+PM2)**2)*(E**2-(PM1-PM2)**2) )/(2*E)

**   TOTAL FACTOR INITIALISATION
      TFACT=TFACT0

**    FILL INITIAL STATE  
      DO 1 K=1,NIN()
1     CALL VNULL4(K)
      
      NX=1
      nstep=nstep +1
***   MOMENTS      
      IF (NIN().EQ.2) THEN
         IF (STRFON(1).OR.STRFON(2)) THEN            
            IF (SSMIN.GE.SSMAX) GOTO 1000             
            NSING=0
            CALL GETREG(NSING,SINGAR,0.D0,1.D0,NSS)

            BES=0
            DO 4 K=1,2
4           IF(STRFON(K)) BES=BES+BE(K)
            IF (BES.GE.1) THEN
               AL=1
            ELSEIF (NSING.EQ.0) THEN
               AL=0
            ELSE               
               AL= 0.5D0
            ENDIF
 
            SS=SQRTS**2     

            XX=X(NEXTNN(NX))
            IF (XX.LT.AL) THEN
               XX=XX/AL             
               CALL REGFUN(2,NSING,SINGAR,SSMIN,SSMAX,XX,STILDA,FCT1)
            ELSE
               XX=(1-XX)/(1-AL)
               STILDA
     &         =SS-(XX*(SS-SSMIN)**BES+(1-XX)*(SS-SSMAX)**BES)**(1/BES)
               CALL  REGFCT(2,NSING,SINGAR,SSMIN,SSMAX, STILDA,FCT1)
            ENDIF
            
            FCT1=FCT1/SS
            IF (BES.LT.1) THEN
               FF=(1-SSMIN/SS)**BES-(1-SSMAX/SS)**BES
               FCT2= (FF/BES)*(1-STILDA/SS)**(1-BES)
               TFACT=FF*TFACT/(AL*FCT2/FCT1+(1-AL))
            ELSE
               TFACT=TFACT*FCT1
            ENDIF
           
            YY=LOG(SS/STILDA)
            IF (STRFON(1).AND.STRFON(2)) THEN
               XX=X(NEXTNN(NX))
               IF( (BE(1).LT.1).AND.(BE(2).LT.1)) THEN
                  AL=BE(2)/(BE(1)+BE(2))
                  IF (XX.LT.AL) THEN
                     XX=XX/AL
                     PSY1=(XX)**(1/BE(1))
                     PSY2=1-PSY1
                  ELSE
                     XX=(1-XX)/(1-AL)
                     PSY2=XX**(1/BE(2))
                     PSY1=1-PSY2
                  ENDIF
                  Y1=YY*PSY1
                  Y2=YY*PSY2
*                               write(*,*) 0,TFACT,PSY1,PSY2
                  TFACT=TFACT/( PSY1**(1-BE(1)) +PSY2**(1-BE(2)) )
*                               write(*,*) 1,TFACT
                  TFACT=TFACT*DIVY(Y1)**(BE(1)-1)*DIVY(Y2)**(BE(2)-1)
*                               write(*,*) 2,TFACT
                  IF (BES.LT.1) THEN
                     TFACT=TFACT*DIVY(YY)**(1-BES)
*                               write(*,*) 3,TFACT

                  ELSE
                     TFACT=TFACT*(BES)*YY**(BES-1)
                  ENDIF
               ELSEIF (BE(1).LT.1) THEN
                  PSY1=(XX)**(1/BE(1))
                  Y1=YY*PSY1
                  Y2=YY*(1-PSY1)
                  TFACT=TFACT*YY**BE(1)*DIVY(Y1)**(BE(1)-1)
               ELSEIF (BE(2).LT.1) THEN
                  PSY2=(XX)**(1/BE(2))
                  Y2=YY*PSY2
                  Y1=YY*(1-PSY2)
                  TFACT=TFACT*YY**BE(2)*DIVY(Y2)**(BE(2)-1)
               ELSE
                  Y1=YY*XX
                  Y2=YY-Y1
                  TFACT=TFACT*YY
               ENDIF
               XPARTN(1)=exp(-Y1)
               XPARTN(2)=exp(-Y2)
            ELSE IF (STRFON(1)) THEN
               XPARTN(1)=STILDA/SS
               XPARTN(2)=1
            ELSE
               XPARTN(1)=1
               XPARTN(2)=STILDA/SS                
            ENDIF
         ELSE
            XPARTN(1)=1
            XPARTN(2)=1
            STILDA=SQRTS**2  
         ENDIF
         P(3,1)= PCM*XPARTN(1)
         P(3,2)=-PCM*XPARTN(2)
         
*******         write(*,*) XPARTN, STILDA, TFACT
******         READ(*,*)
      ENDIF   


**  FILLING ZERO COMPONENTS FOR in-PARTICLES            
      IF (.NOT.QUADRA) THEN              
        DO 20 K=1,NIN()
20         P(0,K)=SQRT(P(3,K)**2+PM(K)**2)     
      ELSE
         DO 21 K=1,NIN()
            DO 22 I=1,3
22             QP(I,K)=P(I,K)
21          QP(0,K)=SQRT(QP(3,K)**2+QPM(K)**2)              
      ENDIF
      IF (NIN().EQ.1) THEN
         RSTILD=PM(1)
         TFACT=TFACT/(2*RSTILD)
      ELSE
         RSTILD=SQRT(STILDA)
         TFACT=TFACT/(2*SQRT((STILDA-(PM(1)+PM(2))**2)*
     &                       (STILDA-(PM(1)-PM(2))**2))) 
      ENDIF
**    X & Y AXISES 
      NVPOSX=NIN()+NOUT()+1
      
      IF (.NOT.QUADRA) THEN              
        CALL VNULL4(NVPOSX)
        P(1,NVPOSX)=1
        NVPOSY=NVPOSX+1 
        CALL VNULL4(NVPOSY)
        P(2,NVPOSY)=1 
      ELSE
        CALL QVNULL(NVPOSX)
        QP(1,NVPOSX)=1
        NVPOSY=NVPOSX+1 
        CALL QVNULL(NVPOSY)
        QP(2,NVPOSY)=1 
      ENDIF
      
      NVPOS=NVPOS0
**    MASS INTEGRATION
      DO 80 I=1,NOUT1
      IF (I.EQ.1) THEN
         SVAL=RSTILD
      ELSE
         SVAL=AMASS(LNKBAB(I),LNKLEG(I))
      ENDIF 
C*      if(nstep.ge.2744) write(*,*)'Mass int ',I,sval,X

      DO 801 K=1,2
C*        if(nstep.ge.2744) write(*,*) 'k=',k
        IF (LVOUT(2,I,K).NE.0) THEN
           IF (K.EQ.1) THEN
              SMAX=( SVAL-SUMMAS(I,2) )**2
           ELSE
              SMAX=( SVAL-AMASS(I,1) )**2 
           ENDIF
           SMIN=SUMMAS(I,K)**2
           
C*           if(nstep.ge.2744) write(*,*) 'smin=',smin,' smax=',smax

           IF (NMSCUT(I,K).NE.0) 
     &              CALL RANCOR(SMIN,SMAX,0.D0,1.D0,NMSCUT(I,K))
C*              if(nstep.ge.2744) write(*,*) 'rancor : smin=',smin,
C*     &           'smax=',smax
           IF (SMIN.GE.SMAX)   GOTO 1000
            
           XX=X(NEXTNN(NX))
C*             if(nstep.ge.2744) write(*,*)'xx=',xx
           IF (NMSREG(I,K).EQ.0) THEN
              SQMASS=(XX*SMAX+(1-XX)*SMIN) 
              FCT=(SMAX-SMIN)           
           ELSE
              NSING=0
              CALL GETREG(NSING,SINGAR,0.D0,1.D0,NMSREG(I,K))
              CALL REGFUN(2,NSING,SINGAR,SMIN,SMAX,XX,SQMASS,FCT)
C*             if(nstep.ge.2744) write(*,*)'regfum:', SQMASS
           ENDIF
           AMASS(I,K)=SQRT(SQMASS)
C*           if(nstep.ge.2744) write(*,*)'amass(i,k)=', AMASS(I,K)
           IF (QUADRA) QAMASS(I,K)=AMASS(I,K)
           TFACT=TFACT*FCT
        ENDIF
801   CONTINUE
      IF( ABS((AMASS(I,1)+AMASS(I,2))/SVAL - 1).LT.1.E-10) GOTO 1000   
80    CONTINUE

      CALL ISNAN(' 80 CONTINUE',TFACT)
**    MAIN CYCLE

      IF (QUADRA) THEN 
           CALL LVTOQV(LVIN(1,1),NVIN(1))      
      ELSE
           CALL  LVTONV(LVIN(1,1),NVIN(1))      
      ENDIF     

      DO 100 I=1,NOUT1
        IF ((I.EQ.1).AND.(NIN().EQ.1)) THEN
          XCOS=RAN2()
        ELSE     
          XCOS=X(NEXTNN(NX))
        ENDIF

        AL=0
        NS_=NSPH(I)
        L=1                   
        IF ( (I.EQ.1).OR.((I.EQ.2).AND.(NIN().EQ.1)) ) THEN
          XFI=RAN2()                   
81        AL=AL+ SPH_WE(I,L)
          IF (XCOS.LT.AL) NS_= L
          L=L+1
          IF(L.LE.NS_) GOTO 81
          XCOS= (AL-XCOS)/SPH_WE(I,NS_) 
        ELSE         
          XFI=X(NEXTNN(NX))        
82        AL=AL+ SPH_WE(I,L)
          IF (XFI.LT.AL) NS_= L
          L=L+1
          IF(L.LE.NS_) GOTO 82
          XFI= (AL-XFI)/SPH_WE(I,NS_)
        ENDIF
*-----------------------------------------------------------                  
        NVATH=NVPOSY
        IF (QUADRA) THEN
          CALL LVTOQV(LVPOLE(1,I,NS_),NVPOLE)         
          CALL QDEC0(NVIN(I),QAMASS(I,1),QAMASS(I,2),FCT0)
          CALL QDEC1(NVPOLE,HSUM,HDIF)   
        ELSE
          CALL LVTONV(LVPOLE(1,I,NS_),NVPOLE)
C*          if(nstep.GE.2744)then
C*          write(*,*) 'decay #',I
C*          write(*,*) 'AMASS=',AMASS(I,1),AMASS(I,2)
C*          write(*,FMT='(A7,4F8.4)')'InVect=',(P(ll,NVIN(I)),LL=0,3)
C*          endif
          CALL DECAY0(NVIN(I),AMASS(I,1),AMASS(I,2),FCT0)
C*          if(nstep.GE.2744)then
C*          write(*,FMT='(A5,5I1,A1,4F8.4)')'Pvec=',
C*     &   ( ABS(LVPOLE(LL,I,NS_)),LL=1,5),'=',(P(ll,NVPOLE),LL=0,3)
C*
C*          WRITE(MESS,FMT='(A7,i5, i2,i2,2F10.3)') 'DECAY0 ', 
C*     &    nstep,I,NS_,AMASS(I,1),AMASS(I,2)
          CALL ISNAN(MESS,FCT0)
C*          endif
          CALL DECAY1(NVPOLE,HSUM,HDIF)            
        ENDIF

C*        if(nstep.GE.2744) 
C*     &       write(*,*) 'FCT0 = ',FCT0, 'hsum =',hsum, 'hdif=',hdif

        COSMIN=-1
        COSMAX= 1
        NSING=0 
         
        DO 90 K=1,2

C*        if(nstep.GE.2744) then
C*        write(*,*)'HSUM=',HSUM,' HDIFF=',HDIF, 'NCSREG=',NCSREG(I,K,NS_)
C*        write(*,*) ((SINGAR(mm,LL),mm=1,3)  ,LL=1,NSING)
C*        endif

        CALL GETREG(NSING,SINGAR,HSUM(K),(2*K-3)/HDIF,NCSREG(I,K,NS_)) 
C*        if(nstep.GE.2744) then
C*        write(*,*) 'NSING=',NSING 
C*        write(*,*) ((SINGAR(mm,LL),mm=1,3)  ,LL=1,NSING)
C*        endif


        CALL RANCOR(COSMIN,COSMAX,HSUM(K),(2*K-3)/HDIF,NCSCUT(I,K,NS_))


        IF (COSMIN.GE.COSMAX) GOTO 1000            
90      CONTINUE

        CALL REGFUN(ITYPEP(I,NS_),NSING,SINGAR,COSMIN,COSMAX,XCOS,
     &      PARCOS,FCT)
        FCT_1=SPH_WE(I,NS_)/FCT  
 
        PARFI=(2*XFI-1)*PI

        IF (QUADRA) THEN 
          CALL QDEC3(NVATH,PARCOS,PARFI,NVOUT(I,1),NVOUT(I,2))
        ELSE   
          CALL DECAY3(NVATH,PARCOS,PARFI,NVOUT(I,1),NVOUT(I,2))
        ENDIF
C*        if(nstep.GE.2744) then
C*          write(*,*) nstep
C*          write(*,*) 'parcos=', parcos, 'parfi=',parfi
C*          write(*,*)'out=', I, P(0,NVOUT(I,1)), P(0,NVOUT(I,2))
C*        endif
**        CALL DECAY2(NVOUT(I,2),COS_)
**        WRITE(*,*) PARCOS,COS_
*-------------------------------------------------
        DO 95 NS=1,NSPH(I)

          IF (NS.EQ.NS_) GOTO 95
          IF (QUADRA) THEN
            CALL LVTOQV(LVPOLE(1,I,NS),NVPOLE)  
            CALL QDEC1(NVPOLE,HSUM,HDIF)
            CALL QDEC2(NVOUT(I,2),PARCOS) 
          ELSE
            CALL LVTONV(LVPOLE(1,I,NS),NVPOLE)
            CALL DECAY1(NVPOLE,HSUM,HDIF)
            CALL DECAY2(NVOUT(I,2),PARCOS)
          ENDIF

          COSMIN=-1
          COSMAX= 1
          NSING=0

          DO 91 K=1,2
          CALL GETREG(NSING,SINGAR,HSUM(K),(2*K-3)/HDIF,NCSREG(I,K,NS))
          CALL RANCOR(COSMIN,COSMAX,HSUM(K),(2*K-3)/HDIF,NCSCUT(I,K,NS))
          IF ((COSMIN.GE.PARCOS).OR.(PARCOS.GE.COSMAX)) GOTO 1000
91        CONTINUE

          CALL REGFCT(ITYPEP(I,NS),NSING,SINGAR,COSMIN,COSMAX,PARCOS
     &     ,FCT)
          FCT_1=FCT_1+SPH_WE(I,NS)/FCT   
*-------------------------------------------------             
95      CONTINUE

        TFACT= TFACT*FCT0/FCT_1
100   CONTINUE

C*      write(*,*) nstep,tfact
      CALL ISNAN(' NAN TFACT from MKMOM',TFACT)
      RETURN
       
1000  TFACT=0
      RETURN
    
************************* INITIALISATION ****************************
      
      ENTRY IMKMOM (QUADR_,NDIM,IERR)

      IERR=0
      NDIM=0
      IF (STRFON(1)) NDIM=NDIM+1
      IF (STRFON(2)) NDIM=NDIM+1
      NDIM = NDIM+NOUT()*3-5
      IF ( NIN().EQ.1)  NDIM=NDIM-2
      IF ( NDIM.LE.0 )  NDIM=1

      QUADRA=QUADR_


      NOUT1=NOUT()-1
      PI=2*DACOS(0.0D0)
      TFACT0=2*PI
      IF (NIN().EQ.2) TFACT0=TFACT0*0.38937966D9               
                     
      DO 200 I=1,NIN()+NOUT()
         CALL PMAS(NSUB,I,DMASS)
         IF (QUADRA) THEN
            QPM(I)=QMASS
* EQUIVALENCE  (DOUBLE PRECISION) DMASS, (DOUBLE PRECISION) QMASS
            PM(I) =QMASS
         ELSE
            PM(I)=DMASS
         ENDIF      
200   CONTINUE
       
      NVPOS=NIN()+NOUT()+3

**  NVOUT( , ) FILLING 
      DO 50 I=1,NOUT1
      DO 50 K=1,2
         IF (LVOUT(2,I,K).EQ.0) THEN
            NVOUT(I,K)=LVOUT(1,I,K)
         ELSE  
            NVOUT(I,K)=NEXTNN(NVPOS)
         ENDIF         
50    CONTINUE

**    NVIN( ) & LNKBUB( ) & LNKLEG( )   FILLING

      NVIN(1)=NEXTNN(NVPOS)
      DO 65 I=2,NOUT1
      DO 60 J=1,I-1
      DO 60 K=1,2
        IF (EQVECT(LVIN(1,I),LVOUT(1,J,K))) THEN
           NVIN(I)=NVOUT(J,K)
           LNKBAB(I)=J
           LNKLEG(I)=K
           GOTO 65
        ENDIF
60    CONTINUE
      WRITE(*,*) 'Error in kinematics, press ENTER '
      READ(*,*)
      STOP                  
65    CONTINUE 

      NVPOLE=NEXTNN(NVPOS)



**   FILL SUMMAS - MINIMUM SQUARED MASS FOR BRANCH
      IF (QUADRA) THEN
        DO 70 K=1,2
        DO 70 I=1,NOUT1
           QSS=0
           J=1
71         QSS=QSS+QPM(LVOUT(J,I,K))
           J=J+1 
           IF (LVOUT(J,I,K).NE.0) GOTO 71
           SUMMAS(I,K)=QSS            
           QAMASS(I,K)=QSS
           AMASS(I,K)=QSS
70      CONTINUE
      ELSE    
        DO 72 K=1,2
        DO 72 I=1,NOUT1
           SS=0
           J=1
73         SS=SS+PM(LVOUT(J,I,K))
           J=J+1 
           IF (LVOUT(J,I,K).NE.0) GOTO 73
           SUMMAS(I,K)=SS            
           AMASS(I,K)=SS
72      CONTINUE
      ENDIF
      
      NVPOS0=NVPOS

      IF (NIN().EQ.2) THEN
         PCM=CMFUN(SQRTS,PM(1),PM(2))
         E1CM=SQRT(PM(1)**2+PCM**2)
         E2CM=SQRT(PM(2)**2+PCM**2)
         IF (STRFON(1).AND.STRFON(2) ) THEN
            SSMIN=(PM(1)+PM(2))**2
         ELSE IF (STRFON(1)) THEN
            SSMIN=PM(1)**2+PM(2)**2+2*PM(1)*E2CM 
         ELSE IF (STRFON(2)) THEN
            SSMIN=PM(1)**2+PM(2)**2+2*PM(2)*E1CM
         ELSE 
           SSMIN=SQRTS    
         ENDIF 
         
         SSMIN=DMAX1(SSMIN,(SUMMAS(1,1)+SUMMAS(1,2))**2)  
         SSMAX=SQRTS**2


         IF(SSMIN.GE.SSMAX) THEN
            IERR=1
            RETURN
         ENDIF         
         LVBUF(1)=1
         LVBUF(2)=2
         LVBUF(3)=0
         CALL CONINV(LVBUF) 
         NSS=NCUT(LVBUF)
         IF (NSS.NE.0)  CALL RANCOR(SSMIN,SSMAX,0.D0,1.D0,NSS)
         NSS=NREG(LVBUF) 
      ENDIF


      DO 110 L=1,200
110      ORIG(L)=.TRUE.

      DO 115 I=1,NOUT1
      NSPH(I)=0
      DO 115 K=1,2 
      NMSREG(I,K)=0
115   NMSCUT(I,K)=0
           
      L=1 
120   IF (LVINVR(1,L).NE.0 ) THEN
        IF (NEXTRG(L).NE.0) ORIG(NEXTRG(L))=.FALSE.
        IF (ORIG(L)) THEN        
          CALL SNGPOS(LVINVR(1,L),I,K,LVBUF)
          IF ( (I.EQ.0).AND.(K.EQ.0)) THEN 
             NSS=L 
          ELSE IF (LVBUF(1).EQ.0) THEN
            NMSREG(I,K)=L
          ELSE
            DO 125 NS=1,NSPH(I)
              IF(EQVECT(LVBUF,LVPOLE(1,I,NS))) THEN
                NCSREG(I,K,NS)=L
                GOTO 130 
              ENDIF
125         CONTINUE
            NS=NSPH(I)+1
            NSPH(I)=NS
            CALL LVCOPY(LVBUF,LVPOLE(1,I,NS))
            DO 127 KK=1,2
            NCSREG(I,KK,NS)=0
127         NCSCUT(I,KK,NS)=0
            ITYPEP(I,NS)=-1
            IF (SPOLE(LVBUF)) ITYPEP(I,NS) =-2
            NCSREG(I,K,NS)=L
130        CONTINUE
          ENDIF
        ENDIF
        L=L+1 
        GOTO 120
      ENDIF  

      L=1 
140   IF ( LVINVC(1,L).NE.0 ) THEN       
        CALL SNGPOS(LVINVC(1,L),I,K,LVBUF)
        IF ((I.EQ.0).AND.(K.EQ.0))  THEN
           CALL RANCOR(SSMIN,SSMAX,0.D0,1.D0,L)
        ELSE IF (LVBUF(1).EQ.0) THEN
          NMSCUT(I,K)=L
        ELSE
          DO 145 NS=1,NSPH(I)
            IF(EQVECT(LVBUF,LVPOLE(1,I,NS))) THEN
              NCSCUT(I,K,NS)=L
              GOTO 150 
            ENDIF
145       CONTINUE
          NS=NSPH(I)+1
          NSPH(I)=NS
          CALL LVCOPY(LVBUF,LVPOLE(1,I,NS))
          DO 147 KK=1,2
          NCSREG(I,KK,NS)=0
147       NCSCUT(I,KK,NS)=0
          ITYPEP(I,NS)=1
          IF (SPOLE(LVBUF)) ITYPEP(I,NS) = 2
          NCSCUT(I,K,NS)=L
150      CONTINUE
        ENDIF
        L=L+1 
        GOTO 140
      ENDIF

      DO 160 I=1,NOUT1
      IF (NSPH(I).EQ.0) THEN
        NSPH(I)=1        
        DO 167 KK=1,2
          NCSREG(I,KK,1)=0
167       NCSCUT(I,KK,1)=0
        IF( (I.EQ.1).AND.(NIN().EQ.1))  THEN
          LVPOLE(1,I,1)=NIN()+NOUT()+1
        ELSE
          LVPOLE(1,I,1)=-1
        ENDIF
        LVPOLE(2,I,1)=0
        ITYPEP(I,1)=1
      ELSE
        NS=NSPH(I)
        IF( (NCSREG(I,1,NS).NE.0).OR.(NCSREG(I,2,NS).NE.0) ) THEN
           ITYPEP(I,1)=-ITYPEP(I,1)
         ENDIF
      ENDIF
 
160   CONTINUE

      DO 180 I=1,NOUT1
      WESUM=0
      DO 175 N=1,NSPH(I)
        NWE=0 
        DO 170 K=1,2 
          L=NCSREG(I,K,N) 
165       IF (L.NE.0) THEN 
            NWE=NWE+1           
            L=NEXTRG(L)
            GOTO 165
          ENDIF
170     CONTINUE 
        IF (ITYPEP(I,N).GE.0) NWE=NWE+1
        SPH_WE(I,N)=NWE
175     WESUM=WESUM+NWE
      DO 180 N=1,NSPH(I)
        SPH_WE(I,N)=SPH_WE(I,N)/WESUM    
180   CONTINUE

      IF(NDEBUG.GT.0) THEN
         WRITE(*,*) 
     & 'decay  weight  type  Pvect  NCSREG1  NCSREG2  NCSCUT1  NCSCUT2'
         DO 901 I=1,NOUT1 
         DO 901 N=1, NSPH(I)
          write(*,'(I2,5x,F7.5,2x,I2,5x, 5I1,2x,4(I2,7x))')  
     &     I, SPH_WE(I,N), ITYPEP(I,N), 
     &   (ABS(lvpole(k,i,n)),k=1,5),
     &    NCSREG(I,1,N), NCSREG(I,2,N),  NCSCUT(I,1,N), NCSCUT(I,2,N)
901     CONTINUE

          write(*,*) 'decay  NMSCUT1 NMSCUT2 NMSREG1 NMSREG2 '
         DO 902 I=1,NOUT1
902      write(*,FMT='(5(1x,I3,4x))') I, NMSCUT(I,1),NMSCUT(I,2),
     &                                   NMSREG(I,1),NMSREG(I,2)      
      ENDIF
           
      END
      
