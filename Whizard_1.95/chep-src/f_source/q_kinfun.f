*********************************************
*   scalar products for in- and out-momenta *
*   and copy Q-vector to D-vector           *
*********************************************
      SUBROUTINE QSPROD
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      COMMON /SCLR/  DP(15)
      COMMON /QPVECT/ QP(0:3,100)
      DOUBLE PRECISION P
      COMMON /PVECT/  P(0:3,100)

      SAVE
      NTOT=NIN()+NOUT()
      DO 10 I=1,NTOT-1
      DO 10 J=I+1,NTOT
10    DP(INDX(I,J))=QVDOT4(I,J)

       DO 101 I=1,NTOT
       DO 101 J=0,3 
101      P(J,I)=QP(J,I)      
      RETURN
      END


*******************************************
*    Scalar product of two 4-vectors:     *
*******************************************
      FUNCTION QVDOT4(I,J)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z) 
      COMMON /QPVECT/ P(400)
      SAVE
      IX = 4*I-3
      JX = 4*J-3
       QVDOT4 = P(IX)*P(JX)-P(IX+1)*P(JX+1)-P(IX+2)*P(JX+2)
     &  -P(IX+3)*P(JX+3) 
      RETURN
      END

      SUBROUTINE LVTOQV(LV,NV)
      DIMENSION  LV(10)      
      CALL QVNULL(NV)
      I=1        
1     N=IABS(LV(I)) 
      IF ( N.EQ.0)  RETURN
      CALL QVSUM4(NV,N,NV,ISIGN(1,LV(I)))
      I=I+1
      GOTO 1
      END

********************************************************
*       SUM or Difference of two 4-vectors:            *
* ISG=1  sum   and  ISG=-1   difference                *
*             P(I) + ISG*P(J)=>P(K)                    *
********************************************************
      SUBROUTINE QVSUM4(I,J,K,ISG)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      COMMON /QPVECT/ P(400)
      SAVE
      IX = 4*I-3
      JX = 4*J-3
      KX = 4*K-3
      IF (ISG .EQ. 1) THEN
           P(KX) = P(IX) + P(JX)
           P(KX+1) = P(IX+1) + P(JX+1)
           P(KX+2) = P(IX+2) + P(JX+2)
           P(KX+3) = P(IX+3) + P(JX+3)
      ELSE
           P(KX) = P(IX) - P(JX)
           P(KX+1) = P(IX+1) - P(JX+1)
           P(KX+2) = P(IX+2) - P(JX+2)
           P(KX+3) = P(IX+3) - P(JX+3)
      ENDIF
      RETURN
      END 


*******************************************
* NULLification of a 4-vector:  P(I) => 0 *
*******************************************
      SUBROUTINE QVNULL(I)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z),INTEGER (I-N)
      COMMON /QPVECT/ P(400)
      SAVE
      IX=4*I-3
      P(IX) = 0
      P(IX+1) = 0
      P(IX+2) = 0
      P(IX+3) = 0
      RETURN
      END

      
      SUBROUTINE QEPS4(N1,N2,N3,N4)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      COMMON /QPVECT/ P(0:3,100)
      SAVE /QPVECT/ 
      A10=P(0,N1)
      A11=P(1,N1)
      A12=P(2,N1)
      A13=P(3,N1)

      A20=P(0,N2)
      A21=P(1,N2)
      A22=P(2,N2)
      A23=P(3,N2)

      A30=P(0,N3)
      A31=P(1,N3)
      A32=P(2,N3)
      A33=P(3,N3)
      
C                               A10  A20  A30  X0
C                               A11  A21  A31  X1
C                               A12  A22  A32  X2
C                               A13  A23  A33  X3


      
      D1021=A10*A21-A20*A11
      D1022=A10*A22-A20*A12
      D1023=A10*A23-A20*A13
      D1122=A11*A22-A21*A12
      D1123=A11*A23-A21*A13
      D1223=A12*A23-A22*A13
      
      P(0,N4)=+(            A31*D1223 - A32*D1123 + A33*D1122)
      P(1,N4)=  A30*D1223 -             A32*D1023 + A33*D1022
      P(2,N4)=-(A30*D1123 - A31*D1023 +             A33*D1021)
      P(3,N4)=  A30*D1122 - A31*D1022 + A32*D1021

      
      
      RETURN
      END
      
