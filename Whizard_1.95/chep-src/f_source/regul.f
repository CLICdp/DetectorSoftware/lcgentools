      SUBROUTINE INIREG
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      COMMON/INVREG/LVINVR(10,200),RGMASS(200),RGWDTH(200),NEXTRG(200),
     & NDEG(200)
      SAVE /INVREG/
      LVINVR(1,1)=0
      RETURN
      END
      

      FUNCTION NNREG()
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      COMMON/INVREG/LVINVR(10,200),RGMASS(200),RGWDTH(200),NEXTRG(200),
     & NDEG(200)
      SAVE /INVREG/

      NNREG=0
1     IF(LVINVR(1,NNREG+1).EQ.0) RETURN
      NNREG=NNREG+1
      GOTO 1 

      END
      

      FUNCTION SNGFUN(NN)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      COMMON/INVREG/LVINVR(10,200),RGMASS(200),RGWDTH(200),NEXTRG(200),
     & NDEG(200)
      SAVE /INVREG/
      IF ( NN.EQ.0) THEN
         SNGFUN=1
         RETURN
      ENDIF

      NVBUFF=NIN()+NOUT()+3
      CALL LVTONV(LVINVR(1,NN),NVBUFF)
      VINV=VDOT4(NVBUFF,NVBUFF)
      IF ( RGWDTH(NN).EQ.0) THEN 
         SNGFUN=1/DABS( VINV-RGMASS(NN)**2 )
      ELSE
         SNGFUN=1/((VINV-RGMASS(NN)**2)**2+(RGMASS(NN)*RGWDTH(NN))**2)        
      ENDIF        

      RETURN
      END

      SUBROUTINE DELREG(NREG)      
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      COMMON/INVREG/LVINVR(10,200),RGMASS(200),RGWDTH(200),NEXTRG(200),
     & NDEG(200)
      SAVE /INVREG/
      IF(NREG.LE.0) GOTO 30       
      DO 10 I=1,NREG    
10    IF (LVINVR(1,I).EQ.0) GOTO 30
      DO 15 I=1,NREG-1
        IF(NEXTRG(I).EQ.NREG) NEXTRG(I)=NEXTRG(NREG)
15      IF(NEXTRG(I).GT.NREG) NEXTRG(I)=NEXTRG(I)-1
      I=NREG
20    I1=I+1
      CALL   LVCOPY(LVINVR(1,I1) ,LVINVR(1,I))
      IF (LVINVR(1,I).EQ.0) GOTO 30      
      RGMASS(I)=RGMASS(I1)
      RGWDTH(I)=RGWDTH(I1)
      NDEG(I)  =NDEG(I1)
      NEXTRG(I)=MAX0(0,NEXTRG(I1)-1)
      I=I+1
      GOTO 20
      
30    RETURN
      END 

      SUBROUTINE VALREG(STYPE,VMASS,VWIDTH,NVDEG)
      DOUBLE PRECISION VMASS,VWIDTH
      LOGICAL STYPE
      character*2 doll
      COMMON /DOLL/ DOLL
      SAVE /DOLL/ 
30    WRITE(*,FMT='(1X,''Enter MASS value (in GeV): '''//doll//')') 
      READ(*,FMT='(G15.0)',ERR=30 ) VMASS
                 


40    IF (STYPE) THEN
       WRITE(*,FMT='(1X,''Enter WIDTH value (in GeV): '''//doll//')') 
         READ(*,FMT='(G15.0)',ERR=40) VWIDTH
      ELSE
       VWIDTH=0 
      ENDIF

      IF (STYPE.AND.(VWIDTH.NE.0)) THEN
        NVDEG=2
        RETURN
      ENDIF      
50    WRITE
     &(*,FMT='(1X,''Enter DEGREE of the pole (1 or 2): '''//doll//')')  
      READ(*,FMT='(I2)',ERR=50) NVDEG
      IF ((NVDEG.NE.1).AND.(NVDEG.NE.2)) GOTO 50
             
      RETURN
      END
      
      SUBROUTINE CHNREG(NREG)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)      
      COMMON/INVREG/LVINVR(10,200),RGMASS(200),RGWDTH(200),NEXTRG(200),
     & NDEG(200)
      SAVE /INVREG/
      LOGICAL SPOLE
      IF(NREG.LE.0)RETURN
      DO 10 I=1,NREG    
10    IF (LVINVR(1,I).EQ.0) RETURN
      CALL VALREG(SPOLE(LVINVR(1,NREG)),RGMASS(NREG),RGWDTH(NREG),
     & NDEG(NREG))
      RETURN
      END
      
      
      
      SUBROUTINE ADDREG(LV,RGMASW,RGWDTW,NNDEG)

      IMPLICIT DOUBLE PRECISION (A-H,O-Z)            
      COMMON/INVREG/LVINVR(10,200),RGMASS(200),RGWDTH(200),NEXTRG(200),
     & NDEG(200)
      SAVE /INVREG/
      LOGICAL EQVECT
      DIMENSION LV(10)
       
      LASTRG=0
      NREG=1
      
10    IF(LVINVR(1,NREG).NE.0) THEN
        IF(  EQVECT(LV,LVINVR(1,NREG)) ) LASTRG=NREG
         NREG=NREG+1
         GOTO 10
      ENDIF
    
      IF (NREG.GE.200) RETURN
      CALL LVCOPY(LV,LVINVR(1,NREG))
            
      RGMASS(NREG)=RGMASW
      RGWDTH(NREG)=RGWDTW 
      NDEG(NREG)=NNDEG
      NEXTRG(NREG)=0
      IF(LASTRG.NE.0) NEXTRG(LASTRG)=NREG
       
      RETURN
      END


      FUNCTION PMMOM(I)
      CHARACTER*3 PMMOM

      IF (I.GT.0) WRITE(PMMOM,100)  '+p ',I
      IF (I.LT.0) WRITE(PMMOM,100)  '-p',-I
      IF (I.EQ.0) PMMOM='   '
      RETURN
100   FORMAT(A2,I1)
      END


      
      SUBROUTINE VIEWRG
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      CHARACTER PTXT(20),doll*2
      common /doll/ doll
      CHARACTER*25 STATUS
      CHARACTER*1 MOM(5)
      LOGICAL SPOLE
      DIMENSION LAUX(10)
      COMMON/INVREG/LVINVR(10,200),RGMASS(200),RGWDTH(200),NEXTRG(200),
     & NDEG(200)
      SAVE /INVREG/
      IF (LVINVR(1,1).EQ.0) THEN
        WRITE(*,*) '****  There are no regularizations introduced *****'
        write(*,*)
        RETURN
      ENDIF   
      WRITE(*,1001)
1001  FORMAT(1X,'-------------------------  REGULARIZATIONS ------',
     &'----------------------------')

      WRITE(*,1002)
1002  FORMAT(1X,'|  N | INVARIANT           |   MASS     | ',
     &'   WIDTH   |DEG|    POSITION      |')

      WRITE(*,1003)
1003  FORMAT(1X,'|    |                     |   [GeV]    | ',
     &'   [GeV]   |   |                  |')

      WRITE(*,1004)
1004  FORMAT(1X,77('-') )     
      I=1
10    IF (LVINVR(1,I).EQ.0) THEN
      WRITE(*,1004)
      RETURN
      ENDIF
      
      J=1
      K=2
      PTXT(1)='(' 
      NP= LVINVR(J,I) 
15    IF (NP.LT.0) THEN
         PTXT(NEXTNN(K))='-'
         NP=-NP
      ELSE 
         PTXT(NEXTNN(K))='+'
      ENDIF  
      PTXT(NEXTNN(K))='p'
      PTXT(NEXTNN(K))=CHAR(NP+48)      
      J=J+1
      NP= LVINVR(J,I)
      IF (NP .NE.0) GOTO 15 
      
      PTXT(NEXTNN(K))=')'
      PTXT(NEXTNN(K))='*'
      PTXT(NEXTNN(K))='*'
      PTXT(NEXTNN(K))='2'
      
      DO 20 J=K,20
20    PTXT(J)=' '

      CALL SNGPOS(LVINVR(1,I) ,NDEC,NCLUST,LAUX )
 
      J=1
21    IF (LAUX(J).NE.0) THEN
         WRITE(MOM(J),FMT='(I1)') ABS(LAUX(J))
         J=J+1
         GOTO 21
      ENDIF
      J1=J
      DO 22 J=J1,5
22    MOM(J)=' '
      

      WRITE(STATUS,1010) NDEC,NCLUST,(MOM(kk),kk=1,5)
1010  FORMAT('S',I1,',L',I1,',P', 5(A1) )

      IF (SPOLE(LVINVR(1,I))) THEN      
      WRITE(*,1006)I,PTXT,RGMASS(I),RGWDTH(I),NDEG(I),STATUS
1006  FORMAT
     &(1X,'|',I3,' |',20A1,' |',1PE11.3,' |',E11.3,' |',I2,' |',A18,'|') 
      ELSE
      WRITE(*,1007)I,PTXT,RGMASS(I),NDEG(I),STATUS
1007  FORMAT
     &(1X,'|',I3,' |',20A1,' |',1PE11.3,' |',11X ,' |',I2,' |',A18,'|') 
      ENDIF
            
25    CONTINUE
      I=I+1
      IF (MOD(I,25).EQ.24) THEN
         WRITE(*,FMT='(1x,''Press ENTER to continue'''//doll//')')
         READ(*,*)         
      ENDIF
      GOTO 10
      
      END 


      SUBROUTINE NEWREG
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      CHARACTER PLIST(20),doll*2,PROCES*30
      COMMON /PROCES/ NSUB,PROCES
      common /doll/ doll
      COMMON /NSESS/ NSESS
      LOGICAL SPOLE
      DIMENSION LV(21)
      COMMON/INVREG/LVINVR(10,200),RGMASS(200),RGWDTH(200),NEXTRG(200),
     & NDEG(200)
      SAVE

      WRITE(*,FMT='(1X,''Enter particle number(s) for an invariant:'''
     &//doll//')')  
         READ(*,FMT='(20A1)') PLIST
         LPOS=1
         DO 10 K=1,20
           NCH=ICHAR(PLIST(K))-ICHAR('0')
           IF ( (1.LE.NCH).AND.(NCH.LE.NIN()+NOUT()) ) 
     &        LV(NEXTNN(LPOS))=NCH
10       CONTINUE 
         LV(LPOS)=0
         CALL ORDINV(LV)
         CALL CONINV(LV)
         IF (LV(1).EQ.0) THEN
           WRITE(*,*) 'Incorrect input'
         ELSE
           CALL VALREG(SPOLE(LV),VMASS,VWIDTH,NVDEG)            
           CALL ADDREG(LV,VMASS,VWIDTH,NVDEG)
         ENDIF           
         RETURN
      END

      SUBROUTINE WRTREG(NCHAN)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      COMMON/INVREG/LVINVR(10,200),RGMASS(200),RGWDTH(200),NEXTRG(200),
     & NDEG(200)
      LOGICAL ZERO, EQVECT
      CHARACTER*1 P(6),S(6),NP(6) 
      CHARACTER*50 TXT1,TXT2
      CHARACTER*10 TXT3
      SAVE/INVREG/

      N=1
10    IF (LVINVR(1,NEXTNN(N)).NE.0) GOTO 10   
      N=N-2
      TXT1='********  '
      TXT2=' REGULALIZATIONS ARE INTRODUCED ********'
      TXT1='=================== There are introduced '
      TXT2=' regularizations =================='
      WRITE(NCHAN, 110) TXT1,N,TXT2 
      IF (N.EQ.0) RETURN
      TXT1= '   MASS='
      TXT2= '  WIDTH='
      TXT3= '    DEG='
      DO 50 I=1,N
         ZERO=.FALSE.
         DO 20 K=1,6
         IF (ZERO) THEN
            P(K)=' '
            S(K)=' '
            NP(K)=' '            
         ELSE 
            P(K)='p'
            IF (LVINVR(K,I).GT.0) THEN
                S(K)='+'
            ELSE
                S(K)='-'
            ENDIF        
            NP(K)=CHAR(48+IABS(LVINVR(K,I)))
         
            ZERO=(LVINVR(K+1,I).EQ.0)
         ENDIF
20       CONTINUE

50       WRITE(NCHAN,120) (S(K),P(K),NP(K), K=1,6),
     &    TXT1 , RGMASS(I), TXT2, RGWDTH(I),'    DEG=',NDEG(I)
      RETURN
      
110   FORMAT(1X,A41,I2,A35)

120   FORMAT(1X,6(A1,A1,A1),A8,1PE17.10,A8,E17.10,A8,I1)


      ENTRY RDRREG(NCHAN)
      READ(NCHAN, 110) TXT1,N,TXT2 
      DO 60 I=1,N
        READ(NCHAN,120) (S(K),P(K),NP(K), K=1,6),
     &    TXT1 , RGMASS(I), TXT2, RGWDTH(I),TXT1,NDEG(I)
      
        DO 65 K=1,6
          IF (NP(K).EQ.' ') THEN 
            LVINVR(K,I)=0
          ELSE
             LVINVR(K,I)=ICHAR(NP(K))-48
             IF(S(K).EQ.'-')  LVINVR(K,I)=-LVINVR(K,I)
         ENDIF        
65      CONTINUE  

60    CONTINUE
      LVINVR(1,N+1)=0           
      
      DO 70 I=1,N
70    NEXTRG(I)=0
       
      DO 80 I=1,N-1
      DO 90 J=I+1,N
         IF( EQVECT(LVINVR(1,I),LVINVR(1,J)) ) THEN
            NEXTRG(I)=J
            GOTO 80
          ENDIF         
90     CONTINUE
80     CONTINUE          
      RETURN
      END
      
      FUNCTION NREG(LV)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)            
        
      COMMON/INVREG/LVINVR(10,200),RGMASS(200),RGWDTH(200),NEXTRG(200),
     & NDEG(200)
      SAVE /INVREG/
      LOGICAL EQVECT
      DIMENSION LV(10)
       
      CALL CONINV(LV)
      NREG=1
10    IF((LVINVR(1,NREG).NE.0).AND.(.NOT.EQVECT(LV,LVINVR(1,NREG))))
     &THEN
         NREG=NREG+1
         GOTO 10
      ENDIF
      IF (LVINVR(1,NREG).EQ.0) NREG=0 
      RETURN
      END

      SUBROUTINE GETREG(NSING,SINGAR,SHIFT,FMULT,NFIRST)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)                    
      COMMON/INVREG/LVINVR(10,200),RGMASS(200),RGWDTH(200),NEXTRG(200),
     & NDEG(200)
      SAVE /INVREG/
      DIMENSION SINGAR(3,100)
      N=NFIRST
10    IF (N.EQ.0) GOTO 20     
      IF (NSING.GE.100) GOTO 20
      NSING=NSING+1
      SINGAR(1,NSING)= (RGMASS(N)**2-SHIFT)*FMULT
      SINGAR(2,NSING)= DABS((RGMASS(N)*RGWDTH(N))*FMULT)
      SINGAR(3,NSING)= NDEG(N)
      N=NEXTRG(N)
      GOTO 10
20    RETURN
      END
      
