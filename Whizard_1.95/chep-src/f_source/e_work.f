      SUBROUTINE E_BEG
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)  
      PARAMETER ( MKEY = 9)
      DIMENSION P(0:3,10),S(0:3)
      CHARACTER*1 CKEY(MKEY),CH
      DIMENSION VMIN(11),VMAX(11),LP(10,11),KEY(11)     
      CHARACTER*100 STR
      CHARACTER*20 WORD
      CHARACTER*7  XNAME
      COMMON/BVEG4/CALLS,TI,TSI
      COMMON/BVEG1/NCALL,ITMX,NPRN_,NDEV_,XL_(10),XU_(10),ACC_
      LOGICAL FILEON
       LOGICAL HISTON
      CHARACTER*3 WGTDIM
      CHARACTER * 6 PINF
      COMMON /MCINTR/ NCALL0,ITMX0,FILEON,MDS0
      DIMENSION LENP(11)
      SAVE  

      DATA CKEY/'A','C','E','M','T','S','W','P','Y'/
      NIN1=NIN()+1
      NTOT=NIN()+NOUT()

      DO 20 K=1,11
         READ(*,FMT='(A80)',ERR=100,END=100 ) STR
         CALL CLIP_W(STR,WORD)
         XNAME=WORD
         
          IF (WORD.EQ.' ') THEN
              HISTON=.FALSE.
              GOTO 200 
          ENDIF

         CH=WORD(1:1)
         KEY(K)=0
         DO 12 I=1,MKEY 
12         IF( CH.EQ.CKEY(I)) KEY(K)=I 
         IF (KEY(K).EQ.0) GOTO 100
         LENP(K)=0
         DO 14 J=2,10
           CH=WORD(J:J)
           IF (CH.NE.' ') THEN
                LP(J-1,K)=ICHAR(CH)-ICHAR('0')
                LENP(K)=J-1
            ENDIF
14       CONTINUE
         CALL CLIP_W(STR,WORD)
         READ(WORD,FMT='(G15.0)',ERR=100 ) VMIN(K)
         CALL CLIP_W(STR,WORD)
         READ(WORD,FMT='(G15.0)',ERR=100 ) VMAX(K)
         IF (STR.EQ.' ') GOTO 20
         CALL CLIP_W(STR,WORD)
         READ(WORD,FMT='(I3)',ERR=100 ) NBIN
         HISTON=.TRUE.
         GOTO 200
20    CONTINUE

100   WRITE(*,FMT='(A20,I2)') 'Error into  line ',K
      STOP 'ERROR'

200   CONTINUE
* test   
      KK=K
      IF (.NOT.HISTON) KK=KK-1 
      DO  101 K=1,KK
        CH=CKEY(KEY(K))
        IF ((CH.NE.'W').AND.(LP(1,K).LE.0)) GOTO 100
        IF(VMIN(K).GE.VMAX(K)) GOTO 100
        CH=CKEY(KEY(K))
        IF ( ((CH.EQ.'C').OR.(CH.EQ.'A').OR.(CH.EQ.'P')).AND. 
     &      ((LENP(K).NE.2).OR.(LP(1,K).EQ.LP(2,k))) ) GOTO 100
        DO 102  L=1,LENP(K)
          IF ( (LP(L,K).GT.NIN()+NOUT() ).OR.(LP(L,K).LT.0) ) GOTO 100       
          IF ( ((CH.EQ.'M').OR.(CH.EQ.'E').OR.(CH.EQ.'T').OR.(CH.EQ.'P')
     &    .OR.(CH.EQ.'Y')).AND.(LP(L,K).LE.NIN()) )  GOTO 100
102     CONTINUE                      
101   CONTINUE 
           IF (HISTON) THEN
      CH=CKEY(KEY(KK))
      IF (CH.EQ.'A') THEN
        WRITE(*,*) 'Diff. cross section [pb/Deg]'
         WRITE(*,FMT='( ''Angle( p('',I1,''),p('',I1,''))'',A5 )') 
     &   LP(1,KK),LP(2,KK),'[Deg]'

      ELSE IF(CH.EQ.'C') THEN
        WRITE(*,*) 'Diff. cross section [pb]'
        WRITE(*,FMT='( ''Cosine( p('',I1,''),p('',I1,''))'')') 
     &   LP(1,KK),LP(2,KK)
      ELSE IF(CH.EQ.'E') THEN
      WRITE(*,*) 'Diff. cross section [pb/GeV]'
      WRITE(*,*) 'Energy ',XNAME(1:LENP(KK)+1),'[GeV]'      
      ELSE IF(CH.EQ.'M') THEN      
        WRITE(*,*) 'Diff. cross section [pb/GeV]'
        WRITE(*,*) ' Mass ',XNAME(1:LENP(KK)+1),'[Gev]' 
      ELSE IF(CH.EQ.'T') THEN
        WRITE(*,*) 'Diff. cross section [pb/GeV]'  
        WRITE(*,*) 'Transverse momentum P_t',XNAME(2:LENP(KK)+1),'[GeV]'
      ELSE IF(CH.EQ.'S') THEN
       WRITE(*,*) 'Diff. cross section [pb/Gev**2]' 
       WRITE(*,*) ' Squared Mass ',XNAME(1:LENP(KK)+1), '[Gev**2]'     
      ELSE IF(CH.EQ.'W') THEN 
       WRITE(*,*) 'Weight distribution [/pb]'
       WRITE(*,*) 'Weight [pb]'
      ELSE IF(CH.EQ.'P') THEN 
        WRITE(*,*) 'Diff. cross section [pb]'
        WRITE(*,FMT='( ''Cosine( p('',I1,'')*,p('',I1,'')*)'')')
     &   LP(1,KK),LP(2,KK)
      ELSE IF(CH.EQ.'Y') THEN
       WRITE(*,*) 'Diff. cross section [pb/Gev**2]' 
       WRITE(*,*) ' Rapidity ',XNAME(1:LENP(KK)+1)
      ENDIF

      CALL H_INI(VMIN(KK),VMAX(KK),NBIN)
      ELSE
       IF(NIN().EQ.1) THEN
         WGTDIM='Gev'
       ELSE
          WGTDIM='pb'
       ENDIF

         WRITE(6,300) WGTDIM, ( (PINF(1,I),J,J=0,3),I=NIN1,NTOT)
300      FORMAT(1x,'  Weight [',A3,']',40(3x,'P{',A3,'}(',I1,
     &                                              ') [Gev]' ))
      ENDIF
      RETURN

      
      ENTRY E_END
        IF (HISTON) THEN
            CALL H_OUT
        ELSE
            ENDFILE(6)
        ENDIF
 
      RETURN

      ENTRY E_CALL(WEIGHT,P)
      DO 60 K=1,KK 
         CH=CKEY(KEY(K))
         GOTO (51,51,54,54,54,54,56,57,54),KEY(K) 
* 'A','C' - case
51       L1=LP(1,K)
         L2=LP(2,K)  
           XX=(P(1,L1)*P(1,L2)+P(2,L1)*P(2,L2)+P(3,L1)*P(3,L2))/
     *     SQRT( (P(1,L1)**2 + P(2,L1)**2 + P(3,L1)**2)*
     *           (P(1,L2)**2 + P(2,L2)**2 + P(3,L2)**2)
     *         )
         IF(KEY(K).EQ.1) XX=ACOS(XX)*180.0/3.14159265358979323846  
         GOTO 50
* 'M','S','E','T','Y'- case
54       JMIN=0
         JMAX=3
         IF (CH.EQ.'E') THEN
            JMAX=0
         ELSEIF(CH.EQ.'T') THEN
            JMIN=1
            IF( NIN().EQ.2) JMAX=2
         ENDIF
         DO 61 J=JMIN,JMAX 
61        S(J)=0
         DO 62 I=1,LENP(K)
           L=LP(I,K)
           LSGN=1
           IF (L.LE.NIN()) LSGN=-1
           DO 62 J=JMIN,JMAX
62          S(J)=S(J)+LSGN*P(J,L)
         IF (CH.EQ.'E') THEN
           XX=S(0) 
         ELSEIF(CH.EQ.'Y') THEN
           XX=LOG((S(0)+S(3))/(S(0)-S(3)))/2
         ELSE
           XX=0
           DO 64 J=1,JMAX
64         XX= XX+S(J)**2
           IF( CH.EQ.'T') THEN
             XX=SQRT(XX)
           ELSE
             XX=S(0)**2-XX
             IF(CH.EQ.'M') XX=SQRT(XX)
           ENDIF
         ENDIF    
         GOTO 50
* 'W' - case 
56       XX=WEIGHT*CALLS*ITMX0
         IF (HISTON.AND.(K.EQ.KK)) WEIGHT=1/(CALLS*ITMX0)
         GOTO 50
* 'P' - case 
57       L1=LP(1,K)
         L2=LP(2,K)
         CALL PMAS(1,L1,AM1)
         CALL PMAS(1,L2,AM2)
         NV=15
         CALL VSUM4(L1,L2,NV,1)
C*         WRITE(*,FMT='(4E15.3 )') (P(jj,NV),jj=0,3)
C*         WRITE(*,*)  ' VDOT4(NV,NV)=', VDOT4(NV,NV)
         AMM=SQRT( VDOT4(NV,NV) )

         PCM=SQRT((AMM**2-(AM1+AM2)**2)*(AMM**2-(AM1-AM2)**2))/(2*AMM)
         PP=P(1,NV)**2+P(2,NV)**2+P(3,NV)**2
         EE=P(0,NV)
         P(0,NV)=PP/EE
         PP=SQRT(PP)
C*         write(*,*) 'AM1=',AM1, 'AM2=',AM2, 'AMM=',AMM
C*         write(*,*) 'PCM=',PCM,'PP',PP,'EE=',EE
C*         WRITE(*,FMT='(4E15.3 )') (P(jj,NV),jj=0,3)
C*         WRITE(*,FMT='(4E15.3 )') (P(jj,L1),jj=0,3)

C*         WRITE(*,*) ' VDOT4(L1,NV)=', VDOT4(L1,NV)
         XX= - VDOT4(L1,NV)*EE/(PP*AMM*PCM) 

50       IF ((XX.LT.VMIN(K)).OR.(XX.GT.VMAX(K)) ) WEIGHT=0

60    CONTINUE
         
      IF (HISTON) THEN
         CALL  H_PUT(XX,WEIGHT)
      ELSE
         IF (WEIGHT.NE.0) WRITE(*,FMT='(1x,1PE14.6,40E18.10)') WEIGHT,
     &   ( (P(I,J),I=0,3),J=NIN1,NTOT)
      ENDIF
        
      RETURN
      END


      SUBROUTINE H_INI(VMIN,VMAX,NBIN)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION SVAL(500),SERR(500),NPOINT(500)
      SAVE
      IF(NBIN.GT.500) THEN
        NBIN_=500
      ELSE
        NBIN_=NBIN
      ENDIF
      VMAX_=VMAX
      VMIN_=VMIN
      NTOT=0
      DV=(VMAX-VMIN)/NBIN_
      DO 10 I=1,500
        NPOINT(I)=0
        SVAL(I)=0
10      SERR(I)=0
      
      RETURN

      ENTRY H_PUT(X,WEIGHT)
      NTOT=NTOT+1
      IF ((X.LT.VMIN_).OR.(X.GE.VMAX_)) RETURN
      N=(X-VMIN_)/DV+1
      SVAL(N)=SVAL(N)+WEIGHT
      SERR(N)=SERR(N)+WEIGHT**2
      NPOINT(N)=NPOINT(N)+1
      RETURN
   
      ENTRY H_OUT
      DO 20 I=1,NBIN_
      WRITE(*,FMT='(1x,3(1PE12.4),I7)') VMIN_+(I-1)*DV,
     &   SVAL(I)/DV ,SQRT(SERR(I)-SVAL(I)**2/NTOT)/DV, NPOINT(I)
20    CONTINUE
      WRITE(*,FMT='(1x,1PE12.4)') VMAX_
      END

        SUBROUTINE CLIP_W(STR,WORD)
        CHARACTER*100 STR,STRBUF
        CHARACTER*20  WORD
        I=0
20      I=I+1
        IF (I.EQ.81) THEN
           WORD=' '
           RETURN
        ENDIF
        IF (STR(I:I).EQ.' ') GOTO 20
        J=I
30      J=J+1
        IF (STR(J:J).NE.' ') GOTO 30
        WORD=STR(I:J-1)
        STRBUF=STR(J:80)
        STR=STRBUF
        END
