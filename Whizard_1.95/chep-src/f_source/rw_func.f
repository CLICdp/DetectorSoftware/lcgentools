*#########################################
*#   Write-Read  procedures              #
*#                                       #
*#   contents:                           #
*#########################################

****************************************
**  read-write the number of session ***
****************************************
      SUBROUTINE WNSESS(MODE)
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      COMMON /NSESS/ NSESS
      SAVE

      WRITE(MODE,20) NSESS,'the session number'
      RETURN
     
*********************
      ENTRY INSESS
       NSESS=1
      RETURN

*********************
      ENTRY RNSESS(MODE)
       READ(MODE,30) NSESS
      RETURN

20    FORMAT(I22,' = ',A)
30    FORMAT(BN,(I22))
      END
      
*********************************************
*   write process string
*********************************************
      SUBROUTINE WRTPRC
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      CHARACTER   PINF*6,PRTCL(10)*2,PROCES*30,XPROC*30
      COMMON /PROCES/ NSUB,PROCES
      SAVE

*** number of subprocess

*** initialization of particle names
      DO 10 KK=1,NIN()+NOUT()
         PRTCL(KK)=PINF(NSUB,KK)
10    CONTINUE

      XPROC=PRTCL(NIN()+NOUT())
      DO 20 I=NOUT()-1,1,-1
       PROCES=PRTCL(NIN()+I)//','//XPROC
       XPROC=PROCES
20    CONTINUE
      PROCES=' -> '//XPROC
      XPROC=PROCES
      IF (NIN().EQ.2) THEN
            PROCES=','//PRTCL(2)//XPROC
            XPROC=PROCES
      ENDIF
      PROCES=PRTCL(1)//XPROC
      RETURN
      END

      SUBROUTINE I_PRC
      CHARACTER PROCES*30, XPROC*30
      COMMON /PROCES/  NSUB,PROCES
      SAVE
         NSUB=1
         CALL  WRTPRC
      RETURN      
 
      ENTRY W_PRC(MODE)
        WRITE(MODE,FMT='(A30,5x,I3)') PROCES ,NSUB
      RETURN

      ENTRY R_PRC(MODE,ICC)         
         READ(MODE,FMT='(A30,5x,I3)',ERR=30) XPROC,NSUB
         IF ( NSUB.GT.NPRC() ) THEN
30         ICC=1
           NSUB=1
           CALL WRTPRC
           RETURN
         ENDIF

         CALL  WRTPRC
         IF (XPROC.NE.PROCES) THEN
             ICC=1
         ELSE
             ICC=0
         ENDIF
      RETURN
      END


**************************************
**  read-write MC parameters       ***
**************************************
      SUBROUTINE W_MC(MODE)
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      COMMON /USRRND/XRND(10),NRND
      LOGICAL FILEON
*      COMMON/MCINTR/ NCALL,ITMX,FILEON,MDS0
      COMMON/MCINTR/ IN(2),FILEON,MDS0
      CHARACTER*10 BUFF
      SAVE      
      WRITE(MODE,20) IN(1),'= NCALL'
      WRITE(MODE,20) IN(2),'= ITMX '
      WRITE(MODE,20) MDS0, '= MDS'
      RETURN
     
*********************
      ENTRY I_MC
          IN(1) = 10000
          IN(2) = 5
          MDS0=1
          NRND=0
      RETURN

*********************
      ENTRY R_MC(MODE)
      DO 11 I=1,2
11      READ(MODE,20) IN(I),BUFF
        READ(MODE,20) MDS0,BUFF
20    FORMAT(I22,A10)
      RETURN
      END
      
**************************************
**  read-write model parameters    ***
**************************************
      SUBROUTINE W_MDL(MODE)
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      CHARACTER NAME*6

      WRITE(MODE,50) NVAR(),'number of physical model parameters'
      DO 10 I=1,NVAR()
          CALL VINF(I,NAME,VAL)
          WRITE(MODE,60) VAL,NAME
10     CONTINUE
      RETURN
     
*********************
      ENTRY I_MDL
      RETURN

*********************
      ENTRY R_MDL(MODE)
      READ(MODE,51) NV
      DO 20 I=1,NV
         READ(MODE,61) VAL
         CALL ASGN(I,VAL)
20    CONTINUE

50    FORMAT(I22,' = ',A)
51    FORMAT(BN,(I22))
60    FORMAT(G22.14,' = ',A)
61    FORMAT(BN,(G22.14))

      RETURN
      END
      
**************************************
**  read-write IN-state parameters  ***
**************************************
      SUBROUTINE W_IN(MODE)
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      LOGICAL STRFON
      COMMON /STRFST/ XPARTN(2),BE(2),STRFON(2)
      COMMON /SQS/ SQRTS,RAPID
      SAVE

      CALL WRT_SF(MODE)
      WRITE(MODE,20) SQRTS,'SQRTS'
      WRITE(MODE,20)  RAPID,'C.M. rapidity'
      RETURN

*********************
      ENTRY I_IN 
      RAPID=0
      STRFON(1)= .FALSE.
      STRFON(2)= .FALSE.
      CALL INI_SF(MODE)   
      RETURN

*********************
      ENTRY R_IN(MODE)
      CALL RD_SF(MODE)
      READ(MODE,21) SQRTS
      READ(MODE,21) RAPID
C*      READ(MODE,23) STRFON(1)
C*      READ(MODE,23) STRFON(2)

20    FORMAT(G22.14,' = ',A)
21    FORMAT(BN,(G22.14))
22    FORMAT(21x,L1,' = ',A)
23    FORMAT(L22)

      RETURN
      END
       
      SUBROUTINE I_WDTH
      LOGICAL GWIDTH,RWIDTH
      COMMON /WDTH/ GWIDTH,RWIDTH
      SAVE
      GWIDTH=.FALSE.
      RWIDTH=.FALSE.
      RETURN
      END

      SUBROUTINE W_WDTH(MODE)
      LOGICAL GWIDTH,RWIDTH
      COMMON /WDTH/ GWIDTH,RWIDTH
      SAVE
      WRITE(MODE,1) GWIDTH, 'gauge invariant widths'
      WRITE(MODE,1) RWIDTH, 'running widths'
1     FORMAT(1x,L1,3x,A)
      RETURN
      END


      SUBROUTINE R_WDTH(MODE)
      LOGICAL GWIDTH,RWIDTH
      COMMON /WDTH/ GWIDTH,RWIDTH
      SAVE
      READ(MODE,1) GWIDTH
      READ(MODE,1) RWIDTH
1     FORMAT(1x,L1)

      RETURN
      END

      
