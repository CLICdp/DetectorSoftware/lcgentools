5 5 50 8

     ON - momenta of last (in the process string)
          particle is expressed through other
          independent particles momenta with the 
          help of law of energy conservation.

     OFF - all particles momenta will appear
          in symbolic answer.
