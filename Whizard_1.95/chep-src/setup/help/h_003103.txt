3 2 72 18                  

                             Particles

        All particles in the physical model must be included in the
  "Particles" table. It contains different properties of particles
   like mass, spin, etc. Some of them have numerical value and must
   be included in the "Parameters" table. Note that the insertion of
   a second gluon is forbidden.

      The "Particles" table consists of 8 fields:
 
      Full name  - full name of particle. This field can contain
                   any information.

      P, aP  - these fields contain the short names (symbols) of the 
               particle and antiparticle correspondingly. If the 
               antiparticle is identical with the particle the name
               of the antiparticle must be the same.

      2*Spin - double spin of particle.

      Mass -  zero or mass identifier. The value of the mass
              must be defined in the "Parameters" table.
              If this field contains zero then CompHEP considers
              the particle as massless and its identifier does not
              appear in the analytical results.

      Width - the width of decay. The value of this field must be
              zero for a stable particle. The concrete value of this
              field must be defined in "Parameters" table.

      Color - the dimension of the representation of the color group.
              For a colorless particle this field must contain 1.

      Aux -  special field to designate the left/right massless spinor
             particles or to denote the particle (with constant 
             propagator) imitating the four-fermions vertex. In the 
             latter case the asterisk (*) must be used. The symbol "G"
             is used for marking (in 'tHooft-Feynman gauge) massive 
             vector bosons as gauge particles and the corresponding 
             Faddev-Popov ghosts and goldstone partners are 
             implemented.

    Example:

          +----------------------------------------------+
          |Full name |P  |aP |2*Spin|Mass|Width|Color|Aux|
          |----------+---+---+------+----+-----+-----+---|
          |Electron  |e1 |E1 |1     |Me  |0    |1    |   |
          |Neutrino  |n1 |N1 |1     |0   |0    |1    |L  |
          |Z-boson   |Z  |Z  |2     |MZ  |WZ   |1    |G  |
          +----------------------------------------------+

    Keys to Edit/View:

       Up/Down     - move cursor from line to line.
       Right/Left  - move cursor from field to field.
       PgUp/PgDn   - move cursor by page.
       T/t         - move cursor to the top of table
       B/b         - move cursor to the bottom of table
       Enter/E/e   - select field to edit ( If you cancel any
                     modification, press [Ecs], if you want to save
                     new content of field then press [Enter]).
       Escape      - return to previous menu.
       Del/D/d     - delete line and put it into scrap.
       Y/y         - copy line to the scrap
       Ins/I/i     - insert line from the scrap.
       M/m         - display last message.
       S/s         - set new size of the field. This is valid
                     for field that mark by '>' and '<' symbols
                     in the head.
       P/p         - print table to the file "model.txt" in printable
                     format.
       Z/z         - view/edit field in zoom format.

    In edit mode the following keys have special action:

       Up/Down     - save changes and start to edit up/down field.
       PgUp/PgDn   - save changes and start to edit left/right field.
