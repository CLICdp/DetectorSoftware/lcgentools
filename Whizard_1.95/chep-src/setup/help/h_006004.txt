10 7 45 7
    Generates REDUCE program for the 
    calculation of squared diagrams.
    A separate program is generated
    for each squared diagram.

    This output can be used for checking
    of built-in symbolic calculator.