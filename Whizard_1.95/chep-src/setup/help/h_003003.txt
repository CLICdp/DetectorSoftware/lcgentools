6 6 51 6
       Restore original model version 
     for built-in physical models.

       Delete user's models. Such models
     are marked by the underscore symbol
     '_' in the first position.