3 2 72 18                  

                               Lagrangian

      This table consists of the list of vertices. The first four 
  fields contain the names of the interacting particles. Vertices 
  with 3 or 4 legs are possible. The last two fields define the 
  vertex itself. Vertices do not include color factors: color 
  factors are generated automatically during the symbolic 
  calculations.
      It is important to note that QCD diagrams with  ghost and 
  four-gluon vertices are taken into account but they are not 
  inserted into the table.
 
     P1,P2,P3,P4 - names of interacting particles. 
 
     Factor - the scalar factor independent of the Lorentz and color
              structure
 
     dL/dA(P1)dA(P2)dA(P3)dA(P4) -  Lorentz part of vertex. The 
              identifiers m1, m2, m3, m4 correspond to the Lorentz 
              indices, p1, p2, p3, p4 correspond to the momentum of 
              the particles. These identifiers cannot be used for 
              other purposes.

   Example:

          +-----------------------------------------------------+
          |P1 |P2 |P3 |P4 |Factor  |dL/dA(P1)dA(P2)dA(P3)dA(P4) |
          |---+---+---+---+--------+----------------------------|
          |U  |u  |G  |   |GG      |G(m3)                       |
          |H  |W+ |W- |   |EE*MW/SW|m2.m3                       |
          +-----------------------------------------------------+


          Here the dot (.) is used for the scalar product of Lorentz 
          4-vectors; G denotes the Dirac gamma matrix, G5 is the
          gamma5 matrix.

    Keys to Edit/View:

       Up/Down     - move cursor from line to line.
       Right/Left  - move cursor from field to field.
       PgUp/PgDn   - move cursor by page.
       T/t         - move cursor to the top of table
       B/b         - move cursor to the bottom of table
       Enter/E/e   - select field to edit ( If you cancel any
                     modification, press [Ecs], if you want to save
                     new content of field then press [Enter]).
       Escape      - return to previous menu.
       Del/D/d     - delete line and put it into scrap.
       Y/y         - copy line to the scrap
       Ins/I/i     - insert line from the scrap.
       M/m         - display last message.
       S/s         - set new size of the field. This is valid
                     for field that mark by '>' and '<' symbols
                     in the head.
       P/p         - print table to the file "model.txt" in printable
                     format.
       Z/z         - view/edit field in zoom format.

    In edit mode the following keys have special action:

       Up/Down     - save changes and start to edit up/down field.
       PgUp/PgDn   - save changes and start to edit left/right field.
