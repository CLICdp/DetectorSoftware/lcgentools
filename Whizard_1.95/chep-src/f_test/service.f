*******************************
*    CompHEP  version  3.2    *
*******************************
      FUNCTION NIN()
         NIN=2
      RETURN
      END

      FUNCTION NOUT()
         NOUT=3
      RETURN
      END

      FUNCTION LENR()
         LENR=8
      RETURN
      END

      SUBROUTINE CPTH(PATH,D_SLASH,F_SLASH)
      CHARACTER*60 PATH
      CHARACTER*1 D_SLASH,F_SLASH
      PATH='../../setup'
      D_SLASH='/'
      F_SLASH='/'
      RETURN
      END


      FUNCTION NPRC()
         NPRC=1
      RETURN
      END

      FUNCTION PINF(NSUB,NPRTCL)
      CHARACTER*6 PINF,NAMES(1,6)
      DATA ( NAMES(1,I),I=1,6)/'A1','A2','A3','A4','A5','A6'/
      PINF=NAMES(NSUB,NPRTCL)
      RETURN
      END

      FUNCTION NVAR()
         NVAR=6
      RETURN
      END

      SUBROUTINE VINF(NUMVAR,NAME,VAL)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DOUBLE PRECISION VAL
      COMMON/VARS/A(6)
      CHARACTER*6 NAMES(6),NAME
      SAVE
      DATA NAMES/'M1','M2','M3','M4','M5','M6'/
      IF (NUMVAR.GT.6) RETURN
      NAME=NAMES(NUMVAR)
      VAL=A(NUMVAR)
      RETURN
      END

      SUBROUTINE ASGN(NUMVAR,VALNEW)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DOUBLE PRECISION VALNEW
      COMMON/VARS/A(6)
      SAVE
      IF((NUMVAR.LT.1).OR.(NUMVAR.GT.6)) RETURN
      A(NUMVAR)=VALNEW
      RETURN
      END

      SUBROUTINE PMAS(NSUB,NPRTCL,VAL)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION NVALUE(1,6)
      COMMON/VARS/A(6)
      SAVE
      DATA ( NVALUE(1,I),I=1,6)/1,2,3,4,5,6/
      N=NVALUE(NSUB,NPRTCL)
      IF (N.EQ.0)  THEN 
         VAL=0
      ELSE
         VAL=A(N)
      ENDIF
      RETURN
      END


      SUBROUTINE  VINI
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DOUBLE PRECISION SQRTS
      COMMON/VARS/A(6)
      COMMON/SQS/SQRTS
      COMMON/LOGG/L(1)
      LOGICAL GWIDTH,RWIDTH
      COMMON/WDTH/ GWIDTH,RWIDTH
      SAVE
      GWIDTH=.FALSE.
      RWIDTH=.FALSE.
      SQRTS= 100.0
      A(1)= 0
      A(2)= 0
      A(3)= 0
      A(4)= 0
      A(5)= 0
      A(6)= 0
      RETURN
      END

      FUNCTION INDX(K,L)
      I=MIN(K,L)
      J=MAX(K,L)
      INDX=I+(J-1)*(J-2)/2
      RETURN
      END

