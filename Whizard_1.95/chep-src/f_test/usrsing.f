      SUBROUTINE INIsng
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      COMMON/INVsng/LVINVR(10,200),RGMASS(200),RGWDTH(200),NEXTRG(200),
     & NDEG(200)
      SAVE /INVsng/
      LVINVR(1,1)=0
      RETURN
      END
      

      FUNCTION NNsng()
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      COMMON/INVsng/LVINVR(10,200),RGMASS(200),RGWDTH(200),NEXTRG(200),
     & NDEG(200)
      SAVE /INVsng/

      NNsng=0
1     IF(LVINVR(1,NNsng+1).EQ.0) RETURN
      NNsng=NNsng+1
      GOTO 1 

      END
      

      FUNCTION SQME(NSUB)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      COMMON/INVsng/LVINVR(10,200),RGMASS(200),RGWDTH(200),NEXTRG(200),
     & NDEG(200)
      SAVE /INVsng/

      NVBUFF=NIN()+NOUT()+3

      SQME=1
      NN=1
1     IF (LVINVR(1,NN).EQ.0) RETURN
      CALL LVTONV(LVINVR(1,NN),NVBUFF)
      VINV=VDOT4(NVBUFF,NVBUFF)
      IF ( RGWDTH(NN).EQ.0) THEN 
         SQME=SQME/DABS( VINV-RGMASS(NN)**2 )**NDEG(NN)
      ELSE
         SQME=SQME/((VINV-RGMASS(NN)**2)**2+(RGMASS(NN)*RGWDTH(NN))**2)        
      ENDIF        
      NN=NN+1
      GOTO 1
      END

      SUBROUTINE DELsng(Nsng)      
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      COMMON/INVsng/LVINVR(10,200),RGMASS(200),RGWDTH(200),NEXTRG(200),
     & NDEG(200)
      SAVE /INVsng/
      IF(Nsng.LE.0) GOTO 30       
      DO 10 I=1,Nsng    
10    IF (LVINVR(1,I).EQ.0) GOTO 30
      DO 15 I=1,Nsng-1
        IF(NEXTRG(I).EQ.Nsng) NEXTRG(I)=NEXTRG(Nsng)
15      IF(NEXTRG(I).GT.Nsng) NEXTRG(I)=NEXTRG(I)-1
      I=Nsng
20    I1=I+1
      CALL   LVCOPY(LVINVR(1,I1) ,LVINVR(1,I))
      IF (LVINVR(1,I).EQ.0) GOTO 30      
      RGMASS(I)=RGMASS(I1)
      RGWDTH(I)=RGWDTH(I1)
      NDEG(I)  =NDEG(I1)
      NEXTRG(I)=MAX0(0,NEXTRG(I1)-1)
      I=I+1
      GOTO 20
      
30    RETURN
      END 

      SUBROUTINE VALsng(STYPE,VMASS,VWIDTH,NVDEG)
      DOUBLE PRECISION VMASS,VWIDTH
      LOGICAL STYPE
      character*2 doll
      COMMON /DOLL/ DOLL
      SAVE /DOLL/ 
30    WRITE(*,FMT='(1X,''Enter MASS value (in GeV): '''//doll//')') 
      READ(*,FMT='(G15.0)',ERR=30 ) VMASS
                 


40    IF (STYPE) THEN
       WRITE(*,FMT='(1X,''Enter WIDTH value (in GeV): '''//doll//')') 
         READ(*,FMT='(G15.0)',ERR=40) VWIDTH
      ELSE
       VWIDTH=0 
      ENDIF

      IF (STYPE.AND.(VWIDTH.NE.0)) THEN
        NVDEG=2
        RETURN
      ENDIF      
50    WRITE
     &(*,FMT='(1X,''Enter DEGREE of the pole (1 or 2): '''//doll//')')  
      READ(*,FMT='(I2)',ERR=50) NVDEG
      IF ((NVDEG.NE.1).AND.(NVDEG.NE.2)) GOTO 50
             
      RETURN
      END
      
      SUBROUTINE CHNsng(Nsng)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)      
      COMMON/INVsng/LVINVR(10,200),RGMASS(200),RGWDTH(200),NEXTRG(200),
     & NDEG(200)
      SAVE /INVsng/
      LOGICAL SPOLE
      IF(Nsng.LE.0)RETURN
      DO 10 I=1,Nsng    
10    IF (LVINVR(1,I).EQ.0) RETURN
      CALL VALsng(SPOLE(LVINVR(1,Nsng)),RGMASS(Nsng),RGWDTH(Nsng),
     & NDEG(Nsng))
      RETURN
      END
      
      
      
      SUBROUTINE ADDsng(LV,RGMASW,RGWDTW,NNDEG)

      IMPLICIT DOUBLE PRECISION (A-H,O-Z)            
      COMMON/INVsng/LVINVR(10,200),RGMASS(200),RGWDTH(200),NEXTRG(200),
     & NDEG(200)
      SAVE /INVsng/
      LOGICAL EQVECT
      DIMENSION LV(10)
       
      LASTRG=0
      Nsng=1
      
10    IF(LVINVR(1,Nsng).NE.0) THEN
        IF(  EQVECT(LV,LVINVR(1,Nsng)) ) LASTRG=Nsng
         Nsng=Nsng+1
         GOTO 10
      ENDIF
    
      IF (Nsng.GE.200) RETURN
      CALL LVCOPY(LV,LVINVR(1,Nsng))
            
      RGMASS(Nsng)=RGMASW
      RGWDTH(Nsng)=RGWDTW 
      NDEG(Nsng)=NNDEG
      NEXTRG(Nsng)=0
      IF(LASTRG.NE.0) NEXTRG(LASTRG)=Nsng
       
      RETURN
      END



      
      SUBROUTINE VIEWsng
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      CHARACTER PTXT(20),doll*2
      common /doll/ doll
      CHARACTER*25 STATUS
      CHARACTER*1 MOM(5)
      LOGICAL SPOLE
      DIMENSION LAUX(10)
      COMMON/INVsng/LVINVR(10,200),RGMASS(200),RGWDTH(200),NEXTRG(200),
     & NDEG(200)
      SAVE /INVsng/
      IF (LVINVR(1,1).EQ.0) THEN
        WRITE(*,*) '****  There are no sngularizations introduced *****'
        write(*,*)
        RETURN
      ENDIF   
      WRITE(*,1001)
1001  FORMAT(1X,'-------------------------  sngULARIZATIONS ------',
     &'----------------------------')

      WRITE(*,1002)
1002  FORMAT(1X,'|  N | INVARIANT           |   MASS     | ',
     &'   WIDTH   |DEG|    POSITION      |')

      WRITE(*,1003)
1003  FORMAT(1X,'|    |                     |   [GeV]    | ',
     &'   [GeV]   |   |                  |')

      WRITE(*,1004)
1004  FORMAT(1X,77('-') )     
      I=1
10    IF (LVINVR(1,I).EQ.0) THEN
      WRITE(*,1004)
      RETURN
      ENDIF
      
      J=1
      K=2
      PTXT(1)='(' 
      NP= LVINVR(J,I) 
15    IF (NP.LT.0) THEN
         PTXT(NEXTNN(K))='-'
         NP=-NP
      ELSE 
         PTXT(NEXTNN(K))='+'
      ENDIF  
      PTXT(NEXTNN(K))='p'
      PTXT(NEXTNN(K))=CHAR(NP+48)      
      J=J+1
      NP= LVINVR(J,I)
      IF (NP .NE.0) GOTO 15 
      
      PTXT(NEXTNN(K))=')'
      PTXT(NEXTNN(K))='*'
      PTXT(NEXTNN(K))='*'
      PTXT(NEXTNN(K))='2'
      
      DO 20 J=K,20
20    PTXT(J)=' '

      CALL SNGPOS(LVINVR(1,I) ,NDEC,NCLUST,LAUX )
 
      J=1
21    IF (LAUX(J).NE.0) THEN
         WRITE(MOM(J),FMT='(I1)') ABS(LAUX(J))
         J=J+1
         GOTO 21
      ENDIF
      J1=J
      DO 22 J=J1,5
22    MOM(J)=' '
      

      WRITE(STATUS,1010) NDEC,NCLUST,(MOM(kk),kk=1,5)
1010  FORMAT('S',I1,',L',I1,',P', 5(A1) )

      IF (SPOLE(LVINVR(1,I))) THEN      
      WRITE(*,1006)I,PTXT,RGMASS(I),RGWDTH(I),NDEG(I),STATUS
1006  FORMAT
     &(1X,'|',I3,' |',20A1,' |',1PE11.3,' |',E11.3,' |',I2,' |',A18,'|') 
      ELSE
      WRITE(*,1007)I,PTXT,RGMASS(I),NDEG(I),STATUS
1007  FORMAT
     &(1X,'|',I3,' |',20A1,' |',1PE11.3,' |',11X ,' |',I2,' |',A18,'|') 
      ENDIF
            
25    CONTINUE
      I=I+1
      IF (MOD(I,25).EQ.24) THEN
         WRITE(*,FMT='(1x,''Press ENTER to continue'''//doll//')')
         READ(*,*)         
      ENDIF
      GOTO 10
      
      END 


      SUBROUTINE NEWsng
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      CHARACTER PLIST(20),doll*2,PROCES*30
      COMMON /PROCES/ NSUB,PROCES
      common /doll/ doll
      COMMON /NSESS/ NSESS
      LOGICAL SPOLE
      DIMENSION LV(21)
      COMMON/INVsng/LVINVR(10,200),RGMASS(200),RGWDTH(200),NEXTRG(200),
     & NDEG(200)
      SAVE

      WRITE(*,FMT='(1X,''Enter particle number(s) for an invariant:'''
     &//doll//')')  
         READ(*,FMT='(20A1)') PLIST
         LPOS=1
         DO 10 K=1,20
           NCH=ICHAR(PLIST(K))-ICHAR('0')
           IF ( (1.LE.NCH).AND.(NCH.LE.NIN()+NOUT()) ) 
     &        LV(NEXTNN(LPOS))=NCH
10       CONTINUE 
         LV(LPOS)=0
         CALL ORDINV(LV)
         CALL CONINV(LV)
         IF (LV(1).EQ.0) THEN
           WRITE(*,*) 'Incorrect input'
         ELSE
           CALL VALsng(SPOLE(LV),VMASS,VWIDTH,NVDEG)            
           CALL ADDsng(LV,VMASS,VWIDTH,NVDEG)
         ENDIF           
         RETURN
      END

      SUBROUTINE WRTsng(NCHAN)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      COMMON/INVsng/LVINVR(10,200),RGMASS(200),RGWDTH(200),NEXTRG(200),
     & NDEG(200)
      LOGICAL ZERO, EQVECT
      CHARACTER*1 P(6),S(6),NP(6) 
      CHARACTER*50 TXT1,TXT2
      CHARACTER*10 TXT3
      SAVE/INVsng/

      N=1
10    IF (LVINVR(1,NEXTNN(N)).NE.0) GOTO 10   
      N=N-2
      TXT1='********  '
      TXT2=' sngULALIZATIONS ARE INTRODUCED ********'
      TXT1='=================== There are introduced '
      TXT2=' sngularizations =================='
      WRITE(NCHAN, 110) TXT1,N,TXT2 
      IF (N.EQ.0) RETURN
      TXT1= '   MASS='
      TXT2= '  WIDTH='
      TXT3= '    DEG='
      DO 50 I=1,N
         ZERO=.FALSE.
         DO 20 K=1,6
         IF (ZERO) THEN
            P(K)=' '
            S(K)=' '
            NP(K)=' '            
         ELSE 
            P(K)='p'
            IF (LVINVR(K,I).GT.0) THEN
                S(K)='+'
            ELSE
                S(K)='-'
            ENDIF        
            NP(K)=CHAR(48+IABS(LVINVR(K,I)))
         
            ZERO=(LVINVR(K+1,I).EQ.0)
         ENDIF
20       CONTINUE

50       WRITE(NCHAN,120) (S(K),P(K),NP(K), K=1,6),
     &    TXT1 , RGMASS(I), TXT2, RGWDTH(I),'    DEG=',NDEG(I)
      RETURN
      
110   FORMAT(1X,A41,I2,A35)

120   FORMAT(1X,6(A1,A1,A1),A8,1PE17.10,A8,E17.10,A8,I1)


      ENTRY RDRsng(NCHAN)
      READ(NCHAN, 110) TXT1,N,TXT2 
      DO 60 I=1,N
        READ(NCHAN,120) (S(K),P(K),NP(K), K=1,6),
     &    TXT1 , RGMASS(I), TXT2, RGWDTH(I),TXT1,NDEG(I)
      
        DO 65 K=1,6
          IF (NP(K).EQ.' ') THEN 
            LVINVR(K,I)=0
          ELSE
             LVINVR(K,I)=ICHAR(NP(K))-48
             IF(S(K).EQ.'-')  LVINVR(K,I)=-LVINVR(K,I)
         ENDIF        
65      CONTINUE  

60    CONTINUE
      LVINVR(1,N+1)=0           
      
      DO 70 I=1,N
70    NEXTRG(I)=0
       
      DO 80 I=1,N-1
      DO 90 J=I+1,N
         IF( EQVECT(LVINVR(1,I),LVINVR(1,J)) ) THEN
            NEXTRG(I)=J
            GOTO 80
          ENDIF         
90     CONTINUE
80     CONTINUE          
      RETURN
      END
      
      
