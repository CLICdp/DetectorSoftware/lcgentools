*************************************************************************
*                       User's cut function                             *
*   This function is evaluated during phase space integration for each  *
*     phase space point and is multiplyed on squared matrix element:    *
*       |M|**2  -->  |M|**2*USRCUT                                      *
*   If USRCUT=0 then the evaluation of |M|**2  is skipped               *
*   It can be used by user to introduce any kinematical cuts (or        *
*     detector sensitive function).                                     *
*************************************************************************
      DOUBLE PRECISION FUNCTION USRCUT()
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      COMMON /PVECT/ P(0:3,100)
*        P(i,N) can be used  to evaluate USRCUT
*          i=0,3 - components number for in- and out-particles momenta 
*          P(0,N) is energy.
*          P(3,N) is the projection of the particle 
*               space momentum on collision axis.
*          P(3,1) > 0 and P(3,2) < 0.
*
*        N=1,..,Nin+Nout -- is the number of in- or out-particles momenta.
*          For collisions:    N=1,2 -- in-momenta, next are out-momenta
*          For decays:        N=1  -- momentum of decayed particles, other
*                              are momenta of decay products.
*          To pick up the particle name use
*              CHARACTER FUNCTION PINF(1,N)*6

      SAVE


      USRCUT=1
      RETURN
      END


      LOGICAL FUNCTION USRMEN(ICODE)
      external VIEWsng,NEWsng,CHNsng,DELsng
      IF(ICODE.NE.0) THEN
         CALL  NCDMEN(VIEWsng,NEWsng,CHNsng,DELsng,'f_8')
      ENDIF
      USRMEN=.TRUE.
      RETURN 
      END

      SUBROUTINE I_USR(MODE)
      CALL INIsng
      RETURN
      END
     
      SUBROUTINE W_USR(MODE)
      CALL WRTsng(MODE)
      RETURN
      END

      SUBROUTINE R_USR(MODE)
      CALL RDRsng(MODE)
      RETURN
      END



      
