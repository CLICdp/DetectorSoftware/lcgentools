#ifndef __POLYNOM_
#define __POLYNOM_


#define poly struct monom *
typedef struct monom
   {  
      poly       next;
      union
      {
         long   num;
         poly   ptr;
      }  coef;  
      union
      { 
         unsigned long    power[2];
         char             tens[2*sizeof(long)];
         struct
         {  char l;
            char  g5;
            char  g[2*sizeof(long)-2];
         } spin; 
      } tail;
   } monom;
#undef poly
typedef struct monom *poly;

extern poly    garbage;
extern boolean levi;
extern poly   *contracts;
extern int    maxLength,monomLength;


extern void  newmonom(poly  * p);
extern void  delpoly(poly  * p);
extern void  delmonom(poly  * p);
extern poly  plusone(void);
extern poly  copypoly(poly  p);
extern void  sewpoly(poly  * p1,	poly  * p2);
extern void  multpolyint(poly     * p, long    i);
extern poly  multtwopoly(poly  q1, poly  q2);
extern poly  scalarmult(char  p1,	char  p2);
extern void  assignsclmult(char  p1,	char  p2,	poly      p);
extern void  deltensor(poly  * t);
extern poly  copytens(poly    t, int ln);
extern void  sewtens(poly     * t1,	poly     * t2,	int ln);
extern void  multtensint(poly     * t,	long i);
extern void  multtenspoly(poly  * t,  poly  p);
extern void (*memoryInfo) (int);
#endif
