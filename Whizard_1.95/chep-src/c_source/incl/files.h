#ifndef __FILES_
#define __FILES_


typedef struct catrec
   {
      integer      nsub_, ndiagr_;
      long      factpos, rnumpos, denompos;
   }      catrec;

    extern FILE *  menup;
    extern FILE *  menuq;
    extern FILE * diagrp;   /* file of Adiagram; */
    extern FILE * diagrq;   /* file of CSdiagram; */

    extern FILE * archiv;
    extern FILE * catalog;

    
#define MENUQ_NAME   scat("%stmp%cmenuq.ch",pathtouser,f_slash)    
#define MENUP_NAME   scat("%stmp%cmenup.ch",pathtouser,f_slash)
#define DIAGRP_NAME  scat("%stmp%cproces.tp",pathtouser,f_slash)
#define DIAGRQ_NAME  scat("%stmp%ccsproces.tp",pathtouser,f_slash)
#define ARCHIV_NAME  scat("%stmp%carchive.bt",pathtouser,f_slash)
#define CATALOG_NAME scat("%stmp%ccatalog.tp",pathtouser,f_slash)
#define ACTVARS_NAME scat("%stmp%cactvars.tp",pathtouser,f_slash)

    
    extern shortstr pathtouser, pathtocomphep,pathtoresults;
    extern shortstr _pathtouser, _pathtocomphep;
    extern char  version[26];
    extern char mdFls[4][10];


	extern void  copyfile(char *   namefrom, char *  nameto);
	extern void  wrt_menu(byte         menutype,
								 word         k,
								 char *       txt,
								 word         ndel,
								 word         ncalc,
								 word         nrest,
								 word         recpos);

	extern void  rd_menu(byte         menutype,
                     word         k,
                     char *       txt,
                     word *       ndel,
                     word *       ncalc,
                     word *       nrest,
							word *       recpos);
	extern   int f_printf(FILE *fp,char * format, ... );
	extern   int f_puts(char * s,FILE *fp);
	extern   size_t f_write(void *ptr,size_t size,size_t n,FILE *fp);
	extern   void (*diskerror) (void);

extern void newFileName(char* f_name,char * firstname,char * ext);
#define  FREAD1(d,f)   fread(&(d),sizeof(d),1,f)
#define  FWRITE1(d,f)  f_write(&(d),sizeof(d),1,f)

#endif
