/* width_13.h 
         S.Shichanin
         revision  2.0          26 Jun 1995
            (order added)
         revision  2.0.1        27 Jun 1995
*/
			

#ifndef __WIDTH_13_
#define __WIDTH_13_

extern double  m0,m1,m2,m3,m4,mm0, mm1, mm2, mm3, eps,
               xdown, xup, ydown, yup, ext_x, ext_y,
               c1,c2,c3;
extern boolean integration; 
extern int     scalee;
extern int     order[4];


extern void    decay13(integer  n_sub);
extern void    dalitzinformation(void);
extern double  sfunc(void);
extern boolean enterintegervalue(char    *txt,
                                 byte     x,
                                 byte     y,
                                 long    *result);
extern double  dgamma_dx(double  x);

#endif

