
#ifndef __TEST_WRT_
#define __TEST_WRT_

extern void  tracePrn(char * format,...);
extern void  writepoly(poly  p);
extern void  writetens(poly  p);
extern void  writespinor(poly  p);
extern void  writeexpression(poly m);

#endif
