#ifndef __PROCVAR_
#define __PROCVAR_

typedef char varname[nvarhighlimit][7];

extern varname *varnamesptr;
extern setofbyte varsofprocess, funcofprocess;



extern void  initvarnames(void);
extern void  getprocessvar(word  nsub,setofbyte ind_var,setofbyte  fnc_var);

#endif
