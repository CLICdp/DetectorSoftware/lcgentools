/*-------------------------------------------------*
 * C -version by Victor Edneral, Moscow University *
 *        The latest revision of 23.01.1994        *
 *-------------------------------------------------*/

#ifndef __GRAF_
#define __GRAF_

  typedef struct{double  xmin, xmax, ymin, ymax;} grafminmaxstruct;
  extern double  xscale, yscale;
  extern grafminmaxstruct  grafminmax;

  extern void doubleToStr(double x, double dx, char *s);
  extern double bscx(int x);
  extern double bscy(int x);
  extern int    scx(double x);
  extern int    scy(double x);
  extern void   gaxes(int scale_,char * upstr,char * xstr,char * ystr);
  extern void   gcurve(realseq  rsec,boolean  line);
  extern void   graf(realseq   rsec,boolean line, char * upstr,
                       char * xstr, char * ystr,char * downstr);
  extern void   gpixel(double  x,  double  y);
  extern void   plot_(realseq rsec, char * upstr, char * xstr,char * ystr);
  extern void   plot_1(boolean  sc, realseq  rsec, boolean line, char*  upstr, 
                 char * xstr, char * ystr);                                     		        
  extern void   plot_2(boolean sc,realseq  rsec, char * upstr,
		      char * xstr,char * ystr);  
#endif

