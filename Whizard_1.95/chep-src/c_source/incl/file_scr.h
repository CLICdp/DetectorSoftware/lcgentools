#ifndef __FILE_SCR_
#define __FILE_SCR_

typedef struct linerec
   {
		struct linerec * next;
		struct linerec * pred;
		char         line[STRSIZ];
	}  linerec;
typedef struct linerec * linelist;


typedef struct table
	{  char   mdlName[30];
		char   headln[80];
		char   format[STRSIZ];
		linelist   strings     ;
	}  table;


extern table modelTab[4];

#define vars_tab modelTab[0]
#define func_tab modelTab[1]
#define prtcls_tab modelTab[2]
#define lgrng_tab modelTab[3]


extern char errorText[256];

	extern int   fgets0 (char * c,int n, FILE * fp);
	extern void  showtext (int x1, int y1, int x2, int y2,
        char* headline,	FILE * fileptr);
        extern void  readtable0(table * tab, FILE* f);
        extern void  writetable0(table * tab,FILE* f);

	extern void  readtable(table * tab, char* fname);
	extern void  writetable(table * tab,char* fname);
	extern void  cleartab(table * tab);

#endif
