#ifndef __GLOBAL_
#define __GLOBAL_

typedef enum  {graphic=0,structfun,quadra,
		       conslow,memory} optionstp;

extern shortstr processch, hadr1ch, hadr2ch, sqrtsch, limpch;
extern integer  graphdriver, graphmode;

extern optionstp options;
extern char  fortname[3];
extern word  totdiag_f, totdiag_sq, deldiag_f,
             deldiag_sq, subproc_f, subproc_sq, calcdiag_sq;

extern char  modelmenu[STRSIZ];
extern int  maxmodel;
extern char  exitmenu[STRSIZ];

extern int  nsub, ndiagr, n_model;
extern int  nin, nout, n_x;   /* Number of X-particles */
extern int  errorcode;

#endif
