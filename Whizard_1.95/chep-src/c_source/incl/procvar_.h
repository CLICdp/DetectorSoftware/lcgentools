#ifndef __PROCVAR__
#define __PROCVAR__

#define VARFILE_NAME scat("%stmp%cdata.ch",pathtouser,f_slash)

typedef struct
   {
      varlist   sourse;
      char      alias[10];
      double      tmpvalue;
      boolean   used, freezen;
   }  singlevardescription;

typedef singlevardescription varsdescription[nvarhighlimit];

extern varsdescription *vararr;

extern void  initvararray(integer  nsub_);
extern void  calcfunctions(integer * errcode);
extern void  makevarfile(void);


#endif

