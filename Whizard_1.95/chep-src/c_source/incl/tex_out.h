/*-------------------------------------------------*
 * C -version by Victor Edneral, Moscow University *
 *        The latest revision of 25.10.1992        *
 *-------------------------------------------------*/

#ifndef __TEX_OUT_
#define __TEX_OUT_

#define scriptsize 12

extern void plottexstart(char *upstr);
extern void plottexfinish(char *buff);

#endif

