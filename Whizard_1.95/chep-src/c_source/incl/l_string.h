#ifndef __L_STRING_
#define __L_STRING_

#define buffsize 1290
#define six  "      "
#define six_ "     ."
#define eq   '='


typedef struct longstr
   {
      integer  len;
      char     txt[buffsize];
   }  longstr;

typedef longstr *longstrptr;

 extern FILE * fortFile;

 extern void  fortwriter(char * name,varptr fortformula);
 extern void  initConsts(void);
 extern void  putnames(void);
 extern void  write_const(void);
 extern void  writelongstr(char * name,longstrptr   longs);
 extern void  addstring(longstrptr   longs, char * s);
 extern void  initdegnames(void);
 extern void  cleardegnames(void);
 extern int   constcount;
 extern int   degnamecount;
 extern int   tmpNameMax;
 extern int   tmpNameNum;
#endif
