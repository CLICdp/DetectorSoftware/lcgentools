/*
 * TPTCMAC.H - Macro Header for use with Turbo Pascal --> C Translator
 *
 * (C) 1986 S.H.Smith (rev. 24-Mar-88)
 * Modifyed by V.Edneral, the last revision was taken place on 01-Mar-1993
 */

#ifndef __TPTCMAC_
#define __TPTCMAC_

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include <stdarg.h>
typedef void * pointer;

typedef unsigned char  byte;
typedef unsigned short word;
typedef          short integer;
typedef char           dirstr[68];
typedef char           namestr[9];
typedef char           extstr[5];



extern integer chround(double x);
extern long chlround(double x);
extern void strinsert(char *subst, char *s, integer from /* from>=1 */);
/*It deletes from s-string num symbols from p>=1 to p+num-1 */

extern int intVal(char* str, int* err);
extern double realVal(char* str, int* err);

extern void trim(char * p);

#define chr(n)           (n)
#define ord(c)           (c)

#define sqr(x)           ((x)*(x))
#define strdelete(s,p,num) strcpy((s)+(p)-1,(s)+(p)+(num)-1)
#define vali(s,res,code) *(code)=0, *(res)=(integer)atoi(s)
/*It transfers string s to integer res, if the operation is ok code is 0*/
#define valw(s,res,code) *(code)=0, *(res)=(word)atoi(s)
/*It transfers string s to word res, if the operation is ok code is 0*/
#define vald(s,res,code) *(code)=0, *(res)=(double)atof(s)
/*It transfers string s to double res, if the operation is ok code is 0*/
#define vall(s,res,code) *(code)=0, *(res)=(long)atol(s)
/*It transfers string s to long res, if the operation is ok code is 0*/
#define lvcpy(d,s)      memcpy((char *)d,(char *)s,(size_t)sizeof(d))
/*It copies SIZEOF(dest) bytes of src to the dest address. */

/* Definition of constans */

typedef int boolean ;


#ifndef FALSE
#define FALSE   0
#endif
#ifndef TRUE
#define TRUE    1
#endif


/*
 * file access support (by V.Edneral)
 */

extern integer ioresult;


extern long     filesize(FILE * f);

#ifndef  tptccomp
extern boolean pseudo_g;

#endif


/*
 *   By V.Edneral
 *
 *   int setof(int a,int b,...,_E)
 *      construct and return a set of the specified character values
 *
 *   boolean inset(int ex,int setrec)
 *      predicate returns true if expression ex is a member of
 *      the set parameter
 *   (It is suppoused enum type setrec takes two bytes here; a,b,ex < 16)
 *
 *   setofbyte setofb(int a,int b,...,_E)
 *   boolean insetb(word ex, setofbyte setrec)
 *   Here a,b,ex<256; setofbyte=word[16].
 *
 *   setofb_cpy( setofbyte dest, setofbyte source)
 *       for setofbyte copying
 *   setofb_uni( setofbyte a, setofbyte b)
 *       for unification
 *   setofb_aun( setofbyte a, setofbyte b)
 *       for antiunification
 *   setofb_its( setofbyte a, setofbyte b)
 *       for intersection
 *   setofb_eql( setofbyte a, setofbyte b)
 *       for comparision
 *   setofb_eq0( setofbyte a)
 *       for comparision with an empty set
 *   are avaluable
 *
 */

#define _E  (-1)        /* end of set marker */
#define UpTo  (-2)        /* UpTo is analog of .. in Pascal */
#define setrec enum

typedef word setofbyte[16];

/* We suppouse here the length of enum variable is 2 bytes,
   i.e. the length of enum type includes 16 objects as maximum.
   Setofbyte is 32 bytes length for set of byte enum type.
*/


extern int setof( int i, ...);
extern boolean inset(int a,int sp);
extern word  * setofb( int i, ...);
extern word  * setofb_add1(setofbyte a, int i);
extern boolean insetb(word a,setofbyte sp);
extern void    setofb_zero(setofbyte sp);
extern void    setofb_cpy( setofbyte dest, setofbyte source);
extern word  * setofb_uni( setofbyte a, setofbyte b);
extern word  * setofb_aun( setofbyte a, setofbyte b);
extern word  * setofb_its( setofbyte a, setofbyte b);
extern boolean setofb_eql( setofbyte a, setofbyte b);
extern boolean setofb_eq0( setofbyte a);
extern void    setofb_dpl( setofbyte a);


/* String's  department */

#define ctos(ch) scat("%c",ch)  /* character to string conversion */


/* char *copy(char *str,integer from,integer len):
 * copy len bytes from the dynamic string dstr
 * starting at position from.
 *
 * String/character concatenation function
 * char *scat(char *control, ...):
 * This function takes a sprintf-like control string, a variable number of
 * parameters, and returns a pointer a static location where the processed
 * string is to be stored.
 *
 * void sbld(char *dest,char *control, ...):
 * string build - like scat, sprintf
 , but will not over-write any
 *                input parameters.
 *
 * integer spos(char *str1,char *str2):
 * returns index of first occurence of str1 within str2;
 *    1=first char of str2
 *    0=nomatch
 *
 * integer cpos(char c,char *str2):
 * returns index of first occurence of c within str2;
 *    1=first char of str2
 *    0=nomatch
 */

extern char *copy(char *str,integer from,integer len);
extern char *scat(char *control, ...);
extern void sbld(char *dest,char *control, ...);
extern integer spos(char *str1,char *str2);
extern integer cpos(char c,char *str2);
extern void doub_str (char * s, double z );

#endif
