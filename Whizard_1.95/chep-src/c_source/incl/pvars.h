#ifndef __PVARS_
#define __PVARS_

typedef struct polyvars
   {
      byte        nvar;   /* <============== Must be initialized to 0  */
      struct
      {
         unsigned long maxdeg,  zerodeg;
         int  num;
         int  wordpos;
         char  name[7];   /*  used by Editor  */
      }    vars[50];
   }          polyvars;

extern polyvars *vardef, *vardef_s;


extern void  addvar(char  * varname,  byte    deg);
extern void  closevars(void);
extern void  unite_vardef(polyvars *vardef_s,polyvars *vardef);
extern byte  calcvarpos(char  * s);

#endif
