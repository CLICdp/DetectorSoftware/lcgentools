#ifndef __PREPDIAG_
#define __PREPDIAG_

typedef setofbyte indset; /*  1..3*maxVert */

typedef char momsum[MAXINOUT+1];

typedef struct
   {
      byte         len;
      boolean      lprtcl;
      boolean      g5;
		byte         vv[2 * maxvert], ll[2 * maxvert],
						 intln[2 * maxvert],intln2[2 * maxvert] ;
		char         invrt[2*maxvert];
   }  fermloopstp;


typedef	 struct
   {
      algvertptr   lgrnptr;
      arr4byte     subst;
      boolean      r_vert;
	}   vertexhlp;

typedef  struct{  byte  vrt1, ln1, vrt2, ln2;} linkhlp;

extern  vertexhlp  vertexes[2 * maxvert];
extern  linkhlp massindpos[5*maxvert];
extern fermloopstp  fermloops[maxvert];
extern indset   setmassindex, setmassindex0;
extern byte     nloop;
extern byte     fermmap[2 * maxvert];
extern char     inoutmasses[MAXINOUT][7];
extern momsum   momdep[3 * maxvert];

 extern void  preperdiagram(void);
 extern void  findinternalmoments(boolean  indep);
 extern void  coloringvcs(hlpcsptr  currentghst);
 extern void  attachvertexes(void);
 extern void findReversVert(void);

#endif
