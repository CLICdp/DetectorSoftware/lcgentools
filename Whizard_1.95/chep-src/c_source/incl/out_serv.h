#ifndef __OUT_SERV_
#define __OUT_SERV_

#include "files.h"

extern void  readvars(char landuage);
extern void  writeF(char * format,...);
extern void  outFileOpen(char * fname);
extern void  outFileClose(void);
extern void  picture(int nsub,int ndiagr,char comment);
extern void  jumppolynom(void);
extern void  rewritepolynom(void);
extern void  findPrtclNum (char * procName,int * prtclNum);
extern void  emitconvlow(int * prtclNum, char  language);
extern void  writeLabel(char  comment);
extern void DiagramToOutFile(vcsect * vcs, int label,char comment);  
extern void  makeOutput(  void (*startOutput)(int,int*,int),
                          void (*diagramOutput)(vcsect*, catrec* ),         
                          void (*endOutput)(int*)              
                       );
                       
extern void seekArchiv(long n);
extern int readNN(void);                       
                                                  
#endif
