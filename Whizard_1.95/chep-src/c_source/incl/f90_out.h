#ifndef F90_MAXDIAG
#define F90_MAXDIAG 100
#endif

#ifndef __FORT90_OUT_
#define __FORT90_OUT_

extern void  fort90prg(boolean precision,
		       boolean include_pictures);

#endif
