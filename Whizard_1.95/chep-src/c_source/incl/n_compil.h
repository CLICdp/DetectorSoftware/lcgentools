#ifndef __N_COMPIL_
#define __N_COMPIL_

#define prgcodeptr struct prgcoderec *
typedef struct prgcoderec
   {
      prgcodeptr   next;
      varptr       totn, totd, rnum;
      byte         denorno;
      byte         deg[2];
      varptr       width[2],mass[2],den[2];
   }  prgcoderec;
#undef prgcodeptr

typedef struct prgcoderec *prgcodeptr;
typedef double *realptr;

#define canalptr struct canal *
typedef struct canal
   {
      canalptr     next;
      char         prtclnames[2][6];
      realptr      prtclmasses[2];
      prgcodeptr   codeptr;
      double         width;
   }  canal;
#undef canalptr

typedef struct canal *canalptr;

extern shortstr  processstr;
extern char      pname[4][6];
extern realptr   pmass[4];
extern setofbyte setfun;
extern double      zero;

extern void  compileprocess(byte  nsub, prgcodeptr * prg);
extern void  findprocname(integer  n_sub,char  * procname,integer * errcd);
extern void  findprtcls(char * procname);
extern void  loadvars(byte nsub);
extern void  compileall(canalptr * allcanal,	integer  * errcd);

#endif


