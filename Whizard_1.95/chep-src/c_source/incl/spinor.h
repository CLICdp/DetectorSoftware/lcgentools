
#ifndef __SPINOR_
#define __SPINOR_


extern poly multtwospin(poly     t1,
                        poly     t2,
                        boolean  forspur);

extern poly  calcspur(poly   spnr);

extern void  multspintens(poly * spn,
                          poly * tns);

extern int spinLength;
#endif

