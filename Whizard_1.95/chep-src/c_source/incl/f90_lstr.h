#ifndef __F90L_STRING_
#define __F90L_STRING_

#define buffsize 300
#define six  "      "
#define six_ "     ."
#define eq   '='


typedef struct longstr {
  integer  len;
  char     txt[buffsize];
}  longstr;

typedef longstr *longstrptr;

extern FILE* f90file;
extern void  fout(int indent, char* format, ... );

extern void  f90writer(char* name, varptr fortformula);
extern void  f90initConsts(void);
extern void  f90putnames(void);
extern void  f90write_const(void);
extern void  f90write_str(char* name, longstrptr longs);
extern void  f90addstring(longstrptr longs, char* s);
extern void  f90initdegnames(void);
extern void  f90cleardegnames(void);
extern int   f90constcount;
extern int   f90degnamecount;
extern int   f90tmpNameMax;
extern int   f90tmpNameNum;
#endif

