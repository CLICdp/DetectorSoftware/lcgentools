#ifndef __CHESS_
#define __CHESS_

typedef setofbyte indvertset;   /*  1..3*maxVert */


typedef struct
   {
      byte   weit, g5, vlnc;
      indvertset   ind;
		byte         link[2*maxvert];
	}  vertinfostr ;

extern vertinfostr	vertinfo[2 * maxvert];
extern byte  n_vrt;
extern byte  prgcode[2 * maxvert][2];


extern void  makeprgcode(void);

#endif
