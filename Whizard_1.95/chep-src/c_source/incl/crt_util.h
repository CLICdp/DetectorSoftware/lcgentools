#ifndef __CRT_UTIL_
#define __CRT_UTIL_

#include "crt.h"

extern void  menu0 (int col,int row,char* label, char* filename,
void (**funcKey)(int i) , char** funcKeyMess, pointer *  hscr, int* kk);

extern int   mess_y_n(int    x1,int    y1,	char  * txtstr);
extern void  messanykey(int    x1,	int    y1, char  * txtstr);
extern int   yesnokey(void);
extern int   informline(int curent,int total);
extern int   str_redact(char * txt, int  npos);
extern int   correctDouble(int x,int y,char* txt,double * var,int clear);
extern int   correctLong(int x,int y,char* txt,long   * var,int clear);
extern int   correctInt(int x,int y,char* txt,int    * var,int clear);

#endif
