#ifndef __FORT_OUT_
#define __FORT_OUT_

extern void  fortprg(boolean      precision,
								boolean      include_pictures,
								char       * globalname);

#endif
