#ifndef __SCREEN_
#define __SCREEN_

#define  helpabsent      0
#define  helpreturn      1000000L

extern byte    menulevel;
extern boolean menuinmenu;



extern void  smalllabel(void);
extern void  menuhelp(void);
extern void  modelinfo(void);
extern void  processinfo(void);
extern void  diagramsinfo(void);
extern void  sq_diagramsinfo(void);
extern void  viewsqdiagr(void);
extern void  sqdiagrmenu(void);
extern void  viewfeyndiag(boolean  del_mode);
extern void  viewresults(void);
extern void  showheap(void);
extern void  menu(byte      col,
		  byte      row,
                  byte      height,
                  char    * filename,
                  long   hlp_num,
                  integer * kk);
                  
extern void menu1(int col,int row,char * name,  char* menustr,
            char * help, void*  hscr,int * kk);

extern void menu_f(int col,int row,char * name, char* f_name,
            char * help, void*  hscr,int * kk);

           						
extern void editModel(boolean edit);
extern void writeModelFiles( int l);

#endif
