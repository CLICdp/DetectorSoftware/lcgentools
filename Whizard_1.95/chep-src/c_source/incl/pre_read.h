#ifndef __PRE_READ_
#define __PRE_READ_

#define idntmaxnmb 20

typedef setrec {abracadabra} typeindlist;    /* [1..9], used by Editor  */

#define preres struct preresrecord *
typedef struct preresrecord
   {
      preres       next;
      boolean      free;
      byte         tp;
      long      num;
      word         maxp;      /*  How many pulses: used by Editor   */
      word         degp;
      boolean      g5;
      word         maxg;
      typeindlist  indlist;
      word         varsdeg[idntmaxnmb];
   }  preresrecord;
#undef preres
typedef struct preresrecord *preres;

extern preres pregarbage;

extern void  clearpregarbage(void);
extern pointer bact(char  ch,	pointer mm1, pointer  mm2);
extern pointer bactF(char  ch,  pointer    mm1,  pointer    mm2);
extern pointer uact(char     * ch,  pointer    mm);
extern pointer rd(char  * s);

#endif
