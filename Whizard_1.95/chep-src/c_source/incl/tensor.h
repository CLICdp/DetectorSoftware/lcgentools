#ifndef __TENSOR_
#define __TENSOR_

extern boolean dellevi; 

extern poly  multtwotens(poly  t1,
                         poly  t2);

extern int tensLength, maxIndex;
#endif
