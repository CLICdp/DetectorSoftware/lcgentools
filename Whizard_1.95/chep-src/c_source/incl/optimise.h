#ifndef __OPTIMISE_
#define __OPTIMISE_

#define infoptr struct inforec *
#define varptr struct var_rec *
typedef struct var_rec
   {
      varptr      next;
      char        sgn;
      infoptr     coef;
      char        vars[STRSIZ];
   }  var_rec;
#undef varptr
#undef infoptr
typedef struct var_rec *varptr;


#define infoptr struct inforec *
typedef struct inforec
   {
      infoptr    next;
      word       count;
      char       name[16];
      enum{numb,expr,rnumb} consttype;
      varptr     const_;
		long    ival;
		double     rval;
   }  inforec;
#undef infoptr
typedef struct inforec *infoptr;


typedef pointer ( * smplemit)(varptr ex);
typedef pointer ( * vfact   )(char  varchar, byte deg,
											 pointer pmult, pointer psum);
typedef pointer ( * cfact   )(infoptr c, pointer pmult, pointer psum);
	
extern void  initinfo(void);
extern void  readpolynom(varptr  * expr_);
extern pointer emitexpr(varptr    ex,smplemit  smplemitfun,
	vfact     vfactfun,  cfact     cfactfun);
extern boolean equalexpr(varptr  v1,	varptr  v2);

#define minvarrec ((word)(sizeof(struct var_rec) + 1 - STRSIZ))

extern infoptr info;
extern byte    cutlevel;

#endif
