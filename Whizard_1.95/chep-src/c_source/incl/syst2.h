#ifndef __SYST2_
#define __SYST2_

#ifndef STRSIZ
#define STRSIZ 512
#endif

#define blocksize  8192

typedef char shortstr[81];

typedef struct  marktp { pointer blk_; word pos_;} marktp;

#ifndef MAX
#define MAX(a,b)        (((a) > (b)) ? (a) : (b))
#endif
#ifndef MIN
#define MIN(a,b)        (((a) < (b)) ? (a) : (b))
#endif


#ifndef FALSE
#define FALSE   0
#endif
#ifndef TRUE
#define TRUE    1  
#endif



extern char   * funstr(long  num);
extern void     revers(pointer  * list);
extern pointer  getmem(size_t size);
extern pointer  getmem_(word  size);
extern void     release_(marktp * mrk);
extern void     mark_(marktp * mrk);
extern int      blockrest(int size); 
extern void     lShift (char * s,int l);
extern  void (*memerror) (void);


extern  long usedmemory;

#endif
