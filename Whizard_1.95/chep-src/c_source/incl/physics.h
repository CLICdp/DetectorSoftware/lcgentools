#ifndef __PHYSICS_
#define __PHYSICS_

#define MAXINOUT 6    /*  maximum number of in+out particles   */
#define ldiagram (2 * MAXINOUT - 3)
/*  maximum number of particles in diagram  */
#define maxsp ((MAXINOUT*(MAXINOUT-1)/2))
#define maxvert (MAXINOUT - 2)
/*  maximal # of verteces in amplitude           */
#define nullvert 253    /*# of next vert for  unused edge              */
#define maxvalence 4    /*  only up to 4-vertices is allowed             */

#define strongconst "GG"
#define nvarhighlimit 250

typedef byte   arr4byte[4];

#define algvertptr struct algvert *
typedef struct algvert
   {
      algvertptr   next;
      arr4byte     fields,perm;
		char *       comcoef;
		char *       description;
   }  algvert;
#undef algvertptr
typedef struct algvert *algvertptr;

#define varlist struct varrec *
typedef struct varrec
   {
      varlist      next;
      char         varname[7];
      double         varvalue;
		char *       comment;
		char *       func;
   }  varrec;
#undef varlist
typedef struct varrec *varlist;

typedef char adiagram[ldiagram];

typedef byte permut[MAXINOUT];

typedef struct csdiagram
   {
      adiagram     dgrm1;
      adiagram     dgrm2;
      permut       lnk;
      byte         mult;
      word         del;
      char     status;   /* -2-outOfMemory,-1-deleted,
                                 0-Rest, 1-calculated,2-Zero  */
   }  csdiagram;

typedef char shortname[6];

#define decaylink struct modeofdecay *
typedef struct modeofdecay
   {
      char     part[3];
      decaylink    link;
   }  modeofdecay;
#undef decaylink
typedef struct modeofdecay *decaylink;

typedef struct whohow
   {  byte         who, how;
   }  whohow[40];

typedef struct hadron
   {
      char         name[8];
      byte         how;
      byte         parton[20];
   }  hadron;
/*  FileChar=File of Char; */

typedef char ptcltype;

/*typedef char momentumtype;*/

typedef struct vertlink
   {  /* # of vert, # of edge in vert (slot) */
		int			vno, edno;
   }  vertlink;

typedef enum {inp, intrp, independentmom } lineprop;
/*  properties of propagators  */

typedef struct edgeinvert
   {
		int			 lorentz;
		int          moment;
      lineprop     prop;
      ptcltype     partcl;
      vertlink     nextvert;
   }  edgeinvert;

typedef edgeinvert vert0[maxvalence];

typedef struct vcsect
   {
      byte         sizel, sizet;
      integer      symnum, symdenum, clrnum, clrdenum;
      byte          /* 1..4 */ valence[2 * maxvert];
      vert0        vertlist[2 * maxvert];
   }  vcsect;

typedef shortname prtclsarray[MAXINOUT];

typedef struct prtcl_base
   {
      shortname    name;
      int         anti, spin;
      double         mass;
      char         massidnt[7], imassidnt[7];
      char     cdim;
      char         hlp;      
      decaylink    top;
   }  prtcl_base;

/* * * * * * * * * * * * * * * * * * * * * * */
/*  global  */
extern boolean    gg_exist;
extern algvertptr lgrgn;
extern varlist    modelvars;
extern prtcl_base prtclbase[127];
extern vcsect     vcs;
extern byte       nparticles;   /*  Number particles in model */
extern hadron     hadr1, hadr2;
extern whohow     inclp, liminsp;
extern double       sqrts, missingmass;
extern byte       nmodelvar;

/* * * * * * * * * * * * * * * * * * * * * * *  */

extern boolean pseudop(byte       np);
extern boolean fermionp(ptcltype  p);
extern boolean bosonp(ptcltype    p);
extern boolean vectorp(byte       p);
extern boolean zeromass(byte      p);
extern boolean photonp(byte       p);
extern boolean ghostp(byte        p);
extern int    ghostmother(int     j);
extern boolean gaugep(int         j);
extern void  addprtcl(whohow      p_list, byte        n);
extern void  delprtcl(whohow      p_list, byte        n);
extern void  nilprtcl(whohow      p_list);
extern void  addlim(whohow       p_list,  byte         j, byte         k);
extern boolean inhadr(hadron *     hadr,  byte         np);
extern void  locateinbase(char *   name,  byte *   number);
extern void  getprtcls(char *      txt,  prtclsarray pnames);

#endif

