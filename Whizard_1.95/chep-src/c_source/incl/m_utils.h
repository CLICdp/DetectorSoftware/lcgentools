#ifndef __M_UTILS_
#define __M_UTILS_

extern boolean  clearresults(void);
extern void  fillModelMenu(void);
extern boolean  deletemodel(int n);
extern boolean  makenewmodel(void);
extern boolean  continuetest(void);
extern void     changeexitmenu(void);
extern void     new_user(void);
extern void     clear_tmp(void);

#endif
