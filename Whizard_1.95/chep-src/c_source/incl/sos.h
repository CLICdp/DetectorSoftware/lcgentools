#ifndef __SOS_
#define __SOS_


extern void  save_sos(char  ercode);

/* ExitLevel=2 == GoTo Restart :    OutOfMemory       */ 
/* ExitLevel=3 == GoTo Edit Model:  ErrorInLagrangian */ 
   
                                                  
extern void  restoreent(int  * nsub,
                        int  * ndiagr,
                        int  * n_model,
                        char * exitlevel);


extern void  saveent(int   nsub,
                     int   ndiagr,
                     int   n_model,
                     char  exitlevel);

#endif
