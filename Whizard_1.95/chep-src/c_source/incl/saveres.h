#ifndef __SAVERES_
#define __SAVERES_

extern poly    rnum;
extern poly    factn, factd;
extern poly    denr[2 * maxvert - 2];
extern byte    denrdeg[2 * maxvert - 2];
extern byte    denrwidth[2 * maxvert - 2][2];
extern byte    denrno;
extern byte    monsize;

extern void  saveanaliticresult(void);
#endif
