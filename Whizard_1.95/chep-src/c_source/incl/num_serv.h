#ifndef __NUM_SERV_
#define __NUM_SERV_

typedef double  (*r_func)(void);
  extern void writetable1(realseq* rr,char* upstr,char* leftstr,char* rightstr);
  extern void errormessage(void);
  extern byte scale_c(realseq  seq);
  extern boolean changedata(void);
  extern void  paramdependence(r_func ff, char * procname,char * resultname);
#endif

