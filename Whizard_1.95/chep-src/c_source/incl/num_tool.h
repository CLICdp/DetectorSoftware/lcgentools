#ifndef __NUM_TOOL_
#define __NUM_TOOL_

#define realseq struct realseqrec *
typedef struct realseqrec
   {
      realseq      next;
      double         x, y,x1,y1;
   }  realseqrec;
#undef realseq

typedef realseqrec *realseq;

extern integer err_code;   /*  0: OK,  1: Division by zero, 2:Stack Check,
                              3: Kinematic Check; 4: UserHalt   */
extern word  numberfuncalls;

typedef double (* r_r_fun)(double x);
extern void  disposerealseq(realseq * r);
extern void  integral_(double  xmin, double  xmax, double  eps,
                 r_r_fun   f, double    * intvalue, realseq * p_beg);
extern void  integral(double xmin, double  xmax, double eps, r_r_fun  f,
		 double * intvalue);
extern double  lambda( double  s, double  m1, double  m2);
extern void  calcconstants(void);
extern double  matrixelement(prgcodeptr   prg);
extern int  sp_pos(int i,int j);
#endif


