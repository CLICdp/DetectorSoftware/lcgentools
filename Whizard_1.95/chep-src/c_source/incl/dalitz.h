/*      dalitz.h
        S.Shichanin
        revision 1.1    Jun 26 1995
*/
#ifndef __DALITZ_
#define __DALITZ_

#define rsqrt(x) ( ((x)>0.0) ? sqrt(x) : 0.0 )

extern double asx1(double x);
extern double asx2(double x);
extern double asy1(double x);
extern double asy2(double x);
extern double sy1(double x);
extern double sy2(double x);
extern void   setlimits(void);
extern void   dalitz_plot(word  ntotal, word nx, word ny);
extern void   integrated_on_m34(void);
extern int  correctDoubleS(int x,int y,char* txt,double * var);
extern void  gpixel(double  x,  double  y);
#endif
