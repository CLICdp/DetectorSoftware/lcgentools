#ifndef __DIAPRINS_
#define __DIAPRINS_

extern void writeTextDiagram(vcsect* diagr,int label,char comment,FILE* outfile);


extern void  pseudo_pict(csdiagram * csd, boolean     debug1,
                                          boolean     sqrd,
                                          boolean   * moveup);

#endif
