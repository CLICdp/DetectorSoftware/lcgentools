#include "tptcmac.h"
#include "syst2.h"
#include "crt_util.h"
#include "file_scr.h"

#include "help.h"

char pathtohelp[256]="";

int show_help(char * fname)
{
   char format[81];
   char f_name[STRSIZ];
   FILE* f;

   int i,j1,j2;
   int z[4];

   sprintf(f_name,"%s%s%s",pathtohelp,fname,".txt");

   f=fopen(f_name,"r"); 
   if (f == NULL) return 0;
   fgets0(format,80,f);


	j2=0;
	for (i=0;i<4;i++)
	{
		j1=j2;    while ( !isdigit(format[j1])) j1++;
		j2=j1+1;	 while ( isdigit(format[j2]) ) j2++;
		format[j2]=0;
		z[i]=atoi(format+j1);
	 }
	z[2]=z[2]+z[0]+1;
	z[3]=z[3]+z[1]+1;

	showtext (z[0],z[1],z[2],z[3]," Help ",f);
	fclose(f);
	return 1;
}



void showhelp(long  num)
{
  char fname[STRSIZ], zerotxt[20];
  long L=100000L;
  int i=0;
  
  while (L > num) { L=L/10; zerotxt[i]='0';i++;}
  zerotxt[i]=0;
  sprintf(fname,"h_%s%ld",zerotxt,num);			 
  if(!show_help(fname)) 
  {  
     messanykey(10,10,"Help is absent$");
     return;
  }  
}

