#include "tptcmac.h"
#include "syst2.h"
#include "global.h"
#include "physics.h"
#include "ghosts.h"

#include "prepdiag.h"

 vertexhlp  vertexes[2 * maxvert];
 linkhlp massindpos[5*maxvert];
 fermloopstp  fermloops[maxvert];
 indset   setmassindex;
 indset   setmassindex0;
 byte     nloop;
 byte     fermmap[2 * maxvert];
 char     inoutmasses[MAXINOUT][7];
 momsum   momdep[3 * maxvert];


void  coloringvcs(hlpcsptr currentghst)
{int       i, j;

   indset   setDel;   
   setofb_zero(setDel);
   for (i = 0; i < vcs.sizet; i++)
   for (j = 0; j < vcs.valence[i]; j++)
   { 
      vcs.vertlist[i][j].partcl += currentghst->hlpcs[i][j];
      switch (currentghst->hlpcs[i][j]) 
      {
         case ghostmark:   
         case antighostmark:
         case sbosonmark:    vcs.vertlist[i][j].lorentz = 0;
             break;
         case tbosonmark:   
         setofb_cpy(setDel,
                    setofb_uni(setDel, setofb(vcs.vertlist[i][j].lorentz,_E))
                   ); 
      } 
   }
   setofb_cpy(setmassindex,setofb_aun(setmassindex0,setDel)); 
    
} 

void  attachvertexes(void)
{ int   v, l; 
  arr4byte  vert; 

   for (v = 0; v < vcs.sizet; v++) 
   { 
      for (l = 0; l < vcs.valence[v]; l++)  vert[l] = vcs.vertlist[v][l].partcl;      
      if (vcs.valence[v] == 3) vert[3] = 0;
      vertinlgr(vert,v+1,vertexes[v].subst,&vertexes[v].lgrnptr); 
   }
}

void findReversVert(void)
{
	byte v,l,lp,k,vin,lin,fin,fout,fl_len;
   for(v=1;v<=vcs.sizet;v++) vertexes[v-1].r_vert=FALSE;
   for(lp=1;lp<=nloop;lp++)
   {  fl_len=fermloops[lp-1].len;
      for(k=1;k<=fl_len;k++)
      {
         l=fermloops[lp-1].ll[k-1];
         v=fermloops[lp-1].vv[k-1];
         if (k>1) {vin=fermloops[lp-1].vv[k-2];
                   lin=fermloops[lp-1].ll[k-2];
                  }
            else  {vin=fermloops[lp-1].vv[fl_len-1];
                   lin=fermloops[lp-1].ll[fl_len-1];
                  }
         lin=vcs.vertlist[vin-1][lin-1].nextvert.edno;
         fin=1 ; while (lin !=vertexes[v-1].subst[fin-1]) fin++;
         fout=1; while (l   !=vertexes[v-1].subst[fout-1])fout++;
         vertexes[v-1].r_vert=(fin>fout);
      }
   }
}



static void  findinoutmasses(void)
{byte  v, l, vln;

   for (v = 1; v <= vcs.sizet; v++) 
   { 
      vln = vcs.valence[v-1]; 
      for (l = 1; l <= vln; l++) 
         if (vcs.vertlist[v-1][l-1].moment > 0 && 
             vcs.vertlist[v-1][l-1].moment <= nin + nout) 
            strcpy(inoutmasses[vcs.vertlist[v-1][l-1].moment-1],
                   prtclbase[vcs.vertlist[v-1][l-1].partcl-1].massidnt); 
   }
} 



static void  findmassindex(void)
{byte  v, l; 

   setofb_zero(setmassindex0); 
   for (v = 1; v <= vcs.sizet; v++)
   for (l = 1; l <= vcs.valence[v-1]; l++) 
   if (vcs.vertlist[v-1][l-1].lorentz != 0 && 
       vcs.vertlist[v-1][l-1].nextvert.vno < v && 
       !photonp(vcs.vertlist[v-1][l-1].partcl) && 
       !gaugep(vcs.vertlist[v-1][l-1].partcl) ) 
   { 
      setofb_cpy(setmassindex0,
         setofb_uni(setmassindex0,
            setofb(vcs.vertlist[v-1][l-1].lorentz,_E))); 
      massindpos[vcs.vertlist[v-1][l-1].lorentz-1].vrt1 = v;
      massindpos[vcs.vertlist[v-1][l-1].lorentz-1].ln1 = l; 
      massindpos[vcs.vertlist[v-1][l-1].lorentz-1].vrt2 = 
         vcs.vertlist[v-1][l-1].nextvert.vno; 
      massindpos[vcs.vertlist[v-1][l-1].lorentz-1].ln2 = 
         vcs.vertlist[v-1][l-1].nextvert.edno; 
   } 
} 


static byte  vectorslot(byte v)
{byte  l;

   l = vcs.valence[v-1];
	while (l > 0 && vcs.vertlist[v-1][l-1].lorentz == 0) --(l);
   return l;
}


static byte  vectorslot2(byte v)
{byte  l;

   l = vcs.valence[v-1];
	while (l > 0 && vcs.vertlist[v-1][l-1].lorentz == 0)  l-- ;
	if (l != 0)
	{  l--;
		while (l > 0 && vcs.vertlist[v-1][l-1].lorentz == 0)  l-- ;
	}
   return l;
}



static void nextFerm(byte * v_,   byte * l_)
{
  byte l1;
  l1 =vcs.vertlist[(*v_)-1][(*l_)-1].nextvert.edno;
  *v_=vcs.vertlist[(*v_)-1][(*l_)-1].nextvert.vno;
  *l_=1;
  while ( (*l_ == l1)||
          (prtclbase[vcs.vertlist[(*v_)-1][(*l_)-1].partcl-1].spin != 1)
        ) (*l_)++;
}


static void  findfermcycles(void)
{byte  v, v1, l, l1;
 byte  count;

   nloop = 0;
   for (v = 1; v <= vcs.sizet; v++) fermmap[v-1] = 0;

   for (v = 1; v <= vcs.sizet; v++)
   {
      if (fermmap[v-1] == 0)
      {
   /*      l = fermslot(v); */
         l=vcs.valence[v-1];
			while ( (l>0)&&(! fermionp(vcs.vertlist[v-1][l-1].partcl))) l--;
         if (l != 0)
         {
            count = 0;
            ++(nloop);
            v1 = v;
            fermloops[nloop-1].lprtcl = FALSE;
            do
            {  /*  Until FermMap[v1]<>0  */
               ++(count);
               fermloops[nloop-1].vv[count-1] = v1;
               fermloops[nloop-1].ll[count-1] = l;
               if (strchr("LR",
                          prtclbase[vcs.vertlist[v1-1][l-1].partcl-1].hlp)
                   != NULL)
                  fermloops[nloop-1].lprtcl = TRUE;
					fermloops[nloop-1].intln[count-1]  = 0;
					fermloops[nloop-1].intln2[count-1] = 0;
               fermmap[v1-1] = nloop;
					l1 = vectorslot(v1);
					if (l1 != 0)
					{
						if (fermmap[vcs.vertlist[v1-1][l1-1].nextvert.vno-1] ==nloop
							) fermloops[nloop-1].intln[count-1] =l1;
						l1=vectorslot2(v1);
						if ( (l1 !=0 )&&
							  ( fermmap[vcs.vertlist[v1-1][l1-1].nextvert.vno-1] == nloop)
							) if (fermloops[nloop-1].intln[count-1] == 0)
										 fermloops[nloop-1].intln[count-1] =l1;
							  else    fermloops[nloop-1].intln2[count-1] =l1;
					}
					nextFerm(&v1,&l);

            }  while (fermmap[v1-1] == 0);
            fermloops[nloop-1].g5 = fermloops[nloop-1].lprtcl;

            fermloops[nloop-1].len = count;
         }
      }
   }
}


static void  findinnerverts(void)
{byte  v, l, nl; 

   for (v = 1; v <= nloop; v++) strcpy(fermloops[v-1].invrt,""); 
   for (v = 1; v <= vcs.sizet; v++) 
      if (fermmap[v-1] == 0)
      { 
         l = vectorslot(v);
         if (l != 0)
         {
            nl = fermmap[vcs.vertlist[v-1][l-1].nextvert.vno-1];
            if (nl != 0)
            {
               for (l = l - 1; l >= 1; l--)
                  if (nl != fermmap[vcs.vertlist[v-1][l-1].nextvert.vno-1])
                     goto label_1;
               sbld(fermloops[nl-1].invrt,
                    "%s%c",fermloops[nl-1].invrt,v); 

label_1: ; 
            } 
         } 
      } 
} 


static void  findsubst(byte v,byte l,char* subst)
{momsum      frontsubst;
 byte        i, j, vv, ll; 

   subst[0] = 0; /* strcpy(subst,""); */
   if ((setof(inp,intrp,_E) & vcs.vertlist[v-1][l-1].prop) != setof(_E)) 
      subst[++subst[0]] = vcs.vertlist[v-1][l-1].moment;
      /* sbld(subst,"%s%c",subst,vcs.vertlist[v-1][l-1].moment); */
   else 
      for (i = 1; i <= vcs.valence[v-1]; i++) 
         if (i != l)
         { 
            vv = vcs.vertlist[v-1][i-1].nextvert.vno; 
            ll = vcs.vertlist[v-1][i-1].nextvert.edno; 
            findsubst(vv,ll,frontsubst);
            for (j = 1; j <= frontsubst[0]; j++)
               subst[subst[0] + j] = frontsubst[j];
            subst[0] += frontsubst[0]; 
            /* sbld(subst,"%s%s",subst,frontsubst); */
         } 
}  /*  FindCond  */ 


static void  changesign(char* subst)
{byte        i; 

   for (i = 1; i <= /*strlen(subst)*/ subst[0]; i++) 
      subst[i] = - subst[i]; 
} 


static void  optimsubst(byte v,byte l,char* subst)
{momsum      frontsubst, backsubst; 
 byte        i, vv, ll; 

   findsubst(v,l,frontsubst); 
   vv = vcs.vertlist[v-1][l-1].nextvert.vno; 
   ll = vcs.vertlist[v-1][l-1].nextvert.edno; 
   findsubst(vv,ll,backsubst); 
   if (/*strlen(frontsubst) <= strlen(backsubst)*/
       frontsubst[0] <= backsubst[0]) 
      for (i = 0; i <= frontsubst[0]; i++) subst[i] = frontsubst[i];
      /* strcpy(subst,frontsubst); */
   else 
   {  for (i = 0; i <= backsubst[0]; i++) subst[i] = backsubst[i];
      /* strcpy(subst,backsubst); */
      changesign(subst); 
   } 
}   /*  OptimCond  */


static void  standartsubst(byte v,byte l,char* subst)
{byte        i, vv, ll; 
 boolean     flg1 = FALSE, flg2 = FALSE; 
 char        ch; 

   if (vcs.vertlist[v-1][l-1].moment == nin + nout) 
   {  subst[0] = 0;
      /* strcpy(subst,""); */
      for (i = 1; i <= nin; i++)
         subst[++subst[0]] = i;          
         /* sbld(subst,"%s%c",subst,i); */
      for (i = nin + 1; i <= nin + nout - 1; i++)
         subst[++subst[0]] = -i;
         /* sbld(subst,"%s%c",subst,-i); */
      return;
   }

   ch = nin + nout; 
   findsubst(v,l,subst); 
   for (i = 1; i <= subst[0]; i++) 
   {  if (subst[i] ==  ch) flg1 = TRUE;
      if (subst[i] == -ch) flg2 = TRUE;
   }
   if (flg1 && flg2) 
   { 
      vv = vcs.vertlist[v-1][l-1].nextvert.vno; 
      ll = vcs.vertlist[v-1][l-1].nextvert.edno;
      findsubst(vv,ll,subst); 
      changesign(subst); 
   } 
}   /*  StandartSubst  */ 


void  findinternalmoments(boolean indep)
{byte     l, v, vln;
 char     m;

   for (m = 1; m <= 3 * maxvert; m++) momdep[m-1][0] = 0;
      /* strcpy(momdep[m-1],"") */
   for (v = 1; v <= vcs.sizet; v++)
   {
      vln = vcs.valence[v-1];
      for (l = 1; l <= vln; l++)
      {
         m = vcs.vertlist[v-1][l-1].moment;
         if (m > 0)   /* ( Consr_Low in Options) */
            if (indep)
               standartsubst(v,l,momdep[m-1]);
            else   /* If m>0 Then OptimSubst(v,l,MomDep[m]) */
               optimsubst(v,l,momdep[m-1]);
      }
   }
}


void  preperdiagram(void)
{
   findinoutmasses();
   findmassindex();
   findfermcycles();
   findinnerverts();
   findinternalmoments(inset(conslow,options));
}
