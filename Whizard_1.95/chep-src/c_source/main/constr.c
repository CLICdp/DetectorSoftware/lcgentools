#include "tptcmac.h"
#include "syst2.h"
#include "global.h"
#include "physics.h"
#include "crt0.h"
#include "crt.h"
#include "crt_util.h"
#include "files.h"
#include "os.h"

#include "constr.h"

#define maxref   (MAXINOUT - 2)
#define lref     (2 * maxref - 2)

#define indexlink struct indexStruct *
     typedef struct indexStruct {
                 byte         outlist[3];
                 word         num;
                 indexlink    ilink;
                          } indexStruct;
#undef indexlink
     typedef struct indexStruct *indexlink;

     typedef char listprtcl[lref];

#define elementlink struct element *
     typedef struct element {
                listprtcl    prtcl;
                elementlink  next;
                            } element;
#undef elementlink
     typedef struct element *elementlink; 

static  FILE *       bufres;   
static  byte         nprimary, n_two;
static  word         kmenu; 
static  indexlink    head; 
static  byte         ndecay; 
static  word         n_diagram; 
static  word         m_diagram; 
static  elementlink  first, zero; 
static  elementlink  ref[127][maxref]; 


static void clearref(void)
{ 
   elementlink  e1, e2; 
   byte         i, j; 

   for (j = 1; j <= nparticles; j++) 
   { 
      ref[j-1][0] = NULL; 
      for (i = 2; i <= ndecay - 1; i++) 
      { 
         e1 = ref[j-1][i-1]; 
         ref[j-1][i-1] = NULL; 
         if (e1 != zero) 
             while (e1 != NULL) 
             { 
                e2 = e1; 
                e1 = e1->next; 
                free(e2); 
             } 
      } 
   } 
} 

static boolean  testin(byte l,adiagram restmp)
{
   char     nneed, np;
   byte         i, j;
   byte         copyincl[MAXINOUT];
   byte         copyins[MAXINOUT];
   double         copymass;

   nneed = l - n_x;   /*  N_x  number of unknoun inclusev particles
         L   number of decay paticles */
   j = 1;
   while (inclp[j-1].who != 0)
      { copyincl[j-1] = inclp[j-1].how; j++; }
   j = 1;
   while (liminsp[j-1].who != 0)
      { copyins[j-1] = liminsp[j-1].how; j++; }
   copymass = missingmass;
   for (i = 1; i <= 2 * (l - 1); i++)
   {
      np = restmp[i-1];
      if (np > 0)
      {
         j = 1;
         while (inclp[j-1].who != 0 && inclp[j-1].who != np) j++;
         if (inclp[j-1].who == 0 || copyincl[j-1] == 0)
         {
            if (pseudop(np)) copymass = 0.0;
            else copymass -= prtclbase[np-1].mass;
				if (copymass <= 1.0E-7) return FALSE;
			}
         else { copyincl[j-1]--; nneed--; }
      }
      if (np < 0)
      {
         np = -np;
         if (np > prtclbase[np-1].anti) np = prtclbase[np-1].anti;
         j = 1;
         while (liminsp[j-1].who != 0 && liminsp[j-1].who != np) j++;
         if (liminsp[j-1].who != 0)
            if (copyins[j-1] == 1)
               return FALSE;
            else
               copyins[j-1]--;
      }
   }
   return (nneed <= 0 ? TRUE : FALSE);
}

static void  conon(adiagram diagram,byte k)
{ 
   adiagram     m_diagram; 
   char     c; 
   byte         l, li, i, length, shift; 

   l = 1; 
   do
   {  /*  until K=L  */ 
      li = l; 
      c = 0; 
      do 
      {  /*  until c=1 */ 
         if (++li == k) goto label_1;
         if (diagram[li-1] > 0) c++; else c--; 
      }  while (c != 1);
      for (i = l + 1; i <= li; i++) m_diagram[i - l-1] = diagram[i-1]; 
      length = li - l; 
      if (diagram[li] != 0) 
      {  /*  3 - particle  vertex  */ 
         k -= length; 
         c = 0; 
         do 
         {  /*  until c=1  */ 
            if (diagram[++li-1] > 0) c++; else c--; 
            diagram[li - length-1] = diagram[li-1]; 
         } while (c != 1); 
         li -= length; 
         for (i = 1; i <= length; i++) diagram[li + i-1] = m_diagram[i-1]; 
      } 
      else   
      {  /*  4 - particle vertex  */ 
         li++; 
         c = 0; 
         do /*  until c=1  */ 
            if (diagram[++li-1] > 0) c++; else c--; 
         while (c != 1);

         if (li >= k)
         {  /*  K in Fragment N 2  */  
            shift = length + 1; 
            k -= shift; 
            for (i = l + 1 + shift; i <= li; i++) 
               diagram[i - shift-1] = diagram[i-1]; 
            li += - shift + 1; 
            diagram[li-1] = 0; 
            for (i = 1; i <= length; i++) diagram[li + i-1] = m_diagram[i-1]; 
         } 
         else
         {  /*  K in Fragment  N 3  */  
            for (i = l + 2 + length; i <= li; i++) 
            m_diagram[i - l - 2] = diagram[i-1]; 
            shift = li - l; 
            k -= shift; 
            c = 0; 
            do 
            {  /*  until c=1  */ 
               if (diagram[++li-1] > 0) c++; else c--;
               diagram[li - shift-1] = diagram[li-1]; 
            }  while (c != 1); 
            li += - shift + 1; 
            diagram[li-1] = 0; 
            for (i = 1; i <= shift - 1; i++) 
            diagram[li + i-1] = m_diagram[i-1]; 
         } /* end if */ 
      } 

      label_1: l++;
   }  while (k != l);
}

static void dooutres(adiagram res)
{
   byte         lmax, m, k, l, i;
   boolean      cond;
   adiagram     copyres;
   adiagram     tmplist[MAXINOUT];


   if (nin == 2)
   {
      lmax = 2 * ndecay - 1;
      m = 0;
      for (k = 2; k <= lmax; k++)
      {
         if (res[k-1] == n_two)
         {
            lvcpy(copyres,res);
            conon(copyres,k);
            if (m != 0) for (l = 1; l <= m; l++)
            {
               cond = TRUE;
               for (i = 2; i <= lmax; i++)
                  cond = cond && (copyres[i-1] == tmplist[l-1][i-1]) ? TRUE : FALSE;
               if (cond) goto label_1;
            }
            n_diagram++;
            FWRITE1(copyres,bufres);
            m++;
            lvcpy(tmplist[m-1],copyres);
            label_1: ;
         }
      }
   }
   else
   {
      n_diagram++;
      f_write(res,sizeof(adiagram),1,bufres);
   }
}


static void dtc(boolean* incond,byte l,adiagram restmp)
{
   byte         i;
   adiagram    res; 

   *incond = testin(l,restmp);
   if (!(*incond) || l < ndecay) return;

   *incond = FALSE;
   res[0] = -nprimary;
   for (i = 1; i <= 2 * (ndecay - 1); i++) res[i] = restmp[i-1];
   dooutres(res);
	goto_xy(1,24);  print("%u",n_diagram); refresh_scr();
   if (n_diagram > m_diagram)
       if ( mess_y_n(35,15,"   Continue   $")) m_diagram += 500;
      else  errorcode = -1;
}


static void decay(byte n_part,byte l)
{
   adiagram     restmp; /* Used in testin,dtc */
   boolean      existence, equalcond, incond;
   decaylink    pp;
   char     pk, d, c;
   byte         m, ni, li, k, jk, ik, i1, i2, i3,
                i2_min, i2_max, i3_min, i3_max,
                i[3], j[3];
   elementlink  el1, el2, el3, elk, newelement, oldelement;
   elementlink  el[3];
  /* * * * * * * * * * * * * * * * */

   if (l == 1)
   {
      k = 1;
      while (inclp[k-1].who != 0 && inclp[k-1].who != n_part) k++;
		if (inclp[/*++*/ k-1].who == 0 &&
          missingmass - prtclbase[n_part-1].mass < 1.0E-5)
         ref[n_part-1][0] = zero;
      else ref[n_part-1][0] = first;
      return;
	}
   existence = FALSE;
   pp=prtclbase[n_part-1].top;

   while (pp != NULL)
   {
      if (pp->part[2] == 0)
        { i3_min = 0; i3_max = 0; }
      else
      {
         if (l == 2) goto label_100;
         i3_min = 1;
         i3_max = l - 2;
      }
      for (i3 = i3_min; i3 <= i3_max; i3++)
      {
         i2_max = l - i3 - 1; 
         if (pp->part[1] == pp->part[2] && i2_max > i3) 
            i2_max = i3; 
         if (pp->part[1] == pp->part[0]) 
            i2_min = (l + 1 - i3) / 2;
         else i2_min = 1; 
         for (i2 = i2_min; i2 <= i2_max; i2++) 
         { 
            i1 = l - i2 - i3; 
            if (i3 == 0) ni = 2; 
            else ni = 3; 
            i[2] = i3; i[1] = i2; i[0] = i1; 

     /*  New REORDER    beg  */ 
            for (k = 1; k <= 3; k++) j[k-1] = k; 
            k = 1;
            while (k < ni) 
            { 
               d = i[j[k-1]-1] - i[j[k]-1]; 
               if(d > 0 || (d == 0 && 
                  pp->part[j[k-1]-1]<pp->part[j[k-1]-1])) 
               { 
                  c = j[k-1]; 
                  j[k-1] = j[k]; 
                  j[k] = c; 
                  if (k == 1) ++(k); else --(k); 
               } 
               else ++(k); 
            } 
     /*  New REORDER     end  */ 
            for (k = 1; k <= ni; k++) 
            { 
               jk = j[k-1]; 
               ik = i[jk-1]; 
               pk = pp->part[jk-1]; 
               if (ref[pk-1][ik-1] == NULL)  decay(pk,ik);
               if (ref[pk-1][ik-1] == zero) goto label_101;
            } 
            if (i3 == 0) el3 = NULL; 
            else el3 = ref[(pp->part[2])-1][i3-1]; 
            do 
            {  /* until El3=NULL */ 
               el2 = ref[(pp->part[1])-1][i2-1]; 
               do 
               {  /*  until El2=NULL  */ 
                  el1 = ref[(pp->part[0])-1][i1-1]; 
                  do 
                  {  /* until El1=NULL */ 
                     el[0] = el1; el[1] = el2; el[2] = el3; 
                     li = 1; 
                     for (k = 1; k <= ni; k++) 
                     { 
                        jk = j[k-1]; 
                        pk = pp->part[jk-1]; 
                        ik = i[jk-1]; 
                        if (ik == 1) restmp[li++-1] = pk; 
                        else 
                        {
                           restmp[li++-1] = -pk; 
                           elk = el[jk-1]; 
                           for (m = 1; m <= 2 * ik - 2; m++) 
                              restmp[li++-1] = elk->prtcl[m-1]; 
                        } 
                        if (ni == 3 && k == 1) restmp[li++-1] = 0; 
                     } 
                     dtc(&incond,l,restmp); 
                     if (errorcode != 0) goto label_102;
                     if (incond) 
                     { 
                        newelement = (elementlink)getmem(sizeof(element));
                        for (m = 1; m <= 2 * (l - 1); m++) 
                           newelement->prtcl[m-1] = restmp[m-1]; 
                        if (existence) 
                           oldelement->next = newelement;
                        else 
                        { 
                           ref[n_part-1][l-1] = newelement; 
                           existence = TRUE; 
                        }
                        oldelement = newelement; 
                     } 
                     equalcond = el1 == el2 ? TRUE : FALSE; 
                     el1 = el1->next; 
                  }  while (!(el1 == NULL || equalcond)); 
                  equalcond = el3 == el2 ? TRUE : FALSE; 
                  el2 = el2->next; 
               }  while (!(el2 == NULL || equalcond)); 
                  if (i3 != 0) el3 = el3->next; 
            }  while (el3 != NULL); 
/*  memory optimisation ?=>	if (l == ndecay && maxavail() < 20000) clearref(); */
            label_101: ; 
         }  /* I2 FOR circl  */ 
      }  /*  I3 FOR circl  */ 
      label_100:   /*  UNTIL pp=Nil; */ 
      pp=pp->link; 
   }                  /*  while pp<>NULL */ 
   label_102: 
   if (existence) oldelement->next = NULL; 
   else 
   if (l < ndecay) ref[n_part-1][l-1] = zero;
} 

static void doindex(void)
{ 
    byte         i, j, k, c; 
    word         recno; 
    adiagram     res; 
    indexlink    next, old; 
    boolean      switch_; 
    whohow       p_list; 
    byte         nout; 

   recno = 0; 
   nout = hadr2.how == 0 ? ndecay : ndecay - 1; 
   old = head;  
   while(FREAD1(res,bufres)==1)   
   {  
      switch_ = hadr2.how == 0 ? TRUE : FALSE; 
      next = (indexlink)getmem(sizeof(indexStruct));
      old->ilink = next; 
      old = next; 
      
      nilprtcl(p_list); 
      i = 2; j = 1; 
      do 
      {   
         if (res[i-1] > 0) 
         if (switch_) 
         { 
            addprtcl(p_list,res[i-1]); 
            j++; 
         } 
         else 
            switch_ = TRUE; 
         i++; 
      }  while (j <= nout); 

      j = 1; 
      while (inclp[j-1].who != 0)
      {  for (i = 1; i <= inclp[j-1].how; i++)
            delprtcl(p_list,inclp[j-1].who);
         j++;
      }
      j = 1; i = 1;
      while (p_list[j-1].who != 0)
      {
         for (k = 1; k <= p_list[j-1].how; k++)
            next->outlist[(i++)-1] = p_list[j-1].who;
         j++;
      }
      if (n_x > 1)
      {
         i = 1;
         do
         {  /*  until i=N_X  */
            c = next->outlist[i-1];
            if (c < next->outlist[i])
            {
               next->outlist[i-1] = next->outlist[i];
               next->outlist[i] = c;
               if (i > 1) i--; else i = 2;
            }
            else  i++;
         }  while (i != n_x);
      }
      next->num = recno++;
   } 
   next->ilink = NULL;
}


static indexlink  mark1, mark2, mark3; /* From sortindex */

static void sorttwoblocks(void)
{ indexlink    mark_1, mark_2;
  integer      i, diff;

   mark_2 = mark2->ilink;
   do
   {
      mark_1 = mark1->ilink;
      i = 1;
      do
         diff = mark_1->outlist[i-1] - mark_2->outlist[i-1];
      while (!(++i > n_x || diff != 0));
      if (diff < 0)
      {
         /*  Reoder  */
         mark2->ilink = mark3->ilink;
         mark3->ilink = mark_1;
         mark1->ilink = mark_2;
         /*  Rename  */
         mark_2 = mark2;   /* Temporary */
         mark2 = mark3;
         mark3 = mark_2;
         mark_2 = mark_1;
      } 
      mark1 = mark1->ilink; 
   }  while (mark1 != mark2); 
} 

static void sortindex(int ndiagram)
{int  lblock, i, di, iend; 
   if (n_x == 0) return;
   lblock = 1; 
   while (lblock < ndiagram) 
   { 
      mark3 = head; 
      iend = 0; 
      while (iend + lblock < ndiagram) 
      { 
         mark1 = mark3; 
         mark2 = mark1; 
         for (i = 1; i <= lblock; i++) mark2 = mark2->ilink; 
         iend += lblock; 
         di = ndiagram - iend; 
         if (di > lblock) di = lblock; 
         mark3 = mark2; 
         for (i = 1; i <= di; i++) mark3 = mark3->ilink; 
         sorttwoblocks(); 
         iend += di; 
      } 
      lblock *= 2; 
   } 
} 


static shortstr   recor_;         /* From addbuf */
static word       firstrec, nsubc; /* From addbuf */
static whohow     outprtcls;      /* From addbuf */

static void addrecordtomenu(void)
{ shortstr     recor; 
  byte         i, j, wh, hw, len; 

   /*  Sorting , Will be removed    */ 
   i = 1; 
   while (outprtcls[i].who != 0) 
   { 
      if (outprtcls[i-1].who <= outprtcls[i].who) ++(i); 
      else 
      { 
         wh = outprtcls[i-1].who; hw = outprtcls[i-1].how; 
         outprtcls[i-1] = outprtcls[i]; 
         outprtcls[i].who = wh; 
         outprtcls[i].how = hw; 
         if (i > 1) --(i); else ++(i); 
      } 
   }    
   /*  End Sorting      */ 
   strcpy(recor,recor_); 
   i = 1; 
   while (outprtcls[i-1].who != 0) 
   { 
      for (j = 1; j <= outprtcls[i-1].how; j++) 
         sbld(recor,"%s%s,",recor,prtclbase[outprtcls[i-1].who-1].name); 
      ++(i); 
   } 

   len = (byte)strlen(recor); 
   recor[len-1] = ' '; 

   wrt_menu(1,kmenu,recor,0,0,nsubc,firstrec/sizeof(adiagram));

}   /*  AddRecordToMenu  */


static void addbuf(void)
{
    byte         i, j;
    byte         mem[3];
    indexlink    old, next;
    adiagram     res;
    boolean      w, nilcond;

   sbld(recor_," %s",prtclbase[nprimary-1].name);
   if (hadr2.how != 0)
      sbld(recor_,"%s,%s",
           recor_,prtclbase[prtclbase[n_two-1].anti-1].name);
   sbld(recor_,"%s -> ",recor_);
   next = head->ilink;
   for (i = 1; i <= n_x; i++) mem[i-1] = next->outlist[i-1];
   firstrec = ftell(diagrp);
   nsubc = 0;
   while (next != NULL)
   {
      w = FALSE;
      for (i = 1; i <= n_x; i++)
         w = w || (next->outlist[i-1] != mem[i-1]); 
      if (w) 
      { 
         lvcpy(outprtcls,inclp); 
         for (j = 1; j <= n_x; j++) addprtcl(outprtcls,mem[j-1]); 
         addrecordtomenu(); 
         kmenu++; 
         firstrec += nsubc*sizeof(adiagram); 
         nsubc = 0; 
         for (j = 1; j <= n_x; j++) mem[j-1] = next->outlist[j-1]; 
      } 
      nilcond = (next->ilink == NULL); 
      if (nilcond) 
      { 
         lvcpy(outprtcls,inclp); 
         for (j = 1; j <= n_x; j++) 
            addprtcl(outprtcls,next->outlist[j-1]); 
         nsubc++; 
         addrecordtomenu(); 
         kmenu++; 
      } 
      nsubc++;      
      fseek(bufres,sizeof(adiagram)*next->num,SEEK_SET);       
      FREAD1(res,bufres); 
      FWRITE1(res,diagrp); 
      old = next; 
      next = next->ilink; 
      free(old); 
   } 
} 

void construct(void)
{
   int          i,j,ndiagram;
   char buf_name[STRSIZ];
   
   diagrp=fopen(DIAGRP_NAME,"wb");
   sprintf(buf_name,"%stmp%cbuf.res",pathtouser,f_slash);
   menup=fopen(MENUP_NAME,"wb");
   m_diagram = 500;
   kmenu = 1;
   f_write("\053\062",2,1,menup);
   head = (indexlink)getmem(sizeof(indexStruct));
   first = (elementlink)getmem(sizeof(element)); first->next = NULL;
   zero = (elementlink)getmem(sizeof(element));  zero->next = NULL;
   for (i = 1; i <= nparticles; i++)
      for (j = 1; j <= maxref; j++) ref[i-1][j-1] = NULL;
   ndecay = nin + nout - 1;
   n_diagram = 0;
   for (i = 1; i <= hadr1.how; i++)
   {  nprimary = hadr1.parton[i-1];
      for (j = 1; j <= (nin==1? 1:hadr2.how); j++)
      {  if (nin==2)
         {  n_two = hadr2.parton[j-1];            
            if (n_two>=nprimary || !inhadr(&hadr1,n_two) 
                                || !inhadr(&hadr2,nprimary))             
            {  n_two = prtclbase[n_two-1].anti;
               addprtcl(inclp,n_two);
            } else goto bypass;
         }
         bufres=fopen(buf_name,"wb");
         decay(nprimary,ndecay);
         if (nin == 2) delprtcl(inclp,n_two);
         clearref();
         ndiagram=(ftell(bufres))/sizeof(adiagram);
         if (ndiagram > 0)
         {
            fclose(bufres); bufres=fopen(buf_name,"rb");
            doindex();
            sortindex(ndiagram);
            addbuf();
         }
         fclose(bufres);
         bypass: ;
         if (errorcode != 0) goto label_2;
      }
   }

   label_2:
   fclose(diagrp);

   unlink(buf_name); 
   subproc_f = filesize(menup) / ('\062');

   fclose(menup);
   free(head);
   free(first);
   free(zero);

   if (n_diagram == 0)
   {  
      messanykey(5,22,"   Processes of this type are absent   $");
      errorcode = -1;
   }
   else
   {
      totdiag_f = n_diagram;
      deldiag_f = 0;
      be_be();
      errorcode = 0;
   }
   for (i = 17; i <= 24; i++) { goto_xy(1,i); clr_eol(); }   
}

