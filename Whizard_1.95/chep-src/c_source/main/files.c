#include "tptcmac.h"
#include "physics.h"
#include "syst2.h"
#include "crt.h"
#include "crt_util.h"
#include "os.h"

#include "files.h"

 FILE * menup;
 FILE * menuq;
 FILE * diagrp;   /* file of Adiagram; */
 FILE * diagrq;   /* file of CSdiagram; */
 FILE * catalog;
 FILE * archiv;

 shortstr pathtouser, pathtocomphep,pathtoresults;
 shortstr _pathtouser, _pathtocomphep;

 char  version[26]="CompHEP  version  3.2    ";


char  mdFls[4][10] = {"vars","func","prtcls","lgrng"};

void  (*diskerror)(void) = NULL;


void wrt_menu(byte menutype,word k,char* txt,word ndel,word ncalc,
                            word nrest,word recpos)
{integer   i,d;
 char      numstr[11];
 shortstr  buff;

   d = menutype == 2 ? 5 : 0;
   if (menutype == 1)
      fseek(menup,((long)k - 1) * (50 + d) + 2,SEEK_SET);
   else
      fseek(menuq,((long)k - 1) * (50 + d) + 2,SEEK_SET);
   sbld(buff,"%4u",k);
   i=(byte)strlen(txt);
   while( i < 27 ) txt[i++]=' ';
   txt[27]='\0';
   sbld(buff,"%s| %s|",buff,txt);
   sbld(numstr,"%4u",ndel);
   sbld(buff,"%s%s|",buff,numstr);
   if (menutype == 2)
   {
      sbld(numstr,"%4u",ncalc);
      sbld(buff,"%s%s|",buff,numstr);
   }
   sbld(numstr,"%4u",nrest);
   sbld(buff,"%s%s",buff,numstr);
	sbld(numstr,"%7u",recpos);
   sbld(buff,"%s%s",buff,numstr);
   if (menutype == 1) f_write(buff,50+d,1,menup);
           else       f_write(buff,50+d,1,menuq);
}


void rd_menu(byte menutype,word k,char* txt,word* ndel,word* ncalc,
                        word* nrest,word* recpos)
{integer      d;
 shortstr     buff;
 integer      errorcode;

   d = menutype == 2 ? 5 : 0;
   if (menutype == 1)
		fseek(menup,(k - 1) * (50 + d) + 2,SEEK_SET);
   else
		fseek(menuq,(k - 1) * (50 + d) + 2,SEEK_SET);

   buff[50 + d] = '\0';
   if (menutype == 1)  fread(buff,50+d,1,menup);
          else         fread(buff,50+d,1,menuq);
	strcpy(txt,copy(buff,7,27));
	valw(copy(buff,35,4),ndel,&errorcode);
   if (menutype == 2)
		valw(copy(buff,40,4),ncalc,&errorcode);
   else
      *ncalc = 0;
	valw(copy(buff,40 + d,4),nrest,&errorcode);
	valw(copy(buff,44 + d,7),recpos,&errorcode);
}

void copyfile(char* namefrom,char* nameto)
{ 

  FILE * filefrom;
  FILE * fileto;
  char  s [STRSIZ];
  filefrom= fopen(namefrom,"r");
  fileto  = fopen(nameto,"w");
  if ( (filefrom==NULL) || (fileto==NULL) ) return;

  while (1)
  if (fgets(s,STRSIZ,filefrom) != NULL) f_puts(s,fileto) ; else
  {
	fclose(fileto);
	fclose(filefrom);
        return;

  }

}



int f_printf(FILE *fp,char * format, ... )
{ va_list args;
  char dump[STRSIZ];
  int  r;
  va_start(args, format);

        vsprintf(dump,format,args);
	va_end(args);
	r = fputs(dump,fp);
	if (r == EOF)
	{  messanykey(5,20,"not free disk space$");
		exit(0);
	}
	return r;
 }


int f_puts(char * s,  FILE * fp)
{  int r;
	 r = fputs(s,fp);
	 if (r == EOF)
	 {  messanykey(5,20,"not free disk space$");
		exit(0);
	 }
	 return r;
}

size_t f_write(void* ptr,  size_t size,  size_t n,  FILE * fp)
{ size_t nn;
   if ((size == 0)||(n == 0)) return 0;
   nn= fwrite(ptr,size,n,fp);
   if ((nn != n) && (diskerror !=NULL ))  (*diskerror)() ;
   return nn;
}

void newFileName(char* f_name,char * firstname,char * ext)
{  int tabnum=0;
   int exist;
   searchrec  s; 
   do
   {  tabnum++;
      sprintf(f_name,"%s%c%s%d.%s",pathtoresults,f_slash,firstname,tabnum,ext);
      exist = find_first(f_name,&s,archive);
   }  while (!exist);
}
