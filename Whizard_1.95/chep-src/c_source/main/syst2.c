#include "tptcmac.h"
#include "syst2.h"

#define ALIGNMENT 
long  usedmemory = 0;

void  (*memerror)(void) = NULL;


#define markreleaseptr struct markreleaserec *
typedef struct markreleaserec
   {
		markreleaseptr    next;
		void *            dummy;
      byte              mfield[blocksize];
   }  markreleaserec;

#undef markreleaseptr
typedef struct markreleaserec *markreleaseptr;

static markreleaseptr   blockptr = NULL;
static word             currentpos=0;

int blockrest(int  size)
{
#ifdef ALIGNMENT
	size= ((size+7)>>3)<<3;
#endif
	return (blocksize - currentpos)/size ;
}

void lShift(char *  s,int  l)
{ int i;
  if (l>0)
  {  int  m=strlen(s);
     for(i=0;i<= m-l; i++) s[i]=s[i+l];
  }
  if (l<0)
  {    i=strlen(s);
		 while ( i >=0 ) {s[i-l]=s[i]; i--;}
		 for (i=0;i<-l;i++) s[i]=' ';
  }
}


char *funstr(long num)
{ static char  txt[11];
   sbld(txt,"%ld",num);
   return txt;
}


void  revers(pointer* list)
{ pointer  *q,*p,*r;

   if (*list == NULL) return;
   r = (pointer *)(*list);
   q = (pointer *)(*r);
   *r = NULL;
   while(q != NULL)
   {  p = (pointer *)(*q);
      *q = (pointer)r;
      r = q;
      q = p;
   }
   *list = (pointer)r;
}


void  mark_(marktp*  mrk)
{
	mrk->blk_  = (pointer)blockptr;
	mrk->pos_  = currentpos;
}

void release_(marktp* mrk)
{
  markreleaseptr   q;
	while (mrk->blk_ != (pointer)blockptr)
	{
		q=blockptr;
		blockptr=q->next;
		free(q);
		usedmemory=usedmemory-sizeof(markreleaserec);
	}
	currentpos=mrk->pos_;
}

pointer  getmem(size_t size)
{  pointer p;
	p=malloc(size);
	if( (p==NULL) && (memerror !=NULL) ) (*memerror)() ;
	return p;
}

pointer  getmem_(word size)
{  markreleaseptr   q;
	pointer          p;
#ifdef ALIGNMENT
	size= ((size+7)>>3)<<3;
#endif
	if( (blockptr == NULL) || (currentpos + size >= blocksize))
   {
      q = blockptr;
      blockptr = (markreleaseptr)getmem(sizeof(markreleaserec));
      if(blockptr == NULL)
      { blockptr=q;
        return NULL;
      }
      blockptr->next = q;
      currentpos = 0;
      usedmemory=usedmemory+sizeof(markreleaserec);
   }
   p = (pointer)&(blockptr->mfield[currentpos]);
   currentpos += size;
   return p;
}
