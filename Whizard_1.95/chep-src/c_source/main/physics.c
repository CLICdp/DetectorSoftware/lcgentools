#include "tptcmac.h"
#include "syst2.h"
#include "global.h"

#include "physics.h"

 boolean    gg_exist;
 algvertptr lgrgn;
 varlist    modelvars;
 prtcl_base prtclbase[127];
 vcsect     vcs;
 byte       nparticles;   /*  Number particles in model */
 hadron     hadr1, hadr2;
 whohow     inclp, liminsp;
 double       sqrts, missingmass;
 byte       nmodelvar;

void   addprtcl(whohow p_list,byte n)
/* whohow p_list;
 byte n;*/
{byte i;
   for (i = 1; i <= 39; i++)
   {
      if (p_list[i-1].who == n)
      { 
         p_list[i-1].how++; 
         return;
      } 
      if (p_list[i-1].who == 0) 
      { 
         p_list[i-1].who = n; 
         p_list[i-1].how = 1; 
         return;
      }
   }
}


void   delprtcl(whohow p_list,byte n)
/* whohow p_list;
 byte n;*/
{byte  i=1;
   while (p_list[i-1].who != 0)
   {
      if (p_list[i-1].who == n)
		{
         if (--p_list[i-1].how == 0)
         do {
               p_list[i-1].who = p_list[i].who;
               p_list[i-1].how = p_list[i].how;
					i++;
				}  while (!(p_list[i-1].who == 0));
         return;
      }
      i++;
   } 
} 


void  nilprtcl(whohow p_list)
/* whohow p_list;*/
{byte   i; 
   for (i = 1; i <= 40; i++) p_list[i-1].who = 0; 
} 

void  addlim(whohow p_list,byte j,byte k)
/* whohow p_list;
 byte j;
 byte k;*/
{byte i; 

   if (prtclbase[j-1].anti < j) j = prtclbase[j-1].anti; 
   for (i = 1; i <= 39; i++) 
   { 
      if (p_list[i-1].who == j) return;
      if (p_list[i-1].who == 0) 
      {  p_list[i-1].who = j; 
         p_list[i-1].how = k; 
         p_list[i].who = 0; 
         return;
      } 
   } 
} 

void  locateinbase(char* name,byte* number)
/* char * name;
 byte * number;*/
{byte   i; 
   for (i = 1; i <= nparticles; i++) 
      if (strcmp(name,prtclbase[i-1].name) == 0) 
      {  *number = i; 
         return;
      }
   *number = 0; 
} 


boolean  inhadr(hadron* hadr,byte np)
/* hadron * hadr;
 byte np;*/
{byte   i; 
   for (i = 1; i <= hadr->how; i++) 
   if (hadr->parton[i-1] == np) 
      return TRUE; 
   return FALSE; 
} 

boolean  pseudop(byte np)
/* byte np;*/
{ 
   return np == 0 || np > nparticles || prtclbase[np-1].hlp == '*' ?
             TRUE : FALSE; 
} 

boolean  fermionp(ptcltype p)
/* ptcltype p;*/
{ 
   return prtclbase[p-1].spin % 2 == 1 && p <= prtclbase[p-1].anti ? 
             TRUE : FALSE; 
} 


boolean  bosonp(ptcltype p)
/* ptcltype p;*/
{ 
   return prtclbase[p-1].spin % 2 == 0 ? 
             TRUE : FALSE; 
} 


boolean  vectorp(byte p)
/* byte p;*/
{ 
   return prtclbase[p-1].spin == 2 ? 
             TRUE : FALSE; 
} 


boolean zeromass(byte p)
/* byte p;*/
{ 
   return strcmp(prtclbase[p-1].massidnt,"0") == 0 ? 
             TRUE : FALSE; 
} 


boolean photonp(byte p)
/* byte p;*/
{ 
   return vectorp(p) && zeromass(p) || pseudop(p) ?
             TRUE : FALSE; 
} 

boolean ghostp(byte p)
/* byte  p;*/
{ 
   return prtclbase[p-1].hlp == 'c' || 
          prtclbase[p-1].hlp == 'C' || 
          prtclbase[p-1].hlp == 'f' ? 
             TRUE : FALSE;
} 

boolean gaugep(int j)
{ 
   return j != 0 && prtclbase[j-1].hlp == 'G' ? 
      TRUE : FALSE;
} 

int ghostmother(int  j)
{
   if (j == 0) return 0;
      switch (prtclbase[j-1].hlp)
      {
         case 'c': return j - 1;
         case 'C': return j - 2;
         case 'f': return j - 3;
         case 't': return j + 1;
         default : return j;
      }
}

void  getprtcls(char* txt1,prtclsarray pnames)
/* char * txt1;
 prtclsarray pnames;*/
{byte   ps, j; 
 char   txt[STRSIZ],pnametmp[STRSIZ]; 

   strcpy(txt,txt1);
   ps = spos("->",txt); 
   txt[ps-1] = ','; 
   txt[ps + 1-1] = ' '; 
   sbld(txt," %s",txt); 
   for (j = nin + nout + 1; j <= MAXINOUT; j++) 
      strcpy(pnames[j-1],"***"); 
   j = 1; 
   do 
   { 
      ps = cpos(',',txt); 
      if (ps == 0) 
      { 
         trim(txt); 
         strncpy(pnames[j-1],txt,3);
         pnames[j-1][3] = '\0'; 
         return;
      } 
      strcpy(pnametmp,copy(txt,1,ps - 1)); 
      strcpy(txt,copy(txt,ps + 1,(integer)strlen(txt) - ps)); 
      trim(pnametmp); 
      strncpy(pnames[j-1],pnametmp,3); 
      pnames[j-1][3] = '\0'; 
      ++(j); 
   }  while (TRUE); 
} 

