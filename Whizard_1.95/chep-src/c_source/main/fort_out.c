#include "tptcmac.h"
#include "syst2.h"
#include "crt.h"
#include "global.h"
#include "physics.h"
#include "files.h"
#include "syst2.h"
#include "procvar.h"
#include "pvars.h"
#include "diaprins.h"
#include "optimise.h"
#include "l_string.h"
#include "parser.h"
#include "reader5.h"
#include "transfer.h"
#include "os.h"

#include "fort_out.h"

#define cnt  "     ."

typedef char s6[7];


/*************************************************
 *     CompHEP (R) FORTRAN code production       *
 *          (C) S.Shichanin Febr 08 1991         *
 *                          June 17 1991         *
 *         Mass identifiers and values           *
 *************************************************/


#define procinfptr struct procinfrec *
typedef struct procinfrec
   {
      procinfptr     next;
      word           tot;
      word           firstdiagpos;
      prtclsarray    p_name;
		int   		   p_masspos[MAXINOUT];
   }  procinfrec;
#undef procinfptr
typedef struct procinfrec *procinfptr;


#define denlist struct denlistrec *
typedef struct denlistrec
   {
      denlist      next;
      varptr       den,mass,width;
   }  denlistrec;
#undef denlist
typedef struct denlistrec *denlist;


typedef struct deninforec
   {
      word         cr_pos;
       byte         tot_den;
      struct denarr
         {
            boolean      zerowidth;
            byte         deg;
            word         dennum;
			}  denarr[2*(MAXINOUT-3)];
   }  deninforec;


static boolean      precision, include_pictures;
static char         globalname[STRSIZ];

static procinfptr   inf, inftmp;  /*  information about subProcess  */

static varlist      modl;   /*  tempory pointer to variables  */

       /*  statictics  */
static word         ndiagrtot, diagrcount;
static integer      nvars, nfunc;

static marktp      heapbeg;/*  for RELEASE  */

static integer      nden_w, nden_0, nsub1; /* from writesubprocess */


static void clearstatistic(void)
{int  i;
  for (i = 17; i < 24; i++) { goto_xy(1,i); clr_eol();}
}

static void init_stat(void)
{
   goto_xy(1,17);
   scrcolor(Yellow,LightBlue);
   print(" FORTRAN Source Codes \n");
   scrcolor(LightRed,Black);
   print(" Process..........\n");
   print(" Total diagrams...\n");
   print(" Processed........\n");
   print(" Current..........\n");
   scrcolor(Yellow,LightBlue);
   print(" Press Esc to stop    ");
   scrcolor(Yellow,Black);
   goto_xy(20,18); print("%s",processch);
   goto_xy(20,19); print("%4u",ndiagrtot);
   goto_xy(20,20); print("   0");
   scrcolor(Yellow ,Black);
   goto_xy(20,21); print("   1");
   scrcolor(Yellow,Black);
}


static void writestatistic(void)
{
   goto_xy(20,19); print("%4u",ndiagrtot);
   goto_xy(20,20);
   print("%2u (%%)",(((diagrcount - 1) * 100) / ndiagrtot));
   scrcolor(Yellow ,Black);
   goto_xy(20,21); print("%4u",diagrcount);
   scrcolor(Yellow,Black);
}


static void writpict(word ndiagr)
{vcsect vcs;
   csdiagram  csdiagr;
   fseek(diagrq,ndiagr*sizeof(csdiagr),SEEK_SET);
   FREAD1(csdiagr,diagrq);
   transfdiagr(&csdiagr,&vcs);   
   writeTextDiagram(&vcs,1,'*',fortFile);
}


static void labl(void)
{
  f_printf(fortFile,"*******************************\n");
  f_printf(fortFile,"*    %s*\n",version);
  f_printf(fortFile,"*******************************\n");
}

  /* =========== Preliminary  calculations ================ */

/* static void  calc_nvars_nfunc()*/
static void calc_nvars_nfunc(void)
{byte  k = 0;

   nvars = 0;
   nfunc = 0;
   modl = modelvars;

   while (modl != NULL)
   {  ++(k);
		if (insetb(k + maxsp+1,varsofprocess)) ++(nvars);
		if (insetb(k + maxsp+1,funcofprocess)) ++(nfunc);
      modl = modl->next;
   }
}   /* Calc_Nvars_NFunc; */


/* static void  calccutlevel()*/
static void calccutlevel(void)
{
   if (nin == 1)
		cutlevel = maxsp;
   else
      if (!inset(structfun,options))
			cutlevel = maxsp-1; /* 14 */
      else
			if (gg_exist && insetb(maxsp+2/*17*/,varsofprocess))
				cutlevel = maxsp+2; /*17*/
         else
				cutlevel = maxsp;/*15*/
}

static void prepareprocinform(void)
{word        ndel, ncalc, nrest, recpos;
 char        txt[STRSIZ];
 setofbyte   allvars;
 integer     i, k, npos;
 csdiagram   csd;
 s6          mass;
 byte        nn;
 int nsubs;

   inf = NULL;
   setofb_cpy(allvars,setofb_uni(varsofprocess, funcofprocess));
   menuq=fopen(MENUQ_NAME,"rb");
   for (nsubs=1;nsubs<=subproc_sq;nsubs++) 
   {
      inftmp = inf;
      inf = (procinfptr)getmem_((word)sizeof(procinfrec));
      inf->next = inftmp;
      rd_menu(2,nsubs,txt,&ndel,&ncalc,&nrest,&recpos);
      inf->firstdiagpos = recpos;
      getprtcls(txt,inf->p_name);
      for (i = 1; i <= nin + nout; i++)
      {
         locateinbase(inf->p_name[i-1],&nn);
         strcpy(mass,prtclbase[nn-1].massidnt);
         npos = 0;
         if (strcmp(mass,"0") != 0)
         {
            modl = modelvars;
            npos = 1;
            k = 1;
            while (strcmp(modl->varname,mass) != 0)
            {
	       if (insetb(k + maxsp+1,allvars)) ++(npos);
               ++(k);
               modl = modl->next;
            }
         }
         inf->p_masspos[i-1] = npos;
      }
		for (i = nin + nout + 1; i <= MAXINOUT; i++)
      {
         strcpy(inf->p_name[i-1],"***");
         inf->p_masspos[i-1] = 0;
      }
      fseek(diagrq,recpos*sizeof(csdiagram),SEEK_SET);
      inf->tot = 0;
      for (i = 1; i <= ndel + ncalc + nrest; i++)
      {
         FREAD1(csd,diagrq);
         if (csd.status == 1) ++(inf->tot);
      }
   }
   nsubs--;
   fclose(menuq);
   revers((pointer*)&inf);
}

/* ==========   assistent procedures =============== */

static void return_end(void)
{
   f_printf(fortFile,"%sRETURN\n",six);
   f_printf(fortFile,"%sEND\n",six);
   f_printf(fortFile,"\n");
}

/* static void  implicitdeclaration()*/
static void implicitdeclaration(void)
{ 
   f_printf(fortFile,"      IMPLICIT ");
   if (precision)
		f_printf(fortFile,"REAL*16");
   else
      f_printf(fortFile,"DOUBLE PRECISION ");
   f_printf(fortFile,"(A-H,O-Z)\n");
}

/* static char *  ext(s)
 char * s;*/
static char* ext(char* s)
{static char buf[STRSIZ];
   sbld(buf,"%s%s",s,globalname);
   return buf;
}

  /* =========== Common blocks Emit =========== */



/* static void  common_scalar()*/
static void common_scalar(void)
{
  byte i;
	f_printf(fortFile,"%sCOMMON/SCLR%s/P1,P2,P3",six,globalname);
	for(i=4;i<=maxsp;i++)
	{  if (i==18) f_printf(fortFile,"\n     .");
		f_printf(fortFile,",P");
		if (i<10) f_printf(fortFile,"%d",i);
		else      f_printf(fortFile,"%c",'A'-10+i);
	}
	f_printf(fortFile,"\n");
}



/* static void  common_scalararray()*/
static void common_scalararray(void)
{
	f_printf(fortFile,"%s%s/PP(%d)\n",six,ext("COMMON/SCLR"),maxsp);
}

/* static void  common_sqs()*/
static void common_sqs(void)
{
   if (nin > 1)
      f_printf(fortFile,"%sCOMMON/SQS%s/SQRTS\n",six,globalname);
}

static void common_vars(void)
{
   if (nvars + nfunc > 0) 
      f_printf(fortFile,
         "%sCOMMON/VARS%s/A(%d)\n",six,globalname,nvars + nfunc); 
} 

static void common_log(void)
{
  f_printf(fortFile,"%s%s/L(%u)\n",six,ext("COMMON/LOGG"),subproc_sq);}

  /* ======= Information functions =========== */

static void geninf(char* name,integer value)
{
   f_printf(fortFile,"%sFUNCTION %s%s()\n",six,name,globalname);
   f_printf(fortFile,"%s   %s%s=%d\n",six,name,globalname,value);
   return_end(); 
} 

/* static void  make_indx()*/
static void make_indx(void)
{ 
   f_printf(fortFile,"%sFUNCTION INDX%s(K,L)\n",six,globalname);
   f_printf(fortFile,"%sI=MIN(K,L)\n",six);
   f_printf(fortFile,"%sJ=MAX(K,L)\n",six);
   f_printf(fortFile,"%sINDX%s=I+(J-1)*(J-2)/2\n",six,globalname);
   return_end();
}

static void scanvars(int mode)
{byte        mcou;
 int num;
 boolean     first, condition;
 shortstr    valstr;

   modl = modelvars;
   mcou = 11;
   first = TRUE;
   switch(mode)
   {
      case 2: /*  f_printf(f->stream,"%sDATA A    /",six); */
        first = FALSE;
        num=0;
      break;

      case 3:   f_printf(fortFile,"%sDATA NAMES/",six);
      break;

/*		case 4:   f_printf(fortFile,"%sDATA NUNIT/",six); */
   }   /* case */
   while (modl != NULL)
   {
      condition =
         insetb(calcvarpos(modl->varname), varsofprocess) ||
         (insetb(calcvarpos(modl->varname),funcofprocess) &&
         mode == 2);
      if (condition)
      {
         switch(mode)
         {
	    case 2:
	    {  char buff[20];
	       doub_str(buff,modl->varvalue);
	       num++;
               f_printf(fortFile,"%sA(%d)=%s\n",six,num,buff);
	    }
            break;

            case 3:   sbld(valstr,"\'%s\'",modl->varname);
            break;

         }   /* CASE */
         if (mode !=2 )
         {
            if (first)
               first = FALSE;
            else
            {  f_printf(fortFile,",");
               ++(mcou);
            }
            if (mcou + (word)strlen(valstr) > 62)
            {
               f_printf(fortFile,"\n");
               f_printf(fortFile,"%s",cnt);
               mcou = 0;
            }
            f_printf(fortFile,"%s",valstr);
            mcou += (byte)strlen(valstr);
         }
      }
      modl = modl->next;
   }
   if (mode !=2)  f_printf(fortFile,"/\n");
}


static void  writesubroutineinit(void)
{varlist      p;
 byte         k;
 char        * s, *ss;
 int          len,xx;
 char         c;

   f_printf(fortFile,"%sSUBROUTINE INIT%s\n",six,globalname);
   implicitdeclaration();
   common_log();

   f_printf(fortFile,"%sLOGICAL RECALC \n",six);

   if (nvars != 0)
   {
      f_printf(fortFile,"%sDIMENSION  AMEM(%d)\n",six,nvars);
      common_vars();
   }
	if (cutlevel == maxsp-1 /*14*/) common_scalararray();
   f_printf(fortFile,"%sSAVE\n",six);

   f_printf(fortFile,"%sDATA RECALC/.TRUE./,P1MEM/0.D0/\n",six);
	if (nvars > 0 && !(nvars == 1 && cutlevel == maxsp+2))
   {
		if (cutlevel == maxsp+2)
         f_printf(fortFile,"%sDO 1 I=2,%d\n",six,nvars);
      else
         f_printf(fortFile,"%sDO 1 I=1,%d\n",six,nvars);

      f_printf(fortFile,"%s   IF (A(I).NE.AMEM(I)) THEN\n",six);
      f_printf(fortFile,"%s      RECALC=.TRUE.\n",six);
      f_printf(fortFile,"%s      AMEM(I)=A(I)\n",six);
      f_printf(fortFile,"%s   ENDIF\n",six);
      f_printf(fortFile,"1     CONTINUE\n");
   }

	if (cutlevel == maxsp-1 /*14*/)
   {
      f_printf(fortFile,"%sIF (PP(1).NE.P1MEM)  THEN\n",six);
      f_printf(fortFile,"%s   RECALC=.TRUE.\n",six);
      f_printf(fortFile,"%s   P1MEM=PP(1)\n",six);
      f_printf(fortFile,"%sENDIF\n",six);
   }
   f_printf(fortFile,"%sIF (RECALC) THEN\n",six);
   f_printf(fortFile,"%s   DO 2 I=1,%u\n",six,subproc_sq);
   f_printf(fortFile,"2        L(I)=0\n");
   p = modelvars;
	k = maxsp+2;
   while (p != NULL)
   {
      if (insetb(k,funcofprocess))
		{
			ss=(char *)readexprassion(p->func,bact5,uact5,rd5);
			s=ss+3;

			f_printf(fortFile,"%s   %s=",six,(*varnamesptr)[k-1]);
			len=strlen(s);

			xx=65 - ( 4+strlen((*varnamesptr)[k-1]) );
			if(len>xx) { c=s[xx];s[xx]=0;}
			f_printf(fortFile,"%s\n",s);
			if (len>xx) {s[xx]=c; s+=xx;}
			len -= xx;

			while (len>0)
			{
				if (len >65){c=s[65]; s[65]=0; }
				f_printf(fortFile,"     .%s\n",s);
				if (len >65){s[65]=c; s+=65;}
				len -= 65;
			}
			free(ss);
      }
      ++(k);
      p = p->next;
   }
   f_printf(fortFile,"%sRECALC=.FALSE.\n",six);
   f_printf(fortFile,"%sENDIF\n",six);
   return_end();
}

static void common_width(void)  
{
   f_printf(fortFile,"%sLOGICAL GWIDTH,RWIDTH\n",six);
   f_printf(fortFile,"%sCOMMON/%s/ GWIDTH,RWIDTH\n",six,ext("WDTH"));
}
static void  make_vini(void)
{
   f_printf(fortFile,"%sSUBROUTINE  %s\n",six,ext("VINI"));
	implicitdeclaration();
	f_printf(fortFile,"%sDOUBLE PRECISION SQRTS\n",six);
        common_vars();
	common_sqs();
	common_log();
        common_width();
	f_printf(fortFile,"%sSAVE\n",six);
	f_printf(fortFile,"%sGWIDTH=.FALSE.\n",six);
        f_printf(fortFile,"%sRWIDTH=.FALSE.\n",six);
	if (nin > 1)
		{ char buff[40];
		  doub_str(buff,sqrts);
		  f_printf(fortFile,"%sSQRTS=%s\n",six,buff);
		}
	if (nvars + nfunc > 0) scanvars(2);
    return_end();
/* 
   f_printf(fortFile,"%sRETURN\n",six);
   f_printf(fortFile,"%sEND\n",six);
   f_printf(fortFile,"\n");
*/
}

static void make_cpth(void)
{
   f_printf(fortFile,"%sSUBROUTINE %s(PATH,D_SLASH,F_SLASH)\n",    
               six,ext("CPTH"));
   f_printf(fortFile,"%sCHARACTER*60 PATH\n",six);
   f_printf(fortFile,"%sCHARACTER*1 D_SLASH,F_SLASH\n",six);
   
   f_printf(fortFile,"%sPATH=\'%s\'\n",six,_pathtocomphep);
   f_printf(fortFile,"%sD_SLASH=\'%c\'\n",six,d_slash);
   f_printf(fortFile,"%sF_SLASH=\'%c\'\n",six,f_slash);
   return_end();
}

static void  common_den(void)
{
   if (nden_0 + nden_w == 0) return;
   f_printf(fortFile,"%s%s/ ",
      six,ext(scat("COMMON /U%s",funstr(nsub1))));
   if (nden_w != 0)
      f_printf(fortFile,"Q0(%d),",nden_w);
   f_printf(fortFile,"Q1(%d),Q2(%d)\n",nden_0 + nden_w,nden_0 + nden_w);
}

static void  onediagram(deninforec * dendescript)
{catrec      cr;
 byte        nexpr, i;
 word        k;
 marktp      bh;
 varptr      totnum, totdenum, rnum;
 longstrptr  tmplongs;
 char        istr[10];
 boolean     addpr;
 integer numm;
 int linepos;

   mark_(&bh);
   initConsts();
   initdegnames();

   fseek(catalog,dendescript->cr_pos,SEEK_SET);
   FREAD1(cr,catalog);
   ++(diagrcount);

   fseek(archiv,cr.factpos,SEEK_SET);
   FREAD1(nexpr,archiv);  /*  Nexpr must be equal 2  */

   readpolynom(&totnum);
   readpolynom(&totdenum);
   fseek(archiv,cr.rnumpos,SEEK_SET);
   FREAD1(nexpr,archiv);   /*  Nexpr must be equal 1  */
   readpolynom(&rnum);
   putnames();
   
   fortFile=fopen(scat("%sresults%cc%d.f",pathtouser,f_slash,diagrcount),"w");
   
   labl();

   f_printf(fortFile,"%sSUBROUTINE CC%d(C)\n",six,diagrcount);
   implicitdeclaration();
   common_vars();
   common_scalar();
   linepos=ftell(fortFile);
   f_printf(fortFile,
   "                                                                        \n");
   f_printf(fortFile,"%sSAVE\n",six);   
   tmpNameMax=0;
   write_const();
   return_end(); 

   fseek(fortFile,linepos,SEEK_SET);
   f_printf(fortFile,"%sDIMENSION C(%d)" ,six,MAX(1,constcount));
   if(degnamecount>0) f_printf(fortFile,",S(%d)",degnamecount);                  
   if(tmpNameMax>0)   f_printf(fortFile,",tmp(%d)",tmpNameMax);                                          
   fclose(fortFile);

   fortFile=fopen(scat("%sresults%cf%d.f",pathtouser,f_slash,diagrcount),"w");
   cleardegnames();
   initdegnames(); 
   tmpNameMax=0;
   labl();
   sbld(istr,"F%u%s",diagrcount,globalname);

   f_printf(fortFile,"%sFUNCTION %s()\n",six,istr);
   if (include_pictures) writpict(cr.ndiagr_ + inftmp->firstdiagpos - 1);
   implicitdeclaration();
   common_log();
   common_vars();
   common_scalar();
   common_den();
   linepos=ftell(fortFile);
   f_printf(fortFile,
   "                                                                        \n");
   f_printf(fortFile,"%sSAVE\n",six);

            
   f_printf(fortFile,"%sIF(L(%d).EQ.0) CALL CC%d%s(C)\n",six,
                           cr.nsub_,diagrcount,globalname);

   fortwriter("TOTNUM",totnum);
   fortwriter("TOTDEN",totdenum);
   fortwriter("RNUM",rnum);

   tmplongs = (longstrptr)getmem(sizeof(longstr));
   tmplongs->len = 0;
   addstring(tmplongs,"RNUM*(TOTNUM/TOTDEN)");

   for (i = 1; i <= dendescript->tot_den; i++)
   {  numm =
         dendescript->denarr[i-1].zerowidth ?
            dendescript->denarr[i-1].dennum + nden_w :
            dendescript->denarr[i-1].dennum;
      if (dendescript->denarr[i-1].deg == 1)
         addstring(tmplongs,scat("*Q1(%d)",numm));
      else
         addstring(tmplongs,scat("*Q2(%d)",numm));
   }
   writelongstr(istr,tmplongs);

	tmplongs->len=0;
	addstring(tmplongs,istr);

	for (k = 1; k <= nden_w; k++)
	{
           addpr = TRUE;
           for (i = 1; i <= dendescript->tot_den; i++)
           if (!dendescript->denarr[i-1].zerowidth &&
		     k == dendescript->denarr[i-1].dennum)  addpr = FALSE;
           if (addpr)  addstring(tmplongs,scat("*Q0(%u)",k));
        }
	if (  tmplongs->len >  strlen(istr) ) writelongstr(istr,tmplongs);
   free(tmplongs);

   return_end();

   fseek(fortFile,linepos,SEEK_SET);
   f_printf(fortFile,"%sDIMENSION C(%d)" ,six,MAX(1,constcount)); 
   if(degnamecount>0) f_printf(fortFile,",S(%d)",degnamecount);
   if(tmpNameMax>0)   f_printf(fortFile,",tmp(%d)",tmpNameMax);                                          
                                            
   fclose(fortFile);
   release_(&bh);
   cleardegnames();
}  /* OneDiagram */

static void  writesubprocess(integer nsub,boolean* breaker)
{ denlist   den_w, den_0, den_tmp;
  varptr     den,mass,width;
  catrec    cr;
  marktp    bh,denmark ;
  word      i, n;
  char      istr[12];
  deninforec   dendescript;
  FILE * fd;                /* file of (deninforec)  */
  char fd_name[STRSIZ];
  int file_pos;
   nsub1 = nsub;

   mark_(&bh);
 
   sprintf(fd_name,"%stmp%cden.inf",pathtouser,f_slash);

   fd=fopen(fd_name,"wb"); 
   
   fortFile=fopen(scat("%sresults%cd%s.f",pathtouser,f_slash,funstr(nsub)),"w");
   f_printf(fortFile,"%sSUBROUTINE %s\n",
                     six,ext(scat("D%s",funstr(nsub))));
   implicitdeclaration();
   common_log();
   common_vars();
   common_width();
   common_scalar();
   file_pos=ftell(fortFile);
   f_printf(fortFile,
   "                                                                       \n");
  /* ================== */
   initConsts();
   initdegnames();
   tmpNameMax=0;
   nden_w = 0;
   nden_0 = 0;

   den_w = NULL;
   mass  = NULL;
   width = NULL;
   den_0 = NULL;

   fseek(catalog,0,SEEK_SET);
   while (FREAD1(cr,catalog))
   {
      if (cr.nsub_ == nsub)
      {
         dendescript.cr_pos = ftell(catalog) - sizeof(cr);
         fseek(archiv,cr.denompos,SEEK_SET);
         FREAD1(dendescript.tot_den,archiv); 
         for (i = 1; i <= dendescript.tot_den; i++)
         {
            FREAD1(dendescript.denarr[i-1].deg,archiv);
	    mark_(&denmark);
            readpolynom(&width);
            readpolynom(&mass);
            readpolynom(&den);
            if (width == NULL)
            {
               dendescript.denarr[i-1].zerowidth = TRUE;
               den_tmp = den_0;
               n = 0;
               while (den_tmp != NULL)
                  if (equalexpr(den,den_tmp->den))
                  {
                     den_tmp = NULL;
		     release_(&denmark);
                  }
                  else
                  {
                     den_tmp = den_tmp->next;
                     ++(n);
                  }
               dendescript.denarr[i-1].dennum = nden_0 - n;
               if (n >= nden_0) 
               {
                  den_tmp = (denlist)getmem_((word)sizeof(denlistrec));
                  den_tmp->next = den_0;
                  den_tmp->den = den;
                  den_tmp->mass= mass;
                  den_tmp->width=width; /*  NULL*/
                  den_0 = den_tmp;
                  ++(nden_0);
                  dendescript.denarr[i-1].dennum = nden_0;
               }
            }
            else
            {
               dendescript.denarr[i-1].zerowidth = FALSE;
               den_tmp = den_w;
               n = 0;
               while (den_tmp != NULL)
               if (equalexpr(den,den_tmp->den) &&
                   equalexpr(width,den_tmp->width))
               {
                  den_tmp = NULL;
		  release_(&denmark);
               }
               else
               {
                  den_tmp = den_tmp->next;
                  ++(n);
               }
               dendescript.denarr[i-1].dennum = nden_w  - n;
               if (n >= nden_w)
               {
                  den_tmp = (denlist)getmem_((word)sizeof(denlistrec));
                  den_tmp->next = den_w;
                  den_tmp->den = den;
                  den_tmp->mass=mass;
                  den_tmp->width=width;
                  den_w = den_tmp;
                  ++(nden_w);
                  dendescript.denarr[i-1].dennum = nden_w;
               }
            }
         }
         FWRITE1(dendescript,fd);
      }  /* if CR.nsub_ =nsub */
   } 
   common_den();
   putnames();
   if(nden_w!=0)f_printf(fortFile,
       "%sDIMENSION DMASS(%d),DWIDTH(%d)\n",six,nden_w,nden_w);
   f_printf(fortFile,"%sSAVE\n",six);

   f_printf(fortFile,"%sGOTO(1),L(%d)\n",six,cr.nsub_);
   write_const();

   f_printf(fortFile,"1     CONTINUE\n");


   revers((pointer*)&den_w);
   den_tmp=den_w;
   
   i=1;
   while (den_w != NULL)
   {
       sbld(istr,"DMASS(%u)",i);   fortwriter(istr,den_w->mass);
       sbld(istr,"DWIDTH(%u)",i);  fortwriter(istr,den_w->width);
       den_w = den_w->next; 
       ++(i);
   }  

   i = 1; den_w=den_tmp;   
   while (den_w != NULL)
   {
       sbld(istr,"Q1(%u)",i);
       fortwriter(istr,den_w->den);
       den_w = den_w->next;
       ++(i);
   }


   revers((pointer*)&den_0);
   while (den_0 != NULL)
   {
       sbld(istr,"Q1(%u)",i);
       fortwriter(istr,den_0->den);
       den_0 = den_0->next;
       ++(i);
   }
   release_(&bh);
   cleardegnames();

   if (nden_w > 0)
   {
      f_printf(fortFile, "%sDO 2 I=1,%d\n", six, nden_w);
      f_printf(fortFile, "%sIF (RWIDTH) THEN\n",six);
      f_printf(fortFile,
 "%s  Q2(I)=1/(Q1(I)**2+((DMASS(I)-Q1(I)/DMASS(I))*DWIDTH(I))**2)\n",six);
      f_printf(fortFile, "%sELSE\n",six);
      f_printf(fortFile, "%s  Q2(I)=1/(Q1(I)**2+(DMASS(I)*DWIDTH(I))**2)\n", six);
      f_printf(fortFile, "%sENDIF\n",six);
      f_printf(fortFile, "%sIF (GWIDTH) THEN\n",six);
      f_printf(fortFile, "%s  Q0(I)=Q2(I)*Q1(I)**2\n", six); 
      f_printf(fortFile, "%sELSE\n",six);
      f_printf(fortFile, "%s  Q0(I)=1 \n", six);
      f_printf(fortFile, "%sENDIF\n",six);
      f_printf(fortFile, "%s  Q1(I)=Q2(I)*Q1(I)\n", six);
      f_printf(fortFile, "2     CONTINUE\n");
   }

   if (nden_0 > 0)
   {
      f_printf(fortFile, "%sDO 3 I=%d,%d\n", six, 1+nden_w, nden_w + nden_0);
      f_printf(fortFile, "%s  Q1(I)=1/Q1(I)\n", six);
      f_printf(fortFile, "%s  Q2(I)=Q1(I)**2\n", six);
      f_printf(fortFile, "3     CONTINUE\n");
   }

  /* ===================== */
   return_end();
   fseek(fortFile,file_pos,SEEK_SET);
   f_printf(fortFile,"%sDIMENSION C(%d)" ,six,MAX(1,constcount)); 
   if(degnamecount>0) f_printf(fortFile,",S(%d)",degnamecount);
   if(tmpNameMax>0)   f_printf(fortFile,",tmp(%d)",tmpNameMax); 
   fclose(fortFile);
   fclose(fd);
   fd=fopen(fd_name,"rb");
   *breaker = FALSE;
   while(FREAD1(dendescript,fd) == 1)
   {
      if (escpressed())
      {  *breaker = TRUE;
         break;
      }
      onediagram(&dendescript);
      writestatistic();
   } 
   
   fclose(fd);
   unlink(fd_name);
}  /*  WriteSubprocess  */


static void  sim(int placeswap,int placesub,int howmanysub)
{char  b[STRSIZ];

   sbld(b,"%s%s",six,"SME");
   f_printf(fortFile,"%s%s%d(%d,%d)\n",b,ext("=SBS"),howmanysub,
                                                            placesub,nsub);

   f_printf(fortFile,"%s%s(%d,%d)\n",six,ext("CALL SWAP"),placeswap + 1,
                                                            placeswap + 2);
   sbld(b,"%s=(%s",b,"SME");
   f_printf(fortFile,"%s%s%d(%d,%d))/4\n", b,ext("+SBS"),howmanysub,
                                                            placesub,nsub);
}

static void  comby(prtclsarray name)
{int     bas[5];
 int		place[2], howmany[2];
 int		group, placeswap, placesub, howmanysub;
 int		i, j, n0;
 char        b[STRSIZ];
   bas[0] = 1; bas[1] = 2; bas[2] = 6; bas[3] = 24; bas[4] = 120;
   /* Nested function: sim */

   n0 = 1;
   i = 1;
   group = 0;

   while (i < nout)
   {
      j = i + 1;
      while (j <= nout && strcmp(name[i + nin-1],name[j + nin-1]) == 0)
         ++(j);
      if (j > i + 1)
      {
         ++(group);
         place[group-1] = nin + i - 1;
         howmany[group-1] = j - i;
         n0 = n0 * bas[j - i-1];
      }
      i = j;
   }
	if (group < 2) sbld(b,"%s%s",six,"SME");
   switch (group)
   {
      case 0:  f_printf(fortFile,"%s%s(%d)\n",
                  b,ext("=SMPL"),nsub);
      break;

		case 1:  f_printf(fortFile,"%s%s%d(%d,%d)/%d\n",
						b,ext("=SBS"),howmany[0],place[0],nsub,n0);
      break;

      case 2:  placeswap = place[0];
               placesub = place[1];
               howmanysub = howmany[1];
               if (howmany[0] != 2)
               {
                  placeswap = place[1];
                  placesub = place[0];
                  howmanysub = howmany[0];
               }
               sim(placeswap,placesub,howmanysub);
   }   /* Case */
}


static void  make_pinf(void)
{
   byte         i;

   f_printf(fortFile,
      "%s%s(NSUB,NPRTCL)\n",six,ext("FUNCTION PINF"));
	f_printf(fortFile,"%sCHARACTER*6 %s,NAMES(%u,%d)\n",
      six, ext("PINF"),subproc_sq,nin + nout);
   inftmp = inf;
   for (nsub = 1; nsub <= subproc_sq; nsub++)
   {
      f_printf(fortFile,"%sDATA ( NAMES(%d,I),I=1,%d)/",
         six,nsub,nin + nout);
      for (i = 1; i <= nin + nout; i++)
      {
         f_printf(fortFile,"%c%s%c",39,inftmp->p_name[i-1],39);
         if (i == nin + nout)
            f_printf(fortFile,"/\n");
         else
            f_printf(fortFile,",");
      }
      inftmp = inftmp->next;
   }
   f_printf(fortFile,"%s%s=NAMES(NSUB,NPRTCL)\n",six,ext("PINF"));
   return_end();
}

static void  make_pmass(void)
{int  i;

   f_printf(fortFile,"%sSUBROUTINE PMAS%s(NSUB,NPRTCL,VAL)\n",
      six,globalname);
   implicitdeclaration();
	f_printf(fortFile,"%sDIMENSION NVALUE(%u,%d)\n",
      six,subproc_sq,nin + nout);
   common_vars();
   f_printf(fortFile,"%sSAVE\n",six);
   inftmp = inf;
   for (nsub = 1; nsub <= subproc_sq; nsub++)
   {
		f_printf(fortFile,"%sDATA ( NVALUE(%d,I),I=1,%d)/",
         six,nsub,nin + nout);
      for (i = 1; i <= nin + nout; i++)
      {
			f_printf(fortFile,"%d",inftmp->p_masspos[i-1]);
         if (i == nin + nout)
            f_printf(fortFile,"/\n");
         else
            f_printf(fortFile,",");
      }
      inftmp = inftmp->next;
   }
   f_printf(fortFile,"%sN=NVALUE(NSUB,NPRTCL)\n",six);
   f_printf(fortFile,"%sIF (N.GT.%u) CALL INIT%s\n",
      six,nvars,globalname);
   f_printf(fortFile,"%sIF (N.EQ.0)  THEN \n",six);
   f_printf(fortFile,"%s   VAL=0\n",six);
   if (nvars + nfunc != 0)
   {
      f_printf(fortFile,"%sELSE\n",six);
      f_printf(fortFile,"%s   VAL=A(N)\n",six);
   }
   f_printf(fortFile,"%sENDIF\n",six);
   return_end();
}

static void  make_sqme(void)
{
   f_printf(fortFile,"%s%s(NSUB)\n",six,ext("FUNCTION SQME"));
   implicitdeclaration();
   f_printf(fortFile,"%sDOUBLE PRECISION %s\n",six,ext("SQME"));
   f_printf(fortFile,"%sSAVE\n",six);
   f_printf(fortFile,"%s%s\n",six,ext("CALL INIT"));

   inftmp = inf;
   for (nsub = 1; nsub <= subproc_sq; nsub++)
   {
      if (nsub == 1)
         f_printf(fortFile,"%sIF(NSUB.EQ.%d) THEN\n",six,nsub);
      else
         f_printf(fortFile,"%sELSE IF(NSUB.EQ.%d) THEN\n",six,nsub);
      comby(inftmp->p_name);
      inftmp = inftmp->next;
   }
   f_printf(fortFile,"%sEND IF\n",six);
   f_printf(fortFile,"%s%s=SME\n",six,ext("SQME"));

   return_end();

}


static void  make_fosimple(void)
{word        i, totcount;
 longstrptr  simpleans;
 char        b[STRSIZ];
 int fileBeg,fileEnd;

   f_printf(fortFile,"%s%s(NSUB)\n",six,ext("FUNCTION SMPL"));
   implicitdeclaration();
   common_log();
   fileBeg=ftell(fortFile);
   f_printf(fortFile,
   "                                                                       \n");
   tmpNameNum=0;

   f_printf(fortFile,"%sSAVE\n",six);
   inftmp = inf;
   totcount = 0;
   for (nsub = 1; nsub <= subproc_sq; nsub++)
   {
      if (nsub == 1)
         f_printf(fortFile,"%sIF(NSUB.EQ.%d) THEN\n",six,nsub);
      else
         f_printf(fortFile,"%sELSE IF(NSUB.EQ.%d) THEN\n",six,nsub);

      if (inftmp->tot == 0)
         f_printf(fortFile,"%s%s=0\n",six,ext("SMPL"));
      else
      {
         f_printf(fortFile,"%sCALL %s\n",
            six,ext(scat("D%s",funstr(nsub))));
         simpleans = (longstrptr)getmem(sizeof(longstr));
         simpleans->len = 0;
         for (i = 1; i <= inftmp->tot; i++)
         {  sbld(b,"+F%s",funstr(totcount + i));
            sbld(b,"%s()",ext(b));
            addstring(simpleans,b);
         }
         writelongstr(ext("SMPL"),simpleans);
         free(simpleans);
         totcount += inftmp->tot;
      }
      inftmp = inftmp->next;
   }
   f_printf(fortFile,"%sEND IF\n",six);
   f_printf(fortFile,"%sL(NSUB)=1\n",six);
   return_end();     
   if(tmpNameNum>0)
   { 
     fileEnd=ftell(fortFile);
     fseek(fortFile,fileBeg,SEEK_SET);
     f_printf(fortFile,"%sDIMENSION tmp(%d)",six,tmpNameNum);                                          
     fseek(fortFile,fileEnd,SEEK_SET);   
   }  

}

static void  make_asgn(void)

{
   f_printf(fortFile,"%s%s(NUMVAR,VALNEW)\n",
      six,ext("SUBROUTINE ASGN"));
	implicitdeclaration();
	f_printf(fortFile,"%sDOUBLE PRECISION VALNEW\n",six);
   common_vars();
   f_printf(fortFile,"%sSAVE\n",six);
   if (nvars > 0)
   {
      f_printf(fortFile,
         "%sIF((NUMVAR.LT.1).OR.(NUMVAR.GT.%d)) RETURN\n",six,nvars);
      f_printf(fortFile,"%sA(NUMVAR)=VALNEW\n",six);
   }
   return_end();
}

static void  make_vinf(void)
{
	f_printf(fortFile,"%s%s(NUMVAR,NAME,VAL)\n",
      six,ext("SUBROUTINE VINF"));
	implicitdeclaration();
	f_printf(fortFile,"%sDOUBLE PRECISION VAL\n",six);
   common_vars();
   if (nvars != 0)
   {
      f_printf(fortFile,"%sCHARACTER*6 NAMES(%d),NAME\n",six,nvars);
/*		f_printf(fortFile,"%sDIMENSION  NUNIT(%d)\n",six,nvars); */
      f_printf(fortFile,"%sSAVE\n",six);
      scanvars(3);
/*		scanvars(4); */
      f_printf(fortFile,"%sIF (NUMVAR.GT.%d) RETURN\n",six,nvars);
      f_printf(fortFile,"%sNAME=NAMES(NUMVAR)\n",six);
      f_printf(fortFile,"%sVAL=A(NUMVAR)\n",six);
/*		f_printf(fortFile,"%sNGEV=NUNIT(NUMVAR)\n",six); */
   }
   return_end(); 
} 

/*  $I FORTMAKE\addswap */   /*    addswap(......)  */

static void  addswap(void)
{char b[STRSIZ];

   f_printf(fortFile,"%s%s(ISHFT,NSUB)\n",
      six,ext("FUNCTION SBS2")); 
   implicitdeclaration(); 
   f_printf(fortFile,"%s%s=0\n",six,ext("SBS2"));
   f_printf(fortFile,"%sDO 1 I=1,2\n",six);
   f_printf(fortFile,"%sCALL %s(2+ISHFT,1+ISHFT)\n",six,ext("SWAP"));
   sbld(b,"%s%s",six,ext("SBS2"));
   sbld(b,"%s%s",b,ext("=SBS2"));
   f_printf(fortFile,"%s%s(NSUB)\n",b,ext("+SMPL"));
   f_printf(fortFile,"1      CONTINUE\n");
   return_end(); 

   f_printf(fortFile,"%s%s(ISHFT,NSUB)\n",
      six,ext("FUNCTION SBS3")); 
   implicitdeclaration(); 
   f_printf(fortFile,"%s%s=0\n",six,ext("SBS3"));
   f_printf(fortFile,"%sDO 1 I=1,3\n",six);
   f_printf(fortFile,"%s%s(3+ISHFT,2+ISHFT)\n",
      six,ext("CALL SWAP")); 
   sbld(b,"%s%s",six,ext("SBS3"));
   sbld(b,"%s%s",b,ext("=SBS3"));
   sbld(b,"%s%s(NSUB)\n",b,ext("+SMPL"));
   f_printf(fortFile,"%s",b);
   f_printf(fortFile,"%s%s(3+ISHFT,1+ISHFT)\n",
      six,ext("CALL SWAP")); 
   f_printf(fortFile,"%s",b);
   f_printf(fortFile,"1      CONTINUE\n");
   return_end(); 

   f_printf(fortFile,"%s%s(ISHFT,NSUB)\n",six,ext("FUNCTION SBS4"));
   implicitdeclaration(); 
   f_printf(fortFile,"%s%s=0\n",six,ext("SBS4"));
   f_printf(fortFile,"%sDO 1 I=1,3\n",six);
   f_printf(fortFile,"%s%s(4+ISHFT,I+ISHFT)\n",
      six,ext("CALL SWAP")); 
   sbld(b,"%s%s",six,ext("SBS4"));
   sbld(b,"%s%s",b,ext(" =SBS4"));
   sbld(b,"%s%s(ISHFT,NSUB)\n",b,ext("+SBS3"));
   f_printf(fortFile,"%s",b);
   f_printf(fortFile,"%s%s(4+ISHFT,I+ISHFT)\n",
      six,ext("CALL SWAP")); 
   f_printf(fortFile,"1      CONTINUE\n");
   sbld(b,"%s%s",six,ext("SBS4"));
   sbld(b,"%s%s",b,ext("=SBS4"));
   sbld(b,"%s%s(ISHFT,NSUB)\n",b,ext("+SBS3"));
   f_printf(fortFile,"%s",b);
   return_end();

   f_printf(fortFile,"%s%s(ISHFT,NSUB)\n",
      six,ext("FUNCTION SBS5")); 
   implicitdeclaration(); 
   f_printf(fortFile,"%s%s=0\n",six,ext("SBS5"));
   f_printf(fortFile,"%sDO 1 I=1,4\n",six);
   f_printf(fortFile,"%s%s(5+ISHFT,I+ISHFT)\n",
      six,ext("CALL SWAP")); 
   sbld(b,"%s%s",six,ext("SBS5"));
   sbld(b,"%s%s",b,ext("=SBS5"));
   sbld(b,"%s%s(ISHFT,NSUB)\n",b,ext("+SBS4"));
   f_printf(fortFile,"%s",b);
   f_printf(fortFile,"%s%s(5+ISHFT,I+ISHFT)\n",
      six,ext("CALL SWAP")); 
   f_printf(fortFile,"1      CONTINUE\n");
   f_printf(fortFile,"%s",b);
   return_end(); 

   f_printf(fortFile,"%s%s(I,J)\n",six,ext("SUBROUTINE SWAP"));
   implicitdeclaration();
   common_scalararray();
   f_printf(fortFile,"%sSAVE\n",six);
   sbld(b,"%sDO 1 I0=1,%s()+",six,ext("NIN"));
   f_printf(fortFile,"%s%s()\n",b,ext("NOUT"));
   f_printf(fortFile,"%s  IF(I0.NE.I.AND.I0.NE.J)THEN\n",six);
   f_printf(fortFile,"%s    A=PP(%s(I0,I))\n",six,ext("INDX"));
   f_printf(fortFile,"%s    PP(%s(I0,I))=PP(%s(I0,J))\n",
      six,ext("INDX"),ext("INDX"));
   f_printf(fortFile,"%s    PP(%s(I0,J))=A\n",six,ext("INDX"));
   f_printf(fortFile,"%s  END IF\n",six);
	f_printf(fortFile,"1%sCONTINUE\n",six);
   return_end();
}

static void zeroHeep(void)
{ goto_xy(1,1);print("Heep is empty!!!");inkey();
exit(0);
}


void  fortprg(boolean precision1,boolean include_pictures1,
		 char* globalname1)
{boolean breaker;

   memerror=zeroHeep;
   precision = precision1;
   include_pictures = include_pictures1;
   strcpy(globalname,globalname1);

  /* ======= Initialisation parth ======= */

	mark_(&heapbeg);
   diagrq=fopen(DIAGRQ_NAME,"rb");

   calccutlevel();

   initvarnames();
   prepareprocinform();

   calc_nvars_nfunc(); ;

  /* ======= End of Initialisation ====== */
   fortFile=fopen(scat("%sresults%cservice.f",pathtouser,f_slash),"w");   
   labl();

   geninf("NIN",nin);
   geninf("NOUT",nout);
   if (precision)
      geninf("LENR",16);
   else
      geninf("LENR",8);
   geninf("NPRC",subproc_sq);

   make_pinf();
   
   geninf("NVAR",nvars);
   geninf("NFUN",nfunc);
   make_vinf();
   
   make_asgn();
   
	make_pmass();
	
/*	make_giw(); */
   writesubroutineinit();
	make_vini();
        make_cpth();
   fclose(fortFile);

   fortFile=fopen(scat("%sresults%csqme.f",pathtouser,f_slash),"w");
   labl();

   make_sqme();
   make_indx();
   addswap();
   make_fosimple();
   fclose(fortFile);

   archiv=fopen(ARCHIV_NAME,"rb");
   catalog=fopen(CATALOG_NAME,"rb");
   
   fseek(catalog,0,SEEK_END);
   ndiagrtot = ftell(catalog)/sizeof(catrec);

   diagrcount = 0;
   inftmp = inf;
   init_stat();

   for (nsub = 1; nsub <= subproc_sq; nsub++)
   {
      if (inftmp->tot != 0)   /*  this subprocess IN archive  */
      {
         writesubprocess(nsub,&breaker);
         
         if (breaker) goto exi;
      }
      inftmp = inftmp->next;
   }

exi:
   clearstatistic();
   fclose(catalog);
   fclose(archiv);
   fclose(diagrq);
   release_(&heapbeg);
}
