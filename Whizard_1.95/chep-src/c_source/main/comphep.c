#include "tptcmac.h"
#include "syst2.h"
#include "os.h"
#include "crt.h"
#include "global.h"
#include "physics.h"
#include "screen.h"
#include "crt_util.h"
#include "lbl.h"
#include "constr.h"
#include "read_mdl.h"
#include "sos.h"
#include "ent_proc.h"
#include "files.h"
#include "squar.h"
#include "r_code.h"
#include "fort_out.h"
#include "f90_out.h"
#include "red_out.h"
#include "math_out.h"
#include "form_out.h"
#include "symbolic.h"
#include "m_utils.h"
#include "help.h"
#include "cs_22.h"
#include "width_12.h"
#include "width_13.h"

#include <stdlib.h>

int      k1, k2, k3, k4,k5, k6;
char     exitlevel;
/* 0-Start; 1-Restart; 2-Heap Error,3-Edit Model,4-UserBreak */
searchrec    dirinfo;
byte         haltnumber;
char         ext[4];
integer      doserror;


int main(int argc,char** argv)
{   /*  MAIN - COMPHEP  */
	int n;
  void * pscr1=NULL; 
  void * pscr2=NULL;
  void * pscr3=NULL;
  void * pscr4=NULL;
  void * pscr5=NULL;
  void * pscr6=NULL;

	strcpy(_pathtocomphep,argv[0]);
	n = strlen(_pathtocomphep)-1;
	while (n>=0 &&  _pathtocomphep[n] != f_slash) n--;
        _pathtocomphep[n]='\0';
	strcpy(_pathtouser,defaultPath);

	for ( n=1;n<=(argc-1);n++)
	{ 	  
	  if (strcmp(argv[n],"blind")==0 ) blind=TRUE;
	  if (strcmp(argv[n],"warm")==0 ) {warm=TRUE;}

	}
        sprintf(pathtouser,"%s%c",_pathtouser,d_slash);
        sprintf(pathtocomphep,"%s%c",_pathtocomphep,d_slash);   
        sprintf(pathtohelp,"%shelp%c",pathtocomphep,f_slash);
        sprintf(pathtoresults,"%sresults",pathtouser);
        
        restoreent(&nsub,&ndiagr,&n_model,&exitlevel);

	start1(version,scat("%s%s",pathtocomphep,"icon"),
	"comphep.ini;../comphep.ini;[-]comphep.ini");

        menulevel = 0;
	fillModelMenu();

	if (inset(exitlevel,setof(0,UpTo,1,_E))) cheplabel(exitlevel);
	if (inset(exitlevel,setof(1,UpTo,4,_E)))
	  {readModelFiles(n_model); loadModel(FALSE);}

   switch (exitlevel)
   {
      case 1:
      case 4:
         goto label_50;  /*  after Freeze  */

      case 2:            /*  after outOfMemory  */
         smalllabel();
         modelinfo();
         processinfo();
         diagramsinfo();
         sq_diagramsinfo();
         menulevel = 2;
         k1 = 2;
         goto restart2;
   }   /*  Case  */

label_20:   /*   Menu2(ModelMenu): */
   menulevel = 0;
   smalllabel();
   menuhelp();
   do
   {
      showheap();
      k1=n_model;
      menu1(56,4,"",modelmenu,"h_200000",&pscr1,&k1);
      n_model=k1;
      if (n_model == 0)
      {
	if ( mess_y_n(56,4,"  Quit session  $"))   /*  Exit  */
            {
               exitlevel = 0;
               haltnumber = 0;
               goto exi;
            }         
      }
      else
         if (n_model > maxmodel)
         {
            clrbox(1,5,48,18);
            if (makenewmodel()) modelinfo();
            menuhelp();
         }
         else
         if (n_model > 0)
         { 
	    readModelFiles(n_model);
	    put_text(&pscr1);
	    goto label_30;
	 }
   }  while(TRUE);

label_30:   /*  Menu3:Enter Process  */
   menulevel = 1;
   modelinfo();
   k2 = 1;
   do
   {
      menu1(56,4,"","\026"
        " Enter Process        "
        " Edit model           "
        " Delete changes       ","h_00300*",&pscr2,&k2);
      switch (k2)
      {
         case 0:  /*  'Esc'  */
           goto_xy(1,1);
           clr_eol();
           goto label_20;

	 case 2:
           editModel(TRUE);
	   break;
         case 3:  /*  Delete  */
	    if ( deletemodel(n_model) )
            {
               goto_xy(1,1);
               clr_eol();
               n_model=1;
               fillModelMenu();
               goto label_20;
            }
            else   readModelFiles(n_model);
      } /*  CASE  */
   }  while (k2 != 1);

	loadModel(FALSE);
label_31:

   enter(&errorcode);   /*  Enter a process  */
   showheap();
   if (errorcode != 0)   /*  'Esc' pressed  */
   {
      menuhelp();
      goto label_30;
   }
   construct();          /*  unSquared diagrams  */
   if (errorcode != 0)
   {
      clrbox(1,19,80,24);
      goto label_31;   /*  processes are absent  */
   }
   else
   { searchrec  s;
   
     clr_scr(White,Black);
     smalllabel();
     modelinfo();
     processinfo();
     diagramsinfo();
      	
     if( 0 == find_first(scat("%sresults%c*.*",pathtouser,f_slash),&s,anyfile) )
     messanykey( 10,10, 
     "There are  files in directory `results`.$ Use F5 key to clear it$");
     goto label_41;	
   }
   

label_40:               /*  Menu4: Squaring,... */
   clr_scr(White,Black);
   smalllabel();
   modelinfo();
   processinfo();
   diagramsinfo();
   showheap();
label_41:   
   k3 = 1;
   do
   {
      menu1(56,4,"","\026"
         " Squaring             "
         " View diagrams        ","h_00400*",&pscr3,&k3);
      switch (k3)
      {
         case 0:  /*  Esc  */
            clrbox(1,2,55,11);
            menuhelp();
            goto label_30;
         case 2:  /*  unSquared process menu   */
            viewfeyndiag(TRUE);
      }   /*  CASE  */
   }  while (k3 != 1);      


/*  diagrams  */
	if (!squaring()) goto label_40;  /*  process are absent  */

   clear_tmp();

   exitlevel = 1;
   saveent(nsub,ndiagr,n_model, exitlevel);
   restoreent(&nsub,&ndiagr,&n_model,&exitlevel);
   exitlevel = 1;

label_50:   /*  Menu5: View squared diagrams.....   */
   clr_scr(White,Black);
   smalllabel();
   modelinfo();
   processinfo();
   diagramsinfo();
   sq_diagramsinfo(); /*   ????????   */

   menulevel = 2;
   showheap();
   k4 = 1;
   while(TRUE)
   {
      menu1(56,4,"","\026"
         " View squared diagrams"
         " Symbolic calculations"
         " Write  results       "
         " REDUCE program       "
         " Numerical calculator "
         " Enter new process    "
         " Interface            ","h_00600*",&pscr4,&k4);
      switch (k4)
      {
         case 0:
            if (mess_y_n(50,3,"Return to previous menu?$"))
            {
               menulevel = 1;
               goto label_40;
            }
         break;

         case 1:
            viewsqdiagr();
         break;

         case 2:     /*  Compute all diagrams   */
restart2:
            calcallproc();
            showheap();
            sq_diagramsinfo();
         break;

         case 3:
            if (continuetest())
            {
               k5 = 1;
               menu1(56,6,"","\026"
                 " FORTRAN77 code       "
                 " FORTRAN90 code       "
                 " REDUCE code          "
                 " MATHEMATICA code     "
                 " FORM code            ",NULL,&pscr5,&k5);
               switch (k5)
               {
                  case 1:   /*  Fortran77 code  */
                     fortprg(inset(quadra,options),inset(graphic,options),fortname);
                  break;

                  case 2:   /*  Fortran90 code  */
                     fort90prg(inset(quadra,options),inset(graphic,options));
                  break;

                  case 3:
                     makeReduceOutput();
                  break;
		  case 4:
		     makeMathOutput();
		     break; 
		  case 5:
		     makeFormOutput();
                  break;
               } /* switch(k5) */
	       put_text(&pscr5);
            }

         break;

         case 4:
            mk_reduceprograms();
			break;

			case 5:
            switch (nin+nout)
            {
               case 3:
                  decay12();
               break;

               case 4:
                  nsub = 1;
                  do
                  {
                     sqdiagrmenu();
                     if (nsub != 0)
                     if (nin == 2)  cs_numcalc(nsub);
                      else          decay13(nsub);
                  }  while (!(nsub ==0 || subproc_sq == 1));
               break;

               default:
                  messanykey(10,15,
                     "Implemented only for$1->2  and  2->2 processes$");
            }
         break;

	    case 6:
            menulevel = 1;
            put_text(&pscr4);
            put_text(&pscr3);
            clrbox(1,3,80,24);
            goto label_30;

            case 7:
            {  /*  Exit  */
               exitlevel = 1;
               k6 = 1;
               do
               {
                  menu1(56,7,"",exitmenu,NULL,&pscr6,&k6);
                  switch (k6)
                  {
                     case 1:
                     case 2:
                     case 3:
                     case 4:
                     case 5:
                     case 6:
                     case 7:
                     case 8:
                     case 9:
                     case 10:
                        haltnumber = 219 + 2*k6;
                        goto exi;

                     case 11:
                        changeexitmenu();
                  }  /* Case */
               }  while(k6 != 0);
            }
      }  /* Case */
   }

exi:  /*  Halt  */
   if (exitlevel <= 1)
   {
      saveent(nsub,ndiagr,n_model,exitlevel);
      finish();
      exit(haltnumber);
   }

   finish();
   return 0;
}
