
/* Modified by S.A. May 11 1991     */

#include <math.h>
#include "tptcmac.h"
#include "os.h"
#include "syst2.h"
#include "crt.h"
#include "optimise.h"
#include "n_compil.h"
#include "num_tool.h"
#include "num_serv.h"
#include "files.h"
#include "crt_util.h"
#include "tex_util.h"
#include "graf.h"

static int X1, Y1, X2, Y2;
double  xscale, yscale;

grafminmaxstruct grafminmax;

static   int pictureX=300;
static   int pictureY=200;
static   char  letterSize[14]="normalsize";

static int  scale;
static int        fgcolor = Black,  bkcolor = White; 

#define odd(x)     (((int)(x)) & 1)


static int nlog10(double x)
{ double lg=log10(x);
  if (lg<0) return lg-1; else return lg;
}


static void axisDesign(double xmin,double xmax, int islog,
              double * xfirst, double * step,int * nsub)
{
  double dx,dx0,dx100;
  int n,n0,n1;
  char xmintxt[50];

  if (islog) 
  { 
    n=1+log10(xmax/xmin)/10;
    *step=pow((double)10,(double)n);     
    *xfirst=pow((double)10,(double)nlog10(xmin));
    *nsub=9;
  }else
  {      
    dx=xmax-xmin;
    n=nlog10(dx); 
    dx0=pow((double)10,(double)n);  

    dx100= 10*dx/dx0;   
    if (dx100<15.0) { *step= 0.3*dx0;  *nsub=3; } else
    if (dx100<24.0) { *step= 0.5*dx0;  *nsub=5; } else 
    if (dx100<30.0) { *step= 0.8*dx0;  *nsub=8; } else 
    if (dx100<45.0) { *step=   1*dx0;  *nsub=10;} else 
    if (dx100<90.0) { *step=   2*dx0;  *nsub=2; } else 
                    { *step=   3*dx0;  *nsub=3; }
    if( fabs(xmin)<=(*step)*10 ) *xfirst=0;else
    {                  
       n0=nlog10(*step);     
       n1=nlog10(fabs(xmin)); 
    
       sprintf(xmintxt,"%.*E",MAX(0,n1-n0-1),xmin);
       trim(xmintxt);
       sscanf(xmintxt,"%lf",xfirst);         
    }       
    while(*xfirst>xmin+(*step)/(*nsub)  ) *xfirst -= *step;
    while(*xfirst+(*step) < xmin) *xfirst += *step;
  }
}

static int  chpround(double  x)
{
   return  x >= 0.0 ? chround(x) : -chround(-x);
}

static void  gminmax(realseq  rsec,int line)
{
   if (rsec == NULL)
   {
      grafminmax.xmin = 
      grafminmax.xmax =
      grafminmax.ymin = 
      grafminmax.ymax = 0; 
      return;
   }
       
   grafminmax.xmin = rsec->x;
   grafminmax.xmax = grafminmax.xmin; 
   grafminmax.ymin = rsec->y;
   grafminmax.ymax = grafminmax.ymin; 
   rsec = rsec->next;
    
   while (rsec != NULL) 
   {               
      if (grafminmax.xmin > rsec->x) grafminmax.xmin = rsec->x;
      if (grafminmax.xmax < rsec->x) grafminmax.xmax = rsec->x; 
      if (grafminmax.ymin > rsec->y) grafminmax.ymin = rsec->y; 
      if (grafminmax.ymax < rsec->y) grafminmax.ymax = rsec->y;
      if (!line)
      {
         if (grafminmax.xmin > rsec->x1) grafminmax.xmin = rsec->x1;
         if (grafminmax.xmax < rsec->x1) grafminmax.xmax = rsec->x1; 
         if (grafminmax.ymin > rsec->y1) grafminmax.ymin = rsec->y1; 
         if (grafminmax.ymax < rsec->y1) grafminmax.ymax = rsec->y1;
      } 
      rsec = rsec->next;
   } 
}


int  scx(double  x)
{ 
   if (odd(scale))
      x = log10(x / grafminmax.xmin); 
   else
      x -= grafminmax.xmin; 
   return X1 + chpround(xscale * x);
} 


int  scy(double  x)
{  
   if (scale > 1)
      x = log10(x / grafminmax.ymin); 
   else
      x -= grafminmax.ymin; 
   return Y1 - chpround(yscale * x);
} 

static double  dscx(double  x)
{ 
   if (odd(scale))
      x = log10(x / grafminmax.xmin); 
   else
      x -= grafminmax.xmin; 
   return X1 + (xscale * x);
} 


static double  dscy(double  x)
{ 
   if (scale > 1)
      x = log10(x / grafminmax.ymin); 
   else
      x -= grafminmax.ymin; 
   return Y1 - (yscale * x);
} 

double   bscx(int x)
{ 
   if (odd(scale))
      return grafminmax.xmin * exp(log(10.)*(x-X1)/xscale);
   else
      return grafminmax.xmin + (x-X1)/xscale;
} 

double   bscy(int y)
{ 
   if (scale > 1)
      return grafminmax.ymin * exp(log(10.)*(y-Y1)/yscale);
   else
      return grafminmax.ymin - (y-Y1)/yscale;
} 

void doubleToStr(double x,double step,char * s)
{ 
   int    n1,n0;
   char * emark;
   char s1[20],s2[20],sgn[5]; float f;
   
   if(fabs(x) < 1.E-2 * step) strcpy(s,"0.0"); else
   {  
      n1=nlog10(fabs(x));
      n0=nlog10(step);
      sprintf(s1,"%.*E",MAX(0,n1-n0),x);
      
      emark=strstr(s1,"E");

      emark[0]=0;    
      emark++;
      sgn[0]=0;
      switch (emark[0])
      { 
        case '-' : strcpy(sgn,"-");emark++;break;
        case '+' : sgn[0]=0; emark++;break;
      }  
      while (emark[0]=='0') emark++;
      if(emark[0]==0) strcpy(s2,s1); else
      {     
         sscanf(s1,"%f",&f);
         if (f==1.0) sprintf(s2,"10^%s%s",sgn,emark);
             else    sprintf(s2,"%s*10^%s%s",s1,sgn,emark);     
      }
      
      sprintf(s1,"%.*lf",MAX(0,-n0),x);
      
      if (strlen(s1)<strlen(s2)) strcpy(s,s1);else strcpy(s,s2);      
   }   
}    


void  gaxes(int scale_, char* upstr, char* xstr, char* ystr)
{double  xmax,xmin,ymax,ymin,aa,step; 
 int     m,naxis,islog,n_sub;

 int th = tg_textheight("0");
 int tw = tg_textwidth("0");
 int  hash = (th+tw)/2; 
 int  texhash=(th*texyscale+tw*texxscale+1)/2;
   xmin = grafminmax.xmin; 
   xmax = grafminmax.xmax; 
   ymin = grafminmax.ymin; 
   ymax = grafminmax.ymax; 
   if ( 1.E-4 * (fabs(ymax) + fabs(ymin)) >= fabs(ymax - ymin)) 
      if (ymin == 0.0) {  ymin = -0.5; ymax = 0.5; } 
        else  if (ymin < 0.0) 
              {  
                 ymin *= 2.0;
                 if (ymax < 0.0) ymax *= 0.5;  else ymax *= 2.0; 
              }  else { ymax *= 2.0; ymin *= 0.5;} 
   grafminmax.ymin = ymin; 
   grafminmax.ymax = ymax; 

   scale = scale_; 
   if (odd(scale) && (xmin <= 0.0 || log10(xmax/xmin)<1 )) scale--; 
   if (scale > 1 && ( ymin <= 0.0 || log10(ymax/ymin)<1 )) scale -= 2;

   tg_setlinestyle(SolidLn,NormWidth);

   X1 = 10*tw; X2 = tg_getmaxx() - 2*tw;  
   Y1 = tg_getmaxy() - 5*th; Y2 = 5*th/2; 

   xscale = (X2 - X1) / (odd(scale) ? log10(xmax / xmin) : xmax - xmin);
   yscale = (Y1 - Y2) / (scale > 1 ? log10(ymax / ymin) : ymax - ymin); 
   tg_settextjustify(CenterText,TopText);
   tg_outtextxy((X1+X2)/2,CenterText,upstr);  
    
   for(naxis=0;naxis<2;naxis++)
   { double zmin,zmax,zmin1,zmax1;
     char xy;
     int xend,yend;
     int len;

     
     if(naxis==0)
     {  
        xy='X';
        if(texflag){ hash=texhash/texyscale;texhash= -texhash;}      
        islog = odd(scale); 
        tg_settextjustify(CenterText,TopText);
        zmin=xmin;
        zmax=xmax;
        xend=X2;
        yend=Y1;              
     }else
     { 
        xy='Y';
        if(texflag){texhash= -texhash; hash=texhash/texxscale;}      
        islog = scale > 1; 
        tg_settextjustify(RightText,CenterText);
        zmin=ymin;
        zmax=ymax;
        xend=X1;
        yend=Y2;              
     }
     
     if(texflag) fprintf(out_tex,"%% ====================   %c-axis =============\n",xy );
     
     zmax1 = zmax + fabs(zmax)*1.E-7;
     zmin1 = zmin - fabs(zmin)*1.E-7;
     axisDesign(zmin,zmax, islog,&aa, &step,&n_sub);     
     if(texflag)
     { double Nd,offset;
       char axis[10];
       char d[10];
       
       if(islog) {  strcpy(axis,"LogAxis"); 
                    Nd=log10(zmax/zmin)/log10(step);
                    offset=10*zmin/(step*aa); 
                    strcpy(d,"");
                 }                              
        else     {  strcpy(axis,"LinAxis");
                    Nd=(zmax-zmin)/step;       
                    offset=-n_sub*(aa-zmin)/step; 
                    sprintf(d,"%d,",n_sub); 
                    if (fabs(offset) >fabs(offset-n_sub) ) offset -=n_sub;                             
                 }
                 
      f_printf(out_tex,"\\%s(%.2f,%.2f)(%.2f,%.2f)(%.3f,%s%d,%.3f,1.5)\n",
        axis,texX(X1),texY(Y1),texX(xend),texY(yend),Nd,d,(2*texhash)/3,offset);                 
     }
     else tg_line(X1,Y1,xend,yend);
     len=0;
     while (aa <= zmax1) 
     {  double da;
        char snum[30];      
        int i;      
        if(aa >= zmin1)
        {  
           if(naxis==0){m=scx(aa);if(!texflag)tg_line(m,Y1,m,Y1+hash);}
           else        {m=scy(aa);if(!texflag)tg_line(X1-hash,m,X1,m);}
           if (islog)  doubleToStr(aa,aa,snum); else doubleToStr(aa,step,snum);
           len=MAX(len,tg_textwidth(snum));
           if(naxis==0) tg_outtextxy(m,Y1+hash,snum);
           else         tg_outtextxy(X1-hash,m,snum);     
        }
        if (islog) da = aa*step -aa ;else da=step;
        da=da/n_sub;
        for(i=1;i<n_sub;i++)
        {  aa +=da;    
           if(!texflag && aa >=zmin1 && aa<=zmax1)
           if (naxis==0) { m = scx(aa);tg_line(m,Y1,m,Y1 + 2*hash/3); }
           else          { m = scy(aa);tg_line(X1 - 2*hash/3, m,X1,m);}    
        }
        aa += da;                                                                                   
     }
     
     if(naxis==0) 
     {  tg_settextjustify(RightText,TopText); 
        tg_outtextxy(X2,Y1 + hash + th ,xstr);
     }else
     if(texflag)
     {
       f_printf(out_tex,"\\rText(%.1lf,%.1lf)[tr][l]{%s}\n",
       texX(X1-len-3*hash/2) ,texY(Y2),ystr); 
     
     }else     
     {  tg_settextjustify(LeftText,CenterText);
        tg_outtextxy(X1,2*th ,ystr);     
     }
   }
   if(texflag) f_printf(out_tex,"%% ============== end of axis ============\n");  
     
}  /* GAxes */ 

void  gcurve(realseq  rsec, boolean  line)
{  double   x,y,xx,yy,ymin,ymax;
 
   ymax = dscy(grafminmax.ymin);
   ymin = dscy(grafminmax.ymax);
   
   if (line) 
   { 
      if(texflag)
      {
        int npoint=0;
        int stat =0;
        double dx,dy;
        f_printf(out_tex,"\n");
        while (rsec != NULL)
        {  
           dx = dscx(rsec->x);
           dy = dscy(rsec->y);
           if (dy <= ymax && dy >= ymin)
           {
              if(stat == 0) {f_printf(out_tex,"\\Curve{");stat=1;npoint=1;}         
              f_printf(out_tex,"(%.2f,%.2f)",texX(dx),texY(dy)); npoint++;
              if(npoint==5)  {f_printf(out_tex,"\n"); npoint=0;}
           } else  if (stat==1) {stat=0;f_printf(out_tex,"}\n");}  
           rsec = rsec->next;             
        }
        if (stat==1) f_printf(out_tex,"}\n");
      }      
      else
      { 
        x = dscx(rsec->x);
        y = dscy(rsec->y);
        rsec = rsec->next;
      
      
        while (rsec != NULL)
        {
           xx = dscx(rsec->x);
           yy = dscy(rsec->y);
           if (yy<=ymax && yy>=ymin && y<=ymax && y>=ymin) 
                    tg_line((int)x,(int)y,(int)xx,(int)yy);
           x = xx;
           y = yy;
           rsec = rsec->next;
        }
      }
   }else
   {
     while (rsec != NULL)
     {  
        x =dscx(rsec->x); 
        y =dscy(rsec->y);
        xx=dscx(rsec->x1);
        yy=dscy(rsec->y1);
        if ( yy < y) 
        { double z;
          z=yy;yy=y;y=z;
          z=xx;xx=x;x=z;
        }
        
        if (yy>ymin &&  y<ymax)         
        {
          if(yy>ymax && y!= yy)
          {  xx= xx-((xx-x)*(yy-ymax))/(yy-y); yy=ymax;}
          if(y<ymin &&  y!= yy)
          {  x= x-((x-xx)*(y-ymin))/(y-yy); y=ymin;}          
          tg_line((int)x,(int)y,(int)xx,(int)yy);           
        }   
        rsec = rsec->next;
     }   
   }
}


void  plot_1(boolean  sc, realseq  rsec, boolean line,
                  char*  upstr, char*  xstr,
                  char*   ystr)
{int         k;
 char        f_name[STRSIZ], menustr[STRSIZ];
 byte        xscale, yscale;
 double      ymin, ymax;
 pointer     prtscr;

   get_text(1,1,maxCol(),maxRow(),&prtscr);
   gminmax(rsec,line);
   if (  1.E-4 * (fabs(grafminmax.xmax) + fabs(grafminmax.xmin)) 
       >= fabs(grafminmax.xmax - grafminmax.xmin))
   {  messanykey(10,10, "Too short interval in X axes !$");
      return;
   }   
   ymin=grafminmax.ymin;
   ymax=grafminmax.ymax;
   scrcolor(fgcolor,bkcolor);
   if (sc)   xscale = 1; else  xscale = 0;
   yscale=(ymin >0 && grafminmax.ymax/grafminmax.ymin >10);   
   k = 0;      
REDRAW:
   clr_scr(fgcolor,bkcolor);
   gaxes(xscale + 2 * yscale,upstr,xstr,ystr);
   gcurve(rsec,line);
   if (texflag)         
   {  f_printf(out_tex,"\\end{picture}\n");
      texfinish();
      messanykey(35,15,scat("%s%s$","LaTeX output is saved in file$",f_name));
      goto contin;              
   }
   tg_settextjustify(BottomText,LeftText);
   scrcolor(Red,bkcolor);
   tg_outtextxy(0,tg_getmaxy(),"Press any key to continue");
   while (inkey()==KB_SIZE); 
   scrcolor(bkcolor,bkcolor);
   tg_outtextxy(0,tg_getmaxy(),"Press any key to continue");     
contin:   
   do
   {  char sScale[20];
       void * pscr = NULL;

      if(yscale) strcpy(sScale,"Log.   "); else strcpy(sScale,"Lin.   ");  
   
      sprintf(menustr,"%c Y-max = %-9.3G Y-min = %-9.3G Y-scale = %s"
      " Redraw plot      "
      " LaTeX            "
      " Exit             ",18,grafminmax.ymax,grafminmax.ymin,sScale);
       menu0(33,7,"",menustr,NULL,NULL,&pscr,&k);
       
       switch (k)
       {  case 6:
             del_text(&pscr); 
          case 0:
             goto exi;
           case 1:   
             correctDouble(33,11,"Y-max = ",&grafminmax.ymax,TRUE );
             break;
           case 2:
             correctDouble(33,11,"Y-min = ",&grafminmax.ymin,TRUE );
             break;
           case 3: 
             yscale=!yscale;
             break;
           case 4: 
           case 5:
             if(grafminmax.ymin >=ymax|| grafminmax.ymax <=ymin ||
               grafminmax.ymin >= grafminmax.ymax)
             { messanykey(10,10," Wrong Y-range $");
               break;
             } 
             if(yscale && (grafminmax.ymin <=0 ||
                   grafminmax.ymax/grafminmax.ymin <=10 ) )
              { messanykey(10,10," Log. scale is forbiden for this Y-range $"); 
               yscale = 0; 
              }   
             if(k==5)
             { 
               if( !texmenu(&pictureX,&pictureY,letterSize)) break;
               newFileName(f_name,"plot_","tex"); 
               texstart(f_name,upstr,letterSize);
               texPicture(0,0,tg_getmaxx(),tg_getmaxy()-tg_textheight("0"),
                                                  pictureX,pictureY);
               f_printf(out_tex,"\\begin{picture}(%d,%d)(0,0)\n",pictureX,pictureY);  
             }    
             del_text(&pscr);
             goto REDRAW;
       }   
   }  while (TRUE); 
      
exi:
   clr_scr(White,Black);
   put_text(&prtscr);
}

void  plot_2(boolean  sc, realseq  rsec,
                  char*  upstr, char*  xstr,
                  char*   ystr)
{
  plot_1 (sc, rsec,TRUE, upstr,  xstr, ystr);
}
