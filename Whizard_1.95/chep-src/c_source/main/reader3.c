#include <math.h>
#include "tptcmac.h"
#include "physics.h"
#include "sos.h"
#include "parser.h"
#include "syst2.h"



#include "reader3.h"


pointer  rd_3(char* s)
/* char    * s;*/
{
	double    * p;
	varlist       q;
	p = (double *) getmem_(sizeof(double));
   if (isdigit(*s))
   {
      *p=atof(s);
       return (pointer) p;
   }
   if (modelvars == NULL)
	{	  rderrcode = 16;
		  return  NULL;
	}
   q = modelvars;
   if (strcmp(s,strongconst) == 0)
      gg_exist = FALSE;
   while (TRUE)
   {
      if (strcmp(q->varname,s) == 0)
      {
          *p = q->varvalue;
          return (pointer) p;
      }
      else q = q->next;
		if (q == NULL)
		{  rderrcode=16;
			return NULL;
		}
   }
}


pointer  uact_3(char* ch,pointer mm)
/* char  * ch;
 pointer mm;*/
{double  * p;

   p = (double *)mm;
   if (strcmp(ch,"-") == 0)
   {  *p = - *p;
      return mm;
   }
   /* If ch='Sqrt' */
	if (*p <  0.0)
	{
	  rderrcode =  negativsqrtarg;
	  return  NULL;

	}
   *p = sqrt(*p);
   return mm;
}


pointer  bact_3(char ch,pointer mm1,pointer mm2)
/* char    ch;
 pointer mm1;
 pointer mm2;*/
{double  * p1, *p2;
 double    q;
 word    i;
 integer d;

   p1 = (double *) mm1;
   p2 = (double *) mm2;
   switch (ch)
   {
      case '+':
         *p1 += *p2;
      break;

      case '*':
         *p1 *= *p2;
      break;

      case '/':
			if (*p2 == 0)
			{
				rderrcode=divisionbyzero;
				return NULL;
			 }
         else
            *p1 /= *p2;
      break;

      case '^':
			d = chround(*p2);
         q = *p1;
         *p1 = 1.0;
         for (i = 1; i <= d; i++)
            *p1 *= q;
   }  /* Case */
   return (pointer) p1;
}
