#include "tptcmac.h"
#include "syst2.h"
#include "global.h"
#include "physics.h"
#include "files.h"
#include "crt.h"
#include "transfer.h"
#include "diaprins.h"
#include "procvar.h"
#include "crt_util.h"
#include "os.h"


#include "out_serv.h"

#define xmax 76


static FILE * outFile;
static int    xpos;
static char varstxt[nvarhighlimit][10];


void seekArchiv(long n)
{  fseek(archiv,n,SEEK_SET);}

int readNN(void)
{ byte  nexpr;
   fread(&nexpr,1,1,archiv);
   return nexpr;
}

void  readvars(char language)
{ int         i, j, k;
 varlist      p;
   k = 1;
   for (i = 2; i <= MAXINOUT; i++)
   for (j = 1; j <= i - 1; j++)
   { switch(language)
     { case 'R':
       case 'F': sprintf( (varstxt)[maxsp  - k ], "p%d.p%d", j, i);break;
       case 'M': sprintf( (varstxt)[maxsp  - k ], "SC[p%d,p%d]", j, i);break;
     }   
     k++;
   }
   strcpy( (varstxt)[k -1], "||||||");
   k++;
   p = modelvars;
   while (p != NULL)
   {
      strcpy( (varstxt)[k -1], p->varname);
      k++;
      p = p->next;
   }
}


static void  wrtln(void)
{
   fprintf(outFile,"\n");
   xpos = 1;
}


static void  wrt(char* ss)
{
	char s[STRSIZ],cc;
	int  l;

   strcpy(s,ss);
   l = strlen(s);
   if ((xpos + l) <= xmax)
   {
      fprintf(outFile,"%s",s);
      xpos += l;
   }
   else
   {
      l = xmax - xpos;

label_1:
      while(l > 0 && strchr("*+-)( ",s[l-1]) == NULL) --(l);

      if (l > 0 && s[l-1] == '*' &&
          (l > 1 && s[l - 1-1] == '*' ||    /* ** !!! */
           (l <  strlen(s) && s[l + 1-1] == '*')))
      {  l -= 2;
         goto label_1;
      }
      if (l <= 0)
         if (xpos == 1)
            l = xmax;
         else
            { wrtln();wrt(" ");wrt(s); return; }
      cc=s[l];s[l]=0;wrt(s);s[l]=cc; 
      wrtln();wrt(" ");
      wrt(s+l);
   }
}

void writeF(char * format,...)
{
  va_list args;
  char dump[STRSIZ] , *beg, *nn;

  va_start(args,format);   
  vsprintf(dump,format,args);
  va_end(args);

  beg=dump;   
  while(TRUE)
  { nn=strchr(beg,'\n');
    if(nn==NULL){wrt(beg);return;}
    nn[0]=0;
    wrt(beg);
    wrtln();
    beg=nn+1;
  }
}

void outFileOpen(char * fname)
{outFile=fopen(fname,"w" );xpos=1;}

void outFileClose(void)
{ fclose(outFile);}



void DiagramToOutFile(vcsect * vcs, int label,char comment)
{
   if (xpos !=1) wrtln();
    writeTextDiagram(vcs,label,comment,outFile);
}

static void  readmonom(char* txt)
{long     l;
 byte		b, b0; /* b must be 'byte' for read_ */
 int       deg;

   FREAD1(l,archiv);
   if( l == 1 || l == -1) txt[0]=0; else sprintf(txt,"%+ld",l);
   FREAD1(b,archiv);
   if (b != 254)
   do
   {
      deg = 0;
      b0 = b;
      do {FREAD1(b,archiv);deg++;}while (b == b0);
      sprintf(txt+strlen(txt),"*%s",(varstxt)[b0-1]); 
      if (deg != 1) sprintf(txt+strlen(txt),"^%d",deg);      
   }  while (b != 254);
   if (txt[0] == 0)   sprintf(txt,"%+ld",l);
   else
   {  if (l ==  1) txt[0]='+';
      if (l == -1) txt[0]='-';
   }
}



void  jumppolynom(void)
{ word  count, exprlen;
  char  monomtxt[STRSIZ];
  FREAD1(exprlen,archiv);
  for (count = 1; count <= exprlen; count++)  readmonom(monomtxt);
}


void  rewritepolynom(void)
{ word  count, exprlen;
  char  monomtxt[STRSIZ];


  FREAD1(exprlen,archiv);
  if (exprlen == 0) {wrt("0");return;}
  readmonom(monomtxt); 
  if (monomtxt[0] == '+') wrt(monomtxt+1); else wrt(monomtxt);                                            
  for (count = 2; count <= exprlen; count++){readmonom(monomtxt);wrt(monomtxt);}
}


void findPrtclNum (char * procName, int * prtclNum)
{
 int	k;
 char * pname,txt1[STRSIZ]; 
 byte pnum;
 
   strcpy(txt1,procName); pname=strstr(txt1,"->");pname[0]=' ';pname[1]=',';
   pname=strtok(txt1,",");
   k=1;
   while ( pname != NULL)
   { 
     trim(pname);locateinbase(pname,&pnum);
     prtclNum[k]=pnum; k++;       
     pname=strtok(NULL,",");
   } 
}

void  emitconvlow(int* prtclNum,char language )
{
 int	i, j, ntot;
 char  	pmass[MAXINOUT+1][11]  ; /* for locateinbase */
 char   s[MAXINOUT+1]; 
 
   ntot = nin + nout;
   for(i=1;i<=ntot;i++) strcpy(pmass[i],prtclbase[prtclNum[i]-1].massidnt);

   for (i = 1; i <= nin; i++) s[i] = 1;
   for (i = nin + 1; i <= ntot; i++) s[i] = -1;
   
   switch (language) 
   {case 'R': writeF("\nLET p%d = ",ntot); break;
    case 'F': writeF("\nid p%d = ",ntot); break;
    case 'M': writeF("\np%d = ",ntot); break;
   }
   for (i = 1; i <= nin ; i++) writeF("+p%d",i);
   for (i = 1; i <= nout - 1; i++) writeF("-p%d",i+nin);
   writeF(";\n");

   for (i = 1; i <= ntot - 1; i++)
   switch (language)     
   {case 'R': writeF("Mass p%d  = %s; Mshell p%d;\n",i,pmass[i],i); break;
    case 'F': writeF("id p%d.p%d  = %s^2;\n",i,i,pmass[i]); break;
    case 'M': writeF("p%d/: SC[p%d,p%d] =%s^2;\n",i,i,i,pmass[i]); break; 
   }
              
   switch (language)     
   {case 'R':writeF("LET p%d.p%d = ",ntot - 2,ntot - 1); break;
    case 'F':writeF("id p%d.p%d = ",ntot - 2,ntot - 1);break;
    case 'M':writeF("p%d/: SC[p%d,p%d] = ",ntot-2,ntot - 2,ntot - 1);break;
   }           

   writeF("%d*(%s^2",s[ntot-1]*s[ntot-2],pmass[ntot]);
   for (i = 1; i <= ntot - 1; i++) writeF("-%s^2",pmass[i]);
   for (i = 2; i <= ntot - 1; i++)
   for (j = 1; j <= i - 1; j++)
   if (j < (ntot - 2))    switch (language)     
   {case 'R':
    case 'F':writeF("%+d*p%d.p%d",-2*s[j]*s[i],j,i);break;
    case 'M':writeF("%+d*SC[p%d.p%d]",-2*s[j]*s[i],j,i);break; 
   }
   writeF(")/2;\n");
}

void writeLabel(char comment)
{
 if (xpos !=1) wrtln();
 fprintf(outFile,"%c    ==============================\n",comment );
 fprintf(outFile,"%c    *  %s *\n",comment,version);
 fprintf(outFile,"%c    ==============================\n",comment);         
}



void  makeOutput(  void (*startOutput)(int,int*,int),
                   void (*diagramOutput)(vcsect*, catrec* ),
                   void (*endOutput)(int*)            
                )
{catrec    cr;
 word      ndel, ncalc, nrest, recpos;
 long   count;
 int graphOn;
 shortstr  txt;
 int prtclNum[MAXINOUT+1];
 csdiagram   csdiagr;
 vcsect      vcs; 
 FILE * catalog;
 
   informline(0,1);
   catalog=fopen(CATALOG_NAME,"rb");
   archiv= fopen(ARCHIV_NAME,"rb");
   menuq=fopen(MENUQ_NAME,"rb");
   count = 0;
   graphOn=inset(graphic,options);
   
   if (graphOn) diagrq=fopen(DIAGRQ_NAME,"rb");

   for(nsub=1;nsub<=subproc_sq;nsub++) 
   {
      rd_menu(2,nsub,txt,&ndel,&ncalc,&nrest,&recpos);
      
      findPrtclNum (txt, prtclNum);
      if (ncalc != 0)
      {
         startOutput(nsub,prtclNum,ncalc);  
         fseek(catalog,0,SEEK_SET);
         while (FREAD1(cr,catalog))
         {
            if (cr.nsub_ == nsub)
            {
               if (graphOn)
               {  fseek(diagrq,(cr.ndiagr_+recpos-1)*sizeof(csdiagram),SEEK_SET);
                  FREAD1(csdiagr,diagrq);
                  transfdiagr(&csdiagr,&vcs);
                  diagramOutput(&vcs,&cr);
               }
                else diagramOutput(NULL,&cr);
                ++(count); 
	       if(informline(count,subproc_sq )) goto escexit;
            }
         }
escexit: 
         endOutput(prtclNum);
      }
   }
   fclose(catalog);
   fclose(archiv);
   fclose(menuq);
   if (graphOn) fclose(diagrq);
   informline(subproc_sq,subproc_sq);
}

