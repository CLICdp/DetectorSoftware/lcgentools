#include "tptcmac.h"
#include "syst2.h"
#include "crt.h"
#include "crt_util.h"
#include "help.h"
#include "tex_util.h"

int texmenu(int * pictureX,int * pictureY,char * letterSize)
{

char sizestr[]="\016"
     " tiny         "
     " scriptsize   "
     " footnotesize "
     " small        "
     " normalsize   "
     " large        "
     " Large        ";

char menustr[100];
void* pscr=NULL;
void* pscr2=NULL;
int k=1;
int l=1;

   while(TRUE)
   {

       sprintf(menustr,"\026"
       " Help                 "
       " picture(%4d,%4d)   "
       " Letter = %-12.12s"
       " Write to file        ",*pictureX,*pictureY,letterSize);

          menu0( 33,11,"LaTex menu",menustr,NULL,NULL,&pscr,&k);
          switch(k)
          { case 0: return 0;
            case 1: show_help("h_tex1");break;
            case 2:  correctInt (15,20,"Picture X-size=",pictureX,TRUE);
                     correctInt (15,20,"Picture Y-size=",pictureY,TRUE);
                     break;
            case 3:  menu0( 12,12,"",sizestr,NULL,NULL,&pscr2,&l);
                     if(l!=0)
                     {  put_text(&pscr2);
                        sprintf(letterSize,"%12.12s",sizestr+2+(l-1)*14);
                      }
                      break;
            case 4:  put_text(&pscr);
                     return 1;
          }
   }

}
