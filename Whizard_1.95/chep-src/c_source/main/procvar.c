#include "tptcmac.h"
#include "syst2.h"
#include "physics.h"
#include "parser.h"
#include "sos.h"
#include "pvars.h"
#include "files.h"
#include "os.h"
#include "procvar.h"

 varname *varnamesptr;
 setofbyte varsofprocess, funcofprocess;


void   initvarnames(void)

{ int       k, kk=0;
  varlist      p;
  setofbyte    allvars;
  int nvar;

  nvar=maxsp+1; /*16*/

   p = modelvars;
   while (p != NULL)
   {
      ++(nvar);
      p = p->next;
   }
	varnamesptr=(varname *)getmem_(7 * nvar);

	for (k = 1; k <= maxsp; k++)
	 if(k<10)	sbld((*varnamesptr)[maxsp- k],"P%c",chr(k + ord('0')));
		else		sbld((*varnamesptr)[maxsp- k],"P%c",chr(55 + k));

	strcpy((*varnamesptr)[maxsp], "||||||");

   getprocessvar(0,varsofprocess,funcofprocess);

	k = maxsp+1; /*16*/
   setofb_cpy( allvars, setofb_uni(varsofprocess, funcofprocess));
   p = modelvars;
   while (p != NULL)
   {
      ++(k);
      if (insetb(calcvarpos(p->varname),allvars))
			sbld((*varnamesptr)[k-1],"A(%d)",++kk);
      p = p->next;
   }
}


static void  getvarset(word nsub,setofbyte varset)
{ setofbyte   set1, set2;
   FILE * actvars;
    
   actvars=fopen(ACTVARS_NAME,"rb");
   if (nsub != 0)
   {
      fseek(actvars,sizeof(setofbyte)*(nsub - 1),SEEK_SET);
      if(!FREAD1(set1,actvars)) setofb_zero(set1) ;
   }
   else
   {
      setofb_zero(set1);
      while (FREAD1(set2,actvars)) setofb_cpy( set1, setofb_uni( set1, set2));
   }
   fclose(actvars);
   setofb_cpy(varset, set1);
}


static void  splitvars(setofbyte set1,setofbyte set_c,setofbyte set_f)
{byte         k;
 varlist      q;

   q = modelvars;
   setofb_zero(set_c);
   setofb_zero(set_f);
	k = maxsp+2; /*17*/
   while (q != NULL)
   {
      if (insetb(k,set1))
			if (q->func == NULL)
            setofb_cpy(set_c, setofb_uni(set_c, setofb(k,_E)));
         else
            setofb_cpy(set_f, setofb_uni(set_f, setofb(k,_E)));
      ++(k);
      q = q->next;
   }
}


static pointer  rd_4(char* s)
{byte         k;
 setofbyte  * p;
 varlist      q;

   p = (setofbyte *)getmem(sizeof(setofbyte));
   setofb_zero(*p);

   if (isdigit(*s))
   {  setofb_zero(*p);
      goto e;
   }

   if (modelvars == NULL)
      save_sos(14);

	k = maxsp+2; /* 17*/;
   q = modelvars;
   while (TRUE)
   {
      if (strcmp(q->varname,s) == 0)
      {
         setofb_cpy(*p,setofb(k,_E));
         goto e;
      }
      else
         q = q->next;
      if (q == NULL)
         save_sos(14);
      ++(k);
   }
e: return (pointer) p;
}


static pointer uact_4(char* ch,pointer mm)
{  return mm; }


static pointer bact_4(char ch,pointer mm1,pointer mm2)
{
   setofb_cpy( * ((setofbyte *) mm1),
               setofb_uni( * ((setofbyte *) mm1),
                           * ((setofbyte *) mm2)));
   free(mm2);
   return mm1;
}

void  getprocessvar(word nsub,setofbyte ind_var,setofbyte fnc_var)
{setofbyte   set1, set_c, set_f, set_ftmp;
 varlist     p, q;
 byte        kp, kq;
 setofbyte * m;

   getvarset(nsub,set1);
   splitvars(set1,set_c,set_f);
   setofb_cpy(ind_var,set_c);
   setofb_zero(fnc_var);
   p = modelvars;
	kp = maxsp+2 /*17*/;
	while (p != NULL && p->func == NULL)
   {  p = p->next;
      ++(kp);
   }

   while (!setofb_eq0(set_f))
   {
      q = p;
      kq = kp;
      while (!insetb(kq,set_f))
      {
         ++(kq);
         q = q->next;
      }

      m = (setofbyte *)readexprassion(q->func,bact_4,uact_4,rd_4);


		splitvars(*m,set_c,set_ftmp);
      free(m);
      setofb_cpy( ind_var, setofb_uni( ind_var, set_c));
      setofb_cpy( fnc_var, setofb_uni( fnc_var, setofb(kq,_E)));
      setofb_cpy( set_f,   setofb_uni( set_f,   set_ftmp)); 
      setofb_cpy( set_f,   setofb_aun( set_f,   setofb(kq,_E))); 
   } 
} 
