/****************************************
*                                       *
*          Written by V.Edneral         *
*              SCL of MSU               *
*               25.02.90                *
*                                       *
****************************************/
#include "tptcmac.h"
#include "physics.h"
#include "crt.h"
#include "syst2.h"
#include "transfer.h"

#include "diaprins.h"

#define xstep 6
#define ystep 4  
#define chsp ' '  
#define chp  'P'  
#define koder (maxvert + 1)
#define xstart 0  
#define xend (xstep * koder - 1)
#define ystart 2  
#define yend (ystep * (koder + 1) + 1)
#define scrwidth 65  

#ifdef BLOCK_GRAPHICS

#define vertsign 0xDB  
#define chrC0    0xC0
#define chrD9    0xD9
#define chrBF    0xBF
#define chrDA    0xDA
#define chrC8    0xC8
#define chrBC    0xBC
#define chrBB    0xBB
#define chrC9    0xC9
#define chrC4    0xC4
#define chrCD    0xCD
#define chrB3    0xB3
#define chrBA    0xBA
#define chrC5    0xC5
#define chrD8    0xD8
#define chrD7    0xD7
#define chrCE    0xCE
#else

#define vertsign '@'  
#define chrC0    '\\'
#define chrD9    '/'
#define chrBF    '\\'
#define chrDA    '/'
#define chrC8    '\\'
#define chrBC    '/'
#define chrBB    '\\'
#define chrC9    '/'
#define chrC4    '-'
#define chrCD    '='
#define chrB3    '|'
#define chrBA    '|'
#define chrC5    '+'
#define chrD8    '+'
#define chrD7    '+'
#define chrCE    '+'
#endif

typedef struct intsect 
   { 
      byte   y, xst, sp, selfvert, selfslot, 
             aliasvert, aliasslot; 
   }  intsect; 

typedef struct dscreen 
   { 
      byte         scr[xend][yend]; 
      intsect      ints[maxvert + 1]; 
      byte         mver, mhor, mxy; 
   }  dscreen; 

typedef byte interscreen[koder][yend + ystep]; 


#define pnt  struct spool * 

typedef struct spool 
   { 
      pnt          add; 
      char         stn[81]; 
   }  spool;

#undef pnt

typedef spool* pnt; 



static  dscreen    * res;          /* from diaprin */
static  boolean      hcontr;       /* from diaprin */
static  byte         isect, jsect; /* from isector */
static  boolean      ssect;        /* from isector */
static  boolean      left;         /* from diaprt  */
static  dscreen      resl, resr;   /* from diaprt  */
static  interscreen  is;           /* from diaprt  */
static  vcsect       diag;         /* from writepict  */
static  boolean      debug;        /* from writepict  */
static  integer      nstoreup;      /* from pseudo_pict*/


/* static void  cr(n,x,y,ivr,slt)
 integer n;
 integer x;
 integer y;
 integer ivr;
 integer slt; */
static void cr(integer n,integer x,integer y,integer ivr,integer slt)
{
   res->scr[x-1][y-1] =
      prtclbase[diag.vertlist[ivr-1][slt-1].partcl-1].spin % 2 == 0 ?
         ((n == 1 && left) || (n == 2 && !left)) ? chrC0 :
         ((n == 2 && left) || (n == 1 && !left)) ? chrD9 :
         ((n == 3 && left) || (n == 4 && !left)) ? chrBF :
                                                   chrDA            :
         ((n == 1 && left) || (n == 2 && !left)) ? chrC8 :
         ((n == 2 && left) || (n == 1 && !left)) ? chrBC :
         ((n == 3 && left) || (n == 4 && !left)) ? chrBB :
                                                   chrC9            ;
}   /* The end of Cr's body */


/* static char *  cmp(nm)
 char * nm; */
static char * cmp(char* nm)
{byte         i;
 static char  n[6];

   for (i = 0; (nm[i] != '\0') && (nm[i] != chsp); i++)
      n[i] = nm[i];
   n[i] = '\0';
   return n; 
} 


static void hline(integer xs,integer xe,integer y,integer ivr,integer slt)
{char    linech, arrow; 
 integer i, j, k, l; 
 char    nm[6]; 

   k = hcontr ? (xs + xe) / 2 : res->mhor - (xstep / 2) + 1; 
   if (y > res->mxy) res->mxy = y;
   
   {edgeinvert * with1 = &diag.vertlist[ivr-1][slt-1];     
      linech = (prtclbase[with1->partcl-1].spin % 2) == 0 ?
               chrC4 : chrCD; 
      l = left ? prtclbase[with1->partcl-1].anti : with1->partcl; 
      arrow = l > prtclbase[l-1].anti ? '<' : '>'; 
      if (!(inset(inp,with1->prop)   || 
            inset(intrp,with1->prop) ||
            l < prtclbase[l-1].anti)) 
         l = prtclbase[l-1].anti; 
      strcpy(nm,cmp(prtclbase[l-1].name)); 
      l = (byte)strlen(nm); 
      for (i = xs; i <= xe; i++)
         res->scr[i-1][y-1] = linech; 
      if (left) 
      { 
         j = l > 1 ? k + 1 - l : k - 1; 
         for (i = 1; i <= l; i++) 
            res->scr[i + j-1][y - 1-1] = nm[i-1]; 
         if (inset(inp,with1->prop)   || 
             inset(intrp,with1->prop) || debug) 
         { 
            if ((left ? TRUE : FALSE) == 
                (with1->moment > 0 ? TRUE : FALSE)) 
               res->scr[k - 1-1][y + 1-1] = '-'; 
            res->scr[k-1][y + 1-1] = chp; 
            res->scr[k + 1-1][y + 1-1] = 
               abs(with1->moment) > 9        ? 
               chr(abs(with1->moment) + '7') :
               chr(abs(with1->moment) + '0') ; 
            if (debug && with1->lorentz > 0) 
               res->scr[k - 1-1][y-1] =
                  with1->lorentz > 9         ? 
                  chr(with1->lorentz + '7')  : 
                  chr(with1->lorentz + '0')  ; 
         } 
      } 
      else 
      { 
         j = l > 1 ? k + l - 1 : k + 1; 
         for (i = 1; i <= l; i++) 
            res->scr[j - i-1][y - 1-1] = nm[i-1]; 
         if (inset(inp,with1->prop)   || 
             inset(intrp,with1->prop) || debug) 
         {  if ((left ? TRUE : FALSE) == 
                (with1->moment > 0 ? TRUE : FALSE)) 
               res->scr[k + 1-1][y + 1-1] = '-'; 
            res->scr[k-1][y + 1-1] = chp; 
            res->scr[k - 1-1][y + 1-1]        = 
               abs(with1->moment) > 9         ?
               chr(abs(with1->moment) + '7')  : 
               chr(abs(with1->moment) + '0')  ; 
            if (debug && with1->lorentz > 0) 
               res->scr[k - 1-1][y-1]         = 
               with1->lorentz > 9             ? 
               chr(with1->lorentz + '7')      :
               chr(with1->lorentz + '0')      ;  
         } 
      } 
      if (with1->partcl != prtclbase[with1->partcl-1].anti) 
         res->scr[k-1][y-1] = arrow;
   }
}   /* The end of Hline's body */ 


static void vline(integer x,integer ys,integer ye,integer ivr,integer slt,
                              boolean txt) 
{char     linech /*, arrow */; 
 integer  i, j, k, l; 
 char     nm[6]; 

   if (ys > ye) 
   {  --(ys);
      ++(ye);
   } 
   else 
   {  ++(ys);
      --(ye);
   } 
   k = (ys + ye) / 2;
    
   {edgeinvert *with1 = &diag.vertlist[ivr-1][slt-1]; 
      
      linech = (prtclbase[with1->partcl-1].spin % 2) == 0 ?
               chrB3 : chrBA; 
      l = left ? prtclbase[with1->partcl-1].anti : with1->partcl; 
/*
      arrow =
         ((left ? TRUE : FALSE) == (ye > ys ? TRUE : FALSE) ? TRUE : FALSE) == 
          (l < prtclbase[l-1].anti ? TRUE : FALSE) ? 'v' : '^'; 
*/ 
      if (!(inset(inp,with1->prop)   || 
            inset(intrp,with1->prop) ||
            l < prtclbase[l-1].anti)) 
         l = prtclbase[l-1].anti; 
      strcpy(nm,cmp(prtclbase[l-1].name)); 
      l = (byte)strlen(nm); 
      if (ye > ys) 
         for (i = ys; i <= ye; i++) res->scr[x-1][i-1] = linech; 
      else 
         for (i = ye; i <= ys; i++) res->scr[x-1][i-1] = linech; 
      if (txt) 
      { 
         if (left) 
         { 
            j = x - 1 - l; 
            for (i = 1; i <= l; i++) res->scr[i + j-1][k-1] = nm[i-1]; 
            if (debug) 
            { 
               if ((left ? TRUE : FALSE) == 
                   (with1->moment > 0 ? TRUE : FALSE)) 
               { 
                  res->scr[x + 1-1][k-1] = '-'; 
                  j = 2; 
               } 
               else 
                  j = 1; 
               res->scr[j + x-1][k-1] = chp;
               res->scr[j + x + 1-1][k-1]       = 
                  abs(with1->moment) > 9        ? 
                  chr(abs(with1->moment) + '7') : 
                  chr(abs(with1->moment) + '0') ; 
               if (with1->lorentz > 0)
                  res->scr[x-1][k + 1-1]        = 
                     with1->lorentz > 9         ?
                     chr(with1->lorentz + '7')  :  
                     chr(with1->lorentz + '0')  ; 
            } 
         } 
         else 
         { 
            j = x + l + 1; 
            for (i = 1; i <= l; i++) res->scr[j - i-1][k-1] = nm[i-1]; 
            if (debug) 
            { 
               if ((left ? TRUE : FALSE) == 
                   (with1->moment > 0) ? TRUE : FALSE) 
               { 
                  res->scr[x - 1-1][k-1] = '-'; 
                  j = 2; 
               } 
               else 
                  j = 1; 
               res->scr[x - j-1][k-1] = chp; 
               res->scr[x - j - 1-1][k-1]       = 
                  abs(with1->moment) > 9        ? 
                  chr(abs(with1->moment) + '7') : 
                  chr(abs(with1->moment) + '0') ; 
               if (with1->lorentz > 0) 
                  res->scr[x-1][k + 1-1]        = 
                     (with1->lorentz > 9)       ?
                     chr(with1->lorentz + '7')  :
                     chr(with1->lorentz + '0')  ; 
            } 
         } 
/*       if (partcl != prtclbase[partcl-1].anti) scr[x-1,k-1] = arrow; */ 
      }        
   } 
}   /* The end of Vline's body */ 


static void mline(integer xs,integer ys,integer xe,integer ye,
                 integer ivr,integer slt,boolean hor)
{boolean lg; 

   if (xe == xend)    
   {intsect *with1 = &res->ints[res->mver + 1-1]; 

      ++(res->mver);        
   /*  Range check error for Z->A,A,A,e1,E1 process  !  */ 
      with1->y = ye; 
      with1->xst = xs + 1; 
      with1->sp = prtclbase[diag.vertlist[ivr-1][slt-1].partcl-1].spin; 
      with1->selfvert = ivr; 
      with1->selfslot = slt; 
      {vertlink *with2 = &diag.vertlist[ivr-1][slt-1].nextvert;
         with1->aliasvert = with2->vno; 
         with1->aliasslot = with2->edno; 
      } 
   } 
         
   lg = xs == xe ? TRUE : FALSE; 
   if (lg || !hor) 
      vline(xs,ys,ye,ivr,slt,lg); 
   else 
      if ((ys == ye || hor) && xe != xend) 
         hline(xs + 1,xe - 1,ys,ivr,slt); 
   if (ys != ye && !lg) 
      if (hor) 
      { 
         cr(3,xe,ys,ivr,slt); 
         vline(xe,ys,ye,ivr,slt,FALSE); 
      } 
      else 
      { 
         if (ye > ys) 
            cr(1,xs,ye,ivr,slt); 
         else 
            cr(4,xs,ye,ivr,slt); 
         if (xe != xend) 
            hline(xs + 1,xe - 1,ye,ivr,slt); 
      } 
}  /* The end of Mline's body */ 


static byte howin(byte i)
{byte k, r=0; 

   for (k = 1; k <= diag.valence[i-1]; k++) 
      if (inset(inp,diag.vertlist[i-1][k-1].prop)) ++(r); 
   return r; 
}   /* The end of Howin's body */ 


static byte wherein(byte i)
{byte  k; 

   k = diag.valence[i-1]; 
   while (k > 0 && !inset(inp,diag.vertlist[i-1][k-1].prop)) --(k); 
   return k; 
}  /* The end of Wherein's body */ 


static void lot(byte* ylo,byte* yhi,byte ivr,byte slt,boolean hor)
{byte    iv,/* sl, */ tn, tp, yl, yh, l; 
 boolean flg; 

   *ylo = 0; 
   *yhi = 0; 
   {edgeinvert *with1 = &diag.vertlist[ivr-1][slt-1]; 
      if (inset(inp,with1->prop) || inset(intrp,with1->prop)) 
         return;
      iv = with1->nextvert.vno; 
/*      sl = with1->nextvert.edno;    */
   } 
      
   tp = howin(iv); 
   flg = TRUE; 
   tn = diag.valence[iv-1]; 
   if (tp == 0  && (tn == 4 || (hor && tn == 3  && 
       inset(intrp,diag.vertlist[iv-1][1].prop) && 
       inset(intrp,diag.vertlist[iv-1][2].prop)))) 
   { 
      lot(&yl,&yh,iv,tn,TRUE); 
      *ylo += yl + yh + 1; 
      --(tn); 
   } 
   for (l = tn; l >= 2; l--) 
   if (!inset(inp,diag.vertlist[iv-1][l-1].prop)) 
   { 
      lot(&yl,&yh,iv,l,flg); 
      if (flg) 
      { 
         flg = FALSE; 
         *ylo += yl; 
         *yhi += yh; 
      } 
      else 
         *yhi += yl + yh + 1; 
   } 
}   /* The end of Lot's body */ 


static void painter(byte xc,byte yc,byte ivr,byte slt,boolean hor)
{byte    tp, l, xcnt, ycnt, xx, yy, hh, 
         ylo, yhi, start, ks, kk, pp; 
 boolean flag; 

   flag = FALSE; 
   start = 2; 
   xcnt = xc; 
   ycnt = yc; 
   tp = howin(ivr); 
   ks = wherein(ivr); 
   kk = ks == diag.valence[ivr-1] ?  
        diag.valence[ivr-1] - 1   : 
        diag.valence[ivr-1]       ; 
   lot(&ylo,&yhi,ivr,kk,hor); 
   pp = ks == kk - 1 ? kk - 2 : kk - 1; 
   if (tp == 2) 
   { 
      if (ylo < 2) 
         ycnt += ystep; 
      else 
      {  ycnt += ylo * ystep; 
         yc += (ylo - 1) * ystep;
      } 
      mline(xstart,yc,xcnt,ycnt,ivr,slt,TRUE); 
      mline(xstart,ycnt,xcnt,ycnt,ivr,ks,TRUE); 
   } 
   else 
      if (tp == 1) 
      { 
         if (yc == ystart) ycnt += ylo * ystep; 
         if (hor && diag.valence[ivr-1] == 4) 
         { 
            lot(&hh,&yy,ivr,pp,hor); 
            ycnt += (yhi + hh + 1) * ystep; 
            start = 1; 
         } 
         mline(xstart,ycnt,xcnt,ycnt,ivr,ks,TRUE); 
      } 
      else 
         if (diag.valence[ivr-1] == 4) 
         { 
            start = 1; 
            if (!hor) cr(1,xcnt++,ycnt,ivr,slt); 
         } 
         else 
            if (hor && inset(intrp,diag.vertlist[ivr-1][1].prop) 
                    && inset(intrp,diag.vertlist[ivr-1][2].prop)) 
               start = 1; 
   res->scr[xcnt-1][ycnt-1] = vertsign; 
   if (xcnt > res->mhor) 
      res->mhor = xcnt; 
   for (l = kk; l >= 2; l--) 
   {edgeinvert *with1 = &diag.vertlist[ivr-1][l-1]; 
      if (!inset(inp,with1->prop)) 
      { 
         if (flag) 
            lot(&ylo,&yhi,ivr,l,start != 3); 
         else 
            flag = TRUE; 
         xx = inset(intrp,with1->prop) ? xend : 
              start == 3 ? xcnt : xcnt + xstep; 
         {vertlink *with2 = &with1->nextvert; 
            if (start == 1) 
            { 
               lot(&hh,&yy,ivr,pp,TRUE); 
               yy = ycnt - (yhi + hh + 1) * ystep; 
               mline(xcnt,ycnt,xx,yy,with2->vno,with2->edno,FALSE); 
               if (!inset(intrp,with1->prop)) 
                  painter(xx,yy,with2->vno,with2->edno,TRUE); 
            } 
            else 
               if (start == 2) 
               { 
                  mline(xcnt,ycnt,xx,ycnt,with2->vno,with2->edno,TRUE); 
                  if (!inset(intrp,with1->prop)) 
                     painter(xx,ycnt,with2->vno,with2->edno,TRUE); 
                  yy = yhi; 
               } 
               else 
               { 
                  yy += ylo + 1; 
                  yy = ycnt + yy * ystep; 
                  mline(xcnt,ycnt,xx,yy,with2->vno,with2->edno,FALSE); 
                  if (!inset(intrp,with1->prop)) 
                     painter(xx,yy,with2->vno,with2->edno,FALSE); 
               } 
         } 
         ++(start); 
      } 
   } 
}   /* The end of Painter's body */ 


static void diaprin(dscreen* res1)
{byte      i, j; 

   res = res1; 
   for (i = 1; i <= xend; i++) 
      for (j = 1; j <= yend; j++) 
         res->scr[i-1][j-1] = chsp; 
   res->mver = 0; 
   res->mhor = 0; 
   res->mxy  = 0; 

   hcontr = TRUE; 
   if (left) 
      painter(xstart + xstep,ystart,1,1,TRUE); 
   else 
      painter(xstart + xstep,ystart,diag.sizel + 1,1,TRUE); 
   res->mhor += xstep - 1; 
   hcontr = FALSE; 

   for (i = 1; i <= res->mver; i++) 
   {intsect *with1 = &res->ints[i-1]; 
      hline(with1->xst,res->mhor,with1->y,
      with1->selfvert,with1->selfslot); 
   } 
}   /* The end of Diaprin's body */ 


static byte lh(byte y)
{   
   if (is[jsect-1][y-1] == chsp) 
      is[jsect-1][y-1]  = ssect ? chrC4 : chrCD; 
   else 
      if (is[jsect-1][y-1] == chrB3) 
         is[jsect-1][y-1]  = ssect ? chrC5 : chrD8; 
      else 
         if (is[jsect-1][y-1] == chrBA) 
            is[jsect-1][y-1]  = ssect ? chrD7 : chrCE; 
         else 
            return 1;
   return 0; 
}  /* The end of Lh's body */ 


static void lv(void)
{byte c; 

   c = is[isect-1][jsect-1];
   
   is[isect-1][jsect-1]           = 
      c == chsp                   ? 
         (ssect ? chrB3 : chrBA)  : 
      c == chrC4                  ? 
         (ssect ? chrC5 : chrD7)  : 
      c == chrCD                  ? 
         (ssect ? chrD8 : chrCE)  : c; 
}   /* The end of Lv's body */ 


static char isector(void)
{integer  k, y1, y2, vv, ss, pp=0, tt=0, sh=0; 

   /* Nested function: lh */ 
   /* Nested function: lv */ 

   k = resl.mver; 
   for (isect = 1; isect <= k; isect++) 
   { 
      {intsect *with1 = &resl.ints[isect-1]; 
         y1 = with1->y; 
         vv = with1->aliasvert; 
         ss = with1->aliasslot;    
      } 
      jsect = 1; 
      while (resr.ints[jsect-1].selfvert != vv || 
             resr.ints[jsect-1].selfslot != ss) 
         ++(jsect); 
      {intsect *with1 = &resr.ints[jsect-1]; 
         if (y1 == with1->y) 
            ++(sh); 
         else 
            if (y1 == with1->y + ystep) 
               ++(pp); 
            else 
               if (y1 == with1->y - ystep) 
                  ++(tt); 
      } 
   } 
   sh = tt > sh ? - ystep : pp > sh ?  ystep : 0; 
   goto b;

a: if (sh < 0) 
      ++(sh); 
   else 
      --(sh); 

b: pp = sh < 0 ? resl.mxy - sh : resr.mxy + sh; 
   if (resr.mxy > pp) pp = resr.mxy; 
   if (resl.mxy > pp) pp = resl.mxy; 
   ++(pp); 
   
   for (isect = 1; isect <= k; isect++) 
      for (jsect = 1; jsect <= pp; jsect++) 
         is[isect-1][jsect-1] = chsp; 

   for (isect = 1; isect <= k; isect++) 
   { 
      {intsect *with1 = &resl.ints[isect-1]; 
         y1 = with1->y; 
         ssect = with1->sp % 2 == 0 ? TRUE : FALSE; 
         vv = with1->aliasvert; 
         ss = with1->aliasslot;    
      }    
      jsect = 1; 
      while (resr.ints[jsect-1].selfvert != vv || 
             resr.ints[jsect-1].selfslot != ss) 
         ++(jsect); 
      y2 = resr.ints[jsect-1].y; 
      if (sh < 0) 
         y1 -= sh; 
      else 
         y2 += sh; 
      if (y1 == y2) 
         for (jsect = 1; jsect <= k; jsect++) 
            tt = lh(y1); 
      else 
         if (y1 < y2) 
         { 
            for (jsect = 1; jsect <= isect - 1; jsect++) 
               if (lh(y1) == 1) 
                  goto a;
            if (is[isect-1][y1-1] != chsp) 
               goto a;
            is[isect-1][y1-1] = ssect ? chrBF : chrBB; 
            for (jsect = y1 + 1; jsect <= y2 - 1; jsect++) 
               lv(); 
            if (is[isect-1][y2-1] != chsp) 
               goto a;
            is[isect-1][y2-1] = ssect ? chrC0 : chrC8; 
            for (jsect = isect + 1; jsect <= k; jsect++) 
               if (lh(y2) == 1) 
                  goto a;
         } 
         else 
         { 
            for (jsect = 1; jsect <= isect - 1; jsect++) 
               if (lh(y1) == 1) 
                  goto a;
            if (is[isect-1][y1-1] != chsp) 
               goto a;
            is[isect-1][y1-1] = ssect ? chrD9 : chrBC; 
            for (jsect = y2 + 1; jsect <= y1 - 1; jsect++) 
               lv(); 
            if (is[isect-1][y2-1] != chsp) 
               goto a;
            is[isect-1][y2-1] = ssect ? chrDA : chrC9; 
            for (jsect = isect + 1; jsect <= k; jsect++) 
               if (lh(y2) == 1) 
                  goto a;
         } 
   } 
   return sh; 
}  /* The end of Isector's body */ 


static void diaprt(pnt* fout)
{integer  i, j, r; 
 integer  sh, p, k, kk; 
 pnt      po, pn = NULL; 


   left = TRUE; 
   diaprin(&resl); 
   left = FALSE; 
   diaprin(&resr); 
   sh = isector(); 
   kk = scrwidth - resl.mhor - resl.mver - resr.mhor - 2; 
   if (kk < 0) kk = 0; 
   kk = kk / 2; 
   r = sh < 0 ? resl.mxy - sh : resr.mxy + sh; 
   if (resr.mxy > r) r = resr.mxy; 
   if (resl.mxy > r) r = resl.mxy; 
   p = 68 - 1 - kk - resl.mhor - resl.mver - resr.mhor;   
/* Pukhov   80 -> 68  */ 
   ++(r); 
   for (j = 1; j <= r; j++) 
   { 
      po = pn; 
      pn=(pnt)getmem(sizeof(spool));
      if (j == 1) 
         *fout = pn; 
      else 
         po->add = pn;
      k = kk; 
      if (k > 0) memset(pn->stn,chsp,k);
      if (sh < 0) 
         if (j <= -sh)
         {
            memset(pn->stn + k,chsp,resl.mhor);
            k += resl.mhor;
         }
         else 
         {
            for (i = 0; i < resl.mhor; i++) 
               pn->stn[k + i] = resl.scr[i][j + sh-1]; 
            k += i;
         }
      else 
         if (j <= r)
         { 
            for (i = 0; i < resl.mhor; i++) 
               pn->stn[k + i] = resl.scr[i][j-1];
            k += i;
         }
         else
         {  
            memset(pn->stn + k,chsp,resl.mhor);
            k += resl.mhor;
         }
      for (i = 0; i < resl.mver; i++) 
         pn->stn[k + i] = is[i][j-1];
      k += i;
#ifdef BLOCK_GRAPHICS
      pn->stn[k++] = '|';
#else
      pn->stn[k++] = '!';
#endif
      if (sh > 0) 
         if (j <= sh)
         {
            memset(pn->stn + k,chsp,resr.mhor);
            k += resr.mhor;
         }
         else
         {
            for (i = 0; i < resr.mhor; i++) 
               pn->stn[k + i] = resr.scr[resr.mhor - i-1][j - sh-1];
            k += i;
         }
      else 
         if (j <= r) 
         {
            for (i = 0; i < resr.mhor; i++) 
               pn->stn[k + i] = resr.scr[resr.mhor - i-1][j-1];
            k += i;
         }
         else 
         {
            memset(pn->stn + k,chsp,resr.mhor);
            k += resr.mhor;
         }
      if (p > 0) 
      {  memset(pn->stn + k,chsp,p);
         k += p;
      }
       pn->stn[k] = '\0';
   } 
   pn->add = NULL; 
}  /* The end of Diaprt's body */ 



void writeTextDiagram(vcsect* diag1,int label,char comment,FILE* outfile)
{pnt  pp, pict; 

   diag= *diag1;
   debug=label;
   diaprt(&pp); 
   while (pp != NULL) 
   { 
     pict = pp; 
     pp = pict->add; 
     fprintf(outfile,"%c%s\n",comment,pict->stn); 
     free(pict);
   } 
} 

/*
void writepict_(csdiagram* csd,boolean debug1,char ch,FILE * outfile)
{pnt         pp, pict; 
   transfdiagr(csd,&diag); 
   debug=debug1;
   diaprt(&pp); 
   while (pp != NULL) 
   { 
     pict = pp; 
     pp = pict->add; 
     fprintf(outfile,"%c%s\n",ch,pict->stn); 
     free(pict);
   } 
} 



void makeTextPicture(vcsect* diagr, pnt *  pp, int label )
{
  debug=label;
  diag =*diagr;
  diaprt(pp);
}
*/

void pseudo_pict(csdiagram* csd,boolean debug1,boolean sqrd,boolean* 
                                           moveup)
{pnt         pp, pict;
 integer     k = 1, l;
 char      * st;

   l = sqrd ? 5 : 20;
   if (*moveup) nstoreup = 0;
   transfdiagr(csd,&diag);
   debug = debug1;
   diaprt(&pp);

   clrbox(1,2,80,23);

   pict = pp;
   while ((pict = pict->add) != NULL) k++;
   k = k > 22 ? 1 : (22 - k)/2 + 1;

   while (pp != NULL)
   {
     pict = pp;
     pp = pict->add;
     if (!(*moveup) && nstoreup > 0)
        nstoreup--;
     else
        if (++k < 23)
        {
           goto_xy(l,k);
           st = pict->stn;
           if (sqrd)
              print("%s",st);
           else
#ifdef BLOCK_GRAPHICS
              while(*st != '|')
#else
              while(*st != '!')
#endif
		 print("%c",*st++); /* putchar(*st++); */
         }
         else
            nstoreup++;
      free(pict);
   }
   *moveup = (nstoreup == 0);
}

