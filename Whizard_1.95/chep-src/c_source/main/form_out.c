#include "tptcmac.h"
#include "syst2.h"
#include "global.h"
#include "physics.h"
#include "files.h"
#include "crt.h"
#include "procvar.h"
#include "os.h"

#include "out_serv.h"
#include "form_out.h"


static void  writeparameters(int nsub)
{setofbyte   varsofprocess, funcofprocess;
 varlist     modl;
 byte        k;
 boolean     first;
 char        ch;


   writeF("\n");
   getprocessvar(nsub,varsofprocess,funcofprocess);
   modl = modelvars;
   k = 0;
   first = TRUE;
   ch = ' ';
   writeF("Symbols\n");

   do
   {  k++;
      if (insetb(k + maxsp+1,varsofprocess))
      { 
         writeF( " %c%s\n",ch,modl->varname);
         ch = ',';
      }
      if (insetb(k + maxsp+1,funcofprocess))
      {
         writeF( " %c%s\n",ch,modl->varname);
         ch = ',';
      }                                                    
      modl = modl->next;
   }  while (modl != NULL);
   writeF(" ;\n");
}



static void  writefunctions(integer nsub)
{setofbyte   varsofprocess, funcofprocess;
 varlist     modl;
 byte        k;
 char        s[STRSIZ];
 
 
 
   writeF("\n");
   getprocessvar(nsub,varsofprocess,funcofprocess);
   modl = modelvars;
   k = 0;
   do
   {
      if (insetb(++k + maxsp+1,funcofprocess))
      {  sscanf(modl->func,"%[^|]",s);
         trim(s);
         writeF("id %s=%s;\n",modl->varname,s);
      }
      modl = modl->next;
   }  while (modl != NULL);
}


static void startForm(int nsub, int* prtclNum,int ncalc)  
{
  char fname[STRSIZ];
  
  readvars('F');
  sprintf(fname,"%sresults%cusr%d.frm",pathtouser, f_slash,nsub);  
  outFileOpen(fname);
  
  writeF("\n#-\nCFunction Sqrt;\n#procedure userWork(nnn)\n");        

  writefunctions(nsub);
  
  emitconvlow(prtclNum,'F');

  writeF("#if 'nnn' == %d\n.end\n#endif\n#endprocedure\n",ncalc);
  outFileClose();

  sprintf(fname,"%sresults%csum%d.frm",pathtouser, f_slash,nsub);
  outFileOpen(fname);                                        
  writeLabel('*');
  writeF("vector p1,p2,p3,p4,p5,p6;\n");         
  writeparameters(nsub);       
  writeF("CFunction den;\n");
  writeF("Symbol factor;\n");
  writeF("#include usr%d.frm\n",nsub);
}


static void  diagramForm(vcsect * vcs, catrec * cr )
{
 int       nexpr,degr; /* must be byte for read_ */
 int         i;

   writeF("\n*Diagrama number %d-%d;\n",cr->nsub_,cr->ndiagr_); 

   if (vcs != NULL)  DiagramToOutFile(vcs,0,'*');

   seekArchiv(cr->factpos);
   nexpr=readNN(); /*  Nexpr must be equal 2  */
   writeF("Local FACTOR%d = (",cr->ndiagr_); 
   rewritepolynom();writeF(")/(");rewritepolynom();writeF(");\n");
   writeF("Local ANS%d = factor*(",cr->ndiagr_);
   
   seekArchiv(cr->rnumpos);
   nexpr=readNN(); /*  Nexpr must be equal 1  */
   rewritepolynom();writeF(")");
   
   seekArchiv(cr->denompos);

   nexpr=readNN();   /*  Nexpr must be equal ??  */ 
   for (i = 1; i <= nexpr; i++)
   {
      degr=readNN();
      jumppolynom();
      jumppolynom();
      writeF("\n *den("); rewritepolynom(); writeF(",-%d)",degr);
   }
   writeF(";\n");
   writeF( "\n#call userWork{%d}\n",cr->ndiagr_);
}

static void endForm(int * prtclNum)
{
  writeF("*\n");
  outFileClose();
}  


void  makeFormOutput(void)
{ makeOutput(startForm,diagramForm,endForm); }

