#include "tptcmac.h"
#include "os.h"
#include "syst2.h"
#include "global.h"
#include "physics.h"
#include "crt.h"
#include "files.h"
#include "procvar.h"
#include "out_serv.h"

#include "math_out.h"


static void  writeprocessname(int* prtclNum)
{  int i; 

   writeF("  Process  ");
   for(i=1;i<=nin;i++) 
   {
     writeF("%s(p%d)",prtclbase[prtclNum[i]-1].name,i);
     if (i<nin) writeF("+"); else writeF("->");
   }
   
   for(i=nin+1;i<=nin+nout;i++) 
   {  writeF("%s(p%d)",prtclbase[prtclNum[i]-1].name,i);
      if (i<nin+nout) writeF("+");else writeF("\n");
   }
} 


static void  emitprocessname(int * prtclNum)
{
   int i;

   writeF("InParticles = {");
   for(i=1;i<=nin;i++) 
   {
      writeF("\"%s\"",prtclbase[prtclNum[i]-1].name);
      if (i<nin) writeF(","); else writeF("}\n");
   }
   
   writeF("OutParticles = {");
   
   for(i=nin+1;i<=nin+nout;i++) 
   {  writeF("\"%s\"",prtclbase[prtclNum[i]-1].name);
      if (i<nin+nout) writeF(",");else writeF("}\n");
   }

} 



static void  emitexpression(catrec*  cr)
{ int nexpr , degr;
  int       i;

   seekArchiv(cr->factpos);
   nexpr=readNN();   /*  Nexpr must be equal 2  */
   writeF("TotFactor = ((");
   rewritepolynom();
   writeF(")/(");
   rewritepolynom();
   writeF("));"); writeF("\n");

   seekArchiv(cr->rnumpos);
   nexpr=readNN();        /*  Nexpr must be equal 1  */
   
   writeF("Rnum =(");rewritepolynom();writeF(");\n");

   seekArchiv(cr->denompos);
   nexpr=readNN();   /*  Nexpr must be equal ??  */

   writeF("N$=%d;\n",(int) nexpr);
   for (i = 1; i <= nexpr; i++)
   {
      degr=readNN();      
      writeF("Degr$[[%d]]=%d;\n",i,degr); 
      writeF("WIDTH$[[%d]]=(",i); rewritepolynom(); writeF(");\n");
      writeF("MASS$[[%d]]=(",i); rewritepolynom(); writeF(");\n"); 
      writeF("SS$[[%d]]=("); rewritepolynom(); writeF(");\n");
   }
   writeF("\n");
}


static void  sim(byte  i, byte  j)
{byte        k; 

   writeF("    Print[\"... symmetrization of process : \",Process];\n"); 
   writeF("    SummTmp = Summ;\n"); 
   writeF("\n"); 
   for (k = i; k <= j - 1; k++) 
      writeF(
              "    Summ = Summ + ( SummTmp /.{p%d->p%d,p%d->p%d} );\n",
              k + nin,j + nin,j + nin,k + nin); 
   writeF("    Summ = Summ /%d;\n",(int)(j + 1 - i)); 
   writeF("\n"); 
} 


static void  symmetrisation(int* prtclNum)
{ int j,i=1;

   writeF("\n(*\n"); 
   writeF("  Symmetrisation of Summ :\n"); 
   writeF("*)\n"); 
   writeF("\n"); 
   writeF("If[ Not[ TrueQ[ UserDefined ] ] ,\n"); 
   i = 1; 
   while (i < nout) 
   { 
      j = i + 1; 
      while (j <= nout && prtclNum[nin+i]==prtclNum[nin+j]) 
      { 
         sim(i,j); 
         ++(j); 
      } 
      i = j; 
   } 
   writeF("    Summ = Together[ Summ ]  ];\n"); 
} 


static void  writedefaultsum(void)
{ 
   writeF("sum$num=0;\n"); 
   writeF("sum$den=1;\n"); 
   writeF("DefaultSum:=\n"); 
   writeF("Module[{b,d,w,ans},\n"); 
   writeF("d=1;\n"); 
   writeF("Do[ \n"); 
	writeF("w=Weps*MW$[[i]]^2 ;\n");
   writeF("If[ w==0 || Degr$[[i]]==2,\n"); 
   writeF("    d=d*(SS$[[i]]^Degr$[[i]]+w),\n"); 
   writeF("    d=d*(SS$[[i]]^2+w)/SS$[[i]]  ],\n"); 
   writeF("{i,1,N$}];\n"); 
   writeF("ans=Cancel[Together[a/sum$den+1/d ]] ;\n"); 
   writeF("sum$den=Denominator[ans] ;\n"); 
   writeF("ans=Numerator[ans] /. Weps->0 ;\n"); 
   writeF("b=ans /. a->0;\n"); 
   writeF("ans=Cancel[FactorTerms[Expand[(ans-b)]]/a] ;\n"); 
   writeF("sum$num=sum$num*ans+b*TotFactor*Rnum;\n"); 
   writeF("Clear[TotFactor,Rnum];\n"); 
   writeF("Summ=sum$num/sum$den;\n"); 
   writeF("];\n"); 
} 


static void  writeparameters(integer nsub)
{setofbyte   varsofprocess, funcofprocess;
 varlist      modl; 
 byte         k = 0, l, ll; 
 boolean      first = TRUE; 
 char         ch = ' '; 
 char         s[STRSIZ]; 

   writeF("\n"); 
   getprocessvar(nsub,varsofprocess,funcofprocess);
   modl = modelvars;
   writeF("Parameters={\n"); 

   do
   {
      if (insetb(++k + maxsp+1,varsofprocess))
      {
         writeF("%c%s -> ",ch,modl->varname);
			sbld(s,"%17.11E",modl->varvalue);
         l = cpos('E',s);
         if (l != 0) 
         { 
            strdelete(s,l,1); 
            strinsert("*10^(",s,l); 
            sbld(s,"%s)",s); 
         } 
         writeF("%s\n",s); 
         if (first) 
         {  first = FALSE; 
            ch = ','; 
         }
      }
      modl = modl->next;
   }  while (modl != NULL);

   writeF("                 };\n"); 
   writeF("\n"); 

   modl = modelvars;
   k = 0;
   do
   {
      if (insetb(++k + maxsp+1,funcofprocess))
      {  sscanf(modl->func,"%[^|]",s);
         trim(s);
         do 
         { 
            l = spos("**",s); 
            if (l != 0) 
            { 
               s[l-1] = '^'; 
               strdelete(s,l + 1,1); 
            } 
         }  while (l != 0); 
         do 
         { 
            l = spos("Sqrt(",s); 
            if (l != 0) 
            { 
               s[l + 4-1] = '['; 
               l += 4; 
               ll = 1; 
               while (ll > 0) 
               { 
                  ++(l); 
                  if (s[l-1] == ')')
                     --(ll);
                  else
                     if (s[l-1] == '(')
                        ++(ll); 
               } 
               s[l-1] = ']'; 
            } 
         }  while (l != 0); 
         sbld(s,"%s=%s;",modl->varname,s); 
         writeF(s);
         writeF("\n");
      }
      modl = modl->next;
   }  while (modl != NULL);
} 


static void startMath(int nsub, int* prtclNum,int ncalc)
{
   char f_name[STRSIZ];
  
   readvars('M'); 

   sprintf(f_name,"%sresults%csum%u.m",pathtouser,f_slash,nsub);
   outFileOpen(f_name);

   writeF("(*\n"); writeLabel(' ');
   writeprocessname(prtclNum);
   writeF("*)\n"); 
   writedefaultsum(); 
   writeparameters(nsub); writeF("\n");
   emitprocessname(prtclNum); 
   writeF("\n"); 
   writeF("SetAttributes[ SC, Orderless ];\n"); 
   writeF("\n"); 
   writeF("SC[ a_ , b_ + c_ ] := SC[a,b]+SC[a,c];\n"); 
   writeF("\n"); 
   writeF("SC[ x_?NumberQ * a_ , b_ ] := x * SC[ a, b ]\n"); 
   writeF("\n"); 
   writeF("\n"); 

   emitconvlow(prtclNum,'M' );
   
   writeF("If[ UserDefined ,\n"); 
   writeF("    Print[\"... running user-defined procedures on process : \",Process ],\n"); 
   writeF("    Print[\"     ... summation of process : \",Process ],\n"); 
   writeF("    Print[\"     ... summation of process : \",Process ]];\n"); 
   
   writeF("\n"); 
   writeF("Degr$=Table[0,{6}];\n"); 
   writeF("MW$=Table[0,{6}];\n"); 
   writeF("SS$=Table[0,{6}];\n"); 
   writeF("Summ = 0;\n"); 
}


static void  diagramMath(vcsect * vcs,catrec * cr)
{ 
   writeF("\n(*\n"); 
   writeF("  Diagram  %d in subprocess %d\n",cr->ndiagr_,cr->nsub_);               
   if (vcs != NULL)  DiagramToOutFile(vcs,0,' ');  
   writeF("*)\n");
   emitexpression(cr);
   writeF("If[ UserDefined, UserProcess, DefaultSum, DefaultSum];\n");
}

static void endMath(int * prtclNum)
{  symmetrisation(prtclNum);
   outFileClose();
}

void makeMathOutput(void)
{ makeOutput(startMath,diagramMath,endMath);}
