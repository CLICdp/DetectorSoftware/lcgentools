#include "tptcmac.h"
#include "syst2.h"
#include "crt.h"
#include "global.h"
#include "lbl.h"


void  cheplabel(byte level)
{
   int key;
   if (level > 1) return;
   goto_xy(28,5);  print("Moscow State University");
   goto_xy(25,7);  print("Institute for Nuclear Physics");
   goto_xy(24,8);  print("Symbolic Computation Laboratory");
   goto_xy(29,12); print("CompHEP 3.2 (24.1.97)");
   goto_xy(16,14); print("a package for computation in high energy physics");
   goto_xy(10,15); print("***** This is W.Kilian's private F90 version (19.3.99) *****");
   goto_xy(3,18);  print("AUTHORS: A.Pukhov, P.Baikov,E.Boos,M.Dubinin,V.Edneral,V.Ilyin,");
   goto_xy(12,19); print("D.Kovalenko, A.Kryukov,S.Shichanin,A.Semenov");
   goto_xy(3,22);  print("For contacts:   ilyin@theory.npi.msu.su");
   goto_xy(18,23); print("pukhov@theory.npi.msu.su");
   goto_xy(3,24) ; print("WWW page: http://theory.npi.msu.su/~pukhov/comphep.html");

   do { key=inkey();} while (key == KB_SIZE); 
   clr_scr(White,Black);
   return;
}
