#include "tptcmac.h"
#include "syst2.h"
#include "os.h"
#include "crt.h"
#include "global.h"
#include "physics.h"
#include "screen.h"
#include "crt_util.h"
#include "procvar_.h"
#include "optimise.h" 
#include "n_compil.h" 
#include "num_tool.h" 
#include "lbl.h"
#include "sos.h"
#include "files.h"
#include "help.h"
#include "graf.h"
#include "file_scr.h"
#include <stdlib.h>


int main(int argc,char** argv)
{   /*  MAIN - COMPHEP  */

 realseq  pans,pans_;
 char   procName[60],xName[60],yName[60];

	int n,nr;
	double x1,y1,er1, x2,y2,er2;


	strcpy(_pathtocomphep,argv[0]);
	n = strlen(_pathtocomphep)-1;
	while (n>=0 &&  _pathtocomphep[n] != f_slash) n--;
        _pathtocomphep[n]='\0';
	strcpy(_pathtouser,defaultPath);



	for ( n=1;n<=(argc-1);n++)
	{ 
	  if (strcmp(argv[n],"blind")==0 )  blind=TRUE;
	}
      strcpy( pathtoresults,defaultPath );


      sprintf(pathtocomphep,"%s%c",_pathtocomphep,d_slash);
      sprintf(pathtohelp,"%shelp%c",pathtocomphep,f_slash);
                        
      fgets0(procName,50,stdin); trim(procName);
      fgets0(yName,50,stdin);  trim(yName);
      fgets0(xName,50,stdin);trim(xName);
      
      pans=NULL;

      fscanf(stdin,"%lf%lf%lf%*d",&x1,&y1,&er1);
         
      while(TRUE)  
      {        
          nr=fscanf(stdin,"%lf%lf%lf%*d",&x2,&y2,&er2);

          pans_=pans;          
          pans=(realseq )getmem(sizeof(realseqrec));
          pans->next=pans_;

          pans->x =x1;
          pans->y =y1;
          pans->x1=x2;
          pans->y1=y1;
          
          pans_=pans;          
          pans=(realseq )getmem(sizeof(realseqrec));
          pans->next=pans_;

          pans->x =(x1+x2)/2;
          pans->y = y1+er1;
          pans->x1=(x1+x2)/2;
          pans->y1=y1-er1;
          if (nr == 1) goto lplot;
          x1=x2;
          y1=y2;
          er1=er2;          
      } 
      lplot:
      start1(version,scat("%s%s",pathtocomphep,"icon"),
                  "comphep.ini;../comphep.ini;[-]comphep.ini");  
      clearTypeAhead();
      plot_1(FALSE,pans,FALSE,procName,xName,yName);       
      finish();

}
