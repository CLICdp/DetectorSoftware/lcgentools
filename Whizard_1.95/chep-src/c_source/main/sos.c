#include "tptcmac.h"
#include "syst2.h"
#include "files.h"
#include "global.h"
#include "physics.h"
#include "crt.h"
#include "crt_util.h"
#include "os.h"

#include "sos.h"

typedef struct allsaved
   {
        char
        n_model;
        hadron       hadr1, hadr2;
        whohow       inclp, liminsp;
        byte         nin, nout, n_x;
        double         sqrts, missingmass;
        shortstr     processch,
                     hadr1ch,
                     hadr2ch,
                     sqrtsch,
                     limpch;
        integer      nsub, ndiagr;
        word         tot_f, tot_sq, del_f, del_sq, sub_f, sub_sq, calc_sq;
        char         exitlevel;
        char         exitmenu[255];
        optionstp    optn;
        char         fortname[3];
   } allsaved;   /*  Type AllSaved  */


void restoreent(int* nsub,int* ndiagr,int* n_model,char* exitlevel)
{
 integer      i;
 allsaved     all;
 FILE * ff;
 
   ff=fopen(scat("%stmp%csafe",pathtouser,f_slash),"rb");
   if (ff != NULL)
   {
      FREAD1(all,ff);
      fclose(ff);

      hadr1 = all.hadr1;
      hadr2 = all.hadr2;
      lvcpy( inclp, all.inclp);
      lvcpy( liminsp, all.liminsp);
      nin = all.nin;
      nout = all.nout;
      n_x = all.n_x;
      sqrts = all.sqrts;
      missingmass = all.missingmass;
      strcpy(processch,all.processch);
      strcpy(hadr1ch,all.hadr1ch);
      strcpy(hadr2ch,all.hadr2ch);
      strcpy(sqrtsch,all.sqrtsch);
      strcpy(limpch,all.limpch);
      *nsub = all.nsub;
      *ndiagr = all.ndiagr;
      *n_model = all.n_model;
      *exitlevel = all.exitlevel;
      strcpy(exitmenu,all.exitmenu);
      options = all.optn;
      strcpy(fortname,all.fortname);
      totdiag_f = all.tot_f;
      totdiag_sq = all.tot_sq;
      deldiag_f = all.del_f;
      deldiag_sq = all.del_sq;
      subproc_f = all.sub_f;
      subproc_sq = all.sub_sq;
      calcdiag_sq = all.calc_sq;
   }
   else
   {
      strcpy(processch,"e1,E1 -> e2,E2");
      strcpy(hadr1ch,"");
      strcpy(hadr2ch,"");
      strcpy(sqrtsch,"90");
      strcpy(limpch,"");
      *nsub = 1;
      *ndiagr = 1;
      *n_model = 1;
		*exitlevel = 0;
		strcpy(exitmenu,"\026 M. - C. integration  "
		                    " Make FORTRAN         ");
		for (i=3;i<=10;i++)
		sbld(exitmenu, "%s program %2i           "  , exitmenu,i);
      sbld(exitmenu, "%s   R E N A M E        "  , exitmenu);
		options = setof(conslow,graphic,_E);
      strcpy(fortname,"");
   }   /*  ioErr  */
}


void saveent(int nsub,int ndiagr,int n_model, char exitlevel)
{  FILE * ff;
   allsaved     all;
    
   all.hadr1 = hadr1;
   all.hadr2 = hadr2;
   lvcpy( all.inclp, inclp);
   lvcpy( all.liminsp, liminsp);
   all.nin = nin;
   all.nout = nout;
   all.n_x = n_x;
   all.sqrts = sqrts;
   all.missingmass = missingmass;
   strcpy(all.processch,processch);
   strcpy(all.hadr1ch,hadr1ch);
   strcpy(all.hadr2ch,hadr2ch);
   strcpy(all.sqrtsch,sqrtsch);
   strcpy(all.limpch,limpch);
   all.nsub = nsub;
   all.ndiagr = ndiagr;
   all.n_model = n_model;
   all.exitlevel = exitlevel;
   strcpy(all.exitmenu,exitmenu);
   all.optn = options;
   strcpy(all.fortname,fortname);
   all.tot_f = totdiag_f;
   all.tot_sq = totdiag_sq;
   all.del_f = deldiag_f;
   all.del_sq = deldiag_sq;
   all.calc_sq = calcdiag_sq;
   all.sub_f = subproc_f;
   all.sub_sq = subproc_sq;

   ff=fopen(scat("%stmp%csafe",pathtouser,f_slash),"w"); 
   fwrite(&all,sizeof(all),1,ff);
   fclose(ff);
}


void  save_sos(char ercode)
{
 word         nproc;
 csdiagram    cd;
 marktp mark;
 
   if (ercode == -2)
   {
      saveent(nsub,ndiagr,n_model,4);

      finish();
      exit(241);  /*  Restart  */
   }
   
   if (ercode == -1) /* Heap is empty */
   { mark.blk_=NULL;
     mark.pos_=0;
     release_(&mark);
   } 
     /*  Heep empty   */
     /*  TooLargeNumber  */
     /*  TooManyIdentifiers  */
     /*  RangeCheckError  */
   if ((ercode < 0)   || (ercode == 7) ||
       (ercode == 11) || (ercode == 12))   /*  Restart  */
   {
      saveent(nsub,ndiagr,n_model,2);
      nproc = ftell(diagrq) - sizeof(cd);
      fseek(diagrq,nproc,SEEK_SET);
      FREAD1(cd,diagrq);
      cd.status = -2;
      fseek(diagrq,nproc,SEEK_SET);
      FWRITE1(cd,diagrq);
      finish();
      exit(241);  /*  Restart  */
   }
   else
/*  not disk space  */
   if ((ercode == 40))
   {
      /*  freez */
      saveent(nsub,ndiagr,n_model,1);

      finish();
      exit(0);  /*  End of work  */
   }
   else   /* 14 */
   {
      saveent(nsub,ndiagr,n_model,3);
      messanykey(10,10," Check model !$");
      finish();
      exit(251);  /*  View and Edit  */
   }
}
