#include "tptcmac.h"
#include "syst2.h"
#include "crt.h"
#include "global.h"
#include "physics.h"
#include "files.h"
#include "crt_util.h"
#include "os.h"
#include "squar.h"


#define permlist struct frgmperm *
typedef struct frgmperm
   {
      permut      perm;
      permlist    next;
   }  frgmperm;
#undef permlist
typedef struct frgmperm *permlist;

#define grooplist struct frgmgroop *
typedef struct frgmgroop
   {
      permut       perm;
      word         left;
      word         res;
      grooplist    next;
   }  frgmgroop;
#undef grooplist
typedef struct frgmgroop *grooplist;

#define ampllist struct frgmampl *
typedef struct frgmampl
   {
      adiagram     dgrm;
      permut       perm;
      permlist     gen;
      word         dim;
      ampllist     next;
   }  frgmampl;
#undef ampllist
typedef struct frgmampl *ampllist;

static csdiagram    sqres;
static word         nstart, ntot, nsdiagram, ndel, nrest,
                    ncalc, firstpos;
static word         nsuba, nsubcs;
static ampllist     amplitudes, amp1, amp2;
static boolean      testsim;
static permut       cononout, rvrs;
static grooplist    groop, sim, sim1;
static permlist     cgen;
static word         constrdiagr, maxdiagr;


static void  smpl_linker(permut perm1,permut perm2,permut perm)
{byte  i, j, c;
 permut perma,permb;
   lvcpy(perma,perm1);
   lvcpy(permb,perm2);
   for (i = 1; i <= nout; i++)
   {
      c = perma[i-1];
      j = 1;
      while (c != permb[j-1]) j++;
      perm[i-1] = j;
      permb[j-1] = 0;
   }
}


static permut      allptest; 

static boolean     allpout[MAXINOUT]; 

static void  constrsim(byte i,permut stab,grooplist* sim)
/* byte i;
 permut stab;
 grooplist * sim;*/
{byte        j; 
 grooplist    ad_sim; 

   if (i > nout) 
   { 
      ad_sim = (grooplist)getmem(sizeof(struct frgmgroop));
      lvcpy(ad_sim->perm,allptest); 
      ad_sim->next = *sim; 
      *sim = ad_sim; 
   } 
   else 
   { 
      for (j = nout; j >= 1; j--) 
      if (allpout[j-1] && (stab[i-1] == stab[j-1])) 
      { 
         allpout[j-1] = FALSE; 
         allptest[i-1] = j; 
         constrsim(i + 1,stab,sim); 
         allpout[j-1] = TRUE; 
      } 
   } 
} 

static void  allperm(permut stab,grooplist* sim)
{byte        i; 

/* Nested function: constrsim */ 

  *sim = NULL; 
  for (i = 1; i <= nout; i++) allpout[i-1] = TRUE; 
  constrsim(1,stab,sim); 
} 


static void  multperm(permut p1,permut p2,permut pres)
{byte  i; 
 permut a,b;
 lvcpy(a,p1);
 lvcpy(b,p2);
   for (i = 1; i <= nout; i++) pres[i-1] = b[a[i-1]-1]; 
} 

static void  revers1(permut p,permut pres)
/* permut p;
 permut pres;*/
{byte  i; 
   for (i = 1; i <= nout; i++) pres[p[i-1]-1] = i; 
} 


static boolean  eq(adiagram branch1,adiagram branch2,byte l1,byte l2)
{byte  i; 

   if (l1 == l2) 
      for (i = 1; i <= l1; i++) 
         if (branch1[i-1] != branch2[i-1]) 
            return FALSE; 
   return l1 == l2; 
} 

static void  addgen(byte nbeg1,byte nbeg2,byte nl,permlist* gen)
{permut       prm; 
 byte         i; 
 permlist     ad_gen; 

   for (i = 1; i <= nout; i++) prm[i-1] = i; 
   for (i = 0; i <= nl - 1; i++) 
   { 
      prm[nbeg1 + i-1] = nbeg2 + i; 
      prm[nbeg2 + i-1] = nbeg1 + i; 
   } 
   ad_gen = (permlist)getmem(sizeof(struct frgmperm));
   lvcpy(ad_gen->perm,prm); 
   ad_gen->next = *gen; 
   *gen = ad_gen; 
} 


static void  onebranch(adiagram branch,byte beg,byte* l,adiagram grph)
{char  c, g, k; 

   c = 0; 
   *l = 0; 
   k = 1; 
   while (c != 1) 
   { 
      g = grph[k + beg - 1-1]; 
      branch[k-1] = g; 
      (*l)++; 
      k++; 
      if (g > 0) c++; else c--; 
   } 
} 

static void  dobranches(byte root,adiagram grph,adiagram branch1,
                 adiagram branch2,adiagram branch3,byte* l1,byte* l2,byte* l3)
{ 
   /* Nested function: onebranch */ 

   onebranch(branch1,root + 1,l1,grph); 
   if (grph[root + *l1 + 1-1] != 0) 
   { 
      onebranch(branch2,root + *l1 + 1,l2,grph); 
      *l3 = 0; 
   } 
   else 
   { 
      onebranch(branch2,root + *l1 + 2,l2,grph); 
      onebranch(branch3,root + *l1 + *l2 + 2,l3,grph); 
   } 
} 

static void  dogen(adiagram grph,permlist* gen,word* dim)
{char     nn; 
 byte     i, ngen; 
 adiagram branch1, branch2, branch3; 
 byte     nbeg1, nbeg2, nbeg3, l1, l2, l3, nl1, nl2; 

   /* Nested function: eq */ 

   /* Nested function: addgen */ 

   /* Nested function: dobranches */ 

   nn = 1 - nin; 
   *gen = NULL; 
   *dim = 1; 
   for (i = 1; i <= 2 * (nin + nout) - 5; i++) 
   { 
      if (grph[i-1] > 0) nn++; 
      if (grph[i-1] < 0) 
      { 
         dobranches(i,grph,branch1,branch2,branch3,&l1,&l2,&l3); 
         nbeg1 = nn + 1; 
         nl1 = 1 + (l1 - 1) / 2; 
         nbeg2 = nbeg1 + nl1; 
         nl2 = 1 + (l2 - 1) / 2; 
         nbeg3 = nbeg2 + nl2; 
         ngen = 0; 
         if (eq(branch2,branch3,l2,l3)) 
         {  addgen(nbeg2,nbeg3,nl2,gen); 
            ngen++; 
         } 
         if (nn >= 0) 
         { 
            if (eq(branch1,branch2,l1,l2)) 
            {  addgen(nbeg1,nbeg2,nl1,gen); 
               ngen++; 
            } 
            if (eq(branch1,branch3,l1,l3) && ngen < 2) 
            {  addgen(nbeg1,nbeg3,nl1,gen); 
               ngen++; 
            } 
         } 
         if (ngen == 1) *dim *= 2; 
         if (ngen == 2) *dim *= 6; 
      } 
   } 
} 


static boolean  eqprm(permut perm1,permut perm2)
{ byte  i; 

   for (i = 1; i <= nout; i++) 
   if (perm1[i-1] != perm2[i-1]) return FALSE; 
   return TRUE; 
} 

static void  doleftident(void)
{grooplist   sim, sim1; 
 permlist    lgen; 
 permut      res; 
 word        c, c1, c2, n; 

   sim = groop; 
   n = 1; 
   while (sim != NULL) 
   {  sim->left = n++; 
      sim = sim->next; 
   }  
   lgen = amp1->gen; 
   while (lgen != NULL) 
   { 
      sim = groop; 
      while (sim != NULL) 
      { 
         multperm(lgen->perm,sim->perm,res); 
         sim1 = sim; 
         do 
         { 
            if (eqprm(res,sim1->perm)) 
            { 
               c1 = sim1->left; 
               c = sim->left; 
               if (c1 != c) 
               { 
                  if (c1 > c) 
                     c2 = c; 
                  else 
                  {  c2 = c1; 
                     c1 = c; 
                  } 
                  sim1 = groop; 
                  while(sim1 != NULL)
                  { 
                     if (sim1->left == c1) sim1->left = c2; 
                     sim1 = sim1->next; 
                  }
               } 
               goto label_1;
            } 
            sim1 = sim1->next; 
         }  while (sim1 != NULL); 
label_1: sim = sim->next; 
      } 
      lgen = lgen->next; 
   } 
} 

static void  doresident(void)
{grooplist   sim, sim1; 
 permlist    rgen; 
 permut      res; 
 word        c,c1,n; 

   sim = groop; 
   while (sim != NULL) 
   { 
      sim->res = sim->left; 
      sim = sim->next; 
   } 
   rgen = amp2->gen; 
   while (rgen != NULL) 
   { 
      n = 1; 
      sim = groop; 
      while (sim != NULL) 
      {  if (sim->left == n) 
         { 
            multperm(sim->perm,rgen->perm,res); 
            sim1 = sim; 
            do 
            {  if (eqprm(res,sim1->perm)) 
               { 
                  c1 = sim1->res; c = sim->res; 
                  if (c1 != c) 
                  { 
                     sim1 = sim; 
                     while (sim1 != NULL) 
                     { 
                        if (sim1->res == c1) sim1->res = c; 
                        sim1 = sim1->next; 
                     } 
                  } 
                  goto label_1;
               } 
               sim1 = sim1->next; 
            }  while (sim1 != NULL);
         } 
label_1: sim = sim->next; 
         n++; 
      } 
      rgen = rgen->next; 
   } 
} 


static void  clearsquaring(void)

{grooplist    g; 
 ampllist     a; 
 permlist     p; 

   if (testsim) 
   { 
      while (groop != NULL) 
      { 
         g = groop; 
         groop = groop->next; 
         free(g); 
      } 
   } 
   while (amplitudes != NULL) 
   { 
      a = amplitudes; 
      if (testsim) 
         while (a->gen != NULL) 
         { 
            p = a->gen; 
            a->gen = a->gen->next; 
            free(p); 
         } 
      amplitudes = amplitudes->next; 
      free(a); 
   } 
}

boolean  squaring(void)
{byte         i, j, k;
 word         n, m;
 shortstr     namesubproc;

   if ((totdiag_f-deldiag_f) >=1000) 
   {
      messanykey(5,17,"Too many diagrams for squaring!$");
      return FALSE;
  }

   menup=fopen(MENUP_NAME,"rb"); 
   menuq=fopen(MENUQ_NAME,"wb"); 
   
   diagrp=fopen(DIAGRP_NAME,"rb");
   diagrq=fopen(DIAGRQ_NAME,"wb"); 

   maxdiagr = 1000;
   constrdiagr = 0;
   f_write("\060\067",2,1,menuq);    
   nsuba = 1;
   nsubcs = 1;
   scrcolor(White,Blue);
   chepbox(10,18,63,22);
   scrcolor(White,Black);
   goto_xy(13,19); print("Subprocess: ");
   goto_xy(13,21); print("0      diagrams are constructed ");
   for (nsuba=1;nsuba<=subproc_f;nsuba++)
   {
      rd_menu(1,nsuba,namesubproc,&ndel,&ncalc,&nrest,&nstart);
      ntot = ndel + ncalc + nrest;
      if (ntot != ndel)
      {
         goto_xy(26,19);
         print("%s",copy(scat("%s              ",namesubproc),1,26));
         nsdiagram = 0;
         firstpos = ftell(diagrq);
         amplitudes = (ampllist)getmem(sizeof(struct frgmampl));
         amp1 = amplitudes;
         fseek(diagrp,nstart*sizeof(adiagram),SEEK_SET);
         for (i = 1; i <= ntot; i++)
         {
            FREAD1(amp1->dgrm,diagrp);
            if (amp1->dgrm[0] < 0)
            {
               amp2 = amp1;
               amp1 = (ampllist)getmem(sizeof(struct frgmampl));
               amp2->next = amp1;
            }
         }
         free(amp1);
         amp2->next = NULL;

         amp1 = amplitudes;
         while (amp1 != NULL)
         {
            k = 2;
            if (nin == 2)
               while (amp1->dgrm[k - 1-1] <= 0) k++;
            j = 1;
            for (i = k; i <= 2 * (nin + nout) - 3; i++)
               if (amp1->dgrm[i-1] > 0)
               {
                  amp1->perm[j-1] = amp1->dgrm[i-1];
                  j++;
               }
            amp1 = amp1->next;
         }

         for (i = 1; i <= nout - 1; i++)
            for (j = i + 1; j <= nout; j++)
               if (amplitudes->perm[i-1] == amplitudes->perm[j-1])
               {  testsim = TRUE;
                  goto label_1;
               }
         testsim = FALSE;

label_1: if (testsim)
         {
            lvcpy(cononout,amplitudes->perm);
            allperm(cononout,&groop);
            amp1 = amplitudes;
            while (amp1 != NULL)
            {
                smpl_linker(amp1->perm,cononout,amp1->perm);
                revers1(amp1->perm,rvrs);
                dogen(amp1->dgrm,&amp1->gen,&amp1->dim);
                cgen = amp1->gen;
                while (cgen != NULL)
                {
                   multperm(rvrs,cgen->perm,cgen->perm);
                   multperm(cgen->perm,amp1->perm,cgen->perm);
                   cgen = cgen->next;
                }
                amp1 = amp1->next;
            }
         }

         amp1 = amplitudes;
         while (amp1 != NULL)
         {
            if (testsim) doleftident();
            amp2 = amp1;
            while (amp2 != NULL)
            {
               lvcpy(sqres.dgrm1,amp1->dgrm);
               lvcpy(sqres.dgrm2,amp2->dgrm);
               if (amp1 == amp2)
                  sqres.mult = 1;
               else
                  sqres.mult = 2;

               if (!testsim)
               {
                  smpl_linker(amp1->perm,amp2->perm,sqres.lnk);
                  sqres.del = 1;
                  sqres.status = 0;
                  FWRITE1(sqres,diagrq);
                  nsdiagram++;
               }
               else
               {
                  doresident();
                  sim = groop;
                  n = 1;
                  while (sim != NULL)
                  {
                     if (sim->res == n)
                     {
                        if (amp1 == amp2)
                        {
                           sqres.mult = 1;
                           revers1(sim->perm,rvrs);
                           sim1 = sim;
                           while (!eqprm(sim1->perm,rvrs))
                           {
                              sim1 = sim1->next;
                              if (sim1 == NULL) goto label_2;
                           }
                           if (sim1->res == n)
                              sqres.mult = 1;
                           else
                              sqres.mult = 2;
                        }
                        m = 1;
                        sim1 = sim->next;
                        while (sim1 != NULL)
                        {
                           if (sim1->res == n) m++;
                           sim1 = sim1->next;
                        }
                        sqres.del = (amp1->dim * amp2->dim) / m;
                        revers1(amp2->perm,rvrs);
                        multperm(sim->perm,rvrs,rvrs);
                        multperm(amp1->perm,rvrs,sqres.lnk);
                        sqres.status = 0;
                        /*  not deleted, not calculated  */
                        FWRITE1(sqres,diagrq);
                        nsdiagram++;
                     }
label_2:             n++;
                     sim = sim->next;
                  }
               }
               amp2 = amp2->next;
            }
            amp1 = amp1->next;
         }
	 wrt_menu(2,nsubcs,namesubproc,0,0,nsdiagram,firstpos/sizeof(csdiagram));
         nsubcs++; 
         clearsquaring(); 
         constrdiagr += nsdiagram; 
         goto_xy(13,21); 
         print("%5d",constrdiagr); 
         if ((constrdiagr) > maxdiagr && (nsuba < subproc_f ))
            if(mess_y_n(3,17," Continue ? $"))
               maxdiagr += 1000; 
            else 
               goto label_3;
      }  
   } 
label_3: 
   subproc_sq = (word)filesize(menuq) / (53+2);
   fclose(menup);
   fclose(menuq);
   fclose(diagrp);
   fclose(diagrq);
	for (i = 17; i <= 24; i++) { goto_xy(1,i); clr_eol(); }
   if (constrdiagr == 0)
   { 
      messanykey(5,17," All process are marked as deleted $"); 
      return FALSE; 
   } 
   else 
   { 
      totdiag_sq = constrdiagr; 
      deldiag_sq = 0; 
      calcdiag_sq = 0; 
      be_be(); 
      return TRUE; 
   } 
} 
