#include <math.h>
#include "tptcmac.h"
#include "crt.h"
#include "syst2.h"
#include "files.h"
#include "physics.h"
#include "parser.h"
#include "optimise.h"
#include "procvar_.h"
#include "n_compil.h"

#include "num_tool.h"

 integer err_code;   /*  0: OK,  1: Division by zero, 2:Stack Check,
                              3: Kinematic Check; 4: UserHalt   */
 word  numberfuncalls;


double  lambda(double  s, double  m1, double  m2)
{ 
   return (s - sqr(m1 + m2)) * (s - sqr(m1 - m2)); 
} 


void  integral_(double  xmin, double  xmax, double  eps,
                     r_r_fun  f, double*  intvalue, realseq*  p_beg)
{/* ,P_beg */ 
 realseq     p1, p2, p3, p4, p_end; 
 double        i0, eps2; 
 boolean     del; 

  /* Nested function: int3_5 */ 

  *intvalue = 0.0; 
  p1 =(realseq) getmem(sizeof(realseqrec));
  p2 =(realseq) getmem(sizeof(realseqrec));
  p3 =(realseq) getmem(sizeof(realseqrec));
  p1->next = p2; 
  p2->next = p3; 
  p3->next = NULL; 
  p_end = p3; 
  *p_beg = p1; 

  p1->x = xmin;
  p2->x = 0.5 * (xmin + xmax);
  p3->x = xmax; 
  p1->y = f(p1->x);
  if (err_code != 0)
  {  disposerealseq(p_beg);
     return;
  } 
  p2->y = f(p2->x);
  if (err_code != 0)
  {  disposerealseq(p_beg);
     return;
  } 
  p3->y = f(p3->x);
  if (err_code != 0)
  {  disposerealseq(p_beg);
     return;
  } 

  numberfuncalls = 3; 

  eps2 = 0.01; 
  i0 = fabs(0.01 * eps2 * (xmax - xmin) * (p1->y + 4 * p2->y + p3->y) / 6.0); 

  del = FALSE; 
  
/*  int3_5(*p_beg); */
/*static void  int3_5(realseq  p)*/
   {realseq     p_, p; 
    double        x1, x2, x3, x4, x5, y1, y2, y3, y4, y5,
                /* dy_anti,*/ i2, di2; 
    boolean     okk; 

      p = *p_beg;
      while (p != p_end) 
      { 
         x1 = p->x; 
         y1 = p->y; 
         p_ = p->next; 
         x3 = p_->x; 
         y3 = p_->y; 
         p_ = p_->next; 
         x5 = p_->x; 
         y5 = p_->y; 
         x2 = 0.5 * (x1 + x3); 
         y2 = f(x2); 
         ++(numberfuncalls); 
/*       if (maxavail() < 100) err_code = 2; */
         if (err_code != 0) 
         { 
            if (del)
               disposerealseq(&p); 
            else
               disposerealseq(p_beg); 
            goto rtn;
         } 
         x4 = 0.5 * (x5 + x3); 
         y4 = f(x4);
         ++(numberfuncalls); 
         if (err_code != 0) 
         { 
            if (del)
               disposerealseq(&p); 
            else
               disposerealseq(p_beg); 
            goto rtn;
         } 
         i2 = (7 * (y1 + y5) + 32 * (y2 + y4) + 12 * y3) / 90; 

         di2 = fabs(y1 - 4 * (y2 + y4) + 6 * y3 + y5) / 180; 

         if (di2 <= eps2 * fabs(i2))
            okk = TRUE; 
         else 
            if (di2 <= 0.05 * fabs(i2) && di2 * fabs(x5 - x1) < i0 / 3) 
            {  okk = TRUE;
               i0 -= di2 * fabs(x5 - x1); 
            } 
            else okk = FALSE; 

         if (okk) 
         { 
            *intvalue += i2 * (x5 - x1); 
            p_ = p; 
            p = p->next; 
            if (del)
               free(p_); 
            else 
            { 
               p2 = (realseq )getmem(sizeof(realseqrec));
               p2->next = p_->next; 
               p_->next = p2; 
               p2->x = x2; 
               p2->y = y2; 
            } 
            p_ = p; 
            p = p->next; 
            if (del)
               free(p_); 
            else 
            { 
               p2 =(realseq) getmem(sizeof(realseqrec));
               p2->next = p_->next; 
               p_->next = p2; 
               p2->x = x4; 
               p2->y = y4; 
            } 
         } 
         else 
         { 
            p2 =(realseq) getmem(sizeof(realseqrec));
            p2->next = p->next; 
            p->next = p2; 
            p2->x = x2; 
            p2->y = y2; 
            p2 = p2->next; 
            p4 = (realseq)getmem(sizeof(realseqrec));
            p4->next = p2->next; 
            p2->next = p4; 
            p4->x = x4; 
            p4->y = y4; 
         } 
      } 
rtn:;
   } 
   if (err_code != 0) return;
   eps2 = eps; 
   i0 = fabs(0.01 * eps * (*intvalue)); 
   *intvalue = 0; 

   {realseq     p_, p; 
    double        x1, x2, x3, x4, x5, y1, y2, y3, y4, y5,
                /* dy_anti,*/ i2, di2; 
    boolean     okk; 

      p = *p_beg;
      while (p != p_end) 
      { 
         x1 = p->x; 
         y1 = p->y; 
         p_ = p->next; 
         x3 = p_->x; 
         y3 = p_->y; 
         p_ = p_->next; 
         x5 = p_->x; 
         y5 = p_->y; 
         x2 = 0.5 * (x1 + x3); 
         y2 = f(x2); 
         ++(numberfuncalls); 
/*       if (maxavail() < 100) err_code = 2; */
         if (err_code != 0) 
         { 
            if (del)
               disposerealseq(&p); 
            else
               disposerealseq(p_beg); 
            goto rtn1;
         } 
         x4 = 0.5 * (x5 + x3); 
         y4 = f(x4);
         ++(numberfuncalls); 
         if (err_code != 0) 
         { 
            if (del)
               disposerealseq(&p); 
            else
               disposerealseq(p_beg); 
            goto rtn1;
         } 
         i2 = (7 * (y1 + y5) + 32 * (y2 + y4) + 12 * y3) / 90; 
         di2 = fabs(y1 - 4 * (y2 + y4) + 6 * y3 + y5) / 180; 

         if (di2 <= eps2 * fabs(i2))
            okk = TRUE; 
         else 
            if (di2 <= 0.05 * fabs(i2) && di2 * fabs(x5 - x1) < i0 / 3) 
            {  okk = TRUE;
               i0 -= di2 * fabs(x5 - x1); 
            } 
            else okk = FALSE; 

         if (okk) 
         { 
            *intvalue += i2 * (x5 - x1); 
            p_ = p; 
            p = p->next; 
            if (del)
               free(p_); 
            else 
            { 
               p2 =(realseq) getmem(sizeof(realseqrec));
               p2->next = p_->next; 
               p_->next = p2; 
               p2->x = x2; 
               p2->y = y2; 
            } 
            p_ = p; 
            p = p->next; 
            if (del)
               free(p_); 
            else 
            { 
               p2 =(realseq) getmem(sizeof(realseqrec));
               p2->next = p_->next; 
               p_->next = p2; 
               p2->x = x4; 
               p2->y = y4; 
            } 
         } 
         else 
         { 
            p2 =(realseq) getmem(sizeof(realseqrec));
            p2->next = p->next; 
            p->next = p2; 
            p2->x = x2; 
            p2->y = y2; 
            p2 = p2->next; 
            p4 =(realseq) getmem(sizeof(realseqrec));
            p4->next = p2->next; 
            p2->next = p4; 
            p4->x = x4; 
            p4->y = y4; 
         } 
      } 
rtn1:;
   } 
   /*   If Err_Code=0 Then   Dispose(P_end);  */ 
} 


void  integral(double  xmin, double  xmax, double  eps,
                    r_r_fun  f, double*  intvalue)
{realseq  p_beg; 

   integral_(xmin,xmax,eps,f,intvalue,&p_beg); 
   disposerealseq(&p_beg); 
} 


void  disposerealseq(realseq*  r)
{pointer  p; 

   while (*r != NULL) 
   { 
      p = (pointer)(*r)->next; 
      free(*r); 
      *r = (realseq)p; 
   }
}


static double  calcpolynom(varptr  q)
{double        s, m;
 byte        i;

   if (q == NULL) return 0.0;
   s = 0.0;
   do
   {
/* Problems with definition of infoptr in optimise!!! */
      m = q->coef->rval;
      for (i = 0; q->vars[i] != '\0' ; i++)
         m *= (*vararr)[q->vars[i]-1].tmpvalue;
      if (q->sgn == '+')
         s += m;
      else
         s -= m;
      q = q->next;
   }  while (q != NULL);
   return s;
}


void  calcconstants(void)
{infoptr  i_ptr;

   i_ptr = info;
   while (i_ptr != NULL)
   {
/* Problems with definition of infoptr in optimise!!! */
      if (i_ptr->consttype == expr)
			i_ptr->rval = calcpolynom(i_ptr->const_);
		else
			i_ptr->rval = i_ptr->ival;
      i_ptr = i_ptr->next;
   }
}


static double  calcdenpolynom(varptr  q)
{double        s, m, mm;
 byte        i;

   if (q == NULL) return 0.0;
   s = 0.0;
   mm = 0.0;
   do
   {
 /* Problems with definition of infoptr in optimise!!! */
      m = q->coef->rval;
      for (i = 0; q->vars[i] != '\0'; i++)
          m *= (*vararr)[q->vars[i]-1].tmpvalue;
      if (mm < fabs(m)) mm = fabs(m);
      if (q->sgn == '+')
         s += m;
      else
         s -= m;
      q = q->next;
   }  while (q != NULL);
   return fabs(s) < mm * 1.E-10 ? 0.0 : s;
}


static double  calconediagr(prgcodeptr  prg_)
{byte        i;
 double        r, rr, irr;

	r = calcpolynom(prg_->totn);
   rr = calcpolynom(prg_->totd);
   if (rr == 0)
   {
      err_code = 1;   /* Division by zero */
      return 0;
   }
   r /= rr;
   rr = calcpolynom(prg_->rnum);
   r *= rr;
   for (i = 0; i < prg_->denorno; i++)
   {
      rr = calcdenpolynom(prg_->den[i]);
      irr = calcpolynom(prg_->mass[i]);
      irr=irr*calcpolynom(prg_->width[i]);
      if (fabs(rr) + fabs(irr) == 0)
      {
         err_code = 1;   /* Division by zero */
         return 0;
      }
      if (prg_->width[i] != NULL && prg_->deg[i] == 1)
         r *= rr;
      if (prg_->width[i] != NULL)
         r /= rr * rr + irr * irr;
      else
      {
         if (prg_->deg[i] == 2) rr = rr * rr;
         r /= rr;
      }
   }
   return r;
}


double  matrixelement(prgcodeptr  prg)
{double        sum; 
 prgcodeptr  prg_; 

   sum = 0.0; 
   prg_ = prg; 
   while (prg_ != NULL) 
   { 
     sum += calconediagr(prg_); 
     if (err_code != 0) return 0.0;
     prg_ = prg_->next; 
   } 
   if (escpressed())
      err_code = 4; 
   return sum; 
} 

int sp_pos(int i,int j)
{
  int tmp;
  if(i > j) {tmp=i;i=j;j=tmp;}
   return maxsp  - (i + ((j - 1) * (j - 2)) / 2);
}
