#include "tptcmac.h"
#include "syst2.h"
#include "physics.h"
#include "global.h"
#include "procvar.h"
#include "syst2.h"
#include "optimise.h"
#include "files.h"

#include "l_string.h"

FILE * fortFile;

#define degreeptr struct degreerec *
typedef struct degreerec
   {
      degreeptr    next;
      char         name[10];
      byte         deg;
   }  degreerec;
#undef degreeptr
typedef struct degreerec *degreeptr;

typedef degreeptr degname[nvarhighlimit];

static degname   *degnamesptr;
int    constcount;
int    degnamecount;
int    tmpNameNum;
int    tmpNameMax=0;

static char buff[2000];  /* used in writelongstring
	declarad as static ie escape
	stack overflow on PC
*/


void  initdegnames(void)
{integer   k;

	degnamesptr=(degname *)getmem((word)sizeof(degreeptr) * (nmodelvar + maxsp+1));
	for (k = 0; k < nmodelvar + maxsp+1; k++) (*degnamesptr)[k] = NULL;
   degnamecount=0;

}


void  cleardegnames(void)
{integer  k;
 degreeptr    p, q;

  for (k = 0; k < nmodelvar + maxsp+1; k++)
  {
     q = (*degnamesptr)[k];
     while (q != NULL)
     {
        p = q;
        q = p->next;
        free(p);
     }
  }
  free(degnamesptr);
}


void  writelongstr(char* name,longstrptr longs)
{
 integer  i, j, rest;
 integer  l, dl,p;

	f_printf(fortFile,"%s%s%c",six,name,eq);
   if (longs == NULL || longs->len == 0)
   {
      f_printf(fortFile,"0\n");
      return;
	}
	p=0;
   dl = 1 + (integer)strlen(name);
   rest = longs->len;
   i = 0;
   while (rest != 0)
	{
      l = MIN(rest,65 - dl);
		if (dl == 0)  
		{  for (j=0;j<5;j++)  buff[p++]=' ';
			buff[p++]='.';
		}
		for(j = 0; j < l; j++) buff[p++] =longs->txt[i + j];
		buff[p++]='\n';
      i += l;
      rest -= l;
      dl = 0;
	}
	buff[p++]  =0;
	f_puts(buff,fortFile);
}


static void  newconstantname(char* s)
{
   sbld(s,"C(%d)",++constcount);
}


void   addstring(longstrptr longs,char* s)

{char  name[STRSIZ];
 integer   i, l;
 word ll;

   ll = longs->len;
   l  = (integer)strlen(s);
   if (ll + l > buffsize)
   {
      sprintf(name,"tmp(%d)",++tmpNameNum);
      writelongstr(name,longs);
      longs->len = 0;
      addstring(longs,name);
      ll = longs->len;
   }
   for (i = 0; i < l; i++) longs->txt[ll + i] = s[i];
   longs->len += l;
}


static pointer  gorner(char* s,longstrptr pmult,longstrptr psum)
{char         name[STRSIZ], name2[STRSIZ];
 longstrptr   ans;
 pointer      pchange;

   if (pmult == NULL) return (pointer)psum;
   ans = (longstrptr)getmem(sizeof(longstr));
   ans->len = 0;
   addstring(ans,s);

   if (3 + ans->len + pmult->len > buffsize)
   {
      sprintf(name,"tmp(%d)",++tmpNameNum);
      writelongstr(name,pmult);
      addstring(ans,scat("*%s",name));
   }
   else
   {
      if (pmult->txt[0] == '+')
      {
         pmult->txt[0] = '(';
         addstring(ans,"*");
      }
      else
         addstring(ans,"*(");
      memcpy(&(ans->txt[ans->len]),pmult->txt,pmult->len);
      ans->len += pmult->len;
      addstring(ans,")");
   }
   free(pmult);

   if (psum == NULL) return (pointer)ans;
   if (ans->len + psum->len > buffsize)
   {
      sprintf(name,"tmp(%d)",++tmpNameNum); 
      if (ans->len > psum->len)
      {
         pchange = (pointer)ans;
         ans = psum;
         psum = (longstrptr)pchange;
      }
      writelongstr(name,psum);
      if (ans->len + strlen(name) >= buffsize)
      {
         sprintf(name2,"tmp(%d)",++tmpNameNum);
         writelongstr(name2,ans);
         ans->len = 0;
         addstring(ans,scat("+%s+%s",name,name2));
      }
      else addstring(ans,scat("+%s",name));
   }
   else
   {
      memcpy(&(ans->txt[ans->len]),psum->txt,psum->len);
      ans->len += psum->len;
   }
   free(psum);
   return (pointer)ans;
}


void  putnames(void)
{infoptr     i;
   i = info;
   while (i != NULL)
   {
      if (i->consttype == numb)    sbld(i->name,"%ld",i->ival);
      else                         newconstantname(i->name);
      i = i->next;
   }
}   /* PutNames */


static char *  writevardeg(byte nv,byte deg)
{
 degreeptr     p;
 static char namest[21];
   if (deg == 1) return (*varnamesptr)[nv-1];
   p = (*degnamesptr)[nv-1];
   while (p != NULL)
      if (p->deg == deg)
         return p->name;
      else
         p = p->next;
   p = (degreeptr)getmem(sizeof(degreerec));
   p->deg = deg;
   sbld(namest,"S(%d)",++degnamecount);

/*     newconstantname(namest);*/
   strcpy(p->name,namest);
   p->next = (*degnamesptr)[nv-1];
   (*degnamesptr)[nv-1] = p;
	f_printf(fortFile,"%s%s%c%s**%u\n",
		six,namest,eq,(*varnamesptr)[nv-1],(unsigned int) deg);
   return namest;
}


static pointer  smpl_emit(varptr ex)
{longstrptr   ans;
 char         s[STRSIZ];
 integer      k;
 byte         bt, nv, deg;
 boolean      star;
 varptr       ex_, exbeg;

   if (ex == NULL) return NULL;
/*  */
   if (ex->sgn == '-')
   {
      ex_ = ex;

      while (ex_->next != NULL && ex_->next->sgn == '-')
         ex_ = ex_->next;
      if (ex_->next != NULL)
      {
         exbeg = ex_->next;
         ex_->next = exbeg->next;
         exbeg->next = ex;
         ex = exbeg;
      }
   }

 
   ans = (longstrptr)getmem(sizeof(longstr));
   ans->len = 0; 
   while (ex != NULL) 
   { 
      sbld(s,"%c",ex->sgn); 
      star = (strcmp((ex->coef)->name,"1") != 0); 
      if (star || strlen(ex->vars) == 0)
         sbld(s,"%s%s",s,(ex->coef)->name); 
      if (strlen(ex->vars) != 0) 
      { 
         bt = (byte)(ex->vars[0]); 
         deg = 1; 
         for (k = 2; k <= (integer)strlen(ex->vars); k++) 
         { 
            nv = (byte)(ex->vars[k-1]); 
            if (bt != nv) 
            {
               if (star) 
                  sbld(s,"%s*",s); 
               else 
                  star = TRUE; 
               sbld(s,"%s%s",s,writevardeg(bt,deg)); 
               deg = 1; 
               bt = nv; 
            } 
            else ++(deg); 
         } 
         if (star)
            sbld(s,"%s*",s); 
         else 
            star = TRUE; 
         sbld(s,"%s%s",s,writevardeg(bt,deg)); 
      } 
      addstring(ans,s); 
      ex = ex->next; 
   } 
   return (pointer) ans; 
}

static pointer  v_gorner(char ch,byte deg,pointer pmult,pointer psum)
{char b[STRSIZ];
   sbld(b,"+%s",writevardeg((byte)ch,deg));
   return gorner(b,pmult,psum);
}

static pointer  c_gorner(infoptr i,pointer pmult,pointer psum)
{char b[STRSIZ];
   sbld(b,"+%s",i->name);
   return gorner(b,pmult,psum);
}


void  fortwriter(char* name,varptr fortformula)
{longstrptr   tmp;

   tmpNameNum=0;
   tmp = (longstrptr)emitexpr(fortformula,smpl_emit,v_gorner,c_gorner);
   writelongstr(name,tmp);
   if (tmp != NULL) free(tmp);
   if (tmpNameMax<tmpNameNum) tmpNameMax=tmpNameNum;
}

void  write_const(void)
{
 infoptr      i;
 byte         cutleveltmp;

   cutleveltmp = cutlevel;
	cutlevel    = nmodelvar+maxsp+1;

   i = info;
   while (i != NULL)
   {
      if (i->consttype == expr)   fortwriter(i->name,(pointer) i->const_);
      i = i->next;
   }
   cutlevel = cutleveltmp;
}   /*  WriteConst  */


void  initConsts(void)
{
   constcount = 0;
   initinfo();
}
