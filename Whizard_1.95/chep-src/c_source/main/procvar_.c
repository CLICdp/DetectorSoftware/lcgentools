#include <math.h>
#include "tptcmac.h"
#include "os.h"
#include "syst2.h"
#include "global.h"
#include "physics.h"
#include "files.h"
#include "parser.h"

#include "procvar_.h"

varsdescription *vararr;


typedef struct
   {
      boolean freezen;
      double    v;
   }  freez_val;


static pointer  uact_5(char*  ch, pointer  mm)
{freez_val  *p; 

   p = (freez_val*) mm; 
   if (strcmp(ch,"-") == 0)
   {  p->v = -p->v;
      return mm;
   } 
   /* If ch='Sqrt' */ 
   if (p->v < 0) 
   { 
      rderrcode = negativsqrtarg; 
      /* Err_Code:=5; */ 
      free(p); 
      return NULL; 
   } 
   p->v = sqrt(p->v); 
   return mm;
} 


static pointer  bact_5(char  ch, pointer  mm1, pointer  mm2)
{freez_val     *p1, *p2; 
 double           q; 
 word           i; 
 integer        d; 

   p1 = (freez_val*) mm1; 
   p2 = (freez_val*) mm2; 
   switch (ch)
   {
      case '+':
         p1->v += p2->v; 
      break; 
   
      case '*':
         p1->v *= p2->v; 
      break; 
   
      case '/':
         if (p2->v == 0) 
         { 
            rderrcode = divisionbyzero; 
            /* Err_Code:=1; */ 
            free(mm1);
            free(mm2); 
            return NULL;
         } 
         else
            p1->v /= p2->v; 
      break; 
   
      case '^':
			d = chround(p2->v);
         q = p1->v; 
         p1->v = 1.0; 
         for (i = 1; i <= d; i++) p1->v *= q; 
   }   /* Case */ 
   p1->freezen = p1->freezen && p2->freezen; 
   free(mm2); 
   return mm1; 
} 


static pointer  rd_5(char*  s)
{integer        ecd; 
 word           k, rderrcodetmp, rderrpostmp; 
 varlist        q; 
 static freez_val  *p; 

   if (isdigit(s[0])) 
   { 
      p = (freez_val*)getmem(sizeof(freez_val));
      vald(s,&p->v,&ecd); 
      p->freezen = TRUE; 
      return (pointer)p; 
   } 
   for (k = maxsp + 2; k <= maxsp + 1 + nmodelvar; k++) 
   { 
      q = (*vararr)[k-1].sourse; 
      if (strcmp(q->varname,s) == 0) 
      {
         if (!(q->func == NULL ||
               q->func[0] == '\0' ||
               (*vararr)[k-1].used)) 
         { 
            rderrcodetmp = rderrcode; 
            rderrpostmp = rderrpos; 
/*!!! This recursion is suspicious!!!:*/
            p = (freez_val*)readexprassion(q->func,bact_5,uact_5,rd_5); 
            if (rderrcode != 0) 
               return NULL; 
            (*vararr)[k-1].freezen = p->freezen; 
            (*vararr)[k-1].tmpvalue = p->v; 
            rderrcode = rderrcodetmp; 
            rderrpos = rderrpostmp; 
         } 
         else 
         { 
            p = (freez_val*)getmem(sizeof(freez_val));
            p->v = (*vararr)[k-1].tmpvalue; 
            p->freezen = (*vararr)[k-1].freezen; 
         } 
         (*vararr)[k-1].used = TRUE; 
         return (pointer)p; 
      } 
   }
   return NULL; 
}


static void  getvarset(int  nsub, setofbyte  varset)
{setofbyte   set1; 

   FILE * actvars;
   actvars=fopen(ACTVARS_NAME,"rb"); 
   if (nsub != 0) 
   { 
      fseek(actvars,(nsub-1)*sizeof(setofbyte),SEEK_SET);
      if ( fread(varset,sizeof(setofbyte),1,actvars) != 1) 
                                            setofb_cpy(varset,setofb(_E));
   }    
   else 
   { 
      setofb_cpy(varset,setofb(_E));
      while (FREAD1(set1,actvars)==1 ) 
                setofb_cpy(varset,setofb_uni(varset,set1)); 
   } 
   fclose(actvars); 
} 


void  initvararray(integer  nsub_)
{word         k, nvar; 
 varlist      p; 
 setofbyte    allvars; 

   nvar = nmodelvar + maxsp + 1; 
   vararr = (varsdescription*)getmem_(nvar * sizeof(singlevardescription)); 

   for (k = 0; k <= maxsp; k++) 
   { 
      (*vararr)[k].sourse = NULL; 
      (*vararr)[k].alias[0] = '\0'; 
      (*vararr)[k].tmpvalue = 0.0; 
      (*vararr)[k].used = TRUE; 
      (*vararr)[k].freezen = FALSE; 
   } 
   (*vararr)[maxsp + 1-1].tmpvalue = sqrts; 

   getvarset(nsub_,allvars); 
   p = modelvars; 
   for (k = maxsp + 1; k < nvar; k++) 
   { 
      (*vararr)[k].sourse = p; 
      (*vararr)[k].alias[0]='\0'; 
      (*vararr)[k].tmpvalue = p->varvalue; 
      (*vararr)[k].used = insetb(k+1,allvars); 
      (*vararr)[k].freezen = FALSE; 
      p = p->next; 
   } 
} 


void  calcfunctions(integer*  errcode)
{word         k; 
 char         s[STRSIZ]; 
 freez_val   *pp; 

   *errcode = 0; 
   for (k = maxsp + 1; k <= nmodelvar + maxsp; k++)
      if ((*vararr)[k].used && 
          (*vararr)[k].sourse->func != NULL &&
          (*vararr)[k].sourse->func[0] != '\0') 
      {  
         strcpy(s,(*vararr)[k].sourse->func); 
         (*vararr)[k].freezen = FALSE; 
         pp = (freez_val*)readexprassion(s,bact_5,uact_5,rd_5); 
         if (rderrcode != 0) 
         { 
            *errcode = rderrcode; 
            return;
         } 
         (*vararr)[k].tmpvalue = pp->v; 
         (*vararr)[k].freezen = pp->freezen; 
         free(pp); 
      } 
} 


void  makevarfile(void)
{ 
 byte k;
 varlist   p;  
 char      buff[STRSIZ]; 
 FILE *   varfile; 
  
   varfile=fopen(VARFILE_NAME,"wb"); 
   f_write("\026\027",2,1,varfile); 
   if (nin == 2) 
   {  sprintf(buff," Energy=%14lf%c",sqrts,(char)(maxsp+1)); 
      f_write(buff,23,1,varfile); 
   } 

   k = maxsp + 1;
   
   while (k <= nmodelvar + maxsp) 
   {  p = (*vararr)[k].sourse;
      if ((*vararr)[k].used && (p->func == NULL || p->func[0] == '\0')) 
      {  
         sprintf(buff," %-6s=%14lf%c",p->varname,p->varvalue,k+1);
         f_write(buff,23,1,varfile); 
      }
      k++; 
   } 
   fclose(varfile);
} 
