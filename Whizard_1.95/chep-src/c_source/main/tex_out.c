#include "tptcmac.h"
#include "syst2.h"
#include "files.h"
#include "os.h"
#include "crt.h"

#include "tex_out.h"

#define iabs(x)   ((x) < 0 ? -(x) : (x)) 

static char f_name[STRSIZ];

void plottexstart(char* upstr)
{int VsizePt, HsizePt, xwidth, tabnum, doserror;
 searchrec  s;

   VsizePt = 250/* *28.35*/;
   HsizePt = 345/* *28.35*/;
   texymax1    = tg_getmaxy()/* - tg_textheight("A") - 5*/;
   xwidth  = tg_getmaxx();
   texxshift  = 0;
   
   texxscale=(double)HsizePt/xwidth;
   texyscale=(double)VsizePt/texymax1;

   texflag=TRUE;
    
   tabnum=0; 
   do
   {  tabnum++;
      sbld(f_name, "%s%cplot_%d.tex",pathtoresults,f_slash,tabnum);
      doserror = find_first(f_name,&s,archive);
   }  while (doserror == 0);

   out_tex=fopen(f_name,"w");
  
   f_printf(out_tex,"%% %s\n",version);
   f_printf(out_tex,"%%Plot for %s\n",upstr);      
   f_printf(out_tex,"%%\\documentstyle[axodraw]{article}\n");

   f_printf(out_tex,"%%\\begin{document}\n");
   f_printf(out_tex,"\\linethickness{0.5pt}\n");
   f_printf(out_tex,"{\\tiny              %% letter  size control\n");
   f_printf(out_tex,"\\def\\chepscale{1.0} %% picture size control\n");
   f_printf(out_tex,"\\unitlength=\\chepscale pt\n");
   f_printf(out_tex,"\\SetWidth{0.7}      %% line    size control\n");
   f_printf(out_tex,"\\SetScale{\\chepscale}\n");
   f_printf(out_tex,"\\begin{center}\n");
   f_printf(out_tex,"\\begin{picture}(%d,%d)(0,0)\n",HsizePt,VsizePt);
}


void plottexfinish(char* buff)
{
   f_printf(out_tex,"\\end{picture}\n");
   f_printf(out_tex,"\\end{center}\n");
   f_printf(out_tex,"%%\\end{document}\n");      
   fclose(out_tex);
   texflag=FALSE; 
   sbld(buff,"TeX output is in %s file, type ENTER:",f_name);
   buff[77] = '\0';
}

