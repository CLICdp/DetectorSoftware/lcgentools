/********************************************************************
 * CompHEP Fortran90 output
 *
 * [adapted from the F77 version by Wolfgang Kilian Mar 19 1999]
 ******************************************************************** 
 * Write modules according to the WHIZARD interface specification
 *
 * Restrictions:
 *   Subprocesses are not supported within the CompHEP module
 *   (we assume that there is only one subprocess)
 *   Quadruple precision is addressed by changing the meaning of 'default'
 *   in the kinds module
 */

/*************************************************
 *     CompHEP (R) FORTRAN code production       *
 *          (C) S.Shichanin Febr 08 1991         *
 *                          June 17 1991         *
 *************************************************/

#include "tptcmac.h"
#include "syst2.h"
#include "crt.h"
#include "crt_util.h"
#include "global.h"
#include "physics.h"
#include "files.h"
#include "syst2.h"
#include "procvar.h"
#include "pvars.h"
#include "diaprins.h"
#include "optimise.h"
#include "f90_lstr.h"
#include "parser.h"
#include "reader5.h"
#include "transfer.h"
#include "os.h"

#include "f90_out.h"

/************************************************************************/
/* Type definitions */

typedef char s6[7];

/* ==================================================================== */
#define procinfptr struct procinfrec *
typedef struct procinfrec {
  procinfptr     next;
  word           tot;
  word           firstdiagpos;
  prtclsarray    p_name;
  int   	 p_masspos[MAXINOUT];
}  procinfrec;
#undef procinfptr
typedef struct procinfrec *procinfptr;

/* ==================================================================== */
#define denlist struct denlistrec *
typedef struct denlistrec {
  denlist      next;
  varptr       den,mass,width;
}  denlistrec;
#undef denlist
typedef struct denlistrec *denlist;

/* ==================================================================== */
typedef struct deninforec {
  word         cr_pos;
  byte         tot_den;
  struct denarr {
    boolean      zerowidth;
    byte         deg;
    word         dennum;
  }  denarr[2*(MAXINOUT-3)];
}  deninforec;

/************************************************************************/
/* Global variables */

static boolean      precision, include_pictures;
static char         globalname[STRSIZ];

static procinfptr   inf, inftmp;  /* information about subProcess  */

static varlist      modl;         /* tempory pointer to variables  */

static word         ndiagrtot, diagrcount;
static integer      nvars;

static marktp       heapbeg;      /* for RELEASE  */

static integer      nden_w, nden_0, nsub1; /* from write_subprocess */
static integer      nsub_tot;

#define DIAGMAX 10000
static int         const1[DIAGMAX], const2[DIAGMAX];



/************************************************************************/
/* Auxiliary routines */

/* If memory allocation [syst2.c] failed: error message and exit */
static void zeroHeap (void) { 
  messanykey(1,1, "Heap is empty!!!");  exit(0);
}

/* ==================================================================== */
/* Read process information */
/* [No change wrt original] */

/* static void  calccutlevel()*/
static void calccutlevel(void) {
  if (nin == 1)
    cutlevel = maxsp;
  else
    if (!inset(structfun,options))
      cutlevel = maxsp-1; /* 14 */
    else
      if (gg_exist && insetb(maxsp+2/*17*/,varsofprocess))
	cutlevel = maxsp+2; /*17*/
      else
	cutlevel = maxsp;/*15*/
}


static void prepare_process_information (void) {
  word        ndel, ncalc, nrest, recpos;
  char        txt[STRSIZ];
  setofbyte   allvars;
  integer     i, k, npos;
  csdiagram   csd;
  s6          mass;
  byte        nn;
  int         nsubs;

  inf = NULL;
  setofb_cpy (allvars, setofb_uni(varsofprocess, funcofprocess));
  menuq = fopen(MENUQ_NAME,"rb");

  for (nsubs=1; nsubs<=subproc_sq; nsubs++) {
    inftmp = inf;
    inf = (procinfptr)getmem_((word)sizeof(procinfrec));
    inf->next = inftmp;

    /* diagrams information */
    rd_menu(2,nsubs,txt,&ndel,&ncalc,&nrest,&recpos);
    inf->firstdiagpos = recpos;
    getprtcls(txt, inf->p_name);

    /* particle information */
    for (i=1; i<=nin+nout; i++)	{
      locateinbase (inf->p_name[i-1], &nn);
      strcpy(mass, prtclbase[nn-1].massidnt);
      npos = 0;
      if (strcmp(mass,"0") != 0) {
	modl = modelvars;
	npos = 1;
	k = 1;
	while (strcmp(modl->varname,mass) != 0) {
	  if (insetb(k + maxsp+1,allvars)) ++(npos);
	  ++(k);
	  modl = modl->next;
	}
      }
      inf->p_masspos[i-1] = npos;
    }
    for (i=nin+nout+1; i<=MAXINOUT; i++) {
      strcpy(inf->p_name[i-1],"***");
      inf->p_masspos[i-1] = 0;
    }
    fseek(diagrq,recpos*sizeof(csdiagram),SEEK_SET);
    inf->tot = 0;
    for (i=1; i<=ndel+ncalc+nrest; i++)	{
      FREAD1(csd, diagrq);
      if (csd.status == 1) ++(inf->tot);
    }
  }

  nsubs--;
  fclose(menuq);
  revers((pointer*)&inf);

}

/* ==================================================================== */
/* we make no distinction between vars and funcs, since everything
   will be initialized by the F90 caller code */

static void calc_nvars(void) {
  byte  k = 0;
  
  nvars = 0;
  modl = modelvars;

  while (modl != NULL) {  
    ++(k);
    if (insetb(k + maxsp+1,varsofprocess)) ++(nvars);
    if (insetb(k + maxsp+1,funcofprocess)) ++(nvars);
    modl = modl->next;
  }
} 

/* Originally in procvar.c */
static void f90initvarnames(void) { 
  int       k, kk=0;
  varlist      p;
  setofbyte    allvars;
  int nvar;

  nvar=maxsp+1; /*16*/

  p = modelvars;
  while (p != NULL) {
    ++(nvar);
    p = p->next;
  }
  varnamesptr=(varname *)getmem_(7 * nvar);

  for (k = 1; k <= maxsp; k++) {
    sbld((*varnamesptr)[maxsp- k],"P(%d)", k);
    /*    if(k<10)	sbld((*varnamesptr)[maxsp- k],"P%c",chr(k + ord('0')));
	  else	sbld((*varnamesptr)[maxsp- k],"P%c",chr(55 + k)); */
  }
  strcpy((*varnamesptr)[maxsp], "||||||");
  
  getprocessvar(0,varsofprocess,funcofprocess);

  k = maxsp+1; /*16*/
  setofb_cpy( allvars, setofb_uni(varsofprocess, funcofprocess));
  p = modelvars;
  while (p != NULL) {
    ++(k);
    if (insetb(calcvarpos(p->varname),allvars))
      sbld((*varnamesptr)[k-1],"A(%d)",++kk);
    p = p->next;
  }
}

/************************************************************************/
/* F90 code output */

/* Write a comment header into the file */
static void f90_header(int ind) {
  fout(ind,"!********************************************\n");
  fout(ind,"!* %s*\n", version);
  fout(ind,"!********************************************\n");
  fout(ind,"!* F90 format by Wolfgang Kilian Jul 19 1999 \n");
  fout(ind,"!********************************************\n");
}

/* Write the process identifier as a comment */
static void process_identifier(int ind) {
  fout(ind, "!\n! Process: %s\n!\n", processch);
}

/* Yes, we use strict typing and a local namespace */
static void module_implicit_declaration(int ind) {
  fout(ind, "implicit none\n");
  fout(ind, "private\n");
}

static void contains(int ind) {
  fout(ind, "contains\n"); }

/* This writes an 'end' statement without trailing newline */
/* Append any F90 declaration after it */
static void f_end(int indent) { fout(indent, "end"); }

static void nl(void) { fout(0, "\n"); }

/* ==================================================================== */
/* The kind type, which comes from module 'utils' */

static void use_precision(int ind) {
  fout(ind, "use kinds, only: default ");
  fout(1, "!NODEP!\n");
}  

/* ==================================================================== */
/* Print a string describing the current process */
/*  static void f_process_str(prtclsarray name) { */
/*    int i; */
/*    for (i=1; i<=nin+nout; i++) { */
/*      fout(1, name[i-1]); */
/*      if (i==nin) fout(1, "=>"); */
/*      else if (i!=nin+nout) fout(0, ","); */
/*    } */
/*  } */

/* ==================================================================== */
/* Module kinds and module file_utils are not comphep-specific, 
   but provided here. */

/*
static void write_module_kinds(void) {
  f90file = fopen(scat("%sresults%ckinds.f90", pathtouser, f_slash), "w");
  f90_header(0);

  fout(0, "module kinds\n");
 
  module_implicit_declaration(2);  nl();

  fout(0, "! Three types of precision.  double is the default, usually.\n");
  fout(2, "public :: single, double, quadruple\n");
  nl();

  fout(2, "integer, parameter :: single = &\n");
  fout(7, "& selected_real_kind (precision(1.), range(1.))\n");
  fout(2, "integer, parameter :: double = &\n");
  fout(7, "& selected_real_kind (precision(1._single) + 1, range(1._single) + 1)\n");
  fout(2, "integer, parameter :: quadruple = &\n");
  fout(7, "& selected_real_kind (precision (1._double) + 1, range (1._double))\n");
  nl();
  fout(2, "integer, parameter :: default = double\n");
  nl();

  fout(0, "end module kinds\n");
  fclose(f90file);
}
*/

/*
static void write_module_file_utils(void) {
  f90file = fopen(scat("%sresults%cfile_utils.f90", pathtouser, f_slash), "w");
  f90_header(0);

  fout(0, "module file_utils\n");
 
  module_implicit_declaration(2);  nl();

  fout(0, "! Function to allow for generic I/O\n");
  fout(0, "! Returns the next free unit, or -1 if it fails\n");
  fout(2, "public :: free_unit\n");
  nl();

  fout(2, "integer, parameter :: MIN_UNIT = 11, MAX_UNIT = 99\n");
  nl();

  contains(0); nl();

  fout(2, "function free_unit () result (unit)\n");
  fout(4, "integer :: unit\n");
  fout(4, "logical :: exists, is_open\n");
  fout(4, "integer :: i, status\n");
  fout(4, "do i = MIN_UNIT, MAX_UNIT\n");
  fout(7, "inquire (unit=i, exist=exists, opened=is_open, iostat=status)\n");
  fout(7, "if (status == 0) then\n");
  fout(10,"if (exists .and. .not. is_open) then\n");
  fout(12,"unit = i; return\n");
  fout(10,"end if\n");
  fout(7, "end if\n");
  fout(4, "end do\n");
  fout(4, "unit = -1\n");
  fout(2, "end function free_unit\n");
  nl();

  fout(0, "end module file_utils\n");
  fclose(f90file);
}
*/

/* ==================================================================== */
/* Module XXX_const contains the definition of constant parameters
   We use static memory.  This requires f90constcount to be known, which 
   is true only after the matrix element has been written */

static void module_const_name(int ind) {
  fout(ind, "module %s_const\n", globalname); }

static void write_module_const(void) {
  f90file = fopen(scat("%sresults%c%s_const.f90",
		       pathtouser, f_slash, globalname),"w");
  f90_header(0);
  process_identifier(0);

  module_const_name(0);  nl();

  use_precision(2); nl();

  fout(2, "implicit none\n");
  fout(2, "public\n");
  nl();

  fout(2, "! Width prescription\n");
  fout(2, "logical :: gwidth, rwidth\n");
  nl();

  fout(2, "! Physical parameters\n");
  fout(2, "real(kind=default), dimension(%d) :: A\n", nvars);
  nl();

  fout(2, "! Derived quantities\n");
  fout(2, "real(kind=default), dimension(%d) :: C\n", f90constcount);
  nl();

  fout(2, "save\n");
  nl();

  f_end(0); module_const_name(1);
  fclose(f90file);

}



/* ==================================================================== */
/* Module parameters contains definitions and routines for
   manipulating physical constants */

/*
static int valid_parameter(char* name) {
  return (strcmp(name,"Sqrt2")!=0) && (strcmp(name,"i")!=0); }

static void pipe_to_excl(char* source, char* result) {
  while(*result++ = (*source != '|') ? *source : '!') source++;
}
static void module_parameters_name(int ind) {
  fout(ind, "module parameters\n"); }

static void write_module_parameters(void) {
  int i;
  char buff[STRSIZ];

  f90file = fopen(scat("%sresults%cparameters.f90",
		       pathtouser, f_slash),"w");
  f90_header(0);
  process_identifier(0);

  module_parameters_name(0);
  use_precision(2);
  fout(2, "use file_utils, only: free_unit\n");

  module_implicit_declaration(2);  nl();

  fout(2, "public :: read, write\n");
  nl();

  fout(2, "type, public :: parameter_set\n");
  fout(5, "logical :: gwidth, rwidth\n");
  fout(5, "real(kind=default) :: sqrt2\n");
  for (modl=modelvars; modl->next != NULL;  modl = modl->next)
    if (valid_parameter(modl->varname))
      fout(5, "real(kind=default) :: %s\n", modl->varname);
  fout(2, "end type parameter_set\n"); nl();
  
  fout(2,"type(parameter_set), parameter, public :: &\n");
  fout(4, "& parameters_default = parameter_set( &\n");
  fout(4, "&  .false., .false., &\n");
  fout(4, "&  1.41421356237309504880168872420970_default ");

  i = 0;
  for (modl=modelvars; modl->next != NULL;  modl = modl->next) {
    if (valid_parameter(modl->varname)) {
      if (i==0) { fout(0, "&\n    &"); i=0; }
      fout(0, ",%17.10E_default", modl->varvalue);
      i++;  if (i==3) i=0;
    }
  }
  fout(0, "&\n    & )\n"); nl();
  
  fout(2, "interface read\n");
  fout(5, "module procedure read_parameters_unit\n");
  fout(5, "module procedure read_parameters_name\n");
  fout(2, "end interface\n");
  fout(2, "interface write\n");
  fout(5, "module procedure write_parameters_unit\n");
  fout(5, "module procedure write_parameters_name\n");
  fout(2, "end interface\n");

  nl(); contains(0); nl();

  fout(2, "subroutine read_parameters_unit(unit, par)\n");
  fout(4, "integer, intent(in) :: unit\n");
  fout(4, "type(parameter_set), intent(inout) :: par\n");
  fout(4, "logical :: gwidth, rwidth\n");
  fout(4, "real(kind=default) :: sqrt2\n");
  fout(4, "real(kind=default) ::");
  i = 0;
  for (modl=modelvars; modl->next != NULL;  modl = modl->next)
    if (valid_parameter(modl->varname)) {
      if (i>0) fout(0, ",");
      i++;  if (i%10==1) { fout(0, " &\n"); fout(6, "&"); }
      fout(1, "%s", modl->varname);
    }
  nl();
  fout(4, "namelist /parameter_input/ gwidth, rwidth");
  i = 0;
  for (modl=modelvars; modl->next != NULL;  modl = modl->next)
    if (valid_parameter(modl->varname)) {
      fout(0, ",");
      i++;  if (i%10==1) { fout(0, " &\n"); fout(6, "&"); }
      fout(1, "%s", modl->varname);
    }
  nl();
  fout(4, "gwidth = par%%gwidth\n");
  fout(4, "rwidth = par%%rwidth\n");
  fout(4, "sqrt2  = par%%sqrt2\n");
  for (modl=modelvars; modl->next != NULL;  modl = modl->next)
    if (valid_parameter(modl->varname)) {
      fout(4, "%-7s = par%%%s\n", modl->varname, modl->varname);
    }
  fout(4, "read(unit, nml=parameter_input)\n");
  fout(4, "par%%gwidth = gwidth\n");
  fout(4, "par%%rwidth = rwidth\n");
  for (modl=modelvars; modl->next != NULL;  modl = modl->next)
     if (valid_parameter(modl->varname)) {
       if (modl->func != NULL) {
	 pipe_to_excl(modl->func, buff);
	 fout(4, "%-7s = %s\n", modl->varname, buff);
       }
       else {
	 fout(4, "par%%%-7s = %s\n", modl->varname, modl->varname);
       }
     }
  for (modl=modelvars; modl->next != NULL;  modl = modl->next)
    if (valid_parameter(modl->varname) && (modl->func != NULL)) {
      fout(4, "par%%%-7s = %s\n", modl->varname, modl->varname);
    }
  fout(2, "end subroutine read_parameters_unit\n"); nl();

  fout(2, "subroutine write_parameters_unit(unit, par)\n");
  fout(4, "integer, intent(in) :: unit\n");
  fout(4, "type(parameter_set), intent(in) :: par\n");
  fout(4, "write(unit, *) '&parameter_input'\n");
  fout(4, "write(unit, *) 'gwidth = ', par%%gwidth\n");
  fout(4, "write(unit, *) 'rwidth = ', par%%rwidth\n");
  for (modl=modelvars; modl->next != NULL;  modl = modl->next)
    if (valid_parameter(modl->varname))
      fout(4, "write(unit, *) '%-7s =', par%%%s\n", 
	   modl->varname, modl->varname);
  fout(4, "write(unit, *) '/'\n");
  fout(2, "end subroutine write_parameters_unit\n"); nl();

  fout(2, "subroutine read_parameters_name(name, par)\n");
  fout(4, "character(len=*), intent(in) :: name\n");
  fout(4, "type(parameter_set), intent(inout) :: par\n");
  fout(4, "integer :: unit\n");
  fout(4, "unit = free_unit ()\n");
  fout(4, "open (unit=unit, action='read', status='old', file=name)\n");
  fout(4, "call read_parameters_unit (unit, par)\n");
  fout(4, "close (unit=unit)\n");
  fout(2, "end subroutine read_parameters_name\n"); nl();

  fout(2, "subroutine write_parameters_name(name, par)\n");
  fout(4, "character(len=*), intent(in) :: name\n");
  fout(4, "type(parameter_set), intent(in) :: par\n");
  fout(4, "integer :: unit\n");
  fout(4, "unit = free_unit ()\n");
  fout(4, "open (unit=unit, action='write', status='replace', file=name)\n");
  fout(4, "call write_parameters_unit (unit, par)\n");
  fout(4, "close (unit=unit)\n");
  fout(2, "end subroutine write_parameters_name\n"); nl();

  f_end(0); module_parameters_name(2);
  fclose(f90file);
}
*/

/************************************************************************/
/* Find the appropriate permutation call for a subprocess */
/* No change in this function, except for generated F90 code */
static void write_permutation (int ind, prtclsarray name) {
  int bas[5];
  int place[2], howmany[2];
  int group, placeswap, placesub, howmanysub;
  int i, j, n0;

  bas[0] = 1; bas[1] = 2; bas[2] = 6; bas[3] = 24; bas[4] = 120;

  n0 = 1;  i = 1;  group = 0;

  /* Count the number of identical particles */
  /* There may be up to two groups of two or three of them */
  while (i < nout) {
    j = i + 1;
    while (j <= nout && strcmp(name[i + nin-1], name[j + nin-1]) == 0)
      ++(j);
    if (j > i + 1) {
      ++(group);
      place[group-1] = nin + i - 1;
      howmany[group-1] = j - i;
      n0 = n0 * bas[j - i - 1];
    }
    i = j;
  }

  switch (group) {
  case 0:  /* all particles different */
    fout(ind, "s = sqme_single(PP)\n");
    break;
  case 1:  /* one group of two,three,four, or five */
    fout(ind, "s = sqme_perm(PP, %d, %d) / %d\n",
             howmany[0], place[0], n0);
    break;
  case 2:  /* two groups: two+two or two+three */
    placeswap = place[0];  placesub = place[1];  howmanysub = howmany[1];
    if (howmany[0] != 2) {
      placeswap = place[1];  placesub = place[0];  howmanysub = howmany[0];
    }
    fout(ind, "s = sqme_perm(PP, %d, %d)\n", 
	     howmanysub, placesub);
    fout(ind, "call swap(PP, %d, %d, %d)\n", 
	     nin+nout, placeswap + 1, placeswap + 2);
    fout(ind, "s = (s + sqme_perm(PP, %d, %d)) / 4\n", 
	     howmanysub, placesub);
  }

}

/************************************************************************/
/* The top-level module which contains the sqme function */

static void module_x_name(int ind) {
  fout(ind, "module %s\n", globalname); }

static void write_module_x(int* sub_file0) {
  int i, j;

  f90file = fopen(scat("%sresults%c%s.f90",
			pathtouser, f_slash, globalname),"w");   

  f90_header(0);                    /* Insert header indicating version */
  process_identifier(0);            /* Write process as comment */  
  fout(0, "! This module contains the WHIZARD interface to the CompHEP matrix element\n\n");

  module_x_name(0); nl();

  use_precision(2); nl();

  fout(2, "use parameters, only: parameter_set\n"); nl();

  fout(2, "use %s_const, only: gwidth, rwidth, A\n", globalname);
/*    for (i=1; i<=subproc_sq; i++) */
/*      fout(2, "use %s_c%s\n", globalname, funstr(i)); */
  for (i=0; i<sub_file0[subproc_sq]; i++)
    fout(2, "use %s_%s, only: set_const_%s, sqme_%s\n",
	 globalname, funstr(i), funstr(i), funstr(i));
  nl();

  module_implicit_declaration(2); nl();

  /* The old interface */
/*    fout(2, "! The user interface\n"); */
/*    fout(2, "public :: n_in, n_out, pdg_code\n"); */
/*    fout(2, "public :: allow_helicities\n"); */
/*    fout(2, "public :: create, destroy\n"); */
/*    fout(2, "public :: set_const, sqme\n"); */

  /* The new interface */
  fout(2, "! The user interface\n");
  fout(2, "public :: scatter_nonzero\n");
  fout(2, "public :: scatter_diagonal, scatter_diagonal_nonzero\n");
  fout(2, "public :: scatter_colored_nonzero, scatter_diagonal_colored_nz\n");
  fout(2, "public :: number_particles, number_particles_in, number_particles_out\n");
  fout(2, "public :: number_spin_states, number_spin_states_in, number_spin_states_out\n");
  fout(2, "public :: spin_states, spin_states_in, spin_states_out\n");
  fout(2, "public :: number_flavor_states, number_flavor_states_in, number_flavor_states_out\n");
  fout(2, "public :: flavor_states, flavor_states_in, flavor_states_out\n");
  fout(2, "public :: number_flavor_zeros, number_flavor_zeros_in, number_flavor_zeros_out\n");
  fout(2, "public :: flavor_zeros, flavor_zeros_in, flavor_zeros_out\n");
  fout(2, "public :: number_color_flows\n");
  fout(2, "public :: color_flows, anticolor_flows\n");
  fout(2, "public :: create, reset, destroy\n");

  /* The particle codes */
  nl();
  fout(0, "include 'prt_codes.inc'\n");

  nl();

  contains(0); nl();

  /* =============================================================== */
  /* Subroutines */

  /* Number of initial/final particles */
  fout(2, "function number_particles () result (n)\n");
  fout(4, "integer :: n\n");
  fout(4, "n = %d + %d\n", nin, nout);
  fout(2, "end function number_particles\n");

  fout(2, "function number_particles_in () result (n)\n");
  fout(4, "integer :: n\n");
  fout(4, "n = %d\n", nin);
  fout(2, "end function number_particles_in\n");

  fout(2, "function number_particles_out () result (n)\n");
  fout(4, "integer :: n\n");
  fout(4, "n = %d\n", nout);
  fout(2, "end function number_particles_out\n");
  nl();

/*    fout(2, "function n_in ()\n"); */
/*    fout(4, "integer :: n_in\n"); */
/*    fout(4, "n_in = %d\n", nin); */
/*    fout(2, "end function n_in\n"); */
/*    nl() */

/*    fout(2, "function n_out ()\n"); */
/*    fout(4, "integer :: n_out\n"); */
/*    fout(4, "n_out = %d\n", nout); */
/*    fout(2, "end function n_out\n"); */
/*    nl(); */

/*    fout(2, "function n_tot ()\n"); */
/*    fout(4, "integer :: n_tot\n"); */
/*    fout(4, "n_tot = %d\n", nin+nout); */
/*    fout(2, "end function n_tot\n"); */
/*    nl(); */

  /* Spin states: CompHEP does not support polarization, so return zero */
  fout(2, "function number_spin_states () result (n)\n");
  fout(4, "integer :: n\n");
  fout(4, "n = 0\n");
  fout(2, "end function number_spin_states\n");

  fout(2, "function number_spin_states_in () result (n)\n");
  fout(4, "integer :: n\n");
  fout(4, "n = 1\n");
  fout(2, "end function number_spin_states_in\n");

  fout(2, "function number_spin_states_out () result (n)\n");
  fout(4, "integer :: n\n");
  fout(4, "n = 1\n");
  fout(2, "end function number_spin_states_out\n");
  nl();

  /* Since there is no polarization, these are dummy routines: */
  fout(2, "subroutine spin_states (a)\n");
  fout(4, "integer, dimension(:,:), intent(inout) :: a\n");
  fout(4, "a = 0\n");
  fout(2, "end subroutine spin_states\n");

  fout(2, "subroutine spin_states_in (a)\n");
  fout(4, "integer, dimension(:,:), intent(inout) :: a\n");
  fout(4, "a = 0\n");
  fout(2, "end subroutine spin_states_in\n");

  fout(2, "subroutine spin_states_out (a)\n");
  fout(4, "integer, dimension(:,:), intent(inout) :: a\n");
  fout(4, "a = 0\n");
  fout(2, "end subroutine spin_states_out\n");
  nl();

  /* Number of active processes */
  /* Currently, we support only a single flavor state per process */
  fout(2, "function number_flavor_states () result (n)\n");
  fout(4, "integer :: n\n");
  fout(4, "n = 1\n");
  fout(2, "end function number_flavor_states\n");

  fout(2, "function number_flavor_states_in () result (n)\n");
  fout(4, "integer :: n\n");
  fout(4, "n = 1\n");
  fout(2, "end function number_flavor_states_in\n");

  fout(2, "function number_flavor_states_out () result (n)\n");
  fout(4, "integer :: n\n");
  fout(4, "n = 1\n");
  fout(2, "end function number_flavor_states_out\n");
  nl();

  /* PDG particle codes */
  /* The nsub loop has only one iteration in realty */

  fout(2, "function pdg_code (name) result (code)\n");
  fout(4, "character(len=*), intent(in) :: name\n");
  fout(4, "integer :: code\n");
  fout(4, "select case (name)\n");
  fout(0, "include 'prt_names.inc'\n");
  fout(4, "case default;  code = 0\n");
  fout(4, "end select\n");
  fout(2, "end function pdg_code\n");
  nl();

  fout(2, "subroutine flavor_states (a)\n");
  fout(4, "integer, dimension(:,:), intent(inout) :: a\n");
  inftmp = inf;
  for (nsub=1; nsub<=nsub_tot; nsub++) {
    for (i = 1; i <= nin + nout; i++) {
      fout(4, "a(%d, 1) = pdg_code ('%s')\n", i, inftmp->p_name[i-1]);
    }
    inftmp = inftmp->next;
  }
  fout(2, "end subroutine flavor_states\n");

  fout(2, "subroutine flavor_states_in (a)\n");
  fout(4, "integer, dimension(:,:), intent(inout) :: a\n");
  inftmp = inf;
  for (nsub=1; nsub<=nsub_tot; nsub++) {
    for (i = 1; i <= nin; i++) {
      fout(4, "a(%d, 1) = pdg_code ('%s')\n", i, inftmp->p_name[i-1]);
    }
    inftmp = inftmp->next;
  }
  fout(2, "end subroutine flavor_states_in\n");

  fout(2, "subroutine flavor_states_out (a)\n");
  fout(4, "integer, dimension(:,:), intent(inout) :: a\n");
  inftmp = inf;
  for (nsub=1; nsub<=nsub_tot; nsub++) {
    for (i = 1; i <= nout; i++) {
      fout(4, "a(%d, 1) = pdg_code ('%s')\n", i, inftmp->p_name[nin+i-1]);
    }
    inftmp = inftmp->next;
  }
  fout(2, "end subroutine flavor_states_out\n");
  nl();

  /* The old version */
/*    fout(2, "function pdg_code (i)\n"); */
/*    fout(4, "integer, intent(in) :: i\n"); */
/*    fout(4, "integer :: pdg_code\n"); */
/*    fout(4, "select case(i)\n"); */

/*    inftmp = inf; */
/*    for (nsub=1; nsub<=nsub_tot; nsub++) { */
/*      for (i = 1; i <= nin + nout; i++) { */
/*        fout(4, "case(%2d); pdg_code = %8d  ! %s\n",   */
/*  	   i, pdg_code(inftmp->p_name[i-1]), inftmp->p_name[i-1]); */
/*      } */
/*      fout(4,   "case default; pdg_code =  0\n"); */
/*      inftmp = inftmp->next; */
/*    } */

/*    fout(4, "end select\n"); */
/*    fout(2, "end function pdg_code\n"); */
/*    nl(); */


  /* Number of deleted processes */
  /* Currently, no flavor state will be deleted */
  fout(2, "function number_flavor_zeros () result (n)\n");
  fout(4, "integer :: n\n");
  fout(4, "n = 0\n");
  fout(2, "end function number_flavor_zeros\n");

  fout(2, "function number_flavor_zeros_in () result (n)\n");
  fout(4, "integer :: n\n");
  fout(4, "n = 0\n");
  fout(2, "end function number_flavor_zeros_in\n");

  fout(2, "function number_flavor_zeros_out () result (n)\n");
  fout(4, "integer :: n\n");
  fout(4, "n = 0\n");
  fout(2, "end function number_flavor_zeros_out\n");
  nl();

  /* PDG particle codes of deleted processes */
  /* These are dummy routines */
  fout(2, "subroutine flavor_zeros (a)\n");
  fout(4, "integer, dimension(:,:), intent(inout) :: a\n");
  fout(4, "a = 0\n");
  fout(2, "end subroutine flavor_zeros\n");

  fout(2, "subroutine flavor_zeros_in (a)\n");
  fout(4, "integer, dimension(:,:), intent(inout) :: a\n");
  fout(4, "a = 0\n");
  fout(2, "end subroutine flavor_zeros_in\n");

  fout(2, "subroutine flavor_zeros_out (a)\n");
  fout(4, "integer, dimension(:,:), intent(inout) :: a\n");
  fout(4, "a = 0\n");
  fout(2, "end subroutine flavor_zeros_out\n");
  nl();

  /* Number of color flows: zero since no color info is generated */
  fout(2, "function number_color_flows () result (n)\n");
  fout(4, "integer :: n\n");
  fout(4, "n = 0\n");
  fout(2, "end function number_color_flows\n");

  /* Since there is no color, these are dummy routines: */
  fout(2, "subroutine color_flows (a)\n");
  fout(4, "integer, dimension(:,:), intent(inout) :: a\n");
  fout(4, "a = 0\n");
  fout(2, "end subroutine color_flows\n");

  fout(2, "subroutine anticolor_flows (a)\n");
  fout(4, "integer, dimension(:,:), intent(inout) :: a\n");
  fout(4, "a = 0\n");
  fout(2, "end subroutine anticolor_flows\n");
  nl();

  /* The multiplicity of equivalent processes.  Irrelevant here */
  fout(2, "subroutine multiplicity (m)\n");
  fout(4, "integer, dimension(:), intent(inout) :: m\n");
  fout(4, "m = 1\n");
  fout(2, "end subroutine multiplicity\n");
  nl();

  /* CompHEP matrix elements are unpolarized */
  /* This is obsolete */
/*    fout(2, "function allow_helicities()\n"); */
/*    fout(4, "logical :: allow_helicities\n"); */
/*    fout(4, "allow_helicities = .false.\n"); */
/*    fout(2, "end function allow_helicities\n"); */
/*    nl(); */

  /* Initialize the const module [no-op] */
  fout(2, "subroutine create\n");
/*    for (nsub=1; nsub<=nsub_tot; nsub++) */
/*      fout(4, "call create_%s\n", funstr(nsub)); */
  fout(2, "end subroutine create\n");
  nl();

  /* Deallocate the const module contents [no-op] */
  fout(2, "subroutine destroy\n");
/*    for (nsub=1; nsub<=nsub_tot; nsub++) */
/*      fout(4, "call destroy_%s\n", funstr(nsub)); */
  fout(2, "end subroutine destroy\n");
  nl();

  /* Fill a C array set from a given parameter set */
  /* This has to be done once for each subprocess */
  /* isub absent means do it for all subprocesses */

  /* The function name has changed */
  fout(2, "subroutine reset (par)\n");
/*    fout(2, "subroutine set_const (par)\n"); */

  fout(4, "type(parameter_set), intent(in) :: par\n");

  fout(4, "gwidth = par%%gwidth\n");
  fout(4, "rwidth = par%%rwidth\n");  

  i=0;
  modl = modelvars;
  while (modl != NULL) {
    if (insetb(calcvarpos(modl->varname), varsofprocess) ||
        insetb(calcvarpos(modl->varname),funcofprocess))
      fout(4, "A(%2d) = par%%%s\n", ++i, modl->varname); 
    modl = modl->next;
  }
/*    fout(4, "if (present(adjust)) then\n"); */
/*    fout(7, "if (adjust) then\n"); */

/*    fout(10, "A_tmp = A\n"); */
/*    i=0; */
/*    for (modl=modelvars; modl->next != NULL;  modl = modl->next) { */
/*      if (insetb(calcvarpos(modl->varname), varsofprocess)) i++; */
/*      else if (insetb(calcvarpos(modl->varname), funcofprocess)) { */
/*        char *s, *ss;  char c;  int len;  int lenmax=60; */
/*        i++; */
/*        ss=(char *)readexprassion(modl->func, bact5, uact5, rd5); */
/*        s=ss+3; */

/*        fout(10, "cb%%A(%2d) = ", i); */
/*        len=strlen(s); */
/*        while (len>0) { */
/*  	if (len > lenmax){ c=s[lenmax]; s[lenmax]=0; } */
/*  	fout(0, s); */
/*  	if (len > lenmax) {  */
/*  	  fout(0, "&\n");  fout(17, "&"); */
/*  	  s[lenmax]=c; s+=lenmax; */
/*  	} */
/*  	len -= lenmax; */
/*        } */
/*        nl();  fout(10, "par%%%s = cb%%A(%2d)\n", modl->varname, i); */
/*        free(ss); */
/*      } */
/*    }  */

/*    fout(7, "end if\n"); */
/*    fout(4, "end if\n"); */

  for (nsub=1; nsub<=nsub_tot; nsub++) {
/*      fout(6, "case(%d)\n", nsub); */
    for (j = sub_file0[nsub-1]; j < sub_file0[nsub]; j++)
      fout(4, "call set_const_%s\n", funstr(j));
  }
  
/*    fout(4, "if (present(isub)) then\n"); */
/*    fout(7, "call set_const(isub)\n"); */
/*    fout(4, "else\n"); */
/*    fout(7, "do i=1, cb%%nsub\n"); */
/*    fout(10, "call set_const(i)\n"); */
/*    fout(7, "end do\n"); */
/*    fout(4, "end if\n"); */
/*    fout(2, "contains\n"); */

/*    if (precision) i='Q';  else i='D'; */
/*    fout(4, "function %cSQRT(x) result(y)\n", i); */
/*    fout(6, "real(kind=default), intent(in) :: x\n"); */
/*    fout(6, "real(kind=default) :: y\n"); */
/*    fout(6, "y = sqrt(x)\n"); */
/*    fout(4, "end function %cSQRT\n", i); */

/*    if (precision) i='Q';  else i='D'; */
/*    fout(4, "function %cFLOAT(x) result(y)\n", i); */
/*    fout(6, "integer, intent(in) :: x\n"); */
/*    fout(6, "real(kind=default) :: y\n"); */
/*    fout(6, "y = real(x, kind=default)\n"); */
/*    fout(4, "end function %cFLOAT\n", i); */

/*    fout(4, "subroutine set_const(isub)\n"); */
/*    fout(6, "integer, intent(in) :: isub\n"); */
/*    fout(6, "select case(isub)\n"); */
/*    for (i=1; i<=nsub_tot; i++) { */
/*      fout(6, "case(%d)\n", i); */
/*      for (j = sub_file0[i-1]; j < sub_file0[i]; j++) */
/*        fout(9, "call chep_set_const_%s(cb%%A, cb%%sub(i)%%C, P)\n", */
/*  	   funstr(j), i); */
/*    } */
/*    fout(6, "end select\n"); */
/*    fout(4, "end subroutine set_const\n"); */

  fout(2, "end subroutine reset\n");
/*    fout(2, "end subroutine set_const\n"); */
  nl();

  /* Scattering generic density matrices is identical to diagonal
     density matrices since polarization is absent anyway.
     This version has a dummy argument 'zero' */
  fout(2, "subroutine scatter_nonzero (p, rho_in, rho_out, zero, n)\n");
  fout(4, "real(kind=default), dimension(0:,:), intent(in) :: p\n");
  fout(4, "complex(kind=default), dimension(:,:,:,:), intent(in) :: rho_in\n");
  fout(4, "complex(kind=default), dimension(:,:,:,:), intent(inout) :: rho_out\n");
  fout(4, "integer, dimension(:,:,:,:), intent(inout) :: zero\n");
  fout(4, "integer, intent(in) :: n\n");
  fout(4, "rho_out = sqme (p) * rho_in\n");
  fout(2, "end subroutine scatter_nonzero\n");
  nl();

  /* The diagonal scatter function: no matrix multiplication either 
     only, we need no matrix multiplication here.  The implicit assumption 
     is that rho_in and rho_out have only one element */
  fout(2, "subroutine scatter_diagonal (p, rho_in, rho_out)\n");
  fout(4, "real(kind=default), dimension(0:,:), intent(in) :: p\n");
  fout(4, "real(kind=default), dimension(:,:), intent(in) :: rho_in\n");
  fout(4, "real(kind=default), dimension(:,:), intent(inout) :: rho_out\n");
  fout(4, "rho_out = sqme (p) * rho_in\n");
  fout(2, "end subroutine scatter_diagonal\n");
  nl();

  /* This version has a dummy argument 'zero' */
  fout(2, "subroutine scatter_diagonal_nonzero (p, rho_in, rho_out, zero, n)\n");
  fout(4, "real(kind=default), dimension(0:,:), intent(in) :: p\n");
  fout(4, "real(kind=default), dimension(:,:), intent(in) :: rho_in\n");
  fout(4, "real(kind=default), dimension(:,:), intent(inout) :: rho_out\n");
  fout(4, "integer, dimension(:,:,:,:), intent(inout) :: zero\n");
  fout(4, "integer, intent(in) :: n\n");
  fout(4, "rho_out = sqme (p) * rho_in\n");
  fout(2, "end subroutine scatter_diagonal_nonzero\n");
  nl();

  /* The colored version.  We do not support color, so we assume that */
  /* the first (color) index of rho_out runs from 1 to 1 */
  /* This version has a dummy argument 'zero' */
  fout(2, "subroutine scatter_colored_nonzero (p, rho_in, rho_out, rho_col_out, zero, n)\n");
  fout(4, "real(kind=default), dimension(0:,:), intent(in) :: p\n");
  fout(4, "complex(kind=default), dimension(:,:,:,:), intent(in) :: rho_in\n");
  fout(4, "complex(kind=default), dimension(:,:,:,:), intent(inout) :: rho_out\n");
  fout(4, "complex(kind=default), dimension(:,:,:,:,:), intent(inout) :: rho_col_out\n");
  fout(4, "integer, dimension(:,:,:,:), intent(inout) :: zero\n");
  fout(4, "integer, intent(in) :: n\n");
  fout(4, "rho_out = sqme (p) * rho_in\n");
  fout(4, "rho_col_out = 1\n");
  fout(2, "end subroutine scatter_colored_nonzero\n");
  nl();

  /* The same for reduced (still trivial) spin densities */
  /* This version has a dummy argument 'zero' */
  fout(2, "subroutine scatter_diagonal_colored_nz (p, rho_in, rho_out, zero, n)\n");
  fout(4, "real(kind=default), dimension(0:,:), intent(in) :: p\n");
  fout(4, "real(kind=default), dimension(:,:), intent(in) :: rho_in\n");
  fout(4, "real(kind=default), dimension(:,:,:), intent(inout) :: rho_out\n");
  fout(4, "integer, dimension(:,:,:,:), intent(inout) :: zero\n");
  fout(4, "integer, intent(in) :: n\n");
  fout(4, "rho_out(1,:,:) = sqme (p) * rho_in\n");
  fout(2, "end subroutine scatter_diagonal_colored_nz\n");
  nl();

  /* ----------------------------------------------------------------- */
  /* Write the SQME subroutine */
  fout(2, "function sqme (p) result(s)\n", globalname);
  fout(4, "real(kind=default), dimension(0:,:), intent(in) :: p\n");
  fout(4, "real(kind=default) :: s\n");
  fout(4, "real(kind=default), dimension(%d) :: PP\n", 
       (nin+nout)*(nin+nout-1)/2);

  /* Set the scalar products array */
  /* Decay process */
  if (nin==1) {
    if (nout>=1) {
      fout(4, "PP(1) = sprod(1,2)\n");
    }
    if (nout>=2) {
      fout(4, "PP(2) = sprod(1,3)\n");
      fout(4, "PP(3) = sprod(2,3)\n");
    }
    if (nout>=3) {
      fout(4, "PP(4) = sprod(1,4)\n");
      fout(4, "PP(5) = sprod(2,4)\n");
      fout(4, "PP(6) = sprod(3,4)\n");
    }
    if (nout>=4) {
      fout(4, "PP(7) = sprod(1,5)\n");
      fout(4, "PP(8) = sprod(2,5)\n");
      fout(4, "PP(9) = sprod(3,5)\n");
      fout(4, "PP(10)= sprod(4,5)\n");
    }
    if (nout>=5) {
      fout(4, "PP(11)= sprod(1,6)\n");
      fout(4, "PP(12)= sprod(2,6)\n");
      fout(4, "PP(13)= sprod(3,6)\n");
      fout(4, "PP(14)= sprod(4,6)\n");
      fout(4, "PP(15)= sprod(5,6)\n");
    }
  /* Scattering process */
  } else {
    fout(4, "PP(1) = sprod(1,2)\n");
    if (nout>=1) {
       fout(4, "PP(2) = sprod(1,3)\n");
       fout(4, "PP(3) = sprod(2,3)\n");
    }
    if (nout>=2) {
       fout(4, "PP(4) = sprod(1,4)\n");
       fout(4, "PP(5) = sprod(2,4)\n");
       fout(4, "PP(6) = sprod(3,4)\n");
    }
    if (nout>=3) {
       fout(4, "PP(7) = sprod(1,5)\n");
       fout(4, "PP(8) = sprod(2,5)\n");
       fout(4, "PP(9) = sprod(3,5)\n");
       fout(4, "PP(10)= sprod(4,5)\n");
    }
    if (nout>=4) {
       fout(4, "PP(11)= sprod(1,6)\n");
       fout(4, "PP(12)= sprod(2,6)\n");
       fout(4, "PP(13)= sprod(3,6)\n");
       fout(4, "PP(14)= sprod(4,6)\n");
       fout(4, "PP(15)= sprod(5,6)\n");
    }
  }

  /* [For each subprocess] take care of identical particles */
  /* and set the SQME appropriately */
  inftmp = inf;
  for (nsub = 1; nsub <= subproc_sq; nsub++) {
/*      fout(4, "case(%d)", nsub); */
/*      fout(30, "!"); f_process_str(inftmp->p_name); nl(); */
    write_permutation(4, inftmp->p_name);
    inftmp = inftmp->next;
  }

  fout(2, "contains\n");

  fout(4, "function sprod(i,j)\n");
  fout(6, "integer, intent(in) :: i, j\n");
  fout(6, "real(kind=default) :: sprod\n");
  fout(6, "sprod = p(0,i)*p(0,j) - dot_product(p(1:,i), p(1:,j))\n");
  fout(4, "end function sprod\n");

  fout(2, "end function sqme\n");
  nl();

  /* ----------------------------------------------------------------- */
  /* Subroutines for permuting identical particles */

  fout(2, "subroutine swap(P, i, j)\n");
  fout(4,"real(kind=default), dimension(:), intent(inout) :: P\n");
  fout(4,"integer, intent(in) :: i,j\n");
  fout(4,"real(kind=default) :: tmp\n");
  fout(4,"integer :: i0\n");
  fout(4,"if (i==j) return\n");
  fout(4,"do i0=1, number_particles()\n");
  fout(7,"if (i0/=i .and. i0/=j) then\n");
  fout(10,"tmp = P(indx(i0,i))\n");
  fout(10,"P(indx(i0,i)) = P(indx(i0,j))\n");
  fout(10,"P(indx(i0,j)) = tmp\n");
  fout(7,"end if\n");
  fout(4,"end do\n");
  contains(2);
  fout(4,"function indx(k,l)\n");
  fout(6,"integer, intent(in) :: k,l\n");
  fout(6,"integer :: indx\n");
  fout(6,"integer :: i,j\n");
  fout(6,"i=min(k,l)\n");
  fout(6,"j=max(k,l)\n");
  fout(6,"indx=i+(j-1)*(j-2)/2\n");
  fout(4,"end function indx\n");
  fout(2, "end subroutine swap\n");
  nl();

  fout(2,"recursive function sqme_perm(P, n, ishift) result(s)\n");
  fout(4,"real(kind=default), dimension(:), intent(inout) :: P\n");
  fout(4,"integer, intent(in) :: n,ishift\n");
  fout(4,"real(kind=default) :: s\n");
  fout(4,"integer :: i\n");
  fout(4,"select case (n)\n");
  fout(4,"case (:1)\n");
  fout(7,"s = sqme_single(P)\n");
  fout(4,"case default\n");
  fout(7,"s = 0\n");
  fout(7,"do i=1,n\n");
  fout(10,"call swap(P, i+ishift, n+ishift)\n");
  fout(10,"s = s + sqme_perm(P, n-1, ishift)\n");
  fout(10,"call swap(P, i+ishift, n+ishift)\n");
  fout(7,"end do\n");
  fout(4,"end select\n");
  fout(2,"end function sqme_perm\n");
  nl();

  /* ----------------------------------------------------------------- */
  /* The low-level SQME function, without permutations */
  fout(2, "function sqme_single(P) result(s)\n");
  fout(4, "real(kind=default), dimension(:), intent(in) :: P\n");
  fout(4, "real(kind=default) :: s\n");

  /* Write SELECT construct with subprocess function calls */
  inftmp = inf;
  fout(4, "s = 0\n");
  for (i = 1; i <= subproc_sq; i++) {
    if (inftmp->tot != 0) {
/*        fout(4, "!"); f_process_str(inftmp->p_name); nl(); */
/*        fout(4, "case (%d)\n", i); */
      for (j = sub_file0[i-1]; j < sub_file0[i]; j++)
	fout(4, "s = s + sqme_%s(P)\n", funstr(j), i);
    }
    inftmp = inftmp->next;
  }
  fout(2,"end function sqme_single\n");
  nl();

  /* ----------------------------------------------------------------- */
  f_end(0);  module_x_name(1); nl();
  fclose(f90file);

}


/************************************************************************/
/* Write subprocesses: Cij, Fij */

/* Display statistics real-time on screen */

static void init_statistics(void) {
   goto_xy(1,17);
   scrcolor(Yellow,LightBlue);
   print(" FORTRAN90 Source Code \n");
   scrcolor(LightRed,Black);
   print(" Process..........\n");
   print(" Total diagrams...\n");
   print(" Processed........\n");
   print(" Current..........\n");
   scrcolor(Yellow,LightBlue);
   print(" Press Esc to stop    ");
   scrcolor(Yellow,Black);
   goto_xy(20,18); print("%s", processch);
   goto_xy(20,19); print("%4u", ndiagrtot);
   goto_xy(20,20); print("   0");
   scrcolor(Yellow ,Black);
   goto_xy(20,21); print("   1");
   scrcolor(Yellow,Black);
}

static void write_statistics(void) {
   goto_xy(20,19); print("%4u",ndiagrtot);
   goto_xy(20,20);
   print("%2u (%%)",(((diagrcount - 1) * 100) / ndiagrtot));
   scrcolor(Yellow ,Black);
   goto_xy(20,21); print("%4u",diagrcount);
   scrcolor(Yellow,Black);
}

static void clear_statistics(void) {
  int  i;
  for (i = 17; i < 24; i++) { goto_xy(1,i); clr_eol(); }
}

/* ==================================================================== */
/* Write one diagram */

/* The picture */
static void writpict (word ndiagr) {
  vcsect vcs;
  csdiagram  csdiagr;
  fseek(diagrq,ndiagr*sizeof(csdiagr),SEEK_SET);
  FREAD1(csdiagr,diagrq);
  transfdiagr(&csdiagr,&vcs);   
  fout(0, "! Diagram #%s \n!\n", funstr(diagrcount));
  writeTextDiagram(&vcs,1,'!',f90file);
  nl();
}

static void  write_one_diagram 
(int* const1, int* const2, deninforec* dendescript) {
  catrec      cr;
  byte        nexpr, i;
  word        k;
  marktp      bh;
  varptr      totnum, totdenum, rnum;
  longstrptr  tmplongs;
  char        istr[10];
  boolean     addpr;
  integer     numm;
  int         linepos,linepos1;

  *const1 = f90constcount+1;

  mark_(&bh);
  f90initConsts();
  f90initdegnames();
  
  fseek(catalog,dendescript->cr_pos,SEEK_SET);
  FREAD1(cr,catalog);
  ++(diagrcount);

  fseek(archiv,cr.factpos,SEEK_SET);
  FREAD1(nexpr,archiv);  /*  Nexpr must be equal 2  */

  readpolynom(&totnum);
  readpolynom(&totdenum);
  fseek(archiv,cr.rnumpos,SEEK_SET);
  FREAD1(nexpr,archiv);   /*  Nexpr must be equal 1  */
  readpolynom(&rnum);
  f90putnames();

  *const2 = f90constcount;
   
  nl();
  if (include_pictures) writpict(cr.ndiagr_ + inftmp->firstdiagpos - 1);

  /* --------------------------------------------------------------- */
  /* The constants for this diagram */

  fout(2,"subroutine set_const%s\n",funstr(diagrcount));

  linepos=ftell(f90file);
  fout(70,"\n");
  f90tmpNameMax=0;
  f90write_const();
  fout(2,"end subroutine set_const%s\n\n",funstr(diagrcount));

  linepos1=ftell(f90file);
  fseek(f90file,linepos,SEEK_SET);
  if(f90degnamecount>0) 
    fout(4, "real(kind=default) :: S(%d);", f90degnamecount);                  
  if(f90tmpNameMax>0)   
    fout(4, "real(kind=default) :: tmp(%d)", f90tmpNameMax);
  fseek(f90file,linepos1,SEEK_SET);

  /* ----------------------------------------------------------------- */
  /* The SQME for this diagram */

  f90cleardegnames();
  f90initdegnames(); 
  f90tmpNameMax=0;

  sbld(istr,"F%u",diagrcount);

  fout(2, "function sqme%s(D,P) result(%s)\n", funstr(diagrcount), istr);
  fout(4, "real(kind=default), dimension(:,0:), intent(in) :: D\n");
  fout(4, "real(kind=default), dimension(:), intent(in) :: P\n");
  fout(4, "real(kind=default) :: %s\n", istr);
  fout(4, "real(kind=default) :: TOTNUM,TOTDEN,RNUM\n");

  linepos=ftell(f90file);
  fout(70, "\n");
  f90writer("TOTNUM",totnum);
  f90writer("TOTDEN",totdenum);
  f90writer("RNUM",rnum);

  tmplongs = (longstrptr)getmem(sizeof(longstr));
  tmplongs->len = 0;
  f90addstring(tmplongs,"RNUM*(TOTNUM/TOTDEN)");

  for (i = 1; i <= dendescript->tot_den; i++) {
    numm = dendescript->denarr[i-1].zerowidth ?
      dendescript->denarr[i-1].dennum + nden_w :
      dendescript->denarr[i-1].dennum;
    if (dendescript->denarr[i-1].deg == 1)
      f90addstring(tmplongs,scat("*D(%d,1)",numm));
    else
      f90addstring(tmplongs,scat("*D(%d,2)",numm));
  }
  f90write_str(istr,tmplongs);

  tmplongs->len=0;
  f90addstring(tmplongs,istr);

  for (k = 1; k <= nden_w; k++) {
    addpr = TRUE;
    for (i = 1; i <= dendescript->tot_den; i++)
      if (!dendescript->denarr[i-1].zerowidth &&
	  k == dendescript->denarr[i-1].dennum)  addpr = FALSE;
    if (addpr)  f90addstring(tmplongs,scat("*D(%u,0)",k));
  }
  if (tmplongs->len > strlen(istr)) f90write_str(istr,tmplongs);
  free(tmplongs);

  fout(2, "end function sqme%s\n\n", funstr(diagrcount));

  linepos1=ftell(f90file);
  fseek(f90file, linepos, SEEK_SET);
  if(f90degnamecount>0) fout(4, "real(kind=default) :: S(%d);", f90degnamecount);
  if(f90tmpNameMax>0)   fout(4, "real(kind=default) :: tmp(%d)", f90tmpNameMax);
  fseek(f90file, linepos1, SEEK_SET);
  
  release_(&bh);
  f90cleardegnames();
}


/* ==================================================================== */
/* Write one subprocess module */
/* The common extension and an integer for the subprocess are appended */

static void module_sub_name(int ind, int i) {
  fout(ind, "module %s_%s\n", globalname, funstr(i));
}

/* Open subprocess file and write header */
static void module_sub_open(int nsub, int ifile) {
  f90file = fopen(scat("%sresults%c%s_%s.f90",
		       pathtouser, f_slash, globalname, funstr(ifile)),"w");   
  f90_header(0);                    /* Insert header indicating version */

  /* Process and subprocess as comments in FORTRAN output */
  process_identifier(0); nl(); 
/*    fout(0, "! Subprocess %d: ", nsub);  f_process_str(inftmp->p_name); nl(); */
/*    fout(0, "!\n\n"); */

  /* Begin module */
  module_sub_name(0, ifile); nl();
  use_precision(2);

  fout(2, "use %s_const, only: A, C\n", globalname);
  fout(2, "use %s_denom, only: calculate_denominators\n", globalname); nl();

  module_implicit_declaration(2); nl();

  /* Exported */
  fout(2, "public :: set_const_%s, sqme_%s\n", funstr(ifile), funstr(ifile));
  nl();
  
  contains(0); nl();
}

/* Write public collection routines and close subprocess file */
static void module_sub_close(int ifile, int diagr_first) {
  int i;

  nl();

  /* Fill the C array */
  fout(2, "subroutine set_const_%s\n", funstr(ifile));
  for(i = diagr_first; i <= diagrcount; i++) {
    fout(4, "call set_const%s\n", funstr(i));
  }
  fout(2, "end subroutine set_const_%s\n\n", funstr(ifile));

  /* Calculation of the squared ME */
  fout(2, "function sqme_%s(P) result(s)\n", funstr(ifile));
  fout(4, "real(kind=default), dimension(:), intent(in) :: P\n");
  fout(4, "real(kind=default) :: s\n");
  fout(4, "real(kind=default), dimension(%d,0:2) :: D\n", nden_0+nden_w);
  fout(4, "call calculate_denominators(D,P)\n");
  fout(4, "s = 0.\n");
  for(i = diagr_first; i <= diagrcount; i++) {
    fout(4, "s = s + sqme%d(D,P)\n", i);
  }
  fout(2, "end function sqme_%s\n", funstr(ifile));
  nl();

  f_end(0); module_sub_name(1, ifile); nl();
  fclose(f90file);
}

/* Write a subprocess; distribute it among several modules if necessary */
static void write_subprocess(int nsub, int* sub_file0, boolean* breaker) {
  denlist   den_w, den_0, den_tmp;
  varptr    den, mass, width;
  catrec    cr;
  marktp    bh, denmark ;
  word      i, n;
  char      istr[12];
  deninforec   dendescript;
  FILE * fd;                /* file of (deninforec)  */
  char fd_name[STRSIZ];
  int file_pos, file_pos1;

  word diagr_first;

  int diag_const1, diag_const2;
  int sub_file;

  nsub1 = nsub;
  sub_file = *sub_file0;

  mark_(&bh);
 
  /* ----------------------------------------------------------------- */
  /* Read from 'den' info file */
  sprintf(fd_name,"%stmp%cden.inf",pathtouser,f_slash);
  fd=fopen(fd_name,"wb"); 
   
  /* These numbers determine the size of the D array */
  nden_w = 0;
  nden_0 = 0;

  den_w = NULL;
  mass  = NULL;
  width = NULL;
  den_0 = NULL;

  /* from f90_lstr.c */
  f90constcount = 0;
  f90initConsts();
  f90initdegnames();
  f90tmpNameMax=0;

  /* Read denominator information from file */
  fseek(catalog,0,SEEK_SET);
  while (FREAD1(cr,catalog)) {
    if (cr.nsub_ == nsub) {
      dendescript.cr_pos = ftell(catalog) - sizeof(cr);
      fseek(archiv,cr.denompos,SEEK_SET);
      FREAD1(dendescript.tot_den,archiv); 
      for (i = 1; i <= dendescript.tot_den; i++) {
	FREAD1(dendescript.denarr[i-1].deg,archiv);
	mark_(&denmark);
	readpolynom(&width);
	readpolynom(&mass);
	readpolynom(&den);
	if (width == NULL) {                          /* Zero width */
	  dendescript.denarr[i-1].zerowidth = TRUE;
	  den_tmp = den_0;
	  n = 0;
	  while (den_tmp != NULL)
	    if (equalexpr(den,den_tmp->den)) {
	      den_tmp = NULL;  release_(&denmark);  }
	    else {  den_tmp = den_tmp->next; ++(n); }
	  dendescript.denarr[i-1].dennum = nden_0 - n;
	  if (n >= nden_0) {
	    den_tmp = (denlist)getmem_((word)sizeof(denlistrec));
	    den_tmp->next = den_0;
	    den_tmp->den = den;
	    den_tmp->mass= mass;
	    den_tmp->width=width; /*  NULL*/
	    den_0 = den_tmp;
	    ++(nden_0);
	    dendescript.denarr[i-1].dennum = nden_0;
	  }
	}
	else {                                       /* Nonzero width */
	  dendescript.denarr[i-1].zerowidth = FALSE;
	  den_tmp = den_w;
	  n = 0;
	  while (den_tmp != NULL)
	    if (equalexpr(den,den_tmp->den) &&
		equalexpr(width,den_tmp->width)) {
	      den_tmp = NULL;
	      release_(&denmark);
	    }
	    else { den_tmp = den_tmp->next; ++(n); }
	  dendescript.denarr[i-1].dennum = nden_w  - n;
	  if (n >= nden_w) {
	    den_tmp = (denlist)getmem_((word)sizeof(denlistrec));
	    den_tmp->next = den_w;
	    den_tmp->den = den;
	    den_tmp->mass=mass;
	    den_tmp->width=width;
	    den_w = den_tmp;
	    ++(nden_w);
	    dendescript.denarr[i-1].dennum = nden_w;
	  }
	}
      }
      FWRITE1(dendescript,fd);
    }  /* if CR.nsub_ =nsub */
  } 
  fclose(fd);
  f90putnames();


  /* -------------------------------------------------------------------- */
  /* Write denominator calculation into a separate module */

  f90file = fopen(scat("%sresults%c%s_denom.f90",
		       pathtouser, f_slash, globalname),"w");   
  f90_header(0);                    /* Insert header indicating version */

  /* Process and subprocess as comments in FORTRAN output */
  process_identifier(0); nl(); 
/*    fout(0, "! Subprocess %d: ", nsub);  f_process_str(inftmp->p_name); nl(); */
/*    fout(0, "!\n\n"); */

  /* Begin module */
  fout(0, "module %s_denom\n", globalname);
  use_precision(2);
  fout(2, "use %s_const, only: gwidth, rwidth, A\n", globalname); nl();

  module_implicit_declaration(2); nl();

  /* Exported */
  fout(2, "public :: calculate_denominators\n");

  nl(); contains(0); nl();

  fout(2, "subroutine calculate_denominators(D,P)\n");
  fout(4, "real(kind=default), dimension(:,0:), intent(out) :: D\n");
  fout(4, "real(kind=default), dimension(:), intent(in) :: P\n");
  fout(4, "real(kind=default), dimension(%d) :: DMASS, DWIDTH\n", nden_w);

  /* Write constants (C,S arrays) */
  file_pos = ftell(f90file);
  fout(70, "\n");
  fout(4, "integer :: i\n");
  f90write_const();

  /* Now the dimensions are known, rewind and write them in header */
  file_pos1 = ftell(f90file);
  fseek(f90file, file_pos, SEEK_SET);
  fout(4, "real(kind=default) :: C(%d)", MAX(1,f90constcount)); 
  if(f90degnamecount>0) fout(0,", S(%d)", f90degnamecount);
  if(f90tmpNameMax>0)   fout(0,", tmp(%d)", f90tmpNameMax); 
  fseek(f90file, file_pos1, SEEK_SET);
  
  revers((pointer*)&den_w);
  den_tmp=den_w;
   
  /* Expressions for denominator masses and widths */
  i=1;
  while (den_w != NULL) {
    sbld(istr,"DMASS(%u)",i);   f90writer(istr,den_w->mass);
    sbld(istr,"DWIDTH(%u)",i);  f90writer(istr,den_w->width);
    den_w = den_w->next; 
    ++(i);
  }  

  /* The D array */
  i = 1; den_w=den_tmp;   
  while (den_w != NULL) {
    sbld(istr,"D(%u,1)",i);
    f90writer(istr,den_w->den);
    den_w = den_w->next;
    ++(i);
  }
  
  revers((pointer*)&den_0);
  while (den_0 != NULL) {
    sbld(istr,"D(%u,1)",i);
    f90writer(istr,den_0->den);
    den_0 = den_0->next;
    ++(i);
  }
  release_(&bh);
  f90cleardegnames();

  /* Expressions that depend on the width convention */
  if (nden_w > 0) {
    fout(4, "do i=1,%d\n", nden_w);
    fout(7, "if (rwidth) then\n");
    fout(10, 
	 "D(i,2)=1/(D(i,1)**2+((DMASS(i)-D(i,1)/DMASS(i))*DWIDTH(i))**2)\n");
    fout(7, "else\n");
    fout(10, "D(i,2)=1/(D(i,1)**2+(DMASS(i)*DWIDTH(i))**2)\n");
    fout(7, "end if\n");
    fout(7, "if (gwidth) then\n");
    fout(10, "D(i,0)=D(i,2)*D(i,1)**2\n"); 
    fout(7, "else\n");
    fout(10, "D(i,0)=1 \n");
    fout(7, "end if\n");
    fout(7, "D(i,1)=D(i,2)*D(i,1)\n");
    fout(4, "end do\n");
  }

  /* Final stuff */
  if (nden_0 > 0) {
    fout(4, "do i=%d,%d\n", 1+nden_w, nden_w + nden_0);
    fout(7, "D(i,1)=1/D(i,1)\n");
    fout(7, "D(i,2)=D(i,1)**2\n");
    fout(4, "end do\n");
  }

  fout(2, "end subroutine calculate_denominators\n");
  nl();
  fout(0, "end module %s_denom\n", globalname);
  fclose(f90file);


  /* -------------------------------------------------------------------- */
  /* Write the Feynman diagram codes into a subprocess module */
  /* If the limit F90_MAXDIAG is reached, start a new module */

  diagr_first = diagrcount+1;
  f90constcount = 0;

  module_sub_open(nsub, sub_file);

  fd=fopen(fd_name,"rb");
  *breaker = FALSE;
  while(FREAD1(dendescript,fd) == 1) {
    if (escpressed()) {
      *breaker = TRUE;
      break;
    }

    if (diagrcount - diagr_first >= F90_MAXDIAG-1) {
      module_sub_close(sub_file++, diagr_first);
      diagr_first = diagrcount+1;
      module_sub_open(nsub, sub_file);
    }

    write_one_diagram (&diag_const1, &diag_const2, &dendescript);

    const1[diagrcount]=diag_const1;
    const2[diagrcount]=diag_const2;
    write_statistics();

  }
  fclose(fd);
  unlink(fd_name);

  /* Finish the subprocess module */
  module_sub_close(sub_file++, diagr_first);
  *sub_file0 = sub_file;

}

/* ==================================================================== */
/* Loop over archive and write subprocesses to file */

static void write_process(void) {
  boolean breaker;

  int*  sub_file0;

  sub_file0 = (int*) malloc(subproc_sq*sizeof(int));

  archiv=fopen(ARCHIV_NAME,"rb");             /* Read from archive files */
  catalog=fopen(CATALOG_NAME,"rb");
  fseek(catalog,0,SEEK_END);
  ndiagrtot = ftell(catalog)/sizeof(catrec);

  sub_file0[0] = 0;
  diagrcount = 0;
  inftmp = inf;
  init_statistics();                          /* Setup display on screen */

  /* Loop */                                  /* Write subprocesses */
  for (nsub = 1; nsub<=subproc_sq; nsub++) {
    sub_file0[nsub] = sub_file0[nsub-1];
    if (inftmp->tot != 0) {                   /* skip deleted subprocesses */
      write_subprocess(nsub, &(sub_file0[nsub]), &breaker);
      if (breaker) goto clean_up;             /* ESC pressed: exit */
    }
    inftmp = inftmp->next;
  }

  nsub_tot = nsub-1;                          /* For later use (global) */

  write_module_x(sub_file0);                 /* Write the main module */

clean_up:
  clear_statistics();
  fclose(catalog);
  fclose(archiv);

  free((void*) sub_file0);
}

/************************************************************************/
/* Top level routine */

/* Arguments: quadruple precision=T or F, */
/*            flag(show diagrams in F90 code) */
/*            extension; appended to the module name 
	      [do we actually use that?] */

void fort90prg (boolean precision1,  
		boolean include_pictures1) { 
  int redres;
  int y0;

  memerror = zeroHeap;
  precision = precision1;
  include_pictures = include_pictures1;
  
  /* Read the module extension string from keyboard */
  y0 = where_y();
  goto_xy(1,y0); print("%s","Enter common extension (default: none): ");
  redres = str_redact(globalname, 1);
   switch (redres)  {
   case KB_ESC:    /*  Esc  */
     goto_xy(1,y0); clr_eol();
     return;
   }   /*  Case  */


  /* Initialization =============================== */

  mark_(&heapbeg);
  diagrq = fopen(DIAGRQ_NAME,"rb"); /* diagram info from DIAGRQ_NAME */
 
  calccutlevel();                   /* Number of free scalar products (?) */
  f90initvarnames();                /* originally from procvar.c */
  prepare_process_information();    /* process info from MENUQ_NAME */
  calc_nvars();                     /* number of parameters */

  /* Write F90 output files ======================= */

/*   write_module_kinds();             /\* Module: Real kinds *\/ */
/*   write_module_file_utils();        /\* Module: File utilities *\/ */
/*   write_module_parameters();        /\* Module: parameter set *\/ */

  write_process();                  /* Modules: sqme */

  write_module_const();             /* Module: constants */

  fclose(diagrq);
  release_(&heapbeg);
}
