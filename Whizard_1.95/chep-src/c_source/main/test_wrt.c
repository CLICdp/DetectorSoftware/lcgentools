#include "tptcmac.h"
#include "syst2.h"
#include "pvars.h"
#include "pre_read.h"
#include "parser.h"
#include "polynom.h"
#include "tensor.h"
#include "crt.h"

#include "test_wrt.h"

#define xmax 70
#define fle stdout

static int xpos=1;

static void  wrtln(void)
{
   fprintf(stderr,"\n");
   xpos = 1;
}


static void  wrt(char* s)
{word l;

   l = strlen(s);
   if (xpos + l <= xmax)
   {
      fprintf(stderr,"%s",s);
      xpos += l;
   }
   else
   {
      l = xmax - xpos;
      while (l > 0 && strchr("*+-)(^ ",s[l-1]) == NULL) --(l);
      if (l == 0)
         if (xpos == 1)
            l = xmax;
         else
         {  wrtln();
            wrt(s);
            return;
         }
      wrt(copy(s,1,l));
      wrtln();
      wrt(copy(s,l + 1,strlen(s) - l));
   }
}


void tracePrn(char * format,...)
{
  va_list args;
  char dump[STRSIZ] , *beg, *nn;

  va_start(args,format);   
  vsprintf(dump,format,args);
  va_end(args);

  beg=dump;   
  while(TRUE)
  { nn=strchr(beg,'\n');
    if(nn==NULL){wrt(beg);return;}
    nn[0]=0;
    wrt(beg);
    wrtln();
    beg=nn+1;
  }
}


void  writepoly(poly p)
{ char     txt[STRSIZ], numtxt[STRSIZ];
  int  i, deg;
  boolean  beg, first;
  int     wpos;
unsigned long		wpower;

   if (p == NULL)
   {  wrt("0");
      return;
   }
   beg = TRUE;

   while (p != NULL)
   {
      strcpy(txt,"");
      i = 1;
      first = TRUE;
      wpos = 1;
      wpower = p->tail.power[wpos-1];
      for (i = 1; i <= vardef->nvar; i++)
      {
         deg = (p->tail.power[vardef->vars[i-1].wordpos-1] /
                vardef->vars[i-1].zerodeg) %
                vardef->vars[i-1].maxdeg;

         if (deg > 0)
         {
            if (first)  first = FALSE;
            else        sbld(txt,"%s*",txt);
            sbld(txt,"%s%s",txt,vardef->vars[i-1].name);
            if (deg > 1)
            {
               sbld(numtxt,"%d",deg);
               sbld(txt,"%s**%s",txt,numtxt);
            }
         }
      }

      if (strlen(txt) != 0)
      {
         if (p->coef.num == 1 || p->coef.num == -1)
            strcpy(numtxt,"");
         else
         {
            sbld(numtxt,"%ld",
                 p->coef.num > 0 ? p->coef.num : -p->coef.num);
            sbld(numtxt,"%s*",numtxt);
         }
      }
      else
         sbld(numtxt,"%ld",
              p->coef.num > 0 ? p->coef.num : -p->coef.num);
      if (p->coef.num < 0)
         sbld(numtxt," - %s",numtxt);
      if (beg)
         beg = FALSE;
      else
         if (p->coef.num > 0)
            sbld(numtxt," + %s",numtxt);
      sbld(txt,"%s%s",numtxt,txt);
      wrt(txt);
      p = p->next;
   }
}   /*  WritePoly  */


void  writetens(poly p)
{char      txt[STRSIZ], num1[STRSIZ], num2[STRSIZ];
 integer   i,s,l;
 boolean   beg, first, bracond;

   if (p == NULL)
   {  wrt("0");
      return;
   }
/*	lbyte = 4 * tenslength; 
   if (levi) lbyte -= 4; 
*/
   beg = TRUE;
   while (p != NULL)
   {
      strcpy(txt,"");
      first = TRUE;
      for (i = 1; i <= maxIndex; i++)
      {
         s = p->tail.tens[i-1];
         if (s < i && s != 0)
         {
            if (first)
               first = FALSE;
            else
               sbld(txt,"%s*",txt); 
            sbld(num1,"%d",i); 
            if (s > 0) 
            { 
               sbld(num2,"%d",s); 
               sbld(txt,"%sg[m%s,m%s]",txt,num1,num2); 
            } 
            else 
            { 
               sbld(num2,"%d",-s); 
               sbld(txt,"%sp%s[m%s]",txt,num2,num1); 
            } 
         } 
      }
      if (levi && p->tail.tens[maxIndex] != 0) 
      { 
        if (first)
           first = FALSE; 
        else
           sbld(txt,"%s*",txt); 
        sbld(txt,"%siEps(",txt); 
        for (l = 1; l <= 4; l++) 
        { 
           s = p->tail.tens[maxIndex+ l-1]; 
           if (s < 0) 
           { 
              sbld(num2,"%d",-s); 
              sbld(txt,"%sp%s",txt,num2); 
           }
           else 
           { 
              sbld(num2,"%d",s); 
              sbld(txt,"%sm%s",txt,num2); 
           } 
           if (l < 4) sbld(txt,"%s,",txt); 
        } 
        sbld(txt,"%s)",txt); 
      } 
      bracond = (p->coef.ptr->next != NULL); 
      if (beg) 
      { 
         beg = FALSE; 
         if (bracond) 
         {
            wrt("{ "); 
            writepoly(p->coef.ptr); 
            wrt(" }"); 
         } 
         else
            writepoly(p->coef.ptr); 
      } 
      else   /*  Beg=False */ 
      { 
         if (bracond) 
         { 
            wrt(" + { "); 
            writepoly(p->coef.ptr); 
            wrt(" }"); 
         }
         else 
         { 
            if (p->coef.ptr->coef.num > 0) wrt(" + "); 
            writepoly(p->coef.ptr); 
         } 
      } 
      if (strlen(txt) != 0) wrt("*"); 
      wrt(txt); 
      p = p->next; 
   } 
} 


void  writespinor(poly p)
{char      txt[STRSIZ], num1[STRSIZ]; 
 integer   i,s,ls; 
 boolean   beg, bracond; 

   if (p == NULL)
   {  wrt("0");
      return;
   } 

   beg = TRUE; 
   while (p != NULL) 
   { 
      ls = p->tail.spin.l; 
      if (p->tail.spin.g5 != 0) 
         if (ls == 0)
            strcpy(txt,"G5");
         else 
            strcpy(txt,"G5*G("); 
      else 
         if (ls != 0)
            strcpy(txt,"G("); 
         else
            strcpy(txt,""); 
      for (i = 1; i <= ls; i++) 
      { 
         s = p->tail.spin.g[i-1]; 
         if (s < 0) 
         { 
            sbld(num1,"%d",-s); 
            sbld(txt,"%sp",txt);
         } 
         else 
         { 
            sbld(num1,"%d",s); 
            sbld(txt,"%sm",txt); 
         } 
         sbld(txt,"%s%s",txt,num1); 
         if (i != ls)
            sbld(txt,"%s,",txt); 
      } 
      if (ls != 0)
         sbld(txt,"%s)",txt); 
      bracond = (p->coef.ptr->next != NULL); 
      if (beg) 
      {
         beg = FALSE; 
         if (bracond) 
         { 
            wrt("{ "); 
            writepoly(p->coef.ptr); 
            wrt(" }"); 
         } 
         else
            writepoly(p->coef.ptr); 
      } 
      else   /*  Beg=False */ 
      { 
         if (bracond) 
         { 
            wrt(" + { ");
            writepoly(p->coef.ptr); 
            wrt(" }"); 
         } 
         else
         {
            if (p->coef.ptr->coef.num > 0) wrt(" + ");
            writepoly(p->coef.ptr);
         }
      }
      if (strlen(txt) != 0) wrt("*");
      wrt(txt);
      p = p->next;
   }
}

void  writeexpression(poly  m)
{char s[5];
 int n;
 poly p;

           if (m->coef.num <= polytp)  writepoly(m->next);
     else  if (m->coef.num == rationtp)
     {
         wrt("(");  writepoly(m->next->coef.ptr);
         wrt(")/(");writepoly(m->next->next);
         wrt(")");
      }
      else if (m->coef.num  == tenstp) writetens(m->next);
      else if (m->coef.num == spintp)  writespinor(m->next);
      else if (m->coef.num == vectortp)
      {
	 p = m->next;
         if (p == NULL) wrt("0");
         else
         do
	 {  n=-p->tail.tens[0];
	    sbld(s,"p%d*(",n);
            wrt(s);
            writepoly(p->coef.ptr);
            wrt(")");
            p = p->next;
            if (p != NULL) wrt("+");
         }  while (p != NULL);
      }
      else
      {
         n = m->next->tail.tens[1-1];
         if (n == 0) n = 1;
	 sbld(s,"l%d",n);
         wrt(s);
      }
}
