#include <limits.h>
#include "tptcmac.h"
#include "physics.h"
#include "polynom.h"
#include "sos.h"
#include "syst2.h"

#include "pvars.h"

#ifdef STRACE
#include "test_wrt.h"
#endif
 polyvars *vardef;



void  unite_vardef(polyvars *vardef_s,polyvars *vardef)
{byte  n, nn;
 char  s[STRSIZ];

   for (nn = 1; nn <= vardef->nvar; nn++)
   {
      strcpy(s,vardef->vars[nn-1].name);
      n = 1;
      while (n <= vardef_s->nvar &&
             strcmp(s,vardef_s->vars[n-1].name) != 0)
         ++(n);
      if (n <= vardef_s->nvar)
         vardef_s->vars[n-1].maxdeg =
            MAX(vardef_s->vars[n-1].maxdeg,vardef->vars[nn-1].maxdeg);
      else
      {
         vardef_s->nvar = n;
         strcpy(vardef_s->vars[n-1].name,s);
         vardef_s->vars[n-1].maxdeg = vardef->vars[nn-1].maxdeg;
      }
   }
}


void  addvar(char* varname,byte deg)
{byte    n;

   n = 1;
   while (n <= vardef->nvar &&
          strcmp(varname,vardef->vars[n-1].name) != 0)
      ++(n);
   if (n <= vardef->nvar)
      vardef->vars[n-1].maxdeg += deg;
   else
   {
      vardef->nvar = n;
      strcpy(vardef->vars[n-1].name,varname);
      vardef->vars[n-1].maxdeg = deg + 1;
   }
}


byte  calcvarpos(char* s)
{byte      bt;
 varlist   varptr;

   if (strcmp(s,"0") == 0) return 0;
   bt = 1;
   varptr = modelvars;
   while (strcmp(varptr->varname,s) != 0)
   {
      ++(bt);
      varptr = varptr->next;
      if (varptr == NULL)   /* Unknoun ident  */
         save_sos(14);
   }
	return bt + maxsp+1;   /*  16 first positions for pi.pj) */
}


void  closevars(void)
{byte     i, j;
 unsigned long  z;
 byte     wp, numtmp, p1, p2;
 byte     maxdegtmp;
 char     nametmp[7];

   for (i = 1; i <= vardef->nvar / 2; i++)   /*  rotation  */
   {
      strcpy(nametmp,vardef->vars[i-1].name);
      maxdegtmp = vardef->vars[i-1].maxdeg;
      j = vardef->nvar + 1 - i;
      strcpy(vardef->vars[i-1].name,vardef->vars[j-1].name);
      vardef->vars[i-1].maxdeg = vardef->vars[j-1].maxdeg;
      strcpy(vardef->vars[j-1].name,nametmp);
      vardef->vars[j-1].maxdeg = maxdegtmp;
   }

   for (i = 1; i <= vardef->nvar; i++)   /*  numeration  */
      if (cpos('.',vardef->vars[i-1].name) != 0)
      {
         p1 = ord(vardef->vars[i-1].name[1]) - ord('0');
         p2 = ord(vardef->vars[i-1].name[4]) - ord('0');
			vardef->vars[i-1].num = maxsp+1 - p1 - ((p2 - 1) * (p2 - 2)) / 2;
      }
      else
         vardef->vars[i-1].num = calcvarpos(vardef->vars[i-1].name);

   if (vardef->nvar > 1)   /*  Sorting  */
   {
      i = 1;
      while (i < vardef->nvar)
      if (vardef->vars[i-1].num < vardef->vars[i + 1-1].num)
         ++(i);
      else
      {   /* exchange */
         j = i + 1;
         strcpy(nametmp,vardef->vars[i-1].name);
         maxdegtmp = vardef->vars[i-1].maxdeg;
         numtmp = vardef->vars[i-1].num;

         strcpy(vardef->vars[i-1].name,vardef->vars[j-1].name);
         vardef->vars[i-1].maxdeg = vardef->vars[j-1].maxdeg;
         vardef->vars[i-1].num = vardef->vars[j-1].num;
         strcpy(vardef->vars[j-1].name,nametmp);
         vardef->vars[j-1].maxdeg = maxdegtmp;
         vardef->vars[j-1].num = numtmp;

         if (i == 1)
            ++(i);
         else
            --(i);
      }
   }

   monomLength = 1;
   z = 1;
   for (i = 1; i <= vardef->nvar; i++)
	{

		if (  z >= ( ULONG_MAX / vardef->vars[i-1].maxdeg) )
      {
         ++(monomLength);
         z = vardef->vars[i-1].maxdeg;
		}
		else   z *= vardef->vars[i-1].maxdeg;

      vardef->vars[i-1].wordpos = monomLength;
   }

   z = 1;
   wp = monomLength;
   for (i = vardef->nvar; i >= 1; i--)
   {
      if (vardef->vars[i-1].wordpos == wp)
      {
         vardef->vars[i-1].zerodeg = z;
         z *= vardef->vars[i-1].maxdeg;
      }
      else
      {
         vardef->vars[i-1].zerodeg = 1;
         z = vardef->vars[i-1].maxdeg;
         --(wp);
      }
   }
	garbage = NULL;
#ifdef STRACE
	tracePrn("vars position \n");
	for (i=0;i< vardef->nvar;i++)
	{  tracePrn(" name= %s pos= %d maxdeg=  %lu zerodeg= %lu\n",
		vardef->vars[i].name,vardef->vars[i].wordpos,vardef->vars[i].maxdeg,
		vardef->vars[i].zerodeg);
	}
	tracePrn("monomLength=  %d\n", monomLength);
#endif

}

