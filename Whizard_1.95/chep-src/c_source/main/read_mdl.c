#include <math.h>
#include "tptcmac.h"
#include "syst2.h"
#include "global.h"
#include "physics.h"
#include "reader3.h"
#include "parser.h"
#include "crt.h"
#include "files.h"
#include "sos.h"
#include "file_scr.h"
#include "crt_util.h"
#include "pre_read.h"
#include "pvars.h"
#define minvarmem  ((word)sizeof(struct varrec)  + 1 - STRSIZ)
#define minlagrmem ((word)sizeof(struct algvert) + 1 - STRSIZ)
#include "os.h"


#include "read_mdl.h"


static int lastModel=0;
static char Sqrt2FuncTxt[10]="Sqrt(2)";
static char *  tabName;
static int nLine;

static void errorMessage( char * fieldName, char *  errComment)
{

#define MAXERRMSG     17
static char errMsgArr[MAXERRMSG][60] =
  {"",                                 /* ok */
   "Record absent",
	"Bracket expected",
	"Syntax error",
   "Unexpected character",
   "Operation expected",
	"Unbalance bracket",
   "Too large number",
   "Too long identifier",
   "Type mismatch",
   "Index uncompatibility",
   "Too many identifiers",
   "Range check error",
   "Division by zero",
   "Negative argument of square root",
   "Unexpected operation",
   "Undefined identifier"
	};

	if (rderrcode > MAXERRMSG) rderrcode = 3;

	if (strcmp(errComment,"*") == 0)
	sprintf(errorText,"Error in table '%s' line %d field '%s'$position %u: %s$",
	  tabName,nLine,fieldName,rderrpos,errMsgArr[rderrcode] );
	else
	sprintf(errorText,"Error in table '%s' line %d field '%s' $ %s$",
		  tabName,nLine,fieldName,errComment);
  messanykey(2,10,errorText);
#undef MAXERRMSG
}


static int tabCharPos(char *  str, int  n)
{
 int k=0;
 int nn=0;
 if (n==0) return 0;
 while (str[k] != 0)
 { if (str[k]=='|')
	{ nn++;
	  if (nn==n) return (k+1);
	}
	k++;
 }
 return k;
}

static boolean  isVarName(char*  s)
{
int i=1;
  if ( !isalpha(s[0]) ) return FALSE;

  while  (s[i] !=0)
  { if (!isalnum(s[i])||s[i]=='_') return FALSE;  /* WK: allow underscore */
	 i++;
  }
  if(s[0]=='p'||s[0]=='m'||s[0]=='M')
  {  i=1;
     while  (isdigit(s[i])) i++;
     if (s[i]==0) return FALSE;
  }    
  
  return TRUE;
}

static boolean  isOriginName(char* s)
{
  varlist mv;
  char newName[10], oldName[10];
  int i=0;
  strcpy(newName,s);
  while (newName[i] !=0) {newName[i]=toupper(newName[i]);i++;}

  mv=modelvars;
  while (mv != NULL)
  {
	 strcpy(oldName,mv->varname);
	 i=0;
	 while( (newName[i] !=0)&& ( newName[i] ==toupper(mv->varname[i])) )i++;
	 if ( newName[i] ==toupper(mv->varname[i]))  return FALSE;
	 mv=mv->next;
	 }
	 return TRUE;
}



static void addImagery1(void)
{
	varlist     mvars,mvars_;
	mvars=(varlist)getmem(sizeof(*mvars));
	strcpy(mvars->varname,"i");
	mvars->func = NULL;
	mvars->varvalue = 0.0;
	mvars->comment=NULL;
	if (strcmp(modelvars->varname,"Sqrt2")==0)
	{ mvars->next=modelvars;

	  modelvars=mvars;
	}
	else
	{ mvars_=modelvars;
	  while (strcmp( (mvars_->next)->varname,"Sqrt2") !=0 ) mvars_=mvars_->next;
	  mvars->next=mvars_->next;
	  mvars_->next=mvars;
	}
	nmodelvar++;
}



static boolean  readvars(boolean  check)
{
 char        varn[60];
 char        numtxt[60];
 char        name_[60];
 char *       ss;
 int         err,nParam,commentShift,funcShift;
 varlist     mvars, q, ggptr;
 double      * r;
 linelist  ln;
 marktp  heapbeg;

   ggptr = NULL;
   gg_exist = FALSE;
	ln=vars_tab.strings;
	tabName=vars_tab.headln;


	commentShift=tabCharPos(vars_tab.format,2);

   modelvars = NULL;
   nLine=1;
   while (ln  != NULL)
	{  ss=ln->line;
		if ( ( check )&&(nLine >200) )
		{  errorMessage("Name","too many parameters");
			goto errExi2;
		}


	  mvars=(varlist)getmem(sizeof(*mvars)); /*minvarmem*/

	  sscanf(ss,"%[^|]%*c%[^|]", varn,numtxt);
		trim(varn);
		if (check && (! isVarName(varn)) )
		{ errorMessage("Name",scat("incorrect name '%s'",varn));
		  goto errExi1;
 		}


		if (check &&  (! isOriginName(varn)) )
		{  errorMessage("Name",scat("duplicate name '%s'",varn));
			goto errExi1;
		}


		strcpy(mvars->varname,varn);
		trim(numtxt);
		mvars->varvalue=  realVal(numtxt,&err);
		if ( ( check )&&(err != 0) )
		{  errorMessage("Value",scat("wrong number '%s'",numtxt));
			goto errExi1;
		}

		mvars->comment=ln->line+commentShift;
		mvars->func= NULL;
		if (strcmp(mvars->varname,strongconst) == 0)
		{  ggptr = mvars;
			gg_exist = TRUE;
		}
		else
		{  mvars->next = modelvars;
			modelvars = mvars;
		}
		ln=ln->next;
		nLine++;
   }

	mvars=(varlist)getmem(sizeof(*mvars));
   strcpy(mvars->varname,"Sqrt2");
	mvars->func = Sqrt2FuncTxt;
	mvars->varvalue = sqrt(2.0);
	mvars->comment=NULL;
   mvars->next = modelvars;
	modelvars = mvars;

	nParam=nLine;
	nLine = 1;
	ln=func_tab.strings;
	tabName=func_tab.headln;
	commentShift=tabCharPos(func_tab.format,2);
	funcShift=tabCharPos(func_tab.format,1);


	while  (ln  != NULL )
	{  ss=ln->line;

		if ( ( check )&&((nLine+nParam) >200) )
		{  errorMessage("Name","too many variabls");
			goto errExi2;
		}

		sscanf(ss,"%[^|]",name_);
		trim(name_);
		if (! isVarName(name_))
		{  errorMessage("Name",scat("incorrect name '%s'",name_));
		  goto errExi2;
		}

		if ( check && (! isOriginName(name_)) )
		{    errorMessage("Name",scat("duplicate name '%s'",name_));
			goto errExi2;
		}


		mvars=(varlist)getmem( sizeof(*mvars) );
		strcpy(mvars->varname,name_);
      mvars->comment=ln->line+commentShift;
		mvars->func=ln->line+funcShift;
		mark_(&heapbeg);
		r = (double *)readexprassion(mvars->func,bact_3,uact_3,rd_3);
		if (check &&  (rderrcode != 0) )
		{
			errorMessage("Expression","*");
			release_(&heapbeg);
			goto errExi1;
		}

		mvars->varvalue = r == NULL ? 0 : *r;
		release_(&heapbeg);
		mvars->next = modelvars;
		modelvars = mvars;
		ln=ln->next;
		nLine++;
	}
   revers((pointer*)&modelvars);

   if (ggptr != NULL)
   {
      ggptr->next = modelvars;
      modelvars = ggptr;
   }

   q = modelvars;
   nmodelvar = 0;
   while(q != NULL)
   {
      ++(nmodelvar);
      q = q->next;
	}

	return TRUE;

errExi1:
	 free (mvars);
errExi2:
	 if (gg_exist) free(ggptr);
	 return FALSE;

}



static void  clearvars(void)
{varlist     p;

   while (modelvars != NULL)
   {
      p = modelvars;
      modelvars = modelvars->next;
		free(p);
   }
}


static void  findvar(char* txt,double* num,int* err)
{varlist     varn;
 byte        lname, i;

   trim(txt);
   lname = strlen(txt);
   varn = modelvars;
   while (varn != NULL)
   {
      i = 0;
      while (i <= lname && varn->varname[i + 1-1] == txt[i + 1-1]) ++(i);
      if (i > lname)
      {
         *num = varn->varvalue;
         *err = 0;
         return;
      }
      varn = varn->next;
   }
   *err = -1;
}


static int  ghostaddition(void)
{int  i, nPrim;
   nPrim=nparticles;
   if ( (prtclbase[nPrim-1].spin == 2) && (prtclbase[nPrim-1].cdim !=1) )
   { 
      nPrim ++;   
      prtclbase[nPrim -1] = prtclbase[nparticles-1];      
      prtclbase[nparticles -1].hlp = 't';
      prtclbase[nparticles -1].spin=4;
       nparticles ++;      
   }
   
   if (gaugep(nPrim))
   {
      nparticles ++;
      prtclbase[nparticles -1] = prtclbase[nPrim-1];
      prtclbase[nparticles -1].hlp = 'c';
      nparticles ++;
      prtclbase[nparticles -1] = prtclbase[nPrim-1];
      prtclbase[nparticles -1].hlp = 'C';
      
      if (strcmp(prtclbase[nPrim-1].massidnt,"0") != 0)
      {
         nparticles ++;
         prtclbase[nparticles -1] = prtclbase[nPrim-1];
         prtclbase[nparticles -1].hlp = 'f';
      }
      for (i=nPrim+1; i<=nparticles;i++)  prtclbase[i -1].spin=0;
   }
   return nPrim;
}


static void  clearlgrgn(void)
{algvertptr  l1;

   while (lgrgn != NULL)
   {
      l1 = lgrgn;
      lgrgn = lgrgn->next;
      free(l1);
   }
}


static void  cleardecaylist(void)
{decaylink   v1, v2;
 byte        j;

   for (j = 1; j <= nparticles; j++)
   {
      v1 = prtclbase[j-1].top;
      prtclbase[j-1].top = NULL;
      while (v1 != NULL)
      {
         v2 = v1;
         v1 = v1->link;
         free(v2);
      }
   }
}


static boolean isPrtclName(char*  p)
{
  if (strlen(p)>3) return FALSE;
  if ((strlen(p)>2) && (p[0] != '~')) return FALSE;
  return TRUE;
}


static boolean  readparticles(boolean  check )
{char *       ss;
 char        fullname[60];
 char        massname[60], imassname[60];
 char        p1[60],p2[60];
 byte        i, j;
 char        s[60];
 char        c[60];
 char        chlp[40];
 integer     itmp;
 int         errcode;
 int         np1,np2;
 linelist    ln;


   ln=prtcls_tab.strings ;
   tabName=prtcls_tab.headln;

   nparticles = 0;
   nLine=1;
   while (ln != NULL)
   {  ss=ln->line;
      if (nparticles == 110)
      {  errorMessage(" P ","too many particles");
	 return FALSE;
      }

      sscanf(ss,"%[^|]%*c%[^|]%*c%[^|]%*c%[^|]%*c%[^|]%*c%[^|]%*c%[^|]%*c%[^|]",
				 fullname,p1,p2,s,massname,imassname,c,chlp);
      trim(p1); trim(p2);

      for ( i=0;i<=1;i++)
      {
         static char fldName[2][5]={" P "," aP"};
         char * pName[2];
         pName[0]=p1;
         pName[1]=p2;

	 if (check && (! isPrtclName(pName[i])))
         {  errorMessage(fldName[i],scat("incorrect particle name '%s'",pName[i])) ;
            return FALSE;
         }

         if (check )
         {
            locateinbase(pName[i],&j);
            if (j != 0)
            {
               errorMessage(fldName[i],scat("duplicate particle name '%s'",pName[i])) ;
               return FALSE;
            }
         }
      }
      nparticles++;
      strcpy(prtclbase[nparticles-1].name,p1);
      trim(s);
      itmp=intVal(s,&errcode);
      if(check)
      {
         if (errcode!=0)
         {  errorMessage("2*spin","number expected");
            return FALSE ;
         }
         if ((itmp!=0)&&(itmp!=1)&&(itmp!=2))
         {  errorMessage("2*spin","value out of range");
            return FALSE;
         }
      }
      prtclbase[nparticles-1].spin=itmp;
      trim(massname);
      if (strcmp(massname,"0") == 0)  prtclbase[nparticles-1].mass = 0.0;
      else
      {
         findvar(massname,&(prtclbase[nparticles-1].mass),&errcode);
         if (check && (errcode != 0))
         {
            errorMessage("mass",scat("unknown variable %s",massname));
            return FALSE;
         }
      }
      strcpy(prtclbase[nparticles-1].massidnt,massname);

      trim(imassname);
      strcpy(prtclbase[nparticles-1].imassidnt,imassname);
      if ((check) && (strcmp(imassname,"0") != 0))
      {  double r;
         findvar(imassname,&r,&errcode);
         if (errcode != 0)
         {  errorMessage("width",scat("unknown variable %s",imassname));
            return FALSE;
         }
      }
      trim(c);
      itmp=intVal(c,&errcode);
      if(check)
      {
         if (errcode!=0)
         {  errorMessage("color","number expected");
            return FALSE;
         }
         if (((itmp!=1)&&(itmp!=3)&&(itmp!=8))||((itmp==3)&&(strcmp(p1,p2)==0))  )
         {  errorMessage("color","value out of range");
            return FALSE;
         }
      }
      prtclbase[nparticles-1].cdim=itmp;
      trim(chlp);
      if (strcmp(chlp,"") == 0) strcpy(chlp," ");
      prtclbase[nparticles-1].hlp = toupper(chlp[0]);
      if(check)
      {  boolean ner;
         ner=TRUE;
         switch  (prtclbase[nparticles-1].hlp)
         {
            case ' ':break;
            case 'L':
            case 'R':
              if ((prtclbase[nparticles-1].spin !=1)
               ||((prtclbase[nparticles-1].massidnt[0])!='0')
               ||(strcmp(p1,p2)==0))  ner=FALSE;
              break;

            case '*':
              if (prtclbase[nparticles-1].massidnt[0]=='0')   ner=FALSE;
              break;
            case 'G':
              if (prtclbase[nparticles-1].spin!=2)   ner=FALSE;
              break;
            default: ner=FALSE;
         }
         if (!ner)
         {  errorMessage("aux","unexpeted character");
            return FALSE;
         }
      }
      np1 = ghostaddition();
      if (strcmp(p1,p2) == 0) prtclbase[np1-1].anti = np1;
      else
      {
        ++(nparticles);
        prtclbase[nparticles-1] = prtclbase[np1-1];
        strcpy(prtclbase[nparticles-1].name,p2);
        if (prtclbase[np1-1].cdim == 3) prtclbase[nparticles-1].cdim = -3;
        np2=ghostaddition();
        prtclbase[np1-1].anti = np2;
        prtclbase[np2-1].anti = np1;        
      }
      ln=ln->next;
      nLine++;
   }

   for (i = 1; i <= nparticles; i++)
   {  prtcl_base *with1 = &prtclbase[i-1];
      with1->top = NULL;
      if (strchr("fcCt",with1->hlp) != NULL)
      {  
         sbld(with1->name,"%s.%c",with1->name,with1->hlp);
                  
         j = prtclbase[ghostmother(i)-1].anti;

         switch (with1->hlp)
         {
            case 'c':                        
               with1->anti = prtclbase[i-1 - 1].anti +2;
               break;
            case 'C':
               with1->anti = prtclbase[i-1 - 2].anti +1;
               break;
            case 'f':
               with1->anti = prtclbase[i-1 - 3].anti +3;
               break;
            case 't':
               with1->anti = prtclbase[i-1 + 1].anti -1;               
         }
      }
   }
   return TRUE;
}

static boolean  testLgrgn(algvertptr lgrgn)
{
  preres  m;
  int n;
/*  goto_xy(1,20); print("%d           ",nLine); */
   vardef->nvar = 0;
  m = (preres) readexprassion(lgrgn->comcoef,bactF,uact,rd);
  if (rderrcode != 0)
  {  errorMessage("Factor","*");
    return FALSE;
  }

  if (m->tp >rationtp)
  {  errorMessage("Factor","scalar expected");
    return FALSE;
  }

  if (m->maxp>0)
  {  errorMessage("Factor",scat("moments p%d are not permitable here",m->maxp) );
    return FALSE;
  }

  for (n = 0; n < vardef->nvar; n++)
  {  int err;
    double val;
    findvar (vardef->vars[n].name   ,&val ,&err);
    if (err)
    {  errorMessage("Factor",scat("unknown variable '%s'", vardef->vars[n].name));
      return FALSE;
    }
  }
   vardef->nvar = 0;
  m = (preres) readexprassion(lgrgn->description,bact,uact,rd);
  if (rderrcode != 0)
  {  errorMessage("Lorentz part","*");
    return FALSE;
  }

  if (m->tp == rationtp)
  {  errorMessage("Lorentz part","division is not permited here");
    return FALSE;
  }

  if( (m->tp == spintp) && ( (prtclbase[lgrgn->fields[0]-1].spin != 1) && (prtclbase[lgrgn->fields[1]-1].spin !=1)
        &&(prtclbase[lgrgn->fields[2]-1].spin !=1) ) )
  {
    errorMessage("Lorentz part","Dirac's gamma matrix not expected");
    return FALSE;
  }

  if( (m->tp == tenstp) && ( (prtclbase[lgrgn->fields[0]-1].spin == 1) || (prtclbase[lgrgn->fields[1]-1].spin ==1)
        ||(prtclbase[lgrgn->fields[2]-1].spin ==1) ) )
  {
    errorMessage("Lorentz part",
    scat("structure as m2.m3 is not permited here.$",
    " use identity G(m2)*G(m3)+G(m3)*G(m2) = 2*m2.m3 ") );
    return FALSE;
  }



  if ((m->maxp == 4)&&(lgrgn->fields[3] == 0))
  {  errorMessage("Lorentz part","p4 are not permited here");
    return FALSE;
  }


  for (n = 0; n < vardef->nvar; n++)
  {  int err;
    double val;
    findvar (vardef->vars[n].name   ,&val ,&err);
    if (err)
    {  errorMessage("Lorentz part",
      scat("unknown variable '%s'",vardef->vars[n].name));
      return FALSE;
    }
  }

  for (n = 0; n <= 3; n++)
  {  int  ind1,ind2,np ;
     
     ind1=0;
     np  = lgrgn->fields[n];
     if ( np != 0 )   switch  (prtclbase[np-1].spin)
     { case 2: ind1=1; break;
       case 4: ind1=3;
     }     
     
     ind2=0;
     if ( inset(n+1, m->indlist) ) ind2 += 1;
     if ( inset(n+1+4, m->indlist) ) ind2 += 2;
     
     if (ind1 != ind2 )
     {  errorMessage("Lorentz part",scat("index 'm%d'  unbalanced",n+1));
        return FALSE;
     }
  }
  return TRUE;
}


static boolean  readlagrangian(boolean check)
{ algvertptr  lgrgn1,lgrgn2;
 byte        i, j, mm;
 char        p1[60], p2[60], p3[60], p4[60];
 char *       ss;
 char *      pPtr[4];
 int  factorShift,lorentzShift;
 arr4byte  f_copy;
 int mLine,    totcolor,color,spinorNumb;
 linelist ln;
 static char fName[4][5] = {"P1","P2","P3","P4"};
 addImagery1();
  lgrgn = NULL;
  nLine=1;
  ln=lgrng_tab.strings;
  factorShift=tabCharPos(lgrng_tab.format,4);
  lorentzShift=tabCharPos(lgrng_tab.format,5);
  tabName=lgrng_tab.headln;
  while (ln != NULL)
  {

    ss=ln->line;
    sscanf(ss,"%[^|]%*c%[^|]%*c%[^|]%*c%[^|]",
         p1,p2,p3,p4);
    pPtr[0]=p1; pPtr[1]=p2;pPtr[2]=p3;pPtr[3]=p4;

    lgrgn1=(algvertptr)getmem( sizeof(*lgrgn1));
    lgrgn1->next = lgrgn;
    lgrgn = lgrgn1;
    lgrgn->comcoef=    ln->line+factorShift;
    lgrgn->description=ln->line+lorentzShift;
    for (i=0;i<4;i++)
    {
      trim(pPtr[i]);
      if (pPtr[i][0]!=0)
      {
        locateinbase(pPtr[i],&j);
        if(check && j == 0)
        { errorMessage( fName[i],scat(" unknown particle %s" ,pPtr[i] ) );
          return FALSE;
        }
        lgrgn->fields[i] = j;
      }
      else
      {  if (i == 3) lgrgn->fields[i] = 0; else
        { errorMessage( fName[i],"particle name is expected");
          return FALSE;
        }
      }

    }

    if (check)
    {
      totcolor=1;
      for (mm=0;((mm<4)&&(lgrgn->fields[mm] !=0));mm++)
      {
        color=prtclbase[lgrgn->fields[mm] -1].cdim;
        if (color==-3) color=5;
        totcolor=totcolor*color;
      }
      if( (totcolor!=1)&&(totcolor!=15)&&(totcolor!=64)&&(totcolor!=120)&&(totcolor!=512) )
      {   errorMessage("Lorentz part","wrong color structure$");
         return FALSE;
      }
      spinorNumb=0;
      for (mm=0;((mm<4)&&(lgrgn->fields[mm] !=0));mm++)
      {
        if( prtclbase[lgrgn->fields[mm] -1].spin ==1 )  spinorNumb++ ;
      }
      if( (spinorNumb!=0)&&(spinorNumb!=2) )
      {  errorMessage("Lorentz part","wrong spinor  structure$");
        return FALSE;
       }
    }
    { boolean testLg;
      pregarbage = NULL;
      vardef = (polyvars *) getmem(sizeof(struct polyvars));
      testLg=testLgrgn(lgrgn);
      free (vardef);
      clearpregarbage();
      if (! testLg ) return FALSE;
    }


    ln=ln->next;
    nLine++;
   }

  lgrgn1 = lgrgn;   /*     Sorting    */
   do
   {
      for (i = 1; i <= 4; i++) lgrgn1->perm[i-1] = i;
      i = 1;
      while (i < 4)
         if (lgrgn1->fields[i-1] >= lgrgn1->fields[i + 1-1]) ++(i);
         else
         {
            mm = lgrgn1->fields[i-1];
            lgrgn1->fields[i-1] = lgrgn1->fields[i + 1-1];
            lgrgn1->fields[i + 1-1] = mm;
            mm = lgrgn1->perm[i-1];
            lgrgn1->perm[i-1] = lgrgn1->perm[i + 1-1];
            lgrgn1->perm[i + 1-1] = mm;
            if (i == 1)
               ++(i);
            else
               --(i);
         }
      lgrgn1 = lgrgn1->next;
  }  while (lgrgn1 != NULL);


  if (check)
  {
    mLine=nLine;
    lgrgn1 = lgrgn;   /*    check1       */
    do
    {
      nLine--;
      lgrgn2=lgrgn1->next;
      while (lgrgn2 != NULL )
      { if (  (lgrgn1->fields[0]==lgrgn2->fields[0]) &&
            (lgrgn1->fields[1]==lgrgn2->fields[1]) &&
            (lgrgn1->fields[2]==lgrgn2->fields[2]) &&
            (lgrgn1->fields[3]==lgrgn2->fields[3])
          )
        {  errorMessage("P1,P2,P3,P4","duplicate vertex");
            return FALSE;
        }
        lgrgn2=lgrgn2->next;
      }
      lgrgn1= lgrgn1->next;
     }  while (lgrgn1 != NULL);


    nLine=mLine;
    lgrgn1 = lgrgn;   /*    check2       */
    do
    {
      nLine--;
      for (i=0;i<4;i++)
      {  f_copy[i]=lgrgn1->fields[i];
        if (f_copy[i] !=0)
        {
          mm=ghostmother(f_copy[i]);
          f_copy[i]=prtclbase[mm-1].anti  + f_copy[i]-mm   ;
         }
      }

      i = 1;
      while (i < 4)
        if (f_copy[i-1] >= f_copy[i ]) ++(i);
        else
        {
          mm = f_copy[i-1];
          f_copy[i-1] = f_copy[i ];
          f_copy[i ] = mm;
          if (i == 1)
            ++(i);
          else
            --(i);
        }

      lgrgn2=lgrgn;
      while ((lgrgn2 != NULL ) && (  (f_copy[0] !=lgrgn2->fields[0]) ||
                          (f_copy[1] !=lgrgn2->fields[1])  ||
                          (f_copy[2] !=lgrgn2->fields[2])  ||
                          (f_copy[3] !=lgrgn2->fields[3])
                         )
          )
         {
         lgrgn2=lgrgn2->next;
          }
      if (lgrgn2 == NULL)
      {  char sss[10];
        strcpy(sss,"");
        for (i=0;i<3;i++)
        { strcat (sss, prtclbase[lgrgn1->fields[i]-1].name);
          strcat(sss," ");
        }
        if (lgrgn1->fields[3] !=0   )
           strcat(sss,prtclbase[lgrgn1->fields[3]-1].name);

      errorMessage("P1,P2,P3,P4",scat("conjugated vertex for %s not found",sss));
            return FALSE;
       }
      lgrgn1= lgrgn1->next;
     }  while (lgrgn1 != NULL);
   }

  return TRUE;
}


static void  filldecaylist(void)
{algvertptr  lgrgn1;
 byte        i, j, k, n;
 byte        pn[5];
 char    cc[3];
 decaylink   kk, qq;

   lgrgn1 = lgrgn;
   do
   {
      for (i = 1; i <= 4; i++)
      {
         pn[i-1] = ghostmother(lgrgn1->fields[i-1]);
         if (pn[i-1] != 0) pn[i-1] = prtclbase[pn[i-1]-1].anti;
      }
      pn[4] = 0;

      for (i = 1; i <= 4; i++)
         if (pn[i-1] != pn[i + 1-1] && pn[i-1] != 0)
         {
            j = 1;
            for (k = 1; k <= 4; k++)
               if (k != i)
               {  cc[j-1] = pn[k-1];
                  ++(j);
               }
            n = prtclbase[pn[i-1]-1].anti;

            if (prtclbase[n-1].top == NULL)
            {
               prtclbase[n-1].top = (decaylink)getmem(sizeof(modeofdecay));
               prtclbase[n-1].top->link = NULL;
               lvcpy(prtclbase[n-1].top->part,cc);
            }
            else
            {
               qq = prtclbase[n-1].top;
               while (TRUE)
               {
                  k = 1;
                  while (k < 4 && qq->part[k-1] == cc[k-1]) ++(k);
                  if (k == 4) goto exi;
                  if (qq->part[k-1] > cc[k-1])
                  {
                     kk = (decaylink)getmem(sizeof(modeofdecay));
                     kk->link = qq->link;
                     qq->link = kk;
                     lvcpy(kk->part,qq->part);
                     lvcpy(qq->part,cc);
                     goto exi;
                  }

                  if (qq->link == NULL)
                  {
                     kk = (decaylink)getmem(sizeof(modeofdecay));
                     kk->link = qq->link;
                     qq->link = kk;
                     lvcpy(kk->part,cc);
                     goto exi;
                  }
                  qq = qq->link;
               }
exi:;
            }
         }
      lgrgn1 = lgrgn1->next;
   }  while (lgrgn1 != NULL);
}


boolean  loadModel(boolean check)
{
  errorText[0]=0;
  if ( (!check)&&(lastModel == n_model) ) return TRUE;
        cleardecaylist();
  clearlgrgn();
  clearvars();
  if ( !readvars(check) )     return FALSE   ;
  if ( !readparticles(check)) return FALSE ;
  if ( !readlagrangian(check)) return FALSE ;
         filldecaylist();
/*  includeQCD(); */
  lastModel = n_model;
  return TRUE;
}

#ifdef TURBO_C
extern unsigned long coreleft(void);
#endif


void readModelFiles(int l)
{
  cleartab( &vars_tab);
  cleartab( &func_tab);
  cleartab( &prtcls_tab);
  cleartab( &lgrng_tab);
   cleardecaylist();
  clearlgrgn();
  clearvars();
#ifdef TURBO_C
if (startMemory ==0)  startMemory=coreleft();
else
if (startMemory !=coreleft())
{
  messanykey(10,10,"Memory losed$");
  startMemory =coreleft();
}
#endif
lastModel=0;

  readtable ( &vars_tab ,  scat("%smodels%cvars%d.mdl",pathtouser,f_slash,l));
  readtable ( &func_tab ,  scat("%smodels%cfunc%d.mdl",pathtouser,f_slash,l));
  readtable ( &prtcls_tab ,scat("%smodels%cprtcls%d.mdl",pathtouser,f_slash,l));
  readtable ( &lgrng_tab , scat("%smodels%clgrng%d.mdl",pathtouser,f_slash,l));
}
