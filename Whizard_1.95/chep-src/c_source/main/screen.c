#include "tptcmac.h"
#include "syst2.h"
#include "crt.h"
#include "global.h"
#include "files.h"
#include "sos.h"
#include "showgrph.h"
#include "red_out.h"
#include "r_code.h"
#include "crt0.h"
#include "file_scr.h"
#include "read_mdl.h"
#include "edittab.h"
#include "os.h"
#include "help.h"
#include "crt_util.h"

#include "screen.h"

 byte    menulevel;
 boolean menuinmenu;

#define tcol Green
#define mpos 7
#define graphpos 8



static void  readtext(char* fname)
{
   FILE * txt;
   trim(fname);
   txt=fopen(fname,"r");
   if (txt == NULL)
   {
      messanykey(10,10," File not found $");
      return;
   }
   showtext (1, 1, 80,maxRow(),"",txt);
   fclose(txt);
}


void writeModelFiles(int l)
{
  int i;
  char fName[STRSIZ];
  sprintf(fName,"%smodels%c%%s%d.mdl",pathtouser,f_slash,l);
  for(i=0;i<4;i++) writetable ( &modelTab[i],scat(fName,mdFls[i]));
}



void  editModel( boolean  edit)
{int  n,i,j;
 void *  pscr = NULL;
 boolean edited=FALSE;
 char  tabMenu[STRSIZ], tabName[80];
 char menuName[30]; 
 char tabhelp[4][10]={ "h_003101", "h_003102", "h_003104", "h_003104"};

 n = 1;
 
cont:
   do
   {
      if (edit) strcpy(menuName,"Edit  model"); 
           else strcpy(menuName,"View   model");
      strcpy(tabMenu,"\017");
      for (i=0;i<4;i++)
      { strcpy(tabName,modelTab[i].headln);
        trim(tabName);
        sbld(tabName," %s",tabName);
	for (j=strlen(tabName); j<15;j++) tabName[j]=' ';
        tabName[15]=0;
	strcat(tabMenu,tabName);
      }
      menu1(17,15,"",tabMenu,"h_00310*",&pscr,&n);

      if (n >0 && n<=4)
	 edited =  edittable(&modelTab[n-1],1,tabhelp[n-1],!edit) || edited;
   }  while (n != 0);

   if  ( edited)
   {
      if  (mess_y_n(15,19," Save corrections ? $") )
      {
         if (loadModel(TRUE) ) writeModelFiles(n_model); else  goto cont;

      }
      else  readModelFiles(n_model);
   }
}

void         smalllabel(void)
{
   goto_xy(70,1);
   scrcolor(Yellow,Blue);
   print("CompHEP");
   scrcolor(White,Black);
}


void         showheap(void)
{ /* goto_xy(60,24);  print("Memory= %lu",usedmemory);*/ }


void         menuhelp(void)
{
	goto_xy(15,5);
	print(  "W A R N I N G S \n\n");
	print("  1.Standard normalizing conventions are used,\n");
	print("    see  Review of Particle Properties,\n");
	print("    Phys. Lett.,B239, 1990. \n\n");
	print("  2.'tHooft-Feynman gauge is used for gauge \n");
	print("    particles. Diagrams containing ghosts and \n");
	print("    4-gluon vertices are not displayed.       \n");
	print("    Corresponding contributions are taken into\n");
	print("    account automatically.                    \n");
	chepbox(1,6,48,17);
}


void         modelinfo(void)
{
     goto_xy(5,1);
     scrcolor(LightRed,Black); print("   Model:  ");
     scrcolor(White,Black);
     print("%s",copy(modelmenu,n_model * 22 - 20,22));
}


void         processinfo(void)
{
   goto_xy(5,3);
   scrcolor(LightRed,Black); print(" Process:  ");
   scrcolor(White,Black);
   print("%s",processch);
}


void         diagramsinfo(void)
{
   goto_xy(15,5);
   scrcolor(LightRed,Black);
   print(" Feynman diagrams \n");
   scrcolor(White,Black);
   print("      diagrams in      subprocesses  are constructed.\n");
   print("      diagrams  are deleted.");
   scrcolor(Yellow,Black);
   goto_xy(1,6);
   print("%u",totdiag_f);
   goto_xy(20,6);
   print("%u",subproc_f);
   goto_xy(1,7);
   print("%u",deldiag_f);
   while (where_y() < 7) print(" ");
}


void  sq_diagramsinfo(void)
{
   goto_xy(15,9);
   scrcolor(LightRed,Black);
   print(" Squared diagrams \n");
   scrcolor(White,Black);
   print("      diagrams in      subprocesses  are constructed.\n");
   print("      diagrams  are deleted.\n");
   print("      diagrams  are calculated.");
   scrcolor(Yellow,Black);
   goto_xy(1,10);
   print("%u",totdiag_sq);
   goto_xy(20,10);
   print("%u",subproc_sq);
   goto_xy(1,11);
   print("%u",deldiag_sq);
   while (where_x() < 7) print(" ");
   goto_xy(1,12);
   print("%u",calcdiag_sq);
   goto_xy(1,13);
}


static void  viewdiag_sq(void)
{ integer  lastkey;
  char f_name[STRSIZ];

   sprintf(f_name,"%stmp%cview.txt",pathtouser,f_slash);
   ndiagr = 1;
   do
   {
      lastkey = showgraphs(2);
      if (lastkey == KB_ENTER)
      {
         makeviewfile(f_name);
	 readtext(f_name);
	 unlink(f_name);	 
      }
      if (lastkey <=0)
      {  makeghostdiagr(-lastkey,f_name);
	 readtext(f_name);
	 unlink(f_name);
      }
   }  while (lastkey != KB_ESC);   /*  Esc  */
}

void  sqdiagrmenu(void)
{  void * pscr = NULL ;
   if (subproc_sq == 1) { nsub = 1; return;}
   do
   {  
      menu_f(10,16,"NN      Subprocess               Del  Calc Rest",
       MENUQ_NAME,"",&pscr,&nsub);
   }  while (nsub < 0);   /*  Esc  */
   clrbox(10,14,70,22);
}   /*  View squared diagrams  */


void  viewsqdiagr(void)
{   /*  View squared diagrams  */
   nsub = 1;
   do
   {
      sqdiagrmenu();
      if (nsub != 0) viewdiag_sq();
      sq_diagramsinfo();
   }  while (!(nsub == 0 || subproc_sq == 1));   /*  Esc  */
   sq_diagramsinfo();
}   /*  View squared diagrams  */


void  viewfeyndiag(boolean del_mode)
{integer    lastkey;
 void * pscr = NULL;
 char   tp;

   tp = del_mode ? 1 : -1;
   nsub = 1;
   do
   {
      if (subproc_f == 1)
         nsub = 1;
      else
      /* Repeat */
      {
         menu_f(10,11,"NN        Subprocess             Del  Rest",
          MENUP_NAME,"",&pscr,&nsub);

      }
      if (nsub > 0)
      do
      {
         ndiagr = 1;
         lastkey = showgraphs(tp);
         diagramsinfo();
      }  while (lastkey != KB_ESC);
   }  while (!(nsub == 0 || subproc_f == 1));

   diagramsinfo();
}


static void  changeoptions(int x,int y)
{
 int    i,k;
 void *   pscr=NULL;
 char * pstr;


   k = 1;
   while (k != 0)
   {
     char   menutxt[] ="\032"
         " Diagrs. in results   |XX1"
         " Structure Functions  |XX2"
         " QUADRUPLE precision  |XX3"
         " Momentum substitution|XX4";
      pstr=strstr(menutxt,"XX1");
      for (i = 1; i <= 4; i++)
      {
         if (inset(i-1,options)) memcpy(pstr,"ON ",3);
         else                    memcpy(pstr,"OFF",3);
         pstr=pstr+menutxt[0];
      }

      menu1(x,y + 1,"Check options",menutxt,"h_00100*",&pscr,&k);
      if (k > 0)
               if (inset(k-1,options))
                  options &= ~setof(k-1,_E);
               else
                  options |= setof(k-1,_E);
   }	
}


void  viewresults(void)
{searchrec  s;
 
 int   i,k,kmenu,doserror;
 void *  pscr  = NULL;
 void *  pscr2 = NULL;
 
 shortstr  newname, oldname;
 char f_name[STRSIZ];
 char  menustr[2016];

   menustr[0]=15;
   k=1;
   doserror = find_first(scat("%sresults%c*.*",pathtouser,f_slash),&s,anyfile);
   while (doserror == 0 && k<=2000)
   {  
     for (i = 1; (i <= strlen(s.name))&&(i<=15); i++) menustr[k++]=s.name[i-1];
     for (i = strlen(s.name) + 1; i <= 15; i++) menustr[k++]=' '; 
     doserror = find_next(&s);
   }
   menustr[k]=0;
   
   if (menustr[1] == 0 )
   {
      messanykey(10,15,"directory RESULTS is empty$");
      return;
   }  
   
   kmenu = 1;   
label_1:

   menu1(10,10,"","\010"
      " View   "
      " Clear  "
      " Rename ","",&pscr,&kmenu);
      
   switch (kmenu)
   {
     case 0:
       return ;         
     case 1:
      k = 1;
      do
      { 
         menu1(10,10,"",menustr,"",&pscr2,&k);
         if (k > 0) 
         {sprintf(f_name,"%sresults%c%.15s",pathtouser,f_slash,menustr+k*15-14);
          readtext(f_name);
         }                   
      }  while (k != 0);         
      break;

     case 2:
      if ( mess_y_n( 6,13," Delete files $") )
      {
        do
        { 
   
          find_first(scat("%sresults%c*.*",pathtouser,f_slash),&s,archive);
          unlink(scat("%sresults%c%s",pathtouser,f_slash,s.name));
          doserror = find_next(&s);
        }while (doserror == 0);
        put_text(&pscr);         
        return;
      }
      break;

     case 3:
      strcpy(newname," ");
      while(TRUE)
      {  void * pscr3;
         get_text(1,maxRow(),maxCol(),maxRow(),&pscr3); 
         goto_xy(1,maxRow());
         print("Enter new name: ");
	 k = str_redact(newname,1);
	 if (k == KB_ESC)
	 {   goto_xy(1,24);
             clr_eol();
             goto label_1;
         }
	 if (k == KB_ENTER)
         {  char dir[10];
            trim(newname);
            if ( f_slash == ']') strcpy(dir,".dir"); else dir[0]=0; 
 
            sbld(newname,"%s%c%s%s",_pathtouser,f_slash,newname,dir);
            sbld(oldname,"%s%c%s%s",_pathtouser,f_slash,"results",dir);
            if(rename(oldname,newname)==0)
            {
               chepmkdir(oldname);
               put_text(&pscr);
               put_text(&pscr3);
               return;
            }
             else  messanykey(10,15," Can't rename the directory$");
         }
         put_text(&pscr3);   
      }
   } 
   goto label_1;
}
  
static char * f_hlp=NULL; 

static void f1_key(int x)
{ 
 int len;
 char ss[STRSIZ]; 
  if (f_hlp == NULL)  goto exi; 
  len=strlen(f_hlp)-1;
  strcpy(ss,f_hlp);  
  if (ss[len] =='*') { if(x<=9) ss[len]='0'+x; else ss[len]='A'-1+x;  }
  if( show_help(ss) ) return; 
     
exi:  messanykey(10,10,"Help is absent$");return;
}


static void f2_key(int x)
{
  if (!menuinmenu)
  {  menuinmenu = TRUE;
	  changeoptions(15,14);
	  menuinmenu = FALSE;
  }
}

static void f3_key(int x)
{
  if ((!menuinmenu)&&(menulevel > 0))
  {
	  menuinmenu = TRUE;
	  editModel(FALSE);
	  menuinmenu = FALSE;
  }  else be_be();
}

static void f4_key(int x)
{ int nsubtmp;
	if (!menuinmenu && menulevel == 2)   /* F4  */
	{
		menuinmenu = TRUE;
		nsubtmp = nsub;
		viewfeyndiag(FALSE);
		nsub = nsubtmp;
		menuinmenu = FALSE;
	}
}

static void f5_key(int x)
{
	if (!menuinmenu )   /*  F5  */
	{
		menuinmenu = TRUE;
		
		
		viewresults();
		menuinmenu = FALSE;
	}
}

static void f10_key(int x)
{
  if (!menuinmenu &&
		(((menulevel == 0 || menulevel == 1) &&
		  mess_y_n(56,maxRow()-5," Quit session? $")) ||
		 (menulevel == 2 &&
		  mess_y_n(56,maxRow()-5," Quit session?  $"))))
	  {
		  if (menulevel == 0) menulevel = 1;
		  saveent(nsub,ndiagr,n_model,menulevel - 1);
		  finish();
		  exit(0);
	  }
}


void menu1 (int col, int row,char* label,char* menustr,char* help,
                                                      void* hscr,int * kk)
{
/* typedef void (*voidfunc)(int i); */
void   (*funcKey[11])(int);
char * funcKeyMess[11];
int i;
static char f1_mess[10]="Help";
static char f2_mess[10]="Options";
static char f3_mess[10]="Models";
static char f4_mess[10]="Diagrams";
static char f5_mess[10]="Results";
static char f10_mess[10]="Quit";
char * help_tmp;

help_tmp=f_hlp;
f_hlp=help;

funcKey[0]=  f1_key;
funcKeyMess[0]=  f1_mess;
for (i=1;i<10;i++) { funcKey[i]=NULL; funcKeyMess[i]=NULL;}

if( ! menuinmenu )
{ 
  funcKey[1]=f2_key;
  funcKeyMess[1]= f2_mess;
  
  if ( menulevel > 0 )   { funcKey[2]=f3_key; funcKeyMess[2]=f3_mess;}
  if ( menulevel > 1 )   { funcKey[3]=f4_key; funcKeyMess[3]=f4_mess;}
  funcKey[4]=f5_key;   funcKeyMess[4]=f5_mess;
  funcKey[9]=f10_key;  funcKeyMess[9]=f10_mess;
}
menu0(col,row,label,menustr,funcKey,funcKeyMess,hscr,kk);

f_hlp=help_tmp;
}
                   
void menu_f (int col, int row,char* label, char* f_name, char * help,
                                             void* hscr,int * kk)
{
FILE * f;
char * help_tmp;
char * menustr;
void   (*funcKey[11])(int);
char * funcKeyMess[11];
int i,nline;
static char f1_mess[10]="Help";
static char f2_mess[10]="Options";
static char f3_mess[10]="Models";
static char f4_mess[10]="Diagrams";
static char f5_mess[10]="Results";
static char f10_mess[10]="Quit";
char ch1,ch2;

f= fopen(f_name,"r");
if (f==NULL) return;

fread(&ch1,1,1,f);
fread(&ch2,1,1,f);

fseek(f,0,SEEK_END);
nline=ftell(f)/ch2;

menustr=(char *) malloc(2+nline*ch1);
fseek(f,2,SEEK_SET);

menustr[0]=ch1;
for (i=1;i<=nline;i++) 
{ fread(menustr + 1+(i-1)*ch1,ch1,1,f); 
  fseek(f,ch2-ch1,SEEK_CUR);
}
fclose(f);
menustr[1+nline*ch1]=0;  

help_tmp=f_hlp;
f_hlp=help;

funcKey[0]=  f1_key;
funcKeyMess[0]=  f1_mess;
for (i=1;i<10;i++) { funcKey[i]=NULL; funcKeyMess[i]=NULL;}

if( ! menuinmenu )
{ 
  funcKey[1]=f2_key;
  funcKeyMess[1]= f2_mess;
  
  if ( menulevel > 0 )   { funcKey[2]=f3_key; funcKeyMess[2]=f3_mess;}
  if ( menulevel > 1 )   { funcKey[3]=f4_key; funcKeyMess[3]=f4_mess;}
  funcKey[4]=f5_key;   funcKeyMess[4]=f5_mess;
  funcKey[9]=f10_key;  funcKeyMess[9]=f10_mess;
}

menu0(col,row,label,menustr,funcKey,funcKeyMess,hscr,kk);
f_hlp=help_tmp;
}
