#include "tptcmac.h"
#include "syst2.h"
#include "physics.h"
#include "global.h"
#include "sos.h"
#include "ghosts.h"
#include "transfer.h"
#include "cweight.h"
#include "prepdiag.h"
#include "crt.h"
#include "files.h"
#include "pvars.h"
#include "chess.h"
#include "polynom.h"
#include "tensor.h"
#include "spinor.h"
#include "saveres.h"
#include "parser.h"
#include "pre_read.h"
#include "reader0.h" 
#include "reader2.h"
#include "crt_util.h"
#include "rfactor.h"
#include "os.h"
#include "symbolic.h"

#ifdef STRACE
#include "test_wrt.h"
#endif

static byte        gamma_map[2 * maxvert]; /* from symbcalc */
static byte        maxmomdeg, n_g5;        /* from symbcalc */
static byte        px, py;                 /* from symbcalc */
static poly        vert[2 * MAXINOUT],     /* from symbcalc */
                   block[2 * MAXINOUT];    /* from symbcalc */
static poly        fermres[maxvert];       /* from symbcalc */


static void memoryInfo_(int used)
{ 

  goto_xy(14,16);
  print("%d Kb    ",used >> 10);
  if (escpressed()) save_sos(-2);

}

static int screenOutput = 1;


static void  wrtoperat(char* s)
{
 scrcolor(Yellow,Black);
 if (screenOutput == 1)
 {
     goto_xy(14,17); clr_eol();
     print(s);
 }	
	
}


static void  firstvertexreading(void)
{preres      m, m_; 
 byte        v, n, ng5; 

   pregarbage = NULL; 
   vardef->nvar = 0;
   m = (preres) readexprassion("1",bact,uact,rd);
   if (rderrcode != 0) save_sos(rderrcode);
   for (v = 1; v <= vcs.sizet; v++)
   {
      m_ = (preres) readexprassion(vertexes[v-1].lgrnptr->description,
                          bact,uact,rd);
                          
                          
      if (rderrcode != 0) save_sos(rderrcode);
      if (m_->g5) fermloops[fermmap[v-1]-1].g5 = TRUE;
      gamma_map[v-1] = m_->maxg;
      m_->g5 = FALSE;
      m_->maxg = 0;
      m_->indlist = setof(_E);
      m_->tp = polytp;
      m = (preres) bact('*',m,m_);
      if (rderrcode != 0) save_sos(rderrcode);
   }
   for (n = 1; n <= vardef->nvar; n++)
      vardef->vars[n-1].maxdeg = m->varsdeg[n-1] + 1;
   maxmomdeg = m->degp;
   ng5 = 0;
   for (n = 1; n <= nloop; n++)
      if (fermloops[n-1].g5) ++(ng5);
   levi = (ng5 > 1);
   clearpregarbage();
}


static void  calctenslength(void)
{byte  v, l, vln;

   maxIndex = 0;
   for (v = 0; v < vcs.sizet; v++)
   {
      vln = vcs.valence[v];
      for (l=0;l<vln;l++)  maxIndex=MAX(maxIndex,vcs.vertlist[v][l].lorentz);

   }
   if(maxIndex == 0) maxIndex=1;
   if (levi) tensLength=1+(maxIndex+3)/sizeof(long);
      else   tensLength=(maxIndex+ sizeof(long) -1)/sizeof(long);
   if (tensLength == 0) tensLength = 1;
#ifdef STRACE
tracePrn("\n tensLength=%d",tensLength);
#endif
}


static void  calcspinlength(void)
{ int  n, currentlen, nl;

   spinLength = 0;
   for (nl = 0; nl < nloop; nl++)
   {  fermloopstp * with1 = &fermloops[nl];
      currentlen = 0;
      for (n = 0; n < with1->len; n++)
      {
         currentlen += gamma_map[with1->vv[n]-1] + 1;
         if (spinLength < currentlen) spinLength = currentlen;
         if (with1->intln[n] != 0 &&
             !insetb(vcs.vertlist[with1->vv[n]-1]
                                 [with1->intln[n]-1].lorentz,
                     setmassindex))
            currentlen -= 2;
      }
   }
   spinLength =1+ (spinLength +1)/sizeof(long);
}


static void  propagatorsfirstreading(void)
{byte  ninout, v, d, n, l, vln;
 char  name[7];

   ninout = nin + nout;
   for (v = 1; v <= vcs.sizet; v++)
   {
      vln = vcs.valence[v-1];
      for (l = 1; l <= vln; l++)
      {edgeinvert * with1 = &vcs.vertlist[v-1][l-1];
         if (with1->moment > 0)
         {
            d = prtclbase[with1->partcl-1].spin;
            if (d == 2 && !insetb(with1->lorentz,setmassindex)) d = 0;
				if (d == 4 && !insetb(with1->lorentz,setmassindex0)) d = 2;
            if (d != 0)
            {
					maxmomdeg += MIN(d,2);
               if (with1->moment > ninout)
               {
                  strcpy(name,prtclbase[with1->partcl-1].massidnt);
                  if (strcmp(name,"0") != 0)
                  {
                     n = 1;
                     while (n <= ninout && strcmp(name,inoutmasses[n-1]) != 0)
                        ++(n);
							if (n > ninout) addvar(name,d);
                     if ( d == 4 ) addvar(name,2);
                  }
               }
            }
         }
      }
   }
}


static void  addinoutmasses(void)
{byte  n, m;
 char  massname[7];

   for (n = 1; n <= nin + nout; n++)
   {
      strcpy(massname,inoutmasses[n-1]);
      if (strcmp(massname,"0") != 0)
      {
         m = 1;
         while (strcmp(massname,inoutmasses[m-1]) != 0) ++(m);
         if (n == m) addvar(inoutmasses[n-1],maxmomdeg);
      }
   }
}


static void  addscmult(void)
{ int      n, m, i, j;
 char  p1, p2;
 char      pname[MAXINOUT][4];
 poly      ee;
 poly      pp, qq;

   px = 0; py = 0;
   for (i = 1; i <= 3 * maxvert; i++)
   {
      if (momdep[i-1][0] == 1) ++(px);  /* For momdep this is a length. V.E. */
      if (momdep[i-1][0] > 0) ++(py);
   }
   for (n = 1; n <= px; n++)
		sbld(pname[n-1],"p%d",n);
   maxmomdeg /= 2;
   for (n = 2; n <= px; n++)
      for (m = 1; m <= n - 1; m++)
         addvar(scat("%s.%s",pname[m-1],pname[n-1]),maxmomdeg);

   closevars();
   maxLength = MAX(MAX(tensLength,spinLength),monomLength);
#ifdef STRACE
	tracePrn("\n maxLength= %d",maxLength);
#endif
   dellevi = FALSE;
   contracts = (poly *) getmem_((sizeof(pointer) * py * (py + 1))/2);

   for (n = 1; n <= nin + nout; n++)
   {
      if (strcmp(inoutmasses[n-1],"0") == 0)
         assignsclmult(-n,-n,NULL);
      else
      {
         ee = (poly) rd_(inoutmasses[n-1]);
         assignsclmult(-n,-n,multtwopoly(ee->next,ee->next));
      }
   }

   for (n = 2; n <= px; n++)
      for (m = 1; m <= n - 1; m++)
      {
         ee = (poly) rd_(scat("%s.%s",pname[m-1],pname[n-1]));
         assignsclmult(-m,-n,ee->next);
      }

   for (n = px + 1; n <= py; n++)
      for (m = 1; m <= n; m++)
         if (!(n == m && n <= nin + nout))
         {
            qq = NULL;
            for (i = 1; i <= momdep[n-1][0]; i++)
            {
               p1 = (char)momdep[n-1][i];
               for (j = 1; j <= momdep[m-1][0]; j++)
               {
                  p2 = (char)momdep[m-1][j];
                  pp = copypoly(scalarmult(-abs(p1),-abs(p2)));
                  if (p1 * p2 < 0) multpolyint(&pp,-1);
                  sewpoly(&qq,&pp);
               }
            }
            assignsclmult(-m,-n,qq);
         }
}


static void  secondvertexreading(void)
{poly        m;
 byte        v, l, ll;
 char    sgn;
#ifdef STRACE
	tracePrn("vertex reading \n");
	tracePrn("tensLength=,  %d\n", tensLength);
	tracePrn("spinLength=,  %d\n", spinLength);
#endif

   for (v = 1; v <= vcs.sizet; v++)
   {
      for (l = 1; l <= MAX(1,vcs.valence[v-1]); l++)
      {
         ll = vertexes[v-1].subst[l-1];
         momsubst[l-1] = vcs.vertlist[v-1][ll-1].moment;
         indsubst[l-1] = vcs.vertlist[v-1][ll-1].lorentz;
      }
      r_reading2=vertexes[v-1].r_vert;
      m=(poly) readexprassion(vertexes[v-1].lgrnptr->description,bact_,uact_,rd_);      
								
#ifdef STRACE 
   tracePrn("\n");
	writeexpression(m);
#endif
      if (m->coef.num <= polytp)
      {
         newmonom(&vert[v-1]);
         vert[v-1]->next = NULL;
         vert[v-1]->coef.ptr = m->next;
         for (l = 0; l < maxLength; l++) vert[v-1]->tail.power[l] = 0;
      }
      else
      vert[v-1] = m->next;


      delmonom(&m);
      if (vcs.valence[v-1] == 3 &&
          prtclbase[vcs.vertlist[v-1][0].partcl-1].cdim == 8 &&
          prtclbase[vcs.vertlist[v-1][1].partcl-1].cdim == 8 &&
          prtclbase[vcs.vertlist[v-1][2].partcl-1].cdim == 8)
      {
         sgn = 1;
         if (vertexes[v-1].subst[0] > vertexes[v-1].subst[1]) sgn = -sgn;
         if (vertexes[v-1].subst[1] > vertexes[v-1].subst[2]) sgn = -sgn;
         if (vertexes[v-1].subst[0] > vertexes[v-1].subst[2]) sgn = -sgn;
         if (sgn == -1) multtensint(&vert[v-1],-1);
		}
   }
}


static void  masscalc(byte v,byte l,byte deg,poly* p)
{char        masstxt[7];
 poly        m; 

   strcpy(masstxt,prtclbase[vcs.vertlist[v-1][l-1].partcl-1].massidnt); 
   if (strcmp(masstxt,"0") == 0)
      *p = NULL; 
   else
   {
      m = (poly) rd_(masstxt);
      if (deg == 1)
         *p = m->next;
      else
      {  *p = multtwopoly(m->next,m->next);
         delpoly(&m);
      }
   }
}


static void  kmorgcalc(byte v,byte l,poly* p)
{char    k;
 byte        i, m;

   newmonom(p);
   (*p)->next = NULL;
   (*p)->coef.ptr = plusone();
   for (i=0;i<tensLength;i++)(*p)->tail.power[i] = 0;
   k = vcs.vertlist[v-1][l-1].moment;
   m = vcs.vertlist[v-1][l-1].lorentz;
   (*p)->tail.tens[m-1] = -abs(k);
   if (k < 0) ((*p)->coef.ptr)->coef.num = -1;
}


static void  kmsubcalc(byte v,byte l,poly* p)
{byte        ll;
 char    k;
 byte        i, m;
 poly        q;

   m = vcs.vertlist[v-1][l-1].lorentz;
   *p = NULL;
   for (ll = 1; ll <= vcs.valence[v-1]; ll++)
   if (l != ll)
   {
      newmonom(&q);
      q->next = NULL;
      q->coef.ptr = plusone();
      for (i=0;i<tensLength;i++) q->tail.power[i] = 0;
      k = vcs.vertlist[v-1][ll-1].moment;
      q->tail.tens[m-1] = -abs(k);
      if (k > 0) (q->coef.ptr)->coef.num = -1;
      sewtens(p,&q,tensLength);
   }
}


static poly  fermpropag(byte v,byte l)
{char  proptxt[16];
 char  mass[7];
 poly  m,p;

   strcpy(mass,prtclbase[vcs.vertlist[v-1][l-1].partcl-1].massidnt);
   if ( prtclbase[vcs.vertlist[v-1][l-1].partcl-1].hlp == '*')
    strcpy(proptxt,mass);
   else
   {
      sbld(proptxt,"G(p%c)",chr(l + ord('0')));
      if (strcmp(mass,"0") != 0)  sbld(proptxt,"%s+%s",proptxt,mass);
      else
	 if (fermionp(vcs.vertlist[v-1][l-1].partcl))
	 {
            if (prtclbase[vcs.vertlist[v-1][l-1].partcl-1].hlp == 'L')
               sbld(proptxt,"%s*(1+G5)",proptxt);
            else
                if (prtclbase[vcs.vertlist[v-1][l-1].partcl-1].hlp == 'R')
				sbld(proptxt,"%s*(1-G5)",proptxt);
	 }
	 else
	 {
	    if (prtclbase[vcs.vertlist[v-1][l-1].partcl-1].hlp == 'R')
                  sbld(proptxt,"%s*(1+G5)",proptxt);
            else
	       if (prtclbase[vcs.vertlist[v-1][l-1].partcl-1].hlp == 'L')
				sbld(proptxt,"%s*(1-G5)",proptxt);
	 }
   }
   momsubst[l-1] = vcs.vertlist[v-1][l-1].moment;
   r_reading2=FALSE;
   m = (poly)readexprassion(proptxt,bact_,uact_,rd_);
   p = (m->next);
   delmonom(&m);
   return p;
}


static boolean  g5_test(poly sp)
{
   while (sp != NULL)
   {
      if (sp->tail.spin.g5 == 1) return TRUE;
      sp = sp->next;
   }
   return FALSE;
}


static void  calcfermloops(void)
{byte     v, l,l1,l2, n, lpcount, m, i, nmassind, nv;
 poly     mult_1, mult_2,mult_1_, mult_2_, frmprpg, fctmp,
			 km1, km2, m_2,m_2_, q, sum;
 poly     subv[4], subs[4], mass2[4];
 poly     pmem;

/* Main Procedure -- FermPrgEmit */
   for (lpcount = 1; lpcount <= nloop; lpcount++)
	{  fermloopstp *with1 = &fermloops[lpcount-1];

		frmprpg = fermpropag(with1->vv[0],with1->ll[0]);
#ifdef STRACE
tracePrn("fermion loop %d  calculation\n",lpcount );
tracePrn("\nvertex=");
writespinor(vert[with1->vv[0]-1] );
tracePrn("\n propagator=");
writespinor( frmprpg);
#endif

		fctmp = multtwospin(vert[with1->vv[0]-1],frmprpg,FALSE);

#ifdef STRACE
tracePrn("\n result=");
writespinor(fctmp);
#endif
      deltensor(&frmprpg);
      for (n = 2; n <= with1->len; n++)
		{
			mult_1 = fctmp;

			frmprpg = fermpropag(with1->vv[n-1],with1->ll[n-1]);
#ifdef STRACE
tracePrn("vertex=");
writespinor(vert[with1->vv[n-1]-1] );
tracePrn("\n propagator=");
writespinor( frmprpg);
#endif
         mult_2 = multtwospin(vert[with1->vv[n-1]-1],frmprpg,FALSE); deltensor(&frmprpg);
         fctmp = multtwospin(mult_1,mult_2,n == with1->len);

			l1=with1->intln[n-1];
			if ( l1 != 0 &&
			  ! insetb(vcs.vertlist[with1->vv[n-1]-1][l1-1].lorentz,setmassindex)
				) l1=0;

			l2=with1->intln2[n-1];
			if ( l2!= 0 &&
			  ! insetb(vcs.vertlist[with1->vv[n-1]-1][l2-1].lorentz,setmassindex)
				) l2=0;

			if (l1 == 0 && l2 != 0) { l=l1;l1=l2;l2=l;}

			if (l2 != 0)
			{ mult_1_ = copytens(mult_1,spinLength);
			  mult_2_ = copytens(mult_2,spinLength);
			}

			if (l1 != 0 )
         {
				v = vcs.vertlist[with1->vv[n-1]-1][l1-1].nextvert.vno;
				l = vcs.vertlist[with1->vv[n-1]-1][l1-1].nextvert.edno;

            masscalc(v,l,2,&m_2);
				multtenspoly(&fctmp,m_2);

            kmsubcalc(v,l,&km1);
				multspintens(&mult_1,&km1);deltensor(&km1);

				kmsubcalc(with1->vv[n-1],l1,&km2);
				multspintens(&mult_2,&km2);deltensor(&km2);

            q = multtwospin(mult_1,mult_2,(boolean)(n == with1->len));
				sewtens(&fctmp,&q,spinLength);
				if (l2 !=0)
				{
					v = vcs.vertlist[with1->vv[n-1]-1][l2-1].nextvert.vno;
					l = vcs.vertlist[with1->vv[n-1]-1][l2-1].nextvert.edno;

					masscalc(v,l,2,&m_2_);
					multtenspoly(&fctmp,m_2_);delpoly(&m_2_);

					kmsubcalc(v,l,&km1);
					multspintens(&mult_1_,&km1);

					kmsubcalc(with1->vv[n-1],l2,&km2);
					multspintens(&mult_2_,&km2);

					q = multtwospin(mult_1_,mult_2_,(boolean)(n == with1->len));
					multtenspoly(&q,m_2);
					sewtens(&fctmp,&q,spinLength);

					multspintens(&mult_1,&km1);
					multspintens(&mult_2,&km2);
					q=multtwospin(mult_1,mult_2,(boolean)(n == with1->len));
					sewtens(&fctmp,&q,spinLength);

					deltensor(&km1);
					deltensor(&km2);
					deltensor(&mult_1_);
					deltensor(&mult_2_);
				}
				delpoly(&m_2);
         }
         deltensor(&mult_1);
			deltensor(&mult_2);
#ifdef STRACE
tracePrn("\n result=");
writespinor(fctmp);
#endif
		}

      for (nv = 1; nv <= strlen(with1->invrt); nv++)
      {
         v = (byte)(with1->invrt[nv-1]);
         nmassind = 0;
         for (l = 1; l <= vcs.valence[v-1]; l++)
         {
            m = vcs.vertlist[v-1][l-1].lorentz;
            if (m != 0 && insetb(m,setmassindex))
            {
               ++(nmassind);
               kmorgcalc(v,l,&subv[nmassind-1]);
               kmsubcalc(vcs.vertlist[v-1][l-1].nextvert.vno,
                         vcs.vertlist[v-1][l-1].nextvert.edno,
                         &subs[nmassind-1]);
               masscalc(v,l,2,&mass2[nmassind-1]);
            }
         }
         if (nmassind == 0)
            multspintens(&fctmp,&vert[v-1]);
         else
         {
            sum = NULL;
            for (n = 0; n <= (1 << nmassind) - 1; n++)
            {
               mult_1 = copytens(vert[v-1],tensLength);
               mult_2 = copytens(fctmp,spinLength);
               for (i = 1; i <= nmassind; i++)
                  if (((1 << (i - 1)) & n) != 0)
                  {
                     multspintens(&mult_2,&subs[i-1]);
                     q = mult_1;
                     pmem = copytens(subv[i-1],tensLength);
                     mult_1 = multtwotens(q,pmem);
                     /*   DelTensor(Q); */
                  }
                  else
                     multtenspoly(&mult_1,mass2[i-1]);
               multspintens(&mult_2,&mult_1);
               deltensor(&mult_1);
               sewtens(&sum,&mult_2,spinLength);
            }
            deltensor(&fctmp);
            fctmp = sum;
            for (n = 1; n <= nmassind; n++)
            {
               deltensor(&subv[n-1]);
               deltensor(&subs[n-1]);
               delpoly(&mass2[n-1]);
            }
         }
         fermmap[v-1] = lpcount;
      }
      with1->g5 = g5_test(fctmp);
      fermres[lpcount-1] = calcspur(fctmp);
      for (n = 1; n <= with1->len; n++)
         deltensor(&vert[with1->vv[n-1]-1]);
      for (nv = 1; nv <= strlen(with1->invrt); nv++)
         deltensor(&vert[(byte)(with1->invrt[nv-1])-1]);
   }
}


static void  multblocks(byte v1,byte v2)
{poly        sub[15], mass2[15];
 poly        t1, t2, sum, q, mult_1, mult_2;
 byte        i, v, l, vmin, nmassind, n;
 indvertset  msind, ind1, ind2;
 boolean     leviflag;
 byte        lastn;
 poly        pmem;
 char        messtxt[STRSIZ];

   leviflag = levi && ( n_g5 == vertinfo[v1-1].g5 + vertinfo[v2-1].g5) ;
   t1 = block[v1-1];
   t2 = block[v2-1];
   setofb_cpy(ind1,vertinfo[v1-1].ind);
   setofb_cpy(ind2,vertinfo[v2-1].ind);
   strcpy(messtxt,"Indices contraction   ");
   for (i = 1; i <= 15; i++)
   if (insetb(i,ind1) && insetb(i,ind2))
      sbld(messtxt,"%s L%s",messtxt,funstr(i));
   sbld(messtxt,"%s           ",messtxt);
   wrtoperat(messtxt);

   setofb_cpy(msind,setofb_its(ind1,ind2));
   setofb_cpy(msind,setofb_its(msind,setmassindex));
   if (setofb_eq0(msind))
   {
      dellevi = leviflag;
      sum = multtwotens(t1,t2); 
      if (leviflag) dellevi = FALSE;
   } 
   else
   { 
      nmassind = 0; 
      for (i = 1; i <= 15; i++) 
         if (insetb(i,msind)) 
         { 
            ++(nmassind); 
            v = massindpos[i-1].vrt1; 
            l = massindpos[i-1].ln1;
            kmorgcalc(v,l,&sub[nmassind-1]);
            masscalc(v,l,2,&mass2[nmassind-1]);
         } 
      sum = NULL;
      lastn = (1 << nmassind) - 1; 
      for (n = 0; n <= lastn; n++) 
      { 
         if (n == lastn) 
         {
            mult_1 = t1; 
            mult_2 = t2; 
         } 
         else 
         { 
            mult_1 = copytens(t1,tensLength);
            mult_2 = copytens(t2,tensLength);
         }
         for (i = 1; i <= nmassind; i++) 
            if (((1 << (i - 1)) & n) != 0)
            { 
               q = mult_1;
               pmem = copytens(sub[i-1],tensLength);
               mult_1 = multtwotens(q,pmem); 
               q = mult_2; 
               pmem = copytens(sub[i-1],tensLength); 
               mult_2 = multtwotens(q,pmem);
               multtensint(&mult_2,-1); 
            } 
            else
               multtenspoly(&mult_1,mass2[i-1]); 
         dellevi = leviflag;
         q = multtwotens(mult_2,mult_1); 
         if (leviflag) dellevi = FALSE;
         sewtens(&sum,&q,tensLength);
      } 
      for (i = 1; i <= nmassind; i++)
      { 
         deltensor(&sub[i-1]);
         delpoly(&mass2[i-1]);
      } 
   } 
   vmin = MIN(v1,v2); 
   block[vmin-1] = sum;
   vertinfo[vmin-1].g5 = vertinfo[v1-1].g5 + vertinfo[v2-1].g5; 
   setofb_cpy(vertinfo[vmin-1].ind,
              setofb_aun(setofb_uni(ind1,ind2),
                         setofb_its(ind1,ind2)));
} 


static long  mcd(long i1,long i2)
{long  c;

   i1 = i1 > 0 ? i1 : -i1;
   i2 = i2 > 0 ? i2 : -i2;
   if (i2 > i1) { c = i1; i1 = i2; i2 = c; }
   while (i2 != 0) { c = i2; i2 = i1 % i2; i1 = c; }
   return i1;
}


static void  intfact(poly p,long* fact)
{poly        q;

   if (p == NULL)
      *fact = 1;
   else
   {
      *fact = p->coef.num > 0 ? p->coef.num : - p->coef.num ;
      q = p->next;
      while (*fact != 1 && q != NULL)
      {
         *fact = mcd(q->coef.num,*fact);
         q = q->next;
      }
      if (p->coef.num < 0) *fact = -(*fact);
   }
   if (*fact != 1)
   {
      q = p;
      while (q != NULL)
      {
         q->coef.num /= *fact;
         q = q->next;
      }
   }
}


static void  monomfact(poly p,poly* m)
{struct
   {  byte   vnum;
      byte   vdeg;
   }         factvars[200];
 byte        i, j, nv, deg;
 poly        ptmp;

   newmonom(m);
   (*m)->next = NULL;
   (*m)->coef.num = 1;
   for (i = 0; i < monomLength; i++) (*m)->tail.power[i] = 0;
   if (p == NULL) return;
   nv = 0;
   for (i = 1; i <= vardef->nvar; i++)
      if (cpos('.',vardef->vars[i-1].name) == 0)
      {
         ++(nv);
         factvars[nv-1].vnum = i;
         factvars[nv-1].vdeg = vardef->vars[i-1].maxdeg - 1;
      }
   ptmp = p;
   while (nv != 0 && ptmp != NULL)
   {
      i = 1;
      while (i <= nv)
      {
         deg = (ptmp->tail.power[vardef->vars[factvars[i-1].vnum-1].wordpos-1] /
                vardef->vars[factvars[i-1].vnum-1].zerodeg) %
               vardef->vars[factvars[i-1].vnum-1].maxdeg;
         if (deg == 0)
         {
            --(nv);
            for (j = i; j <= nv; j++)
               factvars[j-1] = factvars[j + 1-1];
         }
         else
         {
            if (deg < factvars[i-1].vdeg)
               factvars[i-1].vdeg = deg;
            ++(i);
         }
      }   /*  with ??? V.E. */
      ptmp = ptmp->next;
   }

   for (i = 1; i <= nv; i++)
   {
      j = factvars[i-1].vnum;
      (*m)->tail.power[vardef->vars[j-1].wordpos-1] +=
         vardef->vars[j-1].zerodeg * factvars[i-1].vdeg;
   }
   ptmp = p;
   while (ptmp != NULL)
   {
      for (i=0;i<monomLength;i++) ptmp->tail.power[i] -= (*m)->tail.power[i];
      ptmp = ptmp->next;
   }
   intfact(p,&((*m)->coef.num));
}  /* MonomFact */


static void  del_pp(poly* p,long * del)
{byte        d, d1, n, i, j;
 unsigned long		z_d, m_d;
 long     m, l, k;
 poly        q, t, tt, psub;
 integer     del0, del_p;

   *del = 1;
   if (*p == NULL) return;

   n = nin + nout;
   psub = scalarmult(-n,-n);
   multpolyint(&psub,-1);
   for (i = 1; i <= n - 1; i++)
   {
      q = scalarmult(-i,-i);
      sewpoly(&psub,&q);
   }

   for (i = 2; i <= n - 1; i++)
      for (j = 1; j <= i - 1; j++)
         if (j != n - 2)
         {
            q = scalarmult(-i,-j);
            if (i > nin && j <= nin)
               multpolyint(&q,-2);
            else
               multpolyint(&q,2);
            sewpoly(&psub,&q);
         }

   if (nout > 2) multpolyint(&psub,-1);

   intfact(psub,&m);
   l = mcd(m,2);
   if (l == 2)
   {  multpolyint(&psub,m / 2);
      del0 = 1;
   }
   else
   {  multpolyint(&psub,m);
      del0 = 2;
   }


   z_d = vardef->vars[0].zerodeg;
   m_d = vardef->vars[0].maxdeg;

   newmonom(&q);
   q->next = NULL;
   q->coef.ptr = *p;
   d1 = ((*p)->tail.power[0] / z_d) % m_d;
   q->tail.power[0] = d1;
   while (d1 != 0)
   {
      (*p)->tail.power[0] -= d1 * z_d;
      t = *p;
      *p = (*p)->next;
      if (*p == NULL) goto label_1;
      d = ((*p)->tail.power[0] / z_d) % m_d;
      if (d != d1)
      {
         t->next = NULL;
         t = q;
         newmonom(&q);
         q->coef.ptr = *p;
         q->next = t;
         q->tail.power[0] = d;
         d1 = d;
      }
   }

label_1:
   *p = NULL;
   t = plusone();
   d = 0;
   del_p = 1;
   do
	{

      d1 = q->tail.power[0];
      for (i = d + 1; i <= d1; i++)
      {  t = multtwopoly(t,psub);
         del_p *= del0;
      }
		intfact(q->coef.ptr,&m);
		tt = multtwopoly(q->coef.ptr,t);
      delpoly(&q->coef.ptr);

      l = mcd(m,del_p);
      m /= l;
      k = del_p / l;

      l = mcd(k,*del);
      multpolyint(p,k / l);
		*del /= l;
		multpolyint(&tt,(*del) * m);
		*del *= k;
      sewpoly(p,&tt);
      d = d1;
      q = q->next;
   }  while (q != NULL);
   delpoly(&t);
	delpoly(&q);
}


static void  transformfactor(rmptr* t_fact,poly mon,long del)
{byte        i, deg;
 rmptr       vard;
 byte        v, l, n, s, pnum;
 rmptr       tf_add;
 integer     c;
 long     factnum, factdenum;
 char        factortxt[STRSIZ];
 preres      m;
 poly        mm;

   sbld(factortxt,"%ld",mon->coef.num);
   for (i = 1; i <= vardef->nvar; i++)
   {
      deg = (mon->tail.power[vardef->vars[i-1].wordpos-1] /
             vardef->vars[i-1].zerodeg) %
            vardef->vars[i-1].maxdeg;
      if (deg != 0)
      {
         sbld(factortxt,"%s*%s",factortxt,vardef->vars[i-1].name);
         if (deg > 1) sbld(factortxt,"%s**%s",factortxt,funstr(deg));
      }
   }
   vard = (rmptr) read_rmonom(factortxt);
   mult_rptr(t_fact,&vard);

/* ------- Symmetry and Color  Factors ------- */
   factnum = vcs.symnum * vcs.clrnum;
   factdenum = vcs.symdenum * vcs.clrdenum * del;
/* -----  average factor  --------- */
   c = 1;
   for (v = 1; v <= vcs.sizel; v++)
      for (l = 1; l <= vcs.valence[v-1]; l++)
         if (inset(inp,vcs.vertlist[v-1][l-1].prop))
         {
            pnum = vcs.vertlist[v-1][l-1].partcl;
            s = prtclbase[pnum-1].spin;
            switch (s)
            {
               case 1:
                  if (!(prtclbase[pnum-1].hlp == 'L' ||
                        prtclbase[pnum-1].hlp == 'R' ))
                     c *= 2;
               break;

               case 2:
                  if (zeromass(pnum))
                     c *= 2;
                  else
                     c *= 3;
            }   /*  Case  */
            c *= abs(prtclbase[pnum-1].cdim);
         }
   factdenum *= c;

/* ----- Fermion factor  --------- */
   c = 0;
   for (v = 1; v <= vcs.sizel; v++)
      for (l = 1; l <= vcs.valence[v-1]; l++)
      {
         pnum = vcs.vertlist[v-1][l-1].partcl;
         if (prtclbase[pnum-1].spin == 1 &&
             inset(inp,vcs.vertlist[v-1][l-1].prop))
            ++(c);
      }
   c = (c & 1) == 1 ? -1 : 1;
   for (v = 1; v <= nloop; v++) c *= -4;
   factnum *= c;

/* ----- Vector/left spinor  factor  --------- */
   for (v = 1; v <= vcs.sizet; v++)
      for (l = 1; l <= vcs.valence[v-1]; l++)
         if (vcs.vertlist[v-1][l-1].moment > 0)
         {
            pnum = vcs.vertlist[v-1][l-1].partcl;
/*            if (prtclbase[pnum-1].spin == 2)
               factnum = -factnum;
            else
*/               if (prtclbase[pnum-1].hlp == 'L' ||
                   prtclbase[pnum-1].hlp == 'R' )
                  factdenum *= 2;
         }
/*                     */

/* ----------- end of numeric factors collecting--------------- */
   sbld(factortxt,"%ld",factnum);
   tf_add = (rmptr) read_rmonom(factortxt);
   mult_rptr(t_fact,&tf_add);

   sbld(factortxt,"%ld",factdenum);

   for (v = 1; v <= vcs.sizet; v++)
   {
      for (l = 1; l <= vcs.valence[v-1]; l++)
      {edgeinvert *with1 = &vcs.vertlist[v-1][l-1];
         if (with1->moment > 0 &&
             (pseudop(with1->partcl) ||
				  insetb(with1->lorentz,setmassindex0)))
            sbld(factortxt,"%s*%s**2",
                 factortxt,prtclbase[with1->partcl-1].massidnt);
      }
   }
   sbld(factortxt,"1/(%s)",factortxt);
   tf_add = (rmptr) read_rmonom(factortxt);
   mult_rptr(t_fact,&tf_add);

   vardef++;
   vardef->nvar = 0;
   pregarbage = NULL;
   m = (preres)readexprassion(rmonomtxt(**t_fact),bact,uact,rd);
   if (rderrcode != 0) save_sos(rderrcode);
   for (n = 1; n <= vardef->nvar; n++)
      vardef->vars[n-1].maxdeg = m->varsdeg[n-1] + 1;
   clearpregarbage();
   closevars();
   tensLength = 0;
   maxIndex=0;
   spinLength = 0;
   maxLength = MAX(MAX(tensLength,spinLength),monomLength);
   mm = (poly)readexprassion(rmonomtxt(**t_fact),bact_,uact_,rd_);
   if (mm->coef.num == rationtp)
   {  factn = mm->next->coef.ptr;
      factd = mm->next->next;
      delmonom(&mm->next);
      delmonom(&mm);
   }
   else
   {  factn = mm->next;
      factd = plusone();
      delmonom(&mm);
   }
   clrvm((*t_fact)->n.v);
   clrvm((*t_fact)->d.v);
   free(*t_fact);
}


static boolean  equal(poly p1,poly p2)
{poly     q1, q2;
 boolean  eqn;

   q1 = copypoly(p1);
   q2 = copypoly(p2);
   multpolyint(&q1,-1);
   sewpoly(&q1,&q2);
   eqn = (q1 == NULL);
   delpoly(&q1);
   delpoly(&q2);
   return eqn;
}


static byte  nincount(byte v,byte l)
{byte  i, vv, ll, summ;

   if (inset(inp,vcs.vertlist[v-1][l-1].prop)) return 1;
   if (inset(intrp,vcs.vertlist[v-1][l-1].prop)) return 0;
   summ = 0;
   vv = vcs.vertlist[v-1][l-1].nextvert.vno;
   ll = vcs.vertlist[v-1][l-1].nextvert.edno;
   for (i = 1; i <= vcs.valence[vv-1]; i++)
      if (i != ll)
         summ += nincount(vv,i);
   return summ;
}


static boolean  ttypepropag(byte v,byte l)
{
   if (nin == 1) return FALSE;
   return (nincount(v,l) == 1);
}


static void  calcdenominators(void)
{byte  v, l, k;
 poly  pp, mm;

   findinternalmoments(FALSE);
   vardef++;
   vardef->nvar = 0;
   maxmomdeg = 2;
   addinoutmasses();

   for (v = 1; v <= vcs.sizet; v++)
      for (l = 1; l <= vcs.valence[v-1]; l++)
      {edgeinvert *with1 = &vcs.vertlist[v-1][l-1];
         if (with1->moment > 0 && 
             (with1->prop & setof(inp,intrp,_E)) == setof(_E) &&
             strcmp(prtclbase[with1->partcl-1].massidnt,"0") != 0)
				addvar(prtclbase[with1->partcl-1].massidnt,2);
      } 
   tensLength = 0;
   maxIndex=0;
   spinLength = 0;
   addscmult(); 
   denrno = 0;
   for (v = 1; v <= vcs.sizet; v++) 
      for (l = 1; l <= vcs.valence[v-1]; l++)
      {edgeinvert *with1 = &vcs.vertlist[v-1][l-1]; 
         if (with1->moment > 0 && 
             (with1->prop & setof(inp,intrp,_E)) == setof(_E) && 
             !pseudop(with1->partcl))
         {
            ++(denrno); 
            pp = scalarmult(-with1->moment,-with1->moment); 
            multpolyint(&pp,-1);
            masscalc(v,l,2,&mm); 
            sewpoly(&pp,&mm); 
            denr[denrno-1] = pp; 
            denrdeg[denrno-1] = 1; 
            denrwidth[denrno-1][0] =
               calcvarpos(prtclbase[with1->partcl-1].massidnt); 
            denrwidth[denrno-1][1] =
               calcvarpos(prtclbase[with1->partcl-1].imassidnt); 

            if (denrwidth[denrno-1][1] != 0 && ttypepropag(v,l)) 
               denrwidth[denrno-1][1] = 0;
/*
            if (denrwidth[denrno-1][0] > denrwidth[denrno-1][1]) 
            {
               k = denrwidth[denrno-1][1]; 
               denrwidth[denrno-1][1] = denrwidth[denrno-1][0]; 
               denrwidth[denrno-1][0] = k;
            } 
*/            
            for (k = 1; k <= denrno - 1; k++) 
               if (denrwidth[denrno-1][0] == denrwidth[k-1][0] &&
                   denrwidth[denrno-1][1] == denrwidth[k-1][1] && 
                   equal(denr[denrno-1],denr[k-1]))
               { 
                  ++(denrdeg[k-1]);
                  delpoly(&denr[denrno-1]); 
                  --(denrno);
                  goto label_1;
               } 
label_1:;} 
      }
}


static void  formblocks(void)
{byte  v, vv, l, lori, v1, v2, count; 
 byte  vertmap[2 * maxvert];

   for (v = 1; v <= 2 * maxvert; v++)
      vertmap[v-1] = fermmap[v-1];   /*  copy fermMap  */
   for (count = 1; count <= nloop; count++)
      block[count-1] = fermres[count-1];

   count = nloop; 
   for (v = 1; v <= vcs.sizet; v++)  /*  simple  vertex  */ 
      if (vertmap[v-1] == 0) 
      {
         vertmap[v-1] = ++count; 
         block[count-1] = vert[v-1]; 
      }   /*  if  VertMap[v]=0   */  /*  end of vertex filling  */
/* ----------------------------- */ 
                                     /*  Begin of Blk filling  */
   n_vrt = count; 
   for (v = 1; v <= count; v++) 
   {
      vertinfo[v-1].vlnc = 0; 
      vertinfo[v-1].weit = 1;
      setofb_zero(vertinfo[v-1].ind);
      vertinfo[v-1].g5 = 0;
   } 
   
   for (v = 1; v <= vcs.sizet; v++) 
   { 
      v1 = vertmap[v-1];
      if (v1 <= nloop) 
      { 
        vertinfo[v1-1].weit += 2;
        if (fermloops[v1-1].g5) ++(vertinfo[v1-1].weit); 
      } 

      for (l = 1; l <= vcs.valence[v-1]; l++)
      {
         vv = vcs.vertlist[v-1][l-1].nextvert.vno;
         v2 = vertmap[vv-1];
         lori = vcs.vertlist[v-1][l-1].lorentz; 
         if (lori != 0)
         {
            if (v1 != v2)
            { int np;
               ++(vertinfo[v1-1].vlnc);
               vertinfo[v1-1].link[vertinfo[v1-1].vlnc-1] = v2;
               setofb_cpy(vertinfo[v1-1].ind,
                    setofb_uni(vertinfo[v1-1].ind,setofb(lori,_E)));
              np = vcs.vertlist[v-1][l-1].partcl;
              if ( prtclbase[np-1].hlp == 't')
               { 
                  ++(vertinfo[v1-1].vlnc);
                  vertinfo[v1-1].link[vertinfo[v1-1].vlnc-1] = v2;
                  setofb_cpy(vertinfo[v1-1].ind,
                  setofb_uni(vertinfo[v1-1].ind,setofb(lori-1,_E)));
              }
            }
            vertinfo[v1-1].weit += 2;
            if (insetb(lori,setmassindex)) ++(vertinfo[v1-1].weit);
         }
      }
   }
   for (v = 1; v <= nloop; v++)
      if (fermloops[v-1].g5)
         vertinfo[v-1].g5 = 1;

   if (inset(memory,options))
   {
      for (v = 1; v <= n_vrt; v++)
         vertinfo[v-1].weit = 0;
      for (v = 1; v <= vcs.sizet; v++)
         ++(vertinfo[vertmap[v-1]-1].weit);
   }
                                     /*   End of Blk filling  */
} /*  FormBlocks  */


static void  delImageryOne(poly *  rnum,rmptr   t_factor)
{
  vmrec       rec;
  vmptr       m, m1;
  int i ;

  word w_p,m_d,z_d,deg,deg_,fdeg;
  poly q,p,pp;

	rec.next =(t_factor->n).v;
   m = &rec;
	m1 = m->next;
	while (m1 != NULL && (strcmp(m1->name,"i") != 0 ) )
	{
      m = m1;
      m1 = m1->next;
   }

	if (m1 != NULL )
	{
		m->next = m1->next;
		free(m1);
		m1 = m->next;
		fdeg=1;
		(t_factor->n).v=rec.next;
   }
	else 	fdeg=0;

	i=0;
	while ((i<vardef->nvar) && ( strcmp(vardef->vars[i].name, "i") !=0 ) )i++;
	if (i == vardef->nvar)
	{ if (fdeg==1) delpoly(rnum);
	}
	 else
	{
		w_p=vardef->vars[i].wordpos;
		z_d=vardef->vars[i].zerodeg;
		m_d=vardef->vars[i].maxdeg ;
		q=NULL;
		p=(*rnum);
		while (p != NULL)
		{
			pp=p;
			p=p->next;

			deg=(pp->tail.power[w_p-1] / z_d) % m_d;
			deg_=deg+fdeg;
			if ( (deg_ & 1 ) == 1 )  delmonom(&pp); else
			{
				if ( ((deg_/2) & 1 ) == 1 ) pp->coef.num= - pp->coef.num;
				if (deg != 0 ) pp->tail.power[w_p-1] -= z_d*deg;
				pp->next=NULL;
				sewpoly(&q,&pp);
			}
		}
		*rnum=q;
	}

}


static void  symbcalc(hlpcsptr ghst)
{vcsect       vcs_copy;
 hlpcsptr     gstcopy;
 byte         spinl_s, maxmom_s;
 boolean      live_s;
 boolean      first;
 long		 del;
 int          i,j;
 s_listptr    d_facts, df;
 rmptr        t_fact;
 vmptr        coefvar;
 poly         mfact, mon;
 char         ssss[STRSIZ];
  polyvars *vardef_s;

   goto_xy(14,15); clr_eol();
   wrtoperat("Factors normalization");
   diagramsrfactors(ghst,&d_facts,&t_fact);
   wrtoperat("Preparing for calculation");
   vcs_copy = vcs;
   first = TRUE;

   gstcopy = ghst;
   df = d_facts;
   do
   {  
      coloringvcs(ghst);
      attachvertexes();
      firstvertexreading();
      propagatorsfirstreading();
      calcspinlength();
      coefvar = df->monom.v;
      while (coefvar != NULL)
      {
         addvar(coefvar->name,coefvar->deg);
         coefvar = coefvar->next;
      }
      if (first)
      {
         vardef_s = vardef;
         vardef++;
         spinl_s = spinLength;
         maxmom_s = maxmomdeg;
         live_s = levi;
         first = FALSE;
      }
      else
      {
         unite_vardef(vardef_s,vardef);
         spinl_s = MAX(spinLength,spinl_s);
         maxmom_s = MAX(maxmomdeg,maxmom_s);
         live_s = levi || live_s;
      }

      vcs = vcs_copy;
      df = df->next;
      ghst = ghst->next;
   }  while (ghst != NULL);
   vardef = vardef_s;
   spinLength = spinl_s;
   maxmomdeg = maxmom_s;
   levi = live_s;
#ifdef STRACE 
tracePrn("\n spinLength= %d",spinLength);
#endif
   calctenslength();
   addinoutmasses();
   addscmult();
   ghst = gstcopy;
   df = d_facts;

   rnum = NULL;

   do
	{  if (screenOutput == 1)
		{
			goto_xy(14,15);
			print("%u(of %u)  ",ghst->num,ghst->maxnum);
		}
      coloringvcs(ghst);
      attachvertexes();
      findReversVert();
      secondvertexreading();
      wrtoperat("Fermion loops calculation  ");
      calcfermloops();
      n_g5 = 0;
      for (i = 1; i <= nloop; i++)
         if (fermloops[i-1].g5)
            ++(n_g5);
      formblocks();
      makeprgcode();
#ifdef STRACE
tracePrn("n_vrt= %d",n_vrt);
for (i = 0;i<=n_vrt-1;i++)
{
   tracePrn("vrt=%d",i);
   tracePrn("\n");
   writetens(block[i]);
}
#endif
      for (i = n_vrt - 1; i >= 1; i--)
      {
#ifdef STRACE
tracePrn(" multiplication level %d ",i+1);
tracePrn("\n T1= ");
writetens(block[prgcode[i-1][0]-1]);
tracePrn("\n T2= ");
writetens(block[prgcode[i-1][1]-1]);
#endif
          multblocks(prgcode[i-1][0],prgcode[i-1][1]);
#ifdef STRACE
tracePrn("\n result=");
writetens(block[MIN(prgcode[i-1][0],prgcode[i-1][1])-1]);
#endif
}
      {int mom, np;
       poly pp,mm;
          for (i = 0; i < vcs.sizet; i++)
          for (j = 0; j < vcs.valence[i]; j++)
          {
	     mom=vcs.vertlist[i][j].moment;
	     np=vcs.vertlist[i][j].partcl;
             if ((mom >0)&& (prtclbase[np-1].hlp == 't') )
             {   masscalc(i+1,j+1,2,&mm);
                 if (prtclbase[ghostmother(np)-1].hlp=='*')
                 {  multtenspoly(&block[0],mm);
		    delpoly(&mm);                 
                 }
                 else
                 {  pp = copypoly(scalarmult(-mom,-mom));
                    multpolyint(&pp,-1);
                    sewpoly(&pp,&mm);
                    multtenspoly(&block[0],pp);
		    delpoly(&pp);
		 }
		 if ( insetb(vcs.vertlist[i][j].lorentz,setmassindex0))
	         {
	            masscalc(i+1,j+1,2,&mm);
		    multtenspoly(&block[0],mm);
		    delpoly(&mm);
		 }
             }
          }
      }


      if (ghst->sgn != 1)
         multtensint(&block[0],ghst->sgn);

      if (block[0] != NULL)
      {
         strcpy(ssss,smonomtxt(df->monom));
         mfact = (poly) readexprassion(smonomtxt(df->monom),
                                       bact_,uact_,rd_);
         multtenspoly(&block[0],mfact->next);
         delmonom(&mfact);
         sewpoly(&rnum,&block[0]->coef.ptr);
         delmonom(&block[0]);
      }

      vcs = vcs_copy;
      df = df->next;
      ghst = ghst->next;
   }  while (ghst != NULL);
   eraseslist(d_facts);

/* DelSqrt2(Rnum); */

	delImageryOne(&rnum,t_fact);

   if (inset(conslow,options))
      del_pp(&rnum,&del);
   else
      del = 1;

   wrtoperat("Total factor calculation");
	monomfact(rnum,&mon);
   transformfactor(&t_fact,mon,del);

   wrtoperat("Denominator calculation");
   calcdenominators();

}


static void  calcproc(csdiagram* csdiagr)
{hlpcsptr    gstlist;

   transfdiagr(csdiagr,&vcs);
   cwtarg(&vcs);
   if (vcs.clrnum == 0)
      csdiagr->status = 2;
   else
   {
      generateghosts(&vcs,&gstlist);
      if (gstlist == NULL)
         csdiagr->status = 2;
      else
      {
         preperdiagram();
         symbcalc(gstlist);
         csdiagr->status = 1;
      }
      eraseghosts(gstlist);
   }
}


static void  writestatistic(word noutmemtot,char* txt)
{
	if (starPressed())
	{  if (screenOutput == 1)
	  {  screenOutput=0;
		  goto_xy(1,12);  clr_eol();
		  goto_xy(1,13);  clr_eol();
		  goto_xy(17,14); clr_eol();
		  goto_xy(41,14); clr_eol();
	  }
	  else screenOutput=1;
	}
	if ( screenOutput == 1 )
	{
		goto_xy(1,12);  print("%u\n",calcdiag_sq);
		goto_xy(1,13);  print("%u",noutmemtot);
		goto_xy(17,14); print("%4d",ndiagr);
		goto_xy(41,14); print("%s      ",txt);
		goto_xy(14,17); clr_eol();
	}

}


static void heap_is_empty(void)
{
	 save_sos(-1);
}

static void addActivVar(setofbyte varset, polyvars * pv )
{ int k,i;  
  for(k=0;k<3;k++)
  { 
     for (i = 1; i <= pv->nvar; i++)
     if ( strcmp(pv->vars[i-1].name,"i")!=0) setofb_add1(varset,pv->vars[i-1].num);
     pv++;
  }
  for (i = 1; i <= denrno; i++) for (k=1; k>=0;k--)
  if(denrwidth[i-1][k] != 0) setofb_add1(varset,denrwidth[i-1][k]);
}

void  calcallproc(void)
{  word         ndel, ncalc, nrest, nrecord;
   csdiagram    csd;
   word         noutmemtot;
   shortstr     txt;
   FILE * actvars;
   marktp    heap_beg;

   setofbyte varset;
   polyvars  varsInfo[3];
   
   memerror=heap_is_empty;
   memoryInfo=memoryInfo_;

   goto_xy(1,13);
   scrcolor(White,Black);

   print("0     Out of memory\n");
   print("current diagram          in (Sub)process\n");
   print("Subdiagram  :\n");
   print("Used memory :%3d Kb       \n",(int)(usedmemory/1000));
   print("Operation   :\n");
   scrcolor(Yellow,Blue);
   print("\n");
   print(" Press Esc to halt calculations \n");
   
   screenOutput=1;
   scrcolor(Yellow,Black);

   actvars = fopen(ACTVARS_NAME,"r+b");
   if(actvars==NULL) actvars = fopen(ACTVARS_NAME,"w+b");
   
   catalog = fopen(CATALOG_NAME,"ab");
   archiv  = fopen(ARCHIV_NAME,"ab"); 

   calcdiag_sq = 0;
   noutmemtot = 0;
   diagrq=fopen(DIAGRQ_NAME,"rb");
   while(FREAD1(csd,diagrq))
   {
      switch (csd.status)
      {
         case 1:  ++(calcdiag_sq);  break;
         case -2: ++(noutmemtot);    break;
         case 2:  ++(calcdiag_sq);
      }
   }
   fclose(diagrq);
   diagrq=fopen(DIAGRQ_NAME,"r+b");
   menuq=fopen(MENUQ_NAME,"r+b");
   for(nsub=1;nsub<=subproc_f;nsub++)
   {int naux;  
      fseek(actvars,(nsub-1)*sizeof(setofbyte),SEEK_SET);
      if(!FREAD1(varset,actvars))  setofb_zero(varset);
      rd_menu(2,nsub,txt,&ndel,&ncalc,&nrest,&nrecord);
      naux = ndel + ncalc + nrest;
      for (ndiagr = 1; ndiagr <= naux; ndiagr++)
      {
         writestatistic(noutmemtot,txt);
         fseek(diagrq,sizeof(csd)*(nrecord+ndiagr-1),SEEK_SET)  ;
         FREAD1(csd,diagrq);
         if (csd.status == 0)
         {  
            vardef=&(varsInfo[0]);
            mark_(&heap_beg);
            calcproc(&csd);
            if (csd.status == 1)
            {
                fseek(diagrq,sizeof(csd)*(nrecord+ndiagr-1),SEEK_SET);
                FWRITE1(csd,diagrq);
                addActivVar(varset,varsInfo);
                fseek(actvars,(nsub-1)*sizeof(setofbyte),SEEK_SET);
                FWRITE1(varset,actvars); 
                wrtoperat("Writing result             ");
                vardef=&(varsInfo[0]);             
                saveanaliticresult();
            }
            release_(&heap_beg);                     
            ncalc++;
            nrest--;
            calcdiag_sq++;            
            wrt_menu(2,nsub,txt,ndel,ncalc,nrest,nrecord);
            if (escpressed()) goto exi;
         }         
      }
   }
exi:
   if (totdiag_sq - deldiag_sq != calcdiag_sq)
        messanykey(10,15,"Not all diagrams are calculated$");

   fclose(diagrq);
   fclose(menuq);
   
   fclose(catalog);
   fclose(archiv);   
   fclose(actvars);
   
   scrcolor(White,Black);
   clrbox(1,14,70,20);
   memerror=NULL;
}  /* CalcAllProc  */
