/*     main/dalitz.c

 +---------------------------------+
 |     D A L I T Z    PLOTTING     |
 |     S.Shichanin                 |
 |                                 |
 |     June 24 1995  revision 1.1  |
 |     June 26 1995  revision 1.2  |
 |                  ( rsqrt added - no shifts in limits)
 |                  ( some interactive errors corrected)
 |     June 26 1995  revision 2.0  |
 |                  ( order added + control corrections)
 |     June 27 1995  revision 2.0.1|
 |                  ( cuts    corrections)
 |     June 29 1995  revision 2.1  |
 |                  ( order used! )
 |     Sept 15 1995  revision 2.2  |
 |                  ( menus revised)
 |                                 |
 +---------------------------------+
*/ 

#include <math.h>
#include "tptcmac.h"
#include "os.h"
#include "syst2.h"
#include "global.h"
#include "crt.h"
#include "screen.h"
#include "physics.h"
#include "crt_util.h"
#include "optimise.h"
#include "n_compil.h"
#include "num_tool.h"
#include "num_serv.h"
#include "showgrph.h"
#include "graf.h"
#include "width_13.h"
#include "tex_out.h"
#define extern /* globals defined here */
#include "dalitz.h"
#undef extern


static int  strmenu(int  x, int  y, char*  s, int*  k)
{ word     theight, width1, width2;
  int      i, n, npos = 0, key, i1, i2, x1, x2, y1, y2;
  int      koord[8];
  char     ss[STRSIZ], *p;
  int      fgcolor = Black,  bkcolor = White; 

   tg_settextjustify(LeftText,TopText);

   theight = y + tg_textheight(s) + 3;
   width1  = tg_textwidth("O");
   width2  = width1/2;

   p = s;
   while (*p != '\0') if (*p++ == '$') npos++;
   if (*k < 0 || *k > npos) *k = 0;

   x1 = x - 2;
   x2 = x + tg_textwidth(s) + width1 + 2;
   y1 = y - 1;
   y2 = theight + 1;

   scrcolor(fgcolor,bkcolor);
rewrt:

   n = npos;
   tg_bar(x1,y1,x2,y2);
   strcpy(ss + 1,s);
   ss[0] = '$';

   while ((p = strrchr(ss,'$')) != NULL)
   {
      *p++ = '\0';
      i1 = x + (int)strlen(ss)*width1;
      i2 = i1 + ((int)strlen(p) + 1)*width1;
      koord[n] = i2;
      tg_outtextxy(i1 + width2, y + 1, p);
      if (*k == n--)
         tg_setlinestyle(SolidLn,ThickWidth);
      tg_rectangle(i1,y,i2,theight);
      tg_setlinestyle(SolidLn,NormWidth);
   }
   koord[npos] = x2;
   
   do
   {
      key = inkey();
      
      if (key == KB_MOUSE && 
          mouse_info.but1 == 2 &&
          mouse_info.y >= y1 &&
          mouse_info.y <= y2 &&
          mouse_info.x >= x1 &&
          mouse_info.x <  x2
         )
      {  for(i = 0; koord[i] < mouse_info.x;) i++;
         if (i == *k)
            return KB_ENTER;
         *k = i;
         goto rewrt;
      }
      
      switch (key)
      {
         case KB_ESC:
            *k = 0;
            key = KB_ENTER;
         break;

         case KB_RIGHT:
            ++(*k);
            if (*k > npos) *k = 0;
            goto rewrt;

         case KB_LEFT:
            --(*k);
            if (*k < 0) *k = npos;
            goto rewrt;

         case KB_SIZE:
            return KB_SIZE;
      }
   }  while (key != KB_ENTER);
   return key;
}



void  gpixel(double  x, double  y)
{ 
   tg_line(scx(x),scy(y),scx(x)+1,scy(y));
} 


static integer    fgcolor = Black/* ,  bkcolor = White*/;


/*  $I CALC\Plot */ 
/*
     Plotting: Made by S.A.Shichanin
               June  10 1991
               April 09 1992
               May   12 1992
in c (after V.F.Edneral):
               June  12 1995 
************************************/ 
typedef struct cellrec 
{ 
   struct cellrec *next; 
   integer         i, j; 
   double          f; 
}  cellrec;

typedef struct cellrec *cell; 

static unsigned int iseed=1;
static double       xcrest, ycrest;


static double       dx, dy; 
static double              xxmin,xxmax,yymin,yymax; 
static cell         cellist, cellptr; 
static boolean      first, done;     
static double       sum, fsum;      
            
static word         ntotal;       

static double ch_random(void)
{ 
    return  (double)rand()/(double)RAND_MAX ;
}    

void  setlimits(void)
{ 
   xxmin = xdown;
   xxmax = xup;
   yymin = ydown;
   yymax = yup;
   scalee = 0; 

   grafminmax.xmin = xdown;
   grafminmax.xmax = xup ;
   grafminmax.ymin = ydown;
   grafminmax.ymax = yup ;
   /*
   grafminmax.xmin = xdown - 0.01 * (xup - xdown); 
   grafminmax.xmax = xup   + 0.01 * (xup - xdown); 
   grafminmax.ymin = ydown - 0.01 * (yup - ydown); 
   grafminmax.ymax = yup   + 0.01 * (yup - ydown); 
   */
}


static void dlimits(double x,double ma,double mb,double* ylow,double* ygr)
{
   double a,b,d;
   double aa,bb,cc,disc;

   a=m0-x;
   d= x*x-ma;
   b=mm2+ma-mm0-mb+2*a*x;
   aa=4*a*a-4*d;
   bb=4*a*b+8*a*d;
   cc=b*b-4*d*(a*a-mm2);
   if(aa == 0.0) 
      {
      *ylow = *ygr = -cc/bb;
      return;
      }
   disc=bb*bb-4*aa*cc;
   *ygr = (-bb+rsqrt(disc)) / (2*aa);
   *ylow= (-bb-rsqrt(disc)) / (2*aa);
   }
/* E3 max of E1 */
double asy1(double  x)
{
  double ylow,ygr;
  dlimits(x,mm1,mm3,&ylow,&ygr);
  return ygr;
  }
double  sy1(double  x)
  {
  return asy1(x);}

/* E3 min of E1 */

double asy2(double  x)
{
  double ylow,ygr;
  dlimits(x,mm1,mm3,&ylow,&ygr);
  return ylow;
  }
double  sy2(double  x)
  {return MAX(asy2(x),c3);}

/* E1 max of E3 */

double asx1(double  x)
{
  double xlow,xgr;
  dlimits(x,mm3,mm1,&xlow,&xgr);
  return xgr;
  }
static  double  sx1(double  x)
  {return MIN(asx1(x),xup);}

/* E1 min of E3 */

double asx2(double  x)
{
  double ylow,ygr;
  dlimits(x,mm3,mm1,&ylow,&ygr);
  return ylow;
  }
static double  sx2(double  x)
  {return MAX(asx2(x),c1);}

static boolean  inellips(double  x, double   y)
{double  d, u; 

   if (x < xdown || x > xup) 
      return FALSE;

   d = sy2(x); 
   u = sy1(x); 
   return y > u || y < d ? FALSE : TRUE; 
} 
#ifdef  NOT_USED 
static double func(double x,int kind_of_function)
{
   switch(kind_of_function)
   {
   case 1:
      return dgamma_dx(x);
      break;
   case 2:
      ext_x=x;
      break;
   case 3:
      ext_y=x;
   }
   return sfunc();
}

static void loopegrated_on_m34(void)
{realseq     r_, rr_; 
 word        count; 
 byte        i; 

 double      sup, sd, step, xx; 
 int         k; 
 long        npoints=100; 
 char        astr[21], bstr[21], buf2[82]; 
 char        upstr[82]; 

   r_ = NULL; 
   integration = FALSE;   /* !!! Namely FALSE!!! */ 
   setlimits(); 
   npoints = 100; 
   sbld(astr,"%11.6f",xdown); 
   sbld(bstr,"%11.6f",xup); 

   sd=xdown;
   sup=xup;
label2: 
   if(!correctDoubleS(1,24,scat("From  E(%s), [GeV] = ",pname[order[1]]),&sd)) 
      goto exi;
      sd=sd*1.000000001;
   if (sd < xdown || sd > xup) 
   { 
      messanykey(10,10,scat("Range is between$%s and %s$",astr,bstr)); 
      goto label2;
   } 
   sbld(astr,"%11.6f",sd); 

label3:
   if (!correctDoubleS(45,21,scat("To E(%s), [GeV] = ",
             pname[order[1]]),&sup))
      goto label2;
      sup=sup*0.999999999;
   if (sup < sd || sup > xup) 
    { 
     messanykey(10,10,scat("Range is between$%s and %s$",astr,bstr)); 
     goto label3;
    } 


   clr_eol(); 
   if (!correctLong(1,22,"Points         : ",&npoints,0))
      goto label3;
/* ----   PICTURE   ---- */ 
   goto_xy(15,23); 
   scrcolor(Black,Red); 
   for (i = 1; i <= 50; i++) print("%c",' ');
   goto_xy(15,23); 
   count = 0; 
/* --------------------- */ 
   step = (sup - sd) / npoints; 
   xx = sd; 
   count = 0; 
   do 
   { 
      rr_ = (realseq)getmem(sizeof(realseqrec));
      rr_->next = r_; 
      rr_->x = xx; 
      rr_->y = dgamma_dx(xx);
      if (err_code != 0) 
      { 
         errormessage(); 
         if (err_code == 4) 
         { 
            err_code = 0; 
            goto exi;
         } 
         err_code = 0; 
      } 
      if (rr_->y < 0.0)
         rr_->y = 1.E-36; 
      r_ = rr_; 
      /*-------------- print XXXXXXXXXXXX ---------*/
      ++count; 
      i = 15 + (50 * count) / npoints; 
      scrcolor(Black,LightGray); 
      while (where_x() < i) print("%c",'X'); 
      xx += step; 
      /*-------------- print XXXXXXXXXXXX ---------*/
   }  while (count != npoints); 

   if (r_ != NULL && r_->next != NULL) 
   { 
      scrcolor(White,Black); goto_xy(1,21); clr_eol(); 
      scrcolor(White,Black); goto_xy(1,22); clr_eol(); 
      scrcolor(White,Black); goto_xy(1,23); clr_eol(); 
      sbld(upstr,"Process: %s",processstr); 
      sbld(buf2,"dGamma/dE(%s) ", pname[order[1]]);

      k = 1; 
      do 
      { void * pscr = NULL; 
         dalitzinformation();
         menu1(56,8,"","\026"
             " View plot            "
             " Save result in a file", "",&pscr,&k); 
         switch (k) 
         {
            case 1:
               plot_2(FALSE,r_,upstr,
               scat("Energy (%s), GeV", pname[order[1]]),
			buf2); 
               break; 
       
            case 2:
               writetable1(&r_,upstr,
                scat("E(%s), GeV",pname[order[1]]), buf2); 
         }   /*  Case  */ 
         dalitzinformation();
      }  while (k != 0); 
   } 
   
exi: 
   if (r_ != NULL) disposerealseq(&r_); 
} 
#endif

void  integrated_on_m34(void)
{realseq     r_, rr_; 
 word        count; 
 byte        i; 

 double      sup, sd, step, xx; 
 int         k; 
 long        npoints=100; 
 char        astr[21], bstr[21], buf2[82]; 
 char        upstr[82]; 

   r_ = NULL; 
   integration = FALSE;   /* !!! Namely FALSE!!! */ 
   setlimits(); 
   npoints = 100; 
   sbld(astr,"%11.6f",xdown); 
   sbld(bstr,"%11.6f",xup); 

   sd=xdown;
   sup=xup;
label2: 
   if(!correctDoubleS(1,21,scat("From  E(%s), [GeV] = ",pname[order[1]]),&sd)) 
      goto exi;
      sd=sd*1.000000001;
   if (sd < xdown || sd > xup) 
   { 
      messanykey(10,10,scat("Range is between$%s and %s$",astr,bstr)); 
      goto label2;
   } 
   sbld(astr,"%11.6f",sd); 

label3:
   if (!correctDoubleS(45,21,scat("To E(%s), [GeV] = ",pname[order[1]]),&sup))
      goto label2;
      sup=sup*0.999999999;
   if (sup < sd || sup > xup) 
    { 
     messanykey(10,10,scat("Range is between$%s and %s$",astr,bstr)); 
     goto label3;
    } 


   clr_eol(); 
   if (!correctLong(1,22,"Points         : ",&npoints,0))
      goto label3;
/* ----   PICTURE   ---- */ 
   goto_xy(15,23); 
   scrcolor(Black,Red); 
   for (i = 1; i <= 50; i++) print("%c",' ');
   goto_xy(15,23); 
   count = 0; 
/* --------------------- */ 
   step = (sup - sd) / npoints; 
   xx = sd; 
   count = 0; 
   do 
   { 
      rr_ = (realseq)getmem(sizeof(realseqrec));
      rr_->next = r_; 
      rr_->x = xx; 
      rr_->y = dgamma_dx(xx);
      if (err_code != 0) 
      { 
         errormessage(); 
         if (err_code == 4) 
         { 
            err_code = 0; 
            goto exi;
         } 
         err_code = 0; 
      } 
      if (rr_->y < 0.0)
         rr_->y = 1.E-36; 
      r_ = rr_; 
      /*-------------- print XXXXXXXXXXXX ---------*/
      ++count; 
      i = 15 + (50 * count) / npoints; 
      scrcolor(Black,LightGray); 
      while (where_x() < i) print("%c",'X'); 
      xx += step; 
      /*-------------- print XXXXXXXXXXXX ---------*/
   }  while (count != npoints); 

   if (r_ != NULL && r_->next != NULL) 
   { 
      scrcolor(White,Black); goto_xy(1,21); clr_eol(); 
      scrcolor(White,Black); goto_xy(1,22); clr_eol(); 
      scrcolor(White,Black); goto_xy(1,23); clr_eol(); 
      sbld(upstr,"Process: %s",processstr); 
      sbld(buf2, "dGamma/dE(%s)",pname[order[1]]);

      k = 1; 
      do 
      { void * pscr = NULL; 
         dalitzinformation();
         menu1(56,8,"","\026"
            " View plot            "
            " Save result in a file", "",&pscr,&k); 
         switch (k) 
         {
            case 1:
               plot_2(FALSE,r_,upstr,
               scat("Energy (%s), GeV", pname[order[1]]),
                     buf2 ); 
               break; 
       
            case 2:
               writetable1(&r_,upstr,
                           scat("E(%s), GeV",pname[order[1]]),
                           buf2); 
         }   /*  Case  */ 
         dalitzinformation();
      }  while (k != 0); 
   } 
   
exi: 
   if (r_ != NULL) disposerealseq(&r_); 
} 



static void  disposecell(cell  r)
{pointer     p; 

   while (r != NULL) 
   { 
      p = (pointer)r->next; 
      free(r); 
      r = (cell)p; 
   } 
} 


static double  xg(integer  i)
{ 
  return xdown + (dx / 2) * (2 * i - 1); 
} 


static double  yg(integer  i)
{ 
  return ydown + (dy / 2) * (2 * i - 1); 
} 


static void  fillcell(integer * ix, integer * iy)
{double      x, y, fxy, xx, yy; 

   x = xg(*ix); 
   y = yg(*iy); 
   if (inellips(x,y)) 
   { 
      xx = x; 
      yy = y; 
   } 
   else
      if (inellips(x - dx,y + dy)) 
      { 
         xx = x - dx; 
         yy = y + dy; 
      } 
      else
         if (inellips(x - dx,y - dy)) 
         { 
            xx = x - dx; 
            yy = y - dy; 
         } 
         else
            if (inellips(x + dx,y + dy)) 
            { 
               xx = x + dx; 
               yy = y + dy; 
            } 
            else
               if (inellips(x + dx,y - dy)) 
               { 
                  xx = x + dx; 
                  yy = y - dy; 
               } 
               else 
                  return;
   ext_x = xx; 
   ext_y = yy; 
   fxy = sfunc(); 
   if (fxy < 0.0) fxy = 0.0; 
   sum += fxy; 
   cellptr->next = (cellrec*)getmem(sizeof(cellrec));
   cellptr = cellptr->next; 
   cellptr->next = NULL; 
   cellptr->i = *ix; 
   cellptr->j = *iy; 
   cellptr->f = sum; 
   if (first) 
   { 
      cellist = cellptr; 
      first = FALSE; 
   } 
} 

static void g_line(double x, double y, double xx, double yy)
{ 
  tg_line(scx(x),scy(y),scx(xx),scy(yy));
} 
                  

static void  drawellips(double *  x, double*   y)
{byte        ix; 
 double ox,oy;
   *x = xxmin; 
   *y = sy1(*x);
   for (ix = 1; ix < 100; ix++) 
   { 
      ox = *x;
      oy = *y;
      *x = xxmin / 100. * (100 - ix) + xxmax / 100. * ix; 
      *y = sy1(*x); 
      g_line(ox,oy,*x,*y); 
   } 
   ox = *x;
   oy = *y;
   *x = xxmax; 
   *y = sy1(xup); 
   g_line(ox,oy,*x,*y); 
   ox = *x;
   oy = *y;
   *x = xxmax; 
   *y = sy2(xup); 
   g_line(ox,oy,*x,*y); 
   for (ix = 1; ix < 100; ix++) 
   { 
   ox = *x;
   oy = *y;
   *x = xxmin / 100. * (ix) + xxmax / 100. * (100 - ix); 
   *y = sy2(*x); 
   g_line(ox,oy,*x,*y); 
   } 
   ox = *x;
   oy = *y;
   *x = xxmin; 
   *y = sy2(xdown); 
   g_line(ox,oy,*x,*y); 
   ox = *x;
   oy = *y;
   *x = xxmin; 
   *y = sy1(xdown); 
   g_line(ox,oy,*x,*y); 
} 

static void  horizontalcut(void)
{realseq     r_, rr_; 
 word        count; 
 byte        i; 
 
 double      sup, sd, step, xx; 
 int         k; 
 long        npoints; 
 char        astr[21], bstr[21], buf1[82], buf2[82]; 
 char        upstr[82], downstr[82]; 

   
   r_ = NULL; 
   setlimits(); 
   npoints = 100; 
   sbld(astr,"%11.6f",ydown); 
   sbld(bstr,"%11.6f",yup); 
label1:
   xx = ycrest;
   if (!correctDoubleS(1,21,scat("Horizontal slice at: E(%s),  GeV = ",
                          pname[order[3]]),&xx))
      goto exi;

   if (xx < ydown || xx > yup) 
   { 
      messanykey(10,10,scat("Range is between$%s and %s$",astr,bstr)); 
      goto label1;
   } 
   ycrest=xx;
   sup = sx1(ycrest); 
   sd = sx2(ycrest); 
   sbld(astr,"%11.6f",sd); 
   sbld(bstr,"%11.6f",sup); 
   sbld(downstr,"%11.6f",ycrest); 

label2:
   if (!correctDoubleS(1,22,scat("From E(%s), GeV = ",pname[order[1]]),&sd))
      goto label1;
   sd *= 1.000000001; 
   if (sd < sx2(ycrest) || sd > sx1(ycrest)) 
   { 
      messanykey(10,10,scat("Range is between$%s and %s$",astr,bstr)); 
      goto label1;
   } 
   sbld(astr,"%11.6f",sd); 

label3:
   goto_xy(45,22);
   if (!correctDoubleS(45,22,scat("To E(%s), GeV = ",pname[order[1]]),&sup)) 
       goto label2;
   sup *= 0.999999999; 
   if (sup < sd || sup > sx1(ycrest)) 
   { 
      messanykey(10,10,scat("Range is between$%s and %s$",astr,bstr)); 
      goto label3;
   } 

label4:
   goto_xy(60,24);
   if (!correctLong(15,24,"Points:",&npoints,0))
      goto label3;
   if (npoints < 2) 
   {  messanykey(10,12,"Too few points!$");
      goto label4;
   } 
/* ----   PICTURE   ---- */ 
   goto_xy(15,25);
   scrcolor(Black,Red); 
   for (i = 1; i <= 50; i++) print("%c",' ');
   goto_xy(15,25); 
   count = 0; 
/* --------------------- */ 
   step = (sup - sd) / npoints; 
   xx = sd; 
   ext_x = xx; 
   ext_y = ycrest; 
   count = 0; 
   do
   { 
      rr_ = (realseq)getmem(sizeof(realseqrec));
      rr_->next = r_; 
      rr_->x = xx; 
      rr_->y = sfunc();
      if (err_code != 0) 
      { 
         errormessage(); 
         if (err_code == 4) 
         { 
            err_code = 0; 
            goto exi;
         } 
         err_code = 0; 
      } 
      if (rr_->y < 0.0) rr_->y = 1.E-36; 
      r_ = rr_; 
      ++count; 

      i = 15 + (50 * count) / npoints; 
      scrcolor(Black,LightGray); 
      while (where_x() < i) print("%c",'X'); 

      xx += step; 
      ext_x = xx; 
   }  while (count != npoints && inellips(ext_x,ext_y)); 

   if (r_ != NULL && r_->next != NULL) 
   { 
      scrcolor(White,Black);
      goto_xy(1,23);
      clr_eol(); 
      sbld(upstr,"Process: %s",processstr); 
      sbld(buf1,"E(%s), GeV",pname[order[1]]);
      sbld(buf2,"dGamma/dE(%s)dE(%s), 1/GeV ",pname[order[1]],pname[order[3]]);
      k = 1; 
      do
      {  void * pscr = NULL;
         menu1(56,11,"","\026"
            " Show slice plot      "
            " Save result in a file"
            " Change plot          ","",&pscr,&k); 
         switch (k)
         {
            case 1:
               plot_2(FALSE,r_,upstr,buf1,buf2);
            break; 
       
            case 2:
               writetable1(&r_,upstr,buf1,
                 scat("%s    (E(%s)=%s, GeV)",
                   buf2,pname[order[3]],downstr)); 
            break; 
            case 3:
                if (r_ != NULL) disposerealseq(&r_); 
                goto label1;
         }  /*  Case  */ 
      }  while (k != 0); 
   } 
   
exi:
   scrcolor(White,Black); goto_xy(1,21); clr_eol(); 
   scrcolor(White,Black); goto_xy(1,22); clr_eol(); 
   scrcolor(White,Black); goto_xy(1,23); clr_eol(); 
   if (r_ != NULL) disposerealseq(&r_); 
} 


static void  verticalcut(void)
{realseq     r_, rr_; 
 word        count; 
 byte        i; 
 
 double      sup, sd, step, xx; 
 int         k; 
 long        npoints; 
 char        astr[21], bstr[21], buf1[82], buf2[82]; 
 char        upstr[82], downstr[82]; 

   
   r_ = NULL; 
   setlimits(); 
   npoints = 100; 
   sbld(astr,"%11.6f",xdown); 
   sbld(bstr,"%11.6f",xup); 
label1:
   xx = xcrest;
   if (!correctDoubleS(1,21,scat("Vertical slice at :  E(%s), GeV = ",
                          pname[order[1]]),&xx))
      goto exi;

   if (xx < xdown || xx > xup ) 
   { 
      messanykey(10,10,scat("Range is between$%s and %s$",astr,bstr)); 
      goto label1;
   } 
   xcrest=xx;
   sup = sy1(xcrest); 
   sd  = sy2(xcrest); 
   sbld(astr,"%11.6f",sd); 
   sbld(bstr,"%11.6f",sup); 
   sbld(downstr,"%11.6f",xcrest); 

label2:
   goto_xy(1,22);
   if (!correctDoubleS(1,22,scat("From E(%s), GeV = ",pname[order[3]]),&sd))
      goto label1;
   sd *= 1.000000001;                  
   if (sd < sy2(xcrest) || sd > sy1(xcrest)) 
   { 
      messanykey(10,10,scat("Range is between$%s and %s$",astr,bstr)); 
      goto label2;
   } 
   sbld(astr,"%11.6f",sd); 

label3:
   goto_xy(45,22);
   if (!correctDoubleS(45,22,scat("To E(%s), GeV = ",pname[order[3]]),&sup))
      goto label2;
   sup *= 0.999999999; 
   if (sup < sd || sup > sy1(xcrest)) 
   { 
      messanykey(10,10,scat("Range is between$%s and %s$",astr,bstr)); 
      goto label3;
   } 

label4:
   goto_xy(60,24);
   if (!correctLong(15,23,"Points:",&npoints,0))
      goto label3;
   if (npoints < 2) 
   {  messanykey(10,12,"Too few points!$");
      goto label4;
   } 
/* ----   PICTURE   ---- */ 
   goto_xy(15,25);
   scrcolor(Black,Red); 
   for (i = 1; i <= 50; i++) print("%c",' '); 
   goto_xy(15,25); 
   count = 0; 
/* --------------------- */ 

   step = (sup - sd) / npoints; 
   xx = sd; 
   ext_x = xcrest; 
   ext_y = xx; 
   count = 0; 
   do
   { 
      rr_ = (realseq)getmem(sizeof(realseqrec));
      rr_->next = r_; 
      rr_->x = xx; 
      rr_->y = sfunc();
      if (err_code != 0) 
      { 
         errormessage(); 
         if (err_code == 4) 
         { 
            err_code = 0; 
            goto exi;
         } 
         err_code = 0; 
      } 
      if (rr_->y < 0.0) rr_->y = 1.E-36; 
      r_ = rr_; 
      ++count; 
      i = 15 + (50 * count) / npoints; 
      scrcolor(Black,LightGray); 
      while (where_x() < i) print("%c",'X'); 
      xx += step; 
      ext_y = xx; 
   }  while (count != npoints && inellips(ext_x,ext_y)); 

   if (r_ != NULL && r_->next != NULL) 
   { 
      scrcolor(White,Black);
      goto_xy(1,23);
      clr_eol(); 
      sbld(upstr,"Process: %s",processstr); 
      sbld(buf1,"E(%s), GeV",pname[order[3]]);
      sbld(buf2,"dGamma/dE(%s)dE(%s) 1/GeV",pname[order[1]],pname[order[3]]);

      k = 1; 
      do 
      { void * pscr = NULL;
         menu1(56,11,"","\026"
            " Show slice plot      "
            " Save result in a file"
            " Change plot          ","",&pscr,&k); 
         switch (k)
         {
            case 1: 
               plot_2(FALSE,r_,upstr,buf1,buf2 /*,
                 scat("Cut  Point:   E(%s)=%s")*/ ); 
            break; 
       
            case 2:
               writetable1(&r_,upstr,buf1,
       scat("%s    (E(%s)=%s [GeV])",buf2,pname[order[1]],downstr)); 
            break; 
            case 3:
                if (r_ != NULL) disposerealseq(&r_); 
                goto label1;
         }    /*  Case  */ 
      }  while (k != 0); 
   } 
   
exi:
   scrcolor(White,Black); goto_xy(1,21); clr_eol(); 
   scrcolor(White,Black); goto_xy(1,22); clr_eol(); 
   scrcolor(White,Black); goto_xy(1,23); clr_eol(); 
   if (r_ != NULL) disposerealseq(&r_); 
} 

void  dalitz_plot(word  ntotal_, word  nx, word  ny)
{int k;
 integer     i,kk; 
 integer     ix, iy; 
 double      x, y; 
 long        nn, count; 
 char        buff[82];
 int xk,yk;
 cellrec     sel_in_stack;   /* Place for CellPrt^  */ 
 void * pscr=NULL;
 char        buf1[82],buf2[82],buf3[12];
 word        npoint; 


   srand(iseed);

   ntotal = ntotal_;
/* calcdecaycoef(); */
   setlimits(); 

   dx = (xup -xdown ) / nx; 
   dy = (yup -ydown ) / ny; 

   first = TRUE; 
   sum = 0.; 
   goto_xy(15,25);
   scrcolor(Black,Red); 
   for (i = 0; i < 50; i++) print("%c",' ');
   goto_xy(15,25); 
   count = 0; 
   nn = (ny + 2) * (nx + 2); 
   cellptr = &sel_in_stack; 
   for (iy = 0; iy <= ny + 1; iy++) 
   { 
      calcconstants(); 
      for (ix = 0; ix <= nx + 1; ix++) 
      { 
         fillcell(&ix,&iy); 
         ++count; 
         i = 15 + (50 * count) / nn; 
         scrcolor(Black,LightGray); 
         while (where_x() < i) print("%c",'X'); 
         if (escpressed()) 
         { 
   scrcolor(White,Black); goto_xy(1,22); clr_eol(); 
   scrcolor(White,Black); goto_xy(1,23); clr_eol(); 
   scrcolor(White,Black); goto_xy(1,25); clr_eol(); 
            err_code = 4; 
            goto exi0;
         } 
      }  
   } 
/* ------------------------------------------------- */ 
   scrcolor(White,Black); goto_xy(1,22); clr_eol(); 
   scrcolor(White,Black); goto_xy(1,23); clr_eol(); 
   scrcolor(White,Black); goto_xy(1,25); clr_eol(); 
   get_text(1,1,80,25,&pscr);

      
REDRAW:
   setlimits(); 
   clr_scr(Black,White);
   
   npoint = 0; 

   sbld(buf1,"Dalitz-plot of decay: %s",processstr);
   sbld(buf2,"E(%s), GeV",pname[order[1]]);
   sbld(buf3,"E(%s), GeV",pname[order[3]]);

   gaxes(0,buf1,buf2,buf3);
          
   drawellips(&x,&y); 
do
      { 
         cellptr = cellist; 
         done = FALSE; 
         fsum = ch_random() * sum; 
         do 
         { 
         
            if (fsum <= cellptr->f) 
               done = TRUE; 
            else 
               cellptr = cellptr->next; 
         }  while (!done); 
         x = ch_random() * dx + xg(cellptr->i) - dx / 2.; 
         y = ch_random() * dy + yg(cellptr->j) - dy / 2.; 
      
         if (inellips(x,y)) 
         { 
            gpixel(x,y);
            ++npoint; 
         } 
         if (escpressed()) goto eof_spray;
      }  while (npoint < ntotal); 
eof_spray:
      be_be(); 
      if (texflag)
      {  plottexfinish(buff);
         scrcolor(fgcolor,bColor);
         tg_bar(0,tg_getmaxy() - tg_textheight("A")-7,
                tg_getmaxx(),tg_getmaxy());
         tg_settextjustify(LeftText,TopText);
         tg_outtextxy(30,tg_getmaxy() - tg_textheight("A"),buff);
         while ((k = inkey()) != KB_ENTER && k != KB_SIZE);
         tg_bar(0,tg_getmaxy() - tg_textheight("A")-7,
                tg_getmaxx(),tg_getmaxy());
      }
   k = 0; 
   do 
   { 
      if(KB_SIZE ==
      strmenu(10,tg_getmaxy() - 15,scat(" Exit $ Vertical slice $ Horizontal slice $ LaTeX "),&k) ) goto REDRAW; 
      switch (k) 
      {
         case 0:   
            goto exi;
         case 1:
         case 2:
/*            tg_setfillstyle(SolidFill,White); */
            tg_bar(1,tg_getmaxy()-30,tg_getmaxx()-200,tg_getmaxy());
            tg_outtextxy(30,tg_getmaxy() - 30,
                           " Click Left mouse where to slice...");
           mou:
            kk= inkey();
            if (kk == KB_MOUSE)
              { if(!( mouse_info.but1 == 2))
                goto mou;
                else
                {xk = mouse_info.x;
                 yk = mouse_info.y;
                 xcrest=bscx(xk);
                 ycrest=bscy(yk);
                 tg_bar(1,tg_getmaxy()-30,tg_getmaxx()-300,tg_getmaxy());
                 if(!inellips(xcrest,ycrest) )
                 {
                   sbld(buff,"Put Mouse into Dalitz-plot!");          
                   tg_bar(1,tg_getmaxy()-30,tg_getmaxx()-300,tg_getmaxy());
                   tg_outtextxy(35,tg_getmaxy() - 20,buff);
                   be_be(); 
                   goto mou;
                 }
                 xcrest=xcrest; 
                 ycrest=ycrest; 
                }
              }
            else
              { xcrest=0.5 * (xup+xdown);
                ycrest=0.5 * (sy1(xcrest)+sy2(xcrest));
              }
           clr_scr(White,Black);
           if(k==2)horizontalcut();
	   if(k==1)verticalcut();
           goto REDRAW;
         break;

         case 3:

            plottexstart(buf1);
            goto REDRAW;
      }  /* case k */ 
   }  while (TRUE); 

exi:
    clr_scr(White,Black); 
    put_text(&pscr);
exi0:
   disposecell(cellist); 
   dalitzinformation();
} /* end of  dalitz_plot */

int  correctDoubleS(int x,int y,char* txt, double * var)
{  int         npos,key;
   int         rc,err;
   int xx; 
   char buff[200],buff2[200];

   scrcolor(White,Black);
   goto_xy(x,y);
   print(txt);
   xx=where_x();
   
   sprintf(buff,"%lg",*var);

   strcpy(buff2,buff);

   npos = 1;      

   do
   { 
      goto_xy(xx,y);
      key  = str_redact(buff,npos);

      if (key == KB_F1 || key == KB_ESC)
      {  
         rc = 0 ;
         goto EXI;
      }

      if (key == KB_ENTER)
      {
         if (strcmp(buff,buff2) == 0) return 1 ;
         trim(buff);
         err=sscanf(buff,"%lf",var);
         if (err<=0)  messanykey(10,10," incorrect number $"); 
         else
         {
            rc = 1;
            goto EXI;
         }
      } 
   }  while (TRUE);   
EXI: 
   return rc;   
} 
