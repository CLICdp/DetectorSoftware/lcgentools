#include "tptcmac.h"
#include "syst2.h"
#include "files.h"
#include "syst2.h"
#include "physics.h"

#include "optimise.h"

 infoptr info;
 byte    cutlevel;

static infoptr infoone;

void  initinfo(void)
{
   info = (infoptr)getmem_((word)sizeof(struct inforec));
   info->next = NULL;
   strcpy(info->name,"1");
   info->ival = 1;
   info->consttype = numb;
   infoone=info;
}

boolean      equalexpr(varptr v1,varptr v2)
{
	while (v1 != NULL || v2 != NULL)
   {
      if (v1 == NULL || v2 == NULL) return FALSE;
      if (v1->sgn == v2->sgn && v1->coef == v2->coef &&
          strcmp(v1->vars,v2->vars) == 0)

		{
         v1 = v1->next;
         v2 = v2->next;
      }
		else	return FALSE;
	}
   return TRUE;
}

static void  readmonom(char* varstr,char* conststr,long* numc)
{byte      i = 0, bt;

   FREAD1(*numc, archiv);
   FREAD1(bt,archiv);
   while (bt <= cutlevel)
   {
      varstr[++i-1] = (char) bt;
      FREAD1(bt,archiv);
   }
   varstr[i] = '\0';

   i = 0;
   while (bt < 254)
   {
      conststr[++i-1] = (char) bt;
      FREAD1(bt,archiv);
   }
   conststr[i] = '\0';
}   /* ReadMonom */

static void addnum(long n,char* signum,infoptr* ans)
{
   if (n>0) *signum='+'; else { *signum='-';n= -n;}
   *ans=info;
   while (*ans != NULL)
   {  if ( ((*ans)->consttype == numb) && ((*ans)->ival == n) ) return;
      *ans= (*ans)->next;
   }
   *ans=(infoptr) getmem_( (word) sizeof(struct inforec));
   (*ans)->next=info;
   (*ans)->consttype=numb;
   (*ans)->ival=n;
   info = *ans;
}

static boolean addtmpconst(varptr tmpconst,char* s,infoptr* coeff )
{
 varptr     c;

   revers((pointer*)&tmpconst);
   if (tmpconst->sgn =='-')
   {  *s='-';
      c = tmpconst;
      while (c != NULL)

		{
         if (c->sgn=='-' ) c->sgn='+'; else c->sgn='-';
         c = c->next;
      }
   }
   else  *s='+';
   if( (tmpconst->next == NULL ) && (tmpconst->vars[0] == '\0') )
	{  *coeff = tmpconst->coef;

		return   FALSE;
    }

   *coeff = info;
   while (*coeff != NULL)
      if (((*coeff)->consttype == expr)&& (equalexpr((*coeff)->const_,tmpconst)))
			return FALSE;  else	*coeff = (*coeff)->next;
      *coeff = (infoptr)getmem_((word)sizeof(struct inforec));
      (*coeff)->next = info;
      (*coeff)->const_ = tmpconst;
      (*coeff)->consttype = expr;
		info = *coeff;
		return TRUE;
} /*  AddTmpConst */


void  readpolynom(varptr* expr_)
{  word         k;
	word         polylen;
	char         varstr[STRSIZ], conststr[STRSIZ];
	long      n;
	pointer      pntr;
	varptr       tmpconst;
	char         s;
	infoptr      coeff;
	marktp  tmpmark;

        FREAD1(polylen, archiv);
   if (polylen == 0)
   {
      *expr_ = NULL;
      return;
   }
   readmonom(varstr,conststr,&n);

   *expr_ = (varptr)getmem_(minvarrec + (word)strlen(varstr));
   (*expr_)->next = NULL;
   strcpy((*expr_)->vars,varstr);

	addnum(n,&s,&coeff);
	mark_(&tmpmark);
   tmpconst = (varptr)getmem_(minvarrec + (word)strlen(conststr));
   tmpconst->next = NULL;
   strcpy(tmpconst->vars,conststr);
   tmpconst->sgn=s;
   tmpconst->coef=coeff;

   for (k = 2; k <= polylen; k++)
   {
      readmonom(varstr,conststr,&n);
      if (strcmp(varstr,(*expr_)->vars) != 0)
      {  if (! addtmpconst(tmpconst,&((*expr_)->sgn),&((*expr_)->coef) ))
                                   release_(&tmpmark);
         pntr = (pointer)(*expr_);
         *expr_ = (varptr)getmem_(minvarrec + (word)strlen(varstr));
         (*expr_)->next = (varptr)pntr;
         strcpy((*expr_)->vars,varstr);
			pntr = NULL;
			addnum(n,&s,&coeff);
			mark_(&tmpmark);
      }
		else
		{
			pntr = (pointer)tmpconst;
			addnum(n,&s,&coeff);
		}
      tmpconst = (varptr)getmem_(minvarrec + (word)strlen(conststr));
      tmpconst->next = (varptr)pntr;
      strcpy(tmpconst->vars,conststr);
      tmpconst->sgn = s;
      tmpconst->coef =coeff;
   }
	if (! addtmpconst(tmpconst,&((*expr_)->sgn),&((*expr_)->coef)) )
	  release_(&tmpmark);
}   /* ReadPolynom */


static void  findmaxvar(varptr ex,word* n,char* ch,byte* deg)
{word         nnn[nvarhighlimit];
 byte         mdeg[nvarhighlimit];
 integer      k;
 integer      bt, d, nv;

   for (k = 0; k < cutlevel; k++)
   {  nnn[k] = 0;
      mdeg[k] = 0;
   }

   while (ex != NULL)
   {
      if (strlen(ex->vars) != 0)
      {
         d = 1;
         bt = ex->vars[0];
         for (k = 2; k <= (integer)strlen(ex->vars); k++)
         {
            nv = ex->vars[k-1];
            if (nv != bt)
            {
               mdeg[bt-1] = nnn[bt-1] == 0 ? d : MIN(d,mdeg[bt-1]);
               ++(nnn[bt-1]);
               d = 1;
               bt = nv;
            }
            else ++(d);
         }
         mdeg[bt-1] = nnn[bt-1] == 0 ? d : MIN(d,mdeg[bt-1]);
         ++(nnn[bt-1]);
      }
      ex = ex->next;
   }
   bt = 1;
   *n = (word)nnn[0];
   *deg = mdeg[0];
   for (k = 2; k <= cutlevel; k++)
   if (*n < nnn[k-1] || (*n == nnn[k-1] && *deg < mdeg[k-1]))
   {
      *n = (word)nnn[k-1];
      bt = k;
      *deg = mdeg[k-1];
   }
   *ch = (char) bt;
}


static void  findmaxcoef(varptr ex,infoptr* i,word* n)
{
 varptr jj;
   jj=ex;  while (jj != NULL) { (jj->coef)->count=0;jj=jj->next;}
   jj=ex;  while (jj != NULL) { (jj->coef)->count++;jj=jj->next;}

   *n=0;
   infoone=info;
   while (infoone->next != NULL ) infoone=infoone->next;
   (*i)=infoone;

   (*i)->count=0;
   jj=ex;
   while(jj != NULL)
   {  if (*n < (jj->coef)->count)

      {
         *i =  jj->coef;
         *n =  (*i)->count;
      }
      jj = jj->next;
   }
}

static void  clipvar(varptr ex,char ch,byte deg,varptr* ex1,varptr* ex2)
{byte         k;
 varptr       exnext;

 var_rec     ex1rec;
 var_rec     ex2rec;
 varptr      ex1_,ex2_;
  ex1_ =  & ex1rec;
  ex2_ =  & ex2rec;
   while (ex != NULL)
   {
      k = cpos(ch,ex->vars);
      exnext = ex->next;
      if (k == 0)
      {
         ex2_->next = ex;
         ex2_ = ex;
      }
      else
      {
         strdelete(ex->vars,k,deg);
         ex1_->next = ex;
         ex1_ = ex;
      }
      ex = exnext;
   }


   ex1_->next = NULL;  *ex1=ex1rec.next;
   ex2_->next = NULL;  *ex2=ex2rec.next;
}


static void  clipconst(varptr ex,infoptr i,varptr* ex1,varptr* ex2)
{

 varptr       exnext;
 infoptr      one;

   var_rec     ex1rec, ex2rec;
   varptr      ex1_,ex2_;

        one = info;
        while (one->next != NULL) one = one->next;

   ex1_ =  & ex1rec;
   ex2_ =  & ex2rec;

   while (ex != NULL)
   {
      exnext = ex->next;
      if (ex->coef != i)
      {
         ex2_->next = ex;
         ex2_ = ex;
      }
      else
      {
         ex->coef = one;
         ex1_->next = ex;
         ex1_ = ex;
      }
      ex = exnext;
   }


   ex1_->next = NULL;  *ex1=ex1rec.next;
   ex2_->next = NULL;  *ex2=ex2rec.next;

}

pointer      emitexpr(varptr ex,smplemit smplemitfun,vfact vfactfun,
                               cfact cfactfun)
{word         nv, nc;
 char         ch;
 infoptr      i;
 varptr       ex1, ex2;
 pointer      pmult, psum;
 byte         deg;

   findmaxcoef(ex,&i,&nc);
   findmaxvar(ex,&nv,&ch,&deg);
   if (nc < 2 && nv < 2) return smplemitfun(ex);
   if (nv >= nc)
   {
      clipvar(ex,ch,deg,&ex1,&ex2);
      pmult = emitexpr(ex1,smplemitfun,vfactfun,cfactfun);
      psum  = emitexpr(ex2,smplemitfun,vfactfun,cfactfun);
      return  vfactfun(ch,deg,pmult,psum);
   }
   else
	{
      clipconst(ex,i,&ex1,&ex2);
      pmult = emitexpr(ex1,smplemitfun,vfactfun,cfactfun);
      psum = emitexpr(ex2,smplemitfun,vfactfun,cfactfun);
      return cfactfun(i,pmult,psum);
   }
}
