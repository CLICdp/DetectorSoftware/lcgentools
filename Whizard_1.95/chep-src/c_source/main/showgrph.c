#include <math.h>
#include "tptcmac.h"
#include "syst2.h"
#include "crt.h"
#include "physics.h"
#include "global.h"
#include "sos.h"
#include "screen.h"
#include "files.h"
#include "diaprins.h"
#include "help.h"
#include "os.h"
#include "tex_util.h"
#include "showgrph.h"
#include "crt_util.h"

/* The definition of these variables must be transfered to a proper header*/
static char    f_name[STRSIZ];

static char    infline1[]  =
"Help, PgUp, PgDn, +, -, Home, End, Del, Ins, LaTeX, Esc";
static int     inflinek1[12] =
{KB_F1,KB_PAGEU,KB_PAGED,'+','-',KB_HOME,KB_END,KB_DC,KB_IC,'L',KB_ESC};
static char    infline2[]  =
"Help,PgUp,PgDn,+,-,Home,End,Del,Ins,LaTeX,Ghosts,Results,Esc";
static int     inflinek2[14] =
{KB_F1,KB_PAGEU,KB_PAGED,'+','-',KB_HOME,KB_END,KB_DC,KB_IC,'L',KB_F3,KB_ENTER,KB_ESC};


   typedef struct intsect {
           int      y1, y2;
           char     nline;
                          } intsect;

   typedef struct knot {
           char      ty, pt, fr, e1, e2, e3, n1, n2, n3;
                       } knot;

static   int      xn1, xn2; /* From showgrph */
static   adiagram     varc;
static   csdiagram    csdiagr,ars;
static   char         upr, ur, ni;
static   int          ndi_x,ndi_y;
static   int      na, n;
static   word         nn, nm;
static   int      xn, dx, ynu, ys, yh, dy, bh, bhh, bv;
static   int      x[4];
static   int      y[9];
static   word         ecol, ebkc;
static   struct nar { double   b, r; } nar;
static   int      x1,x2;  /* From mline */
static   char         np;     /* From mline */
static   knot         kn[4];  /* From picture */
static   char         kt, kl, kk, ks, nk, se, se1, ku,
                      tpc, pext; /* From picture */
static   intsect      intar[5]; /* For mline */
static   boolean      moveup;
static   boolean      storeup;
static   int      sflag = 0;
static   int cHeight, cWidth;

static   int pictureX;
static   int pictureY;  
static   char  letterSize[14];

  /*
  nn   - first FilePos of subproces;
  nm-1 - last  FilePos of subproces;
  N-1  - current FilePos;
  na-1 - FilePos of first diagram on the screen ;
  */

static void  picture(char  tprc);

static void cleardomain(int x1,int y1,int x2,int y2)
{struct tg_viewporttype vp;

   tg_getviewsettings(&vp);
   tg_setviewport(x1 + vp.left,y1 + vp.top,x2 + vp.left,y2 + vp.top);
   tg_clearviewport();
   tg_setviewport(vp.left,vp.top,vp.right,vp.bottom);
}


static void cleartext(int x,int y,char * s)
{int h,w;
 struct tg_textsettingstype txi;

   h = tg_textheight(s);
   w = tg_textwidth(s);
   tg_gettextsettings(&txi);

   switch(txi.horiz)
   {
      case CenterText:
         if (w%2 != 0) w++;
         x -= w/2;
      break;
      case RightText:
         x -= w;
   };

   switch(txi.vert)
   {
      case CenterText:
         if (h%2 != 0) h++;
         y -= h/2;
         break;
      case BottomText:
         y -= h;
   };

   h += y;
   w += x;
   cleardomain(x,y,w,h);
}


static void  mbase(int n)
{
   n -= na + 1;
   bh = (xn + dx) * (n % (ndi_x)) + bhh;
   bv = (ynu + dy) * (n/ndi_x) + dy;
}
  /* ---------- REDRAW  and  procedures needed for redraw ----- */



static void  wrttext(int n)
{  
   int ypos=bv + ynu -2; 
   tg_settextjustify(LeftText,BottomText);
   scrcolor(ecol,ebkc);
   tg_outtextxy(bh + 3,ypos , scat("%d-%d",nsub,(n - (int)nn)));
   if ((upr > 1 && ars.status == -1) || (upr <= 1 && varc[0] > 0))
   {  scrcolor(LightRed,Black);
      tg_outtextxy(bh + 48,ypos,"DEL");
   }
   if (upr == 2)
      switch(ars.status)
      {
         case -2:
            scrcolor(LightRed,Black);
            tg_outtextxy(bh+48,ypos,"Out of memory");
         break;
         case  1:
            scrcolor(LightBlue,Black);
            tg_outtextxy(bh+48,ypos,"CALC");
         break;
         case  2:
            scrcolor(LightBlue,Black);
            tg_outtextxy(bh+48,ypos,"ZERO ");
      }
}


static void  drawr(word width)
{int w = 2;
   if (tg_getmaxy() < 200) w = 1;


   tg_setlinestyle(SolidLn,fColor == ebkc ?  NormWidth : width);
   if (width == ThickWidth && fColor == ebkc)
   { scrcolor(ecol,ebkc);
		cleardomain(bh - w,bv - w,bh + xn + w,bv + w);
		cleardomain(bh - w,bv + ynu - w,bh + xn + w,bv + ynu + w);
		cleardomain(bh - w,bv - w,bh + w,bv + ynu + w);
		cleardomain(bh + xn - w,bv - w,bh + xn + w,bv + ynu + w);
   }
	else	tg_rectangle(bh,bv,bh + xn,bv + ynu);
}


static void  redraw(int m)
{int   k, l;
 char tpc;

 /*  external variables : nn,nm,n,na  */

   if (m < (int)(nn + 1))
      m = nn + 1;
   else
      if (m > nm)
         m = nm;
   if (m > na && m < (ndi_x*ndi_y) + na + 1)
   {
      if (m == n)
      {  if (!storeup)
            return;
         else
            goto p_graph;
      }
      mbase(n);
      scrcolor(ebkc,Black);
      drawr(ThickWidth);
      scrcolor(LightGray,Black);
      drawr(NormWidth);
      mbase(m);
      scrcolor(White,Black);
      drawr(ThickWidth);
      n = m;
      return;
   }

   storeup = FALSE;
   moveup = TRUE;
   na = nn + (ndi_x*ndi_y) * ((m - nn - 1) / (ndi_x*ndi_y));
   n = m;

p_graph:

   if (upr > 1)
      fseek(diagrq,na*sizeof(csdiagram),SEEK_SET);
   else
      fseek(diagrp,na*sizeof(adiagram),SEEK_SET);

   {
      tg_clearviewport();
      tpc = hadr2.how == 0 ? 1 : 2;
      for (k = 0; k <= (ndi_x*ndi_y) - 1; k++)
      {
         l = na + k + 1;
         if (l > nm) return;
         if (upr > 1)  FREAD1(ars,diagrq);
         else          FREAD1(varc,diagrp);
         mbase(l);
         wrttext(l);
         if (l == m)
         {  scrcolor(White,Black);
            drawr(ThickWidth);
         }
         else
         {  scrcolor(LightGray,Black);
            drawr(NormWidth);
         }
         picture(tpc);
      }
   }
}   /*  Redraw  */
  /* ----------- End of Block REDRAW -------------- */




/* tex tex tex tex tex tex tex tex tex tex */

static void texinvoke(char* pname)
{  
   char dd[6];
   int  n_,dn_;
   boolean flag;
   int y_min,y_max,y_quant;
   int  tex_cWidth;
   if (upr>1)  strcpy(dd,"sd_"); else  strcpy(dd,"fd_");
   newFileName(f_name,dd,"tex"); 

   texstart(f_name,scat("diagrams for process %s",pname),letterSize);    

   y_min=bv;
   y_max=bv+ynu;
   y_quant=y[1]-y[3];
   dn_=0;
   switch(nout)
   { case 2: y_min+=y_quant;y_max-=2*y_quant;break;
     case 3: y_min+=y_quant;y_max-=  y_quant;break;
     case 4:                y_max-=  y_quant;break;     
   }
   
   texxscale=1;
   tex_cWidth=tg_textwidth("W");

   texymax1=y_max;
   texyscale=(double)pictureY/(y_max-y_min);
   texxscale=(double)(pictureX -8-4*tex_cWidth)/(xn-8-4*cWidth);
   texxshift=(4+2*tex_cWidth)/texxscale - bh - 4 -2*cWidth; 


   for (n_ = nn+1; n_ <= nm; n_++)
   {
      if (upr > 1)
      {
          fseek(diagrq, (n_-1)*sizeof(csdiagram),SEEK_SET) ;
          FREAD1(ars,diagrq);
          flag = (ars.status != -1);
      }
      else
      {
         fseek(diagrp,(n_-1)*sizeof(adiagram),SEEK_SET);
         FREAD1(varc,diagrp);
         flag = (varc[0] < 0);
      }

      if (flag)
      {  dn_++;
         f_printf(out_tex,"%%  diagram # %d\n",(n_ -(int)nn));
         f_printf(out_tex,"\\begin{picture}(%d,%d)(0,0)\n",pictureX,pictureY);               
         picture(hadr2.how == 0 ? 1 : 2);
         f_printf(out_tex,"\\Text(%d,0)[b] {diagr.%i}\n",pictureX/2,dn_);
         f_printf(out_tex,"\\end{picture} \\ \n");
      }
   }

   texfinish();
}

/* end tex */

static void mtg_line(int x1,int y1,int x2,int y2,int arrowflag)
{

   switch (arrowflag)
   { case  0: tg_line(x1,y1,x2,y2);      break;
     case  1: tg_arrowline(x1,y1,x2,y2); break;
     case -1: tg_arrowline(x2,y2,x1,y1);
   }
   
}



static void  mouttextxy(int xm,int y)
{
 char      st[STRSIZ];
 int   super;

   super=(prtclbase[np-1].name[0]=='~');
   if(super) strcpy(st,prtclbase[np-1].name+1);
     else    strcpy(st,prtclbase[np-1].name); 
   trim(st);
   
   if(super)
     if(texflag) tg_outtextxy(xm,y,scat("\\stackrel{\\sim}{%s}",st));
      else 
      {  tg_outtextxy(xm,y,st);
         if(strlen(st)==1) strcpy(st,"~"); else strcpy(st,"~~");
         tg_outtextxy(xm,y-tg_textheight("H")/2,st);
      }
    else tg_outtextxy(xm,y,st);
}  


static void  mline(int x1_,int y1,int x2_,int y2,
        char r,char np_,char mm)
{
   int      h, y, xc, yc;
   int         sp, lt=SolidLn, uk;
   int arrowflag;
   int arrowshift=3;

   x1=x1_; x2=x2_; np=np_;
   
   sp = prtclbase[np-1].spin;
   switch(sp)
   { case 0: lt = DottedLn;   break;
     case 1: lt = SolidLn;    break;
     case 2: lt = DashedLn;   break;
   }
   tg_setlinestyle(lt,NormWidth);
   scrcolor(White,Black);
   
   arrowflag =0 ;  /*Shichanin-insert*/

   if (r >= 0)
   {
      xc = (x1 + x2) / 2;
      yc = (y1 + y2) / 2;
      if (prtclbase[np-1].anti >= np)  uk = 0; else uk = 1;
      if (prtclbase[np-1].anti != np)
         if ((uk == 0 && ur > 0) || (uk == 1 && ur < 0))  arrowflag= 1;
                                                    else  arrowflag=-1;
            
      if (uk == 1 && (x1 == x2 || r == 1 || r == 3))
         np = prtclbase[np-1].anti;
      if (ur < 0)
      {
         h = x1; x1 = x2; x2 = h;
         y = y1; y1 = y2; y2 = y;
         if (r < 3) r = 2 - r;
      }
      if (r == 3)  /* unknown case */
      { 
         y = yc - 2;
         if (ur > 0) h = x1; else h = x2;
         tg_settextjustify(1 - ur,BottomText);
      }
      else
      {
         if (x1 != x2)
         switch(r)
         {  
            case 0: /*left end text */ 
              y = y1;
              if(upr==1)
              {   h=x1-1;
                  tg_settextjustify(RightText,CenterText);
              }else
              {   h = x1 - 1 -cWidth   ;
                  tg_settextjustify(CenterText,CenterText);
              }   
              break;
            case 1: /* horizontal line ? */  
                h = xc; y = yc-1;
                if(arrowflag) y=y-arrowshift;
                tg_settextjustify(CenterText,BottomText);
                break;
            case 2: /* right end text */
                 y=y2;
                 if(upr==1)
                 {  h=x2+1;
                    tg_settextjustify(LeftText,CenterText);
                 }else
                 {
                    h = x2+1+cWidth ; 
                    tg_settextjustify(CenterText,CenterText);
                 }    
         } 
         else /* vertical line */
         { 
            switch(r)
            { case 1: /* unknown case */  
                   h = xc; 
                   y = yc + 4;
                   break;
                   
              case 0: /* line with left side label */
                   h = xc-1; 
                   if (arrowflag) h-=arrowshift;
                   y=yc;
                   tg_settextjustify(RightText,CenterText);
                   break;
                   
              case 2: /* line with right side label */
                   h = xc+1;
                   if (arrowflag) h+=arrowshift; 
                   y=yc;
                   tg_settextjustify(LeftText,CenterText);
                   break; 
             }
                  
         }
      }
      mouttextxy(bh + h,bv + y);
   }

   mtg_line(bh + x1,bv + y1,bh + x2,bv + y2,arrowflag);
   if (x1 == x[3])
   {
      intar[mm-1].y2 = bv + y1;
      if (ur == 1) intar[mm-1].nline = lt;
   }
   else
      if (x2 == x[3])
      {
         intar[mm-1].y2 = bv + y2;
         if (ur == 1) intar[mm-1].nline = lt;
      }
}


static char  elong(char m)
{char     el;

   if (m < 0)
      el = 1;
   else
   {  knot *with1 = &kn[m];
      el = elong(-with1->e1) + elong(-with1->e2);
   }
   return el;
}


static char     elongl(char m)
{ char     k, el;

   if (m == se || m == se1)
      el = 0;
   else
      if (m < 0)
         el = 100;
      else
      {  knot *with1 = &kn[m];
         k = MIN(elongl(-with1->e1),elongl(-with1->e2));
         if ((with1->e3 < 0)) k = MIN(k,elongl(-with1->e3));
         el = k + 1;
      }
   return el;
}


static void  order(char* e1,char* e2,char* m1,char* m2)
{ char     e;

     /* (e2>0 and e1>e2) or */   /*  or
      (elong(-e1)=elong(-e2) and kn[-e1].pt>kn[-e2].pt) */
      if ((*e2 > 0 && *e1 < 0) || (*e1 < 0 && *e2 < 0 &&
          elong(-(*e1)) > elong(-(*e2))))
      { e = *e1; *e1 = *e2; *e2 = e; e = *m1; *m1 = *m2; *m2 = e; }
}


static char  empt(char l)
{ char     k, em;

   if (l < 0)
      em = 0;
   else
   {  knot *with1 = &kn[l];
      k = empt(-with1->e1) + empt(-with1->e2);
      if (with1->pt == 0)
         em = k + 1;
      else
         em = k;
   }
   return em;
}


static void  triplet(char l)
{ char  l1, l2, l3, ll, m1, m2, m3, k;

   if (kn[l].pt == 0)
   {
      k = kn[l].fr;
      l1 = kn[k].e1; l2 = kn[l].e1; l3 = kn[l].e2;
      m1 = kn[k].n1; m2 = kn[l].n1; m3 = kn[l].n2;
      if (l1 == -l)
      {  l1 = kn[k].e2;
         m1 = kn[k].n2;
      }
      else
         if (tpc == 2)
            if (se == k)
               se1 = l;
            else
               if (se == l)
               {  ll = l1;
                  l1 = l2;
                  l2 = ll;
                  ll = m1;
                  m1 = m2;
                  m2 = ll;
                  se1 = k;
               }
      order(&l2,&l3,&m2,&m3);
      if (tpc == 1 || (se != l && se != k))
      {  order(&l1,&l2,&m1,&m2);
         order(&l2,&l3,&m2,&m3);
      }

      if (l3 > 0)
      {
         ll = 3;
         if ((se != l) && (se != k)) ks++;
      }
      else
         if (l2 > 0)
            ll = 2;
         else
            if (l1 > 0)
               ll = 1;
            else
               ll = 0;
      {  knot *with1 = &kn[k];
         with1->e1 = l1; with1->e2 = l2; with1->e3 = l3;
         with1->n1 = m1; with1->n2 = m2; with1->n3 = m3;
         with1->ty = ll;
      }

      {  knot *with1 = &kn[l];
         with1->e1 = l1; with1->e2 = l2; with1->e3 = l3;
         with1->n1 = m1; with1->n2 = m2; with1->n3 = m3;
         with1->ty = ll;
      }

      kn[l].fr = kn[k].fr; kn[l].pt = kn[k].pt;
   }
   else
   {  knot *with1 = &kn[l];
      l1 = with1->e1;
      l2 = with1->e2;
      l3 = with1->e3;
   }

   if (l1 < 0) triplet(-l1);
   if (l2 < 0) triplet(-l2);
   if (l3 < 0) triplet(-l3);
}


static void  ladd(char* l)
{
   (*l)++;
   if ((*l < 11) && (nk < 4)) return;
   print("  There is a bad diagram:");
   print("  Type ENTER, please:");
   getchar();
}


static void  filler(adiagram ar)
{char     nf=0, k, l=1;

   ni = 0; nk = 0; se = 100;

nn:if (nk > 3) ladd(&l);
   {  knot *with1 = &kn[nk];

      with1->pt = (char)abs(ar[l-1]);
      ladd(&l); with1->fr = nf;
      with1->ty = 0; with1->e3 = 0;

ll:   if (ar[l-1] > 0)
      {
         if (tpc == 1 || se != 100) ni++;
         if (se == 100) se = nk;
         with1->ty++;
         if (with1->ty == 2)
         {  with1->e2 = ar[l-1];
            with1->n2 = ni;
            ladd(&l); goto pp;
         }
         with1->e1 = ar[l-1];
         with1->n1 = ni;
         ladd(&l);
         goto ll;
      }
      nf = nk;
      with1->e2 = -(++nk);
      goto nn;
   }

pp:for (k = nk; k >= 0; k--)
   {  knot *with1 = &kn[k];
      if (with1->ty == 0)
         if (ar[l-1] > 0)
         {
            with1->e1 = ar[l-1];
            if (tpc == 1 || se != 100)
               with1->n1 = ++ni;
            if (se == 100) se = k;
            ladd(&l);
            with1->ty = 1;
         }
         else
         {  nf = k;
            with1->ty = -1;
            with1->e1 = -(++nk);
            goto nn;
         }
   }
   for (k = 0; k <= nk; k++)
   {  knot *with1 = &kn[k];
      if (with1->ty < 0) with1->ty = 0;
      if (tpc == 1 || k != se)
         order(&with1->e1,&with1->e2,&with1->n1,&with1->n2);
   }

   kl = elong(0) + 1;
   kt = empt(0);
   kk = kl - 2 - kt;
   if (tpc == 2) kn[se].e1 = prtclbase[kn[se].e1-1].anti;

   se1 = se;
   ks = 0;
   if (kt > 0) triplet(0);
}

static void  knot1(char l,int* yl,int* yyl)
{ char     m, m1, m2, m3, p, p1, p2, p3;
  int      z;

   if (l > 0) { m1 = l; m2 = 0; m3 = 0; p1 = pext; goto bb; }

aa:{ knot *with1 = &kn[-l];
      if ((tpc > 1) && ((l == -se) || (l == -se1)))
      {
         if (tpc == 3) mline(x[0],*yl,x[2],*yl,0,with1->e1,0);
         m1 = with1->e2; m2 = with1->e3; m3 = 0;
         p1 = with1->n2; p2 = with1->n3; 
      } 
      else 
      {  m1 = with1->e1; m2 = with1->e2; m3 = with1->e3; 
         p1 = with1->n1; p2 = with1->n2; p3 = with1->n3;
      } 
   } 
   if (ku == 1)
      {  ku = 0; 
         if (m1 < 0) { m = m1; m1 = m2; p = p1; p1 = p2; } 
         else        { m = m2; p = p2; }
         m2 = m3;
         p2 = p3; 
         if (m1 < 0) m3 = m1;
         if ((tpc == 3) && (elongl(-m3) > elongl(-m)))
            {
               if (m1 < 0) { m1 = m; p1 = p; }
               else        { m2 = m; p2 = p; }
               m = m3;
               p = p3; 
            }
         m3 = 0;

         { knot *with1 = &kn[-m]; 

               mline(x[2],*yl,x[2],*yl - ys,0,with1->pt,0);
               mline(x[2],*yl - ys,x[3],*yyl,2,with1->e1,with1->n1);
               *yyl += ys; 
               mline(x[2],*yl - ys,x[3],*yyl,2,with1->e2,with1->n2);
               *yyl += ys; 
         } 
      }


bb: if ((m1 > 0))
      {
         mline(x[2],*yl,x[3],*yyl,2,m1,p1);
         *yyl += ys;
         l = m2;
      }
   else l = m1; 

   if ((m2 > 0)) 
      { 
         mline(x[2],*yl,x[3],*yyl,2,m2,p2);
         *yyl += ys; 
         l = m3; 
      } 

   if ((m3 > 0)) 
         { 
            mline(x[2],*yl,x[3],*yyl,2,m3,p3);
            *yyl += ys;
         }

   if ((l >= 0)) return;

   { knot *with1 = &kn[-l];

         z = *yl + ys;
         if ((tpc == 3) && ((l == -se) || (l == -se1)))
              m = with1->ty - 1; 
         else m = with1->ty;
         if ((m > 1) && (z < *yyl)) z += ys; 
         mline(x[2],*yl,x[2],z,0,with1->pt,0);
         *yl = z;
   } 
      
   goto aa;
} 


static void  knot2(int* yl,int* yyl)
{ char     l, m, m1, m2, p1, p2;
  int      yz;

   l = 0;
   yz = *yl;
   { knot *with1 = &kn[-l];
      if (tpc == 2) { m1 = with1->e2; m2 = with1->e3;
                      p1 = with1->n2; p2 = with1->n3; } 
      else { m1 = with1->e1; m2 = with1->e2;
             p1 = with1->n1; p2 = with1->n2; } 
   }

aa: if (tpc == 3) 
      if ((m1 < 0) && (m2 < 0) && (elongl(-m1) < elongl(-m2))) 
         { m = m1; m1 = m2; m2 = m; }

   if ((tpc == 3) && ((l == -se) || (l == -se1))) 
      {
         mline(x[0],*yl,x[1],*yl,0,m1,0);
         m1 = m2; m2 = 0; p1 = p2; 
      } 

   if (m1 > 0) mline(x[1],yz,x[2],*yl,-1,m1,0);
   else mline(x[1],yz,x[2],yz,1,kn[-m1].pt,0);
   pext = p1;
   knot1(m1,&yz,yyl);

   if (m2 == 0) return;

   l = m2;
   yz += ys; 
   if ((yz + ys < *yyl) || ((yz < *yyl) && ((tpc == 1) ||
       (((l == -se) || (l == -se1)) && (kn[-l].e2 < 0))))) 
      yz += ys; 
   { knot *with1 = &kn[-l];
      
         if ((tpc < 3) && (with1->e2 > 0)) 
              mline(x[1],*yl,x[1],yz,-1,with1->pt,0);
         else mline(x[1],*yl,x[1],yz,0,with1->pt,0);
         *yl = yz; 
         m1 = with1->e1; p1 = with1->n1; 
         m2 = with1->e2; p2 = with1->n2; 
         if ((tpc < 3) && (m2 > 0))
            {  mline(x[1],yz,x[2],yz,1,with1->pt,0);
               knot1(l,yl,yyl);
               return;
            }
   }

   goto aa;
}


static void   draw(void)
{ 
   int      yl, yyl; 
   char     l; 

   if ((tpc == 2) && (se != 0) && (se1 != 0)) tpc = 3; 

   ku = 0; 

   if ((tpc == 2) && (kl == 3)) yyl = y[7]; 
   else yyl = y[8];

   if (((tpc == 3) && (kl == elongl(0) + 3))) yl = y[8];
   else
   if (((tpc < 3) || ((kt > 0) && (ks == 0))) &&
       ((kn[0].ty == 0) || ((kn[0].e2 < 0) &&
       (kn[0].e3 < 0)) || ((tpc == 2) &&
       (kn[0].e2 < 0) && (kn[-(kn[0].e2)].ty == 0)))) 
      { yl = y[5]; ku = 1; }
   else yl = y[7]; 

   if ((tpc == 2) && (kl == 4) && (kk == 1)) 
   { yyl = y[6]; yl = y[5]; } 

   if ((tpc == 2)) 
      if (((tpc == 2) && (kl == 4) && (kk == 1)) || 
          (kn[0].e2 < 0) && (kn[0].e3 < 0)) 
         { 
            mline(x[0],y[7],x[2],yl,0,kn[0].pt,0);
            mline(x[0],y[3],x[2],yl,0,kn[0].e1,0);
            knot1(0,&yl,&yyl); 
         }
      else
         {
            mline(x[0],yl - yh,x[1],yl,0,kn[0].pt,0);
            mline(x[0],yl + yh,x[1],yl,0,kn[0].e1,0);
            l = kn[0].e2;
            if ((l < 0))
               { 
                  if ((ku == 1)) mline(x[1],yl,x[2],yl,3,kn[-l].pt,0);
                  else mline(x[1],yl,x[2],yl,1,kn[-l].pt,0);
                  knot1(l,&yl,&yyl);
               } 
            else knot2(&yl,&yyl); 
         } 
   else 
   if (((tpc == 3) && ((kt == 0) || (ks != 0)) &&
       (kl != elongl(0) + 3)) || (tpc == 1) && 
       (kn[0].e2 < 0) && (kn[-(kn[0].e2)].ty == 0)) 
      {
         mline(x[0],yl,x[1],yl,0,kn[0].pt,0);
         knot2(&yl,&yyl); 
      }
   else
      {
         mline(x[0],yl,x[2],yl,0,kn[0].pt,0);
         knot1(0,&yl,&yyl);
      }
}


static void  reverse(void)
{ int      xx;

   xx = x[0]; x[0] = x[3]; x[3] = xx;
   xx = x[1]; x[1] = x[2]; x[2] = xx; ur = -ur;
}


static void  picture(char tprc)
{char tpp;
 int q;

   scrcolor(ecol,ebkc);
   tpc = tprc;


   if (upr > 1)
      filler(ars.dgrm1);
   else
      filler(varc);
   if (tpc == 2 && kl == 4 && kk == 1) 
      tpp = 0;
   else 
      tpp = tpc + 4;
   if (kl < tpp) {bv += ys; sflag = 1;}
   draw();
   if (kl < tpp) {bv -= ys; sflag = 0;}
   if (upr > 1)
   {
      scrcolor(ecol,ebkc);
      tg_settextjustify(RightText,BottomText);
      for (q = 0; q < ni; q++) intar[q].y1 = intar[q].y2;
      tpc = tprc;
      filler(ars.dgrm2);
      if (tpc == 2 && kl == 4 && kk == 1) 
         tpp = 0;
      else
         tpp = tpc+4;
      if (kl < tpp) bv += ys;
      bh += xn2;
      reverse();
      draw();
      if (kl < tpp) bv -= ys;
      reverse();
      scrcolor(LightGray,Black);
      bh -= xn2;
      for(q = 0; q < ni; q++)
      {
         tg_setlinestyle(intar[q].nline,NormWidth);
	 tg_line(bh+xn1+1,intar[q].y1,bh+xn2,intar[ars.lnk[q]-1].y2);
      }
      scrcolor(White,Black);
   }
}


int  showgraphs(char upravl)
/* char  upravl; *//*  upravl:=1(process),2(Q_diagrams) */
{int   i, l, u, uh = 0, nmax , jmax;
 int       hm_n, hm_x, vm_n, vm_x, vhh;
 word      ndel, nrest, ncalc;
 boolean   onlyview, tmpl;
 char      st[80], stc[80];
 char     *p;
 int       inflen[16];
 pointer   prtscr;
 int  saveColor,saveBkColor;

   if(upr==1) pictureX=70; else pictureX=160;
   pictureY=20*nout;
   strcpy(letterSize,"tiny");
 
   get_text(1,1,maxCol(),maxRow(),&prtscr);
   saveColor=fColor;
   saveBkColor=bColor;

   upr=upravl;
   onlyview = (upr < 0);
   if (onlyview) upr = -upr;

   strcpy(st, (upr > 1 ? infline2 : infline1));
   i = tg_textwidth(",");
   inflen[0] = tg_textwidth(strtok(st,",")) + i;
   jmax = 0;
   while((p = strtok(NULL,",")) != NULL)
   {  inflen[jmax + 1] = tg_textwidth(p) + i + inflen[jmax];
      jmax++;
   }

  if (upr > 1)
   {
     diagrq=fopen(DIAGRQ_NAME,"r+b");
     nmax = (int)filesize(diagrq)/sizeof(csdiagram)   ;
   }
   else
   {
       diagrp=fopen(DIAGRP_NAME,"r+b");
       nmax = (int)filesize(diagrp)/sizeof(adiagram);
   }
   if (nsub > 0)
      if (upr == 1)
         menup=fopen(MENUP_NAME,"r+b");
      else
         menuq=fopen(MENUQ_NAME,"r+b");

   ur = 1;

   tmpl = (upr == 1 && filesize(menup) > 60) ||
          (upr == 2 && filesize(menuq) > 60);

restart:
   rd_menu(upr,nsub,stc,&ndel,&ncalc,&nrest,&nn);
                    /*  Ndel  -  numder of deleted diagrams       */
                    /*  Ncalc -  numder of calculatad diagrams    */
                    /*  Nrest = Ntot-nd-Ncalc  */
                    /*  nn   -  Position of first diagrams in subproces  */
   n = nn + ndiagr; /*  n  - Position of selected diagrams  */
   nm = nn + ndel + ncalc + nrest;
                    /*  nm -Position of last  subprocess diagram +1  */

redrawScreen:
           
   ecol = White; ebkc = Black;
   nar.b = 1; nar.r = 2.;


   cHeight=tg_textheight("H");
   cWidth =tg_textwidth("H");

   clr_scr(White,Black);
              
   scrcolor(Black,White);
   tg_bar(0,0,tg_getmaxx(),cHeight);
   tg_settextjustify(LeftText,TopText);

   if (tmpl) sprintf(st,"  SUBPROCESS:    %s",stc);
   else  sprintf(st,"  PROCESS:  %s",stc);
   sbld(st,"%s    %d diagrams",st,(int)nm - (int)nn);
   tg_outtextxy(0,0,st);

   scrcolor(Yellow,Blue);
   tg_bar(tg_getmaxx() - tg_textwidth("CompHEP") ,0,tg_getmaxx() ,cHeight);
   tg_settextjustify(RightText,TopText);
   tg_outtextxy(tg_getmaxx(),1,"CompHEP" );

   scrcolor(White,Black);
   tg_settextjustify(LeftText,BottomText);
   tg_outtextxy(0,tg_getmaxy(),upr > 1 ? infline2 : infline1);
   
   scrcolor(LightGreen,Black);
   tg_setlinestyle(SolidLn,NormWidth);
   tg_rectangle(1,vm_n=cHeight+2,tg_getmaxx()-1,vm_x=tg_getmaxy()-cHeight-1); 
   tg_setviewport(hm_n=2,vhh=vm_n+1,hm_x=tg_getmaxx()-2,vm_x-1);

{  int quant,tWidth,tHeight;
   tWidth= tg_getmaxx()-4;
   quant=7*cWidth/2;
   dx=4; 
   if(upr>1)
   {  ndi_x=tWidth/(dx+ 12+4*2*cWidth+8*quant );
   
      xn=12+4*2*cWidth+8*quant;
      for(i=0;i<=3;i++) x[i]=4+2*cWidth+i*quant;
      xn1=6+2*2*cWidth + 3*quant;
      xn2=xn1+2*quant;
   }else
   {  ndi_x=tWidth/(dx+8+2*2*cWidth+3*quant );
      xn=8+2*2*cWidth+3*quant;
      for(i=0;i<=3;i++) x[i]=4+2*cWidth+i*quant;
   }  
   bhh=dx/2;
   
   tHeight=tg_getmaxy()-2*cHeight-4;
   quant=quant/2;
   dy=4;
   ndi_y=(tHeight-dy)/(dy+5+2*cHeight+8*quant);
   ynu=5+2*cHeight+8*quant;
   y[0]=ynu-3-cHeight;
   for(i=1;i<=8;i++) y[i]=y[i-1] - quant;
   
   ys = 2*quant;
   yh = quant;
    
/*printf("cWidth=%d,cHeight=%d,quant=%d\n",cWidth,cHeight,quant);          
*/
}
/*printf( "xn=%d, ynu=%d, dx=%d,dy=%d\n x[]=(%d,%d,%d,%d)\n y[]=(%d,%d,%d,%d,%d,%d,%d,%d,%d)\n"
,xn,ynu,dx,dy,x[0],x[1],x[2],x[3],y[0],y[1],y[2],y[3],y[4],y[5],
          y[6],y[7],y[8]);
*/          
   if (n == 0)
   {
      na = nn + (ndi_x*ndi_y);
      redraw(nn + 1);
   }
   else
   {
      na = n + (ndi_x*ndi_y);
      redraw(n);
   }


   while (TRUE)
   {
      tg_settextjustify(LeftText,BottomText);
      if (uh == KB_IC || uh == KB_DC)
      {  
           scrcolor(Black,Black);
           tg_settextjustify(RightText,BottomText);
           tg_outtextxy(tg_getmaxx(),tg_getmaxy(),"WAIT");
      }

      u = inkey();

      if (u == KB_MOUSE)
	 if (mouse_info.but1 == 2)
         {  if (mouse_info.y <= cHeight) u = KB_ESC;
            else
            if (mouse_info.y <= vm_x)
            {  if (mouse_info.x > hm_n && mouse_info.x < hm_x)
               {  int j;
                  i = (mouse_info.x - hm_n - bhh + dx/2)/(xn + dx) + 1;
                  j = (mouse_info.y - vhh - dy )/(ynu + dy) ;
                  if (i > ndi_x) i = ndi_x;
                  if (j>ndi_y) j=ndi_y;
                  i=i+j*ndi_x;
                  i += na;
                  if (i == n) u = ' ';
                  else
                  {  redraw(i);
                     continue;
                  }
               }
            }
            else
            {  i = 0;
               while (i <= jmax && inflen[i] < mouse_info.x) i++;
               u = upr == 2 ? inflinek2[i] : inflinek1[i];
            }
         }

      if (onlyview)
         switch (u)
         {
            case   ' ':   /* Space */
            case KB_DC:   /* Del */
            case KB_IC:   /* Ins */
               u = 0;
               be_be();
         }

      uh = u;
      if (u == KB_IC || u == KB_DC )
      {
         tg_settextjustify(RightText,BottomText);
         scrcolor(LightRed,Black);
         tg_outtextxy( tg_getmaxx(),tg_getmaxy(),"WAIT");
         refresh_scr();
      }

      switch (u)
      {  case '3':
         case KB_F3:        /* F3 pressed,view ghost diagrams */
            if ( upr == 2)
            {
               u = 1 - n;
               goto exi;
            }
         break;
         case 'L':        /* LaTex output for all undel. diagrs. */
         case 'l': 
             if(texmenu(&pictureX,&pictureY,letterSize))
             { 
               texinvoke(st);
               messanykey(20,14,scat("LaTeX output is written in file$ %s$",f_name));
             }
             break;

         case KB_SIZE: goto redrawScreen;

         case '1':
         case '?':
         case KB_F1:        /* F1 */
            switch(upr)
            {
               case 1:
                  if (onlyview)  showhelp(4202);
                  else showhelp(4201);
               break;
               case 2: showhelp(5101);
            }
            break;

         case  KB_ESC:      /* Esc */
            goto exi;

         case KB_DC:        /* Del -   Delete all subprocess  */

            if (upr > 1)
            {
               for ( l = nn; l < nm; l++)
               {
                  fseek(diagrq,l*sizeof(csdiagram),SEEK_SET);
                  FREAD1(ars,diagrq);
                  if (ars.status == 0)
                  {
                     ars.status = -1;
                     --(nrest);
                     ++(ndel);
                     ++(deldiag_sq);
                     fseek(diagrq,l*sizeof(csdiagram),SEEK_SET);
                     FWRITE1(ars,diagrq);
                  }
               }
            }
            else
            {
               for (l = nn; l < nm; l++)
               {
                   fseek(diagrp,l*sizeof(adiagram),SEEK_SET);
                   FREAD1(varc,diagrp);
                   varc[0] = (char)abs(varc[0]);
                   fseek(diagrp,l*sizeof(adiagram),SEEK_SET);
                   FWRITE1(varc,diagrp);
               }
               ndel += nrest;
               deldiag_f += nrest;
               nrest = 0;
            }
            wrt_menu(upr,nsub,stc,ndel,ncalc,nrest,nn);
            ndiagr = n - nn;
            goto  redrawScreen;
         case KB_IC: /*  Ins -  Restore all deleted  */

            if (upr > 1)
               for (l = nn; l < nm; l++)
               {
                  fseek(diagrq,l*sizeof(csdiagram),SEEK_SET);
                  FREAD1(ars,diagrq);
                  if (ars.status == -1) ars.status = 0;
                  fseek(diagrq,l*sizeof(csdiagram),SEEK_SET);
                  FWRITE1(ars,diagrq);
               }
            else
               for (l = nn; l < nm; l++)
               {
                  fseek(diagrp,l*sizeof(adiagram),SEEK_SET);
                  FREAD1(varc,diagrp);
                  varc[0] = -(char)abs(varc[0]);
                  fseek(diagrp,l*sizeof(adiagram),SEEK_SET);
                  FWRITE1(varc,diagrp);
               }
            nrest += ndel;
            if (upr == 1)
               deldiag_f -= ndel;
            else
               deldiag_sq -= ndel;
            ndel = 0;

            wrt_menu(upr,nsub,stc,ndel,ncalc,nrest,nn);
            ndiagr = n - nn;
            goto /*restart*/redrawScreen;

         case '-':   /*  '-' - previous subproces  */
            if (nn > 0)
            {  n = 0;
               nsub--;
               ndiagr = 1;
               goto restart;
            }
         break;

         case '+':  /*  '+' - next subproces  */
            if (nm < nmax)
            {  n = nm + 1;
               nsub++;
               ndiagr = 1;
               goto restart;
            }
         break;

         case KB_HOME:
            redraw(nn + 1); /*  'Home' -first diagr in subproces  */
         break;

         case KB_END:
            redraw(nm);     /*  'End ' -last  diagr in subproces  */
         break;

         case KB_PAGED:     /* PgDn */
            redraw(n + (ndi_x*ndi_y));
         break;

         case KB_PAGEU:     /* PgUp */
            redraw(n - (ndi_x*ndi_y));
         break;

         case KB_LEFT:      /* Left Arrow */
            redraw(n - 1);
         break;

         case KB_RIGHT:     /* Right Arrow */
            redraw(n + 1);
         break;

         case KB_UP:        /* Arrow Up */
               redraw(n - ndi_x);
         break;

         case KB_DOWN:      /* Arrow Down */
               redraw(n + ndi_x);
         break;

         case  KB_ENTER:    /*  Enter -  Show */
            if (upr == 2)
            {  fseek(diagrq,(n-1)*sizeof(csdiagram),SEEK_SET);
               FREAD1(csdiagr,diagrq);
               if (csdiagr.status != 1)
                  be_be();
               else
                  goto exi;
            }
         break;

         case ' ':          /*  Space  Bar   del/ins proceess  */
            {
               tg_settextjustify(LeftText,BottomText);
               mbase(n);
            }
            l = 0;
            if (upr > 1)
            {
               fseek(diagrq,(n-1)*sizeof(csdiagram),SEEK_SET);
               FREAD1(ars,diagrq);
               if (ars.status == -2)
	       cleartext(bh+48,bv+ynu-2,"Out of memory");

               if (ars.status == 0 || ars.status == -2)
               {  l = -1;
                  ars.status = -1;
               }
               else
                  if (ars.status == -1)
                  {  l = 1;
                     ars.status = 0;
                  }
                  fseek(diagrq,(n-1)*sizeof(csdiagram),SEEK_SET);
                  FWRITE1(ars,diagrq);
            }
            else
            {
               fseek(diagrp,(n-1)*sizeof(adiagram),SEEK_SET);
               FREAD1(varc,diagrp);
               varc[0] = -varc[0];
               fseek(diagrp,(n-1)*sizeof(adiagram),SEEK_SET);
               FWRITE1(varc,diagrp);
               if (varc[0] < 0)  l = 1;  else  l = -1;
            }

            if (l != 0)
               {
                  if (l == 1)
                  cleartext(bh + 48,bv + ynu-2,"DEL");
                  if (l == -1)
                  {
                     scrcolor(LightRed,Black);
                     tg_outtextxy(bh + 48,bv + ynu-2,"DEL");
                     scrcolor(ecol,Black);
                  }
               }
            if (nsub > 0 && l != 0)
            {
               nrest += l;
               ndel -= l;
               wrt_menu(upr,nsub,stc,ndel,ncalc,nrest,nn);
               if (upr == 1)
                  deldiag_f -= l;
               else
                  deldiag_sq -= l;
            }
         break;

         case '#':
         case 'N':
         case 'n':
            i = 0;
            while (isdigit(u = inkey()))
               i = 10 * i + u - '0';
            if (u == KB_ENTER)
               redraw(i + nn);
      }  /* Case */
   }  /* While */

exi:
   if (upr == 1)
   { 
     /* Write the list of active diagrams to file */
     FILE* diagfile; 
     diagfile = fopen(scat("%sresults%cdiag.conf", pathtouser, f_slash), "w");
     fprintf(diagfile, "# Active diagrams for process %s\n", processch);
     for (l = 0; l < nmax; l++) {
       fseek(diagrp,l*sizeof(adiagram),SEEK_SET);
       FREAD1(varc,diagrp);
       fprintf(diagfile, "%d ", l+1);
       if (varc[0]<0) {
	 fprintf(diagfile, "T\n");
       } else {
	 fprintf(diagfile, "F\n");
       }
     }
     fclose(diagfile);

     fclose(menup);
     fclose(diagrp);
   }
   else
   {  fclose(menuq);
      fclose(diagrq);
   }  
   clr_scr(saveColor,saveBkColor);
   put_text(&prtscr);
   ndiagr = n - nn;
   return u;
}
