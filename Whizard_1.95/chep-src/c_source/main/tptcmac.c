#define tptccomp
#include "tptcmac.h"
#include "syst2.h"
/*
extern int unlink(const char * filename);
*/

 integer  ioresult=0;
 boolean pseudo_g=FALSE;

integer chround(double  x)
{integer n;
	x += 0.5;
	n = (integer) x;
	return (double) n > x ? n - 1 : n;
}

long chlround(double  x)
{long n;
	x += 0.5;
	n = (long) x;
	return (double) n > x ? n - 1 : n;
}


static boolean isUnsignedInt(char *  str)
{
  int i=0;
  int k=0;
  while ( isdigit(str[i] ) ){i++;k++;}
  if (k==0)  return FALSE ;
  if (str[i] == 0)  return TRUE; else return FALSE;
}




static boolean isInt(char *  str)
{
  int i=0;
  if (( str[i]=='+') || (str[i] == '-') )i++;
  return isUnsignedInt(str+i);
}


static boolean isReal(char *  str)
{
  int k;
  int i=0;
  int ePos= -1;
  int pointPos= -1;
  char buff[STRSIZ];
  while ( str[i] != 0 )
  {  if ((str[i]=='E')||(str[i]=='e')) ePos=i;
	  if (str[i]=='.')  pointPos=i;
	  i++;
	}

	strcpy(buff,str);

	if (ePos != -1)
	{   if (!isInt(buff+ePos+1)) return FALSE; else
		 {   k=atoi(buff+ePos+1);
			  if ((k>16)||(k<-16)) return FALSE;
		 }
		 buff[ePos]=0;
	}
	if (pointPos != -1)
	{  if (!isUnsignedInt(buff+pointPos+1)) return FALSE; else
			buff[pointPos]=0;
	}
	return isInt(buff);

}


int intVal(char* str, int* err)
{
  if( ! isInt(str) ) { *err=1; return 0;}
  *err=0;
  return atoi(str);
}

double realVal(char* str, int* err)
{
  if( ! isReal(str) ) { *err=1; return 0;}
  *err=0;
  return atof(str);
}


void  trim(char* p)
{int   n1=0, n2, k = -1;
   n2 = (int)strlen(p)-1;
   while(!isgraph(p[n1]) && n1 <= n2) n1++;
   while(!isgraph(p[n2]) && n1 <  n2) n2--;
   while(++k < n2-n1+1)  p[k] = p[k+n1];
   p[k] = '\0';
}



long filesize(FILE * f)
{long l,r;

 l=ftell(f);
 fflush(f);
 fseek(f,0,SEEK_END);
 r=ftell(f);
 fseek(f,l,SEEK_SET);
 return r;
}

   
/*
 *   int setof(int a,int b,...,_E)
 *      construct and return a set of the specified character values
 *
 *   boolean inset(int ex,int setrec)
 *      predicate returns TRUE if expression ex is a member of
 *      the set parameter
 *   (It is suppoused enum type setrec takes two bytes here; a,b,ex < 16)
 *
 *   setofbyte setofb(int a,int b,...,_E)
 *   boolean insetb(word ex, setofbyte setrec)
 *   Here a,b,ex<256; setofbyte=word[16].
 *
 *
 *   By V.Edneral
 */

int setof(int i, ...)
{va_list v;
 int k=0;
 int r=0;

   va_start(v,i);

   while(i!=_E)
   {  if (i > 15)
         {  fprintf(stderr,"Too large setof element!! Hit enter:");
            getchar(); exit(-1);
         }
      if (i == UpTo)  i=va_arg(v,int); else k=i;
      for(; k <= i; k++) r |= 1<<k;
      i=va_arg(v,int);
   }
   va_end(v);
   return r;
}

boolean inset(int a,int sp)
{  if (a>15)
	{  fprintf(stderr,"Too large setof element!! Hit enter:");
      getchar();
      exit(-1);
   }
   return ((1 << a) & sp) != 0;
}


word * setofb(int i, ...)
{va_list v;
 int k;
 static setofbyte r;

   va_start(v,i);
   memset( (char *) r,0,sizeof(setofbyte));
   while(i != _E)
   {  if(i>255)
		{  fprintf(stderr,"Too large setofb element!! Hit enter:");
			getchar(); return 0 ;
      }      
      if(i == UpTo) i=va_arg(v,int); else k=i;
      for(;k<=i;k++) r[k >> 4] |= 1 << (k & 0xF);
      i=va_arg(v,int);
   }
   va_end(v);
   return r;
}

word * setofb_add1(setofbyte a, int k)
{ 
  a[k >> 4] |= 1 << (k & 0xF);
  return a;
}


void  setofb_zero(setofbyte sp)
{  memset((char *)sp,0,sizeof(setofbyte));
}


boolean insetb(word a,setofbyte sp)
{  if (a>255)
   {  fprintf(stderr,"Too large setofb element!! Hit enter:");
      getchar();
      exit(-1);
   }
   return ((1 << (a & 0xF)) & sp[a >> 4]) != 0;
}

void  setofb_cpy(setofbyte dest,setofbyte source)
{  memcpy((char *) dest,(char *) source, sizeof(setofbyte)); }

word  * setofb_uni(setofbyte a,setofbyte b)
{static setofbyte res;
 int k;
   for( k=0; k<16; k++) res[k] = a[k] | b[k];
   return res;
}

word  * setofb_aun(setofbyte a,setofbyte b)
{static setofbyte res;
 int k;
   for( k=0; k<16; k++) res[k] = a[k] & (~ b[k]);
   return res;
}

word  * setofb_its(setofbyte a,setofbyte b)
{static setofbyte res;
 int k;
   for( k=0; k<16; k++) res[k] = a[k] & b[k];
   return res;
}


boolean setofb_eql(setofbyte a,setofbyte b)
{int k;
   for( k=0; k<16; k++)
      if( a[k] != b[k]) return FALSE;
   return TRUE;
}

boolean setofb_eq0(setofbyte a)
{int k;
   for( k=0; k<16; k++)
      if( a[k] != 0) return FALSE;
   return TRUE;
}

void setofb_dpl(setofbyte a)
{int k;
   for(k= 0; k< 64; k++) if(insetb(k,a)) fprintf(stderr,"%d ",k);
   fprintf(stderr," ");
   for(    ; k<128; k++) if(insetb(k,a)) fprintf(stderr,"%d ",k);
   fprintf(stderr," ");
   for(    ; k<192; k++) if(insetb(k,a)) fprintf(stderr,"%d ",k);
   fprintf(stderr," ");
   for(    ; k<256; k++) if(insetb(k,a)) fprintf(stderr,"%d ",k);
   fprintf(stderr," ");
}

/*
 * copy len bytes from the dynamic string dstr starting at position from
 *
 */

char *copy(char* str,integer from,integer len)
{
   static char buf[STRSIZ];
   buf[0]='\0';
	if (from>strlen(str))    /* copy past end gives null string */
      return buf;
   strcpy(buf,str+from-1);  /* skip over first part of string */
	if (len < STRSIZ)
		buf[len] = '\0';      /* truncate after len characters */
	return buf;
}


/*
 * String/character concatenation function
 *
 * This function takes a sprintf-like control string, a variable number of
 * parameters, and returns a pointer a static location where the processed

 * string is to be stored.
 *
 */

 static int scatBufNum=0;



char *scat(char *control, ...)
{va_list args;
  static char buf[4][STRSIZ];
  va_start(args, control);     /* get variable arg pointer */
	scatBufNum++;
	if (scatBufNum>3) scatBufNum=0;
	vsprintf(buf[scatBufNum],control,args); /* format into buf with variable args */
   va_end(args);                /* finish the arglist */

	return buf[scatBufNum];                  /* return a pointer to the string */
}

/*
 * string build - like scat, sprintf, but will not over-write any
 *                input parameters
 */


void sbld(char *dest, char *control, ...)
{va_list args;
 char buf[STRSIZ];

   va_start(args, control);     /* get variable arg pointer */

   vsprintf(buf,control,args);  /* format into buf with variable args */
   va_end(args);                /* finish the arglist */

   strcpy(dest,buf);            /* copy result */
}



/*
 * spos(str1,str2) - returns index of first occurence of str1 within str2;
 *    1=first char of str2
 *    0=nomatch
 */

integer spos(char* str1,char* str2)
/* char *str1;
 char *str2;*/
{integer i,k,n;
   n = (integer)strlen(str2)-(integer)strlen(str1);
   for(k = 0; k <= n; k++)
   {  i = 0;
      while(str1[i] != '\0' && str1[i] == str2[i + k]) i++;
      if (str1[i] == '\0') return k+1;
   }
   return 0;
/*
   char *res;
   res = strstr(str2,str1);
   if (res == NULL)
      return 0;
   else
      return (integer)(res - str2 + 1);
*/
}


/*
 * cpos(str1,str2) - returns index of first occurence of c within str2;
 *    1=first char of str2
 *    0=nomatch
 */

integer cpos(char c,char* str2)
/* char c;
 char *str2;*/
{
   char *res;
   res = strchr(str2,c);
   if (res == NULL)
      return 0;
   else
      return (integer)(res - str2 + 1);
}

/*
 * Insert - function for inserting a string into the string
 */

void strinsert(char* subst,char* s,integer from)
/* char *subst;
 char *s;
 integer from ; */ /* from>=1 */
{char buf[STRSIZ];
   --from;
   strcpy(buf, s + from);
   strcpy(s + from, subst);
   strcpy(s + from + (integer)strlen(subst), buf);
}


void doub_str(char * s,  double  z )
{
	char buff[40],power[10];
	char * cpower,* cbuff  ;
	sprintf(buff,"%20.10lE",z);
	cpower=strchr(buff,'E')-6;
	cpower[0]='E';
	cpower[1]=cpower[7];
	cpower[2]=0;
	cpower=cpower+8;
	strcpy(power,cpower);
/*	cpower[0]=0;*/
	cpower=power;
	while ((cpower[0]=='0')&&(strlen(cpower)>2) ) cpower=cpower+1;
	cbuff=buff;
	while (cbuff[0]==' ') cbuff=cbuff+1;

	if (cbuff[0]=='-'||cbuff[0]=='+') sprintf(s, "%s%s",cbuff,cpower);
				    else  sprintf(s," %s%s",cbuff,cpower);
}
