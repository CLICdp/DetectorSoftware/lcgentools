#include "tptcmac.h"
#include "syst2.h"
#include "physics.h"
#include "global.h"
#include "procvar.h"
#include "syst2.h"
#include "optimise.h"
#include "files.h"
#include "crt_util.h"

#include "f90_lstr.h"

FILE * f90file;

#define degreeptr struct degreerec *
typedef struct degreerec
   {
      degreeptr    next;
      char         name[10];
      byte         deg;
   }  degreerec;
#undef degreeptr
typedef struct degreerec *degreeptr;

typedef degreeptr degname[nvarhighlimit];

static degname   *degnamesptr;
int    f90constcount;
int    f90degnamecount;
int    f90tmpNameNum;
int    f90tmpNameMax=0;

static char buff[2000];  /* used in f90write_string
	declared as static ie escape
	stack overflow on PC
*/

/* ==================================================================== */
/* Output to the f90 file.  Allow for indentation. */
void fout(int indent, char* format, ... ) {
  va_list args;
  char dump[STRSIZ];
  int i, r;
  const int blank = ' ';

  for (i=1; i<=indent; i++)  { r = putc(blank, f90file); }
  va_start(args, format);
  vsprintf(dump, format, args);
  va_end(args);
  r = fputs(dump, f90file);
  if (r==EOF) {
    messanykey(5, 20, "I/O Error (disk full?)$");  exit(0);
  }
}

/* ==================================================================== */


void  f90initdegnames (void) {
  integer   k;

  degnamesptr = (degname *)getmem((word)sizeof(degreeptr) 
				  * (nmodelvar + maxsp+1));
  for (k = 0; k < nmodelvar + maxsp+1; k++) (*degnamesptr)[k] = NULL;
  f90degnamecount=0;
}


void  f90cleardegnames (void) {
  integer  k;
  degreeptr    p, q;

  for (k = 0; k < nmodelvar + maxsp+1; k++) {
    q = (*degnamesptr)[k];
    while (q != NULL) {
      p = q;
      q = p->next;
      free(p);
    }
  }
  free(degnamesptr);
}


void  f90write_str (char* name, longstrptr longs) {
  integer  i, j, rest;
  integer  l, dl,p;

  fout(4, "%s%c", name, eq);
  if (longs == NULL || longs->len == 0) {
    fout(0, "0\n");
    return;
  }
  p=0;
  dl = 1 + (integer)strlen(name);
  rest = longs->len;
  i = 0;
  while (rest != 0) {
    l = MIN(rest,65 - dl);
    if (dl == 0) {
      for (j=0;j<5;j++)  buff[p++]=' ';
      buff[p++]='&';
    }
    for(j = 0; j < l; j++) buff[p++] =longs->txt[i + j];
    i += l;
    rest -= l;
    dl = 0;
    if (rest != 0) buff[p++]='&';
    buff[p++]='\n';
  }
  buff[p++]  =0;
  fout(0, buff);
}


static void  newconstantname(char* s) {
   sbld(s,"C(%d)",++f90constcount);
}


void   f90addstring(longstrptr longs, char* s) {
  char  name[STRSIZ];
  integer   i, l;
  word ll;

  ll = longs->len;
  l  = (integer)strlen(s);
  if (ll + l > buffsize) {
    sprintf(name,"tmp(%d)",++f90tmpNameNum);
    f90write_str(name, longs);
    longs->len = 0;
    f90addstring(longs, name);
    ll = longs->len;
  }
  for (i = 0; i < l; i++) longs->txt[ll + i] = s[i];
  longs->len += l;
}


static pointer  gorner(char* s, longstrptr pmult, longstrptr psum) {
  char         name[STRSIZ], name2[STRSIZ];
  longstrptr   ans;
  pointer      pchange;

  if (pmult == NULL) return (pointer)psum;
  ans = (longstrptr)getmem(sizeof(longstr));
  ans->len = 0;
  f90addstring(ans,s);

  if (3 + ans->len + pmult->len > buffsize) {
    sprintf(name,"tmp(%d)",++f90tmpNameNum);
    f90write_str(name,pmult);
    f90addstring(ans,scat("*%s",name));
  }
  else {
    if (pmult->txt[0] == '+') {
      pmult->txt[0] = '(';
      f90addstring(ans,"*");
    }
    else
      f90addstring(ans,"*(");
    memcpy(&(ans->txt[ans->len]),pmult->txt,pmult->len);
    ans->len += pmult->len;
    f90addstring(ans,")");
  }
  free(pmult);

  if (psum == NULL) return (pointer)ans;
  if (ans->len + psum->len > buffsize) {
    sprintf(name,"tmp(%d)",++f90tmpNameNum); 
    if (ans->len > psum->len) {
      pchange = (pointer)ans;
      ans = psum;
      psum = (longstrptr)pchange;
    }
    f90write_str(name,psum);
    if (ans->len + strlen(name) >= buffsize) {
      sprintf(name2,"tmp(%d)",++f90tmpNameNum);
      f90write_str(name2,ans);
      ans->len = 0;
      f90addstring(ans,scat("+%s+%s",name,name2));
    }
    else f90addstring(ans,scat("+%s",name));
  } 
  else {
    memcpy(&(ans->txt[ans->len]),psum->txt,psum->len);
    ans->len += psum->len;
  }
  free(psum);
  return (pointer)ans;
}


void  f90putnames(void) {
  infoptr     i;
  i = info;
  while (i != NULL) {
    if (i->consttype == numb)    sbld(i->name,"%ld",i->ival);
    else                         newconstantname(i->name);
    i = i->next;
  }
}   /* F90putnames */


static char *  writevardeg(byte nv, byte deg) {
  degreeptr     p;
  static char namest[21];
  if (deg == 1) return (*varnamesptr)[nv-1];
  p = (*degnamesptr)[nv-1];
  while (p != NULL)
    if (p->deg == deg)  return p->name;
    else                p = p->next;
  p = (degreeptr)getmem(sizeof(degreerec));
  p->deg = deg;
  sbld(namest,"S(%d)",++f90degnamecount);
  
  /*     newconstantname(namest);*/
  strcpy(p->name,namest);
  p->next = (*degnamesptr)[nv-1];
  (*degnamesptr)[nv-1] = p;
  fout(4,"%s%c%s**%u\n",
       namest,eq,(*varnamesptr)[nv-1],(unsigned int) deg);
  return namest;
}


static pointer  smpl_emit(varptr ex) {
  longstrptr   ans;
  char         s[STRSIZ];
  integer      k;
  byte         bt, nv, deg;
  boolean      star;
  varptr       ex_, exbeg;

  if (ex == NULL) return NULL;
  /*  */
  if (ex->sgn == '-') {
    ex_ = ex;
    while (ex_->next != NULL && ex_->next->sgn == '-')
      ex_ = ex_->next;
    if (ex_->next != NULL) {
      exbeg = ex_->next;
      ex_->next = exbeg->next;
      exbeg->next = ex;
      ex = exbeg;
    }
  }
  
  ans = (longstrptr)getmem(sizeof(longstr));
  ans->len = 0; 
  while (ex != NULL) { 
    sbld(s,"%c",ex->sgn); 
    star = (strcmp((ex->coef)->name,"1") != 0); 
    if (star || strlen(ex->vars) == 0)
      sbld(s,"%s%s",s,(ex->coef)->name); 
    if (strlen(ex->vars) != 0) { 
      bt = (byte)(ex->vars[0]); 
      deg = 1; 
      for (k = 2; k <= (integer)strlen(ex->vars); k++) { 
	nv = (byte)(ex->vars[k-1]); 
	if (bt != nv) {
	  if (star) sbld(s,"%s*",s); 
	  else      star = TRUE; 
	  sbld(s,"%s%s",s,writevardeg(bt,deg)); 
	  deg = 1; 
	  bt = nv; 
	} 
	else ++(deg); 
      } 
      if (star) sbld(s,"%s*",s); 
      else      star = TRUE; 
      sbld(s,"%s%s",s,writevardeg(bt,deg)); 
    } 
    f90addstring(ans,s); 
    ex = ex->next; 
  } 
  return (pointer) ans; 
}

static pointer v_gorner (char ch, byte deg, pointer pmult, pointer psum) {
  char b[STRSIZ];
  sbld(b,"+%s",writevardeg((byte)ch,deg));
  return gorner(b,pmult,psum);
}

static pointer c_gorner (infoptr i, pointer pmult, pointer psum) {
  char b[STRSIZ];
  sbld(b,"+%s",i->name);
  return gorner(b,pmult,psum);
}


void  f90writer(char* name, varptr fortformula) {
  longstrptr   tmp;
  
  f90tmpNameNum=0;
  tmp = (longstrptr) emitexpr (fortformula, smpl_emit, v_gorner, c_gorner);
  f90write_str(name,tmp);
  if (tmp != NULL) free(tmp);
  if (f90tmpNameMax<f90tmpNameNum) f90tmpNameMax=f90tmpNameNum;
}

void  f90write_const (void) {
  infoptr      i;
  byte         cutleveltmp;

  cutleveltmp = cutlevel;
  cutlevel    = nmodelvar+maxsp+1;

  i = info;
  while (i != NULL) {
    if (i->consttype == expr)   { 
      f90writer(i->name,(pointer) i->const_);
    }
    i = i->next;
  }
  cutlevel = cutleveltmp;
}   /*  WriteConst  */


void  f90initConsts (void) {
  /*  f90constcount = 0; */
  initinfo();
}
