#include <math.h>
#include "tptcmac.h"
#include "syst2.h"
#include "crt.h"
#include "global.h"
#include "physics.h"
#include "screen.h"
#include "files.h"
#include "crt_util.h"
#include "showgrph.h"
#include "optimise.h" 
#include "procvar_.h"
#include "n_compil.h"
#include "num_tool.h"
#include "num_serv.h"
#include "graf.h"


#include "cs_22.h"

#ifndef M_PI
#define M_PI		3.14159265358979323846
#endif


static double         lim1[maxsp+1], lim2[maxsp+1];
static double         totcoef;
static double         cos1, cos2;
static double         eps;
static boolean        recalc;
static prgcodeptr     prg;
static char           procname[STRSIZ];


static void  infotext(void)
{
   scrcolor(Yellow,Blue);
	goto_xy(1,22); print("-------------------");
	goto_xy(1,16); print("   Information     \n");
   scrcolor(LightRed,Black);
	print(" (Sub)Process : \n");
	print(" Energy       : \n");
	print(" f - angle      \n");
	print(" Cos(f):    min=                   max=                  \n");
	print(" Cross Section: \n");
   scrcolor(White,Black);
	goto_xy(18,17); print(procname);
	goto_xy(18,19); print("between  in-        and   out-");
   scrcolor(Yellow,Black);
	goto_xy(30,19); print(pname[0]);
	goto_xy(48,19); print(pname[2]);
   scrcolor(White,Black);
}


static void  writeinformation(void)
{
   scrcolor(White,Black);
	goto_xy(18,18); print("%12lf [GeV] ",(*vararr)[maxsp].tmpvalue);   /*  Energy  */
	goto_xy(18,20); print("%8.6lf",cos1);
	goto_xy(42,20); print("%8.6lf",cos2);
}


static void  correctanglerange(void)
{
label_1:
   if (correctDouble(18,20,"",&cos1,0)) 
   { 
      if (cos1 > 1 || cos1 < -1) 
      { 
         messanykey(10,10," Range check error $ "); 
         goto label_1;
      } 
      recalc = TRUE;
   } 
   else
      goto exi;

label_2:
   if (correctDouble(41,20,"",&cos2,0)) 
   { 
      if (cos2 > 1 || cos2 < -1) 
      { 
         messanykey(10,10," Range check error $ "); 
         goto label_2;
      } 

      if (cos2 < cos1) 
      { 
         messanykey(10,10," Range check error $ "); 
         goto label_1;
      } 
      recalc = TRUE;
   } 
   else
      goto label_1;
exi:
   infotext();
   writeinformation(); 
}


static void  calccoef(void)
{byte  i;
 double  lambda12, lambda34, s_;
 double  ms, mm1, mm2, mm3, mm4;
 double cc;

   s_ = (*vararr)[maxsp].tmpvalue;
   err_code = 0;
   calcfunctions(&err_code);
   if (err_code != 0)  goto errorexit;
   for (i = 0; i < nin + nout; i++)  if (*pmass[i] < 0) goto errorexit;
   s_ = s_ * s_;
   ms = *pmass[0] + *pmass[1];
   if (ms >= (*vararr)[maxsp].tmpvalue)	goto errorexit;
   lambda12 = sqrt((s_ - sqr(ms)) * (s_ - sqr(*pmass[0] - *pmass[1])));
   ms = *pmass[2] + *pmass[3];
   if (ms >= (*vararr)[maxsp].tmpvalue)	goto errorexit;
   lambda34 = sqrt((s_ - sqr(ms)) * (s_ - sqr(*pmass[2] - *pmass[3])));

   totcoef = 0.38937966 * 1.E9 * lambda34 /(32.0 * M_PI * lambda12 * s_);

   mm1 = sqr(*pmass[0]);
   mm2 = sqr(*pmass[1]);
   mm3 = sqr(*pmass[2]);
   mm4 = sqr(*pmass[3]);

   /* p1.p3 */
   cc = ((s_ + mm1 - mm2) * (s_ + mm3 - mm4) + lambda12 * lambda34);

   lim1[sp_pos(1,3)] = (sqr(s_) * (mm3 + mm1) - 2 * s_ * (mm1 * mm4 + mm2 * mm3) +
                       (sqr(mm1) + sqr(mm2)) * mm3 + (sqr(mm3) + sqr(mm4)) * mm1 -
                        2 * mm1 * mm3 * (mm2 + mm4)) /cc;

   lim2[sp_pos(1,3)] =cc/(4.0 * s_);

   /* p1.p4 */
   cc = ((s_ + mm1 - mm2) * (s_ + mm4 - mm3) + lambda12 * lambda34);

   lim2[sp_pos(1,4)] = (sqr(s_) * (mm4 + mm1) - 2 * s_ * (mm1 * mm3 + mm2 * mm4) +
                       (sqr(mm1) + sqr(mm2)) * mm4 + (sqr(mm4) + sqr(mm3)) * mm1 -
                       2 * mm1 * mm4 * (mm2 + mm3)) /cc;

   lim1[sp_pos(1,4)] = cc/(4.0 * s_);

   /* p2.p3 */
   cc = ((s_ + mm2 - mm1) * (s_ + mm3 - mm4) + lambda12 * lambda34);

   lim2[sp_pos(2,3)] = (sqr(s_) * (mm3 + mm2) - 2 * s_ * (mm2 * mm4 + mm1 * mm3) +
                       (sqr(mm1) + sqr(mm2)) * mm3 + (sqr(mm3) + sqr(mm4)) * mm2 -
                       2 * mm2 * mm3 * (mm1 + mm4)) /cc;
   lim1[sp_pos(2,3)] = cc/(4.0 * s_);


   /* p2.p4 */
   cc = ((s_ + mm2 - mm1) * (s_ + mm4 - mm3) + lambda12 * lambda34);

   lim1[sp_pos(2,4)] = (sqr(s_) * (mm4 + mm2) - 2 * s_ * (mm2 * mm3 + mm1 * mm4) +
                       (sqr(mm1) + sqr(mm2)) * mm4 + (sqr(mm4) + sqr(mm3)) * mm2 -
                       2 * mm2 * mm4 * (mm1 + mm3)) /cc;
   lim2[sp_pos(2,4)] = cc/(4.0 * s_);


    /* p1.p2 */
    (*vararr)[sp_pos(1,2)].tmpvalue = (s_ - mm1 - mm2) / 2.0;

    /* p3.p4 */
    (*vararr)[sp_pos(3,4)].tmpvalue = (s_ - mm3 - mm4) / 2.0;
       
    calcconstants();
    err_code = 0;
    return;

errorexit:
   if (err_code == 0)  err_code = 3;
}


static void  calcscalars(double  cos_f)
{int  i,j,pos; 
   for (i = 3; i <= 4; i++)
   for (j = 1; j < MIN(i,3)  ; j++)
   { pos=sp_pos(j,i); 
     (*vararr)[pos].tmpvalue = 0.5*(lim1[pos]*(cos_f+1)+lim2[pos]*(1-cos_f));
   }        

} 


static double  cross_section(double  x)
{double  r; 

   calcscalars(x); 
   r = matrixelement(prg); 
   if (strcmp(pname[2],pname[3]) == 0) 
   {
       calcscalars(-x); 
       r = (r + matrixelement(prg)) * 0.5; 
   }
   if (err_code != 0) return 0; 
   return r * totcoef; 
} 


static boolean fillseq(int  n,realseq*  seq)
{

 int        i;
 double      step, ans;
 realseq     seqtmp;

   *seq=NULL;
   err_code = 0;
   step = (cos2 - cos1) / (n - 1);
   for (i = 1; i <= n; i++)
   {
      ans=cross_section(cos1+(i-1)*step);
      if (err_code != 0)
      {
          disposerealseq(seq);
           return FALSE;
      }
      seqtmp = (realseq)getmem(sizeof(realseqrec));
      seqtmp->next = *seq;
      seqtmp->x = cos1 + (i - 1) * step;
      seqtmp->y = ans;
      *seq = seqtmp;
   }
   return TRUE; 

}


static void  drawgraph(void)
{
 shortstr    upstr;
 int          k = 1,n;
 void *     pscr=NULL;
 realseq  seq; 

   if (! fillseq(50,&seq) ) return;

   do
   {
men:   menu1(56,14,"","\026"
          " Show   Plot          "
          " Save result in a file"
          " Recalculate          ","",&pscr,&k);
      {  double val=(*vararr)[maxsp].tmpvalue;                 
         doubleToStr(val,val*1.E-3,upstr);
      }
      trim(procname);
      sbld(upstr,"Process: %s; (Ecm = %sGeV)",procname,upstr);

      switch (k)
      {
         case 1:
            plot_2(FALSE,seq,upstr,"cos(f)", "Diff. cross section [pb]");
            break;

         case 2:
	    writetable1(&seq,upstr," cos(f)","Diff cross section [pb]");
            break;

         case 3:
            n = 50;
            do
            {  if (correctInt(10,24,"Enter number of points",&n,1))
               {
                  if (n < 3) messanykey(10,10,"Too few points$");
                  if (n > 201) messanykey(10,10,"Too many points$");
               }
               else goto men;
            }  while (n < 3 || n > 201 );

            disposerealseq(&seq);
            if( !fillseq(n,&seq)) 
            { messanykey(10,10,"Error in calculation$"); 
              return;
            }
      }  
   }  while (k != 0);
   disposerealseq(&seq);
}


static double  totcs(void)
{double  int_val = 0.0;

   calccoef();
   if (err_code == 0)
      integral(cos1,cos2,eps,cross_section,&int_val);
   return int_val;
}


static double  acs(void)
{double  int_val, int_val_a;

   calccoef();
   if (err_code == 0)
   {
      integral(0.0,cos2,eps,cross_section,&int_val);
      integral(cos1,0.0,eps,cross_section,&int_val_a);
      return (int_val - int_val_a) / (int_val + int_val_a);
   }
   else
      return 0.0;
}


static void  total_cs(realseq*  p)
{double  totcs;

   goto_xy(18,21); clr_eol();
   scrcolor(White,Black);
   print("?"); goto_xy(18,21);
   calccoef();
   if (err_code == 0)
   {
      integral_(cos1,cos2,eps,cross_section,&totcs,p); 
      if (!err_code ) print("%.5lf [pb]",totcs);
   }
   if (err_code) print("incorrect");
}


void  cs_numcalc(integer  n_sub)
{marktp   heapbg;
 int  k, l;
 void * pscr = NULL;
 void * pscr2= NULL;
 realseq  pans;

   mark_(&heapbg);

   findprocname(n_sub,procname,&err_code);
   if (err_code != 0) return;

   loadvars(n_sub);
   findprtcls(procname);
   compileprocess(n_sub,&prg);
   
   cos1 = -1.0;
   cos2 = 1.0;

   infotext();
   writeinformation();
   k = 1;
   l = 1;
   eps = 0.001;
   recalc = TRUE;
   pans = NULL;
   do
   {
      if (recalc)
      {
         total_cs(&pans);
         recalc = FALSE;
         disposerealseq(&pans);
        if (err_code) errormessage();                  
      }
      menu1(56,7,"","\026"
         " View/change data     "
         " Set angular range    "
         " Set precision        "
         " Angular dependence   "
         " Parameter dependence ","h_00651*",&pscr,&k);
      switch (k)
      {
         case 0:  disposerealseq(&pans);  break;
         case 1:  recalc = changedata();  break;
         case 2:  correctanglerange();    break;

         case 3: 
            do {   /* Precision */
               recalc = correctDouble(1,23," Enter precision : ",&eps,1);
               if (eps < 1.E-10 || eps > 0.0011)
                  messanykey(10,12,"Range check error$");
            }  while (!(eps >= 1.E-10 && eps <= 0.03)); 
         break;
         case 4:  if (err_code)  errormessage(); else drawgraph(); break;
         case 5:
            if(err_code)  errormessage(); else 
            do {
               l = 1;
               menu1(56,10,"","\026"
                  " Total Cross Section  "
                  " Asymmetry            ","",&pscr2,&l);
               if (l == 1)
		  paramdependence(totcs,procname,"Cross Section [pb]");
               if (l == 2)
                  paramdependence(acs,procname,"Assimetry");
            }  while (l != 0);
      }  /*  switch  */
      if (k > 0) writeinformation();
   }  while (k != 0);
   clrbox(1,16,70,24);
   release_(&heapbg);
}
