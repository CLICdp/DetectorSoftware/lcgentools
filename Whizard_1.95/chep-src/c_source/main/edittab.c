/****************************************************************

   edittab.c  is a module to view and edit models in CompHEP

Author:		A.Kryukov
E-mail:		kryukov@theory.npi.msu.su

Vertion:	3.22
Release:	Mar. 07, 1995

-----------------------------------------------------------------

Last revision:	Feb. 01, 1995	horizontal menu
                Mar. 07, 1995   cursor move right in zoom mode.

****************************************************************/

/****************************************************************
           Coordinates and etc.  
****************************************************************
               X0   Xt                   Xt
   +----------------------------------->-------------------+
   |           :    :                      Table           |
   |           :    :                                      |
   |           X1   X     X1        Xm     X2   x          |
   |     +------------------------------------->---+       |
   |     |     :    :     :         :      : Screen|       |
   |     |     :    :     :         :      :       |       |
 Y0|...Y1|.....+---------------------------+       |       |
   |     |     |    :     :         :  Win |       |       |
   |     |     |    :     :         :      |       |       |
   |   Y |.....|....CursorF         :      |       |       |
   |     |     |                    :      |       |       |
   |     |     |                    :      |       |       |
   |     |     |                    :      |       |       |
   |   Ym|.....|....................Mouse  |       |       |
   |     |     |                           |       |       |
   |   Y2|.....+---------------------------+       |       |
   |     |                                         |       |
   |     V                                         |       |
   |   y |                                         |       |
   |     +-----------------------------------------+       |
   |                                                       |
   V                                                       |
 Yt|                                                       |
   +-------------------------------------------------------+

****************************************************************/

#include "tptcmac.h"
#include "syst2.h"
#include "file_scr.h"
#include "crt.h"
#include "crt_util.h"

#include "help.h"
#include "edittab.h"




#define STRSIZE STRSIZ
#define	MPRESSED	1
#define	MRELEASED	2
#define EDITTAB_DEBUG   0

#define SE_HLP          3107
#define ZE_HLP          3108

#define STRSIZE STRSIZ
#define SEPR_CHAR ('|')
#define RESIZE_CHAR '<'
#define KBD_BUFF_SIZE 16
/*
#ifndef MAX 
#define MAX(a,b) (((a) > (b))? (a): (b))
#define MIN(a,b) (((a) < (b))? (a): (b))
#endif
*/
#define X2XT(i)  (c_tab.x0+(i)-x1-1)
#define XT2X(k)  ((k)-c_tab.x0+x1+1)
/*
#define DPRINT(n,d) goto_xy(n,1); print("[%d]",d); gotoXY();
#define SPRINT(n,s) goto_xy(n,1); print("[%s]",s); gotoXY();
*/
typedef struct table0
  {
    table *tab;
    linelist top,edit,del;            /* top line */
    int x,y,x1;              /* x,y - cursos position, x1 - end of field */
    int x0,y0;               /* first col. and line that displayed */
    char buf[STRSIZ];            /* buffer for edit of sring */
    int width;
  } table0;

static table0 c_tab;      /* current edited table */

/*************** Work with boxes ***************/

typedef struct box_struct {
  int x1,y1;              /* up-left conner */
  int x2,y2;              /* down-right conner */
  int fg,bg;              /* fore-, back- ground */
  void *buff;             /* buffer to save background */
} box_struct;

static box_struct e_box/* main window */
/*   , s_box,                 string window - simulation 
       z_box;                   zoom window 
*/
;
/*===============   DEFINED BUT NOT USED =======================
static box_struct *box_new(int fg,int bg){
  box_struct *new;
  new=malloc(sizeof(box_struct));
  if (!new) exit(99);
  new->x1=0;
  new->y1=0;
  new->x2=0;
  new->y2=0;
  new->fg=fg;
  new->bg=bg;
  new->buff=0;
  return new;
}

static int box_delete(box_struct * box) {
  return 0;
}

static int box_place(box_struct * box,int x1,int y1,int x2,int y2) {
  box->x1=x1;
  box->y1=y1;
  box->x2=x2;
  box->y2=y2;
  get_text(x1,y1,x2,y2,box->buff);
  return 0;
}

static int box_open(box_struct * box) {
  scrcolor(box->fg,box->bg);
  chepbox(box->x1,box->y1,box->x2,box->y2);
  return 0;
}

static int box_close(box_struct * box,int fg,int bg) {
  scrcolor(fg,bg);
  put_text(box->buff);
  return 0;
}

========= END OF "NOT USED"  */


/****************** static vars. *******************/

static int x1=1;          /* up-left conner of table window */
static int y1=1;
static int x2=80;         /* down-right conner of table window */
static int y2=24;
static int maxX2=80;
static int maxY2=24;
static int x2t=0;         /* width of table (exclude space char.) */
static char buff[STRSIZ]=     /* string buffer for edit and etc. */
"                                                                                ";
static int cursosON = 0;      /* flag. 0 - cursor ON, 1 - cursor OFF */
static int kbd_buff[KBD_BUFF_SIZE]=
	 {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
static int kbd_p1=0;      /* out going position in kbd_buff */
static int kbd_p2=0;      /* in comming position of kbd_buff */
static int editmode=0;    /* See 'show' */

static void gotoXY(void) {
  int xx=MAX(c_tab.x,x1+1);
  xx=MIN(xx,x2);
  goto_xy(xx,c_tab.y);
}

#if 0
static char *ErrMsg ="         Not implemented ...         ";

void showErrMsg(char * msg) {
  pointer save;
  int x=where_x(),
      y=where_y();
  get_text(1,10,maxX2,12,&save);
  chepbox(1,10,maxX2,12);
  goto_xy(2,11);
  print("%s",msg);
  inkey();
  goto_xy(x,y);
  put_text(&save);
}

static int nextSepr(int x) {
  int i;
  for (i = x; c_tab.tab->format[X2XT(i)]
      && c_tab.tab->format[X2XT(i)]!=SEPR_CHAR; i++)
  {};
  return i;
} /* nextSepr */
#endif /* 0 */

/***********************************************************************/
/*************** Cursor ON/OFF *********************** Jul. 05, 1996 ***/
/***********************************************************************/

static void curon(void){}

static void curoff(void){}

static void drawCur(char c,int x,int y){
/* curon */
      scrcolor(Blue,Yellow); 
      goto_xy(x,y);
      print("%c",c);
      scrcolor(White,Blue);
      goto_xy(x,y);
/**/
}

static void clearCur(char c,int x,int y){
/* curon */
      scrcolor(Black,White);
      goto_xy(x,y);
      print("%c",c);
/*      scrcolor(Black,White); */
      goto_xy(x,y);
/**/
}

/***********************************************************************/
/*************** Horizontal button bar *************** Jan. 31, 1995 ***/
/***********************************************************************/

#define HBB_MAX_NLEN 8

typedef struct hbb_item {
   char    name[HBB_MAX_NLEN];    /* name of item */
   int     rc;         /* return code */
   int     status;     /* 0 - disable, 1 - enable. */
} hbb_item;

typedef struct hbb_struct {
   hbb_item   *hbbar;  /* pointer to array of hbbar_item */
   int         nb;     /* number of buttons */
   char        bg[8];  /* background string */ 
   int         x,y;    /* position */
   int         status; /* 0 - disable, 1 - enable. */
   int         onscreen;/*0 - offscreen, 1 - onscreen */
   int         fgc,bgc;/* foreground and background colours */
   int         fgc_off,bgc_off;/* fg and bg colours for off screen */
} hbb_struct;

static int hbb_new(hbb_struct * hbb,hbb_item * hbbar,int nb,char bg){
  int i,j,len=strlen(hbbar[0].name);
  for (j=0;j<nb;j++)
    for (i=0;i<len;i++) 
      if (hbbar[j].name[i]=='-') hbbar[j].name[i]=bg;
  hbb->hbbar=hbbar;
  hbb->nb=nb;
  for (i=0;i<len;i++) hbb->bg[i]=bg;
  hbb->bg[len]=0;
  hbb->x=1;    /* default value, see alsow hbb_place */
  hbb->y=1;
  hbb->status=1;
  hbb->onscreen=0;
  hbb->fgc=White;
  hbb->bgc=Black;
  hbb->fgc_off=Black;
  hbb->bgc_off=White;
  return 0;
}

static int hbb_place(hbb_struct * hbb,int  x,int  y) {
  hbb->x=x;
  hbb->y=y;
#if EDITTAB_DEBUG
  printf("hbb_place: hbb->x=%d, x=%d\n",hbb->x,y);
#endif
  return 0;

}

static int hbb_open(hbb_struct * hbb) {
  int x0=where_x(),
      y0=where_y();
  int i;
  if (hbb->status == 0 || hbb->onscreen) return 0;
  goto_xy(hbb->x,hbb->y);
  scrcolor(hbb->fgc,hbb->bgc);
  for (i=0;i<hbb->nb;i++) {
    print("%s",hbb->hbbar[i].name);
  };
  scrcolor(hbb->fgc_off,hbb->bgc_off);
  goto_xy(x0,y0);
  hbb->onscreen=1;
  return 0;
}

static int hbb_close(hbb_struct * hbb) {
  int x0=where_x(),
      y0=where_y();
  int i;
  if (hbb->status == 0 || !hbb->onscreen) return 0;
  goto_xy(hbb->x,hbb->y);
  scrcolor(hbb->fgc_off,hbb->bgc_off);
  for (i=0;i<hbb->nb;i++) {
    print("%s",hbb->bg);
  };
  goto_xy(x0,y0);
  hbb->onscreen=0;
  return 0;
}

static int hbb_select(hbb_struct * hbb) {
  int x,rc=0;
  if (!hbb->status || !hbb->onscreen) return 0;
  if (mouse_info.row != hbb->y) return 0;  /* out of bar */
  x=(mouse_info.col-hbb->x)/strlen(hbb->hbbar[0].name);
  if (x>=0 && x<hbb->nb && hbb->hbbar[x].status) rc=hbb->hbbar[x].rc;
  return rc;
}

static void hbb_colors(hbb_struct * hbb,int fgc,int bgc,int fgc_off,int bgc_off){
  hbb->fgc=fgc;
  hbb->bgc=bgc;
  hbb->fgc_off=fgc_off;
  hbb->bgc_off=bgc_off;
}

/***********************************************************************/
/********* List of horizontal button bars *********** Feb. 04, 1995 ****/
/***********************************************************************/

typedef struct hbl_struct {
   struct hbl_struct   *next;
   hbb_struct *hbb;
} hbl_struct;

static hbl_struct *hbl_add(hbl_struct * hbl,hbb_struct * hbb) {
  hbl_struct *new;
  new=(hbl_struct *)malloc(sizeof(hbl_struct));
  if (!new) exit(99);
  new->hbb=hbb;
  new->next=hbl;
  return new;
}

static int hbl_delete(hbl_struct * hbl) {
  hbl_struct *w;
  for(;hbl;) {w=hbl; hbl=hbl->next; free(w);};  
  return 0;
}

static int hbl_select(hbl_struct * hbl) {
  hbl_struct *w;
  int rc=0;
  for (w=hbl;w && !rc;w=w->next) rc=hbb_select(w->hbb);
#if EDITTAB_DEBUG
  printf("hbl_select: rc=%d\n",rc);
#endif
  return rc;
}

static int hbl_open(hbl_struct * hbl) {
  hbl_struct *w;
  for (w=hbl;w;w=w->next) hbb_open(w->hbb);
  return 0;
}

static int hbl_close(hbl_struct * hbl) {
  hbl_struct *w;
  for (w=hbl;w;w=w->next) hbb_close(w->hbb);
  return 0;
}

static void hbl_colors(hbl_struct * hbl,int fgc,int bgc,int fgc_off,int bgc_off){
  hbl_struct *w;
  for (w=hbl;w;w=w->next){
    hbb_colors(w->hbb,fgc,bgc,fgc_off,bgc_off);
  };
}

/*********************** statics *********************/

static hbb_item up_items[5]={
   {"-*-",KB_ESC,1},
   {"-pu",KB_PAGEU,1},
   {"-pd",KB_PAGED,1},
   {"-Tp",KB_HOME,1},
   {"-Bt",KB_END,1}
};

static hbb_item down_items[5]={
   {"-F1",KB_F1,1},
   {"-<-",KB_LEFT,1},
   {"->-",KB_RIGHT,1},
   {"-up",KB_UP,1},
   {"-dn",KB_DOWN,1}
};

static hbb_item down2_items[7]={
   {"-In",KB_IC,1},
   {"-De",KB_DC,1},
   {"-Yk",'y',1},
   {"-Mg",'m',1},
   {"-Sz",'s',1},
   {"-Pr",'p',1},
   {"-Zo",'z',1}
};

static hbb_item down2s_items[1]={
   {"-Zo",'z',1}
};

static hbb_struct up_bar,down_bar,down2_bar/* ,down2s_bar */;
static hbl_struct *hbl;

/****************************************************************/
/************* Mouse functions *************** Feb. 04, 1995 ****/
/****************************************************************/

static int m_inbox(int x1,int y1,int x2,int y2) {
  return (x1<mouse_info.col && mouse_info.col<x2 
          && y1<mouse_info.row && mouse_info.row<y2);
}

/**************** Stack command **************/

static void clear_key(void) {
  kbd_p1=kbd_p2;
}

static void push_key(int kk){
  kbd_buff[kbd_p2]=kk;
  kbd_p2++;
  if (kbd_p2==KBD_BUFF_SIZE) kbd_p2=0;
  if (kbd_p2 == kbd_p1) exit(99);
}

static int pop_key(void) {
  int kk;
  if (kbd_p1!=kbd_p2) {
    kk=kbd_buff[kbd_p1];
    kbd_p1++;
    if (kbd_p1==KBD_BUFF_SIZE) kbd_p1=0;
#if EDITTAB_DEBUG
    printf("kbd_buff: p1=%d, kk=%d, %d\n"
          ,kbd_p1,kk
          ,kbd_buff[kbd_p1]);   /* debug */
#endif
  } else {
    kk=inkey();
    kbd_buff[kbd_p1]=kk;
    kbd_p1++;
    kbd_p2++;
    if (kbd_p2==KBD_BUFF_SIZE) kbd_p2=0;
    if (kbd_p1==KBD_BUFF_SIZE) kbd_p1=0;
    switch (kk) {
      case KB_UP:
      case KB_DOWN:
        push_key(1);
        break;
      case KB_LEFT:
        if (editmode) push_key(1);
        else push_key(2);
        break;
      case KB_RIGHT:
        if (editmode) push_key(1);
        else push_key(c_tab.x1-c_tab.x+3);
        break;
    }; /* switch kk */
  }
  return kk;
}

/*********************************************************************/
/***************** Draw table ********************** Feb. 04, 1995 ***/
/*********************************************************************/

static void redrawTable(table * tab) {
   int i;
   linelist lastline=c_tab.top;
   char *s;
   scrcolor(Black,White);
   goto_xy(x1+1,y1+1);
   print("%s",copy(tab->format,c_tab.x0,c_tab.width));
   s = &(buff[where_x()+maxX2-x2]);
   print(s);
/*   print(&(buff[where_x()+maxX2-x2]));*/

   for (i =y1+2; i <y2; i++) {
     goto_xy(x1+1,i);
     if ( lastline !=NULL ) {
       print("%s",copy(lastline->line,c_tab.x0,c_tab.width));
       lastline=lastline->next;
     }
     s = &(buff[where_x()+maxX2-x2]);
     print(s);
/*   print(&(buff[where_x()+maxX2-x2]));*/
   }
   gotoXY();
   cursosON = 0;
} /* redrawTable */

static void cursosOff(void) {
  int i,xx,xz;
  if (!cursosON) return;
  cursosON = 0;
  gotoXY();
  scrcolor(Black,White);
  xz = MAX(x1+1,c_tab.x);
  xx = MIN(c_tab.x1+1,x2);
  for(i=xz;
      i < xx && c_tab.edit->line[c_tab.x0+i-x1-2];
      i++)
    print("%c",c_tab.edit->line[c_tab.x0+i-x1-2]);
  gotoXY();
}

static void cursosOn(void) {
  int i,xx,xz;
  if (cursosON || !c_tab.top) return;
  cursosON = 1;
  gotoXY();
  scrcolor(White,Blue);
  xz = MAX(x1+1,c_tab.x);
  xx = MIN(c_tab.x1+1,x2);
  for(i=xz;
      i < xx && c_tab.edit->line[c_tab.x0+i-x1-2];
      i++)
    print("%c",c_tab.edit->line[c_tab.x0+i-x1-2]);
  scrcolor(Black,White);
  gotoXY();
}

/*************** Shift cursor ***********************/

static int shiftX1(int x) {
  int i,k,res=0;
  table *tab = c_tab.tab;
#if EDITTAB_DEBUG
  printf(" siftX1: x=%d, x2t=%d\n",x,x2t);
#endif
  if (x < 0) {                                   /* Shift left */
    for (i = c_tab.x - 2;
         (c_tab.x0+i-x1-2)>=0 && tab->format[c_tab.x0+i-x1-2] != SEPR_CHAR;
         i--)
      {};
    if (i > x1) c_tab.x = i + 1;
    else {
      res = 1;
      c_tab.x = x1 + 1;
      c_tab.x0 += (i-x1);
      if (c_tab.x0<1) c_tab.x0=1;
    }
/*    c_tab.x1 = nextSepr(c_tab.x) - 1;*/
  }
  else if (x > 0) {                    /* Shift right */
    if (c_tab.x1>x2) {int dif;         /* Field out of right bound.*/
                      dif=MIN(20,c_tab.x1-x2+1);
                      c_tab.x0 = c_tab.x0+ dif;
                      c_tab.x  = c_tab.x - dif;
                      /*c_tab.x1 = c_tab.x1-dif;*/
                      res=1;
    }
    else if (c_tab.x1+1 >= XT2X(x2t)) return 1;
    else {                             /* Go to the right field */
      for (i = c_tab.x;
        tab->format[c_tab.x0+i-x1-2]
	&& tab->format[c_tab.x0+i-x1-2]!=SEPR_CHAR;
        i++)
      {};
      /*DPRINT(1,XT2X(x2t)); DPRINT(10,i);*/
      if (i<XT2X(x2t)) {              /* Last right field */
        if (i>=x2) {                   /* Right field out of the box */
          for (k = x1+1;               /* Find right field from the left */
               tab->format[c_tab.x0+k-x1-2]
               && tab->format[c_tab.x0+k-x1-2]!=SEPR_CHAR;
               k++)
          {};
          if (tab->format[c_tab.x0+k-x1-2] && i<=XT2X(x2t)) {
            res = 1;
            c_tab.x0 += (k-x1);
            c_tab.x -= (k-x1);
            /*c_tab.x1 -= (k-x1);*/
          }
          /*else*/
	       /*c_tab.x -= (k-x1);*/

        }                              /* Right field into box */
        else if (tab->format[c_tab.x0+i-x1-2]) c_tab.x = i + 1;
      } /* if */
    }
    /*c_tab.x1 = nextSepr(c_tab.x) - 1;*/
  } /* if */

  for (i=c_tab.x;
       tab->format[c_tab.x0+i-x1-2]
       && tab->format[c_tab.x0+i-x1-2]!=SEPR_CHAR;
       i++)
    {};
  c_tab.x1 = i - 1;
  /*DPRINT(1,XT2X(x2t)); DPRINT(10,c_tab.x); DPRINT(20,c_tab.x1);*/

  return res;
} /* shiftX */

static int shiftX2(int dx) {
  int xm,res=0;
  xm=c_tab.x+dx;
#if EDITTAB_DEBUG
  printf(" shiftX2: dx=%d\n",dx);
#endif
  if (dx>0) {
    for (;c_tab.x1<xm && !res;) res=shiftX1(1);
  }
  else if (dx<0) {
    for (;c_tab.x>xm && !res;) res=shiftX1(-1);
  }
  else { res=shiftX1(0);};
  return res;
} /* shiftX2 */

/* =================== DEFINED BUT NOT USED =================

static int shiftX(int x) {
  int i,j=1,res=0;
  if (x==0) { res=shiftX1(0); return res;};
  if (x<0) { j=-1; x=-x;};
  for (i=0;i<x;i++) {
    res=shiftX1(j);
  };
  return res;
}
============= END OF "NOT USED" ======================= */

static int shiftY1(int y) {
  int res=0;
  switch (y) {
    case 1:
      if (c_tab.edit->next) {
        c_tab.edit = c_tab.edit->next;
        c_tab.y++;
      }
    break;
    case -1:
      if (c_tab.edit->pred) {
        c_tab.edit = c_tab.edit->pred;
        c_tab.y--;
      }
    break;
  }
  return res;
} /* shiftY */

static int shiftY(int y) {
  int i,j=1;
  if (y<0) { j=-1; y=-y; };
  for (i=0;i<y;i++) {
    shiftY1(j);  
  };
  return 0;
}

static void initEdit(table * tab,int ntop) {
  int i;
                                 /* Coord. of box on the screen */
  x1=1;
  y1=1;
  maxX2=maxCol();
  maxY2=maxRow();
  x2=maxX2;
  y2=maxY2;
                                 /* Initialization of static */
  c_tab.tab = tab;
/*  goto_xy(1,2); print("{%s}",tab->format); inkey();*/
  x2=strlen(tab->format)+1;
  if (x2>maxX2) x2=maxX2;
  c_tab.top = tab->strings;
  for (x2t=strlen(tab->format)-1; x2t && tab->format[x2t]!=SEPR_CHAR; x2t--)
    {};
  if (!c_tab.top) ntop--;
  for (i=1; (i < ntop) && (c_tab.top->next); i++)
    c_tab.top = c_tab.top->next;
  c_tab.edit = c_tab.top;
  c_tab.del = NULL;
  c_tab.del = (linelist) malloc(sizeof(linerec)+2);
  c_tab.del->pred = NULL;
  c_tab.del->next = NULL;
  strcpy(c_tab.del->line ,tab->format);
  c_tab.x0 = 1;
  c_tab.y0 = ntop;
  c_tab.width=x2-x1-1;
  c_tab.x  = x1+1;
  c_tab.y  = y1+2;
  c_tab.x1 = 0;
  shiftX1(0);
} /* initEdit */

static int s_edit(int x1,  int x2, int x0, int  y1, char * s   ) {
  /* x1,x2,y1 - coord of string window (begin,end,Y)
     x0 - the first char. from s which display */
  int i;
  int rc = 0, rc1 = 0;
  int key = 0;
  int x,len,z,slen0=strlen(s);
  int first = 1;         /* The first press of key */
  int dx,dy;

  len=x2 - x1+1;
  x = x1;

  if (x0>len) x1 += (len-x0);
  scrcolor(Yellow,Blue);
curon();
  clearTypeAhead();
  goto_xy(x1,y1);
  while (key != KB_ESC && key != KB_ENTER) {

    switch (key) {

      case KB_BACKSP:
        if (x>x1) { 
          x--;
	  for(i=x0+x-x1; s[i]; i++) s[i] = s[i+1];
          s[i-1]=' ';
          rc1=1;
	};
        key = 0;
	first = 0;
      break;

      case KB_DC:
        for(i=x0+x-x1; s[i]; i++) s[i] = s[i+1];
	s[i-1]=' ';
        rc1=1;
        key = 0;
	first = 0;
      break;

      case KB_LEFT:
        dx=pop_key();
        for (;dx>0;dx--) {
          if (x == x1) {if (x0) x0--;}
	  else x--;
        };
        key = 0;
	first = 0;
      break;

      case KB_RIGHT:
        dx=pop_key();
        for (;dx>0;dx--) {
	  if (x == x2) {if (s[x0+x-x1+1]) x0++;}
	  else x++;
        };
	key = 0;
	first = 0;
      break;

      case KB_HOME:
        x = x1;
        x0 = 0;
        key = 0;
	first = 0;
      break;

      case KB_END:
        z = strlen(s)-x0+x1-x2-1;
	if (z>0) {
          x = x2;
          x0 += z;
        }
	else x = z + x2;
        key = 0;
	first = 0;
      break;

      case KB_UP:
        dy=pop_key();
	push_key(KB_ENTER);
	push_key(KB_UP);
        push_key(dy);
	push_key(KB_ENTER);
        key = 0;
	first = 0;
      break;

      case KB_DOWN:
        dy=pop_key();
	push_key(KB_ENTER);
	push_key(KB_DOWN);
        push_key(dy);
	push_key(KB_ENTER);
        key = 0;
	first = 0;
      break;

      case KB_PAGEU:
	push_key(KB_ENTER);
	push_key(KB_LEFT);
        push_key(2);
	push_key(KB_ENTER);
        key = 0;
	first = 0;
      break;

      case KB_PAGED:
	push_key(KB_ENTER);
	push_key(KB_RIGHT);
        push_key(c_tab.x1-c_tab.x+3);
	push_key(KB_ENTER);
        key = 0;
	first = 0;
      break;
      case KB_MOUSE:
          if (mouse_info.but1 == MRELEASED ) { 
#if EDITTAB_DEBUG
            printf(" mouse: but1=%d, x=%d, y=%d\n",
                 mouse_info.but1, mouse_info.row,
                 mouse_info.col );
            printf(" s-cursor: x=%d\n",x);
#endif
            if (rc=hbl_select(hbl->next)) {
               push_key(rc);
               if (rc==KB_UP) push_key(1);
               else if (rc==KB_DOWN) push_key(1);
               else if (rc==KB_LEFT) push_key(1);
               else if (rc==KB_RIGHT) push_key(1);
            }
            else if (m_inbox(x1-1,y1-1,x2+1,y1+1)){
              dx=mouse_info.col-x;
              if (dx>0) { 
                push_key(KB_RIGHT); 
                push_key(dx);
              }
              else if (dx<0) { 
                push_key(KB_LEFT); 
                push_key(-dx);
              };
            }
            else if (m_inbox(e_box.x1,e_box.y1,e_box.x2,e_box.y2)) {
            /* else { */
              dy=mouse_info.row-c_tab.y;
              if (dy>0) { 
                push_key(KB_ENTER);
	        push_key(KB_DOWN);
                push_key(dy);
	        push_key(KB_ENTER);
              }
              else if (dy<0) { 
	        push_key(KB_ENTER);
	        push_key(KB_UP);
                push_key(-dy);
	        push_key(KB_ENTER);
              };
              dx=mouse_info.col-c_tab.x;
              if (mouse_info.col>c_tab.x1+1) { 
	        push_key(KB_ENTER);
	        push_key(KB_RIGHT);
                push_key(dx);
	        push_key(KB_ENTER);
              }
              else if (mouse_info.col<c_tab.x-1) { 
	        push_key(KB_ENTER);
	        push_key(KB_LEFT);
                push_key(-dx);
	        push_key(KB_ENTER);
              };
            };
          };
          key=0;
          first = 0;
        break;

	 case KB_F1:
	     showhelp(SE_HLP);
             scrcolor(Yellow,Blue);
	     key = 0;
             first = 0;
	 break;

      default:
	if ((key>=32)&&(key<=127)) {
	  if (first) {        /* Clear curent field?  */
	    for(i=0;i<strlen(s);i++) s[i]=' ';
	    first=0;
	  };
	  if (s[strlen(s)-1] == ' ' || strlen(s) < slen0) {
	    for(i=strlen(s)-2; i >= x0+x-x1; i--) s[i+1] = s[i];
	    s[x0+x-x1] = (char) key;
	    if (x<x2) x++;
	    else {if (s[x0+x-x1+1]) x0++;}
	  } else be_be();
          rc1=1;
	}
        else be_be();
        key = 0;
      break;

    } /* switch */

    goto_xy( x1,y1);
    { int charM;
      charM=s[x0+x2-x1+1];
      s[x0+x2-x1+1]=0;
      print(&s[x0]);
/* curon */
      drawCur(s[x0+x-x1],x,y1);
/**/
      s[x0+x2-x1+1]=charM;
    }
    goto_xy(x,y1);
    key = pop_key();
  } /* while */

/*  str_redact0(s,1,strlen(s)); */
  scrcolor(White,Blue);
/*  if (key == KB_ENTER) rc1 = 1; */
curoff();
  return rc1;
} /* s_edit */

/****************** Zoom *******************/

static int wZoom;/* Width of zoom window without border */
static int hZoom;/* High of zoom window without border */
static int sLen; /* The length of the zoomed string */
static int sEnd; /* The real end of the zoomed string */

static int xy2z(int x,int y,int * z){
/*                                        May 12, 1994
   xy2z transorm 2-dim coordinate to linear coordinate along
	the working string.

   x,y - 2-dim. coordinate. Start point is (0,0). Last on is 
	 (hZoom-1,wZoom-1).
   *z   - linear coordinate. Start point is 0. Last one is sLen-1.

   return - 0 if ok and 1 if tranformation impossible.
*/
   int z1;        /* Work vars. */

   z1 = wZoom*y+x;
   if (z1<0) return -1;
   else if (z1>=sLen) return 1;
   else {
     *z = z1;
     return 0;
   };
}

static int z2xy(int z,int * x,int * y){
/*                                        May 12, 1994
   z2xy transorm linear coordinate along the working string to 
	2-dim. coordinate.

   *x,*y - 2-dim. coordinate. Start point is (0,0). Last on is 
	 (hZoom-1,wZoom-1).
   z   - linear coordinate. Start point is 0. Last one is sLen-1.

   return - 0 if ok and 1 if tranformation impossible.
*/
   int x1,y1;         /* Work vars. */

   y1 = z/wZoom;
   x1 = z - (wZoom*y1);
   /*printf("*** z->(x,y)=%d->(%d,%d)",z,*x,*y); inkey();*/
   if ((x1<0) || (x1>=wZoom) || (y1<0) || (y1>=hZoom)) return 1;
   else {
     *x=x1;
     *y=y1;
     return 0;
   };
}

static char *stripStr(char * s){
/*
  stripStr remove all spaces on the end of string.
  s - stripped string.
  return stripped string.
*/
  int l;      /* length of string */
  int i;      /* cycle counter */
  l=strlen(s);
  for(i=l-1;i && s[i]==' ';i--) s[i]=0;
  return s;
}

static void drawLStr(int x,int y,char * s,int z){
/*
   drawLStr draw logn string in the window prepare before.
   x,y - the start point on the screen.
   s   - drawable string.
   z   - number of intial char. from s.
*/
  int i;        /* cycle counter */
  int x1=0,y1=0;/* local coordinate */
  char ws[STRSIZ];  /* work string */

  if (z>=sEnd) sEnd=z+1;
  if (z2xy(z,&x1,&y1)) exit(99);
  z=y1*wZoom;
  for(;y1<hZoom && z<sEnd+1;y1++){
    for(i=x1;(i<wZoom)&&s[z+i]&&z+i<sEnd+1;i++) 
      ws[i]=s[y1*wZoom+i];
    ws[i]=0;
    goto_xy(x+x1,y+y1);
    print(&(ws[x1]));
    x1=0;
    z+=wZoom;
  };
}

static void printLStr(FILE * fiout,char * s){
/*
   printLStr print logn string in the file.
   fiout - file to output string.
   s   - printing string.
*/
  int i;        /* cycle counter */
  int y1=0;     /* local coordinate */
  char ws[STRSIZ];  /* work string */
  int sE;       /* length stripted string */
  int z=0;      /* position in printed string */

  stripStr(s);
  sE=strlen(s);
  if (s[sE-1]==SEPR_CHAR) {
    s[sE-1]=0;
    stripStr(s);
    sE=strlen(s);
  };
  fprintf(fiout,"\n");
  for(;z<sE;y1++){
    for(i=0;(i<maxX2)&&s[z+i];i++) 
      ws[i]=s[y1*maxX2+i];
    ws[i]=0;
    fprintf(fiout,"%s\n",ws);
    z+=maxX2;
  };
}

static void clZWin(int x,int y){
/*
   clZein clear window for zoom.
   x,y - the start point on the screen.
   s   - drawable string.
   z   - number of intial char. from s.
*/
  int i;        /* cycle counter */
  int y1=0;     /* local coordinate */
  char ws[STRSIZ];  /* work string */

  for(;y1<hZoom;y1++){
    for(i=0;(i<wZoom)&&(y1*wZoom+i)<sLen;i++) ws[i]=' ';
    if (y1==(hZoom-1)) for(;i<wZoom;i++) ws[i]='<';
    ws[i]=0;
    goto_xy(x,y+y1);
    print(ws);
  };
}

static int insChar(char c,char * s,int z){
  int i;           /* Cycle var. */
  int l;           /* length of str. */

  l = strlen(s);
  if (s[l-1]!=' ') return 1;
  for (i=l-2;i>=z;i--) s[i+1]=s[i];
  s[z]=c;
  sEnd++;
  return 0;
}

static int delChar(char * s,int z){
  int i;           /* Cycle var. */
  int l;           /* length of str. */

  l = strlen(s);
  for (i=z+1;i<l;i++) s[i-1]=s[i];
  s[l-1]=' ';
  sEnd--;
  return 0;
}

static int zoomStr(char * ws,int x,int y,int b,int show){
/*                                      May 12, 1994 
   zoom shows long fields in separate window.

   ws - zoomed string
   x,y - the coordinates of left-top angle
   b - the width of border;
   show - showed flag. Not implemented.
   return - 0
*/
pointer pbuff;      /* pointer to saving buffer */
int key;            /* code of pressed key */
int x1,y1;          /* shifted screen coordinate. the first position 
		       of the text */
int edit=0;         /* Is it edited? Not implemented */
int zc,zc1;         /* Linear curses coordinate. zc1-previous pos.*/
int xc,yc,xc1,yc1;  /* Local curses coordinate. xc1,yc1 - previous pos. */
int dx,dy;
int rc;

wZoom = 75;
if (strlen(ws)>=STRSIZE) exit(99);
if (show) stripStr(ws);
sLen  = strlen(ws);
for (sEnd=sLen-1;sEnd && ws[sEnd]==' ';sEnd--){};
sEnd++;
hZoom = STRSIZE/wZoom+1;
z2xy(sLen-1,&xc,&yc);
hZoom = yc;
if (!hZoom) wZoom=xc+1; 
   /*printf("*** (x,y)=(%d,%d)",x,y); inkey();*/
hZoom++;
x1=x+b;
y1=y+b;
zc=0;
z2xy(zc,&xc,&yc);

/*get_text(x,y,x+wZoom-1+2*b,y+hZoom-1+2*b,&pbuff);     ????*/
get_text(x,y,x+wZoom-1+2*b,y+hZoom+2*b,&pbuff);
/*printf(" x=%d,y=%d,wZoom=%d,hZoom=%d,b=%d\n",x,y,wZoom,hZoom,b);*/
scrcolor(Black,White);
if (b>0) chepbox(x,y,x+wZoom-1+2*b,y+hZoom-1+2*b);

if (!show) {
  curon();
};
key = 0;
clZWin(x1,y1);

for(;key!=KB_ESC && key!=KB_ENTER;){
  zc1=zc;
  switch (key){
  case 0:
    drawLStr(x1,y1,ws,0);
    drawCur(ws[0],x1+xc,y1+yc);
    goto_xy(x1+xc,y1+yc);
    break;

  case KB_LEFT:
    dx=pop_key();
    for(;dx>0 && zc>0;dx--) {
      zc--;
      z2xy(zc,&xc,&yc);
      goto_xy(x1+xc,y1+yc);
    };
    break;

  case KB_RIGHT:
    dx=pop_key();
    for(;dx>0 && zc<sLen-1;dx--) {
      zc++;
      z2xy(zc,&xc,&yc);
      goto_xy(x1+xc,y1+yc);
    };
    break;

  case KB_UP:
    dy=pop_key();
    for(;dy>0 && yc>0;dy--) {
      yc--;
      xy2z(xc,yc,&zc);
      goto_xy(x1+xc,y1+yc);
    };
    break;

  case KB_DOWN:
    dy=pop_key();
    for(;dy>0 && yc<hZoom-1;dy--) {
      yc++;
      if (xy2z(xc,yc,&zc)) yc--;
      goto_xy(x1+xc,y1+yc);
    };
    break;

  case KB_PAGEU:
  case KB_HOME:
    zc=0;
    z2xy(zc,&xc,&yc);
    goto_xy(x1+xc,y1+yc);
    break;

  case KB_PAGED:
  case KB_END:
    zc=sLen-1;
    z2xy(zc,&xc,&yc);
    goto_xy(x1+xc,y1+yc);
    break;

  case KB_DC:    /* Del. char. */
    if (show) break;
    if(!delChar(ws,zc)) edit=1;
    drawLStr(x1,y1,ws,zc);
/*    drawCur(ws[zc],x1+xc,y1+yc);*/
    goto_xy(x1+xc,y1+yc);
    key=0;
    break;

  case KB_BACKSP:    /* Del. char. before */
    if (show) break;
    if (zc<=0) break;
    zc--;
    z2xy(zc,&xc,&yc);
    goto_xy(x1+xc,y1+yc);
    if(!delChar(ws,zc)) edit=1;
    drawLStr(x1,y1,ws,zc);
/*    drawCur(ws[zc],x1+xc,y1+yc);*/
    goto_xy(x1+xc,y1+yc);
    key=0;
    break;

      case KB_MOUSE:
          if (mouse_info.but1 == MRELEASED ) { 
#if EDITTAB_DEBUG
            printf(" mouse: but1=%d, x=%d, y=%d\n",
                 mouse_info.but1, mouse_info.row,
                 mouse_info.col );
            printf(" Z-cursor: xc=%d, yc=%d, zc=%d\n",xc,yc,zc);
#endif
            if (rc=hbl_select(hbl->next)) {
               push_key(rc);
               if (rc==KB_UP) push_key(1);
               else if (rc==KB_DOWN) push_key(1);
               else if (rc==KB_LEFT) push_key(1);
               else if (rc==KB_RIGHT) push_key(1);
            }
            else if (m_inbox(x,y,x+wZoom-1+2*b,y+hZoom-1+2*b)) {
              dx=mouse_info.col-x1-xc;
              if (dx>0) { 
                push_key(KB_RIGHT); 
                push_key(dx);
              }
              else if (dx<0) { 
                push_key(KB_LEFT); 
                push_key(-dx);
              };
              dy=mouse_info.row-y1-yc;
              if (dy>0) { 
                push_key(KB_DOWN); 
                push_key(dy);
              }
              else if (dy<0) { 
                push_key(KB_UP); 
                push_key(-dy);
              };
            };
          };
          key=0;
        break;

	 case KB_F1:
	     showhelp(ZE_HLP);
	     scrcolor(Black,White);
	     key = 0;
	 break;

  default:
    if (show) break;
    if ((key>=32)&&(key<=127)) {
      if (insChar((char)key,ws,zc)) break;
      edit=1;
      drawLStr(x1,y1,ws,zc);
/*      drawCur(ws[zc],x1+xc,y1+yc);*/
      if(zc<sLen-1)zc++;
      z2xy(zc,&xc,&yc);
      goto_xy(x1+xc,y1+yc);
    };
    key = 0;
    break;
  }; /* switch (key) */
  
  if (!show) {
    goto_xy(x+1,y);
    scrcolor(White,Black);
    /*print("%d",zc+1);*/
    print("%d%c%c%c",zc+1,boxFrame[5],boxFrame[5],boxFrame[5]);
/*    scrcolor(Black,White);*/
    goto_xy(x1+xc,y1+yc);
    drawCur(ws[zc],x1+xc,y1+yc);
    if(zc!=zc1){
      z2xy(zc1,&xc1,&yc1);
      clearCur(ws[zc1],x1+xc1,y1+yc1);
    };
    scrcolor(Black,White);
  }
  else {
  };
  key = pop_key();
}; /* while */

if (!show) curoff();
/* if (key==KB_ESC) edit=0; */

scrcolor(White,Black);
/*put_text(x,y,x+wZoom-1+2*b,y+hZoom-1+2*b,&pbuff);    ???*/
put_text(&pbuff);
/*printf(" x=%d,y=%d,wZoom=%d,hZoom=%d,b=%d\n",x,y,wZoom,hZoom,b);*/
return edit;
}
/*************** Resize Table *************/

static void squeezeField(int k) {
  /* Squeeze the currebt field in table */
  /* k - how much */
  linelist llist;
  int i;
  char *s;
  s = c_tab.tab->format;
  for (i = c_tab.x1+1;s[X2XT(i)-1];i++) s[X2XT(i-k)-1] = s[X2XT(i)-1];
  s[X2XT(c_tab.x1-k)-1] = RESIZE_CHAR;
  s[X2XT(i-k)] = 0;
  llist = c_tab.tab->strings;
  while (llist) {
    for(i=c_tab.x1;llist->line[X2XT(i)];i++)
      llist->line[X2XT(i-k)] = llist->line[X2XT(i)];
    llist->line[X2XT(i-k)] = 0;
    llist = llist->next;
  } /* while (llist) */
  if (c_tab.del) {
    llist = c_tab.del;
    for(i=c_tab.x1;llist->line[X2XT(i)];i++)
      llist->line[X2XT(i-k)] = llist->line[X2XT(i)];
    llist->line[X2XT(i-k)] = 0;
  } /* if (c_tab.del) */
  c_tab.x1 -= k;
  x2t -= k;
} /* squeezeField */

static void spreadLine(char * s,int n,int k) {
  /* return the s where after s[n] insert k spaces */
  int i,len;
  len = strlen(s);
  s[len+k+1] = 0;
  for (i=len;i>n;i--) s[i+k] = s[i];
  for (i=k;i;i--) s[n+i] = ' ';
} /* spreadLine */

static void spreadField(int k) {
  /* sread the currebt field of table */
  /* k - how much */
  linelist llist,w,w0,w1;
  int len,i;
  char *s;
  s = c_tab.tab->format;
  len = strlen(s);
  for (i=len;s[i]!=SEPR_CHAR; i--) {};
  len = i;
  s[len+1] = 0;
  s[X2XT(c_tab.x1)-1] = ' ';
  spreadLine(s,X2XT(c_tab.x1)-1,k);
  s[X2XT(c_tab.x1+k)-1] = RESIZE_CHAR;

  w0 = 0;
  llist = c_tab.tab->strings;
  while (llist) {
    len = strlen(llist->line);
    w1 = (linelist) malloc(len+k+1+2*sizeof(linelist));
    w1->pred = 0;
    w1->next = 0;
    strcpy(w1->line,llist->line);
    spreadLine(w1->line,X2XT(c_tab.x1)-1,k);
    if (!w0) w0 = w1;
    else {
      w1->pred = w;
      w->next = w1;
    }
    w = w1;
    if (c_tab.top == llist) c_tab.top = w;
    if (c_tab.edit == llist) c_tab.edit = w;
    w1 = llist;
    llist = llist->next;
    free(w1);
  } /* while (llist) */
  c_tab.tab->strings = w0;

  if (c_tab.del) {
    len = strlen(c_tab.del->line);
    w1 = (linelist) malloc(len+k+1+2*sizeof(linelist));
    w1->pred = 0;
    w1->next = 0;
    strcpy(w1->line,c_tab.del->line);
    spreadLine(w1->line,X2XT(c_tab.x1)-1,k);
    free(c_tab.del);
    c_tab.del = w1;
  } /* if (c_tab.del) */
  c_tab.x1 += k;
  x2t += k;
} /* spreadField */

static void minmaxFSize(int * min,int * max) {
  /* MIN,MAX - valid size of the current field */
  linelist llist = c_tab.tab->strings; 
  char *s;
  int ext = STRSIZE - (strlen(c_tab.tab->format)+1);
  int i;
  *min = 0;
  *max = c_tab.x1 - c_tab.x + ext;
  while (llist) {
    s = llist->line;
    for (i=c_tab.x1;i>=c_tab.x && s[c_tab.x0+i-x1-2]==' ';i--) {};
    *min = MAX(*min,i-c_tab.x+1);
    llist = llist->next;
  } /* while (llist) */
} /* minmaxFSize */

static int newFSize(int kk,int min,int max) {
  /* return how much to resize field */
  /* kk - current size */
  int key,ok,i;
  pointer box;
  char ss[10];
  ss[4]=0;
  sprintf(ss,"%d",kk);
  get_text(20,10,60,13,&box);
  scrcolor(White,Green);
  chepbox(20,10,60,13);
  goto_xy(21,12);
  print("                                       ");
  goto_xy(21,12);
  print(" Current size: %d, MIN=%d, MAX=%d",kk,min,max);
  goto_xy(21,11);
  print(" Input new size:                       ");
  key = kk;
  ok = 0;
  curon();
  while (!ok) { int len; 
    len=strlen(ss);
    for(i=len;i<4;i++)ss[i]=' ';
    ss[4]=0;
    if (!s_edit(41,44,0,11,ss)) break;
    ok = sscanf(ss,"%d",&key);
    if (ok && (key<min || key>max)) ok = 0;
  } /* while (ok) */
  clear_key();
  curoff();
  scrcolor(White,Black);
  put_text(&box);
  gotoXY();
  return key;
} /* newFSize */

static int resizeField(void) {
  int k,kk=c_tab.x1 - c_tab.x +1;
  int min,max,rc;
  minmaxFSize(&min,&max);
  k = newFSize(kk,min,max);
  rc = 1;
  if (k<kk) squeezeField(kk-k);
  else if (k>kk) spreadField(k-kk);
  else rc = 0;
  return rc;
} /* resizeField */

/******************* end resize ***********/

int  edittable(table * tab,int ntop,char * hlpf,int show) {
/*  int       cpos = 1;*/
  int   key;
  pointer   pscr;
  int   i,l;
  char      ws[STRSIZE];
  int   edit=FALSE;
  FILE *fiout;
  linelist pri;
  int dx,dy;   /*  Mouse */
  int rc;
   
  clearTypeAhead();
  initEdit(tab,ntop);
  e_box.x1=x1;
  e_box.y1=y1;
  e_box.x2=x2;
  e_box.y2=y2;
/*  goto_xy(1,1); print("box= {%d %d %d %d}",x1,y1,x2,y2); inkey(); */
  get_text(x1,y1,x2,y2,&pscr);

  scrcolor(Black,White);
  chepbox(x1,y1,x2,y2);
  l=strlen(tab->headln);
  if ( l < (x2-x1) ) {
    goto_xy(x1+(x2-x1-1-l)/2,y1);
    scrcolor(White,Black);
    print(tab->headln);
  }

  /* up_bottom bar */
  hbb_new(&up_bar,&up_items[0],5,boxFrame[1]);
  hbb_place(&up_bar,3,y1);

  /* down_bottom bar */
  hbb_new(&down_bar,&down_items[0],5,boxFrame[5]);
  hbb_place(&down_bar,3,y2);

  /* down2_bottom bar */
  if (show) {
    hbb_new(&down2_bar,&down2s_items[0],1,boxFrame[5]);
    hbb_place(&down2_bar,50,y2);
  }
  else {
    hbb_new(&down2_bar,&down2_items[0],7,boxFrame[5]);
    hbb_place(&down2_bar,20,y2);
  };
  
  /* init list of hbb's */
  hbl=hbl_add(0,&up_bar);
  hbl=hbl_add(hbl,&down_bar);
  hbl=hbl_add(hbl,&down2_bar);
  hbl_colors(hbl,Black,Green,Black,White);
  hbl_open(hbl);

  redrawTable(tab);
  cursosOn();
  /* getchar(); */  /* debug */
  key = 0;
  while ((key != KB_ESC) ) {
    if (c_tab.top != NULL || key==KB_IC || key=='i' || key=='I'
       || key == 'm' || key=='M' || key==KB_F1) {

      switch (key) {
	case KB_ENTER:
        case 'E':
	case 'e':
	  if (show) {key=0;break;}
	  scrcolor(Black,White   );
	  goto_xy(x2-5,y2);
	  print("EDIT");
          editmode=1;
	  strcpy( ws,&c_tab.edit->line[c_tab.x0+c_tab.x-x1-2]);
	  ws[c_tab.x1 - c_tab.x + 1] = 0;

          hbb_close(hbl->hbb);
	  if (s_edit(MAX(x1+1,c_tab.x),MIN(x2-1,c_tab.x1),MAX(0,x1-c_tab.x+1),
		c_tab.y,ws)) {
	    edit=TRUE;
	    for(i=0; ws[i]; i++)
	      c_tab.edit->line[c_tab.x0+c_tab.x +i-x1-2]=ws[i];
	  }
	  /* scrcolor(White,Black); */
          hbb_open(hbl->hbb);

	  scrcolor(Black,White);goto_xy(x2-5,y2);
	  { int kk; for(kk=0;kk<4;kk++) print("%c",boxFrame[5]);}
          editmode=0;
	  cursosON = 0;
	  cursosOn();
	  key = 0;
	  break;

        case KB_PAGEU:     /*  PgUp    */
          for (i=y1+2;i<y2;i++)
            if (c_tab.top->pred !=NULL) {
              c_tab.top=c_tab.top->pred;
	      c_tab.y0--;
              c_tab.edit = c_tab.edit->pred;
            }
	  redrawTable(tab);
	  cursosOn();
          key = 0;
        break;

        case KB_HOME:
        case 'T':     /*  Top    */
	case 't':
          for (i=y1+2;c_tab.top->pred!=NULL;i++){
              c_tab.top=c_tab.top->pred;
	      c_tab.y0--;
              c_tab.edit = c_tab.edit->pred;
            }
	  redrawTable(tab);
	  cursosOn();
          key = 0;
        break;

	case KB_PAGED:     /*  PgDn    */
	  for (i=y1+2;i<y2;i++)
	    if (c_tab.edit->next !=NULL) {
              c_tab.top=c_tab.top->next;
	      c_tab.y0++;
              c_tab.edit=c_tab.edit->next;
            }
          redrawTable(tab);
          cursosOn();
	  key = 0;
          break;

        case KB_END:
	case 'B':     /* Bottom    */
	case 'b':
	  for (i=y1+2;c_tab.edit->next!=NULL;i++){
              c_tab.top=c_tab.top->next;
	      c_tab.y0++;
              c_tab.edit=c_tab.edit->next;
            }
          redrawTable(tab);
          cursosOn();
	  key = 0;
          break;

	case KB_UP:
          dy=pop_key();
          if (c_tab.y > (y1+1+dy)) { cursosOff(); shiftY(-dy); cursosOn();}
	  else if (c_tab.top->pred !=NULL) {
            c_tab.top=c_tab.top->pred;
            c_tab.y0--;
            c_tab.edit = c_tab.edit->pred;
            redrawTable(tab);
            cursosOn();
	  }
          key = 0;
          break;

	case KB_DOWN:     /*  LineDn  */
          dy=pop_key();
          if (c_tab.y < (y2-dy)) { cursosOff(); shiftY(dy); cursosOn();}
          else if (c_tab.edit->next !=NULL) {
            c_tab.top=c_tab.top->next;
	    c_tab.y0++;
	    c_tab.edit=c_tab.edit->next;
	    redrawTable(tab);
            cursosOn();
	   }
          key = 0;
        break;

        case KB_RIGHT:
	  cursosOff();
          dx=pop_key();
          if (shiftX2(dx)) redrawTable(tab);
          cursosOn();
	  key = 0;
        break;

	case KB_LEFT:
	  cursosOff();
          dx=pop_key();
	  if (shiftX2(-dx)) redrawTable(tab);
	  cursosOn();
	  key = 0;
	break;

	case 'Y':
	case 'y':
	     if (show) {key=0;break;}
	     if (c_tab.del) free(c_tab.del);
	     c_tab.del = (linelist) malloc(sizeof(linerec)+2);
             c_tab.del->next = NULL;
	     c_tab.del->pred = NULL;
             strcpy(c_tab.del->line,c_tab.edit->line);
	     key = 0;
	break;

	case KB_DC:
	case 'D':
	case 'd':
	     if (show) {key=0;break;}
	     if (c_tab.del) free(c_tab.del);
	     c_tab.del = c_tab.edit;
	     if (c_tab.top == c_tab.edit) {
	       if (c_tab.top->next) {
		 c_tab.top = c_tab.top->next;
		 c_tab.top->pred = c_tab.del->pred;
		 if (c_tab.del->pred) 
		   c_tab.del->pred->next = c_tab.top;
                 else c_tab.tab->strings = c_tab.top;
	       }
	       else {
		 c_tab.top = c_tab.top->pred;
		 if (c_tab.top) {
		   c_tab.top->next = 0;
		   c_tab.y0--;
		 }
		 else {
		   c_tab.tab->strings = c_tab.top;
		   c_tab.y0 = 0;
		 }
	       }
	       c_tab.edit = c_tab.top;
	     }
	     else {
	       if (c_tab.edit->next) {
		 c_tab.edit = c_tab.edit->next;
		 c_tab.edit->pred = c_tab.del->pred;
		 c_tab.del->pred->next = c_tab.edit;
	       }
	       else {
		 c_tab.edit = c_tab.edit->pred;
		 c_tab.edit->next = 0;
		 c_tab.y--;
	       }
	     }
	     c_tab.del->next = NULL;
	     c_tab.del->pred = NULL;
	     redrawTable(tab);
	     cursosOn();
	     key = 0;
	     edit=TRUE;
	break;

	case KB_IC:
	case 'I':
	case 'i':
	     if (show) {key=0;break;}
	     if (!c_tab.del) { key = 0; break;}
	     if (!c_tab.edit) {
	       c_tab.top = c_tab.del;
	       c_tab.edit = c_tab.del;
	       c_tab.tab->strings = c_tab.top;
	       c_tab.y0 = 1;
	     }
	     else {
	       c_tab.del->next = c_tab.edit;
	       c_tab.del->pred = c_tab.edit->pred;
	       c_tab.edit->pred = c_tab.del;
               if (c_tab.del->pred) {
		 c_tab.del->pred->next = c_tab.del;
		 if (c_tab.top==c_tab.edit) c_tab.top = c_tab.del;
               }
	       else {
		 c_tab.tab->strings = c_tab.del;
                 c_tab.top = c_tab.del;
               }
               c_tab.edit = c_tab.del;
	     } /* if */
	     c_tab.del = (linelist) malloc(sizeof(linerec)+2);

             c_tab.del->next = NULL;
	     c_tab.del->pred = NULL;
             strcpy(c_tab.del->line,c_tab.edit->line);

	     redrawTable(tab);
	     cursosOn();
	     key = 0;
	     edit=TRUE;
	  break;

	 case '1':
	 case KB_F1:
	     show_help(hlpf);
	     scrcolor(Black,White);
	     key = 0;
	 break;

	case 'm':
	case 'M':
	  if ( errorText[0] != 0 )
	  messanykey(10,10,errorText); else be_be();
	  key=0;
	  break;

	case 's':
	case 'S':
          if (show) { key=0; break;};
	  if (c_tab.tab->format[X2XT(c_tab.x1)-1] == RESIZE_CHAR) {
	    if (resizeField()) {
	      edit = TRUE;
	      redrawTable(tab);
	      cursosOn();
	    }
	  }
	  key=0;
	  break;

	case 'p':
	case 'P':
	  fiout=fopen("model.txt","w");
	  if (fiout) {
	    strcpy(ws,c_tab.tab->format);
	    printLStr(fiout,ws);
	    pri=c_tab.tab->strings;
	    for(;pri;) {
	      strcpy(ws,pri->line);
	      printLStr(fiout,ws);
	      pri=pri->next;
            };
            fclose(fiout);
          };
	  key=0;
	  break;

	 case 'Z':
	 case 'z':
	     strcpy( ws,&c_tab.edit->line[c_tab.x0+c_tab.x-x1-2]);
	     ws[c_tab.x1 - c_tab.x + 1] = 0;

             hbb_close(hbl->hbb);
             if (!show) editmode = 1;
	     edit=zoomStr(ws,3,3,1,show);
             editmode = 0;
	     /* scrcolor(White,Black); */
             hbb_open(hbl->hbb);

	     /*printf(" edit=%d\n",edit);*/
	     if (edit) {
	       for(i=0; ws[i]; i++)
	          c_tab.edit->line[c_tab.x0+c_tab.x +i-x1-2]=ws[i];
               cursosON=0;
	       cursosOn();
	     };
	     key = 0;
	 break;

	case KB_MOUSE:
          if (mouse_info.but1 == MRELEASED ) {
#if EDITTAB_DEBUG
            printf(" mouse: but1=%d, x=%d, y=%d\n",
                 mouse_info.but1, mouse_info.row,
                 mouse_info.col );
            printf(" cursor: x=%d, x1=%d, y=%d\n",c_tab.x,c_tab.x1,c_tab.y);
#endif
            if (rc=hbl_select(hbl)) {
               push_key(rc);
               if (rc==KB_UP) push_key(1);
               else if (rc==KB_DOWN) push_key(1);
               else if (rc==KB_LEFT) push_key(2);
               else if (rc==KB_RIGHT) push_key(c_tab.x1-c_tab.x+3);
            }
            else if (m_inbox(x1,y1,x2,y2)) {
              dy=mouse_info.row-c_tab.y;
              if (dy>0) {
                push_key(KB_DOWN);
                push_key(dy);
              }
              else if (dy<0) {
                push_key(KB_UP);
                push_key(-dy);
              };
              dx=mouse_info.col-c_tab.x;
              if (dy==0 && c_tab.x<=mouse_info.col 
                 && mouse_info.col<=c_tab.x1) {
                push_key(KB_ENTER);
              }
              else if (dx>0) {
                push_key(KB_RIGHT);
                push_key(dx);
              }
              else if (dx<0) {
                push_key(KB_LEFT);
                push_key(-dx);
              };
            }; /* if (rc) */
          };
	  key=0;
	  break;

	default: key = 0;

       }  /*  Case  */
     } /* if */
     goto_xy(x2-20,y1);
	  print(" line = %d %c%c",c_tab.y0+ (c_tab.y-y1-2 ),boxFrame[5],boxFrame[5]);
     curon();
     goto_xy(x2,y1);
     key = pop_key();
     curoff();
     gotoXY();
#if EDITTAB_DEBUG
     printf("Edittab: key=%d\n",key);
#endif
  } /* while */

  if (c_tab.del) free(c_tab.del);
  if (hbl) { hbl_close(hbl); hbl_delete(hbl);};
  scrcolor(White,Black);
  put_text(&pscr);
  return edit;
}  /* edittab */


/*-------------------- end of file ------------------------*/

