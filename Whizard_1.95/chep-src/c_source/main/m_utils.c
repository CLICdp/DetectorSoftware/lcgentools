#include "tptcmac.h"
#include "syst2.h"
#include "crt.h"
#include "global.h"
#include "syst2.h"
#include "physics.h"
#include "screen.h"
#include "files.h"
#include "crt_util.h"
#include "file_scr.h"
#include "read_mdl.h"
#include "os.h"

#include "m_utils.h"


#define nhardmdl 4
#define newmodeltxt "  N E W    M O D E L  "


void fillModelMenu(void)
{ int i,j;
	FILE * txt;
	char name[80];
	strcpy(modelmenu,"\026");
	maxmodel=0;
	for (i=1;i<=9;i++)

	{ 
		txt=fopen(scat("%smodels%c%s%d.mdl",pathtouser,f_slash,mdFls[0],i),
                    "r");		
		if (txt==NULL) goto exi;
		  fgets(name,60,txt);
		  j=strlen(name)-1;
		  if (name[j]=='\n') name[j]=0;
		  trim(name);
		  sbld(name," %s",name);
		  for (j=strlen(name); j<22;j++) name[j]=' ';
		  name[22]=0;
		  strcat(modelmenu,name);
		  fclose(txt);
		  maxmodel++;
		}
 exi:
	  if (maxmodel <9) strcat(modelmenu,newmodeltxt) ;

}



boolean  deletemodel(int n)
{
 int   i;
 char from[STRSIZ],to[STRSIZ];
 searchrec  s;


    sprintf(from,"%smodels%c%s%d.mdl",pathtocomphep,f_slash,mdFls[0],n);

    if (find_first(from,&s,archive)==0)
    {
	if (mess_y_n(44,10,"This model can not be deleted.$Restore original version?$"))
	for (i=0;i<4;i++) 
	{ 
	   sprintf(from,"%smodels%c%s%d.mdl",pathtocomphep,f_slash,mdFls[i],n);
	   sprintf(to,"%smodels%c%s%d.mdl",pathtouser,f_slash,mdFls[i],n);
	   copyfile(from,to);
	}	
	else  return  FALSE;
	}
	else
	if ( mess_y_n(56,10,"Delete model?$"))
        {
           for(i=0;i<4;i++)
           {
              sprintf(to,"%smodels%c%s%d.mdl",pathtouser,f_slash,mdFls[i],n);
	      unlink(to);
           }

           if (n < maxmodel)
           {    
	  for(i=0;i<4;i++)
	  {		  
	    sprintf(from,"%smodels%c%s%d.mdl",pathtouser,f_slash,mdFls[i],maxmodel);
	    sprintf(to,  "%smodels%c%s%d.mdl",pathtouser,f_slash,mdFls[i],n);
	    rename(from,to);	  
	  }
      }
      return TRUE;
   } 
   return FALSE;
}



boolean  makenewmodel(void)
{shortstr  newName;
 int       key, nmdl;
 byte      npos;
 byte      i;
 char oldModels[STRSIZ];
 void *pscr = NULL;
   npos = 1;
	sbld(newName,"%c",' ');
	do
	{  goto_xy(1,7); print("Model name :");
		key = str_redact(newName,npos);
	}while (key!=KB_ENTER &&  key!=KB_ESC);
	trim(newName);
	if (key == KB_ESC || strlen(newName) == 0) return FALSE;
	sbld(newName,"_%s",newName);
   goto_xy(5,9);
   print(" Choose a template ");
	nmdl = 1;
	strcpy(oldModels,copy(modelmenu,1,(integer)strlen(modelmenu) - 22));
	menu1(5,10,"",oldModels,"",&pscr,&nmdl);
	put_text(&pscr);
   if (nmdl == 0) return FALSE;
	readModelFiles (nmdl);
	for (i=0;i<4;i++) strcpy(modelTab[i].mdlName,newName);
	writeModelFiles (n_model);
	fillModelMenu();
   clrbox(5,10,30,12);
   return TRUE;
}


boolean  continuetest(void)
{shortstr  txt;
 word      k, ndel, ncalc, nrest, recpos;

   menuq=fopen(MENUQ_NAME,"rb");
  
   for(k=1;k<=subproc_sq;k++)
   {
      rd_menu(2,k,txt,&ndel,&ncalc,&nrest,&recpos);
      if (nrest != 0)
      {
         messanykey(15,15,"All diagrams must be calculated$or marked as deleted$");
         fclose(menuq);
         return FALSE;
      }
   }
   fclose(menuq);
   return TRUE;
}


void  clear_tmp(void)
{  
   unlink(CATALOG_NAME);
   unlink(ARCHIV_NAME);
   unlink(ACTVARS_NAME);   
}




void  changeexitmenu(void)
{byte     i;
 char     edmenu[STRSIZ];
 int      n;
 char     s[STRSIZ];
 void * pscr = NULL;
   strcpy(edmenu,copy(exitmenu,1,1 + 10 * 22));
   n = 1;
   while (TRUE)
   {
      menu1(56,10,"",edmenu,"",&pscr,&n);
      if (n == 0)
      {
         strdelete(exitmenu,1,1 + 10 * 22);
         strinsert(edmenu,exitmenu,1);
         return;
      }

      strcpy(s,copy(edmenu,2 + (n - 1) * 22,22));
      goto_xy(5,24);
      print("New name: ");
		if (str_redact(s,1) == KB_ENTER)
      {
         trim(s);
         sbld(s," %s",s);
         i = (byte) strlen(s);
         while (i < 21) s[i++] =' ';
         s[21] = '\0';
         for (i = 1; i <= 21; i++)
            edmenu[1 + 22 * (n - 1) + i-1] = s[i-1];
         be_be();
      }
      goto_xy(5,24);
      clr_eol();
   }
}

