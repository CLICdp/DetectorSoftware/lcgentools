#include "tptcmac.h"
#include "syst2.h"
#include "crt.h"
#include "crt_util.h"
#include "help.h"

#include "file_scr.h"


table  modelTab[4] =
{{"","","",NULL},
 {"","","",NULL},
 {"","","",NULL},
 {"","","",NULL}};

char  errorText[256] = "";

static char buff[90]=
"                                                                                ";
int fgets0(char *  c,int  n, FILE * fp)
{  int l;
   char * res;
	res=fgets(c,n,fp);
	if (res==NULL) return (-1);
	l=(integer)strlen(c) - 1;
	if (c[l] == '\n') { c[l] = '\0';  return '\n';}  else return 0;

}

static int fgets1(char *  c,int  n, FILE * fp)
{  int l;
   char buff[STRSIZ];
   
   if(fgets(c,n,fp)==NULL) return 0;	
   l=(integer)strlen(c) - 1;
   if (c[l] == '\n')  c[l] = '\0';  else fgets(buff,STRSIZ,fp);
   return 1;	
}



static boolean readnewline(FILE* fileptr , integer  lwidth , linelist  lastln)
{
	linelist newline;
	int readres;
	if ( (lastln !=NULL)&&  (lastln->next == NULL) )
	{
	  newline=(linelist)getmem(sizeof(linerec)-STRSIZ+lwidth+1);
	  if (newline == NULL)
				strcpy(lastln->line,copy(" -- FILE TOO LARGE --",1,lwidth));
	  else
		 {
			readres=fgets0(newline->line ,lwidth,fileptr);
			if (readres == -1)
			{
			  free(newline);
			  return FALSE;
			}
			lastln->next=newline;
			newline->next=NULL;
			newline->pred=lastln;
		  }

		  return TRUE;
	}
	 return FALSE;
}

static void dellinelist(linelist  ln)
{
  linelist lndel;
  while (ln !=NULL)
  {
	 lndel=ln;
	 ln=ln->next;
	 free(lndel);
  }

}

void cleartab(table * tab)
{
  dellinelist(tab->strings);
  tab->strings=NULL;
}

void showtext(int x1, int y1, int x2, int y2, char* headline,
 FILE * fileptr )
{
	integer   key;
	pointer   pscr;
	integer   i,l;
	linelist  currentline,lastline;
	int width;
	
	if (fileptr==NULL )
        {
	   messanykey(10,10," File is empty $");
           return;
	}

 	width=x2-x1-1;

	get_text(x1,y1,x2,y2,&pscr);

	scrcolor(Black,White);
	chepbox(x1,y1,x2,y2);	
	goto_xy(x1+1,y1); print("*");
	l=strlen(headline);
	if ( l < (x2-x1) )
	 {
		goto_xy(x1+(x2-x1-l)/2,y1);
		scrcolor(White,Black);
		print(headline);
		scrcolor(Black,White);
	}

	currentline=(linelist)getmem(sizeof( linerec) - STRSIZ+width+1);
	currentline->next = NULL;
	currentline->pred = NULL;
	fgets0(currentline->line ,width,fileptr);
   key = 0;
   do
   {
      switch (key)
      {
	case KB_PAGEU:
	   for (i=y1+1;i<y2;i++)
	     if (currentline->pred !=NULL)  currentline=currentline->pred;
           break;

	case KB_UP:
	   for(i=1;i<(y2-y1)/2;i++)
              if (currentline->pred !=NULL)  currentline=currentline->pred;
	   break;

	case KB_PAGED: 
	   for (i=y1+1;i<y2;i++)
	   {
	      readnewline(fileptr,width,currentline);
	      if (currentline->next !=NULL)  currentline=currentline->next;
	   }
	   break;

	case KB_DOWN:
	   for(i=1;i<(y2-y1)/2;i++)
           {
	      readnewline(fileptr ,width,currentline);
	      if (currentline->next !=NULL)  currentline=currentline->next;
	   }
			break;

      }  /*  Case  */

/*    display */

      lastline=currentline;
      goto_xy(x2-4,y1); 
      if( lastline->pred == NULL) for (i=0;i<4;i++) print("%c",boxFrame[1]);
              else                print("PgUp");
      for (i =y1+1; i <y2; i++)
      {
	   goto_xy(x1+1,i);
	   readnewline(fileptr,width,lastline);
	   if ( lastline !=NULL ) { print("%s",lastline->line);
	    lastline=lastline->next;}

	    print(&(buff[ where_x()+80-x2]) );
                        
       }
       goto_xy(x2-4,y2);
       if (lastline == NULL) for (i=0;i<4;i++) print("%c",boxFrame[5]);
               else                            print("PgDn");   
       key = inkey();
/* mouse filter */
       if ( (key == KB_MOUSE)&&(mouse_info.but1==2)
         &&(mouse_info.col >= x1)&&(mouse_info.col <= x2)  )
       { 
         if (x2 - mouse_info.col <4 ) 
         { if(mouse_info.row == y1) key=KB_PAGEU; else            
           if(mouse_info.row == y2) key=KB_PAGED;
         }   
         if ( (mouse_info.row == y1)&&(mouse_info.col - x1 <3)) key=KB_ESC;
         if ((mouse_info.row > y1)&&(mouse_info.row < y2))                        
         { if (mouse_info.row < (y1+y2)/2)  key=KB_UP; else key=KB_DOWN;}
       }                   
/* end of filter */                                        
   }  while ((key != KB_ESC)&&(key !=KB_BACKSP));
     
   scrcolor(White,Black);
	put_text(&pscr);
	while (currentline->pred != NULL )  currentline=currentline->pred;

	dellinelist(currentline);

}


void  readtable0(table*  tab,FILE * txt)
{  
        linelist lastln,newline;
        int len,i;
        linerec zeroline;
   
        if(txt==NULL) return;
        cleartab(tab);
        fgets1(tab->mdlName,STRSIZ,txt);
        fgets1(tab->headln,70,txt);
        fgets1(tab->format,STRSIZ,txt);
        len=strlen(tab->format);
        while (tab-> format[len] !='|')  len--;
        tab->format[len+1]=0;
   
         zeroline.next=NULL;
         lastln= &zeroline;
   
         newline=(linelist)getmem(sizeof(linerec)-STRSIZ+len+2);
         while (fgets1(newline->line ,len+1,txt) && newline->line[0]!='=')
         {
             newline->line[len+1]=0;
             for (i=strlen(newline->line);i<=len;i++) newline->line[i]=' ';
             lastln->next=newline;
             newline->next=NULL;
             newline->pred=lastln;
             lastln=newline;
             newline=(linelist)getmem(sizeof(linerec)-STRSIZ+len+2);
         }
         free( newline);
         tab->strings=zeroline.next;
         if (tab->strings != NULL)tab->strings->pred=NULL;
}

void  readtable(table*  tab, char* fname)
{

  FILE * txt;

  txt=fopen(fname,"r");
  readtable0(tab,txt);
  fclose(txt);
}



void  writetable0(table*  tab, FILE * fi)
{ int i;

  linelist ll=tab->strings;
  if(!(fi)) return;
  fprintf(fi,"%s\n",tab->mdlName);
  fprintf(fi,"%s\n",tab->headln);
  fprintf(fi,"%s\n",tab->format);
  while (ll) {  fprintf(fi,"%s\n",ll->line); ll=ll->next;}
  for(i=0;i<strlen(tab->format);i++) fprintf(fi,"=");
  fprintf(fi,"\n");
}

void  writetable(table*  tab,char*  fname)
{
  FILE * fi;
  fi=fopen(fname,"w");
  writetable0( tab, fi);
  fclose(fi);
}





