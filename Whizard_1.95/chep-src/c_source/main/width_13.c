/*          main/width_13.c
    ==================================
            S.Shichanin (IHEP, Protvino - DESY)
            26 June 1991   original version on Turbo-Pascal
            28 Nov  1995   revision 3.0   E1,E2 vars
    ===================================
*/ 

#include <math.h>
#include "tptcmac.h"
#include "syst2.h"
#include "global.h"
#include "crt.h"
#include "screen.h"
#include "physics.h"
#include "crt_util.h"
#include "optimise.h"
#include "n_compil.h"
#include "num_tool.h"
#include "num_serv.h"
#include "procvar_.h"
#include "graf.h"
#include "dalitz.h"

#define extern /* globals defined here */
#include "width_13.h"
#undef extern


#define pi 3.141592653589793238                   


static byte         ishft, equalparticles; 
static double       width, totcoef;
static double       cutt[4]; 
static double       ccc; 
static marktp       heapbg; 
static integer      l; 
static int          k;
static long         ntotal, nx, ny; 
static double       cut; 
static prgcodeptr   prg; 

static void  calcdecaycoef(void)
{double      pi3; 
 double sc1,sc2,sc3;
 byte        i, j; 

   calcfunctions(&err_code);   /*  CalcFunctions; */ 
   if (err_code != 0) return;
   pi3 = pi * pi * pi; 
   i = 1; 
   equalparticles = 0; 
   while (i < nout) 
   { 
      j = i + 1; 
      while (j <= nout && strcmp(pname[order[i + nin-1]],
             pname[order[j + nin-1]]) == 0)
         ++j; 
      if (j > i + 1) 
      { 
         equalparticles = j - i; 
         ishft = nin + i - 1; 
      } 
      i = j; 
   } 
   for (i = 1; i <= nin + nout; i++) 
      if (*pmass[i-1] < 0 || 
          *pmass[0] <= *pmass[1] + *pmass[2] + *pmass[3]) 
         goto errorexit;

   m0  = *pmass[order[0]]; 
   m1  = *pmass[order[1]]; 
   m2  = *pmass[order[2]]; 
   m3  = *pmass[order[3]]; 
   mm0 = sqr(m0);
   mm1 = sqr(m1);
   mm2 = sqr(m2);
   mm3 = sqr(m3); 
   c1  =   MAX(m1,cutt[order[1]]);
   c2  =   MAX(m2,cutt[order[2]]);
   c3  =   MAX(m3,cutt[order[3]]);
   sc1=sqr(c1);
   sc2=sqr(c2);
   sc3=sqr(c3);
/* ===========================================================

   variables:
	      x = E1 == E(order(1));
	      y = E3 == E(order(3));

   ===========================================================*/
   xdown = m1; 
   ydown = m3; 
   xup   = (mm0+mm1-sqr(m2+m3))/(2*m0);
   yup   = (mm0+mm3-sqr(m2+m1))/(2*m0);

   xdown = (c3 > asy2(xdown)) ? asx2(c3) : xdown;
   xup   = (c3 > asy2(xup))   ? asx1(c3) : xup;

/*   ydown = (c1 > asx2(ydown)) ? asy2(c1) : ydown; */
   ccc=asx2(ydown);
   if (c1 > ccc)
       ydown =  asy2(c1);

   yup   = (c1 > asx2(yup))   ? asy1(c1) : yup;

   xdown = MAX( c1, xdown); 
   ydown = MAX( c3, ydown);

/* xup   = xup*0.9999999999;
   yup   = yup*0.9999999999;
*/
   if(xup <= xdown || yup <= ydown) goto errorexit;
   totcoef = 1. / (64.0 * pi3 * m0 ); 
/*   totcoef = 1. / (4* 64.0 * pi3 * m0 ); */
   calcconstants();
   setlimits();
   err_code = 0;
   return;

   errorexit: err_code = 3;
}



static void   swap(byte  i, byte  j)
{double       temp; 
 byte         ii; 

   for (ii = 1; ii <= nin + nout; ii++) 
      if (ii != i && ii != j) 
      { 
         temp = (*vararr)[sp_pos(ii,i)].tmpvalue; 
         (*vararr)[sp_pos(ii,i)].tmpvalue 
              = (*vararr)[sp_pos(ii,j)].tmpvalue; 
         (*vararr)[sp_pos(ii,j)].tmpvalue = temp; 
      } 
} 

static void  calcscalarproducts(double  x, double  y)
{ 
/* p0.p1 */ (*vararr)[sp_pos(1,1+order[1])].tmpvalue 
	    = m0 * x;

/* p0.p2 */ (*vararr)[sp_pos(1,1+order[2])].tmpvalue 
	    = m0*(m0  - x - y) ;

/* p0.p3 */ (*vararr)[sp_pos(1,1+order[3])].tmpvalue 
	    = m0 * y;

/* p1.p2 */ (*vararr)[sp_pos(1+order[1],1+order[2])].tmpvalue 
            = (mm0+mm3-mm1-mm2- 2 * m0 *y) / 2.0;

/* p1.p3 */ (*vararr)[sp_pos(1+order[1],1+order[3])].tmpvalue 
            = (mm0+mm2-mm1-mm3-2* m0 * (m0-y-x)) / 2.0;

/* p2.p3 */ (*vararr)[sp_pos(1+order[2],1+order[3])].tmpvalue 
            = (mm0+mm1-mm2-mm3-2* m0 * x) / 2.0;
} 


static double  fun(double  x, double  y)
{double        f; 

   calcscalarproducts(x,y); 
   f = totcoef * matrixelement(prg); 
   if (err_code != 0) return f;

/*  if( integration )  gpixel(x,y); */

      if (escpressed()) err_code = 4; 
   return f; 
} 


static double  simple(void)
{double        ans; 

   ans = fun(ext_x,ext_y); 
   return ans < 0.0 ? 0.0 : ans; 
} 


static double  subst2(void)
{byte          i; 
 double        sum; 

  sum = 0.0; 
  for (i = 0; i < 2; i++) 
  { 
     swap(2 + ishft,1 + ishft); 
     sum += simple(); 
  } 
  return sum; 
} 


static double  subst3(void)
{byte          i;
 double        sum; 

   sum = 0.0; 
   for (i = 0; i < 3; i++) 
   { 
      swap(3 + ishft,2 + ishft); 
      sum += simple(); 
      swap(3 + ishft,2 + ishft); 
      sum += simple(); 
   } 
   return sum; 
} 


double  sfunc(void)
{ 
   switch (equalparticles) 
   {
      case 0:
         return simple(); 
   
      case 2:
         return subst2(); 
   
      case 3:
         return subst3(); 
   }  /* case */ 
   return 0.0;
} 


static double  gamma13(double  y)
{ 
   ext_y = y; 
   return sfunc(); 
} 


double  dgamma_dx(double  x)
{double           result; 
 double           ymin, ymax; 

   ext_x = x; 
   ymin = sy2(x); 
   ymax = sy1(x); 
   ymin = MAX(ymin, ydown);
   ymax = MIN(ymax, yup);
   result = 0; 
   if (ymin < ymax) 
    integral(ymin,ymax,eps,gamma13,&result); 
   return result; 
} 

static void  calcwidth13(void)
{
/* char    buf1[82], buf2[82]; */
   err_code=0;
   calcdecaycoef(); 
      if(xdown>= xup) {width=0. ; return;}
 
   if (err_code == 0) 
   { 
      grafminmax.xmin = xdown - 0.01 * (xup - xdown); 
      grafminmax.xmax = xup + 0.01 * (xup - xdown); 
      grafminmax.ymin = ydown - 0.01 * (yup - ydown); 
      grafminmax.ymax = yup + 0.01 * (yup - ydown); 
         
/*      if (integration) 
      { 
         setgraph(); 
         scalee = 0; 
         sbld(buf1,"Integration of %s",processstr);
         sbld(buf2,"E(%s)  GeV",pname[order[1]]);
         gaxes(scalee,buf1,buf2,scat("E(%s)  GeV",pname[order[3]])); 
      }
*/
       
/*  **************  Regularization ************** */ 
      integral(xdown,xup,eps,dgamma_dx,&width);  

/*      if (integration) 
         restorecrt();
*/          
   } 
} 



void  dalitzinformation(void)
{byte        i; 
   processinfo();
   diagramsinfo();
   sq_diagramsinfo();
modelinfo();
   goto_xy(1,16); 
   scrcolor(LightRed,Black); 
   clr_eol(); 
   print(" (Sub)process :   "); 
   scrcolor(White,Black);
   print("%s",processstr); 
   goto_xy(1,17); 
   scrcolor(LightRed,Black); 
   clr_eol(); 
   print(" Width        :   "); 
   scrcolor(White,Black); 
   if (err_code == 0)
      print(" %12g GeV",width); 
   else 
   { 
      scrcolor(Yellow,Black); 
      print("????????????"); 
   } 
   goto_xy(45,17); 
   scrcolor(LightRed,Black); 
   print("  Accuracy : "); 
   scrcolor(White,Black); 
   print("%9g",eps); 
   for (i = 1; i <= 3; i++) 
   { 
      goto_xy(1,17 + i); 
      scrcolor(LightRed,Black); 
      clr_eol(); 
      print(" Min Energy of "); 
      scrcolor(Yellow,Black); 
      print("%s",copy(pname[order[i]],1,3)); 
      scrcolor(LightRed,Black); 
      print(" : "); 
      scrcolor(White,Black); 
      print("%12.5g GeV",cutt[order[i]]); 
   } 
   goto_xy(45,18); 
   clr_eol(); 
   scrcolor(LightRed,Black); 
   print("  X-axis   : "); 
   scrcolor(Yellow,Black); 
   print("Energy(%s)",pname[order[1]]); 
   goto_xy(45,19); 
   clr_eol(); 
   scrcolor(LightRed,Black); 
   print("  Y-axis   : "); 
   scrcolor(Yellow,Black); 
   print("Energy(%s)",pname[order[3]]); 
} 


/* -----------------  D A L I T Z  end  -------------- */ 


static double  pwidth(void)
{ 
   calcwidth13(); 
   return width; 
} 


static void  calctotwidth(void)
{ 
   integration = TRUE; 
   calcwidth13(); 
   if (err_code != 0) 
     errormessage(); 
   dalitzinformation(); 
} 

void  decay13(integer  n_sub)
{int          kk;
 double       oldeps; 
 int          recalc;
 int          olderr;
 char         optmenu[190]; 
 int          tmp;
 int          i;

   order[0]=0;
   order[1]=1;
   order[2]=2;
   order[3]=3;


   mark_(&heapbg); 
   findprocname(n_sub,processstr,&err_code); 
   if (err_code != 0) return;

   loadvars(n_sub); 
   findprtcls(processstr); 

/* InitInfo; */

   compileprocess(n_sub,&prg); 
   for( i=1; i<4; i++) 
      cutt[order[i]] = *pmass[order[i]]; 
   eps = 0.001; 
   ntotal = 300;  
   nx = 30; 
   ny = 30; 
   cut = 0.0; 
   calctotwidth(); 
   sbld(optmenu, "* Set PLOT variables   "); 
   sbld(optmenu,"%s Energy cuts          ",optmenu); 
   sbld(optmenu,"%s Accuracy             ", optmenu); 
   k = 1; 
   l = 1; 
   do 
   { void * pscr = NULL;  
      menu1(56,6,"", "\026"
         " View/change data     "
         " Set energy ranges    "
         " Set accuracy         "
         " Set Dalitz-plot axes "
         " Dalitz-plot          "
         " Energy plot          "
         " Width dependence     "  ,"h_00652*",&pscr,&k); 
      switch (k) 
      {
         case 1: /*  ******* View/change data ********  */ 
            if (changedata())
   		{for( i=1; i<4; i++) 
    		  cutt[order[i]] = *pmass[order[i]]; 
               	 calctotwidth();} 
         break; 
         case 2: /* ****  Cuts        ******* */ 
            kk = 1; 
            recalc=FALSE;
            for (i = 1; i <= 3; i++) 
            { 
 label_reinput:  
                ccc = cutt[order[i]]; 

                 if (i != 2 && correctDoubleS(20,17+i,"",&ccc)) 
               {
                 if(ccc != cutt[order[i]]) {
		   if(m0 <= cutt[1]+cutt[2]+cutt[3]-cutt[order[i]]+ccc)
		   { 
		    messanykey(20,13,"The value is too large! $");
		    goto label_reinput;
		   } 
		   recalc=TRUE;
		   cutt[order[i]] = ccc; 
		}
               }
               else
               dalitzinformation();	
            }   

            if (recalc) calctotwidth();
                  break; 
        
         case 3: /* ****  Accuracy    ******* */ 
            kk = 1; 
            recalc=FALSE;
            oldeps = eps; 
           label1:              
            if (!correctDoubleS(45,17,"  Accuracy : ",&eps))
                   goto label1;
            if (eps > 0.001 || eps < 1.E-8) 
             { 
              messanykey(10,18,"Expected value 1.E-8 - 1.E-3 $"); 
                 goto label1;
             } 
            goto_xy(1,22); clr_eol(); 
            goto_xy(1,23); clr_eol(); 
            if (oldeps > eps) recalc=TRUE; 
            if (recalc) calctotwidth();
                  break; 
      

         case 4: /* ****  Axes        ******* */ 
                  tmp=order[1];
                  order[1]=order[2];
                  order[2]=order[3];
                  order[3]=tmp;     
		  for(i=1;i<=3;i++)
		     cutt[order[i]] = *pmass[order[i]];
                  break; 
         case 5: /*  ***** Dalitz Plot *******  */ 
            calcdecaycoef(); 
            if(xdown >= xup) 
            {break;  }
            if (err_code == 0) 
            { 
               kk = 1; 
               do 
               { void * pscr = NULL;
                  menu1(56,8,"","\026"
                     " Show plot            "
                     " Options              ","",&pscr,&kk); 
                  switch (kk) 
                  {
                  case 1: 
                     olderr=err_code;   
                     integration = FALSE; 
                     dalitz_plot(ntotal,nx,ny); 
                     if (err_code == 4) 
                     {  
                        errormessage(); 
                        err_code = olderr; 
                     } 
                  break; 
            
                  case 2:   
                  if (correctLong(24,22,"Number of sampling points:",
                                                               &ntotal,0))
                      ; 
                  if (correctLong(1,23,"Number of X sub-regions: ",&nx,0))
                     ; 
                  if (correctLong(45,23,"Number of Y sub-regions: ",&ny,0))
                     ; 
                  }   /*  Case  */ 
               }  while (kk != 0); 
               goto_xy(1,22); clr_eol(); 
               goto_xy(1,23); clr_eol(); 
            } 
            else
               errormessage(); 
         break; 
        
         case 6: /* ****  Energy    plot **** */ 
          calcdecaycoef();
          integrated_on_m34();
          dalitzinformation();
         break; 

         case 7: /* ****  Width dependence **** */
            integration = FALSE; 
            paramdependence(pwidth,processstr,"Width [GeV]"); 
      }  /*  Case  */ 
      dalitzinformation(); 
/*    If k<0 Then ShowHelp(PathToComphep+'comphep.hlp',6520-k); */ 
   }  while (k != 0); 
   release_(&heapbg); 
   clrbox(1,16,80,24); 
} 
