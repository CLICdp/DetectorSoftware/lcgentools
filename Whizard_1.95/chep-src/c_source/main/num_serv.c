#include <math.h>
#include "tptcmac.h"
#include "os.h"
#include "syst2.h"
#include "crt.h"
#include "global.h"
#include "physics.h"
#include "files.h"
#include "crt_util.h"
#include "screen.h"
#include "optimise.h"
#include "procvar_.h"
#include "n_compil.h"
#include "num_tool.h"
#include "graf.h"
#include "num_serv.h"


static void  correctdata(int l)
{char        buff[21]; 
 byte        i; 
 char       ch;
 FILE * varf;
   
   varf=fopen(VARFILE_NAME,"r+b");
   fread(&ch,1,1,varf); fread(&ch,1,1,varf);
   fseek(varf,1 + l*ch,SEEK_SET); 
   FREAD1(i,varf); 
      
   if (correctDouble(10,23,"Enter new value :",&(*vararr)[i-1].tmpvalue,1)) 
   { 
      sprintf(buff,"%14lf",(*vararr)[i-1].tmpvalue);
      fseek(varf,10 + (l - 1) * ch,SEEK_SET); 
      f_write(buff,14,1,varf); 
   } 
   fclose(varf);             
} 


void  paramdependence(r_func  ff, char*  procname, char*  resultname)
{  pointer     pscr  = NULL;
   void *      pscr0 = NULL;
   int        l;
   char        prmname[7];
   integer     key;  
   byte        nprm;
   double      minprm, maxprm;
   boolean     logscale;
   int         npoints;
   double        memprm, factprm, stepprm, ans;
   realseq     r_, rr_;
   word        count;
   char        upstr[STRSIZ], downstr[STRSIZ];
   int         k;
   char ch;
   FILE * varfile;

   l = 1;
   menu_f(56,14,"Parameter",VARFILE_NAME,"",&pscr0,&l);

   if (l == 0)
   {  nprm = 0;
      return;
   } 
   
   strcpy(prmname,"      ");
   varfile=fopen(VARFILE_NAME,"r"); 
   fread(&ch,1,1,varfile);fread(&ch,1,1,varfile);
   fseek(varfile,3 + (l - 1) * ch,SEEK_SET); 
   fread(prmname,6,1,varfile) ; 
   goto_xy(1,23);
   scrcolor(LightRed,Black);
   clr_eol(); 
   print("Parameter    : "); 
   scrcolor(White,Black);
	print(prmname);
	print("\n");
   fseek(varfile,1 + l * ch,SEEK_SET); 
   FREAD1(nprm,varfile); 
   fclose(varfile); 
   minprm = (*vararr)[nprm-1].tmpvalue; 
   maxprm = (*vararr)[nprm-1].tmpvalue; 

label1:
   goto_xy(24,23);
   if (!correctDouble(24,23,"min ",&minprm,0)) 
   { 
      nprm = 0;
      goto exi;
   } 

label2:
   if (!correctDouble(62,23,"max ",&maxprm,0)) 
   { 
      goto_xy(62,23);
      clr_eol(); 
      goto label1;
   } 
   if (maxprm <= minprm) 
   { 
      messanykey(10,10,"Range check error$"); 
      goto label2;
   } 

label3:
   logscale = FALSE; 
   if (minprm > 0 && log10(maxprm/minprm)>=1 ) 
   do 
   {
      goto_xy(22,24);
      scrcolor(White,Black);
      print("Scale ");
      scrcolor(Yellow,Black);
      if (logscale)
         print("Log  "); 
      else
         print("Norm"); 
      scrcolor(White,Black); 
      key = inkey(); 
      if (key == KB_F1 || key == KB_ESC)
		{  goto_xy(1,24);
         clr_eol();
         goto label2;
      } 
      else
         if (key != KB_ENTER)
            logscale = !logscale;
   }  while (key != KB_ENTER); 


label4: npoints = 21;
   if (correctInt(37,24,"Number of points ",&npoints,0)) 
   { 
      if (npoints < 3) 
      { 
         messanykey(10,10,"Too few points!$"); 
         goto label4;
      } 
      if (npoints > 201) 
      { 
          messanykey(10,10,"Too many points!$"); 
          goto label4;
      } 
   } 
   else
   { 
      goto_xy(37,24);
      clr_eol(); 
      goto label3;
   }

   if (nprm == 0) goto exi;
   if (logscale) factprm = exp(log(maxprm / minprm)/(npoints - 1)); 
   else stepprm = (maxprm - minprm)/(npoints - 1); 

   informline(0,npoints);
   memprm = (*vararr)[nprm-1].tmpvalue; 
   (*vararr)[nprm-1].tmpvalue = minprm; 
   stepprm = (maxprm - minprm) / (npoints - 1); 

   r_ = NULL; 
   err_code = 0; 
   for (count = 1; count <= npoints; count++)
   { 
      if( err_code == 0 || err_code == 3 || err_code==5)
      {  ans = ff();
         rr_ = (realseq )getmem(sizeof(realseqrec));
         rr_->next = r_; 
         rr_->x = (*vararr)[nprm-1].tmpvalue; 
         if (err_code == 0) rr_->y = ans; else rr_->y =0;
          r_ = rr_;
      }
      else
      {  count=npoints+1;
         errormessage();
         disposerealseq(&r_); 
      }          
      informline(count,npoints);       
      if (logscale) (*vararr)[nprm-1].tmpvalue *= factprm; 
         else       (*vararr)[nprm-1].tmpvalue += stepprm; 
   } 



   scrcolor(White,Black);   
   goto_xy(1,23); clr_eol(); 
   goto_xy(1,24); clr_eol(); 


   if (r_ != NULL && r_->next != NULL) 
   { 
      scrcolor(White,Black);
		goto_xy(1,23);
      clr_eol(); 
      sbld(upstr,"Process: %s",procname);
      strcpy(downstr,"Press <Esc> to continue"); 
      k = 1; 
      do 
      { 
         menu1(56,17,"","\026"
            " Show   Plot          "
            " Save result in a file","",&pscr,&k); 
         switch (k)
         {
            case 1: /* 'Width [GeV]' */ 
               plot_2(logscale,r_,upstr,prmname,resultname); 
            break; 
         
            case 2: /* 'Width [GeV]' */ 
               writetable1(&r_,upstr,prmname,resultname);
         }   /*  Case  */ 
      }  while (k != 0); 
   } 
   disposerealseq(&r_); 
   (*vararr)[nprm-1].tmpvalue = memprm; 
   
exi:
   put_text(&pscr0);
   goto_xy(1,23);
   scrcolor(White,Black);
   clr_eol(); 
} 


boolean  changedata(void)
{void * pscr = NULL;
 int l;
 boolean cd;

   cd = FALSE;   
   do
   { 
      menu_f(56,11,"Select constant",VARFILE_NAME,"",&pscr,&l);
      if (l > 0)
      {  correctdata(l);
         cd = TRUE;
      }
   }  while (l != 0);
   return cd;
}


byte  scale_c(realseq  seq)
{byte sc;

   sc = 0;
   while (seq != NULL && seq->y > 0)
      seq = seq->next;
   if (seq == NULL)
   {
      if (mess_y_n(10,10," log - scale?$")) sc = 2;
   }
   else
      messanykey(10,10," Warning : Function not positive !$");
   return sc;
}


void  writetable1(realseq*  rr_, char*  upstr,
                      char*  leftstr, char*  rigthstr)
{char       filename[STRSIZ],buff[20];
 FILE *     outfile;
 int        i, tabnum = 0, doserror;
 searchrec  s;
 realseq    rr_mem;

   do
   {  tabnum++;
      sprintf(filename,"%s%ctab_%d.txt",pathtoresults,f_slash,tabnum);
      doserror = find_first(filename,&s,archive);
   }  while (doserror == 0);
   revers((pointer*) rr_);
	rr_mem = *rr_;
   outfile=fopen(filename,"w");
	for (i = 1; i <= 47; i++) fputc(' ',outfile);
   fprintf(outfile,"**   %s**\n",version);
   fprintf(outfile," %s\n",upstr);
   fprintf(outfile,"\n");
   fprintf(outfile," %s",leftstr);
   i = (integer)strlen(leftstr);
   while (++i < 20) fputc(' ',outfile);
   fprintf(outfile,"%s\n",rigthstr);
	while (rr_mem != NULL)
	{
		doub_str(buff,rr_mem->x);
		fprintf(outfile," %s          ",buff);
		doub_str(buff,rr_mem->y);
		fprintf(outfile,"%s\n",buff);
		rr_mem = rr_mem->next;
   }
   fclose(outfile);
	revers((pointer*) rr_);
   messanykey(10,12,scat(" You can find results in the file$%s$",filename));
}



void  errormessage(void)
{ 
   switch(err_code)
   {
      case 1:
         messanykey(10,10,"Division by zero$"); 
      break; 
   
      case 2:
         messanykey(10,10,"Out of memory$"); 
      break; 
   
      case 3:
         messanykey(10,10,"Unphysical values of variables$");
      break;
   
      case 4:
         messanykey(10,10,"User Break$"); 
      break; 
   
      case 5:
         messanykey(10,10,"Negative \"Sqrt\" argument$"); 
   } 
} 
