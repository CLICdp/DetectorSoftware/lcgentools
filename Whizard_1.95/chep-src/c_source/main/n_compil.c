#include "tptcmac.h"
#include "os.h"
#include "syst2.h"
#include "global.h"
#include "physics.h"
#include "optimise.h" 
#include "files.h"
#include "crt_util.h"
#include "procvar_.h"

#include "n_compil.h"

 shortstr  processstr;
 char      pname[4][6];
 realptr   pmass[4];
 setofbyte setfun;
 double      zero;



static void  louddiagrexpr(prgcodeptr*  prg, catrec*  crp)
{prgcodeptr  pntr;
 byte        nexpr, i; 

   pntr = *prg; 
   *prg = (prgcodeptr)getmem_(sizeof(prgcoderec)); 
   (*prg)->next = pntr; 

   fseek(archiv,crp->factpos,SEEK_SET); 
   FREAD1(nexpr,archiv);   /*  Nexpr must be equal 2  */ 

   readpolynom(&(*prg)->totn); 
   readpolynom(&(*prg)->totd); 

   fseek(archiv,crp->rnumpos,SEEK_SET); 
   FREAD1(nexpr, archiv);   /*  Nexpr must be equal 2  */ 
   readpolynom(&(*prg)->rnum); 

   fseek(archiv,crp->denompos,SEEK_SET); 
   FREAD1((*prg)->denorno,archiv);   /*  Nexpr must be equal ??  */ 

   for (i = 1; i <= (*prg)->denorno; i++) 
   { 
      FREAD1((*prg)->deg[i-1], archiv); 
      readpolynom(&(*prg)->mass[i-1]);
      readpolynom(&(*prg)->width[i-1]); 
      readpolynom(&(*prg)->den[i-1]); 
   } 
}


static void  compileprocess_(byte  nsub, prgcodeptr*  prg)
{catrec      cr;
 infoptr      i;
   cutlevel = maxsp + 1 - nin;
   *prg = NULL;
   catalog=fopen(CATALOG_NAME,"rb");
   archiv =fopen(ARCHIV_NAME,"rb");
   
   while( FREAD1(cr,catalog)) if (cr.nsub_ == nsub) louddiagrexpr(prg,&cr);
      
   fclose(catalog);
   fclose(archiv);

   i = info;

   while (i != NULL)
   {
      if (i->consttype == numb)
      {
         i->rval = i->ival;
         i->consttype =  rnumb ;
      }
      i = i->next;
   }

}


void  loadvars(byte  nsub)
{integer  err;  
   initvararray(nsub); 
   calcfunctions(&err); 
   makevarfile(); 
} 


void  findprtcls(char*  procname)
{byte     i, j, k, l;
 char    *prc, *tcp, txtcop[STRSIZ], buff[STRSIZ];

   strcpy(txtcop,procname);
   tcp = prc = txtcop;
   do if (*prc != ' ') *tcp++ = *prc;
   while(*prc++ != '\0');
   l = spos("->",txtcop);
   txtcop[l-1] = ','; 
   strdelete(txtcop,l + 1,1); 
   sbld(txtcop,"%s,",txtcop); 
   for (k = 1; k <= nin + nout; k++) 
   { 
      l = cpos(',',txtcop); 
      strcpy(pname[k-1],copy(txtcop,1,l - 1)); 
      trim(pname[k-1]); 
      strdelete(txtcop,1,l); 
   } 

   for (i = 1; i <= nin + nout; i++) 
   { 
      locateinbase(pname[i-1],&j); 
      strcpy(buff,prtclbase[j-1].massidnt); 
      if (strcmp(buff,"0") == 0)
         pmass[i-1] = &zero; 
      else 
      { 
         k = maxsp + 1; 
			while (strcmp((*vararr)[k].sourse->varname,buff) != 0) k++;
         pmass[i-1] = &(*vararr)[k].tmpvalue; 
      } 
   } 
} 


void  findprocname(integer  n_sub, char*  procname, integer*  errcd)
{word ndel, ncalc, nrest, recpos;

   menuq=fopen(MENUQ_NAME,"rb");
   rd_menu(2,n_sub,procname,&ndel,&ncalc,&nrest,&recpos);
   fclose(menuq);
   if (nrest != 0) 
   { 
      messanykey(10,10,
"All diagrams in the subprocess must be$calculated or marked as deleted$"); 
      *errcd = -1; 
      return;
   } 
   if (ncalc == 0) 
   { 
      messanykey(10,10,
         " Set of calculated diagrams for this processis is empty$"); 
      *errcd = -2; 
      return;
   } 
   *errcd = 0; 
} 


void  compileprocess(byte  nsub, prgcodeptr*  prg)
{ 
   initinfo(); 
   compileprocess_(nsub,prg); 
} 


void  compileall(canalptr*  allcanal, integer*  errcd)
{word       n_sub, ndel, ncalc, nrest, recpos; 
 byte       i; 
 canalptr   curentcanal; 
 char       procname[STRSIZ]; 

   initinfo(); 
   *errcd = 0; 
   menuq=fopen(MENUQ_NAME,"rb"); 
   n_sub = 1; 
   *allcanal = NULL; 
   for(n_sub=1;n_sub<=subproc_sq;n_sub++) 
   { 
      rd_menu(2,n_sub,procname,&ndel,&ncalc,&nrest,&recpos); 
      if (nrest != 0) 
      { 
         messanykey(10,10,
"All diagrams in the subprocess must be$calculated or marked as deleted$"); 
         *errcd = -1; 
         return;
      } 
      if (ncalc != 0) 
      {  
         curentcanal = *allcanal; 
         *allcanal =(canalptr ) getmem_(sizeof(canal)); 
         findprtcls(procname); 
         (*allcanal)->next = curentcanal; 
         for (i = 1; i <= 2; i++) 
         { 
            strcpy((*allcanal)->prtclnames[i-1],pname[i + 1-1]); 
            (*allcanal)->prtclmasses[i-1] = pmass[i + 1-1]; 
         } 
         compileprocess_(n_sub,&(*allcanal)->codeptr); 
      } 
   } 
   fclose(menuq); 
   if (*allcanal == NULL) 
   { 
      messanykey(10,10,
         " Set of calculated diagrams for this processis is empty$"); 
      *errcd = -1; 
      return;
   } 
   *errcd = 0; 
} 
