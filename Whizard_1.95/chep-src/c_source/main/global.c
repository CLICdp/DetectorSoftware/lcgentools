#include "tptcmac.h"
#include "syst2.h"
#include "global.h"

shortstr processch, hadr1ch, hadr2ch, sqrtsch, limpch;
integer  graphdriver, graphmode;

optionstp options;
char  fortname[3];
word  totdiag_f, totdiag_sq, deldiag_f,
             deldiag_sq, subproc_f, subproc_sq, calcdiag_sq;

char  modelmenu[STRSIZ];
int   maxmodel;
char  exitmenu[STRSIZ];

int nsub, ndiagr, n_model;
int  nin, nout, n_x;   /* Number of X-particles */
int errorcode;
