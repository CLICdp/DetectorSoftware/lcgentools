#include "tptcmac.h"
#include "syst2.h"
#include "pvars.h"
#include "global.h"
#include "parser.h"
#include "syst2.h"

#include "pre_read.h"

 preres pregarbage;

void  clearpregarbage(void)
{preres  p;

   while (pregarbage != NULL)
   {
      p = pregarbage->next;
      free(pregarbage);
      pregarbage = p;
   }
}


static void  rangecheck(preres q)
/* preres  q;*/
{byte    i;

		if ( q->degp > 255  || q->maxg > 20)
      {  rderrcode = rangecheckerror;
         return;
      }
      for (i = 1; i <= vardef->nvar; i++)
         if (q->varsdeg[i-1] > 127)
         {  rderrcode = rangecheckerror;
            return;
         }
}


static void  newrecord(preres* q)
/* preres  * q;*/
{byte      i;

   *q = pregarbage;
   while (*q != NULL && !(*q)->free) *q = (*q)->next;
   if (*q == NULL)
   {
      *q = (preres)getmem(sizeof(struct preresrecord));
      (*q)->next = pregarbage;
      pregarbage = *q;
   }

      (*q)->free = FALSE;
      (*q)->num = 0;
      (*q)->maxp = 0;
      (*q)->degp = 0;
      (*q)->g5 = FALSE;
      (*q)->maxg = 0;
      (*q)->indlist = setof(_E);
      for (i = 1; i <= idntmaxnmb; i++)
         (*q)->varsdeg[i-1] = 0;
}


pointer  rd(char* s)
/* char     * s;*/
{integer    ecd;
 byte       i;
 preres     m;
 long    li;

   if (isdigit(s[0]))
   {
      vall(s,&li,&ecd);
		if (ecd != 0 )
         rderrcode = toolargenumber;
      else
      {
         newrecord(&m);
         m->num = li;
         m->tp = numbertp;
      }
   }
   else
   {
      if (strlen(s) > 6)
         rderrcode = toolongidentifier;
      else
      {
         newrecord(&m);
         m->tp = polytp;

         if (strlen(s) == 2 && isdigit(s[1]) && s[1] != 0)
         {
            switch (s[0])
            {
               case 'p':
               case 'P':
                  m->tp = vectortp;
                  m->maxp = ord(s[1]) - ord('0');
                  m->degp = 1;
               break;

               case 'm':
                  m->tp = indextp;
                  m->indlist = setof(ord(s[1]) - ord('0'),_E);
                  break;
               case 'M':
                  m->tp = indextp;
                  m->indlist = setof(ord(s[1]) - ord('0') +4 ,_E);                  
            }   /*  Case  */

            if (strcmp(s,"G5") == 0)
            {
               m->tp = spintp;
               m->g5 = TRUE;
            }
         }

         if (m->tp == polytp)
         {
            i = 1;
            while (i <= vardef->nvar &&
                   strcmp(s,vardef->vars[i-1].name) != 0) ++(i);
            if (i > vardef->nvar)
               if (i > idntmaxnmb)
                  rderrcode = toomanyidentifiers;
               else
               {
                  vardef->nvar = i;
                  strcpy(vardef->vars[i-1].name,s);
               }
            m->varsdeg[i-1] = 1;
         }
      }
   }
   return (pointer)m;
}


pointer uact(char* ch,pointer mm)
/* char *   ch;
 pointer  mm;*/
{preres   m;

   m = (preres)mm;
   if (strcmp(ch,"G") == 0 || strcmp(ch,"g") == 0)
   {
      if (!inset(m->tp,setof(vectortp,indextp,_E)))
      {
         rderrcode = typemismatch;
         rderrpos = 3;
         return mm;
      }
      m->tp = spintp;
      m->maxg = 1;
   }
   else
   if (strcmp(ch,"-") == 0)
   {
      if (m->tp == indextp)
      {
         rderrcode = typemismatch;
         return mm;
      }
      m->num = -m->num;
   }
   else
      rderrcode = 14;
   return mm;
}


pointer  bact(char ch,pointer mm1,pointer mm2)
/* char      ch;
 pointer   mm1;
 pointer   mm2;*/
{preres    m1, m2, m3;
 byte      i;
 integer   d, k;

   m1 = (preres)mm1;
   m2 = (preres)mm2;
   switch (ch)
   {
      case '+':
         if (m1->tp < m2->tp) { m3 = m1; m1 = m2; m2 = m3; }
         if (m1->tp == indextp ||
             (m1->tp == vectortp && m2->tp != vectortp) ||
             (m2->tp > polytp    && m1->tp != m2->tp))
         {
            rderrcode = typemismatch;
            return NULL;
         }

         if (m1->indlist != m2->indlist)
         {
            rderrcode = indexuncompatibility;
            return NULL;
         }
         m1->num += m2->num;
         m1->maxp = MAX(m1->maxp,m2->maxp);
         m1->degp = MAX(m1->degp,m2->degp);
         m1->g5 = m1->g5 || m2->g5;
         m1->maxg = MAX(m1->maxg,m2->maxg);
         if (m1->tp == rationtp)
            for (i = 1; i <= vardef->nvar; i++)
               m1->varsdeg[i-1] += m2->varsdeg[i-1];
         else
            for (i = 1; i <= vardef->nvar; i++)
               m1->varsdeg[i-1] = MAX(m1->varsdeg[i-1],m2->varsdeg[i-1]);
      break;

      case '*':
         if (m1->tp < m2->tp) { m3 = m1; m1 = m2; m2 = m3; }
         if (m1->tp == indextp || (m2->tp > polytp && m1->tp != m2->tp))
         {
            rderrcode = typemismatch;
            return NULL;
         }
         if ((m1->indlist & m2->indlist) != setof(_E))
         {
            rderrcode = indexuncompatibility;
            return NULL;
         }
         m1->num *= m2->num;
         m1->maxp = MAX(m1->maxp,m2->maxp);
         m1->degp += m2->degp;
         m1->g5 = m1->g5 || m2->g5;
         m1->maxg += m2->maxg;
         for (i = 1; i <= vardef->nvar; i++)
            m1->varsdeg[i-1] += m2->varsdeg[i-1];
         m1->indlist |= m2->indlist;
      break;

      case '/':
         if (m2->tp > rationtp || m1->tp > rationtp || m1->tp == indextp)
         {
            rderrcode = typemismatch;
            return NULL;
         }

         m1->maxp = MAX(m1->maxp,m2->maxp);
         m1->degp += m2->degp;
         m1->g5 = m1->g5 || m2->g5;
         for (i = 1; i <= vardef->nvar; i++)
            m1->varsdeg[i-1] += m2->varsdeg[i-1];
         m1->tp = rationtp;
      break;

      case '^':
         d = m2->num;
         if (m2->tp != numbertp || d <= 0 || d > 255)
         {
            rderrcode = rangecheckerror;
            return NULL;
         }

         if (m1->tp > rationtp)
         {
            rderrcode = typemismatch;
            return NULL;
         }

         k = m1->num;
         for (i = 1; i <= d - 1; i++) m1->num *= k;
         m1->degp *= d;
         for (i = 1; i <= vardef->nvar; i++) m1->varsdeg[i-1] *= d;
      break;

      case '.':
         if (m1->tp < vectortp || m2->tp < vectortp)
         {
            rderrcode = typemismatch;
            return NULL;
         }
         m1->tp = m1->tp == vectortp && m2->tp == vectortp ?
            polytp : tenstp;
         if (m1->indlist != setof(_E) && m1->indlist == m2->indlist)
         {
            rderrcode = indexuncompatibility;
            return NULL;
         }
         else
            m1->indlist |= m2->indlist;
         m1->num *= m2->num;
         m1->maxp = MAX(m1->maxp,m2->maxp);
         m1->degp += m2->degp;
         m1->g5 = m1->g5 || m2->g5;
         m1->maxg += m2->maxg;
         for (i = 1; i <= vardef->nvar; i++)
            m1->varsdeg[i-1] += m2->varsdeg[i-1];
   } /*  Case  */

   m2->free = TRUE;
   rangecheck(m1);
   return (pointer)m1;
}
pointer  bactF(char ch,pointer mm1,pointer mm2)
{
  if (ch=='+')
  {  rderrcode = unexpectedoperation;
	  return NULL;
  }
  return bact(ch,mm1,mm2);
}
