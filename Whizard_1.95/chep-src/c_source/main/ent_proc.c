#include "tptcmac.h"
#include "syst2.h"
#include "global.h"
#include "crt.h"
#include "physics.h"
#include "crt_util.h"
#include "files.h"
#include "screen.h"
#include "file_scr.h"
#include "help.h"
#include "ent_proc.h"

#define ycons 19
#define errtxt "This particle is absent in the model"

static int errorcode1;


static byte seekcomma(char* txt,byte m)
{byte         i;

   for (i = m; i <= strlen(txt); i++)
      if (txt[i-1] == ',') return i;
   return (byte)strlen(txt) + 1;
}

static void error(char* txt)
{
   print("%s","Error: ");
   scrcolor(LightRed,Black);
   print("%s",txt);
   scrcolor(White,Black);
   be_be();
}

static void enter_h(char* name,hadron* hadr,char* hadrch)
{byte         m, mm, j, i, y0;
 shortstr     frgm;
 boolean      yn;
 integer      redres;

   if (strlen(name) == 0) { errorcode1 = -1; return; }

   j = 0;
   if (strlen(name) < 4) locateinbase(name,&j);
   if (!pseudop(j))
   {
      strcpy(hadr->name,name); hadr->parton[0] = j; hadr->how = 1;
      return;
   }

   y0 = where_y();
   print("Is  \'%s\' a composite particle ",name);
   yn = yesnokey();
   goto_xy(1,y0); clr_eol();
   if (!yn) { errorcode1 = -1; return; }

   if (strcmp(hadr->name,name) != 0)
   { strcpy(hadrch,""); strcpy(hadr->name,name); }
   m = 1;

label_1:
   hadr->how = 0;
	do { goto_xy(1,y0);
		  print("\'%s\'  consists of: ",hadr->name);
		  redres = str_redact(hadrch,m);
		}while (redres!=KB_ENTER &&  redres!=KB_ESC && redres!=KB_F1);
   /*  Help  */
	if (redres == KB_F1)
	{
		 showhelp(2);
		 goto label_1;
	}
   goto_xy(1,y0 + 1); clr_eol();
	if (redres == KB_ESC || strcmp(hadrch,"") == 0)
   {
      errorcode1 = -1;
      goto_xy(1,y0);
      clr_eol();
      return;
	}
   m = 1;
   while (m <= strlen(hadrch))
   {
      mm = seekcomma(hadrch,m);
      strcpy(frgm,copy(hadrch,m,mm - m));
      trim(frgm);
      j = 0;
      if (strlen(frgm) < 4) locateinbase(frgm,&j);
      if (pseudop(j))
      {  error(scat("unknown parton  \'%s\'",frgm));
         goto label_1;
      }
      for (i = 1; i <= hadr->how; i++)
         if (hadr->parton[i-1] == j)
         {  error(scat("duplicate parton  \'%s\'",frgm));
            goto label_1;
         }
      if (j != 0) hadr->parton[++hadr->how-1] = j;
      m = mm + 1;
   }
}

static void enter_lim(void)
{  shortstr     frgm, frgm2;
   byte         m, mm, n, j, k, y0;
   integer      redres;

   y0 = where_y();
   m = 1;

label_1:
   do { goto_xy(1,y0);
        print("%s","Exclude diagrams with ");
        redres = str_redact(limpch,m);
      }while (redres!=KB_ENTER &&  redres!=KB_ESC && redres!=KB_F1);

   goto_xy(1,y0 + 1); clr_eol();
      
   if (redres == KB_ESC) { errorcode1 = -1; return; }
   if (redres == KB_F1)  { showhelp(4); goto label_1;}
	
   nilprtcl(liminsp);
   if (strlen(limpch) == 0) { goto_xy(1,y0); clr_eol(); return; }
   m = 1;
   while (m <= strlen(limpch))
   {
      mm = seekcomma(limpch,m);
      strcpy(frgm,copy(limpch,m,mm - m));
      n = cpos('>',frgm);
      if (n == 0) k = 1;
      else
      {
         strcpy(frgm2,copy(frgm,n + 1,1));
         k = ord(frgm2[0]) - ord('0')+1;
         if ((k < 1) || (k > 9))
         {  error("digit is needed");
            m = n + 1;
            goto label_1;
         }
         strcpy(frgm,copy(frgm,1,n - 1));
      }
      trim(frgm);
      locateinbase(frgm,&j);
      if ((j == 0) || (strlen(frgm) > 3))
      {  error(errtxt);
         goto label_1;
      }
      addlim(liminsp,j,k);
      m = mm + 1;
   }
}


static void  prtcllist(int  key)
{
 char         fullname[STRSIZ];
 char         buf1[60],buf2[60],buf3[60],buf4[60];
 char         hlp[60];
 char         p1[60], p2[60];
 byte         i, j, pnum;
 linelist     ln;
 int  tabMax,tabSz;

 static int    nTot,nFirst;

	tabMax=ycons -7;
	if (key==0)
	{
		scrcolor(White,Black);
		for (i = 2; i <= 24; i++)
		{  goto_xy(1,i);
			clr_eol();
		}
		goto_xy(14,3);
		print("List of particles (antiparticles)");
		nTot=0;
		nFirst=1;
	}
	else
	{
		if (nTot <= 3 *tabMax )   return;
		switch (key)
		{
		  case KB_DOWN : nFirst+=3;         break;
		  case KB_UP   : nFirst-=3;         break;
		  case KB_PAGED: nFirst +=3*tabMax; break;
		  case KB_PAGEU: nFirst -=3*tabMax; break;
		}
		if (nFirst <1) nFirst=1;
		if (nTot-nFirst+3<3*tabMax )  nFirst=1+3*((nTot+2)/3) -3*tabMax;
		clrbox(1,4,79,5+tabMax);
	}
	goto_xy(3,5);
	i=0;
	ln=prtcls_tab.strings;
	while (ln != NULL)
	{  sscanf(ln->line,"%[^|]%*c%[^|]%*c%[^|]%*c%[^|]%*c%[^|]%*c%[^|]%*c%[^|]%*c%[^|]",
			 fullname,p1,p2,buf1,buf2,buf3,buf4,hlp);
		trim(p1);
		locateinbase(p1,&pnum);
		trim(hlp);
		if (prtclbase[pnum-1].top != NULL && strcmp(hlp,"*") != 0)
		{
			i++;
			if (i>=nFirst && (i-nFirst)/3 <tabMax )
			{
				print("%s",p1);
				if (strcmp(p1,p2) == 0) print("     "); else print("(%s)",p2);
				trim(fullname);
				print("- %s",fullname);
				j = i % 3;
				if (j == 0)	goto_xy(3,where_y() + 1);
						else	goto_xy(3 + 26 * j,where_y());
			}
		}
		ln=ln->next;
	}
	nTot=i;
	tabSz=MIN((nTot+2)/3,tabMax);
	scrcolor(White,Blue);
	chepbox(1,4,79,5+tabSz);

	if (nFirst >1 ) { goto_xy(72,4); print("PgUp");  }

	if (nFirst+3*tabSz <= nTot    ) { goto_xy(72,5+tabMax); print("PgDn");  }

	scrcolor(White,Black);

}



void enter(int * errorcode)
{shortstr   frgm, frgm2;
 byte       m, mm, n, j, i, y0, w_y  ;
 int    redres;

   errorcode1 = *errorcode;
   scrcolor(White,Blue);
   /* goto_xy(23,25);Write(' Use F1 for help  and  Esc to exit '); */
   scrcolor(White,Black);
   m = 1;
	prtcllist(0);

label_1:
   y0 = ycons;
   errorcode1 = 0;
   goto_xy(1,y0); print("%s","Enter  process: ");
	redres = str_redact(processch,m);
	goto_xy(1,y0 + 1); clr_eol();
   switch (redres)
   {
	case KB_PAGED:
	case KB_PAGEU:
	case KB_UP:
	case KB_DOWN:
		prtcllist(redres);
		goto label_1;
	case KB_F1:   /*  Help  */

		 showhelp(1);
		 goto label_1;


	case KB_ESC:    /*  Esc  */
					clrbox(1,2,80,24);
               errorcode1 = -1;
               *errorcode = errorcode1;
               return;
   }   /*  Case  */
   n = spos("->",processch);
   if (n == 0) { error("\'->\' is absent "); goto label_1; }

                   /*   Out Particles  */
   n_x = 0;
   m = n + 2;
   nilprtcl(inclp);
   while (m <= strlen(processch))
   {
      mm = seekcomma(processch,m);
      strcpy(frgm,copy(processch,m,mm - m));
      trim(frgm);
      j = 0;
      if (strlen(frgm) < 4) locateinbase(frgm,&j);
      if (pseudop(j))
         if (strlen(frgm) == 3 && frgm[1] == '*' &&
             (frgm[2] == 'X' || frgm[2] == 'x')  &&
             isdigit(frgm[0]))
            n_x += ord(frgm[0]) - ord('0');
         else { error(errtxt); goto label_1; }
      else addprtcl(inclp,j);
      if (n_x >= 4) { error("too many \'X\' particles"); goto label_1; }
      m = mm + 1;
   }
   nout = n_x;

   j = 1;
   while (inclp[j-1].who != 0)
      nout = nout + inclp[(j++)-1].how;
   if (nout < 2) { error("not enough out-particles"); goto label_1; }

                  /*      IN PARTICLES        */
   m = 1;
   mm = seekcomma(processch,1);
   if (mm >= n) mm = n;
   strcpy(frgm,copy(processch,1,mm - 1));
   trim(frgm);
   if (mm > n - 2)
   {
      nin = 1;
      hadr1.how = 1; hadr2.how = 0;
      j = 0;
      if (strlen(frgm) < 4) locateinbase(frgm,&j);
      if (pseudop(j)) { error(errtxt); goto label_1; }
      else hadr1.parton[0] = j;
   }
   else
   {
      nin = 2;
      enter_h(frgm,&hadr1,hadr1ch);
      if (errorcode1 != 0) { error(errtxt); goto label_1; }
      strcpy(frgm2,copy(processch,mm + 1,n - mm - 1));
      if (cpos(',',frgm2) != 0)
          { error("too many in-particles"); goto label_1; }
      trim(frgm2);
      if (strcmp(frgm2,frgm) == 0) hadr2 = hadr1;
      else enter_h(frgm2,&hadr2,hadr2ch);
      if (errorcode1 != 0)
      {
         goto_xy(1,y0 + 1); clr_eol();
         error(errtxt);
         m = mm + 1;
         goto label_1;
      }
   }
   if (nout + nin > MAXINOUT)
      { error("too many out-particles"); goto label_1; }

   missingmass = 0;
   j = 1;
   while (inclp[j-1].who != 0)
   {
      missingmass -= inclp[j-1].how * prtclbase[inclp[j-1].who-1].mass;
      j++;
	}

   if (nin == 2)
   {
      m = 1;
      y0 = where_y();

	label_2:
		do	{  goto_xy(1,y0); print("%s","Enter  Sqrt(S) in GeV : ");
				redres = str_redact(sqrtsch,m);
			}	while (redres!=KB_ENTER &&  redres!=KB_ESC && redres!=KB_F1);
      goto_xy(1,y0 + 1); clr_eol();
		if (redres == KB_F1)   /*  F1 -pressed */
		{	showhelp(3);
			goto label_2;
		}
		if (redres == KB_ESC)   /*  Esc   pressed  */
		{
			w_y=where_y();
			for (j = ycons; j <= w_y; j++) { goto_xy(1,j); clr_eol(); }
         goto label_1;
      }
/*      vald(sqrtsch,&sqrts,&errorcode1); */
      sqrts=realVal(sqrtsch,&errorcode1);
      if (errorcode1 != 0)
         { error("incorrect number"); goto label_2; }
      if (missingmass + sqrts <= 1.0E-5)
         { error("energy is too low"); goto label_2; }
      for (i = 1; i <= hadr1.how; i++)
         for (j = 1; j <= hadr2.how; j++)
            if (sqrts <= (prtclbase[hadr1.parton[i-1]-1].mass +
                          prtclbase[hadr2.parton[j-1]-1].mass))
               { error("energy is too low"); goto label_2; }

      missingmass += sqrts;
   }
   else   /*  Case of Decay  */
   {
      missingmass += prtclbase[hadr1.parton[0]-1].mass;
      if (missingmass <= 0)
           { error("this decay mode is forbidden"); goto label_1; }
   }

   y0 = where_y();
                  /*  Enter Limits  */

/*label_3: */
   goto_xy(1,y0);
   enter_lim();
   if (errorcode1 != 0)
	{
		w_y=where_y();
		for (j = ycons; j <= w_y; j++) { goto_xy(1,j); clr_eol(); }
      goto label_1;
    }
/*	goto_xy(1,23);print("missingmass= %lf",missingmass); inkey(); */
	goto_xy(1,24); clr_eol();
   print("%s","0      diagrams are constructed");
   *errorcode = errorcode1;
}

