#include "tptcmac.h"
#include "syst2.h"
#include "crt.h"
#include "global.h"
#include "physics.h"
#include "pvars.h"
#include "sos.h"
#include "files.h"
#include "ghosts.h"
#include "rfactor.h"
#include "polynom.h"
#include "crt_util.h"

#include "saveres.h"

 poly    rnum;
 poly    factn, factd;
 poly    denr[2 * maxvert - 2];
 byte    denrdeg[2 * maxvert - 2];
 byte    denrwidth[2 * maxvert - 2][2];
 pointer varrnum, varfact, varden;
 byte    denrno;
 byte    monsize;

static  byte  byte_buf[1023];
static  word  buf_pos=0;
static void wAbort(void)
{
      saveent(nsub,ndiagr,n_model,1);
      messanykey(5,20,"No free disk space$");
      finish();
      exit(0);  /*  End of work  */
}


static void clearbuf(void)
{
    f_write(byte_buf, buf_pos ,(size_t)1,archiv);
    buf_pos =0;
}

static void write_byte(byte  bt)
{
	byte_buf[buf_pos] =bt;
	buf_pos++;
	if (buf_pos > 1023) clearbuf();
}


static void  savepoly(poly p)
{poly     p_;
 byte     deg, n, k, nv;
 int i;
 byte  * bytePtr;
 word ww;

   p_ = p;   /*  writing polynom Length as word */
   ww = 0;   
   while (p_ != NULL) { p_ = p_->next; ++(ww); }
   bytePtr =(byte *)  &(ww);

   for (i=0;i<sizeof(ww);i++) write_byte(bytePtr[i]); 

   if (p == NULL) return;

   nv = vardef->nvar;

   p_ = p;

   while (p_ != NULL)
   {  bytePtr=(byte*) &( p_->coef.num);
      for (i=0;i<sizeof(p_->coef.num);i++) write_byte(bytePtr[i]); 

      /*  writing polynom coef as LONGINT */
      for (n = 1; n <= nv; n++)
      {  /*  writing  vars as BYTE    */
         deg = (p_->tail.power[vardef->vars[n-1].wordpos-1] /
                vardef->vars[n-1].zerodeg) %
                vardef->vars[n-1].maxdeg;
         for (k = 1; k <= deg; k++)
            write_byte(vardef->vars[n-1].num);
      }

      n = 254;
      write_byte(n);   /*  writing end monom  marker  */
      p_ = p_->next;
   }
}


void  saveanaliticresult(void)
{catrec      cr;
 byte        npoly;
 byte        i,bt;
 int k,m;
 byte  * bytePtr;

 word ww;
 long ll;


   diskerror = wAbort;

   cr.nsub_ = nsub;
   cr.ndiagr_ = ndiagr;
   clearbuf();
   
   cr.factpos = ftell(archiv);
   npoly = 2;
   write_byte(npoly);
   vardef++;
   savepoly(factn);
   savepoly(factd);

   clearbuf();
   cr.rnumpos = ftell(archiv);

   npoly = 1;
   write_byte(npoly);

   vardef--;
   savepoly(rnum);
   clearbuf();
   cr.denompos = ftell(archiv);
   
   vardef++; vardef++;
   write_byte(denrno);   /*  number of demominatirs  */
   for (i = 1; i <= denrno; i++)
   {
      write_byte(denrdeg[i-1]);   /*  power  1 or 2  */

      /*  image Part writing  */
      for (m=1; m>=0;m--)
      {
          ww = denrwidth[i-1][m] == 0 ? 0 : 1;
          bytePtr=(byte*)&(ww);
          for (k=0;k<sizeof(ww);k++) write_byte(bytePtr[k]);
          bt = 254;
          if (ww != 0)
          {  ll = 1;
             bytePtr=(byte*) &(ll);
             for (k=0;k<sizeof(ll);  k++) write_byte(bytePtr[k]);
             write_byte(denrwidth[i-1][m]);
             write_byte(bt);
          }
      }
      savepoly(denr[i-1]);
   }
   clearbuf();
   FWRITE1(cr,catalog);
   diskerror = NULL;
}
