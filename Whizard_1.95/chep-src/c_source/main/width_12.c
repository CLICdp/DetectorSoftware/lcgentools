#include <math.h>
#include "tptcmac.h"
#include "syst2.h"
#include "crt.h"
#include "global.h"
#include "physics.h"
#include "screen.h"
#include "files.h"
#include "crt_util.h"
#include "optimise.h"
#include "procvar_.h"
#include "n_compil.h"
#include "num_tool.h"
#include "num_serv.h"

#include "width_12.h"

#ifndef M_PI
#define M_PI		3.14159265358979323846
#endif


/*
      Made by      S.Shichanin
      at           03 April 1991
      Last update  26 June  1991
*/ 


static double  totcoef; 


static canalptr     allcanal; 
static double         width12, mm1, mm2, mm3; 


static void  sortcanals(void)
{canalptr    ptmp; 
 canal       ctmp;
 integer     i; 
 canalptr    p1, p2; 

   do 
   { 
      ptmp = allcanal; 
      i = 0; 
      while (ptmp->next != NULL) 
         if (ptmp->width >= ptmp->next->width)
            ptmp = ptmp->next;
         else 
         { 
            ++(i); 
            p1 = ptmp; 
            p2 = ptmp->next; 
            ctmp = *p1; 
            *p1 = *p2; 
            *p2 = ctmp; 
            p2->next = p1->next; 
            p1->next = ctmp.next;
         } 
   }  while (i != 0); 
} 


static void  decay12information(void)
{byte        xcount, ycount; 
 canalptr    curentptr; 

	clrbox(1,16,80,24);
	goto_xy(1,16);
   scrcolor(White,Black); 
   print(" Total width : "); 
   scrcolor(Yellow,Black); 
   if (err_code == 0)
		print("%13lf GeV",width12);
   else
      print(" incorect        "); 
   if (width12 > 1.E-20 && err_code == 0) 
   {
      sortcanals(); 
		goto_xy(1,17);
      scrcolor(White,Black);
      print(" Modes and fractions :"); 
      xcount = 31; ycount = 18; 
      curentptr = allcanal; 
      while (curentptr != NULL) 
      {
         if (ycount <= 25)
         {
				goto_xy(xcount,ycount-1);
            scrcolor(White,Black);
            print("%s %s -  ",curentptr->prtclnames[0],
                               curentptr->prtclnames[1]);
            /*   GotoXY(xCount+7,yCount); */
            scrcolor(Yellow,Black); 
				print("  %5.2lf%%",100 * curentptr->width / width12);
            xcount += 30; 
            if (xcount > 75)
            {  xcount = 1;
               ++(ycount); 
            } 
         } 
         curentptr = curentptr->next; 
      } 
   } 
   scrcolor(White,Black); 
} 


static void  calcwidth12(void)
{canalptr    curentcanal; 
 byte        i; 

   err_code = 0; 
   width12 = 0.0; 
   calcfunctions(&err_code); 
   if (err_code != 0)
   {  errormessage();
      return;
   } 
   calcconstants(); 
   curentcanal = allcanal; 
   while (curentcanal != NULL && err_code == 0) 
   {
      for (i = 1; i <= 2; i++)  pmass[i + 1-1] = curentcanal->prtclmasses[i-1]; 
      for (i = 1; i <= nin + nout; i++) 
      if (*pmass[i-1] < 0) 
      {  err_code = 3; 
         messanykey(10,10,"Kinematical Error$"); 
         return;
      } 
      if (*pmass[0] <= *pmass[1] + *pmass[2]) curentcanal->width = 0.0; 
      else 
      { 
         mm1 = *pmass[0] * *pmass[0]; 
         mm2 = *pmass[1] * *pmass[1];
         mm3 = *pmass[2] * *pmass[2]; 

         totcoef = 1. / (16. * M_PI * mm1 * *pmass[0]) * 
                   sqrt((mm1 - (mm2 + mm3 + 2. * *pmass[1] * *pmass[2])) * 
                   (mm1 - (mm2 + mm3 - 2. * *pmass[1] * *pmass[2]))); 

         (*vararr)[sp_pos(1,2)].tmpvalue =  ( mm1 - mm3 + mm2)/2.0;  /* p1.p2 */
         (*vararr)[sp_pos(1,3)].tmpvalue =  ( mm1 + mm3 - mm2)/2.0;  /* p1.p3 */
         (*vararr)[sp_pos(2,3)].tmpvalue =- (-mm1 + mm3 + mm2)/2.0;  /* p2.p3 */
 
          curentcanal->width = totcoef * matrixelement(curentcanal->codeptr); 
      }
      width12 += curentcanal->width; 
      curentcanal = curentcanal->next;
   } 
   if (err_code != 0)
      errormessage(); 
} 


static double  totwidth(void)
{
   calcwidth12(); 
   return width12;
} 


void  decay12(void)
{marktp   heapbg; 
 int  k;
 void * pscr=NULL; 
  
   /*  Decay12 */
   mark_(&heapbg); 
   loadvars(0); 
   compileall(&allcanal,&err_code); 
   if (err_code != 0) 
   { 
      release_(&heapbg); 
      return;
   } 

   k = 1;
   calcwidth12(); 
   decay12information(); 
   do 
   {
      menu1(56,6,"","\026"
         " View/change data     "
         " Parameter dependence ","h_00653*",&pscr,&k);
      switch (k)
      {
         case 1:     /*  ******* View/change data ********  */
            if (changedata())
            {
               calcwidth12();
               decay12information();
            }
         break;

         case 2:
	   paramdependence(totwidth,processch,"Width  [GeV]");
      }   /*  switch  */
   }  while (k != 0);
	release_(&heapbg);
	clrbox(1,16,80,24);
}
