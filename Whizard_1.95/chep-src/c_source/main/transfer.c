#include "tptcmac.h"
#include "syst2.h"
#include "global.h"
#include "physics.h"

#include "transfer.h"


/*  transformation of diagr representation  */ 

#define invert 0       /*# of prev. vert for in particle           */ 
#define intrvert 254   /*# of next. vert for intermediate particle */ 
#define vacantvert 252 /*# of vacant vert                          */ 
/* outVert = 255; */   /*# of next. vert for out particle          */ 
#define notassignmom 0 /* from labelmom */


    typedef struct vampl { 
                 /*  how many verts  */ 
               byte         size, outno;   /*  how many outgoing edges  */ 
               vert0        vertlist[maxvert];   /*  array of verts  */ 
               vertlink     outer[MAXINOUT];   /*  adresses of external edges */ 
            } vampl; 
            
static integer      initvno;             /*  from mkverts       */

static vampl *      vlist;               /*  from mkverts       */

static adiagram     diag;                /*  from mkverts       */

static byte         vvalence[maxvert];   /*  first free valence */ 
                                         /*  from mkverts       */
static char         decayclock;          /*  decay level ( from Pukhov) */ 
                                         /*  from mkverts       */
static byte         i, j, j0, k, yetin;  /*  from mkverts       */
                       /*  how many in-edges not processed yet  */ 
                       
static csdiagram *  diagr;               /* from mkcsections */

static byte         mylnk[MAXINOUT];     /* from mkcsections */
                                         
static vampl        va1, va2;            /* from mkcsections */

static vcsect *     vcs2;                    /* from labelmom */
static byte         ie, ke, momcnt, lorcnt; /* from labelmom */
static byte         valn;                   /* from labelmom */
static byte         outlst[MAXINOUT];       /* from labelmom */



static byte         hasmaxvalence(vert0 vrt)
/* vert0 vrt;*/
{ byte         count;

  count = maxvalence;
  while ((count > 0) && (vrt[count-1].nextvert.vno == nullvert))
      --(count); 
  return count; 
} 

static byte        nextv(byte v)
/* byte v;*/
  /*  seeks backward first unused vertex  */ 
{ 
 while (!((vvalence[v-1] < maxvalence) || 
        (vlist->vertlist[j0-1][maxvalence-1].nextvert.vno == 
         vacantvert)) && (v > 1)) --(v); 
  return v; 
}   /* nextV */ 

static void         inwardedge(void)
  /*   Processing inWard edge in aDiagram  */ 
  { 
    --(decayclock); 
    /* **** Expanded  Vertex ****  */ 
      { edgeinvert *with1 = &vlist->vertlist[j0-1][vvalence[j0-1]-1]; 
         with1->partcl = -diag[i-1];   /*  out of vertex  */ 
         with1->prop = setof(_E); 
         with1->nextvert.vno = j + initvno - 1; 
         with1->nextvert.edno = vvalence[j-1]; 
      } 
        /*  With  */ 
    /*  *** next     Vertex ****  */ 
      { edgeinvert *with1 = &vlist->vertlist[j-1][vvalence[j-1]-1]; 
        with1->partcl = prtclbase[-diag[i-1]-1].anti;   /*  into vertex  */ 
        with1->prop = setof(_E); 
        with1->nextvert.vno = j0 + initvno - 1; 
        with1->nextvert.edno = vvalence[j0-1]; 
      } 
      /*  With  */ 
    ++(vvalence[j0-1]); 
    ++(vvalence[j-1]); 
    j0 = j;   /*  next vert to be expanded  */ 
    ++(j);   /*  next  */ 
}   /* inword edge */ 


static void         outwardedge(void)
/*   Processing outWard edge in aDiagram  */ 
{ 
    ++(decayclock); 
    /* **** Expanded  Vertex ****  */ 
    { edgeinvert *with1 = &vlist->vertlist[j0-1][vvalence[j0-1]-1]; 
       with1->partcl = diag[i-1]; 
       if (yetin > 0) 
         { 
           --(yetin); 
           with1->prop = setof(inp,_E); 
           vlist->outer[k-1].vno = j0 + initvno - 1; 
           vlist->outer[k-1].edno = vvalence[j0-1]; 
           ++(k); 
           with1->nextvert.vno = invert; 
           with1->nextvert.edno = 0; 
         } 
       else 
         {   /*  intr vertex  */ 
           /* *** pointer to intr edge ***  */ 
           vlist->outer[k-1].vno = j0 + initvno - 1; 
           vlist->outer[k-1].edno = vvalence[j0-1]; 
           ++(k); 
           /* - - - - - - - - -   -  - - -  */ 
           with1->prop = setof(intrp,_E); 
           with1->nextvert.vno = intrvert; 
           with1->nextvert.edno = 0; 
         }   /* else */ 
      } 
     /* With */ 
   ++(vvalence[j0-1]); 
   j0 = nextv(j0); 
}   /*  outwardEdge  */ 


static void         auxedge(void)
/*   Processing auxiliarry edge in aDiagram  */ 
{ 
    --(decayclock); 
    vlist->vertlist[j0-1][maxvalence-1].nextvert.vno = vacantvert; 
    /*  to indicate 4-vertex  */ 
}   /*  auxEdge  */ 


  /*  ************************** Main program ******************************* */ 

  /*  if TRUE then conjugate diagr  */ 
  /*# of first vertex  */ 
                    /*  from Pukhov        */ 
                  
static void         mkverts(boolean conj,integer initvno1,
                                    adiagram diag1,vampl* vlist1)
/* boolean conj;
 integer initvno1;
 adiagram diag1;
 vampl * vlist1; */ 
  /*  output             */ 
  /*    Makes vertex-oriented structure for amplitude diagram         */ 

{    
     /*  edge counter  */ 
     /*  new vertex counter  */ 
     /*# of expanded vertex  */ 
     /*  counter for intr verteces  */ 


   /* Nested function: nextv */ 

   /* Nested function: inwardedge */ 

   /* Nested function: outwardedge */ 

   /* Nested function: auxedge */ 
   
   initvno = initvno1;
   vlist = vlist1;
   lvcpy(diag, diag1);

     for (j = 1; j <= maxvert; j++) 
       { 
         vvalence[j-1] = 1; 
         vlist->vertlist[j-1][maxvalence-1].nextvert.vno = nullvert; 
         vlist->vertlist[j-1][maxvalence-1].partcl = 0; 
       } 

     if (conj)   /*  conjugate diagramm  */ 
       { 
         decayclock = 0; 
         i = 1; 
         do { 
           if (diag[i-1] > 0) 
             { 
               diag[i-1] = prtclbase[diag[i-1]-1].anti; 
               ++(decayclock); 
             } 
           else if (diag[i-1] < 0) 
             { 
               diag[i-1] = -prtclbase[-diag[i-1]-1].anti; 
               --(decayclock); 
             } 
                else --(decayclock); 
            ++(i); 
          }  while (!(decayclock > 0)); 
        }   /*  If  */ 

     decayclock = -1;   
     /*  for checking the end of aDiagram must be 1  */ 
     i = 1;   /*  edge counter  */ 
     j0 = 1;   /*  vertex to be expanded  */ 
     j = 2; 
     k = 1; 
     yetin = nin - 1;   /*  counter for in particles  */ 

  { edgeinvert *with1 = &vlist->vertlist[j0-1][vvalence[j0-1]-1];   
  /*  process 1st edge  */ 
    with1->partcl = prtclbase[-diag[i-1]-1].anti; 
    with1->prop = setof(inp,_E); 
    with1->nextvert.vno = invert; 
    with1->nextvert.edno = 0; 
    vlist->outer[k-1].vno = j0 + initvno - 1; 
    vlist->outer[k-1].edno = vvalence[j0-1]; 
    ++(k); 
    ++(i); 
    ++(vvalence[j0-1]); 
     } 
    /*  With  */ 

  /*  *** main cycle *********  */ 
   do { 
    if (diag[i-1] < 0) inwardedge(); 
    else if (diag[i-1] > 0) outwardedge(); 
    else if (diag[i-1] == 0) auxedge(); 
    ++(i); 
   }  while (!(decayclock > 0)); 
   vlist->outno = k - 1; 
   vlist->size = j - 1; 
}   /* =============================================mkVerts =========== */ 



static void         mkmylnk(void)
  /*  for compatiability with Pukhov's lnk  */ 
{ byte         i; 

  mylnk[0] = 1; 
  if (nin == 1) 
    for (i = 2; i <= va1.outno; i++) 
      mylnk[i-1] = diagr->lnk[i - 1-1] + 1; 
  else 
    { 
      mylnk[1] = 2; 
      for (i = 3; i <= va1.outno; i++) 
           mylnk[i-1] = diagr->lnk[i - 2-1] + 2; 
    } 
}   /*  mkMyLnk  */ 


static void         update(void)
   /*  gluing external edges of amplitudes  */ 
{ byte         i;   /*  counter of external edges  */ 

   /*  ... updating left part ...  */ 
   for (i = 1; i <= va1.outno; i++) 
      va1.vertlist[va1.outer[i-1].vno-1]
                  [va1.outer[i-1].edno-1].nextvert = 
      va2.outer[mylnk[i-1]-1];   
   /*  ... updating right part ...  */ 
   for (i = 1; i <= va2.outno; i++) 
      { 
        va2.vertlist[va2.outer[mylnk[i-1]-1].vno - va1.size-1]
                    [va2.outer[mylnk[i-1]-1].edno-1].nextvert = 
        va1.outer[i-1]; 
      } 
}   /*  Update  */ 


  /*  ******* Main programm *****  */ 

static void  mkcsections(csdiagram* diagr1,vcsect* vcs)
/* csdiagram * diagr1;
 vcsect * vcs;*/
  /*  constructs vertex-oriented strukture for cross sektion diagramm  */ 
{ 
  /*  26.02.90   */ 

   byte         i; 

   /* Nested function: mkverts */ 

   /* Nested function: mkmylnk */ 

   /* Nested function: update */ 

   diagr =diagr1;

   mkverts(FALSE,1,diagr->dgrm1,&va1); 
   mkverts(TRUE,va1.size + 1,diagr->dgrm2,&va2); 
   mkmylnk();   /*  compatiability with Pukhov lnk field  */ 
   update();   /*  updates external edges  */ 
   for (i = 1; i <= va1.size; i++) 
      lvcpy(vcs->vertlist[i-1], va1.vertlist[i-1]); 
   for (i = va1.size + 1; i <= va1.size + va2.size; i++) 
      lvcpy(vcs->vertlist[i-1], va2.vertlist[i - va1.size-1]); 
   vcs->sizel = va1.size; 
/*     sizeR:=vA2.size; */ 
   vcs->sizet = vcs->sizel + va2.size; 
   vcs->symnum = diagr->mult; 
   vcs->symdenum = diagr->del; 
   for (i = 1; i <= vcs->sizet; i++) 
      vcs->valence[i-1] = hasmaxvalence(vcs->vertlist[i-1]); 
}   /* ====================================== mkCSections=======  */ 

/*  $I transfer\label.pas */ 

  /*    labelling diagramm with momentums       */ 
  /*    This procedures labels cross-section diagramm with               */ 
  /*    momenta and lorentz ind.                                         */ 
  /*    Momenta coded with numbers stored in                             */ 
  /*       vCSect.vertList[vertex,edge].moment                           */ 
  /*    Positive number means outgoing momentum, negative - ingoing.     */ 
  /*    Linear relations are stored in vCSect.linear[vertex]             */ 
  /*    as array of momenta with zero sum. Independent - in 1st place    */ 
  /*    Linear relations must be supplemented with relation, describing  */ 
  /*    dependence of external momenta of the amplitude.                 */ 
  /*    Lorentz ind coded with bytes are stored in                       */ 
  /*     vCSect.vertList[vertex,edge].lorentz                            */ 
  /* ******************* A.Taranov    24.07.89 ------------------------- */ 

static void markindmom(void) /* marks edge.prop with independentMom property */ 
{byte      /* 1..6 */ i; 
 byte      /* 1..4 */ k; 

   for (i = 1; i <= vcs2->sizel; i++) 
      { 
         valn = hasmaxvalence(vcs2->vertlist[i-1]); 
         for (k = 1; k <= valn; k++) 
            if ((setof(inp,intrp,_E) & vcs2->vertlist[i-1][k-1].prop) != 
                setof(_E)) 
               vcs2->vertlist[i-1][k-1].prop |= setof(independentmom,_E); 
      } 
}  /*  markIndMom  */ 



static void         assignmom(void)
{ 
 vertlink     next; 
    
   if (vcs2->vertlist[ie-1][ke-1].moment == notassignmom) 
   { 
      next = vcs2->vertlist[ie-1][ke-1].nextvert;   /*  other end of line  */ 
      vcs2->vertlist[ie-1][ke-1].moment = momcnt;   /*  new momentum assigned  */ 
      vcs2->vertlist[next.vno-1][next.edno-1].moment = -momcnt; 
      ++(momcnt); 
   } 
}   /* assignMom */ 


static void         assignoutmom(void)
{ 
 byte         momcnt, nprtcl; 
 vertlink     next; 

   if (vcs2->vertlist[ie-1][ke-1].moment == notassignmom) 
   { 
      nprtcl = vcs2->vertlist[ie-1][ke-1].partcl; 
      momcnt = 1; 
      while (outlst[momcnt-1] != nprtcl) ++(momcnt);
      outlst[momcnt-1] = 0;
      momcnt += nin; 
      next = vcs2->vertlist[ie-1][ke-1].nextvert;   /*  other end of line  */ 
      vcs2->vertlist[ie-1][ke-1].moment = momcnt;   /*  new momentum assigned  */ 
      vcs2->vertlist[next.vno-1][next.edno-1].moment = -momcnt; 
   } 
}   /* assignOutMom */ 

static void         assignlor(void)
/*  only for photon propagator !!!!!  */ 
{ 
   vertlink     next; 

  if (vectorp(vcs2->vertlist[ie-1][ke-1].partcl))   
  /*  if PhotonP(vcs2.vertList[ie,ke].partcl) then */ 
            { 
              next = vcs2->vertlist[ie-1][ke-1].nextvert; 
              if (next.vno > ie) 
                { 
                  vcs2->vertlist[ie-1][ke-1].lorentz = lorcnt; 
                  vcs2->vertlist[next.vno-1][next.edno-1].lorentz = lorcnt; 
                  ++(lorcnt); 
                } 
            } 
/*            else
            begin
              vcs2.vertList[ie,ke].lorentz:=lorCnt;
              ++(lorCnt)
            end 
*/ 
}   /*  assignLor  */ 

  /* ********************main program ************************************** */ 

static void  labelmom(vcsect* vcs1)
/* vcsect * vcs1;*/
{  byte ii,kk;


   /* Nested function: markindmom */ 

   /* Nested function: assignmom */ 

   /* Nested function: assignoutmom */ 

   /* Nested function: assignlor */ 
   
   vcs2 = vcs1;

   for (ie = 1; ie <= vcs2->sizet; ie++)   /*  initialize  */ 
   { 
      for (ke = 1; ke <= maxvalence; ke++) 
      { 
         vcs2->vertlist[ie-1][ke-1].moment = notassignmom; 
         vcs2->vertlist[ie-1][ke-1].lorentz = 0; 
      } 
   } 
   markindmom();   /*  mark independent moments   */ 
   /* --- assign in momentums---- */ 
   momcnt = 1; 

   for (ie = 1; ie <= vcs2->sizel; ie++) 
   {
      valn = hasmaxvalence(vcs2->vertlist[ie-1]);
      for (ke = 1; ke <= valn; ke++)
         if (inset(inp,vcs2->vertlist[ie-1][ke-1].prop))
            assignmom();
   }
   /* ------------------ make list out particles ---------------------- */
	for (ie = 1; ie <= MAXINOUT; ie++) outlst[ie-1] = 0;   /*  for testing  */
   for (ie = 1; ie <= vcs2->sizel; ie++)
   {
      valn = hasmaxvalence(vcs2->vertlist[ie-1]);
      for (ke = 1; ke <= valn; ke++)
         if ( inset(intrp,vcs2->vertlist[ie-1][ke-1].prop) &&
             !inset(inp,vcs2->vertlist[ie-1][ke-1].prop))
         {  outlst[momcnt - nin-1] = vcs2->vertlist[ie-1][ke-1].partcl;
            ++(momcnt);
         }
   }
   /* ---------------- sort List out particles -------------------------- */
   ie = 1;
   while (ie < nout)
   {
      if (outlst[ie-1] <= outlst[ie + 1-1]) ie++;
      else
      {
         ke = outlst[ie-1];
         outlst[ie-1] = outlst[ie + 1-1];
         outlst[ie + 1-1] = ke;
         if (ie > 1) ie--; 
      } 
   } 
   /* ------------------ assign out momentum ----------------------------- */ 

   for (ie = 1; ie <= vcs2->sizel; ie++) 
   { 
      valn = hasmaxvalence(vcs2->vertlist[ie-1]); 
      for (ke = 1; ke <= valn; ke++) 
         if (inset(intrp,vcs2->vertlist[ie-1][ke-1].prop)) 
            assignoutmom(); 
   } 
   /*  ------------------- assign rest ---------------------------------- */ 
   lorcnt = 1; 
   for (ie = 1; ie <= vcs2->sizet; ie++) 
   { 
      valn = hasmaxvalence(vcs2->vertlist[ie-1]); 
      for (ke = 1; ke <= valn; ke++) 
      { 
         assignmom(); 
         assignlor(); 
      } 
   } 
   for (ke = 1; ke <= vcs2->sizel; ke++) 
      for (ie = 1; ie <= vcs2->valence[ke-1]; ie++) 
         if (inset(inp,vcs2->vertlist[ke-1][ie-1].prop)) 
         { 
            vcs2->vertlist[ke-1][ie-1].moment = 
               -vcs2->vertlist[ke-1][ie-1].moment; 
            kk = vcs2->vertlist[ke-1][ie-1].nextvert.vno; 
            ii = vcs2->vertlist[ke-1][ie-1].nextvert.edno; 
            vcs2->vertlist[kk-1][ii-1].moment = 
               -vcs2->vertlist[kk-1][ii-1].moment; 
         } 
}   /* ================================================== labelMom======== */ 


void         transfdiagr(csdiagram* diag,vcsect* vcs)
/* csdiagram * diag;
 vcsect * vcs;*/
{ /*byte         v, i, vv, ii; */

   mkcsections(diag,vcs);     /*  make cross section diagramm  */ 
                               /*  initialize vcs */ 
                               /*  contained in PUKH2TAR  */ 
   labelmom(vcs);              /*  label it with moments & Lorentz ind  */ 
                               /*  contained in LABEL  */ 

}   /*   transfDiagr  */ 



