#include "tptcmac.h"
#include "syst2.h"
#include "global.h"
#include "pvars.h"
#include "polynom.h"
#include "tensor.h"
#include "spinor.h"
#include "parser.h"
#include "sos.h"
#include "reader0.h"

#include "reader2.h"

#ifdef STRACE
#include "test_wrt.h"
#endif

boolean  r_reading2 = FALSE;


pointer   rd_(char* s)
{integer  ecd;


 byte     i;
 poly     p, ans;

   newmonom(&p);

   ans = plusone();
   p->next = ans;
   if (isdigit(s[0]))
   {
      p->coef.num = numbertp;
      vall(s,&ans->coef.num,&ecd);
   }
   else
   {
      p->coef.num = polytp;
      if (strlen(s) == 2 && isdigit(s[1]) && s[1] != 0)
      {
         i = ord(s[1]) - ord('0');
         switch (s[0])
         {
            case 'p':
            case 'P':
               p->coef.num = vectortp;
               ans->tail.tens[0] = -abs(momsubst[i-1]);
               ans->coef.ptr = plusone();
               if (momsubst[i-1] < 0)
                  (ans->coef.ptr)->coef.num = -1;
            break;

            case 'm':
            case 'M':
               p->coef.num = indextp;
               ans->tail.tens[0] = indsubst[i-1];
               if ( s[0] == 'M') ans->tail.tens[0]--;
               if (ans->tail.tens[0] == 1)
                  ans->tail.tens[0] = 0;
               else
                  ans->tail.tens[ans->tail.tens[0]-1] = 1;
               ans->coef.ptr = plusone();               
         } /*  Case  */
         if (strcmp(s,"G5") == 0)
         {
            p->coef.num = spintp;
            ans->tail.spin.g5 = 1;
            ans->coef.ptr = plusone();
         }
      }

      if (p->coef.num == polytp)
      {
         i = 1;
         while (strcmp(vardef->vars[i-1].name,s) != 0)
            if (++i > vardef->nvar) save_sos(14);
         ans->tail.power[vardef->vars[i-1].wordpos-1] =
            vardef->vars[i-1].zerodeg;
      }
	}
#ifdef STRACE
	tracePrn(" \n rd_ (%s) -> ",s);
	writeexpression(p);
#endif

   return (pointer)p;
}


pointer  uact_(char* ch,pointer mm)
{poly      p, m;
 char  np;
 byte      i;

#ifdef STRACE
	tracePrn("\n uact_ (%s)\n",ch);
	writeexpression((poly)mm);
	tracePrn(" -> ");
#endif

   m = (poly)mm;
   p = m->next;
   if (strcmp(ch,"G") == 0 || strcmp(ch,"g") == 0)
   {
      while (p != NULL)
      {
         np = p->tail.tens[0];
         for (i = 0; i < spinLength; i++) p->tail.power[i] = 0;
         p->tail.spin.l = 1;
         p->tail.spin.g5 = 0;
         p->tail.spin.g[0] = np;
         p = p->next;
      }
      if (m->next->tail.spin.g[0] == 0) m->next->tail.spin.g[0] = 1;
		m->coef.num = spintp;
		if (r_reading2) m= (poly) uact_("-",m);
   }
   else
      if (strcmp(ch,"-") == 0 && m->coef.num < 32000)
         switch ((integer)(m->coef.num))
         {
            case numbertp:
            case polytp:
               multpolyint(&p,-1);
            break;

            case spintp:
            case tenstp:
            case vectortp:
               multtensint(&p,-1);
            break;

            case rationtp:
               multpolyint(&p->coef.ptr,-1);
			}
#ifdef STRACE
	writeexpression((poly)m);
#endif

   return (pointer)m;
}


pointer  bact_(char ch,pointer mm1,pointer mm2)
{
 poly      p1, p2, p3, pr1, pr2;
 integer   t1, t2, t3;
 byte      i;
 integer   d;
 pointer mm3;

	if ( r_reading2 && (ch=='*') )
	{
		mm3=mm1;
		mm1=mm2;
		mm2=mm3;
	}

#ifdef STRACE
	tracePrn("\n bact_ (%c)\n",ch);
	writeexpression((poly)mm1);tracePrn(" |%c| ",ch);
	writeexpression((poly)mm2);tracePrn(" -> ");
#endif


   p1 = ((poly)mm1)->next;
   p2 = ((poly)mm2)->next;
   t1 = (integer)(((poly)mm1)->coef.num);
   t2 = (integer)(((poly)mm2)->coef.num);
   delmonom(((poly*)&mm1));
   delmonom(((poly*)&mm2));

   if ((ch == '+' || ch == '*' || ch == '.') && t1 < t2)
   {
      p3 = p1; p1 = p2; p2 = p3;
      t3 = t1; t1 = t2; t2 = t3;
   }
   t3 = -1;   /*  Error  */
   switch (ch)
   {
      case '+':
         switch (t1)
         {
             case numbertp:
             case polytp:
                sewpoly(&p1,&p2);
                p3 = p1;
                t3 = t1;
/* ----- */  break;

             case rationtp:
                if (t2 == t1)
                {
                   p3 = p1;
                   pr1 = multtwopoly(p2->next,p1->coef.ptr);
                   delpoly(&p1->coef.ptr);
                   pr2 = multtwopoly(p1->next,p2->coef.ptr);
                   delpoly(&p2->coef.ptr);
                   sewpoly(&pr1,&pr2);
                   p1->coef.ptr = pr1;
                   pr2 = multtwopoly(p1->next,p2->next);
                   delpoly(&p1->next); delpoly(&p2);
                   p3->next = pr2;
                   t3 = rationtp;
                }
                else
                {
                   pr2 = multtwopoly(p1->next,p2); delpoly(&p2);
                   sewpoly(&p1->coef.ptr,&pr2);
                   p3 = p1;
                   t3 = rationtp;
                }
/* ---- */   break;

             case tenstp:
                sewtens(&p1,&p2,tensLength);
                p3 = p1;
                t3 = tenstp;
/* ----- */  break;

             case spintp:
                if (t2 == spintp)
                {
                   sewtens(&p1,&p2,spinLength);
                   p3 = p1;
                   t3 = spintp;
                }
                else
                {
                   newmonom(&p3);
                   p3->next = NULL;
                   p3->coef.ptr = p2;
                   for (i = 0; i < spinLength; i++) p3->tail.power[i] = 0;
                   sewtens(&p3,&p1,tensLength);
                   t3 = spintp;
                }
/* ----- */  break;

             case vectortp:
                sewtens(&p1,&p2,tensLength);
                p3 = p1;
                t3 = vectortp;
         } /*  Case */
      break;

      case '*':
         switch (t1)
         {
             case numbertp:
             case polytp:
                p3 = multtwopoly(p1,p2);
                delpoly(&p1);
                delpoly(&p2);
                t3 = t1;
             break;

             case rationtp:
                if (t2 == rationtp)
                {
                   newmonom(&p3);
                   p3->coef.ptr = multtwopoly(p1->coef.ptr,p2->coef.ptr);
                   delpoly(&p1->coef.ptr);
                   delpoly(&p2->coef.ptr);
                   p3->next = multtwopoly(p1->next,p2->next);
                   delpoly(&p1);
                   delpoly(&p2);
                   t3 = rationtp;
                }
                else
                {
                   p3 = multtwopoly(p1->coef.ptr,p2);
                   delpoly(&p2);
                   delpoly(&p1->coef.ptr);
                   p1->coef.ptr = p3;
                   p3 = p1;
                   t3 = rationtp;
                }
             break;

             case tenstp:
                if (t2 == tenstp)
                {
                   p3 = multtwotens(p1,p2);
                   t3 = tenstp;
                }
                else
                {
                   multtenspoly(&p1,p2);
                   delpoly(&p2);
                   p3 = p1;
                   t3 = tenstp;
                }
             break;

             case spintp:
                if (t2 == spintp)
                {
                   p3 = multtwospin(p1,p2,FALSE);
                   deltensor(&p1);
                   deltensor(&p2);
                   t3 = spintp;
                }
                else
                {
                   multtenspoly(&p1,p2);
                   delpoly(&p2);
                   p3 = p1;
                   t3 = spintp;
                }
             break;

             case vectortp:
             case indextp:
                multtenspoly(&p1,p2);
                delpoly(&p2);
                p3 = p1;
                t3 = t1;
          } /* Case */
      break;

      case '/':
         if (t1 <= polytp)
         {
            newmonom(&p3);
            p3->next = plusone();
            p3->coef.ptr = p1;
            p1 = p3;
         }
         if (t2 <= polytp)
         {
            newmonom(&p3);
            p3->next = plusone();
            p3->coef.ptr = p2;
            p2 = p3;
         }

         newmonom(&p3);
         p3->coef.ptr = multtwopoly(p1->coef.ptr,p2->next);
         p3->next = multtwopoly(p2->coef.ptr,p1->next);
         delpoly(&p1->coef.ptr);
         delpoly(&p1);
         delpoly(&p2->coef.ptr);
         delpoly(&p2);
         t3 = rationtp;
      break;

      case '^':
         d = (integer)(p2->coef.num);
         delpoly(&p2);
         if (t1 == rationtp)
         {
            p3 = plusone();
            for (i = 1; i <= d; i++)
            {
               p2 = multtwopoly(p3,p1->coef.ptr);
               delpoly(&p3);
               p3 = p2;
            }
            delpoly(&p1->coef.ptr);
            p1->coef.ptr = p3;
            p3 = plusone();
            for (i = 1; i <= d; i++)
            {
               p2 = multtwopoly(p3,p1->next);
               delpoly(&p3);
               p3 = p2;
            }
            delpoly(&p1->next);
            p1->next = p3;
            p3 = p1;
            t3 = rationtp;
         }
         else
         {
            p3 = plusone();
            for (i = 1; i <= d; i++)
            {
               p2 = multtwopoly(p3,p1);
               delpoly(&p3);
               p3 = p2;
            }
            delpoly(&p1);
            t3 = polytp;
         }
      break;

      case '.':
         p3 = multtwotens(p1,p2);         
         if (t1 == vectortp && t2 == vectortp)
         {
            t3 = polytp;
            p1 = p3;
            p3 = p3->coef.ptr;
            delmonom(&p1);
         }
         else
            t3 = tenstp;
   } /* Case */

   if (t3 == rationtp)
   {
      if (p3->next == NULL)
         rderrcode = divisionbyzero;
      else
         if (p3->coef.ptr == NULL)
         {
            delpoly(&p3->next);
            p3->next = plusone();
         }
   }
   newmonom(&p1);
   p1->coef.num = t3;
   p1->next = p3;

#ifdef STRACE
	writeexpression((poly)p1);
#endif


   return (pointer)p1;
}
