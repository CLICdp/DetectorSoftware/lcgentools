#include "tptcmac.h"
#include "syst2.h"
#include "physics.h"
#include "procvar.h"
#include "global.h"

#include "reader5.h"

pointer      bact5(char ch,pointer mm1,pointer mm2)
{
	char       *m1, *m2, *ans, *ss;
	char   r_n,  p_m, q_d;
	boolean r1,r2;

   m1 = (char *) mm1;
   m2 = (char *) mm2;

	if (ch=='+') p_m='P'; else p_m='M';
	r1=(m1[1]=='R'); r2=(m2[1]=='R');

	if ( r1 || r2 || ch=='/') r_n='R'; else r_n='N';

	if ((m1[0] == 'M' || ch =='+')&& ch != '^') lShift(m1,3); 
	                                     else { lShift(m1,2);m1[0]='(';strcat(m1,")"); }
	if ((m2[0] == 'M' || ch =='+')&& ch != '^' &&ch != '/') lShift(m2,3); 
	                                     else { lShift(m2,2);m2[0]='(';strcat(m2,")"); }

	ans= getmem(strlen(m1)+strlen(m2)+24);

   switch (ch)
   {
      case '+': 
                if (m1[0] == '-')  sprintf(ans,"%c%c|%s%s",p_m,r_n,m2,m1);
			  else  if (m2[0] == '-')	sprintf(ans,"%c%c|%s%s",p_m,r_n,m1,m2);
				  else         sprintf(ans,"%c%c|%s+%s",p_m,r_n,m1,m2);
      break;

      case '*':
			if (m2[0] != '-')	sprintf(ans,"%c%c|%s*%s",p_m,r_n,m1,m2);
			  else
			if (m1[0] != '-')	sprintf(ans,"%c%c|%s*%s",p_m,r_n,m2,m1);
			  else   			sprintf(ans,"%c%c|%s*%s",p_m,r_n,m1+1,m2+1);
      break;

		case '/':
			if (inset(quadra,options))  q_d='Q'; else q_d='D';
			if ( !r1)
			{	ss=m1;
				m1=getmem(strlen(m1)+10);
				sprintf(m1, "%cFLOAT(%s)",q_d,ss);
				free(ss);
			}
			if ( !r2)
			{	ss=m2;
				m2=getmem(strlen(m2)+10);
				sprintf(m2, "%cFLOAT(%s)",q_d,ss);
				free(ss);
			}
			if (m2[0] != '-')   sprintf(ans,"%c%c|%s/%s",p_m,r_n,m1,m2);
			else
			{ if (m1[0] == '-') sprintf(ans,"%c%c|%s/%s",p_m,r_n,m1+1,m2+1);
							else    sprintf(ans,"%c%c|-%s/%s",p_m,r_n,m1,m2+1);
			}
      break;

      case '^':     sprintf(ans,"%c%c|%s**%s",p_m,r_n,m1,m2);
   }   /* Case */
	free(m1);
	free(m2);
	return (pointer) ans;
}


pointer uact5(char* ch,pointer mm)
{

  char  *m, *ans;
  char q_d;

  m = (char *) mm;
  ans=getmem(strlen(m)+16);

  if (strcmp(ch,"-") == 0)
  {
		if (m[0] == 'M')
		{	if (m[3] == '-')  sprintf(ans,"M%c|%s",m[1],m+4);
			else              sprintf(ans,"M%c|-%s",m[1],m+3);
		}
		else	sprintf(ans,"M%c|-(%s)",m[1],m+3);
	}

   if (strcmp(ch,"Sqrt") == 0)
   {
		if (inset(quadra,options))   q_d='Q'; else q_d='D';
		if (m[1] == 'N')	sprintf(ans,"MR|%cSQRT(%cFLOAT(%s))",q_d,q_d,m+3);
			else           sprintf(ans,"MR|%cSQRT(%s)",q_d,m+3);

	}
	free(mm);
	return (pointer) ans;
}

pointer  rd5(char* s)
{  char       *p;
	varlist     q;
	byte        k;

	p = getmem(16);
	if ('0' < s[0] && s[0] <= '9') sprintf(p,"MN|%s",s);
   else
   {
      q = modelvars;
		k = maxsp+2;
      while (q != NULL)
      {
         if (strcmp(s,q->varname) == 0)
         {
				sprintf(p,"MR|%s",(*varnamesptr)[k-1]);
            return (pointer) p;
         }
         q = q->next;
         ++(k);
      }
   }
   return (pointer) p;
}
