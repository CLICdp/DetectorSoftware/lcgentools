#include "tptcmac.h"
#include "syst2.h"
#include "crt_util.h"
#define tcol Green

int      yesnokey(void)
{
 byte         x0, y0;
 char         ch;
 int      key;
 boolean      res;

   y0 = where_y();
   x0 = where_x();
   print(" Y/N ?");

label_1: be_be();
   key = inkey();
   if (key < 0) goto label_1;
   ch = (char)key;
   switch (ch)
   {
      case 'Y':   res = TRUE;  break;
      case 'y':   res = TRUE;  break;
      case 'N':   res = FALSE; break;
      case 'n':   res = FALSE; break;
      default:    goto label_1;
   }
   goto_xy(x0,y0);
   print("      "); 
   return res;
} 

static int maxline(int* lines,char* txt)
{ int      tmp=0, i=0, k; 
  
    *lines = 0; 
    for (k = 1; k <= strlen(txt); k++) 
      if (txt[k-1] == '$') 
      { ++(*lines); 
        if ((k - i) > tmp) tmp = k - i;
        i = k; 
      }
    if ((k - i) > tmp) tmp = k - i; 
    return tmp; 
} 

static int  message(int x1,int y1,char* txt1)
{  char         cur_string[STRSIZ],txt[STRSIZ];
   int      mess, ind, marg, i=0, x2, y2;
   pointer      dump;
   int         xold, yold;
      

     strcpy(txt,txt1);
     xold = where_x();
     yold = where_y();
     x2 = x1 + maxline(&y2,txt) + 4;
     y2 = y1 + y2 + 1;
     get_text(x1,y1,x2,y2,&dump);
     chepbox(x1,y1,x2,y2);     
     be_be();
     while (strlen(txt) != 0)
     {
        ++(i);
        ind = cpos('$',txt);
        if (ind == 0) ind = (int)strlen(txt);
        strcpy(cur_string,copy(txt,1,ind - 1));
        strcpy(txt,copy(txt,ind + 1,(int)strlen(txt) - ind));
        marg = (x2 - x1 - strlen(cur_string)) / 2;
        if (y1+i == y2) 
        {  goto_xy(x1+1+marg ,y1 + i);
           print("%s",cur_string);
        }else
        {  char buff[STRSIZ];
           memset(buff,' ',x2-x1 -1);
           memcpy(buff+marg,cur_string,strlen(cur_string));
           buff[x2-x1-1]=0;
           goto_xy(x1+1,y1+i);
           print(buff);
        }   
     }
ret_:
     if (blind || warm) mess='Y' ; else  do{mess = inkey();} while (mess == KB_SIZE);
/* mouse filter */
     if (mess == KB_MOUSE)
     {
        if (  (mouse_info.but1 !=2) ||
              (mouse_info.row < y1) || (mouse_info.row > y2) ||
              (mouse_info.col < x1) || (mouse_info.col > x2)   ) goto ret_;
        if (mouse_info.row == y2 )
        {  if (mouse_info.col < (x1+x2)/2 /* -1*/ ) mess='Y';
           if (mouse_info.col > (x1+x2)/2 /*+1 */) mess='N';
        }       
     }   
/* end of filter */         
     put_text(&dump); 
     goto_xy(xold,yold);     
     return mess;
}   /* message */

int     mess_y_n(int x1,int y1,char* txtstr)
{
 int      key;
 int      fcolor_tmp=fColor;
 int      bcolor_tmp=bColor;

   scrcolor(White,Red);

   while (TRUE)
   {
      key = message(x1,y1,scat("%s( Y / N ?) ",txtstr));
      switch (key)
      {
         case ord('N'):
         case ord('n'): scrcolor(fcolor_tmp,bcolor_tmp);return FALSE;
         case ord('Y'):
         case ord('y'): scrcolor(fcolor_tmp,bcolor_tmp);return TRUE;
      }
   }
}

void  messanykey(int x1,int y1,char* txtstr)
{  
 int  fcolor_tmp=fColor;
 int  bcolor_tmp=bColor;    
   scrcolor(White,Blue);
	message(x1,y1,scat("%sPress any key ",txtstr));
   scrcolor(fcolor_tmp,bcolor_tmp);
}


int informline(int curent,int total)
{  int  x,fc,bc;
   char  b[51];
   static Y;
   fc=fColor;
   bc=bColor;
   if (curent == 0)
   {
      Y=maxRow();
      goto_xy(15,Y); scrcolor(Black,Red);
      memset(b,' ',50); b[50] = '\0';
      print("%s",b);
      goto_xy(15,Y);
      scrcolor(fc,bc);
      return 0;
   }

   x = 15 + (50 * curent) / total;
   if (x > 65) x = 65;
   scrcolor(Black,Red);
   while (where_x() < x) print("%c",'X');
   if (curent >= total)
   {
      scrcolor(White,Black);
      goto_xy(1,Y); clr_eol();
   }
   scrcolor(fc,bc);
   return escpressed();
}


void chepbox(int x1,int y1,int x2,int y2)
{int  i,y;
 char  b[STRSIZ];

   i = x2 - x1;

   b[0]   = boxFrame[0];             /* ACS_ULCORNER */
   memset(b+1,boxFrame[1] ,i-1);     /* ACS_HLINE */
   b[i]   =  boxFrame[2];            /* ACS_URCORNER */
   b[i+1] = '\0';
   goto_xy(x1,y1);print(b);

   b[0]   = boxFrame[6];
   memset(b+1,boxFrame[5] ,i-1);
   b[i]   =  boxFrame[4];
   b[i+1] = '\0';
   goto_xy(x1,y2);  print(b);

   b[0]   = boxFrame[7];
   memset(b+1,' ',i-1);
   b[i]   =  boxFrame[3];
   b[i+1] = '\0';

   for (y = y1 + 1; y < y2; y++) 
   {  goto_xy(x1,y); print("%c",boxFrame[7]);
      goto_xy(x2,y); print("%c",boxFrame[3]); 
   }
      

}


void  menu0 (int col,int row,char* label, char* menstr ,
	  void (**funcKey)(int) ,char** funcKeyMess, pointer * hscr, int* kk)

{ int      i, j, k, col1, npage,lastrow;
  long	     lastpage;
  int   ink;
  int      ncol;
  pointer   pscr;
  int  fkPos[11];
  int  height;
  char fmt[20];
  int lastLine;
  

   lastLine=maxRow();
   if (funcKey == NULL) for (i=0;i<11;i++) fkPos[i]=0; else
   { int xx;
      scrcolor(White,Black);
      goto_xy(1,lastLine); clr_eol();
      xx=0;
      for (j=0;j<10;j++) if (funcKey[j] != NULL ) xx=xx+4+strlen(funcKeyMess[j]);
      xx= (80 - xx )/2 ;
      goto_xy(xx,lastLine);
      for (i=0;i<10;i++)
      { fkPos[i]=where_x();
        if (funcKey[i] != NULL)
        { scrcolor(White,Green); print(" F%i-",i+1);
          scrcolor(Black,Green); print(funcKeyMess[i]);
        }
      }
      fkPos[10]=where_x();
   }


   clearTypeAhead();



   if (*kk < 0) *kk = -(*kk);
      ncol=menstr[0];
      sprintf(fmt,"%%%d.%ds",ncol,ncol);
      height=strlen(menstr)/ncol;
      if (row+height+1 >lastLine-2) height=lastLine-3-row;
      lastpage = 1+    (strlen(menstr)/ncol -1)/height ;
       
   if(label[0] ==0 || row == 1) 
   { if (*hscr == NULL)  get_text(col,row,col+ncol+1,row+2,hscr);} 
   else
   {  char label_[STRSIZ];
      int shft,sz;
      if (*hscr == NULL) get_text(col,row-1,col+ncol+1,row+2,hscr); 
      for(i=0;i<ncol+2;i++) label_[i]=' ';
      label_[ncol+2]=0;  
      sz=strlen(label);
      if(sz >ncol+2) {shft=0;sz=ncol+2;} else shft=(ncol+2 -sz)/2;  
      memcpy(label_+shft,label,sz); 
      scrcolor(Yellow,Blue);
      goto_xy(col,row-1);
      print(label_);
   }
   scrcolor(Black,tcol);
   
   get_text(col,row + 3,col + ncol + 1,row + height + 1,&pscr);
   chepbox(col,row,col + ncol + 1,row + height + 1);
   goto_xy(col+1,row); print("*");


   if (*kk <= 0  || *kk > lastpage * height   )
   {  npage = 1;
      k = 1;
   }
   else {
      k = ((*kk) - 1) % height + 1;
      npage = ((*kk) - 1) / height + 1;
   }
   col1 = col + 1;

label_1:

      chepbox(col,row,col + ncol + 1,row + height + 1);
      goto_xy(col+1,row); print("*");
      if (npage > 1)
      {
         goto_xy(col + ncol - 2,row);
         print("PgUp");
      }
      if (npage < lastpage)
      {
         goto_xy(col + ncol - 2,row + height + 1);
         print("PgDn");
      }

      if(npage<lastpage) lastrow=height;
		  else   lastrow = (strlen(menstr)/ncol)%height;

   lastrow=0;
   for (j = 1; j <= height; j++)
   {  int shift;
      goto_xy(col + 1,row + j);
      shift=1+(j-1 + (npage-1)*height)*ncol;
      if(shift<strlen(menstr)) {print(fmt,menstr+shift );lastrow++;}
		 else           print(fmt," ");

   }

   scrcolor(White,Black);
   if (k > lastrow) k = lastrow;
   goto_xy(col + 1,row + k);
   if (lastrow != 0) print(fmt,menstr+1+(k-1+(npage-1)*height)*ncol);
   scrcolor(Black,tcol);
   goto_xy(col + ncol - 2,row + height + 1);
   while (TRUE)
   { int jump,mousePos;
      jump=1;
      goto_xy(78,1);
      ink = inkey();
/* mouse filter */
      if ((ink==KB_MOUSE)&&(mouse_info.but1 == 2))
      {
         if (mouse_info.row == lastLine )
         for(i=0; i<10;i++)
         if ((mouse_info.col > fkPos[i]) && (mouse_info.col < fkPos[i+1]))
         {  if (i==9)ink='0'; else ink='1'+i;}

         if ( (mouse_info.col >= col ) && (mouse_info.col <=col+ncol+1) )
         {  mousePos = mouse_info.row - row;

            if (col+ncol+1-mouse_info.col <4)
            {
               if (mousePos==0)        ink=KB_PAGEU;
               if (mousePos==height+1) ink=KB_PAGED;
            }

            if ((mousePos == 0 ) && ( mouse_info.col-col <4)) ink=KB_ESC;

            if ((mousePos < 0)&&(mousePos >= height))
            {
               if  (mousePos > k)  { ink=KB_DOWN; jump=mousePos - k;}
               if  (mousePos < k) { ink=KB_UP;   jump=k - mousePos;}
               if (mousePos==k       ) ink=KB_ENTER;
            }
         }
      }
/* end of filter */
      if (lastrow == 0) goto label_3;
label_4:
      switch (ink)
      {
        case KB_MOUSE:
        if (mouse_info.but1 != 2) break;
        if (mouse_info.row == lastLine )
        for(i=0; i<10;i++)
          if ((mouse_info.col > fkPos[i]) && (mouse_info.col < fkPos[i+1]))
          {  if (i==9)ink='0'; else ink='1'+i;
             goto label_4;
          }

        if ( (mouse_info.col < col ) || (mouse_info.col >col+ncol+1) ||
             (mouse_info.row < row ) || (mouse_info.row >row+height+1) ) break;

           mousePos = mouse_info.row - row;
           if ((mousePos == 0 ) && ( mouse_info.col-col <4)) ink=KB_ESC;
           if ((mousePos != 0)&&(mousePos != height+1))
           {
              if  (mousePos > k)  { ink=KB_DOWN; jump=mousePos - k;}
              if (mousePos < k      ) { ink=KB_UP;   jump=k - mousePos;}
           }
           if (col+ncol+1-mouse_info.col <4)
           {
              if (mousePos==0)        ink=KB_PAGEU;
              if (mousePos==height+1) ink=KB_PAGED;
           }
           if (mousePos==k       ) ink=KB_ENTER;
           if (ink!=KB_MOUSE) goto label_4;

          break;

		  case KB_RIGHT: /* key_rigth */
           if (k==lastrow) ink= -81 ;else ink= -80;
           goto label_4;


		  case KB_LEFT: /* key_left */
           if (k==1) ink= -73 ; else ink= -72;
           goto label_4;



			case KB_UP:   /*  UP */
            goto_xy(col1,row + k);
	    print(fmt,menstr+1+(k-1+(npage-1)*height)*ncol);
            k = k - jump;
            if (k == 0) k = lastrow;
            scrcolor(White,Black);   /*    set color W/n  */
            goto_xy(col1,row + k);
	    print(fmt,menstr+1+(k-1+(npage-1)*height)*ncol);
         break;


			case KB_DOWN:   /* DN */
	    goto_xy(col1,row + k);
	    print(fmt,menstr+1+(k-1+(npage-1)*height)*ncol);
            k = k + jump ;
            if (k > lastrow) k = 1;
            scrcolor(White,Black);   /*    set color W/n  */
            goto_xy(col1,row + k);
	    print(fmt,menstr+1+(k-1+(npage-1)*height)*ncol);
         break;

			case KB_ENTER:    /*  ENTER */
            chepbox(col,row,col+ncol+1,row+2);
            scrcolor(White,Black);
            put_text(/*col,row + 3,col + ncol + 1,row + height + 1,*/&pscr);
            goto_xy(col1,row + 1);
	    print(fmt,menstr+1+(k-1+(npage-1)*height)*ncol);
            *kk = (npage - 1) * height + k;
            goto_xy(1,lastLine);clr_eol();
            refresh_scr();
            escpressed();
            return;

			case  KB_BACKSP:       /* Backspace */
			case  KB_ESC:
            goto label_3;

			case KB_PAGEU:    /*  PgUp  */
            if (npage > 1)
            {
               npage--;
               goto label_1;
            }
            else
               be_be();
         break;

			case KB_PAGED:    /*  PgDn  */
            if (npage < lastpage)
            {
               npage++;
               goto label_1;
            }
            else
               be_be();
	    break;

	case  '1':  case'2':	   case  '3':	case  '4':	case  '5':
	case '6':   case '7':	case  '8':	case  '9':	case  '0':
	case KB_F1: case KB_F2: case KB_F3: case KB_F4: case KB_F5:
	case KB_F6: case KB_F7: case KB_F8: case KB_F9: case KB_F10:
	{  int fk;
           if (funcKey==NULL) break;
	   if ( ink>=0 && ink <='9') { fk=ink-'0';if (fk==0) fk=10;}
											  else fk=KB_F1+1-ink;
	   if ((funcKey[fk-1]) != NULL)
           {  pointer saveHlp;
	      get_text(1,lastLine,maxCol(),lastLine,&saveHlp);
	      goto_xy(col+1,row); print("%c",boxFrame[1]);
	      goto_xy(1,lastLine);scrcolor(White,Black);clr_eol();
	      (*funcKey[fk-1])((npage-1)*height+k);
	      put_text(&saveHlp);
	      scrcolor(Black,Green); goto_xy(col+1,row); print("*");
           }
           break;
        }
     }
     scrcolor(Black,tcol);
  }

label_3:
   put_text(hscr);
   put_text(&pscr);
	scrcolor(White,Black);
	goto_xy(1,lastLine); clr_eol();
   *kk = 0;

}


int  str_redact(char* txt,int npos)
{ int    i, x, x0, y0;
  int key;
  boolean first,prn;
  char    txttmp[STRSIZ];
  int t_color=Yellow;   
   clearTypeAhead();

   strcpy(txttmp,txt);
   
   
   first = TRUE;
   
   
   scrcolor(t_color,Black);
   y0 = where_y();
   x0 = where_x();
   clr_eol();
   print("%s",txt);
   if (npos>1) npos=1;
   if (npos>strlen(txt)) npos=strlen(txt)+1;
   x = x0 + npos - 1;
   scrcolor(Black,White); 
   goto_xy(x,y0);
   if(npos>strlen(txt)) print("%c",' ');else print("%c",txt[npos-1]);
   scrcolor(t_color,Black);
   goto_xy(x,y0);      
   
   while (TRUE)
   {
      key = inkey();
      prn=FALSE;		
      switch (key)
      {	
         case KB_MOUSE:
         if ( mouse_info.row == where_y() && mouse_info.but1 == 2 ) 
         {  
             x=mouse_info.col;
             if ( x < x0 ) x=x0;
             if ( x > x0+strlen(txt) ) x=x0+strlen(txt);
         }
         break;
         	
         case 9:      /*  Tab  */
            i = npos;
            do
            {
               ++(i);
               if (i > strlen(txt)) i = 0;
            }  while (!(i == 0 || txt[i-1] == ',' ||
	    strcmp(copy(txt,i - 1,2),"->") == 0));
            x = x0 + i;
         break;

         case KB_END:
            x = x0 + strlen(txt);   /* End    */
         break;

         case KB_HOME:
            x = x0;   /*  Home  */
         break;


         case KB_RIGHT:
            if (x < 80 && npos < strlen(txt) + 1) x++;
         break;

         case KB_LEFT:
            if (x > x0) x--;
         break;

	case KB_DC:
	   if (npos<=strlen(txt))
	   { strdelete(txt,npos,1);
	     prn=TRUE;
	   }
         break;

         case KB_BACKSP:
	   if (npos > 1) 
	   { x--;
	     strdelete(txt,npos-1,1);
	     prn=TRUE;
	   }
         break;

         case KB_ESC:
            strcpy(txt,txttmp);
            npos=1;

         case KB_ENTER:
         case KB_F1:
	 case KB_PAGEU:
	 case KB_PAGED:
	 case KB_UP:
	 case KB_DOWN:
            goto label_1; 

         default:
	    if (key > 31 && key <128)
            {
              if (first)
              {
                 if (spos("->",txt) != 0)
                     sprintf(txt,"%c->",chr(key));
                 else
                     sprintf(txt,"%c",chr(key));
                 npos = 2;
                 x = x0 + npos - 1;
              }
              else
              {
                 sbld(txt,"%s ",txt);
                 for (i = (byte)strlen(txt); i > npos; i--)
                     txt[i-1] = txt[i - 1-1];
                 txt[npos-1] = chr(key); /* INSERT(chr(Key),Txt,Npos); */
                 if (x < 79) x++;
              }

              prn=TRUE;
            }
      }

         goto_xy(x0,y0);
         print("%s",txt);
         clr_eol();
         
         npos = 1 + x - x0;
 
         scrcolor(Black,White); 
         goto_xy(x,y0);        
         if(npos>strlen(txt)) print("%c",' ');else print("%c",txt[npos-1]);
         scrcolor(t_color,Black);
         goto_xy(x,y0);      
   
      first = FALSE;
   }

label_1: 
   goto_xy(x0,y0);
   clr_eol();
   if(key!=KB_ESC) print("%s",txt);
   scrcolor(White,Black);
   return key;
}



static int  correctVar(char* txt,int x,int y, char  type, void * var,int clear)
{  int         npos,key;
   int         rc,err;
   int xx;
   pointer     pscr; 
   char buff[200];
   int *  I;
   long * L;
   double * D;

   if (clear) get_text(x,y,maxCol(),y,&pscr);
   scrcolor(White,Black);
   goto_xy(x,y);
   print(txt);
   xx=where_x();
   
   switch (type)
   {
     case 'L':  L=var;  sprintf(buff,"%ld",*L); break;
     case 'D':  D=var;  sprintf(buff,"%lg",*D); break;
     case 'I':  I=var;  sprintf(buff,"%d" ,*I);  break;
   }    
   npos = 1;      

   do
   { 
      goto_xy(xx,y);
      key  = str_redact(buff,npos);

      if (key == KB_F1 || key == KB_ESC)
      {  
         rc = 0 ;
         goto EXI;
      }

      if (key == KB_ENTER)
      {
         trim(buff);
         
         switch (type)
         {   
           case 'L':   err=sscanf(buff,"%ld",L); break;
           case 'D':   err=sscanf(buff,"%lf",D); break;
           case 'I':   err=sscanf(buff,"%d" ,I); break;
         }                                     
         if (err<=0)   messanykey(10,10," incorrect number $"); 
         else
         {
            rc = 1;
            goto EXI;
         }
      }
      
   }  while (TRUE);
   
EXI: 
   if(clear)put_text(&pscr);   
   return rc;   
} 


int  correctDouble (int x,int y,char* txt,double * var,int clear)
{ return correctVar (txt,x,y,'D',var,clear); }

int  correctLong (int x,int y,char* txt,long * var,int clear)
{ return correctVar (txt,x,y,'L',var,clear); }

int  correctInt (int x,int y,char* txt,int * var,int clear)
{  return correctVar (txt,x,y,'I',var,clear); }

