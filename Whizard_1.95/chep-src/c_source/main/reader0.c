#include "tptcmac.h"
#include "physics.h"
#include "syst2.h"

#include "reader0.h"

char momsubst[9], indsubst[9];
boolean  r_reading0 = FALSE;

pointer  bact0(char ch,pointer mm1,pointer mm2)
{
 char    *m1, *m2, *ans;
 int sgn;
	if ( r_reading0 && (ch == '*' ))
	{
		m1 = (char *) mm2;
		m2 = (char *) mm1;
	}
	else
	{
		m1 = (char *) mm1;
		m2 = (char *) mm2;
	}
	if (ch == '+')
	{
		lShift(m1,2);
		lShift(m2,2);
	}
	else
	{
		if (m1[0] == 'P' || ch == '^' )
		{  lShift(m1,1);
			m1[0]='(';
			strcat(m1,")");
		}  else lShift(m1,2);
		if (m2[0] == 'P' || ch== '^')
		{  lShift(m2,1);
			m2[0]='(';
			strcat(m2,")");
		} else lShift(m2,2);
	}

	ans=(char *) getmem(strlen(m1)+strlen(m2)+8);
   switch (ch)
   {
      case '+':
			if (m1[0] == '-')
			sprintf(ans,"P|%s%s",m2,m1);
         else
				if (m2[0] == '-')
					sprintf(ans,"P|%s%s",m1,m2);
            else
					sprintf(ans,"P|%s+%s",m1,m2);
      break;

		case '*':
			sgn=1;
			if (m1[0]=='-') { lShift(m1,1); sgn= -sgn;}
			if (m2[0]=='-') { lShift(m2,1); sgn= -sgn;}
			if (sgn==1) sprintf(ans,"M|%s*%s",m1,m2);
					 else sprintf(ans,"M|-%s*%s",m1,m2);
		break;

      case '.':
			if (m2[0] != '-')
				sprintf(ans,"M|%s.%s",m1,m2);
         else
				if (m1[0] != '-')
					sprintf(ans,"M|%s.%s",m2,m1);
            else
            {
					lShift(m1,1);
					lShift(m2,1);
					sprintf(ans,"M|%s.%s",m1,m2);
            }
      break;

      case '^':    sprintf(ans,"M|%s**%s",m1,m2);
	}  /* Case */
	free(mm1);free(mm2);
	return (pointer) ans;
}


pointer  uact0(char* ch,pointer mm)
{char  *m, *ans;

	m = (char *) mm;
	ans=(char *) getmem(strlen(m)+10);

   if (strcmp(ch,"-") == 0)
	if (m[0] == 'M')
	{
			if (m[2] == '-') sprintf(ans,"M|%s",m+3);
			else			     sprintf(ans,"M|-%s",m+2);
	}
	else	sprintf(ans,"M|-(%s)",m+2);

	if (strcmp(ch,"G") == 0)
	{
		if (r_reading0) sprintf(ans,"M|-G(ln,%s)",m+2);
					else   sprintf(ans,"M|G(ln,%s)",m+2);
	}
	free(mm);
	return (pointer) ans;
}


pointer  rd0(char* s)
{char    * p;
 char  num;

	p = (char *) getmem(12);
	p[0]=0;
   if (strlen(s) == 2)
      if (s[1] > '0' && s[1] <= '9')
      {
         switch (s[0])
         {
            case 'p':
            case 'P':
               num = ord(s[1]) - ord('0');
               num = momsubst[num-1];
               if (num > 0) sprintf(p,"M|p%d",num);
                    else    sprintf(p,"M|-p%d",-num);
            break;

            case 'm':
               num = ord(s[1]) - ord('0');
               num = indsubst[num-1];
	       sprintf(p,"M|L%d",num);
               break;           
            case 'M':
               num = ord(s[1]) - ord('0');
               num = indsubst[num-1]-1;
	       sprintf(p,"M|L%d",num);
            } 
            
	    if (strcmp(s,"G5") == 0) strcpy(p,"M|G(ln,A)");
      }
	if (strlen(p) == 0) sprintf(p,"M|%s",s);
   return (pointer) p;
}
