#include "tptcmac.h"
#include "syst2.h"
#include "global.h"
#include "physics.h"
#include "files.h"
#include "crt.h"
#include "procvar.h"
#include "os.h"
#include "out_serv.h"


#include "red_out.h"


static void writeprocessname(int * prtclNum)
{
   int i;
    
   writeF("inParticles:={"); 
   for (i = 1; i <= nin; i++)   
     if (i == nin) writeF("\"%s\"}$\n",prtclbase[prtclNum[i]-1].name);          
              else  writeF("\"%s\",",prtclbase[prtclNum[i]-1].name);
      
   writeF("outParticles:={");
   for (i = nin + 1; i <= nin + nout; i++)
     if (i == nin + nout) writeF("\"%s\"}$\n",prtclbase[prtclNum[i]-1].name);
                    else  writeF("\"%s\",",prtclbase[prtclNum[i]-1].name);                        
}


static void  writeparameters(integer nsub)
{setofbyte   varsofprocess, funcofprocess;
 varlist     modl;
 byte        k;
 boolean     first;
 char        ch;
 char        s[STRSIZ];

   writeF("%%\n");
   getprocessvar(nsub,varsofprocess,funcofprocess);
   modl = modelvars;
   k = 0;
   first = TRUE;
   ch = ' ';
   writeF("Parameters:={\n");

   do
   {
      if (insetb(++k + maxsp+1,varsofprocess))
      {  char buff[20];
         doub_str(buff,modl->varvalue);
         writeF("%c{%s,%s}\n",ch,modl->varname,buff);
         if (first) { first = FALSE; ch = ','; 
      }
   }
      modl = modl->next;
   }  while (modl != NULL);
   writeF("              }$\n");

   writeF("%%\n");
   modl = modelvars;
   k = 0;
   do
   {
      if (insetb(++k + maxsp+1,funcofprocess))
      {  sscanf(modl->func,"%[^|]",s);
         trim(s);
         writeF("LET %s=%s$\n",modl->varname,s);
       }
      modl = modl->next;
   }  while (modl != NULL);
}

static void  emitexpression(catrec* cr)
{
 int       nexpr,degr; /* must be byte for read_ */
 int         i;

   seekArchiv(cr->factpos);
   nexpr=readNN();   /*  Nexpr must be equal 2  */
   writeF("TotFactor:=(");
   rewritepolynom();
   writeF(")/(");
   rewritepolynom();
   writeF(")$");
   writeF("\n");

   seekArchiv(cr->rnumpos);
   nexpr= readNN();     /*  Nexpr must be equal 1  */
   writeF("Ans!_num:="); rewritepolynom();writeF("$\n");

   seekArchiv(cr->denompos);
   nexpr=readNN();   /*  Nexpr must be equal ??  */

   writeF("N!!:=%d$\n",nexpr);
   for (i = 1; i <= nexpr; i++)
   {		
      degr=readNN();
      writeF("Degr!!(%d):=%d$\n",i,degr); 
      writeF("WIDTH!!(%d):=",i); rewritepolynom(); writeF("$\n");
      writeF("MASS!!(%d):=",i); rewritepolynom(); writeF("$\n");
      writeF("SS!!(%d):=",i); rewritepolynom(); writeF("$\n");
   }
}


void  makeviewfile(char * f_name)
{
/* 
catrec  cr;
   readvars('R');
   outFileOpen(f_name);
   reset(&menuq);
   reset(&catalog);
   do  FREAD1(cr,catalog.stream); 
   while (!(p_feof(catalog)||(cr.ndiagr_ == ndiagr && cr.nsub_ == nsub))); 
   if (!(cr.ndiagr_ == ndiagr && cr.nsub_ == nsub)) 
   { 
      p_fclose(catalog); 
      outFileClose(); 
      return;
   } 
   reset(&archiv);
   emitexpression(&cr);
   p_fclose(menuq); 
   outFileClose(); 
   p_fclose(catalog); 
   p_fclose(archiv);
*/    
} 


static char  * numstr3(word nn)
{static char   nstr[6]; 

   sbld(nstr,"%u",nn); 
   while (strlen(nstr) < 3)
      sbld(nstr,"0%s",nstr); 
   return nstr; 
} 


static void  sim(int i,int j)
{int k;
   writeF("   TMP!!NUM:=SUM!!NUM$\n");
   writeF("   TMP!!DEN:=SUM!!DEN$\n");
   for (k = i; k <= j - 1; k++)
   {
      writeF("   OFF EXP$\n");
      writeF("   TMP!!ANS:=A!!/SUM!!DEN+1/Sub(p%d=p%d,p%d=p%d,TMP!!DEN)$\n",
         k + nin,j + nin,j + nin,k + nin);
      writeF("   SUM!!DEN:=DEN(TMP!!ANS)$\n");
      writeF("   TMP!!ANS:=SUB(WEPS!!=0,NUM(TMP!!ANS))$\n");
      writeF("   B!!:=SUB(A!!=0,TMP!!ANS)$\n");
      writeF("   TMP!!ANS:=(TMP!!ANS-B!!)/A!!$\n");
      writeF("   ON EXP$\n");
      writeF("   SUM!!NUM:=(TMP!!ANS*SUM!!NUM+B!!*Sub(p%d=p%d,p%d=p%d,TMP!!NUM))$\n",
         k + nin,j + nin,j + nin,k + nin);
      writeF("   CLEAR B!!,TMP!!ANS$\n");
   }
   writeF("   clear TMP!!NUM$ TMP!!DEN$\n");
   writeF("   SUM!!NUM:=SUM!!NUM/%d$\n%%\n",(j + 1) - i);
}


static void  symmetrisation(int * prtclNum)
{  int j,i=1;  
    
   writeF(" IF Not (UserWay = T)  Then \n");
   writeF(" Begin");
   while (i < nout) 
   { 
      j = i + 1; 
      while ((j <= nout) && ( prtclNum[nin+i]==prtclNum[nin+j])) sim(i,j++); 
      i = j; 
   } 
   writeF(" End\n");
} 


static void  writedefaultsum(void)
{ 
   writeF(
      "IF Not (UserWay=T) Then <<Sum!!Num:=0$ $ Sum!!Den:=1$ >>$ \n");
   writeF("%%\n"); 
   writeF("Procedure DefaultSum()$\n"); 
   writeF("%%\n"); 
   writeF("Begin\n"); 
   writeF("   SCALAR B,D,W,Ans $\n"); 
   writeF("   D:=1$\n"); 
   writeF("   OFF Exp$\n"); 
   writeF("   FOR i:=1:N!! DO\n"); 
   writeF("   <<   W:=WEPS!!*WM!!(i)**2$\n"); 
   writeF("        IF (W=0) OR (DEGR!!(i)=2) THEN\n"); 
   writeF("        D:=D*(SS!!(i)**Degr!!(i)  +W)\n"); 
   writeF("        ELSE\n"); 
   writeF("        D:=D*(SS!!(i)**2+W)/SS!!(i)$\n"); 
   writeF("    >>$\n"); 
   writeF("   Ans:=A!!/SUM!!DEN+1/D$\n"); 
   writeF("   SUM!!DEN:=Den(Ans)$\n"); 
   writeF("   Ans:=Sub(WEPS!!=0,Num(Ans))$\n");
   writeF("   B:=Sub(A!!=0,Ans)$\n"); 
   writeF("   Ans:=(Ans-B)/A!!$\n"); 
   writeF("   ON Exp $\n"); 
   writeF(
      "   SUM!!NUM:=SUM!!NUM*Ans+B*TotFactor*Ans!_num$\n"); 
   writeF("   Clear TotFactor,Ans!_num$\n"); 
   writeF("END$\n"); 
} 


static void startReduce(int nsub,int* prtclNum,int ncalc)
{  
   char f_name[STRSIZ];   
   sprintf(f_name,"%sresults%csum%d.red",pathtouser,f_slash,nsub);    
   outFileOpen(f_name);
   writeLabel('%');
   writeprocessname(prtclNum);
   writeparameters(nsub);
   writeF("\n\n");
   writeF("vector p1,p2,p3,p4,p5,p6$\n");
   emitconvlow(prtclNum,'R');
   writeF("ARRAY SS!!(6),WM!!(6),Degr!!(6)$\n");
   writedefaultsum();
}

static void diagramReduce( vcsect * vcs,  catrec * cr )
{
   char      numtxt[6];
   readvars('R');
   writeF("\n");
   strcpy(numtxt,numstr3(cr->nsub_));
   writeF("Diagr!_number:=\"%s-%s\"$\n",numtxt,numstr3(cr->ndiagr_));
   writeF("\n");
   if (vcs != NULL)  DiagramToOutFile(vcs,0,'%');
   emitexpression(cr);
   writeF("\n\n");
   writeF(" IF (UserWay =T) THEN << UserSum()>> ELSE <<DefaultSum()>>$\n");
}

static void endReduce(int * prtclNum)
{ symmetrisation( prtclNum);
  writeF("End$\n");
  outFileClose();
}

void makeReduceOutput(void)
{ makeOutput(startReduce,diagramReduce,endReduce);}

