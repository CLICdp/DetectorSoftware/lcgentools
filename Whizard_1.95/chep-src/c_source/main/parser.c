#include "tptcmac.h"
#include "syst2.h"

#include "parser.h"

 int  rderrcode, rderrpos;

static char     * source;
static binaction  bact;
static unaction   uact;
static rdelement  rd;
static int        count;

static void  readsum(pointer      * s_res);

static void  skip(void)
{
	while (source[count-1] == ' ' && source[count-1] != '|')
      ++(count);
}


static void  readsmplterm(pointer* s_res)
/* pointer * s_res;*/
{
 int      m1;
 char      opername[STRSIZ];

   skip();
	if (source[count-1] == '|')
   {
      rderrcode = syntaxerror;
      rderrpos = count;
      return;
   } 

   if (source[count-1] == '(') 
   { 
      ++(count);
      readsum(s_res);
      if (rderrcode != ok) return;
      if (source[count-1] != ')') 
      { 
         rderrcode = braketexpected; 
         rderrpos = count;
         return;
      } 
      else ++(count);
      goto tst;
   } 

   m1 = count; 
   if (isdigit(source[count-1]) && source[count-1] != '0')
   { 
      do ++(count); 
         while (isdigit(source[count-1])); 
      *s_res = rd(copy(source,m1,count - m1)); 
      if (rderrcode != ok) { rderrpos = count; return; } 
      goto tst;
   } 

   if (isalpha(source[count-1])) 
   { 
      ++(count); 
      while (isalnum(source[count-1])) ++(count);
      if (source[count-1] == '(') 
      { 
         strcpy(opername,copy(source,m1,count - m1)); 
         if (strcmp(opername,"g") == 0 || strcmp(opername,"G") == 0) 
         { 
            ++(count); 
            m1 = count; 
            readsum(s_res); 
            if (rderrcode != ok) return;
            if (source[count-1] != ')') 
            { 
               rderrcode = braketexpected; 
               rderrpos = count; 
               return;
            } 
            else
               ++(count);
            *s_res = uact("G",*s_res); 
            if (rderrcode != ok) { rderrpos = m1; return; } 
            goto tst;
         } 
         else 
            if (strcmp(opername,"Sqrt") == 0) 
            { 
               ++(count); 
               m1 = count; 
               readsum(s_res);
               if (rderrcode != ok) return;
               if (source[count-1] != ')') 
               { 
                  rderrcode = braketexpected; 
                  rderrpos = count; 
                  return;
               }
               else
                  ++(count); 
               *s_res = uact("Sqrt",*s_res); 
               if (rderrcode != ok) { rderrpos = m1; return; }
               goto tst;
            } 
            else 
            { 
               rderrcode = syntaxerror; 
               rderrpos = count; 
               return;
            } 
      } 
      else 
      { 
         *s_res = rd(copy(source,m1,count - m1)); 
         if (rderrcode != ok) { rderrpos = count; return; }
      } 
      goto tst;
   } 
   rderrcode = unexpectedcharacter; 
   rderrpos = count; 
   return;

tst:
   skip(); 
	if (strchr("*/+-).|",source[count-1]) == NULL)
   { 
      rderrcode = operationexpected; 
      rderrpos = count; 
   } 
}   /*  ReadSmplTerm  */ 


static void  readtermexp(pointer* m_res)
/* pointer * m_res;*/
{pointer   m2_res; 
	int	 sgn_pos;

   readsmplterm(m_res); 
   if (rderrcode != ok) return;
	while (strchr(")+-*/|",source[count-1]) == NULL)
   { 
      sgn_pos = count; 
      ++(count); 
      readsmplterm(&m2_res);
      if (rderrcode != ok) return;
      *m_res=bact('.',*m_res,m2_res); 
      if (rderrcode != ok) { rderrpos = sgn_pos; return; } 
   } 
}


static void  readtermmult(pointer* m_res)
/* pointer * m_res;*/
{pointer   m2_res; 
	int		sgn_pos;

   readtermexp(m_res); 
   if (rderrcode != ok) return;

	while (strchr(")+-/|",source[count-1]) == NULL)
   { 
      if (source[count + 1-1] == '*')
         ++(count); 
      else
         return;
      sgn_pos = count;
      ++(count); 
      readtermexp(&m2_res); 
      if (rderrcode != ok) return;
      *m_res = bact('^',*m_res,m2_res); 
      if (rderrcode != ok) { rderrpos = sgn_pos; return; } 
   } 
} 

static void  readmonom(pointer* m_res)
/* pointer * m_res;*/
{char      sgn; 
 pointer   m2_res; 
	int		sgn_pos;

   skip();
   readtermmult(m_res);
   if (rderrcode != ok) return;

	while (strchr(")+-|",source[count-1]) == NULL)
   {
      sgn = source[count-1];
      sgn_pos = count;
      ++(count);
      readtermmult(&m2_res);
      if (rderrcode != ok) return;
      *m_res = bact(sgn,*m_res,m2_res);
      if (rderrcode != ok) { rderrpos = sgn_pos; return; }
   }
}


static void  readsum(pointer* s_res)
/* pointer * s_res;*/
{char      sgn;
 pointer   m_res;
 int      sgn_pos;

   skip();
	if (source[count-1] == '|')
   {
      rderrcode = unbalancedbraket;
      rderrpos = count;
      return;
   }
   sgn = source[count-1];
   sgn_pos = count;
   if (sgn == '-' || sgn == '+')
      ++(count);
   else
      sgn = '+';
   readmonom(s_res);
   if (rderrcode != ok) return;
   if (sgn == '-') *s_res = uact("-",*s_res);
   if (rderrcode != ok)
   {  rderrpos += sgn_pos;
      return;
   }
/* ??????
   if (rderrcode != ok)
   {
      rderrpos = sgn_pos;
      return;
   }
*/
	while (source[count-1] != ')' && source[count-1] != '|')
   {
      sgn = source[count-1];
      sgn_pos = count;
      ++(count);
      readmonom(&m_res);
      if (rderrcode != ok) return;
      if (sgn == '-') m_res = uact("-",m_res);
      if (rderrcode != ok) { rderrpos += sgn_pos; return; }
      *s_res = bact('+',*s_res,m_res);
      if (rderrcode != ok) { rderrpos = sgn_pos; return; }
   }
}   /*  ReadSum  */

  /* var Rslt:Pointer; */

pointer readexprassion(char* source1,binaction bact1,
                    unaction uact1,rdelement rd1)
{pointer      rslt;
 pointer      readexprassion1;
 char       * svsource, buff[STRSIZ];
 int          svcount = count;

	strcpy(buff,source1);
	svsource = source;
	source   = buff;
   bact=bact1,
   uact=uact1,
   rd=rd1;

   count = 1;
	source[strlen(source) + 1-1] = '|';
   rderrpos = 0;
   rderrcode = ok;
   skip();
	if (source[count-1] == '|')
   {  rderrcode = recordabsent;
      rderrpos = count;
   }
   else
   {
      readsum(&rslt);
		if (!rderrcode != ok && source[count-1] != '|')
      {
         rderrcode = unbalancedbraket;
         rderrpos = count;
      }
      readexprassion1 = rderrcode == ok ? rslt : NULL;
	}
	count = svcount;
	source = svsource;
   return readexprassion1;
}
