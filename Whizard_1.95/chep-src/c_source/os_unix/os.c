
/*-------------------------------------------------*
 * C -version by Victor Edneral, Moscow University *
 *        The latest revision of 22.02.1993        *
 *-------------------------------------------------*/

#include "tptcmac.h"
#include "os.h"
#include "syst2.h"

char f_slash = '/';
char d_slash = '/';
char *defaultPath = ".";

       
#include <sys/types.h>
#include <sys/stat.h>

#include <dirent.h>

static  DIR       * dirpff = NULL;
static char        nsff[30];
static char        esff[10];
static boolean     lnff, leff;



static void  fsplit(char* fname,char* path,char* name,char* ext)
{integer k=0,l,m,n;

   trim(fname);
   m = n = (integer)strlen(fname)-1;
   while (m>=0 && fname[m] != '.' && fname[m] != f_slash) m--;
   if (m>=0 && fname[m] == '.') 
   {  for (l=m; l<=n; k++,l++)
         ext[k]=fname[l]; 
      n = --m;
   }
   else
      m=n;
   ext[k]='\0';

   while(m>=0 && fname[m] != d_slash && fname[m] != ':') m--;
   for(k=0,l=m+1; l<=n; k++,l++) name[k]=fname[l]; 
   name[k]='\0';

   for(k=0; k<=m; k++) path[k]=fname[k]; 
   path[k]='\0';
}


int find_first(char* filename,searchrec* filerec,int attrib)
{char      ds[81];
 integer   k;

   if (dirpff != NULL) closedir(dirpff);
   fsplit(filename,ds,nsff,esff);
   k = (integer)strlen(ds);
   ds[k++] = '.';
   ds[k] = '\0';
   lnff = strcmp(nsff,"*")  == 0;
   leff = strcmp(esff,".*") == 0;

   if ((dirpff = opendir(ds)) == NULL) return -1;
   return find_next(filerec);
}

int find_next(searchrec* filerec)
{struct dirent * dp;
 char            buff[10];
 char            ns1[30];
 char            es1[10];
   if (dirpff == NULL) return -1;   
   while ((dp =(struct dirent *     ) readdir(dirpff)) != NULL)
      if ((dp->d_name)[0] != '.')
      {
         fsplit(dp->d_name,buff,ns1,es1);
         if ((lnff || strcmp(nsff,ns1) == 0) &&
             (leff || strcmp(esff,es1) == 0))
         {  strcpy(filerec->name,dp->d_name);
            return 0;
         }
      }
   closedir(dirpff);
   dirpff = NULL;
   return -1;
}


void  chepmkdir(char* p)
{
	searchrec s;
	trim(p);
	if (0 == find_first (p, &s, directory) ) { ioresult=0; return;}
   ioresult = mkdir(p,-1);
}

