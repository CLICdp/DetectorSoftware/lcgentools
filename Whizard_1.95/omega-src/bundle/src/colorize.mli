(* $Id: colorize.mli 227 2008-03-09 22:21:34Z ohl $ *)
(* Copyright (C) 2008- by Thorsten Ohl <ohl@physik.uni-wuerzburg.de> et al.
   O'Mega is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by 
   the Free Software Foundation; either version 2, or (at your option)
   any later version.
   O'Mega is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.  *)  

(* \thocwmodulesection{\ldots} *)

module type Colorized =
  sig
    include Model.T
    type flavor_sans_color
    val amplitude : flavor_sans_color list -> flavor_sans_color list ->
      (flavor list * flavor list) list
  end

module type Colorized_Gauge =
  sig
    include Model.Gauge
    type flavor_sans_color
    val amplitude : flavor_sans_color list -> flavor_sans_color list ->
      (flavor list * flavor list) list
  end

module type Flows = 
  sig
    val max_num : int
  end

module It (F : Flows) (M : Model.T) : Colorized
  with type flavor_sans_color = M.flavor

module Gauge (F : Flows) (M : Model.Gauge) : Colorized_Gauge
  with type flavor_sans_color = M.flavor

(*i
 *  Local Variables:
 *  mode:caml
 *  indent-tabs-mode:nil
 *  page-delimiter:"^(\\* .*\n"
 *  End:
i*)
