(* $Id: omega.ml 1158 2009-09-07 20:31:34Z jr_reuter $ *)
(* Copyright (C) 2000-2004 by Thorsten Ohl <ohl@physik.uni-wuerzburg.de> et al.
   O'Mega is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by 
   the Free Software Foundation; either version 2, or (at your option)
   any later version.
   O'Mega is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.  *)  

module P = Momentum.Default
module PW = Momentum.DefaultW

module type T =
  sig
    val main : unit -> unit
    type flavor
    val diagrams : flavor -> flavor -> flavor list ->
      ((flavor * Momentum.Default.t) *
         (flavor * Momentum.Default.t,
          flavor * Momentum.Default.t) Tree.t *
         (unit, flavor * Momentum.Default.t) Color.amplitude) list
  end

module Make (FM : Fusion.Maker) (TM : Target.Maker) (M : Model.T) =
  struct

    type flavor = M.flavor

(* \begin{dubious}
     NB: this causes the constant initializers in [FM] to be evaluated
     \emph{twice}, once for [F] and the second time for [W].  Such side
     effects must be avoided if the initializers involve expensive
     computations.   \emph{Relying on the fact that the functor will be
     called only once is not a good idea!}
   \end{dubious} *)
    module F = FM(P)(M)
    module MF = Fusion.Multi(F)
    module T = TM(F)(Fusion.Multi)(M)
    module W = Whizard.Make(FM)(P)(PW)(M)
    module C = Cascade.Make(M)(P)

    let version () =
      List.iter (fun s -> prerr_endline ("RCS: " ^ s))
        (ThoList.flatmap RCS.summary (M.rcs :: T.rcs_list @ F.rcs_list))

    let debug (str, descr, opt, var) =
      [ "-warning:" ^ str, Arg.Unit (fun () -> var := (opt, false):: !var),
        "check " ^ descr ^ " and print warning on error";
        "-error:" ^ str, Arg.Unit (fun () -> var := (opt, true):: !var),
        "check " ^ descr ^ " and terminate on error" ]

    let rec include_goldstones = function
      | [] -> false
      | (T.Gauge, _) :: _ -> true
      | _ :: rest -> include_goldstones rest
      
    let p2s p =
      if p >= 0 && p <= 9 then
        string_of_int p
      else if p <= 36 then
        String.make 1 (Char.chr (Char.code 'A' + p - 10))
      else
        "_"

    let format_p wf =
      String.concat "" (List.map p2s (F.momentum_list wf))

    let variable wf = M.flavor_to_string (F.flavor wf) ^ format_p wf
    let variable' wf = M.flavor_symbol (F.flavor wf) ^ format_p wf

    let commands_of_channel channel =
      Omega_parser.file Omega_lexer.token (Lexing.from_channel channel)

    let commands_of_file = function
      | "-" ->
          commands_of_channel stdin
      | name ->
          let channel = open_in name in
          let commands = commands_of_channel channel in
          close_in channel;
          commands

    let execute_process_whizard phs name fortran in_list out_list =
      Options.parse T.options ("module", name);
      let amplitude =
        F.amplitude false C.no_cascades
          (List.map M.flavor_of_string in_list)
          (List.map M.flavor_of_string out_list) in
      T.amplitude_to_channel fortran [] amplitude;
      W.write phs name (W.merge (W.trees amplitude))

    let rec execute_whizard phs name fortran names = function
      | Omega_syntax.Whizard _ -> invalid_arg "recursive whizard"
      | Omega_syntax.Output (name, commands) ->
          let ch = open_out (name ^ ".f90") in
          let names' = List.fold_left
              (execute_whizard phs name ch) (name :: names) commands in
          close_out ch;
          names'
      | Omega_syntax.Model (o, v) ->
          Options.parse M.options (o, v); names
      | Omega_syntax.Target (o, v) ->
          Options.parse T.options (o, v); names
      | Omega_syntax.Process (in_list, out_list) ->
          execute_process_whizard phs name fortran in_list out_list;
          names

    let execute_process channel in_list out_list =
      T.amplitude_to_channel channel []
        (F.amplitude false C.no_cascades
           (List.map M.flavor_of_string in_list)
           (List.map M.flavor_of_string out_list))

    let rec execute channel = function
      | Omega_syntax.Whizard (name, commands) ->
          Options.parse T.options ("whizard", "");
          let phs = open_out (name ^ ".phs") in
          let names =
            List.fold_left (execute_whizard phs name stdout) [] commands in
          close_out phs;
          let interface = open_out (name ^ ".f90") in
          Whizard.write_interface interface names;
          close_out interface;
          let makefile = open_out ("Makefile." ^ name) in
          Whizard.write_makefile_processes makefile names;
          close_out makefile
      | Omega_syntax.Output (name, commands) ->
          let ch = open_out (name ^ ".f90") in
          List.iter (execute ch) commands;
          close_out ch
      | Omega_syntax.Model (o, v) -> Options.parse M.options (o, v)
      | Omega_syntax.Target (o, v) -> Options.parse T.options (o, v)
      | Omega_syntax.Process (in_list, out_list) ->
          execute_process channel in_list out_list

(* \thocwmodulesection{Parsing Process Descriptions} *)

    type 'a bag = 'a list
    type process =
      | Any of flavor bag list
      | Decay of flavor bag * flavor bag list
      | Scattering of flavor bag * flavor bag * flavor bag list

(* [parse_process] decodes process descriptions
   \begin{subequations}
   \begin{align}
     \text{\texttt{"a b c d"}} &\Rightarrow \text{[Any [a; b; c; d]]} \\
     \text{\texttt{"a -> b c d"}} &\Rightarrow \text{[Decay (a, [b; c; d])]} \\
     \text{\texttt{"a b -> c d"}} &\Rightarrow \text{[Scattering (a, b, [c; d])]}
   \end{align}
   \end{subequations}
   where each word is split into a bag of flavors separated by `\texttt{:}'s. *)

    let parse_process process =
      let last = String.length process - 1
      and flavor off len = M.flavor_of_string (String.sub process off len) in

      let add_flavors flavors = function
        | Any l ->  Any (List.rev flavors :: l)
        | Decay (i, f) -> Decay (i, List.rev flavors :: f)
        | Scattering (i1, i2, f) -> Scattering (i1, i2, List.rev flavors :: f) in

      let rec scan_list so_far n =
        if n > last then
          so_far
        else
          let n' = succ n in
          match process.[n] with
          | ' ' | '\n' -> scan_list so_far n'
          | '-' -> scan_gtr so_far n'
          | c -> scan_flavors so_far [] n n'

      and scan_flavors so_far flavors w n =
        if n > last then
          add_flavors (flavor w (last - w + 1) :: flavors) so_far
        else
          let n' = succ n in
          match process.[n] with
          | ' ' | '\n' ->
              scan_list (add_flavors (flavor w (n - w) :: flavors) so_far) n'
          | ':' -> scan_flavors so_far (flavor w (n - w) :: flavors) n' n'
          | _ -> scan_flavors so_far flavors w n'

      and scan_gtr so_far n =
        if n > last then
          invalid_arg "expecting `>'"
        else
          let n' = succ n in
          match process.[n] with
          | '>' ->
	      begin match so_far with
	      | Any [i] ->  scan_list (Decay (i, [])) n'
	      | Any [i2; i1] ->  scan_list (Scattering (i1, i2, [])) n'
	      | Any _ -> invalid_arg "only 1 or 2 particles in |in>"
	      | _ -> invalid_arg "too many `->'s"
	      end
          | _ -> invalid_arg "expecting `>'" in

      match scan_list (Any []) 0 with
      | Any l -> Any (List.rev l)
      | Decay (i, f) -> Decay (i, List.rev f)
      | Scattering (i1, i2, f) -> Scattering (i1, i2, List.rev f)

(* Force interpretation as decay and punt on an explicit scattering
   \verb+"a b -> c d"+. *)
    let parse_decay process =
      match parse_process process with
      | Any (i :: f) ->
          prerr_endline
            "missing `->' in process description, assuming decay.";
          (i, f)
      | Decay (i, f) -> (i, f)
      | _ -> invalid_arg "expecting decay description: got scattering"

(* Force interpretation as scattering and punt on an explicit decay
   \verb+"a -> b c"+. *)
    let parse_scattering process =
      match parse_process process with
      | Any (i1 :: i2 :: f) ->
          prerr_endline
            "missing `->' in process description, assuming scattering.";
          (i1, i2, f)
      | Scattering (i1, i2, f) -> (i1, i2, f)
      | _ -> invalid_arg "expecting scattering description: got decay"

(* Backwards compatibility: *)

    let add_particles =
      let have_warned = ref false in
      fun particles names ->
        if not !have_warned then begin
          Printf.eprintf "unstructured syntax for processes deprecated\n";
          Printf.eprintf "please upgrade to -scatter and -decay\n";
          have_warned := true
        end;
        particles := names :: !particles

(* Syntax for explicit particle lists. *)

    let parse_jr process =      
      let rec colon_parse' str n strlist =
        let len = String.length str - 1 in
        if n > len then
          [str] else
          let n' = succ n in
          match str.[n] with
          | ';' ->
              begin
                match str.[n'] with
                | ';' -> colon_parse' (String.sub str (n'+1) (len - n')) 0
                      (strlist @ [String.sub str 0 n])
                | c -> invalid_arg "wrong syntax"
              end
          | c -> if n = len then
              strlist @ [str]
          else
              colon_parse' str n' strlist in
    let colon_parse str =
      colon_parse' str 0 []
    in
    let parse_process_jr process = 
      List.map parse_process (colon_parse process)
    and
        scatter_flatter sca1 sca2 = 
      match sca1, sca2 with
      | Scattering (l1,l2,l3), Scattering (i1,i2,o) ->
          Scattering (l1 @ i1, l2 @ i2, l3 @ [List.flatten o])
      | _, _ -> failwith "scatter_flatter: not applicable"
    and
        decay_hurray dec1 dec2 = 
      match dec1, dec2 with
      | Decay (l1,l2), Decay (i,o) ->
          Decay (l1 @ i, l2 @ [List.flatten o])
      | _, _ -> failwith "decay_hurray: not applicable" in
    let reshuffle_proc process =      
      match (List.hd (parse_process_jr process)) with
      | Scattering _ -> List.fold_left scatter_flatter 
            (Scattering ([],[],[])) (parse_process_jr process)
      | Decay _ -> List.fold_left decay_hurray
            (Decay ([],[])) (parse_process_jr process)
      | _ -> invalid_arg "reshuffle_proc: not applicable here"
    in
    match !MF.no_tensor with
    | true -> reshuffle_proc process
    | false -> prerr_endline "Fatal error, only applicable together with option: -fusion:no_tensor";
      exit 1

(* Copy of the above, but with process list read from file. *)

    let parse_wk file =      
      let in_channel = open_in file in
      let rec fill strlist =
        try
          let str = input_line in_channel in
          fill (strlist @ [str])
        with
          End_of_file ->
            begin
              close_in in_channel;
              strlist
            end
      in
      let process_list = List.map parse_process (fill [])
      in
      let
          scatter_flatter sca1 sca2 = 
        match sca1, sca2 with
        | Scattering (l1,l2,l3), Scattering (i1,i2,o) ->
            Scattering (l1 @ i1, l2 @ i2, l3 @ [List.flatten o])
        | _, _ -> failwith "scatter_flatter: not applicable"
      and
          decay_hurray dec1 dec2 = 
        match dec1, dec2 with
        | Decay (l1,l2), Decay (i,o) ->
            Decay (l1 @ i, l2 @ [List.flatten o])
        | _, _ -> failwith "decay_hurray: not applicable" 
      in
      let reshuffle_proc process_list =      
        match (List.hd process_list) with
        | Scattering _ -> List.fold_left scatter_flatter 
              (Scattering ([],[],[])) process_list
        | Decay _ -> List.fold_left decay_hurray
              (Decay ([],[])) process_list
        | _ -> invalid_arg "reshuffle_proc: not applicable here"
      in
      match !MF.no_tensor with
      | true -> reshuffle_proc process_list
      | false -> prerr_endline "Fatal error, only applicable together with option: -fusion:no_tensor";
          exit 1


    let proj_scattering = function
      | Scattering (i1,i2,o) -> (i1,i2,Product.thread o)
      | _ -> invalid_arg "proj_scattering"

    let proj_decay = function
      | Decay (i,o) -> (i,Product.thread o)
      | _ -> invalid_arg "proj_decay"

    let parse_scattering_jr process = 
      proj_scattering (parse_jr process)

    let parse_decay_jr process = 
      proj_decay (parse_jr process)
      
    let parse_scattering_wk process = 
      proj_scattering (parse_wk process)

    let parse_decay_wk process = 
      proj_decay (parse_wk process)
      
        
(* \thocwmodulesection{Main Program} *)

    let main () =
      let scattering = ref None
      and rev_decays = ref ([])
      and cascades = ref ([])
      and particles = ref ([])
      and old_interface = ref false
      and checks = ref []
      and print_forest = ref false
      and template = ref false
      and col_decomp = ref false
      and feynmf = ref None
      and feynmf_tex = ref false
      and quiet = ref false
      and write = ref true
      and params = ref false
      and poles = ref false
      and dag_out = ref None
      and dag0_out = ref None in
      Arg.parse
        (Options.cmdline "-target:" T.options @
         Options.cmdline "-model:" M.options @
         Options.cmdline "-fusion:" MF.options @  
         ThoList.flatmap debug
           ["", "arguments", T.All, checks;
            "a", "# of input arguments", T.Arguments, checks;
            "h", "input helicities", T.Helicities, checks;
            "m", "input momenta", T.Momenta, checks;
            "g", "internal Ward identities", T.Gauge, checks] @
         ["-scatter", Arg.String (fun s -> scattering := Some (parse_scattering s)),
          "in1 in2 -> out1 out2 ...";
          "-scatter_list", Arg.String (fun s -> scattering := Some (parse_scattering_jr s)),
          "proc1 ;; proc2 ;; proc3 ;;  ...";
          "-scatter_list_file", Arg.String (fun s -> scattering := Some (parse_scattering_wk s)),
          "read process list from file";
          "-decay", Arg.String (fun s -> rev_decays := parse_decay s :: !rev_decays),
          "in -> out1 out2 ...";
          "-decay_list", Arg.String (fun s -> rev_decays := parse_decay_jr s :: !rev_decays),
          "in -> out1 ;; in -> out2 ;; in -> out3";
          "-decay_list_file", Arg.String (fun s -> rev_decays := parse_decay_wk s :: !rev_decays),
          "read decay list from file";
          "-cascade", Arg.String (fun s -> cascades := s :: !cascades), "...";
          "-col_decomp", Arg.Set col_decomp, "color decomposition (experimental)";
          "-template", Arg.Set template,
          "write a template for using handwritten amplitudes with WHIZARD";
          "-forest", Arg.Set print_forest, "Diagrammatic expansion";
          "-feynmf", Arg.String (fun s -> feynmf := Some s), "print feynmf/mp output";
          "-feynmf_tex", Arg.Set feynmf_tex, "print feynmf/mp/LaTeX output";
          "-revision", Arg.Unit version,
          "print revision control information";
          "-quiet", Arg.Set quiet, "don't print a summary";
          "-old-interface", Arg.Set old_interface, "old Fortran syntax";
          "-summary", Arg.Clear write, "print only a summary";
          "-params", Arg.Set params, "print the model parameters";
          "-poles", Arg.Set poles, "print the Monte Carlo poles";
          "-dag", Arg.String (fun s -> dag_out := Some s),
          "print minimal DAG";
          "-full_dag", Arg.String (fun s -> dag0_out := Some s),
          "print complete DAG";
          "-file", Arg.String (fun file ->
            List.iter (execute stdout) (commands_of_file file); exit 0),
          "read commands from file "])
(*i
          "-T", Arg.Int Topology.Binary.debug_triplet, "";
          "-P", Arg.Int Topology.Binary.debug_partition, ""])
i*)
        (fun names -> add_particles particles names)
        ("usage: " ^ Sys.argv.(0) ^ " [options] [" ^
         String.concat "|" (List.map M.flavor_to_string (M.flavors ())) ^ "]");
      let process =
        match !particles, !scattering, List.rev !rev_decays with
        | [], Some (in1, in2, out), [] -> Scattering (in1, in2, out)
        | [], None, [in1, out] -> Decay (in1, out)
        | particle_list, None, [] ->
            parse_process (String.concat " " (List.rev particle_list))
        | particle_list, _, _ ->
            prerr_endline "clashing old and new syntax!";
            exit 1 in
      let selectors =
        let dim =
          match process with
          | Scattering (_, _, out) -> List.length out + 2
          | Decay (_, out) -> List.length out + 1
          | Any flavors -> List.length flavors in
(*i         Printf.eprintf "%s\n" (C.to_string (C.of_string_list dim !cascades)); i*)
        C.to_selectors (C.of_string_list dim !cascades) in
      if !params then
        T.parameters_to_channel stdout
      else
        let amplitudes =
          match process with
          | Scattering (in1, in2, out) | Any (in1 :: in2 :: out) ->
              MF.amplitudes (include_goldstones !checks)
                selectors [in1; in2] out
          | Decay (in1, out) | Any (in1 :: out) ->
              MF.amplitudes (include_goldstones !checks)
                selectors [in1] out
          | _ ->
              prerr_endline "too few particles!";
              exit 1 in
        if !write then begin
          if !old_interface then
            T.amplitude_to_channel stdout !checks
              (List.hd (MF.all amplitudes))
          else
            T.amplitudes_to_channel stdout !checks amplitudes
        end;
        if not !quiet then begin
          List.iter (fun amplitude ->
            Printf.eprintf "SUMMARY: %d fusions, %d propagators"
              (F.count_fusions amplitude) (F.count_propagators amplitude);
            flush stderr;
            if List.length !particles <= 12 then
              Printf.eprintf ", %d diagrams" (F.count_diagrams amplitude);
            Printf.eprintf "\n") (MF.all amplitudes);
        end;
        if !poles then begin
          List.iter (fun amplitude ->
            W.write stdout "omega" (W.merge (W.trees amplitude)))
            (MF.all amplitudes)
        end;
        begin match !dag0_out with
        | Some name ->
            let ch = open_out name in
            List.iter (F.tower_to_dot ch) (MF.all amplitudes);
            close_out ch
        | None -> ()
        end;
        begin match !dag_out with
        | Some name ->
            let ch = open_out name in
            List.iter (F.amplitude_to_dot ch) (MF.all amplitudes);
            close_out ch
        | None -> ()
        end;
        if !print_forest then
          List.iter (fun amplitude ->
            List.iter (fun t -> Printf.eprintf "%s\n"
                (Tree.to_string
                   (Tree.map (fun (wf, _) -> variable wf) (fun _ -> "") t)))
              (F.forest (List.hd (F.externals amplitude)) amplitude))
            (MF.all amplitudes);
        begin match !feynmf with
        | Some name ->
            let fmf wf =
              let lbl = M.flavor_to_TeX (F.flavor wf) in
              { Tree.style =
                begin match M.propagator (F.flavor wf) with                  
                | Coupling.Prop_Feynman 
                | Coupling.Prop_Col_Feynman 
                | Coupling.Prop_Gauge _ ->
                    begin match (M.color (F.flavor wf)) with
                    | Color.AdjSUN _ -> Some ("gluon", lbl)
                    | _ -> Some ("photon", lbl)
                    end;
                | Coupling.Prop_Unitarity
                | Coupling.Prop_Rxi _ -> Some ("photon", lbl)
                | Coupling.Prop_Majorana -> Some ("plain", lbl)
                | Coupling.Prop_Scalar 
                | Coupling.Prop_Col_Scalar -> Some ("dashes", lbl)
                | Coupling.Prop_Spinor
                | Coupling.Prop_ConjSpinor -> Some ("fermion", lbl)
                | Coupling.Prop_Vectorspinor -> Some ("plain,photon",
                      lbl)
                | Coupling.Prop_Tensor_2 -> Some ("dbl_wiggly", lbl)
                | Coupling.Prop_Ghost -> Some ("dots", lbl)
                | _ -> None
                end;
                Tree.rev =
                begin match M.propagator (F.flavor wf) with
                | Coupling.Prop_Spinor -> false
                | Coupling.Prop_ConjSpinor -> true
                | _ -> false
                end;
                Tree.label = None;
                Tree.tension = None } in
            let a = List.hd (MF.all amplitudes) in
            let wf1 = List.hd (F.externals a)
            and wf2 = List.hd (List.tl (F.externals a)) 
            in
            Tree.to_feynmf feynmf_tex name variable' wf2
              (List.map (Tree.map (fun (n, _) -> fmf n) (fun l -> l))
                 (F.forest wf1 a))
        | None -> ()
        end;
        if !col_decomp then begin
          let id x = x in
          let a = List.hd (MF.all amplitudes) in
          let wf1 = List.hd (F.externals a)
          (*i and wf2 = List.hd (List.tl (F.externals a)) i*) in
          let trees = List.map (Tree.map fst id) (F.forest wf1 a) in
          List.iter (fun t ->
            Printf.printf "%s\n"
              (Tree.to_string (Tree.map variable (fun _ -> "") t));
            let c =
              Color.of_tree (fun wf -> M.color (F.flavor wf)) id t in
            Printf.printf " =1=> %s\n"
              (Color.to_string format_p format_p c);
            Printf.printf " =2=> %s\n"
              (Color.Flows.to_string format_p (Color.Flows.of_amplitude wf1 c)))
            trees;
          flush stdout;
          let colors =
            Array.of_list
              (List.map (fun t ->
                Color.Flows.of_amplitude (F.momentum wf1)
                  (Color.of_tree M.color id
                     (Tree.map F.flavor F.momentum t))) trees) in
(*i
          let p_to_string p =
            String.concat "" (List.map p2s (Momentum.Default.to_ints p)) in
i*)
          Printf.eprintf "compressing colors %d -> " (Array.length colors);
          let compressed_colors = ThoArray.compress colors in
          let colors = ThoArray.uniq compressed_colors in
          Printf.eprintf "%d.\n" (Array.length colors);
          flush stderr;
          let hash = Color.Flows.make_hash () in
          let n = Array.length colors in
          let cc = Array.make_matrix n n 0.0 in
          let start = Sys.time () in
          Printf.eprintf "eval_squaring ...";
          flush stderr;
          for i = 0 to pred n do
            for j = 0 to i do
              let c1c2 =
                Color.Flows.C.to_float
                  (Color.Flows.eval_square_memoized hash
                     Momentum.Default.neg colors.(i) colors.(j)) in
              cc.(i).(j) <- c1c2;
              cc.(j).(i) <- c1c2
            done
          done;
          Printf.eprintf " done in %g secs.\n" (Sys.time () -. start);
          flush stderr;
          Printf.eprintf "compressing colors further %d -> " (Array.length cc);
          flush stderr;
          let compressed_cc = ThoArray.compress2 cc in
          let cc = ThoArray.uniq2 compressed_cc in
          let n = Array.length cc in
          Printf.eprintf "%d.\n" n;
          flush stderr;
          for i = 0 to pred n do
            for j = 0 to pred n do
              Printf.printf " %6.2f" cc.(i).(j)
            done;
            Printf.printf "\n"
          done;
(*i Old Lapack Code:            
          begin try
            let start = Sys.time () in
            Printf.eprintf "diagonalizing ...";
            flush stderr;
            let evals_evecs = Lapack.diagonalize cc in
            Printf.eprintf " done in %g secs.\n" (Sys.time () -. start);
            flush stderr;
            Array.sort (fun (ev1, _) (ev2, _) -> compare ev1 ev2) evals_evecs;
            for i = 0 to pred n do
              let eval, evec = evals_evecs.(i) in
              Printf.printf " eval = %8.3f, evec = [ " eval;
              for j = 0 to pred n do
                Printf.printf " %6.3f" evec.(j)
              done;
              Printf.printf " ]\n"
            done
          with
          | Lapack.Not_available -> ()
          end;
          flush stdout
i*)
        end;
        exit 0

(* \begin{dubious}
     This was only intended for debugging O'Giga \ldots
   \end{dubious} *)

    let decode wf =
      (F.flavor wf, (F.momentum wf : Momentum.Default.t))

    let diagrams in1 in2 out =
      let a = F.amplitude false C.no_cascades [in1; in2] out in
      let wf1 = List.hd (F.externals a)
      and wf2 = List.hd (List.tl (F.externals a)) in
      let wf2 = decode wf2 in
      List.map (fun t ->
        (wf2,
         Tree.map (fun (wf, _) -> decode wf) decode t,
         Color.of_tree (fun wf -> M.color (F.flavor wf)) (fun _ -> ())
           (Tree.map fst decode t)))
        (F.forest wf1 a)

  end
(*i
 *  Local Variables:
 *  mode:caml
 *  indent-tabs-mode:nil
 *  page-delimiter:"^(\\* .*\n"
 *  End:
i*)
