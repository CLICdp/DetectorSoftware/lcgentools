(* $Id: f90_QCD_Col.ml 326 2008-08-17 04:49:19Z reuter $ *)
(* Copyright (C) 2000-2004 by Thorsten Ohl <ohl@physik.uni-wuerzburg.de> et al.
   O'Mega is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by 
   the Free Software Foundation; either version 2, or (at your option)
   any later version.
   O'Mega is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.  *)  

let rcs_file = RCS.parse "F90_QCD_Col" ["QCD with colors"]
    { RCS.revision = "$Revision: 326 $";
      RCS.date = "$Date: 2008-08-17 06:49:19 +0200 (Sun, 17 Aug 2008) $";
      RCS.author = "$Author: reuter $";
      RCS.source
        = "$URL: svn+ssh://jr_reuter@login.hepforge.org/hepforge/svn/whizard/branches/1.xx/omega-src/bundle/src/f90_QCD_Col.ml $" }

(* QCD with colors. *)

module M : Model.T =
  struct
    let rcs = rcs_file

    open Coupling

    let options = Options.empty
        
    type flavor =
      | U | Ubar | D | Dbar | C | Cbar | S | Sbar
      | T | Tbar | B | Bbar | Gl
         
    let external_flavors () = 
      [ "Quarks", [U; Ubar; D; Dbar; C; Cbar; S; Sbar;
                  T; Tbar; B; Bbar];          
        "Gauge Bosons", [Gl]]
    let flavors () = ThoList.flatmap snd (external_flavors ())

    type gauge = unit
    type constant = Gs | G2 | I_Gs

    let colsymm _ = (0,false),(0,false)

    let lorentz = function
      | U | D | C | S | T | B -> Spinor
      | Ubar | Dbar | Cbar | Sbar | Tbar | Bbar -> ConjSpinor 
      | Gl -> Vector

    let color = function
      | U | D | C | S | T | B -> Color.SUN 3
      | Ubar | Dbar | Cbar | Sbar | Tbar | Bbar -> Color.SUN (-3)
      | Gl -> Color.AdjSUN 3

    let propagator = function
      | U | D | C | S | T | B -> Prop_Spinor
      | Ubar | Dbar | Cbar | Sbar | Tbar | Bbar -> Prop_ConjSpinor 
      | Gl -> Prop_Feynman

    let width _ = Timelike

    let goldstone _ =
      None

    let conjugate = function
      | U -> Ubar | Ubar -> U
      | D -> Dbar | Dbar -> D
      | C -> Cbar | Cbar -> C
      | S -> Sbar | Sbar -> S
      | T -> Tbar | Tbar -> T
      | B -> Bbar | Bbar -> B
      | Gl -> Gl

    let fermion = function
      | U | D | C | S | T | B -> 1
      | Ubar | Dbar | Cbar | Sbar | Tbar | Bbar -> -1
      | Gl -> 0

    module F = Models.Fusions (struct
      type f = flavor
      type c = constant
      let compare = compare
      let conjugate = conjugate
    end)

(* This is compatible with CD+. *)
    let color_current =
      [ ((Dbar, Gl, D), FBF ((-1), Psibar, V, Psi), Gs);
        ((Ubar, Gl, U), FBF ((-1), Psibar, V, Psi), Gs);
        ((Cbar, Gl, C), FBF ((-1), Psibar, V, Psi), Gs);
        ((Sbar, Gl, S), FBF ((-1), Psibar, V, Psi), Gs);
        ((Tbar, Gl, T), FBF ((-1), Psibar, V, Psi), Gs);
        ((Bbar, Gl, B), FBF ((-1), Psibar, V, Psi), Gs)]

   let triple_gluon =
      [ ((Gl, Gl, Gl), Gauge_Gauge_Gauge 1, I_Gs)]

    let gauge4 = Vector4 [(2, C_13_42); (-1, C_12_34); (-1, C_14_23)]
        
    let quartic_gluon = 
      [ ((Gl, Gl, Gl, Gl), gauge4, G2) ]
      
    let vertices3 = 
      color_current @ triple_gluon
       
   let vertices4 =
     quartic_gluon

    let vertices () =
      (vertices3, vertices4, [])
      
    let table = F.of_vertices (vertices ())
    let fuse2 = F.fuse2 table
    let fuse3 = F.fuse3 table
    let fuse = F.fuse table
    let max_degree () = 4

    let parameters () = { input = [Gs, 1.0]; derived = []; derived_arrays = [] } 

    let flavor_of_string = function
      | "u" -> U | "ubar" -> Ubar 
      | "d" -> D | "dbar" -> Dbar 
      | "c" -> C | "cbar" -> Cbar 
      | "s" -> S | "sbar" -> Sbar 
      | "t" -> T | "tbar" -> Tbar 
      | "b" -> B | "bbar" -> Bbar 
      | "gl" -> Gl
      | _ -> invalid_arg "Models.QCD_Col.flavor_of_string"

    let flavor_to_string = function
      | U -> "u" | Ubar -> "ubar" 
      | D -> "d" | Dbar -> "dbar"
      | C -> "c" | Cbar -> "cbar"
      | S -> "s" | Sbar -> "sbar"
      | T -> "t" | Tbar -> "tbar" 
      | B -> "b" | Bbar -> "bbar"
      | Gl -> "gl" 

    let flavor_to_TeX = function
      | U -> "u" | Ubar -> "\\bar{u}" 
      | D -> "d" | Dbar -> "\\bar{d}"
      | C -> "c" | Cbar -> "\\bar{c}"
      | S -> "s" | Sbar -> "\\bar{s}"
      | T -> "t" | Tbar -> "\\bar{t}" 
      | B -> "b" | Bbar -> "\\bar{b}"
      | Gl -> "g" 

    let flavor_symbol = function
      | U -> "upa" | Ubar -> "uan"
      | D -> "dpa" | Dbar -> "dan"
      | C -> "cpa" | Cbar -> "can"
      | S -> "spa" | Sbar -> "san" 
      | T -> "tpa" | Tbar -> "tan" 
      | B -> "bpa" | Bbar -> "ban" 
      | Gl -> "gl" 

    let gauge_symbol () =
      failwith "Models.QCD_Col.gauge_symbol: internal error"

    let pdg = function
      | D -> 1 | Dbar -> -1
      | U -> 2 | Ubar -> -2
      | S -> 3 | Sbar -> -3
      | C -> 4 | Cbar -> -4
      | B -> 5 | Bbar -> -5
      | T -> 6 | Tbar -> -6
      | Gl -> 21 

    let mass_symbol f = 
      "mass(" ^ string_of_int (abs (pdg f)) ^ ")"

    let width_symbol f =
      "width(" ^ string_of_int (abs (pdg f)) ^ ")"

    let constant_symbol = function
      | Gs -> "gs"
      | G2 -> "gs**2"
      | I_Gs -> "igs"
  end

(*i 
  We leave here the former explicitly colored version for historic reasons.


module M : Model.T =
  struct
    let rcs = rcs_file

    open Coupling

    let options = Options.empty
        
    type col = 
        Q of int

    let nc = 6

    let nc_list = 
      ThoList.range 1 nc 

    let choose2 set = 
      List.map (function [x;y] -> (x,y) | _ -> failwith "choose2") 
        (Combinatorics.choose 2 set)

    let inequ_pairs = 
      choose2 nc_list @ choose2 (List.rev nc_list)
        
    type flavor =
      | U of col | Ubar of col | D of col | Dbar of col
      | C of col | Cbar of col | S of col | Sbar of col
      | T of col | Tbar of col | B of col | Bbar of col
      | Gl of col*col | Gl0 

    let col_mult n = [U (Q n); Ubar (Q (-n)); D (Q n); Dbar (Q (-n));
                      C (Q n); Cbar (Q (-n)); S (Q n); Sbar (Q (-n));
                      T (Q n); Tbar (Q (-n)); B (Q n); Bbar (Q (-n))]
    let gl (n,m) = [Gl (Q n,Q (-m))] 
         
    let external_flavors () = 
      [ "Quarks", ThoList.flatmap col_mult nc_list;          
        "Gauge Bosons", ThoList.flatmap gl inequ_pairs @ [Gl0]]
    let flavors () = ThoList.flatmap snd (external_flavors ())

    type gauge = unit
    type constant = Gs | G2 | I_Gs

    let lorentz = function
      | U (Q n) | D (Q n) | C (Q n) | S (Q n) | T (Q n) | B (Q n) 
        when n > 0 -> Spinor
      | Ubar (Q n) | Dbar (Q n) | Cbar (Q n) | Sbar (Q n) 
      | Tbar (Q n) | Bbar (Q n) when n < 0 -> ConjSpinor 
      | Gl (_,_) | Gl0 -> Vector
      | _ -> invalid_arg "Models.QCD_Col.lorentz"

    let color = function
      | U _ | D _ | C _ | S _ | T _ | B _ -> Color.SUN 3
      | Ubar _ | Dbar _ | Cbar _ | Sbar _ | Tbar _ | Bbar _ -> Color.SUN (-3)
      | Gl (_,_) -> Color.AdjSUN 3
      | _ -> Color.Singlet

    let propagator = function
      | U (Q n) | D (Q n) | C (Q n) | S (Q n) | T (Q n) | B (Q n)  
        when n > 0 -> Prop_Spinor
      | U _ | D _ | C _ | S _ | T _ | B _ -> 
          invalid_arg "internal error: propagator" 
      | Ubar (Q n) | Dbar (Q n) | Cbar (Q n) | Sbar (Q n) 
      | Tbar (Q n) | Bbar (Q n) when n < 0 -> Prop_ConjSpinor 
      | Ubar _ | Dbar _ | Cbar _ | Sbar _ | Tbar _ | Bbar _ -> 
          invalid_arg "internal_error: propagator"
      | Gl (_,_) -> Prop_Feynman
      | Gl0 -> Prop_Col_Feynman

    let width _ = Timelike

    let goldstone _ =
      None

    let conjugate = function
      | U (Q n) when n > 0 -> Ubar (Q (-n))
      | D (Q n) when n > 0 -> Dbar (Q (-n))
      | C (Q n) when n > 0 -> Cbar (Q (-n))
      | S (Q n) when n > 0 -> Sbar (Q (-n))
      | T (Q n) when n > 0 -> Tbar (Q (-n))
      | B (Q n) when n > 0 -> Bbar (Q (-n))
      | Ubar (Q n) when n < 0 -> U (Q (-n))
      | Dbar (Q n) when n < 0 -> D (Q (-n))
      | Cbar (Q n) when n < 0 -> C (Q (-n))
      | Sbar (Q n) when n < 0 -> S (Q (-n))
      | Tbar (Q n) when n < 0 -> T (Q (-n))
      | Bbar (Q n) when n < 0 -> B (Q (-n))
      | Gl (Q n, Q m) when n > 0 && m < 0 -> Gl (Q (-m), Q (-n))
      | Gl0 -> Gl0 
      | _ -> invalid_arg "Models.QCD_Col.conjugate"

    let fermion = function
      | U _ | D _ | C _ | S _ | T _ | B _ -> 1
      | Ubar _ | Dbar _ | Cbar _ | Sbar _ | Tbar _ | Bbar _ -> -1
      | Gl (_,_) -> 0 | Gl0 -> 0

    let colsymm = function
      | D _ -> (1, true), (0, false)
      | U _ -> (2, true), (0, false)
      | C _ -> (3, true), (0, false)
      | S _ -> (4, true), (0, false)
      | T _ -> (5, true), (0, false)
      | B _ -> (6, true), (0, false)
      | Dbar _ -> (-1, true), (0, false)
      | Ubar _ -> (-2, true), (0, false)
      | Cbar _ -> (-3, true), (0, false)
      | Sbar _ -> (-4, true), (0, false)
      | Tbar _ -> (-5, true), (0, false)
      | Bbar _ -> (-6, true), (0, false)
      | Gl _ -> (3, true), (0, false)
      | Gl0 -> (3, true), (3, true)

    module F = Models.Fusions (struct
      type f = flavor
      type c = constant
      let compare = compare
      let conjugate = conjugate
    end)

    let col_nonsinglet_current (c1,c2) =
      [ ((Dbar (Q (-c2)), Gl (Q c2,Q (-c1)), D (Q c1)), 
                     FBF ((-1), Psibar, V, Psi), Gs);
        ((Ubar (Q (-c2)), Gl (Q c2,Q (-c1)), U (Q c1)), 
                     FBF ((-1), Psibar, V, Psi), Gs);
        ((Cbar (Q (-c2)), Gl (Q c2,Q (-c1)), C (Q c1)), 
                     FBF ((-1), Psibar, V, Psi), Gs);
        ((Sbar (Q (-c2)), Gl (Q c2,Q (-c1)), S (Q c1)), 
                     FBF ((-1), Psibar, V, Psi), Gs);
        ((Tbar (Q (-c2)), Gl (Q c2,Q (-c1)), T (Q c1)), 
                     FBF ((-1), Psibar, V, Psi), Gs);
        ((Bbar (Q (-c2)), Gl (Q c2,Q (-c1)), B (Q c1)), 
                     FBF ((-1), Psibar, V, Psi), Gs)]

    let col_singlet_current c1 =
      [ ((Dbar (Q (-c1)), Gl0, D (Q c1)), FBF ((-1), Psibar, V, Psi), Gs);
        ((Ubar (Q (-c1)), Gl0, U (Q c1)), FBF ((-1), Psibar, V, Psi), Gs);
        ((Cbar (Q (-c1)), Gl0, C (Q c1)), FBF ((-1), Psibar, V, Psi), Gs);
        ((Sbar (Q (-c1)), Gl0, S (Q c1)), FBF ((-1), Psibar, V, Psi), Gs);
        ((Tbar (Q (-c1)), Gl0, T (Q c1)), FBF ((-1), Psibar, V, Psi), Gs);
        ((Bbar (Q (-c1)), Gl0, B (Q c1)), FBF ((-1), Psibar, V, Psi), Gs)]

    let triple_col = 
      List.map (function [x;y;z] -> (x,y,z) | _ -> 
        failwith "triple_col") (Combinatorics.choose 3 nc_list)

    let quartic_col = 
      List.map (function [r;s;t;u] -> (r,s,t,u) | _ -> 
        failwith "quartic_col") (Combinatorics.choose 4 nc_list)

   let three_gluon (c1,c2,c3) =
      [ ((Gl (Q c1,Q (-c3)), Gl (Q c2,Q (-c1)), Gl (Q c3,Q (-c2))), 
         Gauge_Gauge_Gauge 1, I_Gs);
        ((Gl (Q c1,Q (-c2)), Gl (Q c3,Q (-c1)), Gl (Q c2,Q (-c3))), 
         Gauge_Gauge_Gauge 1, I_Gs)]   

    let gauge4 = Vector4 [(2, C_13_42); (-1, C_12_34); (-1, C_14_23)]
    let minus_gauge4 = Vector4 [(-2, C_13_42); (1, C_12_34); (1, C_14_23)]
        
    let four_gluon (c1,c2,c3,c4) = 
      [ ((Gl (Q c1,Q (-c4)), Gl (Q c2,Q (-c1)), 
          Gl (Q c3,Q (-c2)), Gl (Q c4,Q (-c3))), gauge4, G2);
        ((Gl (Q c1,Q (-c4)), Gl (Q c3,Q (-c1)), 
          Gl (Q c2,Q (-c3)), Gl (Q c4,Q (-c2))), gauge4, G2);
        ((Gl (Q c1,Q (-c3)), Gl (Q c4,Q (-c1)), 
          Gl (Q c2,Q (-c4)), Gl (Q c3,Q (-c2))), gauge4, G2);
        ((Gl (Q c1,Q (-c2)), Gl (Q c4,Q (-c1)), 
          Gl (Q c3,Q (-c4)), Gl (Q c2,Q (-c3))), gauge4, G2);
        ((Gl (Q c1,Q (-c3)), Gl (Q c2,Q (-c1)), 
          Gl (Q c4,Q (-c2)), Gl (Q c3,Q (-c4))), gauge4, G2);
        ((Gl (Q c1,Q (-c2)), Gl (Q c3,Q (-c1)), 
          Gl (Q c4,Q (-c3)), Gl (Q c2,Q (-c4))), gauge4, G2)]

      
    let vertices3 = 
      (ThoList.flatmap col_nonsinglet_current inequ_pairs @  
       ThoList.flatmap col_singlet_current nc_list @
       ThoList.flatmap three_gluon triple_col)
       
   let vertices4 =
     (ThoList.flatmap four_gluon quartic_col)

    let vertices () =
      (vertices3, vertices4, [])
      
    let table = F.of_vertices (vertices ())
    let fuse2 = F.fuse2 table
    let fuse3 = F.fuse3 table
    let fuse = F.fuse table
    let max_degree () = 4

    let parameters () = { input = [Gs, 1.0]; derived = []; derived_arrays = [] } 

    let read_color s =
      try
        let offset = (String.index s '/') in 
        let colnum = 
           (String.sub s (succ offset) (String.length s - offset - 1)) in
	if int_of_string colnum > 10 && int_of_string colnum > 0 then
          let s1 = int_of_string (Char.escaped (String.get colnum 0))
          and s2 = int_of_string (Char.escaped (String.get colnum 1)) in
          if s1 > nc or s2 > nc then	
	    invalid_arg "Number of explicit color flows in O'Mega exceeded"	
	  else	
            (s1, s2, String.sub s 0 offset)
        else if int_of_string colnum > nc then
          invalid_arg "Number of explicit color flows in O'Mega exceeded"
	else	
          (int_of_string (String.sub s (succ offset) (String.length s - offset - 1)),
           0,
           String.sub s 0 offset)
      with
      | Not_found -> (0, 0, s)

    let flavor_of_string' = function
      | n, 0, "u" -> U (Q n)
      | n, 0, "d" -> D (Q n)
      | n, 0, "c" -> C (Q n)
      | n, 0, "s" -> S (Q n)
      | n, 0, "t" -> T (Q n)
      | n, 0, "b" -> B (Q n)
      | n, 0, "ubar" -> Ubar (Q (-n))
      | n, 0, "dbar" -> Dbar (Q (-n)) 
      | n, 0, "cbar" -> Cbar (Q (-n))
      | n, 0, "sbar" -> Sbar (Q (-n)) 
      | n, 0, "tbar" -> Tbar (Q (-n))
      | n, 0, "bbar" -> Bbar (Q (-n)) 
      | n, m, "gl" when n = m -> invalid_arg "Models.QCD_Col.flavor_of_string"
      | n, m, "gl" -> Gl (Q n, Q (- m))  
      | 0, 0, "gl0" -> Gl0
      | _, _, _ -> invalid_arg "Models.QCD_Col.flavor_of_string"

    let flavor_of_string s = 
      flavor_of_string' (read_color s)

    let flavor_to_string = function
      | U (Q n) when n > 0 -> "u/" ^ string_of_int n
      | Ubar (Q n) when n < 0 -> "ubar/" ^ string_of_int (-n)
      | D (Q n) when n > 0 -> "d/" ^ string_of_int n
      | Dbar (Q n) when n < 0 -> "dbar/" ^ string_of_int (-n)
      | C (Q n) when n > 0 -> "c/" ^ string_of_int n
      | Cbar (Q n) when n < 0 -> "cbar/" ^ string_of_int (-n)
      | S (Q n) when n > 0 -> "s/" ^ string_of_int n
      | Sbar (Q n) when n < 0 -> "sbar/" ^ string_of_int (-n)
      | T (Q n) when n > 0 -> "t/" ^ string_of_int n
      | Tbar (Q n) when n < 0 -> "tbar/" ^ string_of_int (-n)
      | B (Q n) when n > 0 -> "b/" ^ string_of_int n
      | Bbar (Q n) when n < 0 -> "bbar/" ^ string_of_int (-n)
      | Gl (Q n, Q m) when n > 0 && m < 0 -> 
          "gl/" ^ string_of_int n ^ string_of_int (-m)
      | Gl0 -> "gl0"
      | _ -> invalid_arg "Models.QCD_Col.flavor_to_string"

    let flavor_symbol = function
      | U (Q n) when n > 0 -> "upa_" ^ string_of_int n 
      | Ubar (Q n) when n < 0 -> "uan_" ^ string_of_int (-n)
      | D (Q n) when n > 0 -> "dpa_" ^ string_of_int n
      | Dbar (Q n) when n < 0 -> "dan_" ^ string_of_int (-n)
      | C (Q n) when n > 0 -> "cpa_" ^ string_of_int n 
      | Cbar (Q n) when n < 0 -> "can_" ^ string_of_int (-n)
      | S (Q n) when n > 0 -> "spa_" ^ string_of_int n
      | Sbar (Q n) when n < 0 -> "san_" ^ string_of_int (-n)
      | T (Q n) when n > 0 -> "tpa_" ^ string_of_int n 
      | Tbar (Q n) when n < 0 -> "tan_" ^ string_of_int (-n)
      | B (Q n) when n > 0 -> "bpa_" ^ string_of_int n
      | Bbar (Q n) when n < 0 -> "ban_" ^ string_of_int (-n)
      | Gl (Q n,Q m) when n > 0 && m < 0 -> 
          "gl_" ^ string_of_int n ^ string_of_int (-m)
      | Gl0 -> "gl0" 
      | _ -> invalid_arg "Models.QCD_Col.flavor_symbol"

    let gauge_symbol () =
      failwith "Models.QCD_Col.gauge_symbol: internal error"

    let pdg = function
      | D _ -> 1 | Dbar _ -> -1
      | U _ -> 2 | Ubar _ -> -2
      | S _ -> 3 | Sbar _ -> -3
      | C _ -> 4 | Cbar _ -> -4
      | B _ -> 5 | Bbar _ -> -5
      | T _ -> 6 | Tbar _ -> -6
      | Gl _ | Gl0 -> 21 

    let mass_symbol f = 
      "mass(" ^ string_of_int (abs (pdg f)) ^ ")"

    let width_symbol f =
      "width(" ^ string_of_int (abs (pdg f)) ^ ")"

    let constant_symbol = function
      | Gs -> "gs"
      | G2 -> "gs**2"
      | I_Gs -> "igs"
  end

module O = Omega.Make(Fusion.Mixed23)(Targets.Fortran)(M)
let _ = O.main ()

i*)

module O = Omega.Make(Fusion.Mixed23)(Targets.Fortran)
    (Colorize.It (struct let max_num = 8 end)(M))
let _ = O.main ()


(*i
 *  Local Variables:
 *  mode:caml
 *  indent-tabs-mode:nil
 *  page-delimiter:"^(\\* .*\n"
 *  End:
i*)
