/* (* $Id: omega_parser.mly,v 1.4 2004/06/22 09:30:18 ohl Exp $ *)
(* Copyright (C) 2000-2004 by Thorsten Ohl <ohl@physik.uni-wuerzburg.de> et al.
   O'Mega is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by 
   the Free Software Foundation; either version 2, or (at your option)
   any later version.
   O'Mega is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.  *)  */
%{
%}

%token < string > STRING
%token OPEN CLOSE
%token OUTPUT WHIZARD PROCESS TARGET MODEL
%token END

%start file
%type < Omega_syntax.command list > file

%%

file:
    END                  { [] }
  | commands END         { $1 }
;
commands:
    command              { [ $1 ] }
  | command commands     { $1 :: $2 }
;
command:
    MODEL STRING STRING  { Omega_syntax.Model ($2, $3) }
  | TARGET STRING STRING { Omega_syntax.Target ($2, $3) }
  | PROCESS STRING STRING particles
                         { Omega_syntax.Process ([$2; $3], $4) }
  | WHIZARD STRING OPEN commands CLOSE 
                         { Omega_syntax.Whizard ($2, $4) }
  | OUTPUT STRING OPEN commands CLOSE 
                         { Omega_syntax.Output ($2, $4) }
;
particles:
    STRING               { [ $1 ] }
  | STRING particles     { $1 :: $2 }
;
