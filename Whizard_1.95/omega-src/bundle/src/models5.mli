(* $Id: models3.mli 92 2007-11-29 16:38:05Z reuter $ *)
(* Copyright (C) 2000-2004 by Thorsten Ohl <ohl@physik.uni-wuerzburg.de> et al.
   O'Mega is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by 
   the Free Software Foundation; either version 2, or (at your option)
   any later version.
   O'Mega is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.  *)  

(* \thocwmodulesection{More Hardcoded BSM Models} *)

module type BSM_flags = 
  sig 
    val u1_gauged         : bool
    val whiz_col          : bool
    val anom_ferm_ass     : bool
  end

module BSM_bsm : BSM_flags
module BSM_ungauged : BSM_flags
module BSM_anom : BSM_flags
module BSM_bsm_Col : BSM_flags
module BSM_ungauged_Col : BSM_flags
module BSM_anom_Col : BSM_flags
module Littlest : functor (F: BSM_flags) -> Model.Gauge
module Littlest_Tpar : functor (F: BSM_flags) -> Model.T
module Simplest : functor (F: BSM_flags) -> Model.T
module Xdim : functor (F: BSM_flags) -> Model.Gauge
module UED : functor (F: BSM_flags) -> Model.Gauge
module GravTest : functor (F: BSM_flags) -> Model.Gauge
module Template : functor (F : BSM_flags) -> Model.Gauge



(*i
 *  Local Variables:
 *  mode:caml
 *  indent-tabs-mode:nil
 *  page-delimiter:"^(\\* .*\n"
 *  End:
i*)
