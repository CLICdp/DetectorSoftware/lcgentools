(* $Id: cache.mli 68 2007-11-22 11:11:19Z ohl $ *)
(* Copyright (C) 2005 by Thorsten Ohl <ohl@physik.uni-wuerzburg.de> et al.
   O'Mega is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by 
   the Free Software Foundation; either version 2, or (at your option)
   any later version.
   O'Mega is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.  *)  

type hash = string

val md5_hash : 'a -> hash
val write : ?dir:string -> ?pfx:string -> hash -> 'a -> unit
val read : ?dir:string -> ?pfx:string -> hash -> 'a
val maybe_read : ?dir:string -> ?pfx:string -> hash -> 'a option

val set_directory : string -> unit
val set_prefix : string -> unit

(*i
 *  Local Variables:
 *  mode:caml
 *  indent-tabs-mode:nil
 *  page-delimiter:"^(\\* .*\n"
 *  End:
i*)
