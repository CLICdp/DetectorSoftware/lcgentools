(* $Id: targets.ml 326 2008-08-17 04:49:19Z reuter $ *)
(* Copyright (C) 2000-2004 by Thorsten Ohl <ohl@physik.uni-wuerzburg.de> et al.
   O'Mega is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by 
   the Free Software Foundation; either version 2, or (at your option)
   any later version.
   O'Mega is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.  *)  

(* \begin{dubious}
     Can be set to [true] when WK disables the corresponding pieces
     in his perl script:
   \end{dubious} *)
let new_whizard = false

let rcs_file = RCS.parse "Targets" ["Code Generation"]
    { RCS.revision = "$Revision: 326 $";
      RCS.date = "$Date: 2008-08-17 06:49:19 +0200 (Sun, 17 Aug 2008) $";
      RCS.author = "$Author: reuter $";
      RCS.source
        = "$Source: /home/sources/ohl/ml/omega/src/targets.ml,v $" }

module Dummy (F : Fusion.T) (MF : Fusion.MultiMaker)
    (M : Model.T with type flavor = F.flavor and type constant = F.constant) =
  struct
    let rcs_list = []
    type amplitude = F.amplitude
    type amplitudes = MF(F).amplitudes
    type diagnostic = All | Arguments | Helicities | Momenta | Gauge
    let options = Options.empty
    let amplitude_to_channel oc amplitude = failwith "Targets.Dummy"
    let amplitudes_to_channel oc amplitudes = failwith "Targets.Dummy"
    let parameters_to_channel oc = failwith "Targets.Dummy"
  end

(* \thocwmodulesection{Shared \texttt{Fortran\,90/95}} *)

module Common_Fortran (F : Fusion.T) (M : Model.T with type
      flavor = F.flavor and type constant = F.constant) =
  struct
    let rcs = RCS.rename rcs_file "Targets.Common_Fortran()"
        [ "generates Fortran95 code" ]

    open Coupling
    open Format

    let line_length = ref 80
    let kind = ref "omega_prec"
    let fortran95 = ref true
    let function_name = ref "omega"
    let module_name = ref "omega_amplitude"
    let use_modules = ref []
    let whizard = ref false
    let whiz_qcd = ref false        
    let parameter_module = ref "omega_parameters"
    let no_write = ref false        
    let annotate_orders = ref false
    let km_write = ref false
    let km_pure = ref false

    let options = Options.create
      [ "function", Arg.String (fun s -> function_name := s),
        "function name";
        "90", Arg.Clear fortran95,
        "don't use Fortran95 features that are not in Fortran90";
        "kind", Arg.String (fun s -> kind := s),
        "real and complex kind (default: " ^ !kind ^ ")";
        "width", Arg.Int (fun w -> line_length := w), "approx. line length";
        "module", Arg.String (fun s -> module_name := s), "module name";
        "use", Arg.String (fun s -> use_modules := s :: !use_modules),
        "use module";
        "parameter_module", Arg.String (fun s -> parameter_module := s),
        "parameter_module";
        "whizard", Arg.Unit (fun () ->
          use_modules :=
            ["parameters"; "omega_parameters_whizard"] @ !use_modules;
          whizard := true),
        "include WHIZARD interface";
        "no_write", Arg.Set no_write, "no 'write' statements";
        "whiz_qcd", Arg.Set whiz_qcd, "use Whizard color scripting";
        "orders", Arg.Set annotate_orders, "experimental";
        "kmatrix_write", Arg.Set km_write, "write K matrix functions";
        "kmatrix_write_pure", Arg.Set km_pure, "write K matrix pure functions"]

(* Fortran style line continuation: *)

    let continuing = ref true

    let nl () =
      continuing := false;
      print_newline ();
      continuing := true

    let wrap_newline () =
      let out, flush, newline, space = get_all_formatter_output_functions () in
      let newline' () = if !continuing then out " &" 0 2; newline () in
      set_all_formatter_output_functions out flush newline' space

    let print_list = function 
      | [] -> ()
      | a :: rest ->
          print_string a;
          List.iter (fun s -> printf ",@ %s" s) rest

(* \thocwmodulesubsection{Variables and Declarations} *)

    let p2s p =
      if p >= 0 && p <= 9 then
        string_of_int p
      else if p <= 36 then
        String.make 1 (Char.chr (Char.code 'A' + p - 10))
      else
        "_"

    let format_momentum p =
      "p" ^ String.concat "" (List.map p2s p)

    let format_p wf =
      String.concat "" (List.map p2s (F.momentum_list wf))

    let ext_momentum wf =
      match F.momentum_list wf with
      | [n] -> n
      | _ -> -42 (* \textbf{TEMPORARY HACK!!!} *)
      | _ -> invalid_arg "Targets.Fortran.ext_momentum"

    module P = Set.Make (struct type t = int list let compare = compare end)

    let add_tag wf name =
      match F.wf_tag wf with
      | None -> name
      | Some tag -> name ^ "_" ^ tag

    let variable wf =
      add_tag wf (M.flavor_symbol (F.flavor wf) ^ "_" ^ format_p wf)

    let momentum wf = "p" ^ format_p wf
    let spin wf = "s(" ^ string_of_int (ext_momentum wf) ^ ")"

    let declare_list t = function
      | [] -> ()
      | wfs ->
          printf "    @[<2>%s :: " t; print_list (List.map variable wfs); nl ()

    type declarations =
        { scalars : F.wf list;
          spinors : F.wf list;
          conjspinors : F.wf list;
          realspinors : F.wf list;
          ghostspinors : F.wf list;
          vectorspinors : F.wf list;
          vectors : F.wf list;
          ward_vectors : F.wf list;
          massive_vectors : F.wf list;
          tensors_1 : F.wf list;
          tensors_2 : F.wf list;
          brs_scalars : F.wf list;
          brs_spinors : F.wf list;
          brs_conjspinors : F.wf list;
          brs_realspinors : F.wf list; 
          brs_vectorspinors : F.wf list;
          brs_vectors : F.wf list;
          brs_massive_vectors : F.wf list }

    let rec classify_wfs' acc = function
      | [] -> acc
      | wf :: rest ->
          classify_wfs' 
            (match M.lorentz (F.flavor wf) with
            | Scalar -> {acc with scalars = wf :: acc.scalars}
            | Spinor -> {acc with spinors = wf :: acc.spinors}
            | ConjSpinor -> {acc with conjspinors = wf :: acc.conjspinors}
            | Majorana -> {acc with realspinors = wf :: acc.realspinors}
            | Maj_Ghost -> {acc with ghostspinors = wf :: acc.ghostspinors}
            | Vectorspinor -> 
                {acc with vectorspinors = wf :: acc.vectorspinors}
            | Vector -> {acc with vectors = wf :: acc.vectors}
(*i            | Ward_Vector -> {acc with ward_vectors = wf :: acc.ward_vectors}
i*)  
            | Massive_Vector ->
                {acc with massive_vectors = wf :: acc.massive_vectors}
            | Tensor_1 -> {acc with tensors_1 = wf :: acc.tensors_1}
            | Tensor_2 -> {acc with tensors_2 = wf :: acc.tensors_2}  
            | BRS Scalar -> {acc with brs_scalars = wf :: acc.brs_scalars} 
            | BRS Spinor -> {acc with brs_spinors = wf :: acc.brs_spinors}
            | BRS ConjSpinor -> {acc with brs_conjspinors = 
                                 wf :: acc.brs_conjspinors}  
            | BRS Majorana -> {acc with brs_realspinors = 
                               wf :: acc.brs_realspinors}
            | BRS Vectorspinor -> {acc with brs_vectorspinors =
                                   wf :: acc.brs_vectorspinors}
            | BRS Vector -> {acc with brs_vectors = wf :: acc.brs_vectors}
            | BRS Massive_Vector -> {acc with brs_massive_vectors =
                                     wf :: acc.brs_massive_vectors}
            | BRS _ -> invalid_arg "Targets.wfs_classify': not needed here")
            rest

    let classify_wfs wfs = classify_wfs'
        { scalars = []; spinors = []; conjspinors = []; realspinors = [];
          ghostspinors = []; vectorspinors = []; vectors = []; 
          ward_vectors = [];
          massive_vectors = []; tensors_1 = []; tensors_2 = []; 
          brs_scalars = [] ; brs_spinors = []; brs_conjspinors = []; 
          brs_realspinors = []; brs_vectorspinors = [];
          brs_vectors = []; brs_massive_vectors = []}
        wfs

    let helicities f =
      let rec helicity_of_lorentz = function
        | Scalar  -> [0]
        | Spinor | ConjSpinor | Majorana | Maj_Ghost -> [-1; 1]
        | Vector (*i | Ward_Vector i*) -> [-1; 1]
        | Massive_Vector -> [-1; 0; 1]
        | Tensor_1 -> [-1; 0; 1]
        | Vectorspinor -> [-2; -1; 1; 2]
        | Tensor_2 -> [-2; -1; 0; 1; 2]
        | BRS f -> helicity_of_lorentz f in
      helicity_of_lorentz (M.lorentz f)

(* \thocwmodulesubsection{WHIZARD} *)

    let flavors a = F.incoming a @ F.outgoing a

    let polarization_states f =
      let rec polarization_state_of_lorentz = function
        | Scalar  -> 1
        | Spinor | ConjSpinor | Majorana | Maj_Ghost -> 2
        | Vector (*i | Ward_Vector i*) -> 2
        | Massive_Vector -> 3
        | Tensor_1 -> 3
        | Vectorspinor -> 4
        | Tensor_2 -> 5
        | BRS f -> polarization_state_of_lorentz f in
      polarization_state_of_lorentz (M.lorentz f)

    let print_whizard_declarations a =
      let f = flavors a in
      let n = List.length f
      and pols = List.map polarization_states f
      and pdgs = List.map M.pdg f in
      printf "  public :: n_in, n_out, pdg_code"; nl ();
      printf "  public :: allow_helicities"; nl ();
      printf "  public :: create, destroy"; nl ();
      printf "  public :: set_const, sqme"; nl ();
      printf "  integer, dimension(%d)" (List.fold_left ( * ) 1 pols);
      printf ", save, private :: zero = 0"; nl ();
      printf "  integer, save, private :: num_call = 0"; nl ();
      printf "  @[<2>integer, dimension(%d), parameter, private ::" n;
      printf " @,states = (/ ";
      print_list (List.map string_of_int pols);
      printf " /),@ pdg_codes = (/ ";
      print_list (List.map string_of_int pdgs);
      printf " /) "; nl ()

    let print_whizard_functions a =
      let nin = 2 in
      let nout = List.length (F.externals a) - 2 in
      printf "  function n_in () result (n)"; nl ();
      printf "    integer :: n"; nl ();
      printf "    n = %d" nin; nl ();
      printf "  end function n_in"; nl ();
      printf "  function n_out () result (n)"; nl ();
      printf "    integer :: n"; nl ();
      printf "    n = %d" nout; nl ();
      printf "  end function n_out"; nl ();
      printf "  function pdg_code (i) result (n)"; nl ();
      printf "    integer, intent(in) :: i"; nl ();
      printf "    integer :: n"; nl ();
      printf "    if (i >= 1 .and. i <= size (pdg_codes)) then"; nl ();
      printf "       n = pdg_codes(i)"; nl ();
      printf "    else"; nl ();
      printf "       n = 0"; nl ();
      printf "    end if"; nl ();
      printf "  end function pdg_code"; nl ();
      printf "  function allow_helicities () result (yorn)"; nl ();
      printf "    logical :: yorn"; nl ();
      printf "    yorn = .true."; nl ();
      printf "  end function allow_helicities"; nl ();
      printf "  subroutine create ()"; nl ();
      printf "    zero = 0"; nl ();
      printf "    num_call = 0"; nl ();
      printf "  end subroutine create"; nl ();
      printf "  subroutine destroy ()"; nl ();
      printf "  end subroutine destroy"; nl ();
      printf "  subroutine set_const (par)"; nl ();
      printf "    type(parameter_set), intent(in) :: par"; nl ();
      printf "    call import_from_whizard (par)"; nl ();
      printf "  end subroutine set_const"; nl ();
      printf "  function sqme (p, h) result (s)"; nl ();
      printf "    real(omega_prec), dimension(0:,:), intent(in) :: p";
      nl ();
      printf "    integer, dimension(:), intent(in), optional :: h";
      nl ();
      printf "    real(omega_prec) :: s"; nl ();
      printf "    complex(omega_prec) :: a"; nl ();
      printf "    num_call = num_call + 1"; nl ();
      printf "    if (present(h)) then"; nl ();
      printf "      @[<2>a = omega_nonzero@ ";
      printf "(%s, p, h, zero, num_call, states)" !function_name; nl ();
      printf "      s = conjg (a) * a"; nl ();
      printf "    else"; nl ();
      printf "      @[<2>s = omega_sum_nonzero@ ";
      printf "(%s, p, zero, num_call, states)" !function_name; nl ();
      printf "    end if"; nl ();
      printf "  end function sqme"; nl ()

(* \thocwmodulesubsection{Parameters} *)

    type 'a parameters =
        { real_singles : 'a list;
          real_arrays : ('a * int) list;
          complex_singles : 'a list;
          complex_arrays : ('a * int) list }

    let rec classify_singles acc = function
      | [] -> acc
      | Real p :: rest -> classify_singles
            { acc with real_singles = p :: acc.real_singles } rest
      | Complex p :: rest -> classify_singles
            { acc with complex_singles = p :: acc.complex_singles } rest

    let rec classify_arrays acc = function
      | [] -> acc
      | (Real_Array p, rhs) :: rest -> classify_arrays
            { acc with real_arrays =
              (p, List.length rhs) :: acc.real_arrays } rest
      | (Complex_Array p, rhs) :: rest -> classify_arrays
            { acc with complex_arrays =
              (p, List.length rhs) :: acc.complex_arrays } rest

    let classify_parameters params =
      classify_arrays
        (classify_singles
           { real_singles = [];
             real_arrays = [];
             complex_singles = [];
             complex_arrays = [] }
           (List.map fst params.derived)) params.derived_arrays


    let rec schisma n l = 
      if List.length l <= n then
        [l]
      else
        let a, b = ThoList.splitn n l in
        [a] @ (schisma n b)

    let rec schisma_num i n l = 
      if List.length l <= n then
        [(i,l)]
      else
        let a, b = ThoList.splitn n l in
        [(i,a)] @ (schisma_num (i+1) n b)

    let declare_parameters' t = function        
      | [] -> ()
      | plist -> 
          printf "  @[<2>%s(kind=%s), public, save :: " t !kind;
          print_list (List.map M.constant_symbol plist); nl ()

    let declare_parameters t plist = 
      List.iter (declare_parameters' t) plist 

    let declare_parameter_array t (p, n) =
      printf "  @[<2>%s(kind=%s), dimension(%d), public, save :: %s"
        t !kind n (M.constant_symbol p); nl ()

    let default_parameter (x, v) =
      printf "@ %s = %g_%s" (M.constant_symbol x) v !kind

    let declare_default_parameters t = function
      | [] -> ()
      | p :: plist ->
          printf "  @[<2>%s(kind=%s), public, save ::" t !kind;
          default_parameter p;
          List.iter (fun p' -> printf ","; default_parameter p') plist;
          nl ()

    let rec format_constant = function
      | I -> sprintf "cmplx (0.0_%s, 1.0_%s)" !kind !kind
      | Const c when c < 0 -> sprintf "(%d.0_%s)" c !kind
      | Const c -> sprintf "%d.0_%s" c !kind
      | _ -> invalid_arg "format_constant"

    let rec eval_parameter' = function
      | I -> printf "cmplx (0.0_%s, 1.0_%s)" !kind !kind
      | Const c when c < 0 -> printf "(%d.0_%s)" c !kind
      | Const c -> printf "%d.0_%s" c !kind
      | Atom x -> printf "%s" (M.constant_symbol x)
      | Sum [] -> printf "0.0_%s" !kind
      | Sum [x] -> eval_parameter' x
      | Sum (x :: xs) ->
          printf "@,("; eval_parameter' x;
          List.iter (fun x -> printf "@, + "; eval_parameter' x) xs;
          printf ")"
      | Diff (x, y) ->
          printf "@,("; eval_parameter' x;
          printf " - "; eval_parameter' y; printf ")"
      | Neg x -> printf "@,( - "; eval_parameter' x; printf ")"
      | Prod [] -> printf "1.0_%s" !kind
      | Prod [x] -> eval_parameter' x
      | Prod (x :: xs) ->
          printf "@,("; eval_parameter' x;
          List.iter (fun x -> printf " * "; eval_parameter' x) xs;
          printf ")"
      | Quot (x, y) ->
          printf "@,("; eval_parameter' x;
          printf " / "; eval_parameter' y; printf ")"
      | Rec x ->
          printf "@, (1.0_%s / " !kind; eval_parameter' x; printf ")"
      | Pow (x, n) ->
          printf "@,("; eval_parameter' x; printf "**%d" n; printf ")"
      | Sqrt x -> printf "@,sqrt ("; eval_parameter' x; printf ")"
      | Sin x -> printf "@,sin ("; eval_parameter' x; printf ")"
      | Cos x -> printf "@,cos ("; eval_parameter' x; printf ")"
      | Tan x -> printf "@,tan ("; eval_parameter' x; printf ")"
      | Cot x -> printf "@,cot ("; eval_parameter' x; printf ")"
      | Atan2 (y, x) -> printf "@,atan2 ("; eval_parameter' y;
          printf ",@ "; eval_parameter' x; printf ")"
      | Conj x -> printf "@,conjg ("; eval_parameter' x; printf ")"

    let strip_single_tag = function
      | Real x -> x
      | Complex x -> x

    let strip_array_tag = function
      | Real_Array x -> x
      | Complex_Array x -> x

    let eval_parameter (lhs, rhs) =
      let x = M.constant_symbol (strip_single_tag lhs) in
      printf "    @[<2>%s = " x; eval_parameter' rhs; nl ()

    let eval_para_list n l = 
      printf " subroutine setup_parameters%s ()" (string_of_int n); nl();
      List.iter eval_parameter l;
      printf " end subroutine setup_parameters%s" (string_of_int n); nl()

    let eval_parameter_pair (lhs, rhs) =
      let x = M.constant_symbol (strip_array_tag lhs) in
      let _ = List.fold_left (fun i rhs' ->
        printf "    @[<2>%s(%d) = " x i; eval_parameter' rhs'; nl ();
        succ i) 1 rhs in
      ()

    let eval_para_pair_list n l =
      printf " subroutine setup_parameters%s ()" (string_of_int n); nl();
      List.iter eval_parameter_pair l;
      printf " end subroutine setup_parameters%s" (string_of_int n); nl()

    let print_echo fmt p =
      let s = M.constant_symbol p in
      printf "    write (unit = *, fmt = fmt_%s) \"%s\", %s"
        fmt s s; nl ()
        
    let print_echo_array fmt (p, n) =
      let s = M.constant_symbol p in
      for i = 1 to n do
        printf "    write (unit = *, fmt = fmt_%s_array) " fmt ;
        printf "\"%s\", %d, %s(%d)" s i s i; nl ()
      done

    let parameters_to_fortran oc params =
      set_formatter_out_channel oc;
      set_margin !line_length;
      wrap_newline ();
      let declarations = classify_parameters params in
      printf "module %s" !parameter_module; nl ();
      printf "  use omega_kinds"; nl ();
      printf "  use omega_constants"; nl ();
      printf "  implicit none"; nl ();
      printf "  private"; nl ();
      printf "  @[<2>public :: setup_parameters";
      if !no_write then begin 
        printf "! No print_parameters"; nl();
      end else begin 
        printf "@,, print_parameters"; nl ();
      end;  
      declare_default_parameters "real" params.input;
      declare_parameters "real" (schisma 69 declarations.real_singles);
      List.iter (declare_parameter_array "real") declarations.real_arrays;
      declare_parameters "complex" (schisma 69 declarations.complex_singles);
      List.iter (declare_parameter_array "complex") declarations.complex_arrays;
      printf "contains"; nl ();
      printf "    ! derived parameters:"; nl ();
      let shredded = schisma_num 1 120 params.derived in 
      let shredded_arrays = schisma_num 1 120 params.derived_arrays in 
         let num_sub = List.length shredded in 
         let num_sub_arrays = List.length shredded_arrays in 
      printf "     !length: %s" (string_of_int (List.length params.derived)); 
         nl(); 
      printf "     !Num_Sub: %s" (string_of_int num_sub); nl();
      List.iter (fun (i,l) -> eval_para_list i l) shredded;
      List.iter (fun (i,l) -> eval_para_pair_list (num_sub + i) l) 
        shredded_arrays; 
      printf "  subroutine setup_parameters ()"; nl();
      let sum_sub = num_sub + num_sub_arrays in
      for i = 1 to sum_sub do
        printf "    call setup_parameters%s" (string_of_int i); nl();
      done;
      printf "  end subroutine setup_parameters"; nl();
      if !no_write then begin
        printf "! No print_parameters"; nl();
      end else begin
        printf "  subroutine print_parameters ()"; nl();
        printf "    @[<2>character(len=*), parameter ::";
        printf "@ fmt_real = \"(A12,4X,' = ',E25.18)\",";
        printf "@ fmt_complex = \"(A12,4X,' = ',E25.18,' + i*',E25.18)\",";
        printf "@ fmt_real_array = \"(A12,'(',I2.2,')',' = ',E25.18)\",";
        printf "@ fmt_complex_array = ";
        printf "\"(A12,'(',I2.2,')',' = ',E25.18,' + i*',E25.18)\""; nl ();
        printf "    @[<2>write (unit = *, fmt = \"(A)\") @,";
        printf "\"default values for the input parameters:\""; nl ();
        List.iter (fun (p, _) -> print_echo "real" p) params.input;
        printf "    @[<2>write (unit = *, fmt = \"(A)\") @,";
        printf "\"derived parameters:\""; nl ();
        List.iter (print_echo "real") declarations.real_singles;
        List.iter (print_echo "complex") declarations.complex_singles;
        List.iter (print_echo_array "real") declarations.real_arrays;
        List.iter (print_echo_array "complex") declarations.complex_arrays;
        printf "  end subroutine print_parameters"; nl();
      end;  
      printf "end module %s" !parameter_module; nl ();
      printf "! O'Mega revision control information:"; nl ();
      List.iter (fun s -> printf "!    %s" s; nl ())
        (RCS.summary M.rcs @ RCS.summary rcs);
      printf "!!! program test_parameters"; nl();
      printf "!!!   use %s" !parameter_module; nl();
      printf "!!!   call setup_parameters ()"; nl();
      printf "!!!   call print_parameters ()"; nl();
      printf "!!! end program test_parameters"; nl()

  end

(* \thocwmodulesection{\texttt{Fortran\,90/95}} *)

module type Fermions =
  sig
    open Coupling
    val psi_type : string
    val psibar_type : string
    val chi_type : string
    val grav_type : string
    val psi_incoming : string
    val brs_psi_incoming : string
    val psibar_incoming : string
    val brs_psibar_incoming : string
    val chi_incoming : string
    val brs_chi_incoming : string
    val grav_incoming : string
    val psi_outgoing : string
    val brs_psi_outgoing : string
    val psibar_outgoing : string
    val brs_psibar_outgoing : string 
    val chi_outgoing : string
    val brs_chi_outgoing : string 
    val grav_outgoing : string
    val psi_propagator : string
    val psibar_propagator : string
    val chi_propagator : string
    val grav_propagator : string
    val psi_projector : string
    val psibar_projector : string
    val chi_projector : string
    val grav_projector : string
    val psi_gauss : string
    val psibar_gauss : string
    val chi_gauss : string
    val grav_gauss : string
    val print_current : int * fermionbar * boson * fermion ->
      string -> string -> string -> fuse2 -> unit
    val print_current_p : int * fermion * boson * fermion ->
      string -> string -> string -> fuse2 -> unit
    val print_current_b : int * fermionbar * boson * fermionbar ->
      string -> string -> string -> fuse2 -> unit
    val print_current_g : int * fermionbar * boson * fermion ->
      string -> string -> string -> string -> string -> string 
      -> fuse2 -> unit
    val print_current_g4 : int * fermionbar * boson2 * fermion ->
      string -> string -> string -> string -> fuse3 -> unit
    val reverse_braket : lorentz -> bool
    val use_module : string
    val require_library : string list
    val rcs : RCS.t
   end

module Fortran_Fermions : Fermions =
  struct
    let rcs = RCS.rename rcs_file "Targets.Fortran_Fermions()"
        [ "generates Fortran95 code for Dirac fermions";
          "using revision 2000_10_A of module omega95" ]

    open Coupling
    open Format

    let psi_type = "spinor"
    let psibar_type = "conjspinor"
    let chi_type = "???"
    let grav_type = "???"

    let psi_incoming = "u"            
    let brs_psi_incoming = "brs_u"
    let psibar_incoming = "vbar"
    let brs_psibar_incoming = "brs_vbar"
    let chi_incoming = "???"
    let brs_chi_incoming = "???"
    let grav_incoming = "???"
    let psi_outgoing = "v"
    let brs_psi_outgoing = "brs_v"
    let psibar_outgoing = "ubar"
    let brs_psibar_outgoing = "brs_ubar"
    let chi_outgoing = "???"
    let brs_chi_outgoing = "???"
    let grav_outgoing = "???"

    let psi_propagator = "pr_psi"
    let psibar_propagator = "pr_psibar"
    let chi_propagator = "???"
    let grav_propagator = "???"

    let psi_projector = "pj_psi"
    let psibar_projector = "pj_psibar"
    let chi_projector = "???"
    let grav_projector = "???"

    let psi_gauss = "pg_psi"
    let psibar_gauss = "pg_psibar"
    let chi_gauss = "???"
    let grav_gauss = "???"

    let format_coupling coeff c =
      match coeff with
      | 1 -> c
      | -1 -> "(-" ^ c ^")"
      | coeff -> string_of_int coeff ^ "*" ^ c

    let format_coupling_2 coeff c = 
      match coeff with 
      | 1 -> c
      | -1 -> "-" ^ c
      | coeff -> string_of_int coeff ^ "*" ^ c

    let fastener s i = 
      try 
        let offset = (String.index s '(') in
        if ((String.get s (String.length s - 1)) != ')') then
          failwith "fastener: wrong usage of parentheses"
        else
          let func_name = (String.sub s 0 offset) and
	      tail =
	    (String.sub s (succ offset) (String.length s - offset - 2)) in 
          if (String.contains func_name ')') or 
	    (String.contains tail '(') or 
            (String.contains tail ')') then
            failwith "fastener: wrong usage of parentheses"
          else      
            func_name ^ "(" ^ string_of_int i ^ "," ^ tail ^ ")"
      with
      | Not_found ->  
          if (String.contains s ')') then
	    failwith "fastener: wrong usage of parentheses"
          else
	    s ^ "(" ^ string_of_int i ^ ")"

    let print_fermion_current coeff f c wf1 wf2 fusion =
      let c = format_coupling coeff c in
      match fusion with
      | F13 -> printf "%s_ff(%s,%s,%s)" f c wf1 wf2
      | F31 -> printf "%s_ff(%s,%s,%s)" f c wf2 wf1
      | F23 -> printf "f_%sf(%s,%s,%s)" f c wf1 wf2
      | F32 -> printf "f_%sf(%s,%s,%s)" f c wf2 wf1
      | F12 -> printf "f_f%s(%s,%s,%s)" f c wf1 wf2
      | F21 -> printf "f_f%s(%s,%s,%s)" f c wf2 wf1

(* \begin{dubious}
     Using a two element array for the combined vector-axial and scalar-pseudo
     couplings helps to support HELAS as well.  Since we will probably never
     support general boson couplings with HELAS, it might be retired in favor
     of two separate variables.  For this [Model.constant_symbol] has to be
     generalized.
   \end{dubious} *)
      
(* \begin{dubious}
     NB: passing the array instead of two separate constants would be a
     \emph{bad} idea, because the support for Majorana spinors below will
     have to flip signs!
   \end{dubious} *)
      
    let print_fermion_current2 coeff f c wf1 wf2 fusion =
      let c = format_coupling_2 coeff c in
      let c1 = fastener c 1 and
          c2 = fastener c 2 in 
      match fusion with
      | F13 -> printf "%s_ff(%s,%s,%s,%s)" f c1 c2 wf1 wf2
      | F31 -> printf "%s_ff(%s,%s,%s,%s)" f c1 c2 wf2 wf1
      | F23 -> printf "f_%sf(%s,%s,%s,%s)" f c1 c2 wf1 wf2
      | F32 -> printf "f_%sf(%s,%s,%s,%s)" f c1 c2 wf2 wf1
      | F12 -> printf "f_f%s(%s,%s,%s,%s)" f c1 c2 wf1 wf2
      | F21 -> printf "f_f%s(%s,%s,%s,%s)" f c1 c2 wf2 wf1

    let print_current = function
      | coeff, Psibar, VA, Psi -> print_fermion_current2 coeff "va"
      | coeff, Psibar, V, Psi -> print_fermion_current coeff "v"
      | coeff, Psibar, A, Psi -> print_fermion_current coeff "a"
      | coeff, Psibar, VL, Psi -> print_fermion_current coeff "vl"
      | coeff, Psibar, VR, Psi -> print_fermion_current coeff "vr"
      | coeff, Psibar, VLR, Psi -> print_fermion_current2 coeff "vlr"
      | coeff, Psibar, SP, Psi -> print_fermion_current2 coeff "sp"
      | coeff, Psibar, S, Psi -> print_fermion_current coeff "s"
      | coeff, Psibar, P, Psi -> print_fermion_current coeff "p"
      | coeff, Psibar, SL, Psi -> print_fermion_current coeff "sl"
      | coeff, Psibar, SR, Psi -> print_fermion_current coeff "sr"
      | coeff, Psibar, SLR, Psi -> print_fermion_current2 coeff "slr"
      | coeff, Psibar, _, Psi -> invalid_arg
            "Targets.Fortran_Fermions: no superpotential here"
      | _, Chibar, _, _ | _, _, _, Chi -> invalid_arg
            "Targets.Fortran_Fermions: Majorana spinors not handled"
      | _, Gravbar, _, _ | _, _, _, Grav -> invalid_arg
            "Targets.Fortran_Fermions: Gravitinos not handled"

    let print_current_p = function
      | _, _, _, _ -> invalid_arg
            "Targets.Fortran_Fermions: No clashing arrows here"

    let print_current_b = function
      | _, _, _, _ -> invalid_arg
            "Targets.Fortran_Fermions: No clashing arrows here"

    let print_current_g = function
      | _, _, _, _ -> invalid_arg
            "Targets.Fortran_Fermions: No gravitinos here"

    let print_current_g4 = function
      | _, _, _, _ -> invalid_arg
            "Targets.Fortran_Fermions: No gravitinos here"

    let reverse_braket= function
      | Spinor -> true
      | _ -> false

    let use_module = "omega95"
    let require_library =
      ["omega_spinors_2003_03_A"; "omega_spinor_cpls_2003_03_A"]
  end
      
module Make_Fortran (Fermions : Fermions)
    (F : Fusion.T) (Make_MF : Fusion.MultiMaker)
    (M : Model.T with type flavor = F.flavor and type constant = F.constant) =
  struct
    module Common = Common_Fortran(F)(M)
    open Common
    let options = options

    let rcs_list =
      [ RCS.rename rcs_file "Targets.Make_Fortran()"
          [ "NB: non-gauge vector couplings are not available yet" ];
        Fermions.rcs ] 

    let require_library =
      Fermions.require_library @
      [ "omega_vectors_2003_03_A"; "omega_polarizations_2003_03_A";
        "omega_couplings_2003_03_A"; "omega_utils_2003_03_A" ]
        
    open Coupling
    open Format

(* \thocwmodulesubsection{Run-Time Diagnostics} *)

    type diagnostic = All | Arguments | Helicities | Momenta | Gauge

    type diagnostic_mode = Off | Warn | Panic

    let warn mode =
      match !mode with
      | Off -> false
      | Warn -> true
      | Panic -> true
    let panic mode =
      match !mode with
      | Off -> false
      | Warn -> false
      | Panic -> true

    let suffix mode =
      if panic mode then
        "panic"
      else
        "warn"

    let diagnose_arguments = ref Off
    let diagnose_helicities = ref Off
    let diagnose_momenta = ref Off
    let diagnose_gauge = ref Off

    let rec parse_diagnostic = function
      | All, panic ->
          parse_diagnostic (Arguments, panic);
          parse_diagnostic (Helicities, panic);
          parse_diagnostic (Momenta, panic);
          parse_diagnostic (Gauge, panic)
      | Arguments, panic ->
          diagnose_arguments := if panic then Panic else Warn
      | Helicities, panic ->
          diagnose_helicities := if panic then Panic else Warn
      | Momenta, panic ->
          diagnose_momenta := if panic then Panic else Warn
      | Gauge, panic ->
          diagnose_gauge := if panic then Panic else Warn

(* If diagnostics are required, we have to switch off
   Fortran95 features like pure functions. *)

    let parse_diagnostics = function
      | [] -> ()
      | diagnostics ->
          fortran95 := false;
          List.iter parse_diagnostic diagnostics

(* \thocwmodulesubsection{Amplitude} *)

    module MF = Make_MF(F)
    type amplitude = F.amplitude
    type amplitudes = MF.amplitudes

    let declare_wavefunctions wfs =
      let wfs' = classify_wfs wfs in
      declare_list ("complex(kind=" ^ !kind ^ ")") 
        (wfs'.scalars @ wfs'.brs_scalars);
      declare_list ("type(" ^ Fermions.psi_type ^ ")")
        (wfs'.spinors @ wfs'.brs_spinors);
      declare_list ("type(" ^ Fermions.psibar_type ^ ")") 
        (wfs'.conjspinors @ wfs'.brs_conjspinors);
      declare_list ("type(" ^ Fermions.chi_type ^ ")") 
        (wfs'.realspinors @ wfs'.brs_realspinors @ wfs'.ghostspinors);
      declare_list ("type(" ^ Fermions.grav_type ^ ")") wfs'.vectorspinors;
      declare_list "type(vector)" (wfs'.vectors @ wfs'.massive_vectors @
         wfs'.brs_vectors @ wfs'.brs_massive_vectors @
                                  wfs'.ward_vectors);
      declare_list "type(tensor2odd)" wfs'.tensors_1;
      declare_list "type(tensor)" wfs'.tensors_2

    let print_declarations amplitude =
      declare_wavefunctions (F.externals amplitude);
      declare_wavefunctions (F.variables amplitude);
      match
        P.elements
          (List.fold_left (fun seen wf -> P.add (F.momentum_list wf) seen)
             P.empty (F.variables amplitude)) with
      | [] -> ()
      | momenta ->
          printf "    @[<2>type(momentum) :: ";
          print_list (List.map format_momentum momenta); nl ()

(* [print_current] is the most important function that has to match the functions
   in \verb+omega95+ (see appendix~\ref{sec:fortran}).  It offers plentiful
   opportunities for making mistakes, in particular those related to signs.
   We start with a few auxiliary functions:  *)

    let children2 rhs =
      match F.children rhs with
      | [wf1; wf2] -> (wf1, wf2)
      | _ -> failwith "Targets.children2: can't happen"

    let children3 rhs =
      match F.children rhs with
      | [wf1; wf2; wf3] -> (wf1, wf2, wf3)
      | _ -> invalid_arg "Targets.children3: can't happen"

(* Note that it is (marginally) faster to multiply the two scalar products
   with the coupling constant than the four vector components.
   \begin{dubious}
     This could be part of \verb+omegalib+ as well \ldots
   \end{dubious} *)
        
    let format_coeff = function
      | 1 -> ""
      | -1 -> "-"
      | coeff -> "(" ^ string_of_int coeff ^ ")*"

    let format_coupling coeff c =
      match coeff with
      | 1 -> c
      | -1 -> "(-" ^ c ^")"
      | coeff -> string_of_int coeff ^ "*" ^ c

(* \begin{dubious}
     The following is error prone and should be generated automagically.
   \end{dubious} *)

    let print_vector4 c wf1 wf2 wf3 fusion (coeff, contraction) =
      match contraction, fusion with
      | C_12_34, (F341|F431|F342|F432|F123|F213|F124|F214)
      | C_13_42, (F241|F421|F243|F423|F132|F312|F134|F314)
      | C_14_23, (F231|F321|F234|F324|F142|F412|F143|F413) ->
          printf "((%s%s)*(%s*%s))*%s" (format_coeff coeff) c wf1 wf2 wf3
      | C_12_34, (F134|F143|F234|F243|F312|F321|F412|F421)
      | C_13_42, (F124|F142|F324|F342|F213|F231|F413|F431)
      | C_14_23, (F123|F132|F423|F432|F214|F241|F314|F341) ->
          printf "((%s%s)*(%s*%s))*%s" (format_coeff coeff) c wf2 wf3 wf1
      | C_12_34, (F314|F413|F324|F423|F132|F231|F142|F241)
      | C_13_42, (F214|F412|F234|F432|F123|F321|F143|F341)
      | C_14_23, (F213|F312|F243|F342|F124|F421|F134|F431) ->
          printf "((%s%s)*(%s*%s))*%s" (format_coeff coeff) c wf1 wf3 wf2

    let print_add_vector4 c wf1 wf2 wf3 fusion (coeff, contraction) =
      printf "@ + ";
      print_vector4 c wf1 wf2 wf3 fusion (coeff, contraction)

    let print_vector4_km c pa pb wf1 wf2 wf3 fusion (coeff, contraction) =
      match contraction, fusion with
      | C_12_34, (F341|F431|F342|F432|F123|F213|F124|F214)
      | C_13_42, (F241|F421|F243|F423|F132|F312|F134|F314)
      | C_14_23, (F231|F321|F234|F324|F142|F412|F143|F413) ->
          printf "((%s%s%s+%s))*(%s*%s))*%s" 
            (format_coeff coeff) c pa pb wf1 wf2 wf3
      | C_12_34, (F134|F143|F234|F243|F312|F321|F412|F421)
      | C_13_42, (F124|F142|F324|F342|F213|F231|F413|F431)
      | C_14_23, (F123|F132|F423|F432|F214|F241|F314|F341) ->
          printf "((%s%s%s+%s))*(%s*%s))*%s" 
            (format_coeff coeff) c pa pb wf2 wf3 wf1
      | C_12_34, (F314|F413|F324|F423|F132|F231|F142|F241)
      | C_13_42, (F214|F412|F234|F432|F123|F321|F143|F341)
      | C_14_23, (F213|F312|F243|F342|F124|F421|F134|F431) ->
          printf "((%s%s%s+%s))*(%s*%s))*%s" 
            (format_coeff coeff) c pa pb wf1 wf3 wf2

    let print_add_vector4_km c pa pb wf1 wf2 wf3 fusion (coeff, contraction) =
      printf "@ + ";
      print_vector4_km c pa pb wf1 wf2 wf3 fusion (coeff, contraction)

    let print_dscalar4 c wf1 wf2 wf3 p1 p2 p3 p123
        fusion (coeff, contraction) =
      match contraction, fusion with
      | C_12_34, (F341|F431|F342|F432|F123|F213|F124|F214)
      | C_13_42, (F241|F421|F243|F423|F132|F312|F134|F314)
      | C_14_23, (F231|F321|F234|F324|F142|F412|F143|F413) ->
          printf "((%s%s)*(%s*%s)*(%s*%s)*%s*%s*%s)"
            (format_coeff coeff) c p1 p2 p3 p123 wf1 wf2 wf3
      | C_12_34, (F134|F143|F234|F243|F312|F321|F412|F421)
      | C_13_42, (F124|F142|F324|F342|F213|F231|F413|F431)
      | C_14_23, (F123|F132|F423|F432|F214|F241|F314|F341) ->
          printf "((%s%s)*(%s*%s)*(%s*%s)*%s*%s*%s)"
            (format_coeff coeff) c p2 p3 p1 p123 wf1 wf2 wf3
      | C_12_34, (F314|F413|F324|F423|F132|F231|F142|F241)
      | C_13_42, (F214|F412|F234|F432|F123|F321|F143|F341)
      | C_14_23, (F213|F312|F243|F342|F124|F421|F134|F431) ->
          printf "((%s%s)*(%s*%s)*(%s*%s)*%s*%s*%s)"
            (format_coeff coeff) c p1 p3 p2 p123 wf1 wf2 wf3

    let print_add_dscalar4 c wf1 wf2 wf3 p1 p2 p3 p123
        fusion (coeff, contraction) =
      printf "@ + ";
      print_dscalar4 c wf1 wf2 wf3 p1 p2 p3 p123 fusion (coeff, contraction)

    let print_dscalar2_vector2 c wf1 wf2 wf3 p1 p2 p3 p123
        fusion (coeff, contraction) =
      failwith "Targets.Fortran.print_dscalar2_vector2: incomplete!";
      match contraction, fusion with
      | C_12_34, (F134|F143|F234|F243) ->
          printf "((%s%s)*(%s*%s)*(%s*%s)*%s)"
            (format_coeff coeff) c p123 p1 wf2 wf3 wf1
      | C_12_34, (F312|F321|F412|F421) ->
          printf "((%s%s)*((%s*%s)*%s*%s)*%s)"
            (format_coeff coeff) c p2 p3 wf2 wf3 wf1
      | C_12_34, (F341|F431|F342|F432|F123|F213|F124|F214)
      | C_13_42, (F241|F421|F243|F423|F132|F312|F134|F314)
      | C_14_23, (F231|F321|F234|F324|F142|F412|F143|F413) ->
          printf "((%s%s)*(%s*%s)*(%s*%s)*%s*%s*%s)"
            (format_coeff coeff) c p1 p2 p3 p123 wf1 wf2 wf3
      | C_13_42, (F124|F142|F324|F342|F213|F231|F413|F431)
      | C_14_23, (F123|F132|F423|F432|F214|F241|F314|F341) ->
          printf "((%s%s)*(%s*%s)*(%s*%s)*%s*%s*%s)"
            (format_coeff coeff) c p2 p3 p1 p123 wf1 wf2 wf3
      | C_12_34, (F314|F413|F324|F423|F132|F231|F142|F241)
      | C_13_42, (F214|F412|F234|F432|F123|F321|F143|F341)
      | C_14_23, (F213|F312|F243|F342|F124|F421|F134|F431) ->
          printf "((%s%s)*(%s*%s)*(%s*%s)*%s*%s*%s)"
            (format_coeff coeff) c p1 p3 p2 p123 wf1 wf2 wf3

    let print_add_dscalar2_vector2 c wf1 wf2 wf3 p1 p2 p3 p123
        fusion (coeff, contraction) =
      printf "@ + ";
      print_dscalar2_vector2 c wf1 wf2 wf3 p1 p2 p3 p123
        fusion (coeff, contraction)

    let print_current rhs =
      match F.coupling rhs with
      | V3 (vertex, fusion, constant) ->
          let ch1, ch2 = children2 rhs in
          let wf1 = variable ch1
          and wf2 = variable ch2
          and p1 = momentum ch1
          and p2 = momentum ch2 
          and m1 = M.mass_symbol (F.flavor ch1)
          and m2 = M.mass_symbol (F.flavor ch2) in
          let c = M.constant_symbol constant in
          printf "@, %s " (if (F.sign rhs) < 0 then "-" else "+");
          begin match vertex with

(* Fermionic currents $\bar\psi\fmslash{A}\psi$ and $\bar\psi\phi\psi$
   are handled by the [Fermions] module, since they depend on the
   choice of Feynman rules: Dirac or Majorana. *)

          | FBF (coeff, fb, b, f) ->
              Fermions.print_current (coeff, fb, b, f) c wf1 wf2 fusion
          | PBP (coeff, f1, b, f2) ->
              Fermions.print_current_p (coeff, f1, b, f2) c wf1 wf2 fusion
          | BBB (coeff, fb1, b, fb2) -> 
              Fermions.print_current_b (coeff, fb1, b, fb2) c wf1 wf2 fusion
          | GBG (coeff, fb, b, f) ->  let p12 =
              Printf.sprintf "(-%s-%s)" p1 p2 in
              Fermions.print_current_g (coeff, fb, b, f) c wf1 wf2 p1 p2 
                   p12 fusion

(* Table~\ref{tab:dim4-bosons} is a bit misleading, since if includes
   totally antisymmetric structure constants.  The space-time part alone
   is also totally antisymmetric: *)

          | Gauge_Gauge_Gauge coeff ->
              let c = format_coupling coeff c in
              begin match fusion with
              | (F23|F31|F12) ->
                  printf "g_gg(%s,%s,%s,%s,%s)" c wf1 p1 wf2 p2
              | (F32|F13|F21) ->
                  printf "g_gg(%s,%s,%s,%s,%s)" c wf2 p2 wf1 p1
              end

(* In [Aux_Gauge_Gauge], we can not rely on antisymmetry alone, because of the
   different Lorentz representations of the auxialiary and the gauge field.
   Instead we have to provide the sign in
   \begin{equation}
     (V_2 \wedge V_3) \cdot T_1 =
       \begin{cases}
          V_2 \cdot (T_1 \cdot V_3) = - V_2 \cdot (V_3 \cdot T_1) & \\
          V_3 \cdot (V_2 \cdot T_1) = - V_3 \cdot (T_1 \cdot V_2) &
       \end{cases}
   \end{equation}
   ourselves. Alternatively, one could provide \verb+g_xg+ mirroring
   \verb+g_gx+. *)

          | Aux_Gauge_Gauge coeff ->
              let c = format_coupling coeff c in
              begin match fusion with
              | F23 -> printf "x_gg(%s,%s,%s)" c wf1 wf2
              | F32 -> printf "x_gg(%s,%s,%s)" c wf2 wf1
              | F12 -> printf "g_gx(%s,%s,%s)" c wf2 wf1
              | F21 -> printf "g_gx(%s,%s,%s)" c wf1 wf2
              | F13 -> printf "(-1)*g_gx(%s,%s,%s)" c wf2 wf1
              | F31 -> printf "(-1)*g_gx(%s,%s,%s)" c wf1 wf2
              end

(* These cases are symmetric and we just have to juxtapose the correct fields
   and provide parentheses to minimize the number of multiplications. *)

          | Scalar_Vector_Vector coeff ->
              let c = format_coupling coeff c in
              begin match fusion with
              | (F23|F32) -> printf "%s*(%s*%s)" c wf1 wf2
              | (F12|F13) -> printf "(%s*%s)*%s" c wf1 wf2
              | (F21|F31) -> printf "(%s*%s)*%s" c wf2 wf1
              end

          | Aux_Vector_Vector coeff ->
              let c = format_coupling coeff c in
              begin match fusion with
              | (F23|F32) -> printf "%s*(%s*%s)" c wf1 wf2
              | (F12|F13) -> printf "(%s*%s)*%s" c wf1 wf2
              | (F21|F31) -> printf "(%s*%s)*%s" c wf2 wf1
              end

(* Even simpler: *)

          | Scalar_Scalar_Scalar coeff ->
              printf "(%s*%s*%s)" (format_coupling coeff c) wf1 wf2

          | Aux_Scalar_Scalar coeff ->
              printf "(%s*%s*%s)" (format_coupling coeff c) wf1 wf2

          | Aux_Scalar_Vector coeff ->
              let c = format_coupling coeff c in
              begin match fusion with
              | (F13|F31) -> printf "%s*(%s*%s)" c wf1 wf2
              | (F23|F21) -> printf "(%s*%s)*%s" c wf1 wf2
              | (F32|F12) -> printf "(%s*%s)*%s" c wf2 wf1
              end

          | Vector_Scalar_Scalar coeff ->
              let c = format_coupling coeff c in
              begin match fusion with
              | F23 -> printf "v_ss(%s,%s,%s,%s,%s)" c wf1 p1 wf2 p2
              | F32 -> printf "v_ss(%s,%s,%s,%s,%s)" c wf2 p2 wf1 p1
              | F12 -> printf "s_vs(%s,%s,%s,%s,%s)" c wf1 p1 wf2 p2
              | F21 -> printf "s_vs(%s,%s,%s,%s,%s)" c wf2 p2 wf1 p1
              | F13 -> printf "(-1)*s_vs(%s,%s,%s,%s,%s)" c wf1 p1 wf2 p2
              | F31 -> printf "(-1)*s_vs(%s,%s,%s,%s,%s)" c wf2 p2 wf1 p1
              end

          | Graviton_Scalar_Scalar coeff -> 
              let c = format_coupling coeff c in
              begin match fusion with
              | F12 -> printf "s_gravs(%s,%s,-(%s+%s),%s,%s,%s)" c m2 p1 p2 p2 wf1 wf2 
              | F21 -> printf "s_gravs(%s,%s,-(%s+%s),%s,%s,%s)" c m1 p1 p2 p1 wf2 wf1 
              | F13 -> printf "s_gravs(%s,%s,%s,-(%s+%s),%s,%s)" c m2 p2 p1 p2 wf1 wf2 
              | F31 -> printf "s_gravs(%s,%s,%s,-(%s+%s),%s,%s)" c m1 p1 p1 p2 wf2 wf1 
              | F23 -> printf "grav_ss(%s,%s,%s,%s,%s,%s)" c m1 p1 p2 wf1 wf2 
              | F32 -> printf "grav_ss(%s,%s,%s,%s,%s,%s)" c m1 p2 p1 wf2 wf1
              end

(* In producing a vector in the fusion we always contract the rightmost index with the 
   vector wavefunction from [rhs]. So the first momentum is always the one of the 
   vector boson produced in the fusion, while the second one is that from the [rhs]. 
   This makes the cases [F12] and [F13] as well as [F21] and [F31] equal. In principle,
   we could have already done this for the [Graviton_Scalar_Scalar] case. *)


          | Graviton_Vector_Vector coeff -> 
              let c = format_coupling coeff c in
              begin match fusion with
              | (F12|F13) -> printf "v_gravv(%s,%s,-(%s+%s),%s,%s,%s)" c m2 p1 p2 p2 wf1 wf2
              | (F21|F31) -> printf "v_gravv(%s,%s,-(%s+%s),%s,%s,%s)" c m1 p1 p2 p1 wf2 wf1
              | F23 -> printf "grav_vv(%s,%s,%s,%s,%s,%s)" c m1 p1 p2 wf1 wf2
              | F32 -> printf "grav_vv(%s,%s,%s,%s,%s,%s)" c m1 p2 p1 wf2 wf1
              end

          | Graviton_Spinor_Spinor coeff -> 
              let c = format_coupling coeff c in
              begin match fusion with
              | F23 -> printf "f_gravf(%s,%s,-(%s+%s),(-%s),%s,%s)" c m2 p1 p2 p2 wf1 wf2
              | F32 -> printf "f_gravf(%s,%s,-(%s+%s),(-%s),%s,%s)" c m1 p1 p2 p1 wf2 wf1
              | F12 -> printf "f_fgrav(%s,%s,%s,%s+%s,%s,%s)" c m1 p1 p1 p2 wf1 wf2
              | F21 -> printf "f_fgrav(%s,%s,%s,%s+%s,%s,%s)" c m2 p2 p1 p2 wf2 wf1
              | F13 -> printf "grav_ff(%s,%s,%s,(-%s),%s,%s)" c m1 p1 p2 wf1 wf2
              | F31 -> printf "grav_ff(%s,%s,%s,(-%s),%s,%s)" c m1 p2 p1 wf2 wf1
              end

          | Dim4_Vector_Vector_Vector_T coeff ->
              let c = format_coupling coeff c in
              begin match fusion with
              | F23 -> printf "tkv_vv(%s,%s,%s,%s,%s)" c wf1 p1 wf2 p2
              | F32 -> printf "tkv_vv(%s,%s,%s,%s,%s)" c wf2 p2 wf1 p1
              | F12 -> printf "tv_kvv(%s,%s,%s,%s,%s)" c wf1 p1 wf2 p2
              | F21 -> printf "tv_kvv(%s,%s,%s,%s,%s)" c wf2 p2 wf1 p1
              | F13 -> printf "(-1)*tv_kvv(%s,%s,%s,%s,%s)" c wf1 p1 wf2 p2
              | F31 -> printf "(-1)*tv_kvv(%s,%s,%s,%s,%s)" c wf2 p2 wf1 p1
              end

          | Dim4_Vector_Vector_Vector_L coeff ->
              let c = format_coupling coeff c in
              begin match fusion with
              | F23 -> printf "lkv_vv(%s,%s,%s,%s,%s)" c wf1 p1 wf2 p2
              | F32 -> printf "lkv_vv(%s,%s,%s,%s,%s)" c wf2 p2 wf1 p1
              | F12 | F13 -> printf "lv_kvv(%s,%s,%s,%s)" c wf1 p1 wf2
              | F21 | F31 -> printf "lv_kvv(%s,%s,%s,%s)" c wf2 p2 wf1
              end

          | Dim6_Gauge_Gauge_Gauge coeff ->
              let c = format_coupling coeff c in
              begin match fusion with
              | F23 | F31 | F12 ->
                  printf "kg_kgkg(%s,%s,%s,%s,%s)" c wf1 p1 wf2 p2
              | F32 | F13 | F21 ->
                  printf "kg_kgkg(%s,%s,%s,%s,%s)" c wf2 p2 wf1 p1
              end

          | Dim4_Vector_Vector_Vector_T5 coeff ->
              let c = format_coupling coeff c in
              begin match fusion with
              | F23 -> printf "t5kv_vv(%s,%s,%s,%s,%s)" c wf1 p1 wf2 p2
              | F32 -> printf "t5kv_vv(%s,%s,%s,%s,%s)" c wf2 p2 wf1 p1
              | F12 | F13 -> printf "t5v_kvv(%s,%s,%s,%s,%s)" c wf1 p1 wf2 p2
              | F21 | F31 -> printf "t5v_kvv(%s,%s,%s,%s,%s)" c wf2 p2 wf1 p1
              end

          | Dim4_Vector_Vector_Vector_L5 coeff ->
              let c = format_coupling coeff c in
              begin match fusion with
              | F23 -> printf "l5kv_vv(%s,%s,%s,%s,%s)" c wf1 p1 wf2 p2
              | F32 -> printf "l5kv_vv(%s,%s,%s,%s,%s)" c wf2 p2 wf1 p1
              | F12 -> printf "l5v_kvv(%s,%s,%s,%s)" c wf1 p1 wf2
              | F21 -> printf "l5v_kvv(%s,%s,%s,%s)" c wf2 p2 wf1
              | F13 -> printf "(-1)*l5v_kvv(%s,%s,%s,%s)" c wf1 p1 wf2
              | F31 -> printf "(-1)*l5v_kvv(%s,%s,%s,%s)" c wf2 p2 wf1
              end

          | Dim6_Gauge_Gauge_Gauge_5 coeff ->
              let c = format_coupling coeff c in
              begin match fusion with
              | F23 -> printf "kg5_kgkg(%s,%s,%s,%s,%s)" c wf1 p1 wf2 p2
              | F32 -> printf "kg5_kgkg(%s,%s,%s,%s,%s)" c wf2 p2 wf1 p1
              | F12 -> printf "kg_kg5kg(%s,%s,%s,%s,%s)" c wf1 p1 wf2 p2
              | F21 -> printf "kg_kg5kg(%s,%s,%s,%s,%s)" c wf2 p2 wf1 p1
              | F13 -> printf "(-1)*kg_kg5kg(%s,%s,%s,%s,%s)" c wf1 p1 wf2 p2
              | F31 -> printf "(-1)*kg_kg5kg(%s,%s,%s,%s,%s)" c wf2 p2 wf1 p1
              end

          | Aux_DScalar_DScalar coeff ->
              let c = format_coupling coeff c in
              begin match fusion with
              | (F23|F32) ->
                  printf "%s*(%s*%s)*(%s*%s)" c p1 p2 wf1 wf2
              | (F12|F13) ->
                  printf "%s*(-((%s+%s)*%s))*(%s*%s)" c p1 p2 p2 wf1 wf2
              | (F21|F31) ->
                  printf "%s*(-((%s+%s)*%s))*(%s*%s)" c p1 p2 p1 wf1 wf2
              end

          | Aux_Vector_DScalar coeff ->
              let c = format_coupling coeff c in
              begin match fusion with
              | F23 -> printf "%s*(%s*%s)*%s" c wf1 p2 wf2
              | F32 -> printf "%s*(%s*%s)*%s" c wf2 p1 wf1
              | F12 -> printf "%s*(-((%s+%s)*%s))*%s" c p1 p2 wf2 wf1
              | F21 -> printf "%s*(-((%s+%s)*%s))*%s" c p1 p2 wf1 wf2
              | (F13|F31) -> printf "(-(%s+%s))*(%s*%s*%s)" p1 p2 c wf1 wf2
              end

          | Dim5_Scalar_Gauge2 coeff ->
              let c = format_coupling coeff c in
              begin match fusion with
              | (F23|F32) -> printf "(%s)*((%s*%s)*(%s*%s) - (%s*%s)*(%s*%s))" 
                    c p1 wf2 p2 wf1 p1 p2 wf2 wf1 
              | (F12|F13) -> printf "(%s)*%s*((-((%s+%s)*%s))*%s - ((-(%s+%s)*%s))*%s)" 
                    c wf1 p1 p2 wf2 p2 p1 p2 p2 wf2
              | (F21|F31) -> printf "(%s)*%s*((-((%s+%s)*%s))*%s - ((-(%s+%s)*%s))*%s)" 
                    c wf2 p2 p1 wf1 p1 p1 p2 p1 wf1
              end

          | Dim5_Scalar_Gauge2_Skew coeff -> 
              let c = format_coupling coeff c in
              begin match fusion with
              | (F23|F32) -> printf "(- phi_vv (%s, %s, %s, %s, %s))" c p1 p2 wf1 wf2
              | (F12|F13) -> printf "(- v_phiv (%s, %s, %s, %s, %s))" c wf1 p1 p2 wf2
              | (F21|F31) -> printf "v_phiv (%s, %s, %s, %s, %s)" c wf2 p1 p2 wf1
              end

          | Dim5_Scalar_Vector_Vector_T coeff ->
              let c = format_coupling coeff c in
              begin match fusion with
              | (F23|F32) -> printf "(%s)*(%s*%s)*(%s*%s)" c p1 wf2 p2 wf1
              | (F12|F13) -> printf "(%s)*%s*(-((%s+%s)*%s))*%s" c wf1 p1 p2 wf2 p2
              | (F21|F31) -> printf "(%s)*%s*(-((%s+%s)*%s))*%s" c wf2 p2 p1 wf1 p1
              end

          | Dim6_Vector_Vector_Vector_T coeff ->
              let c = format_coupling coeff c in
              begin match fusion with
              | F23 -> printf "(%s)*(%s*%s)*(%s*%s)*(%s-%s)" c p2 wf1 p1 wf2 p1 p2
              | F32 -> printf "(%s)*(%s*%s)*(%s*%s)*(%s-%s)" c p1 wf2 p2 wf1 p2 p1
              | (F12|F13) -> printf "(%s)*((%s+2*%s)*%s)*(-((%s+%s)*%s))*%s"
                    c p1 p2 wf1 p1 p2 wf2 p2
              | (F21|F31) -> printf "(%s)*((-((%s+%s)*%s))*(%s+2*%s)*%s)*%s"
                    c p2 p1 wf1 p2 p1 wf2 p1
              end

          | Tensor_2_Vector_Vector coeff ->
              let c = format_coupling coeff c in
              begin match fusion with
              | (F23|F32) -> printf "t2_vv(%s,%s,%s)" c wf1 wf2
              | (F12|F13) -> printf "v_t2v(%s,%s,%s)" c wf1 wf2
              | (F21|F31) -> printf "v_t2v(%s,%s,%s)" c wf2 wf1
              end

          | Dim5_Tensor_2_Vector_Vector_1 coeff ->
              let c = format_coupling coeff c in
              begin match fusion with
              | (F23|F32) -> printf "t2_vv_d5_1(%s,%s,%s,%s,%s)" c wf1 p1 wf2 p2
              | (F12|F13) -> printf "v_t2v_d5_1(%s,%s,%s,%s,%s)" c wf1 p1 wf2 p2
              | (F21|F31) -> printf "v_t2v_d5_1(%s,%s,%s,%s,%s)" c wf2 p2 wf1 p1
              end

          | Dim5_Tensor_2_Vector_Vector_2 coeff ->
              let c = format_coupling coeff c in
              begin match fusion with
              | F23 -> printf "t2_vv_d5_2(%s,%s,%s,%s,%s)" c wf1 p1 wf2 p2
              | F32 -> printf "t2_vv_d5_2(%s,%s,%s,%s,%s)" c wf2 p2 wf1 p1
              | (F12|F13) -> printf "v_t2v_d5_2(%s,%s,%s,%s,%s)" c wf1 p1 wf2 p2
              | (F21|F31) -> printf "v_t2v_d5_2(%s,%s,%s,%s,%s)" c wf2 p2 wf1 p1
              end

          | Dim7_Tensor_2_Vector_Vector_T coeff ->
              let c = format_coupling coeff c in
              begin match fusion with
              | F23 -> printf "t2_vv_d7(%s,%s,%s,%s,%s)" c wf1 p1 wf2 p2
              | F32 -> printf "t2_vv_d7(%s,%s,%s,%s,%s)" c wf2 p2 wf1 p1
              | (F12|F13) -> printf "v_t2v_d7(%s,%s,%s,%s,%s)" c wf1 p1 wf2 p2
              | (F21|F31) -> printf "v_t2v_d7(%s,%s,%s,%s,%s)" c wf2 p2 wf1 p1
              end

          end

(* Flip the sign to account for the~$\mathrm{i}^2$ relative to diagrams
   with only cubic couplings.  *)

      | V4 (vertex, fusion, constant) ->
          let c = M.constant_symbol constant
          and ch1, ch2, ch3 = children3 rhs in
          let wf1 = variable ch1
          and wf2 = variable ch2
          and wf3 = variable ch3
          and p1 = momentum ch1
          and p2 = momentum ch2
          and p3 = momentum ch3 in
          printf "@, %s " (if (F.sign rhs) < 0 then "+" else "-");
          begin match vertex with
          | Scalar4 coeff ->
              printf "(%s*%s*%s*%s)" (format_coupling coeff c) wf1 wf2 wf3
          | Scalar2_Vector2 coeff ->
              let c = format_coupling coeff c in
              begin match fusion with
              | F134 | F143 | F234 | F243 ->
                  printf "%s*%s*(%s*%s)" c wf1 wf2 wf3
              | F314 | F413 | F324 | F423 ->
                  printf "%s*%s*(%s*%s)" c wf2 wf1 wf3
              | F341 | F431 | F342 | F432 ->
                  printf "%s*%s*(%s*%s)" c wf3 wf1 wf2
              | F312 | F321 | F412 | F421 ->
                  printf "(%s*%s*%s)*%s" c wf2 wf3 wf1
              | F231 | F132 | F241 | F142 ->
                  printf "(%s*%s*%s)*%s" c wf1 wf3 wf2
              | F123 | F213 | F124 | F214 ->
                  printf "(%s*%s*%s)*%s" c wf1 wf2 wf3
              end
          | Vector4 contractions ->
              begin match contractions with
              | [] -> invalid_arg "Targets.print_current: Vector4 []"
              | head :: tail ->
                  printf "(";
                  print_vector4 c wf1 wf2 wf3 fusion head;
                  List.iter (print_add_vector4 c wf1 wf2 wf3 fusion) tail;
                  printf ")"
              end
          | Vector4_K_Matrix_tho (disc, poles) ->
              let pa, pb = 
                begin match fusion with
                | (F341|F431|F342|F432|F123|F213|F124|F214) -> (p1, p2)
                | (F134|F143|F234|F243|F312|F321|F412|F421) -> (p2, p3)
                | (F314|F413|F324|F423|F132|F231|F142|F241) -> (p1, p3)
                end in
              printf "(%s*(%s*%s)*(%s*%s)*(%s*%s)@,*("
                c p1 wf1 p2 wf2 p3 wf3;
              List.iter (fun (coeff, pole) ->
                printf "+%s/((%s+%s)*(%s+%s)-%s)"
                  (M.constant_symbol coeff) pa pb pa pb
                  (M.constant_symbol pole))
                poles;
              printf ")*(-%s-%s-%s))" p1 p2 p3
          | Vector4_K_Matrix_jr (disc, contractions) ->
              let pa, pb = 
                begin match disc, fusion with
                | 3, (F143|F413|F142|F412|F321|F231|F324|F234) -> (p1, p2)
                | 3, (F314|F341|F214|F241|F132|F123|F432|F423) -> (p2, p3)
                | 3, (F134|F431|F124|F421|F312|F213|F342|F243) -> (p1, p3)
                | _, (F341|F431|F342|F432|F123|F213|F124|F214) -> (p1, p2)
                | _, (F134|F143|F234|F243|F312|F321|F412|F421) -> (p2, p3)
                | _, (F314|F413|F324|F423|F132|F231|F142|F241) -> (p1, p3)
                end in
              begin match contractions with
              | [] -> invalid_arg "Targets.print_current: Vector4_K_Matrix_jr []"
              | head :: tail ->
                  printf "(";
                  print_vector4_km c pa pb wf1 wf2 wf3 fusion head;
                  List.iter (print_add_vector4_km c pa pb wf1 wf2 wf3 fusion) 
                    tail;
                  printf ")"
              end
          | GBBG (coeff, fb, b, f) -> 
              Fermions.print_current_g4 (coeff, fb, b, f) c wf1 wf2 wf3 
                   fusion

(* \begin{dubious}
     In principle, [p4] could be obtained from the left hand side \ldots
   \end{dubious} *)
          | DScalar4 contractions ->
              let p123 = Printf.sprintf "(-%s-%s-%s)" p1 p2 p3 in
              begin match contractions with
              | [] -> invalid_arg "Targets.print_current: DScalar4 []"
              | head :: tail ->
                  printf "(";
                  print_dscalar4 c wf1 wf2 wf3 p1 p2 p3 p123 fusion head;
                  List.iter (print_add_dscalar4
                               c wf1 wf2 wf3 p1 p2 p3 p123 fusion) tail;
                  printf ")"
              end

          | DScalar2_Vector2 contractions ->
              let p123 = Printf.sprintf "(-%s-%s-%s)" p1 p2 p3 in
              begin match contractions with
              | [] -> invalid_arg "Targets.print_current: DScalar4 []"
              | head :: tail ->
                  printf "(";
                  print_dscalar2_vector2
                    c wf1 wf2 wf3 p1 p2 p3 p123 fusion head;
                  List.iter (print_add_dscalar2_vector2
                               c wf1 wf2 wf3 p1 p2 p3 p123 fusion) tail;
                  printf ")"
              end
          end

      | Vn (_, _, _) ->
          invalid_arg "Targets.print_current: n-ary fusion"

    let print_propagator f p m gamma =
      let w =
        begin match M.width f with
        | Vanishing | Fudged -> "0.0_" ^ !kind
        | Constant -> gamma
        | Timelike -> "wd_tl(" ^ p ^ "," ^ gamma ^ ")"
        | Running -> failwith
              "Targets.Fortran: running width not yet available"
        | Custom f -> f ^ "(" ^ p ^ "," ^ gamma ^ ")"
        end in
      match M.propagator f with
      | Prop_Scalar -> printf "pr_phi(%s,%s,%s," p m w
      | Prop_Col_Scalar -> 
          printf "(-1.0_omega_prec/3.0_omega_prec) * pr_phi(%s,%s,%s," p m w
      | Prop_Ghost -> printf "(0,1) * pr_phi(%s, %s, %s," p m w
      | Prop_Spinor ->
          printf "%s(%s,%s,%s," Fermions.psi_propagator p m w
      | Prop_ConjSpinor ->
          printf "%s(%s,%s,%s," Fermions.psibar_propagator p m w
      | Prop_Majorana ->
          printf "%s(%s,%s,%s," Fermions.chi_propagator p m w
      | Prop_Col_Majorana ->
          printf "(-1.0_omega_prec/3.0_omega_prec) * %s(%s,%s,%s," 
            Fermions.chi_propagator p m w 
      | Prop_Unitarity -> printf "pr_unitarity(%s,%s,%s," p m w
      | Prop_Col_Unitarity -> 
          printf "(-1.0_omega_prec/3.0_omega_prec) * pr_unitarity(%s,%s,%s," p m w
      | Prop_Feynman -> printf "pr_feynman(%s," p
      | Prop_Col_Feynman -> 
          printf "(-1.0_omega_prec/3.0_omega_prec) * pr_feynman(%s," p
      | Prop_Gauge xi -> printf "pr_gauge(%s,%s," p (M.gauge_symbol xi)
      | Prop_Rxi xi ->
          printf "pr_rxi(%s,%s,%s,%s," p m w (M.gauge_symbol xi)
      | Prop_Tensor_2 -> printf "pr_tensor(%s,%s,%s," p m w
      | Prop_Vectorspinor -> printf "pr_grav(%s,%s,%s,"
            p m w
      | Aux_Scalar | Aux_Spinor | Aux_ConjSpinor | Aux_Majorana
      | Aux_Vector | Aux_Tensor_1 -> printf "("
      | Only_Insertion -> printf "("

    let print_projector f p m gamma =
      match M.propagator f with
      | Prop_Scalar -> printf "pj_phi(%s,%s," m gamma
      | Prop_Col_Scalar -> 
          printf "(-1.0_omega_prec/3.0_omega_prec) * pj_phi(%s,%s," m gamma
      | Prop_Ghost -> printf "(0,1) * pj_phi(%s,%s," m gamma
      | Prop_Spinor ->
          printf "%s(%s,%s,%s," Fermions.psi_projector p m gamma
      | Prop_ConjSpinor ->
          printf "%s(%s,%s,%s," Fermions.psibar_projector p m gamma
      | Prop_Majorana ->
          printf "%s(%s,%s,%s," Fermions.chi_projector p m gamma
      | Prop_Col_Majorana ->
          printf "(-1.0_omega_prec/3.0_omega_prec) * %s(%s,%s,%s," 
            Fermions.chi_projector p m gamma
      | Prop_Unitarity -> printf "pj_unitarity(%s,%s,%s," p m gamma
      | Prop_Col_Unitarity -> 
          printf "(-1.0_omega_prec/3.0_omega_prec) * pj_unitarity(%s,%s,%s," p m gamma
      | Prop_Feynman | Prop_Col_Feynman -> 
          invalid_arg "no on-shell Feynman propagator!" 
      | Prop_Gauge xi -> invalid_arg "no on-shell massless gauge propagator!"
      | Prop_Rxi xi -> invalid_arg "no on-shell Rxi propagator!"
      | Prop_Vectorspinor -> printf "pj_grav(%s,%s,%s," p m gamma
      | Prop_Tensor_2 -> printf "pj_tensor(%s,%s,%s," p m gamma
      | Aux_Scalar | Aux_Spinor | Aux_ConjSpinor | Aux_Majorana
      | Aux_Vector | Aux_Tensor_1 -> printf "("
      | Only_Insertion -> printf "("

    let print_gauss f p m gamma =
      match M.propagator f with
      | Prop_Scalar -> printf "pg_phi(%s,%s,%s," p m gamma
      | Prop_Ghost -> printf "(0,1) * pg_phi(%s,%s,%s," p m gamma
      | Prop_Spinor ->
          printf "%s(%s,%s,%s," Fermions.psi_projector p m gamma
      | Prop_ConjSpinor ->
          printf "%s(%s,%s,%s," Fermions.psibar_projector p m gamma
      | Prop_Majorana ->
          printf "%s(%s,%s,%s," Fermions.chi_projector p m gamma
      | Prop_Col_Majorana ->
          printf "(-1.0_omega_prec/3.0_omega_prec) * %s(%s,%s,%s," 
            Fermions.chi_projector p m gamma
      | Prop_Unitarity -> printf "pg_unitarity(%s,%s,%s," p m gamma
      | Prop_Feynman | Prop_Col_Feynman -> 
          invalid_arg "no on-shell Feynman propagator!" 
      | Prop_Gauge xi -> invalid_arg "no on-shell massless gauge propagator!"
      | Prop_Rxi xi -> invalid_arg "no on-shell Rxi propagator!"
      | Prop_Tensor_2 -> printf "pg_tensor(%s,%s,%s," p m gamma
      | Aux_Scalar | Aux_Spinor | Aux_ConjSpinor | Aux_Majorana
      | Aux_Vector | Aux_Tensor_1 -> printf "("
      | Only_Insertion -> printf "("
      | _ -> invalid_arg "targets:print_gauss: not available"

    let print_fusion_diagnostics fusion =
      if warn diagnose_gauge then begin
        let lhs = F.lhs fusion in
        let f = F.flavor lhs
        and v = variable lhs
        and p = momentum lhs in
        let mass = M.mass_symbol f in
        match M.propagator f with
        | Prop_Gauge _ | Prop_Feynman
        | Prop_Rxi _ | Prop_Unitarity ->
            printf "    @[<2>%s =" v;
            List.iter print_current (F.rhs fusion); nl();
            begin match M.goldstone f with
            | None ->
                printf "    call omega_ward_%s(\"%s\",%s,%s,%s)"
                  (suffix diagnose_gauge) v mass p v; nl ()
            | Some (g, phase) ->
                let gv = add_tag lhs (M.flavor_symbol g ^ "_" ^ format_p lhs) in
                printf "    call omega_slavnov_%s"
                  (suffix diagnose_gauge);
                printf "(@[\"%s\",%s,%s,%s,@,%s*%s)"
                  v mass p v (format_constant phase) gv; nl ()
            end
        | _ -> ()
      end

    let print_fusion amplitude fusion =
      let lhs = F.lhs fusion in
      let f = F.flavor lhs in
      printf "    @[<2>%s = " (variable lhs);
      if F.on_shell amplitude lhs then
        print_projector f (momentum lhs)
          (M.mass_symbol f) (M.width_symbol f)
      else 
        if F.is_gauss amplitude lhs then
          print_gauss f (momentum lhs)
            (M.mass_symbol f) (M.width_symbol f)
        else
          print_propagator f (momentum lhs) 
            (M.mass_symbol f) (M.width_symbol f);
      List.iter print_current (F.rhs fusion);
      printf ")"; nl ()

    let print_fusions amplitude =
      let momenta =
        List.fold_left (fun seen f ->
          let wf = F.lhs f in
          let p = F.momentum_list wf in
          if not (P.mem p seen) then begin
            let rhs1 = List.hd (F.rhs f) in
            printf "    %s = %s" (momentum wf)
              (String.concat " + "
                 (List.map momentum (F.children rhs1))); nl ()
          end;
          print_fusion_diagnostics f;
          print_fusion amplitude f;
          P.add p seen) P.empty (F.fusions amplitude)
      in
      ()

    let print_braket name braket =
      let bra = F.bra braket
      and ket = F.ket braket in
      printf "    @[<2>%s = %s + " name name;
      begin match Fermions.reverse_braket (M.lorentz (F.flavor bra)) with
      | false ->
          printf "%s*(@," (variable bra);
          List.iter print_current ket;
          printf ")"
      | true ->
          printf "(@,";
          List.iter print_current ket;
          printf ")*%s" (variable bra)
      end; nl ()

(* \begin{equation}
     \ii T = \ii^{\#\text{vertices}}\ii^{\#\text{propagators}} \cdots
           = \ii^{n-2}\ii^{n-3} \cdots
           = -\ii(-1)^n \cdots
   \end{equation} *)

    let print_brakets name amplitude =
      match F.color amplitude with
      | Some (0, _) ->
          printf "    %s = 0 ! by color" name; nl ()
      | _ ->
          printf "    %s = 0" name; nl ();
          List.iter (print_braket name) (F.brakets amplitude);
          let n = List.length (F.externals amplitude) in
          if n mod 2 = 0 then begin
            printf "    %s = - %s ! %d vertices, %d propagators"
              name name (n - 2) (n - 3); nl ()
          end else begin
            printf "    ! %s = %s ! %d vertices, %d propagators"
              name name (n - 2) (n - 3); nl ()
          end;
          let s = 
            if !whiz_qcd then
              (F.symmetry amplitude) * (F.color_symm amplitude) 
                else
              F.symmetry amplitude 
          in
          if s > 1 then begin
            printf "    %s = %s / sqrt(%d.0_%s) ! symmetry factor"
              name name s !kind; nl ()
          end else begin
            printf "    ! unit symmetry factor"; nl ()
          end;
          if !whiz_qcd then
            begin
              printf "    ! Whizard color scripting; no color factor!\n";
            end else
          begin match F.color amplitude with
          | None ->
              Printf.eprintf "WARNING: color factor not known!\n";
              printf "    ! CAVEAT: color factor not known!"; nl ()
          | Some (1, 1) -> printf "    ! unit color factor"; nl ()
          | Some (num, den) ->
              printf "    %s = %s * sqrt (%d.0_%s / %d.0_%s)"
                name name num !kind den !kind; 
              printf " ! color factor"; nl ()
          end;
          printf "    ! Number of external adjoints: %d" (F.num_gl amplitude); nl()  (*i ;
          begin match F.color_fac amplitude with
          | None -> printf "    ! trivial color"; nl();
          | _ -> printf "    ! Not available yet"; nl(); 
          end  i*)


    let print_incoming wf =
      let p = momentum wf
      and s = spin wf
      and f = F.flavor wf in
      let m = M.mass_symbol f in
      match M.lorentz f with
      | Scalar -> printf "1"
      | BRS Scalar -> printf "(0,-1) * (%s * %s - %s**2)" p p m 
      | Spinor ->
          printf "%s (%s, - %s, %s)" Fermions.psi_incoming m p s
      | BRS Spinor ->
          printf "%s (%s, - %s, %s)" Fermions.brs_psi_incoming m p s
      | ConjSpinor ->
          printf "%s (%s, - %s, %s)" Fermions.psibar_incoming m p s
      | BRS ConjSpinor ->
          printf "%s (%s, - %s, %s)" Fermions.brs_psibar_incoming m p s
      | Majorana ->
          printf "%s (%s, - %s, %s)" Fermions.chi_incoming m p s
      | Maj_Ghost -> printf "ghost (%s, - %s, %s)" m p s
      | BRS Majorana ->
          printf "%s (%s, - %s, %s)" Fermions.brs_chi_incoming m p s
      | Vector | Massive_Vector -> 
          printf "eps (%s, - %s, %s)" m p s
(*i      | Ward_Vector -> printf "%s" p   i*)
      | BRS Vector | BRS Massive_Vector -> printf 
            "(0,1) * (%s * %s - %s**2) * eps (%s, -%s, %s)" p p m m p s 
      | Vectorspinor | BRS Vectorspinor -> 
          printf "%s (%s, - %s, %s)" Fermions.grav_incoming m p s
      | Tensor_1 -> invalid_arg "Tensor_1 only internal"
      | Tensor_2 -> printf "eps2 (%s, - %s, %s)" m p s
      | _ -> invalid_arg "no such BRST transformations"
      
    let print_outgoing wf =
      let p = momentum wf
      and s = spin wf
      and f = F.flavor wf in
      let m = M.mass_symbol f in
      match M.lorentz f with
      | Scalar -> printf "1"
      | BRS Scalar -> printf "(0,-1) * (%s * %s - %s**2)" p p m 
      | Spinor ->
          printf "%s (%s, %s, %s)" Fermions.psi_outgoing m p s
      | BRS Spinor ->
          printf "%s (%s, %s, %s)" Fermions.brs_psi_outgoing m p s
      | ConjSpinor ->
          printf "%s (%s, %s, %s)" Fermions.psibar_outgoing m p s
      | BRS ConjSpinor ->
          printf "%s (%s, %s, %s)" Fermions.brs_psibar_outgoing m p s
      | Majorana ->
          printf "%s (%s, %s, %s)" Fermions.chi_outgoing m p s
      | BRS Majorana ->
          printf "%s (%s, %s, %s)" Fermions.brs_chi_outgoing m p s
      | Maj_Ghost -> printf "ghost (%s, %s, %s)" m p s 
      | Vector | Massive_Vector -> 
          printf "conjg (eps (%s, %s, %s))" m p s
(*i       | Ward_Vector -> printf "%s" p   i*)
      | BRS Vector | BRS Massive_Vector -> printf 
            "(0,1) * (%s*%s-%s**2) * (conjg (eps (%s, %s, %s)))" p p m m p s 
      | Vectorspinor | BRS Vectorspinor -> 
          printf "%s (%s, %s, %s)" Fermions.grav_incoming m p s
      | Tensor_1 -> invalid_arg "Tensor_1 only internal"
      | Tensor_2 -> printf "conjg (eps2 (%s, %s, %s))" m p s
      | BRS _ -> invalid_arg "no such BRST transformations"

    let twice_spin wf =
      match M.lorentz (F.flavor wf) with
      | Scalar | BRS Scalar -> "0"
      | Spinor | ConjSpinor | Majorana | Maj_Ghost | Vectorspinor 
      | BRS Spinor | BRS ConjSpinor | BRS Majorana | BRS Vectorspinor -> "1"
      | Vector | BRS Vector | Massive_Vector | BRS Massive_Vector 
      | Tensor_1 -> "2"
      | Tensor_2 -> "4" 
      | BRS _ -> invalid_arg "Targets.twice_spin: no such BRST transformation"

    let print_argument_diagnostics amplitude =
      let externals = (F.externals amplitude) in
      let n = List.length externals
      and masses =
        List.map (fun wf -> M.mass_symbol (F.flavor wf)) externals
      and spins = List.map twice_spin externals in
      if warn diagnose_arguments then begin
        printf "    call omega_check_arguments_%s (%d, k, s)"
          (suffix diagnose_arguments) n; nl ()
      end;
      if warn diagnose_helicities then begin
        printf "    @[<2>call omega_check_helicities_%s ((/ "
          (suffix diagnose_helicities);
        print_list masses;
        printf " /), (/ ";
        print_list spins;
        printf " /), s)"; nl ()
      end;
      if warn diagnose_momenta then begin
        printf "    @[<2>call omega_check_momenta_%s ((/ "
          (suffix diagnose_momenta);
        print_list masses;
        printf " /), k)"; nl ()
      end

    let print_externals amplitude =
      let externals =
        List.combine
          (F.externals amplitude)
          (List.map (fun _ -> true) (F.incoming amplitude) @
           List.map (fun _ -> false) (F.outgoing amplitude)) in
      List.iter (fun (wf, incoming) ->
        if incoming then
          printf "    %s = - k(:,%d) ! incoming %s"
            (momentum wf) (ext_momentum wf)
            (M.flavor_to_string (F.flavor wf))
        else
          printf "    %s =   k(:,%d) ! outgoing %s"
            (momentum wf) (ext_momentum wf)
            (M.flavor_to_string (M.conjugate (F.flavor wf))); nl ()) externals;
      List.iter (fun (wf, incoming) ->
        printf "    %s = " (variable wf);
        (if incoming then print_incoming else print_outgoing) wf; nl ()) externals 

    let flavors_symbol flavors =
      String.concat "" (List.map M.flavor_symbol flavors)

    let flavors_to_string flavors =
      String.concat " " (List.map M.flavor_to_string flavors)

    let format_process amplitude =
      flavors_to_string (F.incoming amplitude) ^ " -> " ^
      flavors_to_string (F.outgoing amplitude)
        
    let print_function_header amplitude =
      let externals = F.externals amplitude in
      printf "  ! process: %s" (format_process amplitude); nl();
      printf "  "; if !fortran95 then printf "pure ";
      printf "@[<2>function %s (k, s) result (amp)"
        (flavors_symbol (flavors amplitude)); nl ();
      printf "    @[<2>real(kind=%s), dimension(0:,:), intent(in) :: k"
        !kind; nl ();
      printf "    @[<2>integer, dimension(:), intent(in) :: s"; nl ();
      printf "    complex(kind=%s) :: amp" !kind; nl ();
      printf "    @[<2>type(momentum) :: ";
      print_list (List.map momentum externals); nl ()

    let print_function_footer amplitude =
      printf "  end function %s" (flavors_symbol (flavors amplitude)); nl ()

    let print_fudge_factor name amplitude =
      List.iter (fun wf ->
        let p = momentum wf
        and f = F.flavor wf in
        match M.width f with
        | Fudged ->
            let m = M.mass_symbol f
            and w = M.width_symbol f in
            printf "    if (%s > 0.0_%s) then" w !kind; nl ();
            printf "      @[<2>%s = %s@ * (%s*%s - %s**2)"
              name name p p m;
            printf "@ / cmplx (%s*%s - %s**2, %s*%s, kind=%s)"
              p p m m w !kind; nl();
            printf "    end if"; nl ()
        | _ -> ()) (F.s_channel amplitude)

    let print_one_amplitude amplitude =
      print_function_header amplitude;
      print_declarations amplitude;
      print_argument_diagnostics amplitude;
      print_externals amplitude;
      print_fusions amplitude;
      print_brakets "amp" amplitude;
      print_fudge_factor "amp" amplitude;
      print_function_footer amplitude;
      nl ()

(* \thocwmodulesubsection{Spin \&\ Flavor Tables} *)

    let num_particles amplitudes =
      List.length (MF.incoming amplitudes) +
        List.length (MF.outgoing amplitudes)

    let num_particles_in amplitudes =
      List.length (MF.incoming amplitudes)

    let num_particles_out amplitudes =
      List.length (MF.outgoing amplitudes)

(* Recently [Product.list] began to guarantee lexicographic order for sorted
   arguments.  Anyway, we still force a lexicographic order. *)
    let rec order_spin_table s1 s2 =
      match s1, s2 with
      | h1 :: t1, h2 :: t2 ->
          let c = compare h1 h2 in
          if c <> 0 then
            c
          else
            order_spin_table t1 t2
      | [], [] -> 0
      | _ -> invalid_arg "order_spin_table: inconsistent lengths"
      
    let sort_spin_table table =
      List.sort order_spin_table table

    let print_spin_index amplitudes =
      printf "  @[<5>"; if !fortran95 then printf "pure ";
      printf "function spin_index (s) result (n)"; nl ();
      printf "    integer, dimension(:), intent(in) :: s"; nl ();
      printf "    integer :: n"; nl ();
      printf "    @[<2>integer, dimension(%d)" (num_particles amplitudes);
      printf ", parameter ::@ num_helicities =@ (/ %s /)"
        (String.concat ", "
           (List.map (fun f ->
             string_of_int (List.length (helicities (List.hd f))))
              (MF.incoming amplitudes @ MF.outgoing amplitudes))); nl ();
      printf "    integer :: j, p"; nl ();
      printf "    n = 1"; nl ();
      printf "    p = 1"; nl ();
      printf "    do j = size (num_helicities), 1, -1"; nl ();
      printf "      select case (num_helicities(j))"; nl ();
      printf "      case (3)"; nl ();
      printf "        n = n + p * (s(j) + 1) "; nl ();
      printf "      case (2)"; nl ();
      printf "        n = n + p * (s(j) + 1) / 2"; nl ();
      printf "      end select"; nl ();
      printf "      p = p * num_helicities(j)"; nl ();
      printf "    end do"; nl ();
      printf "  end function spin_index"; nl ();
      nl ()

(* The following abomination is required to keep the number of continuation
   lines as low as possible.  FORTRAN77-style \texttt{DATA} statements
   are actually a bit nicer here, but they are nor available for
   \emph{constant} arrays. *)

(* \begin{dubious}
     We used to have a more elegent design with a sentinel~0 added to each
     initializer, but some revisions of the Compaq/Digital Compiler have a
     bug that causes it to reject this variant.
   \end{dubious} *)

    let print_spin_table n abbrev name = function
      | [] ->
          printf "  @[<2>integer, dimension(%d,0), private ::" n;
          printf "@ table_spin_%s" name; nl ()
      | _ :: tuples' as tuples ->
          let num_tuples =
            pred (List.fold_left (fun i tuple ->
              printf
                "  @[<2>integer, dimension(%d), parameter, private ::"
                n;
              printf "@ %s%04d = (/ %s /)" abbrev i
                (String.concat ", " (List.map (Printf.sprintf "%2d") tuple));
              nl (); succ i) 1 tuples) in
          printf
            "  @[<2>integer, dimension(%d,%d), parameter, private ::"
            n num_tuples;
          printf "@ table_spin_%s =@ reshape ( (/" name;
          printf "@ %s%04d" abbrev 1;
          ignore (List.fold_left (fun i tuple ->
            printf ",@ %s%04d" abbrev i; succ i) 2 tuples');
          printf "@ /), (/ %d, %d /) )" n num_tuples; nl ()

    let print_spin_tables amplitudes =
      let n = num_particles amplitudes 
          and n_in  = num_particles_in amplitudes 
          and n_out = num_particles_out amplitudes
      in
      let hin =
        List.map (fun f -> helicities (List.hd f)) (MF.incoming amplitudes)
      and hout =
        List.map (fun f -> helicities (List.hd f)) (MF.outgoing amplitudes) in
      print_spin_table n "s" "states"
        (sort_spin_table (Product.list (fun h -> h) (hin @ hout)));
      print_spin_table n_in "si" "states_in"
        (sort_spin_table (Product.list (fun h -> h) hin));
      print_spin_table n_out "so" "states_out"
        (sort_spin_table (Product.list (fun h -> h) hout));
      nl ()

    let print_flavor_table n abbrev name = function
      | [] ->
          printf "  @[<2>integer, dimension(%d,0), private ::" n;
          printf "@ table_flavor_%s" name; nl ()
      | _ :: tuples' as tuples ->
          let num_tuples =
            pred (List.fold_left (fun i tuple ->
              printf
                "  @[<2>integer, dimension(%d), parameter, private ::"
                n;
              printf "@ %s%04d = (/ %s /) ! %s" abbrev i
                (String.concat ", "
                   (List.map (fun f -> Printf.sprintf "%3d" (M.pdg f)) tuple))
                (String.concat " " (List.map M.flavor_to_string tuple));
              nl (); succ i) 1 tuples) in
          printf
            "  @[<2>integer, dimension(%d,%d), parameter, private ::"
            n num_tuples;
          printf "@ table_flavor_%s =@ reshape ( (/" name;
          printf "@ %s%04d" abbrev 1;
          ignore (List.fold_left (fun i tuple ->
            printf ",@ %s%04d" abbrev i; succ i) 2 tuples');
          printf "@ /), (/ %d, %d /) )" n num_tuples; nl ()

    let print_flavor_table_mult n abbrev name mult = function
      | [] ->
          printf "  @[<2>integer, dimension(%d,0), private ::" n;
          printf "@ table_flavor_%s" name; nl ();
          printf "  @[<2>integer, dimension(0), private ::";
          printf "@ table_flavor_%s_mult" name; nl ()
      | _ :: tuples' as tuples ->
          let num_tuples =
            pred (List.fold_left (fun i tuple ->
              printf
                "  @[<2>integer, dimension(%d), parameter, private ::"
                n;
              printf "@ %s%04d = (/ %s /) ! %s" abbrev i
                (String.concat ", "
                   (List.map (fun f -> Printf.sprintf "%3d" (M.pdg f)) tuple))
                (String.concat " " (List.map M.flavor_to_string tuple));
              nl ();
              printf "  @[<2>integer, parameter, private ::";
              printf "@ %s%04dm = %d" abbrev i (mult tuple);
              nl (); succ i) 1 tuples) in
          printf
            "  @[<2>integer, dimension(%d,%d), parameter, private ::"
            n num_tuples;
          printf "@ table_flavor_%s =@ reshape ( (/" name;
          printf "@ %s%04d" abbrev 1;
          ignore (List.fold_left (fun i tuple ->
            printf ",@ %s%04d" abbrev i; succ i) 2 tuples');
          printf "@ /), (/ %d, %d /) )" n num_tuples; nl ();
          printf "  @[<2>integer, dimension(%d), parameter, private ::"
            num_tuples;
          printf "@ table_flavor_%s_mult =@ (/" name;
          printf "@ %s%04dm" abbrev 1;
          ignore (List.fold_left (fun i tuple ->
            printf ",@ %s%04dm" abbrev i; succ i) 2 tuples');
          printf "@ /)"; nl ()

    let print_flavor_tables amplitudes =
      let n = num_particles amplitudes 
          and n_in  = num_particles_in amplitudes
          and n_out = num_particles_out amplitudes
      in
      print_flavor_table_mult n "f" "states"
        (MF.multiplicities_in_out amplitudes) (MF.allowed_in_out amplitudes);
      print_flavor_table n "z" "zeros" (MF.forbidden_in_out amplitudes);
      print_flavor_table_mult n_in "fi" "states_in"
        (MF.multiplicities_in amplitudes) (MF.allowed_in amplitudes);
      print_flavor_table 2 "zi" "zeros_in" (MF.forbidden_in amplitudes);
      print_flavor_table_mult n_out "fo" "states_out"
        (MF.multiplicities_out amplitudes) (MF.allowed_out amplitudes);
      print_flavor_table n_out "zo" "zeros_out"
        (MF.forbidden_out amplitudes);
      nl ()

    let print_symmetry_table amplitudes =
      let amplitudes = MF.allowed amplitudes in
      let n = List.length amplitudes in
      printf "  @[<2>integer, dimension(%d), private ::" n;
      printf "@ table_symmetry = (/";
      begin match List.map F.symmetry amplitudes with
      | [] -> ()
      | s :: symmetry ->
          printf "@ %3d" s;
          List.iter (fun s' -> printf ",@ %3d" s') symmetry
      end;
      printf "@ /)"; nl (); nl ()

(* \thocwmodulesubsection{Dispatch Flavor Amplitudes} *)

(* We can customize the generic [Trie] module so that it will dump the trie. *)

    module FT = Trie.Make
        (Map.Make (struct type t = M.flavor let compare = compare end))

    let flavor_trie_amplitude amplitudes =
      List.fold_left (fun t a ->
        FT.add (flavors a)
          ("amp = " ^ flavors_symbol (flavors a) ^ " (k, s)",
           format_process a) t)
        FT.empty (MF.allowed amplitudes)

    let flavor_trie_index amplitudes =
      let _, trie =
        List.fold_left (fun (i, t) a ->
          (succ i,
           FT.add (flavors a) ("n = " ^ string_of_int i, format_process a) t))
          (1, FT.empty) (MF.allowed amplitudes) in
      trie

    let print_trie name trie =
      let indent n = String.make (2 * n) ' ' in
      FT.export
        (fun n ->
          let n' = succ n in
          printf "%s  select case (%s(%d))" (indent n') name n'; nl ())
        (fun n ->
          let n' = succ n in
          printf "%s  end select" (indent n'); nl ())
        (fun n rev_key ->
          if n > 0 then begin
            printf "%s  case (%d)" (indent n) (M.pdg (List.hd rev_key));
            nl ()
           end)
        (fun n rev_key (stmt, doc) ->
          printf "%s  case (%d) ! %s"
            (indent n) (M.pdg (List.hd rev_key)) doc; nl ();
          printf "%s    %s" (indent n) stmt; nl ())
        trie

    let print_flavor_index amplitudes =
      printf "  @[<5>"; if !fortran95 then printf "pure ";
      printf "function flavor_index (f) result (n)"; nl ();
      printf "    integer, dimension(:), intent(in) :: f"; nl ();
      printf "    integer :: n"; nl ();
      printf "    n = 0"; nl ();
      print_trie "f" (flavor_trie_index amplitudes);
      printf "  end function flavor_index"; nl ();
      nl ()

    let print_amplitude amplitudes =
      printf "  @[<5>"; if !fortran95 then printf "pure ";
      printf "function amplitude (k, s, f)@ result (amp)"; nl ();
      printf "    @[<2>real(kind=%s), dimension(0:,:), intent(in) :: k"
        !kind; nl ();
      printf "    @[<2>integer, dimension(:), intent(in) :: s, f"; nl ();
      printf "    complex(kind=%s) :: amp" !kind; nl ();
      printf "    amp = 0"; nl ();
      print_trie "f" (flavor_trie_amplitude amplitudes);
      printf "  end function amplitude"; nl ();
      nl ()

    let print_amplitude_f amplitudes =
      printf "  @[<5>"; if !fortran95 then printf "pure ";
      printf "function amplitude_f (k, s, f)@ result (amp)"; nl ();
      printf "    @[<2>real(kind=%s), dimension(0:,:), intent(in) :: k"
        !kind; nl ();
      printf "    @[<2>integer, dimension(:), intent(in) :: s"; nl ();
      printf "    @[<2>integer, intent(in) :: f"; nl ();
      printf "    complex(kind=%s) :: amp" !kind; nl ();
      printf "    select case (f)"; nl ();
      ignore (List.fold_left (fun i a ->
        printf "    case (%d) ! %s" i (format_process a); nl ();
        printf "      amp = %s (k, s)"
          (flavors_symbol (flavors a)); nl ();
        succ i) 1 (MF.allowed amplitudes));
      printf "    case default"; nl ();
      printf "      amp = 0"; nl ();
      printf "    end select"; nl ();
      printf "  end function amplitude_f"; nl ();
      nl ()

    let print_amplitude_1 amplitudes =
      printf "  @[<5>"; if !fortran95 then printf "pure ";
      printf "function amplitude_1 (k, s, f)@ result (amp)"; nl ();
      printf "    @[<2>real(kind=%s), dimension(0:,:), intent(in) :: k"
        !kind; nl ();
      printf "    @[<2>integer, intent(in) :: s, f"; nl ();
      printf "    complex(kind=%s) :: amp" !kind; nl ();
      printf "    select case (f)"; nl ();
      ignore (List.fold_left (fun i a ->
        printf "    case (%d) ! %s" i (format_process a); nl ();
        printf "      amp = %s (k, table_spin_states (:,s))"
          (flavors_symbol (flavors a)); nl ();
        succ i) 1 (MF.allowed amplitudes));
      printf "    case default"; nl ();
      printf "      amp = 0"; nl ();
      printf "    end select"; nl ();
      printf "  end function amplitude_1"; nl ();
      nl ()

    let print_amplitude_2 amplitudes =
      printf "  @[<5>"; if !fortran95 then printf "pure ";
      printf "function amplitude_2 ";
      printf "(k, s_in, f_in, s_out, f_out)@ result (amp)"; nl ();
      printf "    @[<2>real(kind=%s), dimension(0:,:), intent(in) :: k"
        !kind; nl ();
      printf
        "    @[<2>integer, intent(in) :: s_in, f_in, s_out, f_out";
      nl ();
      printf "    complex(kind=%s) :: amp" !kind; nl ();
      printf
        "    integer, dimension(size(table_spin_states,dim=1)) :: s";
      nl ();
      printf "    s(1:size(table_spin_states_in,dim=1)) =";
      printf " table_spin_states_in(:,s_in)"; nl ();
      printf "    s(size(table_spin_states_in,dim=1)+1:) =";
      printf " table_spin_states_out(:,s_out)"; nl ();
      printf "    amp = 0"; nl ();
      printf "    select case (f_in)"; nl ();
      ignore (List.fold_left (fun i_in f_in ->
        printf "    case (%d)" i_in; nl ();
        printf "      select case (f_out)"; nl ();
        ignore (List.fold_left (fun i_out f_out ->
          let f = f_in @ f_out in
          if MF.is_allowed amplitudes f then begin
            printf "      case (%d) ! %s -> %s"
              i_out (flavors_symbol f_in) (flavors_symbol f_out); nl ();
            printf "        amp = %s%s (k, s)"
              (flavors_symbol f_in) (flavors_symbol f_out); nl ()
          end;
          succ i_out) 1 (MF.allowed_out amplitudes));
        printf "      end select"; nl ();
        succ i_in) 1 (MF.allowed_in amplitudes));
      printf "    end select"; nl ();
      printf "  end function amplitude_2"; nl ();
      nl ()

(* \begin{equation}
     \rho \to \rho' = T \rho T^{\dagger}
   \end{equation}
   I.\,e.
   \begin{equation}
     \rho'_{ff'} = \sum_{ii'} T_{fi} \rho_{ii'} T^{*}_{i'f'}
   \end{equation} *)
    let print_scatter () =
      printf "  @[<5>"; if !fortran95 then printf "pure ";
      printf "subroutine scatter (k, rho_in, rho_out)"; nl ();
      printf "    real(kind=omega_prec), dimension(0:,:),";
      printf " intent(in) :: k"; nl ();
      printf
        "    complex(kind=omega_prec), dimension(:,:,:,:),";
      printf " intent(in) :: rho_in"; nl ();
      printf
        "    complex(kind=omega_prec), dimension(:,:,:,:),";
      printf " intent(inout) :: rho_out"; nl ();
      printf "    @[<10>call omega_scatter";
      printf " (amplitude_2,@ k,@ rho_in, rho_out";
      printf ",@ table_flavor_states_out_mult)"; nl ();
      printf "  end subroutine scatter"; nl ();
      nl ()

    let print_scatter_nonzero () =
      printf "  @[<5>"; if !fortran95 then printf "pure ";
      printf "subroutine@ scatter_nonzero";
      printf "@ (k, rho_in, rho_out, zero, n)"; nl ();
      printf
        "    real(kind=omega_prec), dimension(0:,:),";
      printf " intent(in) :: k"; nl ();
      printf
        "    complex(kind=omega_prec), dimension(:,:,:,:),";
      printf " intent(in) :: rho_in"; nl ();
      printf
        "    complex(kind=omega_prec), dimension(:,:,:,:),";
      printf " intent(inout) :: rho_out"; nl ();
      printf "    integer, dimension(:,:,:,:),";
      printf " intent(inout) :: zero";
      nl ();
      printf "    integer, intent(in) :: n"; nl ();
      printf "    @[<10>call omega_scatter_nonzero";
      printf " (amplitude_2_nonzero,@ k, rho_in, rho_out";
      printf ",@ table_flavor_states_out_mult,@ zero, n)"; nl ();
      printf "  end subroutine scatter_nonzero"; nl ();
      nl ()

    let print_scatter_colored_nonzero () =
      printf "  @[<5>"; if !fortran95 then printf "pure ";
      printf "subroutine@ scatter_colored_nonzero";
      printf "@ (k, rho_in, rho_out, rho_col_out, zero, n)"; nl ();
      printf
        "    real(kind=omega_prec), dimension(0:,:),";
      printf " intent(in) :: k"; nl ();
      printf
        "    complex(kind=omega_prec), dimension(:,:,:,:),";
      printf " intent(in) :: rho_in"; nl ();
      printf
        "    complex(kind=omega_prec), dimension(:,:,:,:),";
      printf " intent(inout) :: rho_out"; nl ();
      printf
        "    complex(kind=omega_prec), dimension(:,:,:,:,:),";
      printf " intent(inout) :: rho_col_out"; nl ();
      printf "    integer, dimension(:,:,:,:),";
      printf " intent(inout) :: zero";
      nl ();
      printf "    integer, intent(in) :: n"; nl ();
      printf "    @[<10>call omega_scatter_nonzero";
      printf " (amplitude_2_nonzero,@ k, rho_in, rho_out";
      printf ",@ table_flavor_states_out_mult,@ zero, n)"; nl ();
      printf "    rho_col_out = 1"; nl ();
      printf "  end subroutine scatter_colored_nonzero"; nl ();
      nl ()

(* \begin{equation}
     \rho'_{f} = \sum_i T_{fi} \rho_{i} T^{*}_{if}
               = \sum_i |T_{fi}|^2 \rho_{i}
   \end{equation} *)
    let print_scatter_diagonal () =
      printf "  @[<5>"; if !fortran95 then printf "pure ";
      printf "subroutine@ scatter_diagonal@ (k, rho_in, rho_out)"; nl ();
      printf "    real(kind=omega_prec), dimension(0:,:),";
      printf " intent(in) :: k"; nl ();
      printf "    real(kind=omega_prec), dimension(:,:),";
      printf " intent(in) :: rho_in"; nl ();
      printf "    real(kind=omega_prec), dimension(:,:),";
      printf " intent(inout) :: rho_out"; nl ();
      printf "    @[<10>call omega_scatter_diagonal";
      printf " (amplitude_2,@ k,@ rho_in, rho_out";
      printf ",@ table_flavor_states_out_mult)"; nl ();
      printf "  end subroutine scatter_diagonal"; nl ();
      nl ()

    let print_scatter_diagonal_nonzero () =
      printf "  @[<5>"; if !fortran95 then printf "pure ";
      printf "subroutine@ scatter_diagonal_nonzero";
      printf "@ (k, rho_in, rho_out, zero, n)"; nl ();
      printf "    real(kind=omega_prec), dimension(0:,:),";
      printf " intent(in) :: k"; nl ();
      printf "    real(kind=omega_prec), dimension(:,:),";
      printf " intent(in) :: rho_in"; nl ();
      printf "    real(kind=omega_prec), dimension(:,:),";
      printf " intent(inout) :: rho_out"; nl ();
      printf "    integer, dimension(:,:,:,:),";
      printf " intent(inout) :: zero"; nl ();
      printf "    integer, intent(in) :: n"; nl ();
      printf "    @[<10>call omega_scatter_diagonal_nonzero";
      printf " (amplitude_2_nonzero,@ k,@ rho_in, rho_out";
      printf ",@ table_flavor_states_out_mult,@ zero, n)"; nl ();
      printf "  end subroutine scatter_diagonal_nonzero"; nl ();
      nl ()

    let print_scatter_diagonal_colored_nonzero () =
      printf "  @[<5>"; if !fortran95 then printf "pure ";
      printf "subroutine@ scatter_diagonal_colored_nz";
      printf "@ (k, rho_in, rho_out, zero, n)"; nl ();
      printf "    real(kind=omega_prec), dimension(0:,:),";
      printf " intent(in) :: k"; nl ();
      printf "    real(kind=omega_prec), dimension(:,:),";
      printf " intent(in) :: rho_in"; nl ();
      printf "    real(kind=omega_prec), dimension(:,:),";
      printf " intent(inout) :: rho_out"; nl ();
      printf "    integer, dimension(:,:,:,:),";
      printf " intent(inout) :: zero"; nl ();
      printf "    integer, intent(in) :: n"; nl ();
      printf "    @[<10>call omega_scatter_diagonal_nonzero";
      printf " (amplitude_2_nonzero,@ k,@ rho_in, rho_out";
      printf ",@ table_flavor_states_out_mult,@ zero, n)"; nl ();
      printf "  end subroutine scatter_diagonal_colored_nz"; nl ();
      nl ()

    let print_dispatch_functions amplitudes =
      print_amplitude amplitudes;
      print_amplitude_1 amplitudes;
      print_amplitude_f amplitudes;
      print_amplitude_2 amplitudes;
      print_scatter ();
      print_scatter_diagonal ()

    let print_amplitude_nonzero () =
      printf "  @[<5>"; if !fortran95 then printf "pure ";
      printf "subroutine amplitude_nonzero";
      printf "@ (amp, k, s, f, zero, n)"; nl ();
      printf "    complex(kind=omega_prec), intent(out) :: amp"; nl ();
      printf "    real(kind=omega_prec), dimension(0:,:),";
      printf " intent(in) :: k"; nl ();
      printf "    integer, dimension(:), intent(in) :: s, f"; nl ();
      printf "    integer, dimension(:,:), intent(inout) :: zero"; nl ();
      printf "    integer, intent(in) :: n"; nl ();
      printf "    @[<2>call amplitude_1_nonzero";
      printf "@ (amp, k, spin_index (s), flavor_index (f),";
      printf " zero, n)"; nl ();
      printf "  end subroutine amplitude_nonzero"; nl ();
      nl ()

    let print_amplitude_f_nonzero () =
      printf "  @[<5>"; if !fortran95 then printf "pure ";
      printf "subroutine amplitude_f_nonzero";
      printf "@ (amp, k, s, f, zero, n)"; nl ();
      printf "    complex(kind=omega_prec), intent(out) :: amp"; nl ();
      printf "    real(kind=omega_prec), dimension(0:,:),";
      printf " intent(in) :: k"; nl ();
      printf "    integer, dimension(:), intent(in) :: s"; nl ();
      printf "    integer, intent(in) :: f"; nl ();
      printf "    integer, dimension(:,:), intent(inout) :: zero"; nl ();
      printf "    integer, intent(in) :: n"; nl ();
      printf "    @[<2>call amplitude_1_nonzero";
      printf "@ (amp, k, spin_index (s), f, zero, n)"; nl ();
      printf "  end subroutine amplitude_f_nonzero"; nl ();
      nl ()

    let print_amplitude_1_nonzero () =
      printf "  @[<5>"; if !fortran95 then printf "pure ";
      printf "subroutine amplitude_1_nonzero";
      printf "@ (amp, k, s, f, zero, n)"; nl ();
      printf "    complex(kind=omega_prec), intent(out) :: amp"; nl ();
      printf "    real(kind=omega_prec), dimension(0:,:),";
      printf " intent(in) :: k"; nl ();
      printf "    integer, intent(in) :: s, f"; nl ();
      printf "    integer, dimension(:,:), intent(inout) :: zero"; nl ();
      printf "    integer, intent(in) :: n"; nl ();
      printf "    @[<7>call omega_amplitude_1_nonzero";
      printf "@ (amplitude_1, amp, k,@ s, f,@ zero, n)"; nl ();
      printf "  end subroutine amplitude_1_nonzero"; nl ();
      nl ()

    let print_amplitude_2_nonzero () =
      printf "  @[<5>"; if !fortran95 then printf "pure ";
      printf "subroutine amplitude_2_nonzero";
      printf "@ (amp, k, s_in, f_in, s_out, f_out, zero, n)"; nl ();
      printf "    complex(kind=omega_prec), intent(out) :: amp"; nl ();
      printf "    real(kind=omega_prec), dimension(0:,:),";
      printf " intent(in) :: k"; nl ();
      printf "    integer, intent(in) :: s_in, f_in, s_out, f_out"; nl ();
      printf "    integer, dimension(:,:,:,:),";
      printf " intent(inout) :: zero"; nl ();
      printf "    integer, intent(in) :: n"; nl ();
      printf "    @[<7>call omega_amplitude_2_nonzero";
      printf "@ (amplitude_2, amp, k,@ s_in, f_in, s_out, f_out";
      printf ",@ zero, n)"; nl ();
      printf "  end subroutine amplitude_2_nonzero"; nl ();
      nl ()

    let print_dispatch_functions_nonzero () =
      print_amplitude_nonzero ();
      print_amplitude_1_nonzero ();
      print_amplitude_f_nonzero ();
      print_amplitude_2_nonzero ();
      print_scatter_nonzero ();
      if new_whizard then
        print_scatter_colored_nonzero ();
      print_scatter_diagonal_nonzero ();
      if new_whizard then
        print_scatter_diagonal_colored_nonzero ()

    let print_spin_sum_sqme () =
      printf "  @[<5>"; if !fortran95 then printf "pure ";
      printf "function spin_sum_sqme (k, f, smask) result (amp2)"; nl();
      printf "    real(kind=omega_prec), dimension(0:,:),";
      printf " intent(in) :: k"; nl();
      printf "    integer, dimension(:), intent(in) :: f"; nl();
      printf "    logical, dimension(:), intent(in),";
      printf " optional :: smask"; nl ();
      printf "    real(kind=omega_prec) :: amp2"; nl();
      printf "    amp2 = spin_sum_sqme_1";
      printf " (k, flavor_index (f), smask)"; nl();
      printf "  end function spin_sum_sqme"; nl();
      nl ()

    let print_spin_sum_sqme_1 () =
      printf "  @[<5>"; if !fortran95 then printf "pure ";
      printf "function spin_sum_sqme_1 (k, f, smask)";
      printf " result (amp2)"; nl ();
      printf "    real(kind=omega_prec), dimension(0:,:),";
      printf " intent(in) :: k"; nl ();
      printf "    integer, intent(in) :: f"; nl ();
      printf "    logical, dimension(:), intent(in),";
      printf " optional :: smask"; nl ();
      printf "    real(kind=omega_prec) :: amp2"; nl ();
      printf "    amp2 = @[<2>omega_spin_sum_sqme_1";
      printf "@ (amplitude_1, k, f, @ size (table_spin_states, dim=2)";
      printf ",@ smask)"; nl ();
      printf "  end function spin_sum_sqme_1"; nl ();
      nl ()

    let print_sum_sqme () =
      printf "  @[<5>"; if !fortran95 then printf "pure ";
      printf "function sum_sqme (k, smask, fmask) result (amp2)"; nl ();
      printf "    real(kind=omega_prec), dimension(0:,:),";
      printf " intent(in) :: k"; nl ();
      printf "    logical, dimension(:), intent(in),";
      printf " optional :: smask, fmask"; nl ();
      printf "    real(kind=omega_prec) :: amp2"; nl ();
      printf "    amp2 = @[<2>omega_sum_sqme@ (amplitude_1, k";
      printf ",@ size (table_spin_states, dim=2)";
      printf ",@ size (table_flavor_states, dim=2)";
      printf ",@ table_flavor_states_mult,@ smask, fmask)"; nl ();
      printf "  end function sum_sqme"; nl ();
      nl ()

    let print_dispatch_functions_sqme () =
      print_spin_sum_sqme ();
      print_spin_sum_sqme_1 ();
      print_sum_sqme ()

    let print_spin_sum_sqme_nonzero () =
      printf "  @[<5>"; if !fortran95 then printf "pure ";
      printf "subroutine spin_sum_sqme_nonzero";
      printf "@ (amp2, k, f, zero, n, smask)"; nl ();
      printf "    real(kind=omega_prec), intent(out) :: amp2"; nl ();
      printf "    real(kind=omega_prec), dimension(0:,:),";
      printf " intent(in) :: k"; nl ();
      printf "    integer, dimension(:), intent(in) :: f"; nl ();
      printf "    integer, dimension(:,:), intent(inout) :: zero"; nl ();
      printf "    integer, intent(in) :: n"; nl ();
      printf "    logical, dimension(:), intent(in),";
      printf " optional :: smask"; nl ();
      printf "    @[<2>call spin_sum_sqme_1_nonzero";
      printf "@ (amp2, k, flavor_index (f), zero, n, smask)"; nl ();
      printf "  end subroutine spin_sum_sqme_nonzero"; nl ();
      nl ()

    let print_spin_sum_sqme_1_nonzero () =
      printf "  @[<5>"; if !fortran95 then printf "pure ";
      printf "subroutine spin_sum_sqme_1_nonzero";
      printf "@ (amp2, k, f, zero, n, smask)"; nl ();
      printf "    real(kind=omega_prec), intent(out) :: amp2"; nl ();
      printf "    real(kind=omega_prec), dimension(0:,:),";
      printf " intent(in) :: k"; nl ();
      printf "    integer, intent(in) :: f"; nl ();
      printf "    integer, dimension(:,:), intent(inout) :: zero"; nl ();
      printf "    integer, intent(in) :: n"; nl ();
      printf "    logical, dimension(:), intent(in),";
      printf " optional :: smask"; nl ();
      printf "    @[<7>call omega_spin_sum_sqme_1_nonzero";
      printf "@ (amplitude_1,@ amp2, k,@ f,@ zero, n,@ smask);"; nl ();
      printf "  end subroutine spin_sum_sqme_1_nonzero"; nl ();
      nl ()

    let print_sum_sqme_nonzero () =
      printf "  @[<5>"; if !fortran95 then printf "pure ";
      printf "subroutine sum_sqme_nonzero";
      printf "@ (amp2, k, zero, n, smask, fmask)"; nl ();
      printf "    real(kind=omega_prec), intent(out) :: amp2"; nl ();
      printf "    real(kind=omega_prec), dimension(0:,:),";
      printf " intent(in) :: k"; nl ();
      printf "    integer, dimension(:,:), intent(inout) :: zero"; nl ();
      printf "    integer, intent(in) :: n"; nl ();
      printf "    logical, dimension(:), intent(in),";
      printf " optional :: smask, fmask"; nl ();
      printf "    @[<7>call omega_sum_sqme_nonzero";
      printf "@ (amplitude_1,@ amp2, k,@ table_flavor_states_mult";
      printf ",@ zero, n,@ smask, fmask)"; nl ();
      printf "  end subroutine sum_sqme_nonzero"; nl ();
      nl ()

    let print_dispatch_functions_sqme_nonzero () =
      print_spin_sum_sqme_nonzero ();
      print_spin_sum_sqme_1_nonzero ();
      print_sum_sqme_nonzero ()

(* \thocwmodulesubsection{Maintenance \&\ Inquiry Functions} *)

    let print_maintenance_functions () =
      printf "  subroutine allocate_zero_1 (zero)"; nl ();
      printf "    integer, dimension(:,:), pointer :: zero"; nl ();
      printf "    allocate (zero(@[size(table_spin_states,dim=2)";
      printf ",@,size(table_flavor_states,dim=2)))"; nl ();
      printf "    zero = 0"; nl ();
      printf "  end subroutine allocate_zero_1"; nl ();
      nl ();
      printf "  subroutine allocate_zero_2 (zero)"; nl ();
      printf "    integer, dimension(:,:,:,:), pointer :: zero"; nl ();
      printf "    allocate(zero(@[size(table_spin_states_in,dim=2)";
      printf ",@,size(table_flavor_states_in,dim=2)";
      printf ",@,size(table_spin_states_out,dim=2)";
      printf ",@,size(table_flavor_states_out,dim=2)))"; nl ();
      printf "    zero = 0"; nl ();
      printf "  end subroutine allocate_zero_2"; nl ();
      nl ();
      printf "  subroutine create ()"; nl ();
      printf "  end subroutine create"; nl ();
      if !whizard then begin
        printf "  subroutine reset (par)"; nl ();
        printf "    type(parameter_set), intent(in) :: par"; nl ();
        printf "    call import_from_whizard (par)"; nl ();
        printf "  end subroutine reset"; nl ();
      end else begin
        printf "  subroutine reset ()"; nl ();
        printf "  end subroutine reset"; nl ();
      end;
      printf "  subroutine destroy ()"; nl ();
      printf "  end subroutine destroy"; nl ();
      nl ()

    let print_inquiry_function_declarations name =
      printf "  @[<2>public :: number_%s" name;
      printf ",@ number_%s_in,@ number_%s_out" name name;
      printf ",@ %s,@ %s_in, %s_out" name name name;
      nl ()
      
    let print_numeric_inquiry_functions () = 
      printf "  @[<5>"; if !fortran95 then printf "pure ";
      printf "function number_particles () result (n)"; nl ();
      printf "    integer :: n"; nl ();
      printf "    n = size (table_flavor_states, dim=1)"; nl ();
      printf "  end function number_particles"; nl ();
      printf "  @[<5>"; if !fortran95 then printf "pure ";
      printf "function number_particles_in () result (n)"; nl ();
      printf "    integer :: n"; nl ();
      printf "    n = size (table_flavor_states_in, dim=1)"; nl ();
      printf "  end function number_particles_in"; nl ();
      printf "  @[<5>"; if !fortran95 then printf "pure ";
      printf "function number_particles_out () result (n)"; nl ();
      printf "    integer :: n"; nl ();
      printf "    n = size (table_flavor_states_out, dim=1)"; nl ();
      printf "  end function number_particles_out"; nl ();
      nl ()

    let print_inquiry_functions name =
      printf "  @[<5>"; if !fortran95 then printf "pure ";
      printf "function number_%s () result (n)" name; nl ();
      printf "    integer :: n"; nl ();
      printf "    n = size (table_%s, dim=2)" name; nl ();
      printf "  end function number_%s" name; nl ();
      printf "  @[<5>"; if !fortran95 then printf "pure ";
      printf "subroutine %s (a)" name; nl ();
      printf "    integer, dimension(:,:), intent(inout) :: a"; nl ();
      printf "    a = table_%s" name; nl ();
      printf "  end subroutine %s" name; nl ();
      printf "  @[<5>"; if !fortran95 then printf "pure ";
      printf "function number_%s_in () result (n)" name; nl ();
      printf "    integer :: n"; nl ();
      printf "    n = size (table_%s_in, dim=2)" name; nl ();
      printf "  end function number_%s_in" name; nl ();
      printf "  @[<5>"; if !fortran95 then printf "pure ";
      printf "subroutine %s_in (a)" name; nl ();
      printf "    integer, dimension(:,:), intent(inout) :: a"; nl ();
      printf "    a = table_%s_in" name; nl ();
      printf "  end subroutine %s_in" name; nl ();
      printf "  @[<5>"; if !fortran95 then printf "pure ";
      printf "function number_%s_out () result (n)" name; nl ();
      printf "    integer :: n"; nl ();
      printf "    n = size (table_%s_out, dim=2)" name; nl ();
      printf "  end function number_%s_out" name; nl ();
      printf "  @[<5>"; if !fortran95 then printf "pure ";
      printf "subroutine %s_out (a)" name; nl ();
      printf "    integer, dimension(:,:), intent(inout) :: a"; nl ();
      printf "    a = table_%s_out" name; nl ();
      printf "  end subroutine %s_out" name; nl ();
      nl ()

    let print_color_flows () =
      printf "  @[<5>"; if !fortran95 then printf "pure ";
      printf "function number_color_flows () result (n)"; nl ();
      printf "    integer :: n"; nl ();
      printf "    n = 0"; nl ();
      printf "  end function number_color_flows"; nl ();
      printf "  @[<5>"; if !fortran95 then printf "pure ";
      printf "subroutine color_flows (a)"; nl ();
      printf "    integer, dimension(:,:), intent(inout) :: a"; nl ();
      printf "    a = 0"; nl ();
      printf "  end subroutine color_flows"; nl ();
      printf "subroutine anticolor_flows (a)"; nl ();
      printf "    integer, dimension(:,:), intent(inout) :: a"; nl ();
      printf "    a = 0"; nl ();
      printf "  end subroutine anticolor_flows"; nl ();
      nl ()

    let print_multiplicities suffix =
      printf "  @[<5>"; if !fortran95 then printf "pure ";
      printf "subroutine multiplicities%s (a)" suffix; nl ();
      printf "    integer, dimension(:), intent(inout) :: a"; nl ();
      printf "    a = table_flavor_states%s_mult" suffix; nl ();
      printf "  end subroutine multiplicities%s" suffix; nl ();
      nl ()

    let print_symmetry () =
      printf "  @[<5>"; if !fortran95 then printf "pure ";
      printf "function symmetry (f) result (s)"; nl ();
      printf "    real(kind=omega_prec) :: s"; nl ();
      printf "    integer, dimension(:), intent(in) :: f"; nl ();
      printf "    integer :: n"; nl ();
      printf "    n = flavor_index (f)"; nl ();
      printf "    if (n == 0) then"; nl ();
      printf "      s = 0"; nl ();
      printf "    else"; nl ();
      printf "      @[<2>s = sqrt (real (table_symmetry(n),";
      printf "@ kind=omega_prec))"; nl ();
      printf "    end if"; nl ();
      printf "  end function symmetry"; nl ();
      nl ()

(* Special functions for the K matrix approach. This might be generalized
   to other functions that have to have access to the parameters and
   coupling constants. At the moment, this is hardcoded. *)

    let print_k_matrix_functions () = 
      let pure =
        if !km_pure then "pure "
        else 
          ""
      in
      if !km_write or !km_pure then
        begin 
          printf "  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"; nl (); 
          printf "  !!! Special K matrix functions"; nl (); 
          printf "  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"; nl (); 
          nl();
          printf "  %sfunction width_res (z,res,w_wkm,m,g) result (w)" pure; nl ();
          printf "      real(kind=omega_prec), intent(in) :: z, w_wkm, m, g"; nl ();
          printf "      integer, intent(in) :: res"; nl ();
          printf "      real(kind=omega_prec) :: w"; nl ();
          printf "      if (z.eq.0) then"; nl ();
          printf "        w = 0"; nl ();
          printf "      else"; nl ();
          printf "        if (w_wkm.eq.0) then"; nl ();
          printf "          select case (res)"; nl ();
          printf "            case (1) !!! Scalar isosinglet"; nl ();
          printf "              w = 3.*g**2/32./PI * m**3/vev**2"; nl ();
          printf "            case (2) !!! Scalar isoquintet"; nl ();
          printf "              w = g**2/64./PI * m**3/vev**2"; nl ();
          printf "            case (3) !!! Vector isotriplet"; nl ();
          printf "              w = g**2/48./PI * m"; nl ();
          printf "            case (4) !!! Tensor isosinglet"; nl ();
          printf "              w = g**2/320./PI * m**3/vev**2"; nl ();
          printf "            case (5) !!! Tensor isoquintet"; nl ();
          printf "              w = 3.*g**2/1920./PI * m**3/vev**2"; nl ();
          printf "            case default"; nl ();
          printf "              w = 0"; nl ();
          printf "          end select"; nl ();
          printf "        else"; nl ();
          printf "          w = w_wkm"; nl ();
          printf "        end if"; nl ();
          printf "      end if"; nl ();
          printf "  end function width_res"; nl ();          
          nl ();
          printf "  %sfunction s0stu (s, m) result (s0)" pure; nl ();
          printf "      real(kind=omega_prec), intent(in) :: s, m"; nl ();
          printf "      real(kind=omega_prec) :: s0"; nl ();
          printf "      if (m.ge.1.0e08) then"; nl ();
          printf "        s0 = 0"; nl ();  
          printf "      else"; nl ();
          printf "        s0 = m**2 - s/2 + m**4/s * log(m**2/(s+m**2))"; nl ();
          printf "      end if"; nl ();
          printf "  end function s0stu"; nl(); 
          nl ();
          printf "  %sfunction s1stu (s, m) result (s1)" pure; nl ();
          printf "      real(kind=omega_prec), intent(in) :: s, m"; nl ();
          printf "      real(kind=omega_prec) :: s1"; nl ();
          printf "      if (m.ge.1.0e08) then"; nl ();
          printf "        s1 = 0"; nl ();  
          printf "      else"; nl ();
          printf "        s1 = 2*m**4/s + s/6 + m**4/s**2*(2*m**2+s) &"; nl();
          printf "             * log(m**2/(s+m**2))"; nl ();
          printf "      end if"; nl ();
          printf "  end function s1stu"; nl(); 
          nl ();
          printf "  %sfunction s2stu (s, m) result (s2)" pure; nl ();
          printf "      real(kind=omega_prec), intent(in) :: s, m"; nl ();
          printf "      real(kind=omega_prec) :: s2"; nl ();
          printf "      if (m.ge.1.0e08) then"; nl ();
          printf "        s2 = 0"; nl ();  
          printf "      else"; nl ();
          printf "        s2 = m**4/s**2 * (6*m**2 + 3*s) + &"; nl();
          printf "             m**4/s**3 * (6*m**4 + 6*m**2*s + s**2) &"; nl();
          printf "             * log(m**2/(s+m**2))"; nl ();
          printf "      end if"; nl ();
          printf "  end function s2stu"; nl(); 
          nl ();
          printf "  %sfunction p0stu (s, m) result (p0)" pure; nl ();
          printf "      real(kind=omega_prec), intent(in) :: s, m"; nl ();
          printf "      real(kind=omega_prec) :: p0"; nl ();
          printf "      if (m.ge.1.0e08) then"; nl ();
          printf "        p0 = 0"; nl ();  
          printf "      else"; nl ();
          printf "        p0 = 1 + (2*s+m**2)*log(m**2/(s+m**2))/s"; nl ();
          printf "      end if"; nl (); 
          printf "  end function p0stu"; nl(); 
          nl ();
          printf "  %sfunction p1stu (s, m) result (p1)" pure; nl ();
          printf "      real(kind=omega_prec), intent(in) :: s, m"; nl ();
          printf "      real(kind=omega_prec) :: p1"; nl ();
          printf "      if (m.ge.1.0e08) then"; nl ();
          printf "        p1 = 0"; nl ();  
          printf "      else"; nl ();
          printf "        p1 = (m**2 + 2*s)/s**2 * (2*s+(2*m**2+s) &"; nl(); 
          printf "                * log(m**2/(s+m**2)))"; nl ();
          printf "      end if"; nl ();
          printf "  end function p1stu"; nl(); 
          nl ();
          printf "  %sfunction d0stu (s, m) result (d0)" pure; nl ();
          printf "      real(kind=omega_prec), intent(in) :: s, m"; nl ();
          printf "      real(kind=omega_prec) :: d0"; nl ();
          printf "      if (m.ge.1.0e08) then"; nl ();
          printf "        d0 = 0"; nl ();  
          printf "      else"; nl ();
          printf "        d0 = (2*m**2+11*s)/2 + (m**4+6*m**2*s+6*s**2) &"; nl();
          printf "              /s * log(m**2/(s+m**2))"; nl ();
          printf "      end if"; nl ();
          printf "  end function d0stu"; nl(); 
          nl ();
          printf "  %sfunction d1stu (s, m) result (d1)" pure; nl ();
          printf "      real(kind=omega_prec), intent(in) :: s, m"; nl ();
          printf "      real(kind=omega_prec) :: d1"; nl ();
          printf "      if (m.ge.1.0e08) then"; nl ();
          printf "        d1 = 0"; nl ();  
          printf "      else"; nl ();
          printf "        d1 = (s*(12*m**4 + 72*m**2*s + 73*s**2) &"; nl();
          printf "            + 6*(2*m**2 + s)*(m**4 + 6*m**2*s + 6*s**2) &"; nl();
          printf "            * log(m**2/(s+m**2)))/6/s**2"; nl ();
          printf "      end if"; nl ();
          printf "  end function d1stu"; nl(); 
          nl ();
          printf "  %sfunction da00 (cc, s, m) result (amp_00)" pure; nl ();
          printf "      real(kind=omega_prec), intent(in) :: s"; nl ();
          printf "      real(kind=omega_prec), dimension(1:5), intent(in) :: m, cc"; nl ();
          printf "      real(kind=omega_prec) :: a00_0, a00_1"; nl ();
          printf "      complex(kind=omega_prec), dimension(1:6) :: a00"; nl ();
          printf "      complex(kind=omega_prec) :: ii, jj, amp_00"; nl ();
          printf "      ii = cmplx(0.0,1.0/32.0/Pi,omega_prec)"; nl ();
          printf "      jj = s**2/vev**4*ii"; nl ();
          printf "      !!! Scalar isosinglet"; nl ();
          printf "      if (cc(1).ne.0) then"; nl ();
          printf "        if (fudge_km.ne.0) then"; nl ();
          printf "          a00(1) = vev**4/s**2 * fudge_km * &"; nl ();
          printf "              cmplx(0.0,32.0*Pi,omega_prec)*(1.0 + &"; nl ();
          printf "              (s-m(1)**2)/(ii*cc(1)**2/vev**2*(3.0*s**2 + &"; nl ();
          printf "              (s-m(1)**2)*2.0*s0stu(s,m(1))) - (s-m(1)**2)))"; nl ();
          printf "        else"; nl ();
          printf "          a00(1) = vev**2/s**2 * cc(1)**2 * &"; nl ();
          printf "              (3.0 * s**2/cmplx(s-m(1)**2,m(1)*width_res(w_res,1,&"; nl ();
          printf "              wkm(1),m(1),cc(1))) + 2.0 * s0stu(s,m(1)))"; nl ();
          printf "        end if"; nl ();          
          printf "      else"; nl ();
          printf "         a00(1) = 0"; nl ();
          printf "      end if"; nl ();
          printf "      !!! Scalar isoquintet"; nl ();
          printf "      a00(2) = 5.0*cc(2)**2/vev**2 * s0stu(s,m(2)) / 3.0"; nl ();
          printf "      a00(2) = vev**4/s**2*a00(2) /&"; nl();
          printf "                  (1.0_omega_prec - fudge_km*ii*a00(2))"; nl ();
          printf "      !!! Vector isotriplet"; nl ();
          printf "      a00(3) = cc(3)**2*(4.0*p0stu(s,m(3)) + 3.0*s/m(3)**2)"; nl ();
          printf "      a00(3) = vev**4/s**2*a00(3)/&"; nl ();
          printf "                  (1.0_omega_prec - fudge_km*ii*a00(3))"; nl ();
          printf "      !!! Tensor isosinglet"; nl ();
          printf "      a00(4) = cc(4)**2/vev**2 * (d0stu(s,m(4)) &"; nl ();
          printf "              /3.0 + 11.0*s**2/m(4)**2/36.0)"; nl ();
          printf "      a00(4) = vev**4/s**2*a00(4)/&"; nl ();
          printf "                 (1.0_omega_prec - fudge_km*ii*a00(4))"; nl ();
          printf "      !!! Tensor isoquintet"; nl ();
          printf "      a00(5) = 5.0*cc(5)**2/vev**2*(d0stu(s,m(5))&"; nl ();
          printf "              /3.0 + s**2/m(5)**2/18.0)/6.0"; nl ();
          printf "      a00(5) = vev**4/s**2*a00(5)/&"; nl ();
          printf "                 (1.0_omega_prec - fudge_km*ii*a00(5))"; nl ();
          printf "      !!! Low energy theory alphas"; nl ();
          printf "      a00_0 = 2*fudge_higgs*vev**2/s + 8*(7*a4 + 11*a5)/3"; nl ();
          printf "      a00_1 = 25*log(lam_reg**2/s)/9 + 11./54.0_omega_prec"; nl ();
          printf "      a00(6) = a00_0 !!! + a00_1/16/Pi**2"; nl ();
          printf "      a00(6) = fudge_km*jj*a00(6)**2 / (1.0_omega_prec - jj*a00(6))"; nl ();
          printf "      amp_00 = sum(a00)"; nl ();
          printf "  end function da00"; nl();
          nl ();
          printf "  %sfunction da02 (cc, s, m) result (amp_02)" pure; nl ();
          printf "      real(kind=omega_prec), intent(in) :: s"; nl ();
          printf "      real(kind=omega_prec), dimension(5), intent(in) :: m, cc"; nl ();
          printf "      real(kind=omega_prec) :: a02_0, a02_1"; nl ();
          printf "      complex(kind=omega_prec), dimension(1:6) :: a02"; nl ();
          printf "      complex(kind=omega_prec) :: ii, jj, amp_02"; nl ();
          printf "      ii = cmplx(0.0,1.0/32.0/Pi,omega_prec)"; nl ();
          printf "      jj = s**2/vev**4*ii"; nl ();
          printf "      !!! Scalar  isosinglet"; nl ();
          printf "      a02(1) = 2.0*cc(1)**2/vev**2 * s2stu(s,m(1))"; nl ();
          printf "      a02(1) = vev**4/s**2*a02(1)/&"; nl (); 
          printf "                 (1.0_omega_prec - fudge_km*ii*a02(1))"; nl ();
          printf "      !!! Scalar isoquintet"; nl ();
          printf "      a02(2) = 5.0*cc(2)**2/vev**2 * s2stu(s,m(2)) / 3.0"; nl ();
          printf "      a02(2) = vev**4/s**2*a02(2)/&"; nl (); 
          printf "                 (1.0_omega_prec - fudge_km*ii*a02(2))"; nl ();
          printf "      !!! Vector isotriplet"; nl (); 
          printf "      a02(3) = 4.0*cc(3)**2*(2*s+m(3)**2)*s2stu(s,m(3))/m(3)**4"; nl ();
          printf "      a02(3) = vev**4/s**2*a02(3)/&"; nl ();
          printf "                 (1.0_omega_prec - fudge_km*ii*a02(3))"; nl ();
          printf "      !!! Tensor isosinglet"; nl (); 
          printf "      if (cc(4).ne.0) then"; nl ();
          printf "        if (fudge_km.ne.0) then"; nl ();
          printf "         a02(4) = vev**4/s**2 * fudge_km * &"; nl ();
          printf "              cmplx(0.0,32.0*Pi,omega_prec)*(1.0 + &"; nl ();
          printf "              (s-m(4)**2)/(ii*cc(4)**2/vev**2*(s**2/10.0 + &"; nl ();
          printf "              (s-m(4)**2)*((1.0+6.0*s/m(4)**2+6.0* &"; nl ();
          printf "              s**2/m(4)**4)* s2stu(s,m(4))/3.0 &"; nl ();
          printf "              + s**2/m(4)**2/180.0)) - (s-m(4)**2)))"; nl ();
          printf "        else"; nl ();
          printf "          a02(4) = vev**2/s**2 * cc(4)**2 * ( s**2/ &"; nl (); 
          printf "               cmplx(s-m(4)**2,m(4)*width_res(w_res,4,wkm(4),&"; nl ();
          printf "               m(4),cc(4)))/10.0 + &"; nl ();
          printf "               (1.+6.*s/m(4)**2+6.*s**2/m(4)**4)*s2stu(s,m(4))/ &"; nl ();
          printf "               3. + s**2/m(4)**2/180.)"; nl ();
          printf "        end if"; nl ();          
          printf "      else"; nl ();
          printf "         a02(4) = 0"; nl ();
          printf "      end if"; nl ();
          printf "      !!! Tensor isoquintet"; nl ();
          printf "      a02(5) = cc(5)**2/vev**2*(5.0*(1.0+6.0* &"; nl ();
          printf "               s/m(5)**2+6.0*s**2/m(5)**4)*s2stu(s,m(5))/3.0 &"; nl ();  
          printf "               + s**2/m(5)**2/216.0)/6.0"; nl ();
          printf "      a02(5) = vev**4/s**2*a02(5)/&"; nl (); 
          printf "                 (1.0_omega_prec - fudge_km*ii*a02(5))"; nl ();
          printf "      !!! Low energy theory alphas"; nl ();
          printf "      a02_0 = 8*(2*a4 + a5)/15"; nl ();
          printf "      a02_1 = log(lam_reg**2/s)/9 - 7./135.0_omega_prec"; nl ();
          printf "      a02(6) = a02_0 !!! + a02_1/16/Pi**2"; nl ();
          printf "      a02(6) = fudge_km*jj*a02(6)**2 / (1.0_omega_prec - jj*a02(6))"; nl ();
          printf "      amp_02 = sum(a02)"; nl ();
          printf "  end function da02"; nl(); 
          nl ();
          printf "  %sfunction da11 (cc, s, m) result (amp_11)" pure; nl ();
          printf "      real(kind=omega_prec), intent(in) :: s"; nl ();
          printf "      real(kind=omega_prec), dimension(5), intent(in) :: m, cc"; nl ();
          printf "      real(kind=omega_prec) :: a11_0, a11_1"; nl ();
          printf "      complex(kind=omega_prec), dimension(1:6) :: a11"; nl ();
          printf "      complex(kind=omega_prec) :: ii, jj, amp_11"; nl ();
          printf "      ii = cmplx(0.0,1.0/32.0/Pi,omega_prec)"; nl ();
          printf "      jj = s**2/vev**4*ii"; nl ();
          printf "      !!! Scalar isosinglet"; nl ();
          printf "      a11(1) = 2.0*cc(1)**2/vev**2 * s1stu(s,m(1))"; nl ();
          printf "      a11(1) = vev**4/s**2*a11(1)/&"; nl ();
          printf "                 (1.0_omega_prec - fudge_km*ii*a11(1))"; nl ();
          printf "      !!! Scalar isoquintet"; nl ();
          printf "      a11(2) = - 5.0*cc(2)**2/vev**2 * s1stu(s,m(2)) / 6.0"; nl ();
          printf "      a11(2) = vev**4/s**2*a11(2)/&"; nl (); 
          printf "                 (1.0_omega_prec - fudge_km*ii*a11(2))"; nl ();
          printf "      !!! Vector isotriplet"; nl ();
          printf "      if (cc(3).ne.0) then"; nl ();
          printf "        if (fudge_km.ne.0) then"; nl ();
          printf "          a11(3) = vev**4/s**2 * fudge_km * &"; nl ();
          printf "              cmplx(0.0,32.0*Pi,omega_prec)*(1.0 + (s-m(3)**2) &"; nl ();
          printf "              /(ii*cc(3)**2*(2.0*s/3.0 + (s-m(3)**2)&"; nl ();
          printf "              *(s/m(3)**2+2.0*p1stu(s,m(3)))) - (s-m(3)**2)))"; nl ();
          printf "        else"; nl ();
          printf "          a11(3) = vev**4/s**2 * cc(3)**2 * ( 2.*s / &"; nl (); 
          printf "              cmplx(s-m(3)**2,m(3)*width_res(w_res,3,wkm(3),m(3),&"; nl ();
          printf "              cc(3)))/3. + s/m(3)**2 + 2.*p1stu(s,m(3)))"; nl ();
          printf "        end if"; nl ();          
          printf "      else"; nl ();
          printf "         a11(3) = 0"; nl ();
          printf "      end if"; nl ();
          printf "      !!! Tensor isosinglet"; nl ();
          printf "      a11(4) = cc(4)**2/vev**2*(d1stu(s,m(4)) &"; nl ();
          printf "               /3.0 - s**2/m(4)**2/36.0)"; nl ();
          printf "      a11(4) = vev**4/s**2*a11(4)/&"; nl (); 
          printf "                 (1.0_omega_prec - fudge_km*ii*a11(4))"; nl ();
          printf "      !!! Tensor isoquintet"; nl ();
          printf "      a11(5) = 5.0*cc(5)**2/vev**2*(-d1stu(s,m(5)) &"; nl ();
          printf "               + s**2/m(5)**2/12.0)/36.0"; nl ();
          printf "      a11(5) = vev**4/s**2*a11(5)/&"; nl (); 
          printf "                 (1.0_omega_prec - fudge_km*ii*a11(5))"; nl ();
          printf "      !!! Low energy theory alphas"; nl ();
          printf "      a11_0 = fudge_higgs*vev**2/3/s + 4*(a4 - 2*a5)/3"; nl ();
          printf "      a11_1 = - 1.0/54.0_omega_prec"; nl ();
          printf "      a11(6) = a11_0 !!! + a11_1/16/Pi**2"; nl ();
          printf "      a11(6) = fudge_km*jj*a11(6)**2 / (1.0_omega_prec - jj*a11(6))"; nl ();
          printf "      amp_11 = sum(a11)"; nl ();
          printf "  end function da11"; nl(); 
          nl ();
          printf "  %sfunction da20 (cc, s, m) result (amp_20)" pure; nl ();
          printf "      real(kind=omega_prec), intent(in) :: s"; nl ();
          printf "      real(kind=omega_prec), dimension(1:5), intent(in) :: m, cc"; nl ();
          printf "      real(kind=omega_prec) :: a20_0, a20_1"; nl ();
          printf "      complex(kind=omega_prec), dimension(1:6) :: a20"; nl ();
          printf "      complex(kind=omega_prec) :: ii, jj, amp_20"; nl ();
          printf "      ii = cmplx(0.0,1.0/32.0/Pi,omega_prec)"; nl ();
          printf "      jj = s**2/vev**4*ii"; nl ();
          printf "      !!! Scalar isosinglet"; nl ();
          printf "      a20(1) = 2.0*cc(1)**2/vev**2 * s0stu(s,m(1))"; nl ();
          printf "      a20(1) = vev**4/s**2*a20(1)/&"; nl ();
          printf "                 (1.0_omega_prec - fudge_km*ii*a20(1))"; nl ();
          printf "      !!! Scalar isoquintet"; nl ();
          printf "      if (cc(2).ne.0) then"; nl ();
          printf "        if (fudge_km.ne.0) then"; nl ();
          printf "          a20(2) = vev**4/s**2 * fudge_km * &"; nl (); 
          printf "              cmplx(0.0,32.0*Pi,omega_prec)*(1.0 + &"; nl ();
          printf "              (s-m(2)**2)/(ii*cc(2)**2/vev**2*(s**2/2.0 + &"; nl ();
          printf "              (s-m(2)**2)*s0stu(s,m(2))/6.0) - (s-m(2)**2)))"; nl ();
          printf "        else"; nl ();
          printf "          a20(2) = vev**2/s**2 * cc(2)**2 * ( s**2 / &"; nl (); 
          printf "              cmplx(s-m(2)**2,m(2)*width_res(w_res,2,wkm(2),&"; nl ();
          printf "              m(2),cc(2)))/2. + s0stu(s,m(2))/6.)"; nl ();
          printf "        end if"; nl ();          
          printf "      else"; nl ();
          printf "         a20(2) = 0"; nl ();
          printf "      end if"; nl ();
          printf "      !!! Vector isotriplet"; nl ();
          printf "      a20(3) = - cc(3)**2*(2.0*p0stu(s,m(3)) + 3.0*s/m(3)**2)"; nl ();
          printf "      a20(3) = vev**4/s**2*a20(3)/&"; nl (); 
          printf "                 (1.0_omega_prec - fudge_km*ii*a20(3))"; nl ();
          printf "      !!! Tensor isosinglet"; nl ();
          printf "      a20(4) = cc(4)**2/vev**2*(d1stu(s,m(4)) &"; nl ();
          printf "               /3.0 + s**2/m(4)**2/18.0)"; nl ();
          printf "      a20(4) = vev**4/s**2*a20(4)/&"; nl (); 
          printf "                 (1.0_omega_prec - fudge_km*ii*a20(4))"; nl ();
          printf "      !!! Tensor isoquintet"; nl ();
          printf "      a20(5) = cc(5)**2/vev**2*(d0stu(s,m(5)) &"; nl ();
          printf "                + 5.0*s**2/m(4)**2/3.0)/36.0"; nl ();
          printf "      a20(5) = vev**4/s**2*a20(5)/&"; nl (); 
          printf "                 (1.0_omega_prec - fudge_km*ii*a20(5))"; nl ();
          printf "      !!! Low energy theory alphas"; nl ();
          printf "      a20_0 = -fudge_higgs*vev**2/s + 16*(2*a4 + a5)/3"; nl ();
          printf "      a20_1 = 10*log(lam_reg**2/s)/9 + 25/108.0_omega_prec"; nl ();
          printf "      a20(6) = a20_0 !!! + a20_1/16/Pi**2"; nl ();
          printf "      a20(6) = fudge_km*jj*a20(6)**2 / (1.0_omega_prec - jj*a20(6))"; nl ();
          printf "      amp_20 = sum(a20)"; nl ();
          printf "  end function da20"; nl(); 
          nl ();
          printf "  %sfunction da22 (cc, s, m) result (amp_22)" pure; nl ();
          printf "      real(kind=omega_prec), intent(in) :: s"; nl ();
          printf "      real(kind=omega_prec), dimension(1:5), intent(in) :: m, cc"; nl ();
          printf "      real(kind=omega_prec) :: a22_0, a22_1"; nl ();
          printf "      complex(kind=omega_prec), dimension(1:6) :: a22"; nl ();
          printf "      complex(kind=omega_prec) :: ii, jj, amp_22"; nl ();
          printf "      ii = cmplx(0.0,1.0/32.0/Pi,omega_prec)"; nl ();
          printf "      jj = s**2/vev**4*ii"; nl ();
          printf "      !!! Scalar isosinglet"; nl ();
          printf "      a22(1) = 2.0*cc(1)**2/vev**2 * s2stu(s,m(1))"; nl ();
          printf "      a22(1) = vev**4/s**2*a22(1)/&"; nl (); 
          printf "                 (1.0_omega_prec - fudge_km*ii*a22(1))"; nl ();
          printf "      !!! Scalar isoquintet"; nl ();
          printf "      a22(2) = cc(2)**2/vev**2 * s2stu(s,m(2)) / 6.0"; nl ();
          printf "      a22(2) = vev**4/s**2*a22(2)/&"; nl (); 
          printf "                 (1.0_omega_prec - fudge_km*ii*a22(2))"; nl ();
          printf "      !!! Vector triplet"; nl ();
          printf "      a22(3) = - 2.0*cc(3)**2*(2*s+m(3)**2)*s2stu(s,m(3))/m(3)**4"; nl ();
          printf "      a22(3) = vev**4/s**2*a22(3)/&"; nl ();
          printf "                 (1.0_omega_prec - fudge_km*ii*a22(3))"; nl ();
          printf "      !!! Tensor isosinglet"; nl ();
          printf "      a22(4) = cc(4)**2/vev**2*((1.0 + 6.0*s/m(4)**2+6.0* &"; nl ();
          printf "            s**2/m(4)**4)*s2stu(s,m(4))/3.0 + s**2/m(4)**2/180.0)"; nl ();
          printf "      a22(4) = vev**4/s**2*a22(4)/&"; nl ();
          printf "                 (1.0_omega_prec - fudge_km*ii*a22(4))"; nl ();
          printf "      !!! Tensor isoquintet"; nl ();
          printf "      if (cc(5).ne.0) then"; nl ();
          printf "        if (fudge_km.ne.0) then"; nl ();
          printf "          a22(5) = vev**4 / s**2 * & "; nl ();
          printf "              cmplx(0.0,32.0*Pi,omega_prec)*(1.0 + &"; nl ();
          printf "              (s-m(5)**2)/(ii*cc(5)**2/vev**2*(s**2/60.0 + &"; nl ();
          printf "              (s-m(5)**2)*((1.0+6.0*s/m(5)**2+6.0* &"; nl ();
          printf "              s**2/m(5)**4)*s2stu(s,m(5))/36.0 &"; nl (); 
          printf "              + s**2/m(5)**2/2160.0)) - (s-m(5)**2)))"; nl ();
          printf "        else"; nl ();
          printf "          a22(5) = vev**2/s**2 * cc(5)**2 * ( s**2 / &"; nl (); 
          printf "              cmplx(s-m(5)**2,m(5)*width_res(w_res,5,wkm(5),&"; nl ();
          printf "              m(5),cc(5)))/80. + (1.0+6.0* &"; nl ();
          printf "              s/m(5)**2+6.0*s**2/m(5)**4)*s2stu(s,m(5))/36.0 + &"; nl ();
          printf "              s**2/m(5)**2/2160.0)"; nl ();
          printf "        end if"; nl ();                    
          printf "      else"; nl ();
          printf "         a22(5) = 0"; nl ();
          printf "      end if"; nl ();
          printf "      !!! Low energy theory alphas"; nl ();
          printf "      a22_0 = 4*(a4 + 2*a5)/15"; nl ();
          printf "      a22_1 = 2*log(lam_reg**2/s)/45 - 247/5400.0_omega_prec"; nl ();
          printf "      a22(6) = a22_0 !!! + a22_1/16/Pi**2"; nl ();
          printf "      a22(6) = fudge_km*jj*a22(6)**2 / (1.0_omega_prec - jj*a22(6))"; nl ();
          printf "      amp_22 = sum(a22)"; nl ();
          printf "  end function da22"; nl();
          nl ();
          printf "  %sfunction dalzz0_s (cc,m,k) result (alzz0_s)" pure; nl ();
          printf "      type(momentum), intent(in) :: k"; nl ();
          printf "      real(kind=omega_prec), dimension(1:5), intent(in) :: cc, m"; nl ();
          printf "      complex(kind=omega_prec) :: alzz0_s"; nl ();
          printf "      real(kind=omega_prec) :: s"; nl ();
          printf "      s = k*k"; nl ();
          printf "      alzz0_s = 2*g**4/costhw**2*((da00(cc,s,m) &"; nl ();
          printf "                - da20(cc,s,m))/24 &"; nl ();
          printf "                - 5*(da02(cc,s,m) - da22(cc,s,m))/12)"; nl ();
          printf "  end function dalzz0_s"; nl ();
          nl ();
          printf "  %sfunction dalzz0_t (cc,m,k) result (alzz0_t)" pure; nl ();
          printf "      type(momentum), intent(in) :: k"; nl ();
          printf "      real(kind=omega_prec), dimension(1:5), intent(in) :: cc, m"; nl ();
          printf "      complex(kind=omega_prec) :: alzz0_t"; nl ();
          printf "      real(kind=omega_prec) :: s"; nl ();
          printf "      s = k*k"; nl ();
          printf "      alzz0_t = 5*g**4/costhw**2*(da02(cc,s,m) - &"; nl ();
          printf "                da22(cc,s,m))/4"; nl (); 
          printf "  end function dalzz0_t"; nl ();
          nl ();
          printf "  %sfunction dalzz1_s (cc,m,k) result (alzz1_s)" pure; nl ();
          printf "      type(momentum), intent(in) :: k"; nl ();
          printf "      real(kind=omega_prec), dimension(1:5), intent(in) :: cc, m"; nl ();
          printf "      complex(kind=omega_prec) :: alzz1_s"; nl ();
          printf "      real(kind=omega_prec) :: s"; nl ();
          printf "      s = k*k"; nl ();
          printf "      alzz1_s = g**4/costhw**2*(da20(cc,s,m)/8 &"; nl ();
          printf "                - 5*da22(cc,s,m)/4)"; nl (); 
          printf "  end function dalzz1_s"; nl ();
          nl ();  
          printf "  %sfunction dalzz1_t (cc,m,k) result (alzz1_t)" pure; nl ();
          printf "      type(momentum), intent(in) :: k"; nl ();
          printf "      real(kind=omega_prec), dimension(1:5), intent(in) :: cc, m"; nl ();
          printf "      complex(kind=omega_prec) :: alzz1_t"; nl ();
          printf "      real(kind=omega_prec) :: s"; nl ();
          printf "      s = k*k"; nl ();
          printf "      alzz1_t = g**4/costhw**2*(- 3*da11(cc,s,m)/8 &"; nl ();
          printf "                + 15*da22(cc,s,m)/8)"; nl (); 
          printf "  end function dalzz1_t"; nl ();
          nl ();  
          printf "  %sfunction dalzz1_u (cc,m,k) result (alzz1_u)" pure; nl ();
          printf "      type(momentum), intent(in) :: k"; nl ();
          printf "      real(kind=omega_prec), dimension(1:5), intent(in) :: cc, m"; nl ();
          printf "      complex(kind=omega_prec) :: alzz1_u"; nl ();
          printf "      real(kind=omega_prec) :: s"; nl ();
          printf "      s = k*k"; nl ();
          printf "      alzz1_u = g**4/costhw**2*(3*da11(cc,s,m)/8 &"; nl ();
          printf "                + 15*da22(cc,s,m)/8)"; nl (); 
          printf "  end function dalzz1_u"; nl ();
          nl ();  
          printf "  %sfunction dalww0_s (cc,m,k) result (alww0_s)" pure; nl ();
          printf "      type(momentum), intent(in) :: k"; nl ();
          printf "      real(kind=omega_prec), dimension(1:5), intent(in) :: cc, m"; nl ();
          printf "      complex(kind=omega_prec) :: alww0_s"; nl ();
          printf "      real(kind=omega_prec) :: s"; nl ();
          printf "      s = k*k"; nl ();
          printf "      alww0_s = g**4*((2*da00(cc,s,m) + da20(cc,s,m))/24 &"; nl ();
          printf "                  - 5*(2*da02(cc,s,m) + da22(cc,s,m))/12)"; nl (); 
          printf "  end function dalww0_s"; nl ();
          nl ();  
          printf "  %sfunction dalww0_t (cc,m,k) result (alww0_t)" pure; nl ();
          printf "      type(momentum), intent(in) :: k"; nl ();
          printf "      real(kind=omega_prec), dimension(1:5), intent(in) :: cc, m"; nl ();
          printf "      complex(kind=omega_prec) :: alww0_t"; nl ();
          printf "      real(kind=omega_prec) :: s"; nl ();
          printf "      s = k*k"; nl ();
          printf "      alww0_t = g**4*(10*da02(cc,s,m) - 3*da11(cc,s,m) &"; nl ();
          printf "                + 5*da22(cc,s,m))/8"; nl (); 
          printf "  end function dalww0_t"; nl ();
          nl ();  
          printf "  %sfunction dalww0_u (cc,m,k) result (alww0_u)" pure; nl ();
          printf "      type(momentum), intent(in) :: k"; nl ();
          printf "      real(kind=omega_prec), dimension(1:5), intent(in) :: cc, m"; nl ();
          printf "      complex(kind=omega_prec) :: alww0_u"; nl ();
          printf "      real(kind=omega_prec) :: s"; nl ();
          printf "      s = k*k"; nl ();
          printf "      alww0_u = g**4*(10*da02(cc,s,m) + 3*da11(cc,s,m) &"; nl (); 
          printf "                + 5*da22(cc,s,m))/8"; nl (); 
          printf "  end function dalww0_u"; nl ();
          nl ();
          printf "  %sfunction dalww2_s (cc,m,k) result (alww2_s)" pure; nl ();
          printf "      type(momentum), intent(in) :: k"; nl ();
          printf "      real(kind=omega_prec), dimension(1:5), intent(in) :: cc, m"; nl ();
          printf "      complex(kind=omega_prec) :: alww2_s"; nl ();
          printf "      real(kind=omega_prec) :: s"; nl ();
          printf "      s = k*k"; nl ();
          printf "      alww2_s = g**4*(da20(cc,s,m) - 10*da22(cc,s,m))/4 "; nl (); 
          printf "  end function dalww2_s"; nl ();
          nl ();
          printf "  %sfunction dalww2_t (cc,m,k) result (alww2_t)" pure; nl ();
          printf "      type(momentum), intent(in) :: k"; nl ();
          printf "      real(kind=omega_prec), dimension(1:5), intent(in) :: cc, m"; nl ();
          printf "      complex(kind=omega_prec) :: alww2_t"; nl ();
          printf "      real(kind=omega_prec) :: s"; nl ();
          printf "      s = k*k"; nl ();
          printf "      alww2_t = 15*g**4*da22(cc,s,m)/4"; nl (); 
          printf "  end function dalww2_t"; nl ();
          nl ();
          printf "  %sfunction dalz4_s (cc,m,k) result (alz4_s)" pure; nl ();
          printf "      type(momentum), intent(in) :: k"; nl ();
          printf "      real(kind=omega_prec), dimension(1:5), intent(in) :: cc, m"; nl ();
          printf "      complex(kind=omega_prec) :: alz4_s"; nl ();
          printf "      real(kind=omega_prec) :: s"; nl ();
          printf "      s = k*k"; nl ();
          printf "      alz4_s = g**4/costhw**4*((da00(cc,s,m) &"; nl ();
          printf "               + 2*da20(cc,s,m))/12 &"; nl (); 
          printf "               - 5*(da02(cc,s,m)+2*da22(cc,s,m))/6)"; nl (); 
          printf "  end function dalz4_s"; nl ();
          nl ();
          printf "  %sfunction dalz4_t (cc,m,k) result (alz4_t)" pure; nl ();
          printf "      type(momentum), intent(in) :: k"; nl ();
          printf "      real(kind=omega_prec), dimension(1:5), intent(in) :: cc, m"; nl ();
          printf "      complex(kind=omega_prec) :: alz4_t"; nl ();
          printf "      real(kind=omega_prec) :: s"; nl ();
          printf "      s = k*k"; nl ();
          printf "      alz4_t = g**4/costhw**4*5*(da02(cc,s,m) &"; nl ();
          printf "               + 2*da22(cc,s,m))/4"; nl (); 
          printf "  end function dalz4_t"; nl ();
          nl ()
        end


(*i   Backup !!!
    let print_k_matrix_functions () = 
      if !km_write then
        begin 
          printf "  ! Special K matrix functions"; nl (); 
          nl();
          printf "  function da00 (s, m) result (a00)"; nl ();
          printf "      real(kind=omega_prec), intent(in) :: s, m"; nl ();
          printf "      real(kind=omega_prec) :: a00, a00_0, a00_1"; nl ();
          printf "      a00_0 = 8*(7*a4 + 11*a5)/3"; nl ();
          printf "      a00_1 = 25*log(m**2/s)/9 + 11./54.0_omega_prec"; nl ();
          printf "      a00 = a00_0 !!! + a00_1/16/Pi**2"; nl ();
          printf "  end function da00"; nl(); 
          nl ();
          printf "  function da02 (s, m) result (a02)"; nl ();
          printf "      real(kind=omega_prec), intent(in) :: s, m"; nl ();
          printf "      real(kind=omega_prec) :: a02, a02_0, a02_1"; nl ();
          printf "      a02_0 = 8*(2*a4 + a5)/15"; nl ();
          printf "      a02_1 = log(m**2/s)/9 - 7./135.0_omega_prec"; nl ();
          printf "      a02 = a02_0 !!! + a02_1/16/Pi**2"; nl ();
          printf "  end function da02"; nl(); 
          nl ();
          printf "  function da11 (s, m) result (a11)"; nl ();
          printf "      real(kind=omega_prec), intent(in) :: s, m"; nl ();
          printf "      real(kind=omega_prec) :: a11, a11_0, a11_1"; nl ();
          printf "      a11_0 = 4*(a4 - 2*a5)/3"; nl ();
          printf "      a11_1 = - 1./54.0_omega_prec"; nl ();
          printf "      a11 = a11_0 !!! + a11_1/16/Pi**2"; nl ();
          printf "  end function da11"; nl(); 
          nl ();
          printf "  function da20 (s, m) result (a20)"; nl ();
          printf "      real(kind=omega_prec), intent(in) :: s, m"; nl ();
          printf "      real(kind=omega_prec) :: a20, a20_0, a20_1"; nl ();
          printf "      a20_0 = 16*(2*a4 + a5)/3"; nl ();
          printf "      a20_1 = 10*log(m**2/s)/9 + 25/108.0_omega_prec"; nl ();
          printf "      a20 = a20_0 !!! + a20_1/16/Pi**2"; nl ();
          printf "  end function da20"; nl();
          nl ();
          printf "  function da22 (s, m) result (a22)"; nl ();
          printf "      real(kind=omega_prec), intent(in) :: s, m"; nl ();
          printf "      real(kind=omega_prec) :: a22, a22_0, a22_1"; nl ();
          printf "      a22_0 = 4*(a4 + 2*a5)/15"; nl ();
          printf "      a22_1 = 2*log(m**2/s)/45 - 247/5400.0_omega_prec"; nl ();
          printf "      a22 = a22_0 !!! + a22_1/16/Pi**2"; nl ();
          printf "  end function da22"; nl();
          nl ();
          printf "  function dalzz0_s (k) result (alzz0_s)"; nl ();
          printf "      type(momentum), intent(in) :: k"; nl ();
          printf "      real(kind=omega_prec) :: m"; nl ();
          printf "      complex(kind=omega_prec) :: ii, alzz0_s"; nl ();
          printf "      real(kind=omega_prec) :: s"; nl ();
          printf "      s = k*k"; nl ();
          printf "      m = lam_reg"; nl (); 
          printf "      ii = cmplx(0.0,s**2/32.0/Pi/vev**4,omega_prec)"; nl();
          printf "      alzz0_s = 2*g**4/costhw**2*((da00(s,m) - da20(s,m))/24 &"; nl ();
          printf "                    - 5*(da02(s,m) - da22(s,m))/12)"; nl ();
          printf "      alzz0_s = ii*alzz0_s**2 / (1.0_omega_prec - ii*alzz0_s)"; nl ();
          printf "  end function dalzz0_s"; nl ();
          nl ();
          printf "  function dalzz0_t (k) result (alzz0_t)"; nl ();
          printf "      type(momentum), intent(in) :: k"; nl ();
          printf "      real(kind=omega_prec) :: m"; nl ();
          printf "      complex(kind=omega_prec) :: ii, alzz0_t"; nl ();
          printf "      real(kind=omega_prec) :: s"; nl ();
          printf "      s = k*k"; nl ();
          printf "      m = lam_reg"; nl (); 
          printf "      ii = cmplx(0.0,s**2/32.0/Pi/vev**4,omega_prec)"; nl();
          printf "      alzz0_t = 5*g**4/costhw**2*(da02(s,m) - da22(s,m))/4"; nl (); 
          printf "      alzz0_t = ii*alzz0_t**2 / (1.0_omega_prec - ii*alzz0_t)"; nl ();
          printf "  end function dalzz0_t"; nl ();
          nl ();
          printf "  function dalzz1_s (k) result (alzz1_s)"; nl ();
          printf "      type(momentum), intent(in) :: k"; nl ();
          printf "      real(kind=omega_prec) :: m"; nl ();
          printf "      complex(kind=omega_prec) :: ii, alzz1_s"; nl ();
          printf "      real(kind=omega_prec) :: s"; nl ();
          printf "      s = k*k"; nl ();
          printf "      m = lam_reg"; nl (); 
          printf "      ii = cmplx(0.0,s**2/32.0/Pi/vev**4,omega_prec)"; nl();
          printf "      alzz1_s = g**4/costhw**2*(da20(s,m)/8 - 5*da22(s,m)/4)"; nl (); 
          printf "      alzz1_s = ii*alzz1_s**2 / (1.0_omega_prec - ii*alzz1_s)"; nl ();
          printf "  end function dalzz1_s"; nl ();
          nl ();  
          printf "  function dalzz1_t (k) result (alzz1_t)"; nl ();
          printf "      type(momentum), intent(in) :: k"; nl ();
          printf "      real(kind=omega_prec) :: m"; nl ();
          printf "      complex(kind=omega_prec) :: ii, alzz1_t"; nl ();
          printf "      real(kind=omega_prec) :: s"; nl ();
          printf "      s = k*k"; nl ();
          printf "      m = lam_reg"; nl (); 
          printf "      ii = cmplx(0.0,s**2/32.0/Pi/vev**4,omega_prec)"; nl();
          printf "      alzz1_t = g**4/costhw**2*(- 3*da11(s,m)/8 + 15*da22(s,m)/8)"; nl (); 
          printf "      alzz1_t = ii*alzz1_t**2 / (1.0_omega_prec - ii*alzz1_t)"; nl ();
          printf "  end function dalzz1_t"; nl ();
          nl ();  
          printf "  function dalzz1_u (k) result (alzz1_u)"; nl ();
          printf "      type(momentum), intent(in) :: k"; nl ();
          printf "      real(kind=omega_prec) :: m"; nl ();
          printf "      complex(kind=omega_prec) :: ii, alzz1_u"; nl ();
          printf "      real(kind=omega_prec) :: s"; nl ();
          printf "      s = k*k"; nl ();
          printf "      m = lam_reg"; nl (); 
          printf "      ii = cmplx(0.0,s**2/32.0/Pi/vev**4,omega_prec)"; nl();
          printf "      alzz1_u = g**4/costhw**2*(3*da11(s,m)/8 + 15*da22(s,m)/8)"; nl (); 
          printf "      alzz1_u = ii*alzz1_u**2 / (1.0_omega_prec - ii*alzz1_u)"; nl ();
          printf "  end function dalzz1_u"; nl ();
          nl ();  
          printf "  function dalww0_s (k) result (alww0_s)"; nl ();
          printf "      type(momentum), intent(in) :: k"; nl ();
          printf "      real(kind=omega_prec) :: m"; nl ();
          printf "      complex(kind=omega_prec) :: ii, alww0_s"; nl ();
          printf "      real(kind=omega_prec) :: s"; nl ();
          printf "      s = k*k"; nl ();
          printf "      m = lam_reg"; nl (); 
          printf "      ii = cmplx(0.0,s**2/32.0/Pi/vev**4,omega_prec)"; nl();
          printf "      alww0_s = g**4*((2*da00(s,m) + da20(s,m))/24 &"; nl ();
          printf "                  - 5*(2*da02(s,m) + da22(s,m))/12)"; nl (); 
          printf "      alww0_s = ii*alww0_s**2 / (1.0_omega_prec - ii*alww0_s)"; nl ();
          printf "  end function dalww0_s"; nl ();
          nl ();  
          printf "  function dalww0_t (k) result (alww0_t)"; nl ();
          printf "      type(momentum), intent(in) :: k"; nl ();
          printf "      real(kind=omega_prec) :: m"; nl ();
          printf "      complex(kind=omega_prec) :: ii, alww0_t"; nl ();
          printf "      real(kind=omega_prec) :: s"; nl ();
          printf "      s = k*k"; nl ();
          printf "      m = lam_reg"; nl (); 
          printf "      ii = cmplx(0.0,s**2/32.0/Pi/vev**4,omega_prec)"; nl();
          printf "      alww0_t = g**4*(10*da02(s,m) - 3*da11(s,m) + 5*da22(s,m))/8"; nl (); 
          printf "      alww0_t = ii*alww0_t**2 / (1.0_omega_prec - ii*alww0_t)"; nl ();
          printf "  end function dalww0_t"; nl ();
          nl ();  
          printf "  function dalww0_u (k) result (alww0_u)"; nl ();
          printf "      type(momentum), intent(in) :: k"; nl ();
          printf "      real(kind=omega_prec) :: m"; nl ();
          printf "      complex(kind=omega_prec) :: ii, alww0_u"; nl ();
          printf "      real(kind=omega_prec) :: s"; nl ();
          printf "      s = k*k"; nl ();
          printf "      m = lam_reg"; nl (); 
          printf "      ii = cmplx(0.0,s**2/32.0/Pi/vev**4,omega_prec)"; nl();
          printf "      alww0_u = g**4*(10*da02(s,m) + 3*da11(s,m) + 5*da22(s,m))/8"; nl (); 
          printf "      alww0_u = ii*alww0_u**2 / (1.0_omega_prec - ii*alww0_u)"; nl ();
          printf "  end function dalww0_u"; nl ();
          nl ();
          printf "  function dalww2_s (k) result (alww2_s)"; nl ();
          printf "      type(momentum), intent(in) :: k"; nl ();
          printf "      real(kind=omega_prec) :: m"; nl ();
          printf "      complex(kind=omega_prec) :: ii, alww2_s"; nl ();
          printf "      real(kind=omega_prec) :: s"; nl ();
          printf "      s = k*k"; nl ();
          printf "      m = lam_reg"; nl (); 
          printf "      ii = cmplx(0.0,s**2/32.0/Pi/vev**4,omega_prec)"; nl();
          printf "      alww2_s = g**4*(da20(s,m) - 10*da22(s,m))/4 "; nl (); 
          printf "      alww2_s = ii*alww2_s**2 / (1.0_omega_prec - ii*alww2_s)"; nl ();
          printf "  end function dalww2_s"; nl ();
          nl ();
          printf "  function dalww2_t (k) result (alww2_t)"; nl ();
          printf "      type(momentum), intent(in) :: k"; nl ();
          printf "      real(kind=omega_prec) :: m"; nl ();
          printf "      complex(kind=omega_prec) :: ii, alww2_t"; nl ();
          printf "      real(kind=omega_prec) :: s"; nl ();
          printf "      s = k*k"; nl ();
          printf "      m = lam_reg"; nl (); 
          printf "      ii = cmplx(0.0,s**2/32.0/Pi/vev**4,omega_prec)"; nl();
          printf "      alww2_t = 15*g**4*da22(s,m)/4"; nl (); 
          printf "      alww2_t = ii*alww2_t**2 / (1.0_omega_prec - ii*alww2_t)"; nl ();
          printf "  end function dalww2_t"; nl ();
          nl ();
          printf "  function dalz4_s (k) result (alz4_s)"; nl ();
          printf "      type(momentum), intent(in) :: k"; nl ();
          printf "      real(kind=omega_prec) :: m"; nl ();
          printf "      complex(kind=omega_prec) :: ii, alz4_s"; nl ();
          printf "      real(kind=omega_prec) :: s"; nl ();
          printf "      s = k*k"; nl ();
          printf "      m = lam_reg"; nl (); 
          printf "      ii = cmplx(0.0,s**2/32.0/Pi/vev**4,omega_prec)"; nl();
          printf "      alz4_s = g**4/costhw**4*((da00(s,m) + 2*da20(s,m))/12 &"; nl (); 
          printf "                  - 5*(da02(s,m)+2*da22(s,m))/6)"; nl (); 
          printf "      alz4_s = ii*alz4_s**2 / (1.0_omega_prec - ii*alz4_s)"; nl ();
          printf "  end function dalz4_s"; nl ();
          nl ();
          printf "  function dalz4_t (k) result (alz4_t)"; nl ();
          printf "      type(momentum), intent(in) :: k"; nl ();
          printf "      real(kind=omega_prec) :: m"; nl ();
          printf "      complex(kind=omega_prec) :: ii, alz4_t"; nl ();
          printf "      real(kind=omega_prec) :: s"; nl ();
          printf "      s = k*k"; nl ();
          printf "      m = lam_reg"; nl (); 
          printf "      ii = cmplx(0.0,s**2/32.0/Pi/vev**4,omega_prec)"; nl();
          printf "      alz4_t = g**4/costhw**4*5*(da02(s,m) + 2*da22(s,m))/4"; nl (); 
          printf "      alz4_t = ii*alz4_t**2 / (1.0_omega_prec - ii*alz4_t)"; nl ();
          printf "  end function dalz4_t"; nl ();
          nl ()
        end
i*)

(* \thocwmodulesubsection{Main Function} *)

    let print_description amplitudes =
      printf "! O'Mega scattering amplitudes for the processes"; nl ();
      printf "!"; nl ();
      printf "!   allowed:"; nl ();
      printf "!"; nl ();
      List.iter (fun a ->
        printf "!     %s" (format_process a);
        let s = MF.multiplicities_in_out amplitudes (flavors a)
        and s_in = MF.multiplicities_in amplitudes (F.incoming a)
        and s_out = MF.multiplicities_out amplitudes (F.outgoing a) in
        if s <> s_in * s_out then
          printf " (diagnostics S = %d, S_out = %d, S_in = %d)" s s_out s_in
          (* failwith "print_description: s <> s_in * s_out" *)
        else if s > 1 || s_in > 1 || s_out > 1 then begin
          if s_in = 1 then
            printf " (S = S_out = %d)" s
          else if s_out = 1 then
            printf " (S = S_in = %d)" s
          else 
            printf " (S = %d, S_in = %d, S_out = %d)" s s_in s_out
        end; nl();
        begin match F.constraints a with
        | None -> ()
        | Some s ->
            printf "!         NB: diagram selection %s" s; nl ();
            printf "!             might break gauge invariance!"; nl ()
        end) (MF.allowed amplitudes);
      if !whiz_qcd then 
          begin 
            printf "!"; nl (); 
          end
      else
        begin
          printf "!"; nl ();
          printf "!   forbidden:"; nl ();
          printf "!"; nl ();
          List.iter (fun a ->
            printf "!     %s" (format_process a); nl();
            begin match F.constraints a with
            | None -> ()
            | Some s ->
                printf "!         NB: diagram selection %s" s; nl()
            end) (MF.forbidden amplitudes);
          printf "!"; nl ();
        end;
      begin match RCS.description M.rcs with
      | line1 :: lines ->
          printf "! in %s" line1; nl ();
          List.iter (fun s -> printf "!    %s" s; nl ()) lines
      | [] -> printf "! in %s" (RCS.name M.rcs); nl ()
      end;
      printf "!"; nl ()

    let print_version () =
      printf "! O'Mega revision control information:"; nl ();
      List.iter (fun s -> printf "!    %s" s; nl ())
        (ThoList.flatmap RCS.summary (M.rcs :: rcs_list @ F.rcs_list))

    let print_public = function
      | name1 :: names ->
          printf "  @[<2>public :: %s" name1;
          List.iter (fun n -> printf ",@ %s" n) names; nl ()
      | [] -> ()

    let print_public_interface generic procedures =
      printf "  public :: %s" generic; nl ();
      begin match procedures with
      | name1 :: names ->
          printf "  interface %s" generic; nl ();
          printf "     @[<2>module procedure %s" name1;
          List.iter (fun n -> printf ",@ %s" n) names; nl ();
          printf "  end interface"; nl ();
          print_public procedures
      | [] -> ()
      end
      
    let print_module_header amplitudes =
      (*i let allowed = MF.allowed amplitudes in i*)
      printf "module %s" !module_name; nl ();
      List.iter (fun s -> printf "  use %s" s; nl ())
        ("omega_kinds" :: Fermions.use_module :: !parameter_module ::
         !use_modules);
      printf "  implicit none"; nl ();
      printf "  private"; nl ();
      print_public ["amplitude"; "amplitude_f";
                    "amplitude_1"; "amplitude_2"];
      print_public ["amplitude_nonzero"; "amplitude_f_nonzero";
                    "amplitude_1_nonzero"; "amplitude_2_nonzero"];
      print_public ["spin_sum_sqme"; "spin_sum_sqme_1"; "sum_sqme"];
      print_public ["spin_sum_sqme_nonzero"; "spin_sum_sqme_1_nonzero";
                    "sum_sqme_nonzero"];
      print_public ["scatter"; "scatter_diagonal";
                    "scatter_nonzero"; "scatter_diagonal_nonzero"];
      if new_whizard then
        print_public ["scatter_colored_nonzero"; "scatter_diagonal_colored_nz"];
      print_public_interface "allocate_zero"
        ["allocate_zero_1"; "allocate_zero_2"];
      print_public ["number_particles";
                    "number_particles_in"; "number_particles_out"];
      if new_whizard then
        print_public ["number_color_flows"; "color_flows"; "anticolor_flows"];
      print_public ["multiplicities";
                    "multiplicities_in"; "multiplicities_out"];
      print_public ["symmetry"];
      List.iter print_inquiry_function_declarations
        ["spin_states"; "flavor_states"; "flavor_zeros"];
      print_public ["create"; "reset"; "destroy"];
      printf "  ! DON'T EVEN THINK of removing the following!"; nl ();
      printf "  ! If the compiler complains about undeclared"; nl ();
      printf "  ! or undefined variables, you are compiling"; nl ();
      printf "  ! against an incompatible omega95 module!"; nl ();
      printf "  @[<2>integer, dimension(%d), parameter, private :: "
        (List.length require_library);
      printf "require =@ (/ @[";
      print_list require_library;
      printf " /)"; nl(); nl ();
      print_spin_tables amplitudes;
      print_flavor_tables amplitudes;
      print_symmetry_table amplitudes;
      printf "contains"; nl (); nl ();
      print_maintenance_functions ();
      print_numeric_inquiry_functions ();
      List.iter print_inquiry_functions
        ["spin_states"; "flavor_states"; "flavor_zeros"];
      if new_whizard then
        print_color_flows ();
      List.iter print_multiplicities [""; "_in"; "_out"];
      print_symmetry ();
      print_spin_index amplitudes;
      print_flavor_index amplitudes

    let print_module_footer () =
      printf "end module %s" !module_name; nl ()

    let amplitudes_to_channel oc diagnostics amplitudes =
      set_formatter_out_channel oc;
      set_margin !line_length;
      wrap_newline ();
      parse_diagnostics diagnostics;
      print_description amplitudes;
      print_module_header amplitudes;
      print_dispatch_functions amplitudes;
      print_dispatch_functions_nonzero ();
      print_dispatch_functions_sqme ();
      print_dispatch_functions_sqme_nonzero ();
      print_k_matrix_functions (); 
      List.iter print_one_amplitude (MF.allowed amplitudes);
      print_module_footer ();
      print_version ();
      print_flush ()

    let parameters_to_channel oc =
      parameters_to_fortran oc (M.parameters ())

(* \thocwmodulesubsection{Compatibility} *)

    let print_old_function_header amplitude =
      let externals = F.externals amplitude in
      printf "  ! process: %s" (format_process amplitude); nl();
      printf "  "; if !fortran95 then printf "pure ";
      printf "@[<2>function %s (k, s) result (amp)" !function_name; nl ();
      printf "    @[<2>real(kind=%s), dimension(0:,:), intent(in) :: k"
        !kind; nl ();
      printf "    @[<2>integer, dimension(:), intent(in) :: s"; nl ();
      printf "    complex(kind=%s) :: amp" !kind; nl ();
      printf "    @[<2>type(momentum) :: ";
      print_list (List.map momentum externals); nl ()

    let print_old_function_footer () =
      printf "  end function %s" !function_name; nl ()

    let print_old_description amplitude =
      printf "! O'Mega scattering amplitude for the process"; nl ();
      printf "!"; nl ();
      printf "!   %s" (format_process amplitude); nl();
      printf "!"; nl ();
      begin match RCS.description M.rcs with
      | line1 :: lines ->
          printf "! in %s" line1; nl ();
          List.iter (fun s -> printf "!    %s" s; nl ()) lines
      | [] -> printf "! in %s" (RCS.name M.rcs); nl ()
      end;
      printf "!"; nl ()

    let print_old_module_header amplitude =
      printf "module %s" !module_name; nl ();
      List.iter (fun s -> printf "  use %s" s; nl ())
        ("omega_kinds" :: Fermions.use_module :: !parameter_module ::
         !use_modules);
      printf "  implicit none"; nl ();
      printf "  private"; nl ();
      if !whizard then
        print_whizard_declarations amplitude
      else begin
        printf "  public :: %s" !function_name; nl ()
      end;
      printf "  ! DON'T EVEN THINK of removing the following!"; nl ();
      printf "  ! If the compiler complains about undeclared"; nl ();
      printf "  ! or undefined variables, you are compiling"; nl ();
      printf "  ! against an incompatible omega95 module!"; nl ();
      printf "  @[<2>integer, dimension(%d), parameter, private :: "
        (List.length require_library);
      printf "require =@ (/ @[";
      print_list require_library;
      printf " /)"; nl(); 
      printf "contains"; nl ();
      if !whizard then
        print_whizard_functions amplitude

    let amplitude_to_channel oc diagnostics amplitude =
      set_formatter_out_channel oc;
      set_margin !line_length;
      wrap_newline ();
      parse_diagnostics diagnostics;
      print_old_description amplitude;
      print_old_module_header amplitude;
      print_old_function_header amplitude;
      print_declarations amplitude;
      print_argument_diagnostics amplitude;
      print_externals amplitude;
      print_fusions amplitude;
      print_brakets "amp" amplitude;
      print_fudge_factor "amp" amplitude;
      print_old_function_footer ();
      print_module_footer ();
      print_version ();
      print_flush ()

  end

module Fortran = Make_Fortran(Fortran_Fermions)

(* \thocwmodulesection{\texttt{Fortran\,90/95} Supporting Majorana Fermions} *)

(* \begin{JR}
   For this function we need a different approach due to our aim of 
   implementing the fermion vertices with the right line as ingoing (in a
   calculational sense) and the left line in a fusion as outgoing. In
   defining all external lines and the fermionic wavefunctions built out of
   them as ingoing we have to invert the left lines to make them outgoing.
   This happens by multiplying them with the inverse charge conjugation 
   matrix in an appropriate representation and then transposing it. We must 
   distinguish whether the direction of calculation and the physical direction
   of the fermion number flow are parallel or antiparallel. In the first case 
   we can use the "normal" Feynman rules for Dirac particles, while in the 
   second, according to the paper of Denner et al., we have to reverse the 
   sign of the vector and antisymmetric bilinears of the Dirac spinors, cf. 
   the [Coupling] module. 

   Note the subtlety for the left- and righthanded couplings: Only the vector 
   part of these couplings changes in the appropriate cases its sign, 
   changing the chirality to the negative of the opposite.
   \end{JR} *)

module Fortran_Majorana_Fermions : Fermions =
  struct
    let rcs = RCS.rename rcs_file "Targets.Fortran_Majorana_Fermions()"
        [ "generates Fortran95 code for Dirac and Majorana fermions";
          " using revision 2003_03_A of module omega95_bispinors" ]

    open Coupling
    open Format

    let psi_type = "bispinor"
    let psibar_type = "bispinor"
    let chi_type = "bispinor"
    let grav_type = "vectorspinor"

(* \begin{JR}
   Because of our rules for fermions we are going to give all incoming fermions
   a [u] spinor and all outgoing fermions a [v] spinor, no matter whether they
   are Dirac fermions, antifermions or Majorana fermions.
   \end{JR} *)
        
    let psi_incoming = "u"
    let brs_psi_incoming = "brs_u"
    let psibar_incoming = "u"
    let brs_psibar_incoming = "brs_u"
    let chi_incoming = "u"
    let brs_chi_incoming = "brs_u"
    let grav_incoming = "ueps"

    let psi_outgoing = "v"
    let brs_psi_outgoing = "brs_v"    
    let psibar_outgoing = "v"
    let brs_psibar_outgoing = "brs_v"
    let chi_outgoing = "v"
    let brs_chi_outgoing = "brs_v"
    let grav_outgoing = "veps"

    let psi_propagator = "pr_psi"
    let psibar_propagator = "pr_psi"
    let chi_propagator = "pr_psi"
    let grav_propagator = "pr_grav"

    let psi_projector = "pj_psi"
    let psibar_projector = "pj_psi"
    let chi_projector = "pj_psi"
    let grav_projector = "pj_grav"

    let psi_gauss = "pg_psi"
    let psibar_gauss = "pg_psi"
    let chi_gauss = "pg_psi"
    let grav_gauss = "pg_grav"

    let format_coupling coeff c =
      match coeff with
      | 1 -> c
      | -1 -> "(-" ^ c ^")"
      | coeff -> string_of_int coeff ^ "*" ^ c

    let format_coupling_2 coeff c = 
      match coeff with 
      | 1 -> c
      | -1 -> "-" ^ c
      | coeff -> string_of_int coeff ^ "*" ^ c

    let fastener s i = 
      try 
        let offset = (String.index s '(') in
        if ((String.get s (String.length s - 1)) != ')') then
          failwith "fastener: wrong usage of parentheses"
        else
          let func_name = (String.sub s 0 offset) and
	      tail =
	    (String.sub s (succ offset) (String.length s - offset - 2)) in 
          if (String.contains func_name ')') or 
	    (String.contains tail '(') or 
            (String.contains tail ')') then
            failwith "fastener: wrong usage of parentheses"
          else      
            func_name ^ "(" ^ string_of_int i ^ "," ^ tail ^ ")"
      with
      | Not_found ->  
          if (String.contains s ')') then
	    failwith "fastener: wrong usage of parentheses"
          else
	    s ^ "(" ^ string_of_int i ^ ")"

    let print_fermion_current coeff f c wf1 wf2 fusion =
      let c = format_coupling coeff c in
      match fusion with
      | F13 | F31 -> printf "%s_ff(%s,%s,%s)" f c wf1 wf2
      | F23 | F21 -> printf "f_%sf(%s,%s,%s)" f c wf1 wf2
      | F32 | F12 -> printf "f_%sf(%s,%s,%s)" f c wf2 wf1

    let print_fermion_current2 coeff f c wf1 wf2 fusion =
      let c = format_coupling_2 coeff c in
      let c1 = fastener c 1 and
          c2 = fastener c 2 in 
      match fusion with
      | F13 | F31 -> printf "%s_ff(%s,%s,%s,%s)" f c1 c2 wf1 wf2
      | F23 | F21 -> printf "f_%sf(%s,%s,%s,%s)" f c1 c2 wf1 wf2
      | F32 | F12 -> printf "f_%sf(%s,%s,%s,%s)" f c1 c2 wf2 wf1

    let print_fermion_current_vector coeff f c wf1 wf2 fusion =
      let c = format_coupling coeff c in
      match fusion with
      | F13 -> printf "%s_ff(%s,%s,%s)" f c wf1 wf2
      | F31 -> printf "%s_ff(-%s,%s,%s)" f c wf1 wf2
      | F23 -> printf "f_%sf(%s,%s,%s)" f c wf1 wf2
      | F32 -> printf "f_%sf(%s,%s,%s)" f c wf2 wf1
      | F12 -> printf "f_%sf(-%s,%s,%s)" f c wf2 wf1
      | F21 -> printf "f_%sf(-%s,%s,%s)" f c wf1 wf2

    let print_fermion_current2_vector coeff f c wf1 wf2 fusion =
      let c  = format_coupling_2 coeff c in
      let c1 = fastener c 1 and
          c2 = fastener c 2 in 
      match fusion with
      | F13 -> printf "%s_ff(%s,%s,%s,%s)" f c1 c2 wf1 wf2
      | F31 -> printf "%s_ff(-(%s),%s,%s,%s)" f c1 c2 wf1 wf2
      | F23 -> printf "f_%sf(%s,%s,%s,%s)" f c1 c2 wf1 wf2 
      | F32 -> printf "f_%sf(%s,%s,%s,%s)" f c1 c2 wf2 wf1 
      | F12 -> printf "f_%sf(-(%s),%s,%s,%s)" f c1 c2 wf2 wf1 
      | F21 -> printf "f_%sf(-(%s),%s,%s,%s)" f c1 c2 wf1 wf2 

    let print_fermion_current_chiral coeff f1 f2 c wf1 wf2 fusion =
      let c = format_coupling coeff c in
      match fusion with
      | F13 -> printf "%s_ff(%s,%s,%s)" f1 c wf1 wf2
      | F31 -> printf "%s_ff(-%s,%s,%s)" f2 c wf1 wf2
      | F23 -> printf "f_%sf(%s,%s,%s)" f1 c wf1 wf2
      | F32 -> printf "f_%sf(%s,%s,%s)" f1 c wf2 wf1
      | F12 -> printf "f_%sf(-%s,%s,%s)" f2 c wf2 wf1
      | F21 -> printf "f_%sf(-%s,%s,%s)" f2 c wf1 wf2

    let print_fermion_current2_chiral coeff f c wf1 wf2 fusion =
      let c = format_coupling_2 coeff c in
      let c1 = fastener c 1 and
          c2 = fastener c 2 in  
      match fusion with
      | F13 -> printf "%s_ff(%s,%s,%s,%s)" f c1 c2 wf1 wf2
      | F31 -> printf "%s_ff(-(%s),-(%s),%s,%s)" f c2 c1 wf1 wf2
      | F23 -> printf "f_%sf(%s,%s,%s,%s)" f c1 c2 wf1 wf2 
      | F32 -> printf "f_%sf(%s,%s,%s,%s)" f c1 c2 wf2 wf1 
      | F12 -> printf "f_%sf(-(%s),-(%s),%s,%s)" f c2 c1 wf2 wf1 
      | F21 -> printf "f_%sf(-(%s),-(%s),%s,%s)" f c2 c1 wf1 wf2 

    let print_current = function
      | coeff, _, VA, _ -> print_fermion_current2_vector coeff "va"
      | coeff, _, V, _ -> print_fermion_current_vector coeff "v"
      | coeff, _, A, _ -> print_fermion_current coeff "a"
      | coeff, _, VL, _ -> print_fermion_current_chiral coeff "vl" "vr"
      | coeff, _, VR, _ -> print_fermion_current_chiral coeff "vr" "vl"
      | coeff, _, VLR, _ -> print_fermion_current2_chiral coeff "vlr"
      | coeff, _, SP, _ -> print_fermion_current2 coeff "sp"
      | coeff, _, S, _ -> print_fermion_current coeff "s"
      | coeff, _, P, _ -> print_fermion_current coeff "p"
      | coeff, _, SL, _ -> print_fermion_current coeff "sl"
      | coeff, _, SR, _ -> print_fermion_current coeff "sr"
      | coeff, _, SLR, _ -> print_fermion_current2 coeff "slr"
      | coeff, _, POT, _ -> print_fermion_current_vector coeff "pot"
      | coeff, _, _, _ -> invalid_arg 
            "Targets.Fortran_Majorana_Fermions: Not needed in the models"

    let print_current_p = function
      | coeff, Psi, SL, Psi -> print_fermion_current coeff "sl"   
      | coeff, Psi, SR, Psi -> print_fermion_current coeff "sr"   
      | coeff, Psi, SLR, Psi -> print_fermion_current2 coeff "slr"       
      | coeff, _, _, _ -> invalid_arg 
            "Targets.Fortran_Majorana_Fermions: Not needed in the used models"

    let print_current_b = function
      | coeff, Psibar, SL, Psibar -> print_fermion_current coeff "sl"
      | coeff, Psibar, SR, Psibar -> print_fermion_current coeff "sr"
      | coeff, Psibar, SLR, Psibar -> print_fermion_current2 coeff "slr"
      | coeff, _, _, _  -> invalid_arg 
            "Targets.Fortran_Majorana_Fermions: Not needed in the used models"

(* This function is for the vertices with three particles including two
   fermions but also a momentum, therefore with a dimensionful coupling
   constant, e.g. the gravitino vertices. One has to dinstinguish between
   the two kinds of canonical orders in the string of gamma matrices. Of 
   course, the direction of the string of gamma matrices is reversed if one
   goes from the [Gravbar, _, Psi] to the [Psibar, _, Grav] vertices, and 
   the same is true for the couplings of the gravitino to the Majorana
   fermions. For more details see the tables in the [coupling] 
   implementation. *)

(* We now have to fix the directions of the momenta. For making the compiler
   happy and because we don't want to make constructions of infinite 
   complexity we list the momentum including vertices without gravitinos 
   here; the pattern matching says that's better. Perhaps we have to find a
   better name now.  

   For the cases of $MOM$, $MOM5$, $MOML$ and $MOMR$ which arise only in 
   BRST transformations we take the mass as a coupling constant. For 
   $VMOM$ we don't need a mass either. These vertices are like kinetic terms
   and so need not have a coupling constant. By this we avoid a strange and
   awful construction with a new variable. But be careful with a 
   generalization if you want to use these vertices for other purposes. 
*)

    let format_coupling_mom coeff c =
      match coeff with
      | 1 -> c
      | -1 -> "(-" ^ c ^")"
      | coeff -> string_of_int coeff ^ "*" ^ c

    let commute_proj f = 
      match f with 
      | "moml" -> "lmom" 
      | "momr" -> "rmom"
      | "lmom" -> "moml"
      | "rmom" -> "momr"
      | "svl"  -> "svr"
      | "svr"  -> "svl"
      | "sl" -> "sr"
      | "sr" -> "sl"
      | "s" -> "s"
      | "p" -> "p"
      | _ -> invalid_arg "Targets:Fortran_Majorana_Fermions: wrong case"

    let print_fermion_current_mom coeff f c wf1 wf2 p1 p2 p12 fusion =
      let c = format_coupling_mom coeff c in
      let c1 = fastener c 1 and
          c2 = fastener c 2 in 
      match fusion with
      | F13 -> printf "%s_ff(%s,%s,%s,%s,%s)" f c1 c2 wf1 wf2 p12 
      | F31 -> printf "%s_ff(%s,%s,%s,%s,%s)" f c1 c2 wf1 wf2 p12
      | F23 -> printf "f_%sf(%s,%s,%s,%s,%s)" f c1 c2 wf1 wf2 p1
      | F32 -> printf "f_%sf(%s,%s,%s,%s,%s)" f c1 c2 wf2 wf1 p2 
      | F12 -> printf "f_%sf(%s,%s,%s,%s,%s)" f c1 c2 wf2 wf1 p2
      | F21 -> printf "f_%sf(%s,%s,%s,%s,%s)" f c1 c2 wf1 wf2 p1

    let print_fermion_current_mom_sign coeff f c wf1 wf2 p1 p2 p12 fusion =
      let c = format_coupling_mom coeff c in
      let c1 = fastener c 1 and
          c2 = fastener c 2 in 
      match fusion with
      | F13 -> printf "%s_ff(%s,%s,%s,%s,%s)" f c1 c2 wf1 wf2 p12 
      | F31 -> printf "%s_ff(%s,%s,%s,%s,-(%s))" f c1 c2 wf1 wf2 p12
      | F23 -> printf "f_%sf(%s,%s,%s,%s,%s)" f c1 c2 wf1 wf2 p1
      | F32 -> printf "f_%sf(%s,%s,%s,%s,%s)" f c1 c2 wf2 wf1 p2 
      | F12 -> printf "f_%sf(%s,%s,%s,%s,-(%s))" f c1 c2 wf2 wf1 p2
      | F21 -> printf "f_%sf(%s,%s,%s,%s,-(%s))" f c1 c2 wf1 wf2 p1

    let print_fermion_current_mom_sign_1 coeff f c wf1 wf2 p1 p2 p12 fusion =
      let c = format_coupling coeff c in
      match fusion with
      | F13 -> printf "%s_ff(%s,%s,%s,%s)" f c wf1 wf2 p12 
      | F31 -> printf "%s_ff(%s,%s,%s,-(%s))" f c wf1 wf2 p12
      | F23 -> printf "f_%sf(%s,%s,%s,%s)" f c wf1 wf2 p1
      | F32 -> printf "f_%sf(%s,%s,%s,%s)" f c wf2 wf1 p2 
      | F12 -> printf "f_%sf(%s,%s,%s,-(%s))" f c wf2 wf1 p2
      | F21 -> printf "f_%sf(%s,%s,%s,-(%s))" f c wf1 wf2 p1

    let print_fermion_current_mom_chiral coeff f c wf1 wf2 p1 p2 p12 fusion =
      let c  = format_coupling_mom coeff c and
          cf = commute_proj f in
      let c1 = fastener c 1 and
          c2 = fastener c 2 in 
      match fusion with 
      | F13 -> printf "%s_ff(%s,%s,%s,%s,%s)" f c1 c2 wf1 wf2 p12
      | F31 -> printf "%s_ff(%s,%s,%s, %s,-(%s))" cf c1 c2 wf1 wf2 p12
      | F23 -> printf "f_%sf(%s,%s,%s,%s,%s)" f c1 c2 wf1 wf2 p1
      | F32 -> printf "f_%sf(%s,%s,%s,%s,%s)" f c1 c2 wf2 wf1 p2 
      | F12 -> printf "f_%sf(%s,%s,%s,%s,-(%s))" cf c1 c2 wf2 wf1 p2
      | F21 -> printf "f_%sf(%s,%s,%s,%s,-(%s))" cf c1 c2 wf1 wf2 p1

    let print_fermion_g_current coeff f c wf1 wf2 p1 p2 p12 fusion =
      let c = format_coupling coeff c in
      match fusion with
      | F13 -> printf "%s_grf(%s,%s,%s,%s)" f c wf1 wf2 p12 
      | F31 -> printf "%s_fgr(%s,%s,%s,%s)" f c wf1 wf2 p12
      | F23 -> printf "gr_%sf(%s,%s,%s,%s)" f c wf1 wf2 p1
      | F32 -> printf "gr_%sf(%s,%s,%s,%s)" f c wf2 wf1 p2 
      | F12 -> printf "f_%sgr(%s,%s,%s,%s)" f c wf2 wf1 p2
      | F21 -> printf "f_%sgr(%s,%s,%s,%s)" f c wf1 wf2 p1

    let print_fermion_g_2_current coeff f c wf1 wf2 p1 p2 p12 fusion =
      let c = format_coupling coeff c in
      match fusion with
      | F13 -> printf "%s_grf(%s(1),%s(2),%s,%s,%s)" f c c wf1 wf2 p12 
      | F31 -> printf "%s_fgr(%s(1),%s(2),%s,%s,%s)" f c c wf1 wf2 p12
      | F23 -> printf "gr_%sf(%s(1),%s(2),%s,%s,%s)" f c c wf1 wf2 p1
      | F32 -> printf "gr_%sf(%s(1),%s(2),%s,%s,%s)" f c c wf2 wf1 p2 
      | F12 -> printf "f_%sgr(%s(1),%s(2),%s,%s,%s)" f c c wf2 wf1 p2
      | F21 -> printf "f_%sgr(%s(1),%s(2),%s,%s,%s)" f c c wf1 wf2 p1

    let print_fermion_g_current_rev coeff f c wf1 wf2 p1 p2 p12 fusion =
      let c = format_coupling coeff c in
      match fusion with
      | F13 -> printf "%s_fgr(%s,%s,%s,%s)" f c wf1 wf2 p12 
      | F31 -> printf "%s_grf(%s,%s,%s,%s)" f c wf1 wf2 p12
      | F23 -> printf "f_%sgr(%s,%s,%s,%s)" f c wf1 wf2 p1
      | F32 -> printf "f_%sgr(%s,%s,%s,%s)" f c wf2 wf1 p2 
      | F12 -> printf "gr_%sf(%s,%s,%s,%s)" f c wf2 wf1 p2
      | F21 -> printf "gr_%sf(%s,%s,%s,%s)" f c wf1 wf2 p1

    let print_fermion_g_2_current_rev coeff f c wf1 wf2 p1 p2 p12 fusion =
      let c = format_coupling coeff c in
      match fusion with
      | F13 -> printf "%s_fgr(%s(1),%s(2),%s,%s,%s)" f c c wf1 wf2 p12 
      | F31 -> printf "%s_grf(%s(1),%s(2),%s,%s,%s)" f c c wf1 wf2 p12
      | F23 -> printf "f_%sgr(%s(1),%s(2),%s,%s,%s)" f c c wf1 wf2 p1
      | F32 -> printf "f_%sgr(%s(1),%s(2),%s,%s,%s)" f c c wf2 wf1 p2 
      | F12 -> printf "gr_%sf(%s(1),%s(2),%s,%s,%s)" f c c wf2 wf1 p2
      | F21 -> printf "gr_%sf(%s(1),%s(2),%s,%s,%s)" f c c wf1 wf2 p1

    let print_fermion_g_current_vector coeff f c wf1 wf2 p1 p2 p12 fusion =
      let c = format_coupling coeff c in
      match fusion with
      | F13 -> printf "%s_grf(%s,%s,%s)" f c wf1 wf2
      | F31 -> printf "%s_fgr(-%s,%s,%s)" f c wf1 wf2
      | F23 -> printf "gr_%sf(%s,%s,%s)" f c wf1 wf2
      | F32 -> printf "gr_%sf(%s,%s,%s)" f c wf2 wf1
      | F12 -> printf "f_%sgr(-%s,%s,%s)" f c wf2 wf1
      | F21 -> printf "f_%sgr(-%s,%s,%s)" f c wf1 wf2

    let print_fermion_g_current_vector_rev coeff f c wf1 wf2 p1 p2 p12 fusion =
      let c = format_coupling coeff c in
      match fusion with
      | F13 -> printf "%s_fgr(%s,%s,%s)" f c wf1 wf2
      | F31 -> printf "%s_grf(-%s,%s,%s)" f c wf1 wf2
      | F23 -> printf "f_%sgr(%s,%s,%s)" f c wf1 wf2
      | F32 -> printf "f_%sgr(%s,%s,%s)" f c wf2 wf1
      | F12 -> printf "gr_%sf(-%s,%s,%s)" f c wf2 wf1
      | F21 -> printf "gr_%sf(-%s,%s,%s)" f c wf1 wf2

    let print_current_g = function
      | coeff, _, MOM, _ -> print_fermion_current_mom_sign coeff "mom"
      | coeff, _, MOM5, _ -> print_fermion_current_mom coeff "mom5"
      | coeff, _, MOML, _ -> print_fermion_current_mom_chiral coeff "moml"
      | coeff, _, MOMR, _ -> print_fermion_current_mom_chiral coeff "momr"
      | coeff, _, LMOM, _ -> print_fermion_current_mom_chiral coeff "lmom"
      | coeff, _, RMOM, _ -> print_fermion_current_mom_chiral coeff "rmom"
      | coeff, _, VMOM, _ -> print_fermion_current_mom_sign_1 coeff "vmom"
      | coeff, Gravbar, S, _ -> print_fermion_g_current coeff "s"
      | coeff, Gravbar, SL, _ -> print_fermion_g_current coeff "sl"
      | coeff, Gravbar, SR, _ -> print_fermion_g_current coeff "sr"
      | coeff, Gravbar, SLR, _ -> print_fermion_g_2_current coeff "slr"
      | coeff, Gravbar, P, _ -> print_fermion_g_current coeff "p"
      | coeff, Gravbar, V, _ -> print_fermion_g_current coeff "v"   
      | coeff, Gravbar, VLR, _ -> print_fermion_g_2_current coeff "vlr"   
      | coeff, Gravbar, POT, _ -> print_fermion_g_current_vector coeff "pot"
      | coeff, _, S, Grav -> print_fermion_g_current_rev coeff "s"
      | coeff, _, SL, Grav -> print_fermion_g_current_rev coeff "sl"
      | coeff, _, SR, Grav -> print_fermion_g_current_rev coeff "sr"
      | coeff, _, SLR, Grav -> print_fermion_g_2_current_rev coeff "slr"
      | coeff, _, P, Grav -> print_fermion_g_current_rev (-coeff) "p"
      | coeff, _, V, Grav -> print_fermion_g_current_rev coeff "v"
      | coeff, _, VLR, Grav -> print_fermion_g_2_current_rev coeff "vlr"
      | coeff, _, POT, Grav -> print_fermion_g_current_vector_rev coeff "pot"
      | coeff, _, _, _ -> invalid_arg
          "Targets.Fortran_Majorana_Fermions: not used in the models"

(* We need support for dimension-5 vertices with two fermions and two 
   bosons, appearing in theories of supergravity and also together with in
   insertions of the supersymmetric current. There is a canonical order 
   [fermionbar], [boson_1], [boson_2], [fermion], so what one has to do is a 
   mapping from the fusions [F123] etc. to the order of the three wave 
   functions [wf1], [wf2] and [wf3]. *)

(* The function [d_p] (for distinct the particle) distinguishes which particle
   (scalar or vector) must be fused to in the special functions. *)

    let d_p = function 
      | 1, ("sv"|"pv"|"svl"|"svr"|"slrv") -> "1"
      | 1, _ -> ""
      | 2, ("sv"|"pv"|"svl"|"svr"|"slrv") -> "2"
      | 2, _ -> ""
      | _, _ -> invalid_arg "Targets.Fortran_Majorana_Fermions: not used"

    let wf_of_f wf1 wf2 wf3 f =
      match f with 
      | (F123|F423) -> [wf2; wf3; wf1]
      | (F213|F243|F143|F142|F413|F412) -> [wf1; wf3; wf2]
      | (F132|F432) -> [wf3; wf2; wf1]
      | (F231|F234|F134|F124|F431|F421) -> [wf1; wf2; wf3]
      | (F312|F342) -> [wf3; wf1; wf2]
      | (F321|F324|F314|F214|F341|F241) -> [wf2; wf1; wf3]

    let print_fermion_g4_brs_vector_current coeff f c wf1 wf2 wf3 fusion = 
      let cf = commute_proj f and
          cp = format_coupling coeff c and
          cm = if f = "pv" then
            format_coupling coeff c 
          else
            format_coupling (-coeff) c 
      and
          d1 = d_p (1,f) and
          d2 = d_p (2,f) and
          f1 = (List.nth (wf_of_f wf1 wf2 wf3 fusion) 0) and
          f2 = (List.nth (wf_of_f wf1 wf2 wf3 fusion) 1) and
          f3 = (List.nth (wf_of_f wf1 wf2 wf3 fusion) 2) in
      match fusion with
      | (F123|F213|F132|F231|F312|F321) -> 
          printf "f_%sf(%s,%s,%s,%s)" cf cm f1 f2 f3 
      | (F423|F243|F432|F234|F342|F324) -> 
          printf "f_%sf(%s,%s,%s,%s)" f cp f1 f2 f3       
      | (F134|F143|F314) -> printf "%s%s_ff(%s,%s,%s,%s)" f d1 cp f1 f2 f3 
      | (F124|F142|F214) -> printf "%s%s_ff(%s,%s,%s,%s)" f d2 cp f1 f2 f3 
      | (F413|F431|F341) -> printf "%s%s_ff(%s,%s,%s,%s)" cf d1 cm f1 f2 f3 
      | (F241|F412|F421) -> printf "%s%s_ff(%s,%s,%s,%s)" cf d2 cm f1 f2 f3 

    let print_fermion_g4_svlr_current coeff f c wf1 wf2 wf3 fusion = 
      let c = format_coupling_2 coeff c and
          f1 = (List.nth (wf_of_f wf1 wf2 wf3 fusion) 0) and
          f2 = (List.nth (wf_of_f wf1 wf2 wf3 fusion) 1) and
          f3 = (List.nth (wf_of_f wf1 wf2 wf3 fusion) 2) in
      let c1 = fastener c 1 and
          c2 = fastener c 2 in
      match fusion with
      | (F123|F213|F132|F231|F312|F321) -> 
          printf "f_svlrf(-(%s),-(%s),%s,%s,%s)" c2 c1 f1 f2 f3 
      | (F423|F243|F432|F234|F342|F324) -> 
          printf "f_svlrf(%s,%s,%s,%s,%s)" c1 c2 f1 f2 f3       
      | (F134|F143|F314) -> 
          printf "svlr2_ff(%s,%s,%s,%s,%s)" c1 c2 f1 f2 f3 
      | (F124|F142|F214) -> 
          printf "svlr1_ff(%s,%s,%s,%s,%s)" c1 c2 f1 f2 f3 
      | (F413|F431|F341) -> 
          printf "svlr2_ff(-(%s),-(%s),%s,%s,%s)" c2 c1 f1 f2 f3 
      | (F241|F412|F421) -> 
          printf "svlr1_ff(-(%s),-(%s),%s,%s,%s)" c2 c1 f1 f2 f3 

    let print_fermion_s2_current coeff f c wf1 wf2 wf3 fusion = 
      let cp = format_coupling coeff c and
          cm = if f = "p" then 
            format_coupling (-coeff) c 
          else
            format_coupling coeff c
      and
          cf = commute_proj f and          
          f1 = (List.nth (wf_of_f wf1 wf2 wf3 fusion) 0) and
          f2 = (List.nth (wf_of_f wf1 wf2 wf3 fusion) 1) and
          f3 = (List.nth (wf_of_f wf1 wf2 wf3 fusion) 2) in
      match fusion with
      | (F123|F213|F132|F231|F312|F321) -> 
          printf "%s * f_%sf(%s,%s,%s)" f1 cf cm f2 f3 
      | (F423|F243|F432|F234|F342|F324) -> 
          printf "%s * f_%sf(%s,%s,%s)" f1 f cp f2 f3       
      | (F134|F143|F314) -> 
          printf "%s * %s_ff(%s,%s,%s)" f2 f cp f1 f3 
      | (F124|F142|F214) ->                
          printf "%s * %s_ff(%s,%s,%s)" f2 f cp f1 f3 
      | (F413|F431|F341) ->                
          printf "%s * %s_ff(%s,%s,%s)" f2 cf cm f1 f3 
      | (F241|F412|F421) ->                
          printf "%s * %s_ff(%s,%s,%s)" f2 cf cm f1 f3 

    let print_fermion_s2p_current coeff f c wf1 wf2 wf3 fusion = 
      let c = format_coupling_2 coeff c and
          f1 = (List.nth (wf_of_f wf1 wf2 wf3 fusion) 0) and
          f2 = (List.nth (wf_of_f wf1 wf2 wf3 fusion) 1) and
          f3 = (List.nth (wf_of_f wf1 wf2 wf3 fusion) 2) in
      let c1 = fastener c 1 and
          c2 = fastener c 2 in 
      match fusion with
      | (F123|F213|F132|F231|F312|F321) -> 
          printf "%s * f_%sf(%s,-(%s),%s,%s)" f1 f c1 c2 f2 f3 
      | (F423|F243|F432|F234|F342|F324) -> 
          printf "%s * f_%sf(%s,%s,%s,%s)" f1 f c1 c2 f2 f3       
      | (F134|F143|F314) -> 
          printf "%s * %s_ff(%s,%s,%s,%s)" f2 f c1 c2 f1 f3 
      | (F124|F142|F214) ->                
          printf "%s * %s_ff(%s,%s,%s,%s)" f2 f c1 c2 f1 f3 
      | (F413|F431|F341) ->                
          printf "%s * %s_ff(%s,-(%s),%s,%s)" f2 f c1 c2 f1 f3 
      | (F241|F412|F421) ->                
          printf "%s * %s_ff(%s,-(%s),%s,%s)" f2 f c1 c2 f1 f3 

    let print_fermion_s2lr_current coeff f c wf1 wf2 wf3 fusion = 
      let c = format_coupling_2 coeff c and
          f1 = (List.nth (wf_of_f wf1 wf2 wf3 fusion) 0) and
          f2 = (List.nth (wf_of_f wf1 wf2 wf3 fusion) 1) and
          f3 = (List.nth (wf_of_f wf1 wf2 wf3 fusion) 2) in
      let c1 = fastener c 1 and
          c2 = fastener c 2 in
      match fusion with
      | (F123|F213|F132|F231|F312|F321) -> 
          printf "%s * f_%sf(%s,%s,%s,%s)" f1 f c2 c1 f2 f3 
      | (F423|F243|F432|F234|F342|F324) -> 
          printf "%s * f_%sf(%s,%s,%s,%s)" f1 f c1 c2 f2 f3       
      | (F134|F143|F314) -> 
          printf "%s * %s_ff(%s,%s,%s,%s)" f2 f c1 c2 f1 f3 
      | (F124|F142|F214) ->                
          printf "%s * %s_ff(%s,%s,%s,%s)" f2 f c1 c2 f1 f3 
      | (F413|F431|F341) ->                
          printf "%s * %s_ff(%s,%s,%s,%s)" f2 f c2 c1 f1 f3 
      | (F241|F412|F421) ->                
          printf "%s * %s_ff(%s,%s,%s,%s)" f2 f c2 c1 f1 f3 

    let print_fermion_g4_current coeff f c wf1 wf2 wf3 fusion =
      let c = format_coupling coeff c and
          f1 = (List.nth (wf_of_f wf1 wf2 wf3 fusion) 0) and
          f2 = (List.nth (wf_of_f wf1 wf2 wf3 fusion) 1) and
          f3 = (List.nth (wf_of_f wf1 wf2 wf3 fusion) 2) in
      match fusion with
      | (F123|F213|F132|F231|F312|F321) -> 
          printf "f_%sgr(-%s,%s,%s,%s)" f c f1 f2 f3
      | (F423|F243|F432|F234|F342|F324) ->
          printf "gr_%sf(%s,%s,%s,%s)" f c f1 f2 f3
      | (F134|F143|F314|F124|F142|F214) ->
          printf "%s_grf(%s,%s,%s,%s)" f c f1 f2 f3
      | (F413|F431|F341|F241|F412|F421) -> 
          printf "%s_fgr(-%s,%s,%s,%s)" f c f1 f2 f3 

    let print_fermion_2_g4_current coeff f c wf1 wf2 wf3 fusion =
      let f1 = (List.nth (wf_of_f wf1 wf2 wf3 fusion) 0) and
          f2 = (List.nth (wf_of_f wf1 wf2 wf3 fusion) 1) and
          f3 = (List.nth (wf_of_f wf1 wf2 wf3 fusion) 2) in
      let c = format_coupling_2 coeff c in
      let c1 = fastener c 1 and
          c2 = fastener c 2 in 
      match fusion with
      | (F123|F213|F132|F231|F312|F321) -> 
          printf "f_%sgr(-(%s),-(%s),%s,%s,%s)" f c2 c1 f1 f2 f3
      | (F423|F243|F432|F234|F342|F324) ->
          printf "gr_%sf(%s,%s,%s,%s,%s)" f c1 c2 f1 f2 f3
      | (F134|F143|F314|F124|F142|F214) ->
          printf "%s_grf(%s,%s,%s,%s,%s)" f c1 c2 f1 f2 f3
      | (F413|F431|F341|F241|F412|F421) -> 
          printf "%s_fgr(-(%s),-(%s),%s,%s,%s)" f c2 c1 f1 f2 f3 

    let print_fermion_2_g4_current coeff f c wf1 wf2 wf3 fusion =
      let f1 = (List.nth (wf_of_f wf1 wf2 wf3 fusion) 0) and
          f2 = (List.nth (wf_of_f wf1 wf2 wf3 fusion) 1) and
          f3 = (List.nth (wf_of_f wf1 wf2 wf3 fusion) 2) in
      let c = format_coupling_2 coeff c in
      let c1 = fastener c 1 and
          c2 = fastener c 2 in 
      match fusion with
      | (F123|F213|F132|F231|F312|F321) -> 
          printf "f_%sgr(-(%s),-(%s),%s,%s,%s)" f c2 c1 f1 f2 f3
      | (F423|F243|F432|F234|F342|F324) ->
          printf "gr_%sf(%s,%s,%s,%s,%s)" f c1 c2 f1 f2 f3
      | (F134|F143|F314|F124|F142|F214) ->
          printf "%s_grf(%s,%s,%s,%s,%s)" f c1 c2 f1 f2 f3
      | (F413|F431|F341|F241|F412|F421) -> 
          printf "%s_fgr(-(%s),-(%s),%s,%s,%s)" f c2 c1 f1 f2 f3 


    let print_fermion_g4_current_rev coeff f c wf1 wf2 wf3 fusion =
      let c = format_coupling coeff c and
          f1 = (List.nth (wf_of_f wf1 wf2 wf3 fusion) 0) and
          f2 = (List.nth (wf_of_f wf1 wf2 wf3 fusion) 1) and
          f3 = (List.nth (wf_of_f wf1 wf2 wf3 fusion) 2) in
      match fusion with
      | (F123|F213|F132|F231|F312|F321) -> 
          printf "f_%sgr(%s,%s,%s,%s)" f c f1 f2 f3 
      | (F423|F243|F432|F234|F342|F324) -> 
          printf "gr_%sf(-%s,%s,%s,%s)" f c f1 f2 f3 
      | (F134|F143|F314|F124|F142|F214) -> 
          printf "%s_grf(-%s,%s,%s,%s)" f c f1 f2 f3 
      | (F413|F431|F341|F241|F412|F421) -> 
          printf "%s_fgr(%s,%s,%s,%s)" f c f1 f2 f3 

(* Here we have to distinguish which of the two bosons is produced in the
   fusion of three particles which include both fermions. *)

    let print_fermion_g4_vector_current coeff f c wf1 wf2 wf3 fusion = 
      let c = format_coupling coeff c and
          d1 = d_p (1,f) and
          d2 = d_p (2,f) and
          f1 = (List.nth (wf_of_f wf1 wf2 wf3 fusion) 0) and
          f2 = (List.nth (wf_of_f wf1 wf2 wf3 fusion) 1) and
          f3 = (List.nth (wf_of_f wf1 wf2 wf3 fusion) 2) in
      match fusion with
      | (F123|F213|F132|F231|F312|F321) -> 
          printf "f_%sgr(%s,%s,%s,%s)" f c f1 f2 f3 
      | (F423|F243|F432|F234|F342|F324) -> 
          printf "gr_%sf(%s,%s,%s,%s)" f c f1 f2 f3       
      | (F134|F143|F314) -> printf "%s%s_grf(%s,%s,%s,%s)" f d1 c f1 f2 f3 
      | (F124|F142|F214) -> printf "%s%s_grf(%s,%s,%s,%s)" f d2 c f1 f2 f3 
      | (F413|F431|F341) -> printf "%s%s_fgr(%s,%s,%s,%s)" f d1 c f1 f2 f3 
      | (F241|F412|F421) -> printf "%s%s_fgr(%s,%s,%s,%s)" f d2 c f1 f2 f3 

    let print_fermion_2_g4_vector_current coeff f c wf1 wf2 wf3 fusion = 
      let d1 = d_p (1,f) and
          d2 = d_p (2,f) and
          f1 = (List.nth (wf_of_f wf1 wf2 wf3 fusion) 0) and
          f2 = (List.nth (wf_of_f wf1 wf2 wf3 fusion) 1) and
          f3 = (List.nth (wf_of_f wf1 wf2 wf3 fusion) 2) in
      let c = format_coupling_2 coeff c in
      let c1 = fastener c 1 and
          c2 = fastener c 2 in 
      match fusion with
      | (F123|F213|F132|F231|F312|F321) -> 
          printf "f_%sgr(%s,%s,%s,%s,%s)" f c1 c2 f1 f2 f3 
      | (F423|F243|F432|F234|F342|F324) -> 
          printf "gr_%sf(%s,%s,%s,%s,%s)" f c1 c2 f1 f2 f3       
      | (F134|F143|F314) -> printf "%s%s_grf(%s,%s,%s,%s,%s)" f d1 c1 c2 f1 f2 f3 
      | (F124|F142|F214) -> printf "%s%s_grf(%s,%s,%s,%s,%s)" f d2 c1 c2 f1 f2 f3 
      | (F413|F431|F341) -> printf "%s%s_fgr(%s,%s,%s,%s,%s)" f d1 c1 c2 f1 f2 f3 
      | (F241|F412|F421) -> printf "%s%s_fgr(%s,%s,%s,%s,%s)" f d2 c1 c2 f1 f2 f3 

    let print_fermion_g4_vector_current_rev coeff f c wf1 wf2 wf3 fusion = 
      let c = format_coupling coeff c and
          d1 = d_p (1,f) and
          d2 = d_p (2,f) and
          f1 = (List.nth (wf_of_f wf1 wf2 wf3 fusion) 0) and
          f2 = (List.nth (wf_of_f wf1 wf2 wf3 fusion) 1) and
          f3 = (List.nth (wf_of_f wf1 wf2 wf3 fusion) 2) in
      match fusion with
      | (F123|F213|F132|F231|F312|F321) -> 
          printf "gr_%sf(%s,%s,%s,%s)" f c f1 f2 f3
      | (F423|F243|F432|F234|F342|F324) -> 
          printf "f_%sgr(%s,%s,%s,%s)" f c f1 f2 f3
      | (F134|F143|F314) -> printf "%s%s_fgr(%s,%s,%s,%s)" f d1 c f1 f2 f3 
      | (F124|F142|F214) -> printf "%s%s_fgr(%s,%s,%s,%s)" f d2 c f1 f2 f3 
      | (F413|F431|F341) -> printf "%s%s_grf(%s,%s,%s,%s)" f d1 c f1 f2 f3
      | (F241|F412|F421) -> printf "%s%s_grf(%s,%s,%s,%s)" f d2 c f1 f2 f3 

    let print_fermion_2_g4_current_rev coeff f c wf1 wf2 wf3 fusion =
      let c = format_coupling_2 coeff c in
      let c1 = fastener c 1 and
          c2 = fastener c 2  and
          d1 = d_p (1,f) and
          d2 = d_p (2,f) in
      let f1 = (List.nth (wf_of_f wf1 wf2 wf3 fusion) 0) and
          f2 = (List.nth (wf_of_f wf1 wf2 wf3 fusion) 1) and
          f3 = (List.nth (wf_of_f wf1 wf2 wf3 fusion) 2) in
      match fusion with
      | (F123|F213|F132|F231|F312|F321) -> 
          printf "gr_%sf(%s,%s,%s,%s,%s)" f c1 c2 f1 f2 f3
      | (F423|F243|F432|F234|F342|F324) ->
          printf "f_%sgr(-(%s),-(%s),%s,%s,%s)" f c1 c2 f1 f2 f3
      | (F134|F143|F314) ->
          printf "%s%s_fgr(-(%s),-(%s),%s,%s,%s)" f d1 c1 c2 f1 f2 f3 
      | (F124|F142|F214) ->
          printf "%s%s_fgr(-(%s),-(%s),%s,%s,%s)" f d2 c1 c2 f1 f2 f3 
      | (F413|F431|F341) ->
          printf "%s%s_grf(%s,%s,%s,%s,%s)" f d1 c1 c2 f1 f2 f3 
      | (F241|F412|F421) ->
          printf "%s%s_grf(%s,%s,%s,%s,%s)" f d2 c1 c2 f1 f2 f3 

    let print_fermion_2_g4_vector_current_rev coeff f c wf1 wf2 wf3 fusion = 
      (* Here we put in the extra minus sign from the coeff. *)
      let c = format_coupling coeff c in
      let c1 = fastener c 1 and
          c2 = fastener c 2 in 
      let d1 = d_p (1,f) and
          d2 = d_p (2,f) and
          f1 = (List.nth (wf_of_f wf1 wf2 wf3 fusion) 0) and
          f2 = (List.nth (wf_of_f wf1 wf2 wf3 fusion) 1) and
          f3 = (List.nth (wf_of_f wf1 wf2 wf3 fusion) 2) in
      match fusion with
      | (F123|F213|F132|F231|F312|F321) -> 
          printf "gr_%sf(%s,%s,%s,%s,%s)" f c1 c2 f1 f2 f3
      | (F423|F243|F432|F234|F342|F324) -> 
          printf "f_%sgr(%s,%s,%s,%s,%s)" f c1 c2 f1 f2 f3
      | (F134|F143|F314) -> printf "%s%s_fgr(%s,%s,%s,%s,%s)" f d1 c1 c2 f1 f2 f3 
      | (F124|F142|F214) -> printf "%s%s_fgr(%s,%s,%s,%s,%s)" f d2 c1 c2 f1 f2 f3 
      | (F413|F431|F341) -> printf "%s%s_grf(%s,%s,%s,%s,%s)" f d1 c1 c2 f1 f2 f3
      | (F241|F412|F421) -> printf "%s%s_grf(%s,%s,%s,%s,%s)" f d2 c1 c2 f1 f2 f3 


    let print_current_g4 = function
      | coeff, Gravbar, S2, _ -> print_fermion_g4_current coeff "s2" 
      | coeff, Gravbar, SV, _ -> print_fermion_g4_vector_current coeff "sv" 
      | coeff, Gravbar, SLV, _ -> print_fermion_g4_vector_current coeff "slv" 
      | coeff, Gravbar, SRV, _ -> print_fermion_g4_vector_current coeff "srv" 
      | coeff, Gravbar, SLRV, _ -> print_fermion_2_g4_vector_current coeff "slrv" 
      | coeff, Gravbar, PV, _ -> print_fermion_g4_vector_current coeff "pv"
      | coeff, Gravbar, V2, _ -> print_fermion_g4_current coeff "v2"
      | coeff, Gravbar, V2LR, _ -> print_fermion_2_g4_current coeff "v2lr"
      | coeff, Gravbar, _, _ -> invalid_arg "print_current_g4: not implemented"
      | coeff, _, S2, Grav -> print_fermion_g4_current_rev coeff "s2" 
      | coeff, _, SV, Grav -> print_fermion_g4_vector_current_rev (-coeff) "sv"
      | coeff, _, SLV, Grav -> print_fermion_g4_vector_current_rev (-coeff) "slv"
      | coeff, _, SRV, Grav -> print_fermion_g4_vector_current_rev (-coeff) "srv"
      | coeff, _, SLRV, Grav -> print_fermion_2_g4_vector_current_rev coeff "slrv"
      | coeff, _, PV, Grav -> print_fermion_g4_vector_current_rev coeff "pv" 
      | coeff, _, V2, Grav -> print_fermion_g4_vector_current_rev coeff "v2" 
      | coeff, _, V2LR, Grav -> print_fermion_2_g4_current_rev coeff "v2lr"
      | coeff, _, _, Grav -> invalid_arg "print_current_g4: not implemented"
      | coeff, _, S2, _ -> print_fermion_s2_current coeff "s"
      | coeff, _, P2, _ -> print_fermion_s2_current coeff "p"
      | coeff, _, S2P, _ -> print_fermion_s2p_current coeff "sp"
      | coeff, _, S2L, _ -> print_fermion_s2_current coeff "sl"
      | coeff, _, S2R, _ -> print_fermion_s2_current coeff "sr"
      | coeff, _, S2LR, _ -> print_fermion_s2lr_current coeff "slr"
      | coeff, _, V2, _ -> print_fermion_g4_brs_vector_current coeff "v2"
      | coeff, _, SV, _ -> print_fermion_g4_brs_vector_current coeff "sv"
      | coeff, _, PV, _ -> print_fermion_g4_brs_vector_current coeff "pv"
      | coeff, _, SLV, _ -> print_fermion_g4_brs_vector_current coeff "svl"
      | coeff, _, SRV, _ -> print_fermion_g4_brs_vector_current coeff "svr"
      | coeff, _, SLRV, _ -> print_fermion_g4_svlr_current coeff "svlr"
      | coeff, _, V2LR, _ -> invalid_arg "Targets.print_current: not available"

    let reverse_braket _ = false

    let use_module = "omega95_bispinors"
    let require_library =
      ["omega_bispinors_2003_03_A"; "omega_bispinor_cpls_2003_03_A"]
   end

module Fortran_Majorana = Make_Fortran(Fortran_Majorana_Fermions)

(* \thocwmodulesubsection{\texttt{FORTRAN\,77}} *)

module Fortran77 = Dummy

(* \thocwmodulesection{HELAS} *)

(* \begin{dubious}
     Starting from $2\to9$ processes, Fortran compilers can run out
     of memory.  Since the code is linear, I can only imagine that
     the large number of variables is the culprit.  One solution is
     to use a dense notation and arrays for momenta and wavefunctions.
     (Bitvectors like~\cite{ALPHA:1997,HELAC:2000} are also possible,
     but they would waste about 50\%{} of space.)
   \end{dubious} *)

module Helas (F : Fusion.T) (Make_MF : Fusion.MultiMaker)
    (M : Model.T with type flavor = F.flavor and type constant = F.constant) =
  struct

    let rcs_list =
      [ RCS.rename rcs_file "Targets.Helas()"
          [ "incomplete Fortran95 code calling HELAS routines";
            "only fermionic vector currents are supported" ] ]

    open Coupling
    module MF = Make_MF(F)
    type amplitude = F.amplitude
    type amplitudes = MF.amplitudes
    type diagnostic = All | Arguments | Helicities | Momenta | Gauge

    let function_name = ref "omega"
    let module_name = ref "omega_amplitude"
    let use_modules = ref []
    let kind = ref "omega_prec"
    let pure = "" (* ["pure "] *)

    let options = Options.create
      [ "function", Arg.String (fun s -> function_name := s),
        "function name";
        "kind", Arg.String (fun s -> kind := s), "real and complex kind";
        "module", Arg.String (fun s -> module_name := s), "module name";
        "use", Arg.String (fun s -> use_modules := s :: !use_modules),
        "use module" ]

    let out_channel = ref stdout

    let statement s =
      output_string !out_channel "      ";
      output_string !out_channel s;
      output_string !out_channel "\n"

    let p2s p =
      if p >= 0 && p <= 9 then
        string_of_int p
      else if p <= 36 then
        String.make 1 (Char.chr (Char.code 'A' + p - 10))
      else
        "_"

    let couplings = "gva"

    let format_sign rhs =
      if F.sign rhs < 0 then " - " else " + "

    let variable wf =
      M.flavor_symbol (F.flavor wf) ^
      String.concat "" (List.map p2s (F.momentum_list wf))

    let ext_momentum wf =
      match F.momentum_list wf with
      | [n] -> "k(:," ^ string_of_int n ^ ")"
      | _ -> invalid_arg "Targets.Helas.ext_momentum"

    let spin wf =
      match F.momentum_list wf with
      | [n] -> "s(" ^ string_of_int n ^ ")"
      | _ -> invalid_arg "Targets.Helas.spin"

    let declare_wavefunction wf =
      "complex(kind=" ^ !kind ^ "), dimension(" ^
      begin match M.lorentz (F.flavor wf) with
      | Scalar -> "3"
      | Spinor | ConjSpinor | Vector | Massive_Vector -> "6"
      | Tensor_1 -> "?"
      | Tensor_2 -> invalid_arg "Targets.Helas: Tensor_2 not handled"
      | Majorana ->
          invalid_arg "Targets.Helas: Majorana spinors not handled"
      | Maj_Ghost -> invalid_arg "Targets.Helas: SUSY ghosts not handled"
      | Vectorspinor -> invalid_arg "Targets.Helas: Spin 3/2 not handled"
      | BRS _ -> invalid_arg "Targets.Helas: no BRST"
      end ^ ") :: " ^ variable wf

    let temporary = function
      | Scalar -> "tmpsca"
      | Spinor -> "tmpspi"
      | ConjSpinor -> "tmpspb"
      | Vector | Massive_Vector -> "tmpvec"
      | Tensor_1 -> "tmpten"
      | Tensor_2 -> invalid_arg "Targets.Helas: Tensor_2 not handled"
      | Majorana ->
          invalid_arg "Targets.Helas: Majorana spinors not handled"
      | Maj_Ghost -> invalid_arg "Targets.Helas: SUSY ghosts not handled"
      | Vectorspinor -> invalid_arg "Targets.Helas: Spin 3/2 not handled"
      | BRS _ -> invalid_arg "Targets.Helas: no BRST"

    let declare_wavefunctions amplitude =
      statement ("complex(kind=" ^ !kind ^ "), dimension(3) :: tmpsca");
      statement ("complex(kind=" ^ !kind ^ "), dimension(6) :: tmpspi");
      statement ("complex(kind=" ^ !kind ^ "), dimension(6) :: tmpspb");
      statement ("complex(kind=" ^ !kind ^ "), dimension(6) :: tmpvec");
      List.iter (fun wf -> statement (declare_wavefunction wf))
        (F.externals amplitude @ F.variables amplitude)

(* The rules for external particles in HELAS are as follows:
   \begin{center}
     \hfil\\
     \hfil\\
     \begin{fmfgraph*}(30,15)
       \fmfincoming{em,ep}
       \fmfoutgoing{mm,mp}
       \fmflabel{$u(p_{e^-})$}{em}
       \fmflabel{$\bar v(p_{e^+})$}{ep}
       \fmflabel{$\bar u(p_{\mu^-})$}{mm}
       \fmflabel{$v(p_{\mu^+})$}{mp}
       \fmf{fermion}{em,v,ep}
       \fmf{fermion}{mp,v,mm}
       \fmfblob{.2w}{v}
     \end{fmfgraph*}\\
     \hfil\\
     \hfil\\
     \begin{tabular}{r<{:}>{\ttfamily}l}
                $u_h(p)$ & ixxxxx (p, mass, 2h, +1, wf) \\
           $\bar v_h(p)$ & oxxxxx (p, mass, 2h, -1, wf) \\
                $v_h(p)$ & ixxxxx (p, mass, 2h, -1, wf) \\
           $\bar u_h(p)$ & oxxxxx (p, mass, 2h, +1, wf) \\
         $\epsilon_h(p)$ & vxxxxx (p, mass, \ h, -1, wf) \\
       $\epsilon_h^*(p)$ & vxxxxx (p, mass, \ h, +1, wf)
     \end{tabular}
   \end{center} *)
 
    let format_incoming wf =
      let var = variable wf
      and f = F.flavor wf
      and p = ext_momentum wf
      and s = spin wf in
      let m = M.mass_symbol f in
      match M.lorentz f with
      | Scalar ->
          Printf.sprintf "sxxxxx (%s, -1, %s)" p var
      | Spinor ->
          Printf.sprintf "ixxxxx (%s, %s, %s, 1, %s)" p m s var
      | ConjSpinor ->
          Printf.sprintf "oxxxxx (%s, %s, %s, -1, %s)" p m s var
      | Vector | Massive_Vector ->
          Printf.sprintf "vxxxxx (%s, %s, %s, -1, %s)" p m s var
      | Tensor_1 -> "????"
      | Tensor_2 -> invalid_arg "Targets.Helas: Spin 2 not handled"
      | Majorana ->
          invalid_arg "Targets.Helas: Majorana spinors not handled"
      | Maj_Ghost -> invalid_arg "Targets.Helas: SUSY ghosts not handled"
      | Vectorspinor -> invalid_arg "Targets.Helas: Spin 3/2 not handled"
      | BRS _ -> invalid_arg "Targets.Helas: no BRST"
      
    let format_outgoing wf =
      let var = variable wf
      and f = F.flavor wf
      and p = ext_momentum wf
      and s = spin wf in
      let m = M.mass_symbol f in
      match M.lorentz f with
      | Scalar ->
          Printf.sprintf "sxxxxx (%s, 1, %s)" p var
      | Spinor ->
          Printf.sprintf "ixxxxx (%s, %s, %s, -1, %s)" p m s var
      | ConjSpinor ->
          Printf.sprintf "oxxxxx (%s, %s, %s, 1, %s)" p m s var
      | Vector | Massive_Vector ->
          Printf.sprintf "vxxxxx (%s, %s, %s, 1, %s)" p m s var
      | Tensor_1 -> "????"
      | Tensor_2 -> invalid_arg "Targets.Helas: Spin 2 not handled"
      | Majorana ->
          invalid_arg "Targets.Helas: Majorana spinors not handled"
      | Maj_Ghost -> invalid_arg "Targets.Helas: SUSY ghosts not handled"
      | Vectorspinor -> invalid_arg "Targets.Helas: Spin 3/2 not handled"
      | BRS _ -> invalid_arg "Targets.Helas: no BRST"

    let format_externals amplitude =
      match F.externals amplitude with
      | in1 :: in2 :: out ->
          statement ("call " ^ format_incoming in1);
          statement ("call " ^ format_incoming in2);
          List.iter (fun wf -> statement ("call " ^ format_outgoing wf)) out
      | _ -> invalid_arg "Targets.format_externals: # < 2"

    let current name wf1 wf2 coupl mass width var =
      Printf.sprintf "%sxxx (%s, %s, %s, %s, %s, %s)"
        name (variable wf1) (variable wf2) coupl mass width var
      
    let children rhs =
      match F.children rhs with
      | [wf1; wf2] -> (wf1, wf2)
      | [] | [_] -> failwith "Targets.children: can't happen"
      | _ -> invalid_arg "Targets.children: non-binary fusion"

    let format_coupling coeff c =
      match coeff with
      | 1 -> c
      | -1 -> "(-" ^ c ^")"
      | coeff -> string_of_int coeff ^ "*" ^ c

    let format_current f value rhs =
      let wf1, wf2 = children rhs
      and mass = M.mass_symbol f
      and width = M.width_symbol f in
      match F.coupling rhs with
      | V3 (v, fusion, constant) ->
          let c = M.constant_symbol constant in
          begin match v with
          | FBF (coeff, Psibar, V, Psi) ->
              let c = format_coupling coeff c in
              begin match fusion with
              | F13 -> current "jio" wf2 wf1 c mass width value
              | F31 -> current "jio" wf1 wf2 c mass width value
              | F23 -> current "fvi" wf2 wf1 c mass width value
              | F32 -> current "fvi" wf1 wf2 c mass width value
              | F12 -> current "fvo" wf1 wf2 c mass width value
              | F21 -> current "fvo" wf2 wf1 c mass width value
              end
          | _ -> current "???" wf1 wf2 "???" mass width value
          end
      | _ -> invalid_arg "Targets.format_current: non-binary fusion"

    let format_fusion fusion =
      let lhs = F.lhs fusion
      and rhs = F.rhs fusion in
      let value = variable lhs
      and f = F.flavor lhs in
      let tmp = temporary (M.lorentz f) in
      statement (value ^ " = 0");
      List.iter (fun c ->
        statement ("call " ^ format_current f tmp c);
        statement (value ^ " = " ^ value ^ format_sign c ^ tmp)) rhs

    let vertex name wf1 wf2 wf3 coupl var =
      Printf.sprintf "%sxxx (%s, %s, %s, %s, %s)"
        name (variable wf1) (variable wf2) (variable wf3) coupl var

    let format_vertex value bra rhs =
      let wf1, wf2 = children rhs in
      match F.coupling rhs with
      | V3 (v, fusion, constant) ->
          let c = M.constant_symbol constant in
          begin match v with
          | FBF (coeff, Psibar, V, Psi) ->
              let c = format_coupling coeff c in
              begin match fusion with
              | F13 -> vertex "iov" wf2 wf1 bra c value
              | F31 -> vertex "iov" wf1 wf2 bra c value
              | F23 -> vertex "iov" wf2 bra wf1 c value
              | F32 -> vertex "iov" wf1 bra wf2 c value
              | F12 -> vertex "iov" bra wf1 wf2 c value
              | F21 -> vertex "iov" bra wf2 wf1 c value
              end
          | _ -> vertex "???" bra wf1 wf2 "???" value
          end
      | _ -> invalid_arg "Targets.format_vertex: non-binary fusion"

    let format_braket name braket =
      let bra = F.bra braket in
      List.iter (fun rhs ->
        statement ("call " ^ format_vertex "tmpamp" bra rhs);
        statement (name ^ " = " ^ name ^ format_sign rhs ^ "tmpamp"))
        (F.ket braket)

    let format_header amplitude =
      (*i let externals = F.externals amplitude in i*)
      statement
        (pure ^ "function " ^ !function_name ^ " (k, s) result (amp)");
      statement ("complex(kind=" ^ !kind ^ ") :: amp");
      statement
        ("real(kind=" ^ !kind ^ "), dimension(0:,:), intent(in) :: k");
      statement ("integer, dimension(:), intent(in) :: s")

    let format_module_header () =
      statement ("module " ^ !module_name);
      statement ("use omega_kinds");
      statement ("use omega_parameters");
      statement ("use dhelas95");
      List.iter (fun s -> statement ("use " ^ s)) !use_modules;
      statement ("implicit none");
      statement ("contains")

    let format_module_footer () =
      statement ("end module " ^ !module_name)

    let amplitude_to_channel oc diagnostics amplitude =
      out_channel := oc;
      format_module_header ();
      format_header amplitude;
      declare_wavefunctions amplitude;
      statement ("complex(kind=" ^ !kind ^ ") :: tmpamp");
      format_externals amplitude;
      List.iter format_fusion (F.fusions amplitude);
      statement ("amp = 0");
      List.iter (format_braket "amp") (F.brakets amplitude);
      let s = F.symmetry amplitude in
      if s > 1 then begin
        statement ("amp = amp / sqrt(" ^
                   string_of_int s ^ ".0_" ^ !kind ^ ")");
      end;
      statement ("end function " ^ !function_name);
      format_module_footer ()

    let amplitudes_to_channel oc diagnostics amplitudes =
      failwith "Helas"

    module Common = Common_Fortran(F)(M)
    let parameters_to_channel oc =
      Common.parameters_to_fortran oc (M.parameters ())

  end

(* \thocwmodulesection{O'Mega Virtual Machine} *)

module VM (F : Fusion.T) (Make_MF : Fusion.MultiMaker)
    (M : Model.T with type flavor = F.flavor and type constant = F.constant) =
  struct
    let rcs_list =
      [ RCS.rename rcs_file "Targets.VM()"
          [ "Bytecode for the O'Mega Virtual Machine" ] ]

    module MF = Make_MF(F)
    type amplitude = F.amplitude
    type amplitudes = MF.amplitudes
    type diagnostic = All | Arguments | Helicities | Momenta | Gauge
    let options = Options.empty

    let flavors_to_string flavors =
      String.concat " " (List.map M.flavor_to_string flavors)

    let format_process amplitude =
      flavors_to_string (F.incoming amplitude) ^ " -> " ^
      flavors_to_string (F.outgoing amplitude)

    open Format
    open Coupling

    let ovm_LOAD_SCALAR = 1
    let ovm_LOAD_U = 2
    let ovm_LOAD_UBAR = 3
    let ovm_LOAD_V = 4
    let ovm_LOAD_VBAR = 5
    let ovm_LOAD_VECTOR = 6

    let ovm_ADD_MOMENTA = 10

    let ovm_PROPAGATE_SCALAR = 11
    let ovm_PROPAGATE_SPINOR = 12
    let ovm_PROPAGATE_CONJSPINOR = 13
    let ovm_PROPAGATE_UNITARITY = 14
    let ovm_PROPAGATE_FEYNMAN = 15
    let ovm_PROPAGATE_TENSOR2 = 16

    let ovm_FUSE_VECTOR_PSIBAR_PSI = 21
    let ovm_FUSE_PSI_VECTOR_PSI = 22
    let ovm_FUSE_PSIBAR_PSIBAR_VECTOR = 23

    type instruction = 
        { code : int; sign : int; coupl : int;
          lhs : int; rhs1 : int; rhs2 : int }

    let printi i =
      printf "@\n%3d %3d %3d %3d %3d %3d"
        i.code i.sign i.coupl i.lhs i.rhs1 i.rhs2

    let load lhs f rhs =
      let code =
        match M.lorentz f with
        | Scalar -> ovm_LOAD_SCALAR
        | Spinor -> ovm_LOAD_U
        | ConjSpinor -> ovm_LOAD_UBAR
        | Majorana -> failwith "load: Majoranas not implemented yet"
        | Maj_Ghost -> failwith "load: SUSY ghosts not implemented yet"
        | Vector | Massive_Vector -> ovm_LOAD_VECTOR
        | Vectorspinor -> invalid_arg "external spin must be <=1"
        | Tensor_1 -> invalid_arg "Tensor_1 only internal"
        | Tensor_2 -> invalid_arg "external spin must be <= 1"
        | BRS _ -> invalid_arg "no BRST"
 in
      { code = code; sign = 0; coupl = M.pdg f;
        lhs = lhs; rhs1 = rhs; rhs2 = rhs }
      
    let print_external count flavor =
      printi (load count (F.flavor flavor) count);
      succ count

    let print_externals amplitude =
      printf "@\n@[<2>BEGIN EXTERNALS";
      ignore (List.fold_left print_external 1 (F.externals amplitude));
      printf "@]@\nEND EXTERNALS"

    let print_current rhs =
      match F.coupling rhs with
      | V3 (vertex, fusion, constant) -> printf "@\nV3"
      | V4 (vertex, fusion, constant) -> printf "@\nV4"
      | Vn (_, _, _) -> printf "@\nVn"

    let p2s p =
      if p >= 0 && p <= 9 then
        string_of_int p
      else if p <= 36 then
        String.make 1 (Char.chr (Char.code 'A' + p - 10))
      else
        "_"

    let format_p wf =
      String.concat "" (List.map p2s (F.momentum_list wf))

    let print_fusion fusion =
      let lhs = F.lhs fusion in
      let f = F.flavor lhs in
      (*i let momentum = format_p lhs in i*)
      List.iter print_current (F.rhs fusion);
      let propagate code =
        printi { code = code; sign = 0; coupl = 0;
                 lhs = int_of_string (format_p lhs);
                 rhs1 = abs (M.pdg f); rhs2 = abs (M.pdg f) } in
      match M.propagator f with
      | Prop_Scalar -> propagate ovm_PROPAGATE_SCALAR
      | Prop_Col_Scalar -> 
          failwith "print_fusion: Prop_Col_Scalar not implemented yet!" 
      | Prop_Ghost -> 
          failwith "print_fusion: Prop_Ghost not implemented yet!" 
      | Prop_Spinor -> propagate ovm_PROPAGATE_SPINOR
      | Prop_ConjSpinor -> propagate ovm_PROPAGATE_CONJSPINOR
      | Prop_Majorana | Prop_Col_Majorana ->
          failwith "print_fusion: Prop_Majorana not implemented yet!"
      | Prop_Unitarity -> propagate ovm_PROPAGATE_UNITARITY
      | Prop_Col_Unitarity -> 
          failwith "print_fusion: Prop_Col_Unitarity not implemented yet!" 
      | Prop_Feynman -> propagate ovm_PROPAGATE_FEYNMAN
      | Prop_Col_Feynman -> 
          failwith "print_fusion: Prop_Col_Feynman not implemented yet!"
      | Prop_Gauge xi ->
          failwith "print_fusion: Prop_Gauge not implemented yet!"
      | Prop_Rxi xi ->
          failwith "print_fusion: Prop_Rxi not implemented yet!"
      | Prop_Vectorspinor -> 
          failwith "print_fusion: Prop_Vectorspinor not implemented yet!"
      | Prop_Tensor_2 -> propagate ovm_PROPAGATE_TENSOR2
      | Aux_Scalar | Aux_Spinor | Aux_ConjSpinor | Aux_Majorana
      | Aux_Vector | Aux_Tensor_1 -> ()
      | Only_Insertion -> ()

    module P = Set.Make (struct type t = int list let compare = compare end)

    let rec add_momenta lhs = function
      | [] | [_] -> invalid_arg "add_momenta"
      | [rhs1; rhs2] ->
          printi { code = ovm_ADD_MOMENTA; sign = 0; coupl = 0;
                   lhs = int_of_string (format_p lhs);
                   rhs1 = int_of_string (format_p rhs1);
                   rhs2 = int_of_string (format_p rhs2) }
      | rhs1 :: rhs ->
          add_momenta lhs rhs;
          add_momenta lhs [lhs; rhs1]

    let print_fusions amplitude =
      printf "@\n@[<2>BEGIN FUSIONS";
      let momenta =
        List.fold_left (fun seen f ->
          let wf = F.lhs f in
          let p = F.momentum_list wf in
          let momentum = format_p wf in
          if not (P.mem p seen) then
            add_momenta wf (F.children (List.hd (F.rhs f)));
          print_fusion f;
          P.add p seen) P.empty (F.fusions amplitude)
      in
      printf "@]@\nEND FUSIONS"

    let print_brakets amplitude =
      printf "@\n@[<2>BEGIN BRAKETS";
      printf "@\n!!! not implemented yet !!!";
      printf "@]@\nEND BRAKETS"

    let print_fudge_factor amplitude = 
      printf "@\n@[<2>BEGIN FUDGE";
      printf "@\n!!! not implemented yet !!!";
      printf "@]@\nEND FUDGE"

    let amplitude_to_channel oc diagnostics amplitude =
      set_formatter_out_channel oc;
      printf "@\n@[<2>BEGIN AMPLITUDE %s" (format_process amplitude);
      print_externals amplitude;
      print_fusions amplitude;
      print_brakets amplitude;
      print_fudge_factor amplitude;
      printf "@]@\nEND AMPLITUDE"

    let amplitudes_to_channel oc diagnostics amplitudes =
      List.iter (amplitude_to_channel oc diagnostics) (MF.allowed amplitudes)

    let parameters_to_channel oc =
      set_formatter_out_channel oc;
      (*i let params = M.parameters () in i*)
      printf "@[<2>BEGIN PARAMETERS@\n";
      printf "!!! not implemented yet !!!@]@\n";
      printf "END PARAMETERS@\n"

  end

(* \thocwmodulesection{\texttt{C}} *)

module C = Dummy

(* \thocwmodulesubsection{\texttt{C++}} *)

module Cpp = Dummy

(* \thocwmodulesubsection{Java} *)

module Java = Dummy

(* \thocwmodulesection{O'Caml} *)

module Ocaml = Dummy

(* \thocwmodulesection{\LaTeX} *)

module LaTeX = Dummy

(*i
 *  Local Variables:
 *  mode:caml
 *  indent-tabs-mode:nil
 *  page-delimiter:"^(\\* .*\n"
 *  End:
i*)
