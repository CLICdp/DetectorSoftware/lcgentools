(* $Id: color.ml 68 2007-11-22 11:11:19Z ohl $ *)
(* Copyright (C) 2001-2004 by Thorsten Ohl <ohl@physik.uni-wuerzburg.de> et al.
   O'Mega is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by 
   the Free Software Foundation; either version 2, or (at your option)
   any later version.
   O'Mega is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.  *)  

(* \thocwmodulesection{Quantum Numbers} *)

type t =
  | Singlet
  | SUN of int
  | AdjSUN of int

let conjugate = function
  | Singlet -> Singlet
  | SUN n -> SUN (-n)
  | AdjSUN n -> AdjSUN n

module type NC =
  sig
    val nc : int
  end
module NC3 = struct let nc = 3 end

(* \thocwmodulesection{Realistic Amplitudes} *)

type ('a, 'e) amplitude =
  | S of ('a, 'e) sng
  | F of ('a, 'e) fnd
  | C of ('a, 'e) cjg
  | A of ('a, 'e) adj

and ('a, 'e) sng =
  | S_ext of 'a * 'e
  | S_of_S of 'a * ('a, 'e) sng list
  | S_of_FC of 'a * ('a, 'e) fnd * ('a, 'e) cjg * ('a, 'e) sng list

and ('a, 'e) fnd =
  | F_ext of 'a * 'e
  | F_of_F of 'a * ('a, 'e) fnd * ('a, 'e) sng list
  | F_of_FA of 'a * ('a, 'e) fnd * ('a, 'e) adj * ('a, 'e) sng list

and ('a, 'e) cjg =
  | C_ext of 'a * 'e
  | C_of_C of 'a * ('a, 'e) cjg * ('a, 'e) sng list
  | C_of_CA of 'a * ('a, 'e) cjg * ('a, 'e) adj * ('a, 'e) sng list

and ('a, 'e) adj =
  | A_ext of 'a * 'e
  | A_of_FC of 'a * ('a, 'e) fnd * ('a, 'e) cjg * ('a, 'e) sng list
  | A_of_A of 'a * ('a, 'e) adj * ('a, 'e) sng list
  | A_of_AA of 'a * ('a, 'e) adj * ('a, 'e) adj * ('a, 'e) sng list

(* \thocwmodulesubsection{Construction} *)

exception Mismatch
exception Impossible_quartic
exception Incomplete

let ext c tag ext_tag =
  match c with
  | Singlet -> S (S_ext (tag, ext_tag))
  | SUN n when n > 0 -> F (F_ext (tag, ext_tag))
  | SUN n -> C (C_ext (tag, ext_tag))
  | AdjSUN n -> A (A_ext (tag, ext_tag))

let fuse2 color tag a1 a2 =
  match a1, a2 with
  | S s1, S s2 ->
      begin match color with 
      | Singlet ->
          begin match s1, s2 with
          | (S_ext _ as s), S_of_S (_, slist)
          | S_of_S (_, slist), (S_ext _ as s) ->
              S (S_of_S (tag, List.sort compare (s :: slist)))
          | S_of_S (_, slist1), S_of_S (_, slist2) ->
              S (S_of_S (tag, List.sort compare (slist1 @ slist2)))
          | _, _ ->
              if s1 < s2 then
                S (S_of_S (tag, [s1; s2]))
              else
                S (S_of_S (tag, [s2; s1]))
          end
      | _ -> raise Mismatch
      end
  | S s, F f | F f, S s ->
      begin match color with 
      | SUN n when n > 0 -> F (F_of_F (tag, f, [s]))
      | _ -> raise Mismatch
      end
  | S s, C c | C c, S s ->
      begin match color with 
      | SUN n when n < 0 -> C (C_of_C (tag, c, [s]))
      | _ -> raise Mismatch
      end
  | S s, A a | A a, S s ->
      begin match color with 
      | AdjSUN n -> A (A_of_A (tag, a, [s]))
      | _ -> raise Mismatch
      end
  | F _, F _ -> raise Mismatch
  | F f, C c | C c, F f ->
      begin match color with 
      | AdjSUN n -> A (A_of_FC (tag, f, c, []))
      | Singlet -> S (S_of_FC (tag, f, c, []))
      | _ -> raise Mismatch
      end
  | F f, A a | A a, F f ->
      begin match color with 
      | SUN n when n > 0 -> F (F_of_FA (tag, f, a, []))
      | _ -> raise Mismatch
      end
  | C _, C _ -> raise Mismatch
  | C c, A a | A a, C c ->
      begin match color with 
      | SUN n when n < 0 -> C (C_of_CA (tag, c, a, []))
      | _ -> raise Mismatch
      end

(* We must not redorder gluonic children, because the Feynman rule
   is antisymmetric: *)
  | A a1, A a2 ->
      begin match color with 
      | AdjSUN n -> A (A_of_AA (tag, a1, a2, []))
      | _ -> raise Mismatch
      end

(* The color amplitude factorizes \emph{only} for three gluon vertices and
   the four gluon vertices have to be implemented using an auxiliary field! *)

let fuse3 color tag a1 a2 a3 =
  match a1, a2, a3 with
  | A a1, A a2, A a3 -> raise Impossible_quartic
  | S s1, S s2, S s3 ->
      begin match color with 
      | Singlet -> S (S_of_S (tag, List.sort compare [s1; s2; s3]))
      | _ -> raise Mismatch
      end
  | _, _, _ -> raise Incomplete

let fuse color tag = function
  | [a1; a2] -> fuse2 color tag a1 a2
  | [a1; a2; a3] -> fuse3 color tag a1 a2 a3
  | _ -> raise Incomplete

let of_tree color proj tree =
  Tree.fold
    (fun node leaf -> ext (color node) (proj node) leaf)
    (fun node -> fuse (color node) (proj node))
    tree

(* \thocwmodulesubsection{Functionals} *)

type ('tag, 'ext, 'sng, 'fnd, 'cjg, 'adj, 'a) fold_functions =
    { s_ext : ('tag -> 'ext -> 'sng);
      s_of_s : ('tag -> 'sng list -> 'sng);
      s_of_fc : ('tag -> 'fnd -> 'cjg -> 'sng list -> 'sng);
      s_final : ('sng -> 'a);
      f_ext : ('tag -> 'ext -> 'fnd);
      f_of_f : ('tag -> 'fnd -> 'sng list -> 'fnd);
      f_of_fa : ('tag -> 'fnd -> 'adj -> 'sng list -> 'fnd);
      f_final : ('fnd -> 'a);
      c_ext : ('tag -> 'ext -> 'cjg);
      c_of_c : ('tag -> 'cjg -> 'sng list -> 'cjg);
      c_of_ca : ('tag -> 'cjg -> 'adj -> 'sng list -> 'cjg);
      c_final : ('cjg -> 'a);
      a_ext : ('tag -> 'ext -> 'adj);
      a_of_a : ('tag -> 'adj -> 'sng list -> 'adj);
      a_of_aa : ('tag -> 'adj -> 'adj -> 'sng list -> 'adj);
      a_of_fc : ('tag -> 'fnd -> 'cjg -> 'sng list -> 'adj);
      a_final : ('adj -> 'a) }

let rec fold_sng fct = function
  | S_ext (tag, ext_tag) -> fct.s_ext tag ext_tag
  | S_of_S (tag, s) -> fct.s_of_s tag (List.map (fold_sng fct) s)
  | S_of_FC (tag, f, c, s) ->
      fct.s_of_fc tag (fold_fnd fct f) (fold_cjg fct c) (List.map (fold_sng fct) s)

and fold_fnd fct = function
  | F_ext (tag, ext_tag) -> fct.f_ext tag ext_tag
  | F_of_F (tag, f, s) ->
      fct.f_of_f tag (fold_fnd fct f) (List.map (fold_sng fct) s)
  | F_of_FA (tag, f, a, s) ->
      fct.f_of_fa tag (fold_fnd fct f) (fold_adj fct a) (List.map (fold_sng fct) s)

and fold_cjg fct = function
  | C_ext (tag, ext_tag) -> fct.c_ext tag ext_tag
  | C_of_C (tag, c, s) ->
      fct.c_of_c tag (fold_cjg fct c) (List.map (fold_sng fct) s)
  | C_of_CA (tag, c, a, s) ->
      fct.c_of_ca tag (fold_cjg fct c) (fold_adj fct a) (List.map (fold_sng fct) s)

and fold_adj fct = function
  | A_ext (tag, ext_tag) -> fct.a_ext tag ext_tag
  | A_of_A (tag, a, s) ->
      fct.a_of_a tag (fold_adj fct a) (List.map (fold_sng fct) s)
  | A_of_FC (tag, f, c, s) ->
      fct.a_of_fc tag (fold_fnd fct f) (fold_cjg fct c) (List.map (fold_sng fct) s)
  | A_of_AA (tag, a1, a2, s) ->
      fct.a_of_aa tag (fold_adj fct a1) (fold_adj fct a2)
        (List.map (fold_sng fct) s)

let fold fct = function
  | S s -> fct.s_final (fold_sng fct s)
  | F f -> fct.f_final (fold_fnd fct f)
  | C c -> fct.c_final (fold_cjg fct c)
  | A a -> fct.a_final (fold_adj fct a)

(*i (* THIS IS FUN, BUT NOT REQUIRED ANYMORE!!! *)

let rec fold2_sng f2 f1 = function
  | S_ext (tag, ext_tag) -> f2.s_ext tag ext_tag
  | S_of_S (tag, s) -> f2.s_of_s tag (List.map (fold2_sng f1 f1) s)
  | S_of_FC (tag, f, c, s) ->
      f2.s_of_fc tag (fold2_fnd f1 f1 f) (fold2_cjg f1 f1 c)
        (List.map (fold2_sng f1 f1) s)

and fold2_fnd f2 f1 = function
  | F_ext (tag, ext_tag) -> f2.f_ext tag ext_tag
  | F_of_F (tag, f, s) ->
      f2.f_of_f tag (fold2_fnd f1 f1 f) (List.map (fold2_sng f1 f1) s)
  | F_of_FA (tag, f, a, s) ->
      f2.f_of_fa tag (fold2_fnd f1 f1 f) (fold2_adj f1 f1 a)
        (List.map (fold2_sng f1 f1) s)

and fold2_cjg f2 f1 = function
  | C_ext (tag, ext_tag) -> f2.c_ext tag ext_tag
  | C_of_C (tag, c, s) ->
      f2.c_of_c tag (fold2_cjg f1 f1 c) (List.map (fold2_sng f1 f1) s)
  | C_of_CA (tag, c, a, s) ->
      f2.c_of_ca tag (fold2_cjg f1 f1 c) (fold2_adj f1 f1 a)
        (List.map (fold2_sng f1 f1) s)

and fold2_adj f2 f1 = function
  | A_ext (tag, ext_tag) -> f2.a_ext tag ext_tag
  | A_of_A (tag, a, s) ->
      f2.a_of_a tag (fold2_adj f1 f1 a) (List.map (fold2_sng f1 f1) s)
  | A_of_FC (tag, f, c, s) ->
      f2.a_of_fc tag (fold2_fnd f1 f1 f) (fold2_cjg f1 f1 c)
        (List.map (fold2_sng f1 f1) s)
  | A_of_AA (tag, a1, a2, s) ->
      f2.a_of_aa tag (fold2_adj f1 f1 a1) (fold2_adj f1 f1 a2)
        (List.map (fold2_sng f1 f1) s)

let fold2 f2 f1 = function
  | S s -> f2.s_final (fold2_sng f2 f1 s)
  | F f -> f2.f_final (fold2_fnd f2 f1 f)
  | C c -> f2.c_final (fold2_cjg f2 f1 c)
  | A a -> f2.a_final (fold2_adj f2 f1 a)

let fold f = fold2 f f
i*)

(* \thocwmodulesubsection{Printing} *)

let to_string_fold_functions fmt fmt_ext =
  let outer pfx s =
    (* [pfx ^ ":" ^] *) s
  and ext pfx tag ext_tag =
    "<" ^ pfx ^ fmt_ext ext_tag ^ ">"
  and fuse pfx tag children =
    "<" ^ pfx ^ fmt tag ^ ">(" ^ String.concat "," children ^ ")" in
  let fuse1 pfx tag child children =
    fuse pfx tag (child :: children)
  and fuse2 pfx tag child1 child2 children =
    fuse pfx tag (child1 :: child2 :: children)
  (*i and fuse3 pfx tag child1 child2 child3 children =
    fuse pfx tag (child1 :: child2 :: child3 :: children) i*) in
  { s_ext = ext "S";
    s_of_s = fuse "S";
    s_of_fc = fuse2 "S";
    s_final = outer "S";
    f_ext = ext "F";
    f_of_f = fuse1 "F";
    f_of_fa =  fuse2 "F";
    f_final = outer "F";
    c_ext = ext "C";
    c_of_c = fuse1 "C";
    c_of_ca = fuse2 "C";
    c_final = outer "C";
    a_ext = ext "A";
    a_of_a = fuse1 "A";
    a_of_aa = fuse2 "A";
    a_of_fc = fuse2 "A";
    a_final = outer "A" }
    
let to_string fmt fmt_ext = fold (to_string_fold_functions fmt fmt_ext)

(* \thocwmodulesection{Evaluation} *)

module type Ring =
  sig
    type t
    val null : t
    val unit : t
    val mul : t -> t -> t
    val add : t -> t -> t
    val sub : t -> t -> t
    val neg : t -> t
    val to_float : t -> float
    val to_string : t -> string
  end

module type Rational =
  sig
    include Ring
    val is_null : t -> bool
    val make : int -> int -> t
  end

module type Coeff =
  sig
    include Ring
    val is_null : t -> bool
    val atom : int -> t
    val coeff : int -> int -> t
  end

module type Sum =
  sig
    module C : Coeff
    type 'a t
    val zero : 'a t
    val atom : 'a -> 'a t
    val scale : C.t -> 'a t -> 'a t
    val add : 'a t -> 'a t -> 'a t
    val sub : 'a t -> 'a t -> 'a t
    val mul : ('a -> 'b -> 'c) -> 'a t -> 'b t -> 'c t
    val mulx : ('a -> 'b -> 'c t) -> 'a t -> 'b t -> 'c t -> 'c t
    val mulc : ('a -> 'b -> C.t) -> 'a t -> 'b t -> C.t -> C.t
    val map : ('a -> 'b) -> 'a t -> 'b t
    val eval : ('a -> C.t) -> 'a t -> C.t
    val to_string : ('a -> string) -> 'a t -> string
    val terms : 'a t -> 'a list
  end

(* \thocwmodulesection{Color Flow Representation} *)

module type Flows =
  sig
    module C : Coeff
    type 'a t
    type 'a wf
    val to_string : ('a -> string) -> 'a t -> string
    val of_amplitude : 'e -> ('a, 'e) amplitude -> 'e t
    val square : ('a -> 'a) -> 'a t -> 'a t -> 'a t
    val eval : 'a t -> C.t
    val eval_square : ('a -> 'a) -> 'a t -> 'a t -> C.t
    type 'a hash
    val make_hash : unit -> 'a hash
    val eval_memoized : 'a hash -> 'a t -> C.t
    val eval_square_memoized : 'a hash -> ('a -> 'a) -> 'a t -> 'a t -> C.t
  end

module Make_Flows (S : Sum) : Flows with module C = S.C =
  struct

    module C = S.C

    let one = S.C.unit
    let minus_one = S.C.neg one
    let two = S.C.coeff 2 1
    let half = S.C.coeff 1 2
    let nc = S.C.atom 1
    let minus_one_over_two_nc = S.C.neg (S.C.mul half (S.C.atom (-1)))

    type 'a lines = ('a * 'a) list

    let canonicalize lines = List.sort compare lines

(* \begin{dubious}
     [expand_sum] (and the functions using it) assumes too much about the
     physical representation of singlet terms! 
   \end{dubious} *)
    let expand_sum sum = List.fold_left (S.mul (@)) (S.atom []) sum

(* The first member of these pairs is always the list of tags of external
   gluons contained in the amplitude.  This information must be maintained
   in order to avoid duplicate application of the completeness relation
   for them. *)
    type 'a sng = 'a list * ('a lines) S.t
    type 'a fnd = 'a list * ('a * 'a lines) S.t
    type 'a cjg = 'a list * ('a * 'a lines) S.t
    type 'a adj = 'a list * ('a * 'a * 'a lines) S.t

    type 'a t = 'a sng

    type 'a wf = 
      | S of 'a sng
      | F of 'a fnd
      | C of 'a cjg
      | A of 'a adj

    let ext_s tag' tag = ([], S.atom [])
    let ext_f tag' tag = ([], S.atom (tag, []))
    let ext_c tag' tag = ([], S.atom (tag, []))
    let ext_a tag' tag = ([tag], S.atom (tag, tag, []))

(* Things are trivial, as long as there are no colors at all or all colors
   are coupled to singlets.  *)

    let merge_s tag sngs =
      let gluons, sum = List.split sngs in
      (List.concat gluons, expand_sum sum)

    let mul_s_fc (f, lf) (c, lc) = canonicalize ((f, c) :: lf @ lc)

    let merge_s_fc tag (gluons_f, sum_f) (gluons_c, sum_c) sngs =
      let gluons, sum = List.split sngs in
      (gluons_f @ gluons_c @ List.concat gluons,
       expand_sum (S.mul mul_s_fc sum_f sum_c :: sum))

(* Things remain simple, as long as colored particles emit and absorb
   only colorless particles: *)

    let merge1 mul (gluons1, sum1) sngs =
      let gluons, sum = List.split sngs in
      (gluons1 @ List.concat gluons, S.mul mul sum1 (expand_sum sum))
        
    let mul_f (f, lf) l = (f, canonicalize (lf @ l))
    let mul_c (c, lc) l = (c, canonicalize (lc @ l))
    let mul_a (f, c, la) l = (f, c, canonicalize (la @ l))

    let merge_f tag f sngs = merge1 mul_f f sngs
    let merge_c tag c sngs = merge1 mul_c c sngs
    let merge_a tag a sngs = merge1 mul_a a sngs

(* We have only one way to emit a gluon from a quark anti-quark current: *)

    let mul_a_fc (f, lf) (c, lc) = (f, c, canonicalize (lf @ lc))

    let merge_a_fc tag (gluons_f, sum_f) (gluons_c, sum_c) sngs =
      let gluons, sum = List.split sngs in
      (gluons_f @ gluons_c @ List.concat gluons,
       S.mul mul_a (S.mul mul_a_fc sum_f sum_c) (expand_sum sum))

(* Using the $\mathrm{SU}(N_C)$ completeness relation
   \begin{equation}
     T^{a}_{ij} T^{a}_{kl} = 
         \frac{1}{2} \delta_{il} \delta_{jk}
       - \frac{1}{2N_C} \delta_{ij} \delta_{kl}
   \end{equation}
   for the conventional normalization 
   \begin{equation}
     \textrm{tr}(T_{a}T_{b}) = \frac{1}{2}\delta_{ab}
   \end{equation} *)

    let merge2 mul mulx (gluons1, sum1) (gluons2, sum2) sngs =
      let gluons, sum = List.split sngs in
      (gluons1 @ gluons2 @ List.concat gluons,
       S.mul mul (S.mulx mulx sum1 sum2 S.zero) (expand_sum sum))

    let absorb_glue mul1 mul2 q (af, ac, _ as a) =
      let q' = S.atom q
      and a' = S.atom a in
      if af = ac then
        S.mul mul1 q' a'
      else
        S.add
          (S.scale half (S.mul mul1 q' a'))
          (S.scale minus_one_over_two_nc (S.mul mul2 q' a'))

    let mul_fa_1 (f, lf) (af, ac, la) = (af, canonicalize ((f, ac) :: lf @ la))
    let mul_fa_2 (f, lf) (af, ac, la) = (f, canonicalize ((af, ac) :: lf @ la))

    let mul_ca_1 (c, lc) (af, ac, la) = (ac, canonicalize ((af, c) :: lc @ la))
    let mul_ca_2 (c, lc) (af, ac, la) = (c, canonicalize ((af, ac) :: lc @ la))

    let merge_fa tag f a sngs =
      merge2 mul_f (absorb_glue mul_fa_1 mul_fa_2) f a sngs

    let merge_ca tag c a sngs =
      merge2 mul_c (absorb_glue mul_ca_1 mul_ca_2) c a sngs

(* The fun starts here, but the completeness relation turns out to be
   surprinsingly simple.  The $1/N_{C}$-terms cancel always due to the
   antisymmetry \ldots{} *)

    let mul_aa_1 (f1, c1, l1) (f2, c2, l2) =
      (f1, c2, canonicalize ((f2, c1) :: l1 @ l2))
    let mul_aa_2 (f1, c1, l1) (f2, c2, l2) =
      (f2, c1, canonicalize ((f1, c2) :: l1 @ l2))

(* \ldots{} and only the prefactor changes if we do apply the completeness
   relation for internal gluons or don't for external gluons:
   \begin{subequations}
   \begin{align}
     f_{abc} &= - 2\ii \tr\left(\lbrack T_a,T_b\rbrack T_c\right) \\
     \parbox{26mm}{\fmfframe(2,2)(2,1){\begin{fmfgraph*}(22,22)
       \fmftop{g}
       \fmfbottom{g1,g2}
       \fmf{gluon}{v,g1}
       \fmf{gluon}{v,g2}
       \fmf{gluon}{v,g}
       \fmfdot{v}
     \end{fmfgraph*}}}
     \quad
      &= 2\left(
            \parbox{26mm}{\fmfframe(2,2)(2,1){\begin{fmfgraph*}(22,22)
              \fmftop{g}
              \fmfbottom{g1,g2}
              \fmf{gluon}{v1,g1}
              \fmf{gluon}{v2,g2}
              \fmf{gluon}{v3,g}
              \fmf{plain_arrow,tension=0.5,left=0.4}{v1,v3,v2,v1}
              \fmfdot{v1,v2,v3}
            \end{fmfgraph*}}}
          \right)
       - 2\left(
            \parbox{26mm}{\fmfframe(2,2)(2,1){\begin{fmfgraph*}(22,22)
              \fmftop{g}
              \fmfbottom{g1,g2}
              \fmf{gluon}{v1,g1}
              \fmf{gluon}{v2,g2}
              \fmf{gluon}{v3,g}
              \fmf{plain_arrow,tension=0.5,right=0.4}{v1,v2,v3,v1}
              \fmfdot{v1,v2,v3}
            \end{fmfgraph*}}}
          \right)
   \end{align}
   \end{subequations}
   Both incoming gluons external, i.\,e.~the completeness relation is
   never applied
   \begin{equation}
     \parbox{26mm}{\fmfframe(2,2)(2,1){\begin{fmfgraph*}(22,22)
       \fmftop{g}
       \fmfbottom{g1,g2}
       \fmf{gluon}{v,g1}
       \fmf{gluon}{v,g2}
       \fmf{gluon}{v,g}
       \fmfdot{v}
     \end{fmfgraph*}}}
     \quad
       = 2\left(
            \parbox{26mm}{\fmfframe(2,2)(2,1){\begin{fmfgraph*}(22,22)
              \setupthreegluons
              \fmfi{plain_arrow,rubout}{a2{v-g2}...{g1-v}b1}
              \fmfi{plain_arrow,rubout}{a1{v-g1}...{g3-v}b3}
              \fmfi{plain_arrow,rubout}{a3{v-g3}...{g2-v}b2}
            \end{fmfgraph*}}}
          \right)
       - 2\left(
            \parbox{26mm}{\fmfframe(2,2)(2,1){\begin{fmfgraph*}(22,22)
              \setupthreegluons
              \fmfi{plain_arrow}{a1{v-g1}...{g2-v}b2}
              \fmfi{plain_arrow}{a2{v-g2}...{g3-v}b3}
              \fmfi{plain_arrow}{a3{v-g3}...{g1-v}b1}
            \end{fmfgraph*}}}
          \right)
   \end{equation}
   The right incoming gluon is external and the left internal,
   i.\,e.~the completeness relation is only applied to the left
   \begin{multline}
     \parbox{26mm}{\fmfframe(2,2)(2,1){\begin{fmfgraph*}(22,22)
       \fmftop{g}
       \fmfbottom{g1,g2}
       \fmf{gluon}{v,g1}
       \fmf{gluon}{v,g2}
       \fmf{gluon}{v,g}
       \fmfdot{v}
     \end{fmfgraph*}}}
     \quad
       =  \left(
            \parbox{26mm}{\fmfframe(2,2)(2,1){\begin{fmfgraph*}(22,22)
              \setupthreegluons
              \fmfi{plain_arrow,rubout}{a2{v-g2}...{g1-v}b1}
              \fmfi{plain_arrow,rubout}{a1{v-g1}...{g3-v}b3}
              \fmfi{plain_arrow,rubout}{a3{v-g3}...{g2-v}b2}
            \end{fmfgraph*}}}
          \right)
       - \frac{1}{N_C}\left(
            \parbox{26mm}{\fmfframe(2,2)(2,1){\begin{fmfgraph*}(22,22)
              \setupthreegluons
              \fmfi{plain_arrow,rubout}{a1{v-g1}..v..{g1-v}b1}
              \fmfi{plain_arrow,rubout}{a2{v-g2}...{g3-v}b3}
              \fmfi{plain_arrow,rubout}{a3{v-g3}...{g2-v}b2}
            \end{fmfgraph*}}}
          \right) \\
       - \left(
            \parbox{26mm}{\fmfframe(2,2)(2,1){\begin{fmfgraph*}(22,22)
              \setupthreegluons
              \fmfi{plain_arrow}{a1{v-g1}...{g2-v}b2}
              \fmfi{plain_arrow}{a2{v-g2}...{g3-v}b3}
              \fmfi{plain_arrow}{a3{v-g3}...{g1-v}b1}
            \end{fmfgraph*}}}
          \right)
       + \frac{1}{N_C}\left(
            \parbox{26mm}{\fmfframe(2,2)(2,1){\begin{fmfgraph*}(22,22)
              \setupthreegluons
              \fmfi{plain_arrow,rubout}{a1{v-g1}..v..{g1-v}b1}
              \fmfi{plain_arrow,rubout}{a2{v-g2}...{g3-v}b3}
              \fmfi{plain_arrow,rubout}{a3{v-g3}...{g2-v}b2}
            \end{fmfgraph*}}}
          \right) \\
       =  \left(
            \parbox{26mm}{\fmfframe(2,2)(2,1){\begin{fmfgraph*}(22,22)
              \setupthreegluons
              \fmfi{plain_arrow,rubout}{a2{v-g2}...{g1-v}b1}
              \fmfi{plain_arrow,rubout}{a1{v-g1}...{g3-v}b3}
              \fmfi{plain_arrow,rubout}{a3{v-g3}...{g2-v}b2}
            \end{fmfgraph*}}}
          \right)
       -  \left(
            \parbox{26mm}{\fmfframe(2,2)(2,1){\begin{fmfgraph*}(22,22)
              \setupthreegluons
              \fmfi{plain_arrow}{a1{v-g1}...{g2-v}b2}
              \fmfi{plain_arrow}{a2{v-g2}...{g3-v}b3}
              \fmfi{plain_arrow}{a3{v-g3}...{g1-v}b1}
            \end{fmfgraph*}}}
          \right)
   \end{multline}
   The left incoming gluon is external and the right internal,
   i.\,e.~the completeness relation is only applied to the right
   \begin{multline}
     \parbox{26mm}{\fmfframe(2,2)(2,1){\begin{fmfgraph*}(22,22)
       \fmftop{g}
       \fmfbottom{g1,g2}
       \fmf{gluon}{v,g1}
       \fmf{gluon}{v,g2}
       \fmf{gluon}{v,g}
       \fmfdot{v}
     \end{fmfgraph*}}}
     \quad
       =  \left(
            \parbox{26mm}{\fmfframe(2,2)(2,1){\begin{fmfgraph*}(22,22)
              \setupthreegluons
              \fmfi{plain_arrow,rubout}{a2{v-g2}...{g1-v}b1}
              \fmfi{plain_arrow,rubout}{a1{v-g1}...{g3-v}b3}
              \fmfi{plain_arrow,rubout}{a3{v-g3}...{g2-v}b2}
            \end{fmfgraph*}}}
          \right)
       - \frac{1}{N_C}\left(
            \parbox{26mm}{\fmfframe(2,2)(2,1){\begin{fmfgraph*}(22,22)
              \setupthreegluons
              \fmfi{plain_arrow,rubout}{a2{v-g2}..v..{g2-v}b2}
              \fmfi{plain_arrow,rubout}{a1{v-g1}...{g3-v}b3}
              \fmfi{plain_arrow,rubout}{a3{v-g3}...{g1-v}b1}
            \end{fmfgraph*}}}
          \right) \\
       - \left(
            \parbox{26mm}{\fmfframe(2,2)(2,1){\begin{fmfgraph*}(22,22)
              \setupthreegluons
              \fmfi{plain_arrow}{a1{v-g1}...{g2-v}b2}
              \fmfi{plain_arrow}{a2{v-g2}...{g3-v}b3}
              \fmfi{plain_arrow}{a3{v-g3}...{g1-v}b1}
            \end{fmfgraph*}}}
          \right)
       + \frac{1}{N_C}\left(
            \parbox{26mm}{\fmfframe(2,2)(2,1){\begin{fmfgraph*}(22,22)
              \setupthreegluons
              \fmfi{plain_arrow,rubout}{a2{v-g2}..v..{g2-v}b2}
              \fmfi{plain_arrow,rubout}{a1{v-g1}...{g3-v}b3}
              \fmfi{plain_arrow,rubout}{a3{v-g3}...{g1-v}b1}
            \end{fmfgraph*}}}
          \right) \\
       =  \left(
            \parbox{26mm}{\fmfframe(2,2)(2,1){\begin{fmfgraph*}(22,22)
              \setupthreegluons
              \fmfi{plain_arrow,rubout}{a2{v-g2}...{g1-v}b1}
              \fmfi{plain_arrow,rubout}{a1{v-g1}...{g3-v}b3}
              \fmfi{plain_arrow,rubout}{a3{v-g3}...{g2-v}b2}
            \end{fmfgraph*}}}
          \right)
       -  \left(
            \parbox{26mm}{\fmfframe(2,2)(2,1){\begin{fmfgraph*}(22,22)
              \setupthreegluons
              \fmfi{plain_arrow}{a1{v-g1}...{g2-v}b2}
              \fmfi{plain_arrow}{a2{v-g2}...{g3-v}b3}
              \fmfi{plain_arrow}{a3{v-g3}...{g1-v}b1}
            \end{fmfgraph*}}}
          \right)
   \end{multline}
   Both incoming gluons internal, i.\,e.~the completeness relation is
   applied twice and as we have seen it corresponds to a factor of~$1/2$
   each time.
   \begin{multline}
     \parbox{26mm}{\fmfframe(2,2)(2,1){\begin{fmfgraph*}(22,22)
       \fmftop{g}
       \fmfbottom{g1,g2}
       \fmf{gluon}{v,g1}
       \fmf{gluon}{v,g2}
       \fmf{gluon}{v,g}
       \fmfdot{v}
     \end{fmfgraph*}}}
     \quad
       = \frac{1}{2}\left(
            \parbox{26mm}{\fmfframe(2,2)(2,1){\begin{fmfgraph*}(22,22)
              \setupthreegluons
              \fmfi{plain_arrow,rubout}{a2{v-g2}...{g1-v}b1}
              \fmfi{plain_arrow,rubout}{a1{v-g1}...{g3-v}b3}
              \fmfi{plain_arrow,rubout}{a3{v-g3}...{g2-v}b2}
            \end{fmfgraph*}}}
          \right)
       - \frac{1}{2}\left(
            \parbox{26mm}{\fmfframe(2,2)(2,1){\begin{fmfgraph*}(22,22)
              \setupthreegluons
              \fmfi{plain_arrow}{a1{v-g1}...{g2-v}b2}
              \fmfi{plain_arrow}{a2{v-g2}...{g3-v}b3}
              \fmfi{plain_arrow}{a3{v-g3}...{g1-v}b1}
            \end{fmfgraph*}}}
          \right)
   \end{multline} *)
        
    let fuse_aa (a1f, a1c, _ as a1) (a2f, a2c, _ as a2) =
      let a1 = S.atom a1
      and a2 = S.atom a2 in
      let aa = S.sub (S.mul mul_aa_1 a1 a2) (S.mul mul_aa_2 a1 a2) in
      match (a1f = a1c), (a2f = a2c) with
      | true, true -> S.scale two aa
      | false, false -> S.scale half aa
      | true, false | false, true -> aa

    let merge_aa tag a1 a2 sngs = merge2 mul_a fuse_aa a1 a2 sngs

    let finalize_s s' t = t

    let finalize_f f' (gluons, sum) =
      (gluons, S.map (fun (f, l) -> canonicalize ((f, f') :: l)) sum)

    let finalize_c c' (gluons, sum) =
      (gluons, S.map (fun (c, l) -> canonicalize ((c', c) :: l)) sum)

    let finalize_a a' (gluons, sum) =
      (a' :: gluons,
       S.map (fun (f, c, l) -> canonicalize ((a', c) :: (f, a') :: l)) sum)

    let of_amplitude_fold_functions root =
      { s_ext = ext_s;
        s_of_s = merge_s;
        s_of_fc = merge_s_fc;
        s_final = finalize_s root;
        f_ext = ext_f;
        f_of_f = merge_f;
        f_of_fa = merge_fa;
        f_final = finalize_f root;
        c_ext = ext_c;
        c_of_c = merge_c;
        c_of_ca = merge_ca;
        c_final = finalize_c root;
        a_ext = ext_a;
        a_of_a = merge_a;
        a_of_aa = merge_aa;
        a_of_fc = merge_a_fc;
        a_final = finalize_a root }

    let of_amplitude root a =
      fold (of_amplitude_fold_functions root) a

    exception Open_flow

(* It is crucial to apply the completeness relation
   \begin{equation}
     T^{a}_{ij} (T^{a}_{kl})^{*} = T^{a}_{ij} T^{a}_{lk} = 
         \frac{1}{2} \delta_{ik} \delta_{jl}
       - \frac{1}{2N_C} \delta_{ij} \delta_{kl}
   \end{equation}
   either to the flow or the conjugated flow.  It was appealing to apply it to
   the product, but this results in  a desastrous quadratic behaviour!
   \begin{subequations}
   \begin{equation}
      T:  (f,g)(g,c)  \to  (f,g)(g',c) 
   \end{equation} *)

    let flip_fc_left flip is_gluon sum =
      S.map (List.map (fun (f, c) -> ((if is_gluon f then flip f else f), c))) sum

(* \begin{equation}
     \begin{aligned}
        T^*:&  (f,c)       \to  (c,f)  \\
        T^*:&  (f,g)(g,c)  \to
               (c,g)(g,f)  \to \frac{1}{2} (c,g')(g,f) - \frac{1}{2N_C} (g,g')(c,f) 
     \end{aligned}
   \end{equation}
   \end{subequations} *)

    let find_gluon gluon pairs =
      let rec find_gluon_cf seen = function
        | [] -> raise Not_found
        | (c, f as cf) :: cfs ->
            if c = gluon then
              find_gluon_f f seen cfs
            else if f = gluon then
              find_gluon_c c seen cfs
            else
              find_gluon_cf (cf :: seen) cfs
      and find_gluon_f f seen = function
        | [] -> invalid_arg "incomplete gluon"
        | (c', f' as cf) :: cfs ->
            if f' = gluon then
              ((c', f), List.rev_append seen cfs)
            else if c' = gluon then
              invalid_arg "duplicate gluon"
            else
              find_gluon_f f (cf :: seen) cfs
      and find_gluon_c c seen = function
        | [] -> invalid_arg "incomplete gluon"
        | (c', f' as cf) :: cfs ->
            if c' = gluon then
              ((c, f'), List.rev_append seen cfs)
            else if f' = gluon then
              invalid_arg "duplicate gluon"
            else
              find_gluon_c c (cf :: seen) cfs in
      find_gluon_cf [] pairs

    let flip1_cf_right flip gluon sum =
      let gluon' = flip gluon in
      S.add
        (S.scale half
           (S.map (List.map (fun (c, f) ->
             (c, if f = gluon then gluon' else f))) sum))
        (S.scale minus_one_over_two_nc
           (S.map (fun pairs ->
             let cf', cfs' = find_gluon gluon pairs in
             (gluon, gluon') :: cf' :: cfs') sum))

    let flip_fc_right flip gluons sum =
      List.fold_right (flip1_cf_right flip) gluons
        (S.map (List.map (fun (f, c) -> (c, f))) sum)

(* \begin{dubious}
     Possible further optimizations:
     \begin{itemize}
       \item count and consume all \emph{non-gluon} cycles \emph{before} applying
         the completeness relation in [flip_fc], then apply the completeness relation
         and count and consume the processed cycles
     \end{itemize}
   \end{dubious} *)

    let square flip (gluons1, sum1) (gluons2, sum2) =
      assert (List.sort compare gluons1 = List.sort compare gluons2);
      ([],
       S.mul (fun l1 l2 -> l1 @ l2)
         (flip_fc_left flip (fun g -> List.mem g gluons1) sum1)
         (flip_fc_right flip gluons2 sum2))

(* \begin{dubious}
     The following algorithm for counting the cycles is quadratic since it
     performs nested scans of the lists.  If this was a serious problem one could
     replace the lists of pairs by a [Map] and replace one power by a logarithm.
   \end{dubious}
   However \ldots
   \begin{dubious}
     \ldots{} (much to my surprise), the most expensive (i.\,e.~inefficient)
     operation turned out to be an inefficient implementation of [square].
   \end{dubious} *)

    let consume_cycle f0 c0 lines =
      let rec consume_cycle' c' seen = function
        | [] -> raise Open_flow
        | (f, c) :: fc ->
            if c = f0 then
              (f, c') :: List.rev_append seen fc
            else if f = c' then
              consume_cycle' c [] (List.rev_append seen fc)
            else
              consume_cycle' c' ((f, c) :: seen) fc in
      consume_cycle' c0 [] lines

    let count_cycles lines =
      let rec count_cycles' acc = function
        | [] -> acc
        | (f, c) :: fc ->
	    if f = c then
	      count_cycles' (S.C.mul acc nc) fc
	    else
              count_cycles' acc (consume_cycle f c fc) in
      count_cycles' one lines

    let eval (gluons, sum) =
      assert (List.length gluons = 0);
      S.eval count_cycles sum

(* This deforestation is very helpful and conserves \emph{a lot} of memory! *)

    let eval_square flip (gluons1, sum1) (gluons2, sum2) =
      assert (List.sort compare gluons1 = List.sort compare gluons2);
      S.mulc (fun l1 l2 -> count_cycles (l1 @ l2))
        (flip_fc_left flip (fun g -> List.mem g gluons1) sum1)
        (flip_fc_right flip gluons2 sum2)
        S.C.null

(* Memoization is precisely as useful as the lookup is efficient.  Empirically,
   more than 99\%{} of all lookups will be successful in complicated applications.
   However, the naive use of [Hashtbl] leads to \emph{terrible} results which
   are more than an order of magnitude slower than naive evaluation. *)

(* \begin{dubious}
     On the other hand, using a polymorphic [Trie] doesn't slow down
     things significantly, but it doesn't appear to speed them up either.
   \end{dubious} *)

    module PT = Trie.MakePoly (Pmap.Tree)

    type 'a hash = ('a * 'a, S.C.t) PT.t ref

    let make_hash () =
      ref PT.empty

    let count_cycles_memoized hash lines =
      try
        PT.find compare lines !hash
      with
      | Not_found ->
          let result = count_cycles lines in
          hash := PT.add compare lines result !hash;
          result

    let eval_memoized hash (gluons, sum) =
      assert (List.length gluons = 0);
      S.eval (count_cycles_memoized hash) sum

    let eval_square_memoized hash flip (gluons1, sum1) (gluons2, sum2) =
      assert (List.sort compare gluons1 = List.sort compare gluons2);
      S.mulc (fun l1 l2 -> count_cycles_memoized hash (l1 @ l2))
        (flip_fc_left flip (fun g -> List.mem g gluons1) sum1)
        (flip_fc_right flip gluons2 sum2)
        S.C.null

(* \thocwmodulesubsection{Printing Revisited} *)

    let gluons_to_string fmt = function
      | [] -> ""
      | gluons -> "<glue=" ^ String.concat "," (List.map fmt gluons) ^ ">"
        
    let sng_to_string fmt lines =
      String.concat "/" (List.map (fun (f, c) -> fmt f ^ ":" ^ fmt c) lines)

    let to_string fmt (gluons, sum) =
      gluons_to_string fmt gluons ^ S.to_string (sng_to_string fmt) sum

  end

(* \thocwmodulesection{Evaluation Revisited} *)

module Make_Sum_Simple (C : Coeff) : Sum =
  struct

    module C = C

    let one = C.coeff 1 1
    let minus_one = C.coeff (-1) 1

    type 'a summand = { coeff : C.t; term : 'a }

(* \begin{dubious}
     This implementation does not combine identical terms.
   \end{dubious} *)

    type 'a t = 'a summand list

    let zero : 'a t = []

    let atom1 t = { coeff = one; term = t }
    let atom t = [atom1 t]

    let add x y = x @ y

    let mul1 mul_term x y =
      { coeff = C.mul x.coeff y.coeff; term = mul_term x.term y.term }

    let mul mul_term x y =
      Product.list2 (mul1 mul_term) x y

    let scale c x =
      List.map (fun t -> { t with coeff = C.mul c t.coeff }) x

    let sub x y = x @ (scale minus_one y)

    let mulx mul_term x y acc =
      Product.fold2 (fun x' y' ->
        add (scale (C.mul x'.coeff y'.coeff) (mul_term x'.term y'.term))) x y acc

    let mulc mul_term x y acc =
      Product.fold2 (fun x' y' ->
        C.add (C.mul (C.mul x'.coeff y'.coeff) (mul_term x'.term y'.term))) x y acc

    let map f sum =
      List.map (fun t -> { t with term = f t.term }) sum

    let eval to_coeff x =
      List.fold_right (fun t -> C.add (C.mul t.coeff (to_coeff t.term))) x C.null

    let to_string fmt sum =
      "(" ^ String.concat " + "
              (List.map (fun s ->
                C.to_string s.coeff ^ "*[" ^ fmt s.term ^ "]") sum) ^ ")"

    module M = Pmap.Tree

    let terms sum =
      List.map fst
        (M.elements (List.fold_left (fun acc s -> M.add compare s.term () acc) M.empty sum))

  end

module Make_Sum (C : Coeff) : Sum =
  struct

    module C = C

    let one = C.coeff 1 1

    type 'a summand = { coeff : C.t; term : 'a }

    module M = Pmap.Tree

    type 'a t = ('a, C.t) M.t

    let zero = M.empty

    let atom t = M.singleton t one

    let scale c x = M.map (C.mul c) x

    let insert1 binop t c sum =
      let c' = binop (try M.find compare t sum with Not_found -> C.null) c in
      if C.is_null c' then
        M.remove compare t sum
      else
        M.add compare t c' sum

    let add x y = M.fold (insert1 C.add) x y
    let sub x y = M.fold (insert1 C.sub) y x

    let fold2 f x y =
      M.fold (fun tx cx -> M.fold (f tx cx) y) x

    let mul mul_term x y =
      fold2 (fun tx cx ty cy -> insert1 C.add (mul_term tx ty) (C.mul cx cy))
        x y zero

    let mulx mul_term x y acc =
      fold2 (fun tx cx ty cy -> add (scale (C.mul cx cy) (mul_term tx ty))) x y acc

    let mulc mul_term x y acc =
      fold2 (fun tx cx ty cy -> C.add (C.mul (C.mul cx cy) (mul_term tx ty))) x y acc

    let map f sum = M.fold (fun t -> insert1 C.add (f t)) sum M.empty

    let eval to_coeff x =
      M.fold (fun t c -> C.add (C.mul c (to_coeff t))) x C.null

    let to_string fmt sum =
      "(" ^ String.concat " + "
              (M.fold (fun t c acc ->
                (C.to_string c ^ "*[" ^ fmt t ^ "]") :: acc) sum []) ^ ")"

    let terms sum =
      List.map fst (M.elements (M.fold (fun s _ -> M.add compare s ()) sum M.empty))

  end

(* \thocwmodulesubsection{Floating Point Arithmetic} *)

(* Floatig point arithmetic for a fixed~$N_C$ is of course the fasted
   approach and it appears to be reasonable accurate in most cases.  *)

module SUN_Float (NC : NC) : Coeff =
  struct
    type t = float
    let nc = float NC.nc
    let is_null x = (x = 0.0)
    let null = 0.0
    let unit = 1.0
    let atom p = nc ** (float p)
    let coeff n d = float n /. float d
    let mul = ( *. )
    let add = ( +. )
    let sub = ( -. )
    let neg c = -. c
    let to_float c = c
    let to_string = string_of_float
  end

(* \thocwmodulesubsection{Rational Arithmetic} *)

module SUN_Rational (R : Rational) (NC : NC) : Coeff =
  struct

    type t = R.t
    let null = R.null
    let unit = R.unit

    let is_null = R.is_null

    let nc = R.make NC.nc 1
    let one_over_nc = R.make 1 NC.nc

    let rec pow n p =
      if p < 0 then
        invalid_arg "pow"
      else if p = 0 then
        unit
      else
        R.mul n (pow n (pred p))

    let atom p =
      if p < 0 then
        pow one_over_nc (-p)
      else if p = 0 then
        unit
      else
        pow nc p

    let coeff = R.make

    let mul = R.mul
    let add = R.add
    let sub = R.sub
    let neg = R.neg

    let to_float = R.to_float
    let to_string = R.to_string

  end

(* \thocwmodulesubsection{Symbolic Arithmetic} *)

module SUN_Coeff (R : Rational) (NC : NC) : Coeff =
  struct

    module IMap = Map.Make (struct type t = int let compare = compare end)
    type t = R.t IMap.t

    let null = IMap.empty
    let unit = IMap.add 0 R.unit null

    let is_null c = (c = IMap.empty)

    let atom p = IMap.add p R.unit null
    let coeff n d = IMap.add 0 (R.make n d) null

    let neg = IMap.map R.neg

    let insert1 binop p r c =
      let r' = binop (try IMap.find p c with Not_found -> R.null) r in
      if R.is_null r' then
        IMap.remove p c
      else
        IMap.add p r' c

    let add x y = IMap.fold (insert1 R.add) x y
    let sub x y = IMap.fold (insert1 R.sub) y x

    let insert2 p1 r1 p2 r2 c =
      insert1 R.add (p1 + p2) (R.mul r1 r2) c

    let mul1 c2 p1 r1 c = IMap.fold (insert2 p1 r1) c2 c
    let mul c1 c2 = IMap.fold (mul1 c2) c1 IMap.empty

    let to_list c = IMap.fold (fun p r acc -> (p, r) :: acc) c []
    let to_string c =
      "(" ^ String.concat " + "
              (List.map
                 (fun (p, r) ->
                   if p = 0 then
                     R.to_string r
                   else
                     Printf.sprintf "%s*N^{%d}" (R.to_string r) p)
                 (List.sort
                    (fun (p1, _) (p2, _) -> compare p2 p1)
                    (to_list c))) ^ ")"

    let nc = float NC.nc
    let to_float c =
      IMap.fold (fun p r acc -> (R.to_float r) *. nc ** (float p) +. acc) c 0.0

  end

(* \thocwmodulesubsection{Naive Rational Arithmetic} *)

(* \begin{dubious}
     This \emph{is} dangerous and will overflow even for simple
     applications.  The production code will have to be linked to
     a library for large integer arithmetic.
   \end{dubious} *)

(* Anyway, here's Euclid's algorithm: *)
let rec gcd i1 i2 =
  if i2 = 0 then
    abs i1
  else
    gcd i2 (i1 mod i2)

let lcm i1 i2 = (i1 / gcd i1 i2) * i2

module Small_Rational : Rational =
  struct
    type t = int * int
    let is_null (n, _) = (n = 0)
    let null = (0, 1)
    let unit = (1, 1)
    let make n d =
      let c = gcd n d in
      (n / c, d / c)
    let mul (n1, d1) (n2, d2) = make (n1 * n2) (d1 * d2)
    let add (n1, d1) (n2, d2) = make (n1 * d2 + n2 * d1) (d1 * d2)
    let sub (n1, d1) (n2, d2) = make (n1 * d2 - n2 * d1) (d1 * d2)
    let neg (n, d) = (- n, d)
    let to_float (n, d) = float n /. float d
    let to_string (n, d) =
      if d = 1 then
        Printf.sprintf "%d" n
      else
        Printf.sprintf "(%d/%d)" n d
  end

(*i
(* \thocwmodulesubsection{Arbitrary Size Rational Arithmetic} *)

module Rational : Rational =
  struct
    type t = Num.num
    let null = Num.num_of_int 0
    let unit = Num.num_of_int 1
    let is_null = Num.eq_num null
    let make n d =
      Num.div_num (Num.num_of_int n) (Num.num_of_int d)
    let mul = Num.mult_num
    let add = Num.add_num
    let sub = Num.sub_num
    let neg = Num.minus_num
    let to_float = Num.float_of_num
    let to_string = Num.string_of_num
  end
i*)

module Flows = Make_Flows(Make_Sum(SUN_Coeff(Small_Rational)(NC3)))

(* slightly faster, but noticeably less precise:
   [module Flows = Make_Flows(Make_Sum(SUN_Float(NC3)))] *)

(*i
(* \thocwmodulesection{Traces} *)

module type Traces =
  sig
    type index
    type term
    type coeff
    val make : index list -> term
    val make_term : index list list -> term
    val mul : term -> term -> term
    val eval : term -> coeff
    val format : term -> string
    val to_string : coeff -> string
    val to_float : coeff -> float
  end

module Make_SUN_Traces (C : Coeff) : Traces with type index = int =
  struct

    type index = int
    type trace = index list
    type term = trace list
    type sum = (C.t * term) list
    type coeff = C.t

    let make tr = [tr]
    let make_term traces = traces
    let mul = (@)

    let trace_to_string tr =
      "tr(" ^ String.concat "," (List.map string_of_int tr) ^ ")"

    let term_to_string term =
      String.concat "*" (List.map trace_to_string term)

    let sum_to_string sum =
      String.concat " + "
        (List.map (fun (c, term) -> C.to_string c ^ "*" ^ term_to_string term) sum)

    let format = term_to_string

(* Check that each index appears exactly twice:  *)
    module IM = Map.Make (struct type t = index let compare = compare end)

    let count i map =
      IM.add i (try IM.find i map + 1 with Not_found -> 1) map

    let check term =
      IM.iter
        (fun i n ->
          if n <> 2 then
            invalid_arg (Printf.sprintf "index %d appears %d times!" i n))
        (List.fold_right (List.fold_right count) term IM.empty)

(* The required reduction formulae
   \begin{subequations}
   \begin{align}
     \mathrm{tr} (\mathbf{1}) &= N_C \\
     \mathrm{tr} (T^{a}) &= 0 \\
     \mathrm{tr} (T^{a} T^{a} M) &= C_F \mathrm{tr} (M) \\
     \mathrm{tr} (T^{a} M_1 T^{a} M_2) &=
         \frac{1}{2} \mathrm{tr} (M_1) \mathrm{tr} (M_2)
       - \frac{1}{2N_C} \mathrm{tr} (M_1 M_2) \\
     \mathrm{tr} (T^{a} M) \mathrm{tr} (M_1 T^{a} M_2) &=
         \frac{1}{2} \mathrm{tr} (M_1 M M_2)
       - \frac{1}{2N_C} \mathrm{tr} (M) \mathrm{tr} (M_1 M_2)
   \end{align}
   \end{subequations}
   are derived using the $\mathrm{SU}(N_C)$ completeness relation
   \begin{equation}
     T^{a}_{ij} T^{a}_{kl} = 
         \frac{1}{2} \delta_{il} \delta_{jk}
       - \frac{1}{2N_C} \delta_{ij} \delta_{kl}
   \end{equation}
   for the conventional normalization 
   \begin{equation}
     \textrm{tr}(T_{a}T_{b}) = \frac{1}{2}\delta_{ab}
   \end{equation}
   I.\,e.
   \begin{subequations}
   \begin{multline}
     \mathrm{tr} (T^{a} M_1 T^{a} M_2)
       = T^{a}_{ij} M_{1,jk} T^{a}_{kl} M_{2,li}
       =   \frac{1}{2} \delta_{jk} M_{1,jk} \delta_{il} M_{2,li} 
         - \frac{1}{2N_C} \delta_{ij} M_{1,jk} \delta_{kl} M_{2,li} \\
       =   \frac{1}{2} M_{1,jj} M_{2,ii} - \frac{1}{2N_C} M_{1,ik} M_{2,ki}
       =   \frac{1}{2} \mathrm{tr} (M_1) \mathrm{tr} (M_2)
         - \frac{1}{2N_C} \mathrm{tr} (M_1 M_2)
   \end{multline}
   and
   \begin{multline}
     \mathrm{tr} (T^{a} M_1) \mathrm{tr} (T^{a} M_2)
       = T^{a}_{ij} M_{1,ji} T^{a}_{kl} M_{2,lk}
       =   \frac{1}{2} \delta_{jk} M_{1,ji} \delta_{il} M_{2,lk} 
         - \frac{1}{2N_C} \delta_{ij} M_{1,ji} \delta_{kl} M_{2,lk}  \\
       =   \frac{1}{2} M_{1,jl} M_{2,lj} - \frac{1}{2N_C} M_{1,ii} M_{2,kk}
       =   \frac{1}{2} \mathrm{tr} (M_1 M_2)
         - \frac{1}{2N_C} \mathrm{tr} (M_1) \mathrm{tr} (M_2)
   \end{multline}
   \end{subequations}
   The generalization to arbitrary normalizations is straightforward.
   Similar formulae are available for~$\mathrm{SO}(N_C)$, but the
   completeness relations involve other invariant tensors
   besides~$\delta_{ij}$ for~$\mathrm{Sp}{(2N)}$ and the exceptional
   Lie algebras. *)

(* Construct the frequently used constants in the coefficient ring:
   \begin{equation*}
     \frac{1}{2},\; N_C,\; \frac{N_C}{2},\; -\frac{1}{2N_C},\;
     C_F = \frac{N_C^2-1}{2N_C} = \frac{N_C}{2} - \frac{1}{2N_C}
   \end{equation*} *)
    let half = C.coeff 1 2
    let nc = C.atom 1
    let nc_over_2 = C.mul half (C.atom 1)
    let minus_over_2nc = C.neg (C.mul half (C.atom (-1)))
    let cf = C.add nc_over_2 minus_over_2nc

(* \begin{dubious}
     The following type could be made redundant by making [eval_term] and
     [eval'] mutually recursive, but the resulting code is a bit more obscure.
   \end{dubious} *)
    type result =
      | Number of C.t
      | Sum of sum

(* Try to evaluate one term, replacing Casimirs by their eigenvalue in the
   fundamental representation.  If successive pairs do not match, first try
   to contract within the first trace. *)
    let rec eval_term coeff = function
      | [] -> Number coeff
      | [] :: traces -> (* $\mathrm{tr} (\mathbf{1}) = N_C$ *)
          eval_term (C.mul coeff nc) traces
      | [a] :: traces -> (* $\mathrm{tr} (T^{a}) = 0$ *)
          Number C.null
      | (a :: b :: m) :: traces ->
	  if a = b then
            (* $\mathrm{tr} (T^{a} T^{a} M) = C_F \mathrm{tr} (M)$ *)
	    eval_term (C.mul coeff cf) (m :: traces)
	  else
	    contract_inner coeff a [b] traces m

(* Try to contract a free index [a] with a matching index in the first trace.
   If this fails, consult the other traces. *)
    and contract_inner coeff a rev_m1 traces = function
      | [] -> contract_outer coeff a rev_m1 [] traces
      | b :: m2 ->
	  if a = b then
            match m2 with
            | [] -> (* $\mathrm{tr} (T^{a} M T^{a}) = C_F \mathrm{tr} (M)$ *)
	        eval_term (C.mul coeff cf) (List.rev rev_m1 :: traces)
            | _ -> (* $\mathrm{tr} (T^{a} M_1 T^{a} M_2) =
                      \frac{1}{2} \mathrm{tr} (M_1) \mathrm{tr} (M_2) -
                      \frac{1}{2N_C} \mathrm{tr} (M_1 M_2)$ *)
                let m1 = List.rev rev_m1
                and m1m2 = List.rev_append rev_m1 m2 in
                Sum [(C.mul half coeff, m1 :: m2 :: traces);
                     (C.mul minus_over_2nc coeff, m1m2 :: traces)]
          else
            contract_inner coeff a (b :: rev_m1) traces m2

(* Find a matching index in another trace. *)
    and contract_outer coeff a rev_m rev_traces traces =
      match traces with
      | [] -> invalid_arg "contract_outer"
      | trace :: traces ->
          contract_outer' coeff a rev_m rev_traces traces [] trace

    and contract_outer' coeff a rev_m rev_traces traces rev_m1 = function
      | [] -> contract_outer coeff a rev_m (List.rev rev_m1 :: rev_traces) traces
      | b :: m2 ->
	  if a = b then
            (* $\mathrm{tr} (T^{a} M) \mathrm{tr} (M_1 T^{a} M_2) =
                \frac{1}{2} \mathrm{tr} (M_1 M M_2) -
                \frac{1}{2N_C} \mathrm{tr} (M) \mathrm{tr} (M_1 M_2)$ *)
            let m = List.rev rev_m
            and m1m2 = List.rev_append rev_m1 m2
            and m1mm2 = List.rev_append rev_m1 (List.rev_append rev_m m2)
            and traces = List.rev_append rev_traces traces in
            Sum [(C.mul half coeff, m1mm2 :: traces);
                 (C.mul minus_over_2nc coeff, m :: m1m2 :: traces)]
          else
            contract_outer' coeff a rev_m rev_traces traces (b :: rev_m1) m2

    let rec eval' sum expr =
      match expr with
      | [] -> sum
      | (coeff, traces) :: expr ->
          match eval_term coeff traces with
          | Number term -> eval' (C.add sum term) expr
          | Sum terms -> eval' sum (List.rev_append terms expr)

    let eval traces =
      check traces;
      eval' C.null [(C.unit, traces)]

    let to_string = C.to_string
    let to_float = C.to_float

  end
i*)

(*i
module SU3_Traces_Rational =
  Make_SUN_Traces(SUN_Rational(Rational)(NC3))

module SU3_Traces_Symbolic =
  Make_SUN_Traces(SUN_Coeff(Rational)(NC3))
i*)

(*i
module SU3_Traces_Small_Rational =
  Make_SUN_Traces(SUN_Rational(Small_Rational)(NC3))

module SU3_Traces_Small_Symbolic =
  Make_SUN_Traces(SUN_Coeff(Small_Rational)(NC3))

module SU3_Traces_Float =
  Make_SUN_Traces(SUN_Float(NC3))

module SU3_Traces = SU3_Traces_Float
i*)

(*i
 *  Local Variables:
 *  mode:caml
 *  indent-tabs-mode:nil
 *  page-delimiter:"^(\\* .*\n"
 *  End:
i*)
