(* $Id: models.mli 232 2008-03-11 08:59:20Z reuter $ *)
(* Copyright (C) 2000-2004 by Thorsten Ohl <ohl@physik.uni-wuerzburg.de> et al.
   O'Mega is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by 
   the Free Software Foundation; either version 2, or (at your option)
   any later version.
   O'Mega is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.  *)  

(* \thocwmodulesection{Compilation} *)

module type Flavor =
  sig
    type f
    type c
    val compare : f -> f -> int
    val conjugate : f -> f
  end

module type Fusions =
  sig
    type t
    type f
    type c
    val fuse2 : t -> f -> f -> (f * c Coupling.t) list
    val fuse3 : t -> f -> f -> f -> (f * c Coupling.t) list
    val fuse : t -> f list -> (f * c Coupling.t) list
    val of_vertices :
        (((f * f * f) * c Coupling.vertex3 * c) list
           * ((f * f * f * f) * c Coupling.vertex4 * c) list
           * (f list * c Coupling.vertexn * c) list) -> t
  end

module Fusions : functor (F : Flavor) ->
  Fusions with type f = F.f and type c = F.c

(* \thocwmodulesection{Mutable Models} *)

module Mutable : functor (FGC : sig type f and g and c end) ->
  Model.Mutable with type flavor = FGC.f and type gauge = FGC.g
  and type constant = FGC.c

(* \thocwmodulesection{Hardcoded Models} *)

module Phi3 : Model.T
module Phi4 : Model.T
module QED : Model.T
module YM : Model.T
module type SM_flags =
  sig
    val triple_anom : bool
    val quartic_anom : bool
    val higgs_anom : bool
    val k_matrix : bool
    val ckm_present : bool
    val col_whiz : bool
  end
module SM_no_anomalous : SM_flags
module SM_anomalous : SM_flags
module SM_k_matrix : SM_flags
module SM_whizcol : SM_flags
module SM_whizcol_anom : SM_flags
module SM_no_anomalous_ckm : SM_flags
module SM_anomalous_ckm : SM_flags
module SM_whizcol_ckm : SM_flags
module SM_whizcol_anom_ckm : SM_flags
module SM_whizcol_k_matrix : SM_flags
module SM3 : functor (F : SM_flags) -> Model.Gauge
module SM : functor (F : SM_flags) -> Model.Gauge
module SM_Col : functor (F : SM_flags) -> Model.Gauge
module SM_Rxi : Model.T

module Groves : functor (M : Model.Gauge) -> Model.Gauge
module SM_clones : Model.Gauge
module SM3_clones : Model.Gauge

(*i
 *  Local Variables:
 *  mode:caml
 *  indent-tabs-mode:nil
 *  page-delimiter:"^(\\* .*\n"
 *  End:
i*)
