/* (* $Id: cascade_parser.mly,v 1.3 2004/06/22 09:30:18 ohl Exp $ *)
(* Copyright (C) 2004 by Thorsten Ohl <ohl@physik.uni-wuerzburg.de> et al.
   O'Mega is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by 
   the Free Software Foundation; either version 2, or (at your option)
   any later version.
   O'Mega is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.  *)  */
%{
open Cascade_syntax
let parse_error msg =
  raise (Syntax_Error (msg, symbol_start (), symbol_end ()))
%}

%token < string > FLAVOR
%token < int > INT
%token LPAREN RPAREN
%token AND OR PLUS COLON
%token ONSHELL OFFSHELL GAUSS
%token END
%left OR
%left AND
%left PLUS COLON

%start main
%type < (string, int list) Cascade_syntax.t > main

%%

main:
    END                             { mk_true () }
  | cascades END                    { $1 }
;

cascades:
    cascade                         { $1 }
  | cascade RPAREN                  { parse_error "cascade_parser: unexpected `)'!" }
  | cascade PLUS                    { parse_error "cascade_parser: unexpected `+'!" }
  | cascade ONSHELL                 { parse_error "cascade_parser: unexpected `='!" }
  | cascade OFFSHELL                { parse_error "cascade_parser: unexpected `~'!" } 
  | cascade GAUSS                   { parse_error "cascade_parser: unexpected `#'!" } 
  | LPAREN cascades RPAREN          { $2 }
  | cascades AND cascades           { mk_and $1 $3 }
  | cascades OR cascades            { mk_or $1 $3 }
;

cascade:
    momentum_list                   { mk_any_flavor $1 }
  | momentum_list ONSHELL FLAVOR    { mk_on_shell $3 $1 }
  | momentum_list OFFSHELL FLAVOR   { mk_off_shell [$3] $1 }
  | momentum_list GAUSS FLAVOR      { mk_gauss $3 $1 }
;

momentum_list:
  | momentum                        { [$1] }
  | momentum_list PLUS momentum     { $3 :: $1 }
;

momentum:
    INT                             { $1 }
;
