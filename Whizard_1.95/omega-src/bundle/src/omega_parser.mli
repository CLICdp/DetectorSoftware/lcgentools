type token =
  | STRING of ( string )
  | OPEN
  | CLOSE
  | OUTPUT
  | WHIZARD
  | PROCESS
  | TARGET
  | MODEL
  | END

val file :
  (Lexing.lexbuf  -> token) -> Lexing.lexbuf ->  Omega_syntax.command list 
