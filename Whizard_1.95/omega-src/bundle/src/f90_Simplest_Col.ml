(* $Id: f90_Simplest_Col.ml 539 2009-04-09 15:32:26Z jr_reuter $ *)
(* Copyright (C) 2000-2004 by Thorsten Ohl <ohl@physik.uni-wuerzburg.de>
   O'Mega is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by 
   the Free Software Foundation; either version 2, or (at your option)
   any later version.
   O'Mega is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.  *)  

(*i Old explicitly colorized version. 
module O' = Omega.Make(Fusion.Mixed23)(Targets.Fortran_Majorana)
    (Models5.Simplest_Col(Models5.BSM_bsm_Col))
i*)

module O = Omega.Make(Fusion.Mixed23)(Targets.Fortran)
    (Colorize.It (struct let max_num = 6 end) 
    (Models5.Simplest(Models5.BSM_bsm_Col)))
let _ = O.main ()

(*i
 *  Local Variables:
 *  mode:caml
 *  indent-tabs-mode:nil
 *  page-delimiter:"^(\\* .*\n"
 *  End:
i*)
