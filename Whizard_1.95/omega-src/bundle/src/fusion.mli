(* $Id: fusion.mli 68 2007-11-22 11:11:19Z ohl $ *)
(* Copyright (C) 2000-2004 by Thorsten Ohl <ohl@physik.uni-wuerzburg.de> et al.
   O'Mega is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by 
   the Free Software Foundation; either version 2, or (at your option)
   any later version.
   O'Mega is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.  *)  

module type T =
  sig

(* Wavefunctions are an abstract data type, containing a momentum~[p]
   and additional quantum numbers, collected in~[flavor]. *)
    type wf

(* Obviously, [flavor] is not restricted to the physical notion of
   flavor, but can carry spin, color, etc. *)
    type flavor
    val flavor : wf -> flavor

(* Momenta are represented by an abstract datatype (defined
   in~[Momentum]) that is optimized for performance.  They can be
   accessed either abstractly or as lists of indices of the external
   momenta.  These indices are assigned sequentially by [amplitude] below. *)
    type p
    val momentum : wf -> p
    val momentum_list : wf -> int list

(* At tree level, the wave functions are uniquely specified by [flavor]
   and momentum.  If loops are included, we need to distinguish among
   orders.  Also, if we build a result from an incomplete sum of diagrams,
   we need to add a distinguishing mark.  At the moment, we assume that a
   [string] that can be attached to the symbol suffices.  *)
    val wf_tag : wf -> string option

(* Coupling constants *)
    type constant

(* and right hand sides of assignments.  The latter are formed from a sign from
   Fermi statistics, a coupling (constand and Lorentz structure) and wave
   functions. *)
    type rhs
    type 'a children
    val sign : rhs -> int
    val coupling : rhs -> constant Coupling.t

    val coupling_tag : rhs -> string option

(* In renormalized perturbation theory, couplings come in different orders
   of the loop expansion.  Be prepared: [val order : rhs -> int] *)

(* \begin{dubious}
     This is here only for the benefit of [Target] and shall become
     [val children : rhs -> wf children] later \ldots
   \end{dubious} *)
    val children : rhs -> wf list

(* Fusions come in two types: fusions of wave functions to off-shell wave
   functions:
   \begin{equation*}
     \phi(p+q) = \phi(p)\phi(q)
   \end{equation*} *)
    type fusion
    val lhs : fusion -> wf
    val rhs : fusion -> rhs list

(* and products at the keystones:
   \begin{equation*}
     \phi(-p-q)\cdot\phi(p)\phi(q)
   \end{equation*} *)
    type braket
    val bra : braket -> wf
    val ket : braket -> rhs list

(* [amplitude goldstones incoming outgoing] calculates the
   amplitude for scattering of [incoming] to [outgoing].  If
   [goldstones] is true, also non-propagating off-shell Goldstone
   amplitudes are included to allow the checking of Slavnov-Taylor
   identities. *)
    type amplitude
    type selectors
    val amplitude : bool -> selectors ->
      flavor list -> flavor list -> amplitude

(* We should be precise regarding the semantics of the following functions, since
   modules implementating [Target] must not make any mistakes interpreting the
   return values.  Instead of calculating the amplitude
   \begin{subequations}
   \begin{equation}
   \label{eq:physical-amplitude}
     \Braket{f_3,p_3,f_4,p_4,\ldots|T|f_1,p_1,f_2,p_2}
   \end{equation}
   directly, O'Mega calculates the---equivalent, but more symmetrical---crossed
   amplitude 
   \begin{equation}
     \Braket{\bar f_1,-p_1,\bar f_2,-p_2,f_3,p_3,f_4,p_4,\ldots|T|0}
   \end{equation}
   Internally, all flavors are represented by their charge conjugates
   \begin{equation}
   \label{eq:internal-amplitude}
     A(f_1,-p_1,f_2,-p_2,\bar f_3,p_3,\bar f_4,p_4,\ldots)
   \end{equation}
   \end{subequations}
   The correspondence of vertex and term in the lagrangian
   \begin{equation}
     \parbox{26\unitlength}{%
       \fmfframe(5,3)(5,3){%
         \begin{fmfgraph*}(15,20)
           \fmfleft{v}
           \fmfright{p,A,e}
           \fmflabel{$\mathrm{e}^-$}{e}
           \fmflabel{$\mathrm{e}^+$}{p}
           \fmflabel{$\mathrm{A}$}{A}
           \fmf{fermion}{p,v,e}
           \fmf{photon}{A,v}
           \fmfdot{v}
         \end{fmfgraph*}}}: \bar\psi\fmslash{A}\psi
   \end{equation}
   suggests to denote the \emph{outgoing} particle by the flavor of the
   \emph{anti}particle and the \emph{outgoing} \emph{anti}particle by the
   flavor of the particle, since this choice allows to represent the vertex
   by a triple
   \begin{equation}
     \bar\psi\fmslash{A}\psi: (\mathrm{e}^+,A,\mathrm{e}^-)
   \end{equation}
   which is more intuitive than the alternative $(\mathrm{e}^-,A,\mathrm{e}^+)$.
   Also, when thinking in terms of building wavefunctions from the outside in,
   the outgoing \emph{antiparticle} is represented by a \emph{particle}
   propagator and vice versa\footnote{Even if this choice will appear slightly
   counter-intuitive on the [Target] side, one must keep in mind that much more
   people are expected to prepare [Model]s.}.
   [incoming] and [outgoing] are the physical flavors as
   in~(\ref{eq:physical-amplitude}) *)
    val incoming : amplitude -> flavor list
    val outgoing : amplitude -> flavor list

(* [externals] are flavors and momenta as in~(\ref{eq:internal-amplitude}) *)
    val externals : amplitude -> wf list

    val variables : amplitude -> wf list
    val fusions : amplitude -> fusion list
    val brakets : amplitude -> braket list
    val on_shell : amplitude -> (wf -> bool)
    val is_gauss : amplitude -> (wf -> bool)
    val constraints : amplitude -> string option
    val symmetry : amplitude -> int
    val color : amplitude -> (int * int) option
    val color_fac : amplitude -> string option
    val color_symm : amplitude -> int 
    val num_gl : amplitude -> int

    val allowed : amplitude -> bool

(* \thocwmodulesubsection{Diagnostics} *)

    val count_fusions : amplitude -> int
    val count_propagators : amplitude -> int
    val count_diagrams : amplitude -> int
    val count_color_flows : amplitude -> int

    type coupling
    val forest : wf -> amplitude -> ((wf * coupling option, wf) Tree.t) list
    val poles : amplitude -> wf list list
    val s_channel : amplitude -> wf list

    val tower_to_dot : out_channel -> amplitude -> unit
    val amplitude_to_dot : out_channel -> amplitude -> unit

    val rcs_list : RCS.t list
  end

(* There is more than one way to make fusions.  *)

module type Maker =
    functor (P : Momentum.T) -> functor (M : Model.T) ->
      T with type p = P.t and type flavor = M.flavor
      and type constant = M.constant
      and type selectors = Cascade.Make(M)(P).selectors

(* Straightforward Dirac fermions vs. slightly more complicated
   Majorana fermions: *)

module Binary : Maker
module Binary_Majorana : Maker

module Mixed23 : Maker
module Mixed23_Majorana : Maker

module Nary : functor (B : Tuple.Bound) -> Maker
module Nary_Majorana : functor (B : Tuple.Bound) -> Maker

(* We can also proceed \'a la~\cite{HELAC:2000}.  Empirically,
   this will use slightly~($O(10\%)$) fewer fusions than the
   symmetric factorization.  Our implementation uses
   significantly~($O(50\%)$) fewer fusions than reported
   by~\cite{HELAC:2000}.  Our pruning of the DAG might
   be responsible for this.  *)

module Helac : functor (B : Tuple.Bound) -> Maker
module Helac_Majorana : functor (B : Tuple.Bound) -> Maker

(* \thocwmodulesection{Multiple Amplitudes} *)

module type Multi =
  sig
    val options : Options.t
    type flavor
    type p
    type amplitude
    type selectors
    type amplitudes
    val no_tensor : bool ref
    val amplitudes : bool -> selectors ->
      flavor list list -> flavor list list -> amplitudes
    val incoming : amplitudes -> flavor list list
    val outgoing : amplitudes -> flavor list list
    val all : amplitudes -> amplitude list
    val allowed : amplitudes -> amplitude list
    val forbidden : amplitudes -> amplitude list
    val allowed_in_out : amplitudes -> flavor list list
    val allowed_in : amplitudes -> flavor list list
    val allowed_out : amplitudes -> flavor list list
    val forbidden_in_out : amplitudes -> flavor list list
    val forbidden_in : amplitudes -> flavor list list
    val forbidden_out : amplitudes -> flavor list list
    val is_allowed : amplitudes -> flavor list -> bool
    val multiplicities_in_out : amplitudes -> flavor list -> int
    val multiplicities_in : amplitudes -> flavor list -> int
    val multiplicities_out : amplitudes -> flavor list -> int
  end

module type MultiMaker = functor (F : T) ->
  Multi with type flavor = F.flavor and type p = F.p
  and type amplitude = F.amplitude
  and type selectors = F.selectors

module Multi : MultiMaker

(* \thocwmodulesection{Tags} *)

(* It appears that there are useful applications for tagging couplings
   and wave functions, e.\,g.~skeleton expansion and diagram selections.
   We can abstract this in a [Tags] signature: *)

module type Tags =
  sig
    type wf
    type coupling
    type 'a children
    val null_wf : wf
    val null_coupling : coupling
    val fuse : coupling -> wf children -> wf
    val wf_to_string : wf -> string option
    val coupling_to_string : coupling -> string option
  end

module type Tagger =
    functor (PT : Tuple.Poly) -> Tags with type 'a children = 'a PT.t

module type Tagged_Maker =
    functor (Tagger : Tagger) ->
      functor (P : Momentum.T) -> functor (M : Model.T) ->
        T with type p = P.t and type flavor = M.flavor
        and type constant = M.constant

module Tagged_Binary : Tagged_Maker

(*i
 *  Local Variables:
 *  mode:caml
 *  indent-tabs-mode:nil
 *  page-delimiter:"^(\\* .*\n"
 *  End:
i*)
