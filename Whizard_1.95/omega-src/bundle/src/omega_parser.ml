type token =
  | STRING of ( string )
  | OPEN
  | CLOSE
  | OUTPUT
  | WHIZARD
  | PROCESS
  | TARGET
  | MODEL
  | END

open Parsing;;
# 15 "omega_parser.mly"
# 15 "omega_parser.ml"
let yytransl_const = [|
  258 (* OPEN *);
  259 (* CLOSE *);
  260 (* OUTPUT *);
  261 (* WHIZARD *);
  262 (* PROCESS *);
  263 (* TARGET *);
  264 (* MODEL *);
  265 (* END *);
    0|]

let yytransl_block = [|
  257 (* STRING *);
    0|]

let yylhs = "\255\255\
\001\000\001\000\002\000\002\000\003\000\003\000\003\000\003\000\
\003\000\004\000\004\000\000\000"

let yylen = "\002\000\
\001\000\002\000\001\000\002\000\003\000\003\000\004\000\005\000\
\005\000\001\000\002\000\002\000"

let yydefred = "\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\001\000\
\012\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\002\000\004\000\000\000\000\000\000\000\006\000\005\000\000\000\
\000\000\000\000\007\000\009\000\008\000\011\000"

let yydgoto = "\002\000\
\009\000\010\000\011\000\027\000"

let yysindex = "\010\000\
\013\255\000\000\011\255\012\255\014\255\027\255\028\255\000\000\
\000\000\005\255\019\255\029\255\030\255\032\255\033\255\034\255\
\000\000\000\000\019\255\019\255\035\255\000\000\000\000\036\255\
\037\255\035\255\000\000\000\000\000\000\000\000"

let yyrindex = "\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\007\255\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\254\254\000\000\000\000\000\000\000\000"

let yygindex = "\000\000\
\000\000\245\255\000\000\004\000"

let yytablesize = 40
let yytable = "\018\000\
\010\000\010\000\010\000\010\000\010\000\010\000\010\000\024\000\
\025\000\003\000\001\000\012\000\013\000\017\000\014\000\003\000\
\003\000\004\000\005\000\006\000\007\000\008\000\003\000\004\000\
\005\000\006\000\007\000\015\000\016\000\030\000\019\000\020\000\
\021\000\022\000\023\000\026\000\000\000\000\000\028\000\029\000"

let yycheck = "\011\000\
\003\001\004\001\005\001\006\001\007\001\008\001\009\001\019\000\
\020\000\003\001\001\000\001\001\001\001\009\001\001\001\009\001\
\004\001\005\001\006\001\007\001\008\001\009\001\004\001\005\001\
\006\001\007\001\008\001\001\001\001\001\026\000\002\001\002\001\
\001\001\001\001\001\001\001\001\255\255\255\255\003\001\003\001"

let yynames_const = "\
  OPEN\000\
  CLOSE\000\
  OUTPUT\000\
  WHIZARD\000\
  PROCESS\000\
  TARGET\000\
  MODEL\000\
  END\000\
  "

let yynames_block = "\
  STRING\000\
  "

let yyact = [|
  (fun _ -> failwith "parser")
; (fun __caml_parser_env ->
    Obj.repr(
# 28 "omega_parser.mly"
                         ( [] )
# 99 "omega_parser.ml"
               :  Omega_syntax.command list ))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 1 : 'commands) in
    Obj.repr(
# 29 "omega_parser.mly"
                         ( _1 )
# 106 "omega_parser.ml"
               :  Omega_syntax.command list ))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : 'command) in
    Obj.repr(
# 32 "omega_parser.mly"
                         ( [ _1 ] )
# 113 "omega_parser.ml"
               : 'commands))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 1 : 'command) in
    let _2 = (Parsing.peek_val __caml_parser_env 0 : 'commands) in
    Obj.repr(
# 33 "omega_parser.mly"
                         ( _1 :: _2 )
# 121 "omega_parser.ml"
               : 'commands))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 1 :  string ) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 :  string ) in
    Obj.repr(
# 36 "omega_parser.mly"
                         ( Omega_syntax.Model (_2, _3) )
# 129 "omega_parser.ml"
               : 'command))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 1 :  string ) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 :  string ) in
    Obj.repr(
# 37 "omega_parser.mly"
                         ( Omega_syntax.Target (_2, _3) )
# 137 "omega_parser.ml"
               : 'command))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 2 :  string ) in
    let _3 = (Parsing.peek_val __caml_parser_env 1 :  string ) in
    let _4 = (Parsing.peek_val __caml_parser_env 0 : 'particles) in
    Obj.repr(
# 39 "omega_parser.mly"
                         ( Omega_syntax.Process ([_2; _3], _4) )
# 146 "omega_parser.ml"
               : 'command))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 3 :  string ) in
    let _4 = (Parsing.peek_val __caml_parser_env 1 : 'commands) in
    Obj.repr(
# 41 "omega_parser.mly"
                         ( Omega_syntax.Whizard (_2, _4) )
# 154 "omega_parser.ml"
               : 'command))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 3 :  string ) in
    let _4 = (Parsing.peek_val __caml_parser_env 1 : 'commands) in
    Obj.repr(
# 43 "omega_parser.mly"
                         ( Omega_syntax.Output (_2, _4) )
# 162 "omega_parser.ml"
               : 'command))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 :  string ) in
    Obj.repr(
# 46 "omega_parser.mly"
                         ( [ _1 ] )
# 169 "omega_parser.ml"
               : 'particles))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 1 :  string ) in
    let _2 = (Parsing.peek_val __caml_parser_env 0 : 'particles) in
    Obj.repr(
# 47 "omega_parser.mly"
                         ( _1 :: _2 )
# 177 "omega_parser.ml"
               : 'particles))
(* Entry file *)
; (fun __caml_parser_env -> raise (Parsing.YYexit (Parsing.peek_val __caml_parser_env 0)))
|]
let yytables =
  { Parsing.actions=yyact;
    Parsing.transl_const=yytransl_const;
    Parsing.transl_block=yytransl_block;
    Parsing.lhs=yylhs;
    Parsing.len=yylen;
    Parsing.defred=yydefred;
    Parsing.dgoto=yydgoto;
    Parsing.sindex=yysindex;
    Parsing.rindex=yyrindex;
    Parsing.gindex=yygindex;
    Parsing.tablesize=yytablesize;
    Parsing.table=yytable;
    Parsing.check=yycheck;
    Parsing.error_function=parse_error;
    Parsing.names_const=yynames_const;
    Parsing.names_block=yynames_block }
let file (lexfun : Lexing.lexbuf -> token) (lexbuf : Lexing.lexbuf) =
   (Parsing.yyparse yytables 1 lexfun lexbuf :  Omega_syntax.command list )
