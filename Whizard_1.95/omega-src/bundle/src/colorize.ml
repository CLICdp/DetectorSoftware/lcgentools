(* $Id: colorize.ml 222 2008-03-05 18:31:36Z reuter $ *)
(* Copyright (C) 2008- by Thorsten Ohl <ohl@physik.uni-wuerzburg.de> et al.
   O'Mega is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by 
   the Free Software Foundation; either version 2, or (at your option)
   any later version.
   O'Mega is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.  *)  

(* \thocwmodulesection{Colorizing a Monochrome Model} *)

module type Colorized =
    sig
      include Model.T
      type flavor_sans_color
      val amplitude : flavor_sans_color list -> flavor_sans_color list -> 
        (flavor list * flavor list) list
    end

module type Colorized_Gauge =
    sig
      include Model.Gauge
      type flavor_sans_color
      val amplitude : flavor_sans_color list -> flavor_sans_color list -> 
        (flavor list * flavor list) list
    end

module type Flows = 
    sig
      val max_num :int
    end

module It (F : Flows) (M : Model.T) = 
  struct

    open Coupling

    module C = Color

    let incomplete s =
      failwith ("Colorize.It()." ^ s ^ " not done yet!")

    let incomplete s =
      Printf.eprintf "WARNING: Colorize.It().%s not done yet!\n" s;
      []

    let su0 s =
      invalid_arg ("Colorize.It()." ^ s ^ ": found SU(0)!")

    let colored_vertex s =
      invalid_arg ("Colorize.It()." ^ s ^ ": colored vertex!")

    let color_flow_ambiguous s =
      invalid_arg ("Colorize.It()." ^ s ^ ": ambiguous color flow!")

    let color_flow_of_string s =
      let c = int_of_string s in
      if c < 1 then
        invalid_arg ("Colorize.It()." ^ s ^ ": color flow # < 1!")
      else if c > F.max_num then
        invalid_arg ("Colorize.It()." ^ s ^ ": color flow # too large")
      else
        c
        
    type flavor_sans_color = M.flavor

    type flavor =
      | White of M.flavor
      | CF_in of M.flavor * int
      | CF_out of M.flavor * int
      | CF_io of M.flavor * int * int
      | CF_aux of M.flavor

    let get_flavor = function
      | White f -> f
      | CF_in (f, _) -> f
      | CF_out (f, _) -> f
      | CF_io (f, _, _) -> f
      | CF_aux f -> f

    let pullback f arg1 =
      f (get_flavor arg1)

    type gauge = M.gauge
    type constant = M.constant
    let options = M.options

    let color = pullback M.color
    let pdg = pullback M.pdg
    let lorentz = pullback M.lorentz

(* For the propagator we cannot use pullback because we have to add the case
   of the color singlet propagator by hand. *)

    let colorize_propagator = function
      | Prop_Scalar -> Prop_Col_Scalar  (* Spin 0 octets. *)
      | Prop_Majorana -> Prop_Col_Majorana   (* Spin 1/2 octets. *)
      | Prop_Feynman -> Prop_Col_Feynman   (* Spin 1 states, massless. *)
      | Prop_Unitarity -> Prop_Col_Unitarity   (* Spin 1 states, massive. *)
      | _ -> failwith ("Colorize.It().colorize_propagator: not possible!")

    let propagator = function
      | CF_aux f -> colorize_propagator (M.propagator f)
      | White f -> M.propagator f
      | CF_in (f, _) -> M.propagator f
      | CF_out (f, _) -> M.propagator f
      | CF_io (f, _, _) -> M.propagator f

    let width = pullback M.width

    let goldstone = function
      | White f ->
          begin match M.goldstone f with
          | None -> None
          | Some (f', g) -> Some (White f', g)
          end
      | CF_in (f, c) ->
          begin match M.goldstone f with
          | None -> None
          | Some (f', g) -> Some (CF_in (f', c), g)
          end
      | CF_out (f, c) ->
          begin match M.goldstone f with
          | None -> None
          | Some (f', g) -> Some (CF_out (f', c), g)
          end
      | CF_io (f, c1, c2) ->
          begin match M.goldstone f with
          | None -> None
          | Some (f', g) -> Some (CF_io (f', c1, c2), g)
          end
      | CF_aux f ->
          begin match M.goldstone f with
          | None -> None
          | Some (f', g) -> Some (CF_aux f', g)
          end

    let conjugate = function
      | White f -> White (M.conjugate f)
      | CF_in (f, c) -> CF_out (M.conjugate f, c)
      | CF_out (f, c) -> CF_in (M.conjugate f, c)
      | CF_io (f, c1, c2) -> CF_io (M.conjugate f, c2, c1)
      | CF_aux f -> CF_aux (M.conjugate f)

    let fermion = pullback M.fermion

    let permute_triple (a, b, c) =
      List.map
        (function
          | [a'; b'; c'] -> (a', b', c')
          | _ -> failwith "Colorize.It().permute_triple: internal error")
        (Combinatorics.permute [a; b; c])

    let permute_quadruple (a, b, c, d) =
      List.map
        (function
          | [a'; b'; c'; d'] -> (a', b', c', d')
          | _ -> failwith "Colorize.It().permute_quadruple: internal error")
        (Combinatorics.permute [a; b; c; d])
      
    let max_degree = M.max_degree

    let color_flows =
      ThoList.range 1 F.max_num

    let color_flow_pairs =
      ThoList.flatmap
        (function
          | [c1; c2] -> [(c1, c2); (c2, c1)]
          | _ -> failwith "Colorize.It().color_flow_pairs: internal error")
        (Combinatorics.choose 2 color_flows)

    let color_flow_triples = 
      List.map
        (function
          | [c1; c2; c3] -> (c1, c2, c3)
          | _ -> failwith "Colorize.It().color_flow_triples: internal error")
        (Combinatorics.choose 3 color_flows)

    let color_flow_quadruples = 
      List.map
        (function
          | [c1; c2; c3; c4] -> (c1, c2, c3, c4)
          | _ -> failwith "Colorize.It().color_flow_quadruples: internal error")
        (Combinatorics.choose 4 color_flows)

    let colorize_flavor f =
      match M.color f with
      | C.Singlet -> [White f]
      | C.SUN nc ->
          if nc > 0 then
            List.map (fun c -> CF_in (f, c)) color_flows
          else if nc < 0 then
            List.map (fun c -> CF_out (f, c)) color_flows
          else
            su0 "colorize_flavor"
      | C.AdjSUN _ ->
          CF_aux f :: (List.map (fun (c1, c2) -> CF_io (f, c1, c2)) color_flow_pairs)
            
    let flavors () =
      ThoList.flatmap colorize_flavor (M.flavors ())

    let external_flavors () =
      List.map
        (fun (name, flist) -> (name, ThoList.flatmap colorize_flavor flist))
        (M.external_flavors ())

    let parameters = M.parameters

    module Fusion = Models.Fusions (struct
      type f = flavor
      type c = constant
      let compare = compare
      let conjugate = conjugate
    end)

(* \thocwmodulesubsection{Auxiliary functions} *)

    let mult_vertex3 fac = function
      | FBF (c,fb,coup,f) -> FBF ((fac*c),fb,coup,f) 
      | PBP (c,fb,coup,f) -> PBP ((fac*c),fb,coup,f) 
      | BBB (c,fb,coup,f) -> BBB ((fac*c),fb,coup,f) 
      | GBG (c,fb,coup,f) -> GBG ((fac*c),fb,coup,f) 
      | Gauge_Gauge_Gauge c -> Gauge_Gauge_Gauge (fac*c)
      | Aux_Gauge_Gauge c -> Aux_Gauge_Gauge (fac*c)
      | Scalar_Vector_Vector c -> Scalar_Vector_Vector (fac*c)
      | Aux_Vector_Vector c -> Aux_Vector_Vector (fac*c)
      | Aux_Scalar_Vector c -> Aux_Scalar_Vector (fac*c) 
      | Scalar_Scalar_Scalar c -> Scalar_Scalar_Scalar (fac*c)
      | Aux_Scalar_Scalar c -> Aux_Scalar_Scalar (fac*c) 
      | Vector_Scalar_Scalar c -> Vector_Scalar_Scalar (fac*c) 
      | Graviton_Scalar_Scalar c -> Graviton_Scalar_Scalar (fac*c)
      | Graviton_Vector_Vector c -> Graviton_Vector_Vector (fac*c)
      | Graviton_Spinor_Spinor c -> Graviton_Spinor_Spinor (fac*c) 
      | Dim4_Vector_Vector_Vector_T c -> Dim4_Vector_Vector_Vector_T (fac*c)
      | Dim4_Vector_Vector_Vector_L c -> Dim4_Vector_Vector_Vector_L (fac*c)
      | Dim4_Vector_Vector_Vector_T5 c -> Dim4_Vector_Vector_Vector_T5 (fac*c) 
      | Dim4_Vector_Vector_Vector_L5 c -> Dim4_Vector_Vector_Vector_L5 (fac*c)
      | Dim6_Gauge_Gauge_Gauge c -> Dim6_Gauge_Gauge_Gauge (fac*c)
      | Dim6_Gauge_Gauge_Gauge_5 c -> Dim6_Gauge_Gauge_Gauge_5 (fac*c)
      | Aux_DScalar_DScalar c -> Aux_DScalar_DScalar (fac*c)
      | Aux_Vector_DScalar c -> Aux_Vector_DScalar (fac*c)
      | Dim5_Scalar_Gauge2 c -> Dim5_Scalar_Gauge2 (fac*c) 
      | Dim5_Scalar_Gauge2_Skew c -> Dim5_Scalar_Gauge2_Skew (fac*c)
      | Dim5_Scalar_Vector_Vector_T c -> Dim5_Scalar_Vector_Vector_T (fac*c)
      | Dim6_Vector_Vector_Vector_T c -> Dim6_Vector_Vector_Vector_T (fac*c)
      | Tensor_2_Vector_Vector c -> Tensor_2_Vector_Vector (fac*c)
      | Dim5_Tensor_2_Vector_Vector_1 c -> Dim5_Tensor_2_Vector_Vector_1 (fac*c)
      | Dim5_Tensor_2_Vector_Vector_2 c -> Dim5_Tensor_2_Vector_Vector_2 (fac*c)
      | Dim7_Tensor_2_Vector_Vector_T c -> Dim7_Tensor_2_Vector_Vector_T (fac*c)

    let mult_vertex4 fac = function
      | Scalar4 c -> Scalar4 (fac*c) 
      | Scalar2_Vector2 c -> Scalar2_Vector2 (fac*c)
      | Vector4 ic4_list -> Vector4 (List.map (fun (c,icl) -> ((fac*c),icl)) ic4_list)
      | DScalar4 ic4_list -> DScalar4 (List.map (fun (c,icl) -> ((fac*c),icl)) ic4_list)
      | DScalar2_Vector2 ic4_list -> DScalar2_Vector2 (List.map (fun (c,icl) -> ((fac*c),icl)) ic4_list)
      | GBBG (c,fb,b2,f) -> GBBG ((fac*c),fb,b2,f)
      | Vector4_K_Matrix_tho (c,ch2_list) -> Vector4_K_Matrix_tho ((fac*c), ch2_list)
      | Vector4_K_Matrix_jr (c,ch2_list) -> Vector4_K_Matrix_jr ((fac*c), ch2_list)

(* \thocwmodulesubsection{Cubic Vertices} *)

    let vertices3, vertices4, verticesn = M.vertices ()

(* \textbf{Important}: In the following, we don't test that the
   $\mathrm{SU}(N)$ groups match and that $N>0$, since we can assume
   that [colorize_flavor] would have thrown an exception. *)

    let colorize_vertex3 ((f1, f2, f3), v, g) =
      match M.color f1, M.color f2, M.color f3 with

(* The trivial case. *)

      | C.Singlet, C.Singlet, C.Singlet ->
          [(White f1, White f2, White f3), v, g]

(* Coupling a quark, an anti-quark and a colorless particle: all
   particles are \emph{guaranteed} to be different and no nontrivial
   symmetry can arise. *)

      | C.SUN nc1, C.SUN nc2, C.Singlet ->
          if nc1 <> - nc2 then
            colored_vertex "colored_vertex3"
          else if nc1 > 0 then
            List.map 
              (fun c -> ((CF_in (f1, c), CF_out (f2, c), White f3), v, g)) 
              color_flows
          else
            List.map 
              (fun c -> ((CF_out (f1, c), CF_in (f2, c), White f3), v, g)) 
              color_flows

      | C.SUN nc1, C.Singlet, C.SUN nc3 ->
          if nc1 <> - nc3 then
            colored_vertex "colored_vertex3"
          else if nc1 > 0 then
            List.map 
              (fun c -> ((CF_in (f1, c), White f2, CF_out (f3, c)), v, g)) 
              color_flows
          else
            List.map 
              (fun c -> ((CF_out (f1, c), White f2, CF_in (f3, c)), v, g)) 
              color_flows
                
      | C.Singlet, C.SUN nc2, C.SUN nc3 ->
          if nc2 <> - nc3 then
            colored_vertex "colored_vertex3"
          else if nc2 > 0 then
            List.map 
              (fun c -> ((White f1, CF_in (f2, c), CF_out (f3, c)), v, g)) 
              color_flows
          else
            List.map 
              (fun c -> ((White f1, CF_out (f2, c), CF_in (f3, c)), v, g)) 
              color_flows

(* Coupling a quark, an anti-quark and a gluon: all particles are
   again \emph{guaranteed} to be different and no nontrivial symmetry
   can arise. *)

      | C.SUN nc1, C.SUN nc2, C.AdjSUN _ ->
          if nc1 <> - nc2 then
            colored_vertex "colored_vertex3"
          else if nc1 > 0 then
            List.map
              (fun (c1, c2) -> 
                ((CF_in (f1, c1), CF_out (f2, c2), CF_io (f3, c2, c1)), v, g))
              color_flow_pairs
            @ (List.map
                 (fun c -> ((CF_in (f1, c), CF_out (f2, c), CF_aux f3), v, g))
                 color_flows)
          else
            List.map
              (fun (c1, c2) -> 
                ((CF_out (f1, c2), CF_in (f2, c1), CF_io (f3, c2, c1)), v, g))
              color_flow_pairs
            @ (List.map
                 (fun c -> ((CF_out (f1, c), CF_in (f2, c), CF_aux f3), v, g))
                 color_flows)

      | C.SUN nc1, C.AdjSUN _, C.SUN nc3 ->
          if nc1 <> - nc3 then
            colored_vertex "colored_vertex3"
          else if nc1 > 0 then
            List.map
              (fun (c1, c3) -> 
                ((CF_in (f1, c1), CF_io (f2, c3, c1), CF_out (f3, c3)), v, g))
              color_flow_pairs
            @ (List.map
                 (fun c -> ((CF_in (f1, c), CF_aux f2, CF_out (f3, c)), v, g))
                 color_flows)
          else
            List.map
              (fun (c1, c3) -> 
                ((CF_out (f1, c1), CF_io (f2, c1, c3), CF_in (f3, c3)), v, g))
              color_flow_pairs
            @ (List.map
                 (fun c -> ((CF_out (f1, c), CF_aux f2, CF_in (f3, c)), v, g))
                 color_flows)

      | C.AdjSUN _, C.SUN nc2, C.SUN nc3 ->
          if nc2 <> - nc3 then
            colored_vertex "colored_vertex3"
          else if nc2 > 0 then
            List.map
              (fun (c2, c3) -> 
                ((CF_io (f1, c3, c2), CF_in (f2, c2), CF_out (f3, c3)), v, g))
              color_flow_pairs
            @ (List.map
                 (fun c -> ((CF_aux f1, CF_in (f2, c), CF_out (f3, c)), v, g))
                 color_flows)
          else
            List.map
              (fun (c2, c3) -> 
                ((CF_io (f1, c2, c3), CF_out (f2, c2), CF_in (f3, c3)), v, g))
              color_flow_pairs
            @ (List.map
                 (fun c -> ((CF_aux f1, CF_out (f2, c), CF_in (f3, c)), v, g))
                 color_flows)

(* Coupling two gluons with a colorless particle: 

  To make the color algebra correct, we need to introduce the vertex with the two
  ghost gluons with a relative factor of -3.  *)


      | C.AdjSUN _, C.AdjSUN _, C.Singlet ->
          List.map (fun (c1, c2) -> ((CF_io (f1, c1, c2), CF_io (f2, c2, c1), White f3), v, g))
            color_flow_pairs
            @
           [((CF_aux f1, CF_aux f2, White f3),  (mult_vertex3 (-3) v), g)]

      | C.AdjSUN _, C.Singlet, C.AdjSUN _ ->
          List.map (fun (c1, c3) -> ((CF_io (f1, c1, c3), White f2, CF_io (f3, c3, c1)), v, g))
            color_flow_pairs
            @
           [((CF_aux f1, White f2, CF_aux f3),  (mult_vertex3 (-3) v), g)]

      | C.Singlet, C.AdjSUN _, C.AdjSUN _ ->
          List.map (fun (c2, c3) -> ((White f1, CF_io (f2, c2, c3), CF_io (f3, c3, c2)), v, g))
            color_flow_pairs
            @
           [((White f1, CF_aux f2, CF_aux f3),  (mult_vertex3 (-3) v), g)]

(* Coupling three gluons: *)

      | C.AdjSUN _, C.AdjSUN _, C.AdjSUN _ ->
          if f1 = f2 && f2 = f3 then
            ThoList.flatmap
              (fun (c1, c2, c3) ->
                [((CF_io (f1, c1, c3), CF_io (f2, c2, c1), CF_io (f3, c3, c2)), v, g);
                 ((CF_io (f1, c1, c2), CF_io (f2, c3, c1), CF_io (f3, c2, c3)), v, g)])
              color_flow_triples
          else
          ThoList.flatmap
            (fun (c1, c2, c3) ->
              (List.map (fun (c1', c2', c3') ->
                ((CF_io (f1, c1', c3'), CF_io (f2, c2', c1'), CF_io (f3, c3', c2')), v, g))
                (permute_triple (c1, c2, c3))))
            color_flow_triples

(* The rest is \emph{verboten}!

   JR mildly protests, because in principle a diquark coupling like in the baryon-number 
   violating superpotential of three (s)quarks might be allowed. Might be an interesting task 
   to work out the color flow combinations.

   Tho concedes that he forgot the special case of a $\mathrm{SU}(3)$-baryon-like coupling
   \ldots

 *)

      | C.SUN _, (C.Singlet|C.AdjSUN _), (C.Singlet|C.AdjSUN _)
      | (C.Singlet|C.AdjSUN _), C.SUN _, (C.Singlet|C.AdjSUN _)
      | (C.Singlet|C.AdjSUN _), (C.Singlet|C.AdjSUN _), C.SUN _ ->
          colored_vertex "colored_vertex3: single quark/anti-quark"

      | C.SUN _, C.SUN _, C.SUN _ ->
          colored_vertex "colored_vertex3: three quarks/anti-quarks"

      | C.Singlet, C.Singlet, C.AdjSUN _
      | C.Singlet, C.AdjSUN _, C.Singlet
      | C.AdjSUN _, C.Singlet, C.Singlet -> 
          colored_vertex "colored_vertex3: single gluon"

(* \thocwmodulesubsection{Quartic Vertices} *)

    let colorize_vertex4 ((f1, f2, f3, f4), v, g) =
      match M.color f1, M.color f2, M.color f3, M.color f4 with

(* The trivial case. *)

      | C.Singlet, C.Singlet, C.Singlet, C.Singlet ->
          [(White f1, White f2, White f3, White f4), v, g]

(* Coupling a quark, an anti-quark and two colorless particles: *)

      | C.SUN nc1, C.SUN nc2, C.Singlet, C.Singlet ->
          if nc1 <> - nc2 then
            colored_vertex "colorize_vertex4"
          else if nc1 > 0 then
            List.map
              (fun c -> ((CF_in (f1, c), CF_out (f2, c), White f3, White f4), v, g))
              color_flows
          else
            List.map
              (fun c -> ((CF_out (f1, c), CF_in (f2, c), White f3, White f4), v, g))
              color_flows

      | C.SUN nc1, C.Singlet, C.SUN nc3, C.Singlet ->
          if nc1 <> - nc3 then
            colored_vertex "colorize_vertex4"
          else if nc1 > 0 then
            List.map
              (fun c -> ((CF_in (f1, c), White f2, CF_out (f3, c), White f4), v, g))
              color_flows
          else
            List.map
              (fun c -> ((CF_out (f1, c), White f2, CF_in (f3, c), White f4), v, g))
              color_flows

      | C.SUN nc1, C.Singlet, C.Singlet, C.SUN nc4 ->
          if nc1 <> - nc4 then
            colored_vertex "colorize_vertex4"
          else if nc1 > 0 then
            List.map
              (fun c -> ((CF_in (f1, c), White f2, White f3, CF_out (f4, c)), v, g))
              color_flows
          else
            List.map
              (fun c -> ((CF_out (f1, c), White f2, White f3, CF_in (f4, c)), v, g))
              color_flows
              
      | C.Singlet, C.SUN nc2, C.SUN nc3, C.Singlet ->
          if nc2 <> - nc3 then
            colored_vertex "colorize_vertex4"
          else if nc2 > 0 then
            List.map
              (fun c -> ((White f1, CF_in (f2, c), CF_out (f3, c), White f4), v, g))
              color_flows
          else
            List.map
              (fun c -> ((White f1, CF_out (f2, c), CF_in (f4, c), White f4), v, g))
              color_flows

      | C.Singlet, C.SUN nc2, C.Singlet, C.SUN nc4 ->
          if nc2 <> - nc4 then
            colored_vertex "colorize_vertex4"
          else if nc2 > 0 then
            List.map
              (fun c -> ((White f1, CF_in (f2, c), White f3, CF_out (f4, c)), v, g))
              color_flows
          else
            List.map
              (fun c -> ((White f1, CF_out (f2, c), White f3, CF_in (f4, c)), v, g))
              color_flows

      | C.Singlet, C.Singlet, C.SUN nc3, C.SUN nc4 ->
          if nc3 <> - nc4 then
            colored_vertex "colorize_vertex4"
          else if nc3 > 0 then
            List.map
              (fun c -> ((White f1, White f2, CF_in (f3, c), CF_out (f4, c)), v, g))
              color_flows
          else
            List.map
              (fun c -> ((White f1, White f2, CF_out (f3, c), CF_in (f4, c)), v, g))
              color_flows

(* Coupling two quarks and two anti-quarks requires additional colorflow
   specification: better use an auxiliary field here!: *)

      | C.SUN _, C.SUN _, C.SUN _, C.SUN _ ->
          color_flow_ambiguous "colorize_vertex4: four quarks/anti-quarks"

(* Coupling a quark, an anti-quark, a gluon and a colorless particle:
   all particles are again \emph{guaranteed} to be different and no
   nontrivial symmetry can arise. *)

      | C.SUN nc1, C.SUN nc2, C.AdjSUN _, C.Singlet ->
          if nc1 <> - nc2 then
            colored_vertex "colorize_vertex4"
          else if nc1 > 0 then
            List.map
              (fun (c1, c2) -> ((CF_in (f1, c1), CF_out (f2, c2), CF_io (f3, c2, c1), White f4), v, g))
              color_flow_pairs
            @ (List.map
                 (fun c -> ((CF_in (f1, c), CF_out (f2, c), CF_aux f3, White f4), v, g))
                 color_flows)
          else
            List.map
              (fun (c1, c2) -> ((CF_out (f1, c2), CF_in (f2, c1), CF_io (f3, c2, c1), White f4), v, g))
              color_flow_pairs
            @ (List.map
                 (fun c -> ((CF_out (f1, c), CF_in (f2, c), CF_aux f3, White f4), v, g))
                 color_flows)
 
      | C.SUN nc1, C.SUN nc2, C.Singlet, C.AdjSUN _ ->
          if nc1 <> - nc2 then
            colored_vertex "colorize_vertex4"
          else if nc1 > 0 then
            List.map
              (fun (c1, c2) -> ((CF_in (f1, c1), CF_out (f2, c2), White f3, CF_io (f4, c2, c1)), v, g))
              color_flow_pairs
            @ (List.map
                 (fun c -> ((CF_in (f1, c), CF_out (f2, c), White f3, CF_aux f4), v, g))
                 color_flows)
          else
            List.map
              (fun (c1, c2) -> ((CF_out (f1, c2), CF_in (f2, c1), White f3, CF_io (f4, c2, c1)), v, g))
              color_flow_pairs
            @ (List.map
                 (fun c -> ((CF_out (f1, c), CF_in (f2, c), White f3, CF_aux f4), v, g))
                 color_flows)

      | C.SUN nc1, C.AdjSUN _, C.SUN nc3, C.Singlet ->
          if nc1 <> - nc3 then
            colored_vertex "colorize_vertex4"
          else if nc1 > 0 then
            List.map
              (fun (c1, c3) -> ((CF_in (f1, c1), CF_io (f2, c3, c1), CF_out (f3, c3), White f4), v, g))
              color_flow_pairs
            @ (List.map
                 (fun c -> ((CF_in (f1, c), CF_aux f2, CF_out (f3, c), White f4), v, g))
                 color_flows)
          else
            List.map
              (fun (c1, c3) -> ((CF_out (f1, c3), CF_io (f2, c3, c1), CF_in (f3, c1), White f4), v, g))
              color_flow_pairs
            @ (List.map
                 (fun c -> ((CF_out (f1, c), CF_aux f2, CF_in (f3, c), White f4), v, g))
                 color_flows)

      | C.SUN nc1, C.Singlet, C.SUN nc3, C.AdjSUN _ ->
          if nc1 <> - nc3 then
            colored_vertex "colorize_vertex4"
          else if nc1 > 0 then
            List.map
              (fun (c1, c3) -> ((CF_in (f1, c1), White f2, CF_out (f3, c3), CF_io (f4, c3, c1)), v, g))
              color_flow_pairs
            @ (List.map
                 (fun c -> ((CF_in (f1, c), White f2, CF_out (f3, c), CF_aux f4), v, g))
                 color_flows)
          else
            List.map
              (fun (c1, c3) -> ((CF_out (f1, c3), White f2, CF_in (f3, c1), CF_io (f4, c3, c1)), v, g))
              color_flow_pairs
            @ (List.map
                 (fun c -> ((CF_out (f1, c), White f2, CF_in (f3, c), CF_aux f4), v, g))
                 color_flows)

      | C.SUN nc1, C.AdjSUN _, C.Singlet, C.SUN nc4 ->
          if nc1 <> - nc4 then
            colored_vertex "colorize_vertex4"
          else if nc1 > 0 then
            List.map
              (fun (c1, c4) -> ((CF_in (f1, c1), CF_io (f2, c4, c1), White f3, CF_out (f4, c4)), v, g))
              color_flow_pairs
            @ (List.map
                 (fun c -> ((CF_in (f1, c), CF_aux f2, White f3, CF_out (f4, c)), v, g))
                 color_flows)
          else
            List.map
              (fun (c1, c4) -> ((CF_out (f1, c4), CF_io (f2, c4, c1), White f3, CF_in (f4, c1)), v, g))
              color_flow_pairs
            @ (List.map
                 (fun c -> ((CF_out (f1, c), CF_aux f2, White f3, CF_in (f4, c)), v, g))
                 color_flows)

      | C.SUN nc1, C.Singlet, C.AdjSUN _, C.SUN nc4 ->
          if nc1 <> - nc4 then
            colored_vertex "colorize_vertex4"
          else if nc1 > 0 then
            List.map
              (fun (c1, c4) -> ((CF_in (f1, c1), White f2, CF_io (f3, c4, c1), CF_out (f4, c4)), v, g))
              color_flow_pairs
            @ (List.map
                 (fun c -> ((CF_in (f1, c), White f2, CF_aux f3, CF_out (f4, c)), v, g))
                 color_flows)
          else
            List.map
              (fun (c1, c4) -> ((CF_out (f1, c4), White f2, CF_io (f3, c4, c1), CF_in (f4, c1)), v, g))
              color_flow_pairs
            @ (List.map
                 (fun c -> ((CF_out (f1, c), White f2, CF_aux f3, CF_in (f4, c)), v, g))
                 color_flows)

      | C.AdjSUN nc1, C.SUN nc2, C.SUN nc3, C.Singlet ->
          if nc2 <> - nc3 then
            colored_vertex "colorize_vertex4"
          else if nc2 > 0 then
            List.map
              (fun (c2, c3) -> ((CF_io (f1, c3, c2), CF_in (f2, c2), CF_out (f3, c3), White f4), v, g))
              color_flow_pairs
            @ (List.map
                 (fun c -> ((CF_aux f1, CF_in (f2, c), CF_out (f3, c), White f4), v, g))
                 color_flows)
          else
            List.map
              (fun (c2, c3) -> ((CF_io (f1, c3, c2), CF_out (f2, c3), CF_in (f3, c2), White f4), v, g))
              color_flow_pairs
            @ (List.map
                 (fun c -> ((CF_aux f1, CF_out (f2, c), CF_in (f3, c), White f4), v, g))
                 color_flows)

      | C.Singlet, C.SUN nc2, C.SUN nc3, C.AdjSUN _ ->
          if nc2 <> - nc3 then
            colored_vertex "colorize_vertex4"
          else if nc2 > 0 then
            List.map
              (fun (c2, c3) -> ((White f1, CF_in (f2, c2), CF_out (f3, c3), CF_io (f4, c3, c2)), v, g))
              color_flow_pairs
            @ (List.map
                 (fun c -> ((White f1, CF_in (f2, c), CF_out (f3, c), CF_aux f4), v, g))
                 color_flows)
          else
            List.map
              (fun (c2, c3) -> ((White f1, CF_out (f2, c3), CF_in (f3, c2), CF_io (f4, c3, c2)), v, g))
              color_flow_pairs
            @ (List.map
                 (fun c -> ((White f1, CF_out (f2, c), CF_in (f3, c), CF_aux f4), v, g))
                 color_flows)

      | C.AdjSUN _, C.SUN nc2, C.Singlet, C.SUN nc4 ->
          if nc2 <> - nc4 then
            colored_vertex "colorize_vertex4"
          else if nc2 > 0 then
            List.map
              (fun (c2, c4) -> ((CF_io (f1, c4, c2), CF_in (f2, c2), White f3, CF_out (f4, c4)), v, g))
              color_flow_pairs
            @ (List.map
                 (fun c -> ((CF_aux f1, CF_in (f2, c), White f3, CF_out (f4, c)), v, g))
                 color_flows)
          else
            List.map
              (fun (c2, c4) -> ((CF_io (f1, c4, c2), CF_out (f2, c4), White f3, CF_in (f4, c2)), v, g))
              color_flow_pairs
            @ (List.map
                 (fun c -> ((CF_aux f1, CF_out (f2, c), White f3, CF_in (f4, c)), v, g))
                 color_flows)

      | C.Singlet, C.SUN nc2, C.AdjSUN _, C.SUN nc4 ->
          if nc2 <> - nc4 then
            colored_vertex "colorize_vertex4"
          else if nc2 > 0 then
            List.map
              (fun (c2, c4) -> ((White f1, CF_in (f2, c2), CF_io (f3, c4, c2), CF_out (f4, c4)), v, g))
              color_flow_pairs
            @ (List.map
                 (fun c -> ((White f1, CF_in (f2, c), CF_aux f3, CF_out (f4, c)), v, g))
                 color_flows)
          else
            List.map
              (fun (c2, c4) -> ((White f1, CF_out (f2, c4), CF_io (f3, c4, c2), CF_in (f4, c2)), v, g))
              color_flow_pairs
            @ (List.map
                 (fun c -> ((White f1, CF_out (f2, c), CF_aux f3, CF_in (f4, c)), v, g))
                 color_flows)

      | C.AdjSUN _, C.Singlet, C.SUN nc3, C.SUN nc4 ->
          if nc3 <> - nc4 then
            colored_vertex "colorize_vertex4"
          else if nc3 > 0 then
            List.map
              (fun (c3, c4) -> ((CF_io (f1, c4, c3), White f2, CF_in (f3, c3), CF_out (f4, c4)), v, g))
              color_flow_pairs
            @ (List.map
                 (fun c -> ((CF_aux f1, White f2, CF_in (f3, c), CF_out (f4, c)), v, g))
                 color_flows)
          else
            List.map
              (fun (c3, c4) -> ((CF_io (f1, c4, c3), White f2, CF_out (f2, c4), CF_in (f4, c3)), v, g))
              color_flow_pairs
            @ (List.map
                 (fun c -> ((CF_aux f1, White f2, CF_out (f2, c), CF_in (f4, c)), v, g))
                 color_flows)

      | C.Singlet, C.AdjSUN _, C.SUN nc3, C.SUN nc4 ->
          if nc3 <> - nc4 then
            colored_vertex "colorize_vertex4"
          else if nc3 > 0 then
            List.map
              (fun (c3, c4) -> ((White f1, CF_io (f2, c4, c3), CF_in (f3, c3), CF_out (f4, c4)), v, g))
              color_flow_pairs
            @ (List.map
                 (fun c -> ((White f1, CF_aux f2, CF_in (f3, c), CF_out (f4, c)), v, g))
                 color_flows)
          else
            List.map
              (fun (c3, c4) -> ((White f1, CF_io (f2, c4, c3), CF_out (f2, c4), CF_in (f4, c3)), v, g))
              color_flow_pairs
            @ (List.map
                 (fun c -> ((White f1, CF_aux f2, CF_out (f2, c), CF_in (f4, c)), v, g))
                 color_flows)

(* Coupling a quark, an anti-quark and two gluons. For two different octets (is there a 
   realistic situation for this we need twelve combinations as well as two combinations 
   for the rest. *)

      | C.SUN nc1, C.SUN nc2, C.AdjSUN _, C.AdjSUN _ ->
          if (compare f3 f4) <> 0 then
            incomplete "colorize_vertex4" 
          else
            if nc1 <> - nc2 then
              colored_vertex "colorize_vertex4"
            else if nc1 > 0 then
              ThoList.flatmap 
                  (fun (c1,c2,c3) -> List.map (fun (c1', c2', c3') -> 
                   ((CF_in (f1, c1'), CF_out (f2, c2'), CF_io (f3, c2', c3'), CF_io (f4, c3', c1')), v, g))
                   (permute_triple (c1,c2,c3))) color_flow_triples
                  @ List.map (fun (c1, c2) -> 
                    ((CF_in (f1, c1), CF_out (f2, c2), CF_io (f3, c2, c1), CF_aux f4), 
                       (mult_vertex4 2 v), g)) color_flow_pairs
                  @ (List.map (fun c -> 
                    ((CF_in (f1, c), CF_out (f2, c), CF_aux f3, CF_aux f4), (mult_vertex4 2 v), g))
                       color_flows)
            else
              ThoList.flatmap 
                  (fun (c1,c2,c3) -> List.map (fun (c1', c2', c3') -> 
                   ((CF_out (f1, c2'), CF_in (f2, c1'), CF_io (f3, c2', c3'), CF_io (f4, c3', c1')), v, g))
                   (permute_triple (c1,c2,c3))) color_flow_triples
                  @ (List.map (fun (c1, c2) -> 
                    ((CF_out (f1, c2), CF_in (f2, c1), CF_io (f3, c2, c1), CF_aux f4), 
                       (mult_vertex4 2 v), g)) color_flow_pairs)
                  @ (List.map (fun c -> 
                    ((CF_out (f1, c), CF_in (f2, c), CF_aux f3, CF_aux f4), (mult_vertex4 2 v), g))
                       color_flows)

      | C.SUN nc1, C.AdjSUN _, C.SUN nc3, C.AdjSUN _ ->
          if (compare f2 f4) <> 0 then
            incomplete "colorize_vertex4" 
          else
            if nc1 <> - nc3 then
              colored_vertex "colorize_vertex4"
            else if nc1 > 0 then
              ThoList.flatmap 
                  (fun (c1,c2,c3) -> List.map (fun (c1', c2', c3') -> 
                   ((CF_in (f1, c1'), CF_io (f2, c2', c3'), CF_out (f3, c2'), CF_io (f4, c3', c1')), v, g))
                   (permute_triple (c1,c2,c3))) color_flow_triples
                  @ List.map (fun (c1, c2) -> 
                    ((CF_in (f1, c1), CF_io (f2, c2, c1), CF_out (f3, c2), CF_aux f4), 
                       (mult_vertex4 2 v), g)) color_flow_pairs
                  @ (List.map (fun c -> 
                    ((CF_in (f1, c), CF_aux f2, CF_out (f3, c), CF_aux f4), (mult_vertex4 2 v), g))
                       color_flows)
            else
              ThoList.flatmap 
                  (fun (c1,c2,c3) -> List.map (fun (c1', c2', c3') -> 
                   ((CF_out (f1, c2'), CF_io (f2, c2', c3'), CF_in (f3, c1'), CF_io (f4, c3', c1')), v, g))
                   (permute_triple (c1,c2,c3))) color_flow_triples
                  @ (List.map (fun (c1, c2) -> 
                    ((CF_out (f1, c2), CF_io (f2, c2, c1), CF_in (f3, c1), CF_aux f4), 
                       (mult_vertex4 2 v), g)) color_flow_pairs)
                  @ (List.map (fun c -> 
                    ((CF_out (f1, c), CF_aux f2, CF_in (f3, c), CF_aux f4), (mult_vertex4 2 v), g))
                       color_flows)

      | C.SUN nc1, C.AdjSUN _, C.AdjSUN _, C.SUN nc4 ->
          if (compare f2 f3) <> 0 then
            incomplete "colorize_vertex4" 
          else
            if nc1 <> - nc4 then
              colored_vertex "colorize_vertex4"
            else if nc1 > 0 then
              ThoList.flatmap 
                  (fun (c1,c2,c3) -> List.map (fun (c1', c2', c3') -> 
                   ((CF_in (f1, c1'), CF_io (f2, c2', c3'), CF_io (f3, c3', c1'), CF_out (f4, c2')), v, g))
                   (permute_triple (c1,c2,c3))) color_flow_triples
                  @ List.map (fun (c1, c2) -> 
                    ((CF_in (f1, c1), CF_io (f2, c2, c1), CF_aux f3, CF_out (f4, c2)), 
                       (mult_vertex4 2 v), g)) color_flow_pairs
                  @ (List.map (fun c -> 
                    ((CF_in (f1, c), CF_aux f2, CF_aux f3, CF_out (f4, c)), (mult_vertex4 2 v), g))
                       color_flows)
            else
              ThoList.flatmap 
                  (fun (c1,c2,c3) -> List.map (fun (c1', c2', c3') -> 
                   ((CF_out (f1, c2'), CF_io (f2, c2', c3'), CF_io (f3, c3', c1'), CF_in (f4, c1')), v, g))
                   (permute_triple (c1,c2,c3))) color_flow_triples
                  @ (List.map (fun (c1, c2) -> 
                    ((CF_out (f1, c2), CF_io (f2, c2, c1), CF_aux f3, CF_in (f4, c1)), 
                       (mult_vertex4 2 v), g)) color_flow_pairs)
                  @ (List.map (fun c -> 
                    ((CF_out (f1, c), CF_aux f2, CF_aux f3, CF_in (f4, c)), (mult_vertex4 2 v), g))
                       color_flows)

      | C.AdjSUN _, C.SUN nc2, C.SUN nc3, C.AdjSUN _ ->
          if (compare f1 f4) <> 0 then
            incomplete "colorize_vertex4" 
          else
            if nc2 <> - nc3 then
              colored_vertex "colorize_vertex4"
            else if nc2 > 0 then
              ThoList.flatmap 
                  (fun (c1,c2,c3) -> List.map (fun (c1', c2', c3') -> 
                   ((CF_io (f1, c2', c3'), CF_in (f2, c1'), CF_out (f3, c2'), CF_io (f4, c3', c1')), v, g))
                   (permute_triple (c1,c2,c3))) color_flow_triples
                  @ List.map (fun (c1, c2) -> 
                    ((CF_io (f1, c2, c1), CF_in (f2, c1), CF_out (f3, c2), CF_aux f4), 
                       (mult_vertex4 2 v), g)) color_flow_pairs
                  @ (List.map (fun c -> 
                    ((CF_aux f1, CF_in (f2, c), CF_out (f3, c), CF_aux f4), (mult_vertex4 2 v), g))
                       color_flows)
            else
              ThoList.flatmap 
                  (fun (c1,c2,c3) -> List.map (fun (c1', c2', c3') -> 
                   ((CF_io (f1, c2', c3'), CF_out (f2, c2'), CF_in (f3, c1'), CF_io (f4, c3', c1')), v, g))
                   (permute_triple (c1,c2,c3))) color_flow_triples
                  @ (List.map (fun (c1, c2) -> 
                    ((CF_io (f1, c2, c1), CF_out (f2, c2), CF_in (f3, c1), CF_aux f4), 
                       (mult_vertex4 2 v), g)) color_flow_pairs)
                  @ (List.map (fun c -> 
                    ((CF_aux f1, CF_out (f2, c), CF_in (f3, c), CF_aux f4), (mult_vertex4 2 v), g))
                       color_flows)

      | C.AdjSUN _, C.SUN nc2, C.AdjSUN _, C.SUN nc4 ->
          if (compare f1 f3) <> 0 then
            incomplete "colorize_vertex4" 
          else
          if nc2 <> - nc4 then
            colored_vertex "colorize_vertex4"
            else if nc2 > 0 then
              ThoList.flatmap 
                  (fun (c1,c2,c3) -> List.map (fun (c1', c2', c3') -> 
                   ((CF_io (f1, c2', c3'), CF_in (f2, c1'), CF_io (f3, c3', c1'), CF_out (f4, c2')), v, g))
                   (permute_triple (c1,c2,c3))) color_flow_triples
                  @ List.map (fun (c1, c2) -> 
                    ((CF_io (f1, c2, c1), CF_in (f2, c1), CF_aux f3, CF_out (f4, c2)), 
                       (mult_vertex4 2 v), g)) color_flow_pairs
                  @ (List.map (fun c -> 
                    ((CF_aux f1, CF_in (f2, c), CF_aux f3, CF_out (f4, c)), (mult_vertex4 2 v), g))
                       color_flows)
            else
              ThoList.flatmap 
                  (fun (c1,c2,c3) -> List.map (fun (c1', c2', c3') -> 
                   ((CF_io (f1, c2', c3'), CF_out (f2, c2'), CF_io (f3, c3', c1'), CF_in (f4, c1')), v, g))
                   (permute_triple (c1,c2,c3))) color_flow_triples
                  @ (List.map (fun (c1, c2) -> 
                    ((CF_io (f1, c2, c1), CF_out (f2, c2), CF_aux f3, CF_in (f4, c1)), 
                       (mult_vertex4 2 v), g)) color_flow_pairs)
                  @ (List.map (fun c -> 
                    ((CF_aux f1, CF_out (f2, c), CF_aux f3, CF_in (f4, c)), (mult_vertex4 2 v), g))
                       color_flows)

      | C.AdjSUN _, C.AdjSUN _, C.SUN nc3, C.SUN nc4 ->
          if (compare f1 f2) <> 0 then
            incomplete "colorize_vertex4" 
          else
            if nc3 <> - nc4 then
              colored_vertex "colorize_vertex4"
            else if nc3 > 0 then
              ThoList.flatmap 
                  (fun (c1,c2,c3) -> List.map (fun (c1', c2', c3') -> 
                   ((CF_io (f1, c2', c3'), CF_io (f2, c3', c1'), CF_in (f3, c1'), CF_out (f4, c2')), v, g))
                   (permute_triple (c1,c2,c3))) color_flow_triples
                  @ List.map (fun (c1, c2) -> 
                    ((CF_io (f1, c2, c1), CF_aux f2, CF_in (f3, c1), CF_out (f4, c2)), 
                       (mult_vertex4 2 v), g)) color_flow_pairs
                  @ (List.map (fun c -> 
                    ((CF_aux f1, CF_aux f2, CF_in (f3, c), CF_out (f4, c)), (mult_vertex4 2 v), g))
                       color_flows)
            else
              ThoList.flatmap 
                  (fun (c1,c2,c3) -> List.map (fun (c1', c2', c3') -> 
                   ((CF_io (f1, c2', c3'), CF_io (f2, c3', c1'), CF_out (f3, c2'), CF_in (f4, c1')), v, g))
                   (permute_triple (c1,c2,c3))) color_flow_triples
                  @ (List.map (fun (c1, c2) -> 
                    ((CF_io (f1, c2, c1), CF_aux f2, CF_out (f3, c2), CF_in (f4, c1)), 
                       (mult_vertex4 2 v), g)) color_flow_pairs)
                  @ (List.map (fun c -> 
                    ((CF_aux f1, CF_aux f2, CF_out (f3, c), CF_in (f4, c)), (mult_vertex4 2 v), g))
                       color_flows)

(* Coupling two gluons and two colorless particles. *)

      | C.AdjSUN _, C.AdjSUN _, C.Singlet, C.Singlet ->
          List.map (fun (c1, c2) -> ((CF_io (f1, c1, c2), CF_io (f2, c2, c1), White f3, White f4), v, g))
            color_flow_pairs
            @
           [((CF_aux f1, CF_aux f2, White f3, White f4),  (mult_vertex4 (-3) v), g)]

      | C.AdjSUN _, C.Singlet, C.AdjSUN _, C.Singlet ->
          List.map (fun (c1, c3) -> ((CF_io (f1, c1, c3), White f2, CF_io (f3, c3, c1), White f4), v, g))
            color_flow_pairs
            @
           [((CF_aux f1, White f2, CF_aux f3, White f4),  (mult_vertex4 (-3) v), g)]

      | C.AdjSUN _, C.Singlet, C.Singlet, C.AdjSUN _ ->
          List.map (fun (c1, c4) -> ((CF_io (f1, c1, c4), White f2, White f3, CF_io (f4, c4, c1)), v, g))
            color_flow_pairs
            @
           [((CF_aux f1, White f2, White f3, CF_aux f4),  (mult_vertex4 (-3) v), g)]

      | C.Singlet, C.AdjSUN _, C.AdjSUN _, C.Singlet ->
          List.map (fun (c2, c3) -> ((White f1, CF_io (f2, c2, c3), CF_io (f3, c3, c2), White f4), v, g))
            color_flow_pairs
            @
           [((White f1, CF_aux f2, CF_aux f3, White f4),  (mult_vertex4 (-3) v), g)]

      | C.Singlet, C.AdjSUN _, C.Singlet, C.AdjSUN _ ->
          List.map (fun (c2, c4) -> ((White f1, CF_io (f2, c2, c4), White f3, CF_io (f4, c4, c2)), v, g))
            color_flow_pairs
            @
           [((White f1, CF_aux f2, White f3, CF_aux f4),  (mult_vertex4 (-3) v), g)]

      | C.Singlet, C.Singlet, C.AdjSUN _, C.AdjSUN _ ->
          List.map (fun (c3, c4) -> ((White f1, White f2, CF_io (f3, c3, c4), CF_io (f4, c4, c3)), v, g))
            color_flow_pairs
            @
           [((White f1, White f2, CF_aux f3, CF_aux f4),  (mult_vertex4 (-3) v), g)]

(* Coupling tree gluons and a colorless particle. *)

      | C.AdjSUN _, C.AdjSUN _, C.AdjSUN _, C.Singlet ->
          ThoList.flatmap
            (fun (c1, c2, c3) ->
              [((CF_io (f1, c1, c3), CF_io (f2, c2, c1), CF_io (f3, c3, c2), White f4), v, g);
               ((CF_io (f1, c1, c2), CF_io (f2, c3, c1), CF_io (f3, c2, c3), White f4), v, g)])
            color_flow_triples

      | C.AdjSUN _, C.AdjSUN _, C.Singlet, C.AdjSUN _ ->
          ThoList.flatmap
            (fun (c1, c2, c4) ->
              [((CF_io (f1, c1, c4), CF_io (f2, c2, c1), White f3, CF_io (f4, c4, c2)), v, g);
               ((CF_io (f1, c1, c2), CF_io (f2, c4, c1), White f3, CF_io (f4, c2, c4)), v, g)])
            color_flow_triples

      | C.AdjSUN _, C.Singlet, C.AdjSUN _, C.AdjSUN _ ->
          ThoList.flatmap
            (fun (c1, c3, c4) ->
              [((CF_io (f1, c1, c4), White f2, CF_io (f3, c3, c1), CF_io (f4, c4, c3)), v, g);
               ((CF_io (f1, c1, c3), White f2, CF_io (f3, c4, c1), CF_io (f4, c3, c4)), v, g)])
            color_flow_triples

      | C.Singlet, C.AdjSUN _, C.AdjSUN _, C.AdjSUN _ ->
          ThoList.flatmap
            (fun (c2, c3, c4) ->
              [((White f1, CF_io (f2, c2, c4), CF_io (f3, c3, c2), CF_io (f4, c4, c3)), v, g);
               ((White f1, CF_io (f2, c2, c3), CF_io (f3, c4, c2), CF_io (f4, c3, c4)), v, g)])
            color_flow_triples

(* Coupling four gluons. Tho still has concerns about symmetry factors for KK gluons. It's the same 
   problem that already appears for the gluino-gluon-gluino vertex. *)

(*
   \begin{equation}
     \parbox{28mm}{\fmfframe(2,2)(2,1){\begin{fmfgraph*}(24,24)
       \fmfsurround{d1,e1,d2,e2,d3,e3,d4,e4}
       \fmf{gluon}{v,e1}
       \fmf{gluon}{v,e2}
       \fmf{gluon}{v,e3}
       \fmf{gluon}{v,e4}
       \fmflabel{1}{e1}
       \fmflabel{2}{e2}
       \fmflabel{3}{e3}
       \fmflabel{4}{e4}
       \fmfdot{v}
       \fmffreeze
       \fmf{warrow_right}{v,e1}
       \fmf{warrow_right}{v,e2}
       \fmf{warrow_right}{v,e3}
       \fmf{warrow_right}{v,e4}
     \end{fmfgraph*}}} \,= 
     \begin{split}
         \mbox{} - & \ii g^2 f_{a_1a_2b}f_{a_3a_4b}
                     (g_{\mu_1\mu_3} g_{\mu_4\mu_2} - g_{\mu_1\mu_4} g_{\mu_2\mu_3}) \\
         \mbox{} - & \ii g^2 f_{a_1a_3b}f_{a_4a_2b}
                     (g_{\mu_1\mu_4} g_{\mu_2\mu_3} - g_{\mu_1\mu_2} g_{\mu_3\mu_4}) \\
         \mbox{} - & \ii g^2 f_{a_1a_4b}f_{a_2a_3b}
                     (g_{\mu_1\mu_2} g_{\mu_3\mu_4} - g_{\mu_1\mu_3} g_{\mu_4\mu_2})
     \end{split}
   \end{equation}
 *)


      | C.AdjSUN _, C.AdjSUN _, C.AdjSUN _, C.AdjSUN _ ->
          if f1 = f2 && f2 = f3 && f3 = f4 then
          ThoList.flatmap
            (fun (c1, c2, c3, c4) ->
              let c1' = c1 in
              List.map (fun (c2', c3', c4') ->
                ((CF_io (f1, c1', c2'), CF_io (f2, c3', c1'),
                  CF_io (f3, c4', c3'), CF_io (f4, c2', c4')), v, g))
                (permute_triple (c2, c3, c4)))
            color_flow_quadruples
          else
          ThoList.flatmap
            (fun (c1, c2, c3, c4) ->
              List.map (fun (c1',c2', c3', c4') ->
                ((CF_io (f1, c1', c2'), CF_io (f2, c3', c1'),
                  CF_io (f3, c4', c3'), CF_io (f4, c2', c4')), v, g))
                (permute_quadruple (c1, c2, c3, c4)))
            color_flow_quadruples

(* The rest is \emph{verboten}! *)



      | C.SUN _, (C.Singlet|C.AdjSUN _), (C.Singlet|C.AdjSUN _), (C.Singlet|C.AdjSUN _)
      | (C.Singlet|C.AdjSUN _), C.SUN _, (C.Singlet|C.AdjSUN _), (C.Singlet|C.AdjSUN _)
      | (C.Singlet|C.AdjSUN _), (C.Singlet|C.AdjSUN _), C.SUN _, (C.Singlet|C.AdjSUN _)
      | (C.Singlet|C.AdjSUN _), (C.Singlet|C.AdjSUN _), (C.Singlet|C.AdjSUN _), C.SUN _ ->
          colored_vertex "colorize_vertex4: single quark/anti-quark"

      | C.SUN _, C.SUN _, C.SUN _, (C.Singlet|C.AdjSUN _)
      | C.SUN _, C.SUN _, (C.Singlet|C.AdjSUN _), C.SUN _
      | C.SUN _, (C.Singlet|C.AdjSUN _), C.SUN _, C.SUN _
      | (C.Singlet|C.AdjSUN _), C.SUN _, C.SUN _, C.SUN _ ->
          colored_vertex "colorize_vertex4: three quarks/anti-quarks"

      | C.Singlet, C.Singlet, C.Singlet, C.AdjSUN _
      | C.Singlet, C.Singlet, C.AdjSUN _, C.Singlet
      | C.Singlet, C.AdjSUN _, C.Singlet, C.Singlet
      | C.AdjSUN _, C.Singlet, C.Singlet, C.Singlet ->
          colored_vertex "colorize_vertex4: single gluon"

(* \thocwmodulesubsection{Higher Vertices} *)

    let colorize_vertexn (flist, v, g) =
      if List.for_all 
          (fun f -> match M.color f with C.Singlet -> true | _ -> false) 
          flist 
      then
        [(List.map (fun f -> White f) flist, v, g)]
      else
        incomplete "colorize_vertexn"

(* Discuss with {\em tho:} Is there possibly a functor that could take a vertex structure and 
   add a singlet ??? *)


    let vertices () =
      (ThoList.flatmap colorize_vertex3 vertices3,
       ThoList.flatmap colorize_vertex4 vertices4,
       ThoList.flatmap colorize_vertexn verticesn)

    let table = Fusion.of_vertices (vertices ()) 
    let fuse2 = Fusion.fuse2 table
    let fuse3 = Fusion.fuse3 table
    let fuse = Fusion.fuse table                                    
    let max_degree = M.max_degree

    let split_color_string s =
      try
        let i1 = String.index s '/' in
        let i2 = String.index_from s (succ i1) '/' in
        let sf = String.sub s 0 i1
        and sc1 = String.sub s (succ i1) (i2 - i1 - 1)
        and sc2 = String.sub s (succ i2) (String.length s - i2 - 1) in
        (sf, sc1, sc2)
      with
      | Not_found -> (s, "", "")

    let flavor_of_string s =
      try 
        let sf, sc1, sc2 = split_color_string s in
        let f = M.flavor_of_string sf in
        match M.color f with
        | C.Singlet -> White f
        | C.SUN nc ->
            if nc > 0 then
              CF_in (f, color_flow_of_string sc1)
            else
              CF_out (f, color_flow_of_string sc2)
        | C.AdjSUN _ ->
            begin match sc1, sc2 with
            | "", "" -> CF_aux f
            | _, _ -> CF_io (f, color_flow_of_string sc1, color_flow_of_string sc2)
            end
      with
      | Failure "int_of_string" ->
          invalid_arg "Colorize().flavor_of_string: expecting integer"

    let flavor_to_string = function
      | White f ->
          M.flavor_to_string f
      | CF_in (f, c) ->
          M.flavor_to_string f ^ "/" ^ string_of_int c ^ "/"
      | CF_out (f, c) ->
          M.flavor_to_string f ^ "//" ^ string_of_int c
      | CF_io (f, c1, c2) ->
          M.flavor_to_string f ^ "/" ^ string_of_int c1 ^ "/" ^ string_of_int c2
      | CF_aux f ->
          M.flavor_to_string f ^ "//"

    let flavor_to_TeX = function
      | White f ->
          M.flavor_to_TeX f
      | CF_in (f, c) ->
          "{" ^ M.flavor_to_TeX f ^ "}_c" ^ string_of_int c 
      | CF_out (f, c) ->
          "{" ^ M.flavor_to_TeX f ^ "}_a" ^ string_of_int c
      | CF_io (f, c1, c2) ->
          "{" ^ M.flavor_to_TeX f ^ "}_{c" ^ string_of_int c1 ^ "_a" ^ string_of_int c2 ^ "}"
      | CF_aux f ->
          "{" ^ M.flavor_to_TeX f ^ "}_0"

    let flavor_symbol = function
      | White f ->
          M.flavor_symbol f
      | CF_in (f, c) ->
          M.flavor_symbol f ^ "_" ^ string_of_int c ^ "_"
      | CF_out (f, c) ->
          M.flavor_symbol f ^ "__" ^ string_of_int c
      | CF_io (f, c1, c2) ->
          M.flavor_symbol f ^ "_" ^ string_of_int c1 ^ "_" ^ string_of_int c2
      | CF_aux f ->
          M.flavor_symbol f ^ "__"

(* Old WHIZARD version: 

    let rec split_color_string s =
      try
        let i = String.index s '/' in
        let sf = String.sub s 0 i
        and sc = String.sub s (succ i) ((String.length s) - 1) in
            [sf; sc]
      with
      | Not_found -> [s; ""]

    let flavor_of_string s =
      try 
        let sf, sc1, sc2 = split_color_string s in
        let f = M.flavor_of_string sf in
        (*i Printf.eprintf "%s -> %s/%s/%s\n" s (M.flavor_to_string f) sc1 sc2; i*)
        match M.color f with
        | C.Singlet -> White f
        | C.SUN nc ->
            if nc > 0 then
              CF_in (f, color_flow_of_string sc1)
            else
              CF_out (f, color_flow_of_string sc2)
        | C.AdjSUN _ ->
            begin match sc1, sc2 with
            | "", "" -> CF_aux f
            | _, _ -> CF_io (f, color_flow_of_string sc1, color_flow_of_string sc2)
            end
      with
      | Failure "int_of_string" ->
          invalid_arg "Colorize().flavor_of_string: expecting integer"

    let flavor_to_string = function
      | White f ->
          M.flavor_to_string f
      | CF_in (f, c) ->
          M.flavor_to_string f ^ "/" ^ string_of_int c
      | CF_out (f, c) ->
          M.flavor_to_string f ^ "/" ^ string_of_int c
      | CF_io (f, c1, c2) ->
          M.flavor_to_string f ^ "/" ^ string_of_int c1 ^ string_of_int c2
      | CF_aux f ->
          M.flavor_to_string f ^ "0"

    let flavor_to_TeX = function
      | White f ->
          M.flavor_to_TeX f
      | CF_in (f, c) ->
          "{" ^ M.flavor_to_TeX f ^ "}_c" ^ string_of_int c
      | CF_out (f, c) ->
          "{" ^ M.flavor_to_TeX f ^ "}_a" ^ string_of_int c
      | CF_io (f, c1, c2) ->
          "{" ^ M.flavor_to_TeX f ^ "}_c" ^ string_of_int c1 ^ string_of_int c2
      | CF_aux f ->
          "{" ^ M.flavor_to_TeX f ^ "}_0"

    let flavor_symbol = function
      | White f ->
          M.flavor_symbol f
      | CF_in (f, c) ->
          M.flavor_symbol f ^ "_" ^ string_of_int c
      | CF_out (f, c) ->
          M.flavor_symbol f ^ "_" ^ string_of_int c
      | CF_io (f, c1, c2) ->
          M.flavor_symbol f ^ "_" ^ string_of_int c1 ^ string_of_int c2
      | CF_aux f ->
          M.flavor_symbol f ^ "0"

*)

    let gauge_symbol = M.gauge_symbol

(* Masses and widths must not depend on the colors anyway! *)
    let mass_symbol = pullback M.mass_symbol
    let width_symbol = pullback M.width_symbol

    let constant_symbol = M.constant_symbol

    let colsymm = function
      | White f -> (0,false),(0,false)
      | CF_in (f,_) -> (M.pdg f,true),(0,false)
      | CF_out (f,_) -> (M.pdg f,true),(0,false)
      | CF_io (f,_,_) -> (M.pdg f,true),(0,false)
      | CF_aux f -> (M.pdg f, true),(M.pdg f,true)

(* \thocwmodulesubsection{Adding Color to External Particles} *)

    let count_color_strings f_list =
      let rec count_color_strings' n_in n_out n_glue = function
        | f :: rest ->
            begin match M.color f with
            | C.Singlet -> count_color_strings' n_in n_out n_glue rest
            | C.SUN nc ->
                if nc > 0 then
                  count_color_strings' (succ n_in) n_out n_glue rest
                else if nc < 0 then
                  count_color_strings' n_in (succ n_out) n_glue rest
                else
                  su0 "count_color_strings"
            | C.AdjSUN _ ->
                count_color_strings' (succ n_in) (succ n_out) (succ n_glue) rest
            end
        | [] -> (n_in, n_out, n_glue)
      in
      count_color_strings' 0 0 0 f_list

    let external_color_flows f_list =
      let n_in, n_out, n_glue = count_color_strings f_list in
      if n_in <> n_out then
        invalid_arg "external_color_flows: crossed amplitude not a singlet!"
      else
        let color_strings = ThoList.range 1 n_in in
        List.map
          (fun permutation -> (color_strings, permutation))
          (Combinatorics.permute color_strings)

    let rec colorize_crossed_amplitude1 f_list (ecf_in, ecf_out) =
      match f_list with
      | f :: rest ->
          begin match M.color f with
          | C.Singlet ->
              White f :: colorize_crossed_amplitude1 rest (ecf_in, ecf_out) 
          | C.SUN nc ->
              if nc > 0 then
                CF_in (f, List.hd ecf_in) ::
                colorize_crossed_amplitude1 rest (List.tl ecf_in, ecf_out) 
              else if nc < 0 then
                CF_out (f, List.hd ecf_out) ::
                colorize_crossed_amplitude1 rest (ecf_in, List.tl ecf_out) 
              else
                su0 "colorize_flavor"
          | C.AdjSUN _ ->
              let ecf_in' = List.hd ecf_in
              and ecf_out' = List.hd ecf_out in
              if ecf_in' = ecf_out' then
                CF_aux f ::
                colorize_crossed_amplitude1 rest (List.tl ecf_in, List.tl ecf_out) 
              else
                CF_io (f, ecf_in', ecf_out') ::
                colorize_crossed_amplitude1 rest (List.tl ecf_in, List.tl ecf_out) 
          end
      | [] ->
          begin match ecf_in, ecf_out with
          | [], [] -> []
          | _ -> invalid_arg "colorize_crossed_amplitude1"
          end
      
    let colorize_crossed_amplitude p_list =
      List.map (colorize_crossed_amplitude1 p_list) (external_color_flows p_list)

    let cross_uncolored p_in p_out =
      (List.map M.conjugate p_in) @ p_out

    let uncross_colored n_in p_lists_colorized =
      let p_in_out_colorized = List.map (ThoList.splitn n_in) p_lists_colorized in
      List.map
        (fun (p_in_colored, p_out_colored) ->
          (List.map conjugate p_in_colored, p_out_colored))
        p_in_out_colorized

    let amplitude p_in p_out =
      uncross_colored
        (List.length p_in)
        (colorize_crossed_amplitude (cross_uncolored p_in p_out))

(*i
module M = Models.SM(Models.SM_whizcol);;
module CM = Colorize.Gauge (struct let max_num = 10 end) (M);;

let test_amplitude p_in p_out =
  List.map
    (fun (i, o) ->
       (List.map CM.flavor_to_string i, List.map CM.flavor_to_string o))
    (CM.amplitude (List.map M.flavor_of_string p_in) (List.map M.flavor_of_string p_out));;
i*)

    let rcs =
      RCS.rename M.rcs
        ("Colorize.It(" ^ RCS.name M.rcs ^ ")")
        [String.concat " " (["Colorization Functor ("] @ RCS.description M.rcs @ [")"])]

  end

module Gauge (F : Flows) (M : Model.Gauge) = 
  struct

    module CM = It (F) (M)

    type flavor_sans_color = M.flavor
    type flavor = CM.flavor
    type gauge = CM.gauge
    type constant = CM.constant

    let color = CM.color
    let pdg = CM.pdg
    let lorentz = CM.lorentz
    let propagator = CM.propagator
    let width = CM.width
    let conjugate = CM.conjugate
    let fermion = CM.fermion
    let colsymm = CM.colsymm
    let max_degree = CM.max_degree
    let vertices = CM.vertices
    let fuse2 = CM.fuse2
    let fuse3 = CM.fuse3
    let fuse = CM.fuse
    let flavors = CM.flavors
    let external_flavors = CM.external_flavors
    let goldstone = CM.goldstone
    let parameters = CM.parameters
    let flavor_of_string = CM.flavor_of_string
    let flavor_to_string = CM.flavor_to_string
    let flavor_to_TeX = CM.flavor_to_TeX
    let flavor_symbol = CM.flavor_symbol
    let gauge_symbol = CM.gauge_symbol
    let mass_symbol = CM.mass_symbol
    let width_symbol = CM.width_symbol
    let constant_symbol = CM.constant_symbol
    let options = CM.options

    let incomplete s =
      failwith ("Colorize.Gauge()." ^ s ^ " not done yet!")

    type matter_field = M.matter_field
    type gauge_boson = M.gauge_boson
    type other = M.other

    type field =
      | Matter of matter_field
      | Gauge of gauge_boson
      | Other of other

    let field f = 
      incomplete "field"

    let matter_field f =
      incomplete "matter_field"

    let gauge_boson f =
      incomplete "gauge_boson"

    let other f =
      incomplete "other"

    let amplitude = CM.amplitude

    let rcs =
      RCS.rename M.rcs
        ("Colorize.Gauge(" ^ RCS.name M.rcs ^ ")")
        [String.concat " " (["Gauged Colorization Functor ("] @ RCS.description M.rcs @ [")"])]


  end

(*i
 *  Local Variables:
 *  mode:caml
 *  indent-tabs-mode:nil
 *  page-delimiter:"^(\\* .*\n"
 *  End:
i*)
