(* $Id: cascade.ml 228 2008-03-09 23:44:38Z reuter $ *)
(* Copyright (C) 2004 by Thorsten Ohl <ohl@physik.uni-wuerzburg.de>
   O'Mega is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by 
   the Free Software Foundation; either version 2, or (at your option)
   any later version.
   O'Mega is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.  *)  

module type T =
  sig

    type flavor
    type p

    type t
    val of_string_list : int -> string list -> t
    val to_string : t -> string

    type selectors
    val to_selectors : t -> selectors
    val no_cascades : selectors

    val select_wf : selectors -> (flavor -> p -> p list -> bool)
    val select_p : selectors -> (p -> p list -> bool)
    val on_shell : selectors -> (flavor -> p -> bool)
    val is_gauss : selectors -> (flavor -> p -> bool)

    val description : selectors -> string option

  end

module Make (M : Model.T) (P : Momentum.T) :
    (T with type flavor = M.flavor and type p = P.t) =
  struct

    module CS = Cascade_syntax

    type flavor = M.flavor
    type p = P.t

(* Since we have
   \begin{equation}
      p \le q \Longleftrightarrow (-q) \le (-p)
   \end{equation}
   also for $\le$ as set inclusion [lesseq], only four of the eight
   combinations are independent
   \begin{equation}
     \begin{aligned}
        p &\le q    &&\Longleftrightarrow & (-q) &\le (-p) \\
        q &\le p    &&\Longleftrightarrow & (-p) &\le (-q) \\
        p &\le (-q) &&\Longleftrightarrow & q    &\le (-p) \\
     (-q) &\le p    &&\Longleftrightarrow & (-p) &\le q 
     \end{aligned}
   \end{equation}  *)

    let one_compatible p q =
      let neg_q = P.neg q in
      P.lesseq p q ||
      P.lesseq q p ||
      P.lesseq p neg_q ||
      P.lesseq neg_q p

(* 'tis wasteful \ldots (at least by a factor of two, because every momentum
    combination is generated, including the negative ones. *)

    let all_compatible p p_list q =
      let l = List.length p_list in
      if l <= 2 then
        one_compatible p q
      else
        let tuple_lengths = ThoList.range 2 (succ l / 2) in
        let tuples = ThoList.flatmap (fun n -> Combinatorics.choose n p_list) tuple_lengths in
        let momenta = List.map (List.fold_left P.add (P.zero (P.dim q))) tuples in
        List.for_all (one_compatible q) momenta

(* The following assumes that the [flavor list] is always very short.  Otherwise
   one should use an efficient set implementation. *)

    type t =
      | True
      | False
      | On_shell of flavor * P.t
      | Off_shell of flavor list * P.t
      | Gauss of flavor * P.t
      | Any_flavor of P.t
      | Or of t list
      | And of t list

    let of_string s = 
      Cascade_parser.main Cascade_lexer.token (Lexing.from_string s)

    let import dim =
      let rec import' = function
        | CS.True -> True
        | CS.False -> False
        | CS.On_shell (f, p) ->
            On_shell (M.flavor_of_string f, P.of_ints dim p)
        | CS.Off_shell (fs, p) ->
            Off_shell (List.map M.flavor_of_string fs, P.of_ints dim p)
        | CS.Gauss (f, p) ->
            Gauss (M.flavor_of_string f, P.of_ints dim p)
        | CS.Any_flavor p -> Any_flavor (P.of_ints dim p)
        | CS.Or cs -> Or (List.map import' cs)
        | CS.And cs -> And (List.map import' cs) in
      import'

    let of_string_list dim strings =
      match List.map of_string strings with
      | [] -> True
      | first :: next ->
          import dim (List.fold_right CS.mk_or next first)

    let rec to_string = function
      | True -> "true"
      | False -> "false"
      | On_shell (f, p) ->
          P.to_string p ^ " = " ^ M.flavor_to_string f
      | Off_shell (fs, p) ->
          P.to_string p  ^ " ~ " ^
          (String.concat ":" (List.map M.flavor_to_string fs))
      | Gauss (f, p) -> 
          P.to_string p ^ " # " ^ M.flavor_to_string f
      | Any_flavor p ->
          P.to_string p ^ " ~ ?"
      | Or cs ->
          String.concat " || " (List.map (fun c -> "(" ^ to_string c ^ ")") cs)
      | And cs ->
          String.concat " && " (List.map (fun c -> "(" ^ to_string c ^ ")") cs)

    type selectors =
        { select_p : p -> p list -> bool;
          select_wf : flavor -> p -> p list -> bool;
          on_shell : flavor -> p -> bool;
          is_gauss : flavor -> p -> bool;
          description : string option }

    let no_cascades =
      { select_p = (fun _ _ -> true);
        select_wf = (fun _ _ _ -> true);
        on_shell = (fun _ _ -> false);
        is_gauss = (fun _ _ -> false);
        description = None }

    let select_p s = s.select_p
    let select_wf s = s.select_wf
    let on_shell s = s.on_shell
    let is_gauss s = s.is_gauss
    let description s = s.description

    let to_select_p cascades p p_in =

      let rec to_select_p' = function
        | True -> true
        | False -> false
        | On_shell (_, momentum) -> all_compatible p p_in momentum
        | Off_shell (_, momentum) -> all_compatible p p_in momentum
        | Gauss (_, momentum) -> all_compatible p p_in momentum
        | Any_flavor momentum -> all_compatible p p_in momentum
        | Or cs -> eval_or cs
        | And cs -> eval_and cs

      and eval_or = function
        | [] -> false
        | c :: cs ->
            if to_select_p' c then
              true
            else
              eval_or cs

      and eval_and = function
        | [] -> true
        | c :: cs ->
            if to_select_p' c then
              eval_and cs
            else
              false

      in
      to_select_p' cascades


    let to_select_wf cascades f p p_in =

      let f' = M.conjugate f in

      let rec to_select_wf' = function
        | True -> true
        | False -> false
        | On_shell (flavor, momentum) ->
            if p = momentum || p = P.neg momentum then
              f = flavor || f' = flavor
            else
              one_compatible p momentum && all_compatible p p_in momentum
        | Off_shell (flavors, momentum) ->
            if p = momentum || p = P.neg momentum then
              List.mem f flavors || List.mem f' flavors
            else
              one_compatible p momentum && all_compatible p p_in momentum
        | Gauss (flavor, momentum) ->
            if p = momentum || p = P.neg momentum then
              f = flavor || f' = flavor
            else
              one_compatible p momentum && all_compatible p p_in momentum
        | Any_flavor momentum ->
            one_compatible p momentum && all_compatible p p_in momentum
        | Or cs -> eval_or cs
        | And cs -> eval_and cs

      and eval_or = function
        | [] -> false
        | c :: cs ->
            if to_select_wf' c then
              true
            else
              eval_or cs

      and eval_and = function
        | [] -> true
        | c :: cs ->
            if to_select_wf' c then
              eval_and cs
            else
              false

      in
      to_select_wf' cascades


(* In case you're wondering: [to_on_shell f p] and [is_gauss f p] only search
   for on shell conditions and are to be used in a target, not in [Fusion]! *)

    let to_on_shell cascades f p =

      let rec to_on_shell' = function
        | True | False | Off_shell (_, _) | Gauss (_, _) | Any_flavor _ -> false
        | On_shell (flavor, momentum) ->
            (p = momentum || p = P.neg momentum) && (f = flavor || M.conjugate f = flavor)
        | Or cs | And cs -> descent cs

      and descent = function
        | [] -> false
        | c :: cs ->
            if to_on_shell' c then
              true
            else
              descent cs

      in
      to_on_shell' cascades

    let to_gauss cascades f p =

      let rec to_gauss' = function
        | True | False | Off_shell (_, _) | Any_flavor _ | On_shell (_, _) -> false
        | Gauss (flavor, momentum) ->
            (p = momentum || p = P.neg momentum) && (f = flavor || M.conjugate f = flavor)
        | Or cs | And cs -> descent cs

      and descent = function
        | [] -> false
        | c :: cs ->
            if to_gauss' c then
              true
            else
              descent cs

      in
      to_gauss' cascades

    let to_selectors = function
      | True -> no_cascades
      | c -> { select_p = to_select_p c;
               select_wf = to_select_wf c;
               on_shell = to_on_shell c;
               is_gauss = to_gauss c;
               description = Some (to_string c) }


  end

(*i
 *  Local Variables:
 *  mode:caml
 *  indent-tabs-mode:nil
 *  page-delimiter:"^(\\* .*\n"
 *  End:
i*)

