(* $Id: thoList.ml 68 2007-11-22 11:11:19Z ohl $ *)
(* Copyright (C) 1999-2004 by Thorsten Ohl <ohl@physik.uni-wuerzburg.de> et al.
   O'Mega is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by 
   the Free Software Foundation; either version 2, or (at your option)
   any later version.
   O'Mega is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.  *)  

let rec hdn n l =
  if n <= 0 then
    []
  else
    match l with
    | x :: rest -> x :: hdn (pred n) rest
    | [] -> invalid_arg "ThoList.hdn"

let rec tln n l =
  if n <= 0 then
    l
  else
    match l with
    | _ :: rest -> tln (pred n) rest
    | [] -> invalid_arg "ThoList.tln"

let rec splitn' n l1_rev l2 =
  if n <= 0 then
    (List.rev l1_rev, l2)
  else
    match l2 with
    | x :: l2' -> splitn' (pred n) (x :: l1_rev) l2'
    | [] -> invalid_arg "ThoList.splitn n > len"

let splitn n l =
  if n < 0 then
    invalid_arg "ThoList.splitn n < 0"
  else
    splitn' n [] l

let of_subarray n1 n2 a =
  let rec of_subarray' n1 n2 =
    if n1 > n2 then
      []
    else
      a.(n1) :: of_subarray' (succ n1) n2 in
  of_subarray' (max 0 n1) (min n2 (pred (Array.length a)))

let range ?(stride=1) n1 n2 =
  if stride <= 0 then
    invalid_arg "ThoList.range: stride <= 0"
  else
    let rec range' n =
      if n > n2 then
        []
      else
        n :: range' (n + stride) in
    range' n1

let rec flatmap f = function
  | [] -> []
  | x :: rest -> f x @ flatmap f rest

let fold_left2 f acc lists =
  List.fold_left (List.fold_left f) acc lists

let fold_right2 f lists acc =
  List.fold_right (List.fold_right f) lists acc

let compare ?(cmp=Pervasives.compare) l1 l2 =
  let rec compare' l1' l2' =
    match l1', l2' with
    | [], [] -> 0
    | [], _ -> -1
    | _, [] -> 1
    | n1 :: r1, n2 :: r2 ->
        let c = cmp n1 n2 in
        if c <> 0 then
          c
        else
          compare' r1 r2
  in
  compare' l1 l2

let rec uniq' x = function
  | [] -> []
  | x' :: rest ->
      if x' = x then
        uniq' x rest
      else
        x' :: uniq' x' rest

let uniq = function
  | [] -> []
  | x :: rest -> x :: uniq' x rest

(* If we needed it, we could use a polymorphic version of [Set] to
   speed things up from~$O(n^2)$ to~$O(n\ln n)$.  But not before it
   matters somewhere \ldots *)
let classify l =
  let rec add_to_class a = function
    | [] -> [1, a]
    | (n, a') :: rest ->
        if a = a' then
          (succ n, a) :: rest
        else
          (n, a') :: add_to_class a rest
  in
  let rec classify' cl = function
    | [] -> cl
    | a :: rest -> classify' (add_to_class a cl) rest
  in
  classify' [] l

let rec factorize l =
  let rec add_to_class x y = function
    | [] -> [(x, [y])]
    | (x', ys) :: rest ->
        if x = x' then
          (x, y :: ys) :: rest
        else
          (x', ys) :: add_to_class x y rest
  in
  let rec factorize' fl = function
    | [] -> fl
    | (x, y) :: rest -> factorize' (add_to_class x y fl) rest
  in
  List.map (fun (x, ys) -> (x, List.rev ys)) (factorize' [] l)
    
let rec clone n x =
  if n < 0 then
    invalid_arg "ThoList.clone"
  else if n = 0 then
    []
  else
    x :: clone (pred n) x

let rec rev_multiply n rl l =
  if n < 0 then
    invalid_arg "ThoList.multiply"
  else if n = 0 then
    []
  else
    List.rev_append rl (rev_multiply (pred n) rl l)

let multiply n l = rev_multiply n (List.rev l) l

(*i
 *  Local Variables:
 *  mode:caml
 *  indent-tabs-mode:nil
 *  page-delimiter:"^(\\* .*\n"
 *  End:
i*)





