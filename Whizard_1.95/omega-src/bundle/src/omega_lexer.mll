(* $Id: omega_lexer.mll,v 1.4 2004/06/22 09:30:18 ohl Exp $ *)
(* Copyright (C) 2000-2004 by Thorsten Ohl <ohl@physik.uni-wuerzburg.de> et al.
   O'Mega is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by 
   the Free Software Foundation; either version 2, or (at your option)
   any later version.
   O'Mega is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.  *)
{
open Omega_parser
}
rule token = parse
    [' ' '\t' '\n']    { token lexbuf }     (* skip blanks *)
  | "#" [^'\n']* '\n'  { token lexbuf }     (* skip comments *)
  | '{'                { OPEN }
  | '}'                { CLOSE }
  | "whizard"          { WHIZARD }
  | "output"           { OUTPUT }
  | "process"          { PROCESS }
  | "target"           { TARGET }
  | "model"            { MODEL }
  | ['a'-'z' 'A'-'Z'] [^ ' ' '\t' '\n' '{' '}' ]*
                       { STRING (Lexing.lexeme lexbuf) }
  | eof                { END }
