(* $Id: fusion.ml 326 2008-08-17 04:49:19Z reuter $ *)
(* Copyright (C) 2000-2004 by Thorsten Ohl <ohl@physik.uni-wuerzburg.de> et al.
   O'Mega is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by 
   the Free Software Foundation; either version 2, or (at your option)
   any later version.
   O'Mega is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.  *)  

let rcs_file = RCS.parse "Fusion" ["General Fusions"]
    { RCS.revision = "$Revision: 326 $";
      RCS.date = "$Date: 2008-08-17 06:49:19 +0200 (Sun, 17 Aug 2008) $";
      RCS.author = "$Author: reuter $";
      RCS.source
        = "$URL: svn+ssh://jr_reuter@login.hepforge.org/hepforge/svn/whizard/branches/1.xx/omega-src/bundle/src/fusion.ml $" }

module type T =
  sig
    type wf
    type flavor
    val flavor : wf -> flavor
    type p
    val momentum : wf -> p
    val momentum_list : wf -> int list
    val wf_tag : wf -> string option
    type constant
    type rhs
    type 'a children
    val sign : rhs -> int
    val coupling : rhs -> constant Coupling.t
    val coupling_tag : rhs -> string option
    val children : rhs -> wf list
    type fusion
    val lhs : fusion -> wf
    val rhs : fusion -> rhs list
    type braket
    val bra : braket -> wf
    val ket : braket -> rhs list
    type amplitude
    type selectors
    val amplitude : bool -> selectors ->
      flavor list -> flavor list -> amplitude
    val incoming : amplitude -> flavor list
    val outgoing : amplitude -> flavor list
    val externals : amplitude -> wf list
    val variables : amplitude -> wf list
    val fusions : amplitude -> fusion list
    val brakets : amplitude -> braket list
    val on_shell : amplitude -> (wf -> bool)
    val is_gauss : amplitude -> (wf -> bool)
    val constraints : amplitude -> string option
    val symmetry : amplitude -> int
    val color : amplitude -> (int * int) option
    val color_fac : amplitude -> string option
    val color_symm : amplitude -> int
    val num_gl : amplitude -> int
    val allowed : amplitude -> bool
    val count_fusions : amplitude -> int
    val count_propagators : amplitude -> int
    val count_diagrams : amplitude -> int
    val count_color_flows : amplitude -> int
    type coupling
    val forest : wf -> amplitude -> ((wf * coupling option, wf) Tree.t) list
    val poles : amplitude -> wf list list
    val s_channel : amplitude -> wf list
    val tower_to_dot : out_channel -> amplitude -> unit
    val amplitude_to_dot : out_channel -> amplitude -> unit
    val rcs_list : RCS.t list
  end

module type Maker =
    functor (P : Momentum.T) -> functor (M : Model.T) ->
      T with type p = P.t and type flavor = M.flavor
      and type constant = M.constant
      and type selectors = Cascade.Make(M)(P).selectors

(* \thocwmodulesection{Simple $\textrm{SU}(N)$ Color Factors} *)

module type SimpleColor =
  sig

(* Return the \emph{square} of the $\textrm{SU}(N)$ color
   factor for simple configurations, or [None] if the configuration
   is to complex.  *)
    val squared : Color.t list -> Color.t list -> (int * int) option
    val check_col : Color.t list -> Color.t list -> string option
    val count_gluons : Color.t list -> int 

  end

module SimpleColor : SimpleColor =
  struct
    open Color


(* We can easily compute color factors that reproduce cross sections summed
   over final state colors and averaged over initial state colors for up to
   two colored particles:
   \begin{align}
     C(\mathbf{N}\otimes\overline{\mathbf{N}} \to
        \mathbf{1}\otimes\ldots\otimes\mathbf{1}) &= \frac{1}{\sqrt{N}} \\
     C(\mathbf{1}\otimes\mathbf{1} \to
        \mathbf{N}\otimes\overline{\mathbf{N}}\otimes
        \mathbf{1}\otimes\ldots\otimes\mathbf{1}) &= \sqrt{N} \\
     C(\mathbf{N}\otimes\mathbf{1} \to
        \mathbf{N}\otimes\mathbf{1}\otimes\ldots\otimes\mathbf{1}) &= 1 \\
     C(\overline{\mathbf{N}}\otimes\mathbf{1} \to
        \overline{\mathbf{N}}\otimes\mathbf{1}\otimes\ldots\otimes\mathbf{1}) &= 1 \\
     C(\mathbf{N}\otimes\mathbf{N} \to
        \mathbf{1}\otimes\ldots\otimes\mathbf{1}) &= 0 \\
     C(\overline{\mathbf{N}}\otimes\overline{\mathbf{N}} \to
        \mathbf{1}\otimes\ldots\otimes\mathbf{1}) &= 0
   \end{align}
   \begin{dubious}
     Other cases that we can probably handle similarly:
     \begin{itemize}
       \item two fundamentals and one adjoint is trivial (``single gluon
         bremsstrahlung'')
       \item two fundamentals and two adjoints should still be feasible
         (``QCD Compton'')
       \item four fundamentals requires a splitting of the amplitude that
         should also be feasible
     \end{itemize}
   \end{dubious}
   \begin{dubious}
     A systematic approach will count the number of ways we can couple the
     external particles to a singlet (with the initial state conjugated). 
     If there is only one way, we can calculate the corresponding factor.
     If there are a few, we can probably use grove like methods to
     select color ``eigenamplitudes''.
   \end{dubious} *)
    let adj n = n * n - 1
    let punt = None
    let zero = Some (0, 1)
    let one = Some (1, 1)
    let initial n = Some (1, abs n) (* $1/\sqrt{N}$ *)
    let final n = Some (abs n, 1) (* $\sqrt{N}$ *)
    let initial_adj n = Some (1, adj n) (* $1/\sqrt{N^2 - 1}$ *)
    let final_adj n = Some (adj n, 1) (* $\sqrt{N^2 - 1}$ *)

(* Punt, if there is color in the final state.  *)
    let rec all_singlets cf = function
      | [] -> cf
      | SUN _ :: _ | AdjSUN _ :: _ -> punt
      | Singlet :: rest -> all_singlets cf rest

(* Expect exactly one $N$-plett. If there is none, the amplitude
   vanishes, of there are more, we punt. *)
    let rec match_fundamental cf n = function
      | [] -> zero
      | Singlet :: rest -> match_fundamental cf n rest
      | SUN n' :: rest when n = n' -> all_singlets cf rest
      | SUN _ :: _ | AdjSUN _ :: _ -> None

    let rec match_adj cf n = function
      | [] -> zero
      | Singlet :: rest -> match_adj cf n rest
      | SUN _ :: _ -> None
      | AdjSUN n' :: rest when n = n' -> all_singlets cf rest
      | AdjSUN _ :: _ -> None

(* Allow one pair of colored particles. *)
    let rec one_pair = function
      | [] -> one
      | Singlet :: rest -> one_pair rest
      | SUN n :: rest -> match_fundamental (final n) (-n) rest
      | AdjSUN n :: rest -> match_adj (final_adj n) n rest

(* For two colors: scattering, for one color: decay. *)

    let squared cin cout =
      match cin with
      | [c1; c2] ->
          begin match c1, c2 with
          | Singlet, Singlet -> one_pair cout
          | SUN n, SUN n' when n = - n' -> all_singlets (initial n) cout
          | SUN n, SUN n' -> all_singlets zero cout
          | SUN n, Singlet -> match_fundamental one n cout
          | Singlet, SUN n -> match_fundamental one n cout
          | AdjSUN n, Singlet -> match_adj one n cout
          | Singlet, AdjSUN n -> match_adj one n cout
          | AdjSUN _, SUN _ -> all_singlets zero cout
          | SUN _, AdjSUN _ -> all_singlets zero cout
          | AdjSUN n, AdjSUN n' when n = n' -> 
              all_singlets (initial_adj n) cout
          | AdjSUN n, AdjSUN n' -> all_singlets zero cout
          end
      | [c] ->
          begin match c with
          | Singlet -> one_pair cout
          | SUN n -> match_fundamental one n cout 
          | AdjSUN n -> match_adj one n cout
          end
      | _ -> None

    let check_col cin cout =
      match cin with
      | [c1;c2] ->
          begin match c1, c2 with
          | Singlet, Singlet -> None               
          | SUN n, SUN n' when n = - n' -> Some "fund/conj"
          | SUN n, SUN n' when n = n' -> Some "fund/fund or conj/conj"
          | _ -> Some "yet undefined"
          end
      | _ -> None

    let rec count_gluons c = 
      match c with
      | [] -> 0
      | Singlet :: rest -> count_gluons rest
      | SUN _ :: rest -> count_gluons rest
      | AdjSUN _ :: rest -> 1 + count_gluons rest
      
  end

(* \thocwmodulesection{Fermi Statistics} *)

module type Stat =
  sig
    type flavor
    type stat
    exception Impossible
    val stat : flavor -> int -> stat
    val stat_fuse : stat -> stat -> flavor -> stat
    val stat_sign : stat -> int
    val rcs : RCS.t
  end

module type Stat_Maker = functor (M : Model.T) ->
  Stat with type flavor = M.flavor

(* \thocwmodulesection{Dirac Fermions} *)

module Stat_Dirac (M : Model.T) : (Stat with type flavor = M.flavor) =
  struct 
    let rcs = RCS.rename rcs_file "Fusion.Stat_Dirac()"
        [ "Fermi statistics for Dirac fermions"]

    type flavor = M.flavor

(* \begin{equation}
     \gamma_\mu\psi(1)\,G^{\mu\nu}\,\bar\psi(2)\gamma_\nu\psi(3)
         - \gamma_\mu\psi(3)\,G^{\mu\nu}\,\bar\psi(2)\gamma_\nu\psi(1)
   \end{equation} *)

    type stat =
      | Fermion of int * (int option * int option) list
      | AntiFermion of int * (int option * int option) list
      | Boson of (int option * int option) list

    let stat f p =
      let s = M.fermion f in
      if s = 0 then
        Boson []
      else if s < 0 then
        AntiFermion (p, [])
      else (* [if s > 0 then] *)
        Fermion (p, [])

    exception Impossible

    let stat_fuse s1 s2 f =
      match s1, s2 with
      | Boson l1, Boson l2 -> Boson (l1 @ l2)
      | Boson l1, Fermion (p, l2) -> Fermion (p, l1 @ l2)
      | Boson l1, AntiFermion (p, l2) -> AntiFermion (p, l1 @ l2)
      | Fermion (p, l1), Boson l2 -> Fermion (p, l1 @ l2)
      | AntiFermion (p, l1), Boson l2 -> AntiFermion (p, l1 @ l2)
      | AntiFermion (pbar, l1), Fermion (p, l2) ->
          Boson ((Some pbar, Some p) :: l1 @ l2)
      | Fermion (p, l1), AntiFermion (pbar, l2) ->
          Boson ((Some pbar, Some p) :: l1 @ l2)
      | Fermion _, Fermion _ | AntiFermion _, AntiFermion _ ->
          raise Impossible

(* \begin{figure}
     \begin{displaymath}
       \parbox{26\unitlength}{%
         \begin{fmfgraph*}(25,15)
           \fmfstraight
           \fmfleft{f}
           \fmfright{f1,f2,f3}
           \fmflabel{$\psi(1)$}{f1}
           \fmflabel{$\bar\psi(2)$}{f2}
           \fmflabel{$\psi(3)$}{f3}
           \fmflabel{$0$}{f}
           \fmf{fermion}{f1,v1,f}
           \fmffreeze
           \fmf{fermion,tension=0.5}{f3,v2,f2}
           \fmf{photon}{v1,v2}
           \fmfdot{v1,v2}
         \end{fmfgraph*}}
       \qquad\qquad-\qquad
       \parbox{26\unitlength}{%
         \begin{fmfgraph*}(25,15)
           \fmfstraight
           \fmfleft{f}
           \fmfright{f1,f2,f3}
           \fmflabel{$\psi(1)$}{f1}
           \fmflabel{$\bar\psi(2)$}{f2}
           \fmflabel{$\psi(3)$}{f3}
           \fmflabel{$0$}{f}
           \fmf{fermion}{f3,v1,f}
           \fmffreeze
           \fmf{fermion,tension=0.5}{f1,v2,f2}
           \fmf{photon}{v1,v2}
           \fmfdot{v1,v2}
         \end{fmfgraph*}}
     \end{displaymath} 
     \caption{\label{fig:stat_fuse} Relative sign from Fermi statistics.}
   \end{figure} *)

(* \begin{equation}
     \epsilon \left(\left\{ (0,1), (2,3) \right\}\right)
       = - \epsilon \left(\left\{ (0,3), (2,1) \right\}\right)
   \end{equation} *)

    let permutation lines =
      let fout, fin = List.split lines in
      let eps_in, _ = Combinatorics.sort_signed compare fin
      and eps_out, _ = Combinatorics.sort_signed compare fout in
      (eps_in * eps_out)

(* \begin{dubious}
     This comparing of permutations of fermion lines is a bit tedious
     and takes a macroscopic fraction of time.  However, it's less than
     20\,\%, so we don't focus on improving on it yet.
   \end{dubious} *)

    let stat_sign = function
      | Boson lines -> permutation lines
      | Fermion (p, lines) -> permutation ((None, Some p) :: lines)
      | AntiFermion (pbar, lines) -> permutation ((Some pbar, None) :: lines)

  end

(* \thocwmodulesection{Tags} *)

module type Tags =
  sig
    type wf
    type coupling
    type 'a children
    val null_wf : wf
    val null_coupling : coupling
    val fuse : coupling -> wf children -> wf
    val wf_to_string : wf -> string option
    val coupling_to_string : coupling -> string option
  end

module type Tagger =
    functor (PT : Tuple.Poly) -> Tags with type 'a children = 'a PT.t

module type Tagged_Maker =
    functor (Tagger : Tagger) ->
      functor (P : Momentum.T) -> functor (M : Model.T) ->
        T with type p = P.t and type flavor = M.flavor
        and type constant = M.constant

(* No tags is one option for good tags \ldots *)

module No_Tags (PT : Tuple.Poly) =
  struct
    type wf = unit
    type coupling = unit
    type 'a children = 'a PT.t
    let null_wf = ()
    let null_coupling = ()
    let fuse () _ = ()
    let wf_to_string () = None
    let coupling_to_string () = None
  end

(* \begin{dubious}
     Here's a simple additive tag that can grow into something useful
     for loop calculations.
   \end{dubious} *)

module Loop_Tags (PT : Tuple.Poly) =
  struct
    type wf = int
    type coupling = int
    type 'a children = 'a PT.t
    let null_wf = 0
    let null_coupling = 0
    let fuse c wfs = PT.fold_left (+) c wfs
    let wf_to_string n = Some (string_of_int n)
    let coupling_to_string n = Some (string_of_int n)
  end
    
(* \thocwmodulesection{The [Fusion.Make] Functor} *)

module Tagged (Tagger : Tagger) (PT : Tuple.Poly)
    (Stat : Stat_Maker) (T : Topology.T with type 'a children = 'a PT.t)
    (P : Momentum.T) (M : Model.T) =
  struct 
    let rcs = RCS.rename rcs_file "Fusion.Make()"
        [ "Fusions for arbitrary topologies" ]

    open Coupling

    module S = Stat(M)

    type stat = S.stat
    let stat = S.stat
    let stat_sign = S.stat_sign

(* \begin{dubious}
     This will do \emph{something} for 4-, 6-, \ldots fermion vertices,
     but not necessarily the right thing \ldots
   \end{dubious} *)

    let stat_fuse s f =
      PT.fold_right_internal (fun s' acc -> S.stat_fuse s' acc f) s

    type flavor = M.flavor
    type constant = M.constant

(* \thocwmodulesubsection{Wave Functions} *)

(* \begin{dubious}
     The code below is not yet functional.  Too often, we assign to
     [Tags.null_wf] instead of calling [Tags.fuse].
   \end{dubious} *)

    module Tags = Tagger(PT)

    type p = P.t
    type wf =
        { flavor : flavor;
          momentum : p;
          wf_tag : Tags.wf }

    let flavor wf = wf.flavor
    let momentum wf = wf.momentum
    let momentum_list wf = P.to_ints wf.momentum
    let wf_tag_raw wf = wf.wf_tag
    let wf_tag wf = Tags.wf_to_string (wf_tag_raw wf)

(* Operator insertions can be fused only if they are external. *)
    let is_source wf =
      match M.propagator wf.flavor with
      | Only_Insertion -> P.rank wf.momentum = 1
      | _ -> true

(* [is_goldstone_of g v] is [true] if and only if [g] is the Goldstone boson
   corresponding to the gauge particle [v]. *)
    let is_goldstone_of g v =
      match M.goldstone v with
      | None -> false
      | Some (g', _) -> g = g'

(* In the future, we might want to have [Coupling] among the functor
   arguments.  However, for the moment, [Coupling] is assumed to be
   comprehensive. *)

    type sign = int
    type coupling =
        { sign : sign;
          coupling : constant Coupling.t;
          coupling_tag : Tags.coupling }

    type 'a children = 'a PT.t

(* This \emph{must} be a pair matching the [edge * node children] pairs of
   [DAG.Forest]! *)
    type rhs = coupling * wf children

    let sign ({ sign = s }, _) = s
    let coupling ({ coupling = c }, _) = c
    let coupling_tag_raw ({ coupling_tag = t }, _) = t
    let coupling_tag rhs = Tags.coupling_to_string (coupling_tag_raw rhs)
    let children (_, wfs) = PT.to_list wfs

(* \begin{dubious}
     In the end, [PT.to_list] should become redudant!
   \end{dubious} *)
    let fuse_rhs rhs = M.fuse (PT.to_list rhs)

(* \thocwmodulesubsection{Vertices} *)

(* Compute the set of all vertices in the model from the allowed
   fusions and the set of all flavors:
   \begin{dubious}
     One could think of using [M.vertices] instead of [M.fuse2],
     [M.fuse3] and [M.fuse] \ldots
   \end{dubious} *)

    module VSet = Map.Make(struct type t = flavor let compare = compare end)

    let add_vertices f rhs m =
      VSet.add f (try rhs :: VSet.find f m with Not_found -> [rhs]) m

    let collect_vertices rhs =
      List.fold_right (fun (f1, c) -> add_vertices (M.conjugate f1) (c, rhs))
        (fuse_rhs rhs)

(* The set of all vertices with common left fields factored. *)

(*   I used to think that constant initializers are a good idea to allow
     compile time optimizations.  The down side turned out to be that the
     constant initializers will be evaluated \emph{every time} the functor
     is applied.   \emph{Relying on the fact that the functor will be
     called only once is not a good idea!} *)

    let vertices max_degree flavors :
        (flavor * (constant Coupling.t * flavor PT.t) list) list =
      VSet.fold (fun f rhs v -> (f, rhs) :: v)
        (PT.power_fold collect_vertices flavors VSet.empty) []

(* Performance hack: *)

    let vertices_cache = ref None
    let hash = Cache.md5_hash (M.vertices ())

    let vertices max_degree flavors :
        (flavor * (constant Coupling.t * flavor PT.t) list) list =
      match !vertices_cache with 
      | None -> 
         begin match Cache.maybe_read hash with
         | None ->
             let result = vertices max_degree flavors in
             Cache.write hash result;
             vertices_cache := Some result;
             result
         | Some result -> result
         end
      | Some result -> result

(* \thocwmodulesubsection{Partitions} *)

(* Vertices that are not crossing invariant need special treatment so
   that they're only generated for the correct combinations of momenta.  *)

(* \begin{dubious}
     Using [PT.Mismatched_arity] is not really good style \ldots

   Tho's approach doesn't work since he does not catch charge conjugated processes or
   crossed processes. Another very strange thing is that O'Mega seems always to run in the
   q2 q3 timelike case, but not in the other two. (Property of how the DAG is built?).    
   For the $ZZZZ$ vertex I add the same vertex again, but interchange 1 and 3 in the 
   [crossing] vertex

   \end{dubious} *)

    let crossing c momenta =
      match c with
      | V4 (Vector4_K_Matrix_tho (disc,_), fusion, _) 
      | V4 (Vector4_K_Matrix_jr (disc,_), fusion, _) ->
          let s12, s23, s13 =
            begin match PT.to_list momenta with
            | [q1; q2; q3] -> (P.timelike (P.add q1 q2),
                               P.timelike (P.add q2 q3),
                               P.timelike (P.add q1 q3))
            | _ -> raise PT.Mismatched_arity
            end in
          begin match disc, s12, s23, s13, fusion with
          | 0, true, false, false, (F341|F431|F342|F432|F123|F213|F124|F214)
          | 0, false, true, false, (F134|F143|F234|F243|F312|F321|F412|F421)
          | 0, false, false, true, (F314|F413|F324|F423|F132|F231|F142|F241) ->
              true
          | 1, true, false, false, (F341|F431|F342|F432) 
          | 1, false, true, false, (F134|F143|F234|F243)
          | 1, false, false, true, (F314|F413|F324|F423) ->
              true
          | 2, true, false, false, (F123|F213|F124|F214)
          | 2, false, true, false, (F312|F321|F412|F421)
          | 2, false, false, true, (F132|F231|F142|F241) ->
              true
          | 3, true, false, false, (F143|F413|F142|F412|F321|F231|F324|F234)
          | 3, false, true, false, (F314|F341|F214|F241|F132|F123|F432|F423)
          | 3, false, false, true, (F134|F431|F124|F421|F312|F213|F342|F243) ->
              true 
          | _ -> false 
          end
      | _ -> true

(* Match a set of flavors to a set of momenta.  Form the direct product for
   the lists of momenta two and three with the list of couplings and flavors
   two and three.  *)

    let flavor_keystone select_p dim (f1, f23) (p1, p23) =
      ({ flavor = f1;
         momentum = P.of_ints dim p1;
         wf_tag = Tags.null_wf },
       Product.fold2 (fun (c, f) p acc ->
         try
           if select_p
               (P.of_ints dim p1)
               (PT.to_list (PT.map (P.of_ints dim) p)) then begin
             if crossing c (PT.map (P.of_ints dim) p) then
               (c, PT.map2 (fun f' p' -> { flavor = f';
                                         momentum = P.of_ints dim p';
                                           wf_tag = Tags.null_wf }) f p) :: acc
             else
               acc
           end else
             acc
         with
         | PT.Mismatched_arity -> acc) f23 p23 [])

(*i
    let cnt = ref 0

    let gc_stat () =
      let minor, promoted, major = Gc.counters () in
      Printf.sprintf "(%12.0f, %12.0f, %12.0f)" minor promoted major

    let flavor_keystone select_p n (f1, f23) (p1, p23) =
      incr cnt;
      Gc.set { (Gc.get()) with Gc.space_overhead = 20 };
      Printf.eprintf "%6d@%8.1f: %s\n" !cnt (Sys.time ()) (gc_stat ());
      flush stderr;
      flavor_keystone select_p n (f1, f23) (p1, p23)
i*)

(* Produce all possible combinations of vertices (flavor keystones)
   and momenta by forming the direct product.  The semantically equivalent
   [Product.list2 (flavor_keystone select_wf n) vertices keystones] with
   \emph{subsequent} filtering would be a \emph{very bad} idea, because
   a potentially huge intermediate list is built for large models.
   E.\,g.~for the MSSM this would lead to non-termination by thrashing
   for $2\to4$ processes on most PCs. *)

    let flavor_keystones filter select_p dim vertices keystones =
      Product.fold2 (fun v k acc ->
        filter (flavor_keystone select_p dim v k) acc) vertices keystones []

(* Flatten the nested lists of vertices into a list of attached lines. *)

    let flatten_keystones t =
      ThoList.flatmap (fun (p1, p23) ->
        p1 :: (ThoList.flatmap (fun (_, rhs) -> PT.to_list rhs) p23)) t

(* Once more, but without duplicates this time. *)

(* Order wavefunctions so that the external come first, then the pairs, etc.
   Also put possible Goldstone bosons \emph{before} their gauge bosons. *)

    let lorentz_ordering f =
      match M.lorentz f with
      | Coupling.Scalar -> 0
      | Coupling.Spinor -> 1
      | Coupling.ConjSpinor -> 2
      | Coupling.Majorana -> 3
      | Coupling.Vector -> 4
      | Coupling.Massive_Vector -> 5
      | Coupling.Tensor_2 -> 6
      | Coupling.Tensor_1 -> 7
      | Coupling.Vectorspinor -> 8
      | Coupling.BRS Coupling.Scalar -> 9
      | Coupling.BRS Coupling.Spinor -> 10
      | Coupling.BRS Coupling.ConjSpinor -> 11
      | Coupling.BRS Coupling.Majorana -> 12
      | Coupling.BRS Coupling.Vector -> 13
      | Coupling.BRS Coupling.Massive_Vector -> 14
      | Coupling.BRS Coupling.Tensor_2 -> 15
      | Coupling.BRS Coupling.Tensor_1 -> 16
      | Coupling.BRS Coupling.Vectorspinor -> 17
      | Coupling.BRS _ -> invalid_arg "Fusion.lorentz_ordering: not needed"
      | Coupling.Maj_Ghost -> 18
(*i      | Coupling.Ward_Vector -> 19  i*)
      
    let order_flavor f1 f2 =
      let c = compare (lorentz_ordering f1) (lorentz_ordering f2) in
      if c <> 0 then
        c
      else
        compare f1 f2

    let order_wf wf1 wf2 =
      let c = P.compare wf1.momentum wf2.momentum in
      if c <> 0 then
        c
      else
        let c = order_flavor wf1.flavor wf2.flavor in
        if c <> 0 then
          c
        else
          compare wf1.wf_tag wf2.wf_tag

    let wavefunctions t =
      let module WF =
        Set.Make (struct type t = wf let compare = order_wf end) in
      WF.elements (List.fold_left (fun set (wf1, wf23) ->
        WF.add wf1 (List.fold_left (fun set' (_, wfs) ->
          PT.fold_right WF.add wfs set') set wf23)) WF.empty t)
          
(* \thocwmodulesubsection{Subtrees} *)

(* Fuse a tuple of wavefunctions, keeping track of Fermi statistics.
   Record only the the sign \emph{relative} to the children.
   (The type annotation is only for documentation.) *)

    let fuse select_wf wfss : (wf * stat * rhs) list =
      if PT.for_all (fun (wf, _) -> is_source wf) wfss then
        try
          let wfs, ss = PT.split wfss in
          let flavors = PT.map flavor wfs
          and momenta = PT.map momentum wfs
          (*i and wf_tags = PT.map wf_tag_raw wfs i*) in
          let p = PT.fold_left_internal P.add momenta in
          List.fold_left
            (fun acc (f, c) ->
              if select_wf f p (PT.to_list momenta) && crossing c momenta then
                let s = stat_fuse ss f in
                let flip =
                  PT.fold_left (fun acc s' -> acc * stat_sign s') (stat_sign s) ss in
                ({ flavor = f;
                   momentum = p;
                   wf_tag = Tags.null_wf }, s,
                 ({ sign = flip;
                    coupling = c;
                    coupling_tag = Tags.null_coupling }, wfs)) :: acc
              else
                acc)
            [] (fuse_rhs flavors)
        with
        | P.Duplicate _ | S.Impossible -> []
      else
        []

    module D = DAG.Make
        (DAG.Forest(PT)
           (struct type t = wf let compare = order_wf end)
           (struct type t = coupling let compare = compare end))

(* \begin{dubious}
     Eventually, the pairs of [tower] and [dag] in [fusion_tower']
     below could and should be replaced by a graded [DAG].  This will
     look like, but currently [tower] containts statistics information
     that is missing from [dag]:
     \begin{quote}
       \verb+Type node = flavor * p is not compatible with type wf * stat+
     \end{quote}
     This should be easy to fix.  However, replacing [type t = wf]
     with [type t = wf * stat] is \emph{not} a good idea because the variable
     [stat] makes it impossible to test for the existance of a particular
     [wf] in a [DAG].
   \end{dubious}
   \begin{dubious}
     In summary, it seems that [(wf * stat) list array * D.t] should be
     replaced by [(wf -> stat) * D.t].
   \end{dubious} *)
    module GF =
      struct
        module Nodes =
          struct
            type t = wf
            module G = struct type t = int let compare = compare end
            let compare = order_wf
            let rank wf = P.rank (momentum wf)
          end
        module Edges = struct type t = coupling let compare = compare end
        module F = DAG.Forest(PT)(Nodes)(Edges)
        type node = Nodes.t
        type edge = F.edge
        type children = F.children
        type t = F.t
        let compare = F.compare
        let for_all = F.for_all
        let fold = F.fold
      end

    module D' = DAG.Graded(GF)

    let tower_of_dag dag =
      let _, max_rank = D'.min_max_rank dag in
      Array.init max_rank (fun n -> D'.ranked n dag)

    module Stat = Map.Make (struct type t = wf let compare = order_wf end)

(* The function [fusion_tower']
   recursively builds the tower of all fusions from bottom up to a chosen
   level. The argument [tower] is an array of lists, where the $i$-th sublist
   (counting from 0) represents all off shell wave functions depending on
   $i+1$~momenta and their Fermistatistics.
   \begin{equation}
     \begin{aligned}
         \Bigl\lbrack
                 & \{ \phi_1(p_1), \phi_2(p_2), \phi_3(p_3), \ldots \}, \\
                 & \{ \phi_{12}(p_1+p_2), \phi'_{12}(p_1+p_2), \ldots,
                      \phi_{13}(p_1+p_3), \ldots, \phi_{23}(p_2+p_3), \ldots \}, \\
                 & \ldots \\
                 & \{ \phi_{1\cdots n}(p_1+\cdots+p_n),
                      \phi'_{1\cdots n}(p_1+\cdots+p_n), \ldots \} \Bigr\rbrack
     \end{aligned}
   \end{equation}
   The argument [dag] is a DAG representing all the fusions calculated so far.
   NB: The outer array in [tower] is always very short, so we could also
   have accessed a list with [List.nth].   Appending of new members at the
   end brings no loss of performance.  NB: the array is supposed to be
   immutable.  *)

(* The towers must be sorted so that the combinatorical functions can
   make consistent selections.
   \begin{dubious}
     Intuitively, this seems to be correct.  However, one could have
     expected that no element appears twice and that this ordering is
     not necessary \ldots
   \end{dubious} *)
    let grow select_wf tower =
      let rank = succ (Array.length tower) in
      List.sort Pervasives.compare
        (PT.graded_sym_power_fold rank
           (fun wfs acc -> fuse select_wf wfs @ acc) tower [])

    let add_offspring dag (wf, _, rhs) =
      D.add_offspring wf rhs dag

    let filter_offspring fusions =
      List.map (fun (wf, s, _) -> (wf, s)) fusions

    let rec fusion_tower' n_max select_wf tower dag : (wf * stat) list array * D.t =
      if Array.length tower >= n_max then
        (tower, dag)
      else
        let tower' = grow select_wf tower in
        fusion_tower' n_max select_wf
          (Array.append tower [|filter_offspring tower'|])
          (List.fold_left add_offspring dag tower')

(* Discard the tower and return a map from wave functions to Fermistatistics
   together with the DAG. *)

    let make_external_dag wfs =
      List.fold_left (fun m (wf, _) -> D.add_node wf m) D.empty wfs

    let mixed_fold_left f acc lists =
      Array.fold_left (List.fold_left f) acc lists

    let fusion_tower height select_wf wfs : (wf -> stat) * D.t =
      let tower, dag =
        fusion_tower' height select_wf [|wfs|] (make_external_dag wfs) in
      let stats = mixed_fold_left
          (fun m (wf, s) -> Stat.add wf s m) Stat.empty tower in
      ((fun wf -> Stat.find wf stats), dag)

(* Calculate the minimal tower of fusions that suffices for calculating
   the amplitude.  *)

    let minimal_fusion_tower n select_wf wfs : (wf -> stat) * D.t =
      fusion_tower (T.max_subtree n) select_wf wfs

(* Calculate the complete tower of fusions.  It is much larger than required,
   but it allows a complete set of gauge checks.  *)
    let complete_fusion_tower select_wf wfs : (wf -> stat) * D.t =
      fusion_tower (List.length wfs - 1) select_wf wfs

(* \begin{dubious}
     There is a natural product of two DAGs using [fuse].  Can this be
     used in a replacement for [fusion_tower]?  The hard part is to avoid
     double counting, of course.  A straight forward solution
     could do a diagonal sum (in order to reject flipped offspring representing
     the same fusion) and rely on the uniqueness in [DAG] otherwise.
     However, this will (probably) slow down the procedure significanty,
     because most fusions (including Fermi signs!) will be calculated before
     being rejected by [DAD().add_offspring].
   \end{dubious} *)

(* Add to [dag] all Goldstone bosons defined in [tower] that correspond
   to gauge bosons in [dag].  This is only required for checking
   Slavnov-Taylor identities in unitarity gauge.  Currently, it is not used,
   because we use the complete tower for gauge checking. *)
    let harvest_goldstones tower dag =
      D.fold_nodes (fun wf dag' ->
        match M.goldstone wf.flavor with
        | Some (g, _) ->
            let wf' = { wf with flavor = g } in
            if D.is_node wf' tower then begin
              D.harvest tower wf' dag'
            end else begin
              dag'
            end
        | None -> dag') dag dag

(* Calculate the sign from Fermi statistics that is not already included
   in the children.
   \begin{dubious}
     The use of [PT.of2_kludge] is the largest skeleton on the cupboard of
     unified fusions.   Currently, it is just another name for [PT.of2],
     but the existence of the latter requires binary fusions.  Of course, this
     is just a symptom for not fully supporting four fermion vertices \ldots
   \end{dubious} *)
    let stat_keystone stats wf1 wfs =
      let wf1' = stats wf1
      and wfs' = PT.map stats wfs in
      stat_sign
        (stat_fuse
           (PT.of2_kludge wf1' (stat_fuse wfs' (M.conjugate (flavor wf1))))
           (flavor wf1))
        * PT.fold_left (fun acc wf -> acc * stat_sign wf) (stat_sign wf1') wfs'

(* Test all members of a list of wave functions are defined by the DAG
   simultaneously: *)
    let test_rhs dag (_, wfs) =
      PT.for_all (fun wf -> is_source wf && D.is_node wf dag) wfs

(* Add the keystone [(wf1,pairs)] to [acc] only if it is present in [dag]
   and calculate the statistical factor depending on [stats]
   \emph{en passant}: *)
    let filter_keystone stats dag (wf1, pairs) acc =
      if is_source wf1 && D.is_node wf1 dag then
        match List.filter (test_rhs dag) pairs with
        | [] -> acc
        | pairs' -> (wf1, List.map (fun (c, wfs) ->
            ({ sign = stat_keystone stats wf1 wfs;
               coupling = c;
               coupling_tag = Tags.null_coupling },
             wfs)) pairs') :: acc
      else
        acc

(* \begin{figure}
     \begin{center}
       \thocwincludegraphics{width=\textwidth}{bhabha0}\\
       \hfil\\
       \thocwincludegraphics{width=\textwidth}{bhabha}
     \end{center}
     \caption{\label{fig:bhabha}
       The DAGs for Bhabha scattering before and after weeding out unused
       nodes. The blatant asymmetry of these DAGs is caused by our
       prescription for removing doubling counting for an even number
       of external lines.}
   \end{figure}
   \begin{figure}
     \begin{center}
       \thocwincludegraphics{width=\textwidth}{epemudbarmunumubar0}\\
       \hfil\\
       \thocwincludegraphics{width=\textwidth}{epemudbarmunumubar}
     \end{center}
     \caption{\label{fig:epemudbarmunumubar}
       The DAGs for $e^+e^-\to u\bar d \mu^-\bar\nu_\mu$ before and after
       weeding out unused nodes.}
   \end{figure}
   \begin{figure}
     \begin{center}
       \thocwincludegraphics{width=\textwidth}{epemudbardubar0}\\
       \hfil\\
       \thocwincludegraphics{width=\textwidth}{epemudbardubar}
     \end{center}
     \caption{\label{fig:epemudbardubar}
       The DAGs for $e^+e^-\to u\bar d d\bar u$ before and after weeding
       out unused nodes.}
   \end{figure} *)

(* \thocwmodulesubsection{Amplitudes} *)

    type fusion = wf * rhs list

    let lhs (l, _) = l
    let rhs (_, r) = r

    type braket = wf * rhs list

    let bra (b, _) = b
    let ket (_, k) = k

    type amplitude =
        { fusions : fusion list;
          brakets : braket list;
          on_shell : wf -> bool;
          is_gauss : wf -> bool;
          constraints : string option;
          incoming : flavor list;
          outgoing : flavor list;
          externals : wf list;
          color : (int * int) option;
          color_fac : string option;
          color_symm : int; 
          num_gl : int;
          symmetry : int;
          fusion_tower : D.t;
          fusion_dag : D.t }

    module C = Cascade.Make(M)(P)
    type selectors = C.selectors

    let incoming a = a.incoming
    let outgoing a = a.outgoing
    let externals a = a.externals
    let fusions a = a.fusions
    let brakets a = a.brakets
    let symmetry a = a.symmetry
    let on_shell a = a.on_shell
    let is_gauss a = a.is_gauss
    let constraints a = a.constraints
    let color a = a.color
    let color_fac a = a.color_fac
    let color_symm a = a.color_symm
    let num_gl a = a.num_gl
    let variables a = List.map fst a.fusions

    let allowed amplitude =
      match brakets amplitude, color amplitude with
      | [], _ | _, Some (0, _) -> false
      | _ -> true

    let external_wfs n particles =
      List.map (fun (f, p) ->
        ({ flavor = f;
           momentum = P.singleton n p;
           wf_tag = Tags.null_wf },
         stat f p)) particles

(* \thocwmodulesubsection{Main Function} *)

(* [map_amplitude_wfs f a] applies the function [f : wf -> wf] to all
   wavefunctions appearing in the amplitude [a]. *)
    let map_amplitude_wfs f a =
      let map_rhs (c, wfs) = (c, PT.map f wfs) in
      let map_braket (wf, rhs) = (f wf, List.map map_rhs rhs)
      and map_fusion (lhs, rhs) = (f lhs, List.map map_rhs rhs) in
      let map_brakets = List.map map_braket
      and map_fusions = List.map map_fusion
      and map_dag = D.map f (fun node rhs -> map_rhs rhs) in
      { fusions = map_fusions a.fusions;
        brakets = map_brakets a.brakets;
        on_shell = a.on_shell;
        is_gauss = a.is_gauss;
        constraints = a.constraints;
        incoming = a.incoming;
        outgoing = a.outgoing;
        externals = List.map f a.externals;
        symmetry = a.symmetry;
        color = a.color;
        color_fac = a.color_fac;
        color_symm = a.color_symm;
        num_gl = a.num_gl;
        fusion_tower = map_dag a.fusion_tower;
        fusion_dag = map_dag a.fusion_dag }

(*i
(* \begin{dubious}
     Just a silly little test:
   \end{dubious} *)

    let hack_amplitude =
      map_amplitude_wfs (fun wf -> { wf with momentum = P.split 2 16 wf.momentum })
i*)

(* This is the main function that constructs the amplitude for sets
   of incoming and outgoing particles and returns the results in
   conveniently packaged pieces.  *)

    let amplitude goldstones selectors fin fout =

      (* Set up external lines and match flavors with numbered momenta. *)
      let f = fin @ List.map M.conjugate fout in
      let nin, nout = List.length fin, List.length fout in
      let n = nin + nout in
      let externals = List.combine f (ThoList.range 1 n) in
      let wfs = external_wfs n externals in
      let select_wf = C.select_wf selectors in
      let select_p = C.select_p selectors in

      (* Build the full fusion tower (including nodes that are never
         needed in the amplitude). *)
      let stats, tower =

        if goldstones then
          complete_fusion_tower select_wf wfs
        else
          minimal_fusion_tower n select_wf wfs in

      (* Find all vertices for which \emph{all} off shell wavefunctions
         are defined by the tower. *)

      let brakets =
        flavor_keystones (filter_keystone stats tower) select_p n
          (vertices (M.max_degree ()) (M.flavors ()))
          (T.keystones (ThoList.range 1 n)) in

      (* Remove the part of the DAG that is never needed in the amplitude. *)
      let dag =
        if goldstones then
          tower
        else
          D.harvest_list tower (wavefunctions brakets) in

      (* Remove the leaf nodes of the DAG, corresponding to external lines. *)
      let fusions =
        List.filter (function (_, []) -> false | _ -> true) (D.lists dag) in

      (* Calculate the symmetry factor for identical particles in the
         final state. *)
      let symmetry = Combinatorics.symmetry fout
      and color_symm = 
        let sym_list = List.split (List.map M.colsymm fout) in 
          (Combinatorics.symmetry 
             (List.filter snd (fst sym_list))) /
          (Combinatorics.symmetry
             (List.filter snd (snd sym_list)))
      and color = SimpleColor.squared
        (List.map M.color fin) (List.map M.color fout) 
      and num_gl = SimpleColor.count_gluons
          (List.map M.color (fin @ fout))
      and color_fac = SimpleColor.check_col
        (List.map M.color fin) (List.map M.color fout) in

      (* Finally: package the results: *)
      { fusions = fusions;
        brakets = brakets;
        on_shell = (fun wf -> C.on_shell selectors (flavor wf) (momentum wf));
        is_gauss = (fun wf -> C.is_gauss selectors (flavor wf) (momentum wf));
        constraints = C.description selectors;
        incoming = fin;
        outgoing = fout;
        externals = List.map fst wfs;        
        symmetry = symmetry;
        color = color;
        color_fac = color_fac;
        color_symm = color_symm;
        num_gl = num_gl;
        fusion_tower = tower;
        fusion_dag = dag }

(* \thocwmodulesubsection{Diagnostics} *)

    let count_propagators a =
      List.length a.fusions

    let count_fusions a =
      List.fold_left (fun n (_, a) -> n + List.length a) 0 a.fusions
        + List.fold_left (fun n (_, t) -> n + List.length t) 0 a.brakets
        + List.length a.brakets

(* \begin{dubious}
     This brute force approach blows up for more than ten particles.
     Find a smarter algorithm.
   \end{dubious} *)

    let count_diagrams a =
      List.fold_left (fun n (wf1, wf23) ->
        n + D.count_trees wf1 a.fusion_dag *
          (List.fold_left (fun n' (_, wfs) ->
            n' + PT.fold_left (fun n'' wf ->
              n'' * D.count_trees wf a.fusion_dag) 1 wfs) 0 wf23))
        0 a.brakets

    exception Impossible
        
    let count_color_flows a =
      1

(* \begin{dubious}
     We still need to perform the appropriate charge conjugations so that we
     get the correct flavors for the external tree representation.
   \end{dubious} *)

    let forest' a =
      let below wf = D.forest_memoized wf a.fusion_dag in
      ThoList.flatmap
        (fun (bra, ket) ->
          (Product.list2 (fun bra' ket' -> bra' :: ket')
             (below bra)
             (ThoList.flatmap
                (fun (_, wfs) ->
                  Product.list (fun w -> w) (PT.to_list (PT.map below wfs)))
                ket)))
        a.brakets

    let cross wf =
      { flavor = M.conjugate wf.flavor;
        momentum = P.neg wf.momentum;
        wf_tag = wf.wf_tag }

    let fuse_trees wf ts =
      Tree.fuse (fun (wf', e) -> (cross wf', e))
        wf (fun t -> List.mem wf (Tree.leafs t)) ts
      
    let forest wf a =
      List.map (fuse_trees wf) (forest' a)

    let poles_beneath wf dag =
      D.eval_memoized (fun wf' -> [[]])
        (fun wf' _ p -> List.map (fun p' -> wf' :: p') p)
        (fun wf1 wf2 ->
          Product.fold2 (fun wf' wfs' wfs'' -> (wf' @ wfs') :: wfs'') wf1 wf2 [])
        (@) [[]] [[]] wf dag

    let poles a =
      ThoList.flatmap (fun (wf1, wf23) ->
        let poles_wf1 = poles_beneath wf1 a.fusion_dag in
        (ThoList.flatmap (fun (_, wfs) ->
          Product.list List.flatten
            (PT.to_list (PT.map (fun wf ->
              poles_wf1 @ poles_beneath wf a.fusion_dag) wfs)))
           wf23))
        a.brakets

    let s_channel a =
      let module WF =
        Set.Make (struct type t = wf let compare = order_wf end) in
        WF.elements (ThoList.fold_right2
                       (fun wf wfs ->
                         if P.timelike (momentum wf) then
                           WF.add wf wfs
                         else
                           wfs) (poles a) WF.empty)
      
(* \begin{dubious}
     This should be much faster!  Is it correct?  Is it faster indeed?
   \end{dubious} *)

    let poles' a =
      List.map lhs a.fusions

    let s_channel a =
      let module WF =
        Set.Make (struct type t = wf let compare = order_wf end) in
        WF.elements (List.fold_right
                       (fun wf wfs ->
                         if P.timelike (momentum wf) then
                           WF.add wf wfs
                         else
                           wfs) (poles' a) WF.empty)
      
(* \thocwmodulesubsection{Pictures} *)

(* Export the DAG in the \texttt{dot(1)} file format so that we can
   draw pretty pictures to impress audiences \ldots *)

    let p2s p =
      if p >= 0 && p <= 9 then
        string_of_int p
      else if p <= 36 then
        String.make 1 (Char.chr (Char.code 'A' + p - 10))
      else
        "_"

    let variable wf =
      M.flavor_symbol (flavor wf) ^
      String.concat "" (List.map p2s (momentum_list wf))

    module Int = Map.Make (struct type t = int let compare = compare end)

    let add_to_list i n m =
      Int.add i (n :: try Int.find i m with Not_found -> []) m

    let classify_nodes dag =
      Int.fold (fun i n acc -> (i, n) :: acc)
        (D.fold_nodes (fun wf -> add_to_list (P.rank (momentum wf)) wf)
           dag Int.empty) []

    let dag_to_dot ch brakets dag =
      Printf.fprintf ch "digraph OMEGA {\n";
      D.iter_nodes (fun wf ->
        Printf.fprintf ch "  \"%s\" [ label = \"%s\" ];\n"
          (variable wf) (variable wf)) dag;
      List.iter (fun (_, wfs) ->
        Printf.fprintf ch "  { rank = same;";
        List.iter (fun n ->
          Printf.fprintf ch " \"%s\";" (variable n)) wfs;
        Printf.fprintf ch " };\n") (classify_nodes dag);
      List.iter (fun n ->
        Printf.fprintf ch " \"*\" -> \"%s\";\n" (variable n))
        (flatten_keystones brakets);
      D.iter (fun n (_, ns) ->
        let p = variable n in
        PT.iter (fun n' ->
          Printf.fprintf ch "  \"%s\" -> \"%s\";\n" p (variable n')) ns) dag;
      Printf.fprintf ch "}\n"

    let tower_to_dot ch a =
      dag_to_dot ch a.brakets a.fusion_tower

    let amplitude_to_dot ch a =
      dag_to_dot ch a.brakets a.fusion_dag


    let rcs_list = [D.rcs; T.rcs; P.rcs; rcs]

  end

module Make = Tagged(No_Tags)

module Binary = Make(Tuple.Binary)(Stat_Dirac)(Topology.Binary)
module Tagged_Binary (T : Tagger) =
  Tagged(T)(Tuple.Binary)(Stat_Dirac)(Topology.Binary)

(* \thocwmodulesection{Fusions with Majorana Fermions} *)

module Stat_Majorana (M : Model.T) : (Stat with type flavor = M.flavor) =
  struct 
    let rcs = RCS.rename rcs_file "Fusion.Stat_Dirac()"
        [ "Fermi statistics for Dirac fermions"]

    type flavor = M.flavor

    type stat =
      | Fermion of int * int list
      | AntiFermion of int * int list
      | Boson of int list
      | Majorana of int * int list        

    let stat f p =
      let s = M.fermion f in
      if s = 0 then
        Boson []
      else if s < 0 then
        AntiFermion (p, [])
      else if s = 1 then (* [if s = 1 then] *)
        Fermion (p, [])
      else (* [if s > 1 then] *)
        Majorana (p, [])   

(* \begin{JR}
   In the formalism of~\cite{Denner:Majorana}, it does not matter to distinguish
   spinors and conjugate spinors, it is only important to know in which direction
   a fermion line is calculated. So the sign is made by the calculation together
   with an aditional one due to the permuation of the pairs of endpoints of
   fermion lines in the direction they are calculated. We propose a
   ``canonical'' direction from the right to the left child at a fusion point
   so we only have to keep in mind which external particle hangs at each side.
   Therefore we need not to have a list of pairs of conjugate spinors and
   spinors but just a list in which the pairs are right-left-right-left
   and so on. Unfortunately it is unavoidable to have couplings with clashing 
   arrows in supersymmetric theories so we need transmutations from fermions 
   in antifermions and vice versa as well.
   \end{JR} *)   

    exception Impossible

(*i
    let stat_fuse s1 s2 f =
      match s1, s2, M.lorentz f with
      | Boson l1, Boson l2, _ -> Boson (l1 @ l2)
      | Boson l1, Fermion (p, l2), Coupling.Majorana ->
          Majorana (p, l1 @ l2)
      | Boson l1, Fermion (p, l2), _ -> Fermion (p, l1 @ l2)
      | Boson l1, AntiFermion (p, l2), Coupling.Majorana ->
          Majorana (p, l1 @ l2)
      | Boson l1, AntiFermion (p, l2), _ -> AntiFermion (p, l1 @ l2)
      | Fermion (p, l1), Boson l2, Coupling.Majorana ->
          Majorana (p, l1 @ l2)
      | Fermion (p, l1), Boson l2, _ -> Fermion (p, l1 @ l2)
      | AntiFermion (p, l1), Boson l2, Coupling.Majorana ->
          Majorana (p, l1 @ l2)
      | AntiFermion (p, l1), Boson l2, _ ->
          AntiFermion (p, l1 @ l2)
      | Majorana (p, l1), Boson l2, Coupling.Spinor ->
          Fermion (p, l1 @ l2)
      | Majorana (p, l1), Boson l2, Coupling.ConjSpinor ->
          AntiFermion (p, l1 @ l2)
      | Majorana (p, l1), Boson l2, _ ->
          Majorana (p, l1 @ l2)
      | Boson l1, Majorana (p, l2), Coupling.Spinor ->
          Fermion (p, l1 @ l2)
      | Boson l1, Majorana (p, l2), Coupling.ConjSpinor ->
          AntiFermion (p, l1 @ l2)
      | Boson l1, Majorana (p, l2),  _ ->
          Majorana (p, l1 @ l2)
      | AntiFermion (pbar, l1), Fermion (p, l2), _ ->
          Boson ([p; pbar] @ l1 @ l2)
      | Fermion (p, l1), AntiFermion (pbar, l2), _ ->
          Boson ([pbar; p] @ l1 @ l2)
      | Fermion (pf, l1), Majorana (pm, l2), _ ->
          Boson ([pm; pf] @ l1 @ l2)
      | Majorana (pm, l1), Fermion (pf, l2), _ ->
          Boson ([pf; pm] @ l1 @ l2)
      | AntiFermion (pa, l1), Majorana (pm, l2), _ ->
          Boson ([pm; pa] @ l1 @ l2)
      | Majorana (pm, l1), AntiFermion (pa, l2), _ ->
          Boson ([pa; pm] @ l1 @ l2)
      | Majorana (p1, l1), Majorana (p2, l2), _ ->
          Boson ([p2; p1] @ l1 @ l2)
      | Fermion _, Fermion _, _ | AntiFermion _,
          AntiFermion _, _ -> raise Impossible     
i*)

    let stat_fuse s1 s2 f =
      match s1, s2, M.lorentz f with
      | Boson l1, Fermion (p, l2), Coupling.Majorana 
      | Boson l1, AntiFermion (p, l2), Coupling.Majorana 
      | Fermion (p, l1), Boson l2, Coupling.Majorana 
      | AntiFermion (p, l1), Boson l2, Coupling.Majorana 
      | Majorana (p, l1), Boson l2, Coupling.Majorana 
      | Boson l1, Majorana (p, l2),  Coupling.Majorana ->
          Majorana (p, l1 @ l2)
      | Boson l1, Fermion (p, l2), Coupling.Spinor 
      | Boson l1, AntiFermion (p, l2), Coupling.Spinor 
      | Fermion (p, l1), Boson l2, Coupling.Spinor 
      | AntiFermion (p, l1), Boson l2, Coupling.Spinor 
      | Majorana (p, l1), Boson l2, Coupling.Spinor 
      | Boson l1, Majorana (p, l2), Coupling.Spinor ->
          Fermion (p, l1 @ l2)
      | Boson l1, Fermion (p, l2), Coupling.ConjSpinor
      | Boson l1, AntiFermion (p, l2), Coupling.ConjSpinor 
      | Fermion (p, l1), Boson l2, Coupling.ConjSpinor 
      | AntiFermion (p, l1), Boson l2, Coupling.ConjSpinor 
      | Majorana (p, l1), Boson l2, Coupling.ConjSpinor 
      | Boson l1, Majorana (p, l2), Coupling.ConjSpinor ->
          AntiFermion (p, l1 @ l2)
      | Boson l1, Fermion (p, l2), Coupling.Vectorspinor
      | Boson l1, AntiFermion (p, l2), Coupling.Vectorspinor
      | Fermion (p, l1), Boson l2, Coupling.Vectorspinor
      | AntiFermion (p, l1), Boson l2, Coupling.Vectorspinor
      | Majorana (p, l1), Boson l2, Coupling.Vectorspinor
      | Boson l1, Majorana (p, l2), Coupling.Vectorspinor ->
          Majorana (p, l1 @ l2)
      | Boson l1, Boson l2, _ -> Boson (l1 @ l2)
      | AntiFermion (p1, l1), Fermion (p2, l2), _ 
      | Fermion (p1, l1), AntiFermion (p2, l2), _ 
      | Fermion (p1, l1), Fermion (p2, l2), _ 
      | AntiFermion (p1, l1), AntiFermion (p2, l2), _ 
      | Fermion (p1, l1), Majorana (p2, l2), _ 
      | Majorana (p1, l1), Fermion (p2, l2), _ 
      | AntiFermion (p1, l1), Majorana (p2, l2), _ 
      | Majorana (p1, l1), AntiFermion (p2, l2), _ 
      | Majorana (p1, l1), Majorana (p2, l2), _ ->
          Boson ([p2; p1] @ l1 @ l2)
      | Boson l1, Majorana (p, l2), _ -> Majorana (p, l1 @ l2)
      | Boson l1, Fermion (p, l2), _  -> Fermion (p, l1 @ l2)
      | Boson l1, AntiFermion (p, l2), _ -> AntiFermion (p, l1 @ l2)
      | Fermion (p, l1), Boson l2, _ -> Fermion (p, l1 @ l2)
      | AntiFermion (p, l1), Boson l2, _ -> AntiFermion (p, l1 @ l2)
      | Majorana (p, l1), Boson l2, _ -> Majorana (p, l1 @ l2)

(*i These are the old Impossible raising rules. We keep them to ask Ohl
    what the generalized topologies do and if our stat_fuse does the right
    for 4-vertices with

      | Boson l1, AntiFermion (p, l2), _
      | Fermion (p, l1), Boson l2, _
      | AntiFermion (p, l1), Boson l2, _
      | Majorana (p, l1), Boson l2, _
      | Boson l1, Majorana (p, l2), _ ->
          raise Impossible
i*)

    let permutation lines = fst(Combinatorics.sort_signed compare lines)   

    let stat_sign = function
      | Boson lines -> permutation lines
      | Fermion (p, lines) -> permutation (p :: lines)
      | AntiFermion (pbar, lines) -> permutation (pbar :: lines)
      | Majorana (pm, lines) -> permutation (pm :: lines)  

  end

module Binary_Majorana =
  Make(Tuple.Binary)(Stat_Majorana)(Topology.Binary)

module Nary (B: Tuple.Bound) =
  Make(Tuple.Nary(B))(Stat_Dirac)(Topology.Nary(B))
module Nary_Majorana (B: Tuple.Bound) =
  Make(Tuple.Nary(B))(Stat_Majorana)(Topology.Nary(B))

module Mixed23 =
  Make(Tuple.Mixed23)(Stat_Dirac)(Topology.Mixed23)
module Mixed23_Majorana =
  Make(Tuple.Mixed23)(Stat_Majorana)(Topology.Mixed23)

module Helac (B: Tuple.Bound) =
  Make(Tuple.Nary(B))(Stat_Dirac)(Topology.Helac(B))
module Helac_Majorana (B: Tuple.Bound) =
  Make(Tuple.Nary(B))(Stat_Majorana)(Topology.Helac(B))

(* \thocwmodulesection{Multiple Amplitudes} *)

module type Multi =
  sig
    val options : Options.t
    type flavor
    type p
    type amplitude
    type selectors
    type amplitudes
    val no_tensor : bool ref
    val amplitudes : bool -> selectors ->
      flavor list list -> flavor list list -> amplitudes
    val incoming : amplitudes -> flavor list list
    val outgoing : amplitudes -> flavor list list
    val all : amplitudes -> amplitude list
    val allowed : amplitudes -> amplitude list
    val forbidden : amplitudes -> amplitude list
    val allowed_in_out : amplitudes -> flavor list list
    val allowed_in : amplitudes -> flavor list list
    val allowed_out : amplitudes -> flavor list list
    val forbidden_in_out : amplitudes -> flavor list list
    val forbidden_in : amplitudes -> flavor list list
    val forbidden_out : amplitudes -> flavor list list
    val is_allowed : amplitudes -> flavor list -> bool
    val multiplicities_in_out : amplitudes -> flavor list -> int
    val multiplicities_in : amplitudes -> flavor list -> int
    val multiplicities_out : amplitudes -> flavor list -> int
  end

module type MultiMaker = functor (F : T) ->
  Multi with type flavor = F.flavor and type p = F.p
  and type amplitude = F.amplitude
  and type selectors = F.selectors

module Multi (F : T) =
  struct

    let no_tensor = ref false
    let tensor_in_out = ref false

    let options = Options.create
      [ "no_tensor", Arg.Set no_tensor, "do not use tensor products";
        "tensor_in_out", Arg.Set tensor_in_out, "combine all in with all out states"]

    type flavor = F.flavor
    type p = F.p
    type amplitude = F.amplitude
    type selectors = F.selectors

    type amplitudes =
        { incoming : flavor list list;
          outgoing : flavor list list;
          all : amplitude list;
          allowed : amplitude list;
          forbidden : amplitude list;
          allowed_in_out : flavor list list;
          allowed_in : flavor list list;
          allowed_out : flavor list list;
          forbidden_in_out : flavor list list;
          forbidden_in : flavor list list;
          forbidden_out : flavor list list;
          is_allowed : flavor list -> bool;
          multiplicities_in_out : flavor list -> int;
          multiplicities_in : flavor list -> int;
          multiplicities_out : flavor list -> int }

    let incoming a = a.incoming
    let outgoing a = a.outgoing
    let all a = a.all
    let allowed a = a.allowed
    let forbidden a = a.forbidden
    let allowed_in_out a = a.allowed_in_out
    let allowed_in a = a.allowed_in
    let allowed_out a = a.allowed_out
    let forbidden_in_out a = a.forbidden_in_out
    let forbidden_in a = a.forbidden_in
    let forbidden_out a = a.forbidden_out
    let is_allowed a = a.is_allowed
    let multiplicities_in_out a = a.multiplicities_in_out
    let multiplicities_in a = a.multiplicities_in
    let multiplicities_out a = a.multiplicities_out

    module SFM =
      Map.Make (struct type t = flavor list let compare = compare end)

    let add_sorted_flavors f map =
      let sf = List.sort compare f in
      SFM.add sf (succ (try SFM.find sf map with Not_found -> 0)) map

    let sorted_flavor_count flavors =
      List.fold_left (fun m f -> add_sorted_flavors f m) SFM.empty flavors
    
    let add_sorted_flavors2 (fi, fo) map =
      let sf = List.sort compare fi @ List.sort compare fo in
      SFM.add sf (succ (try SFM.find sf map with Not_found -> 0)) map

    let sorted_flavor_count2 flavors2 =
      List.fold_left (fun m f2 -> add_sorted_flavors2 f2 m) SFM.empty flavors2
    
    module FS = Set.Make (struct type t = flavor list let compare = compare end)

    let flavor_set flavors =
      List.fold_left (fun s f -> FS.add f s) FS.empty flavors

    let id x = x

    let amplitudes goldstones select_wf fin fout =
      let processes =
        if !no_tensor then 
          List.map2 (fun fi fo -> (fi, fo)) 
            (Product.thread fin) (Product.thread fout) 
        else 
          if !tensor_in_out then
            Product.list2 (fun fi fo -> (fi, fo))
              (Product.thread fin) (Product.thread fout)
          else
            Product.list2 (fun fi fo -> (fi, fo))
              (Product.list id fin) (Product.list id fout) in
      let all =
        List.map (fun (fi, fo) -> F.amplitude goldstones select_wf fi fo) processes in
      let allowed, forbidden = List.partition F.allowed all in
      let allowed_in_out =
        List.map (fun a -> F.incoming a @ F.outgoing a) allowed
      and forbidden_in_out =
        List.map (fun a -> F.incoming a @ F.outgoing a) forbidden in
      let all_in_set = flavor_set (List.map F.incoming all)
      and all_out_set = flavor_set (List.map F.outgoing all)
      and allowed_in_set = flavor_set (List.map F.incoming allowed)
      and allowed_out_set = flavor_set (List.map F.outgoing allowed)
      and allowed_set = flavor_set allowed_in_out in
      let allowed_in = FS.elements allowed_in_set
      and allowed_out = FS.elements allowed_out_set in
      let is_allowed f = FS.mem f allowed_set in
      let multiplicities_in_out_map =
        sorted_flavor_count2
          (List.map (fun a -> (F.incoming a, F.outgoing a)) allowed)
      and multiplicities_in_map = sorted_flavor_count allowed_in
      and multiplicities_out_map = sorted_flavor_count allowed_out in
      let multiplicities_in_out f =
        let fi, fo = ThoList.splitn (List.length fin) f in
        let fs = List.sort compare fi @ List.sort compare fo in
        SFM.find fs multiplicities_in_out_map
      and multiplicities_in f =
        SFM.find (List.sort compare f) multiplicities_in_map
      and multiplicities_out f =
        SFM.find (List.sort compare f) multiplicities_out_map in
      { incoming = fin;
        outgoing = fout;
        all = all;
        allowed = allowed;
        forbidden = forbidden;
        allowed_in_out = allowed_in_out;
        allowed_in = allowed_in;
        allowed_out = allowed_out;
        forbidden_in_out = forbidden_in_out;
        forbidden_in = FS.elements (FS.diff all_in_set allowed_in_set);
        forbidden_out = FS.elements (FS.diff all_out_set allowed_out_set);
        is_allowed = is_allowed;
        multiplicities_in_out = multiplicities_in_out;
        multiplicities_in = multiplicities_in;
        multiplicities_out = multiplicities_out }

  end

(*i
 *  Local Variables:
 *  mode:caml
 *  indent-tabs-mode:nil
 *  page-delimiter:"^(\\* .*\n"
 *  End:
i*)
