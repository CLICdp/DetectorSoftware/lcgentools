(* $Id: models.ml 326 2008-08-17 04:49:19Z reuter $ *)
(* Copyright (C) 2000-2004 by Thorsten Ohl <ohl@physik.uni-wuerzburg.de> et al.
   O'Mega is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by 
   the Free Software Foundation; either version 2, or (at your option)
   any later version.
   O'Mega is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.  *)  

let rcs_file = RCS.parse "Models" ["Lagragians"]
    { RCS.revision = "$Revision: 326 $";
      RCS.date = "$Date: 2008-08-17 06:49:19 +0200 (Sun, 17 Aug 2008) $";
      RCS.author = "$Author: reuter $";
      RCS.source
        = "$Source: /home/sources/ohl/ml/omega/src/models.ml,v $" }

(* \thocwmodulesection{Compilation} *)

(* Flavors and coupling constants:  flavors can be tested for equality
   and charge conjugation is defined.  *)

module type Flavor =
  sig
    type f
    type c
    val compare : f -> f -> int
    val conjugate : f -> f
  end

(* Compiling fusions from a list of vertices:  *)

module type Fusions =
  sig
    type t
    type f
    type c
    val fuse2 : t -> f -> f -> (f * c Coupling.t) list
    val fuse3 : t -> f -> f -> f -> (f * c Coupling.t) list
    val fuse : t -> f list -> (f * c Coupling.t) list
    val of_vertices :
        (((f * f * f) * c Coupling.vertex3 * c) list
           * ((f * f * f * f) * c Coupling.vertex4 * c) list
           * (f list * c Coupling.vertexn * c) list) -> t
  end

module Fusions (F : Flavor) : Fusions with type f = F.f and type c = F.c =
  struct

    type f = F.f
    type c = F.c

    module F2 =
      struct
        type t = f * f
        let hash = Hashtbl.hash
        let compare (f1, f2) (f1', f2') =
          let c1 = F.compare f1 f1' in
          if c1 <> 0 then
            c1
          else
            F.compare f2 f2'
        let equal f f' = compare f f' = 0
      end

    module F3 =
      struct
        type t = f * f * f
        let hash = Hashtbl.hash
        let compare (f1, f2, f3) (f1', f2', f3') =
          let c1 = F.compare f1 f1' in
          if c1 <> 0 then
            c1
          else
            let c2 = F.compare f2 f2' in
            if c2 <> 0 then
              c2
            else
              F.compare f3 f3'
        let equal f f' = compare f f' = 0
      end

    module Fn =
      struct
        type t = f list
        let hash = Hashtbl.hash
        let compare f f' = ThoList.compare ~cmp:F.compare f f'
        let equal f f' = compare f f' = 0
      end

    module H2 = Hashtbl.Make (F2)
    module H3 = Hashtbl.Make (F3)
    module Hn = Hashtbl.Make (Fn)

    type t =
        { v3 : (f * c Coupling.t) list H2.t;
          v4 : (f * c Coupling.t) list H3.t;
          vn : (f * c Coupling.t) list Hn.t }

    let fuse2 table f1 f2 =
      try
        H2.find table.v3 (f1, f2)
      with
      | Not_found -> []

    let fuse3 table f1 f2 f3 =
      try
        H3.find table.v4 (f1, f2, f3)
      with
      | Not_found -> []

    let fusen table f =
      try
        Hn.find table.vn f
      with
      | Not_found -> []

    let fuse table = function 
      | [] | [_] -> invalid_arg "Fusions().fuse"
      | [f1; f2] -> fuse2 table f1 f2
      | [f1; f2; f3] -> fuse3 table f1 f2 f3
      | f -> fusen table f

(* Note that a pair or a triplet can appear more than once
   (e.\,g.~$e^+e^-\to \gamma$ and~$e^+e^-\to Z$).  Therefore don't
   replace the entry, but augment it instead.  *)

    let add_fusion2 table f1 f2 fusions =
      H2.add table.v3 (f1, f2) (fusions :: fuse2 table f1 f2)

    let add_fusion3 table f1 f2 f3 fusions =
      H3.add table.v4 (f1, f2, f3) (fusions :: fuse3 table f1 f2 f3)

    let add_fusionn table f fusions =
      Hn.add table.vn f (fusions :: fusen table f)

(* \begin{dubious}
     Do we need to take into account the charge conjugation
     of the coupling constants here?
   \end{dubious} *)

(* If some flavors are identical, we must not introduce the
   same vertex more than once: *)

    open Coupling

    let permute3 (f1, f2, f3) =
      [ (f1, f2), F.conjugate f3, F12;
        (f2, f1), F.conjugate f3, F21;
        (f2, f3), F.conjugate f1, F23;
        (f3, f2), F.conjugate f1, F32;
        (f3, f1), F.conjugate f2, F31;
        (f1, f3), F.conjugate f2, F13 ]

(* Here we add identical permutations of pairs only once: *)

    module F2' = Set.Make (F2)

    let add_permute3 table v c set ((f1, f2 as f12), f, p) =
      if F2'.mem f12 set then
        set
      else begin
        add_fusion2 table f1 f2 (f, V3 (v, p, c));
        F2'.add f12 set
      end

    let add_vertex3 table (f123, v, c) =
      ignore (List.fold_left (fun set f -> add_permute3 table v c set f)
                F2'.empty (permute3 f123))

(* \begin{dubious}
     Handling all the cases explicitely is OK for cubic vertices, but starts
     to become questionable already for quartic couplings.  The advantage
     remains that we can check completeness in [Targets].
   \end{dubious} *)

    let permute4 (f1, f2, f3, f4) =
      [ (f1, f2, f3), F.conjugate f4, F123;
        (f2, f3, f1), F.conjugate f4, F231;
        (f3, f1, f2), F.conjugate f4, F312;
        (f2, f1, f3), F.conjugate f4, F213;
        (f3, f2, f1), F.conjugate f4, F321;
        (f1, f3, f2), F.conjugate f4, F132;
        (f1, f2, f4), F.conjugate f3, F124;
        (f2, f4, f1), F.conjugate f3, F241;
        (f4, f1, f2), F.conjugate f3, F412;
        (f2, f1, f4), F.conjugate f3, F214;
        (f4, f2, f1), F.conjugate f3, F421;
        (f1, f4, f2), F.conjugate f3, F142;
        (f1, f3, f4), F.conjugate f2, F134;
        (f3, f4, f1), F.conjugate f2, F341;
        (f4, f1, f3), F.conjugate f2, F413;
        (f3, f1, f4), F.conjugate f2, F314;
        (f4, f3, f1), F.conjugate f2, F431;
        (f1, f4, f3), F.conjugate f2, F143;
        (f2, f3, f4), F.conjugate f1, F234;
        (f3, f4, f2), F.conjugate f1, F342;
        (f4, f2, f3), F.conjugate f1, F423;
        (f3, f2, f4), F.conjugate f1, F324;
        (f4, f3, f2), F.conjugate f1, F432;
        (f2, f4, f3), F.conjugate f1, F243 ]

(* Add identical permutations of triplets only once: *)

    module F3' = Set.Make (F3)

    let add_permute4 table v c set ((f1, f2, f3 as f123), f, p) =
      if F3'.mem f123 set then
        set
      else begin
        add_fusion3 table f1 f2 f3 (f, V4 (v, p, c));
        F3'.add f123 set
      end

    let add_vertex4 table (f1234, v, c) =
      ignore (List.fold_left (fun set f -> add_permute4 table v c set f)
                F3'.empty (permute4 f1234))

    let of_vertices (vlist3, vlist4, vlistn) =
      match vlistn with
      | [] ->
          let table =
            { v3 = H2.create 37; v4 = H3.create 37; vn = Hn.create 37 } in
          List.iter (add_vertex3 table) vlist3;
          List.iter (add_vertex4 table) vlist4;
          table
      | _ -> failwith "Models.Fusions.of_vertices: incomplete"

  end

(* \thocwmodulesection{Mutable Models} *)

module Mutable (FGC : sig type f and g and c end) =
  struct
    type flavor = FGC.f
    type gauge = FGC.g
    type constant = FGC.c

    let options = Options.empty

    exception Uninitialized of string
    let unitialized name =
      raise (Uninitialized name)
      
(* Note that [lookup] works, by the magic of currying, for any arity.  But
   we need to supply one argument to delay evaluation. *)

(* Also note that the references are \emph{not} shared among results
   of functor applications.  Simple module renaming causes sharing.  *)
    let declare template =
      let reference = ref template in
      let update fct = reference := fct
      and lookup arg = !reference arg in
      (update, lookup)

    let set_color, color =
      declare (fun f -> unitialized "color")
    let set_pdg, pdg =
      declare (fun f -> unitialized "pdg")
    let set_lorentz, lorentz =
      declare (fun f -> unitialized "lorentz")
    let set_propagator, propagator =
      declare (fun f -> unitialized "propagator")
    let set_width, width =
      declare (fun f -> unitialized "width")
    let set_goldstone, goldstone =
      declare (fun f -> unitialized "goldstone")
    let set_conjugate, conjugate =
      declare (fun f -> unitialized "conjugate")
    let set_fermion, fermion =
      declare (fun f -> unitialized "fermion")
    let set_colsymm, colsymm =
      declare (fun f -> unitialized "colsymm")
    let set_max_degree, max_degree =
      declare (fun () -> unitialized "max_degree")
    let set_vertices, vertices =
      declare (fun () -> unitialized "vertices")
    let set_fuse2, fuse2 =
      declare (fun f1 f2 -> unitialized "fuse2")
    let set_fuse3, fuse3 =
      declare (fun f1 f2 f3 -> unitialized "fuse3")
    let set_fuse, fuse =
      declare (fun f -> unitialized "fuse")
    let set_flavors, flavors =
      declare (fun () -> [])
    let set_external_flavors, external_flavors =
      declare (fun () -> [("unitialized", [])])
    let set_parameters, parameters =
      declare (fun f -> unitialized "parameters")
    let set_flavor_of_string, flavor_of_string =
      declare (fun f -> unitialized "flavor_of_string")
    let set_flavor_to_string, flavor_to_string =
      declare (fun f -> unitialized "flavor_to_string")
    let set_flavor_to_TeX, flavor_to_TeX =
      declare (fun f -> unitialized "flavor_to_TeX")
    let set_flavor_symbol, flavor_symbol =
      declare (fun f -> unitialized "flavor_symbol")
    let set_gauge_symbol, gauge_symbol =
      declare (fun f -> unitialized "gauge_symbol")
    let set_mass_symbol, mass_symbol =
      declare (fun f -> unitialized "mass_symbol")
    let set_width_symbol, width_symbol =
      declare (fun f -> unitialized "width_symbol")
    let set_constant_symbol, constant_symbol =
      declare (fun f -> unitialized "constant_symbol")

    let setup ~color ~pdg ~lorentz ~propagator ~width ~goldstone
        ~conjugate ~fermion ~colsymm ~max_degree ~vertices 
        ~fuse:(fuse2, fuse3, fusen)
        ~flavors ~parameters ~flavor_of_string
        ~flavor_to_string ~flavor_to_TeX ~flavor_symbol 
        ~gauge_symbol ~mass_symbol
        ~width_symbol ~constant_symbol =
      set_color color;
      set_pdg pdg;
      set_lorentz lorentz;
      set_propagator propagator;
      set_width width;
      set_goldstone goldstone;
      set_conjugate conjugate;
      set_fermion fermion;
      set_colsymm colsymm;
      set_max_degree (fun () -> max_degree);
      set_vertices vertices;
      set_fuse2 fuse2;
      set_fuse3 fuse3;
      set_fuse fusen;
      set_external_flavors (fun f -> flavors);
      let flavors = ThoList.flatmap snd flavors in
      set_flavors (fun f -> flavors);
      set_parameters parameters;
      set_flavor_of_string flavor_of_string;
      set_flavor_to_string flavor_to_string;
      set_flavor_to_TeX flavor_to_TeX;
      set_flavor_symbol flavor_symbol;
      set_gauge_symbol gauge_symbol;
      set_mass_symbol mass_symbol;
      set_width_symbol width_symbol;
      set_constant_symbol constant_symbol

    let rcs = RCS.rename rcs_file "Models.Mutable" ["Mutable Model"]
  end

(* \thocwmodulesection{$\phi^3$} *)

module Phi3 =
  struct
    let rcs = RCS.rename rcs_file "Models.Phi3"
        ["phi**3 with a single flavor"]

    open Coupling

    let options = Options.empty

    type flavor = Phi
    let external_flavors () = [ "", [Phi]]
    let flavors () = ThoList.flatmap snd (external_flavors ())

    type gauge = unit
    type constant = G

    let lorentz _ = Scalar
    let color _ = Color.Singlet
    let propagator _ = Prop_Scalar
    let width _ = Timelike
    let goldstone _ = None
    let conjugate f = f
    let fermion _ = 0
    let colsymm _ = (0, false), (0, false)

    module F = Fusions (struct
      type f = flavor
      type c = constant
      let compare = compare
      let conjugate = conjugate
    end)

    let vertices () =
      ([(Phi, Phi, Phi), Scalar_Scalar_Scalar 1, G], [], [])

    let table = F.of_vertices (vertices ())
    let fuse2 = F.fuse2 table
    let fuse3 = F.fuse3 table
    let fuse = F.fuse table
    let max_degree () = 3
    let parameters () = { input = [G, 1.0]; derived = []; derived_arrays = [] }

    let flavor_of_string = function
      | "p" -> Phi
      | _ -> invalid_arg "Models.Phi3.flavor_of_string"

    let flavor_to_string Phi = "phi"
    let flavor_to_TeX Phi = "\\phi"
    let flavor_symbol Phi = "phi"

    let gauge_symbol () =
      failwith "Models.Phi3.gauge_symbol: internal error"

    let pdg _ = 1
    let mass_symbol _ = "m"
    let width_symbol _ = "w"
    let constant_symbol G = "g"

  end

(* \thocwmodulesection{$\lambda_3\phi^3+\lambda_4\phi^4$} *)

module Phi4 =
  struct
    let rcs = RCS.rename rcs_file "Models.Phi4"
        ["phi**4 with a single flavor"]

    open Coupling

    let options = Options.empty

    type flavor = Phi
    let external_flavors () = [ "", [Phi]]
    let flavors () = ThoList.flatmap snd (external_flavors ())

    type gauge = unit
    type constant = G3 | G4

    let lorentz _ = Scalar
    let color _ = Color.Singlet
    let propagator _ = Prop_Scalar
    let width _ = Timelike
    let goldstone _ = None
    let conjugate f = f
    let fermion _ = 0
    let colsymm _ = (0, false), (0, false)

    module F = Fusions (struct
      type f = flavor
      type c = constant
      let compare = compare
      let conjugate = conjugate
    end)

    let vertices () =
      ([(Phi, Phi, Phi), Scalar_Scalar_Scalar 1, G3],
       [(Phi, Phi, Phi, Phi), Scalar4 1, G4], [])

    let fuse2 _ = failwith "Models.Phi4.fuse2"
    let fuse3 _ = failwith "Models.Phi4.fuse3"
    let fuse = function
      | [] | [_] -> invalid_arg "Models.Phi4.fuse"
      | [_; _] -> [Phi, V3 (Scalar_Scalar_Scalar 1, F23, G3)]
      | [_; _; _] -> [Phi, V4 (Scalar4 1, F234, G4)]
      | _ -> []
    let max_degree () = 4
    let parameters () =
      { input = [G3, 1.0; G4, 1.0]; derived = []; derived_arrays = [] }

    let flavor_of_string = function
      | "p" -> Phi
      | _ -> invalid_arg "Models.Phi4.flavor_of_string"

    let flavor_to_string Phi = "phi"
    let flavor_to_TeX Phi = "\\phi"
    let flavor_symbol Phi = "phi"

    let gauge_symbol () =
      failwith "Models.Phi4.gauge_symbol: internal error"

    let pdg _ = 1
    let mass_symbol _ = "m"
    let width_symbol _ = "w"
    let constant_symbol = function
      | G3 -> "g3"
      | G4 -> "g4"

  end

(* \thocwmodulesection{Quantum Electro Dynamics} *)

module QED =
  struct
    let rcs = RCS.rename rcs_file "Models.QED"
        ["QED with two leptonic flavors"]

    open Coupling

    let options = Options.empty

    type flavor =
      | Electron | Positron
      | Muon | AntiMuon
      | Tau | AntiTau
      | Photon

    let external_flavors () =
      [ "Leptons", [Electron; Positron; Muon; AntiMuon; Tau; AntiTau];
        "Gauge Bosons", [Photon] ]
    let flavors () = ThoList.flatmap snd (external_flavors ())

    type gauge = unit
    type constant = Q

    let lorentz = function
      | Electron | Muon | Tau -> Spinor
      | Positron | AntiMuon | AntiTau -> ConjSpinor
      | Photon -> Vector

    let color _ = Color.Singlet

    let propagator = function
      | Electron | Muon | Tau -> Prop_Spinor
      | Positron | AntiMuon | AntiTau -> Prop_ConjSpinor
      | Photon -> Prop_Feynman

    let width _ = Timelike

    let goldstone _ =
      None

    let conjugate = function
      | Electron -> Positron | Positron -> Electron
      | Muon -> AntiMuon | AntiMuon -> Muon
      | Tau -> AntiTau | AntiTau -> Tau
      | Photon -> Photon

    let fermion = function
      | Electron | Muon | Tau -> 1
      | Positron | AntiMuon | AntiTau -> -1
      | Photon -> 0

    let colsymm _ = (0, false), (0, false)

    module F = Fusions (struct
      type f = flavor
      type c = constant
      let compare = compare
      let conjugate = conjugate
    end)

    let vertices () = 
      ([(Positron, Photon, Electron), FBF (1, Psibar, V, Psi), Q;
        (AntiMuon, Photon, Muon), FBF (1, Psibar, V, Psi), Q;
        (AntiTau, Photon, Tau), FBF (1, Psibar, V, Psi), Q], [], [])

    let table = F.of_vertices (vertices ())
    let fuse2 = F.fuse2 table
    let fuse3 = F.fuse3 table
    let fuse = F.fuse table
    let max_degree () = 3

    let parameters () = { input = [Q, 1.0]; derived = []; derived_arrays = [] }

    let flavor_of_string = function
      | "e-" -> Electron | "e+" -> Positron
      | "m-" -> Muon | "m+" -> AntiMuon
      | "t-" -> Tau | "t+" -> AntiTau
      | "A" -> Photon
      | _ -> invalid_arg "Models.QED.flavor_of_string"

    let flavor_to_string = function
      | Electron -> "e-" | Positron -> "e+"
      | Muon -> "m-" | AntiMuon -> "m+"
      | Tau -> "t-" | AntiTau -> "t+"
      | Photon -> "A"

    let flavor_to_TeX = function
      | Electron -> "e^-" | Positron -> "e^+"
      | Muon -> "\\mu^-" | AntiMuon -> "\\mu^+"
      | Tau -> "^\\tau^-" | AntiTau -> "\\tau+^"
      | Photon -> "\\gamma"

    let flavor_symbol = function
      | Electron -> "ele" | Positron -> "pos"
      | Muon -> "muo" | AntiMuon -> "amu"
      | Tau -> "tau" | AntiTau -> "ata"
      | Photon -> "gam"

    let gauge_symbol () =
      failwith "Models.QED.gauge_symbol: internal error"

    let pdg = function
      | Electron -> 11 | Positron -> -11
      | Muon -> 13 | AntiMuon -> -13
      | Tau -> 15 | AntiTau -> -15
      | Photon -> 22

    let mass_symbol f = 
      "mass(" ^ string_of_int (abs (pdg f)) ^ ")"

    let width_symbol f =
      "width(" ^ string_of_int (abs (pdg f)) ^ ")"

    let constant_symbol = function
      | Q -> "qlep"
  end

(* \thocwmodulesection{Quantum Chromo Dynamics} *)

module YM =
  struct
    let rcs = RCS.rename rcs_file "Models.YM"
        ["incomplete Yang-Mills theory with one quark flavor"]

    open Coupling

    let options = Options.empty

    type flavor = Quark | Antiquark | Gluon | Gluon_aux
    let external_flavors () =
      [ "Quarks", [Quark; Antiquark];
        "Gauge Bosons", [Gluon] ]
    let flavors () = ThoList.flatmap snd (external_flavors ()) @ [ Gluon_aux ]

    type gauge = unit
    type constant = G

    let lorentz = function
      | Quark -> Spinor
      | Antiquark -> ConjSpinor
      | Gluon -> Vector
      | Gluon_aux -> Tensor_1

    let color = function 
      | Quark -> Color.SUN 3
      | Antiquark -> Color.SUN (-3)
      | Gluon | Gluon_aux -> Color.AdjSUN 3

    let propagator = function
      | Quark -> Prop_Spinor
      | Antiquark -> Prop_ConjSpinor
      | Gluon -> Prop_Feynman
      | Gluon_aux -> Aux_Tensor_1

    let width _ = Timelike

    let goldstone _ =
      None

    let conjugate = function
      | Quark -> Antiquark
      | Antiquark -> Quark
      | Gluon -> Gluon
      | Gluon_aux -> Gluon_aux

    let fermion = function
      | Quark -> 1
      | Antiquark -> -1
      | Gluon | Gluon_aux -> 0

    let colsymm _ = (0, false), (0, false)

    module F = Fusions (struct
      type f = flavor
      type c = constant
      let compare = compare
      let conjugate = conjugate
    end)

    let vertices () =
      ([(Antiquark, Gluon, Quark), FBF (1, Psibar, V, Psi), G;
        (Gluon, Gluon, Gluon), Gauge_Gauge_Gauge 1, G;
        (Gluon_aux, Gluon, Gluon), Aux_Gauge_Gauge 1, G], [], [])

(*i
    let vertices () =
      ([(Antiquark, Gluon, Quark), FBF (1, Psibar, V, Psi), G;
        (Gluon, Gluon, Gluon), Gauge_Gauge_Gauge 1, G],
       [(Gluon, Gluon, Gluon, Gluon), Vector4 [1, C_12_34], G], [])
i*)

    let table = F.of_vertices (vertices ())
    let fuse2 = F.fuse2 table
    let fuse3 = F.fuse3 table
    let fuse = F.fuse table
    let max_degree () = 4
        
    let parameters () =
      { input = [G, 1.0];
        derived = [];
        derived_arrays = [] }

    let flavor_of_string = function
      | "q" -> Quark
      | "Q" -> Antiquark
      | "g" -> Gluon
      | _ -> invalid_arg "Models.YM.flavor_of_string"

    let flavor_to_string = function
      | Quark -> "q"
      | Antiquark -> "Q"
      | Gluon -> "g"
      | Gluon_aux -> "x"

    let flavor_to_TeX = function
      | Quark -> "q"
      | Antiquark -> "\\bar{q}"
      | Gluon -> "g"
      | Gluon_aux -> "x"

    let flavor_symbol = function
      | Quark -> "qu"
      | Antiquark -> "aq"
      | Gluon -> "gl"
      | Gluon_aux -> "gl_aux"

    let gauge_symbol () =
      failwith "Models.YM.gauge_symbol: internal error"

    let pdg = function
      | Quark -> 1
      | Antiquark -> -1
      | Gluon -> 21
      | Gluon_aux -> 0

    let mass_symbol = function
      | Quark -> "mass(1)"
      | Antiquark -> "mass(1)"
      | Gluon | Gluon_aux -> "mass(21)"

    let width_symbol = function
      | Quark -> "width(1)"
      | Antiquark -> "width(1)"
      | Gluon | Gluon_aux -> "width(21)"

    let constant_symbol = function
      | G -> "g"

  end

(* \thocwmodulesection{Complete Minimal Standard Model (Unitarity Gauge)} *)

module type SM_flags =
  sig
    val triple_anom : bool
    val quartic_anom : bool
    val higgs_anom : bool
    val k_matrix : bool
    val ckm_present : bool
    val col_whiz : bool 
  end

module SM_no_anomalous : SM_flags =
  struct
    let triple_anom = false
    let quartic_anom = false
    let higgs_anom = false
    let k_matrix = false
    let ckm_present = false
    let col_whiz = false
  end

module SM_no_anomalous_ckm : SM_flags =
  struct
    let triple_anom = false
    let quartic_anom = false
    let higgs_anom = false
    let k_matrix = false
    let ckm_present = true
    let col_whiz = false
  end

module SM_anomalous : SM_flags =
  struct
    let triple_anom = true
    let quartic_anom = true
    let higgs_anom = true
    let k_matrix = false
    let ckm_present = false
    let col_whiz = false
  end

module SM_anomalous_ckm : SM_flags =
  struct
    let triple_anom = true
    let quartic_anom = true
    let higgs_anom = true
    let k_matrix = false
    let ckm_present = true
    let col_whiz = false
  end

module SM_k_matrix : SM_flags =
  struct
    let triple_anom = false
    let quartic_anom = true
    let higgs_anom = false
    let k_matrix = true
    let ckm_present = false
    let col_whiz = false
  end

module SM_whizcol : SM_flags =
  struct
    let triple_anom = false
    let quartic_anom = false
    let higgs_anom = false
    let k_matrix = false
    let ckm_present = false
    let col_whiz = true
  end

module SM_whizcol_ckm : SM_flags =
  struct
    let triple_anom = false
    let quartic_anom = false
    let higgs_anom = false
    let k_matrix = false
    let ckm_present = true
    let col_whiz = true
  end

module SM_whizcol_anom : SM_flags =
  struct
    let triple_anom = true
    let quartic_anom = true
    let higgs_anom = true
    let k_matrix = false
    let ckm_present = false
    let col_whiz = true
  end

module SM_whizcol_anom_ckm : SM_flags =
  struct
    let triple_anom = true
    let quartic_anom = true
    let higgs_anom = true
    let k_matrix = false
    let ckm_present = true
    let col_whiz = true
  end

module SM_whizcol_k_matrix : SM_flags =
  struct
    let triple_anom = false
    let quartic_anom = true
    let higgs_anom = false
    let k_matrix = true
    let ckm_present = false
    let col_whiz = true
  end

module SM3 (Flags : SM_flags) =
  struct
    let rcs = RCS.rename rcs_file "Models.SM3"
        [ "minimal electroweak standard model in unitarity gauge";
          "with emulation of 4-point vertices; no CKM matrix" ]

    open Coupling

    let default_width = ref Timelike
    let use_fudged_width = ref false

    let options = Options.create
      [ "constant_width", Arg.Unit (fun () -> default_width := Constant),
        "use constant width (also in t-channel)";
        "fudged_width", Arg.Set use_fudged_width,
        "use fudge factor for charge particle width";
        "custom_width", Arg.String (fun f -> default_width := Custom f),
        "use custom width";
        "cancel_widths", Arg.Unit (fun () -> default_width := Vanishing),
        "use vanishing width"]

    type matter_field = L of int | N of int | U of int | D of int
    type gauge_boson = Ga | Wp | Wm | Z | Gl
    type other =
      | XWp | XWm | XW3 | XGl
      | Phip | Phim | Phi0 | H | XH
      | XH_W | XH_W' | XH_Z | XH_Z'
      | XSWm | XSWp | XSWpp | XSWmm
      | XSWZ0 | XSZW0 | XSW3 | XSZZ
      | XDH_W | XDH_W' | XDH_Z | XDH_Z'
      | XDH_Wm | XDH_Wp | XDH_Z''
      | XDH2

    type flavor = M of matter_field | G of gauge_boson | O of other
    let matter_field f = M f
    let gauge_boson f = G f
    let other f = O f

    type field =
      | Matter of matter_field
      | Gauge of gauge_boson
      | Other of other

    let field = function
      | M f -> Matter f
      | G f -> Gauge f
      | O f -> Other f

    type gauge = unit

    let gauge_symbol () =
      failwith "Models.SM3.gauge_symbol: internal error"

(* The auxiliary fields [XH_W] and [XH_W'] are
   mutual charge conjugates.  This way the vertex $W^+_\mu W^{-,\mu}HH$
   can be split into $W^+_\mu W^{-,\mu}X_{HW}$ and $X_{HW}^*HH$ without
   introducing the additional $W^+_\mu W^{-,\mu}W^+_\nu W^{-,\nu}$ and $HHHH$
   couplings that a neutral auxiliary field would produce. *)

    let family n = List.map matter_field [ L n; N n; U n; D n ]

    let external_flavors () =
      [ "1st Generation", ThoList.flatmap family [1; -1];
        "2nd Generation", ThoList.flatmap family [2; -2];
        "3rd Generation", ThoList.flatmap family [3; -3];
        "Gauge Bosons", List.map gauge_boson [Ga; Z; Wp; Wm; Gl];
        "Higgs", List.map other [H];
        "Goldstone Bosons", List.map other [Phip; Phim; Phi0] ]

    let flavors () = 
      ThoList.flatmap snd (external_flavors ()) @
      List.map other 
        [ XWp; XWm; XW3; XGl; XH; XH_W; XH_W'; XH_Z; XH_Z';
          XSWm; XSWp; XSWpp; XSWmm; XSWZ0; XSZW0; XSW3; XSZZ;
          XDH_W; XDH_W'; XDH_Z; XDH_Z';
          XDH_Wm; XDH_Wp; XDH_Z''; XDH2 ]

    let spinor n =
      if n >= 0 then
        Spinor
      else
        ConjSpinor

    let lorentz = function
      | M f ->
          begin match f with
          | L n -> spinor n | N n -> spinor n
          | U n -> spinor n | D n -> spinor n
          end
      | G f ->
          begin match f with
          | Ga | Gl -> Vector
          | Wp | Wm | Z -> Massive_Vector
          end
      | O f ->
          begin match f with
          | XWp | XWm | XW3 | XGl -> Tensor_1
          | Phip | Phim | Phi0 -> Scalar
          | H -> Scalar | XH -> Scalar
          | XH_W | XH_W' -> Scalar
          | XH_Z | XH_Z' -> Scalar
          | XSWm | XSWp | XSWpp | XSWmm
          | XSWZ0 | XSZW0 | XSW3 | XSZZ -> Scalar
          | XDH_W | XDH_W' | XDH_Z | XDH_Z'
          | XDH_Wm | XDH_Wp | XDH_Z'' | XDH2 -> Scalar
          end

    let color = function 
      | M (U n) -> Color.SUN (if n > 0 then 3 else -3)
      | M (D n) -> Color.SUN  (if n > 0 then 3 else -3)
      | G Gl -> Color.AdjSUN 3
      | _ -> Color.Singlet

    let prop_spinor n =
      if n >= 0 then
        Prop_Spinor
      else
        Prop_ConjSpinor

    let propagator = function
      | M f ->
          begin match f with
          | L n -> prop_spinor n | N n -> prop_spinor n
          | U n -> prop_spinor n | D n -> prop_spinor n
          end
      | G f ->
          begin match f with
          | Ga | Gl -> Prop_Feynman
          | Wp | Wm | Z -> Prop_Unitarity
          end
      | O f ->
          begin match f with
          | XWp | XWm | XW3 | XGl -> Aux_Tensor_1
          | Phip | Phim | Phi0 -> Only_Insertion
          | H -> Prop_Scalar | XH -> Aux_Scalar
          | XH_W | XH_W' -> Aux_Scalar
          | XH_Z | XH_Z' -> Aux_Scalar
          | XSWm | XSWp | XSWpp | XSWmm
          | XSWZ0 | XSZW0 | XSW3 | XSZZ -> Aux_Scalar
          | XDH_W | XDH_W' | XDH_Z | XDH_Z'
          | XDH_Wm | XDH_Wp | XDH_Z'' | XDH2 -> Aux_Scalar
          end

(* Optionally, ask for the fudge factor treatment for the widths of
   charged particles.  Currently, this only applies to $W^\pm$ and top. *)

    let width f =
      if !use_fudged_width then
        match f with
        | G Wp | G Wm | M (U 3) | M (U (-3)) -> Fudged
        | _ -> !default_width
      else
        !default_width

    let goldstone = function
      | G f ->
          begin match f with
          | Wp -> Some (O Phip, Coupling.Const 1)
          | Wm -> Some (O Phim, Coupling.Const 1)
          | Z -> Some (O Phi0, Coupling.Const 1)
          | _ -> None
          end
      | _ -> None

    let conjugate = function
      | M f ->
          M (begin match f with
          | L n -> L (-n) | N n -> N (-n)
          | U n -> U (-n) | D n -> D (-n)
          end)
      | G f ->
          G (begin match f with
          | Gl -> Gl | Ga -> Ga | Z -> Z
          | Wp -> Wm | Wm -> Wp
          end)
      | O f ->
          O (begin match f with
          | XWp -> XWm | XWm -> XWp
          | XW3 -> XW3 | XGl -> XGl
          | Phip -> Phim | Phim -> Phip | Phi0 -> Phi0
          | H -> H | XH -> XH
          | XH_W -> XH_W' | XH_W' -> XH_W
          | XH_Z -> XH_Z' | XH_Z' -> XH_Z
          | XSWm -> XSWp | XSWp -> XSWm 
          | XSWpp -> XSWmm | XSWmm -> XSWpp
          | XSWZ0 -> XSZW0 | XSZW0 -> XSWZ0
          | XSW3 -> XSW3 | XSZZ -> XSZZ
          | XDH_W -> XDH_W' | XDH_W' -> XDH_W
          | XDH_Z -> XDH_Z' | XDH_Z' -> XDH_Z
          | XDH_Wm -> XDH_Wp | XDH_Wp -> XDH_Wm 
          | XDH_Z'' -> XDH_Z'' | XDH2 -> XDH2
          end)

    let fermion = function
      | M f ->
          begin match f with
          | L n -> if n > 0 then 1 else -1
          | N n -> if n > 0 then 1 else -1
          | U n -> if n > 0 then 1 else -1
          | D n -> if n > 0 then 1 else -1
          end
      | G f ->
          begin match f with
          | Gl | Ga | Z | Wp | Wm -> 0
          end
      | O f ->
          begin match f with
          | XWp | XWm | XW3 | XGl -> 0
          | Phip | Phim | Phi0 -> 0
          | H | XH -> 0
          | XH_W | XH_W' | XH_Z | XH_Z' -> 0
          | XSWm | XSWp | XSWpp | XSWmm
          | XSWZ0 | XSZW0 | XSW3 | XSZZ -> 0
          | XDH_W | XDH_W' | XDH_Z | XDH_Z'
          | XDH_Wm | XDH_Wp | XDH_Z'' | XDH2 -> 0
          end

    let colsymm _ = (0, false), (0, false)


    type constant =
      | Unit | Pi | Alpha_QED | Sin2thw
      | Sinthw | Costhw | E | G_weak | Vev
      | Q_lepton | Q_up | Q_down | G_CC 
      | G_NC_neutrino | G_NC_lepton | G_NC_up | G_NC_down
      | I_Q_W | I_G_ZWW | I_G_WWW
      | I_G1_AWW | I_G1_ZWW
      | I_G1_plus_kappa_plus_G4_AWW
      | I_G1_plus_kappa_plus_G4_ZWW
      | I_G1_plus_kappa_minus_G4_AWW
      | I_G1_plus_kappa_minus_G4_ZWW
      | I_G1_minus_kappa_plus_G4_AWW
      | I_G1_minus_kappa_plus_G4_ZWW
      | I_G1_minus_kappa_minus_G4_AWW
      | I_G1_minus_kappa_minus_G4_ZWW
      | I_lambda_AWW | I_lambda_ZWW
      | G5_AWW | G5_ZWW
      | I_kappa5_AWW | I_kappa5_ZWW 
      | I_lambda5_AWW | I_lambda5_ZWW
      | I_Alpha_WWWW0 | I_Alpha_ZZWW1 | I_Alpha_WWWW2
      | I_Alpha_ZZWW0 | I_Alpha_ZZZZ
      | G_HWW | G_HHWW | G_HZZ | G_HHZZ | G_Hmm
      | G_Htt | G_Hbb | G_Hcc | G_Htautau | G_H3 | G_H4
      | G_HGaZ | G_HGaGa | G_Hgg
      | G_strong
      | Mass of flavor | Width of flavor
      | I_G_DH4 | G_DH2W2 | G_DH2Z2 | G_DHW2 | G_DHZ2

(* \begin{dubious}
     The current abstract syntax for parameter dependencies is admittedly
     tedious. Later, there will be a parser for a convenient concrete syntax
     as a part of a concrete syntax for models.  But as these examples show,
     it should include simple functions.
   \end{dubious} *)

(* \begin{subequations}
     \begin{align}
        \alpha_{\text{QED}} &= \frac{1}{137.0359895} \\
             \sin^2\theta_w &= 0.23124
     \end{align}
   \end{subequations} *)
    let input_parameters =
      [ Alpha_QED, 1. /. 137.0359895;
        Sin2thw, 0.23124;
        Mass (G Z), 91.187;
        Mass (M (N 1)), 0.0; Mass (M (L 1)), 0.51099907e-3;
        Mass (M (N 2)), 0.0; Mass (M (L 2)), 0.105658389;
        Mass (M (N 3)), 0.0; Mass (M (L 3)), 1.77705;
        Mass (M (U 1)), 5.0e-3; Mass (M (D 1)), 3.0e-3;
        Mass (M (U 2)), 1.2; Mass (M (D 2)), 0.1;
        Mass (M (U 3)), 174.0; Mass (M (D 3)), 4.2 ]

(* \begin{subequations}
     \begin{align}
                        e &= \sqrt{4\pi\alpha} \\
             \sin\theta_w &= \sqrt{\sin^2\theta_w} \\
             \cos\theta_w &= \sqrt{1-\sin^2\theta_w} \\
                        g &= \frac{e}{\sin\theta_w} \\
                      m_W &= \cos\theta_w m_Z \\
                        v &= \frac{2m_W}{g} \\
                  g_{CC}   =
       -\frac{g}{2\sqrt2} &= -\frac{e}{2\sqrt2\sin\theta_w} \\
       Q_{\text{lepton}}   =
      -q_{\text{lepton}}e &= e \\
           Q_{\text{up}}   =
          -q_{\text{up}}e &= -\frac{2}{3}e \\
         Q_{\text{down}}   =
        -q_{\text{down}}e &= \frac{1}{3}e \\
        \ii q_We           =
        \ii g_{\gamma WW} &= \ii e \\
              \ii g_{ZWW} &= \ii g \cos\theta_w \\
              \ii g_{WWW} &= \ii g
     \end{align}
   \end{subequations} *)

(* \begin{dubious}
   \ldots{} JR leaves this dubious as it is \ldots{}
   but JR has corrected the errors....
   \end{dubious} 
   \begin{subequations}
     \begin{align}
                  g_{HWW} &= g m_W = 2 \frac{m_W^2}{v} \\
                 g_{HHWW} &= \frac{g}{\sqrt{2}} = \frac{\sqrt{2} m_W}{v} \\
                  g_{HZZ} &= \frac{g}{\cos\theta_w}m_Z \\
                 g_{HHZZ} &= \frac{g}{\sqrt{2}\cos\theta_w} = \frac{\sqrt{2} m_Z}{v} \\
                  g_{Htt} &= \lambda_t \\
                  g_{Hbb} &= \lambda_b=\frac{m_b}{m_t}\lambda_t \\
                  g_{H^3} &= - \frac{3g}{2} \frac{m_H^2}{m_W} = - 3 \frac{m_H^2}{v} \\
                  g_{H^4} &= \ii \frac{g}{2} \frac{m_H}{m_W} = \ii \frac{m_H}{v} 
     \end{align}
   \end{subequations} *)

    let derived_parameters =
      [ Real E, Sqrt (Prod [Const 4; Atom Pi; Atom Alpha_QED]);
        Real Sinthw, Sqrt (Atom Sin2thw);
        Real Costhw, Sqrt (Diff (Const 1, Atom Sin2thw));
        Real G_weak, Quot (Atom E, Atom Sinthw);
        Real (Mass (G Wp)), Prod [Atom Costhw; Atom (Mass (G Z))];
        Real Vev, Quot (Prod [Const 2; Atom (Mass (G Wp))], Atom G_weak);
        Real Q_lepton, Atom E;
        Real Q_up, Prod [Quot (Const (-2), Const 3); Atom E];
        Real Q_down, Prod [Quot (Const 1, Const 3); Atom E];
        Real G_CC, Neg (Quot (Atom G_weak, Prod [Const 2; Sqrt (Const 2)]));
        Complex I_Q_W, Prod [I; Atom E];
        Complex I_G_ZWW, Prod [I; Atom G_weak; Atom Costhw];
        Complex I_G_WWW, Prod [I; Atom G_weak] ]
             
(* \begin{equation}
      - \frac{g}{2\cos\theta_w}
   \end{equation} *)
    let g_over_2_costh =
      Quot (Neg (Atom G_weak), Prod [Const 2; Atom Costhw])

(* \begin{subequations}
     \begin{align}
           - \frac{g}{2\cos\theta_w} g_V
        &= - \frac{g}{2\cos\theta_w} (T_3 - 2 q \sin^2\theta_w) \\
           - \frac{g}{2\cos\theta_w} g_A
        &= - \frac{g}{2\cos\theta_w} T_3
     \end{align}
   \end{subequations} *)
    let nc_coupling c t3 q =
      (Real_Array c,
       [Prod [g_over_2_costh; Diff (t3, Prod [Const 2; q; Atom Sin2thw])];
        Prod [g_over_2_costh; t3]])

    let half = Quot (Const 1, Const 2)

    let derived_parameter_arrays =
      [ nc_coupling G_NC_neutrino half (Const 0);
        nc_coupling G_NC_lepton (Neg half) (Const (-1));
        nc_coupling G_NC_up half (Quot (Const 2, Const 3));
        nc_coupling G_NC_down (Neg half) (Quot (Const (-1), Const 3)) ]

    let parameters () =
      { input = input_parameters;
        derived = derived_parameters;
        derived_arrays = derived_parameter_arrays }

    module F = Fusions (struct
      type f = flavor
      type c = constant
      let compare = compare
      let conjugate = conjugate
    end)

(* \begin{equation}
     \mathcal{L}_{\textrm{EM}} =
        - e \sum_i q_i \bar\psi_i\fmslash{A}\psi_i
   \end{equation} *)

    let mgm ((m1, g, m2), fbf, c) = ((M m1, G g, M m2), fbf, c)

    let electromagnetic_currents n =
      List.map mgm
        [ ((L (-n), Ga, L n), FBF (1, Psibar, V, Psi), Q_lepton);
          ((U (-n), Ga, U n), FBF (1, Psibar, V, Psi), Q_up);
          ((D (-n), Ga, D n), FBF (1, Psibar, V, Psi), Q_down) ]

(* \begin{equation}
     \mathcal{L}_{\textrm{NC}} =
        - \frac{g}{2\cos\theta_W}
            \sum_i \bar\psi_i\fmslash{Z}(g_V^i-g_A^i\gamma_5)\psi_i
   \end{equation} *)

    let neutral_currents n =
      List.map mgm
        [ ((L (-n), Z, L n), FBF (1, Psibar, VA, Psi), G_NC_lepton);
          ((N (-n), Z, N n), FBF (1, Psibar, VA, Psi), G_NC_neutrino);
          ((U (-n), Z, U n), FBF (1, Psibar, VA, Psi), G_NC_up);
          ((D (-n), Z, D n), FBF (1, Psibar, VA, Psi), G_NC_down) ] 

(* \begin{equation}
     \mathcal{L}_{\textrm{CC}} =
        - \frac{g}{2\sqrt2} \sum_i \bar\psi_i
               (T^+\fmslash{W}^+ + T^-\fmslash{W}^-)(1-\gamma_5)\psi_i 
   \end{equation} *)


    let charged_currents n =
      List.map mgm
        [ ((L (-n), Wm, N n), FBF (1, Psibar, VL, Psi), G_CC);
          ((N (-n), Wp, L n), FBF (1, Psibar, VL, Psi), G_CC);
          ((D (-n), Wm, U n), FBF (1, Psibar, VL, Psi), G_CC);
          ((U (-n), Wp, D n), FBF (1, Psibar, VL, Psi), G_CC) ] 

    let yukawa =
      [ ((M (U (-3)), O H, M (U 3)), FBF (1, Psibar, S, Psi), G_Htt);
        ((M (D (-3)), O H, M (D 3)), FBF (1, Psibar, S, Psi), G_Hbb);
        ((M (U (-2)), O H, M (U 2)), FBF (1, Psibar, S, Psi), G_Hcc);
        ((M (L (-2)), O H, M (L 2)), FBF (1, Psibar, S, Psi), G_Hmm);
        ((M (L (-3)), O H, M (L 3)), FBF (1, Psibar, S, Psi), G_Htautau) ]
      
(* \begin{equation}
     \mathcal{L}_{\textrm{TGC}} =
        - e \partial_\mu A_\nu W_+^\mu W_-^\nu + \ldots
        - e \cot\theta_w  \partial_\mu Z_\nu W_+^\mu W_-^\nu + \ldots
   \end{equation} *)

    let tgc ((g1, g2, g3), t, c) = ((G g1, G g2, G g3), t, c)

    let standard_triple_gauge =
      List.map tgc
        [ ((Ga, Wm, Wp), Gauge_Gauge_Gauge 1, I_Q_W);
          ((Z, Wm, Wp), Gauge_Gauge_Gauge 1, I_G_ZWW) ]

(* \begin{multline}
     \mathcal{L}_{\textrm{TGC}}(g_1,\kappa)
        =   g_1 \mathcal{L}_T(V,W^+,W^-) \\
          + \frac{\kappa+g_1}{2} \Bigl(\mathcal{L}_T(W^-,V,W^+)
                                         - \mathcal{L}_T(W^+,V,W^-)\Bigr)\\
          + \frac{\kappa-g_1}{2} \Bigl(\mathcal{L}_L(W^-,V,W^+)
                                         - \mathcal{L}_T(W^+,V,W^-)\Bigr)
   \end{multline} *)

(* \begin{dubious}
   The whole thing in the LEP2 workshop notation:
   \begin{multline}
     \ii\mathcal{L}_{\textrm{TGC},V} / g_{WWV} = \\
            g_1^V V^\mu (W^-_{\mu\nu}W^{+,\nu}-W^+_{\mu\nu}W^{-,\nu})
          + \kappa_V  W^+_\mu W^-_\nu V^{\mu\nu}
          + \frac{\lambda_V}{m_W^2} V_{\mu\nu}
               W^-_{\rho\mu} W^{+,\hphantom{\nu}\rho}_{\hphantom{+,}\nu} \\
          + \ii g_5^V \epsilon_{\mu\nu\rho\sigma}
              \left(   (\partial^\rho W^{-,\mu}) W^{+,\nu}
                     -  W^{-,\mu}(\partial^\rho W^{+,\nu}) \right) V^\sigma \\
          + \ii g_4^V W^-_\mu W^+_\nu (\partial^\mu V^\nu + \partial^\nu V^\mu)
          - \frac{\tilde\kappa_V}{2}  W^-_\mu W^+_\nu \epsilon^{\mu\nu\rho\sigma}
              V_{\rho\sigma}
          - \frac{\tilde\lambda_V}{2m_W^2}
               W^-_{\rho\mu} W^{+,\mu}_{\hphantom{+,\mu}\nu} \epsilon^{\nu\rho\alpha\beta}
                V_{\alpha\beta}
   \end{multline}
   using the conventions of Itzykson and Zuber with $\epsilon^{0123} = +1$.
   \end{dubious} *)

(* \begin{dubious}
   This is equivalent to the notation of Hagiwara et al.~\cite{HPZH87}, if we
   remember that they have opposite signs for~$g_{WWV}$:
   \begin{multline}
     \mathcal{L}_{WWV} / (-g_{WWV})  = \\
       \ii g_1^V \left( W^\dagger_{\mu\nu} W^\mu 
                         - W^\dagger_\mu W^\mu_{\hphantom{\mu}\nu} \right) V^\nu
     + \ii \kappa_V  W^\dagger_\mu W_\nu V^{\mu\nu}
     + \ii \frac{\lambda_V}{m_W^2}
          W^\dagger_{\lambda\mu} W^\mu_{\hphantom{\mu}\nu} V^{\nu\lambda} \\
     - g_4^V  W^\dagger_\mu W_\nu
          \left(\partial^\mu V^\nu + \partial^\nu V^\mu \right)
     + g_5^V \epsilon^{\mu\nu\lambda\sigma}
           \left( W^\dagger_\mu \stackrel{\leftrightarrow}{\partial_\lambda}
                  W_\nu \right) V_\sigma\\
     + \ii \tilde\kappa_V  W^\dagger_\mu W_\nu \tilde{V}^{\mu\nu}
     + \ii\frac{\tilde\lambda_V}{m_W^2}
           W^\dagger_{\lambda\mu} W^\mu_{\hphantom{\mu}\nu} \tilde{V}^{\nu\lambda}
   \end{multline}
   Here $V^\mu$ stands for either the photon or the~$Z$ field, $W^\mu$ is the
   $W^-$ field, $W_{\mu\nu} = \partial_\mu W_\nu - \partial_\nu W_\mu$,
   $V_{\mu\nu} = \partial_\mu V_\nu - \partial_\nu V_\mu$, and
   $\tilde{V}_{\mu\nu} = \frac{1}{2} \epsilon_{\mu\nu\lambda\sigma}
   V^{\lambda\sigma}$.
   \end{dubious} *)

    let anomalous_triple_gauge =
      List.map tgc
        [ ((Ga, Wm, Wp), Dim4_Vector_Vector_Vector_T (-1),
           I_G1_AWW);
          ((Z, Wm, Wp), Dim4_Vector_Vector_Vector_T (-1),
           I_G1_ZWW);
          ((Wm, Ga, Wp), Dim4_Vector_Vector_Vector_T 1,
           I_G1_plus_kappa_minus_G4_AWW);
          ((Wm, Z, Wp), Dim4_Vector_Vector_Vector_T 1,
           I_G1_plus_kappa_minus_G4_ZWW);
          ((Wp, Ga, Wm), Dim4_Vector_Vector_Vector_T (-1),
           I_G1_plus_kappa_plus_G4_AWW);
          ((Wp, Z, Wm), Dim4_Vector_Vector_Vector_T (-1),
           I_G1_plus_kappa_plus_G4_ZWW);
          ((Wm, Ga, Wp), Dim4_Vector_Vector_Vector_L (-1),
           I_G1_minus_kappa_plus_G4_AWW);
          ((Wm, Z, Wp), Dim4_Vector_Vector_Vector_L (-1),
           I_G1_minus_kappa_plus_G4_ZWW);
          ((Wp, Ga, Wm), Dim4_Vector_Vector_Vector_L 1,
           I_G1_minus_kappa_minus_G4_AWW);
          ((Wp, Z, Wm), Dim4_Vector_Vector_Vector_L 1,
           I_G1_minus_kappa_minus_G4_ZWW);
          ((Ga, Wm, Wp), Dim4_Vector_Vector_Vector_T5 (-1),
           I_kappa5_AWW);
          ((Z, Wm, Wp), Dim4_Vector_Vector_Vector_T5 (-1),
           I_kappa5_ZWW);
          ((Ga, Wm, Wp), Dim4_Vector_Vector_Vector_L5 (-1),
           G5_AWW);
          ((Z, Wm, Wp), Dim4_Vector_Vector_Vector_L5 (-1),
           G5_ZWW);
          ((Ga, Wp, Wm), Dim6_Gauge_Gauge_Gauge (-1),
           I_lambda_AWW);
          ((Z, Wp, Wm), Dim6_Gauge_Gauge_Gauge (-1),
           I_lambda_ZWW);
          ((Ga, Wp, Wm), Dim6_Gauge_Gauge_Gauge_5 (-1),
           I_lambda5_AWW);
          ((Z, Wp, Wm), Dim6_Gauge_Gauge_Gauge_5 (-1),
           I_lambda5_ZWW) ]

    let triple_gauge =
      if Flags.triple_anom then
        anomalous_triple_gauge
      else
        standard_triple_gauge

(* \begin{equation}
     \mathcal{L}_{\textrm{QGC}} =
        - g^2 W_{+,\mu} W_{-,\nu} W_+^\mu W_-^\nu + \ldots
   \end{equation} *)

    let tgc_aux ((aux, g1, g2), t, c) = ((O aux, G g1, G g2), t, c)

    let standard_quartic_gauge =
      List.map tgc_aux
        [ ((XW3, Wm, Wp), Aux_Gauge_Gauge 1, I_G_WWW);
          ((XWm, Wp, Ga), Aux_Gauge_Gauge 1, I_Q_W);
          ((XWm, Wp, Z), Aux_Gauge_Gauge 1, I_G_ZWW);
          ((XWp, Ga, Wm), Aux_Gauge_Gauge 1, I_Q_W);
          ((XWp, Z, Wm), Aux_Gauge_Gauge 1, I_G_ZWW) ]

(* \begin{subequations}
   \begin{align}
     \mathcal{L}_4
       &= \alpha_4 \left(   \frac{g^4}{2}\left(   (W^+_\mu W^{-,\mu})^2
                                                + W^+_\mu W^{+,\mu} W^-_\mu W^{-,\mu}
                                               \right)\right.\notag \\
       &\qquad\qquad\qquad \left.
                          + \frac{g^4}{\cos^2\theta_w} W^+_\mu Z^\mu W^-_\nu Z^\nu
                          + \frac{g^4}{4\cos^4\theta_w} (Z_\mu Z^\mu)^2 \right) \\
     \mathcal{L}_5
       &= \alpha_5 \left(   g^4 (W^+_\mu W^{-,\mu})^2
                          + \frac{g^4}{\cos^2\theta_w}  W^+_\mu W^{-,\mu} Z_\nu Z^\nu
                          + \frac{g^4}{4\cos^4\theta_w} (Z_\mu Z^\mu)^2 \right)
   \end{align}
   \end{subequations}
   or
   \begin{multline}
     \mathcal{L}_4 + \mathcal{L}_5
       =   (\alpha_4+2\alpha_5) g^4 \frac{1}{2} (W^+_\mu W^{-,\mu})^2 \\
         + 2\alpha_4 g^4 \frac{1}{4} W^+_\mu W^{+,\mu} W^-_\mu W^{-,\mu}
         + \alpha_4 \frac{g^4}{\cos^2\theta_w} W^+_\mu Z^\mu W^-_\nu Z^\nu \\
         + 2\alpha_5 \frac{g^4}{\cos^2\theta_w} \frac{1}{2} W^+_\mu W^{-,\mu} Z_\nu Z^\nu
         + (2\alpha_4 + 2\alpha_5) \frac{g^4}{\cos^4\theta_w} \frac{1}{8} (Z_\mu Z^\mu)^2
   \end{multline}
   and therefore
   \begin{subequations}
   \begin{align}
     (\ii\alpha_{(WW)_0})^2 &= (\alpha_4+2\alpha_5) g^4 \\
     (\ii\alpha_{(WW)_2})^2 &= 2\alpha_4 g^4 \\
     (\ii\alpha_{(WZ)_\pm})^2 &= \alpha_4 \frac{g^4}{\cos^2\theta_w} \\
     (\ii\alpha_{(WZ)_0})^2 &= 2\alpha_5 \frac{g^4}{\cos^2\theta_w} \\
     (\ii\alpha_{ZZ})^2 &= (2\alpha_4 + 2\alpha_5) \frac{g^4}{\cos^4\theta_w}
   \end{align}
   \end{subequations}
   Not that the auxiliary couplings are purely imaginary, because~$\alpha_4$
   and~$\alpha_5$ are defined with a \emph{positive} sign and we expect
   quartic couplings to have a \emph{negative} sign for the energy to be
   bounded from below. *)

    let anomalous_quartic_gauge =
      List.map tgc_aux
        [ ((XSW3, Wm, Wp), Aux_Vector_Vector 1, I_Alpha_WWWW0);
          ((XSWpp, Wm, Wm), Aux_Vector_Vector 1, I_Alpha_WWWW2);
          ((XSWmm, Wp, Wp), Aux_Vector_Vector 1, I_Alpha_WWWW2);
          ((XSWm, Wp, Z), Aux_Vector_Vector 1, I_Alpha_ZZWW1);
          ((XSWp, Wm, Z), Aux_Vector_Vector 1, I_Alpha_ZZWW1);
          ((XSWZ0, Wp, Wm), Aux_Vector_Vector 1, I_Alpha_ZZWW0);
          ((XSZW0, Z, Z), Aux_Vector_Vector 1, I_Alpha_ZZWW0);
          ((XSZZ, Z, Z), Aux_Vector_Vector 1, I_Alpha_ZZZZ) ]

    let quartic_gauge =
      if Flags.quartic_anom then
        standard_quartic_gauge @ anomalous_quartic_gauge
      else
        standard_quartic_gauge

    let standard_gauge_higgs =
      [ ((O H, G Wp, G Wm), Scalar_Vector_Vector 1, G_HWW);
        ((O H, G Z, G Z), Scalar_Vector_Vector 1, G_HZZ);
        ((O XH_W, G Wp, G Wm), Aux_Vector_Vector 1, G_HHWW);
        ((O XH_W', O H, O H), Aux_Scalar_Scalar 1, G_HHWW);
        ((O XH_Z, G Z, G Z), Aux_Vector_Vector 1, G_HHZZ);
        ((O XH_Z', O H, O H), Aux_Scalar_Scalar 1, G_HHZZ) ]

(* WK's couplings (apparently, he still intends to divide by
   $\Lambda^2_{\text{EWSB}}=16\pi^2v_{\mathrm{F}}^2$):
   \begin{subequations}
   \begin{align}
     \mathcal{L}^{\tau}_4 &=
      \left\lbrack (\partial_{\mu}H)(\partial^{\mu}H)
                     + \frac{g^2v_{\mathrm{F}}^2}{4} V_{\mu} V^{\mu} \right\rbrack^2 \\
     \mathcal{L}^{\tau}_5 &=
      \left\lbrack (\partial_{\mu}H)(\partial_{\nu}H)
                     + \frac{g^2v_{\mathrm{F}}^2}{4} V_{\mu} V_{\nu} \right\rbrack^2
   \end{align}
   \end{subequations}
   with
   \begin{equation}
      V_{\mu} V_{\nu} =
        \frac{1}{2} \left( W^+_{\mu} W^-_{\nu} + W^+_{\nu} W^-_{\mu} \right)
         + \frac{1}{2\cos^2\theta_{w}} Z_{\mu} Z_{\nu}
   \end{equation}
   (note the symmetrization!), i.\,e.
   \begin{subequations}
   \begin{align}
     \mathcal{L}_4 &= \alpha_4 \frac{g^4v_{\mathrm{F}}^4}{16} (V_{\mu} V_{\nu})^2 \\
     \mathcal{L}_5 &= \alpha_5 \frac{g^4v_{\mathrm{F}}^4}{16} (V_{\mu} V^{\mu})^2
   \end{align}
   \end{subequations} *)

(* Breaking thinks up
   \begin{subequations}
   \begin{align}
     \mathcal{L}^{\tau,H^4}_4 &=
       \left\lbrack (\partial_{\mu}H)(\partial^{\mu}H) \right\rbrack^2 \\
     \mathcal{L}^{\tau,H^4}_5 &=
       \left\lbrack (\partial_{\mu}H)(\partial^{\mu}H) \right\rbrack^2
   \end{align}
   \end{subequations}
   and
   \begin{subequations}
   \begin{align}
     \mathcal{L}^{\tau,H^2V^2}_4 &= \frac{g^2v_{\mathrm{F}}^2}{2}
              (\partial_{\mu}H)(\partial^{\mu}H) V_{\mu}V^{\mu}   \\
     \mathcal{L}^{\tau,H^2V^2}_5 &= \frac{g^2v_{\mathrm{F}}^2}{2}
              (\partial_{\mu}H)(\partial_{\nu}H) V_{\mu}V_{\nu}
   \end{align}
   \end{subequations}
   i.\,e.
   \begin{subequations}
   \begin{align}
     \mathcal{L}^{\tau,H^2V^2}_4 &=
        \frac{g^2v_{\mathrm{F}}^2}{2}
          \left\lbrack
              (\partial_{\mu}H)(\partial^{\mu}H) W^+_{\nu}W^{-,\nu}
            + \frac{1}{2\cos^2\theta_{w}} (\partial_{\mu}H)(\partial^{\mu}H) Z_{\nu} Z^{\nu}
          \right\rbrack \\
     \mathcal{L}^{\tau,H^2V^2}_5 &=
          \frac{g^2v_{\mathrm{F}}^2}{2}
          \left\lbrack
              (W^{+,\mu}\partial_{\mu}H) (W^{-,\nu}\partial_{\nu}H)
            + \frac{1}{2\cos^2\theta_{w}} (Z^{\mu}\partial_{\mu}H)(Z^{\nu}\partial_{\nu}H)
          \right\rbrack
   \end{align}
   \end{subequations} *)

(* \begin{multline}
     \tau^4_8 \mathcal{L}^{\tau,H^2V^2}_4 + \tau^5_8 \mathcal{L}^{\tau,H^2V^2}_5 = \\
       - \frac{g^2v_{\mathrm{F}}^2}{2} \Biggl\lbrack
            2\tau^4_8
              \frac{1}{2}(\ii\partial_{\mu}H)(\ii\partial^{\mu}H) W^+_{\nu}W^{-,\nu}
          + \tau^5_8
              (W^{+,\mu}\ii\partial_{\mu}H) (W^{-,\nu}\ii\partial_{\nu}H) \\
          + \frac{2\tau^4_8}{\cos^2\theta_{w}}
              \frac{1}{4} (\ii\partial_{\mu}H)(\ii\partial^{\mu}H) Z_{\nu} Z^{\nu}
          + \frac{\tau^5_8}{\cos^2\theta_{w}}
              \frac{1}{2} (Z^{\mu}\ii\partial_{\mu}H)(Z^{\nu}\ii\partial_{\nu}H)
          \Biggr\rbrack
   \end{multline}
   where the two powers of $\ii$ make the sign conveniently negative,
   i.\,e.
   \begin{subequations}
   \begin{align}
     \alpha_{(\partial H)^2W^2}^2 &= \tau^4_8 g^2v_{\mathrm{F}}^2\\
     \alpha_{(\partial HW)^2}^2 &= \frac{\tau^5_8 g^2v_{\mathrm{F}}^2}{2}  \\
     \alpha_{(\partial H)^2Z^2}^2 &= \frac{\tau^4_8 g^2v_{\mathrm{F}}^2}{\cos^2\theta_{w}} \\ 
     \alpha_{(\partial HZ)^2}^2 &=\frac{\tau^5_8 g^2v_{\mathrm{F}}^2}{2\cos^2\theta_{w}}
   \end{align}
   \end{subequations} *)

    let anomalous_gauge_higgs =
      [ ((O XDH_W, O H, O H), Aux_DScalar_DScalar 1, G_DH2W2);
        ((O XDH_W', G Wp, G Wm), Aux_Vector_Vector 1, G_DH2W2);
        ((O XDH_Z, O H, O H), Aux_DScalar_DScalar 1, G_DH2Z2);
        ((O XDH_Z', G Z, G Z), Aux_Vector_Vector 1, G_DH2Z2);
        ((O XDH_Wm, G Wp, O H), Aux_Vector_DScalar 1, G_DHW2);
        ((O XDH_Wp, G Wm, O H), Aux_Vector_DScalar 1, G_DHW2);
        ((O XDH_Z'', G Z, O H), Aux_Vector_DScalar 1, G_DHZ2) ]

    let gauge_higgs =
      if Flags.higgs_anom then
        standard_gauge_higgs @ anomalous_gauge_higgs
      else
        standard_gauge_higgs

(* \begin{equation}
     \mathcal{L}_{\text{Higgs}} =
       \frac{1}{3!} g_{H,3} H^3 - \frac{1}{4!} g_{H,4}^2 H^4
   \end{equation}
   According to~(\ref{eq:quartic-aux}), the required negative sign
   for the quartic piece is reproduced by any real $g_{H,4}$ in the
   auxiliary field couplings.
   \begin{multline}
     \mathcal{L}_{\text{Higgs}} =
       - \frac{1}{4!} g_{H,4}^2 \left((\phi^\dagger\phi)^2 - \mu^2\right)^2 \\
       \to - \frac{1}{4!} g_{H,4}^2 \left((\mu+H)^2 - \mu^2\right)^2
        =  - \frac{1}{4!} g_{H,4}^2 \left(2\mu H + H^2\right)^2 \\
        =  - \frac{1}{4!} g_{H,4}^2 H^4
           - \frac{1}{3!} g_{H,4}^2 \mu H^3
           - \frac{1}{3!} g_{H,4}^2 \mu^2 H^2
   \end{multline} *)

    let standard_higgs =
      [ ((O H, O H, O H), Scalar_Scalar_Scalar 1, G_H3);
        ((O XH, O H, O H), Aux_Scalar_Scalar 1, G_H4) ]

(* \begin{equation}
     \tau^4_8 \mathcal{L}^{\tau,H^4}_4 + \tau^5_8 \mathcal{L}^{\tau,H^4}_5
       = 8 (\tau^4_8+\tau^5_8) \frac{1}{8}
             \left\lbrack (\ii\partial_{\mu}H)(\ii\partial^{\mu}H) \right\rbrack^2
   \end{equation}
   since there are four powers of $\ii$, the sign remains positive,
   i.\,e.
   \begin{equation}
     (\ii\alpha_{(\partial H)^4})^2 = 8 (\tau^4_8+\tau^5_8)
   \end{equation} *)

    let anomalous_higgs =
      [ ((O XDH2, O H, O H), Aux_DScalar_DScalar 1, I_G_DH4) ]

    let anomaly_higgs = 
      []
(*i      [ (O H, G Ga, G Ga), Dim5_Scalar_Gauge2 1, G_HGaGa;
        (O H, G Ga, G Z), Dim5_Scalar_Gauge2 1, G_HGaZ;
        (O H, G Gl, G Gl), Dim5_Scalar_Gauge2 1, G_Hgg ] i*)

    let higgs =	
      if Flags.higgs_anom then
	  standard_higgs @ anomalous_higgs
      else
          standard_higgs

    let goldstone_vertices =
      [ ((O Phi0, G Wm, G Wp), Scalar_Vector_Vector 1, I_G_ZWW);
        ((O Phip, G Ga, G Wm), Scalar_Vector_Vector 1, I_Q_W);
        ((O Phip, G Z, G Wm), Scalar_Vector_Vector 1, I_G_ZWW);
        ((O Phim, G Wp, G Ga), Scalar_Vector_Vector 1, I_Q_W);
        ((O Phim, G Wp, G Z), Scalar_Vector_Vector 1, I_G_ZWW) ]

    let vertices3 =
      (ThoList.flatmap electromagnetic_currents [1;2;3] @
       ThoList.flatmap neutral_currents [1;2;3] @
       ThoList.flatmap charged_currents [1;2;3] @
       yukawa @ triple_gauge @ quartic_gauge @
       gauge_higgs @ higgs @ anomaly_higgs @ goldstone_vertices)

    let vertices () = (vertices3, [], [])

(* For efficiency, make sure that [F.of_vertices vertices] is
   evaluated only once. *)

    let table = F.of_vertices (vertices ())
    let fuse2 = F.fuse2 table
    let fuse3 = F.fuse3 table
    let fuse = F.fuse table
    let max_degree () = 3

    let flavor_of_string = function
      | "e-" -> M (L 1) | "e+" -> M (L (-1))
      | "mu-" -> M (L 2) | "mu+" -> M (L (-2))
      | "tau-" -> M (L 3) | "tau+" -> M (L (-3))
      | "nue" -> M (N 1) | "nuebar" -> M (N (-1))
      | "numu" -> M (N 2) | "numubar" -> M (N (-2))
      | "nutau" -> M (N 3) | "nutaubar" -> M (N (-3))
      | "u" -> M (U 1) | "ubar" -> M (U (-1))
      | "c" -> M (U 2) | "cbar" -> M (U (-2))
      | "t" -> M (U 3) | "tbar" -> M (U (-3))
      | "d" -> M (D 1) | "dbar" -> M (D (-1))
      | "s" -> M (D 2) | "sbar" -> M (D (-2))
      | "b" -> M (D 3) | "bbar" -> M (D (-3))
      | "g" -> G Gl
      | "A" -> G Ga | "Z" | "Z0" -> G Z
      | "W+" -> G Wp | "W-" -> G Wm
      | "H" -> O H
      | _ -> invalid_arg "Models.SM3.flavor_of_string"

    let flavor_to_string = function
      | M f ->
          begin match f with
          | L 1 -> "e-" | L (-1) -> "e+"
          | L 2 -> "mu-" | L (-2) -> "mu+"
          | L 3 -> "tau-" | L (-3) -> "tau+"
          | L _ -> invalid_arg
                "Models.SM3.flavor_to_string: invalid lepton"
          | N 1 -> "nue" | N (-1) -> "nuebar"
          | N 2 -> "numu" | N (-2) -> "numubar"
          | N 3 -> "nutau" | N (-3) -> "nutaubar"
          | N _ -> invalid_arg
                "Models.SM3.flavor_to_string: invalid neutrino"
          | U 1 -> "u" | U (-1) -> "ubar"
          | U 2 -> "c" | U (-2) -> "cbar"
          | U 3 -> "t" | U (-3) -> "tbar"
          | U _ -> invalid_arg
                "Models.SM3.flavor_to_string: invalid up type quark"
          | D 1 -> "d" | D (-1) -> "dbar"
          | D 2 -> "s" | D (-2) -> "sbar"
          | D 3 -> "b" | D (-3) -> "bbar"
          | D _ -> invalid_arg
                "Models.SM3.flavor_to_string: invalid down type quark"
          end
      | G f ->
          begin match f with
          | Gl -> "g"
          | Ga -> "A" | Z -> "Z"
          | Wp -> "W+" | Wm -> "W-"
          end
      | O f ->
          begin match f with
          | XWp -> "W+aux" | XWm -> "W-aux"
          | XW3 -> "W3aux" | XGl -> "gaux"
          | Phip -> "phi+" | Phim -> "phi-" | Phi0 -> "phi0" 
          | H -> "H" | XH -> "Haux"
          | XH_W -> "HW1aux" | XH_W' -> "HW2aux"
          | XH_Z -> "HZ1aux" | XH_Z' -> "HZ2aux"
          | XSWm -> "W-Zaux" | XSWp -> "W+Zaux"
          | XSWpp -> "W+W+aux" | XSWmm -> "W-W-aux"
          | XSWZ0 -> "W+W-/ZZaux" | XSZW0 -> "ZZ/W+W-aux"
          | XSW3 -> "W+W-aux" | XSZZ -> "ZZaux"
          | XDH_W -> "DHDH/W+W-aux" | XDH_W' -> "DHDH/W+W-aux'"
          | XDH_Z -> "DHDH/ZZaux" | XDH_Z' -> "DHDH/ZZaux'"
          | XDH_Wm -> "DHW-aux" | XDH_Wp -> "DHW+aux"
          | XDH_Z'' -> "DHZaux" | XDH2 -> "DHDHaux"
          end

    let flavor_to_TeX = function
      | M f ->
          begin match f with
          | L 1 -> "e^-" | L (-1) -> "e^+"
          | L 2 -> "\\mu^-" | L (-2) -> "\\mu^+"
          | L 3 -> "\\tau^-" | L (-3) -> "\\tau^+"
          | L _ -> invalid_arg
                "Models.SM3.flavor_to_TeX: invalid lepton"
          | N 1 -> "\\nu_e" | N (-1) -> "\\bar{\\nu}_e"
          | N 2 -> "\\nu_\\mu" | N (-2) -> "\\bar{\\nu}_\\mu"
          | N 3 -> "\\nu_\\tau" | N (-3) -> "\\bar{\\nu}_\\tau"
          | N _ -> invalid_arg
                "Models.SM3.flavor_to_TeX: invalid neutrino"
          | U 1 -> "u" | U (-1) -> "\\bar{u}"
          | U 2 -> "c" | U (-2) -> "\\bar{c}"
          | U 3 -> "t" | U (-3) -> "\\bar{t}"
          | U _ -> invalid_arg
                "Models.SM3.flavor_to_TeX: invalid up type quark"
          | D 1 -> "d" | D (-1) -> "\\bar{d}"
          | D 2 -> "s" | D (-2) -> "\\bar{s}"
          | D 3 -> "b" | D (-3) -> "\\bar{b}"
          | D _ -> invalid_arg
                "Models.SM3.flavor_to_TeX: invalid down type quark"
          end
      | G f ->
          begin match f with
          | Gl -> "g"
          | Ga -> "\\gamma" | Z -> "Z"
          | Wp -> "W^+" | Wm -> "W^-"
          end
      | O f ->
          begin match f with
          | XWp -> "W+aux" | XWm -> "W-aux"
          | XW3 -> "W3aux" | XGl -> "gaux"
          | Phip -> "phi+" | Phim -> "phi-" | Phi0 -> "phi0" 
          | H -> "H" | XH -> "Haux"
          | XH_W -> "HW1aux" | XH_W' -> "HW2aux"
          | XH_Z -> "HZ1aux" | XH_Z' -> "HZ2aux"
          | XSWm -> "W-Zaux" | XSWp -> "W+Zaux"
          | XSWpp -> "W+W+aux" | XSWmm -> "W-W-aux"
          | XSWZ0 -> "W+W-/ZZaux" | XSZW0 -> "ZZ/W+W-aux"
          | XSW3 -> "W+W-aux" | XSZZ -> "ZZaux"
          | XDH_W -> "DHDH/W+W-aux" | XDH_W' -> "DHDH/W+W-aux'"
          | XDH_Z -> "DHDH/ZZaux" | XDH_Z' -> "DHDH/ZZaux'"
          | XDH_Wm -> "DHW-aux" | XDH_Wp -> "DHW+aux"
          | XDH_Z'' -> "DHZaux" | XDH2 -> "DHDHaux"
          end

    let flavor_symbol = function
      | M f ->
          begin match f with
          | L n when n > 0 -> "l" ^ string_of_int n
          | L n -> "l" ^ string_of_int (abs n) ^ "b"
          | N n when n > 0 -> "n" ^ string_of_int n
          | N n -> "n" ^ string_of_int (abs n) ^ "b"
          | U n when n > 0 -> "u" ^ string_of_int n
          | U n -> "u" ^ string_of_int (abs n) ^ "b"
          | D n when n > 0 ->  "d" ^ string_of_int n
          | D n -> "d" ^ string_of_int (abs n) ^ "b"
          end
      | G f ->
          begin match f with
          | Gl -> "gl"
          | Ga -> "a" | Z -> "z"
          | Wp -> "wp" | Wm -> "wm"
          end
      | O f ->
          begin match f with
          | XWp -> "xwp" | XWm -> "xwm"
          | XW3 -> "xw3" | XGl -> "xgl"
          | Phip -> "pp" | Phim -> "pm" | Phi0 -> "p0" 
          | H -> "h" | XH -> "xh"
          | XH_W -> "xhw1" | XH_W' -> "xhw2"
          | XH_Z -> "xhz1" | XH_Z' -> "xhz2"
          | XSWm -> "xswm" | XSWp -> "xswp"
          | XSWpp -> "xswpp" | XSWmm -> "xswmm"
          | XSWZ0 -> "xswz0" | XSZW0 -> "xszw0"
          | XSW3 -> "xsww" | XSZZ -> "xszz"
          | XDH_W -> "xdhw1" | XDH_W' -> "xdhw2"
          | XDH_Z -> "xdhz1" | XDH_Z' -> "xdhz2"
          | XDH_Wm -> "xdhwm" | XDH_Wp -> "xdhwp"
          | XDH_Z'' -> "xdhz" | XDH2 -> "xdh"
          end

    let pdg = function
      | M f ->
          begin match f with
          | L n when n > 0 -> 9 + 2*n
          | L n -> - 9 + 2*n
          | N n when n > 0 -> 10 + 2*n
          | N n -> - 10 + 2*n
          | U n when n > 0 -> 2*n
          | U n -> 2*n
          | D n when n > 0 -> - 1 + 2*n
          | D n -> 1 + 2*n
          end
      | G f ->
          begin match f with
          | Gl -> 21
          | Ga -> 22 | Z -> 23
          | Wp -> 24 | Wm -> (-24)
          end
      | O f ->
          begin match f with
          | XWp | XWm | XW3 | XGl -> 0
          | Phip | Phim -> 27 | Phi0 -> 26
          | H -> 25
          | XH -> 0
          | XH_W | XH_W' -> 0
          | XH_Z | XH_Z' -> 0
          | XSWm | XSWp | XSWpp | XSWmm
          | XSWZ0 | XSZW0 | XSW3 | XSZZ -> 0
          | XDH_W | XDH_W' | XDH_Z | XDH_Z'
          | XDH_Wm | XDH_Wp | XDH_Z'' | XDH2 -> 0
          end

    let mass_symbol f = 
      "mass(" ^ string_of_int (abs (pdg f)) ^ ")"

    let width_symbol f =
      "width(" ^ string_of_int (abs (pdg f)) ^ ")"

    let constant_symbol = function
      | Unit -> "unit" | Pi -> "PI"
      | Alpha_QED -> "alpha" | E -> "e" | G_weak -> "g" | Vev -> "vev"
      | Sin2thw -> "sin2thw" | Sinthw -> "sinthw" | Costhw -> "costhw"
      | Q_lepton -> "qlep" | Q_up -> "qup" | Q_down -> "qdwn"
      | G_NC_lepton -> "gnclep" | G_NC_neutrino -> "gncneu"
      | G_NC_up -> "gncup" | G_NC_down -> "gncdwn"
      | G_CC -> "gcc"
      | I_Q_W -> "iqw" | I_G_ZWW -> "igzww" | I_G_WWW -> "igwww"
      | I_G1_AWW -> "ig1a" | I_G1_ZWW -> "ig1z"
      | I_G1_plus_kappa_plus_G4_AWW -> "ig1pkpg4a"
      | I_G1_plus_kappa_plus_G4_ZWW -> "ig1pkpg4z"
      | I_G1_plus_kappa_minus_G4_AWW -> "ig1pkmg4a"
      | I_G1_plus_kappa_minus_G4_ZWW -> "ig1pkmg4z"
      | I_G1_minus_kappa_plus_G4_AWW -> "ig1mkpg4a"
      | I_G1_minus_kappa_plus_G4_ZWW -> "ig1mkpg4z"
      | I_G1_minus_kappa_minus_G4_AWW -> "ig1mkmg4a"
      | I_G1_minus_kappa_minus_G4_ZWW -> "ig1mkmg4z"
      | I_lambda_AWW -> "ila"
      | I_lambda_ZWW -> "ilz"
      | G5_AWW -> "rg5a"
      | G5_ZWW -> "rg5z"
      | I_kappa5_AWW -> "ik5a"
      | I_kappa5_ZWW -> "ik5z"
      | I_lambda5_AWW -> "il5a" | I_lambda5_ZWW -> "il5z"
      | I_Alpha_WWWW0 -> "ialww0" | I_Alpha_WWWW2 -> "ialww2"
      | I_Alpha_ZZWW0 -> "ialzw0" | I_Alpha_ZZWW1 -> "ialzw1"
      | I_Alpha_ZZZZ  -> "ialzz"
      | G_HWW -> "ghww" | G_HZZ -> "ghzz"
      | G_HHWW -> "ghhww" | G_HHZZ -> "ghhzz"
      | G_Htt -> "ghtt" | G_Hbb -> "ghbb"
      | G_Htautau -> "ghtautau" | G_Hcc -> "ghcc" | G_Hmm -> "ghmm"
      | G_H3 -> "gh3" | G_H4 -> "gh4"
      | G_HGaZ -> "ghgaz" | G_HGaGa -> "ghgaga" | G_Hgg -> "ghgg"
      | G_strong -> "gs" 
      | Mass f -> "mass" ^ flavor_symbol f
      | Width f -> "width" ^ flavor_symbol f
      | I_G_DH4 -> "igdh4"
      | G_DH2W2 -> "gdh2w2" | G_DH2Z2 -> "gdh2z2"
      | G_DHW2 -> "gdhw2" | G_DHZ2 -> "gdhz2"

  end

(* \thocwmodulesection{Complete Minimal Standard Model with Genuine Quartic Couplings} *)

module SM (Flags : SM_flags) =
  struct
    let rcs = RCS.rename rcs_file "Models.SM"
        [ "minimal electroweak standard model in unitarity gauge"]

    open Coupling

    let default_width = ref Timelike
    let use_fudged_width = ref false

    let options = Options.create
      [ "constant_width", Arg.Unit (fun () -> default_width := Constant),
        "use constant width (also in t-channel)";
        "fudged_width", Arg.Set use_fudged_width,
        "use fudge factor for charge particle width";
        "custom_width", Arg.String (fun f -> default_width := Custom f),
        "use custom width";
        "cancel_widths", Arg.Unit (fun () -> default_width := Vanishing),
        "use vanishing width"]

    type matter_field = L of int | N of int | U of int | D of int
    type gauge_boson = Ga | Wp | Wm | Z | Gl
    type other = Phip | Phim | Phi0 | H
    type flavor = M of matter_field | G of gauge_boson | O of other

    let matter_field f = M f
    let gauge_boson f = G f
    let other f = O f

    type field =
      | Matter of matter_field
      | Gauge of gauge_boson
      | Other of other

    let field = function
      | M f -> Matter f
      | G f -> Gauge f
      | O f -> Other f

    type gauge = unit

    let gauge_symbol () =
      failwith "Models.SM.gauge_symbol: internal error"

    let family n = List.map matter_field [ L n; N n; U n; D n ]

    let external_flavors () =
      [ "1st Generation", ThoList.flatmap family [1; -1];
        "2nd Generation", ThoList.flatmap family [2; -2];
        "3rd Generation", ThoList.flatmap family [3; -3];
        "Gauge Bosons", List.map gauge_boson [Ga; Z; Wp; Wm; Gl];
        "Higgs", List.map other [H];
        "Goldstone Bosons", List.map other [Phip; Phim; Phi0] ]

    let flavors () = ThoList.flatmap snd (external_flavors ()) 

    let spinor n =
      if n >= 0 then
        Spinor
      else
        ConjSpinor

    let lorentz = function
      | M f ->
          begin match f with
          | L n -> spinor n | N n -> spinor n
          | U n -> spinor n | D n -> spinor n
          end
      | G f ->
          begin match f with
          | Ga | Gl -> Vector
          | Wp | Wm | Z -> Massive_Vector
          end
      | O f -> Scalar

    let color = function 
      | M (U n) -> Color.SUN (if n > 0 then 3 else -3)
      | M (D n) -> Color.SUN  (if n > 0 then 3 else -3)
      | G Gl -> Color.AdjSUN 3
      | _ -> Color.Singlet

    let prop_spinor n =
      if n >= 0 then
        Prop_Spinor
      else
        Prop_ConjSpinor

    let propagator = function
      | M f ->
          begin match f with
          | L n -> prop_spinor n | N n -> prop_spinor n
          | U n -> prop_spinor n | D n -> prop_spinor n
          end
      | G f ->
          begin match f with
          | Ga | Gl -> Prop_Feynman
          | Wp | Wm | Z -> Prop_Unitarity
          end
      | O f ->
          begin match f with
          | Phip | Phim | Phi0 -> Only_Insertion
          | H -> Prop_Scalar
          end

(* Optionally, ask for the fudge factor treatment for the widths of
   charged particles.  Currently, this only applies to $W^\pm$ and top. *)

    let width f =
      if !use_fudged_width then
        match f with
        | G Wp | G Wm | M (U 3) | M (U (-3)) -> Fudged
        | _ -> !default_width
      else
        !default_width

    let goldstone = function
      | G f ->
          begin match f with
          | Wp -> Some (O Phip, Coupling.Const 1)
          | Wm -> Some (O Phim, Coupling.Const 1)
          | Z -> Some (O Phi0, Coupling.Const 1)
          | _ -> None
          end
      | _ -> None

    let conjugate = function
      | M f ->
          M (begin match f with
          | L n -> L (-n) | N n -> N (-n)
          | U n -> U (-n) | D n -> D (-n)
          end)
      | G f ->
          G (begin match f with
          | Gl -> Gl | Ga -> Ga | Z -> Z
          | Wp -> Wm | Wm -> Wp
          end)
      | O f ->
          O (begin match f with
          | Phip -> Phim | Phim -> Phip | Phi0 -> Phi0
          | H -> H
          end)

    let fermion = function
      | M f ->
          begin match f with
          | L n -> if n > 0 then 1 else -1
          | N n -> if n > 0 then 1 else -1
          | U n -> if n > 0 then 1 else -1
          | D n -> if n > 0 then 1 else -1
          end
      | G f ->
          begin match f with
          | Gl | Ga | Z | Wp | Wm -> 0
          end
      | O _ -> 0

    let colsymm _ = (0, false), (0, false)

    type constant =
      | Unit | Pi | Alpha_QED | Sin2thw
      | Sinthw | Costhw | E | G_weak | Vev
      | Q_lepton | Q_up | Q_down | G_CC | G_CCQ of int*int
      | G_NC_neutrino | G_NC_lepton | G_NC_up | G_NC_down
      | I_Q_W | I_G_ZWW
      | G_WWWW | G_ZZWW | G_AZWW | G_AAWW
      | I_G1_AWW | I_G1_ZWW
      | I_G1_plus_kappa_plus_G4_AWW
      | I_G1_plus_kappa_plus_G4_ZWW
      | I_G1_plus_kappa_minus_G4_AWW
      | I_G1_plus_kappa_minus_G4_ZWW
      | I_G1_minus_kappa_plus_G4_AWW
      | I_G1_minus_kappa_plus_G4_ZWW
      | I_G1_minus_kappa_minus_G4_AWW
      | I_G1_minus_kappa_minus_G4_ZWW
      | I_lambda_AWW | I_lambda_ZWW
      | G5_AWW | G5_ZWW
      | I_kappa5_AWW | I_kappa5_ZWW 
      | I_lambda5_AWW | I_lambda5_ZWW
      | Alpha_WWWW0 | Alpha_ZZWW1 | Alpha_WWWW2
      | Alpha_ZZWW0 | Alpha_ZZZZ
      | D_Alpha_ZZWW0_S | D_Alpha_ZZWW0_T | D_Alpha_ZZWW1_S
      | D_Alpha_ZZWW1_T | D_Alpha_ZZWW1_U | D_Alpha_WWWW0_S
      | D_Alpha_WWWW0_T | D_Alpha_WWWW0_U | D_Alpha_WWWW2_S
      | D_Alpha_WWWW2_T | D_Alpha_ZZZZ_S | D_Alpha_ZZZZ_T
      | G_HWW | G_HHWW | G_HZZ | G_HHZZ
      | G_Htt | G_Hbb | G_Hcc | G_Hmm | G_Htautau | G_H3 | G_H4
      | G_HGaZ | G_HGaGa | G_Hgg
      | Gs | I_Gs | G2
      | Mass of flavor | Width of flavor
      | K_Matrix_Coeff of int | K_Matrix_Pole of int

(* \begin{dubious}
     The current abstract syntax for parameter dependencies is admittedly
     tedious. Later, there will be a parser for a convenient concrete syntax
     as a part of a concrete syntax for models.  But as these examples show,
     it should include simple functions.
   \end{dubious} *)

(* \begin{subequations}
     \begin{align}
        \alpha_{\text{QED}} &= \frac{1}{137.0359895} \\
             \sin^2\theta_w &= 0.23124
     \end{align}
   \end{subequations} *)
    let input_parameters =
      [ Alpha_QED, 1. /. 137.0359895;
        Sin2thw, 0.23124;
        Mass (G Z), 91.187;
        Mass (M (N 1)), 0.0; Mass (M (L 1)), 0.51099907e-3;
        Mass (M (N 2)), 0.0; Mass (M (L 2)), 0.105658389;
        Mass (M (N 3)), 0.0; Mass (M (L 3)), 1.77705;
        Mass (M (U 1)), 5.0e-3; Mass (M (D 1)), 3.0e-3;
        Mass (M (U 2)), 1.2; Mass (M (D 2)), 0.1;
        Mass (M (U 3)), 174.0; Mass (M (D 3)), 4.2 ]

(* \begin{subequations}
     \begin{align}
                        e &= \sqrt{4\pi\alpha} \\
             \sin\theta_w &= \sqrt{\sin^2\theta_w} \\
             \cos\theta_w &= \sqrt{1-\sin^2\theta_w} \\
                        g &= \frac{e}{\sin\theta_w} \\
                      m_W &= \cos\theta_w m_Z \\
                        v &= \frac{2m_W}{g} \\
                  g_{CC}   =
       -\frac{g}{2\sqrt2} &= -\frac{e}{2\sqrt2\sin\theta_w} \\
       Q_{\text{lepton}}   =
      -q_{\text{lepton}}e &= e \\
           Q_{\text{up}}   =
          -q_{\text{up}}e &= -\frac{2}{3}e \\
         Q_{\text{down}}   =
        -q_{\text{down}}e &= \frac{1}{3}e \\
        \ii q_We           =
        \ii g_{\gamma WW} &= \ii e \\
              \ii g_{ZWW} &= \ii g \cos\theta_w \\
              \ii g_{WWW} &= \ii g
     \end{align}
   \end{subequations} *)

(* \begin{dubious}
   \ldots{} to be continued \ldots{}
   The quartic couplings can't be correct, because the dimensions are wrong!
   \begin{subequations}
     \begin{align}
                  g_{HWW} &= g m_W = 2 \frac{m_W^2}{v}\\
                 g_{HHWW} &= 2 \frac{m_W^2}{v^2} = \frac{g^2}{2} \\
                  g_{HZZ} &= \frac{g}{\cos\theta_w}m_Z \\
                 g_{HHZZ} &= 2 \frac{m_Z^2}{v^2} = \frac{g^2}{2\cos\theta_w} \\
                  g_{Htt} &= \lambda_t \\
                  g_{Hbb} &= \lambda_b=\frac{m_b}{m_t}\lambda_t \\
                  g_{H^3} &= - \frac{3g}{2}\frac{m_H^2}{m_W} = - 3 \frac{m_H^2}{v} 
                  g_{H^4} &= - \frac{3g^2}{4} \frac{m_W^2}{v^2} = -3 \frac{m_H^2}{v^2}  
     \end{align}
   \end{subequations}
   \end{dubious} *)

    let derived_parameters =
      [ Real E, Sqrt (Prod [Const 4; Atom Pi; Atom Alpha_QED]);
        Real Sinthw, Sqrt (Atom Sin2thw);
        Real Costhw, Sqrt (Diff (Const 1, Atom Sin2thw));
        Real G_weak, Quot (Atom E, Atom Sinthw);
        Real (Mass (G Wp)), Prod [Atom Costhw; Atom (Mass (G Z))];
        Real Vev, Quot (Prod [Const 2; Atom (Mass (G Wp))], Atom G_weak);
        Real Q_lepton, Atom E;
        Real Q_up, Prod [Quot (Const (-2), Const 3); Atom E];
        Real Q_down, Prod [Quot (Const 1, Const 3); Atom E];
        Real G_CC, Neg (Quot (Atom G_weak, Prod [Const 2; Sqrt (Const 2)]));
        Complex I_Q_W, Prod [I; Atom E];
        Complex I_G_ZWW, Prod [I; Atom G_weak; Atom Costhw]]
             
(* \begin{equation}
      - \frac{g}{2\cos\theta_w}
   \end{equation} *)
    let g_over_2_costh =
      Quot (Neg (Atom G_weak), Prod [Const 2; Atom Costhw])

(* \begin{subequations}
     \begin{align}
           - \frac{g}{2\cos\theta_w} g_V
        &= - \frac{g}{2\cos\theta_w} (T_3 - 2 q \sin^2\theta_w) \\
           - \frac{g}{2\cos\theta_w} g_A
        &= - \frac{g}{2\cos\theta_w} T_3
     \end{align}
   \end{subequations} *)
    let nc_coupling c t3 q =
      (Real_Array c,
       [Prod [g_over_2_costh; Diff (t3, Prod [Const 2; q; Atom Sin2thw])];
        Prod [g_over_2_costh; t3]])

    let half = Quot (Const 1, Const 2)

    let derived_parameter_arrays =
      [ nc_coupling G_NC_neutrino half (Const 0);
        nc_coupling G_NC_lepton (Neg half) (Const (-1));
        nc_coupling G_NC_up half (Quot (Const 2, Const 3));
        nc_coupling G_NC_down (Neg half) (Quot (Const (-1), Const 3)) ]

    let parameters () =
      { input = input_parameters;
        derived = derived_parameters;
        derived_arrays = derived_parameter_arrays }

    module F = Fusions (struct
      type f = flavor
      type c = constant
      let compare = compare
      let conjugate = conjugate
    end)

(* \begin{equation}
     \mathcal{L}_{\textrm{EM}} =
        - e \sum_i q_i \bar\psi_i\fmslash{A}\psi_i
   \end{equation} *)

    let mgm ((m1, g, m2), fbf, c) = ((M m1, G g, M m2), fbf, c)

    let electromagnetic_currents n =
      List.map mgm
        [ ((L (-n), Ga, L n), FBF (1, Psibar, V, Psi), Q_lepton);
          ((U (-n), Ga, U n), FBF (1, Psibar, V, Psi), Q_up);
          ((D (-n), Ga, D n), FBF (1, Psibar, V, Psi), Q_down) ]
        
    let color_currents n =
      List.map mgm
        [ ((U (-n), Gl, U n), FBF ((-1), Psibar, V, Psi), Gs);
          ((D (-n), Gl, D n), FBF ((-1), Psibar, V, Psi), Gs) ]

(* \begin{equation}
     \mathcal{L}_{\textrm{NC}} =
        - \frac{g}{2\cos\theta_W}
            \sum_i \bar\psi_i\fmslash{Z}(g_V^i-g_A^i\gamma_5)\psi_i
   \end{equation} *)

    let neutral_currents n =
      List.map mgm
        [ ((L (-n), Z, L n), FBF (1, Psibar, VA, Psi), G_NC_lepton);
          ((N (-n), Z, N n), FBF (1, Psibar, VA, Psi), G_NC_neutrino);
          ((U (-n), Z, U n), FBF (1, Psibar, VA, Psi), G_NC_up);
          ((D (-n), Z, D n), FBF (1, Psibar, VA, Psi), G_NC_down) ] 

(* \begin{equation}
     \mathcal{L}_{\textrm{CC}} =
        - \frac{g}{2\sqrt2} \sum_i \bar\psi_i
               (T^+\fmslash{W}^+ + T^-\fmslash{W}^-)(1-\gamma_5)\psi_i 
   \end{equation} *)

    let charged_currents' n = 
      List.map mgm
        [ ((L (-n), Wm, N n), FBF (1, Psibar, VL, Psi), G_CC);
          ((N (-n), Wp, L n), FBF (1, Psibar, VL, Psi), G_CC) ] 

    let charged_currents'' n =
      List.map mgm 
        [ ((D (-n), Wm, U n), FBF (1, Psibar, VL, Psi), G_CC);
          ((U (-n), Wp, D n), FBF (1, Psibar, VL, Psi), G_CC) ] 

    let charged_currents_triv = 
      ThoList.flatmap charged_currents' [1;2;3] @
      ThoList.flatmap charged_currents'' [1;2;3]

    let charged_currents_ckm = 
      let charged_currents_2 n1 n2 = 
        List.map mgm 
          [ ((D (-n1), Wm, U n2), FBF (1, Psibar, VL, Psi), G_CCQ (n2,n1));
            ((U (-n1), Wp, D n2), FBF (1, Psibar, VL, Psi), G_CCQ (n1,n2)) ] in
      ThoList.flatmap charged_currents' [1;2;3] @ 
      List.flatten (Product.list2 charged_currents_2 [1;2;3] [1;2;3])

    let yukawa =
      [ ((M (U (-3)), O H, M (U 3)), FBF (1, Psibar, S, Psi), G_Htt);
        ((M (D (-3)), O H, M (D 3)), FBF (1, Psibar, S, Psi), G_Hbb);
        ((M (U (-2)), O H, M (U 2)), FBF (1, Psibar, S, Psi), G_Hcc);
        ((M (L (-2)), O H, M (L 2)), FBF (1, Psibar, S, Psi), G_Hmm);
        ((M (L (-3)), O H, M (L 3)), FBF (1, Psibar, S, Psi), G_Htautau) ]
      
(* \begin{equation}
     \mathcal{L}_{\textrm{TGC}} =
        - e \partial_\mu A_\nu W_+^\mu W_-^\nu + \ldots
        - e \cot\theta_w  \partial_\mu Z_\nu W_+^\mu W_-^\nu + \ldots
   \end{equation} *)

    let tgc ((g1, g2, g3), t, c) = ((G g1, G g2, G g3), t, c)

    let standard_triple_gauge =
      List.map tgc
        [ ((Ga, Wm, Wp), Gauge_Gauge_Gauge 1, I_Q_W);
          ((Z, Wm, Wp), Gauge_Gauge_Gauge 1, I_G_ZWW);
          ((Gl, Gl, Gl), Gauge_Gauge_Gauge 1, I_Gs)]

    let anomalous_triple_gauge =
      List.map tgc
        [ ((Ga, Wm, Wp), Dim4_Vector_Vector_Vector_T (-1),
           I_G1_AWW);
          ((Z, Wm, Wp), Dim4_Vector_Vector_Vector_T (-1),
           I_G1_ZWW);
          ((Wm, Ga, Wp), Dim4_Vector_Vector_Vector_T 1,
           I_G1_plus_kappa_minus_G4_AWW);
          ((Wm, Z, Wp), Dim4_Vector_Vector_Vector_T 1,
           I_G1_plus_kappa_minus_G4_ZWW);
          ((Wp, Ga, Wm), Dim4_Vector_Vector_Vector_T (-1),
           I_G1_plus_kappa_plus_G4_AWW);
          ((Wp, Z, Wm), Dim4_Vector_Vector_Vector_T (-1),
           I_G1_plus_kappa_plus_G4_ZWW);
          ((Wm, Ga, Wp), Dim4_Vector_Vector_Vector_L (-1),
           I_G1_minus_kappa_plus_G4_AWW);
          ((Wm, Z, Wp), Dim4_Vector_Vector_Vector_L (-1),
           I_G1_minus_kappa_plus_G4_ZWW);
          ((Wp, Ga, Wm), Dim4_Vector_Vector_Vector_L 1,
           I_G1_minus_kappa_minus_G4_AWW);
          ((Wp, Z, Wm), Dim4_Vector_Vector_Vector_L 1,
           I_G1_minus_kappa_minus_G4_ZWW);
          ((Ga, Wm, Wp), Dim4_Vector_Vector_Vector_T5 (-1),
           I_kappa5_AWW);
          ((Z, Wm, Wp), Dim4_Vector_Vector_Vector_T5 (-1),
           I_kappa5_ZWW);
          ((Ga, Wm, Wp), Dim4_Vector_Vector_Vector_L5 (-1),
           G5_AWW);
          ((Z, Wm, Wp), Dim4_Vector_Vector_Vector_L5 (-1),
           G5_ZWW);
          ((Ga, Wp, Wm), Dim6_Gauge_Gauge_Gauge (-1),
           I_lambda_AWW);
          ((Z, Wp, Wm), Dim6_Gauge_Gauge_Gauge (-1),
           I_lambda_ZWW);
          ((Ga, Wp, Wm), Dim6_Gauge_Gauge_Gauge_5 (-1),
           I_lambda5_AWW);
          ((Z, Wp, Wm), Dim6_Gauge_Gauge_Gauge_5 (-1),
           I_lambda5_ZWW) ]

    let triple_gauge =
      if Flags.triple_anom then
        anomalous_triple_gauge
      else
        standard_triple_gauge

(* \begin{equation}
     \mathcal{L}_{\textrm{QGC}} =
        - g^2 W_{+,\mu} W_{-,\nu} W_+^\mu W_-^\nu + \ldots
   \end{equation} *)

(* Actually, quartic gauge couplings are a little bit more straightforward
   using auxiliary fields.  Here we have to impose the antisymmetry manually:
   \begin{subequations}
   \begin{multline}
     (W^{+,\mu}_1 W^{-,\nu}_2 - W^{+,\nu}_1 W^{-,\mu}_2)
     (W^+_{3,\mu} W^-_{4,\nu} - W^+_{3,\nu} W^-_{4,\mu}) \\
        = 2(W^+_1W^+_3)(W^-_2W^-_4) - 2(W^+_1W^-_4)(W^-_2W^+_3)
   \end{multline}
   also ($V$ can be $A$ or $Z$)
   \begin{multline}
     (W^{+,\mu}_1 V^\nu_2 - W^{+,\nu}_1 V^\mu_2)
     (W^-_{3,\mu} V_{4,\nu} - W^-_{3,\nu} V_{4,\mu}) \\
        = 2(W^+_1W^-_3)(V_2V_4) - 2(W^+_1V_4)(V_2W^-_3)
   \end{multline}
   \end{subequations} *)

(* \begin{subequations}
   \begin{multline}
      W^{+,\mu} W^{-,\nu} W^+_\mu W^-_\nu
   \end{multline}
   \end{subequations} *)

    let qgc ((g1, g2, g3, g4), t, c) = ((G g1, G g2, G g3, G g4), t, c)

    let gauge4 = Vector4 [(2, C_13_42); (-1, C_12_34); (-1, C_14_23)]
    let minus_gauge4 = Vector4 [(-2, C_13_42); (1, C_12_34); (1, C_14_23)]
    let standard_quartic_gauge =
      List.map qgc
        [ (Wm, Wp, Wm, Wp), gauge4, G_WWWW;
          (Wm, Z, Wp, Z), minus_gauge4, G_ZZWW;
          (Wm, Z, Wp, Ga), minus_gauge4, G_AZWW;
          (Wm, Ga, Wp, Ga), minus_gauge4, G_AAWW;
          (Gl, Gl, Gl, Gl), gauge4, G2 ]

(* \begin{subequations}
   \begin{align}
     \mathcal{L}_4
       &= \alpha_4 \left(   \frac{g^4}{2}\left(   (W^+_\mu W^{-,\mu})^2
                                                + W^+_\mu W^{+,\mu} W^-_\mu W^{-,\mu}
                                               \right)\right.\notag \\
       &\qquad\qquad\qquad \left.
                          + \frac{g^4}{\cos^2\theta_w} W^+_\mu Z^\mu W^-_\nu Z^\nu
                          + \frac{g^4}{4\cos^4\theta_w} (Z_\mu Z^\mu)^2 \right) \\
     \mathcal{L}_5
       &= \alpha_5 \left(   g^4 (W^+_\mu W^{-,\mu})^2
                          + \frac{g^4}{\cos^2\theta_w}  W^+_\mu W^{-,\mu} Z_\nu Z^\nu
                          + \frac{g^4}{4\cos^4\theta_w} (Z_\mu Z^\mu)^2 \right)
   \end{align}
   \end{subequations}
   or
   \begin{multline}
     \mathcal{L}_4 + \mathcal{L}_5
       =   (\alpha_4+2\alpha_5) g^4 \frac{1}{2} (W^+_\mu W^{-,\mu})^2 \\
         + 2\alpha_4 g^4 \frac{1}{4} W^+_\mu W^{+,\mu} W^-_\mu W^{-,\mu}
         + \alpha_4 \frac{g^4}{\cos^2\theta_w} W^+_\mu Z^\mu W^-_\nu Z^\nu \\
         + 2\alpha_5 \frac{g^4}{\cos^2\theta_w} \frac{1}{2} W^+_\mu W^{-,\mu} Z_\nu Z^\nu
         + (2\alpha_4 + 2\alpha_5) \frac{g^4}{\cos^4\theta_w} \frac{1}{8} (Z_\mu Z^\mu)^2
   \end{multline}
   and therefore
   \begin{subequations}
   \begin{align}
     \alpha_{(WW)_0} &= (\alpha_4+2\alpha_5) g^4 \\
     \alpha_{(WW)_2} &= 2\alpha_4 g^4 \\
     \alpha_{(WZ)_0} &= 2\alpha_5 \frac{g^4}{\cos^2\theta_w} \\
     \alpha_{(WZ)_1} &= \alpha_4 \frac{g^4}{\cos^2\theta_w} \\
     \alpha_{ZZ} &= (2\alpha_4 + 2\alpha_5) \frac{g^4}{\cos^4\theta_w}
   \end{align}
   \end{subequations} *)

    let anomalous_quartic_gauge =
      if Flags.quartic_anom then
        List.map qgc
          [ ((Wm, Wm, Wp, Wp),
             Vector4 [(1, C_13_42); (1, C_14_23)], Alpha_WWWW0);
            ((Wm, Wm, Wp, Wp),
             Vector4 [1, C_12_34], Alpha_WWWW2);
            ((Wm, Wp, Z, Z),
             Vector4 [1, C_12_34], Alpha_ZZWW0);
            ((Wm, Wp, Z, Z),
             Vector4 [(1, C_13_42); (1, C_14_23)], Alpha_ZZWW1);
            ((Z, Z, Z, Z),
             Vector4 [(1, C_12_34); (1, C_13_42); (1, C_14_23)], Alpha_ZZZZ) ]
      else
        []

(* In any diagonal channel~$\chi$, the scattering amplitude~$a_\chi(s)$ is
   unitary iff\footnote{%
     Trivial proof:
     \begin{equation}
       -1 = \textrm{Im}\left(\frac{1}{a_\chi(s)}\right)
          = \frac{\textrm{Im}(a_\chi^*(s))}{|a_\chi(s)|^2}
          = - \frac{\textrm{Im}(a_\chi(s))}{|a_\chi(s)|^2}
     \end{equation}
     i.\,e.~$\textrm{Im}(a_\chi(s)) = |a_\chi(s)|^2$.}
   \begin{equation}
     \textrm{Im}\left(\frac{1}{a_\chi(s)}\right) = -1
   \end{equation}
   For a real perturbative scattering amplitude~$r_\chi(s)$ this can be
   enforced easily--and arbitrarily--by
   \begin{equation}
     \frac{1}{a_\chi(s)} = \frac{1}{r_\chi(s)} - \mathrm{i}
   \end{equation} 

*)

    let k_matrix_quartic_gauge =
      if Flags.k_matrix then
        List.map qgc
          [ ((Wm, Wp, Wm, Wp), Vector4_K_Matrix_jr (0,
                   [(1, C_12_34)]), D_Alpha_WWWW0_S);
            ((Wm, Wp, Wm, Wp), Vector4_K_Matrix_jr (0,
                   [(1, C_14_23)]), D_Alpha_WWWW0_T);
            ((Wm, Wp, Wm, Wp), Vector4_K_Matrix_jr (0,
                   [(1, C_13_42)]), D_Alpha_WWWW0_U);
            ((Wp, Wm, Wp, Wm), Vector4_K_Matrix_jr (0,
                   [(1, C_12_34)]), D_Alpha_WWWW0_S);
            ((Wp, Wm, Wp, Wm), Vector4_K_Matrix_jr (0,
                   [(1, C_14_23)]), D_Alpha_WWWW0_T);
            ((Wp, Wm, Wp, Wm), Vector4_K_Matrix_jr (0,
                   [(1, C_13_42)]), D_Alpha_WWWW0_U);
            ((Wm, Wm, Wp, Wp), Vector4_K_Matrix_jr (0,
                   [(1, C_12_34)]), D_Alpha_WWWW2_S);
            ((Wm, Wm, Wp, Wp), Vector4_K_Matrix_jr (0,
                   [(1, C_13_42); (1, C_14_23)]), D_Alpha_WWWW2_T);
            ((Wm, Wp, Z, Z), Vector4_K_Matrix_jr (0,
                   [(1, C_12_34)]), D_Alpha_ZZWW0_S);
            ((Wm, Wp, Z, Z), Vector4_K_Matrix_jr (0,
                   [(1, C_13_42); (1, C_14_23)]), D_Alpha_ZZWW0_T);
            ((Wm, Z, Wp, Z), Vector4_K_Matrix_jr (0,
                   [(1, C_12_34)]), D_Alpha_ZZWW1_S);
            ((Wm, Z, Wp, Z), Vector4_K_Matrix_jr (0,
                   [(1, C_13_42)]), D_Alpha_ZZWW1_T);
            ((Wm, Z, Wp, Z), Vector4_K_Matrix_jr (0,
                   [(1, C_14_23)]), D_Alpha_ZZWW1_U);
            ((Wp, Z, Z, Wm), Vector4_K_Matrix_jr (1,
                   [(1, C_12_34)]), D_Alpha_ZZWW1_S);
            ((Wp, Z, Z, Wm), Vector4_K_Matrix_jr (1,
                   [(1, C_13_42)]), D_Alpha_ZZWW1_U);
            ((Wp, Z, Z, Wm), Vector4_K_Matrix_jr (1,
                   [(1, C_14_23)]), D_Alpha_ZZWW1_T); 
            ((Z, Wp, Wm, Z), Vector4_K_Matrix_jr (2,
                   [(1, C_12_34)]), D_Alpha_ZZWW1_S); 
            ((Z, Wp, Wm, Z), Vector4_K_Matrix_jr (2,
                   [(1, C_13_42)]), D_Alpha_ZZWW1_U);
            ((Z, Wp, Wm, Z), Vector4_K_Matrix_jr (2,
                   [(1, C_14_23)]), D_Alpha_ZZWW1_T); 
            ((Z, Z, Z, Z), Vector4_K_Matrix_jr (0,
                   [(1, C_12_34)]), D_Alpha_ZZZZ_S);
            ((Z, Z, Z, Z), Vector4_K_Matrix_jr (0,
                   [(1, C_13_42); (1, C_14_23)]), D_Alpha_ZZZZ_T); 
            ((Z, Z, Z, Z), Vector4_K_Matrix_jr (3,
                   [(1, C_14_23)]), D_Alpha_ZZZZ_S);
            ((Z, Z, Z, Z), Vector4_K_Matrix_jr (3,
                   [(1, C_13_42); (1, C_12_34)]), D_Alpha_ZZZZ_T) ]
      else
        []


(*i Thorsten's original implementation of the K matrix, which we keep since
   it still might be usefull for the future. 

    let k_matrix_quartic_gauge =
      if Flags.k_matrix then
        List.map qgc
          [ ((Wm, Wp, Wm, Wp), Vector4_K_Matrix_tho (true, [K_Matrix_Coeff 0, 
                         K_Matrix_Pole 0]), Alpha_WWWW0);
            ((Wm, Wm, Wp, Wp), Vector4_K_Matrix_tho (true, [K_Matrix_Coeff 2, 
                         K_Matrix_Pole 2]), Alpha_WWWW2);
            ((Wm, Wp, Z, Z), Vector4_K_Matrix_tho (true, [(K_Matrix_Coeff 0, 
                         K_Matrix_Pole 0); (K_Matrix_Coeff 2, 
                         K_Matrix_Pole 2)]), Alpha_ZZWW0);
            ((Wm, Z, Wp, Z), Vector4_K_Matrix_tho (true, [K_Matrix_Coeff 1, 
                         K_Matrix_Pole 1]), Alpha_ZZWW1);
            ((Z, Z, Z, Z), Vector4_K_Matrix_tho (0, [K_Matrix_Coeff 0, 
                         K_Matrix_Pole 0]), Alpha_ZZZZ) ]
      else
        []
i*)

    let quartic_gauge =
      standard_quartic_gauge @ anomalous_quartic_gauge @ k_matrix_quartic_gauge

    let standard_gauge_higgs =
      [ ((O H, G Wp, G Wm), Scalar_Vector_Vector 1, G_HWW);
        ((O H, G Z, G Z), Scalar_Vector_Vector 1, G_HZZ) ]

    let standard_gauge_higgs4 =
      [ (O H, O H, G Wp, G Wm), Scalar2_Vector2 1, G_HHWW;
        (O H, O H, G Z, G Z), Scalar2_Vector2 1, G_HHZZ ]
       
    let standard_higgs =
      [ (O H, O H, O H), Scalar_Scalar_Scalar 1, G_H3 ]

    let standard_higgs4 =
      [ (O H, O H, O H, O H), Scalar4 1, G_H4 ]

(* WK's couplings (apparently, he still intends to divide by
   $\Lambda^2_{\text{EWSB}}=16\pi^2v_{\mathrm{F}}^2$):
   \begin{subequations}
   \begin{align}
     \mathcal{L}^{\tau}_4 &=
      \left\lbrack (\partial_{\mu}H)(\partial^{\mu}H)
                     + \frac{g^2v_{\mathrm{F}}^2}{4} V_{\mu} V^{\mu} \right\rbrack^2 \\
     \mathcal{L}^{\tau}_5 &=
      \left\lbrack (\partial_{\mu}H)(\partial_{\nu}H)
                     + \frac{g^2v_{\mathrm{F}}^2}{4} V_{\mu} V_{\nu} \right\rbrack^2
   \end{align}
   \end{subequations}
   with
   \begin{equation}
      V_{\mu} V_{\nu} =
        \frac{1}{2} \left( W^+_{\mu} W^-_{\nu} + W^+_{\nu} W^-_{\mu} \right)
         + \frac{1}{2\cos^2\theta_{w}} Z_{\mu} Z_{\nu}
   \end{equation}
   (note the symmetrization!), i.\,e.
   \begin{subequations}
   \begin{align}
     \mathcal{L}_4 &= \alpha_4 \frac{g^4v_{\mathrm{F}}^4}{16} (V_{\mu} V_{\nu})^2 \\
     \mathcal{L}_5 &= \alpha_5 \frac{g^4v_{\mathrm{F}}^4}{16} (V_{\mu} V^{\mu})^2
   \end{align}
   \end{subequations} *)

(* Breaking thinks up
   \begin{subequations}
   \begin{align}
     \mathcal{L}^{\tau,H^4}_4 &=
       \left\lbrack (\partial_{\mu}H)(\partial^{\mu}H) \right\rbrack^2 \\
     \mathcal{L}^{\tau,H^4}_5 &=
       \left\lbrack (\partial_{\mu}H)(\partial^{\mu}H) \right\rbrack^2
   \end{align}
   \end{subequations}
   and
   \begin{subequations}
   \begin{align}
     \mathcal{L}^{\tau,H^2V^2}_4 &= \frac{g^2v_{\mathrm{F}}^2}{2}
              (\partial_{\mu}H)(\partial^{\mu}H) V_{\mu}V^{\mu}   \\
     \mathcal{L}^{\tau,H^2V^2}_5 &= \frac{g^2v_{\mathrm{F}}^2}{2}
              (\partial_{\mu}H)(\partial_{\nu}H) V_{\mu}V_{\nu}
   \end{align}
   \end{subequations}
   i.\,e.
   \begin{subequations}
   \begin{align}
     \mathcal{L}^{\tau,H^2V^2}_4 &=
        \frac{g^2v_{\mathrm{F}}^2}{2}
          \left\lbrack
              (\partial_{\mu}H)(\partial^{\mu}H) W^+_{\nu}W^{-,\nu}
            + \frac{1}{2\cos^2\theta_{w}} (\partial_{\mu}H)(\partial^{\mu}H) Z_{\nu} Z^{\nu}
          \right\rbrack \\
     \mathcal{L}^{\tau,H^2V^2}_5 &=
          \frac{g^2v_{\mathrm{F}}^2}{2}
          \left\lbrack
              (W^{+,\mu}\partial_{\mu}H) (W^{-,\nu}\partial_{\nu}H)
            + \frac{1}{2\cos^2\theta_{w}} (Z^{\mu}\partial_{\mu}H)(Z^{\nu}\partial_{\nu}H)
          \right\rbrack
   \end{align}
   \end{subequations} *)

(* \begin{multline}
     \tau^4_8 \mathcal{L}^{\tau,H^2V^2}_4 + \tau^5_8 \mathcal{L}^{\tau,H^2V^2}_5 = \\
       - \frac{g^2v_{\mathrm{F}}^2}{2} \Biggl\lbrack
            2\tau^4_8
              \frac{1}{2}(\ii\partial_{\mu}H)(\ii\partial^{\mu}H) W^+_{\nu}W^{-,\nu}
          + \tau^5_8
              (W^{+,\mu}\ii\partial_{\mu}H) (W^{-,\nu}\ii\partial_{\nu}H) \\
          + \frac{2\tau^4_8}{\cos^2\theta_{w}}
              \frac{1}{4} (\ii\partial_{\mu}H)(\ii\partial^{\mu}H) Z_{\nu} Z^{\nu}
          + \frac{\tau^5_8}{\cos^2\theta_{w}}
              \frac{1}{2} (Z^{\mu}\ii\partial_{\mu}H)(Z^{\nu}\ii\partial_{\nu}H)
          \Biggr\rbrack
   \end{multline}
   where the two powers of $\ii$ make the sign conveniently negative,
   i.\,e.
   \begin{subequations}
   \begin{align}
     \alpha_{(\partial H)^2W^2}^2 &= \tau^4_8 g^2v_{\mathrm{F}}^2\\
     \alpha_{(\partial HW)^2}^2 &= \frac{\tau^5_8 g^2v_{\mathrm{F}}^2}{2}  \\
     \alpha_{(\partial H)^2Z^2}^2 &= \frac{\tau^4_8 g^2v_{\mathrm{F}}^2}{\cos^2\theta_{w}} \\ 
     \alpha_{(\partial HZ)^2}^2 &=\frac{\tau^5_8 g^2v_{\mathrm{F}}^2}{2\cos^2\theta_{w}}
   \end{align}
   \end{subequations} *)

    let anomalous_gauge_higgs =
      []

    let anomalous_gauge_higgs4 =
      []

    let anomalous_higgs =
      []

    let anomaly_higgs = 
      []
(*i      [ (O H, G Ga, G Ga), Dim5_Scalar_Gauge2 1, G_HGaGa;
        (O H, G Ga, G Z), Dim5_Scalar_Gauge2 1, G_HGaZ;
        (O H, G Gl, G Gl), Dim5_Scalar_Gauge2 1, G_Hgg ] i*)

    let anomalous_higgs4 =
      []

    let gauge_higgs =
      if Flags.higgs_anom then
        standard_gauge_higgs @ anomalous_gauge_higgs
      else
        standard_gauge_higgs

    let gauge_higgs4 =
      if Flags.higgs_anom then
        standard_gauge_higgs4 @ anomalous_gauge_higgs4
      else
        standard_gauge_higgs4

    let higgs =
      if Flags.higgs_anom then
        standard_higgs @ anomalous_higgs
      else
        standard_higgs

    let higgs4 =
      if Flags.higgs_anom then
        standard_higgs4 @ anomalous_higgs4
      else
        standard_higgs4

    let goldstone_vertices =
      [ ((O Phi0, G Wm, G Wp), Scalar_Vector_Vector 1, I_G_ZWW);
        ((O Phip, G Ga, G Wm), Scalar_Vector_Vector 1, I_Q_W);
        ((O Phip, G Z, G Wm), Scalar_Vector_Vector 1, I_G_ZWW);
        ((O Phim, G Wp, G Ga), Scalar_Vector_Vector 1, I_Q_W);
        ((O Phim, G Wp, G Z), Scalar_Vector_Vector 1, I_G_ZWW) ]

    let vertices3 =
      (ThoList.flatmap electromagnetic_currents [1;2;3] @
       ThoList.flatmap color_currents [1;2;3] @
       ThoList.flatmap neutral_currents [1;2;3] @
       (if Flags.ckm_present then
         charged_currents_ckm
       else
         charged_currents_triv) @
       yukawa @ triple_gauge @
       gauge_higgs @ higgs @ anomaly_higgs 
       @ goldstone_vertices)

    let vertices4 =
      quartic_gauge @ gauge_higgs4 @ higgs4

    let vertices () = (vertices3, vertices4, [])

(* For efficiency, make sure that [F.of_vertices vertices] is
   evaluated only once. *)

    let table = F.of_vertices (vertices ())
    let fuse2 = F.fuse2 table
    let fuse3 = F.fuse3 table
    let fuse = F.fuse table
    let max_degree () = 4

    let flavor_of_string = function
      | "e-" -> M (L 1) | "e+" -> M (L (-1))
      | "mu-" -> M (L 2) | "mu+" -> M (L (-2))
      | "tau-" -> M (L 3) | "tau+" -> M (L (-3))
      | "nue" -> M (N 1) | "nuebar" -> M (N (-1))
      | "numu" -> M (N 2) | "numubar" -> M (N (-2))
      | "nutau" -> M (N 3) | "nutaubar" -> M (N (-3))
      | "u" -> M (U 1) | "ubar" -> M (U (-1))
      | "c" -> M (U 2) | "cbar" -> M (U (-2))
      | "t" -> M (U 3) | "tbar" -> M (U (-3))
      | "d" -> M (D 1) | "dbar" -> M (D (-1))
      | "s" -> M (D 2) | "sbar" -> M (D (-2))
      | "b" -> M (D 3) | "bbar" -> M (D (-3))
      | "g" | "gl" -> G Gl
      | "A" -> G Ga | "Z" | "Z0" -> G Z
      | "W+" -> G Wp | "W-" -> G Wm
      | "H" -> O H
      | _ -> invalid_arg "Models.SM.flavor_of_string"

    let flavor_to_string = function
      | M f ->
          begin match f with
          | L 1 -> "e-" | L (-1) -> "e+"
          | L 2 -> "mu-" | L (-2) -> "mu+"
          | L 3 -> "tau-" | L (-3) -> "tau+"
          | L _ -> invalid_arg
                "Models.SM.flavor_to_string: invalid lepton"
          | N 1 -> "nue" | N (-1) -> "nuebar"
          | N 2 -> "numu" | N (-2) -> "numubar"
          | N 3 -> "nutau" | N (-3) -> "nutaubar"
          | N _ -> invalid_arg
                "Models.SM.flavor_to_string: invalid neutrino"
          | U 1 -> "u" | U (-1) -> "ubar"
          | U 2 -> "c" | U (-2) -> "cbar"
          | U 3 -> "t" | U (-3) -> "tbar"
          | U _ -> invalid_arg
                "Models.SM.flavor_to_string: invalid up type quark"
          | D 1 -> "d" | D (-1) -> "dbar"
          | D 2 -> "s" | D (-2) -> "sbar"
          | D 3 -> "b" | D (-3) -> "bbar"
          | D _ -> invalid_arg
                "Models.SM.flavor_to_string: invalid down type quark"
          end
      | G f ->
          begin match f with
          | Gl -> "gl"
          | Ga -> "A" | Z -> "Z"
          | Wp -> "W+" | Wm -> "W-"
          end
      | O f ->
          begin match f with
          | Phip -> "phi+" | Phim -> "phi-" | Phi0 -> "phi0" 
          | H -> "H"
          end

    let flavor_to_TeX = function
      | M f ->
          begin match f with
          | L 1 -> "e^-" | L (-1) -> "e^+"
          | L 2 -> "\\mu^-" | L (-2) -> "\\mu^+"
          | L 3 -> "\\tau^-" | L (-3) -> "\\tau^+"
          | L _ -> invalid_arg
                "Models.SM.flavor_to_TeX: invalid lepton"
          | N 1 -> "\\nu_e" | N (-1) -> "\\bar{\\nu}_e"
          | N 2 -> "\\nu_\\mu" | N (-2) -> "\\bar{\\nu}_\\mu"
          | N 3 -> "\\nu_\\tau" | N (-3) -> "\\bar{\\nu}_\\tau"
          | N _ -> invalid_arg
                "Models.SM.flavor_to_TeX: invalid neutrino"
          | U 1 -> "u" | U (-1) -> "\\bar{u}"
          | U 2 -> "c" | U (-2) -> "\\bar{c}"
          | U 3 -> "t" | U (-3) -> "\\bar{t}"
          | U _ -> invalid_arg
                "Models.SM.flavor_to_TeX: invalid up type quark"
          | D 1 -> "d" | D (-1) -> "\\bar{d}"
          | D 2 -> "s" | D (-2) -> "\\bar{s}"
          | D 3 -> "b" | D (-3) -> "\\bar{b}"
          | D _ -> invalid_arg
                "Models.SM.flavor_to_TeX: invalid down type quark"
          end
      | G f ->
          begin match f with
          | Gl -> "g"
          | Ga -> "\\gamma" | Z -> "Z"
          | Wp -> "W^+" | Wm -> "W^-"
          end
      | O f ->
          begin match f with
          | Phip -> "\\phi^+" | Phim -> "\\phi^-" | Phi0 -> "\\phi^0" 
          | H -> "H"
          end

    let flavor_symbol = function
      | M f ->
          begin match f with
          | L n when n > 0 -> "l" ^ string_of_int n
          | L n -> "l" ^ string_of_int (abs n) ^ "b"
          | N n when n > 0 -> "n" ^ string_of_int n
          | N n -> "n" ^ string_of_int (abs n) ^ "b"
          | U n when n > 0 -> "u" ^ string_of_int n
          | U n -> "u" ^ string_of_int (abs n) ^ "b"
          | D n when n > 0 ->  "d" ^ string_of_int n
          | D n -> "d" ^ string_of_int (abs n) ^ "b"
          end
      | G f ->
          begin match f with
          | Gl -> "gl"
          | Ga -> "a" | Z -> "z"
          | Wp -> "wp" | Wm -> "wm"
          end
      | O f ->
          begin match f with
          | Phip -> "pp" | Phim -> "pm" | Phi0 -> "p0" 
          | H -> "h"
          end

    let pdg = function
      | M f ->
          begin match f with
          | L n when n > 0 -> 9 + 2*n
          | L n -> - 9 + 2*n
          | N n when n > 0 -> 10 + 2*n
          | N n -> - 10 + 2*n
          | U n when n > 0 -> 2*n
          | U n -> 2*n
          | D n when n > 0 -> - 1 + 2*n
          | D n -> 1 + 2*n
          end
      | G f ->
          begin match f with
          | Gl -> 21
          | Ga -> 22 | Z -> 23
          | Wp -> 24 | Wm -> (-24)
          end
      | O f ->
          begin match f with
          | Phip | Phim -> 27 | Phi0 -> 26
          | H -> 25
          end

    let mass_symbol f = 
      "mass(" ^ string_of_int (abs (pdg f)) ^ ")"

    let width_symbol f =
      "width(" ^ string_of_int (abs (pdg f)) ^ ")"

    let constant_symbol = function
      | Unit -> "unit" | Pi -> "PI"
      | Alpha_QED -> "alpha" | E -> "e" | G_weak -> "g" | Vev -> "vev"
      | Sin2thw -> "sin2thw" | Sinthw -> "sinthw" | Costhw -> "costhw"
      | Q_lepton -> "qlep" | Q_up -> "qup" | Q_down -> "qdwn"
      | G_NC_lepton -> "gnclep" | G_NC_neutrino -> "gncneu"
      | G_NC_up -> "gncup" | G_NC_down -> "gncdwn"
      | G_CC -> "gcc"
      | G_CCQ (n1,n2) -> "gccq" ^ string_of_int n1 ^ string_of_int n2
      | I_Q_W -> "iqw" | I_G_ZWW -> "igzww" 
      | G_WWWW -> "gw4" | G_ZZWW -> "gzzww"
      | G_AZWW -> "gazww" | G_AAWW -> "gaaww"
      | I_G1_AWW -> "ig1a" | I_G1_ZWW -> "ig1z"
      | I_G1_plus_kappa_plus_G4_AWW -> "ig1pkpg4a"
      | I_G1_plus_kappa_plus_G4_ZWW -> "ig1pkpg4z"
      | I_G1_plus_kappa_minus_G4_AWW -> "ig1pkmg4a"
      | I_G1_plus_kappa_minus_G4_ZWW -> "ig1pkmg4z"
      | I_G1_minus_kappa_plus_G4_AWW -> "ig1mkpg4a"
      | I_G1_minus_kappa_plus_G4_ZWW -> "ig1mkpg4z"
      | I_G1_minus_kappa_minus_G4_AWW -> "ig1mkmg4a"
      | I_G1_minus_kappa_minus_G4_ZWW -> "ig1mkmg4z"
      | I_lambda_AWW -> "ila"
      | I_lambda_ZWW -> "ilz"
      | G5_AWW -> "rg5a"
      | G5_ZWW -> "rg5z"
      | I_kappa5_AWW -> "ik5a"
      | I_kappa5_ZWW -> "ik5z"
      | I_lambda5_AWW -> "il5a" | I_lambda5_ZWW -> "il5z"
      | Alpha_WWWW0 -> "alww0" | Alpha_WWWW2 -> "alww2"
      | Alpha_ZZWW0 -> "alzw0" | Alpha_ZZWW1 -> "alzw1"
      | Alpha_ZZZZ  -> "alzz"
      | D_Alpha_ZZWW0_S -> "dalzz0_s(gkm,mkm,"
      | D_Alpha_ZZWW0_T -> "dalzz0_t(gkm,mkm,"
      | D_Alpha_ZZWW1_S -> "dalzz1_s(gkm,mkm,"
      | D_Alpha_ZZWW1_T -> "dalzz1_t(gkm,mkm,"
      | D_Alpha_ZZWW1_U -> "dalzz1_u(gkm,mkm,"
      | D_Alpha_WWWW0_S -> "dalww0_s(gkm,mkm,"
      | D_Alpha_WWWW0_T -> "dalww0_t(gkm,mkm,"
      | D_Alpha_WWWW0_U -> "dalww0_u(gkm,mkm,"
      | D_Alpha_WWWW2_S -> "dalww2_s(gkm,mkm,"
      | D_Alpha_WWWW2_T -> "dalww2_t(gkm,mkm,"
      | D_Alpha_ZZZZ_S -> "dalz4_s(gkm,mkm,"
      | D_Alpha_ZZZZ_T -> "dalz4_t(gkm,mkm,"
      | G_HWW -> "ghww" | G_HZZ -> "ghzz"
      | G_HHWW -> "ghhww" | G_HHZZ -> "ghhzz"
      | G_Htt -> "ghtt" | G_Hbb -> "ghbb"
      | G_Htautau -> "ghtautau" | G_Hcc -> "ghcc" | G_Hmm -> "ghmm"
      | G_HGaZ -> "ghgaz" | G_HGaGa -> "ghgaga" | G_Hgg -> "ghgg"
      | G_H3 -> "gh3" | G_H4 -> "gh4"
      | Gs -> "gs" | I_Gs -> "igs" | G2 -> "gs**2"
      | Mass f -> "mass" ^ flavor_symbol f
      | Width f -> "width" ^ flavor_symbol f
      | K_Matrix_Coeff i -> "kc" ^ string_of_int i
      | K_Matrix_Pole i -> "kp" ^ string_of_int i

  end

(* \thocwmodulesection{Complete Minimal Standard Model with Genuine Quartic Couplings and Colors for Whizard Scripting} *)

module SM_Col (Flags : SM_flags) =
  struct
    let rcs = RCS.rename rcs_file "Models.SM_Col"
        [ "minimal electroweak standard model in unitarity gauge";
          "with colors for Whizard scripting" ]

    open Coupling

    let default_width = ref Timelike
    let use_fudged_width = ref false

    type col = 
        Q of int

    let options = Options.create
      [ "constant_width", Arg.Unit (fun () -> default_width := Constant),
        "use constant width (also in t-channel)";
        "fudged_width", Arg.Set use_fudged_width,
        "use fudge factor for charge particle width";
        "custom_width", Arg.String (fun f -> default_width := Custom f),
        "use custom width";
        "cancel_widths", Arg.Unit (fun () -> default_width := Vanishing),
        "use vanishing width"]

    type matter_field = L of int | N of int | U of int*col | D of int*col
    type gauge_boson = Ga | Wp | Wm | Z | Gl of col*col | Gl0
    type other = Phip | Phim | Phi0 | H
    type flavor = M of matter_field | G of gauge_boson | O of other

    let matter_field f = M f
    let gauge_boson f = G f
    let other f = O f

    type field =
      | Matter of matter_field
      | Gauge of gauge_boson
      | Other of other

    let field = function
      | M f -> Matter f
      | G f -> Gauge f
      | O f -> Other f

    type gauge = unit

    let gauge_symbol () =
      failwith "Models.SM_Col.gauge_symbol: internal error"

    let nc = 6

    let nc_list = 
      ThoList.range 1 nc 

    let choose2 set = 
      List.map (function [x;y] -> (x,y) | _ -> failwith "choose2") 
        (Combinatorics.choose 2 set)

    let inequ_pairs = 
      choose2 nc_list @ choose2 (List.rev nc_list)
        
    let triple_col = 
      List.map (function [x;y;z] -> (x,y,z) | _ -> 
        failwith "triple_col") (Combinatorics.choose 3 nc_list)

    let quartic_col = 
      List.map (function [r;s;t;u] -> (r,s,t,u) | _ -> 
        failwith "quartic_col") (Combinatorics.choose 4 nc_list)

    let lep_family n = List.map matter_field [ L n; N n ]
    let quark_family n c = List.map matter_field [ U (n,Q c); U (-n,Q (-c));
                                                   D (n,Q c); D (-n,Q (-c))]
    let gluons (n,m) = [G (Gl (Q n, Q (-m)))]

    let external_flavors () =
      [ "1st Generation leptons", ThoList.flatmap lep_family [1; -1];
        "2nd Generation leptons", ThoList.flatmap lep_family [2; -2];
        "3rd Generation leptons", ThoList.flatmap lep_family [3; -3];
        "Quarks", List.flatten (Product.list2 quark_family [1;2;3] nc_list);
        "Electroweak Gauge Bosons", List.map gauge_boson [Ga; Z; Wp; Wm];
        "Gluons",  ThoList.flatmap gluons inequ_pairs @ [G Gl0];
        "Higgs", List.map other [H];
        "Goldstone Bosons", List.map other [Phip; Phim; Phi0] ]

    let flavors () = ThoList.flatmap snd (external_flavors ())

    let spinor n m =
      if n >= 0 && m >= 0 then
        Spinor
      else if
        n <= 0 && m <=0 then
        ConjSpinor
      else
        invalid_arg "Models.SM_Col.spinor: internal error"

    let lorentz = function
      | M f ->
          begin match f with
          | L n -> spinor n 0 | N n -> spinor n 0 
          | U (n,Q m) -> spinor n m | D (n,Q m) -> spinor n m
          end
      | G f ->
          begin match f with
          | Ga | Gl0 -> Vector
          | Gl (Q n, Q m) when n > 0 && m < 0 -> Vector
          | Wp | Wm | Z -> Massive_Vector
          | _ -> invalid_arg "Models.SM_Col.lorentz: internal error"
          end
      | O f -> Scalar


    let color = function 
      | M (U (n,Q m)) -> Color.SUN 
            (if n > 0 && m > 0 then 3 else -3)
      | M (D (n,Q m)) -> Color.SUN  
            (if n > 0 && m > 0 then 3 else -3)
      | G (Gl _) -> Color.AdjSUN 3
      | _ -> Color.Singlet

    let prop_spinor n m =
      if n >= 0 && m >=0 then
        Prop_Spinor
      else if 
        n <=0 && m <=0 then
        Prop_ConjSpinor
      else 
        invalid_arg "Models.SM_Col.prop_spinor: internal error"

    let propagator = function
      | M f ->
          begin match f with
          | L n -> prop_spinor n 0 | N n -> prop_spinor n 0
          | U (n, Q m) -> prop_spinor n m | D (n,Q m) -> prop_spinor n m
          end
      | G f ->
          begin match f with
          | Ga | Gl _ -> Prop_Feynman
          | Gl0 -> Prop_Col_Feynman
          | Wp | Wm | Z -> Prop_Unitarity
          end
      | O f ->
          begin match f with
          | Phip | Phim | Phi0 -> Only_Insertion
          | H -> Prop_Scalar
          end

(* Optionally, ask for the fudge factor treatment for the widths of
   charged particles.  Currently, this only applies to $W^\pm$ and top. *)

    let width f =
      if !use_fudged_width then
        match f with
        | G Wp | G Wm | M (U (3,_)) | M (U (-3,_)) -> Fudged
        | _ -> !default_width
      else
        !default_width

    let goldstone = function
      | G f ->
          begin match f with
          | Wp -> Some (O Phip, Coupling.Const 1)
          | Wm -> Some (O Phim, Coupling.Const 1)
          | Z -> Some (O Phi0, Coupling.Const 1)
          | _ -> None
          end
      | _ -> None

    let conjugate = function
      | M f ->
          M (begin match f with
          | L n -> L (-n) | N n -> N (-n)
          | U (n,Q m) -> U (-n,Q (-m)) 
          | D (n,Q m) -> D (-n,Q (-m))
          end)
      | G f ->
          G (begin match f with
          | Gl (Q n, Q m) when n > 0 && m < 0 -> Gl (Q (-m), Q (-n)) 
          | Ga -> Ga | Z -> Z | Gl0 -> Gl0 
          | Wp -> Wm | Wm -> Wp
          | _ -> invalid_arg "Models.SM_Col.conjugate: internal error"
          end)
      | O f ->
          O (begin match f with
          | Phip -> Phim | Phim -> Phip | Phi0 -> Phi0
          | H -> H
          end)

    let fermion = function
      | M f ->
          begin match f with
          | L n -> if n > 0 then 1 else -1
          | N n -> if n > 0 then 1 else -1
          | U (n,_) -> if n > 0 then 1 else -1
          | D (n,_) -> if n > 0 then 1 else -1
          end
      | G f ->
          begin match f with
          | Gl _ | Ga | Z | Wp | Wm | Gl0 -> 0
          end
      | O _ -> 0

    let colsymm = function
      | M f ->
          begin match f with
          | D (n,_) -> (n,true),(0,false)
          | U (n,_) -> if n > 0 then (n+3,true),(0,false) 
                              else (n-3,true),(0,false)
          | _ -> (0, false), (0, false)
          end
      | G f ->
          begin match f with
          | Gl _ -> (7,true),(0,false)
          | Gl0 -> (7,true),(7,true)
          | _ -> (0, false), (0, false)
          end
      | O _ -> (0, false), (0, false)

    type constant =
      | Unit | Pi | Alpha_QED | Sin2thw
      | Sinthw | Costhw | E | G_weak | Vev
      | Q_lepton | Q_up | Q_down | G_CC | G_CCQ of int*int
      | G_NC_neutrino | G_NC_lepton | G_NC_up | G_NC_down
      | I_Q_W | I_G_ZWW 
      | G_WWWW | G_ZZWW | G_AZWW | G_AAWW
      | I_G1_AWW | I_G1_ZWW
      | I_G1_plus_kappa_plus_G4_AWW
      | I_G1_plus_kappa_plus_G4_ZWW
      | I_G1_plus_kappa_minus_G4_AWW
      | I_G1_plus_kappa_minus_G4_ZWW
      | I_G1_minus_kappa_plus_G4_AWW
      | I_G1_minus_kappa_plus_G4_ZWW
      | I_G1_minus_kappa_minus_G4_AWW
      | I_G1_minus_kappa_minus_G4_ZWW
      | I_lambda_AWW | I_lambda_ZWW
      | G5_AWW | G5_ZWW
      | I_kappa5_AWW | I_kappa5_ZWW 
      | I_lambda5_AWW | I_lambda5_ZWW
      | Alpha_WWWW0 | Alpha_ZZWW1 | Alpha_WWWW2
      | Alpha_ZZWW0 | Alpha_ZZZZ
      | D_Alpha_ZZWW0_S | D_Alpha_ZZWW0_T | D_Alpha_ZZWW1_S
      | D_Alpha_ZZWW1_T | D_Alpha_ZZWW1_U | D_Alpha_WWWW0_S
      | D_Alpha_WWWW0_T | D_Alpha_WWWW0_U | D_Alpha_WWWW2_S
      | D_Alpha_WWWW2_T | D_Alpha_ZZZZ_S | D_Alpha_ZZZZ_T
      | G_HWW | G_HHWW | G_HZZ | G_HHZZ
      | G_Htt | G_Hbb | G_Hcc | G_Htautau | G_Hmm | G_H3 | G_H4
      | G_HGaZ | G_HGaGa | G_Hgg
      | Gs | I_Gs | G2 
      | Mass of flavor | Width of flavor
      | K_Matrix_Coeff of int | K_Matrix_Pole of int

(* \begin{dubious}
     The current abstract syntax for parameter dependencies is admittedly
     tedious. Later, there will be a parser for a convenient concrete syntax
     as a part of a concrete syntax for models.  But as these examples show,
     it should include simple functions.
   \end{dubious} *)

(* \begin{subequations}
     \begin{align}
        \alpha_{\text{QED}} &= \frac{1}{137.0359895} \\
             \sin^2\theta_w &= 0.23124
     \end{align}
   \end{subequations} *)
    let input_parameters =
      [ Alpha_QED, 1. /. 137.0359895;
        Sin2thw, 0.23124;
        Mass (G Z), 91.187;
        Mass (M (N 1)), 0.0; Mass (M (L 1)), 0.51099907e-3;
        Mass (M (N 2)), 0.0; Mass (M (L 2)), 0.105658389;
        Mass (M (N 3)), 0.0; Mass (M (L 3)), 1.77705;
        Mass (M (U (1,Q 1))), 5.0e-3; Mass (M (D (1,Q 1))), 3.0e-3;
        Mass (M (U (2,Q 1))), 1.2; Mass (M (D (2,Q 1))), 0.1;
        Mass (M (U (3,Q 1))), 178.0; Mass (M (D (3,Q 1))), 4.2 ]

(* \begin{subequations}
     \begin{align}
                        e &= \sqrt{4\pi\alpha} \\
             \sin\theta_w &= \sqrt{\sin^2\theta_w} \\
             \cos\theta_w &= \sqrt{1-\sin^2\theta_w} \\
                        g &= \frac{e}{\sin\theta_w} \\
                      m_W &= \cos\theta_w m_Z \\
                        v &= \frac{2m_W}{g} \\
                  g_{CC}   =
       -\frac{g}{2\sqrt2} &= -\frac{e}{2\sqrt2\sin\theta_w} \\
       Q_{\text{lepton}}   =
      -q_{\text{lepton}}e &= e \\
           Q_{\text{up}}   =
          -q_{\text{up}}e &= -\frac{2}{3}e \\
         Q_{\text{down}}   =
        -q_{\text{down}}e &= \frac{1}{3}e \\
        \ii q_We           =
        \ii g_{\gamma WW} &= \ii e \\
              \ii g_{ZWW} &= \ii g \cos\theta_w \\
              \ii g_{WWW} &= \ii g
     \end{align}
   \end{subequations} *)

(* \begin{dubious}
   \ldots{} to be continued \ldots{}
   The quartic couplings can't be correct, because the dimensions are wrong!
   \begin{subequations}
     \begin{align}
                  g_{HWW} &= g m_W \\
                 g_{HHWW} &= \frac{g}{2}m_W \\
                  g_{HZZ} &= \frac{g}{\cos\theta_w}m_Z \\
                 g_{HHZZ} &= \frac{g}{2\cos\theta_w}m_Z \\
                  g_{Htt} &= \lambda_t \\
                  g_{Hbb} &= \lambda_b=\frac{m_b}{m_t}\lambda_t \\
                  g_{H^3} &= ? \\
                  g_{H^4} &= ?
     \end{align}
   \end{subequations}
   \end{dubious} *)

    let derived_parameters =
      [ Real E, Sqrt (Prod [Const 4; Atom Pi; Atom Alpha_QED]);
        Real Sinthw, Sqrt (Atom Sin2thw);
        Real Costhw, Sqrt (Diff (Const 1, Atom Sin2thw));
        Real G_weak, Quot (Atom E, Atom Sinthw);
        Real (Mass (G Wp)), Prod [Atom Costhw; Atom (Mass (G Z))];
        Real Vev, Quot (Prod [Const 2; Atom (Mass (G Wp))], Atom G_weak);
        Real Q_lepton, Atom E;
        Real Q_up, Prod [Quot (Const (-2), Const 3); Atom E];
        Real Q_down, Prod [Quot (Const 1, Const 3); Atom E];
        Real G_CC, Neg (Quot (Atom G_weak, Prod [Const 2; Sqrt (Const 2)]));
        Complex I_Q_W, Prod [I; Atom E];
        Complex I_G_ZWW, Prod [I; Atom G_weak; Atom Costhw]]
             
(* \begin{equation}
      - \frac{g}{2\cos\theta_w}
   \end{equation} *)
    let g_over_2_costh =
      Quot (Neg (Atom G_weak), Prod [Const 2; Atom Costhw])

(* \begin{subequations}
     \begin{align}
           - \frac{g}{2\cos\theta_w} g_V
        &= - \frac{g}{2\cos\theta_w} (T_3 - 2 q \sin^2\theta_w) \\
           - \frac{g}{2\cos\theta_w} g_A
        &= - \frac{g}{2\cos\theta_w} T_3
     \end{align}
   \end{subequations} *)
    let nc_coupling c t3 q =
      (Real_Array c,
       [Prod [g_over_2_costh; Diff (t3, Prod [Const 2; q; Atom Sin2thw])];
        Prod [g_over_2_costh; t3]])

    let half = Quot (Const 1, Const 2)

    let derived_parameter_arrays =
      [ nc_coupling G_NC_neutrino half (Const 0);
        nc_coupling G_NC_lepton (Neg half) (Const (-1));
        nc_coupling G_NC_up half (Quot (Const 2, Const 3));
        nc_coupling G_NC_down (Neg half) (Quot (Const (-1), Const 3)) ]

    let parameters () =
      { input = input_parameters;
        derived = derived_parameters;
        derived_arrays = derived_parameter_arrays }

    module F = Fusions (struct
      type f = flavor
      type c = constant
      let compare = compare
      let conjugate = conjugate
    end)

(* \begin{equation}
     \mathcal{L}_{\textrm{EM}} =
        - e \sum_i q_i \bar\psi_i\fmslash{A}\psi_i
   \end{equation} *)

    let mgm ((m1, g, m2), fbf, c) = ((M m1, G g, M m2), fbf, c)

    let electromagnetic_currents' n c = 
      List.map mgm
     [((U (-n, Q (-c)), Ga, U (n, Q c)), FBF (1, Psibar, V, Psi), Q_up);
      ((D (-n, Q (-c)), Ga, D (n, Q c)), FBF (1, Psibar, V, Psi), Q_down) ]
    let electromagnetic_currents n =
      List.map mgm
        [ ((L (-n), Ga, L n), FBF (1, Psibar, V, Psi), Q_lepton)] @
      ThoList.flatmap (electromagnetic_currents' n) nc_list

    let col_nonsinglet_currents' n (c1,c2) =
      List.map mgm
      [ ((D (-n, Q (-c2)), Gl (Q c2,Q (-c1)), D (n, Q c1)), 
                     FBF ((-1), Psibar, V, Psi), Gs);
        ((U (-n, Q (-c2)), Gl (Q c2,Q (-c1)), U (n, Q c1)), 
                     FBF ((-1), Psibar, V, Psi), Gs)]
    let col_nonsinglet_currents n = 
      ThoList.flatmap (col_nonsinglet_currents' n) inequ_pairs 

    let col_singlet_currents' n c1 =
      List.map mgm 
      [ ((D (-n, Q (-c1)), Gl0, D (n, Q c1)), FBF ((-1), Psibar, V, Psi), Gs);
        ((U (-n, Q (-c1)), Gl0, U (n, Q c1)), FBF ((-1), Psibar, V, Psi), Gs)]
    let col_singlet_currents n = 
      ThoList.flatmap (col_singlet_currents' n) nc_list 
        
    let neutral_currents' n c = 
      List.map mgm 
        [ ((U (-n,Q (-c)), Z, U (n,Q c)), FBF (1, Psibar, VA, Psi), G_NC_up);
          ((D (-n,Q (-c)), Z, D (n,Q c)), FBF (1, Psibar, VA, Psi), G_NC_down)]
    let neutral_currents n =
      List.map mgm
        [ ((L (-n), Z, L n), FBF (1, Psibar, VA, Psi), G_NC_lepton);
          ((N (-n), Z, N n), FBF (1, Psibar, VA, Psi), G_NC_neutrino)] @
      ThoList.flatmap (neutral_currents' n) nc_list 
             
    let charged_currents' n c =
      List.map mgm 
      [ ((D (-n,Q (-c)), Wm, U (n, Q c)), FBF (1, Psibar, VL, Psi), G_CC);
        ((U (-n,Q (-c)), Wp, D (n, Q c)), FBF (1, Psibar, VL, Psi), G_CC) ] 
    let charged_currents_lep n =
      List.map mgm
        [ ((L (-n), Wm, N n), FBF (1, Psibar, VL, Psi), G_CC);
          ((N (-n), Wp, L n), FBF (1, Psibar, VL, Psi), G_CC)]      
    let charged_currents_triv =
      ThoList.flatmap charged_currents_lep [1;2;3] @
      List.flatten (Product.list2 charged_currents' [1;2;3] nc_list)
    let charged_currents'' c n1 n2 = 
      List.map mgm 
        [ ((D (-n1,Q (-c)), Wm, U (n2, Q c)), FBF (1, Psibar, VL, Psi), 
           G_CCQ (n2,n1));
          ((U (-n1,Q (-c)), Wp, D (n2, Q c)), FBF (1, Psibar, VL, Psi), 
           G_CCQ (n1,n2)) ] 
    let charged_currents''' c = List.flatten (Product.list2 
        (charged_currents'' c) [1;2;3] [1;2;3])
    let charged_currents_ckm =
      ThoList.flatmap charged_currents_lep [1;2;3] @
      ThoList.flatmap charged_currents''' nc_list

    let yukawa' c = 
      [ ((M (U (-3, Q (-c))), O H, M (U (3,Q c))), 
               FBF (1, Psibar, S, Psi), G_Htt);
        ((M (U (-2, Q (-c))), O H, M (U (2,Q c))), 
               FBF (1, Psibar, S, Psi), G_Hcc);
        ((M (D (-3, Q (-c))), O H, M (D (3,Q c))), 
               FBF (1, Psibar, S, Psi), G_Hbb)]        
    let yukawa =
      [ ((M (L (-2)), O H, M (L 2)), FBF (1, Psibar, S, Psi), G_Hmm);      
        ((M (L (-3)), O H, M (L 3)), FBF (1, Psibar, S, Psi), G_Htautau) ] @
      ThoList.flatmap yukawa' nc_list

    let tgc ((g1, g2, g3), t, c) = ((G g1, G g2, G g3), t, c)

    let standard_triple_gauge =
      List.map tgc
        [ ((Ga, Wm, Wp), Gauge_Gauge_Gauge 1, I_Q_W);
          ((Z, Wm, Wp), Gauge_Gauge_Gauge 1, I_G_ZWW) ]

    let triple_gluon (c1,c2,c3) =
      List.map tgc
        [ ((Gl (Q c1,Q (-c3)), Gl (Q c2,Q (-c1)), Gl (Q c3,Q (-c2))), 
           Gauge_Gauge_Gauge 1, I_Gs);
          ((Gl (Q c1,Q (-c2)), Gl (Q c3,Q (-c1)), Gl (Q c2,Q (-c3))), 
           Gauge_Gauge_Gauge 1, I_Gs)]   

    let anomalous_triple_gauge =
      List.map tgc
        [ ((Ga, Wm, Wp), Dim4_Vector_Vector_Vector_T (-1),
           I_G1_AWW);
          ((Z, Wm, Wp), Dim4_Vector_Vector_Vector_T (-1),
           I_G1_ZWW);
          ((Wm, Ga, Wp), Dim4_Vector_Vector_Vector_T 1,
           I_G1_plus_kappa_minus_G4_AWW);
          ((Wm, Z, Wp), Dim4_Vector_Vector_Vector_T 1,
           I_G1_plus_kappa_minus_G4_ZWW);
          ((Wp, Ga, Wm), Dim4_Vector_Vector_Vector_T (-1),
           I_G1_plus_kappa_plus_G4_AWW);
          ((Wp, Z, Wm), Dim4_Vector_Vector_Vector_T (-1),
           I_G1_plus_kappa_plus_G4_ZWW);
          ((Wm, Ga, Wp), Dim4_Vector_Vector_Vector_L (-1),
           I_G1_minus_kappa_plus_G4_AWW);
          ((Wm, Z, Wp), Dim4_Vector_Vector_Vector_L (-1),
           I_G1_minus_kappa_plus_G4_ZWW);
          ((Wp, Ga, Wm), Dim4_Vector_Vector_Vector_L 1,
           I_G1_minus_kappa_minus_G4_AWW);
          ((Wp, Z, Wm), Dim4_Vector_Vector_Vector_L 1,
           I_G1_minus_kappa_minus_G4_ZWW);
          ((Ga, Wm, Wp), Dim4_Vector_Vector_Vector_T5 (-1),
           I_kappa5_AWW);
          ((Z, Wm, Wp), Dim4_Vector_Vector_Vector_T5 (-1),
           I_kappa5_ZWW);
          ((Ga, Wm, Wp), Dim4_Vector_Vector_Vector_L5 (-1),
           G5_AWW);
          ((Z, Wm, Wp), Dim4_Vector_Vector_Vector_L5 (-1),
           G5_ZWW);
          ((Ga, Wp, Wm), Dim6_Gauge_Gauge_Gauge (-1),
           I_lambda_AWW);
          ((Z, Wp, Wm), Dim6_Gauge_Gauge_Gauge (-1),
           I_lambda_ZWW);
          ((Ga, Wp, Wm), Dim6_Gauge_Gauge_Gauge_5 (-1),
           I_lambda5_AWW);
          ((Z, Wp, Wm), Dim6_Gauge_Gauge_Gauge_5 (-1),
           I_lambda5_ZWW) ]

    let triple_gauge =
      if Flags.triple_anom then
        anomalous_triple_gauge
      else
        standard_triple_gauge

    let qgc ((g1, g2, g3, g4), t, c) = ((G g1, G g2, G g3, G g4), t, c)

    let gauge4 = Vector4 [(2, C_13_42); (-1, C_12_34); (-1, C_14_23)]
    let minus_gauge4 = Vector4 [(-2, C_13_42); (1, C_12_34); (1, C_14_23)]
    let standard_quartic_gauge =
      List.map qgc
        [ (Wm, Wp, Wm, Wp), gauge4, G_WWWW;
          (Wm, Z, Wp, Z), minus_gauge4, G_ZZWW;
          (Wm, Z, Wp, Ga), minus_gauge4, G_AZWW;
          (Wm, Ga, Wp, Ga), minus_gauge4, G_AAWW ]

    let anomalous_quartic_gauge =
      if Flags.quartic_anom then
        List.map qgc
          [ ((Wm, Wm, Wp, Wp),
             Vector4 [(1, C_13_42); (1, C_14_23)], Alpha_WWWW0);
            ((Wm, Wm, Wp, Wp),
             Vector4 [1, C_12_34], Alpha_WWWW2);
            ((Wm, Wp, Z, Z),
             Vector4 [1, C_12_34], Alpha_ZZWW0);
            ((Wm, Wp, Z, Z),
             Vector4 [(1, C_13_42); (1, C_14_23)], Alpha_ZZWW1);
            ((Z, Z, Z, Z),
             Vector4 [(1, C_12_34); (1, C_13_42); (1, C_14_23)], Alpha_ZZZZ) ]
      else
        []

    let k_matrix_quartic_gauge =
      if Flags.k_matrix then
        List.map qgc
          [ ((Wm, Wp, Wm, Wp), Vector4_K_Matrix_jr (0,
                   [(1, C_12_34)]), D_Alpha_WWWW0_S);
            ((Wm, Wp, Wm, Wp), Vector4_K_Matrix_jr (0,
                   [(1, C_14_23)]), D_Alpha_WWWW0_T);
            ((Wm, Wp, Wm, Wp), Vector4_K_Matrix_jr (0,
                   [(1, C_13_42)]), D_Alpha_WWWW0_U);
            ((Wp, Wm, Wp, Wm), Vector4_K_Matrix_jr (0,
                   [(1, C_12_34)]), D_Alpha_WWWW0_S);
            ((Wp, Wm, Wp, Wm), Vector4_K_Matrix_jr (0,
                   [(1, C_14_23)]), D_Alpha_WWWW0_T);
            ((Wp, Wm, Wp, Wm), Vector4_K_Matrix_jr (0,
                   [(1, C_13_42)]), D_Alpha_WWWW0_U);
            ((Wm, Wm, Wp, Wp), Vector4_K_Matrix_jr (0,
                   [(1, C_12_34)]), D_Alpha_WWWW2_S);
            ((Wm, Wm, Wp, Wp), Vector4_K_Matrix_jr (0,
                   [(1, C_13_42); (1, C_14_23)]), D_Alpha_WWWW2_T);
            ((Wm, Wp, Z, Z), Vector4_K_Matrix_jr (0,
                   [(1, C_12_34)]), D_Alpha_ZZWW0_S);
            ((Wm, Wp, Z, Z), Vector4_K_Matrix_jr (0,
                   [(1, C_13_42); (1, C_14_23)]), D_Alpha_ZZWW0_T);
            ((Wm, Z, Wp, Z), Vector4_K_Matrix_jr (0,
                   [(1, C_12_34)]), D_Alpha_ZZWW1_S);
            ((Wm, Z, Wp, Z), Vector4_K_Matrix_jr (0,
                   [(1, C_13_42)]), D_Alpha_ZZWW1_T);
            ((Wm, Z, Wp, Z), Vector4_K_Matrix_jr (0,
                   [(1, C_14_23)]), D_Alpha_ZZWW1_U);
            ((Wp, Z, Z, Wm), Vector4_K_Matrix_jr (1,
                   [(1, C_12_34)]), D_Alpha_ZZWW1_S);
            ((Wp, Z, Z, Wm), Vector4_K_Matrix_jr (1,
                   [(1, C_13_42)]), D_Alpha_ZZWW1_U);
            ((Wp, Z, Z, Wm), Vector4_K_Matrix_jr (1,
                   [(1, C_14_23)]), D_Alpha_ZZWW1_T); 
            ((Z, Wp, Wm, Z), Vector4_K_Matrix_jr (2,
                   [(1, C_12_34)]), D_Alpha_ZZWW1_S); 
            ((Z, Wp, Wm, Z), Vector4_K_Matrix_jr (2,
                   [(1, C_13_42)]), D_Alpha_ZZWW1_U);
            ((Z, Wp, Wm, Z), Vector4_K_Matrix_jr (2,
                   [(1, C_14_23)]), D_Alpha_ZZWW1_T); 
            ((Z, Z, Z, Z), Vector4_K_Matrix_jr (0,
                   [(1, C_12_34)]), D_Alpha_ZZZZ_S);
            ((Z, Z, Z, Z), Vector4_K_Matrix_jr (0,
                   [(1, C_13_42); (1, C_14_23)]), D_Alpha_ZZZZ_T); 
            ((Z, Z, Z, Z), Vector4_K_Matrix_jr (3,
                   [(1, C_14_23)]), D_Alpha_ZZZZ_S);
            ((Z, Z, Z, Z), Vector4_K_Matrix_jr (3,
                   [(1, C_13_42); (1, C_12_34)]), D_Alpha_ZZZZ_T) ]
      else
        []

(*i Thorsten's original implementation of the K matrix, which we keep since
   it still might be usefull for the future. 

    let k_matrix_quartic_gauge =
      if Flags.k_matrix then
        List.map qgc
          [ ((Wm, Wp, Wm, Wp), Vector4_K_Matrix_tho [K_Matrix_Coeff 0, 
                         K_Matrix_Pole 0], Alpha_WWWW0);
            ((Wm, Wm, Wp, Wp), Vector4_K_Matrix_tho [K_Matrix_Coeff 2, 
                         K_Matrix_Pole 2], Alpha_WWWW0);
            ((Wm, Wp, Z, Z), Vector4_K_Matrix_tho [(K_Matrix_Coeff 0, 
                         K_Matrix_Pole 0); (K_Matrix_Coeff 2, 
                         K_Matrix_Pole 2)], Alpha_WWWW0);
            ((Wm, Z, Wp, Z), Vector4_K_Matrix_tho [K_Matrix_Coeff 1, 
                         K_Matrix_Pole 1], Alpha_WWWW0);
            ((Z, Z, Z, Z), Vector4_K_Matrix_tho [K_Matrix_Coeff 0, 
                         K_Matrix_Pole 0], Alpha_WWWW0) ]
      else
        []
i*)


(* These are the six different color flows for the 4-gluon vertex. *)
        
    let quartic_gluon (c1,c2,c3,c4) = 
        List.map qgc 
      [ ((Gl (Q c1,Q (-c4)), Gl (Q c2,Q (-c1)), 
          Gl (Q c3,Q (-c2)), Gl (Q c4,Q (-c3))), gauge4, G2);
        ((Gl (Q c1,Q (-c4)), Gl (Q c3,Q (-c1)), 
          Gl (Q c2,Q (-c3)), Gl (Q c4,Q (-c2))), gauge4, G2);
        ((Gl (Q c1,Q (-c3)), Gl (Q c4,Q (-c1)), 
          Gl (Q c2,Q (-c4)), Gl (Q c3,Q (-c2))), gauge4, G2);
        ((Gl (Q c1,Q (-c2)), Gl (Q c4,Q (-c1)), 
          Gl (Q c3,Q (-c4)), Gl (Q c2,Q (-c3))), gauge4, G2);
        ((Gl (Q c1,Q (-c3)), Gl (Q c2,Q (-c1)), 
          Gl (Q c4,Q (-c2)), Gl (Q c3,Q (-c4))), gauge4, G2);
        ((Gl (Q c1,Q (-c2)), Gl (Q c3,Q (-c1)), 
          Gl (Q c4,Q (-c3)), Gl (Q c2,Q (-c4))), gauge4, G2)]

    let quartic_gauge =
      standard_quartic_gauge @ anomalous_quartic_gauge @ k_matrix_quartic_gauge

    let standard_gauge_higgs =
      [ ((O H, G Wp, G Wm), Scalar_Vector_Vector 1, G_HWW);
        ((O H, G Z, G Z), Scalar_Vector_Vector 1, G_HZZ) ]

    let standard_gauge_higgs4 =
      [ (O H, O H, G Wp, G Wm), Scalar2_Vector2 1, G_HHWW;
        (O H, O H, G Z, G Z), Scalar2_Vector2 1, G_HHZZ ]
       
    let standard_higgs =
      [ (O H, O H, O H), Scalar_Scalar_Scalar 1, G_H3 ]

    let standard_higgs4 =
      [ (O H, O H, O H, O H), Scalar4 1, G_H4 ]

(* The factor (-3) accounts for the correct color factors for the 
    anomalous vertex singlet-octet-octet. *)

    let anomaly_higgs = 
      []
(*i      [ (O H, G Ga, G Ga), Dim5_Scalar_Gauge2 1, G_HGaGa;
        (O H, G Ga, G Z), Dim5_Scalar_Gauge2 1, G_HGaZ;
        (O H, G Gl0, G Gl0), Dim5_Scalar_Gauge2 (-3), G_Hgg] i*)

    let gluon_higgs (c1,c2) = 
      [ (O H, G (Gl (Q c1,Q (-c2))), G (Gl (Q c2,Q (-c1)))), 
                Dim5_Scalar_Gauge2 1, G_Hgg]

    let anomalous_gauge_higgs =
      []

    let anomalous_gauge_higgs4 =
      []

    let anomalous_higgs =
      []

    let anomalous_higgs4 =
      []

    let gauge_higgs =
      if Flags.higgs_anom then
        standard_gauge_higgs @ anomalous_gauge_higgs
      else
        standard_gauge_higgs

    let gauge_higgs4 =
      if Flags.higgs_anom then
        standard_gauge_higgs4 @ anomalous_gauge_higgs4
      else
        standard_gauge_higgs4

    let higgs =
      if Flags.higgs_anom then
          standard_higgs @ anomalous_higgs 
      else
          standard_higgs

    let higgs4 =
      if Flags.higgs_anom then
        standard_higgs4 @ anomalous_higgs4
      else
        standard_higgs4

    let goldstone_vertices =
      [ ((O Phi0, G Wm, G Wp), Scalar_Vector_Vector 1, I_G_ZWW);
        ((O Phip, G Ga, G Wm), Scalar_Vector_Vector 1, I_Q_W);
        ((O Phip, G Z, G Wm), Scalar_Vector_Vector 1, I_G_ZWW);
        ((O Phim, G Wp, G Ga), Scalar_Vector_Vector 1, I_Q_W);
        ((O Phim, G Wp, G Z), Scalar_Vector_Vector 1, I_G_ZWW) ]

    let vertices3 =
      (ThoList.flatmap electromagnetic_currents [1;2;3] @
       ThoList.flatmap col_nonsinglet_currents [1;2;3] @
       ThoList.flatmap col_singlet_currents [1;2;3] @ 
       ThoList.flatmap neutral_currents [1;2;3] @
       (if Flags.ckm_present then
         charged_currents_ckm
       else
         charged_currents_triv) @
       yukawa @ triple_gauge @ 
       ThoList.flatmap triple_gluon triple_col @
       gauge_higgs @ higgs @
       anomaly_higgs @
       ThoList.flatmap gluon_higgs (choose2 nc_list) @ 
       goldstone_vertices)

    let vertices4 =
      quartic_gauge @ gauge_higgs4 @ higgs4 @
      ThoList.flatmap quartic_gluon quartic_col

    let vertices () = (vertices3, vertices4, [])

(* For efficiency, make sure that [F.of_vertices vertices] is
   evaluated only once. *)

    let table = F.of_vertices (vertices ())
    let fuse2 = F.fuse2 table
    let fuse3 = F.fuse3 table
    let fuse = F.fuse table
    let max_degree () = 4

   let read_color s =
      try
        let offset = (String.index s '/') in 
        let colnum = 
           (String.sub s (succ offset) (String.length s - offset - 1)) in
	if int_of_string colnum > 10 && int_of_string colnum > 0 then
          let s1 = int_of_string (Char.escaped (String.get colnum 0)) and
              s2 = int_of_string (Char.escaped (String.get colnum 1)) in
          if s1 > nc or s2 > nc then	
		invalid_arg "Number of explicit color flows in O'Mega exceeded"	
		else	
          (s1,s2,String.sub s 0 offset)
        else
	  if int_of_string colnum > nc then
          invalid_arg "Number of explicit color flows in O'Mega exceeded"
	else	
          (int_of_string
             (String.sub s (succ offset) (String.length s - offset - 1)),
           0,
           String.sub s 0 offset)
      with
      | Not_found -> (0, 0, s)

    let flavor_of_string' = function
      | n, 0, "u"  -> M (U (1, Q n)) | n, 0, "ubar" -> M (U (-1, Q (-n)))
      | n, 0, "c"  -> M (U (2, Q n)) | n, 0, "cbar" -> M (U (-2, Q (-n)))
      | n, 0, "t"  -> M (U (3, Q n)) | n, 0, "tbar" -> M (U (-3, Q (-n)))
      | n, 0, "d" -> M (D (1, Q n)) | n, 0, "dbar" -> M (D (-1, Q (-n)))
      | n, 0, "s" -> M (D (2, Q n)) | n, 0, "sbar" -> M (D (-2, Q (-n)))
      | n, 0, "b" -> M (D (3, Q n)) | n, 0, "bbar" -> M (D (-3, Q (-n)))
      | n, m, "gl" when n = m -> invalid_arg "Models.SM_Col.flavor_of_string"
      | n, m, "gl" -> G (Gl (Q n, Q (- m))) | 0, 0, "gl0" -> G Gl0
      | 0, 0, "e-" -> M (L 1) | 0, 0, "e+" -> M (L (-1))
      | 0, 0, "mu-" -> M (L 2) | 0, 0, "mu+" -> M (L (-2))
      | 0, 0, "tau-" -> M (L 3) | 0, 0, "tau+" -> M (L (-3))
      | 0, 0, "nue" -> M (N 1) | 0, 0, "nuebar" -> M (N (-1))
      | 0, 0, "numu" -> M (N 2) | 0, 0, "numubar" -> M (N (-2))
      | 0, 0, "nutau" -> M (N 3) | 0, 0, "nutaubar" -> M (N (-3))
      | 0, 0, "A" -> G Ga | 0, 0, "Z" | 0, 0, "Z0" -> G Z
      | 0, 0, "W+" -> G Wp | 0, 0, "W-" -> G Wm
      | 0, 0, "H" -> O H
      | _ -> invalid_arg "Models.SM_Col.flavor_of_string"

    let flavor_of_string s = 
      flavor_of_string' (read_color s)

    let flavor_to_string = function
      | M f ->
          begin match f with
          | L 1 -> "e-" | L (-1) -> "e+"
          | L 2 -> "mu-" | L (-2) -> "mu+"
          | L 3 -> "tau-" | L (-3) -> "tau+"
          | L _ -> invalid_arg
                "Models.SM_Col.flavor_to_string: invalid lepton"
          | N 1 -> "nue" | N (-1) -> "nuebar"
          | N 2 -> "numu" | N (-2) -> "numubar"
          | N 3 -> "nutau" | N (-3) -> "nutaubar"
          | N _ -> invalid_arg
                "Models.SM_Col.flavor_to_string: invalid neutrino"
          | U (1, Q n) when n > 0 -> "u/" ^ string_of_int n 
          | U (2, Q n) when n > 0 -> "c/" ^ string_of_int n 
          | U (3, Q n) when n > 0 -> "t/" ^ string_of_int n 
          | U (-1, Q n) when n < 0 -> "ubar/" ^ string_of_int (-n)
          | U (-2, Q n) when n < 0 -> "cbar/" ^ string_of_int (-n)
          | U (-3, Q n) when n < 0 -> "tbar/" ^ string_of_int (-n)
          | U _ -> invalid_arg
                "Models.SM_Col.flavor_to_string: invalid up type quark"
          | D (1, Q n) when n > 0 -> "d/" ^ string_of_int n
          | D (2, Q n) when n > 0 -> "s/" ^ string_of_int n
          | D (3, Q n) when n > 0 -> "b/" ^ string_of_int n
          | D (-1, Q n) when n < 0 -> "dbar/" ^ string_of_int (-n)
          | D (-2, Q n) when n < 0 -> "sbar/" ^ string_of_int (-n)
          | D (-3, Q n) when n < 0 -> "bbar/" ^ string_of_int (-n)
          | D _ -> invalid_arg
                "Models.SM_Col.flavor_to_string: invalid down type quark"
          end
      | G f ->
          begin match f with
          | Gl (Q n, Q m) when n > 0 && m < 0 -> 
              "gl/" ^ string_of_int n ^ string_of_int (-m) 
          | Ga -> "A" | Z -> "Z" | Gl0 -> "gl0"
          | Wp -> "W+" | Wm -> "W-" 
          | _ -> invalid_arg 
                "Models.SM_Col.flavor_to_string: invalid gauge boson"
          end
      | O f ->
          begin match f with
          | Phip -> "phi+" | Phim -> "phi-" | Phi0 -> "phi0" 
          | H -> "H"
          end

    let flavor_to_TeX = function
      | M f ->
          begin match f with
          | L 1 -> "e^-" | L (-1) -> "e^+"
          | L 2 -> "\\mu^-" | L (-2) -> "\\mu^+"
          | L 3 -> "\\tau^-" | L (-3) -> "\\tau^+"
          | L _ -> invalid_arg
                "Models.SM_Col.flavor_to_TeX: invalid lepton"
          | N 1 -> "\\nu_e" | N (-1) -> "\\bar{\\nu}_e"
          | N 2 -> "\\nu_\\mu" | N (-2) -> "\\bar{\\nu}_\\mu"
          | N 3 -> "\\nu_\\tau" | N (-3) -> "\\bar{\\nu}_\\tau"
          | N _ -> invalid_arg
                "Models.SM_Col.flavor_to_TeX: invalid neutrino"
          | U (1, Q n) when n > 0 -> "u_{" ^ string_of_int n ^ "}" 
          | U (2, Q n) when n > 0 -> "c_{" ^ string_of_int n ^ "}" 
          | U (3, Q n) when n > 0 -> "t_{" ^ string_of_int n ^ "}" 
          | U (-1, Q n) when n < 0 -> "\\bar{u}_{" ^ string_of_int (-n) ^ "}"
          | U (-2, Q n) when n < 0 -> "\\bar{c}_{" ^ string_of_int (-n) ^ "}"
          | U (-3, Q n) when n < 0 -> "\\bar{t}_{" ^ string_of_int (-n) ^ "}"
          | U _ -> invalid_arg
                "Models.SM_Col.flavor_to_TeX: invalid up type quark"
          | D (1, Q n) when n > 0 -> "d_{" ^ string_of_int n ^ "}"
          | D (2, Q n) when n > 0 -> "s_{" ^ string_of_int n ^ "}"
          | D (3, Q n) when n > 0 -> "b_{" ^ string_of_int n ^ "}"
          | D (-1, Q n) when n < 0 -> "\\bar{d}_{" ^ string_of_int (-n) ^ "}"
          | D (-2, Q n) when n < 0 -> "\\bar{s}_{" ^ string_of_int (-n) ^ "}"
          | D (-3, Q n) when n < 0 -> "\\bar{b}_{" ^ string_of_int (-n) ^ "}"
          | D _ -> invalid_arg
                "Models.SM_Col.flavor_to_TeX: invalid down type quark"
          end
      | G f ->
          begin match f with
          | Gl (Q n, Q m) when n > 0 && m < 0 -> 
              "g_{" ^ string_of_int n ^ string_of_int (-m) ^ "}"
          | Ga -> "\\gamma" | Z -> "Z" | Gl0 -> "g_0"
          | Wp -> "W^+" | Wm -> "W^-" 
          | _ -> invalid_arg 
                "Models.SM_Col.flavor_to_TeX: invalid gauge boson"
          end
      | O f ->
          begin match f with
          | Phip -> "\\phi^+" | Phim -> "\\phi^-" | Phi0 -> "\\phi^0" 
          | H -> "H"
          end

    let flavor_symbol = function
      | M f ->
          begin match f with
          | L n when n > 0 -> "l" ^ string_of_int n
          | L n -> "l" ^ string_of_int (abs n) ^ "b"
          | N n when n > 0 -> "n" ^ string_of_int n
          | N n -> "n" ^ string_of_int (abs n) ^ "b"
          | U (n, Q m) when n > 0 -> "u" ^ string_of_int n ^ "_" ^ string_of_int m
          | U (n, Q m)  -> "ub" ^ string_of_int (abs n) ^ "_" 
              ^ string_of_int (abs m)
          | D (n, Q m) when n > 0 ->  "d" ^ string_of_int n ^ "_" ^ string_of_int m
          | D (n, Q m) -> "db" ^ string_of_int (abs n) ^ "_" 
              ^ string_of_int (abs m)
          end
      | G f ->
          begin match f with
          | Gl (Q n, Q m) -> "gl_" ^ string_of_int n ^ string_of_int (abs m)
          | Ga -> "a" | Z -> "z" | Gl0 -> "gl0"
          | Wp -> "wp" | Wm -> "wm"
          end
      | O f ->
          begin match f with
          | Phip -> "pp" | Phim -> "pm" | Phi0 -> "p0" 
          | H -> "h"
          end

    let pdg = function
      | M f ->
          begin match f with
          | L n when n > 0 -> 9 + 2*n
          | L n -> - 9 + 2*n
          | N n when n > 0 -> 10 + 2*n
          | N n -> - 10 + 2*n
          | U (n, _) when n > 0 -> 2*n
          | U (n, _) -> 2*n
          | D (n, _) when n > 0 -> - 1 + 2*n
          | D (n, _) -> 1 + 2*n
          end
      | G f ->
          begin match f with
          | Gl _ | Gl0 -> 21
          | Ga -> 22 | Z -> 23
          | Wp -> 24 | Wm -> (-24)
          end
      | O f ->
          begin match f with
          | Phip | Phim -> 27 | Phi0 -> 26
          | H -> 25
          end

    let mass_symbol f = 
      "mass(" ^ string_of_int (abs (pdg f)) ^ ")"

    let width_symbol f =
      "width(" ^ string_of_int (abs (pdg f)) ^ ")"

    let constant_symbol = function
      | Unit -> "unit" | Pi -> "PI"
      | Alpha_QED -> "alpha" | E -> "e" | G_weak -> "g" | Vev -> "vev"
      | Sin2thw -> "sin2thw" | Sinthw -> "sinthw" | Costhw -> "costhw"
      | Q_lepton -> "qlep" | Q_up -> "qup" | Q_down -> "qdwn"
      | G_NC_lepton -> "gnclep" | G_NC_neutrino -> "gncneu"
      | G_NC_up -> "gncup" | G_NC_down -> "gncdwn"
      | G_CC -> "gcc"
      | G_CCQ (n1,n2) -> "gccq" ^ string_of_int n1 ^ string_of_int n2
      | I_Q_W -> "iqw" | I_G_ZWW -> "igzww" 
      | G_WWWW -> "gw4" | G_ZZWW -> "gzzww"
      | G_AZWW -> "gazww" | G_AAWW -> "gaaww"
      | I_G1_AWW -> "ig1a" | I_G1_ZWW -> "ig1z"
      | I_G1_plus_kappa_plus_G4_AWW -> "ig1pkpg4a"
      | I_G1_plus_kappa_plus_G4_ZWW -> "ig1pkpg4z"
      | I_G1_plus_kappa_minus_G4_AWW -> "ig1pkmg4a"
      | I_G1_plus_kappa_minus_G4_ZWW -> "ig1pkmg4z"
      | I_G1_minus_kappa_plus_G4_AWW -> "ig1mkpg4a"
      | I_G1_minus_kappa_plus_G4_ZWW -> "ig1mkpg4z"
      | I_G1_minus_kappa_minus_G4_AWW -> "ig1mkmg4a"
      | I_G1_minus_kappa_minus_G4_ZWW -> "ig1mkmg4z"
      | I_lambda_AWW -> "ila"
      | I_lambda_ZWW -> "ilz"
      | G5_AWW -> "rg5a"
      | G5_ZWW -> "rg5z"
      | I_kappa5_AWW -> "ik5a"
      | I_kappa5_ZWW -> "ik5z"
      | I_lambda5_AWW -> "il5a" | I_lambda5_ZWW -> "il5z"
      | Alpha_WWWW0 -> "alww0" | Alpha_WWWW2 -> "alww2"
      | Alpha_ZZWW0 -> "alzw0" | Alpha_ZZWW1 -> "alzw1"
      | Alpha_ZZZZ  -> "alzz"
      | D_Alpha_ZZWW0_S -> "dalzz0_s(gkm,mkm,"
      | D_Alpha_ZZWW0_T -> "dalzz0_t(gkm,mkm,"
      | D_Alpha_ZZWW1_S -> "dalzz1_s(gkm,mkm,"
      | D_Alpha_ZZWW1_T -> "dalzz1_t(gkm,mkm,"
      | D_Alpha_ZZWW1_U -> "dalzz1_u(gkm,mkm,"
      | D_Alpha_WWWW0_S -> "dalww0_s(gkm,mkm,"
      | D_Alpha_WWWW0_T -> "dalww0_t(gkm,mkm,"
      | D_Alpha_WWWW0_U -> "dalww0_u(gkm,mkm,"
      | D_Alpha_WWWW2_S -> "dalww2_s(gkm,mkm,"
      | D_Alpha_WWWW2_T -> "dalww2_t(gkm,mkm,"
      | D_Alpha_ZZZZ_S -> "dalz4_s(gkm,mkm,"
      | D_Alpha_ZZZZ_T -> "dalz4_t(gkm,mkm,"
      | G_HWW -> "ghww" | G_HZZ -> "ghzz"
      | G_HHWW -> "ghhww" | G_HHZZ -> "ghhzz"
      | G_Htt -> "ghtt" | G_Hbb -> "ghbb"
      | G_Htautau -> "ghtautau" | G_Hcc -> "ghcc" | G_Hmm -> "ghmm"
      | G_HGaZ -> "ghgaz" | G_HGaGa -> "ghgaga" | G_Hgg -> "ghgg"
      | G_H3 -> "gh3" | G_H4 -> "gh4"
      | G2 -> "gs**2" | Gs -> "gs" | I_Gs -> "igs"
      | Mass f -> "mass" ^ flavor_symbol f
      | Width f -> "width" ^ flavor_symbol f
      | K_Matrix_Coeff i -> "kc" ^ string_of_int i
      | K_Matrix_Pole i -> "kp" ^ string_of_int i

  end

(* \thocwmodulesection{Incomplete Standard Model in $R_\xi$ Gauge} *)

(* \begin{dubious}
     At the end of the day, we want a functor mapping from gauge models
     in unitarity gauge to $R_\xi$ gauge and vice versa.  For this, we
     will need a more abstract implementation of (spontaneously broken)
     gauge theories.
   \end{dubious} *)

module SM_Rxi =
  struct
    let rcs = RCS.rename rcs_file "Models.SM_Rxi"
        [ "minimal electroweak standard model in R-xi gauge";
          "NB: very incomplete still!, no CKM matrix" ]

    open Coupling

    module SM = SM(SM_no_anomalous)
    let options = SM.options
    type flavor = SM.flavor
    let flavors = SM.flavors
    let external_flavors = SM.external_flavors
    type constant = SM.constant
    let lorentz = SM.lorentz
    let color = SM.color
    let goldstone = SM.goldstone
    let conjugate = SM.conjugate
    let fermion = SM.fermion
    let colsymm = SM.colsymm

(* \begin{dubious}
     Check if it makes sense to have separate gauge fixing parameters 
     for each vector boson.  There's probably only one independent
     parameter for each group factor.
   \end{dubious} *)

    type gauge =
      | XiA | XiZ | XiW

    let gauge_symbol = function
      | XiA -> "xia" | XiZ -> "xi0" | XiW -> "xipm"

(* Change the gauge boson propagators and make the Goldstone bosons
   propagating.  *)
    let propagator = function
      | SM.G SM.Ga -> Prop_Gauge XiA
      | SM.G SM.Z -> Prop_Rxi XiZ
      | SM.G SM.Wp | SM.G SM.Wm -> Prop_Rxi XiW
      | SM.O SM.Phip | SM.O SM.Phim | SM.O SM.Phi0 -> Prop_Scalar
      | f -> SM.propagator f

    let width = SM.width

    module F = Fusions (struct
      type f = flavor
      type c = constant
      let compare = compare
      let conjugate = conjugate
    end)

    let vertices = SM.vertices

    let table = F.of_vertices (vertices ())
    let fuse2 = F.fuse2 table
    let fuse3 = F.fuse3 table
    let fuse = F.fuse table
    let max_degree () = 3

    let parameters = SM.parameters
    let flavor_of_string = SM.flavor_of_string
    let flavor_to_string = SM.flavor_to_string
    let flavor_to_TeX = SM.flavor_to_TeX
    let flavor_symbol = SM.flavor_symbol
    let pdg = SM.pdg
    let mass_symbol = SM.mass_symbol
    let width_symbol = SM.width_symbol
    let constant_symbol = SM.constant_symbol

  end

(* \thocwmodulesection{Groves} *)

module Groves (M : Model.Gauge) : Model.Gauge =
  struct
    let max_generations = 5
    let rcs = RCS.rename M.rcs
        ("Models.Groves(" ^ (RCS.name M.rcs) ^ ")")
        ([ "experimental Groves functor";
           Printf.sprintf "for maximally %d flavored legs"
             (2 * max_generations) ] @
         RCS.description M.rcs)

    let options = M.options

    type matter_field = M.matter_field * int
    type gauge_boson = M.gauge_boson
    type other = M.other
    type field =
      | Matter of matter_field
      | Gauge of gauge_boson
      | Other of other
    type flavor = M of matter_field | G of gauge_boson | O of other
    let matter_field (f, g) = M (f, g)
    let gauge_boson f = G f
    let other f = O f
    let field = function
      | M f -> Matter f
      | G f -> Gauge f
      | O f -> Other f
    let project = function
      | M (f, _) -> M.matter_field f
      | G f -> M.gauge_boson f
      | O f -> M.other f
    let inject g f =
      match M.field f with
      | M.Matter f -> M (f, g)
      | M.Gauge f -> G f
      | M.Other f -> O f
    type gauge = M.gauge
    let gauge_symbol = M.gauge_symbol
    let color f = M.color (project f)
    let pdg f = M.pdg (project f)
    let lorentz f = M.lorentz (project f)
    let propagator f = M.propagator (project f)
    let fermion f = M.fermion (project f)
    let colsymm f = M.colsymm (project f)
    let width f = M.width (project f)
    let mass_symbol f = M.mass_symbol (project f)
    let width_symbol f = M.width_symbol (project f)
    let flavor_symbol f = M.flavor_symbol (project f)

    type constant = M.constant
    let constant_symbol = M.constant_symbol
    let max_degree = M.max_degree
    let parameters = M.parameters

    let conjugate = function
      | M (_, g) as f -> inject g (M.conjugate (project f))
      | f -> inject 0 (M.conjugate (project f))

    let read_generation s =
      try
        let offset = String.index s '/' in
        (int_of_string
           (String.sub s (succ offset) (String.length s - offset - 1)),
         String.sub s 0 offset)
      with
      | Not_found -> (1, s)

    let format_generation c s =
      s ^ "/" ^ string_of_int c

    let flavor_of_string s =
      let g, s = read_generation s in
      inject g (M.flavor_of_string s)
        
    let flavor_to_string = function
      | M (_, g) as f -> format_generation g (M.flavor_to_string (project f))
      | f -> M.flavor_to_string (project f)
        
    let flavor_to_TeX = function
      | M (_, g) as f -> format_generation g (M.flavor_to_TeX (project f))
      | f -> M.flavor_to_TeX (project f)

    let goldstone = function
      | G _ as f ->
          begin match M.goldstone (project f) with
          | None -> None
          | Some (f, c) -> Some (inject 0 f, c)
          end
      | M _ | O _ -> None

    let clone generations flavor =
      match M.field flavor with
      | M.Matter f -> List.map (fun g -> M (f, g)) generations
      | M.Gauge f -> [G f]
      | M.Other f -> [O f]

    let generations = ThoList.range 1 max_generations

    let flavors () =
      ThoList.flatmap (clone generations) (M.flavors ())

    let external_flavors () =
      List.map (fun (s, fl) -> (s, ThoList.flatmap (clone generations) fl))
        (M.external_flavors ())

    module F = Fusions (struct
      type f = flavor
      type c = constant
      let compare = compare
      let conjugate = conjugate
    end)

(* In the following functions, we might replace [_] by [(M.Gauge _ | M.Other _)],
   in order to allow the compiler to check completeness.  However, this
   makes the code much less readable. *)

    let clone3 ((f1, f2, f3), v, c) =
      match M.field f1, M.field f2, M.field f3 with
      | M.Matter _, M.Matter _, M.Matter _ ->
          invalid_arg "Models.Groves().vertices: three matter fields!"
      | M.Matter f1', M.Matter f2', _ ->
          List.map (fun g -> ((M (f1', g), M (f2', g), inject 0 f3), v, c))
            generations
      | M.Matter f1', _, M.Matter f3' ->
          List.map (fun g -> ((M (f1', g), inject 0 f2, M (f3', g)), v, c))
            generations
      | _, M.Matter f2', M.Matter f3' ->
          List.map (fun g -> ((inject 0 f1, M (f2', g), M (f3', g)), v, c))
            generations
      | M.Matter _, _, _ | _, M.Matter _, _ | _, _, M.Matter _ ->
          invalid_arg "Models.Groves().vertices: lone matter field!"
      | _, _, _ ->
          [(inject 0 f1, inject 0 f2, inject 0 f3), v, c]
      
    let clone4 ((f1, f2, f3, f4), v, c) =
      match M.field f1, M.field f2, M.field f3, M.field f4 with
      | M.Matter _, M.Matter _, M.Matter _, M.Matter _ ->
          invalid_arg "Models.Groves().vertices: four matter fields!"
      | M.Matter _, M.Matter _, M.Matter _, _
      | M.Matter _, M.Matter _, _, M.Matter _
      | M.Matter _, _, M.Matter _, M.Matter _
      | _, M.Matter _, M.Matter _, M.Matter _ ->
          invalid_arg "Models.Groves().vertices: three matter fields!"
      | M.Matter f1', M.Matter f2', _, _ ->
          List.map (fun g ->
            ((M (f1', g), M (f2', g), inject 0 f3, inject 0 f4), v, c))
            generations
      | M.Matter f1', _, M.Matter f3', _ ->
          List.map (fun g ->
            ((M (f1', g), inject 0 f2, M (f3', g), inject 0 f4), v, c))
            generations
      | M.Matter f1', _, _, M.Matter f4' ->
          List.map (fun g ->
            ((M (f1', g), inject 0 f2, inject 0 f3, M (f4', g)), v, c))
            generations
      | _, M.Matter f2', M.Matter f3', _ ->
          List.map (fun g ->
            ((inject 0 f1, M (f2', g), M (f3', g), inject 0 f4), v, c))
            generations
      | _, M.Matter f2', _, M.Matter f4'  ->
          List.map (fun g ->
            ((inject 0 f1, M (f2', g), inject 0 f3, M (f4', g)), v, c))
            generations
      | _, _, M.Matter f3', M.Matter f4'  ->
          List.map (fun g ->
            ((inject 0 f1, inject 0 f2, M (f3', g), M (f4', g)), v, c))
            generations
      | M.Matter _, _, _, _ | _, M.Matter _, _, _
      | _, _, M.Matter _, _ | _, _, _, M.Matter _ ->
          invalid_arg "Models.Groves().vertices: lone matter field!"
      | _, _, _, _ ->
          [(inject 0 f1, inject 0 f2, inject 0 f3, inject 0 f4), v, c]
      
    let clonen (fl, v, c) =
      match List.map M.field fl with
      | _ -> failwith "Models.Groves().vertices: incomplete"
      
    let vertices () =
      let vertices3, vertices4, verticesn = M.vertices () in
      (ThoList.flatmap clone3 vertices3,
       ThoList.flatmap clone4 vertices4,
       ThoList.flatmap clonen verticesn)
        
    let table = F.of_vertices (vertices ())
    let fuse2 = F.fuse2 table
    let fuse3 = F.fuse3 table
    let fuse = F.fuse table

(* \begin{dubious}
     The following (incomplete) alternative implementations are
     included for illustrative purposes only:
   \end{dubious} *)

    let injectl g fcl =
      List.map (fun (f, c) -> (inject g f, c)) fcl
      
    let alt_fuse2 f1 f2 =
      match f1, f2 with
      | M (f1', g1'), M (f2', g2') ->
          if g1' = g2' then
            injectl 0 (M.fuse2 (M.matter_field f1') (M.matter_field f2'))
          else
            []
      | M (f1', g'), _ -> injectl g' (M.fuse2 (M.matter_field f1') (project f2))
      | _, M (f2', g') -> injectl g' (M.fuse2 (project f1) (M.matter_field f2'))
      | _, _ -> injectl 0 (M.fuse2 (project f1) (project f2))

    let alt_fuse3 f1 f2 f3 =
      match f1, f2, f3 with
      | M (f1', g1'), M (f2', g2'), M (f3', g3') ->
          invalid_arg "Models.Groves().fuse3: three matter fields!"
      | M (f1', g1'), M (f2', g2'), _ ->
          if g1' = g2' then
            injectl 0
              (M.fuse3 (M.matter_field f1') (M.matter_field f2') (project f3))
          else
            []
      | M (f1', g1'), _, M (f3', g3') ->
          if g1' = g3' then
            injectl 0
              (M.fuse3 (M.matter_field f1') (project f2) (M.matter_field f3'))
          else
            []
      | _, M (f2', g2'), M (f3', g3') ->
          if g2' = g3' then
            injectl 0
              (M.fuse3 (project f1) (M.matter_field f2') (M.matter_field f3'))
          else
            []
      | M (f1', g'), _, _ ->
          injectl g' (M.fuse3 (M.matter_field f1') (project f2) (project f3))
      | _, M (f2', g'), _ ->
          injectl g' (M.fuse3 (project f1) (M.matter_field f2') (project f3))
      | _, _, M (f3', g') ->
          injectl g' (M.fuse3 (project f1) (project f2) (M.matter_field f3'))
      | _, _, _ -> injectl 0 (M.fuse3 (project f1) (project f2) (project f3))

  end

(* \thocwmodulesection{MSM With Cloned Families} *)

module SM_clones = Groves(SM(SM_no_anomalous))
module SM3_clones = Groves(SM3(SM_no_anomalous))

(*i
 *  Local Variables:
 *  mode:caml
 *  indent-tabs-mode:nil
 *  page-delimiter:"^(\\* .*\n"
 *  End:
i*)

