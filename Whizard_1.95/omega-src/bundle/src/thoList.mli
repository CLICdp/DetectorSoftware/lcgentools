(* $Id: thoList.mli 68 2007-11-22 11:11:19Z ohl $ *)
(* Copyright (C) 1999-2004 by Thorsten Ohl <ohl@physik.uni-wuerzburg.de> et al.
   O'Mega is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by 
   the Free Software Foundation; either version 2, or (at your option)
   any later version.
   O'Mega is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.  *)  

(* [splitn n l = (hdn l, tln l)], but more efficient. *)
val hdn : int -> 'a list -> 'a list
val tln : int -> 'a list -> 'a list
val splitn : int -> 'a list -> 'a list * 'a list

(* [of_subarray n m a] is $[\ocwlowerid{a.}(\ocwlowerid{n});
   \ocwlowerid{a.}(\ocwlowerid{n}+1);\ldots;
   \ocwlowerid{a.}(\ocwlowerid{m})]$.  Values of~[n] and~[m]
   out of bounds are silently shifted towards these bounds.  *)
val of_subarray : int -> int -> 'a array -> 'a list

(* [range s n m] is $[\ocwlowerid{n}; \ocwlowerid{n}+\ocwlowerid{s};
   \ocwlowerid{n}+2\ocwlowerid{s};\ldots;
   \ocwlowerid{m} - ((\ocwlowerid{m}-\ocwlowerid{n})\mod s)]$ *)
val range : ?stride:int -> int -> int -> int list

(* Compress identical elements in a sorted list.  Identity
   is determined using the polymorphic equality function
   [Pervasives.(=)]. *)
val uniq : 'a list -> 'a list

(* [compare cmp l1 l2] compare two lists [l1] and [l2] according to
   [cmp].  [cmp] defaults to the polymorphic [Pervasives.compare].  *)
val compare : ?cmp:('a -> 'a -> int) -> 'a list -> 'a list -> int

(* Collect and count identical elements in a list.  Identity
   is determined using the polymorphic equality function
   [Pervasives.(=)].  [classify] does not assume that the list
   is sorted.  However, it is~$O(n)$ for sorted lists and~$O(n^2)$
   in the worst case.  *)
val classify : 'a list -> (int * 'a) list

(* Collect the second factors with a common first factor in lists. *)
val factorize : ('a * 'b) list -> ('a * 'b list) list

(* [flatmap f] is equivalent to $\ocwlowerid{List.flatten} \circ
   (\ocwlowerid{List.map}\;\ocwlowerid{f})$, but more efficient,
   because no intermediate lists are built. *)
val flatmap : ('a -> 'b list) -> 'a list -> 'b list

val clone : int -> 'a -> 'a list
val multiply : int -> 'a list -> 'a list

(* \begin{dubious}
     Invent other names to avoid confusions with [List.fold_left2]
     and [List.fold_right2].
   \end{dubious} *)
val fold_right2 : ('a -> 'b -> 'b) -> 'a list list -> 'b -> 'b
val fold_left2 : ('b -> 'a -> 'b) -> 'b -> 'a list list -> 'b

(*i
 *  Local Variables:
 *  mode:caml
 *  indent-tabs-mode:nil
 *  page-delimiter:"^(\\* .*\n"
 *  End:
i*)
