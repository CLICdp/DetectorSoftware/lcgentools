(* $Id: f90_QCD.ml,v 1.7.8.1 2005/11/07 01:50:15 ohl Exp $ *)
(* Copyright (C) 2000-2004 by Thorsten Ohl <ohl@physik.uni-wuerzburg.de> et al.
   O'Mega is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by 
   the Free Software Foundation; either version 2, or (at your option)
   any later version.
   O'Mega is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.  *)  

let rcs_file = RCS.parse "F90_QCD" ["QCD"]
    { RCS.revision = "$Revision: 1.7.8.1 $";
      RCS.date = "$Date: 2005/11/07 01:50:15 $";
      RCS.author = "$Author: ohl $";
      RCS.source
        = "$Source: /home/sources/ohl/ml/omega/src/f90_QCD.ml,v $" }

(* QCD with colors. *)

module M : Model.T =
  struct
    let rcs = rcs_file

    open Coupling

    let options = Options.empty
        
    type flavor =
      | U | Ubar | D | Dbar
      | C | Cbar | S | Sbar
      | T | Tbar | B | Bbar
      | Gl 
         
    let external_flavors () = 
      [ "Quarks", [U; D; C; S; T; B; Ubar; Dbar; Cbar; Sbar; Tbar; Bbar]; 
        "Gauge Bosons", [Gl]]
    let flavors () = ThoList.flatmap snd (external_flavors ())

    type gauge = unit
    type constant = Gs | G2 | I_Gs

    let lorentz = function
      | U | D | C | S | T | B -> Spinor
      | Ubar | Dbar | Cbar | Sbar | Tbar | Bbar -> ConjSpinor 
      | Gl -> Vector

    let color = function
      | U | D | C | S | T | B -> Color.SUN 3
      | Ubar | Dbar | Cbar | Sbar | Tbar | Bbar -> Color.SUN (-3)
      | Gl -> Color.AdjSUN 3
      | _ -> Color.Singlet

    let propagator = function
      | U | D | C | S | T | B -> Prop_Spinor
      | Ubar | Dbar | Cbar | Sbar | Tbar | Bbar -> Prop_ConjSpinor 
      | Gl -> Prop_Feynman

    let width _ = Timelike

    let goldstone _ =
      None

    let conjugate = function
      | U -> Ubar
      | D -> Dbar
      | C -> Cbar
      | S -> Sbar
      | T -> Tbar
      | B -> Bbar
      | Ubar -> U 
      | Dbar -> D 
      | Cbar -> C 
      | Sbar -> S 
      | Tbar -> T 
      | Bbar -> B 
      | Gl -> Gl 

    let fermion = function
      | U | D | C | S | T | B -> 1
      | Ubar | Dbar | Cbar | Sbar | Tbar | Bbar -> -1
      | Gl -> 0

    let colsymm _ = (0,false),(0,false)

    module F = Models.Fusions (struct
      type f = flavor
      type c = constant
      let compare = compare
      let conjugate = conjugate
    end)

(* This is compatible with CD+. *)

    let color_current =
      [ ((Dbar, Gl, D), FBF ((-1), Psibar, V, Psi), Gs);
        ((Ubar, Gl, U), FBF ((-1), Psibar, V, Psi), Gs);
        ((Cbar, Gl, C), FBF ((-1), Psibar, V, Psi), Gs);
        ((Sbar, Gl, S), FBF ((-1), Psibar, V, Psi), Gs);
        ((Tbar, Gl, T), FBF ((-1), Psibar, V, Psi), Gs);
        ((Bbar, Gl, B), FBF ((-1), Psibar, V, Psi), Gs)]

   let three_gluon =
      [ ((Gl, Gl, Gl), Gauge_Gauge_Gauge 1, Gs)]

    let gauge4 = Vector4 [(2, C_13_42); (-1, C_12_34); (-1, C_14_23)]

    let four_gluon = 
      [ ((Gl, Gl, Gl, Gl), gauge4, G2)]

    let vertices3 = 
      (color_current @ three_gluon)
       
   let vertices4 = four_gluon

    let vertices () =
      (vertices3, vertices4, [])
      
    let table = F.of_vertices (vertices ())
    let fuse2 = F.fuse2 table
    let fuse3 = F.fuse3 table
    let fuse = F.fuse table
    let max_degree () = 4

    let parameters () = { input = [Gs, 1.0]; derived = []; derived_arrays = [] } 
    let flavor_of_string = function
      | "u" -> U 
      | "d" -> D 
      | "c" -> C 
      | "s" -> S 
      | "t" -> T 
      | "b" -> B 
      | "ubar" -> Ubar
      | "dbar" -> Dbar
      | "cbar" -> Cbar
      | "sbar" -> Sbar
      | "tbar" -> Tbar
      | "bbar" -> Bbar
      | "gl" -> Gl 
      | _ -> invalid_arg "Models.QCD.flavor_of_string"

    let flavor_to_string = function
      | U -> "u" 
      | Ubar -> "ubar" 
      | D -> "d" 
      | Dbar -> "dbar" 
      | C -> "c" 
      | Cbar -> "cbar" 
      | S -> "s" 
      | Sbar -> "sbar" 
      | T -> "t" 
      | Tbar -> "tbar" 
      | B -> "b" 
      | Bbar -> "bbar" 
      | Gl -> "gl" 

    let flavor_symbol = function
      | U -> "u"
      | Ubar -> "ubar"
      | D -> "d"
      | Dbar -> "dbar"
      | C -> "c"
      | Cbar -> "cbar"
      | S -> "s"
      | Sbar -> "sbar"
      | T -> "t"
      | Tbar -> "tbar"
      | B -> "b"
      | Bbar -> "bbar"
      | Gl -> "gl" 

    let gauge_symbol () =
      failwith "Models.QCD.gauge_symbol: internal error"

    let pdg = function
      | D -> 1 | Dbar -> -1
      | U -> 2 | Ubar -> -2
      | S -> 3 | Sbar -> -3
      | C -> 4 | Cbar -> -4
      | B -> 5 | Bbar -> -5
      | T -> 6 | Tbar -> -6
      | Gl -> 21 

    let mass_symbol f = 
      "mass(" ^ string_of_int (abs (pdg f)) ^ ")"

    let width_symbol f =
      "width(" ^ string_of_int (abs (pdg f)) ^ ")"

    let constant_symbol = function
      | Gs -> "gs"
      | G2 -> "gs**2"
  end

module O = Omega.Make(Fusion.Mixed23)(Targets.Fortran)(M)

let _ = O.main ()


(*i
 *  Local Variables:
 *  mode:caml
 *  indent-tabs-mode:nil
 *  page-delimiter:"^(\\* .*\n"
 *  End:
i*)
