(* $Id: color.mli 68 2007-11-22 11:11:19Z ohl $ *)
(* Copyright (C) 2001-2004 by Thorsten Ohl <ohl@physik.uni-wuerzburg.de> et al.
   O'Mega is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by 
   the Free Software Foundation; either version 2, or (at your option)
   any later version.
   O'Mega is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.  *)  

(* \thocwmodulesection{Quantum Numbers} *)

(* Color is not necessarily the~$\textrm{SU}(3)$ of QCD.  Conceptually,
   it can be any \emph{unbroken} symmetry (\emph{broken} symmetries correspond
   to [Model.flavor]).  In order to keep the group theory simple, we confine
   ourselves to the fundamental and adjoint representation
   of~$\textrm{SU}(N_C)$ for the moment and use the~$\mathrm{SU}(N_C)$
   completeness relation\footnote{The corresponding formulae for the other
   Lie algebras (except~$E_8$) can be found in~\cite{Cvi76}.}
   \begin{equation}
     T^{a}_{ij} T^{a}_{kl} = 
         \frac{1}{2} \delta_{il} \delta_{jk}
       - \frac{1}{2N_C} \delta_{ij} \delta_{kl}
   \end{equation}
   for the conventional normalization 
   \begin{equation}
     \textrm{tr}(T_{a}T_{b}) = \frac{1}{2}\delta_{ab}
   \end{equation}
   Therefore, particles are either color singlets or live in the defining
   representation of $\textrm{SU}(N)$: [SUN]$(|n|)$, its conjugate [SUN]$(-|n|)$
   or in the adjoint representation of $\textrm{SU}(N)$: [AdjSUN]$(n)$. *)

type t = Singlet | SUN of int | AdjSUN of int
val conjugate : t -> t

module type NC =
  sig
    val nc : int
  end

(* \thocwmodulesection{Realistic Amplitudes} *)

(* The applications that we are interested in are typically not in an
   asymptotic region where one particular algorithm is always optimal.
   In fact, empirically it appears that different amplitudes will prefer
   different algorithms:
   \begin{enumerate}
     \setcounter{enumi}{-1}
     \item \textit{trivial case:} constant factor if there is either only
       a single color flow or none at all.
     \item \textit{brute force:} calculate the amplitude in the fundamental
       and adjoint representation.  This will multiply the complexity of
       the squared amplitude by
       \begin{equation}
         N_C^{\#\text{quarks}}\cdot
         N_C^{\#\text{anti-quarks}}\cdot
         (N_C^2-1)^{\#\text{gluons}}
       \end{equation}
       which is prohibitive in most cases of interest.  But there is always
       the option of replacing the color summation by a Monte Carlo
       integration~\cite{Kleiss/etal:Color-Monte-Carlo}.
     \item \textit{\texttt{MADGRAPH}:} give up on O'Mega-style factorization, go
       back to Feynman diagrams and sum over colors in the
       \texttt{MADGRAPH}~\cite{Barger/etal:1992:color,MADGRAPH:1994} way.
     \item \textit{factorized \texttt{MADGRAPH}:} factorize each color
       eigenamplitude. This will multiply the O'Mega complexity the a small
       power of the number of color eigenamplitudes.
     \item \textit{color flow basis:} calculate the amplitudes in a color
       flow basis. This should allow complete O'Mega-style factorization,
       but each fusion introduces a product of the number of contributing
       color flows. The overall complexity is very hard to estimate a priori.
   \end{enumerate} *)

(* \thocwmodulesubsection{General Case}
   We will implement a variation of numeric color
   diagonalization~\cite{Barger/etal:1992:color}. *)

(* \begin{dubious}
     The old trick for Feynman diagrams as implemented in \texttt{MADGRAPH} is based
     on the observation that the QCD Feynman rules factorize \emph{exactly}
     into a color part and a combined spin/flavor part, iff
     quartic couplings are represented by cubic coupling to auxiliary
     non-propagating antisymmetric tensor field
     \begin{multline*}
       \parbox{26mm}{\fmfframe(2,2)(2,1){\begin{fmfgraph*}(22,22)
         \fmfsurround{d1,e1,d2,e2,d3,e3,d4,e4}
         \fmf{gluon}{v12,e1}
         \fmf{gluon}{v12,e2}
         \fmf{gluon}{v34,e3}
         \fmf{gluon}{v34,e4}
         \fmf{dashes}{v12,v34}
         \fmflabel{1}{e1}
         \fmflabel{2}{e2}
         \fmflabel{3}{e3}
         \fmflabel{4}{e4}
         \fmfdot{v12,v34}
       \end{fmfgraph*}}} \,= 
         (\ii g f_{a_1a_2b} T_{\mu_1\mu_2,\nu_1\nu_2})
           \left(\frac{\ii g^{\nu_1\nu_3} g^{\nu_2\nu_4}}{2}\right)
         (\ii g f_{a_3a_4b} T_{\mu_3\mu_4,\nu_3\nu_4}) \\
         = \mbox{} - \ii g^2 f_{a_1a_2b}f_{a_3a_4b}
             (g_{\mu_1\mu_3} g_{\mu_4\mu_2} - g_{\mu_1\mu_4} g_{\mu_2\mu_3})
     \end{multline*}
     with $T_{\mu_1\mu_2,\mu_3\mu_4} = 
           g_{\mu_1\mu_3}g_{\mu_4\mu_2}-g_{\mu_1\mu_4}g_{\mu_2\mu_3}$.
     Therefore the Feynman diagrams also factorize \emph{exactly} into a
     color part and a combined spin/flavor part, but not amplitudes and
     sums of Feynman diagrams.
     
     Anyway, the scattering amplitude can be written
     \begin{equation*}
       T = \sum_{i=1}^{\#\text{diagrams}} C_i T_i
     \end{equation*}
     and the squared amplitude
     \begin{equation*}
       TT^{\dagger} =
         \sum_{i,j=1}^{\#\text{diagrams}} C_iC_j^{\dagger} T_iT_j^{\dagger}
     \end{equation*}
     can be summed and/or averaged over colors \emph{separately}
     \begin{equation*}
       \sum_{\text{colors}} TT^{\dagger} =
         \sum_{i,j=1}^{\#\text{diagrams}} \mathbf{C}_{ij} T_iT_j^{\dagger}
     \end{equation*}
     with
     \begin{equation*}
       \mathbf{C}_{ij} = \sum_{\text{colors}} C_iC_j^{\dagger}
     \end{equation*}
     
     The matrix $\mathbf{C}$ is hermitian (even real symmetric) and
     can be diagonalized with an \emph{orthogonal} matrix~$c$
     \begin{equation*}
       \mathbf{C} = c^T \mathbf{\hat C} c
     \end{equation*}
     i.\,e.~$\mathbf{C}_{ij} = \sum_a c_a^{\hphantom{a}i}
             \mathbf{\hat C}_a  c_a^{\hphantom{a}j}$ with
     \begin{equation*}
       \sum_{a=1}^{\#\text{diagrams}}
          c_a^{\hphantom{a}i} c_a^{\hphantom{a}j} = \delta^{ij}\quad\text{and}\quad
       \sum_{i=1}^{\#\text{diagrams}}
          c_a^{\hphantom{a}i} c_b^{\hphantom{b}i} = \delta_{ab}
     \end{equation*}
     Then
     \begin{equation*}
       \sum_{\text{colors}} TT^{\dagger} =
         \sum_{a=1}^{\#\text{eigenvalues}} \mathbf{\hat C}_a
           \hat T_a (\hat T_a)^{\dagger}
     \end{equation*}
     with $\hat T_a = \sum_i c_a^{\hphantom{a}i} T_i$ and only the non-zero
     eigenvalues contribute.  This formalism ist therefore useful iff are
     only a few.  However, the formalism \emph{appears} to depend on
     Feynman diagrams, i.\,e.~unsuitable for factorized scattering amplitudes
     
     %%% For the sums over Feynman diagrams, color eigenamplitudes and wave
     %%% functions, we introduce the following conventions:
     %%% \begin{align*}
     %%%   i &\in \{ 1, 2, \ldots, N_{\mathrm{FD}}\} \\
     %%%   a &\in \{ 1, 2, \ldots, N_{\mathrm{ev}}, \ldots, N_{\mathrm{FD}}\} \\
     %%%   n &\in \{ 1, 2, \ldots, N_{\mathrm{WF}}\}
     %%% \end{align*}
     
     A 1POW can be written as a sum over
     \emph{all} Feynman diagrams 
     \begin{equation*}
       W_n = \sum_{i=1}^{\#\text{diagrams}} w_{n,i} = \Braket{0|\phi|n}
     \end{equation*}
     where $w_{n,i} = \Braket{0|\phi|n}_{\text{diagram \#$i$}}$
     corresponds to the contribution of diagram~$i$ to the
     1POW~$W_n$ (most of which vanish, of course).
     
     In analogy to
     \begin{equation*}
       \hat T_a = \sum_{i=1}^{\#\text{diagrams}} c_a^{\hphantom{a}i} T_i
     \end{equation*}
     we define
     \begin{equation*}
       \widehat W_{n,a} = \sum_{i=1}^{\#\text{diagrams}} c_a^{\hphantom{a}i} w_{n,i}
     \end{equation*}
     and since~$c$ is \emph{orthogonal} we have the inverse relation
     \begin{equation*}
       w_{n,i} = \sum_a \widehat W_{n,a} c_a^{\hphantom{a}i}
     \end{equation*}
     where the sum \emph{must} include the vanishing
     eigenvalues this time.
     
     Decomposing the keystones in
     $T = \sum_{p,q,r} K^3_{pqr} W_p W_q W_r + \text{quartic} + \ldots$
     according to Feynman diagrams
     \begin{equation*}
       T_i = \sum_{p,q,r} k^{3,i}_{pqr} w_{p,i} w_{q,i} w_{r,i}
                 + \text{quartic} + \ldots
     \end{equation*}
     we can express the color eigenamplitudes
     \begin{multline*}
       \hat T_a
         = \sum_{\substack{i\\p,q,r}} c_a^{\hphantom{a}i}
             k^{3,i}_{pqr} w_{p,i} w_{q,i} w_{r,i} + \text{quartic} + \ldots \\
         = \sum_{\substack{i\\b,c,d\\p,q,r}}
             k^{3,i}_{pqr} c_a^{\hphantom{a}i}
             c_b^{\hphantom{b}i} c_c^{\hphantom{c}i} c_d^{\hphantom{d}i}
            \widehat W_{p,b} \widehat W_{q,c} \widehat W_{r,d} + \ldots
         = \sum_{\substack{b,c,d\\p,q,r}}
            \hat K^{3,a,bcd}_{pqr}
            \widehat W_{p,b} \widehat W_{q,c} \widehat W_{r,d} + \ldots
     \end{multline*}
     through colored keystones
     \begin{equation*}
       \hat K^{3,a,bcd}_{pqr} =
         \sum_i k^{3,i}_{pqr}
             c_a^{\hphantom{a}i} c_b^{\hphantom{b}i}
             c_c^{\hphantom{c}i} c_d^{\hphantom{d}i}
     \end{equation*}
     \emph{with all diagrams summed!}.
     
     Analogous expressions can be derived for quartic and higher couplings,
     of course.  The same approach works for colored fusions that build the
     1POWs recursively:
     \begin{equation*}
       \widehat W_{p,a} =
           \sum_{\substack{b,c\\q,r}}
              F^{3,a,bc}_{pqr} \widehat W_{p,b} \widehat W_{q,c} + \ldots
     \end{equation*}
     The resulting algorithm is
     \begin{enumerate}
       \item \emph{Expand} expand the
         DAG into the corresponding list of Feynman diagrams
       \item \emph{Diagonalize} calculate the elements
         $\mathbf{C}_{ij}$ of the color matrix $\mathbf{C}_{ij}$ and
         diagonalize it numerically for the relevant number of colors.
       \item \emph{Classify} find which Feynman diagrams
         contribute to which 1POW and construct a
         dictionary
       \item \emph{Recombine}  numerically calculate the
         colored keystone and fusion
         coefficients and generate the code for the amplitude
     \end{enumerate}
   \end{dubious}
   \begin{dubious}
     Here's a sketch of the algorithm:
     \begin{enumerate}
       \item expand the DAG~$D$ to a list~$L$ of trees
       \item numerically calculate the matrix~$C$ of color factors
         for the squared matrix element
       \item diagonalize~$C$
       \item tag the wave functions in~$D$ by the list of their
         appearances in~$L$
       \item for each wavefunction in~$D$, calculate the coefficients
         of the eigenvectors corresponding to non-zero eigenvalues of~$C$
       \item (like for Fermi statistics) keep only the factors that are
         \emph{not} already in the daughter wave functions
     \end{enumerate}
   \end{dubious} *)

(* \begin{dubious}
     This multiplies the complexity of the colorless amplitude
     by the number of eigenvectors with non-zero eigenvalues of~$C$.
     Asymptotically, this will beat~\cite{MADGRAPH:1994}, but it is
     not obvious where the break-even point is for many eigenvectors.
     Therefore more precise estimates will be useful \ldots
   \end{dubious} *)

(* We allow different types for propagators and external lines.  This
   allows to calculate ``pure'' color diagrams as [(unit, 'e) amplitude]
   with manifest equivalence of different particles with identical
   color representations. *)
type ('a, 'e) amplitude

(* [ext color itag et] constructs an external particle wavefunction in
   the color representation [color] with external tag [etag] and internal
   tag [itag]. *)
val ext : t -> 'a -> 'e -> ('a, 'e) amplitude

(* [fuse2 color itag wf1 wf2] fuses the wavefunctions [wf1] and [wf2] to
   a wavefunction in the color representation [color] with the tag [itag].  *)
val fuse2 : t -> 'a -> ('a, 'e) amplitude ->
  ('a, 'e) amplitude -> ('a, 'e) amplitude

(* [fuse3 color itag wf1 wf2 wf3] ditto, but for quartic vertices. *)
val fuse3 : t -> 'a -> ('a, 'e) amplitude ->
  ('a, 'e) amplitude -> ('a, 'e) amplitude -> ('a, 'e) amplitude

(* [fuse color itag wf_list] is the same in principle for arbitrary
   arity, but the implementation is liekely to raise [Incomplete]
   for too high arity.  *)
val fuse : t -> 'a -> ('a, 'e) amplitude list -> ('a, 'e) amplitude

(* [Mismatch] is raised when the Feynman rules are inconsistent and
   a diagram contains a colored vertex. *)
exception Mismatch

(* [Impossible_quartic] is raised when the Feynman contain quartic gluon
   vertices that destroy factorization of color amplitudes.  This problem
   can be circumvented using a non-propagating auxiliary field for
   representing quartic couplings through cubic couplings. *)
exception Impossible_quartic

(* As an intermediate representation for color amplitudes corresponding
   to a [('a, 'e) Tree.t], objects of type [('a, 'e) amplitude] could be
   avoided with some effort using [Tree.fold] directly.   However, we
   also want to eliminate redundancies and have to check for equivalent
   color amplitudes.  Also: we are guaranteed that [('a, 'e) amplitude]
   is well formed, while [('a, 'e) Tree.t] could have been built from
   an inconsistent set of Feynman rules (i.\,e.~colored vertices).  *)

(* [of_tree color proj tree] constructs the color amplitude for the
   tree diagram [tree] using [color] for determining the color from
   internal tags and [proj] for projecting out the irrelevant information
   from internal tags.  *)
val of_tree : ('a -> t) -> ('a -> 'b) -> ('a, 'e) Tree.t -> ('b, 'e) amplitude

(* [to_string ifmt efmt amplitude] constructs a textual representation of the
   color amplitude [amplitude], using the formatters [ifmt] and [efmt] for
   the internal and external tags respectively.  *)
val to_string : ('a -> string) -> ('e -> string) -> ('a, 'e) amplitude -> string

(*i
type ('tag, 'ext, 'sng, 'fnd, 'cjg, 'adj, 'a) fold_functions
val fold : ('tag, 'ext, 'sng, 'fnd, 'cjg, 'adj, 'a) fold_functions ->
  ('tag, 'ext) amplitude -> 'a
i*)

(* \thocwmodulesubsection{Case of Few Color Flows}
   Iff there are only few contributing color flows, it is more efficient to
   perform all calculations directly in a color flow basis. *)

(* \thocwmodulesection{Evaluation} *)

(* For further processing we can either reduce internal gluons via the
   completeness relation or construct a trace of the square in one step.
   The first approach reduces part of the complexity from~$N^2$ to~$N$. *)

(* \thocwmodulesubsection{Algebraic Infrastructure} *)

(* Allow for different implementations (symbolic and numeric) of the
   coefficient ring.  *)

module type Ring =
  sig
    type t
    val null : t
    val unit : t
    val mul : t -> t -> t
    val add : t -> t -> t
    val sub : t -> t -> t
    val neg : t -> t
    val to_float : t -> float
    val to_string : t -> string
  end

module type Rational =
  sig
    include Ring
    val is_null : t -> bool
    val make : int -> int -> t
  end

(* The coefficient ring required evaluating traces
   in~$\mathrm{SU}(N_C)$ and~$\mathrm{SO}(N_C)$ is the ring
   generated by the rational numbers~$\mathbf{Q}$ and the
   atoms~$N_C$ and~$N_C^{-1}$.  It is generated almost freely,
   but we take into account that $N_C \cdot N_C^{-1} = 1$. *)

module type Coeff =
  sig
    include Ring
    val is_null : t -> bool

(* [atom p] creates the power~$N_C^{p}$ and [coef n d] creates
   the rational coefficient~$n/d$.  All possible coefficients can
   be generated by ring operations from these two.*)
    val atom : int -> t
    val coeff : int -> int -> t
  end

(* The [Sum] signature describes a variant of rings.  The main difference
   is that ['a Sum.t] is polymorphic.  One could think of using functors
   to implement a monomorphic [Sum.t], but this would make dealing with
   [Sum.map] much harder.  Therefore, we refrain from casting [Sum] into
   the [Ring] mold. *)

module type Sum =
  sig
    module C : Coeff
    type 'a t
    val zero : 'a t
    val atom : 'a -> 'a t
    val scale : C.t -> 'a t -> 'a t
    val add : 'a t -> 'a t -> 'a t
    val sub : 'a t -> 'a t -> 'a t
    val mul : ('a -> 'b -> 'c) -> 'a t -> 'b t -> 'c t
    val mulx : ('a -> 'b -> 'c t) -> 'a t -> 'b t -> 'c t -> 'c t
    val mulc : ('a -> 'b -> C.t) -> 'a t -> 'b t -> C.t -> C.t
    val map : ('a -> 'b) -> 'a t -> 'b t
    val eval : ('a -> C.t) -> 'a t -> C.t
    val to_string : ('a -> string) -> 'a t -> string
    val terms : 'a t -> 'a list
  end

(* \thocwmodulesection{Color Flow Representation} *)

module type Flows =
  sig
    module C : Coeff
    type 'a t
    type 'a wf
    val to_string : ('a -> string) -> 'a t -> string

(* [of_amplitude root a] calculates the sum of color flows
   corresponding to amplitude [a] with [root] as label for
   the particle at the root. *)
    val of_amplitude : 'e -> ('a, 'e) amplitude -> 'e t

(* [square flip a1 a2] calculates the product of the colorflows
   [a1] and [a2], where the duplicate gluon labels are flipped
   by [flip]. *)
    val square : ('a -> 'a) -> 'a t -> 'a t -> 'a t
    val eval : 'a t -> C.t
    val eval_square : ('a -> 'a) -> 'a t -> 'a t -> C.t

    type 'a hash
    val make_hash : unit -> 'a hash
    val eval_memoized : 'a hash -> 'a t -> C.t
    val eval_square_memoized : 'a hash -> ('a -> 'a) -> 'a t -> 'a t -> C.t

  end

module Make_Flows (S : Sum) : Flows with module C = S.C

module Flows : Flows

(*i
(* \thocwmodulesection{Traces}
   \begin{dubious}
     Strategy for constructing the trace corresponding to the color sum
     for a pair of a diagram and a conjugated diagram:
     \begin{enumerate}
       \item represent each amplitude by a set of strings
         of~$T^{a}$-matrices. Each string is labeled by a pair
         consisting of the incoming and outgoing momentum.  External
         gluons are also labeled by the momenta.  It is probably
         easiest to use momenta also as summation indices, but there
         are two caveats:
         \begin{enumerate}
           \item make sure that~$p$ and~$-p$ are not confused
           \item allow to tag the indices additionally for the
             conjugate diagram, because the same momentum can
             appear twice (and will certainly for the squares
             on the diagonal).
         \end{enumerate}
      \item combine the open strings of~$T^{a}$-matrices at matching
         momenta to color traces and evaluate them.
     \end{enumerate}
   \end{dubious} *)


(* We're following in general the procedure of~\cite{Cvi76}. *)

module type Traces =
  sig
    type index
    type term
    type coeff
    val make : index list -> term
    val make_term : index list list -> term
    val mul : term -> term -> term
    val eval : term -> coeff
    val format : term -> string
    val to_string : coeff -> string
    val to_float : coeff -> float
  end

module SU3_Traces : Traces with type index = int

module SU3_Traces_Float : Traces with type index = int
module SU3_Traces_Small_Rational : Traces with type index = int
module SU3_Traces_Small_Symbolic : Traces with type index = int
module SU3_Traces_Rational : Traces with type index = int
module SU3_Traces_Symbolic : Traces with type index = int

i*)

(*i
 *  Local Variables:
 *  mode:caml
 *  indent-tabs-mode:nil
 *  page-delimiter:"^(\\* .*\n"
 *  End:
i*)
