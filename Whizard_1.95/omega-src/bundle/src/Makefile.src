# $Id: Makefile.src 796 2006-05-15 12:52:32Z  $
# WHIZARD 1.94 Fri Sep 11 2009
# 
# (C) 1999-2009 by 
#     Wolfgang Kilian <kilian@hep.physik.uni-siegen.de>
#     Thorsten Ohl <ohl@physik.uni-wuerzburg.de>
#     Juergen Reuter <juergen.reuter@physik.uni-freiburg.de>
#     with contributions by Sebastian Schmidt, Christian Speckner
#
# WHIZARD is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# WHIZARD is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#
########################################################################
#
# This Makefile does NOT pretent to be portable and requires GNU make.
# GNU make is the native make for Linux systems and available on most
# other systems as `gmake'
#
########################################################################
#
# All source files for library modules:
#   * must be in the correct sequence for linking
#   * foo.mll or foo.mly imply foo.ml and foo.mli
#   * foo.ml as a source file implies foo.mli as a source files
#
########################################################################

ifneq ($(SELECT_PROGRAMS_DEVELOPERS),)
VERTEX_MODULES = vertex_syntax.ml vertex_lexer.mll vertex_parser.mly vertex.ml
MODEL_MODULES = model_syntax.ml model_lexer.mll model_parser.mly model_file.ml
else
VERTEX_MODULES = 
MODEL_MODULES = 
endif

MODULES = \
	pmap.ml thoList.ml thoArray.ml thoString.ml rCS.ml \
	cache.ml trie.ml linalg.ml \
	algebra.ml options.ml product.ml combinatorics.ml partition.ml tree.ml \
	tuple.ml topology.ml dAG.ml momentum.ml phasespace.ml \
	complex.ml color.ml \
	cascade_syntax.ml cascade_lexer.mll cascade_parser.mly cascade.ml \
	fusion.ml coupling.mli target.mli targets.ml \
	$(VERTEX_MODULES) \
	model.mli models.ml models2.ml models3.ml models4.ml models5.ml \
	oVM.ml whizard.ml \
	comphep_syntax.ml comphep_lexer.mll comphep_parser.mly comphep.ml \
	$(MODEL_MODULES) colorize.ml \
	omega_syntax.ml omega_lexer.mll omega_parser.mly omega.ml

GUI_MODULES = \
	thoGButton.ml thoGWindow.ml thoGMenu.ml thoGDraw.ml

########################################################################
#
# All source files for executable programs:
#   * .ml will be appended
#
########################################################################

########################################################################
# Programs for the general public:
########################################################################

# Released models: QED, SM (+ some anomalous couplings), MSSM

PROGRAMS_released = f90_QED \
	f90_SM f90_SM_CKM \
	f90_SM_ac f90_SM_ac_CKM \
	f90_MSSM f90_MSSM_CKM \
	f90_NMSSM f90_NMSSM_CKM f90_PSSSM \
	f90_MSSM_Grav \
	f90_Littlest f90_Littlest_Eta \
	f90_Littlest_Tpar \
	f90_Simplest f90_Simplest_univ \
	f90_Xdim f90_GravTest \
	f90_SM_km f90_UED f90_Zprime \
	f90_Template
 
# Colorized for WHiZard
PROGRAMS_released += \
	f90_QED_Col f90_QCD_Col \
	f90_SM_Col f90_SM_CKM_Col \
	f90_SM_ac_Col f90_SM_ac_CKM_Col \
	f90_MSSM_Col f90_MSSM_CKM_Col \
	f90_MSSM_Grav_Col \
	f90_NMSSM_Col f90_NMSSM_CKM_Col f90_PSSSM_Col \
	f90_Littlest_Col f90_Littlest_Eta_Col \
	f90_Littlest_Tpar_Col \
	f90_Simplest_Col f90_Simplest_univ_Col \
	f90_Xdim_Col f90_SM_km_Col \
	f90_UED_Col f90_GravTest_Col \
	f90_Zprime_Col f90_Template_Col 


# Self tests:
PROGRAMS_tests = f90_SM_clones count

########################################################################
# Programs for advanced users:
########################################################################

# Not tested comprehensively:
PROGRAMS_unreleased = f90_SM_Rxi f90_2HDM f90_CQED f90_QCD_EW_Col

# Theoretical Models:
PROGRAMS_theoretical = f90_Phi3 f90_Phi3h f90_Phi4 f90_Phi4h

# Alternative implementations of released models:
PROGRAMS_redundant = f90Maj_SM f90_SMh

########################################################################
# Programs for developers:
########################################################################

# Delevopment tools:
PROGRAMS_delevopment = test_linalg whizard_tool model_file

# Known to be incomplete:
PROGRAMS_incomplete = f90_Comphep ovm_SM

# Known to be unphysical:
# PROGRAMS_unphysical = f90_QCD

########################################################################
# Obsolete Programs:
########################################################################

# The old Standard Model with auxiliary fields
PROGRAMS_obsolete = f90_SM3 f90_SM3_ac f90_SM3_clones 
PROGRAMS_obsolete += f90Maj_SM3 f90_SM3h

# The old HELAS backend (incomplete!):
PROGRAMS_obsolete += helas_QED helas_QCD helas_SM

########################################################################

PROGRAMS_public = \
	$(PROGRAMS_released) \
	$(PROGRAMS_tests) \
	$(PROGRAMS_unreleased) \
	$(PROGRAMS_theoretical) \
	$(PROGRAMS_redundant)

PROGRAMS_private = \
	$(PROGRAMS_not_public) \
	$(PROGRAMS_delevopment) \
	$(PROGRAMS_incomplete) \
	$(PROGRAMS_unphysical) \
	$(PROGRAMS_obsolete)

########################################################################

PROGRAMS :=
GUI_PROGRAMS :=

########################################################################

ifneq ($(SELECT_PROGRAMS_CUSTOM),)

PROGRAMS := $(SELECT_PROGRAMS_CUSTOM)

else

ifneq ($(SELECT_PROGRAMS_RELEASED),)
  PROGRAMS += $(PROGRAMS_released) $(PROGRAMS_tests)
endif

ifneq ($(SELECT_PROGRAMS_UNRELEASED),)
  PROGRAMS += $(PROGRAMS_unreleased)
endif

ifneq ($(SELECT_PROGRAMS_THEORETICAL),)
  PROGRAMS += $(PROGRAMS_theoretical)
endif

ifneq ($(SELECT_PROGRAMS_REDUNDANT),)
  PROGRAMS += $(PROGRAMS_redundant)
endif

ifneq ($(SELECT_PROGRAMS_DEVELOPERS),)
  PROGRAMS += \
	$(PROGRAMS_not_public) \
	$(PROGRAMS_delevopment) \
	$(PROGRAMS_incomplete) \
	$(PROGRAMS_unphysical)
endif

ifneq ($(SELECT_PROGRAMS_OBSOLETE),)
  PROGRAMS += $(PROGRAMS_obsolete)
endif

endif

########################################################################

ifneq ($(SELECT_PROGRAMS_GUI),)
  GUI_PROGRAMS := ogiga
endif

########################################################################
#
# Primary files (sources):
#
########################################################################

LIB_SRC_ML = $(filter %.ml,$(MODULES))
LIB_SRC_MLI = $(filter %.mli,$(MODULES)) $(LIB_SRC_ML:.ml=.mli)
LIB_SRC_MLL = $(filter %.mll,$(MODULES))
LIB_SRC_MLY = $(filter %.mly,$(MODULES))

GUI_LIB_SRC_ML = $(filter %.ml,$(GUI_MODULES))
GUI_LIB_SRC_MLI = $(filter %.mli,$(GUI_MODULES)) $(GUI_LIB_SRC_ML:.ml=.mli)
GUI_LIB_SRC_MLL = $(filter %.mll,$(GUI_MODULES))
GUI_LIB_SRC_MLY = $(filter %.mly,$(GUI_MODULES))

APP_ML = $(addsuffix .ml,$(PROGRAMS))
GUI_APP_ML = $(addsuffix .ml,$(GUI_PROGRAMS))

SRC_ML = $(LIB_SRC_ML) $(GUI_LIB_SRC_ML) $(APP_ML) $(GUI_APP_ML)
SRC_MLI = $(LIB_SRC_MLI) $(GUI_LIB_SRC_MLI)
SRC_MLL = $(LIB_SRC_MLL) $(GUI_LIB_SRC_MLL)
SRC_MLY = $(LIB_SRC_MLY) $(GUI_LIB_SRC_MLY)

SOURCE_OCAML = $(SRC_ML) $(SRC_MLI) $(SRC_MLL) $(SRC_MLY)

APP_ML_public = $(addsuffix .ml,$(PROGRAMS_public))
APP_ML_private = $(addsuffix .ml,$(PROGRAMS_private))
APP_ML_all = $(APP_ML_public) $(APP_ML_private)

SRC_ML_public = $(LIB_SRC_ML) $(GUI_LIB_SRC_ML) $(APP_ML_public) $(GUI_APP_ML)
SRC_ML_private = $(APP_ML_private)
SRC_ML_all = $(SRC_ML_public) $(SRC_ML_private)

SOURCE_OCAML_public = $(SRC_ML_public) $(SRC_MLI) $(SRC_MLL) $(SRC_MLY)
SOURCE_OCAML_private = $(SRC_ML_private)
SOURCE_OCAML_all = $(SOURCE_OCAML_public) $(SOURCE_OCAML_private)

########################################################################
#
# Derived files:
#
########################################################################

LIB_ML = $(filter %.ml,$(patsubst %.mll,%.ml,$(patsubst %.mly,%.ml,$(MODULES))))
LIB_MLI = $(filter %.mli,$(patsubst %.mly,%.mli,$(patsubst %.ml,%.mli,$(MODULES))))
DERIVED_ML = $(filter-out $(LIB_SRC_ML),$(LIB_ML))
DERIVED_MLI = $(filter-out $(LIB_SRC_MLI),$(LIB_MLI))

GUI_LIB_ML = $(filter %.ml,$(patsubst %.mll,%.ml,$(patsubst %.mly,%.ml,$(GUI_MODULES))))
GUI_LIB_MLI = $(filter %.mli,$(patsubst %.mly,%.mli,$(patsubst %.ml,%.mli,$(GUI_MODULES))))
GUI_DERIVED_ML = $(filter-out $(GUI_LIB_SRC_ML),$(GUI_LIB_ML))
GUI_DERIVED_MLI = $(filter-out $(GUI_LIB_SRC_MLI),$(GUI_LIB_MLI))

DERIVED_OCAML = $(DERIVED_ML) $(DERIVED_MLI) $(GUI_DERIVED_ML) $(GUI_DERIVED_MLI)

LIB_CMI = $(LIB_MLI:.mli=.cmi)
LIB_CMO = $(LIB_ML:.ml=.cmo)
LIB_CMX = $(LIB_ML:.ml=.cmx)

APP_BIN = $(addsuffix .bin,$(PROGRAMS))
APP_OPT = $(addsuffix .opt,$(PROGRAMS))
APP_CMO = $(addsuffix .cmo,$(PROGRAMS))
APP_CMX = $(addsuffix .cmx,$(PROGRAMS))

ifeq ($(SELECT_PROGRAMS_GUI),yes)
  GUI_LIB_CMI = $(GUI_LIB_MLI:.mli=.cmi)
  GUI_LIB_CMO = $(GUI_LIB_ML:.ml=.cmo)
  GUI_LIB_CMX = $(GUI_LIB_ML:.ml=.cmx)
  GUI_APP_BIN = $(addsuffix .bin,$(GUI_PROGRAMS))
  GUI_APP_OPT = $(addsuffix .opt,$(GUI_PROGRAMS))
  GUI_APP_CMO = $(addsuffix .cmo,$(GUI_PROGRAMS))
  GUI_APP_CMX = $(addsuffix .cmx,$(GUI_PROGRAMS))
endif

INTERFACES = $(SRC_MLI:.mli=.interface)
IMPLEMENTATIONS = $(SRC_ML_all:.ml=.implementation) \
	$(SRC_MLL:.mll=.implementation) $(SRC_MLY:.mly=.implementation)

DERIVED_TEX = index.tex $(INTERFACES) $(IMPLEMENTATIONS) omegalib.tex

DAGS = bhabha0.eps bhabha.eps \
	epemudbardubar0.eps epemudbardubar.eps \
	epemudbarmunumubar0.eps epemudbarmunumubar.eps

########################################################################
#
# Fortran90/95/03
#
########################################################################

########################################################################
# derived straightforwardly from omegalib.nw
########################################################################

FC_LIBSRC_FROM_OMEGALIB_NW_public := \
	omega_kinds.f90 omega_constants.f90 omega_spinors.f90 \
	omega_bispinors.f90 omega_vectorspinors.f90 omega_vectors.f90 \
	omega_couplings.f90 omega_polarizations.f90 omega_polarizations_madgraph.f90 \
	omega_tensors.f90 omega_tensor_polarizations.f90 \
	omega_vspinor_polarizations.f90 \
	omega_spinor_couplings.f90 omega_bispinor_couplings.f90 \
	omega_utils.f90 omega95.f90 omega95_bispinors.f90 \
	omega_parameters.f90 omega_parameters_madgraph.f90 

# The unfinished O'Mega virtual machine
# (don't build it by default, because some compilers trip over it!)
FC_LIBSRC_FROM_OMEGALIB_NW_private := \
	omegavm95.f90

FC_LIBSRC_public := $(FC_LIBSRC_FROM_OMEGALIB_NW_public)
FC_LIBSRC_private := $(FC_LIBSRC_FROM_OMEGALIB_NW_private)
	
########################################################################
# derived from other files or using preprocessors
########################################################################
	 
# tho's unreleased code
FC_LIBSRC_private += omega_spinor_colors.f90 omega_bispinor_colors.f90

########################################################################

FC_LIBSRC := $(FC_LIBSRC_public)
FC_LIBSRC_FROM_OMEGALIB_NW := $(FC_LIBSRC_FROM_OMEGALIB_NW_public)

ifneq ($(SELECT_PROGRAMS_DEVELOPERS),)
  FC_LIBSRC += $(FC_LIBSRC_private)
  FC_LIBSRC_FROM_OMEGALIB_NW += $(FC_LIBSRC_FROM_OMEGALIB_NW_private)
endif

########################################################################

FC_TSTLIBSRC = omega_testtools.f90
FC_TSTSRC = test_omega95.f90 test_omega95_bispinors.f90

FC_LIBOBJ = $(FC_LIBSRC:.f90=.o)
FC_LIBOBJP = $(FC_LIBSRC:.f90=_p.o)
FC_TSTLIBOBJ = $(FC_TSTLIBSRC:.f90=.o)
FC_TSTLIBOBJP = $(FC_TSTLIBSRC:.f90=_p.o)
FC_TSTOBJ = $(FC_TSTSRC:.f90=.o)
FC_TSTOBJP = $(FC_TSTSRC:.f90=_p.o)

########################################################################


