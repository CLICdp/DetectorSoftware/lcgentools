(* $Id: cache.ml 68 2007-11-22 11:11:19Z ohl $ *)
(* Copyright (C) 2005 by Thorsten Ohl <ohl@physik.uni-wuerzburg.de> et al.
   O'Mega is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by 
   the Free Software Foundation; either version 2, or (at your option)
   any later version.
   O'Mega is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.  *)

let directory = ref Filename.current_dir_name
let prefix = ref "omega_cache_"

let set_directory d =
  directory := d

let set_prefix p =
  prefix := p

type hash = string

let md5_hash value =
  Digest.to_hex (Digest.string (Marshal.to_string value []))

let file ?(dir = !directory) ?(pfx = !prefix) hash =
  Filename.concat dir (pfx ^ hash)

let write ?dir ?pfx hash value =
  let oc = open_out_bin (file ?dir ?pfx hash) in
  Marshal.to_channel oc value [];
  close_out oc

let read ?dir ?pfx hash =
  try
    let ic = open_in_bin (file ?dir ?pfx hash) in
    let value = Marshal.from_channel ic in
    close_in ic;
    value
  with
  | _ -> raise Not_found
      
let maybe_read ?dir ?pfx hash =
  try Some (read ?dir ?pfx hash) with Not_found -> None
      
(*i
 *  Local Variables:
 *  mode:caml
 *  indent-tabs-mode:nil
 *  page-delimiter:"^(\\* .*\n"
 *  End:
i*)





