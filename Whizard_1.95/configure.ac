dnl WHIZARD configuration
dnl
dnl Process this file with autoconf to produce a configure script.
dnl ************************************************************************
dnl Environment variables.
dnl Set these to a value only if you need to override configure defaults.
dnl
dnl Mandatory tools:
dnl   GMAKE                [GNU Make for steering]
dnl   PERL                 [Perl interpreter for scripts]
dnl   FC                   [Fortran 95/03 compiler, needed for main program]
dnl
dnl Directories to search for optional external packages:
dnl   LHAPDF_DIR           [LHAPDF structure function library]
dnl   CERNLIB_DIR          [CERN libraries, default for the following:]
dnl   PDFLIB_DIR           [PDF library (structure functions)]
dnl   STDHEP_DIR           [STDHEP library (portable binary I/O)]
dnl   PYTHIA_DIR           [PYTHIA library (Monte-Carlo, fragmentation etc.)]
dnl   HERWIG_DIR           [HERWIG library (ditto)]
dnl
dnl Optional tools:
dnl   FC_EXTRA             [Fortran compiler for precompiled external libs]
dnl                        [(to determine proper runtime libraries)]
dnl   GTAR                 [GNU TAR for 'restricted bundle']
dnl   MPOST                [METAPOST and LaTeX for drawing]
dnl   LATEX 
dnl   DVIPS 
dnl   PS2PDF
dnl   NOWEBDIR             [path to 'notangle', 'noweave', 'cpif']
dnl   HEVEA                [path to 'hevea', 'hacha']
dnl
dnl User options:
dnl   TMPDIR     (/tmp)    [workspace for the matrix element generator(s)]
dnl   FC_PRECISION (double) [default precision: single|double|quadruple]
dnl   CHEP_MAXDIAG (100)   [maximum number of diagrams per file (comphep only)]
dnl   FCFLAGS              [flags for the f95/03 compiler]
dnl   FCFLAGS_ME           [flags for the matrix element code]
dnl                        [(overrides FCFLAGS if set)]
dnl   USERLIBS             [flags for the linker, e.g., for user libs]
dnl   SYSTEMLIBS           [flags for the linker, e.g., for system libs]
dnl
dnl ************************************************************************

dnl Start
AC_INIT([WHIZARD], [01.95 Feb 25 2010])

dnl Autoconf version requirement
AC_PREREQ([2.60])

WK_CONFIGURE_SECTION([Start of package configuration])

AC_MSG_NOTICE([Package name: AC_PACKAGE_NAME()])
AC_MSG_NOTICE([Version:      AC_PACKAGE_VERSION()])

if test "$PACKAGE_STRING" != "`cat VERSION`"; then
AC_MSG_ERROR([Package version mismatch (check file 'VERSION')])
fi

WK_CONFIGURE_SECTION([Initialization and sanity checks])

dnl Read in user defaults (in addition to the ones in CONFIG_SITE, if set)
if test -r config.site; then
AC_MSG_NOTICE([loading site-specific configure script config.site])
. ./config.site
fi

dnl This is activated for the restricted bundle
dnl If RESTRICTED is set, certain tests and messages are omitted
dnl and the Makefiles depend on this parameter
dnl###define(WHIZARD_RESTRICTED)
ifdef([WHIZARD_RESTRICTED], [RESTRICTED=yes])
AC_SUBST(RESTRICTED)

dnl ------------------------------------------------------------------------
dnl Packages for matrix elements

ifdef([WHIZARD_RESTRICTED],
dnl####with_omega#[WK_WHIZARD_PACKAGE(OMEGA, omega, Makefile)]
dnl####without_omega#[WK_WHIZARD_SET(OMEGA, no)]
,
[WK_WHIZARD_ME_GENERATOR_ENABLE(OMEGA, omega, 
[  --enable-omega          enable O'Mega as matrix element generator [[yes]]],
Makefile.in, Makefile naive-color.pl insert-color.pl)]
)
if test "$OMEGA" != "no"; then
WITH_OMEGA=-lomega
fi
AC_SUBST(WITH_OMEGA)

dnl Edit and uncomment this for adding more packages:
dnl ifdef([WHIZARD_RESTRICTED], ,[
dnl WK_WHIZARD_ME_GENERATOR(MY_PACKAGE, my-package, Makefile)
dnl ])


ifdef([WHIZARD_RESTRICTED], 
[WK_WHIZARD_SET(CHSRC, no)],
[WK_WHIZARD_ME_GENERATOR_DISABLE(CHSRC, chep, 
[  --enable-chep           enable CompHEP as matrix element generator [[no]]],
unix_com/comphep, unix_com/c_compile Makefile)
])


ifdef([WHIZARD_RESTRICTED], 
[WK_WHIZARD_SET(MADGRAPH, no)]
dnl####with_helas#[WK_WHIZARD_PACKAGE(HELAS, helas, Makefile)]
dnl####without_helas#[WK_WHIZARD_SET(HELAS, no)]
,
[WK_WHIZARD_ME_GENERATOR_DISABLE(MADGRAPH, mad, 
[  --enable-mad            enable Madgraph as matrix element generator [[no]]],
driver.f, Makefile)
# Enable HELAS as well if Madgraph is enabled
if test "$MADGRAPH" != "no"; then
WK_WHIZARD_PACKAGE(HELAS, helas, Makefile)
WITH_HELAS=-ldhelas
else
WK_WHIZARD_SET(HELAS, no)
fi
])
AC_SUBST(WITH_HELAS)

dnl ------------------------------------------------------------------------
dnl Beamstrahlung

ifdef([WHIZARD_RESTRICTED],
dnl####with_circe#[WK_WHIZARD_PACKAGE_ENABLE(CIRCE, circe,
dnl####with_circe#[  --enable-circe          enable CIRCE v1 for beamstrahlung spectra [[yes]]],
dnl####with_circe#circe.f, Makefile)]
dnl####without_circe#[WK_WHIZARD_SET(CIRCE, no)]
,
[WK_WHIZARD_PACKAGE_ENABLE(CIRCE, circe,
[  --enable-circe          enable CIRCE v1 for beamstrahlung spectra [[yes]]],
circe.f, Makefile)])

ifdef([WHIZARD_RESTRICTED],
dnl####with_circe2#[WK_WHIZARD_PACKAGE_ENABLE(CIRCE2, circe2,
dnl####with_circe2#[  --enable-circe2         enable CIRCE v2 for beamstrahlung spectra [[yes]]],
dnl####with_circe2#circe2.f, Makefile)]
dnl####without_circe2#[WK_WHIZARD_SET(CIRCE2, no)]
,
[WK_WHIZARD_PACKAGE_ENABLE(CIRCE2, circe2,
[  --enable-circe2         enable CIRCE v2 for beamstrahlung spectra [[yes]]],
circe2.f, Makefile)])

dnl ------------------------------------------------------------------------
dnl Global variables (directories and program names)

dnl The main directory
PWD=`pwd`
WK_WHIZARD_SET(SRCDIR, $PWD)


dnl Scripts and files
WK_WHIZARD_SET(MAKEPROC, make-proc.pl)
WK_WHIZARD_SET(MAKEINCLUDE, make-include.pl)
WK_WHIZARD_SET(F77TOF90, f77tof90.pl)
WK_WHIZARD_SET(F77TOF90_SIMPLE, f90contline.pl)
WK_WHIZARD_SET(MAKEDEPEND, make-depend)
WK_WHIZARD_SET(MAKESELFILE, make-selfile.pl)
WK_WHIZARD_SET(MAKEPARFILE, make-parfile.pl)
WK_WHIZARD_SET(MAKETEST, fulltest.pl)
WK_WHIZARD_SET(CONFIG, whizard.prc)


dnl Where the executables are.  In the restricted bundle, not all are present.
dnl The listed files are to be preprocessed by configure
ifdef([WHIZARD_RESTRICTED],
[WK_WHIZARD_SUBDIR(BIN, bin, Makefile, $F77TOF90 $MAKETEST)],
[WK_WHIZARD_SUBDIR(BIN, bin, Makefile, $F77TOF90 $MAKETEST $MAKEPROC $F77TOF90_SIMPLE $MAKEPARFILE $MAKESELFILE $MAKEINCLUDE)]
)
WK_WHIZARD_SET(BINDIR, $SRCDIR/$BIN)


dnl Where the libraries generated by WHIZARD are stored
WK_WHIZARD_SUBDIR(LIB, lib)
WK_WHIZARD_SET(LIBDIR, $SRCDIR/$LIB)

dnl Where the modules and include files are stored
WK_WHIZARD_SUBDIR(MOD, include)


dnl Directories where programs needed for WHIZARD are located
WK_WHIZARD_PACKAGE(KINDS, kinds, Makefile kinds)
WK_WHIZARD_PACKAGE(VAMP, vamp, Makefile)
WK_WHIZARD_PACKAGE(WCODE, whizard, Makefile)

dnl Here will the generated matrix element code be located
ifdef([WHIZARD_RESTRICTED],
[WK_WHIZARD_PACKAGE(CHCODE, processes, Makefile)],
[WK_WHIZARD_PACKAGE(CHCODE, processes)]
)

dnl The configuration files
WK_WHIZARD_SUBDIR(CONFSRC, conf)

dnl The calculation results
WK_WHIZARD_SUBDIR(RESULTS, results, Makefile)


dnl Plotting package GAMELAN
WK_WHIZARD_PACKAGE(GMLSRC, gml, Makefile, gml)
WK_WHIZARD_SET(GMLDIR, $SRCDIR/$GMLSRC)

dnl FeynMF package
WK_WHIZARD_PACKAGE(FMFSRC, fmf)
WK_WHIZARD_SET(FMFDIR, $SRCDIR/$FMFSRC)

dnl Documentation
WK_WHIZARD_SUBDIR(DOC, doc, Makefile)

dnl The main program
WK_WHIZARD_SET(PRG, whizard)
OUTFILES="$OUTFILES Makefile"

dnl Temporary workspace for the matrix element generators.
dnl (This we don't need in restricted mode)
ifdef([WHIZARD_RESTRICTED], ,
[AC_CACHE_CHECK([for tmp directory], [wk_cv_tmpdir],
[ac_tmpdir_path="$TMPDIR /tmp /var/tmp"
for ac_dummy in $ac_tmpdir_path; do
  if test -d $ac_dummy -a -w $ac_dummy; then
    wk_cv_tmpdir=$ac_dummy
    break
  fi
done
])
TMPDIR=$wk_cv_tmpdir
test -z "$TMPDIR" && AC_MSG_ERROR([No writable directory found in $ac_tmpdir_path])
AC_SUBST(TMPDIR)
])


dnl C Preprocessor macros (for use in the CompHEP compilation):
test -n "$CHEP_MAXDIAG" && AC_DEFINE_UNQUOTED(F90_MAXDIAG, $CHEP_MAXDIAG)

dnl disable 'pure' routines upon request, to make a debugging version:
AC_ARG_ENABLE(fc_impure,
[  --enable-fc-impure      remove PURE qualifiers, even if supported [[no]]])
FC_IMPURE=${enable_fc_impure:-no}
AC_SUBST(FC_IMPURE)

########################################################################
# NOWEB
########################################################################

AC_ARG_ENABLE([noweb],
[  --disable-noweb         Disable the noweb programs, even if available.],
[case "$enableval" in
  no) AC_MSG_WARN([Disabling NOWEB by request.])
      NOWEAVE=false
      NOTANGLE=false
      ;;
esac])
dnl

dnl ************************************************************************
dnl ************************************************************************
dnl Checks

WK_CONFIGURE_SECTION([Check generic programs and tools])

dnl ************************************************************************
dnl Checks for programs.

AC_PROG_AWK

WK_PROG_GNU_MAKE()
if test "$GMAKE" = "no"; then
  AC_MSG_WARN([*** GNU Make not found.  The Makefiles will probably not run correctly. ***])
elif test "$GMAKE" != make; then
  AC_MSG_WARN([*** Use GNU Make ($GMAKE) for compilation. ***])
fi

if test -z "$PERL"; then
  AC_PATH_PROG(PERL,perl)
  test -z "$PERL" && AC_MSG_ERROR([perl not found in \$PATH])
fi
test -x "$PERL" || AC_MSG_ERROR([Can't execute $PERL])

WK_PROG_GNU_TAR()
if test "$GTAR" = "no"; then
  AC_MSG_WARN([*** GNU tar not found.  Tarring will not work. ***])
else
  TAR="$GTAR"
fi
AC_SUBST(TAR)

AC_PROG_RANLIB()
test $RANLIB = ranlib || AC_MSG_WARN([*** ranlib not found.  Linking problems may result. ***])

AC_PROG_LN_S

ifdef([WHIZARD_RESTRICTED],
[WK_WHIZARD_SET(AUTOCONF,no)],
[AC_CHECK_PROG(AUTOCONF,autoconf,autoconf,no)
if test "$AUTOCONF" = "no"; then
  AC_MSG_WARN([*** autoconf not found.  Making a restricted bundle is not possible. ***])
fi
])

dnl ------------------------------------------------------------------------
dnl Check for NOWEB programs; the system should, however, work without them.
test -n "$NOWEB_DIR" && NOWEBDIR=$NOWEB_DIR
if test -n "$NOWEBDIR"; then
  NOTANGLE=$NOWEBDIR/notangle
  CPIF=$NOWEBDIR/cpif
  NOWEAVE=$NOWEBDIR/noweave
fi

if test -z "$NOWEAVE"; then
  AC_PATH_PROG(NOWEAVE,noweave)
fi
if test -n "$NOWEAVE"; then
  test -x "$NOWEAVE" || AC_MSG_ERROR([Can't execute $NOWEAVE])
fi

if test -z "$NOTANGLE"; then
  AC_PATH_PROG(NOTANGLE,notangle)
fi
if test -n "$NOTANGLE"; then
  test -x "$NOTANGLE" || AC_MSG_ERROR([Can't execute $NOTANGLE.])
fi

if test -z "$CPIF"; then
  AC_PATH_PROG(CPIF,cpif)
fi
if test -n "$CPIF"; then
  test -x "$CPIF" || AC_MSG_ERROR([Can't execute $CPIF])
else
  CPIF="$BINDIR/cat"
fi

AC_PATH_PROG(M4,m4,false)

dnl ------------------------------------------------------------------------
dnl Check for working METAPOST installation
dnl latex and dvips are needed for histograms (and documentation)

AC_PATH_PROG(LATEX,latex)
test -z "$LATEX" && AC_MSG_WARN([*** latex not found in \$PATH])

AC_PATH_PROG(DVIPS,dvips)
test -z "$DVIPS" && AC_MSG_WARN([*** dvips not found in \$PATH])

dnl These are needed for documentation only, no warnings here
AC_PATH_PROG(GHOSTVIEW,gv ghostview,false)
AC_PATH_PROG(EPSTOPDF,epstopdf,false)
AC_PATH_PROG(PS2PDF,ps2pdf)
AC_PATH_PROG(PDFLATEX,pdflatex)
AC_PATH_PROG(ACROREAD,acroread,false)
AC_PATH_PROG(DOT,dot,false)
AC_PATH_PROG(HEVEA,hevea)
AC_PATH_PROG(IMAGEN,imagen)
AC_PATH_PROG(HACHA,hacha)
AC_PATH_PROG(GZIP,gzip,false)

AC_ARG_ENABLE(metapost,
[  --enable-metapost       use Metapost/gamelan for plotting histograms [[yes]]])
if test "$enable_metapost" = "no"; then
AC_MSG_CHECKING([for mpost])
AC_MSG_RESULT([(disabled)])
unset MPOST
else
if test -z "$MPOST"; then
  AC_PATH_PROGS(MPOST,mpost mp)  
fi
fi

if test -n "$MPOST"; then
  WK_PROG_MPOST_WORKS()
fi
AC_SUBST(MPOST)

dnl Without latex and dvips, metapost is useless for us
test -z "$LATEX" -o -z "$DVIPS" && unset MPOST 


dnl ************************************************************************
dnl Checks for compilers

dnl ------------------------------------------------------------------------
dnl Check the Fortran Compiler

WK_CONFIGURE_SECTION([Check Fortran 90/95/2003 compiler])

dnl First select a compiler using the standard autoconf macro.
dnl The environment variable FC may be used to override the default.
dnl FCFLAGS is also recognized
dnl (compatibility: recognize also F03/F95/F90 and F95FLAGS/F90FLAGS)
FC=${FC:-$F95}
FCFLAGS=${FCFLAGS:-$F95FLAGS}
FC=${FC:-$F90}
FCFLAGS=${FCFLAGS:-$F90FLAGS}

if test -n "$FC"; then
AC_MSG_CHECKING([$FC])
AC_MSG_RESULT([(requested Fortran compiler)])
fi

WK_PROG_FC_F95()
FCFLAGS="$FCFLAGS $FCFLAGS_f90"

THO_FORTRAN_FIND_EXTENSION([FC_EXT], [$FC], [f95 f90])

WK_FC_GET_VENDOR()


########################################################################
# Vendor-specific switches that can't be guessed ...
########################################################################

AC_SUBST([FC_PROF])
AC_SUBST([FC_MDIR])
AC_SUBST([FC_MDIR_NOSPACE])
AC_SUBST([FC_WIDE])
AC_SUBST([FC_DUSTY])

case "$FC_VENDOR" in

  Intel)

    FC_IFC_VERSION=$FC_MAJOR_VERSION
    AC_SUBST([FC_IFC_VERSION])
    if test "$FC_IFC_VERSION" -lt 7; then
      AC_MSG_ERROR([versions before 7.0 of the Intel Fortran compiler dnl
are not supported, because they are too old and buggy.])
    fi

    THO_FORTRAN_FIND_OPTION([FC_OPT], [$FC], [$FC_EXT], [-O3 -O])
    THO_FORTRAN_FILTER_OPTIONS([FC_OPT], [$FC], [$FC_EXT], [-u])
    THO_FORTRAN_FIND_OPTION([FC_PROF], [$FC], [$FC_EXT], [-p])

    if test "$FC_IFC_VERSION" -ge 8; then
      FC_MDIR=-module
      FC_WIDE=-132
      FC_DUSTY=-FI
    else
      FC_MDIR=
      FC_WIDE=-extend_source
      FC_DUSTY=-FI
    fi

    if test "$FC_IFC_VERSION" -eq 7; then
      if test -z "$FC_IFC_MINOR_VERSION"; then
	AC_MSG_WARN([Disabling PURE procedures, because they trigger a bug dnl
in some variants of the Intel Fortran compiler Version 7.x.])
	FC_PURE=no
      else
	if test "$FC_IFC_MINOR_VERSION" -eq 0; then
	  AC_MSG_WARN([Disabling PURE procedures, because they trigger a bug dnl
in the Intel Fortran compiler Version 7.0.])
	  FC_PURE=no
	else
	  AC_MSG_WARN([Not disabling PURE procedures by default, because your dnl
Intel Fortran compiler appears to be at least Version 7.1.])
	fi
      fi
    fi
    ;;

  Lahey)
    THO_FORTRAN_FILTER_OPTIONS([FC_OPT], [$FC], [$FC_EXT],
      [-O --tpp --nap --nchk --npca --nsav --ntrace dnl
       --fc --in --nli --quiet --warn])
    THO_FORTRAN_FIND_OPTION([FC_PROF], [$FC], [$FC_EXT], [-pg])
    FC_MDIR=
    FC_WIDE=--wide
    FC_DUSTY=--fix
    ;;

  NAG)
    THO_FORTRAN_FIND_OPTION([FC_OPT], [$FC], [$FC_EXT],
      ["-O3 -Oassumed=contig" -O3 -O])
    THO_FORTRAN_FIND_OPTION([FC_PROF], [$FC], [$FC_EXT], [-pg])
    FC_MDIR=-mdir
    FC_WIDE=-132
    FC_DUSTY="-dcfuns -fixed"
    ;;

  Compaq)
    THO_FORTRAN_FIND_OPTION([FC_OPT], [$FC], [$FC_EXT], [-O])
    THO_FORTRAN_FIND_OPTION([FC_PROF], [$FC], [$FC_EXT], [-pg])
    FC_MDIR=-module
    FC_WIDE=-132
    FC_DUSTY=-extend_source
    ;;

  Sun)
    THO_FORTRAN_FIND_OPTION([FC_OPT], [$FC], [$FC_EXT], [-O])
    THO_FORTRAN_FIND_OPTION([FC_PROF], [$FC], [$FC_EXT], [-pg])
    FC_MDIR=-moddir=
    FC_MDIR_NOSPACE=yes
    FC_WIDE=-e
    FC_DUSTY=-fixed
    ;;

  *)
    THO_FORTRAN_FIND_OPTION([FC_OPT], [$FC], [$FC_EXT], [-O])
    THO_FORTRAN_FIND_OPTION([FC_PROF], [$FC], [$FC_EXT], [-pg])
    FC_MDIR=
    FC_WIDE=-132
    FC_DUSTY="-dcfuns -fixed"
    ;;

esac

########################################################################
#
# Guess the Fortran90/95 module file name convention
#
########################################################################

THO_FORTRAN90_MODULE_FILE([FC_MODULE_NAME], [FC_MODULE_EXT], [$FC], [$FC_EXT])
THO_FILENAME_CASE_CONVERSION

AC_SUBST([FC_MAKE_MODULE_NAME])
case "$FC_MODULE_NAME" in
  module_NAME)
    FC_MAKE_MODULE_NAME='$*.$(FC_MODULE_EXT)'
    ;;
  module_name)
    FC_MAKE_MODULE_NAME='"`echo $* | $(LOWERCASE)`".$(FC_MODULE_EXT)'
    ;;
  MODULE_NAME)
    FC_MAKE_MODULE_NAME='"`echo $* | $(UPPERCASE)`".$(FC_MODULE_EXT)'
    ;;
  conftest)
    FC_MAKE_MODULE_NAME='$*.$(FC_MODULE_EXT)'
    ;;
  *)
    ;;
esac

THO_FORTRAN_TEST_PURE([FC_PURE], [$FC], [$FC_EXT])

WK_FC_CHECK_F95()

WK_FC_CHECK_TR15581()

WK_FC_CHECK_C_BINDING()

WK_FC_CHECK_CMDLINE()

WK_FC_CHECK_QUADRUPLE()

WK_FC_CHECK_PROFILING()
if test "$FC_SUPPORTS_PROFILING" = "no"; then
  if test "$enable_profiling" = "yes"; then
    AC_MSG_ERROR([Profiling requested but not available])
  else
    FC_PROFILING="no"  
  fi
else
  if test "$enable_profiling" = "yes"; then
    FC_PROFILING="yes"
    if test -n "$FCFLAGS"; then
      FCFLAGS="-pg $FCFLAGS"
    else
      FCFLAGS="-pg"
    fi
    test -n "$FCFLAGS_ME" && FCFLAGS_ME="-pg $FCFLAGS_ME"
  else
    FC_PROFILING="no"
  fi
fi
AC_SUBST(FC_PROFILING)

dnl Specific issues:
# Don't care about old ifc anymore
if test "$FC_VENDOR" = "Intel"; then
  if test "$FC_MAJOR_VERSION" -le 8; then
    AC_MSG_ERROR([Intel Compiler version is too old, please upgrade])
  fi
fi

# Make backslash character in strings normal character
# (no longer needed for Intel 8.x)
test "$FC_VENDOR" = "PGI" && FCFLAGS="$FCFLAGS -Mbackslash"

# NAG F90 option: set -maxcontin=200 if not already set
if test "$FC_VENDOR" = "NAG"; then
  if (echo $FCFLAGS | grep -qve '-maxcontin') then
    FCFLAGS="$FCFLAGS -maxcontin=200"
  fi
fi

# Lahey: link this first to avoid name clash with g77 lib [now obsolete]
#if test "$FC_VENDOR" = "Lahey"; then
#  LIBS="-lfj9i6 $LIBS"
#fi

# Special flags for the matrix element code only
test -z "$FCFLAGS_ME" && FCFLAGS_ME=$FCFLAGS
AC_SUBST(FCFLAGS_ME)

dnl ------------------------------------------------------------------------
dnl Determine runtime libraries for additional Fortran compilers

WK_FC_LIBRARY_LDFLAGS($FC_EXTRA)
LIBS="$LIBS $FCLIBS"

dnl The first compiler in this list is assumed to be the compiler
dnl used by PYTHIA (needed for I/O wrapper functions)
dnl unless F77 is set by the user
for comp in $FC_EXTRA; do
  test -z "$F77" && F77=$comp
done
AC_SUBST(F77)
AC_SUBST(F77FLAGS)

dnl ------------------------------------------------------------------------
dnl Look for the C preprocessor and set CPP

AC_PROG_CPP()

dnl ------------------------------------------------------------------------
dnl Check if we are able to compile CompHEP
dnl The F77 compiler will not be checked, not needed for WHIZARD

ifdef([WHIZARD_RESTRICTED], , [
if test "$enable_chep" != "no"; then
COMPHEP_PATH=$CHSRC/unix_com
COMPHEP_CC=CC
AC_MSG_CHECKING([the C compiler selected by CompHEP])
AC_MSG_RESULT([($COMPHEP_PATH/$COMPHEP_CC)])
WK_PROG_COMPHEP_CC_WORKS()
fi
])

dnl ************************************************************************
dnl Checks for libraries.

WK_CONFIGURE_SECTION([Check libraries])

dnl ------------------------------------------------------------------------
dnl Possible locations of the CERNLIB

CERNLIBPATH=$CERNLIB_DIR:$CERNLIBDIR
test -n "$CERN" -a -n "$CERN_LEVEL" && CERNLIBPATH=$CERNLIBPATH:$CERN/$CERN_LEVEL/lib
CERNLIBPATH=$CERNLIBPATH:/usr/local/cern/pro/lib:/cern/pro/lib:/usr/local/lib/cern:/usr/local/lib:/usr/lib

dnl ------------------------------------------------------------------------
dnl The LHAPDF structure function library

WHIZARD_LIB_ENABLE(LHAPDF, LHAPDF,
[  --enable-lhapdf         enable LHAPDF for structure functions [[yes]]
                          (if not found, set LHAPDF_DIR)],
libLHAPDF.a,
$LHAPDF_DIR:$LHAPDFDIR:$CERNLIBPATH)

WHIZARD_CHECK_LIB_FC(LHAPDF, $LHAPDF_LIB, $LHAPDF_DIR,
initpdfset)

dnl ------------------------------------------------------------------------
dnl PDFLIB

WHIZARD_LIB_ENABLE(PDFLIB, pdflib,
[  --enable-pdflib         enable the PDF library [[yes]]
                          (if not found, set PDFLIB_DIR or CERNLIB_DIR)],
libpdflib.a libpdflib8*.a,
$PDFLIB_DIR:$PDFLIBDIR:$CERNLIBPATH)

WHIZARD_LIB_IF_ENABLED(KERNLIB, kernlib,
PDFLIB,
libkernlib.a,
$PDFLIB_DIR:$PDFLIBDIR:$CERNLIBPATH)

WHIZARD_LIB_IF_ENABLED(MATHLIB, mathlib,
PDFLIB,
libmathlib.a,
$PDFLIB_DIR:$PDFLIBDIR:$CERNLIBPATH)

test "$KERNLIB_DIR" = "$PDFLIB_DIR" || KERNLIB_PATH="-L$KERNLIB_DIR"
test "$MATHLIB_DIR" = "$PDFLIB_DIR" || MATHLIB_PATH="-L$MATHLIB_DIR"

WHIZARD_CHECK_LIB_FC(PDFLIB, $PDFLIB_LIB, $PDFLIB_DIR,
pdfset, 
-l$MATHLIB_LIB $MATHLIB_PATH -l$KERNLIB_LIB $KERNLIB_PATH)


dnl ------------------------------------------------------------------------
dnl PYTHIA

WHIZARD_LIB_ENABLE(PYTHIA, pythia,
[  --enable-pythia         enable the PYTHIA fragmentation library [[yes]]
                          (if not found, set PYTHIA_DIR or CERNLIB_DIR)],
libpythia.a libpythia6*.a,
$PYTHIA_DIR:$PYTHIADIR:$CERNLIBPATH)

WHIZARD_CHECK_LIB_FC(PYTHIA, $PYTHIA_LIB, $PYTHIA_DIR,
pyinit)

dnl Check for the newer user-process interface
if test "$PYTHIA" != "no"; then
AC_CHECK_LIB($PYTHIA_LIB,upinit,PYTHIA_HEPRUP="yes",PYTHIA_HEPRUP="no")
AC_SUBST(PYTHIA_HEPRUP)
fi

dnl ------------------------------------------------------------------------
dnl The STDHEP library for binary I/O

WHIZARD_LIB_ENABLE(STDHEP, stdhep,
[  --enable-stdhep         enable the STDHEP library for writing events [[yes]]
                          (if not found, set STDHEP_DIR or CERNLIB_DIR)],
libstdhep.a,
$STDHEP_DIR:$STDHEPDIR:$CERNLIBPATH)

WHIZARD_LIB_IF_ENABLED(FMCFIO, Fmcfio, 
STDHEP,
libFmcfio.a,
$STDHEP_DIR:$STDHEPDIR:$CERNLIBPATH)

test "$FMCFIO_DIR" = "$STDHEP_DIR" || FMCFIO_PATH="-L$FMCFIO_DIR"

WHIZARD_CHECK_LIB_FC(STDHEP, $STDHEP_LIB, $STDHEP_DIR,
stdxwinit, 
-l$FMCFIO_LIB $FMCFIO_PATH)


dnl ------------------------------------------------------------------------
dnl Check the O'Caml Compiler

WK_CONFIGURE_SECTION([Check O'Caml compiler])

########################################################################
#
# Objective Caml:
#
########################################################################

THO_OCAML_BASE
THO_OCAML_LIBDIR
THO_OCAML_VERSION
THO_OCAML_REQUIRE_VERSION(304000)
THO_OCAML_LABLGTK
THO_OCAMLWEB
THO_OCAMLWEB_VERSION
THO_OCAMLWEB_REQUIRE_VERSION(009000)

########################################################################
#
# For development only:
#
########################################################################

AC_PATH_PROGS(OCAMLDOT,ocamldot)
AC_PATH_PROGS(OCAMLDEFUN,ocamldefun)

LIBOMEGA=libomega95.a
AC_SUBST(LIBOMEGA)

if test "$OMEGA" != "no"; then

OMEGADIR=$SRCDIR/omega-src/bundle

OMEGA_VERSION_FILE=$OMEGADIR/VERSION

AC_CACHE_CHECK([the O'Mega version], 
[wk_cv_omega_version],
[wk_cv_omega_version=`cat $OMEGA_VERSION_FILE`
OMEGAPATH=`echo $OMEGA_VERSION_FILE | sed -e 's|/VERSION||'`
OMEGA_VERSION=$wk_cv_omega_version
])
AC_SUBST(OMEGA_VERSION)
[OMEGA_MAJOR_VERSION=`echo $OMEGA_VERSION | sed -e 's/.*\([0-9][0-9][0-9]\)\.\([0-9][0-9][0-9]\).*/\1/'`]
[OMEGA_MINOR_VERSION=`echo $OMEGA_VERSION | sed -e 's/.*\([0-9][0-9][0-9]\)\.\([0-9][0-9][0-9]\).*/\2/'`]
AC_SUBST(OMEGA_MAJOR_VERSION)
AC_SUBST(OMEGA_MINOR_VERSION)

OMEGALIB=$OMEGADIR/lib/$LIBOMEGA


########################################################################
#
# Select O'Mega TARGET/MODEL pairs to build
#
########################################################################

AC_SUBST(SELECT_PROGRAMS_CUSTOM)
AC_SUBST(SELECT_PROGRAMS_RELEASED)
AC_SUBST(SELECT_PROGRAMS_UNRELEASED)
AC_SUBST(SELECT_PROGRAMS_THEORETICAL)
AC_SUBST(SELECT_PROGRAMS_REDUNDANT)
AC_SUBST(SELECT_PROGRAMS_DEVELOPERS)
AC_SUBST(SELECT_PROGRAMS_OBSOLETE)
AC_SUBST(SELECT_PROGRAMS_GUI)
SELECT_PROGRAMS_CUSTOM=
SELECT_PROGRAMS_RELEASED=yes
SELECT_PROGRAMS_UNRELEASED=
SELECT_PROGRAMS_THEORETICAL=
SELECT_PROGRAMS_REDUNDANT=
SELECT_PROGRAMS_DEVELOPERS=
SELECT_PROGRAMS_OBSOLETE=
SELECT_PROGRAMS_GUI=

fi  # OMEGA != no

AC_SUBST(OMEGADIR)
AC_SUBST(OMEGALIB)

########################################################################
#
# Options
#
########################################################################

########################################################################
# Fortran Compiler
########################################################################

dnl disable the 'signal' service routines upon request
AC_ARG_ENABLE(signals,
[  --enable-signals        accept external signals at runtime [[yes]]])
WHIZARD_ALLOW_SIGNALS=${enable_signals:-yes}
AC_SUBST(WHIZARD_ALLOW_SIGNALS)

dnl Profiling enabled upon request
AC_ARG_ENABLE(profiling,
[  --enable-fc-profiling   enable profiling for Fortran code [[no]]])

dnl Quadruple precision enforced upon request
AC_ARG_ENABLE(quadruple,
[  --enable-quadruple      use quadruple precision in Fortran code [[no]]])
if test "$enable_quadruple" = "yes"; then
  FC_PRECISION=quadruple
else
  FC_PRECISION=double
fi
AC_SUBST(FC_PRECISION)

########################################################################
# O'Mega Unreleased
########################################################################

AC_ARG_ENABLE([unreleased],
[  --enable-unreleased     Build some unreleased models that have not been
                          tested extensively.],
[case "$enableval" in
  yes) SELECT_PROGRAMS_UNRELEASED=yes;;
esac])

########################################################################
# O'Mega Theoretical
########################################################################

AC_ARG_ENABLE([theoretical],
[  --enable-theoretical    Build some theoretical models that are not
                          realized in nature.],
[case "$enableval" in
  yes) SELECT_PROGRAMS_THEORETICAL=yes;;
esac])

########################################################################
# O'Mega Redundant
########################################################################

AC_ARG_ENABLE([redundant],
[  --enable-redundant      Build some redundant models that add no new
                          physics for consistency checking.],
[case "$enableval" in
  yes) SELECT_PROGRAMS_REDUNDANT=yes;;
esac])

########################################################################
# O'Mega: Only for developers
########################################################################

if $development_source_tree; then

AC_ARG_ENABLE([developers],
[  --enable-developers     Build unreleased components (developers only!).],
[case "$enableval" in
  yes) AC_MSG_WARN([The development version needs a more up-to-date compiler:])
       THO_OCAML_REQUIRE_VERSION(307000)
       SELECT_PROGRAMS_DEVELOPERS=yes
       ;;
esac])

AC_ARG_ENABLE([obsoletel],
[  --enable-obsolete       Build some obsolete components that are only
                          of historical value (developers only!).],
[case "$enableval" in
  yes) SELECT_PROGRAMS_OBSOLETE=yes;;
esac])

AC_ARG_ENABLE([unsupported],
[  --enable-unsupported    Build also the unsupported applications that
                          are still under development and might produce
			  incorrect results.],
[case "$enableval" in
  yes) SELECT_PROGRAMS_UNRELEASED=yes
       SELECT_PROGRAMS_THEORETICAL=yes
       SELECT_PROGRAMS_REDUNDANT=yes
       SELECT_PROGRAMS_DEVELOPERS=yes
       SELECT_PROGRAMS_OBSOLETE=yes
       ;;
esac])

fi

########################################################################
# Custom collection
########################################################################

AC_ARG_ENABLE([program],
[  --enable-program=name   Build only one or more selected applications.],
[case "$enableval" in
   no) SELECT_PROGRAMS_CUSTOM=""
       ;;
    *) SELECT_PROGRAMS_CUSTOM="$SELECT_PROGRAMS_CUSTOM $enableval"
       ;;
esac])

########################################################################
# GUI
########################################################################

AC_ARG_ENABLE([gui],
[  --enable-omega-gui      Build a partial GUI that does nothing useful yet
                          (requires LablGTK!!!).],
[case "$enableval" in
  yes) if test -n "$LABLGTKDIR"; then
         SELECT_PROGRAMS_GUI="$enableval"
       else
         AC_MSG_ERROR([GUI requested, but LablGTK not found!])
       fi
       ;;
esac])


dnl ************************************************************************
dnl Finalize

WK_CONFIGURE_SECTION([Final setup of build system])

########################################################################
#  Makefiles
########################################################################

# These Makefiles must be avalaible:
PUBLIC_MAKEFILES="
  $OMEGADIR/Makefile
  $OMEGADIR/src/Makefile
  $OMEGADIR/bin/Makefile
  $OMEGADIR/lib/Makefile"

# These Makefiles are only required for developers:
PRIVATE_MAKEFILES="
  $OMEGADIR/doc/Makefile
  $OMEGADIR/web/Makefile
  $OMEGADIR/models/Makefile
  $OMEGADIR/tools/Makefile
  $OMEGADIR/tests/SM/Makefile
  $OMEGADIR/tests/MSSM/Makefile
  $OMEGADIR/tests/people/jr/Makefile
  $OMEGADIR/tests/people/tho/Makefile
  $OMEGADIR/extensions/people/jr/Makefile
  $OMEGADIR/extensions/people/tho/Makefile"

MAKEFILES="$PUBLIC_MAKEFILES"
for f in $PRIVATE_MAKEFILES; do
  if test -f $f.in; then
    MAKEFILES="$MAKEFILES $f"
  fi
done

OUTFILES="$OUTFILES $MAKEFILES"

dnl Prepend/append extra user-defined linker flags
test -n "$USERLIBS" && LIBS="$USERLIBS $LIBS"
test -n "$SYSTEMLIBS" && LIBS="$LIBS $SYSTEMLIBS"

dnl The linker
WK_WHIZARD_EXECUTABLES(bin, whizard.ld)

dnl Extra commands in AC_OUTPUT:
dnl Make executables executable
AC_OUTPUT_COMMANDS(
[for file in $EXECUTABLES; do
  chmod u+x $file
done],
EXECUTABLES="$EXECUTABLES")

dnl Don't overwrite kinds.f90 if it hasn't changed
AC_OUTPUT_COMMANDS(
[cat $KINDS/kinds | $CPIF $KINDS/kinds.f90
rm -f $KINDS/kinds
],
KINDS=$KINDS
CPIF=$CPIF
)

dnl ------------------------------------------------------------------------
dnl Write a summary of the configure results

AC_OUTPUT_COMMANDS(
[
echo 
echo "--- Configure summary: ---"
echo "--- Enabled features: ---"
if test "$OMEGA" != "no"; then
  echo " O'Mega    (Matrix elements)"; enabled=1
fi
if test "$CHSRC" != "no"; then
  echo " CompHEP   (Matrix elements)";  enabled=1
fi
if test "$MADGRAPH" != "no"; then
  echo " Madgraph  (Matrix elements)";     enabled=1
fi
if test "$CIRCE" != "no"; then
  echo " CIRCE     (Beamstrahlung)       $CIRCE/";    enabled=1
fi
if test "$CIRCE2" != "no"; then
  echo " CIRCE2    (Beamstrahlung)       $CIRCE2/";   enabled=1
fi
if test "$LHAPDF" != "no"; then
  echo " LHAPDF    (Structure functions) $LHAPDF"; enabled=1
fi
if test "$PDFLIB" != "no"; then
  echo " PDFLIB    (Structure functions) $PDFLIB";    enabled=1
fi
if test "$PYTHIA" != "no"; then
  echo " PYTHIA    (Fragmentation)       $PYTHIA";    enabled=1
fi
if test "$STDHEP" != "no"; then
  echo " STDHEP    (Binary event files)  $STDHEP";    enabled=1
fi
if test -n "$MPOST"; then
  echo " LaTeX/Metapost  (Histograms)    $MPOST";     enabled=1
fi
if test "$AUTOCONF" != "no"; then
  echo " Autoconf  (Restricted bundle)   $AUTOCONF";  enabled=1
fi
if test -z "$enabled"; then
  echo " (none)"
fi
echo
echo "--- Disabled or absent features: ---"
if test "$OMEGA" = "no"; then
  echo " O'Mega    (Matrix elements)";     disabled=1
fi
if test "$CHSRC" = "no"; then
  echo " CompHEP   (Matrix elements)";     disabled=1
fi
if test "$MADGRAPH" = "no"; then
  echo " Madgraph  (Matrix elements)";     disabled=1
fi
if test "$CIRCE" = "no"; then
  echo " CIRCE     (Beamstrahlung)";       disabled=1
fi
if test "$CIRCE2" = "no"; then
  echo " CIRCE2    (Beamstrahlung)";       disabled=1
fi
if test "$LHAPDF" = "no"; then
  echo " LHAPDF    (Structure functions)"; disabled=1
fi
if test "$PDFLIB" = "no"; then
  echo " PDFLIB    (Structure functions)"; disabled=1
fi
if test "$PYTHIA" = "no"; then
  echo " PYTHIA    (Fragmentation)";       disabled=1
fi
if test "$STDHEP" = "no"; then
  echo " STDHEP    (Binary event files)";  disabled=1
fi
if test -z "$MPOST"; then
  echo " LaTeX/Metapost  (Histograms)";    disabled=1
fi
if test "$AUTOCONF" = "no"; then
  echo " Autoconf  (Restricted bundle)";   disabled=1
fi
if test -z "$disabled"; then
  echo " (none)"
fi
echo
echo "--- Configuration complete. ---"
],
RESTRICTED=$RESTRICTED
OMEGA=$OMEGA
OMEGABINPATH=$OMEGABINPATH
CHSRC=$CHSRC
MADGRAPH=$MADGRAPH
CIRCE=$CIRCE
CIRCE2=$CIRCE2
LHAPDF=$LHAPDF
PDFLIB=$PDFLIB
PYTHIA=$PYTHIA
STDHEP=$STDHEP
MPOST=$MPOST
AUTOCONF=$AUTOCONF
)

dnl ------------------------------------------------------------------------

dnl Finalize, write Makefiles etc., and execute the extra commands above.
AC_OUTPUT($OUTFILES)

dnl The end.
