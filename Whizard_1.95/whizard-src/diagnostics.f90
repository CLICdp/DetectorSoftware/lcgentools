! WHIZARD 1.95 Feb 25 2010
! 
! (C) 1999-2008 by 
!     Wolfgang Kilian <wolfgang.kilian@desy.de>
!     Thorsten Ohl <ohl@physik.uni-wuerzburg.de>
!     Juergen Reuter <juergen.reuter@physik.uni-freiburg.de>
!
! WHIZARD is free software; you can redistribute it and/or modify it
! under the terms of the GNU General Public License as published by 
! the Free Software Foundation; either version 2, or (at your option)
! any later version.
!
! WHIZARD is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! This file has been stripped of most comments.  For documentation, refer
! to the source 'whizard.nw'

module diagnostics

  use kinds, only: i64 !NODEP!
  use limits, only: BUFFER_SIZE, WHIZARD_ROOT, MAX_ERRORS
  use mpi90, only: mpi90_rank

  implicit none
  private

  public :: msg_list_clear
  public :: msg_summary
  public :: msg_listing
  public :: msg_terminate
  public :: msg_bug, msg_fatal, msg_error, msg_warning
  public :: msg_message, msg_result, msg_debug
  public :: terminate_soon
  public :: terminate_now_maybe

  integer, parameter, public :: &
       & TERMINATE=-2, BUG=-1, &
       & FATAL=0, ERROR=1, WARNING=2, MESSAGE=3, RESULT=4, DEBUG=5
  integer, parameter, public :: &
       & CONTINUE=0, KILLED=1, INTERRUPTED=2, &
       & OS_TIME_EXCEEDED=3, TIME_EXCEEDED=4, &
       & MAX_FILE_COUNT_EXCEEDED=5

  type :: string_list
     character(len=BUFFER_SIZE) :: string
     type(string_list), pointer :: next
  end type string_list
  type :: string_list_pointer
     type(string_list), pointer :: first, last
  end type string_list_pointer

  integer, public, save :: msg_level = RESULT
  integer, dimension(TERMINATE:DEBUG), public, save :: msg_count = 0
  character(len=BUFFER_SIZE), public, save :: msg_buffer = " "

  type(string_list_pointer), dimension(TERMINATE:WARNING), save :: &
       & msg_list = string_list_pointer (null(), null())
  integer, public, save :: terminate_status = CONTINUE

contains

  subroutine msg_add (level)
    integer, intent(in) :: level
    type(string_list), pointer :: message
    select case (level)
    case (TERMINATE:WARNING)
       allocate (message)
       message%string = msg_buffer
       nullify (message%next)
       if (.not.associated (msg_list(level)%first)) &
            & msg_list(level)%first => message
       if (associated (msg_list(level)%last)) &
            & msg_list(level)%last%next => message
       msg_list(level)%last => message
       msg_count(level) = msg_count(level) + 1
    end select
  end subroutine msg_add

  subroutine msg_list_clear
    integer :: level
    type(string_list), pointer :: message
    do level = TERMINATE, WARNING
       do while (associated (msg_list(level)%first))
          message => msg_list(level)%first
          msg_list(level)%first => message%next
          deallocate (message)
       end do
       nullify (msg_list(level)%last)
    end do
    msg_count = 0
  end subroutine msg_list_clear

  subroutine msg_summary (unit)
    integer, intent(in), optional :: unit
1   format (1x,A,1x,I2,1x,A,I2,1x,A)
    if (msg_count(ERROR) > 0 .and. msg_count(WARNING) > 0) then
       write (msg_buffer, 1) "There were", &
            & msg_count(ERROR), "error(s) and", &
            & msg_count(WARNING), "warning(s)."
       call msg_message (unit=unit)
    else if (msg_count(ERROR) > 0) then
       write (msg_buffer, 1) "There were", &
            & msg_count(ERROR), "error(s) and no warnings."
       call msg_message (unit=unit)
    else if (msg_count(WARNING) > 0) then
       write (msg_buffer, 1) "There were no errors and", &
            & msg_count(WARNING), "warning(s)."
       call msg_message (unit=unit)
    end if
  end subroutine msg_summary

  subroutine msg_listing (level, unit, prefix)
    integer, intent(in) :: level
    integer, intent(in), optional :: unit
    character(len=*), intent(in), optional :: prefix
    type(string_list), pointer :: message
    integer :: u
    u = 6
    if (present (unit))  u = unit
    message => msg_list(level)%first
    do while (associated (message))
       if (present (prefix)) then
          write (u, "(A)") prefix // trim (message%string)
       else
          write (u, "(A)") trim (message%string)
       end if
       message => message%next
    end do
  end subroutine msg_listing

  subroutine buffer_clear
    msg_buffer = " "
  end subroutine buffer_clear

  subroutine message_print (level, string, unit)
    integer, intent(in) :: level
    character(len=*), intent(in), optional :: string
    integer, intent(in), optional :: unit
    character(len=80) :: prep_string
    integer :: proc_id
    call mpi90_rank (proc_id)
    if (proc_id==WHIZARD_ROOT) then
       select case (level)
       case (TERMINATE)
          prep_string = " !"
       case (BUG)
          prep_string = " ! *** WHIZARD bug:"
       case (FATAL)
          prep_string = " ! *** Fatal error:"
       case (ERROR)
          prep_string = " ! *** Error:"
       case (WARNING)
          prep_string = " ! Warning:"
       case (MESSAGE, DEBUG)
          prep_string = " !"
       case default
          prep_string = " "
       end select
       if (present(string))  msg_buffer = string
       if (present(unit)) then
          if (unit/=6) then
             write (unit, "(A,A)") trim(prep_string), trim(msg_buffer)
          else if (level <= msg_level) then
             print "(A,A)", trim(prep_string), trim(msg_buffer)
          end if
       else if (level <= msg_level) then
          print "(A,A)", trim(prep_string), trim(msg_buffer)
       end if
    end if
    call msg_add (level)
    call buffer_clear
  end subroutine message_print

  subroutine msg_terminate (string, unit)
    integer, intent(in), optional :: unit
    character(len=*), intent(in), optional :: string
    call message_print (TERMINATE, string, unit)
    call msg_summary (unit)
    call message_print (MESSAGE, " WHIZARD run finished.", unit)
    stop
  end subroutine msg_terminate

  subroutine msg_bug (string, unit)
    integer, intent(in), optional :: unit
    character(len=*), intent(in), optional :: string
    call message_print (BUG, string, unit)
    stop "WHIZARD run aborted."
  end subroutine msg_bug

  subroutine msg_fatal (string, unit)
    integer, intent(in), optional :: unit
    character(len=*), intent(in), optional :: string
    call message_print (FATAL, string, unit)
    stop "WHIZARD run aborted."
  end subroutine msg_fatal

  subroutine msg_error (string, unit)
    integer, intent(in), optional :: unit
    character(len=*), intent(in), optional :: string
    call message_print (ERROR, string, unit)
    if (msg_count(ERROR) >= MAX_ERRORS) then
       call msg_fatal (" Too many errors encountered.")
    else if (.not.present(unit))  then
       call message_print (MESSAGE, "            (WHIZARD run continues)")
    end if
  end subroutine msg_error

  subroutine msg_warning (string, unit)
    integer, intent(in), optional :: unit
    character(len=*), intent(in), optional :: string
    call message_print (WARNING, string, unit)
  end subroutine msg_warning

  subroutine msg_message (string, unit)
    integer, intent(in), optional :: unit
    character(len=*), intent(in), optional :: string
    call message_print (MESSAGE, string, unit)
  end subroutine msg_message

  subroutine msg_result (string, unit)
    integer, intent(in), optional :: unit
    character(len=*), intent(in), optional :: string
    call message_print (RESULT, string, unit)
  end subroutine msg_result

  subroutine msg_debug (string, unit)
    integer, intent(in), optional :: unit
    character(len=*), intent(in), optional :: string
    call message_print (DEBUG, string, unit)
  end subroutine msg_debug

  subroutine terminate_soon (reason)
    integer, intent(in) :: reason
    terminate_status = reason
  end subroutine terminate_soon

  subroutine terminate_now_maybe (n_events)
    integer(i64), intent(in), optional :: n_events
    if (terminate_status /= CONTINUE) then
       if (present (n_events)) then
          write (msg_buffer, "(1x,A,1x,I12,1x,A)")  &
               & "Generated", n_events, "events."
          call msg_message
       end if
       select case (terminate_status)
       case (KILLED)
          call msg_terminate (" *** Process killed by external signal: Terminating.")
       case (INTERRUPTED)
          call msg_terminate (" *** Process interrupted by external signal: Terminating.")
       case (OS_TIME_EXCEEDED)
          call msg_terminate (" *** Process received signal SIGXCPU (time exceeded):  Terminating.")
       case (TIME_EXCEEDED)
          call msg_terminate (" *** User-defined time limit exceeded:  Terminating.")
       case (MAX_FILE_COUNT_EXCEEDED)
          call msg_terminate (" *** User-defined limit for event file count exceeded:  Terminating.")
       case default
          call msg_bug (" Process terminates for unknown reason")
       end select
    end if
  end subroutine terminate_now_maybe


end module diagnostics
