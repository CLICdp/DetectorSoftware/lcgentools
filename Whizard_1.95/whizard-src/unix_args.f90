! WHIZARD 1.95 Feb 25 2010
! 
! (C) 1999-2008 by 
!     Wolfgang Kilian <wolfgang.kilian@desy.de>
!     Thorsten Ohl <ohl@physik.uni-wuerzburg.de>
!     Juergen Reuter <juergen.reuter@physik.uni-freiburg.de>
!
! WHIZARD is free software; you can redistribute it and/or modify it
! under the terms of the GNU General Public License as published by 
! the Free Software Foundation; either version 2, or (at your option)
! any later version.
!
! WHIZARD is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! This file has been stripped of most comments.  For documentation, refer
! to the source 'whizard.nw'

module unix_args

  use limits, only: MAX_CMDLINE_OPTIONS, CMDLINE_ARG_LEN, CMDLINE_OPTKEY_LEN
  use limits, only: SIGTERM, SIGINT, SIGXCPU
  use diagnostics, only: msg_message, msg_bug
  use diagnostics, only: terminate_soon, KILLED, INTERRUPTED, OS_TIME_EXCEEDED

  include "unix_args.inc"

  private

  public :: catch_signals, restore_signals
  public :: cmd_line_create, cmd_line_destroy
  public :: cmd_option_define
  public :: cmd_line_read
  public :: cmd_line_write

  type :: cmdline_option
     character(len=CMDLINE_OPTKEY_LEN) :: key = "", alt_key = ""
     character(len=CMDLINE_ARG_LEN) :: default_value = ""
     integer :: key_len = 0, alt_key_len = 0
     logical :: needs_value = .false.
     logical :: is_defined = .false.
     logical :: has_default = .false.
     logical, pointer :: is_set => null ()
     character(len=CMDLINE_ARG_LEN), pointer :: value => null ()
  end type cmdline_option

  type :: cmdline
     character(len=CMDLINE_ARG_LEN) :: prg = ""
     character(len=CMDLINE_ARG_LEN), dimension(:), pointer :: arg => null ()
     type(cmdline_option), dimension(MAX_CMDLINE_OPTIONS) :: opt
     integer :: n_entries = 0, n_args = 0, n_opts = 0
  end type cmdline


  logical, save :: signals_will_be_caught = .false.
  type(cmdline), save :: cmd

contains

  subroutine catch_signals
    integer :: dummy
    signals_will_be_caught = .true.
    dummy = signal (SIGINT, signal_handler_interrupt, -1)
    dummy = signal (SIGTERM, signal_handler_kill, -1)
    dummy = signal (SIGXCPU, signal_handler_xcpu, -1)
  end subroutine catch_signals
    
  subroutine restore_signals
    integer :: dummy
    if (signals_will_be_caught) then
       dummy = signal (SIGINT, signal_handler_interrupt, 0)
       dummy = signal (SIGTERM, signal_handler_kill, 0)
       dummy = signal (SIGXCPU, signal_handler_xcpu, 0)
       signals_will_be_caught = .false.
    end if
  end subroutine restore_signals

  function signal_dummy (signum, signal_handler, flag) result (dummy)
    integer :: signum, flag, dummy
    interface
       function signal_handler (signum) result (flag)
         integer :: signum, flag
       end function signal_handler
    end interface
    signals_will_be_caught = .false.
    dummy = 0
  end function signal_dummy

  function signal_handler_kill (signum) result (flag)
    integer :: signum, flag
    call restore_signals
    call terminate_soon (reason = KILLED)
    flag = 1
  end function signal_handler_kill

  function signal_handler_interrupt (signum) result (flag)
    integer :: signum, flag
    call restore_signals
    call terminate_soon (reason = INTERRUPTED)
    flag = 1
  end function signal_handler_interrupt

  function signal_handler_xcpu (signum) result (flag)
    integer :: signum, flag
    call restore_signals
    call terminate_soon (reason = OS_TIME_EXCEEDED)
    flag = 1
  end function signal_handler_xcpu

  subroutine cmd_line_create (n_entries)
    integer, intent(in) :: n_entries
    allocate (cmd%arg (n_entries))
    cmd%n_entries = n_entries
  end subroutine cmd_line_create

  subroutine cmd_line_destroy ()
    !XXX return [old Intel compiler bug]
    deallocate (cmd%arg)
    cmd%n_entries = 0
  end subroutine cmd_line_destroy

  subroutine cmd_option_define (key, is_set, value, default_value, alt_key)
    character(len=*), intent(in) :: key
    logical, target, intent(inout) :: is_set
    character(len=*), target, intent(inout), optional :: value
    character(len=*), intent(in), optional :: default_value, alt_key
    integer :: i
    do i = 1, size (cmd%opt)
       if (.not.cmd%opt(i)%is_defined) then
          cmd%opt(i)%key = key
          cmd%opt(i)%key_len = len_trim (key)
          if (present (alt_key)) then
             cmd%opt(i)%alt_key = alt_key
             cmd%opt(i)%alt_key_len = len_trim (alt_key)
          end if
          cmd%opt(i)%is_set => is_set
          is_set = .false.
          if (present (default_value)) then
             cmd%opt(i)%has_default = .true.
             cmd%opt(i)%default_value = default_value
          end if
          if (present (value)) then
             cmd%opt(i)%needs_value = .true.
             cmd%opt(i)%value => value
             if (present (default_value)) then
                cmd%opt(i)%has_default = .true.
                cmd%opt(i)%default_value = default_value
                cmd%opt(i)%value = cmd%opt(i)%default_value
                cmd%opt(i)%is_set = .true.
             end if
          end if
          cmd%opt(i)%is_defined = .true.
          return
       end if
    end do
    call msg_bug (" Too many command-line option definitions")
  end subroutine cmd_option_define

  subroutine cmd_line_read (usage)
    interface
       subroutine usage
       end subroutine usage
    end interface
    character(len=CMDLINE_ARG_LEN) :: buffer
    integer :: count
    logical :: look_for_options
    count = max (command_argument_count (), 0)
    call cmd_line_create (count)
    if (command_argument_count () >= 0)  call get_command_argument (0, cmd%prg)
    look_for_options = .true.
    count = 0
    SCAN_CMDLINE: do while (count < cmd%n_entries)
       count = count + 1
       call get_command_argument (count, buffer)
       if (trim (buffer) == "-") then
          look_for_options = .false.
       else
          if (look_for_options .and. buffer(1:1) == "-") then
             cmd%n_opts = cmd%n_opts + 1
             call option_get &
                  & (cmd%opt, buffer, count, cmd%n_entries, usage)
          else
             cmd%n_args = cmd%n_args + 1
             cmd%arg(cmd%n_args) = buffer
          end if
       end if
    end do SCAN_CMDLINE
  end subroutine cmd_line_read

  recursive subroutine option_get (opt, opt_string, count, count_max, usage)
    type(cmdline_option), dimension(:), intent(inout) :: opt
    character(len=*), intent(in) :: opt_string
    integer, intent(inout) :: count
    integer, intent(in) :: count_max
    interface
       subroutine usage
       end subroutine usage
    end interface
    integer :: i
    logical :: found
    found = .false.
    SCAN_OPTIONS: do i = 1, size (opt)
       if (opt(i)%is_defined) then
          if (opt_string(1:opt(i)%key_len) == trim (opt(i)%key)) then
             found = .true.
             call option_set &
                  & (opt, opt_string(opt(i)%key_len+1:), &
                  &  i, count, count_max, .true., usage)
          else if (opt(i)%alt_key_len /= 0 .and. &
               &   trim (opt_string) == trim (opt(i)%alt_key)) &
               & then
             found = .true.
             call option_set &
                  & (opt, opt_string(opt(i)%alt_key_len+1:), &
                  &  i, count, count_max, .false., usage)
          end if
          if (found)  exit SCAN_OPTIONS
       end if
    end do SCAN_OPTIONS
    if (.not.found) then
       call msg_message (" Unknown option: "// trim (opt_string))
       call usage
       stop
    end if
  end subroutine option_get

  recursive subroutine option_set &
       & (opt, opt_string, opt_i, count, count_max, cumulate, usage)
    type(cmdline_option), dimension(:), intent(inout) :: opt
    character(len=*), intent(in) :: opt_string
    integer, intent(in) :: opt_i
    integer, intent(inout) :: count
    integer, intent(in) :: count_max
    logical, intent(in) :: cumulate
    interface
       subroutine usage
       end subroutine usage
    end interface
    character(len=CMDLINE_ARG_LEN) :: buffer
    opt(opt_i)%is_set = .true.
    if (opt(opt_i)%needs_value) then
       if (len_trim (opt_string) > 0) then
          opt(opt_i)%value = opt_string
       else if (count < count_max) then
          count = count + 1
          call get_command_argument (count, buffer)
          if (buffer(1:1) == "-")  call missing_arg
          opt(opt_i)%value = buffer
       else
          call missing_arg
       end if
    else if (cumulate .and. len_trim (opt_string) > 0) then
       call option_get (opt, "-"//opt_string, count, count_max, usage)
    end if
  contains
    subroutine missing_arg
      call msg_message (" Option " // &
           & trim (opt(opt_i)%key) // ": argument expected")
      call usage
      stop
    end subroutine missing_arg
  end subroutine option_set

  subroutine cmd_line_write (unit)
    integer, intent(in) :: unit
    integer :: i
    call msg_message (" Program name: " // trim (cmd%prg), unit=unit)
    call msg_message (" Command line options:", unit=unit)
    do i = 1, size (cmd%opt)
       if (cmd%opt(i)%is_defined) then
          if (cmd%opt(i)%needs_value) then
             if (cmd%opt(i)%has_default) then
                call msg_message ("  " // trim (cmd%opt(i)%key) // " = " // &
                     & "'" // trim (cmd%opt(i)%value) // "'" // &
                     & " [" // trim (cmd%opt(i)%default_value) // "]", &
                     & unit=unit)
             else if (cmd%opt(i)%is_set) then
                call msg_message ("  " // trim (cmd%opt(i)%key) // " = " // &
                     & "'" // trim (cmd%opt(i)%value) // "'", &
                     & unit=unit)
             else
                call msg_message ("  " // trim (cmd%opt(i)%key) // &
                     & " unset", unit=unit)
             end if
          else if (cmd%opt(i)%is_set) then
             call msg_message ("  " // trim (cmd%opt(i)%key) // &
                  & " set", unit=unit)
          else
             call msg_message ("  " // trim (cmd%opt(i)%key) // &
                  & " unset", unit=unit)
          end if
       end if
    end do
    call msg_message (" Command line arguments:")
    do i = 1, cmd%n_args
       call msg_message ("  " // trim (cmd%arg(i)))
    end do
  end subroutine cmd_line_write


end module unix_args
