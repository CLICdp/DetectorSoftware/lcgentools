! WHIZARD 1.95 Feb 25 2010
! 
! (C) 1999-2008 by 
!     Wolfgang Kilian <wolfgang.kilian@desy.de>
!     Thorsten Ohl <ohl@physik.uni-wuerzburg.de>
!     Juergen Reuter <juergen.reuter@physik.uni-freiburg.de>
!
! WHIZARD is free software; you can redistribute it and/or modify it
! under the terms of the GNU General Public License as published by 
! the Free Software Foundation; either version 2, or (at your option)
! any later version.
!
! WHIZARD is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! This file has been stripped of most comments.  For documentation, refer
! to the source 'whizard.nw'

module unix_args

  use limits, only: CMDLINE_ARG_LEN
  use diagnostics, only: msg_error

  implicit none
  private

  public :: catch_signals, restore_signals
  public :: cmd_option_define
  public :: cmd_line_create, cmd_line_destroy
  public :: cmd_line_read
  public :: cmd_line_write

contains

  subroutine catch_signals
  end subroutine catch_signals

  subroutine restore_signals
  end subroutine restore_signals

  subroutine cmd_option_define (key, is_set, value, alt_key, default_value)
    character(len=*), intent(in) :: key
    logical, target, intent(inout) :: is_set
    character(len=*), target, intent(inout), optional :: value
    character(len=*), intent(in), optional :: alt_key
    character(len=*), intent(in), optional :: default_value
  end subroutine cmd_option_define

  subroutine cmd_line_create (dummy)
    integer, intent(in) :: dummy
  end subroutine cmd_line_create

  subroutine cmd_line_destroy
  end subroutine cmd_line_destroy

  subroutine cmd_line_read (usage)
    interface
       subroutine usage
       end subroutine usage
    end interface
  end subroutine cmd_line_read

  subroutine cmd_line_write (unit)
    integer, intent(in) :: unit
    call msg_error (" cmd_line_write: cmd-line module is not available")
  end subroutine cmd_line_write

end module unix_args
