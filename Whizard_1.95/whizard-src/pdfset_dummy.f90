! PDFSET dummy routine, to be removed when PDFLIB is to be linked.
subroutine pdfset (parm,value)
  implicit double precision(a-h, o-z)
  implicit integer(i-n)
  character*20 parm(20)
  double precision value(20)
  stop " Can't happen: PDFSET dummy routine called"
end
