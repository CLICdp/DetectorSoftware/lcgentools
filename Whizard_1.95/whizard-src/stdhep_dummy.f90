! WHIZARD 1.95 Feb 25 2010
! 
! (C) 1999-2008 by 
!     Wolfgang Kilian <wolfgang.kilian@desy.de>
!     Thorsten Ohl <ohl@physik.uni-wuerzburg.de>
!     Juergen Reuter <juergen.reuter@physik.uni-freiburg.de>
!
! WHIZARD is free software; you can redistribute it and/or modify it
! under the terms of the GNU General Public License as published by 
! the Free Software Foundation; either version 2, or (at your option)
! any later version.
!
! WHIZARD is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! This file has been stripped of most comments.  For documentation, refer
! to the source 'whizard.nw'

module stdhep_interface

  use kinds, only: i64 !NODEP!
  use diagnostics, only: msg_error, msg_message

  implicit none
  private

  public :: stdhep_init, stdhep_write, stdhep_end
 
  integer, parameter, public :: &
       STDHEP_HEPEVT = 1, STDHEP_HEPEUP = 11, STDHEP_HEPRUP = 12

contains

  subroutine stdhep_init (file, title, nevt)
    character(len=*), intent(in) :: file, title
    integer(i64), intent(in) :: nevt
    call msg_error (" STDHEP output is not available.")
    call msg_message (" To enable STDHEP, rerun configure and recompile WHIZARD.")
  end subroutine stdhep_init

  subroutine stdhep_write (dummy)
    integer, intent(in) :: dummy
  end subroutine stdhep_write

  subroutine stdhep_end
  end subroutine stdhep_end

end module stdhep_interface
