! WHIZARD 1.95 Feb 25 2010
! 
! (C) 1999-2008 by 
!     Wolfgang Kilian <wolfgang.kilian@desy.de>
!     Thorsten Ohl <ohl@physik.uni-wuerzburg.de>
!     Juergen Reuter <juergen.reuter@physik.uni-freiburg.de>
!
! WHIZARD is free software; you can redistribute it and/or modify it
! under the terms of the GNU General Public License as published by 
! the Free Software Foundation; either version 2, or (at your option)
! any later version.
!
! WHIZARD is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! This file has been stripped of most comments.  For documentation, refer
! to the source 'whizard.nw'

module jetset_interface

  use kinds, only: default !NODEP!
  use kinds, only: double !NODEP!
  use diagnostics, only: msg_error, msg_message
  use limits, only: BUFFER_SIZE, PYTHIA_PARAMETERS_LEN

  implicit none
  private

  integer, parameter, public :: PYGIVE_LEN = 100

  type, public :: pythia_init_data
     integer :: n_external
     character(len=BUFFER_SIZE) :: process_id, pyproc, beam, target
     logical :: fixed_energy, show_pymaxi
     real(kind=default) :: sqrts, integral, error
     character(len=PYTHIA_PARAMETERS_LEN) :: chin
  end type pythia_init_data


  public :: jetset_init, pythia_init
  public :: jetset_fragment
  public :: pythia_fragment
  public :: pythia_up_fill
  public :: jetset_open_write, jetset_close
  public :: jetset_write
  public :: jetset_suppress_banner
  public :: pythia_print_statistics

  logical, public :: new_event_needed_pythia = .true.
  integer, public :: number_events_whizard = 0
  integer, public :: number_events_pythia = 0
  integer, dimension(2), public :: number_events_written = 0

contains

  subroutine jetset_init (dummy)
    character(len=*), intent(in) :: dummy
    call msg_error (" JETSET fragmentation is not available.")
    call msg_message &
         & (" To enable JETSET/PYTHIA, rerun configure and recompile WHIZARD.")
  end subroutine jetset_init

  subroutine pythia_init (dummy)
    type(pythia_init_data), intent(in) :: dummy
    call msg_error (" PYTHIA fragmentation is not available.")
    call msg_message &
         & (" To enable PYTHIA, rerun configure and recompile WHIZARD.")
  end subroutine pythia_init

  subroutine jetset_fragment (color_flow_dummy, anticolor_flow_dummy)
    integer, dimension(:), intent(in) :: color_flow_dummy, anticolor_flow_dummy
  end subroutine jetset_fragment
  subroutine pythia_fragment (m_dummy, p_dummy, code_dummy, &
       & color_flow_dummy, anticolor_flow_dummy)
    real(kind=default), dimension(:), intent(in) :: m_dummy
    real(kind=default), dimension(:,0:), intent(in) :: p_dummy
    integer, dimension(:), intent(in) :: code_dummy
    integer, dimension(:), intent(in) :: color_flow_dummy, anticolor_flow_dummy
  end subroutine pythia_fragment
  subroutine pythia_up_fill
  end subroutine pythia_up_fill

  subroutine jetset_open_write (unit, file)
    integer, intent(in) :: unit
    character (len=*), intent(in) :: file
  end subroutine jetset_open_write

  subroutine jetset_close (unit)
    integer, intent(in) :: unit
  end subroutine jetset_close

  subroutine jetset_write (dummy1, dummy2)
    integer, intent(in) :: dummy1, dummy2
    call msg_error (" PYTHIA event listing format is not available.")
    call msg_message (" To enable PYTHIA, rerun configure and recompile WHIZARD.")
  end subroutine jetset_write
  subroutine jetset_suppress_banner
  end subroutine jetset_suppress_banner
  subroutine pythia_print_statistics
  end subroutine pythia_print_statistics

end module jetset_interface
