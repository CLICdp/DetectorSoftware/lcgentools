! WHIZARD 1.95 Feb 25 2010
! 
! (C) 1999-2008 by 
!     Wolfgang Kilian <wolfgang.kilian@desy.de>
!     Thorsten Ohl <ohl@physik.uni-wuerzburg.de>
!     Juergen Reuter <juergen.reuter@physik.uni-freiburg.de>
!
! WHIZARD is free software; you can redistribute it and/or modify it
! under the terms of the GNU General Public License as published by 
! the Free Software Foundation; either version 2, or (at your option)
! any later version.
!
! WHIZARD is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! This file has been stripped of most comments.  For documentation, refer
! to the source 'whizard.nw'

module bytes

  use kinds, only: i8, i32, i64 !NODEP!
  
  implicit none
  private

  type, public :: byte_t
     private
     integer(i8) :: i
  end type byte_t

  type, public :: word32_t
     private
     integer(i32) :: i
     integer :: fill = 0
  end type word32_t

  type, public :: word64_t
     private
     integer(i64) :: i
  end type word64_t


  public :: assignment(=)
  public :: byte_write
  public :: word32_empty, word32_filled, word32_fill
  public :: word32_append_byte
  public :: byte_from_word32
  public :: word32_write
  public :: not, ior, ieor, iand, ishftc, operator(+)
  public :: byte_from_word64, word32_from_word64

  type(byte_t), parameter, public :: byte_zero = byte_t (0_i8)


  interface assignment(=)
     module procedure set_byte_from_i8
  end interface
  interface byte_write
     module procedure byte_write_unit, byte_write_string
  end interface
  interface assignment(=)
     module procedure word32_set_from_i32
     module procedure word32_set_from_byte
  end interface
  interface word32_write
     module procedure word32_write_unit
  end interface
  interface not
     module procedure word_not
  end interface
  interface ior
     module procedure word_or
  end interface
  interface ieor
     module procedure word_eor
  end interface
  interface iand
     module procedure word_and
  end interface
  interface ishftc
     module procedure word_shftc
  end interface
  interface operator(+)
     module procedure word_add
  end interface
  interface assignment(=)
     module procedure word64_set_from_i64
     module procedure word64_set_from_word32
  end interface
  interface word64_write
     module procedure word64_write_unit
  end interface

contains

  subroutine set_byte_from_i8 (b, i)
    type(byte_t), intent(out) :: b
    integer(i8), intent(in) :: i
    b%i = i
  end subroutine set_byte_from_i8

  subroutine byte_write_unit (u, b, decimal, newline)
    integer, intent(in) :: u
   type(byte_t), intent(in), optional :: b
    logical, intent(in), optional :: decimal, newline
    logical :: dc, nl
    type(word32_t) :: w
    dc = .false.;  if (present (decimal))  dc = decimal
    nl = .false.;  if (present (newline))  nl = newline
    if (dc) then
       w = b
       write (u, '(I3)', advance='no')  w%i
    else
       write (u, '(z2.2)', advance='no')  b%i
    end if
    if (nl) write (u, *)
  end subroutine byte_write_unit

  subroutine byte_write_string (s, b)
    type(byte_t), intent(in) :: b
    character(len=2), intent(inout) :: s
    write (s, '(z2.2)')  b%i
  end subroutine byte_write_string

  subroutine word32_set_from_i32 (w, i)
    type(word32_t), intent(out) :: w
    integer(i32), intent(in) :: i
    w%i = i
    w%fill = 32
  end subroutine word32_set_from_i32

  subroutine word32_set_from_byte (w, b)
    type(word32_t), intent(out) :: w
    type(byte_t), intent(in) :: b
    if (b%i >= 0_i8) then
       w%i = b%i
    else
       w%i = 2_i32*(huge(0_i8)+1_i32) + b%i
    end if
    w%fill = 32
  end subroutine word32_set_from_byte

  function word32_empty (w)
    type(word32_t), intent(in) :: w
    logical :: word32_empty
    word32_empty = (w%fill == 0)
  end function word32_empty

  function word32_filled (w)
    type(word32_t), intent(in) :: w
    logical :: word32_filled
    word32_filled = (w%fill == 32)
  end function word32_filled

  function word32_fill (w)
    type(word32_t), intent(in) :: w
    integer :: word32_fill
    word32_fill = w%fill
  end function word32_fill

  subroutine word32_append_byte (w, b)
    type(word32_t), intent(inout) :: w
    type(byte_t), intent(in) :: b
    type(word32_t) :: w1
    if (.not. word32_filled (w)) then
       w1 = b
       call mvbits (w1%i, 0, 8, w%i, w%fill)
       w%fill = w%fill + 8
    end if
  end subroutine word32_append_byte

  function byte_from_word32 (w, i) result (b)
    type(word32_t), intent(in) :: w
    integer, intent(in) :: i
    type(byte_t) :: b
    integer(i32) :: j
    j = 0
    if (i >= 0 .and. i*8 < w%fill) then
       call mvbits (w%i, i*8, 8, j, 0)
    end if
    b%i = j
  end function byte_from_word32

  subroutine word32_write_unit (u, w, bytes, decimal, newline)
    integer, intent(in) :: u
   type(word32_t), intent(in) :: w
    logical, intent(in), optional :: bytes, decimal, newline
    logical :: dc, by, nl
    type(word64_t) :: ww
    integer :: i
    by = .false.;  if (present (bytes))   by = bytes
    dc = .false.;  if (present (decimal)) dc = decimal
    nl = .false.;  if (present (newline)) nl = newline
    if (by) then
       do i = 0, 3
          if (i>0)  write (u, '(1x)', advance='no')
          if (8*i < w%fill) then
             call byte_write_unit (u, byte_from_word32 (w, i), decimal=decimal)
          else if (dc) then
             write (u, '(3x)', advance='no')
          else
             write (u, '(2x)', advance='no')
          end if
       end do
    else if (dc) then
       ww = w
       write (u, '(I10)', advance='no') ww%i
    else
       select case (w%fill)
       case ( 0)
       case ( 8);  write (6, '(1x,z8.2)', advance='no') ibits (w%i, 0, 8)
       case (16);  write (6, '(1x,z8.4)', advance='no') ibits (w%i, 0,16)
       case (24);  write (6, '(1x,z8.6)', advance='no') ibits (w%i, 0,24)
       case (32);  write (6, '(1x,z8.8)', advance='no') ibits (w%i, 0,32)
       end select
    end if
    if (nl) write (u, *)
  end subroutine word32_write_unit

  function word_not (w1) result (w2)
    type(word32_t), intent(in) :: w1
    type(word32_t) :: w2
    w2 = not (w1%i)
  end function word_not

  function word_or (w1, w2) result (w3)
    type(word32_t), intent(in) :: w1, w2
    type(word32_t) :: w3
    w3 = ior (w1%i, w2%i)
  end function word_or

  function word_eor (w1, w2) result (w3)
    type(word32_t), intent(in) :: w1, w2
    type(word32_t) :: w3
    w3 = ieor (w1%i, w2%i)
  end function word_eor

  function word_and (w1, w2) result (w3)
    type(word32_t), intent(in) :: w1, w2
    type(word32_t) :: w3
    w3 = iand (w1%i, w2%i)
  end function word_and

  function word_shftc (w1, s) result (w2)
    type(word32_t), intent(in) :: w1
    integer, intent(in) :: s
    type(word32_t) :: w2
    w2 = ishftc (w1%i, s, 32)
  end function word_shftc

  function word_add (w1, w2) result (w3)
    type(word32_t), intent(in) :: w1, w2
    type(word32_t) :: w3
    w3 = w1%i + w2%i
  end function word_add

  subroutine word64_set_from_i64 (ww, i)
    type(word64_t), intent(out) :: ww
    integer(i64), intent(in) :: i
    ww%i = i
  end subroutine word64_set_from_i64

  subroutine word64_set_from_word32 (ww, w)
    type(word64_t), intent(out) :: ww
    type(word32_t), intent(in) :: w
    if (w%i >= 0_i32) then
       ww%i = w%i
    else
       ww%i = 2_i64*(huge(0_i32)+1_i64) + w%i
    end if
  end subroutine word64_set_from_word32

  function byte_from_word64 (ww, i) result (b)
    type(word64_t), intent(in) :: ww
    integer, intent(in) :: i
    type(byte_t) :: b
    integer(i64) :: j
    j = 0
    if (i >= 0 .and. i*8 < 64) then
       call mvbits (ww%i, i*8, 8, j, 0)
    end if
    b%i = j
  end function byte_from_word64

  function word32_from_word64 (ww, i) result (w)
    type(word64_t), intent(in) :: ww
    integer, intent(in) :: i
    type(word32_t) :: w
    integer(i64) :: j
    j = 0
    select case (i)
    case (0);  call mvbits (ww%i,  0, 32, j, 0)
    case (1);  call mvbits (ww%i, 32, 32, j, 0)
    end select
    w = int (j, kind=i32)
  end function word32_from_word64

  subroutine word64_write_unit (u, ww, words, bytes, decimal, newline)
    integer, intent(in) :: u
   type(word64_t), intent(in) :: ww
    logical, intent(in), optional :: words, bytes, decimal, newline
    logical :: wo, by, dc, nl
    wo = .false.;  if (present (words))    wo = words
    by = .false.;  if (present (bytes))    by = bytes
    dc = .false.;  if (present (decimal))  dc = decimal
    nl = .false.;  if (present (newline))  nl = newline
    if (wo .or. by) then
       call word32_write_unit (u, word32_from_word64 (ww, 0), by, dc)
       write (u, '(2x)', advance='no')
       call word32_write_unit (u, word32_from_word64 (ww, 1), by, dc)
    else if (dc) then
       write (u, '(I19)', advance='no') ww%i
    else
       write (u, '(Z16)', advance='no') ww%i
    end if
    if (nl) write (u, *)
  end subroutine word64_write_unit


end module bytes
