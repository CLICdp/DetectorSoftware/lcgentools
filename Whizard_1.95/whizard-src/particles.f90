! WHIZARD 1.95 Feb 25 2010
! 
! (C) 1999-2008 by 
!     Wolfgang Kilian <wolfgang.kilian@desy.de>
!     Thorsten Ohl <ohl@physik.uni-wuerzburg.de>
!     Juergen Reuter <juergen.reuter@physik.uni-freiburg.de>
!
! WHIZARD is free software; you can redistribute it and/or modify it
! under the terms of the GNU General Public License as published by 
! the Free Software Foundation; either version 2, or (at your option)
! any later version.
!
! WHIZARD is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! This file has been stripped of most comments.  For documentation, refer
! to the source 'whizard.nw'

module particles

  use kinds, only: default !NODEP!
  use limits, only: PRT_NAME_LEN
  use lorentz
  use parameters, only: parameter_set !NODEP!

  implicit none
  private

  type, public :: particle
     private
     integer :: code
     real(kind=default) :: mass
     type(four_momentum) :: p
  end type particle

  public :: particle_new
  public :: particle_set_momentum
  public :: particle_set_energy
  public :: particle_set_mass
  public :: particle_reset_mass
  public :: write
  public :: operator(*)
  public :: operator(+)
  public :: sum
  public :: set
  public :: particle_code
  public :: particle_four_momentum
  public :: particle_three_momentum
  public :: particle_energy
  public :: particle_absolute_momentum
  public :: particle_mass
  public :: particle_name
  public :: particle_tex_form
  public :: particle_width
  public :: particle_color
  public :: particle_3charge
  public :: particle_charge
  public :: particle_isospin
  public :: particle_spin_type
  public :: particle_multiplicity
  public :: unpolarized_density_matrix
  public :: is_parton
  public :: visible
  public :: has_antiparticle
  public :: is_antiparticle
  public :: antiparticle_code

  type(particle), parameter, public :: particle_null = &
       & particle( 0, 0._default, four_momentum_null )
  integer, parameter, public :: SCALAR=1, FERMION=2, VECTOR_BOSON=3, &
        GRAVITINO=4, TENSOR=5
  integer, parameter, public :: UNKNOWN=0
include "prt_codes.inc"
  integer, parameter, public :: PROTON=2212, PION=211
  integer, parameter, public :: &
       & HADRON_REMNANT=90, KEYSTONE=98, COMPOSITE=99, UNDEFINED=0
       
  interface particle_new
     module procedure particle_new_mp
     module procedure particle_new_mp_three
     module procedure particle_new_p
     module procedure particle_new_canonical
     module procedure particle_new_mass
     module procedure particle_new_code
  end interface
  interface particle_set_momentum
     module procedure particle_set_momentum_four
     module procedure particle_set_momentum_three
  end interface

  interface write
     module procedure particle_write_unit
  end interface
  interface operator(*)
     module procedure prod_LT_prt
     module procedure prod_prt_prt
  end interface
  interface operator(+)
     module procedure add_prt_prt
  end interface
  interface sum
     module procedure sum_prt
  end interface
  interface set
     module procedure particle_set_p
     module procedure particle_set_code
  end interface
  interface particle_code
     module procedure particle_code_single, particle_code_multi
  end interface
  interface particle_four_momentum
     module procedure particle_four_momentum_multi
     module procedure particle_four_momentum_single
  end interface
  interface particle_three_momentum
     module procedure particle_three_momentum_multi, particle_three_momentum_single
  end interface
  interface particle_energy
     module procedure particle_energy_multi, particle_energy_single
  end interface
  interface particle_absolute_momentum
     module procedure particle_absolute_momentum_multi
     module procedure particle_absolute_momentum_single
  end interface
  interface particle_mass
     module procedure particle_mass_single, particle_mass_multi
  end interface
  interface particle_code
     module procedure particle_code_name
  end interface
  interface particle_mass
     module procedure particle_mass_code, particle_mass_code_array
  end interface
  interface particle_width
     module procedure particle_width_code
  end interface
  interface particle_color
     module procedure particle_color_code
     module procedure particle_color_get
  end interface
  interface particle_3charge
     module procedure particle_3charge_code
     module procedure particle_3charge_get
  end interface
  interface particle_charge
     module procedure particle_charge_code
     module procedure particle_charge_get
  end interface
  interface particle_isospin
     module procedure particle_isospin_code
     module procedure particle_isospin_get
  end interface
  interface particle_spin_type
     module procedure particle_spin_type_code
  end interface
  interface particle_multiplicity
     module procedure particle_multiplicity_code
  end interface
  interface is_parton
     module procedure is_parton_multi, is_parton_single
  end interface
  interface visible
     module procedure visible_multi, visible_single
  end interface
  interface has_antiparticle
     module procedure has_antiparticle_single, has_antiparticle_multi
  end interface
  interface is_antiparticle
     module procedure is_antiparticle_single, is_antiparticle_multi
  end interface
  interface antiparticle_code
     module procedure antiparticle_code_single, antiparticle_code_multi
  end interface

contains

  function particle_new_mp (code, mass, p) result(prt)
    integer, intent(in) :: code
    real(kind=default), intent(in) :: mass
    type(four_momentum), intent(in) :: p
    type(particle) :: prt
    prt%code = code
    prt%mass = mass
    prt%p    = p
  end function particle_new_mp
  function particle_new_mp_three (code, mass, p) result(prt)
    integer, intent(in) :: code
    real(kind=default), intent(in) :: mass
    type(three_momentum), intent(in) :: p
    type(particle) :: prt
    prt%code = code
    prt%mass = mass
    prt%p    = four_momentum_moving (energy (p, mass), p)
  end function particle_new_mp_three
  function particle_new_p (code, p) result(prt)
    integer, intent(in) :: code
    type(four_momentum), intent(in) :: p
    type(particle) :: prt
    prt%code = code
    prt%mass = p**1
    prt%p    = p
  end function particle_new_p
  function particle_new_canonical (code, p, k, mass) result(prt)
    integer, intent(in) :: code
    real(kind=default), intent(in) :: p
    integer, intent(in) :: k
    real(kind=default), intent(in), optional :: mass
    type(particle) :: prt
    prt%code = code
    if (present(mass)) then
       prt%mass = mass
    else
       prt%mass = 0
    end if
    prt%p    = four_momentum_moving (energy(p, mass), p, k)
  end function particle_new_canonical
  function particle_new_mass(code, mass) result(prt)
    integer, intent(in) :: code
    real(kind=default), intent(in) :: mass
    type(particle) :: prt
    prt%code = code
    prt%mass = mass
    prt%p    = four_momentum_at_rest(mass)
  end function particle_new_mass
  function particle_new_code(code, par) result(prt)
    integer, intent(in) :: code
    type(parameter_set), intent(in) :: par
    type(particle) :: prt
    prt = particle_new_mass( code, particle_mass(code, par) )
  end function particle_new_code

  subroutine particle_set_momentum_four (prt, p)
    type(particle), intent(inout) :: prt
    type(four_momentum), intent(in) :: p
    prt%p = p
  end subroutine particle_set_momentum_four

  subroutine particle_set_momentum_three (prt, p)
    type(particle), intent(inout) :: prt
    type(three_momentum), intent(in) :: p
    prt%p = four_momentum_moving (energy (prt%p), p)
  end subroutine particle_set_momentum_three

  subroutine particle_set_energy (prt, E)
    type(particle), intent(inout) :: prt
    real(default), intent(in) :: E
    prt%p = four_momentum_moving (E, space_part (prt%p))
  end subroutine particle_set_energy

  subroutine particle_set_mass (prt, m)
    type(particle), intent(inout) :: prt
    real(default), intent(in) :: m
    prt%mass = m
  end subroutine particle_set_mass

  subroutine particle_reset_mass (prt)
    type(particle), intent(inout) :: prt
    prt%mass = prt%p ** 1
  end subroutine particle_reset_mass

  subroutine particle_write_unit(u,prt)
    integer, intent(in) :: u
    type(particle), intent(in) :: prt
    character(len=PRT_NAME_LEN) :: prt_name
    prt_name = particle_name(prt%code)
    write(u,*) 'Particle type: ', prt_name
    write(u,*) 'M =', prt%mass
    call write(u,prt%p)
  end subroutine particle_write_unit
  function prod_LT_prt(L, prt0) result(prt)
    type(lorentz_transformation), intent(in) :: L
    type(particle), intent(in) :: prt0
    type(particle) :: prt
    prt = particle( prt0%code, prt0%mass, L * prt0%p )
  end function prod_LT_prt
  function prod_prt_prt(prt1, prt2) result(s)
    type(particle), intent(in) :: prt1, prt2
    real(kind=default) :: s
    s = prt1%p * prt2%p
  end function prod_prt_prt
    
  function add_prt_prt (prt1, prt2) result(prt)
    type(particle), intent(in) :: prt1, prt2
    type(particle) :: prt
    prt = particle_new_p (code = COMPOSITE,  p = prt1%p + prt2%p)
  end function add_prt_prt

  function sum_prt (prt_array) result (prt)
    type(particle), dimension(:), intent(in) :: prt_array
    type(particle) :: prt
    prt = particle_new_p (code = COMPOSITE, p = sum (prt_array%p))
  end function sum_prt

  subroutine particle_set_p(prt, p)
    type(particle), intent(inout) :: prt
    type(four_momentum), intent(in) :: p
    prt%p = p
  end subroutine particle_set_p
  subroutine particle_set_code(prt, code)
    type(particle), intent(inout) :: prt
    integer, intent(in) :: code
    prt%code = code
  end subroutine particle_set_code
  function particle_code_multi (prt) result (code)
    type(particle), dimension(:), intent(in) :: prt
    integer, dimension(size(prt)) :: code
    integer :: i
    do i=1, size(prt)
       code(i) = particle_code_single (prt(i))
    end do
  end function particle_code_multi
  function particle_code_single (prt) result (code)
    type(particle), intent(in) :: prt
    integer :: code
    code = prt%code
  end function particle_code_single
  function particle_four_momentum_multi (prt) result (p)
    type(particle), dimension(:), intent(in) :: prt
    type(four_momentum), dimension(size(prt)) :: p
    integer :: i
    do i=1, size(prt)
       p(i) = particle_four_momentum_single (prt(i))
    end do
  end function particle_four_momentum_multi
  function particle_four_momentum_single (prt) result (p)
    type(particle), intent(in) :: prt
    type(four_momentum) :: p
    p = prt%p
  end function particle_four_momentum_single
  function particle_three_momentum_multi (prt) result (p)
    type(particle), dimension(:), intent(in) :: prt
    type(three_momentum), dimension(size(prt)) :: p
    integer :: i
    do i=1, size(prt)
       p(i) = particle_three_momentum_single (prt(i))
    end do
  end function particle_three_momentum_multi
  function particle_three_momentum_single (prt) result (p)
    type(particle), intent(in) :: prt
    type(three_momentum) :: p
    p = space_part (prt%p)
  end function particle_three_momentum_single
  function particle_energy_multi (prt) result (E)
    type(particle), dimension(:), intent(in) :: prt
    real(kind=default), dimension(size(prt)) :: E
    integer :: i
    do i=1, size(prt)
       E(i) = particle_energy_single (prt(i))
    end do
  end function particle_energy_multi
  function particle_energy_single (prt) result (E)
    type(particle), intent(in) :: prt
    real(kind=default) :: E
    E = energy (prt%p)
  end function particle_energy_single
  function particle_absolute_momentum_multi (prt) result (pabs)
    type(particle), dimension(:), intent(in) :: prt
    real(kind=default), dimension(size(prt)) :: pabs
    integer :: i
    do i=1, size(prt)
       pabs(i) = particle_absolute_momentum_single (prt(i))
    end do
  end function particle_absolute_momentum_multi
  function particle_absolute_momentum_single (prt) result (pabs)
    type(particle), intent(in) :: prt
    real(kind=default) :: pabs
    pabs = absolute_momentum (prt%p)
  end function particle_absolute_momentum_single
  function particle_mass_multi (prt) result (mass)
    type(particle), dimension(:), intent(in) :: prt
    real(kind=default), dimension(size(prt)) :: mass
    integer :: i
    do i=1, size(prt)
       mass(i) = particle_mass_single (prt(i))
    end do
  end function particle_mass_multi
  function particle_mass_single (prt) result (mass)
    type(particle), intent(in) :: prt
    real(kind=default) :: mass
    mass = prt%mass
  end function particle_mass_single
  function particle_name (code) result (name)
    integer, intent(in) :: code
    character(len=PRT_NAME_LEN) :: name
    select case (abs(code))
  include "prt_assignments.inc"
    case(PROTON);         name = 'p'
    case(PION);           name = 'pi'
    case(KEYSTONE);       name = 'KEYSTONE'
    case(HADRON_REMNANT); name = 'HADRON_REMNANT'
    case(COMPOSITE);      name = 'COMPOSITE'
    case(UNDEFINED);      name = 'UNDEFINED'
    case default;  name = "UNDEFINED"
    end select
    if (code<0) then
       name = 'a-'//name
    end if
  end function particle_name
  function particle_tex_form (code) result (name)
    integer, intent(in) :: code
    character(len=PRT_NAME_LEN) :: name
    select case (code)
  include "prt_texforms.inc"
    case(PROTON);         name = 'p'
    case(-PROTON);        name = 'p~'
    case(PION);           name = 'pi'
    case(-PION);          name = 'pi~'
    case default;        name = ""
    end select
  end function particle_tex_form
  function particle_code_name (string) result (code)
    character(len=*), intent(in) :: string
    character(len=PRT_NAME_LEN) :: name
    integer :: code
    name = string
    select case (name)
  include "prt_names.inc"
    case('p     ');            code = PROTON
    case('a-p   ', 'p~    ');  code = -PROTON
    case('pi    ');            code = PION
    case('a-pi  ', 'pi~   ');  code = -PION
    case default;    code = UNDEFINED
    end select
  end function particle_code_name

  pure function particle_mass_code (code, par) result (mass)
    integer, intent(in) :: code
    type(parameter_set), intent(in) :: par
    real(kind=default) :: mass
    select case(abs(code))
  include "prt_masses.inc"
    case default;        mass = 0
    end select
  end function particle_mass_code
  pure function particle_mass_code_array (code, par) result (mass)
    integer, dimension(:), intent(in) :: code
    type(parameter_set), intent(in) :: par
    real(kind=default), dimension(size(code)) :: mass
    integer :: i
    forall (i = 1:size(code))
       mass(i) = particle_mass_code (code(i), par)
    end forall
  end function particle_mass_code_array
  function particle_width_code (code, par) result (width)
    integer, intent(in) :: code
    type(parameter_set), intent(in) :: par
    real(kind=default) :: width
    select case(abs(code))
  include "prt_widths.inc"
    case default;        width = 0
    end select
  end function particle_width_code
  function particle_color_code (code) result (color)
    integer, intent(in) :: code
    integer :: color
    select case(abs(code))
  include "prt_colors.inc"
    case default;        color = 0
    end select
    if (is_antiparticle (code)) color = -color
  end function particle_color_code
  function particle_color_get (prt) result (color)
    type(particle), intent(in) :: prt
    integer :: color
    color = particle_color_code (particle_code(prt))
  end function particle_color_get

  function particle_3charge_code (code) result (charge)
    integer, intent(in) :: code
    integer :: charge
    select case(abs(code))
  include "prt_charges.inc"
    case default;  charge = 0
    end select
    if (is_antiparticle (code)) charge = -charge
  end function particle_3charge_code
  function particle_3charge_get (prt) result (charge)
    type(particle), intent(in) :: prt
    integer :: charge
    charge = particle_3charge_code (particle_code(prt))
  end function particle_3charge_get

  function particle_charge_code (code) result (charge)
    integer, intent(in) :: code
    real(kind=default) :: charge
    charge = particle_3charge_code (code) / 3._default
  end function particle_charge_code
  function particle_charge_get (prt) result (charge)
    type(particle), intent(in) :: prt
    real(kind=default) :: charge
    charge = particle_charge_code (particle_code(prt))
  end function particle_charge_get

  function particle_isospin_code (code) result (isospin)
    integer, intent(in) :: code
    integer :: isospin
    select case(abs(code))
  include "prt_isospins.inc"
    case default;  isospin = 0
    end select
  end function particle_isospin_code
  function particle_isospin_get (prt) result (isospin)
    type(particle), intent(in) :: prt
    integer :: isospin
    isospin = particle_isospin_code (particle_code(prt))
  end function particle_isospin_get

  function particle_spin_type_code (code) result (spin)
    integer, intent(in) :: code
    integer :: spin
    select case(abs(code))
  include "prt_spin.inc"
    case(PROTON)
       spin = FERMION
    case(PION)
       spin = SCALAR
    case default
       spin = UNKNOWN
    end select
  end function particle_spin_type_code

  function particle_multiplicity_code (code) result (multiplicity)
    integer, intent(in) :: code
    integer :: multiplicity
    select case (abs(code))
  include "prt_multiplicities.inc"
    case default
       multiplicity = particle_spin_type (code)
    end select
  end function particle_multiplicity_code

  function unpolarized_density_matrix (code) result (rho)
    integer, intent(in) :: code
    complex(kind=default), dimension(-2:2,-2:2) :: rho
    rho = 0
    select case (particle_multiplicity (code))
    case (2)
       rho(-1,-1) = 1._default / 2
       rho( 1, 1) = 1._default / 2
    case (3)
       rho(-1,-1) = 1._default / 3
       rho( 0, 0) = 1._default / 3
       rho( 1, 1) = 1._default / 3
    case (4)
       rho(-2,-2) = 1._default / 4
       rho(-1,-1) = 1._default / 4
       rho( 1, 1) = 1._default / 4
       rho( 2, 2) = 1._default / 4
    case (5)
       rho(-2,-2) = 1._default / 5
       rho(-1,-1) = 1._default / 5
       rho( 0, 0) = 1._default / 5
       rho( 1, 1) = 1._default / 5
       rho( 2, 2) = 1._default / 5
    case default
       rho(0,0) = 1
    end select
  end function unpolarized_density_matrix

  function is_parton_multi (code) result (ok)
    integer, dimension(:), intent(in) :: code
    logical, dimension(size(code)) :: ok
    integer :: i
    do i=1, size(code)
       ok(i) = is_parton_single (code(i))
    end do
  end function is_parton_multi

  function is_parton_single (code) result (ok)
    integer, intent(in) :: code
    logical :: ok
    select case (abs(code))
  include "prt_parton.inc"
    case default
       ok = .false.
    end select
  end function is_parton_single

  function visible_multi (code) result (ok)
    integer, dimension(:), intent(in) :: code
    logical, dimension(size(code)) :: ok
    integer :: i
    do i=1, size(code)
       ok(i) = visible_single (code(i))
    end do
  end function visible_multi

  function visible_single (code) result (ok)
    integer, intent(in) :: code
    logical :: ok
    ok = (particle_3charge(code) /= 0) .or. (particle_color(code) /= 0 .or. &
         & code == PHOTON)
  end function visible_single

  function has_antiparticle_multi (code) result (has_antiparticle)
    integer, dimension(:), intent(in) :: code
    logical, dimension(size(code)) :: has_antiparticle
    integer :: i
    do i = 1, size (code)
       has_antiparticle(i) = has_antiparticle_single (code(i))
    end do
  end function has_antiparticle_multi

  function has_antiparticle_single (code) result (has_antiparticle)
    integer, intent(in) :: code
    logical :: has_antiparticle
    select case (abs(code))
  include "prt_antiparticle.inc"
    case(PROTON, PION)
       has_antiparticle = .true.
    case default
       has_antiparticle = .false.
    end select
  end function has_antiparticle_single

  function is_antiparticle_multi (code) result (is_antiparticle)
    integer, dimension(:), intent(in) :: code
    logical, dimension(size(code)) :: is_antiparticle
    integer :: i
    do i = 1, size (code)
       is_antiparticle(i) = (code(i) < 0)
    end do
  end function is_antiparticle_multi

  function is_antiparticle_single (code) result (is_antiparticle)
    integer, intent(in) :: code
    logical :: is_antiparticle
    is_antiparticle = (code < 0)
  end function is_antiparticle_single

  function antiparticle_code_multi (code) result (acode)
    integer, dimension(:), intent(in) :: code
    integer, dimension(size(code)) :: acode
    integer :: i
    do i = 1, size (code)
       acode(i) = antiparticle_code_single (code(i))
    end do
  end function antiparticle_code_multi

  function antiparticle_code_single (code) result (acode)
    integer, intent(in) :: code
    integer :: acode
    if (has_antiparticle (code)) then
       acode = -code
    else
       acode = code
    end if
  end function antiparticle_code_single


end module particles
