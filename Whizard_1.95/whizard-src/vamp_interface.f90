! WHIZARD 1.95 Feb 25 2010
! 
! (C) 1999-2008 by 
!     Wolfgang Kilian <wolfgang.kilian@desy.de>
!     Thorsten Ohl <ohl@physik.uni-wuerzburg.de>
!     Juergen Reuter <juergen.reuter@physik.uni-freiburg.de>
!
! WHIZARD is free software; you can redistribute it and/or modify it
! under the terms of the GNU General Public License as published by 
! the Free Software Foundation; either version 2, or (at your option)
! any later version.
!
! WHIZARD is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! This file has been stripped of most comments.  For documentation, refer
! to the source 'whizard.nw'

module vamp_interface

  use kinds, only: default !NODEP!
  use kinds, only: double !NODEP!

  use density_vectors, only: state_density_t, compute_state_density, select_state
  use density_vectors, only: nullify
  use user, only: user_weight => weight
  use monte_carlo_states, only: monte_carlo_state, mcs, mc_process_index
  use monte_carlo_states, only: write
  use monte_carlo_states, only: current_event, rng
  use monte_carlo_states, only: monte_carlo_input
  use monte_carlo_states, only: checksum_error
  use monte_carlo_states, only: mc_get_accuracy, mc_get_efficiency
!  use monte_carlo_states, only: mc_get_efficiency_vector
  use monte_carlo_states, only: mc_write_result, mc_read_result
  use monte_carlo_states, only: mc_write_header, mc_write_hline
  use cuts, only: read, write, apply, mpi90_broadcast
  use partons, only: structure_functions
  use phase_space, only: event_fill
  use events, only: event, write
  use events, only: event_set_beams, event_in_state_from_partons, set_out_state
  use events, only: destroy_rho_in, event_reset_shower
  use showers, only: shower_parameters_t, pair_shower_generate
  use particles, only: particle_four_momentum, particle_code, particle_mass, set
  use lorentz, only: array
  use clock, only: time, time_current, time_null
  use clock, only: time_to_seconds, time_from_seconds
  use clock, only: operator(+), operator(-), operator(>), write
  use files, only: choose_filename, concat, file_exists_else_default
  use diagnostics, only: msg_buffer, msg_level, msg_fatal, msg_bug
  use diagnostics, only: msg_message, msg_result, msg_warning, msg_error
  use diagnostics, only: terminate_soon, terminate_now_maybe, TIME_EXCEEDED
  use limits, only: WHIZARD_ROOT, VERSION_STRLEN, VERSION_STRING
  use limits, only: FILENAME_LEN, GRID_EXTENSION_LENGTH

  use decay_forests, only: decay_forest, equivalences
  use vamp, only: vamp_grids, vamp_create_grids !NODEP!
  use vamp, only: vamp_read_grids, vamp_write_grids !NODEP!
  use vamp, only: vamp_read_grids_raw, vamp_write_grids_raw !NODEP!
  use vamp, only: vamp_copy_grids, vamp_discard_integrals !NODEP!
  use vamp, only: vamp_sample_grids !NODEP!
  use vamp, only: vamp_refine_grids, vamp_update_weights !NODEP!
  use vamp, only: vamp_next_event, vamp_probability, vamp_get_variance !NODEP!
  use vamp, only: vamp_equivalence_list, create, write !NODEP!
  use vamp, only: vamp_equivalence_set, vamp_equivalences_update !NODEP!
  use vamp, only: IDENTITY, INVERT, SYMMETRIC, INVARIANT !NODEP!
  use exceptions, only: exception, clear_exception, EXC_ERROR !NODEP!
  use permutations, only: permute

  use tao_random_numbers, only: tao_random_state, tao_random_number !NODEP!
  use tao_random_numbers, only: tao_random_seed !NODEP!
  use mpi90, only: mpi90_rank, mpi90_broadcast, vamp_broadcast_grids !NODEP!
  use file_utils, only: free_unit !NODEP!

  implicit none
  private

  public :: mc_random_number
  public :: mc_integrate
  public :: mc_show_time_estimate
  public :: mc_enable_grid
  public :: mc_generate 
  private :: mc_sample
  public :: mc_recalculate

  integer, parameter :: MC_GRID_MAGIC_NUMBER = 7777777

contains

  subroutine mc_random_number (r)
    real(kind=double) :: r
    call tao_random_number (rng, r)
  end subroutine mc_random_number

  subroutine mc_setup_equivalences (vamp_eq, f, azimuthal_dependence)
    type(vamp_equivalence_list), intent(inout) :: vamp_eq
    type(decay_forest), intent(in) :: f
    logical, intent(in) :: azimuthal_dependence
    type(equivalences), pointer :: eq
    integer :: n_dim_masses, n_dim_angles, n_dim_momenta, n_dim_extra
    integer :: i, j
    integer, dimension(f%n_dim_integration) :: perm, mode
    if (f%n_eq /= vamp_eq%n_eq) &
         & call msg_bug (" Mismatch in VAMP equivalence list length")
    n_dim_masses  = f%n_dim_masses
    n_dim_angles  = f%n_dim_angles
    n_dim_momenta = f%n_dim_momenta
    n_dim_extra   = f%n_dim_extra
    eq => f%eq
    do i=1, f%n_eq
       if (.not. associated (eq))  call msg_bug &
            & (" Invalid pointer in equivalence list")
       do j=1, n_dim_masses
          perm(j) = permute (j, eq%msq_perm)
          mode(j) = IDENTITY
       end do
       do j=1, n_dim_angles
          perm(j+n_dim_masses) = permute (j, eq%angle_perm) + n_dim_masses
          select case (mod(j,2))
          case (1)
             if (azimuthal_dependence) then
                mode(j+n_dim_masses) = IDENTITY
             else if (j==1) then
                mode(j+n_dim_masses) = INVARIANT
             else
                mode(j+n_dim_masses) = SYMMETRIC
             end if
          case (0)
             if (eq%angle_sig(j)) then
                mode(j+n_dim_masses) = IDENTITY
             else
                mode(j+n_dim_masses) = INVERT
             end if
          end select
       end do
       do j=1, n_dim_extra
          perm(j+n_dim_momenta) = j + n_dim_momenta
          if (any (f%generated_externally == j)) then
             mode(j+n_dim_momenta) = INVARIANT
          else
             mode(j+n_dim_momenta) = IDENTITY
          end if
       end do
       call vamp_equivalence_set (vamp_eq%eq(i), eq%left, eq%right, perm, mode)
       eq => eq%next
    end do
    call vamp_equivalences_update (vamp_eq)
  end subroutine mc_setup_equivalences

  subroutine mc_integrate (mcs, mci, default_filename, time_per_event)

    type(monte_carlo_state), intent(inout) :: mcs
    type(monte_carlo_input), intent(in) :: mci
    character(len=*), intent(in) :: default_filename
    real(kind=default), intent(out) :: time_per_event
    character(len=FILENAME_LEN) :: &
         & write_grids_file_current, read_grids_file_current, &
         & write_all_grids_file_current, &
         & read_cuts_file, write_logfile_file
    logical :: grids_exist, grids_raw

    type result
       real(kind=default) :: integral, error, chi2, efficiency, accuracy
    end type result

    integer :: proc_id
    type(result) :: best
    integer :: it_best, it_file

    call mpi90_rank (proc_id)
    mcs%it = 0
    mcs%it_vamp = 0
    write_grids_file_current = &
         & concat ( "", &
         &          choose_filename (mcs%write_grids_file, default_filename), &
         &          mcs%process_id_current)
    write_all_grids_file_current = &
         & concat ( "", &
         &          choose_filename (mcs%write_all_grids_file, default_filename), &
         &          mcs%process_id_current)
    read_grids_file_current = &
         & concat ( "", &
         &          choose_filename (mcs%read_grids_file, default_filename), &
         &          mcs%process_id_current)
    read_cuts_file = &
         & choose_filename (mcs%read_cuts_file, default_filename)
    write_logfile_file = &
         & choose_filename (mcs%write_logfile_file, default_filename)
    best = result (0._default, 0._default, 0._default, &
         &         0._default, huge(1._default)) 

    call integrate_create_grids (mcs%n_channels, mcs%calls(2,1))
    call integrate_read_grids &
         & (mcs%directory, read_grids_file_current, &
         &  it_best, it_file, grids_exist, grids_raw)
    if (grids_exist .and. (grids_raw .neqv. mcs%write_grids_raw)) then
       call integrate_write_grids &
            & (mcs%directory, write_grids_file_current, &
            &  write_all_grids_file_current, &
            &  it_file, it_best)
    end if
    if (mcs%reset_seed_each_process)  call tao_random_seed (rng, mcs%seed)
    call mc_write_header &
         & (6, mcs%n_in, mcs%process_id_current, mcs%process_factor)
    call integrate_fixed_weights (1)
    call integrate_variable_weights (2)
    call integrate_fixed_weights (3, time_per_event)
    call integrate_warmup (4)

  contains

    subroutine integrate_fixed_weights (pass, time_per_event)
      integer, intent(in) :: pass
      real(kind=default), intent(out), optional :: time_per_event
      type(time) :: time0, time1
      integer :: it_initial, it_final
      logical :: improved_grid
      real(kind=default) :: tmp
      character(len=80) :: buffer
      type(exception) :: exc
      mcs%adaptation_start_time = time_current ()
      mcs%adaptation_stop_time = &
           & mcs%adaptation_start_time + time (0,0,mcs%time_limit_adaptation,0,0)
      it_initial = mcs%it
      it_final   = it_initial + 1
      call vamp_copy_grids (mcs%g, mcs%g_best)
      call integrate_read_cuts (mcs%directory, read_cuts_file, pass)
      if (it_file < it_final) then
         call vamp_discard_integrals &
              & (mcs%g, mcs%calls(2,pass), stratified=mcs%stratified, &
              &  eq = mcs%eq)
         if (pass==1) then
            buffer = "Preparing (fixed weights):"
         else
            buffer = "Integrating (fixed wgts.):"
         end if
         if (mcs%calls(1,pass)==1) then
            write(msg_buffer, "(1x,A,1x,I3,1x,A,1x,I10,1x,A)") &
                 & trim(buffer), mcs%calls(1,pass), "sample of", &
                 & mcs%calls(2,pass), "calls ..."
         else
            write(msg_buffer, "(1x,A,1x,I3,1x,A,1x,I10,1x,A)") &
                 & trim(buffer), mcs%calls(1,pass), "samples of", &
                 & mcs%calls(2,pass), "calls ..."
         end if
         call msg_message
         time0 = time_current ()
         call clear_exception (exc)
         call vamp_sample_grids &
              & (rng, mcs%g, mc_sample, mcs%calls(1, pass), &
              &  integral=mcs%integral, std_dev=mcs%error, avg_chi2=mcs%chi2, &
              &  history=mcs%history(mcs%it_vamp+1:), &
              &  histories=mcs%histories(mcs%it_vamp+1:,:), &
              &  eq=mcs%eq, exc=exc, warn_error=mcs%warn_empty_channel)
         if (exc%level >= EXC_ERROR) &
              & call msg_fatal (trim(" "//exc%message))
         time1 = time_current ()
         mcs%actual_integration_done = .true.
         mcs%integral = mcs%integral * mcs%process_factor
         mcs%error = mcs%error * mcs%process_factor
         mcs%efficiency = mc_get_efficiency (mcs%g)
         mcs%accuracy = mc_get_accuracy &
              & (mcs%integral, mcs%error, product(mcs%calls(:,pass)))
         call integrate_update_best_grid (it_final, improved_grid, it_best)
         call mc_write_result &
              & (mcs%logtable(it_final), &
              &  it_final, product(mcs%calls(:,pass)), &
              &  mcs%integral, mcs%error, mcs%efficiency, mcs%accuracy, &
              &  use_efficiency=mcs%use_efficiency, improved=improved_grid, &
              &  chi2=mcs%chi2, iterations=mcs%calls(1,pass))
         call integrate_write_grids &
              & (mcs%directory, write_grids_file_current, &
              &  write_all_grids_file_current, it_final, it_best)
      else
         call msg_message (" Using grids and results from file:")
         time0 = time_null
         time1 = time_null
         improved_grid = .false.
      end if
      if (present(time_per_event)) then
         tmp = product(mcs%calls(:,pass)) * best%efficiency
         if (tmp > 0) then
            time_per_event = time_to_seconds (time1 - time0) / tmp
         else
            time_per_event = 0
         end if
      end if
      mcs%it = it_final
      mcs%it_vamp = mcs%it_vamp + 1
      mcs%weights (mcs%it,:) = mcs%g%weights
      call write_logfile (trim(write_logfile_file), mcs, mci)
      call msg_result (mcs%logtable(it_final))
      call mc_write_hline (6)
    end subroutine integrate_fixed_weights

    subroutine integrate_variable_weights (pass)
      integer, intent(in) :: pass
      integer :: it_initial, it_final, it_new, it
      logical :: first_old, first_new, sufficient, improved_grid
      character (len=80) :: buffer
      type(exception) :: exc
      it_initial = mcs%it
      it_final = it_initial + mcs%calls(1,pass)
      if (it_file <= 1) then
         it_new = it_final - 1
      else if (it_file <= it_final) then
         it_new = it_final - it_file
      else
         it_new = 0
      end if
      first_old = .true.
      first_new = .true.
      sufficient = .false.
      call vamp_copy_grids (mcs%g, mcs%g_best)
      call integrate_read_cuts (mcs%directory, read_cuts_file, pass)
      call integrate_check_accuracy (sufficient)
      call integrate_check_time_limit (sufficient)
      do it = it_initial+1, it_final
         if (it_file < it) then
            if (.not.sufficient) then
               if (first_new) then
                  buffer = "Adapting (variable wgts.):"
                  if (it_new==1) then
                     write (msg_buffer, "(1x,A,1x,I3,1x,A,1x,I10,1x,A)") &
                          & trim(buffer), it_new, "sample of", &
                          & mcs%calls(2,pass), "calls ..."
                  else
                     write (msg_buffer, "(1x,A,1x,I3,1x,A,1x,I10,1x,A)") &
                          & trim(buffer), it_new, "samples of", &
                          & mcs%calls(2,pass), "calls ..."
                  end if
                  call msg_message
                  call vamp_discard_integrals &
                       & (mcs%g, mcs%calls(2,pass), stratified=mcs%stratified, &
                       &  eq = mcs%eq)
                  first_new = .false.
               end if
               call vamp_refine_grids (mcs%g)
               call integrate_refine_weights (mcs%calls(2,pass))
               call clear_exception (exc)
               call vamp_sample_grids &
                    & (rng, mcs%g, mc_sample, 1, &
                    &  integral=mcs%integral, &
                    &  std_dev=mcs%error, avg_chi2=mcs%chi2, &
                    &  history=mcs%history(mcs%it_vamp+1:), &
                    &  histories=mcs%histories(mcs%it_vamp+1:,:), &
                    &  eq=mcs%eq, exc=exc, warn_error=mcs%warn_empty_channel)
               if (exc%level >= EXC_ERROR) &
                    & call msg_fatal (trim(" "//exc%message))
               mcs%actual_integration_done = .true.
               mcs%integral = mcs%integral * mcs%process_factor
               mcs%error = mcs%error * mcs%process_factor
               mcs%efficiency = mc_get_efficiency (mcs%g)
               mcs%accuracy = mc_get_accuracy &
                    & (mcs%integral, mcs%error, mcs%calls(2,pass))
               call integrate_update_best_grid (it, improved_grid, it_best)
               call mc_write_result &
                    & (mcs%logtable(it), it, mcs%calls(2,pass), &
                    &  mcs%integral, mcs%error, mcs%efficiency, mcs%accuracy, &
                    &  use_efficiency=mcs%use_efficiency, &
                    &  improved=improved_grid)
               call integrate_write_grids &
                    & (mcs%directory, write_grids_file_current, &
                    &  write_all_grids_file_current, it, it_best)
            else
               improved_grid = .false.
            end if
         else
            if (first_old) then
               call msg_message (" Using grids and results from file:")
               first_old = .false.
            end if
            improved_grid = .false.
         end if
         mcs%it = it
         mcs%it_vamp = mcs%it_vamp + 1
         mcs%weights (mcs%it,:) = mcs%g%weights
         call write_logfile (trim(write_logfile_file), mcs, mci)
         if (len_trim (mcs%logtable(it)) > 0) &
              & call msg_result (mcs%logtable(it))
         call integrate_check_accuracy (sufficient)
         call integrate_check_time_limit (sufficient)
      end do
      if (mcs%screen_results .and. proc_id==WHIZARD_ROOT) then
         if ((mcs%accuracy_goal > 0 .or. mcs%efficiency_goal < 100) &
              & .and. .not.sufficient)  call msg_warning &
              & (" Accuracy and/or efficiency goal not reached.")
         call mc_write_hline (6)
      end if
    end subroutine integrate_variable_weights

    subroutine integrate_check_accuracy (sufficient)
      logical, intent(inout) :: sufficient
      if (sufficient)  return
      if (mcs%accuracy <= mcs%accuracy_goal &
           & .and. 100*mcs%efficiency >= mcs%efficiency_goal) then
         call msg_message (" Accuracy and efficiency goals have been reached.")
         sufficient = .true.
      else if (mcs%accuracy <= mcs%accuracy_goal &
           & .and. mcs%efficiency_goal == 100) then
         call msg_message (" Accuracy goal has been reached.")
         sufficient = .true.
      else if (mcs%accuracy_goal == 0 &
           & .and. 100*mcs%efficiency >= mcs%efficiency_goal) then
         call msg_message (" Efficiency goal has been reached.")
         sufficient = .true.
      else
         sufficient = .false.
      end if
    end subroutine integrate_check_accuracy

    subroutine integrate_check_time_limit (sufficient)
      logical, intent(inout) :: sufficient
      if (sufficient)  return
      if (mcs%time_limit_adaptation /= 0) then
         if (time_current () > mcs%adaptation_stop_time) then
            call msg_warning &
                 & (" Time limit exceeded: skipping further grid adaptation iterations.")
            sufficient = .true.
         end if
      end if
    end subroutine integrate_check_time_limit

    subroutine integrate_warmup (pass)
      integer, intent(in) :: pass
      integer :: it_initial, it_final
      logical :: improved_grid
      if (mcs%calls(1,pass) == 0)  return
      it_initial = mcs%it
      it_final = it_initial + 1
      if (it_file < it_final) then
         call vamp_copy_grids (mcs%g, mcs%g_best)
         call vamp_discard_integrals &
              & (mcs%g, mcs%calls(2,pass), stratified=.false.)
         call integrate_read_cuts (mcs%directory, read_cuts_file, pass)
         write (msg_buffer, "(1x,A,1x,I3,1x,A,1x,I10,1x,A)") &
              & "Maximum weight search:     ", &
              & mcs%calls(1,pass), "sample of", &
              & mcs%calls(2,pass), "calls ..."
         call msg_message
         call vamp_sample_grids (rng, mcs%g, mc_sample, 1)
         mcs%actual_integration_done = .true.
         mcs%efficiency = mc_get_efficiency (mcs%g)
         call integrate_update_best_grid &
              & (it_final, improved_grid, it_best, force=.true.)
         call mc_write_result &
              & (mcs%logtable(it_final), &
              &  it_final, product(mcs%calls(:,pass)), &
              &  mcs%integral, mcs%error, mcs%efficiency, mcs%accuracy, &
              &  use_efficiency=mcs%use_efficiency, improved=improved_grid, &
              &  chi2=mcs%chi2, iterations=mcs%calls(1,pass))
         call integrate_write_grids &
              & (mcs%directory, write_grids_file_current, &
              &  write_all_grids_file_current, it_final, it_best)
      else
         call msg_message (" Using grids from file:")
         improved_grid = .false.
      end if
      mcs%it = it_final
      mcs%it_vamp = mcs%it_vamp + 1
      mcs%weights (mcs%it,:) = mcs%g%weights
      call write_logfile (trim(write_logfile_file), mcs, mci)
      call mc_write_result (6, &
           &  it_final, product(mcs%calls(:,pass)), &
           &  mcs%integral, mcs%error, mcs%efficiency, mcs%accuracy, &
           &  use_efficiency=mcs%use_efficiency, improved=improved_grid, &
           &  chi2=mcs%chi2, iterations=mcs%calls(1,pass))
      call mc_write_hline (6)
    end subroutine integrate_warmup

      subroutine integrate_create_grids (n_channels, calls)
        integer, intent(in) :: n_channels, calls
        real(kind=default), dimension(n_channels) :: weights
        real(kind=default), dimension(2,mcs%f%n_dim_integration) :: region
        integer, dimension(mcs%f%n_dim_integration) :: num_div
        integer :: min_calls, num_div_show
        region(1,:) = 0;  region(2,:) = 1
        weights = 1
        min_calls = mcs%min_calls_per_bin * n_channels
        if (min_calls /= 0) then
           num_div = max (mcs%min_bins, &
                & min (calls / min_calls, mcs%max_bins))
        else
           num_div = mcs%max_bins
        end if
        call vamp_create_grids &
             & (mcs%g, region, calls, weights, stratified=mcs%stratified, &
             &  num_div=num_div)
        call vamp_create_grids &
             & (mcs%g_best, region, calls, weights, stratified=mcs%stratified, &
             &  num_div=num_div)
        if (size(num_div) > 0) then
           num_div_show = num_div(1)
        else
           num_div_show = 0
        end if
        call msg_message
        write (msg_buffer, "(1x,A,I7,1x,A,I3,1x,A,I3,1x,A)") &
             & "Created grids:", n_channels, "channels,", &
             & mcs%f%n_dim_integration, "dimensions with", num_div_show, "bins"
        call msg_message
        call create &
             & (mcs%eq, mcs%f%n_eq, mcs%f%n_channels, mcs%f%n_dim_integration)
        call mc_setup_equivalences (mcs%eq, mcs%f, mcs%azimuthal_dependence)
      end subroutine integrate_create_grids

    subroutine integrate_read_grids &
         & (prefix, filename, it_best, it_file, grids_exist, read_grids_raw)
      character(len=*), intent(in) :: prefix, filename
      integer, intent(out) :: it_best, it_file
      logical, intent(out) :: grids_exist, read_grids_raw
      integer :: it_dummy
      character(len=FILENAME_LEN) :: file_grc, file_grb
      read_grids_raw = mcs%read_grids_raw
      if (proc_id == WHIZARD_ROOT) then
         call look_for_grids (read_grids_raw, prefix, filename, &
              &               file_grc, file_grb, grids_exist)
         if (.not.grids_exist) then
            read_grids_raw = .not.read_grids_raw
            call look_for_grids (read_grids_raw, prefix, filename, &
                 &               file_grc, file_grb, grids_exist)
         end if
         if (mcs%read_grids .and. grids_exist) then
            if (mcs%read_grids_force) then
               call mc_read_grids &
                    & (mcs%g_best, file_grb, it_dummy, it_best, &
                    &  best%integral, best%error, &
                    &  best%chi2, best%efficiency, best%accuracy, mcs%logtable, &
                    &  raw = read_grids_raw)
               call mc_read_grids &
                    & (mcs%g, file_grc, it_dummy, it_file, &
                    &  mcs%integral, mcs%error, &
                    &  mcs%chi2, mcs%efficiency, mcs%accuracy, mcs%logtable, &
                    &  raw = read_grids_raw)
            else
               call mc_read_grids &
                    & (mcs%g_best, file_grb, it_dummy, it_best, &
                    &  best%integral, best%error, &
                    &  best%chi2, best%efficiency, best%accuracy, &
                    &  mcs%logtable, mcs%checksum, &
                    &  raw = read_grids_raw)
               call mc_read_grids &
                    & (mcs%g, file_grc, it_dummy, it_file, &
                    &  mcs%integral, mcs%error, &
                    &  mcs%chi2, mcs%efficiency, mcs%accuracy, &
                    &  mcs%logtable, mcs%checksum, &
                    &  raw = read_grids_raw)
            end if
            call vamp_broadcast_grids (mcs%g, WHIZARD_ROOT)
            call vamp_broadcast_grids (mcs%g_best, WHIZARD_ROOT)
         else
            it_best = 0
            it_file = 0
         end if
      end if
      call mpi90_broadcast (best%integral, WHIZARD_ROOT)
      call mpi90_broadcast (best%error, WHIZARD_ROOT)
      call mpi90_broadcast (best%chi2, WHIZARD_ROOT)
      call mpi90_broadcast (best%efficiency, WHIZARD_ROOT)
      call mpi90_broadcast (best%accuracy, WHIZARD_ROOT)
      call mpi90_broadcast (it_best, WHIZARD_ROOT)
      call mpi90_broadcast (it_file, WHIZARD_ROOT)
    end subroutine integrate_read_grids

    subroutine look_for_grids (read_grids_raw, prefix, filename, &
         &                     file_grc, file_grb, grids_exist)
      logical, intent(in) :: read_grids_raw
      character(len=*) :: prefix, filename, file_grc, file_grb
      logical, intent(out) :: grids_exist
      logical :: grc_exist, grb_exist
      if (read_grids_raw) then
         file_grc = concat (prefix, filename, "gxc")
         file_grb = concat (prefix, filename, "gxb")
      else
         file_grc = concat (prefix, filename, "grc")
         file_grb = concat (prefix, filename, "grb")
      end if
      inquire (file = trim(file_grc), exist = grc_exist)
      inquire (file = trim(file_grb), exist = grb_exist)
      if (.not.grc_exist)  file_grc = file_grb
      if (.not.grb_exist)  file_grb = file_grc
      grids_exist = grc_exist .or. grb_exist
    end subroutine look_for_grids

    subroutine integrate_write_grids &
         & (prefix, filename, filename_all, it, it_best)
      character(len=*), intent(in) :: prefix, filename, filename_all
      integer, intent(in) :: it, it_best
      integer, parameter :: itext_len = GRID_EXTENSION_LENGTH
      character(len=FILENAME_LEN) :: file_grc, file_grb, file_grx
      character(len=*), parameter :: &
           & itext_fmt = "(A,A,i" // char(ichar('0')+itext_len) // &
           &             "." // char(ichar('0')+itext_len) // ")"
      if (.not.mcs%write_grids) return
      if (proc_id == WHIZARD_ROOT) then
         if (mcs%write_grids_raw) then
            file_grc = concat (prefix, filename, "gxc")
            file_grb = concat (prefix, filename, "gxb")
         else
            file_grc = concat (prefix, filename, "grc")
            file_grb = concat (prefix, filename, "grb")
         end if
         call mc_write_grids (mcs%g_best, file_grb, it, it_best, &
              & best%integral, best%error, best%chi2, best%efficiency, &
              & best%accuracy, &
              & mcs%logtable(:it), trim(mcs%process_id_current), mcs%checksum, &
              & raw = mcs%write_grids_raw)
         call mc_write_grids (mcs%g, file_grc, it, it, &
              & mcs%integral, mcs%error, mcs%chi2, mcs%efficiency, &
              & mcs%accuracy, &
              & mcs%logtable(:it), trim(mcs%process_id_current), mcs%checksum, &
              & raw = mcs%write_grids_raw)
         if (mcs%write_all_grids) then
            if (filename_all(1:1) == "/") then
               write (file_grx,itext_fmt) trim(filename_all), ".gr", it
            else
               write (file_grx,itext_fmt) &
                    & trim(prefix)//trim(filename_all), ".gr", it
            end if
            call mc_write_grids (mcs%g, file_grx, it, it, &
                 & mcs%integral, mcs%error, mcs%chi2, mcs%efficiency, &
                 & mcs%accuracy, &
                 & mcs%logtable(:it), trim(mcs%process_id_current), mcs%checksum)
         end if
      end if
    end subroutine integrate_write_grids

    subroutine integrate_update_best_grid (it, improved_grid, it_best, force)
      integer, intent(in) :: it
      logical, intent(out) :: improved_grid
      integer, intent(inout) :: it_best
      logical, intent(in), optional :: force
      logical :: check
      check = .true.
      if (present(force)) then
         if (force)  check = .false.
      end if
      if (check) then
         if (mcs%use_efficiency) then
            improved_grid = (mcs%efficiency > best%efficiency)
         else if (mcs%accuracy > 0) then
            improved_grid = (mcs%accuracy < best%accuracy)
         else
            improved_grid = .false.
         end if
      else
         improved_grid = .true.
      end if
      if (improved_grid) then
         call vamp_copy_grids (mcs%g_best, mcs%g)
         best%integral = mcs%integral
         best%error = mcs%error
         best%chi2 = mcs%chi2
         best%efficiency = mcs%efficiency
         best%accuracy = mcs%accuracy
         it_best = it
      end if
    end subroutine integrate_update_best_grid

    subroutine integrate_refine_weights (calls)
      integer, intent(in) :: calls
      real(kind=default), dimension(size(mcs%g%weights)) :: weights
      real(kind=default) :: w, w0
      logical, dimension(size(mcs%g%weights)) :: too_low
      integer :: i, n
      if (mcs%n_channels == 0)  return
      if (mcs%use_efficiency) then
         weights = mcs%g%weights &
              & * mc_get_efficiency_vector(mcs%g, size(mcs%g%grids)) &
              &   ** mcs%weights_power
      else
         weights = mcs%g%weights &
              & * vamp_get_variance (mcs%g%grids) ** mcs%weights_power
      end if
      do i=1, mcs%f%n_groves
         w = sum(weights(mcs%f%grove(i)%first : mcs%f%grove(i)%last)) &
              &       / (mcs%f%grove(i)%last - mcs%f%grove(i)%first + 1)
         weights(mcs%f%grove(i)%first : mcs%f%grove(i)%last) = w
      end do
      if (calls /= 0 .and. any (weights /= 0)) then
         weights = weights / sum (weights)
         too_low = (weights /= 0 .and. &
              &     weights * calls < mcs%min_calls_per_channel)
         if (any (too_low)) then
            w = sum (weights, mask=too_low)
            n = count (too_low)
            w0 = mcs%min_calls_per_channel / real(calls, kind=default)
            where (too_low)
               weights = w0
            elsewhere
               weights = weights * (1 - n * w0) / (1 - w)
            end where
         end if
      end if
      call vamp_update_weights(mcs%g, weights)
    end subroutine integrate_refine_weights

    function mc_get_efficiency_vector (g, s) result(efficiency)
      type(vamp_grids), intent(in) :: g
      integer, intent(in) :: s
      real(kind=default), dimension(s) :: efficiency
      where(g%num_calls >= 2 .and. g%grids%f_max>0)
         efficiency = g%grids%mu(1) / g%grids%f_max
      elsewhere
         efficiency = 0
      end where
    end function mc_get_efficiency_vector

    subroutine write_logfile (filename, mcs, mci)
      character(len=*), intent(in) :: filename
      type(monte_carlo_state), intent(in) :: mcs
      type(monte_carlo_input), intent(in) :: mci
      if (mcs%write_logfile) then
         call write (filename, mcs, mci)
      end if
    end subroutine write_logfile

    subroutine integrate_read_cuts (prefix, filename, pass)
      character(len=*), intent(in) :: prefix, filename
      integer, intent(in) :: pass
      character(len=FILENAME_LEN) :: file
      character(len=4) :: extension
      write (extension, "(A3,I1)")  "cut", pass
      if (proc_id==WHIZARD_ROOT) then
         file = file_exists_else_default (prefix, filename, extension)
         if (len_trim (file) /= 0) then
            call read (trim(file), mcs%cc, trim(mcs%process_id_current), pass)
         else if (pass == 1) then
            call msg_message (" No user cut configuration file found.")
         end if
         if (mcs%screen_results) call write (6, mcs%cc, pass)
      end if
      call mpi90_broadcast (mcs%cc, WHIZARD_ROOT)
    end subroutine integrate_read_cuts


  end subroutine mc_integrate

  subroutine mc_show_time_estimate (time_per_event)
    real(kind=default), intent(in) :: time_per_event
    type(time) :: t
    if (time_per_event > 0) then
       t = time_from_seconds (10000 * time_per_event)
       call msg_message
       write (msg_buffer, "(1x,A)") &
            & "Time estimate for generating 10000 unweighted events:"
       call write (msg_buffer(len_trim(msg_buffer)+1:), t)
       call msg_message
    end if
  end subroutine mc_show_time_estimate

  subroutine mc_write_grids &
       & (g, file, iterations, it, &
       &  integral, error, chi2, efficiency, accuracy, &
       &  logtable, process_id, checksum, raw)
    type(vamp_grids), intent(in) :: g
    character(len=*), intent(in) :: file, process_id
    integer, intent(in) :: iterations, it
    real(kind=default), intent(in) :: integral, error, chi2
    real(kind=default), intent(in) :: efficiency, accuracy
    character(len=*), dimension(:), intent(in) :: logtable
    character(32), intent(in) :: checksum
    logical, intent(in), optional :: raw
    integer :: u
    character(len=VERSION_STRLEN) :: version_string_file = VERSION_STRING
    logical, parameter :: write_integrals = .true.
    logical :: write_raw
    write_raw = .false.;  if (present (raw))  write_raw = raw
    u = free_unit()
    if (write_raw) then
       open (u, file = trim(file), action = "write", status = "replace", &
            & form = "unformatted")
       write (u) version_string_file
       write (u) checksum
       write (u) iterations
       write (u) it
       write (u) integral
       write (u) error
       write (u) chi2
       write (u) efficiency
       write (u) accuracy
       call mc_write_result (u, iterations, logtable, raw = .true.)
       write (u) MC_GRID_MAGIC_NUMBER
       call vamp_write_grids_raw (g, u, write_integrals)
       close (u)
    else
       open (u, file = trim(file), action = "write", status = "replace")
       write (u, "(1x,a)")   trim (VERSION_STRING)
       write (u, "(1x,a16,1x,a)")    "process_id     =", process_id
       write (u, "(1x,a16,1x,a32)")  "checksum       =", checksum
       write (u, "(1x,a16,1x,i4)")   "iterations     =", iterations
       write (u, "(1x,a16,1x,i4)")   "this_iteration =", it
       write (u, "(1x,a16,1x,e30.22)") "integral       =", integral
       write (u, "(1x,a16,1x,e30.22)") "error          =", error
       write (u, "(1x,a16,1x,e30.22)") "chi2           =", chi2
       write (u, "(1x,a16,1x,e30.22)") "efficiency     =", efficiency
       write (u, "(1x,a16,1x,e30.22)") "accuracy       =", accuracy
       write (u, "(1x,a)")   "begin logtable"
       call mc_write_result (u, iterations, logtable)
       write (u, "(1x,a)")   "end logtable"
       call vamp_write_grids (g, u, write_integrals)
       close (u)
    end if
  end subroutine mc_write_grids
  subroutine mc_read_grids &
       & (g, file, iterations, it, &
       &  integral, error, chi2, efficiency, accuracy, &
       &  logtable, checksum, raw)
    type(vamp_grids), intent(inout) :: g
    character(len=*), intent(in) :: file
    integer, intent(inout) :: iterations, it
    real(kind=default), intent(inout) :: integral, error, chi2
    real(kind=default), intent(inout) :: efficiency, accuracy
    character(len=*), dimension(:), intent(inout) :: logtable
    character(32), intent(in), optional :: checksum
    logical, intent(in), optional :: raw
    character(len=VERSION_STRLEN) :: version_string_file
    real :: version
    real, parameter :: current_version = 1.95
    character :: cdummy
    integer :: u, ichsum, magic
    character(32) :: chsum
    logical :: ok, read_integrals, read_raw, compatibility_142
    read_raw = .false.;  if (present (raw))  read_raw = raw
    read_integrals = .true.
    inquire (file = trim(file), exist=ok)
    if (ok) then
       call msg_message (" Reading grid data from file " // trim(file))
       u = free_unit()
       if (read_raw) then
          open (u, file = trim(file), action="read", status="old", &
               & form = "unformatted")
          read (u) version_string_file
          call check_version
          if (compatibility_142) then
             read (u) ichsum
          else
             read (u) chsum
             if (present (checksum)) then
                if (chsum /= checksum) call checksum_error ("grids")
             end if
          end if
          read (u) iterations
          read (u) it
          read (u) integral
          read (u) error
          read (u) chi2
          read (u) efficiency
          read (u) accuracy
          call mc_read_result (u, iterations, logtable, raw = .true.)
          read (u) magic
          if (magic /= MC_GRID_MAGIC_NUMBER) then
             call msg_fatal (" Magic number not found: This grid can't be used, sorry")
          end if
          call vamp_read_grids_raw (g, u, read_integrals)
          close (u)
       else
          open (u, file = trim(file), action="read", status="old")
          read (u, "(1x,a)") version_string_file
          call check_version
          read (u, *) cdummy                 ! process_id just for info
          if (compatibility_142) then
             read (u, "(1x,a16,1x,i15)") cdummy, ichsum
          else
             read (u, "(1x,a16,1x,a32)") cdummy, chsum
             if (present (checksum)) then
                if (chsum /= checksum) call checksum_error ("grids")
             end if
          end if
          read (u, "(1x,a16,1x,i4)")  cdummy, iterations
          read (u, "(1x,a16,1x,i4)")  cdummy, it
          read (u, "(1x,a16,1x,e30.22)") cdummy, integral
          read (u, "(1x,a16,1x,e30.22)") cdummy, error
          read (u, "(1x,a16,1x,e30.22)") cdummy, chi2
          read (u, "(1x,a16,1x,e30.22)") cdummy, efficiency
          read (u, "(1x,a16,1x,e30.22)") cdummy, accuracy
          read (u, *)
          call mc_read_result (u, iterations, logtable)
          read (u, *)
          call vamp_read_grids (g, u, read_integrals)
          close (u)
       end if
    end if
  contains
    subroutine check_version
      compatibility_142 = .false.
      if (version_string_file /= VERSION_STRING) then
         call msg_warning (" Grid file has been generated by " // &
              &            trim(version_string_file))
         read (version_string_file, "(a15,1x,g5.2)") cdummy, version
         if (version < 1.43) then
            call msg_warning (" Using compatibility mode for version < 1.43:")
            call msg_message ("          Checksum won't be tested")
            compatibility_142 = .true.
         end if
      end if
    end subroutine check_version
  end subroutine mc_read_grids

  subroutine mc_enable_grid (mcs, g, safety_factor)
    type(monte_carlo_state), intent(inout) :: mcs
    type(vamp_grids), intent(inout) :: g
    real(kind=default), intent(in), optional :: safety_factor
    call vamp_copy_grids (mcs%g, g)
    if (present(safety_factor)) &
         & mcs%g%grids%f_max = mcs%g%grids%f_max * mcs%safety_factor
  end subroutine mc_enable_grid

  subroutine mc_generate(mcs, weight, probability, channel, excess)
    type(monte_carlo_state), intent(inout) :: mcs
    real(kind=default), intent(out), optional :: weight, probability
    integer, intent(in), optional :: channel
    real(kind=default), intent(out), optional :: excess
    real(kind=default), dimension(mcs%f%n_dim_integration) :: x
    real(kind=default) :: wgt
    call event_reset_shower (current_event)
    if (present(channel).and.present(probability)) then
       call vamp_next_event(x, rng, mcs%g%grids(channel), mc_sample, &
            &               wgt, channel, mcs%g%weights, mcs%g%grids)
       probability = wgt / mcs%g%grids(channel)%f_max
       if (present(weight)) weight = wgt
    else
       call vamp_next_event(x, rng, mcs%g, mc_sample, phi, excess=excess)
    end if
    call select_final_state &
         & (current_event, mcs%rho_out, rng, &
         &  mcs%code_out, mcs%hel_state_out, &
         &  mcs%col_flow, mcs%acl_flow, mcs%n_col)
    call event_apply_shower (current_event, rng, mcs%spar)
    current_event%count = current_event%count + 1
    current_event%count_file = current_event%count_file + 1
  end subroutine mc_generate

  function phi (xi, channel_dummy) result (x)
    use kinds !NODEP!
    real(kind=default), dimension(:), intent(in) :: xi
    integer, intent(in) :: channel_dummy
    real(kind=default), dimension(size(xi)) :: x
    x = xi
  end function phi

  subroutine select_final_state &
       & (evt, rho, rng, flv_out, hel_out, col_flow, acl_flow, n_col)
    type(event), intent(inout) :: evt
    type(state_density_t), intent(in) :: rho
    type(tao_random_state), intent(inout) :: rng
    integer, dimension(:,:), intent(in) :: flv_out, hel_out, col_flow, acl_flow
    integer, intent(in) :: n_col
    integer :: col, hel, flv
    real(kind=double) :: r
    integer :: i
    call mc_random_number (r)
    call select_state (rho, real(r,kind=default), col, hel, flv)
    do i=1, evt%n_out
       call set (evt%prt(i), flv_out(i,flv))
    end do
    evt%hel_out = hel_out(:,hel)
    if (n_col > 0) then
       evt%color_flow = col_flow(:,col)
       evt%anticolor_flow = acl_flow(:,col)
    else
       evt%color_flow = col_flow(:,flv)
       evt%anticolor_flow = acl_flow(:,flv)
    end if
  end subroutine select_final_state

  subroutine event_apply_shower (evt, rng, spar)
    type(event), intent(inout) :: evt
    type(tao_random_state), intent(inout) :: rng
    type(shower_parameters_t), intent(in) :: spar
    integer :: i1, i2, i3, ii1, ii2, ii3
    if (.not. spar%shower_on)  return
    if (any (evt%color_flow /= 0 .and. evt%anticolor_flow /= 0)) &
         call err_gluon
    allocate (evt%shower_index (evt%n_out), evt%shower (evt%n_out))
    evt%shower_index = 0
    do i1 = 1, evt%n_out
       ii1 = evt%n_in + i1
       ii2 = evt%color_flow(ii1)
       ii3 = evt%anticolor_flow(ii1)
       i2 = ii2 - evt%n_in
       i3 = ii3 - evt%n_in
       if (ii2 /= 0) then
          if (i2 <= 0)  call err_connection
          call pair_shower_generate &
               (evt%shower(i1), evt%prt((/i1,i2/)), rng, spar)
          evt%shower_index((/i1,i2/)) = i1
       else if (ii3 /= 0) then
          if (i3 <= 0)  call err_connection
       end if
    end do
  contains
    subroutine err_gluon
      call msg_fatal (" Parton shower: Color connection between " &
                    // "incoming and outgoing particles found, " &
                    // "but not supported yet")
    end subroutine err_gluon
    subroutine err_connection
      call msg_fatal (" Parton shower: Color connection between " &
                    // "incoming and outgoing particles found, " &
                    // "but not supported yet")
    end subroutine err_connection
  end subroutine event_apply_shower

  function mc_sample (xi, weights, channel, grids) result (s)
    use vamp_grid_type !NODEP!
    real(kind=default), dimension(:), intent(in) :: xi
    real(kind=default), dimension(:), intent(in), optional :: weights
    integer, intent(in), optional :: channel
    type(vamp_grid), dimension(:), intent(in), optional :: grids
    real(kind=default) :: s
    logical :: ok

    call mc_sample_state (mcs(mc_process_index), current_event, rng, ok)
    if (ok) then
       s = current_event%mc_function_value
    else
       s = 0
    end if

    if (mcs(mc_process_index)%time_limit /= 0) then
       if (time_current () > mcs(mc_process_index)%program_stop_time) &
            & call terminate_soon (reason = TIME_EXCEEDED)
    end if
    if (.not.mcs(mc_process_index)%generating_events) &
         & call terminate_now_maybe ()

  contains

    subroutine mc_sample_state (mcs, evt, rng, ok)
      type(monte_carlo_state), intent(inout) :: mcs
      type(event), intent(inout) :: evt
      type(tao_random_state), intent(inout) :: rng
      real(kind=default), &
           & dimension(mcs%f%n_dim_integration, mcs%f%n_channels) :: rr
      real(kind=default) :: mass_out
      real(kind=default), dimension(mcs%f%n_channels) :: phase_space_factor
      real(kind=default) :: phase_space_volume, user_weight_value
      real(kind=default), dimension(mcs%f%n_channels) :: p_evt, g_xi
      logical, dimension(mcs%f%n_channels) :: active
      real(kind=default) :: dp
      integer :: ch
      logical, intent(out) :: ok
      evt%process_id = mcs%process_id_current
      evt%process_index = mc_process_index
      call destroy_rho_in (evt)
      active = (mcs%g%num_calls >= 2)
      rr(:, channel) = xi
      call event_set_beams (evt, mcs%beam, mcs%sqrts, mcs%par, ok)
      if (ok) then
         if (mcs%partons%single_particle) then
            mass_out = particle_mass (mcs%code(mcs%n_in+1,1), mcs%par)
         else
            mass_out = 0
         end if
         call structure_functions &
              & (mcs%partons, mcs%beam, &
              &  rr(mcs%f%n_dim_momenta+1:, channel), &
              &  mcs%hel_state_in, mcs%code_in, mass_out, mc_random_number)
         do ch=1, mcs%n_channels
            rr(mcs%f%n_dim_momenta+1:, ch) = rr(mcs%f%n_dim_momenta+1:, channel)
         end do
         call event_in_state_from_partons &
              & (evt, mcs%partons, mcs%par, &
              &  mcs%keep_initials, mcs%keep_beam_remnants, ok=ok)
      end if
      if (ok) then
         call set_out_state (evt, mcs%code(mcs%n_in+1:,1), mcs%par)
         call event_fill (evt, rr, mcs%f, &
              & phase_space_factor, phase_space_volume, channel, active, ok)
         evt%count_internal = evt%count_internal + 1
      end if
      if (ok) call apply (mcs%cc, evt, ok)
      if (ok) then
         where (.not.active)
            phase_space_factor = 0
         end where
         do ch=1, mcs%f%n_channels
            if (active(ch) .and. phase_space_factor(ch)>0) then
               p_evt(ch) = vamp_probability(grids(ch), rr(:,ch))
               g_xi(ch)  = p_evt(ch) / phase_space_factor(ch)
            else
               p_evt(ch) = 0
               g_xi(ch) = 0
            end if
         end do
         dp = dot_product(weights, g_xi)
         if (dp>0) then
            evt%phase_space_factor = phase_space_volume * p_evt(channel) / dp
            call scatter_set_probabilities &
                 & (evt, trim(mcs%process_id_current), mcs%rho_out, &
                 &  diagonal=all(.not.mcs%beam%vector_polarization), &
                 &  colored=(mcs%n_col>0), &
                 &  ok=ok)
            if (mcs%user_weight_mode /= 0) then
               user_weight_value = &
                    & user_weight (particle_four_momentum (evt%prt(1:)), &
                    &              particle_code (evt%prt(1:)), &
                    &              mcs%user_weight_mode)
               if (.not.mcs%recalculate .and. user_weight_value < 0) then
                  call write (6, evt)
                  call msg_fatal (" Negative user weight function value.")
               end if
               evt%mc_function_value = evt%mc_function_value * user_weight_value
            end if
            if (evt%mc_function_value < 0) then
               select case (mcs%handle_negative_weight)
               case (0)
                  ! do nothing
               case (1)
                  call nullify (mcs%rho_out)
                  evt%mc_function_value = 0
               case (2)
                  write (msg_buffer, "(1x,A,I10,1x,A)") &
                       & "Event #", evt%count_internal, "has negative integrand value."
                  call msg_warning
               case (3)
                  write (msg_buffer, "(1x,A,I10,1x,A)") &
                       & "Event #", evt%count_internal, ": negative integrand value is set to zero."
                  call msg_warning
                  call nullify (mcs%rho_out)
                  evt%mc_function_value = 0
               case (4)
                  call write (6, evt)
                  call write (6, mcs%rho_out)
                  write (msg_buffer, "(1x,A,I10,1x,A)") &
                       & "Event #", evt%count_internal, "has negative integrand value."
                  call msg_fatal
               end select
            end if
            if (.not. ok) then
               call write (6, evt)
               call write (6, mcs%rho_out)
               write (msg_buffer, "(1x,A,I10,1x,A)") &
                    & "Event #", evt%count_internal, "has negative entry in probability matrix."
               call msg_fatal
            end if
            evt%mc_function_ratio = 1
         end if
      end if
    end subroutine mc_sample_state

  end function mc_sample

  subroutine mc_recalculate (mcs, evt)
    type(monte_carlo_state), intent(inout) :: mcs
    type(event), intent(inout) :: evt
    real(kind=default) :: mc_function_value_old, user_weight_value
    logical :: ok
    mc_function_value_old = evt%mc_function_value
    call scatter_set_probabilities &
         & (evt, trim(mcs%process_id_current), mcs%rho_out, &
         &  diagonal=all(.not.mcs%beam%vector_polarization), &
         &  colored=(mcs%n_col>0), &
         &  ok=ok)
    if (mcs%user_weight_mode /= 0) then
       user_weight_value = &
            & user_weight (particle_four_momentum (evt%prt(1:)), &
            &              particle_code (evt%prt(1:)), &
            &              mcs%user_weight_mode)
       if (.not.mcs%recalculate .and. user_weight_value < 0) then
          call write (6, evt)
          call msg_fatal (" Negative user weight function value.")
       end if
       evt%mc_function_value = evt%mc_function_value * user_weight_value
    end if
    
    if (ok .and. mc_function_value_old /= 0) then
       evt%mc_function_ratio = evt%mc_function_value / mc_function_value_old
    else
       evt%mc_function_ratio = 0
    end if
  end subroutine mc_recalculate

  subroutine scatter_set_probabilities &
       & (evt, process_id, rho_out, diagonal, colored, ok)
    type(event), intent(inout) :: evt
    character(len=*), intent(in) :: process_id
    type(state_density_t), intent(inout) :: rho_out
    logical, intent(in) :: diagonal, colored
    logical, intent(out) :: ok
    real(kind=default), dimension(0:3, evt%n_tot) :: p
    real(default) :: norm
    integer :: i
    if (evt%scattering) then
       do i=1,2
          p(:,i) = array(particle_four_momentum(evt%prt(-i)))
       end do
    else
       p(:,1) = array(particle_four_momentum(evt%prt(0)))
    end if
    do i=1, evt%n_out
       p(:,evt%n_in+i) = array(particle_four_momentum(evt%prt(i)))
    end do
    call compute_state_density &
         & (p, evt%rho_in, rho_out, norm, process_id, diagonal, colored, ok)
    evt%mc_function_value = &
         & norm * evt%phase_space_factor * evt%flux_factor
  end subroutine scatter_set_probabilities


end module vamp_interface
