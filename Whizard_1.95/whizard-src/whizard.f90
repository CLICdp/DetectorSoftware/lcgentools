! WHIZARD 1.95 Feb 25 2010
! 
! (C) 1999-2008 by 
!     Wolfgang Kilian <wolfgang.kilian@desy.de>
!     Thorsten Ohl <ohl@physik.uni-wuerzburg.de>
!     Juergen Reuter <juergen.reuter@physik.uni-freiburg.de>
!
! WHIZARD is free software; you can redistribute it and/or modify it
! under the terms of the GNU General Public License as published by 
! the Free Software Foundation; either version 2, or (at your option)
! any later version.
!
! WHIZARD is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! This file has been stripped of most comments.  For documentation, refer
! to the source 'whizard.nw'

module whizard

  use kinds, only: default !NODEP!
  use kinds, only: double, i64 !NODEP!
  use tao_random_numbers, only: tao_random_destroy !NODEP!
  use vamp_interface, only: mc_integrate, mc_enable_grid, mc_generate
  use vamp_interface, only: mc_recalculate
  use vamp_interface, only: mc_show_time_estimate, mc_random_number
  use analyze, only: analysis_bundle, create, destroy, apply, read, write
  use analyze, only: TEXT_FMT, GML_FMT
  use analyze, only: write_grid_drivers, write_histories_driver
  use monte_carlo_states, only: mcs, monte_carlo_state
  use monte_carlo_states, only: mc_process_index, mc_number_processes
  use monte_carlo_states, only: get_number_processes
  use monte_carlo_states, only: current_event, rng
  use monte_carlo_states, only: monte_carlo_input, mpi90_broadcast
  use monte_carlo_states, only: create, destroy, read, write
  use monte_carlo_states, only: mc_write_hline, mc_write_dline
  use monte_carlo_states, only: mc_write_header_summary, mc_write_result
  use monte_carlo_states, only: checksum_error
  use monte_carlo_states, only: JETSET_FRAGMENTATION, PYTHIA_FRAGMENTATION
  use monte_carlo_states, only: NO_FRAGMENTATION, USER_FRAGMENTATION
  use hepevt_common, only: hepevt_fill, hepevt_write
  use hepevt_common, only: heprup_fill, hepeup_fill
  use hepevt_common, only: lhef_write_init, lhef_write_final
  use hepevt_common, only: lhef_write_heprup, lhef_write_hepeup
  use hepevt_common, only: nhep
  use beams, only: beams_pythia_format
  use events, only: read, write, event, event_set_beams, destroy
  use events, only: RAW_FMT, JETSET_FMT1, JETSET_FMT2, JETSET_FMT3
  use events, only: STDHEP_FMT, STDHEP_FMT4, STDHEP_UP_FMT, LHEF_FMT
  use density_vectors, only: create, destroy
  use particles, only: particle_mass, particle_four_momentum, particle_code
  use particles, only: particle_color
  use lorentz, only: array
  use histograms, only: histogram, create_histogram !NODEP!
  use histograms, only: fill_histogram, write_histogram !NODEP!
  use files, only: choose_filename, concat
  use diagnostics, only: msg_buffer, msg_list_clear, msg_summary
  use diagnostics, only: msg_message, msg_warning, msg_error, msg_fatal
  use diagnostics, only: msg_terminate, terminate_now_maybe, terminate_soon
  use diagnostics, only: terminate_status
  use diagnostics, only: CONTINUE, TIME_EXCEEDED, MAX_FILE_COUNT_EXCEEDED
  use unix_args, only: catch_signals, restore_signals !NODEP!
  use clock, only: time, time_current, operator(+), operator(>)
  use mpi90, only: mpi90_rank !NODEP!
  use mpi90, only: mpi90_init, mpi90_broadcast, mpi90_finalize !NODEP!
  use file_utils, only: free_unit !NODEP!
  use limits, only: DEFAULT_FILENAME, FILENAME_LEN
  use limits, only: VERSION_STRLEN, VERSION_STRING
  use limits, only: PYTHIA_BEAM_STRLEN
  use limits, only: WHIZARD_ROOT
  use limits, only: WEIGHT_HISTOGRAMS_NBIN
  use limits, only: STDHEP_BYTES_PER_FILE
  use limits, only: STDHEP_BYTES_PER_EVENT,STDHEP_BYTES_PER_ENTRY
  use stdhep_interface, only: stdhep_init, stdhep_write, stdhep_end !NODEP!
  use stdhep_interface, only: STDHEP_HEPRUP !NODEP!
  use jetset_interface, only: jetset_suppress_banner !NODEP!
  use jetset_interface, only: jetset_open_write, jetset_close !NODEP!
  use jetset_interface, only: pythia_init, jetset_init !NODEP!
  use jetset_interface, only: pythia_init_data !NODEP!
  use jetset_interface, only: jetset_fragment, pythia_fragment !NODEP!
  use jetset_interface, only: pythia_print_statistics !NODEP!
  use jetset_interface, only: new_event_needed_pythia !NODEP!
  use jetset_interface, only: number_events_whizard !NODEP!
  use jetset_interface, only: number_events_pythia !NODEP!
  use user, only: user_fragment_init => fragment_init
  use user, only: user_fragment_call => fragment_call
  use user, only: user_fragment_end => fragment_end

  implicit none
  private

  public :: whizard_set_directory
  public :: whizard_set_filename
  public :: whizard_set_input_file
  public :: whizard_enable_input_file
  public :: whizard_set_input
  public :: whizard_read_input
  public :: whizard_write_output
  public :: whizard_integrate
  public :: whizard_number_processes
  public :: whizard_get_results
  public :: whizard_generate
  public :: whizard_events_begin
  public :: whizard_event
  public :: whizard_events_end
  public :: whizard_end

  public :: whizard_input

  type(monte_carlo_input) :: whizard_input
  type(time) :: program_start_time
  character(len=FILENAME_LEN) :: whizard_directory = ""
  character(len=FILENAME_LEN) :: whizard_filename = DEFAULT_FILENAME
  character(len=FILENAME_LEN) :: whizard_input_file = DEFAULT_FILENAME
  logical :: whizard_enable_input = .true.
  character(len=FILENAME_LEN) :: whizard_process_input = ""
  character(len=FILENAME_LEN) :: whizard_integration_input = ""
  character(len=FILENAME_LEN) :: whizard_simulation_input = ""
  character(len=FILENAME_LEN) :: whizard_diagnostics_input = ""
  character(len=FILENAME_LEN) :: whizard_parameter_input = ""
  character(len=FILENAME_LEN) :: whizard_beam_input1 = ""
  character(len=FILENAME_LEN) :: whizard_beam_input2 = ""
  type(analysis_bundle), dimension(:), allocatable :: whizard_analysis
  logical :: whizard_input_exists = .false.
  logical :: mcs_exists = .false.
  logical :: event_files_open = .false.
  logical :: mpi_initialized = .false.
  integer :: proc_id
  integer :: uraw, uevt, uwgt
  real(kind=default) :: total_integral, total_error, total_efficiency
  real(kind=default), dimension(:), allocatable :: process_probability
  real(kind=default) :: default_event_weight

  integer(i64) :: n_events_expected = 0
  integer :: event_file_count = 0
  logical :: compatibility_142 = .false.

  save

contains

  subroutine whizard_set_directory (directory)
    character(len=*), intent(in) :: directory
    if (len_trim (directory) > FILENAME_LEN) then
       call msg_warning (" Directory name too long; will be truncated.")
    end if
    whizard_directory = trim (directory)
  end subroutine whizard_set_directory

  subroutine whizard_set_filename (filename)
    character(len=*), intent(in) :: filename
    if (len_trim (filename) > FILENAME_LEN) then
       call msg_warning (" Filename too long; will be truncated.")
    end if
    whizard_filename = trim (filename)
  end subroutine whizard_set_filename

  subroutine whizard_set_input_file (input_file)
    character(len=*), intent(in) :: input_file
    if (len_trim (input_file) > FILENAME_LEN) then
       call msg_warning (" Input_File too long; will be truncated.")
    end if
    whizard_input_file = trim (input_file)
  end subroutine whizard_set_input_file

  subroutine whizard_enable_input_file (enable)
    logical, intent(in) :: enable
    whizard_enable_input = enable
  end subroutine whizard_enable_input_file

  subroutine whizard_set_input &
       & (process_input, integration_input, simulation_input, &
       &  diagnostics_input, parameter_input, beam_input1, beam_input2)
    character(len=*), intent(in) :: process_input, integration_input
    character(len=*), intent(in) :: simulation_input, diagnostics_input
    character(len=*), intent(in) :: parameter_input
    character(len=*), intent(in) :: beam_input1, beam_input2
    whizard_process_input = process_input
    whizard_integration_input = integration_input
    whizard_simulation_input = simulation_input
    whizard_diagnostics_input = diagnostics_input
    whizard_parameter_input = parameter_input
    whizard_beam_input1 = beam_input1
    whizard_beam_input2 = beam_input2
  end subroutine whizard_set_input

  subroutine whizard_read_input
    integer :: unit
    if (whizard_input_exists .or. mcs_exists)  call whizard_end
    if (.not.mpi_initialized)  then
       call mpi90_init;  mpi_initialized = .true.
    end if
    call mpi90_rank (proc_id)
    program_start_time = time_current ()
    call create (whizard_input)
    if (whizard_directory /= "") &
         & whizard_input%directory = whizard_directory
    if (whizard_filename /= "") &
         & whizard_input%filename = whizard_filename
    call msg_message (" WHIZARD 1.95 (Feb 25 2010)")
    if (proc_id == WHIZARD_ROOT .and. whizard_enable_input) then
       if (whizard_input_file /= "") then
          call read (trim (whizard_directory), trim (whizard_input_file), &
               &     whizard_input)
       else
          call read (trim (whizard_directory), trim (whizard_filename), &
               &     whizard_input)
       end if
    end if
    unit = free_unit ()
    open (unit, status = "scratch", action = "readwrite")
    call write_scratch_input_file (unit)
    rewind (unit)
    call read (unit, whizard_input)
    close (unit)
    if (whizard_input%directory /= "") &
         & whizard_directory = whizard_input%directory
    if (whizard_input%filename /= "") &
         & whizard_filename = whizard_input%filename
    call mpi90_broadcast (whizard_input, WHIZARD_ROOT)
    call write_global_log_file
    whizard_input_exists = .true.
  contains
    subroutine write_scratch_input_file (unit)
      integer, intent(in) :: unit
      write (unit, *) "&process_input"
      write (unit, *) whizard_process_input
      write (unit, *) "/"
      write (unit, *) "&integration_input"
      write (unit, *) whizard_integration_input
      write (unit, *) "/"
      write (unit, *) "&simulation_input"
      write (unit, *) whizard_simulation_input
      write (unit, *) "/"
      write (unit, *) "&diagnostics_input"
      write (unit, *) whizard_diagnostics_input
      write (unit, *) "/"
      write (unit, *) "&parameter_input"
      write (unit, *) whizard_parameter_input
      write (unit, *) "/"
      write (unit, *) "&beam_input"
      write (unit, *) whizard_beam_input1
      write (unit, *) "/"
      write (unit, *) "&beam_input"
      write (unit, *) whizard_beam_input2
      write (unit, *) "/"
    end subroutine write_scratch_input_file
  end subroutine whizard_read_input

  subroutine write_global_log_file (with_results)
    logical, intent(in), optional :: with_results
    integer :: u
    character(len=FILENAME_LEN) :: filename, file
    filename = choose_filename &
         & (whizard_input%write_logfile_file, whizard_filename)
    file = concat (whizard_input%directory, filename, "out")
    u = free_unit ()
    open (u, file = trim(file), action="write")
    call msg_message (" " // trim(VERSION_STRING), unit=u)
    call processes_list (u)
    if (present (with_results)) then
       if (with_results)  call write_process_summary (u)
    end if
    call msg_message &
         & (" For the process-specific logs, consult the file(s) " &
         &  // trim (concat (whizard_input%directory, filename, &
         &                   "XXX", "out")), unit=u)
    call msg_message (" Input data, including default values:", unit=u)
    call write (u, whizard_input)
    close (u)
    call msg_message (" Wrote " // trim(file))
  end subroutine write_global_log_file

  subroutine whizard_write_output
    character(len=FILENAME_LEN) :: write_logfile_file
    call mpi90_rank (proc_id)
    if (proc_id == WHIZARD_ROOT) then
       if (whizard_input_exists) then
          write_logfile_file = choose_filename &
               & (whizard_input%write_logfile_file, whizard_filename)
          if (mcs_exists) then
             call write (trim(write_logfile_file), mcs, whizard_input)
          else
             mc_number_processes = &
                  & get_number_processes (whizard_input%process_id)
             allocate (mcs(mc_number_processes))
             call create (mcs, rng, current_event, &
                  &       whizard_input, trim(whizard_filename), &
                  &       program_start_time)
             call write (trim(write_logfile_file), mcs, whizard_input)
             call destroy (mcs)
             deallocate (mcs)
             call destroy (current_event)
             call tao_random_destroy (rng)
          end if
       else
          call msg_warning &
               & (" Output file requested before input is known.  Ignored.")
       end if
    end if
  end subroutine whizard_write_output

  subroutine whizard_integrate
    real(kind=default) :: time_per_event
    real(kind=default), dimension(:), allocatable :: tmp
    if (mcs_exists)  call whizard_end
    if (.not.whizard_input_exists)  call whizard_read_input
    call mpi90_rank (proc_id)
    mc_number_processes = &
         & get_number_processes (whizard_input%process_id)
    if (mc_number_processes == 0) then
       call processes_list (6)
       call msg_terminate (" No process selected in input file: exiting.")
    end if
    allocate (mcs(mc_number_processes))
    call create &
         & (mcs, rng, current_event, whizard_input, trim(whizard_filename), &
         &  program_start_time)
    call write_histories_driver (trim(whizard_filename), mcs)
    mcs_exists = .true.
    mcs%generating_events = .false.
    if (mcs(1)%catch_signals)  call catch_signals
    do mc_process_index=1, mc_number_processes
       call mc_integrate (mcs(mc_process_index), &
            &             whizard_input, &
            &             default_filename=trim(whizard_filename), &
            &             time_per_event=time_per_event)
       call mc_show_time_estimate (time_per_event)
       call write_histories_driver (trim(whizard_filename), mcs)
    end do
    mc_process_index = mc_number_processes
    total_integral = sum (mcs%integral)
    total_error = sqrt (sum (mcs%error**2))
    allocate (tmp(mc_number_processes))
    where (mcs%efficiency /= 0)
       tmp = mcs%integral / mcs%efficiency
    elsewhere
       tmp = 0
    end where
    if (sum(tmp) /= 0) then
       total_efficiency = total_integral / sum(tmp)
    else
       total_efficiency = 0
    end if
    deallocate (tmp)
    allocate (process_probability(mc_number_processes))
    if (total_integral /= 0) then
       process_probability = mcs%integral / total_integral
    else
       process_probability = 1 / real(mc_number_processes, kind=default)
    end if
    call write_process_summary (6)
    call write_global_log_file (with_results = .true.)
    call msg_message (" Integration complete.")
    if (proc_id == WHIZARD_ROOT) then
       if (mcs(1)%screen_diagnostics) then
          do mc_process_index=1, mc_number_processes
             call write (6, mcs(mc_process_index), whizard_input)
          end do
          mc_process_index = mc_number_processes
       end if
       if (len_trim(mcs(1)%plot_grids_channels) > 0) then
          call write_grid_drivers (whizard_filename, mcs)
       end if
    end if
  end subroutine whizard_integrate

  subroutine write_process_summary (u)
    integer, intent(in) :: u
    call mc_write_dline (u)
    call msg_message (" Summary (all processes):", unit=u)
    call mc_write_header_summary (u, mcs(1)%n_in)
    do mc_process_index=1, mc_number_processes
       call mc_write_result (u, mcs(mc_process_index)%process_id_current, &
            &                mcs(mc_process_index)%integral, &
            &                mcs(mc_process_index)%error, &
            &                total_integral)
    end do
    mc_process_index = mc_number_processes
    call mc_write_hline (u)
    call mc_write_result &
         & (u, "sum", total_integral, total_error, total_integral)
    call mc_write_dline (u)
  end subroutine write_process_summary

  subroutine whizard_number_processes (n)
    integer, intent(out) :: n
    n = 0
    if (mcs_exists) then
       n = mc_number_processes
    else
       call msg_warning (" Number of processes requested but not yet known.")
    end if
  end subroutine whizard_number_processes

  subroutine whizard_get_results &
       & (process_index, integral, error, chi2, efficiency)
    integer, intent(in) :: process_index
    real(kind=double), intent(out) :: integral, error, chi2, efficiency
    integral = 0
    error = 0
    chi2 = 0
    efficiency = 0
    if (mcs_exists) then
       if (process_index == 0) then
          integral = total_integral
          error = total_error
          chi2 = 0
          efficiency = total_efficiency
       else if (process_index<0 .or. process_index > mc_number_processes) then
          call msg_error &
               & (" Integration results for illegal process index requested")
       else
          integral = mcs(process_index)%integral
          error = mcs(process_index)%error
          chi2 = mcs(process_index)%chi2
          efficiency = mcs(process_index)%efficiency
       end if
    else
       call msg_warning (" Integration results requested but not yet known.")
    end if
  end subroutine whizard_get_results

  subroutine whizard_generate
    integer(i64) :: ievt, n_events, n_calls
    real(kind=default) :: lumi
    if (mcs(1)%n_in == 1 .and. mcs(1)%luminosity /= 0)  call msg_warning &
         & (" I'll ignore the luminosity value -- meaningless for decays")
    call get_n_calls (mcs, n_events, n_calls, lumi)
    mcs%luminosity = lumi
    if (n_calls > 0) then
       if (mcs(1)%unweighted) then
          call whizard_events_begin (n_events)
          if (mcs(1)%normalize_weight) then
             default_event_weight = 1
          else
             default_event_weight = total_integral / n_events
          end if
          if (mcs(1)%n_in == 2) then
             write (msg_buffer, "(1x,A,1x,G11.4)") &
                  & "Event sample corresponds to luminosity [fb-1] =", lumi
             call msg_message
          end if
          write (msg_buffer, "(1x,A,1x,I10,1x,A)") &
                  & "Event sample corresponds to ", n_calls, " weighted events"
          call msg_message
          if (mcs(1)%read_events_raw) then
             write (msg_buffer, "(1x,A,1x,I10,1x,A)") &
                  & "Reading", n_events, "unweighted events ..."
          else
             write (msg_buffer, "(1x,A,1x,I10,1x,A)") &
                  & "Generating", n_events, "unweighted events ..."
          end if
          call msg_message
          if (mcs(1)%recalculate) then
             call msg_message (" (recalculating matrix element values)")
          end if
          EVENT_LOOP: do ievt=1, n_events
             call whizard_event
          end do EVENT_LOOP
       else
          call whizard_events_begin (n_calls)
          if (mcs(1)%normalize_weight .and. total_integral > 0) then
             default_event_weight = n_events / total_integral / n_calls
          else
             default_event_weight = 1._default / n_calls
          end if
          if (mcs(1)%n_in == 2) then
             write (msg_buffer, "(1x,A,1x,G11.4)") &
                  & "Event sample corresponds to luminosity [fb-1] =", lumi
             call msg_message
          end if
          write (msg_buffer, "(1x,A,1x,I10,1x,A)") &
                  & "Event sample corresponds to ", n_events, &
                  & " unweighted events"
          call msg_message
          if (mcs(1)%read_events_raw) then
             write (msg_buffer, "(1x,A,1x,I10,1x,A)") &
                  & "Reading", n_calls, "weighted events ..."
          else
             write (msg_buffer, "(1x,A,1x,I10,1x,A)") &
                  & "Generating", n_calls, "weighted events ..."
          end if
          call msg_message
          call generate_events_weighted
       end if
       call whizard_events_end
    else
       call msg_message (" No event generation requested")
    end if
  contains
    subroutine get_n_calls (mcs, n_events, n_calls, lumi)
      type(monte_carlo_state), dimension(:), intent(inout) :: mcs
      integer(i64), intent(out) :: n_events, n_calls
      real(kind=default), intent(out) :: lumi
      real(kind=default) :: n_events_real, factor
      real(kind=default), dimension(size(mcs)) :: n_calls_real
      if (mcs(1)%calls(1,5) > 0) then
         if (total_integral > 0 .and. total_efficiency > 0 &
              & .and. all (mcs%efficiency > 0)) then
            if (mcs(1)%n_in == 2) then
               n_events_real = mcs(1)%luminosity * total_integral
            else
               n_events_real = 0
            end if
            if (mcs(1)%n_events > n_events_real) &
                 & n_events_real = mcs(1)%n_events
            n_calls_real = n_events_real &
                 & * (mcs%integral / total_integral / mcs%efficiency)
            !!! Since calls(2,5) is standard integer (i.e. i32) one
            !!! should use here 0 instead of 0_i64
            if (sum (n_calls_real) > huge(0_i64)) then
               call msg_error (" Requested number of events too large " &
                    &  //      "-- inserting maximum value")
               factor = huge(0) / sum (n_calls_real)
               n_calls_real = n_calls_real * factor
               n_events_real = n_events_real * factor
            end if
            !!! if (sum (n_calls_real) > huge(0_i64)) then
            !!!    call msg_error (" Requested number of events too large " &
            !!!         &  //      "-- inserting maximum value")
            !!!    factor = huge(0_i64) / sum (n_calls_real)
            !!!    n_calls_real = n_calls_real * factor
            !!!    n_events_real = n_events_real * factor
            !!! end if
            n_events = nint (n_events_real,i64)
            mcs%calls(2,5) = nint (n_calls_real)
            !! n_calls = sum(nint(n_calls_real,i64))
            n_calls = sum (int(mcs%calls(2,5),i64)) 
            if (n_calls < mcs(1)%n_calls) then
               mcs%calls(2,5) = nint (mcs(1)%n_calls &
                    & * mcs%integral / mcs%efficiency &
                    & / sum (mcs%integral / mcs%efficiency))
               n_calls = sum (int(mcs%calls(2,5),i64))
               n_events = sum (nint (mcs%calls(2,5) * mcs%efficiency))
            end if
            if (n_calls > 0 .and. n_events < 1)  n_events = 1
            lumi = n_events / total_integral
         else
            call msg_error (" Integral or efficiency vanishes: no event generation possible")
            n_events = 0
            n_calls = 0
            lumi = 0
         end if
      else
         n_events = 0
         n_calls = 0
         lumi = 0
      end if
      if (mcs(1)%n_in == 1)  lumi = 0
    end subroutine get_n_calls
  end subroutine whizard_generate

  subroutine whizard_events_begin (n_events)
    integer(i64), intent(in) :: n_events
    logical :: pythia_first, jetset_first
    character(len=FILENAME_LEN) :: read_analysis_file
    if (event_files_open)  call whizard_events_end
    if (.not.mcs_exists)  call whizard_integrate
    call mpi90_rank (proc_id)
    if (total_integral <= 0)  &
         & call msg_fatal ("Integral must be positive for simulation.")
    default_event_weight = 1
    read_analysis_file = &
         & choose_filename (mcs(1)%read_analysis_file, whizard_filename)
    allocate (whizard_analysis(mc_number_processes))
    do mc_process_index=1, mc_number_processes
    call heprup_fill &
         & (pro_id  = mc_process_index, &
         &  pro_max  = mc_number_processes, &
         &  beam_code   = mcs(1)%beam%particle_code, &
         &  beam_energy = (/ mcs(1)%sqrts / 2, mcs(1)%sqrts / 2 /), &
         &  PDF_ngroup  = mcs(1)%beam%PDF_ngroup, &
         &  PDF_nset    = mcs(1)%beam%PDF_nset, &
         &  integral   = mcs(mc_process_index)%integral, &
         &  error      = mcs(mc_process_index)%error )
       call mc_enable_grid (mcs(mc_process_index), &
            &               mcs(mc_process_index)%g_best, &
            &               mcs(mc_process_index)%safety_factor)
       call create (whizard_analysis(mc_process_index))
       if (proc_id == WHIZARD_ROOT) then
          call read (mcs(mc_process_index)%directory, read_analysis_file, &
               &     whizard_analysis(mc_process_index), &
               &     trim(mcs(mc_process_index)%process_id_current))
       end if
    end do
    mc_process_index = mc_number_processes
!    call mpi90_broadcast (whizard_analysis, WHIZARD_ROOT)
    if (mcs(1)%fragment) then
       jetset_first = .true.
       pythia_first = .true.
       do mc_process_index=1, mc_number_processes
          call prepare_fragmentation &
               &   (mcs(mc_process_index), maxval(mcs%n_tot), &
               &    jetset_first, pythia_first)
       end do
       mc_process_index = mc_number_processes
    end if
    current_event%count = 0
    current_event%count_file = 0
    current_event%count_bytes = 0
    n_events_expected = n_events
    call open_raw_events_file (uraw)
    if (mcs(1)%events_per_file == 0 .and. mcs(1)%bytes_per_file == 0) then
       event_file_count = 0
    else
       event_file_count = mcs(1)%min_file_count
    end if
    call open_output_events_file (uevt)
    event_files_open = .true.
    mcs%generating_events = .true.
  contains
    subroutine prepare_fragmentation &
         & (mcs, n_tot_max, jetset_first, pythia_first)
      type(monte_carlo_state), intent(inout) :: mcs
      integer, intent(in) :: n_tot_max
      logical, intent(inout) :: jetset_first, pythia_first
      character(len=PYTHIA_BEAM_STRLEN) :: beam_str, target_str
      type(pythia_init_data) :: pydat
      select case (mcs%fragmentation_method)
      case (NO_FRAGMENTATION)
      case (JETSET_FRAGMENTATION)
         if (.not.mcs%show_pythia_banner)  call jetset_suppress_banner
         if (jetset_first) then
            call jetset_init (mcs%pythia_parameters)
            jetset_first = .false.
         end if
         if (.not.color_flow_known (mcs%code(:,1), mcs%n_col)) then
            call msg_warning &
                 & (" The color flow in process " &
                 &  //trim(mcs%process_id_current)//" is unknown.")
            call msg_message &
                 & (" JETSET will deduce the color configuration " &
                 &     //"from the particle ordering.")
         else if (.not.color_flow_known (mcs%code_in(:,1), 0)) then
            call msg_warning &
                 & (" The color flow in process " &
                 &  //trim(mcs%process_id_current) &
                 &  //" involves the initial state.")
            call msg_message &
                 & (" JETSET fragmentation treats the final state only.")
            call msg_message &
                 & (" To keep all color connections, " &
                 &  //"use PYTHIA fragmentation instead.")
         end if
      case (PYTHIA_FRAGMENTATION)
         if (.not.mcs%show_pythia_banner)  call jetset_suppress_banner
         if (color_flow_known (mcs%code(:,1), mcs%n_col)) then
            if (pythia_first) then
               call beams_pythia_format &
                    & (mcs%beam, mcs%code_in(:,1), beam_str, target_str)
               pydat%n_external = n_tot_max
               pydat%process_id = mcs%process_id
               pydat%pyproc = mcs%pythia_processes
               pydat%beam = beam_str
               pydat%target = target_str
               pydat%fixed_energy = all(mcs%beam%fixed_energy)
               pydat%show_pymaxi = mcs%show_pythia_initialization
               pydat%sqrts = mcs%sqrts
               pydat%integral = total_integral
               pydat%error = total_error
               pydat%chin = mcs%pythia_parameters
               call pythia_init (pydat)
               jetset_first = .false.
               pythia_first = .false.
            end if
         else
            call msg_error &
                 & (" PYTHIA fragmentation is unavailable for this process.")
            call msg_message &
                 & (" Reason: The color flow in process " &
                 &  //trim(mcs%process_id)//" is unknown.")
            call msg_message &
                 & (" I will fall back to JETSET fragmentation.")
            call msg_message &
                 & (" JETSET will deduce the color configuration " &
                 &     //"from the particle ordering.")
            mcs%fragmentation_method = JETSET_FRAGMENTATION
            if (jetset_first) then
               call jetset_init (mcs%pythia_parameters)
               jetset_first = .false.
            end if
         end if
      case (USER_FRAGMENTATION)
         call user_fragment_init (mcs%user_fragmentation_mode, mcs%sqrts, &
              &                   mcs%seed, mcs%pythia_parameters)
      case default
         call jetset_suppress_banner
      end select
    end subroutine prepare_fragmentation

    function color_flow_known (code, n_col) result (ok)
      integer, dimension(:), intent(in) :: code
      integer, intent(in) :: n_col
      logical :: ok
      integer :: i
      if (n_col > 0) then
         ok = .true.
      else
         ok = .true.
         do i=1, size(code)
            if (particle_color (code(i)) /= 0)  ok = .false.
         end do
      end if
    end function color_flow_known

  end subroutine whizard_events_begin

  subroutine whizard_event
    real(kind=default) :: weight, excess
    real(kind=double) :: r
    integer :: iostat
    logical :: ok
    call mpi90_rank (proc_id)
    if (mcs(mc_process_index)%write_events) then
       call check_event_file_split (uevt, mcs(mc_process_index), ok)
       call terminate_now_maybe (current_event%count)
    end if
    if (new_event_needed_pythia) then
       if (.not.event_files_open) then
          call msg_error ("The call to whizard_events_begin is missing.")
          call msg_message &
               & ("I will call whizard_events_begin with argument 0.")
          call whizard_events_begin (0_i64)
       end if
       excess = 0
       if (mcs(1)%read_events_raw) then
          call event_set_beams &
               & (current_event, mcs(1)%beam, mcs(1)%sqrts, mcs(1)%par)
          if (proc_id == WHIZARD_ROOT) then
             call read (uraw, current_event, &
                  &     any(mcs(1)%beam%vector_polarization), &
                  &     compatibility_142, &
                  &     weight=weight, &
                  &     fmt=RAW_FMT, iostat=iostat)
          end if
!           call mpi90_broadcast (current_event, WHIZARD_ROOT)
          call mpi90_broadcast (weight, WHIZARD_ROOT)
          call mpi90_broadcast (iostat, WHIZARD_ROOT)
          if (weight > 1._default)  excess = weight - 1._default
          if (iostat == 0) then
             current_event%count = current_event%count + 1
             current_event%count_file = current_event%count_file + 1
             mc_process_index = current_event%process_index
          else if (iostat < 0) then
             call reopen_raw_events_file (uraw)
          else
             call msg_fatal &
                  & (" I/O error occured while reading raw event file.")
          end if
          if (mcs(1)%recalculate) then
             call mc_recalculate (mcs(mc_process_index), current_event)
          end if
       end if
       if (.not.mcs(1)%read_events_raw) then
          call mc_random_number (r)
          call process_select (mc_process_index, real(r,kind=default))
          call mc_generate (mcs(mc_process_index), excess=excess)
          if (mcs(1)%write_events_raw) then
             if (proc_id == WHIZARD_ROOT) call write &
                  & (uraw, current_event, &
                  &  any(mcs(1)%beam%vector_polarization), &
                  &  weight=1._default+excess, fmt=RAW_FMT)
          end if
       end if
       call hepevt_fill (current_event)
       if (mcs(1)%normalize_weight) then
          call apply (whizard_analysis(mc_process_index), &
               &      current_event, excess=excess)
       else
          call apply (whizard_analysis(mc_process_index), &
               &      current_event, weight=default_event_weight, &
               &      excess=excess)
       end if
    end if
    call fragment_current_event (mcs(mc_process_index))
    if (mcs(mc_process_index)%write_events) &
         & call write_event (uevt, mcs(mc_process_index))
    if (mcs(mc_process_index)%screen_events) &
         & call write_event (6, mcs(mc_process_index))
    if (mcs(mc_process_index)%time_limit /= 0) then
       if (time_current () > mcs(mc_process_index)%program_stop_time) &
            & call terminate_soon (reason = TIME_EXCEEDED)
    end if
    if (terminate_status /= CONTINUE) call whizard_events_end
    call terminate_now_maybe (current_event%count)
  contains
    subroutine process_select (n, r)
      integer, intent(out) :: n
      real(kind=default), intent(in) :: r
      real(kind=default) :: psum
      integer :: i
      psum = 0
      n = mc_number_processes
      LOOP: do i=1, mc_number_processes
         psum = psum + process_probability(i)
         if (r < psum) then
            n = i
            exit LOOP
         end if
      end do LOOP
    end subroutine process_select
      
    subroutine fragment_current_event (mcs)
      type(monte_carlo_state), intent(in) :: mcs
  !    write (66, *)  current_event%count, current_event%mc_function_value, current_event%mc_function_ratio
      call hepeup_fill (current_event, mc_process_index)
      if (mcs%fragment) then
         select case (mcs%fragmentation_method)
         case (NO_FRAGMENTATION)
         case (JETSET_FRAGMENTATION)
            call jetset_fragment &
                 & (current_event%color_flow, current_event%anticolor_flow)
         case (PYTHIA_FRAGMENTATION)
            call pythia_fragment_call (current_event)
         case (USER_FRAGMENTATION)
            call user_fragment_call &
                 & (mcs%user_fragmentation_mode, current_event%count)
         end select
      end if
    end subroutine fragment_current_event

    subroutine pythia_fragment_call (evt)
      type(event), intent(in) :: evt
      integer, dimension(evt%n_tot) :: code
      real(kind=default), dimension(evt%n_tot, 0:3) :: p
      real(kind=default), dimension(evt%n_tot) :: m
      integer :: i
      do i=1, 2
         code(i) = particle_code (evt%prt(-i))
         m(i) = particle_mass (evt%prt(-i))
         p(i,0:3) = array (particle_four_momentum (evt%prt(-i)))
      end do
      do i=3, evt%n_tot
         code(i) = particle_code (evt%prt(i-2))
         m(i) = particle_mass (evt%prt(i-2))
         p(i,0:3) = array (particle_four_momentum (evt%prt(i-2)))
      end do
      call pythia_fragment (m, p, code, evt%color_flow, evt%anticolor_flow)
    end subroutine pythia_fragment_call

  end subroutine whizard_event

  subroutine whizard_events_end
    if (.not.event_files_open)  return
    mcs%it = mcs%it + 1
    if (any (mcs%fragment)) then
       select case (mcs(mc_process_index)%fragmentation_method)
       case (PYTHIA_FRAGMENTATION)
          if (any(mcs%show_pythia_statistics))  call pythia_print_statistics
          call mc_write_dline (6)
          call msg_message (" WHIZARD/PYTHIA fragmentation summary:")
          write (msg_buffer, "(3x,A,1x,I12)") &
               & "Events generated and fragmented by PYTHIA =           ", &
               & number_events_pythia
          call msg_message
          write (msg_buffer, "(3x,A,1x,I12)") &
               & "Events generated by WHIZARD and fragmented by PYTHIA =", &
               & number_events_whizard
          call msg_message
       case (USER_FRAGMENTATION)
          call user_fragment_end &
               & (mcs(mc_process_index)%user_fragmentation_mode, &
               &  current_event%count, mcs(mc_process_index)%integral)
       end select
    end if
    call msg_message (" Event generation finished.")
    call mpi90_rank (proc_id)
    if (proc_id == WHIZARD_ROOT) then
       do mc_process_index=1, mc_number_processes
          call write (6, whizard_analysis(mc_process_index), &
               &      mcs(mc_process_index), &
               &      show_histograms=mcs(mc_process_index)%screen_histograms)
       end do
       mc_process_index = mc_number_processes
       call write (trim(whizard_filename), TEXT_FMT, &
            &      whizard_analysis, mcs)
       call write (trim(whizard_filename), GML_FMT, &
            &      whizard_analysis, mcs)
    end if
    do mc_process_index=1, mc_number_processes
       call destroy (whizard_analysis(mc_process_index))
    end do
    mc_process_index = mc_number_processes
    deallocate (whizard_analysis)
    call close_raw_events_file (uraw)
    call close_output_events_file (uevt)
    event_files_open = .false.
  end subroutine whizard_events_end

  subroutine generate_events_weighted
    real(kind=default) :: weight, efftmp
    real(kind=default), dimension(:,:), allocatable :: eff, effsum
    type(histogram), dimension(:,:), allocatable :: hh
    type(histogram), dimension(:), allocatable :: h
    character(len=FILENAME_LEN) :: filename, file
    integer(i64) :: nc_begin, nc_tot
    allocate (hh(maxval(mcs%f%n_channels), mc_number_processes))
    allocate (h(mc_number_processes))
    do mc_process_index=1, mc_number_processes
       call create_histogram (h(mc_process_index), &
            & 0._default, 1._default, WEIGHT_HISTOGRAMS_NBIN)
    end do
    mc_process_index = mc_number_processes
    allocate (eff(maxval(mcs%f%n_channels), mc_number_processes))
    allocate (effsum(maxval(mcs%f%n_channels), mc_number_processes))
    eff = 0
    effsum = 0
    do mc_process_index=1, mc_number_processes
       call msg_message (" Generating events for process " &
            & // trim(mcs(mc_process_index)%process_id_current))
       if (proc_id == WHIZARD_ROOT &
            & .and. mcs(mc_process_index)%write_weights) then
          filename = choose_filename &
               & (mcs(mc_process_index)%write_weights_file, whizard_filename)
          uwgt = free_unit ()
          file = concat (mcs(mc_process_index)%directory, filename, &
               &         mcs(mc_process_index)%process_id_current, "wgt")
          call msg_message &
               & (" Writing weight distribution to file "//trim(file))
          open (uwgt, file = file, status = "replace", action = "write")
       else
          uwgt = 0
       end if
       if (mcs(mc_process_index)%recalculate) then
          call msg_message (" (recalculating matrix element values)")
       end if
       nc_begin = current_event%count
       call generate_weighted_single (mcs(mc_process_index), &
            & h(mc_process_index), hh(:,mc_process_index), &
            & eff(:,mc_process_index), effsum(:,mc_process_index))
       nc_tot = current_event%count - nc_begin
       if (uwgt /= 0) then
          call msg_message (unit=uwgt)
          call msg_message (" Multi-channel:", unit=uwgt)
          call write_histogram (h(mc_process_index), uwgt, over=.true.)
          call msg_message (unit=uwgt)
          write (msg_buffer, "(1x,A,1x,I12)") &
               & "Warmup events    =", &
               & mcs(mc_process_index)%calls(1,4) &
               & * mcs(mc_process_index)%calls(2,4)
          call msg_message (unit=uwgt)
          write (msg_buffer, "(1x,A,1x,I12)") &
               & "Generated events =", nc_tot
          call msg_message (unit=uwgt)
          if (nc_tot > 0) then
             write (msg_buffer, "(1x,A,1x,F7.3)") &
                  & "Efficiency (%)   =", &
                  & 100 * sum(effsum(:,mc_process_index)) / nc_tot
             call msg_message (unit=uwgt)
          else
             call msg_message (" Efficiency (%)   = 0", unit=uwgt)
          end if
          close (uwgt)
       end if
    end do
    mc_process_index = mc_number_processes
    deallocate (h, hh)
    deallocate (eff, effsum)

  contains

    subroutine generate_weighted_single (mcs, h, hh, eff, effsum)
      type(monte_carlo_state), intent(inout) :: mcs
      type(histogram), intent(inout) :: h
      type(histogram), dimension(:), intent(inout) :: hh
      real(kind=default), dimension(:), intent(inout) :: eff, effsum
      integer :: ievt, ch, ch_read, iostat, nc_channel
      logical :: ok
      if (uwgt /= 0) then
         call msg_message (" WHIZARD run for process " &
              & // trim(mcs%process_id_current), unit=uwgt)
         call msg_message (" Weight distribution:", unit=uwgt)
      end if
      do ch=1, mcs%f%n_channels
         if (mcs%g%num_calls(ch) < 2) cycle
         if (uwgt /= 0) then
            call msg_message (unit=uwgt)
            write (msg_buffer, "(1x,A,1x,I4)") "Channel #", ch
            call msg_message (unit=uwgt)
         end if
         call create_histogram (hh(ch), &
              & 0._default, 1._default, WEIGHT_HISTOGRAMS_NBIN)
         nc_channel = nint (mcs%calls(2,5) * mcs%g%weights(ch))
         do ievt=1, nc_channel
            if (mcs%write_events) then
               call check_event_file_split (uevt, mcs, ok)
               if (.not. ok)  call msg_fatal &
                    & (" Event file number limit exceeded while generating weighted events")
            end if
            if (mcs%read_events_raw) then
               call event_set_beams &
                    & (current_event, mcs%beam, mcs%sqrts, mcs%par)
               if (proc_id == WHIZARD_ROOT) &
                    & call read (uraw, current_event, &
                    &            any(mcs%beam%vector_polarization), &
                    &            compatibility_142, &
                    &            fmt=RAW_FMT, &
                    &            probability=eff(ch), &
                    &            weight=weight, channel=ch_read, &
                    &            iostat=iostat)
!               call mpi90_broadcast (current_event, WHIZARD_ROOT)
               if (ch_read /= ch) &
                    call msg_fatal (" Channel mismatch occured while reading raw event file.")
               if (iostat == 0) then
                  current_event%count = current_event%count + 1
                  current_event%count_file = current_event%count_file + 1
               else if (iostat < 0) then
                  call reopen_raw_events_file (uraw)
               else
                  call msg_fatal (" I/O error occurred while reading raw event file.")
               end if
               if (mcs%recalculate) then
                  call mc_recalculate (mcs, current_event)
               end if
            end if
            if (.not.mcs%read_events_raw) then
               call mc_generate (mcs, &
                    &            probability=eff(ch), &
                    &            weight=weight, channel=ch)
               if (proc_id == WHIZARD_ROOT) then
                  if (mcs%write_events_raw) &
                       & call write (uraw, current_event, &
                       &             any(mcs%beam%vector_polarization), &
                       &             fmt=RAW_FMT, &
                       &             probability=eff(ch), &
                       &             weight=weight, channel=ch)
               end if
            end if
            if (mcs%write_events)  call write_event (uevt, mcs, weight=weight)
            if (mcs%screen_events)  call write_event (6, mcs, weight=weight)
            effsum(ch) = effsum(ch) + eff(ch)
            call fill_histogram (hh(ch), eff(ch))
            call fill_histogram (h, eff(ch))
            call apply (whizard_analysis(mc_process_index), &
                 &      current_event, &
                 &      weight=weight*default_event_weight)
            call terminate_now_maybe (current_event%count)
         end do
         if (uwgt /= 0) then
            call write_histogram (hh(ch), uwgt, over=.true.)
            write(uwgt,*) "!"
            if (nc_channel > 0) then
               efftmp = effsum(ch) / nc_channel
            else
               efftmp = 0
            end if
            write(uwgt,*) "! Efficiency = ", efftmp
         end if
      end do
    end subroutine generate_weighted_single
    
  end subroutine generate_events_weighted

  subroutine open_raw_events_file (uraw)
    integer, intent(out) :: uraw
    character(len=VERSION_STRLEN) :: version_string_file
    character(len=FILENAME_LEN) :: file_read, file_write
    file_read = concat (mcs(1)%directory, choose_filename &
         & (mcs(1)%read_events_raw_file, whizard_filename), "evx")
    file_write = concat (mcs(1)%directory, choose_filename &
         & (mcs(1)%write_events_raw_file, whizard_filename), "evx")
    if (mcs(1)%read_events_raw) then
       if (proc_id == WHIZARD_ROOT) then
          call msg_message &
               & (" Looking for raw event file "//trim(file_read)//" ...")
          inquire (file = trim(file_read), exist = mcs(1)%read_events_raw)
          if (.not.mcs(1)%read_events_raw)  call msg_message &
               & (" ... not found. Events will be generated.")
       end if
       call mpi90_broadcast (mcs(1)%read_events_raw, WHIZARD_ROOT)
    end if
    uraw = free_unit()
    if (proc_id == WHIZARD_ROOT) then
       if (mcs(1)%read_events_raw) then
          call open_raw_for_reading (uraw)
       else if (mcs(1)%write_events_raw) then
          call open_raw_for_writing (uraw)
       end if
    end if
!    call mpi90_broadcast (checksum, WHIZARD_ROOT)
  contains
    subroutine open_raw_for_writing (uraw)
      integer, intent(in) :: uraw
      integer :: i
      open (unit = uraw, file = trim(file_write), &
           & status = 'replace', action = 'write', form = 'unformatted')
      version_string_file = VERSION_STRING
      write(uraw) version_string_file
      write(uraw) mcs(1)%unweighted
      write(uraw) mcs(1)%keep_beam_remnants
      write(uraw) mc_number_processes
      write(uraw) n_events_expected
      do i = 1, mc_number_processes
         write(uraw) mcs(i)%checksum
      end do
    end subroutine open_raw_for_writing
    subroutine open_raw_for_reading (uraw)
      integer, intent(in) :: uraw
      character(32), dimension(mc_number_processes) :: checksum_file
      logical :: unweighted, keep_beam_remnants
      integer :: number_processes
      integer(i64) :: n_events_file
      integer :: ichsum2, iostat, i
      open (unit = uraw, file = trim(file_read), &
           & status = 'old', action = 'read', form = 'unformatted')
      read(uraw, iostat=iostat) version_string_file 
      if (iostat == 0) then
         read(uraw) unweighted
         if (unweighted .neqv. mcs(1)%unweighted)   call msg_fatal &
              & (" Event file has different setting of 'unweighted'")
         read(uraw) keep_beam_remnants
         if (keep_beam_remnants .neqv. mcs(1)%keep_beam_remnants) &
              & call msg_fatal &
              & (" Event file has different setting of 'keep_beam_remnants'")
         read(uraw) number_processes
         if (number_processes /= mc_number_processes)  call msg_fatal &
              & (" Event file has different number of processes")
         read(uraw) n_events_file
         if (.not. unweighted .and. n_events_file /= n_events_expected) &
              & call msg_fatal &
              & (" Event file has different number of (weighted) events")
         do i = 1, mc_number_processes
            read(uraw) checksum_file(i)
         end do
      else                          ! we're probably reading a pre-1.43 file
         rewind (uraw)
         read(uraw) ichsum2
         read(uraw) version_string_file 
      end if
      call check_version
      if (mcs(1)%read_events_force .or. compatibility_142) then
         ! ignore checksum
      else if (any (checksum_file /= mcs%checksum)) then
         call checksum_error ("events")
      end if
    end subroutine open_raw_for_reading
    subroutine check_version
      character(15) :: cdummy
      real :: version
      compatibility_142 = .false.
      if (version_string_file /= VERSION_STRING) then
         call msg_warning (" Raw event file generated by " // &
              &            trim(version_string_file))
         read (version_string_file, "(a15,1x,g5.2)") cdummy, version
         if (version < 1.43)  then
            call msg_warning (" Using compatibility mode for version < 1.43:")
            call msg_message ("          Checksum won't be tested")
            call msg_message ("          Event energy scale missing, will be set equal to c.m. energy")
            compatibility_142 = .true.
         end if
      end if
    end subroutine check_version
  end subroutine open_raw_events_file

  subroutine reopen_raw_events_file (uraw)
    integer, intent(in) :: uraw
    character(len=FILENAME_LEN) :: file_write
    file_write = concat (mcs(1)%directory, choose_filename &
         & (mcs(1)%write_events_raw_file, whizard_filename), "evx")
    mcs%read_events_raw = .false.
    if (proc_id == WHIZARD_ROOT) then
       close (uraw)
       write (msg_buffer, "(1x,A,1x,I12,1x,A)") &
            & " End of file reached after reading", &
            & current_event%count, "events"  
       call msg_warning
       if (mcs(1)%recalculate) then
          call msg_message (" Matrix element recalculation required, but no reference values available.")
          call msg_fatal (" Event generation must be stopped.")
       else
          call msg_message (" Generating fresh events ...")
          if (mcs(1)%write_events_raw) then
             open ( unit = uraw, file = trim(file_write), &
                  & status = 'old', position = 'append', &
                  & action = 'write', form = 'unformatted')
          end if
       end if
    end if
  end subroutine reopen_raw_events_file

  subroutine close_raw_events_file (uraw)
    integer, intent(in) :: uraw
    if (proc_id == WHIZARD_ROOT) then
       if (mcs(1)%read_events_raw .or. mcs(1)%write_events_raw)  close (uraw)
    end if
  end subroutine close_raw_events_file

  subroutine open_output_events_file (uevt)
    integer, intent(out) :: uevt
    character(len=FILENAME_LEN) :: file
    if (proc_id == WHIZARD_ROOT .and. mcs(1)%write_events) then
       current_event%count_file = 0
       current_event%count_bytes = 0
       file = event_file_name (mcs(1))
       if (mcs(1)%unweighted) then
          select case (mcs(1)%write_events_format)
          case (JETSET_FMT1, JETSET_FMT2, JETSET_FMT3)
             call msg_message (" Writing events to file "// trim(file))
             uevt = free_unit ()
             call jetset_open_write (unit = uevt, file = trim(file))
          case (STDHEP_FMT)
             call msg_message (" Writing events to file "// trim(file))
             call stdhep_init &
                  & (trim(file), "WHIZARD event sample", n_events_expected)
             current_event%count_bytes = STDHEP_BYTES_PER_FILE
!             call mpi90_broadcast (current_event%count_bytes, WHIZARD_ROOT)
          case (STDHEP_FMT4)
             call msg_message (" Writing events to file "// trim(file))
             call stdhep_init &
                  & (trim(file), "WHIZARD event sample", n_events_expected)
             current_event%count_bytes = STDHEP_BYTES_PER_FILE
          case (STDHEP_UP_FMT)
             call msg_message (" Writing events to file "// trim(file))
             call stdhep_init &
                  & (trim(file), "WHIZARD event sample", n_events_expected)
             call stdhep_write (STDHEP_HEPRUP)
             current_event%count_bytes = STDHEP_BYTES_PER_FILE
!             call mpi90_broadcast (current_event%count_bytes, WHIZARD_ROOT)
          case (LHEF_FMT)
             call msg_message (" Writing events to file "// trim(file))
             uevt = free_unit()
             open (unit = uevt, file = trim(file), &
                  & status = 'replace', action = 'write')
             call lhef_write_init (uevt, mcs%process_id_current)
             call lhef_write_heprup (uevt, mcs%process_id_current)
          case default
             call msg_message (" Writing events to file "// trim(file))
             uevt = free_unit()
             open (unit = uevt, file = trim(file), &
                  & status = 'replace', action = 'write')
          end select
       else
          select case (mcs(1)%write_events_format)
          case (JETSET_FMT1, JETSET_FMT2, JETSET_FMT3)
             call msg_error (" Writing weighted events in JETSET formats is not supported")
          case (STDHEP_FMT)
             call msg_error (" Writing weighted events in STDHEP/HEPEVT format is not supported")
          case (STDHEP_FMT4)
             call msg_error (" Writing weighted events in STDHEP/HEPEVT, HEPEV4 format is not supported")
          case (LHEF_FMT)
             call msg_message (" Writing weighted events to file "// trim(file))
             uevt = free_unit()
             open (unit = uevt, file = trim(file), &
                  & status = 'replace', action = 'write')
             call lhef_write_init (uevt, mcs%process_id)
             call lhef_write_heprup (uevt, mcs%process_id)
          case default
             call msg_message (" Writing weighted events to file "//trim(file))
             uevt = free_unit()
             open (unit = uevt, file = trim(file), &
                  & status = 'replace', action = 'write')
          end select
       end if
       if (mcs(1)%screen_events) then
          select case (mcs(1)%write_events_format)
          case (STDHEP_FMT, STDHEP_FMT4, STDHEP_UP_FMT)
             call msg_error (" Can't display events in STDHEP format")
             mcs%screen_events = .false.
          case default
             call msg_message (" Displaying generated events ...")
          end select
       end if
    end if
  end subroutine open_output_events_file

  function event_file_name (mcs) result (file)
    type(monte_carlo_state), intent(in) :: mcs
    character(len=FILENAME_LEN) :: basename, file
    character(len=12) :: fmt
    integer :: d
    basename = choose_filename (mcs%write_events_file, whizard_filename)
    if (event_file_count > 0) then
       d = max (int (log10 (real (mcs%max_file_count, kind=default) + 0.5) + 1), 1)
       write (fmt, "(A,I1,A,I1,A)")  "(A,'.',I", d, ".", d, ")"
       write (file, fmt)  trim (basename), event_file_count
       event_file_count = event_file_count + 1
    else
       file = basename
    end if
    select case (mcs%write_events_format)
    case (JETSET_FMT1, JETSET_FMT2, JETSET_FMT3)
       file = concat (mcs%directory, file, "pythia")
    case (STDHEP_FMT, STDHEP_FMT4, STDHEP_UP_FMT)
       file = concat (mcs%directory, file, "stdhep")
    case (LHEF_FMT)
       file = concat (mcs%directory, file, "lhe")
    case default
       file = concat (mcs%directory, file, "evt")
    end select
  end function event_file_name

  subroutine check_event_file_split (uevt, mcs, ok)
    integer, intent(inout) :: uevt
    type(monte_carlo_state), intent(in) :: mcs
    logical, intent(out), optional :: ok
    logical :: close, reopen
    if (present (ok))  ok = .true.
    close = .false.
    reopen = .false.
    if (event_files_open) then
       if (current_event%count > 0) then
          if (mcs%events_per_file > 0) then
             if (current_event%count_file >= mcs%events_per_file) then
                close = .true.
             end if
          end if
          if (mcs%bytes_per_file > 0) then
             select case (mcs%write_events_format)
             case (STDHEP_FMT)
                current_event%count_bytes = current_event%count_bytes + STDHEP_BYTES_PER_EVENT + STDHEP_BYTES_PER_ENTRY*nhep
             case (STDHEP_FMT4)
!               print *, " count,bytes_per_entry,nhep= ", current_event%count,STDHEP_BYTES_PER_ENTRY,nhep
                current_event%count_bytes = current_event%count_bytes + STDHEP_BYTES_PER_EVENT+108 + (STDHEP_BYTES_PER_ENTRY+32)*nhep
             end select
!             print *, " count,count_bytes,bytes_per_file= ", current_event%count,current_event%count_bytes,mcs%bytes_per_file
             if (current_event%count_bytes >= mcs%bytes_per_file) then
                close = .true.
             end if
          end if
       end if
    end if
    if (close) then
       call close_output_events_file (uevt)
       if (event_file_count <= mcs%max_file_count) then
          reopen = .true.
       else
          reopen = .false.
          call terminate_soon (reason = MAX_FILE_COUNT_EXCEEDED)
          if (present (ok))  ok = .false.
       end if
    end if
    if (reopen) then
       call open_output_events_file (uevt)
    end if
  end subroutine check_event_file_split

  subroutine write_event (uevt, mcs, weight)
    integer, intent(in) :: uevt
    type(monte_carlo_state), intent(in) :: mcs
    real(kind=default), intent(in), optional :: weight
    real(kind=default) :: wgt
    if (proc_id == WHIZARD_ROOT) then
       if (mcs%unweighted .and. mcs%fragment) then
          select case (mcs%write_events_format)
          case (LHEF_FMT)
             call lhef_write_hepeup (uevt, mc_process_index)
          case default
             call hepevt_write (uevt, fmt = mcs%write_events_format)
          end select
       else
          if (present (weight)) then
             select case (mcs%write_events_format)
             case (JETSET_FMT1, JETSET_FMT2, JETSET_FMT3, STDHEP_FMT, STDHEP_FMT4)
                return
             end select
             wgt = weight * default_event_weight
          else
             wgt = default_event_weight
          end if
          select case (mcs%write_events_format)
          case (LHEF_FMT)
             call lhef_write_hepeup (uevt, mc_process_index)
          case default
             call write (uevt, current_event, &
                  &      any (mcs%beam%vector_polarization), &
                  &      weight = wgt, &
                  &      fmt = mcs%write_events_format)
          end select
       end if
    end if
  end subroutine write_event

  subroutine close_output_events_file (uevt)
    integer, intent(in) :: uevt
    if (proc_id == WHIZARD_ROOT .and. mcs(1)%write_events) then
       select case (mcs(1)%write_events_format)
       case (JETSET_FMT1, JETSET_FMT2, JETSET_FMT3)
          if (mcs(1)%unweighted)  call jetset_close (uevt)
       case (STDHEP_FMT, STDHEP_FMT4)
          if (mcs(1)%unweighted)  call stdhep_end
       case (STDHEP_UP_FMT)
          call stdhep_end
       case (LHEF_FMT)
          call lhef_write_final (uevt)
          close (uevt)
       case default
          close (uevt)
       end select
    end if
  end subroutine close_output_events_file

  subroutine whizard_end
    if (event_files_open)  call whizard_events_end
    deallocate (process_probability)
    if (mcs_exists) then
       !!![tho:] segfault w/ ifort 9.1.040 (at least), still in 10.1.012
       !!! call destroy (mcs)
       return ! XXX
       deallocate (mcs) 
       call destroy (current_event)
       call tao_random_destroy (rng)
       mcs_exists = .false.
    end if
    if (whizard_input_exists) then
       call destroy (whizard_input)
       whizard_input_exists = .false.
    end if
    call msg_summary
    call msg_message (" WHIZARD run finished.")
    call msg_list_clear
!    print *, "! WHIZARD run finished"
    if (mpi_initialized) then
       call mpi90_finalize
       mpi_initialized = .false.
    end if
    call restore_signals
  end subroutine whizard_end

end module whizard
