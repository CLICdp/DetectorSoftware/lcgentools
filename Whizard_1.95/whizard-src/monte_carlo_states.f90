! WHIZARD 1.95 Feb 25 2010
! 
! (C) 1999-2008 by 
!     Wolfgang Kilian <wolfgang.kilian@desy.de>
!     Thorsten Ohl <ohl@physik.uni-wuerzburg.de>
!     Juergen Reuter <juergen.reuter@physik.uni-freiburg.de>
!
! WHIZARD is free software; you can redistribute it and/or modify it
! under the terms of the GNU General Public License as published by 
! the Free Software Foundation; either version 2, or (at your option)
! any later version.
!
! WHIZARD is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! This file has been stripped of most comments.  For documentation, refer
! to the source 'whizard.nw'

module monte_carlo_states

  use kinds, only: default !NODEP!
  use kinds, only: i32, i64 !NODEP!
  use file_utils, only: free_unit !NODEP!
  use mpi90, only: mpi90_broadcast, mpi90_rank !NODEP!
  use limits, only: PROCESS_ID_LEN, BUFFER_SIZE
  use limits, only: WHIZARD_ROOT, VERSION_STRING
  use limits, only: LOGTABLE_LINE_LENGTH
  use limits, only: FILENAME_LEN, PYTHIA_PARAMETERS_LEN
  use files, only: concat, choose_filename, file_exists_else_default
  use files, only: write
  use md5, only: md5test, md5sum
  use parser, only: get_string, lex_clear
  use parameters !NODEP!
  use particles
  use density_vectors, only: state_density_t, create, destroy
  use beams, only: beam_data_t, beam_setup
  use beams, only: create, destroy, read, write, mpi90_broadcast
  use partons, only: parton_state, create, destroy
  use cascades, only: phase_space_switches
  use decay_forests
  use showers, only: shower_parameters_t, shower_parameters_init
  use events
  use cuts
  use diagnostics, only: msg_buffer, msg_level, msg_result, msg_message
  use diagnostics, only: msg_warning, msg_fatal, msg_error, msg_bug
  use diagnostics, only: msg_terminate
  use clock, only: time, operator(+)
  use tao_random_numbers !NODEP!
  use vamp, only: vamp_grids, vamp_history !NODEP!
  use vamp, only: vamp_delete_grids !NODEP!
  use vamp, only: vamp_create_history, vamp_write_history !NODEP!
  use vamp_equivalences, only: vamp_equivalence_list, destroy, write !NODEP!
  use slha_interface, only: slha_data, create, destroy, read, write
  use slha_interface, only: contains_whizard_input, write_whizard_input
  use slha_interface, only: slha_write_process
  use slha_interface, only: slha_write_whizard_info
  use slha_interface, only: slha_set, parameters_set

  implicit none
  private

  public :: create, destroy
  public :: read, write
  public :: mpi90_broadcast
  public :: get_number_processes
  public :: mc_checksum
  public :: checksum_error
  public :: mc_write_header
  public :: mc_write_header_summary
  public :: mc_write_hline, mc_write_dline
  public :: mc_write_result
  public :: mc_read_result
  public :: mc_get_efficiency
  public :: mc_get_accuracy

  type, public :: monte_carlo_input
     !character(len=PROCESS_ID_LEN*NUMBER_PROCESSES_MAX) :: process_id
     character(len=BUFFER_SIZE) :: process_id
     real(kind=default) :: luminosity
     logical :: polarized_beams
     logical :: structured_beams
     logical :: beam_recoil
     logical :: recoil_conserve_momentum
     character(len=FILENAME_LEN) :: filename
     character(len=FILENAME_LEN) :: directory
     character(len=FILENAME_LEN) :: input_file
     logical :: input_slha_format
     logical :: cm_frame
     real(kind=default) :: sqrts
     integer(kind=default), dimension(2,5) :: calls
     integer :: seed
     logical :: reset_seed_each_process
     real(kind=default) :: accuracy_goal
     real(kind=default) :: efficiency_goal
     integer :: time_limit_adaptation
     logical :: stratified
     logical :: use_efficiency
     real(kind=default) :: weights_power
     integer :: min_bins
     integer :: max_bins
     integer :: min_calls_per_bin
     integer :: min_calls_per_channel
     logical :: write_grids
     logical :: write_grids_raw
     character(len=FILENAME_LEN) :: write_all_grids_file
     logical :: write_all_grids
     character(len=FILENAME_LEN) :: write_grids_file
     logical :: read_grids
     logical :: read_grids_raw
     logical :: read_grids_force
     character(len=FILENAME_LEN) :: read_grids_file
     logical :: generate_phase_space
     character(len=FILENAME_LEN) :: read_model_file
     character(len=FILENAME_LEN) :: write_phase_space_channels_file
     character(len=FILENAME_LEN) :: write_phase_space_file
     logical :: read_phase_space
     character(len=FILENAME_LEN) :: read_phase_space_file
     logical :: overwrite_phase_space
     logical :: phase_space_only
     logical :: matrix_element_only
     logical :: use_equivalences
     logical :: azimuthal_dependence
     character(len=FILENAME_LEN) :: write_default_cuts_file
     character(len=FILENAME_LEN) :: read_cuts_file
     integer :: user_cut_mode
     integer :: user_weight_mode
     integer :: handle_negative_weight
     integer :: off_shell_lines
     integer :: extra_off_shell_lines
     integer :: splitting_depth
     integer :: exchange_lines
     logical :: show_deleted_channels
     logical :: single_off_shell_decays
     logical :: double_off_shell_decays
     logical :: single_off_shell_branchings
     logical :: double_off_shell_branchings
     logical :: massive_fsr
     real(kind=default) :: threshold_mass
     real(kind=default) :: threshold_mass_t
     real(kind=default) :: default_jet_cut
     real(kind=default) :: default_mass_cut
     real(kind=default) :: default_energy_cut
     real(kind=default) :: default_q_cut
     integer :: n_events
     integer(i64) :: n_calls
     integer :: n_events_warmup
     logical :: unweighted
     logical :: normalize_weight
     logical :: write_weights
     character(len=FILENAME_LEN) :: write_weights_file
     real(kind=default) :: safety_factor
     logical :: write_events_raw
     character(len=FILENAME_LEN) :: write_events_raw_file
     logical :: write_events
     integer :: write_events_format
     character(len=FILENAME_LEN) :: write_events_file
     integer :: events_per_file
     integer :: bytes_per_file
     integer :: min_file_count
     integer :: max_file_count
     logical :: read_events_raw
     character(len=FILENAME_LEN) :: read_events_raw_file
     logical :: read_events
     logical :: read_events_force
     logical :: keep_beam_remnants
     logical :: keep_initials
     logical :: guess_color_flow
     logical :: recalculate
     logical :: fragment
     integer :: fragmentation_method
     integer :: user_fragmentation_mode
     character(len=BUFFER_SIZE) :: pythia_processes
     character(len=PYTHIA_PARAMETERS_LEN) :: pythia_parameters
     logical :: shower
     integer :: shower_nf
     logical :: shower_running_alpha_s
     real(default) :: shower_alpha_s
     real(default) :: shower_lambda
     real(default) :: shower_t_min
     real(default) :: shower_md, shower_mu, shower_ms, shower_mc, shower_mb
     integer :: chattiness
     logical :: catch_signals
     integer :: time_limit
     logical :: warn_empty_channel
     logical :: screen_results
     logical :: screen_events
     logical :: screen_histograms
     logical :: screen_diagnostics
     logical :: show_pythia_banner
     logical :: show_pythia_initialization
     logical :: show_pythia_statistics
     logical :: write_logfile
     logical :: plot_history
     character(len=FILENAME_LEN) :: write_logfile_file
     logical :: show_input
     logical :: show_results
     logical :: show_phase_space
     logical :: show_cuts
     logical :: show_histories
     logical :: show_history
     logical :: show_weights
     logical :: show_event
     logical :: show_histograms
     logical :: show_overflow
     logical :: show_excess
     character(len=FILENAME_LEN) :: read_analysis_file
     integer :: plot_width, plot_height
     logical :: plot_histograms, plot_curves, plot_symbols
     logical :: plot_smoothly, plot_errors
     logical :: plot_excess
     logical :: plot_per_bin
     character(len=BUFFER_SIZE) :: plot_grids_channels
     real(kind=default) :: plot_grids_logscale
     logical :: slha_rewrite_input
     logical :: slha_ignore_errors
     type(parameter_set) :: par
     type(beam_data_t), dimension(:), allocatable :: beam
     type(slha_data), pointer :: slha
  end type monte_carlo_input

  type, public :: monte_carlo_state
     !character(len=PROCESS_ID_LEN*NUMBER_PROCESSES_MAX) :: process_id
     character(len=BUFFER_SIZE) :: process_id
     real(kind=default) :: luminosity
     logical :: polarized_beams
     logical :: structured_beams
     logical :: beam_recoil
     logical :: recoil_conserve_momentum
     character(len=FILENAME_LEN) :: filename
     character(len=FILENAME_LEN) :: directory
     character(len=FILENAME_LEN) :: input_file
     logical :: input_slha_format
     logical :: cm_frame
     real(kind=default) :: sqrts
     integer, dimension(2,5) :: calls
     integer :: seed
     logical :: reset_seed_each_process
     real(kind=default) :: accuracy_goal
     real(kind=default) :: efficiency_goal
     integer :: time_limit_adaptation
     logical :: stratified
     logical :: use_efficiency
     real(kind=default) :: weights_power
     integer :: min_bins
     integer :: max_bins
     integer :: min_calls_per_bin
     integer :: min_calls_per_channel
     logical :: write_grids
     logical :: write_grids_raw
     character(len=FILENAME_LEN) :: write_all_grids_file
     logical :: write_all_grids
     character(len=FILENAME_LEN) :: write_grids_file
     logical :: read_grids
     logical :: read_grids_raw
     logical :: read_grids_force
     character(len=FILENAME_LEN) :: read_grids_file
     logical :: generate_phase_space
     character(len=FILENAME_LEN) :: read_model_file
     character(len=FILENAME_LEN) :: write_phase_space_channels_file
     character(len=FILENAME_LEN) :: write_phase_space_file
     logical :: read_phase_space
     character(len=FILENAME_LEN) :: read_phase_space_file
     logical :: overwrite_phase_space
     logical :: phase_space_only
     logical :: matrix_element_only
     logical :: use_equivalences
     logical :: azimuthal_dependence
     character(len=FILENAME_LEN) :: write_default_cuts_file
     character(len=FILENAME_LEN) :: read_cuts_file
     integer :: user_cut_mode
     integer :: user_weight_mode
     integer :: handle_negative_weight
     real(kind=default) :: default_jet_cut
     real(kind=default) :: default_mass_cut
     real(kind=default) :: default_energy_cut
     real(kind=default) :: default_q_cut
     integer :: n_events
     integer(i64) :: n_calls
     integer :: n_events_warmup
     logical :: unweighted
     logical :: normalize_weight
     logical :: write_weights
     character(len=FILENAME_LEN) :: write_weights_file
     real(kind=default) :: safety_factor
     logical :: write_events_raw
     character(len=FILENAME_LEN) :: write_events_raw_file
     logical :: write_events
     integer :: write_events_format
     character(len=FILENAME_LEN) :: write_events_file
     integer :: events_per_file
     integer :: bytes_per_file
     integer :: min_file_count
     integer :: max_file_count
     logical :: read_events_raw
     character(len=FILENAME_LEN) :: read_events_raw_file
     logical :: read_events
     logical :: read_events_force
     logical :: keep_beam_remnants
     logical :: keep_initials
     logical :: guess_color_flow
     logical :: recalculate
     logical :: fragment
     integer :: fragmentation_method
     integer :: user_fragmentation_mode
     character(len=BUFFER_SIZE) :: pythia_processes
     character(len=PYTHIA_PARAMETERS_LEN) :: pythia_parameters
     integer :: chattiness
     logical :: catch_signals
     integer :: time_limit
     logical :: warn_empty_channel
     logical :: screen_results
     logical :: screen_events
     logical :: screen_histograms
     logical :: screen_diagnostics
     logical :: show_pythia_banner
     logical :: show_pythia_initialization
     logical :: show_pythia_statistics
     logical :: write_logfile
     logical :: plot_history
     character(len=FILENAME_LEN) :: write_logfile_file
     logical :: show_input
     logical :: show_results
     logical :: show_phase_space
     logical :: show_cuts
     logical :: show_histories
     logical :: show_history
     logical :: show_weights
     logical :: show_event
     logical :: show_histograms
     logical :: show_overflow
     logical :: show_excess
     character(len=FILENAME_LEN) :: read_analysis_file
     integer :: plot_width, plot_height
     logical :: plot_histograms, plot_curves, plot_symbols
     logical :: plot_smoothly, plot_errors
     logical :: plot_excess
     logical :: plot_per_bin
     character(len=BUFFER_SIZE) :: plot_grids_channels
     real(kind=default) :: plot_grids_logscale
     logical :: slha_rewrite_input
     logical :: slha_ignore_errors
     type(parameter_set) :: par
     type(beam_data_t), dimension(:), allocatable :: beam
     type(state_density_t) :: rho_out
     type(shower_parameters_t) :: spar
     character(len=PROCESS_ID_LEN) :: process_id_current
     real(kind=default) :: process_factor
     type(time) :: program_start_time, program_stop_time
     type(time) :: adaptation_start_time, adaptation_stop_time
     integer :: n_in, n_out, n_tot, n_col, n_hel, n_flv, n_flvz
     integer :: n_col_in, n_col_out, n_hel_in, n_hel_out, n_flv_in, n_flv_out
     integer :: n_flvz_in, n_flvz_out
     integer, dimension(:,:), pointer :: code
     integer, dimension(:,:), pointer :: code_in
     integer, dimension(:,:), pointer :: code_out
     integer, dimension(:,:), pointer :: code_zero
     integer, dimension(:,:), pointer :: code_zero_in
     integer, dimension(:,:), pointer :: code_zero_out
     integer, dimension(:,:), pointer :: hel_state
     integer, dimension(:,:), pointer :: hel_state_in
     integer, dimension(:,:), pointer :: hel_state_out
     integer, dimension(:,:), pointer :: col_flow
     integer, dimension(:,:), pointer :: acl_flow
     integer :: n_iterations_whizard
     integer :: n_iterations_vamp
     integer :: n_channels
     type(phase_space_switches) :: phs
     type(decay_forest) :: f
     type(default_cuts) :: cuts
     logical :: generating_events
     type(cut_array) :: cc
     character(32) :: checksum
     integer :: it, it_vamp
     type(vamp_grids) :: g, g_best
     type(vamp_equivalence_list) :: eq
     type(vamp_history), dimension(:), pointer :: history
     type(vamp_history), dimension(:,:), pointer :: histories
     character(len=LOGTABLE_LINE_LENGTH), dimension(:), pointer :: logtable
     real(kind=default), dimension(:,:), pointer :: weights
     logical :: actual_integration_done
     real(kind=default) :: integral, error, chi2, efficiency, accuracy
     type(parton_state) :: partons
  end type monte_carlo_state

  integer, parameter, public :: &
       & NO_FRAGMENTATION=0, JETSET_FRAGMENTATION=1, PYTHIA_FRAGMENTATION=2
  integer, parameter, public :: USER_FRAGMENTATION=3

  type(monte_carlo_state), dimension(:), allocatable, public :: mcs
  integer, public :: mc_process_index = 0, mc_number_processes = 0
  type(event), public :: current_event
  type(tao_random_state), public :: rng

  character(5), dimension(2), parameter :: PHYS_UNIT = (/ "[GeV]", "[fb] " /)

  save

  interface create
     module procedure mc_input_create
  end interface
  interface destroy
     module procedure mc_input_destroy
  end interface
  interface read
     module procedure mc_input_read_unit
     module procedure mc_input_read_name
  end interface
  interface write
     module procedure mc_input_write_unit
     module procedure mc_input_write_name
  end interface
  interface mpi90_broadcast
     module procedure mc_input_broadcast
  end interface
  interface mpi90_broadcast
     module procedure character_broadcast
  end interface
  
  interface mpi90_broadcast
     module procedure parameter_set_broadcast
  end interface
  interface
     function n_tot (proc_id)
       character(len=*), intent(in) :: proc_id
       integer :: n_tot
     end function n_tot
  end interface
  interface
     function n_in (proc_id)
       character(len=*), intent(in) :: proc_id
       integer :: n_in
     end function n_in
  end interface
  interface
     function n_out (proc_id)
       character(len=*), intent(in) :: proc_id
       integer :: n_out
     end function n_out
  end interface
  interface
     function n_col (proc_id)
       character(len=*), intent(in) :: proc_id
       integer :: n_col
     end function n_col
  end interface
  interface
     function n_hel (proc_id)
       character(len=*), intent(in) :: proc_id
       integer :: n_hel
     end function n_hel
  end interface
  interface
     function n_hel_in (proc_id)
       character(len=*), intent(in) :: proc_id
       integer :: n_hel_in
     end function n_hel_in
  end interface
  interface
     function n_hel_out (proc_id)
       character(len=*), intent(in) :: proc_id
       integer :: n_hel_out
     end function n_hel_out
  end interface
  interface
     function n_flv (proc_id)
       character(len=*), intent(in) :: proc_id
       integer :: n_flv
     end function n_flv
  end interface
  interface
     function n_flv_in (proc_id)
       character(len=*), intent(in) :: proc_id
       integer :: n_flv_in
     end function n_flv_in
  end interface
  interface
     function n_flv_out (proc_id)
       character(len=*), intent(in) :: proc_id
       integer :: n_flv_out
     end function n_flv_out
  end interface
  interface
     function n_flvz (proc_id)
       character(len=*), intent(in) :: proc_id
       integer :: n_flvz
     end function n_flvz
  end interface
  interface
     function n_flvz_in (proc_id)
       character(len=*), intent(in) :: proc_id
       integer :: n_flvz_in
     end function n_flvz_in
  end interface
  interface
     function n_flvz_out (proc_id)
       character(len=*), intent(in) :: proc_id
       integer :: n_flvz_out
     end function n_flvz_out
  end interface
  interface
     subroutine flv_states (hel, proc_id)
       character(len=*), intent(in) :: proc_id
       integer, dimension(:,:), intent(inout) :: hel
     end subroutine flv_states
  end interface
  interface
     subroutine flv_states_in (hel, proc_id)
       character(len=*), intent(in) :: proc_id
       integer, dimension(:,:), intent(inout) :: hel
     end subroutine flv_states_in
  end interface
  interface
     subroutine flv_states_out (hel, proc_id)
       character(len=*), intent(in) :: proc_id
       integer, dimension(:,:), intent(inout) :: hel
     end subroutine flv_states_out
  end interface
  interface
     subroutine flv_zeros (hel, proc_id)
       character(len=*), intent(in) :: proc_id
       integer, dimension(:,:), intent(inout) :: hel
     end subroutine flv_zeros
  end interface
  interface
     subroutine flv_zeros_in (hel, proc_id)
       character(len=*), intent(in) :: proc_id
       integer, dimension(:,:), intent(inout) :: hel
     end subroutine flv_zeros_in
  end interface
  interface
     subroutine flv_zeros_out (hel, proc_id)
       character(len=*), intent(in) :: proc_id
       integer, dimension(:,:), intent(inout) :: hel
     end subroutine flv_zeros_out
  end interface
  interface
     subroutine hel_states (hel, proc_id)
       character(len=*), intent(in) :: proc_id
       integer, dimension(:,:), intent(inout) :: hel
     end subroutine hel_states
  end interface
  interface
     subroutine hel_states_in (hel, proc_id)
       character(len=*), intent(in) :: proc_id
       integer, dimension(:,:), intent(inout) :: hel
     end subroutine hel_states_in
  end interface
  interface
     subroutine hel_states_out (hel, proc_id)
       character(len=*), intent(in) :: proc_id
       integer, dimension(:,:), intent(inout) :: hel
     end subroutine hel_states_out
  end interface
  interface
     subroutine col_flows (index, proc_id)
       character(len=*), intent(in) :: proc_id
       integer, dimension(:,:), intent(inout) :: index
     end subroutine col_flows
  end interface
  interface
     subroutine acl_flows (index, proc_id)
       character(len=*), intent(in) :: proc_id
       integer, dimension(:,:), intent(inout) :: index
     end subroutine acl_flows
  end interface
  interface
     function process_active (proc_id) result (active)
       character(len=*), intent(in) :: proc_id
       logical :: active
     end function process_active
  end interface
  interface create
     module procedure mc_state_create_multi, mc_state_create_single
  end interface
  interface destroy
     module procedure mc_state_destroy_multi, mc_state_destroy_single
  end interface
  interface write
     module procedure mc_state_write_unit
     module procedure mc_state_write_name_single
     module procedure mc_state_write_name_multi
  end interface
  interface checksum_error
     module procedure mc_checksum_error
  end interface
  interface mc_write_header
     module procedure mc_write_header_unit
  end interface
  interface mc_write_hline
     module procedure mc_write_hline_unit
  end interface
  interface mc_write_dline
     module procedure mc_write_dline_unit
  end interface
  interface mc_write_result
     module procedure mc_write_result_string
     module procedure mc_write_result_unit
     module procedure mc_write_summary_unit
     module procedure mc_write_logtable_unit
  end interface
  interface mc_read_result
     module procedure mc_read_logtable_unit
  end interface
  interface mc_get_accuracy
     module procedure mc_get_accuracy_integer, mc_get_accuracy_real
  end interface

contains

  subroutine mc_input_create (mci)
    type(monte_carlo_input), intent(out) :: mci
    allocate (mci%beam(2))
    call create (mci%beam)
    mci%process_id = " "
    mci%luminosity = 0
    mci%polarized_beams = .false.
    mci%structured_beams = .false.
    mci%beam_recoil = .false.
    mci%recoil_conserve_momentum = .false.
    mci%filename = ""
    mci%directory = ""
    mci%input_file = ""
    mci%input_slha_format = .false.
    mci%cm_frame = .true.
    mci%sqrts = 0
    mci%calls = 0
    call system_clock(mci%seed)
    mci%reset_seed_each_process = .false.
    mci%accuracy_goal = 0
    mci%efficiency_goal = 100
    mci%time_limit_adaptation = 0
    mci%stratified = .true.
    mci%use_efficiency = .false.
    mci%weights_power = 0.25_default
    mci%min_bins = 3
    mci%max_bins = 20
    mci%min_calls_per_bin = 10
    mci%min_calls_per_channel = 0
    mci%write_grids = .true.
    mci%write_grids_raw = .false.
    mci%write_all_grids_file = ""
    mci%write_all_grids = .false.
    mci%write_grids_file = ""
    mci%read_grids = .false.
    mci%read_grids_raw = .false.
    mci%read_grids_force = .true.
    mci%read_grids_file = ""
    mci%generate_phase_space = .true.
    mci%read_model_file = ""
    mci%write_phase_space_channels_file = ""
    mci%write_phase_space_file = ""
    mci%read_phase_space = .true.
    mci%read_phase_space_file = ""
    mci%overwrite_phase_space = .true.
    mci%phase_space_only = .false.
    mci%matrix_element_only = .false.
    mci%use_equivalences = .true.
    mci%azimuthal_dependence = .false.
    mci%off_shell_lines = 1
    mci%extra_off_shell_lines = 1
    mci%splitting_depth = 1
    mci%exchange_lines = 3
    mci%show_deleted_channels = .false.
    mci%single_off_shell_decays = .true.
    mci%double_off_shell_decays = .false.
    mci%single_off_shell_branchings = .true.
    mci%double_off_shell_branchings = .true.
    mci%massive_fsr = .true.
    mci%threshold_mass = -10
    mci%threshold_mass_t = -10
    mci%default_jet_cut = 10
    mci%default_mass_cut = 10
    mci%default_energy_cut = 10
    mci%default_q_cut = 10
    mci%write_default_cuts_file = ""
    mci%read_cuts_file = ""
    mci%user_cut_mode = 0
    mci%user_weight_mode = 0
    mci%handle_negative_weight = 4
    mci%n_events = 0
    mci%n_calls = 0
    mci%n_events_warmup = 0
    mci%unweighted = .true.
    mci%normalize_weight = .true.
    mci%write_weights = .false.
    mci%write_weights_file = ""
    mci%safety_factor = 1
    mci%write_events_raw = .true.
    mci%write_events_raw_file = ""
    mci%write_events = .false.
    mci%write_events_format = HEPEVT_FMT
    mci%write_events_file = ""
    mci%events_per_file = 0
    mci%bytes_per_file = 0
    mci%min_file_count = 1
    mci%max_file_count = 999
    mci%read_events_raw = .false.
    mci%read_events_raw_file = ""
    mci%read_events = .false.
    mci%read_events_force = .true.
    mci%keep_beam_remnants = .false.
    mci%keep_initials = .false.
    mci%guess_color_flow = .false.
    mci%recalculate = .false.
    mci%shower = .false.
    mci%shower_nf = 5
    mci%shower_running_alpha_s = .false.
    mci%shower_alpha_s = 0.2_default
    mci%shower_lambda = 0.29_default
    mci%shower_t_min = 1._default
    mci%shower_md = 0.330_default
    mci%shower_mu = 0.330_default
    mci%shower_ms = 0.500_default
    mci%shower_mc = 1.5_default
    mci%shower_mb = 4.8_default
    mci%fragment = .false.
    mci%fragmentation_method = JETSET_FRAGMENTATION
    mci%user_fragmentation_mode = 0
    mci%pythia_processes = ' '
    mci%pythia_parameters = ' '
    mci%chattiness = 4
    mci%catch_signals = .true.
    mci%time_limit = 0
    mci%warn_empty_channel = .false.
    mci%screen_results = .true.
    mci%screen_events = .false.
    mci%screen_histograms = .false.
    mci%screen_diagnostics = .false.
    mci%show_pythia_banner = .true.
    mci%show_pythia_initialization = .true.
    mci%show_pythia_statistics = .true.
    mci%write_logfile = .true.
    mci%plot_history = .true.
    mci%write_logfile_file = ""
    mci%show_input = .true.
    mci%show_results = .true.
    mci%show_phase_space = .false.
    mci%show_cuts = .true.
    mci%show_histories = .false.
    mci%show_history = .true.
    mci%show_weights = .true.
    mci%show_event = .false.
    mci%show_histograms = .false.
    mci%show_overflow = .false.
    mci%show_excess = .true.
    mci%read_analysis_file = ""
    mci%plot_width = 130
    mci%plot_height = 90
    mci%plot_histograms = .true.
    mci%plot_curves = .false.
    mci%plot_symbols = .false.
    mci%plot_smoothly = .false.
    mci%plot_errors = .false.
    mci%plot_excess = .true.
    mci%plot_per_bin = .false.
    mci%plot_grids_channels = ""
    mci%plot_grids_logscale = 10
    mci%slha_rewrite_input = .true.
    mci%slha_ignore_errors = .false.
    call init (mci%par)
    nullify (mci%slha)
  end subroutine mc_input_create

  subroutine mc_input_destroy (mci)
    type(monte_carlo_input), intent(inout) :: mci
    call destroy (mci%beam)
    deallocate (mci%beam)
    if (associated (mci%slha)) then
       call destroy (mci%slha)
       deallocate (mci%slha)
    end if
  end subroutine mc_input_destroy

  subroutine mc_input_read_unit (u, mci)
    integer, intent(in) :: u
    type(monte_carlo_input), intent(inout) :: mci
    character(len=BUFFER_SIZE) :: first_line
    logical :: read_namelists
    !character(len=PROCESS_ID_LEN*NUMBER_PROCESSES_MAX) :: process_id
    character(len=BUFFER_SIZE) :: process_id
    real(kind=default) :: luminosity
    logical :: polarized_beams
    logical :: structured_beams
    logical :: beam_recoil
    logical :: recoil_conserve_momentum
    character(len=FILENAME_LEN) :: filename
    character(len=FILENAME_LEN) :: directory
    character(len=FILENAME_LEN) :: input_file
    logical :: input_slha_format
    logical :: cm_frame
    real(kind=default) :: sqrts
    integer, dimension(2,5) :: calls
    integer :: seed
    logical :: reset_seed_each_process
    real(kind=default) :: accuracy_goal
    real(kind=default) :: efficiency_goal
    integer :: time_limit_adaptation
    logical :: stratified
    logical :: use_efficiency
    real(kind=default) :: weights_power
    integer :: min_bins
    integer :: max_bins
    integer :: min_calls_per_bin
    integer :: min_calls_per_channel
    logical :: write_grids
    logical :: write_grids_raw
    character(len=FILENAME_LEN) :: write_all_grids_file
    logical :: write_all_grids
    character(len=FILENAME_LEN) :: write_grids_file
    logical :: read_grids
    logical :: read_grids_raw
    logical :: read_grids_force
    character(len=FILENAME_LEN) :: read_grids_file
    logical :: generate_phase_space
    character(len=FILENAME_LEN) :: read_model_file
    character(len=FILENAME_LEN) :: write_phase_space_channels_file
    character(len=FILENAME_LEN) :: write_phase_space_file
    logical :: read_phase_space
    character(len=FILENAME_LEN) :: read_phase_space_file
    logical :: overwrite_phase_space
    logical :: phase_space_only
    logical :: matrix_element_only
    logical :: use_equivalences
    logical :: azimuthal_dependence
    character(len=FILENAME_LEN) :: write_default_cuts_file
    character(len=FILENAME_LEN) :: read_cuts_file
    integer :: user_cut_mode
    integer :: user_weight_mode
    integer :: handle_negative_weight
    integer :: off_shell_lines
    integer :: extra_off_shell_lines
    integer :: splitting_depth
    integer :: exchange_lines
    logical :: show_deleted_channels
    logical :: single_off_shell_decays
    logical :: double_off_shell_decays
    logical :: single_off_shell_branchings
    logical :: double_off_shell_branchings
    logical :: massive_fsr
    real(kind=default) :: threshold_mass
    real(kind=default) :: threshold_mass_t
    real(kind=default) :: default_jet_cut
    real(kind=default) :: default_mass_cut
    real(kind=default) :: default_energy_cut
    real(kind=default) :: default_q_cut
    integer :: n_events
    integer(i64) :: n_calls
    integer :: n_events_warmup
    logical :: unweighted
    logical :: normalize_weight
    logical :: write_weights
    character(len=FILENAME_LEN) :: write_weights_file
    real(kind=default) :: safety_factor
    logical :: write_events_raw
    character(len=FILENAME_LEN) :: write_events_raw_file
    logical :: write_events
    integer :: write_events_format
    character(len=FILENAME_LEN) :: write_events_file
    integer :: events_per_file
    integer :: bytes_per_file
    integer :: min_file_count
    integer :: max_file_count
    logical :: read_events_raw
    character(len=FILENAME_LEN) :: read_events_raw_file
    logical :: read_events
    logical :: read_events_force
    logical :: keep_beam_remnants
    logical :: keep_initials
    logical :: guess_color_flow
    logical :: recalculate
    logical :: fragment
    integer :: fragmentation_method
    integer :: user_fragmentation_mode
    character(len=BUFFER_SIZE) :: pythia_processes
    character(len=PYTHIA_PARAMETERS_LEN) :: pythia_parameters
    logical :: shower
    integer :: shower_nf
    logical :: shower_running_alpha_s
    real(default) :: shower_alpha_s
    real(default) :: shower_lambda
    real(default) :: shower_t_min
    real(default) :: shower_md, shower_mu, shower_ms, shower_mc, shower_mb
    integer :: chattiness
    logical :: catch_signals
    integer :: time_limit
    logical :: warn_empty_channel
    logical :: screen_results
    logical :: screen_events
    logical :: screen_histograms
    logical :: screen_diagnostics
    logical :: show_pythia_banner
    logical :: show_pythia_initialization
    logical :: show_pythia_statistics
    logical :: write_logfile
    logical :: plot_history
    character(len=FILENAME_LEN) :: write_logfile_file
    logical :: show_input
    logical :: show_results
    logical :: show_phase_space
    logical :: show_cuts
    logical :: show_histories
    logical :: show_history
    logical :: show_weights
    logical :: show_event
    logical :: show_histograms
    logical :: show_overflow
    logical :: show_excess
    character(len=FILENAME_LEN) :: read_analysis_file
    integer :: plot_width, plot_height
    logical :: plot_histograms, plot_curves, plot_symbols
    logical :: plot_smoothly, plot_errors
    logical :: plot_excess
    logical :: plot_per_bin
    character(len=BUFFER_SIZE) :: plot_grids_channels
    real(kind=default) :: plot_grids_logscale
    logical :: slha_rewrite_input
    logical :: slha_ignore_errors
    namelist /process_input/ process_id
    namelist /process_input/ luminosity
    namelist /process_input/ polarized_beams
    namelist /process_input/ structured_beams
    namelist /process_input/ beam_recoil
    namelist /process_input/ recoil_conserve_momentum
    namelist /process_input/ filename
    namelist /process_input/ directory
    namelist /process_input/ input_file
    namelist /process_input/ input_slha_format
    namelist /process_input/ cm_frame
    namelist /process_input/ sqrts
    namelist /integration_input/ calls
    namelist /integration_input/ seed
    namelist /integration_input/ reset_seed_each_process
    namelist /integration_input/ accuracy_goal
    namelist /integration_input/ efficiency_goal
    namelist /integration_input/ time_limit_adaptation
    namelist /integration_input/ stratified
    namelist /integration_input/ use_efficiency
    namelist /integration_input/ weights_power
    namelist /integration_input/ min_bins
    namelist /integration_input/ max_bins
    namelist /integration_input/ min_calls_per_bin
    namelist /integration_input/ min_calls_per_channel
    namelist /integration_input/ write_grids
    namelist /integration_input/ write_grids_raw
    namelist /integration_input/ write_all_grids_file
    namelist /integration_input/ write_all_grids
    namelist /integration_input/ write_grids_file
    namelist /integration_input/ read_grids
    namelist /integration_input/ read_grids_raw
    namelist /integration_input/ read_grids_force
    namelist /integration_input/ read_grids_file
    namelist /integration_input/ generate_phase_space
    namelist /integration_input/ read_model_file
    namelist /integration_input/ write_phase_space_channels_file
    namelist /integration_input/ write_phase_space_file
    namelist /integration_input/ read_phase_space
    namelist /integration_input/ read_phase_space_file
    namelist /integration_input/ overwrite_phase_space
    namelist /integration_input/ phase_space_only
    namelist /integration_input/ matrix_element_only
    namelist /integration_input/ use_equivalences
    namelist /integration_input/ azimuthal_dependence
    namelist /integration_input/ off_shell_lines
    namelist /integration_input/ extra_off_shell_lines
    namelist /integration_input/ splitting_depth
    namelist /integration_input/ exchange_lines
    namelist /integration_input/ show_deleted_channels
    namelist /integration_input/ single_off_shell_decays
    namelist /integration_input/ double_off_shell_decays
    namelist /integration_input/ single_off_shell_branchings
    namelist /integration_input/ double_off_shell_branchings
    namelist /integration_input/ massive_fsr
    namelist /integration_input/ threshold_mass
    namelist /integration_input/ threshold_mass_t
    namelist /integration_input/ default_jet_cut
    namelist /integration_input/ default_mass_cut
    namelist /integration_input/ default_energy_cut
    namelist /integration_input/ default_q_cut
    namelist /integration_input/ write_default_cuts_file
    namelist /integration_input/ read_cuts_file
    namelist /integration_input/ user_cut_mode
    namelist /integration_input/ user_weight_mode
    namelist /integration_input/ handle_negative_weight
    namelist /simulation_input/ n_events
    namelist /simulation_input/ n_calls
    namelist /simulation_input/ n_events_warmup
    namelist /simulation_input/ unweighted
    namelist /simulation_input/ normalize_weight
    namelist /simulation_input/ write_weights
    namelist /simulation_input/ write_weights_file
    namelist /simulation_input/ safety_factor
    namelist /simulation_input/ write_events_raw
    namelist /simulation_input/ write_events_raw_file
    namelist /simulation_input/ write_events
    namelist /simulation_input/ write_events_format
    namelist /simulation_input/ write_events_file
    namelist /simulation_input/ events_per_file
    namelist /simulation_input/ bytes_per_file
    namelist /simulation_input/ min_file_count
    namelist /simulation_input/ max_file_count
    namelist /simulation_input/ read_events_raw
    namelist /simulation_input/ read_events_raw_file
    namelist /simulation_input/ read_events
    namelist /simulation_input/ read_events_force
    namelist /simulation_input/ keep_beam_remnants
    namelist /simulation_input/ keep_initials
    namelist /simulation_input/ guess_color_flow
    namelist /simulation_input/ recalculate
    namelist /simulation_input/ shower
    namelist /simulation_input/ shower_nf
    namelist /simulation_input/ shower_running_alpha_s
    namelist /simulation_input/ shower_alpha_s
    namelist /simulation_input/ shower_lambda
    namelist /simulation_input/ shower_t_min
    namelist /simulation_input/ shower_md, shower_mu, shower_ms, &
         shower_mc, shower_mb
    namelist /simulation_input/ fragment
    namelist /simulation_input/ fragmentation_method
    namelist /simulation_input/ user_fragmentation_mode
    namelist /simulation_input/ pythia_processes
    namelist /simulation_input/ pythia_parameters
    namelist /diagnostics_input/ chattiness
    namelist /diagnostics_input/ catch_signals
    namelist /diagnostics_input/ time_limit
    namelist /diagnostics_input/ warn_empty_channel
    namelist /diagnostics_input/ screen_results
    namelist /diagnostics_input/ screen_events
    namelist /diagnostics_input/ screen_histograms
    namelist /diagnostics_input/ screen_diagnostics
    namelist /diagnostics_input/ show_pythia_banner
    namelist /diagnostics_input/ show_pythia_initialization
    namelist /diagnostics_input/ show_pythia_statistics
    namelist /diagnostics_input/ write_logfile
    namelist /diagnostics_input/ plot_history
    namelist /diagnostics_input/ write_logfile_file
    namelist /diagnostics_input/ show_input
    namelist /diagnostics_input/ show_results
    namelist /diagnostics_input/ show_phase_space
    namelist /diagnostics_input/ show_cuts
    namelist /diagnostics_input/ show_histories
    namelist /diagnostics_input/ show_history
    namelist /diagnostics_input/ show_weights
    namelist /diagnostics_input/ show_event
    namelist /diagnostics_input/ show_histograms
    namelist /diagnostics_input/ show_overflow
    namelist /diagnostics_input/ show_excess
    namelist /diagnostics_input/ read_analysis_file
    namelist /diagnostics_input/ plot_width, plot_height
    namelist /diagnostics_input/ plot_histograms, plot_curves, plot_symbols
    namelist /diagnostics_input/ plot_smoothly, plot_errors
    namelist /diagnostics_input/ plot_excess
    namelist /diagnostics_input/ plot_per_bin
    namelist /diagnostics_input/ plot_grids_channels
    namelist /diagnostics_input/ plot_grids_logscale
    namelist /diagnostics_input/ slha_rewrite_input
    namelist /diagnostics_input/ slha_ignore_errors
    read (u, "(A)") first_line
    rewind (u)
    if (mci%input_slha_format .or. &
         & first_line(1:25) == "# SUSY Les Houches Accord") then
       read_namelists = .false.
       call msg_message (" Reading SUSY Les Houches Accord (SLHA) data")
       mci%input_slha_format = .false.
       allocate (mci%slha)
       call create (mci%slha)
       call slha_set (mci%slha, mci%par)
       call read (u, mci%slha, &
            &     ignore_errors = mci%slha_ignore_errors, &
            &     rewrite_input = mci%slha_rewrite_input)
       call parameters_set (mci%par, mci%slha)
       if (contains_whizard_input (mci%slha)) then
          close (u)
          open (u, status="scratch")
          call write_whizard_input (u, mci%slha)
          rewind (u)
          read_namelists = .true.
       end if
    else
       read_namelists = .true.
    end if
    if (read_namelists) then
       process_id = mci%process_id
       luminosity = mci%luminosity
       polarized_beams = mci%polarized_beams
       structured_beams = mci%structured_beams
       beam_recoil = mci%beam_recoil
       recoil_conserve_momentum = mci%recoil_conserve_momentum
       filename = mci%filename
       directory = mci%directory
       input_file = mci%input_file
       input_slha_format = mci%input_slha_format
       cm_frame = mci%cm_frame
       sqrts = mci%sqrts
       calls = mci%calls
       seed = mci%seed
       reset_seed_each_process = mci%reset_seed_each_process
       accuracy_goal = mci%accuracy_goal
       efficiency_goal = mci%efficiency_goal
       time_limit_adaptation = mci%time_limit_adaptation
       stratified = mci%stratified
       use_efficiency = mci%use_efficiency
       weights_power = mci%weights_power
       min_bins = mci%min_bins
       max_bins = mci%max_bins
       min_calls_per_bin = mci%min_calls_per_bin
       min_calls_per_channel = mci%min_calls_per_channel
       write_grids = mci%write_grids
       write_grids_raw = mci%write_grids_raw
       write_all_grids_file = mci%write_all_grids_file
       write_all_grids = mci%write_all_grids
       write_grids_file = mci%write_grids_file
       read_grids = mci%read_grids
       read_grids_raw = mci%read_grids_raw
       read_grids_force = mci%read_grids_force
       read_grids_file = mci%read_grids_file
       generate_phase_space = mci%generate_phase_space
       read_model_file = mci%read_model_file
       write_phase_space_channels_file = mci%write_phase_space_channels_file
       write_phase_space_file = mci%write_phase_space_file
       read_phase_space = mci%read_phase_space
       read_phase_space_file = mci%read_phase_space_file
       overwrite_phase_space = mci%overwrite_phase_space
       phase_space_only = mci%phase_space_only
       matrix_element_only = mci%matrix_element_only
       use_equivalences = mci%use_equivalences
       azimuthal_dependence = mci%azimuthal_dependence
       off_shell_lines = mci%off_shell_lines
       extra_off_shell_lines = mci%extra_off_shell_lines
       splitting_depth = mci%splitting_depth
       exchange_lines = mci%exchange_lines
       show_deleted_channels = mci%show_deleted_channels
       single_off_shell_decays = mci%single_off_shell_decays
       double_off_shell_decays = mci%double_off_shell_decays
       single_off_shell_branchings = mci%single_off_shell_branchings
       double_off_shell_branchings = mci%double_off_shell_branchings
       massive_fsr = mci%massive_fsr
       threshold_mass = mci%threshold_mass
       threshold_mass_t = mci%threshold_mass_t
       default_jet_cut = mci%default_jet_cut
       default_mass_cut = mci%default_mass_cut
       default_energy_cut = mci%default_energy_cut
       default_q_cut = mci%default_q_cut
       write_default_cuts_file = mci%write_default_cuts_file
       read_cuts_file = mci%read_cuts_file
       user_cut_mode = mci%user_cut_mode
       user_weight_mode = mci%user_weight_mode
       handle_negative_weight = mci%handle_negative_weight
       n_events = mci%n_events
       n_calls = mci%n_calls
       n_events_warmup = mci%n_events_warmup
       unweighted = mci%unweighted
       normalize_weight = mci%normalize_weight
       write_weights = mci%write_weights
       write_weights_file = mci%write_weights_file
       safety_factor = mci%safety_factor
       write_events_raw = mci%write_events_raw
       write_events_raw_file = mci%write_events_raw_file
       write_events = mci%write_events
       write_events_format = mci%write_events_format
       write_events_file = mci%write_events_file
       events_per_file = mci%events_per_file
       bytes_per_file = mci%bytes_per_file
       min_file_count = mci%min_file_count
       max_file_count = mci%max_file_count
       read_events_raw = mci%read_events_raw
       read_events_raw_file = mci%read_events_raw_file
       read_events = mci%read_events
       read_events_force = mci%read_events_force
       keep_beam_remnants = mci%keep_beam_remnants
       keep_initials = mci%keep_initials
       guess_color_flow = mci%guess_color_flow
       recalculate = mci%recalculate
       shower = mci%shower
       shower_nf = mci%shower_nf
       shower_running_alpha_s = mci%shower_running_alpha_s
       shower_alpha_s = mci%shower_alpha_s
       shower_lambda = mci%shower_lambda
       shower_t_min = mci%shower_t_min
       shower_md = mci%shower_md
       shower_mu = mci%shower_mu
       shower_ms = mci%shower_ms
       shower_mc = mci%shower_mc
       shower_mb = mci%shower_mb
       fragment = mci%fragment
       fragmentation_method = mci%fragmentation_method
       user_fragmentation_mode = mci%user_fragmentation_mode
       pythia_processes = mci%pythia_processes
       pythia_parameters = mci%pythia_parameters
       chattiness = mci%chattiness
       catch_signals = mci%catch_signals
       time_limit = mci%time_limit
       warn_empty_channel = mci%warn_empty_channel
       screen_results = mci%screen_results
       screen_events = mci%screen_events
       screen_histograms = mci%screen_histograms
       screen_diagnostics = mci%screen_diagnostics
       show_pythia_banner = mci%show_pythia_banner
       show_pythia_initialization = mci%show_pythia_initialization
       show_pythia_statistics = mci%show_pythia_statistics
       write_logfile = mci%write_logfile
       plot_history = mci%plot_history
       write_logfile_file = mci%write_logfile_file
       show_input = mci%show_input
       show_results = mci%show_results
       show_phase_space = mci%show_phase_space
       show_cuts = mci%show_cuts
       show_histories = mci%show_histories
       show_history = mci%show_history
       show_weights = mci%show_weights
       show_event = mci%show_event
       show_histograms = mci%show_histograms
       show_overflow = mci%show_overflow
       show_excess = mci%show_excess
       read_analysis_file = mci%read_analysis_file
       plot_width = mci%plot_width
       plot_height = mci%plot_height
       plot_histograms = mci%plot_histograms
       plot_curves = mci%plot_curves
       plot_symbols = mci%plot_symbols
       plot_smoothly = mci%plot_smoothly
       plot_errors = mci%plot_errors
       plot_excess = mci%plot_excess
       plot_per_bin = mci%plot_per_bin
       plot_grids_channels = mci%plot_grids_channels
       plot_grids_logscale = mci%plot_grids_logscale
       slha_rewrite_input = mci%slha_rewrite_input
       slha_ignore_errors = mci%slha_ignore_errors
       read(u, nml=process_input)
       read(u, nml=integration_input)
       read(u, nml=simulation_input)
       read(u, nml=diagnostics_input)
       call read(u, mci%par)
       call read(u, mci%beam)
       mci%process_id = process_id
       mci%luminosity = luminosity
       mci%polarized_beams = polarized_beams
       mci%structured_beams = structured_beams
       mci%beam_recoil = beam_recoil
       mci%recoil_conserve_momentum = recoil_conserve_momentum
       mci%filename = filename
       mci%directory = directory
       mci%input_file = input_file
       mci%input_slha_format = input_slha_format
       mci%cm_frame = cm_frame
       mci%sqrts = sqrts
       mci%calls(:,1:3) = calls(:,1:3)
       mci%seed = seed
       mci%reset_seed_each_process = reset_seed_each_process
       mci%accuracy_goal = accuracy_goal
       mci%efficiency_goal = efficiency_goal
       mci%time_limit_adaptation = time_limit_adaptation
       mci%stratified = stratified
       mci%use_efficiency = use_efficiency
       mci%weights_power = weights_power
       mci%min_bins = min_bins
       mci%max_bins = max_bins
       mci%min_calls_per_bin = min_calls_per_bin
       mci%min_calls_per_channel = min_calls_per_channel
       mci%write_grids = write_grids
       mci%write_grids_raw = write_grids_raw
       mci%write_all_grids_file = write_all_grids_file
       mci%write_all_grids = write_all_grids
       mci%write_grids_file = write_grids_file
       mci%read_grids = read_grids
       mci%read_grids_raw = read_grids_raw
       mci%read_grids_force = read_grids_force
       mci%read_grids_file = read_grids_file
       mci%generate_phase_space = generate_phase_space
       mci%read_model_file = read_model_file
       mci%write_phase_space_channels_file = write_phase_space_channels_file
       mci%write_phase_space_file = write_phase_space_file
       mci%read_phase_space = read_phase_space
       mci%read_phase_space_file = read_phase_space_file
       mci%overwrite_phase_space = overwrite_phase_space
       mci%phase_space_only = phase_space_only
       mci%matrix_element_only = matrix_element_only
       mci%use_equivalences = use_equivalences
       mci%azimuthal_dependence = azimuthal_dependence
       mci%off_shell_lines = off_shell_lines
       mci%extra_off_shell_lines = extra_off_shell_lines
       mci%splitting_depth = splitting_depth
       mci%exchange_lines = exchange_lines
       mci%show_deleted_channels = show_deleted_channels
       mci%single_off_shell_decays = single_off_shell_decays
       mci%double_off_shell_decays = double_off_shell_decays
       mci%single_off_shell_branchings = single_off_shell_branchings
       mci%double_off_shell_branchings = double_off_shell_branchings
       mci%massive_fsr = massive_fsr
       mci%threshold_mass = threshold_mass
       mci%threshold_mass_t = threshold_mass_t
       mci%default_jet_cut = default_jet_cut
       mci%default_mass_cut = default_mass_cut
       mci%default_energy_cut = default_energy_cut
       mci%default_q_cut = default_q_cut
       mci%write_default_cuts_file = write_default_cuts_file
       mci%read_cuts_file = read_cuts_file
       mci%user_cut_mode = user_cut_mode
       mci%user_weight_mode = user_weight_mode
       mci%handle_negative_weight = handle_negative_weight
       mci%n_events = n_events
       mci%n_calls = n_calls
       mci%n_events_warmup = n_events_warmup
       mci%unweighted = unweighted
       mci%normalize_weight = normalize_weight
       mci%write_weights = write_weights
       mci%write_weights_file = write_weights_file
       mci%safety_factor = safety_factor
       mci%write_events_raw = write_events_raw
       mci%write_events_raw_file = write_events_raw_file
       mci%write_events = write_events
       mci%write_events_format = write_events_format
       mci%write_events_file = write_events_file
       mci%events_per_file = events_per_file
       mci%bytes_per_file = bytes_per_file
       mci%min_file_count = min_file_count
       mci%max_file_count = max_file_count
       mci%read_events_raw = read_events_raw
       mci%read_events_raw_file = read_events_raw_file
       mci%read_events = read_events
       mci%read_events_force = read_events_force
       mci%keep_beam_remnants = keep_beam_remnants
       mci%keep_initials = keep_initials
       mci%guess_color_flow = guess_color_flow
       mci%recalculate = recalculate
       mci%shower = shower
       mci%shower_nf = shower_nf
       mci%shower_running_alpha_s = shower_running_alpha_s
       mci%shower_alpha_s = shower_alpha_s
       mci%shower_lambda = shower_lambda
       mci%shower_t_min = shower_t_min
       mci%shower_md = shower_md
       mci%shower_mu = shower_mu
       mci%shower_ms = shower_ms
       mci%shower_mc = shower_mc
       mci%shower_mb = shower_mb
       mci%fragment = fragment
       mci%fragmentation_method = fragmentation_method
       mci%user_fragmentation_mode = user_fragmentation_mode
       mci%pythia_processes = pythia_processes
       mci%pythia_parameters = pythia_parameters
       mci%chattiness = chattiness
       mci%catch_signals = catch_signals
       mci%time_limit = time_limit
       mci%warn_empty_channel = warn_empty_channel
       mci%screen_results = screen_results
       mci%screen_events = screen_events
       mci%screen_histograms = screen_histograms
       mci%screen_diagnostics = screen_diagnostics
       mci%show_pythia_banner = show_pythia_banner
       mci%show_pythia_initialization = show_pythia_initialization
       mci%show_pythia_statistics = show_pythia_statistics
       mci%write_logfile = write_logfile
       mci%plot_history = plot_history
       mci%write_logfile_file = write_logfile_file
       mci%show_input = show_input
       mci%show_results = show_results
       mci%show_phase_space = show_phase_space
       mci%show_cuts = show_cuts
       mci%show_histories = show_histories
       mci%show_history = show_history
       mci%show_weights = show_weights
       mci%show_event = show_event
       mci%show_histograms = show_histograms
       mci%show_overflow = show_overflow
       mci%show_excess = show_excess
       mci%read_analysis_file = read_analysis_file
       mci%plot_width = plot_width
       mci%plot_height = plot_height
       mci%plot_histograms = plot_histograms
       mci%plot_curves = plot_curves
       mci%plot_symbols = plot_symbols
       mci%plot_smoothly = plot_smoothly
       mci%plot_errors = plot_errors
       mci%plot_excess = plot_excess
       mci%plot_per_bin = plot_per_bin
       mci%plot_grids_channels = plot_grids_channels
       mci%plot_grids_logscale = plot_grids_logscale
       mci%slha_rewrite_input = slha_rewrite_input
       mci%slha_ignore_errors = slha_ignore_errors
    end if
  end subroutine mc_input_read_unit

  subroutine mc_input_write_unit(u, mci)
    integer, intent(in) :: u
    type(monte_carlo_input), intent(in) :: mci
    write(u,*) '&process_input'
    write(u,*) 'process_id         = ', '"', trim(mci%process_id), '"'
    write(u,'(1x,A,1x,G10.5)') 'luminosity         =', mci%luminosity
    write(u,*) 'polarized_beams    =', mci%polarized_beams
    write(u,*) 'structured_beams   =', mci%structured_beams
    write(u,*) 'beam_recoil        =', mci%beam_recoil
    write(u,*) 'recoil_conserve_momentum =', mci%recoil_conserve_momentum
    write(u,*) 'filename           = ', '"', trim(mci%filename), '"'
    write(u,*) 'directory          = ', '"', trim(mci%directory), '"'
    write(u,*) 'input_file         = ', '"', trim(mci%input_file), '"'
    write(u,*) 'input_slha_format  = ', mci%input_slha_format
    write(u,*) 'cm_frame           =', mci%cm_frame
    write(u,'(1x,A,1x,G10.5)') 'sqrts              =', mci%sqrts
    write(u,*) '/'
    write(u,*) '&integration_input'
    write(u,*) 'calls ='
    write(u,'(3x,I3,1x,I12)') mci%calls(:,1:3)
    write(u,*) 'seed =', mci%seed
    write(u,*) 'reset_seed_each_process =', mci%reset_seed_each_process
    write(u,'(1x,A,1x,G10.5)') 'accuracy_goal         =', mci%accuracy_goal
    write(u,'(1x,A,1x,G10.5)') 'efficiency_goal       =', mci%efficiency_goal
    write(u,*) 'time_limit_adaptation =', mci%time_limit_adaptation
    write(u,*) 'stratified            =', mci%stratified
    write(u,*) 'use_efficiency        =', mci%use_efficiency
    write(u,'(1x,A,1x,G10.5)') 'weights_power         =', mci%weights_power
    write(u,*) 'min_bins              =', mci%min_bins
    write(u,*) 'max_bins              =', mci%max_bins
    write(u,*) 'min_calls_per_bin     =', mci%min_calls_per_bin
    write(u,*) 'min_calls_per_channel =', mci%min_calls_per_channel
    write(u,*) 'write_grids           =', mci%write_grids
    write(u,*) 'write_grids_raw       =', mci%write_grids_raw
    write(u,*) 'write_all_grids_file  = ', &
            & '"', trim(mci%write_all_grids_file), '"'
    write(u,*) 'write_all_grids       =', mci%write_all_grids
    write(u,*) 'write_grids_file      = ', '"', trim(mci%write_grids_file), '"'
    write(u,*) 'read_grids         =', mci%read_grids
    write(u,*) 'read_grids_raw     =', mci%read_grids_raw
    write(u,*) 'read_grids_force      =', mci%read_grids_force
    write(u,*) 'read_grids_file       = ', '"', trim(mci%read_grids_file), '"'
    write(u,*) 'generate_phase_space   =', mci%generate_phase_space
    write(u,*) 'read_model_file        = ', '"', trim(mci%read_model_file), '"'
    write(u,*) 'write_phase_space_channels_file = ', '"', trim(mci%write_phase_space_channels_file), '"'
    write(u,*) 'write_phase_space_file = ', &
         & '"', trim(mci%write_phase_space_file), '"'
    write(u,*) 'read_phase_space       =', mci%read_phase_space
    write(u,*) 'read_phase_space_file  = ', &
         & '"', trim(mci%read_phase_space_file), '"'
    !write(u,*) 'overwrite_phase_space =', mci%overwrite_phase_space
    write(u,*) 'phase_space_only       =', mci%phase_space_only
    !write(u,*) 'matrix_element_only    =', mci%matrix_element_only
    write(u,*) 'use_equivalences   =', mci%use_equivalences
    write(u,*) 'azimuthal_dependence =', mci%azimuthal_dependence
    write(u,*) 'off_shell_lines             =', mci%off_shell_lines
    write(u,*) 'extra_off_shell_lines       =', mci%extra_off_shell_lines
    write(u,*) 'splitting_depth             =', mci%splitting_depth
    write(u,*) 'exchange_lines              =', mci%exchange_lines
    write(u,*) 'show_deleted_channels       =', mci%show_deleted_channels
    write(u,*) 'single_off_shell_decays     =', mci%single_off_shell_decays
    write(u,*) 'double_off_shell_decays     =', mci%double_off_shell_decays
    write(u,*) 'single_off_shell_branchings =', mci%single_off_shell_branchings
    write(u,*) 'double_off_shell_branchings =', mci%double_off_shell_branchings
    write(u,*) 'massive_fsr                 =', mci%massive_fsr
    write(u,*) 'threshold_mass     =', mci%threshold_mass
    write(u,*) 'threshold_mass_t   =', mci%threshold_mass_t
    write(u,*) 'default_jet_cut    =', mci%default_jet_cut
    write(u,*) 'default_mass_cut   =', mci%default_mass_cut
    write(u,*) 'default_energy_cut =', mci%default_energy_cut
    write(u,*) 'default_q_cut      =', mci%default_q_cut
    write(u,*) 'write_default_cuts_file = ', &
         & '"', trim(mci%write_default_cuts_file), '"'
    write(u,*) 'read_cuts_file          = ', &
         & '"', trim(mci%read_cuts_file), '"'
    write(u,*) 'user_cut_mode      =', mci%user_cut_mode
    write(u,*) 'user_weight_mode   =', mci%user_weight_mode
    write(u,*) 'handle_negative_weight =', mci%handle_negative_weight
    write(u,*) '/'
    write(u,*) '&simulation_input'
    write(u,'(1x,A,1x,G10.5)') 'n_events              =', mci%n_events
    write(u,'(1x,A,1x,G10.5)') 'n_calls               =', mci%n_calls
    write(u,'(1x,A,1x,G10.5)') 'n_events_warmup       =', mci%n_events_warmup
    write(u,*) 'unweighted            =', mci%unweighted
    write(u,*) 'normalize_weight      =', mci%normalize_weight
    write(u,*) 'write_weights         =', mci%write_weights
    write(u,*) 'write_weights_file    = ', &
            & '"', trim (mci%write_weights_file), '"'
    write(u,'(1x,A,1x,G10.5)') 'safety_factor         =', mci%safety_factor
    write(u,*) 'write_events_raw      =', mci%write_events_raw
    write(u,*) 'write_events_raw_file = ', '"', &
         & trim(mci%write_events_raw_file), '"'
    write(u,*) 'write_events          =', mci%write_events
    write(u,*) 'write_events_format   =', mci%write_events_format
    write(u,*) 'write_events_file     = "', trim(mci%write_events_file), '"'
    write(u,*) 'events_per_file       = ', mci%events_per_file
    write(u,*) 'bytes_per_file        = ', mci%bytes_per_file
    write(u,*) 'min_file_count        = ', mci%min_file_count
    write(u,*) 'max_file_count        = ', mci%max_file_count
    write(u,*) 'read_events_raw       =', mci%read_events_raw
    write(u,*) 'read_events_raw_file  = ', '"', &
         & trim(mci%read_events_raw_file), '"'
    write(u,*) 'read_events           =', mci%read_events
    write(u,*) 'read_events_force     =', mci%read_events_force
    write(u,*) 'keep_beam_remnants    =', mci%keep_beam_remnants
    write(u,*) 'keep_initials         =', mci%keep_initials
    write(u,*) 'guess_color_flow      =', mci%guess_color_flow
    write(u,*) 'recalculate           =', mci%recalculate
    write(u,*) 'shower                 =', mci%shower
    write(u,*) 'shower_nf              =', mci%shower_nf
    write(u,*) 'shower_running_alpha_s =', mci%shower_running_alpha_s
    write(u,*) 'shower_alpha_s         =', mci%shower_alpha_s
    write(u,*) 'shower_lambda          =', mci%shower_lambda
    write(u,*) 'shower_t_min          =', mci%shower_t_min
    write(u,*) 'shower_md             =', mci%shower_md
    write(u,*) 'shower_mu             =', mci%shower_mu
    write(u,*) 'shower_ms             =', mci%shower_ms
    write(u,*) 'shower_mc             =', mci%shower_mc
    write(u,*) 'shower_mb             =', mci%shower_mb
    write(u,*) 'fragment              =', mci%fragment
    write(u,*) 'fragmentation_method  =', mci%fragmentation_method
    write(u,*) 'user_fragmentation_mode  =', mci%user_fragmentation_mode
    !if (mci%pythia_processes == ' ') then
    !   write(u,*) 'pythia_processes  = " "'
    !else
       write(u,*) 'pythia_processes  = "', trim(mci%pythia_processes), '"'
    !end if
    !if (mci%pythia_parameters == ' ') then
    !   write(u,*) 'pythia_parameters = " "'
    !else
       write(u,*) 'pythia_parameters = "', trim(mci%pythia_parameters), '"'
    !end if
    write(u,*) '/'
    write(u,*) '&diagnostics_input'
    write(u,*) 'chattiness         =', mci%chattiness
    write(u,*) 'catch_signals      =', mci%catch_signals
    write(u,*) 'time_limit         =', mci%time_limit
    write(u,*) 'warn_empty_channel =', mci%warn_empty_channel
    !write(u,*) 'screen_results     =', mci%screen_results
    write(u,*) 'screen_events      =', mci%screen_events
    write(u,*) 'screen_histograms  =', mci%screen_histograms
    write(u,*) 'screen_diagnostics =', mci%screen_diagnostics
    write(u,*) 'show_pythia_banner         =', mci%show_pythia_banner
    write(u,*) 'show_pythia_initialization =', mci%show_pythia_initialization
    write(u,*) 'show_pythia_statistics     =', mci%show_pythia_statistics
    write(u,*) 'write_logfile      =', mci%write_logfile
    write(u,*) 'plot_history       =', mci%plot_history
    write(u,*) 'write_logfile_file = ', '"', trim(mci%write_logfile_file), '"'
    write(u,*) 'show_input         =', mci%show_input
    write(u,*) 'show_results       =', mci%show_results
    write(u,*) 'show_phase_space   =', mci%show_phase_space
    write(u,*) 'show_cuts          =', mci%show_cuts
    write(u,*) 'show_histories     =', mci%show_histories
    write(u,*) 'show_history       =', mci%show_history
    write(u,*) 'show_weights       =', mci%show_weights
    write(u,*) 'show_event         =', mci%show_event
    write(u,*) 'show_histograms    =', mci%show_histograms
    write(u,*) 'show_overflow      =', mci%show_overflow
    write(u,*) 'show_excess        =', mci%show_excess
    write(u,*) 'read_analysis_file = ', &
         & '"', trim(mci%read_analysis_file), '"'
    write(u,*) 'plot_width         =', mci%plot_width
    write(u,*) 'plot_height        =', mci%plot_height
    write(u,*) 'plot_histograms    =', mci%plot_histograms
    write(u,*) 'plot_curves        =', mci%plot_curves
    write(u,*) 'plot_symbols       =', mci%plot_symbols
    write(u,*) 'plot_smoothly      =', mci%plot_smoothly
    write(u,*) 'plot_errors        =', mci%plot_errors
    write(u,*) 'plot_excess        =', mci%plot_excess
    write(u,*) 'plot_per_bin       =', mci%plot_per_bin
    write(u,*) 'plot_grids_channels = ', &
         & '"', trim(mci%plot_grids_channels), '"'
    write(u,*) 'plot_grids_logscale = ', mci%plot_grids_logscale
    write(u,*) 'slha_rewrite_input = ', mci%slha_rewrite_input
    write(u,*) 'slha_ignore_errors = ', mci%slha_ignore_errors
    write(u,*) '/'
    call write(u, mci%par)
    write(u,*) '/'
    call write(u, mci%beam)
  end subroutine mc_input_write_unit

  recursive subroutine mc_input_read_name (prefix, filename, mci)
    character(len=*), intent(in) :: prefix, filename
    type(monte_carlo_input), intent(inout) :: mci
    character(len=FILENAME_LEN) :: file, new_filename, new_prefix
    integer :: u
    file = file_exists_else_default (prefix, filename, "in")
    if (len_trim (file) /= 0) then
       call msg_message (" Reading process data from file "//trim(file))
       u = free_unit ()
       open(u, file=trim(file), action='read', status='old')
       call mc_input_read_unit (u, mci)
       close (u)
       new_prefix = mci%directory
       if (mci%input_file /= "") then
          new_filename = mci%input_file
       else
          new_filename = filename
       end if
       if (trim(new_filename) /= trim(filename) .or. &
            & trim(new_prefix) /= trim(prefix)) then
          call mc_input_read_name (new_prefix, new_filename, mci)
       end if
    else
       call msg_warning (" Input file not found")
    end if
  end subroutine mc_input_read_name

  subroutine mc_input_write_name (name, mci)
    character(len=*) :: name
    type(monte_carlo_input), intent(inout) :: mci
    integer :: u
    u = free_unit()
    open (u, file=name, action='write', status='replace')
    call mc_input_write_unit (u, mci)
    close (u)
  end subroutine mc_input_write_name

  subroutine mc_input_broadcast(mci, root, domain, error)
    integer, dimension(2,5) ::  local_calls
    type(monte_carlo_input), intent(inout) :: mci
    integer, intent(in) :: root
    integer, intent(in), optional :: domain
    integer, intent(out), optional :: error
    call mpi90_broadcast(mci%process_id, root, domain, error)
    call mpi90_broadcast(mci%luminosity, root, domain, error)
    call mpi90_broadcast(mci%polarized_beams, root, domain, error)
    call mpi90_broadcast(mci%structured_beams, root, domain, error)
    call mpi90_broadcast(mci%beam_recoil, root, domain, error)
    call mpi90_broadcast(mci%recoil_conserve_momentum, root, domain, error)
    call mpi90_broadcast(mci%filename, root, domain, error)
    call mpi90_broadcast(mci%directory, root, domain, error)
    call mpi90_broadcast(mci%input_file, root, domain, error)
    call mpi90_broadcast(mci%input_slha_format, root, domain, error)
    call mpi90_broadcast(mci%cm_frame, root, domain, error)
    call mpi90_broadcast(mci%sqrts, root, domain, error)
    local_calls=mci%calls
    call mpi90_broadcast(local_calls, root, domain, error)
    call mpi90_broadcast(mci%seed, root, domain, error)
    call mpi90_broadcast(mci%reset_seed_each_process, root, domain, error)
    call mpi90_broadcast(mci%accuracy_goal, root, domain, error)
    call mpi90_broadcast(mci%efficiency_goal, root, domain, error)
    call mpi90_broadcast(mci%time_limit_adaptation, root, domain, error)
    call mpi90_broadcast(mci%stratified, root, domain, error)
    call mpi90_broadcast(mci%use_efficiency, root, domain, error)
    call mpi90_broadcast(mci%weights_power, root, domain, error)
    call mpi90_broadcast(mci%min_bins, root, domain, error)
    call mpi90_broadcast(mci%max_bins, root, domain, error)
    call mpi90_broadcast(mci%min_calls_per_bin, root, domain, error)
    call mpi90_broadcast(mci%min_calls_per_channel, root, domain, error)
    call mpi90_broadcast(mci%write_grids, root, domain, error)
    call mpi90_broadcast(mci%write_grids_raw, root, domain, error)
    call mpi90_broadcast(mci%write_all_grids_file, root, domain, error)
    call mpi90_broadcast(mci%write_all_grids, root, domain, error)
    call mpi90_broadcast(mci%write_grids_file, root, domain, error)
    call mpi90_broadcast(mci%read_grids, root, domain, error)
    call mpi90_broadcast(mci%read_grids_raw, root, domain, error)
    call mpi90_broadcast(mci%read_grids_force, root, domain, error)
    call mpi90_broadcast(mci%read_grids_file, root, domain, error)
    call mpi90_broadcast(mci%generate_phase_space, root, domain, error)
    call mpi90_broadcast(mci%read_model_file, root, domain, error)
    call mpi90_broadcast(mci%write_phase_space_channels_file, root, domain, error)
    call mpi90_broadcast(mci%write_phase_space_file, root, domain, error)
    call mpi90_broadcast(mci%read_phase_space, root, domain, error)
    call mpi90_broadcast(mci%read_phase_space_file, root, domain, error)
    call mpi90_broadcast(mci%overwrite_phase_space, root, domain, error)
    call mpi90_broadcast(mci%phase_space_only, root, domain, error)
    call mpi90_broadcast(mci%matrix_element_only, root, domain, error)
    call mpi90_broadcast(mci%use_equivalences, root, domain, error)
    call mpi90_broadcast(mci%azimuthal_dependence, root, domain, error)
    call mpi90_broadcast(mci%off_shell_lines, root, domain, error)
    call mpi90_broadcast(mci%extra_off_shell_lines, root, domain, error)
    call mpi90_broadcast(mci%splitting_depth, root, domain, error)
    call mpi90_broadcast(mci%exchange_lines, root, domain, error)
    call mpi90_broadcast(mci%show_deleted_channels, root, domain, error)
    call mpi90_broadcast(mci%single_off_shell_decays, root, domain, error)
    call mpi90_broadcast(mci%double_off_shell_decays, root, domain, error)
    call mpi90_broadcast(mci%single_off_shell_branchings, root, domain, error)
    call mpi90_broadcast(mci%double_off_shell_branchings, root, domain, error)
    call mpi90_broadcast(mci%massive_fsr, root, domain, error)
    call mpi90_broadcast(mci%threshold_mass, root, domain, error)
    call mpi90_broadcast(mci%threshold_mass, root, domain, error)
    call mpi90_broadcast(mci%default_jet_cut, root, domain, error)
    call mpi90_broadcast(mci%default_mass_cut, root, domain, error)
    call mpi90_broadcast(mci%default_energy_cut, root, domain, error)
    call mpi90_broadcast(mci%default_q_cut, root, domain, error)
    call mpi90_broadcast(mci%write_default_cuts_file, root, domain, error)
    call mpi90_broadcast(mci%read_cuts_file, root, domain, error)
    call mpi90_broadcast(mci%user_cut_mode, root, domain, error)
    call mpi90_broadcast(mci%user_weight_mode, root, domain, error)
    call mpi90_broadcast(mci%handle_negative_weight, root, domain, error)
    call mpi90_broadcast(mci%n_events, root, domain, error)
    !call mpi90_broadcast(mci%n_calls, root, domain, error)
    call mpi90_broadcast(mci%n_events_warmup, root, domain, error)
    call mpi90_broadcast(mci%unweighted, root, domain, error)
    call mpi90_broadcast(mci%normalize_weight, root, domain, error)
    call mpi90_broadcast(mci%write_weights, root, domain, error)
    call mpi90_broadcast(mci%write_weights_file, root, domain, error)
    call mpi90_broadcast(mci%safety_factor, root, domain, error)
    call mpi90_broadcast(mci%write_events_raw, root, domain, error)
    call mpi90_broadcast(mci%write_events_raw_file, root, domain, error)
    call mpi90_broadcast(mci%write_events, root, domain, error)
    call mpi90_broadcast(mci%write_events_format, root, domain, error)
    call mpi90_broadcast(mci%write_events_file, root, domain, error)
    call mpi90_broadcast(mci%events_per_file, root, domain, error)
    call mpi90_broadcast(mci%bytes_per_file, root, domain, error)
    call mpi90_broadcast(mci%min_file_count, root, domain, error)
    call mpi90_broadcast(mci%max_file_count, root, domain, error)
    call mpi90_broadcast(mci%read_events_raw, root, domain, error)
    call mpi90_broadcast(mci%read_events_raw_file, root, domain, error)
    call mpi90_broadcast(mci%read_events, root, domain, error)
    call mpi90_broadcast(mci%read_events_force, root, domain, error)
    call mpi90_broadcast(mci%keep_beam_remnants, root, domain, error)
    call mpi90_broadcast(mci%keep_initials, root, domain, error)
    call mpi90_broadcast(mci%guess_color_flow, root, domain, error)
    call mpi90_broadcast(mci%recalculate, root, domain, error)
    call mpi90_broadcast(mci%shower, root, domain, error)
    call mpi90_broadcast(mci%shower_nf, root, domain, error)
    call mpi90_broadcast(mci%shower_running_alpha_s, root, domain, error)
    call mpi90_broadcast(mci%shower_alpha_s, root, domain, error)
    call mpi90_broadcast(mci%shower_lambda, root, domain, error)
    call mpi90_broadcast(mci%shower_t_min, root, domain, error)
    call mpi90_broadcast(mci%shower_md, root, domain, error)
    call mpi90_broadcast(mci%shower_mu, root, domain, error)
    call mpi90_broadcast(mci%shower_ms, root, domain, error)
    call mpi90_broadcast(mci%shower_mc, root, domain, error)
    call mpi90_broadcast(mci%shower_mb, root, domain, error)
    call mpi90_broadcast(mci%fragment, root, domain, error)
    call mpi90_broadcast(mci%fragmentation_method, root, domain, error)
    call mpi90_broadcast(mci%user_fragmentation_mode, root, domain, error)
    !call mpi90_broadcast(mci%pythia_processes, root, domain, error)
    !call mpi90_broadcast(mci%pythia_parameters, root, domain, error)
    call mpi90_broadcast(mci%chattiness, root, domain, error)
    call mpi90_broadcast(mci%catch_signals, root, domain, error)
    call mpi90_broadcast(mci%time_limit, root, domain, error)
    call mpi90_broadcast(mci%warn_empty_channel, root, domain, error)
    call mpi90_broadcast(mci%screen_results, root, domain, error)
    call mpi90_broadcast(mci%screen_events, root, domain, error)
    call mpi90_broadcast(mci%screen_histograms, root, domain, error)
    call mpi90_broadcast(mci%screen_diagnostics, root, domain, error)
    call mpi90_broadcast(mci%show_pythia_banner, root, domain, error)
    call mpi90_broadcast(mci%show_pythia_initialization, root, domain, error)
    call mpi90_broadcast(mci%show_pythia_statistics, root, domain, error)
    call mpi90_broadcast(mci%write_logfile, root, domain, error)
    call mpi90_broadcast(mci%plot_history, root, domain, error)
    call mpi90_broadcast(mci%write_logfile_file, root, domain, error)
    call mpi90_broadcast(mci%show_input, root, domain, error)
    call mpi90_broadcast(mci%show_results, root, domain, error)
    call mpi90_broadcast(mci%show_phase_space, root, domain, error)
    call mpi90_broadcast(mci%show_cuts, root, domain, error)
    call mpi90_broadcast(mci%show_histories, root, domain, error)
    call mpi90_broadcast(mci%show_history, root, domain, error)
    call mpi90_broadcast(mci%show_weights, root, domain, error)
    call mpi90_broadcast(mci%show_event, root, domain, error)
    call mpi90_broadcast(mci%show_histograms, root, domain, error)
    call mpi90_broadcast(mci%show_overflow, root, domain, error)
    call mpi90_broadcast(mci%show_excess, root, domain, error)
    call mpi90_broadcast(mci%read_analysis_file, root, domain, error)
    call mpi90_broadcast(mci%plot_width, root, domain, error)
    call mpi90_broadcast(mci%plot_height, root, domain, error)
    call mpi90_broadcast(mci%plot_histograms, root, domain, error)
    call mpi90_broadcast(mci%plot_curves, root, domain, error)
    call mpi90_broadcast(mci%plot_symbols, root, domain, error)
    call mpi90_broadcast(mci%plot_smoothly, root, domain, error)
    call mpi90_broadcast(mci%plot_errors, root, domain, error)
    call mpi90_broadcast(mci%plot_excess, root, domain, error)
    call mpi90_broadcast(mci%plot_per_bin, root, domain, error)
    call mpi90_broadcast(mci%plot_grids_channels, root, domain, error)
    call mpi90_broadcast(mci%plot_grids_logscale, root, domain, error)
    call mpi90_broadcast(mci%slha_rewrite_input, root, domain, error)
    call mpi90_broadcast(mci%slha_ignore_errors, root, domain, error)
    call mpi90_broadcast(mci%par, root, domain, error)
    call mpi90_broadcast(mci%beam, root, domain, error)
  end subroutine mc_input_broadcast
  subroutine character_broadcast(string, root, domain, error)
    character(len=*), intent(inout) :: string
    integer, intent(in) :: root
    integer, intent(in), optional :: domain
    integer, intent(out), optional :: error
!     integer, dimension(:), allocatable :: buffer
!     allocate(buffer(size(transfer(string, buffer))))
!     buffer = transfer(string, buffer)
!     call mpi90_broadcast(buffer, root, domain, error)
!     string = transfer(buffer, string)
!     deallocate(buffer)
    if (present(error))  error = 0
  end subroutine character_broadcast

  subroutine parameter_set_broadcast(par, root, domain, error)
    type(parameter_set), intent(inout) :: par
    integer, intent(in) :: root
    integer, intent(in), optional :: domain
    integer, intent(out), optional :: error
!     integer, dimension(:), allocatable :: buffer
!     allocate(buffer(size(transfer(par, buffer))))
!     buffer = transfer(par, buffer)
!     call mpi90_broadcast(buffer, root, domain, error)
!     par = transfer(buffer, par)
!     deallocate(buffer)
    if (present(error))  error = 0
  end subroutine parameter_set_broadcast

  function get_number_processes (process_id) result (n)
    character(len=*), intent(in) :: process_id
    integer :: n
    integer :: unit, iostat
    character(len=len(process_id)) :: dummy
    unit = free_unit ()
    open (unit, status="scratch")
    write (unit, "(A)")  process_id
    rewind (unit)
    n = 0
    SCAN_TOKENS: do
       dummy = get_string (unit, iostat)
       if (iostat /= 0)  exit SCAN_TOKENS
       n = n + 1
    end do SCAN_TOKENS
    close (unit, status="delete")
    call lex_clear
  end function get_number_processes

  subroutine get_process_ids &
       & (process_id, process_id_array, process_factor_array)
    character(len=*), intent(in) :: process_id
    character(len=*), dimension(:), intent(inout) :: process_id_array
    real(kind=default), dimension(:), intent(inout) :: process_factor_array
    character(len=len(process_id)) :: str
    integer :: n, i, pos
    integer :: unit, iostat
    unit = free_unit ()
    open (unit, status="scratch")
    write (unit, "(A)")  process_id
    rewind (unit)
    n = size (process_id_array)
    SCAN_TOKENS: do i=1, n
       str = get_string (unit, iostat)
       if (iostat /= 0)  call msg_bug &
            & ("Process_id string read error")
       pos = scan (str, "*")
       if (pos==0) then
          process_id_array(i) = str
          process_factor_array(i) = 1
       else
          process_id_array(i) = str(1:pos-1)
          str(1:pos) = " "
          str = adjustl (str)
          read (str,*) process_factor_array(i)
       end if
    end do SCAN_TOKENS
    close (unit, status="delete")
    call lex_clear
  end subroutine get_process_ids

  subroutine guess_color_flow &
       & (guess, n_in, code, col_flow, acl_flow, warn, fail)
    logical, intent(in) :: guess
    integer, intent(in) :: n_in
    integer, dimension(:,:), intent(in) :: code
    integer, dimension(:,:), intent(inout) :: col_flow, acl_flow
    logical, intent(out) :: warn, fail
    integer, dimension(ubound(code,dim=1)) :: code_out, color
    integer :: i, j, col
    integer :: n_tot, n_flv, n_colored, n_quark, n_antiquark, n_gluon
    integer :: i_quark, i_antiquark, i_gluon, j_gluon
    n_tot = ubound (code, dim=1)
    n_flv = ubound (code, dim=2)
    warn = .false.
    fail = .false.
    col_flow = 0
    acl_flow = 0
    if (guess) then
       if (n_flv > 1) then
          do i=1, n_tot
             col = particle_color (code(i,1))
             do j=2, n_flv
                if (particle_color (code(i,j)) /= col) then
                   fail = .true.;  return
                end if
             end do
          end do
       end if
    end if
    LOOP_FLV: do j=1, n_flv
       code_out = code (:,j)
       do i=1, n_in
          code_out(i) = antiparticle_code (code_out(i))
       end do
       i_quark = 0;  i_antiquark = 0;  i_gluon = 0;  j_gluon = 0
       do i=1, n_tot
          color(i) = particle_color (code_out(i))
          select case (color(i))
          case ( 3);  i_quark     = i
          case (-3);  i_antiquark = i
          case (8)   
             if (i_gluon == 0) then
                i_gluon = i
             else
                j_gluon = i
             end if
          end select
       end do
       n_colored   = count (color /= 0)
       n_quark     = count (color == 3)
       n_antiquark = count (color == -3)
       n_gluon     = count (color == 8)
       if (n_colored == 0) then
          col_flow(:,j) = 0
          acl_flow(:,j) = 0
       else if ((n_colored == n_quark + n_antiquark + n_gluon) &
            & .and. ((n_quark < 2 .and. n_antiquark < 2 .and. n_gluon < 2) &
            &  .or. (n_quark == 0 .and. n_antiquark == 0 .and. n_gluon == 2)))&
            & then
          select case (n_gluon)
          case (0)
             col_flow (i_quark,    j) = i_antiquark
             acl_flow (i_antiquark,j) = i_quark
          case (1)
             col_flow (i_quark,    j) = i_gluon
             col_flow (i_gluon,    j) = i_antiquark
             acl_flow (i_antiquark,j) = i_gluon
             acl_flow (i_gluon,    j) = i_quark
          case (2)
             col_flow (i_gluon,j) = j_gluon
             col_flow (j_gluon,j) = i_gluon
             acl_flow (j_gluon,j) = i_gluon
             acl_flow (i_gluon,j) = j_gluon
          end select
       else if (.not.guess) then
          fail = .true.;  exit LOOP_FLV
       else if (any (color(:n_in) /= 0)) then
          fail = .true.;  exit LOOP_FLV
       else
          warn = .true.
          n_quark = 0;  n_antiquark = 0;  n_gluon = 0
          SCAN_COLORS: do i=n_in+1, n_tot
             select case (color(i))
             case (0)
             case (3)
                if (n_gluon > 0) then
                   col_flow (i,j) = i_gluon;  acl_flow (i_gluon,j) = i
                   n_gluon = 0
                else if (n_antiquark > 0) then
                   col_flow (i,j) = i_antiquark;  acl_flow (i_antiquark,j) = i
                   n_antiquark = 0
                else if (n_quark > 0) then
                   fail = .true.;  exit LOOP_FLV
                else
                   n_quark = 1;  i_quark = i
                end if
             case (-3)
                if (n_gluon > 0) then
                   acl_flow (i,j) = i_gluon;  col_flow (i_gluon,j) = i
                   n_gluon = 0
                else if (n_quark > 0) then
                   acl_flow (i,j) = i_quark;  col_flow (i_quark,j) = i
                   n_quark = 0
                else if (n_antiquark > 0) then
                   fail = .true.;  exit LOOP_FLV
                else
                   n_antiquark = 1;  i_antiquark = i
                end if
             case (8)
                if (n_gluon > 0) then
                   if (col_flow (i_gluon,j)==0) then
                      col_flow (i_gluon,j) = i;  acl_flow (i,j) = i_gluon
                   else
                      acl_flow (i_gluon,j) = i;  col_flow (i,j) = i_gluon
                   end if
                else if (n_quark > 0) then
                   acl_flow (i,j) = i_quark;  col_flow (i_quark,j) = i
                   n_quark = 0
                else if (n_antiquark > 0) then
                   col_flow (i,j) = i_antiquark;  acl_flow (i_antiquark,j) = i
                   n_antiquark = 0
                end if
                n_gluon = 1;  i_gluon = i
             case default
                fail = .true.;  exit LOOP_FLV
             end select
          end do SCAN_COLORS
          if (n_quark + n_antiquark + n_gluon > 0) then
             fail = .true.;  exit LOOP_FLV
          end if
       end if
    end do LOOP_FLV
    if (fail) then
       col_flow = 0;  acl_flow = 0
    end if
  end subroutine guess_color_flow

  subroutine check_sqrts (sqrts, n_flv, code, par)
    real(kind=default), intent(in) :: sqrts
    integer, intent(in) :: n_flv
    integer, dimension(:,:), intent(in) :: code
    type(parameter_set), intent(in) :: par
    real(kind=default), dimension(n_flv) :: mass_sum
    integer :: i
    forall (i = 1:n_flv)
       mass_sum (i) = sum (particle_mass (code(:,i), par))
    end forall
    if (all (mass_sum > sqrts))  call msg_warning &
         & (" Initial state energy too low -- result will be zero.")
  end subroutine check_sqrts

  subroutine check_calls_per_channel (calls, calls_min)
    integer, dimension(:), intent(inout) :: calls
    integer, intent(in) :: calls_min
    if (any (calls < calls_min)) then
       where (calls < calls_min)
          calls = calls_min
       end where
       write (msg_buffer, "(1x,A,I9)") &
            & " Increased number of calls to minimum value: ", calls_min
       call msg_warning
    end if
  end subroutine check_calls_per_channel

  subroutine mc_state_create_multi &
       & (mcs, rng, current_event, mci, default_filename, program_start_time)
    character(len=*), intent(in) :: default_filename
    type(monte_carlo_state), dimension(:), intent(inout) :: mcs
    type(tao_random_state), intent(inout) :: rng
    type(event), intent(inout) :: current_event
    type(monte_carlo_input), intent(in) :: mci
    type(time), intent(in) :: program_start_time
    integer :: unit_phs_old, unit_phs_new, proc_id
    character(len=PROCESS_ID_LEN), dimension(size(mcs)) :: process_id_tmp
    real(kind=default), dimension(size(mcs)) :: process_factor_tmp
    character(len=FILENAME_LEN) :: read_phase_space_file
    character(len=FILENAME_LEN) :: write_phase_space_file
    character(len=FILENAME_LEN) :: write_logfile_file
    call mpi90_rank (proc_id)
    call get_process_ids &
         & (mci%process_id, process_id_tmp, process_factor_tmp)
    mcs%process_id_current = process_id_tmp
    mcs%process_factor = process_factor_tmp
    read_phase_space_file = &
         & choose_filename (mci%read_phase_space_file, default_filename)
    if (proc_id == WHIZARD_ROOT) then
       unit_phs_old = free_unit ()
       open (unit_phs_old, status="scratch")
       unit_phs_new = free_unit ()
       open (unit_phs_new, status="scratch")
       if (mci%read_phase_space) &
            & call forest_fill_tmp_file &
            &        (mci%directory, read_phase_space_file, unit_phs_old)
    end if
    do mc_process_index=1, mc_number_processes
       call mc_state_create_single &
            & (mcs(mc_process_index), mci, default_filename, &
            &  unit_phs_old, unit_phs_new, program_start_time)
    end do
    mc_process_index = mc_number_processes
!    call check_initial_states
    call create (current_event, maxval(mcs%n_in), maxval(mcs%n_out), &
         &       maxval(mcs%n_col_out), maxval(mcs%n_hel_out), &
         &       maxval(mcs%n_flv_out))
    call tao_random_create (rng, mci%seed+proc_id)
    do mc_process_index=1, mc_number_processes
       write_logfile_file = &
            & choose_filename (mcs(1)%write_logfile_file, default_filename)
       if (mcs(mc_process_index)%write_logfile) then
          call write (trim(write_logfile_file), mcs(mc_process_index), mci)
       end if
    end do
    mc_process_index = mc_number_processes
    write_phase_space_file = &
         & choose_filename (mcs(1)%write_phase_space_file, default_filename)
    if (proc_id == WHIZARD_ROOT) then
       rewind (unit_phs_new)
       call forest_append_tmp_file &
            & (unit_phs_new, mcs(1)%directory, write_phase_space_file)
       close (unit_phs_old, status="delete")
       close (unit_phs_new, status="delete")
    end if
    if (mcs(1)%phase_space_only) then
       call msg_terminate &
            & (" WHIZARD called for phase space generation only: exiting.")
    end if
  contains
    subroutine check_initial_states
      integer, dimension(size(mcs)) :: code
      integer :: code_cmp
      logical :: ok
      integer :: i, j
      if (any (mcs%n_in /= mcs(1)%n_in))  call msg_fatal &
           & (" Process selection: Mixed decay and scattering processes.")
      do i=1, mcs(1)%n_in
         do j=1, size (mcs)
            if (mcs(j)%n_flv > 0) then
               code(j) = mcs(j)%code_in(i,1)
            else
               code(j) = mcs(j)%code_zero_in(i,1)
            end if
         end do
         if (mcs(1)%n_flv > 0) then
            code_cmp = mcs(1)%code_in(i,1)
         else
            code_cmp = mcs(1)%code_zero_in(i,1)
         end if
         if (any (code/=code_cmp)) then
            if (mcs(1)%n_in == 1) then
               ok = .false.      ! Particle decay: must be unique
            else if (all (is_parton (code))) then
               if (mcs(1)%beam(i)%fixed_energy &
                    & .or. .not.mcs(1)%beam(i)%PDF_on) then
                  ok = .false.   ! Partonic initial state
               else
                  ok = .true.    ! Hadronic initial state
               end if
            else
               ok = .false.      ! Non-hadronic initial state
            end if
         else
            ok = .true.          ! Consistent initial state
         end if
         if (.not.ok) call msg_fatal &
              & (" Process selection: Ambiguous initial state.")
      end do
    end subroutine check_initial_states
  end subroutine mc_state_create_multi

  subroutine mc_state_create_single &
       & (mcs, mci, default_filename, &
       &  unit_phs_old, unit_phs_new, program_start_time)
    character(len=*), intent(in) :: default_filename
    type(monte_carlo_state), intent(inout) :: mcs
    type(monte_carlo_input), intent(in) :: mci
    type(time), intent(in) :: program_start_time
    integer, intent(in) :: unit_phs_old, unit_phs_new
    integer :: f, i, proc_id
    logical :: new_phase_space, warn_color, fail_color
    character(len=FILENAME_LEN) :: read_model_file
    character(len=FILENAME_LEN) :: write_phase_space_channels_file
    character(len=FILENAME_LEN) :: write_default_cuts_file
    call mpi90_rank (proc_id)
    mcs%n_tot = n_tot (mcs%process_id_current)
    mcs%n_in  = n_in (mcs%process_id_current)
    mcs%n_out = n_out (mcs%process_id_current)
    mcs%n_col = n_col (mcs%process_id_current)
    mcs%n_hel = n_hel (mcs%process_id_current)
    mcs%n_flv = n_flv (mcs%process_id_current)
    mcs%n_flvz = n_flvz (mcs%process_id_current)
    mcs%n_col_in = 1
    if (mcs%n_col > 0) then
       mcs%n_col_out = mcs%n_col
    else
       mcs%n_col_out = 1
    end if
    mcs%n_hel_in = n_hel_in(mcs%process_id_current)
    mcs%n_hel_out = n_hel_out(mcs%process_id_current)
    mcs%n_flv_in = n_flv_in(mcs%process_id_current)
    mcs%n_flv_out = n_flv_out(mcs%process_id_current)
    mcs%n_flvz_in = n_flvz_in(mcs%process_id_current)
    mcs%n_flvz_out = n_flvz_out(mcs%process_id_current)
    if (mcs%n_in == 1) then
       if (mcs%n_flv_in > 1)  call msg_fatal &
            & (" Particle decay: ambiguous in-state not allowed")
    end if
    allocate(mcs%code(mcs%n_tot, mcs%n_flv))
    allocate(mcs%code_in(mcs%n_in, mcs%n_flv_in))
    allocate(mcs%code_out(mcs%n_out, mcs%n_flv_out))
    allocate(mcs%code_zero(mcs%n_tot, mcs%n_flvz))
    allocate(mcs%code_zero_in(mcs%n_in, mcs%n_flvz_in))
    allocate(mcs%code_zero_out(mcs%n_out, mcs%n_flvz_out))
    call flv_states(mcs%code, mcs%process_id_current)
    call flv_states_in(mcs%code_in, mcs%process_id_current)
    call flv_states_out(mcs%code_out, mcs%process_id_current)
    call flv_zeros(mcs%code_zero, mcs%process_id_current)
    call flv_zeros_in(mcs%code_zero_in, mcs%process_id_current)
    call flv_zeros_out(mcs%code_zero_out, mcs%process_id_current)
    call msg_message
    if (mcs%n_flv > 0) then
       call write_process &
            & (6, mcs%process_id_current, mcs%code, mcs%n_in, mcs%n_out)
    else if (mcs%n_flvz > 0) then
       call write_process &
            & (6, mcs%process_id_current, mcs%code_zero, mcs%n_in, mcs%n_out)
       call msg_message (" Note: Matrix element vanishes.")
    else
       call msg_bug (" No particle information found in matrix element code")
    end if
    do f=2, mcs%n_flv
       do i=1, mcs%n_tot
    !       if (particle_spin_type(mcs%code(i,f)) &
    !            & /= particle_spin_type(mcs%code(i,1))) call msg_fatal &
    !            & (" Subprocesses with different spin assignment cannot be combined.")
    !      if (particle_mass(mcs%code(i,f), mcs%par) &
    !           & /= particle_mass(mcs%code(i,1), mcs%par)) call msg_fatal &
    !           & (" Subprocesses with different mass assignment cannot be combined.")
       end do
    end do
    allocate(mcs%hel_state(mcs%n_tot, mcs%n_hel))
    allocate(mcs%hel_state_in(mcs%n_in, mcs%n_hel_in))
    allocate(mcs%hel_state_out(mcs%n_out, mcs%n_hel_out))
    call hel_states(mcs%hel_state, mcs%process_id_current)
    call hel_states_in(mcs%hel_state_in, mcs%process_id_current)
    call hel_states_out(mcs%hel_state_out, mcs%process_id_current)
    if (mcs%n_col > 0) then
       allocate(mcs%col_flow(mcs%n_tot, mcs%n_col_out))
       allocate(mcs%acl_flow(mcs%n_tot, mcs%n_col_out))
       call col_flows(mcs%col_flow, mcs%process_id_current)
       call acl_flows(mcs%acl_flow, mcs%process_id_current)
    else
       allocate(mcs%col_flow(mcs%n_tot, mcs%n_flv))
       allocate(mcs%acl_flow(mcs%n_tot, mcs%n_flv))
       call guess_color_flow &
            & (mci%guess_color_flow, mcs%n_in, mcs%code, &
            &  mcs%col_flow, mcs%acl_flow, warn_color, fail_color)
       if (mci%guess_color_flow) then
          if (fail_color) then
             call msg_error &
                  & (" Color flow could not be guessed for process " &
                  &  // trim(mcs%process_id_current))
          else if (warn_color) then
             call msg_warning &
                  & (" Color flow has been guessed " &
                  &  // "for process " // trim(mcs%process_id_current))
          end if
       else if (fail_color) then
          call msg_warning &
               & (" No color flow information available for process " &
               &  // trim(mcs%process_id_current))
       end if
    end if
    mcs%process_id = mci%process_id
    mcs%luminosity = mci%luminosity
    mcs%polarized_beams = mci%polarized_beams
    mcs%structured_beams = mci%structured_beams
    mcs%beam_recoil = mci%beam_recoil
    mcs%recoil_conserve_momentum = mci%recoil_conserve_momentum
    mcs%filename = mci%filename
    mcs%directory = mci%directory
    mcs%input_file = mci%input_file
    mcs%input_slha_format = mci%input_slha_format
    mcs%par = mci%par
    mcs%cm_frame = mci%cm_frame
    allocate (mcs%beam(mcs%n_in))
    call create (mcs%beam)
    if (mcs%polarized_beams) then
       if (mcs%n_hel == 0) then
          call msg_error (" Unpolarized matrix element:  Beam polarization will be ignored.")
          mcs%polarized_beams = .false.
       end if
    end if
    if (mcs%n_flv > 0) then
       call beam_setup &
            & (mcs%beam, mcs%sqrts, &
            &  mci%sqrts, mcs%par, mcs%code_in(:,1), &
            &  mcs%cm_frame, mcs%polarized_beams, &
            &  mcs%structured_beams, &
            &  mcs%beam_recoil, mcs%recoil_conserve_momentum, &
            &  mci%beam, mcs%directory)
    else
       call beam_setup &
            & (mcs%beam, mcs%sqrts, &
            &  mci%sqrts, mcs%par, mcs%code_zero_in(:,1), &
            &  mcs%cm_frame, mcs%polarized_beams, &
            &  mcs%structured_beams, &
            &  mcs%beam_recoil, mcs%recoil_conserve_momentum, &
            &  mci%beam, mcs%directory)
    end if
    if (any (mcs%beam%vector_polarization))  mcs%azimuthal_dependence = .true.
    call create (mcs%rho_out, mcs%n_col_out, mcs%n_hel_out, mcs%n_flv_out, &
         &       diagonal=all(.not.mcs%beam%vector_polarization))
    mcs%calls = 0
    mcs%calls(:,1:3) = mci%calls(:,1:3)
    mcs%seed = mci%seed
    mcs%reset_seed_each_process = mci%reset_seed_each_process
    mcs%accuracy_goal = mci%accuracy_goal
    mcs%efficiency_goal = mci%efficiency_goal
    mcs%time_limit_adaptation = mci%time_limit_adaptation
    mcs%stratified = mci%stratified
    mcs%use_efficiency = mci%use_efficiency
    mcs%weights_power = mci%weights_power
    mcs%min_bins = mci%min_bins
    mcs%max_bins = mci%max_bins
    mcs%min_calls_per_bin = mci%min_calls_per_bin
    mcs%min_calls_per_channel = mci%min_calls_per_channel
    mcs%write_grids = mci%write_grids
    mcs%write_grids_raw = mci%write_grids_raw
    mcs%write_all_grids_file = mci%write_all_grids_file
    mcs%write_all_grids = mci%write_all_grids
    mcs%write_grids_file = mci%write_grids_file
    mcs%read_grids = mci%read_grids
    mcs%read_grids_raw = mci%read_grids_raw
    mcs%read_grids_force = mci%read_grids_force
    mcs%read_grids_file = &
         & choose_filename (mci%read_grids_file, mcs%write_grids_file)
    mcs%generate_phase_space = mci%generate_phase_space
    mcs%read_model_file = mci%read_model_file
    mcs%write_phase_space_channels_file = mci%write_phase_space_channels_file
    mcs%write_phase_space_file = mci%write_phase_space_file
    mcs%read_phase_space = mci%read_phase_space
    mcs%read_phase_space_file = &
         & choose_filename (mci%read_phase_space_file, mcs%write_phase_space_file)
    mcs%overwrite_phase_space = mci%overwrite_phase_space
    mcs%phase_space_only = mci%phase_space_only
    mcs%matrix_element_only = mci%matrix_element_only
    mcs%use_equivalences = mci%use_equivalences
    mcs%azimuthal_dependence = mci%azimuthal_dependence
    mcs%phs%off_shell_lines = mci%off_shell_lines
    mcs%phs%extra_off_shell_lines = mci%extra_off_shell_lines
    mcs%phs%splitting_depth = mci%splitting_depth
    mcs%phs%exchange_lines = mci%exchange_lines
    mcs%phs%show_deleted_channels = mci%show_deleted_channels
    mcs%phs%single_off_shell_decays = mci%single_off_shell_decays
    mcs%phs%double_off_shell_decays = mci%double_off_shell_decays
    mcs%phs%single_off_shell_branchings = mci%single_off_shell_branchings
    mcs%phs%double_off_shell_branchings = mci%double_off_shell_branchings
    mcs%phs%massive_fsr = mci%massive_fsr
    if (mci%threshold_mass < 0) then
       mcs%phs%threshold_mass = mcs%sqrts / 20
    else
       mcs%phs%threshold_mass = mci%threshold_mass
    end if
    if (mci%threshold_mass_t < 0) then
       mcs%phs%threshold_mass_t = mcs%sqrts / 2
    else
       mcs%phs%threshold_mass_t = mci%threshold_mass_t
    end if
    mcs%cuts%default_jet_cut = mci%default_jet_cut
    mcs%cuts%default_mass_cut = mci%default_mass_cut
    mcs%cuts%default_energy_cut = mci%default_energy_cut
    mcs%cuts%default_q_cut = mci%default_q_cut
    mcs%write_default_cuts_file = mci%write_default_cuts_file
    mcs%read_cuts_file = mci%read_cuts_file
    mcs%user_cut_mode = mci%user_cut_mode
    mcs%user_weight_mode = mci%user_weight_mode
    mcs%handle_negative_weight = mci%handle_negative_weight
    mcs%n_events = mci%n_events
    mcs%n_calls = mci%n_calls
    mcs%n_events_warmup = mci%n_events_warmup
    mcs%unweighted = mci%unweighted
    mcs%normalize_weight = mci%normalize_weight
    mcs%write_weights = mci%write_weights
    mcs%write_weights_file = mci%write_weights_file
    mcs%safety_factor = mci%safety_factor
    mcs%write_events_raw = mci%write_events_raw
    mcs%write_events_raw_file = mci%write_events_raw_file
    mcs%write_events = mci%write_events
    mcs%write_events_format = mci%write_events_format
    mcs%write_events_file = mci%write_events_file
    mcs%events_per_file = mci%events_per_file
    mcs%bytes_per_file = mci%bytes_per_file
    mcs%min_file_count = mci%min_file_count
    mcs%max_file_count = mci%max_file_count
    mcs%read_events_raw = mci%read_events_raw
    if (mcs%read_events_raw) then
       mcs%read_grids = .true.
    end if
    mcs%read_events_raw_file = &
         & choose_filename (mci%read_events_raw_file, mcs%write_events_raw_file)
    mcs%read_events = mci%read_events
    if (mcs%read_events) then
       mcs%read_events_raw = .true.
       mcs%read_grids = .true.
    end if
    mcs%read_events_force = mci%read_events_force
    mcs%keep_beam_remnants = mci%keep_beam_remnants
    mcs%keep_initials = mci%keep_initials
    mcs%guess_color_flow = mci%guess_color_flow
    mcs%recalculate = mci%recalculate
    if (mcs%recalculate) then
       mcs%read_grids = .true.
       mcs%read_grids_force = .true.
       mcs%read_events = .true.
       mcs%read_events_raw = .true.
       mcs%read_events_force = .true.
    end if
    mcs%spar%shower_on = mci%shower
    mcs%spar%nf = mci%shower_nf
    mcs%spar%running_alpha_s = mci%shower_running_alpha_s
    mcs%spar%alpha_s = mci%shower_alpha_s
    mcs%spar%lambda = mci%shower_lambda
    mcs%spar%t_min = mci%shower_t_min
    mcs%spar%mass(1) = mci%shower_md
    mcs%spar%mass(2) = mci%shower_mu
    mcs%spar%mass(3) = mci%shower_ms
    mcs%spar%mass(4) = mci%shower_mc
    mcs%spar%mass(5) = mci%shower_mb
    mcs%fragment = mci%fragment
    mcs%fragmentation_method = mci%fragmentation_method
    mcs%user_fragmentation_mode = mci%user_fragmentation_mode
    mcs%pythia_processes = mci%pythia_processes
    mcs%pythia_parameters = mci%pythia_parameters
    mcs%chattiness = mci%chattiness
    mcs%catch_signals = mci%catch_signals
    mcs%time_limit = mci%time_limit
    mcs%warn_empty_channel = mci%warn_empty_channel
    mcs%screen_results = mci%screen_results
    mcs%screen_events = mci%screen_events
    mcs%screen_histograms = mci%screen_histograms
    mcs%screen_diagnostics = mci%screen_diagnostics
    mcs%show_pythia_banner = mci%show_pythia_banner
    mcs%show_pythia_initialization = mci%show_pythia_initialization
    mcs%show_pythia_statistics = mci%show_pythia_statistics
    mcs%write_logfile = mci%write_logfile
    mcs%plot_history = mci%plot_history
    mcs%write_logfile_file = mci%write_logfile_file
    mcs%show_input = mci%show_input
    mcs%show_results = mci%show_results
    mcs%show_phase_space = mci%show_phase_space
    mcs%show_cuts = mci%show_cuts
    mcs%show_histories = mci%show_histories
    mcs%show_history = mci%show_history
    mcs%show_weights = mci%show_weights
    mcs%show_event = mci%show_event
    mcs%show_histograms = mci%show_histograms
    mcs%show_overflow = mci%show_overflow
    mcs%show_excess = mci%show_excess
    mcs%read_analysis_file = mci%read_analysis_file
    mcs%plot_width = mci%plot_width
    mcs%plot_height = mci%plot_height
    mcs%plot_histograms = mci%plot_histograms
    mcs%plot_curves = mci%plot_curves
    mcs%plot_symbols = mci%plot_symbols
    mcs%plot_smoothly = mci%plot_smoothly
    mcs%plot_errors = mci%plot_errors
    mcs%plot_excess = mci%plot_excess
    mcs%plot_per_bin = mci%plot_per_bin
    mcs%plot_grids_channels = mci%plot_grids_channels
    mcs%plot_grids_logscale = mci%plot_grids_logscale
    mcs%slha_rewrite_input = mci%slha_rewrite_input
    mcs%slha_ignore_errors = mci%slha_ignore_errors
    mcs%program_start_time = program_start_time
    mcs%program_stop_time = &
         mcs%program_start_time + time (0,0,mcs%time_limit,0,0)    
    msg_level = mcs%chattiness
    if (mcs%n_flv > 0) &
         & call check_sqrts (mcs%sqrts, mcs%n_flv_out, mcs%code_out, mcs%par)
    call create (mcs%partons, &
         &       mcs%polarized_beams, mcs%n_hel > 0, &
         &       mcs%beam%vector_polarization, &
         &       mcs%beam_recoil, mcs%recoil_conserve_momentum, &
         &       mcs%n_out==1, mcs%n_in, &
         &       mcs%sqrts, mcs%beam, mcs%n_col_in, mcs%n_hel_in, mcs%n_flv_in)
    if (mcs%calls(1,1) <= 0) then
       mcs%calls(1,1) = 1
    end if
    if (mcs%calls(1,2) <= 0) then
       select case (mcs%n_out)
       case (:2);  mcs%calls(1,2) =  3
       case (3);   mcs%calls(1,2) =  5
       case (4);   mcs%calls(1,2) = 10
       case (5);   mcs%calls(1,2) = 15
       case (6);   mcs%calls(1,2) = 20
       case (7);   mcs%calls(1,2) = 25
       case (8:);  mcs%calls(1,2) = 30
       end select
    end if
    if (mcs%calls(1,3) <= 0) then
       mcs%calls(1,3) = 3
    end if
    if (mcs%n_events_warmup > 0) then
       mcs%calls(1,4) = 1
    end if
    if (mcs%n_events > 0 .or. mcs%n_calls > 0 .or. mcs%luminosity > 0) then
       mcs%calls(1,5) = 1
    end if
    if (mcs%calls(2,1) <= 0) then
       select case (mcs%n_out)
       case (:2);  mcs%calls(2,1) =   5000
       case (3);   mcs%calls(2,1) =  10000
       case (4);   mcs%calls(2,1) =  20000
       case (5);   mcs%calls(2,1) =  50000
       case (6);   mcs%calls(2,1) = 100000
       case (7);   mcs%calls(2,1) = 200000
       case (8:);  mcs%calls(2,1) = 500000
       end select
    end if
    if (mcs%calls(2,2) <= 0) then
       mcs%calls(2,2) = mcs%calls(2,1)
    end if
    if (mcs%calls(2,3) <= 0) then
       mcs%calls(2,3) = mcs%calls(2,2)
    end if
    if (mcs%n_events_warmup > 0) then
       mcs%calls(2,4) = mcs%n_events_warmup
    end if
    mcs%calls(2,5) = 0
    mcs%n_iterations_whizard = &
         & 1 + mcs%calls(1,2) + 1 + mcs%calls(1,4) + mcs%calls(1,5)
    mcs%n_iterations_vamp = sum(mcs%calls(1,:))
    read_model_file = &
         & choose_filename (mcs%read_model_file, default_filename)
    write_phase_space_channels_file = &
         & choose_filename (mcs%write_phase_space_channels_file, default_filename)
    if (proc_id == WHIZARD_ROOT) then
       if (mcs%n_flv > 0) then
          call forest_count (unit_phs_old, unit_phs_new, &
               & mcs%directory, read_model_file, write_phase_space_channels_file, &
               & trim(mcs%process_id_current), mcs%code, mcs%n_in, mcs%n_out, &
               & mcs%par, mcs%sqrts, &
               & mcs%generate_phase_space, mcs%phs, mcs%n_channels, &
               & new_phase_space)
       else
          call msg_message (" Zero matrix element -- skipping phase space generation")
          mcs%n_channels = 0
       end if
    end if
    call mpi90_broadcast(mcs%n_channels, WHIZARD_ROOT)
    call check_calls_per_channel &
         & (mcs%calls(2,1:3), mcs%n_channels * &
         &  max(mcs%min_calls_per_channel, mcs%min_bins * mcs%min_calls_per_bin))
    call mpi90_broadcast(mcs%n_channels, WHIZARD_ROOT)
    call create (mcs%f, mcs%n_in, mcs%n_out, mcs%n_channels)
    if (mcs%n_channels > 0) then
       if (proc_id == WHIZARD_ROOT) then
          if (new_phase_space) then
             rewind (unit_phs_new)
             call forest_read (unit_phs_new, mcs%f, trim(mcs%process_id_current))
          else
             rewind (unit_phs_old)
             call forest_read (unit_phs_old, mcs%f, trim(mcs%process_id_current))
          end if
       end if
       call mpi90_broadcast(mcs%f, WHIZARD_ROOT)
       call forest_set_strfun &
            & (mcs%f, sum(mcs%beam%n_strfun), sum(mcs%beam%n_recoil), &
            &  mcs%beam%generated_externally)
       call forest_prepare (mcs%f, mcs%code(mcs%n_in+1:,1), &
            &               mcs%par, mcs%cuts, mcs%use_equivalences)
       call forest_set_equivalences (mcs%f)
    end if
    if (.not.process_active (trim(mcs%process_id_current))) then
       call process_create (trim(mcs%process_id_current))
    else
       call msg_warning &
            & (" Process "//trim(mcs%process_id_current) &
            & // " is requested more than once.")
    end if
    call process_reset (mcs%par, trim(mcs%process_id_current))
    if (mcs%user_weight_mode /= 0) then
       call msg_message (" Activating matrix element reweighting function in user.f90")
    end if
    call shower_parameters_init (mcs%spar)
    if (mcs%spar%shower_on) then
       call msg_warning &
            (" >>>>> Parton shower is in alpha phase, use with caution! <<<<<")
    end if
    call create (mcs%cc)
    if (mcs%n_flv > 0) then
       write_default_cuts_file = &
            & choose_filename (mcs%write_default_cuts_file, default_filename)
       call make_cuts &
            & (mcs%directory, write_default_cuts_file, mcs%process_id_current, &
            &  mcs%cc, &
            &  mcs%code, mcs%n_in, mcs%n_out, sum(mcs%beam%n_strfun), &
            &  mcs%par, mcs%cuts)
       if (mcs%user_cut_mode /= 0) then
          write (msg_buffer, "(1x,A,I5)") &
               & "Activating additional preselection cuts in user.f90: Mode #", &
               & mcs%user_cut_mode
          call msg_message
          call set_user_cut (mcs%cc, mcs%user_cut_mode)
       end if
    end if
    mcs%checksum = mc_checksum (mcs)
    mcs%it = 0
    mcs%it_vamp = 0
    allocate(mcs%history(mcs%n_iterations_vamp))
    allocate(mcs%histories(mcs%n_iterations_vamp, mcs%n_channels))
    call vamp_create_history(mcs%history, verbose=.false.)
    call vamp_create_history(mcs%histories, verbose=.false.)
    allocate(mcs%logtable(mcs%n_iterations_whizard))
    mcs%logtable = " "
    allocate(mcs%weights(mcs%n_iterations_whizard, mcs%n_channels))
    ! mcs%weights(1,:) = mcs%initial_weights(1:mcs%n_channels)
    mcs%weights(1,:) = 1
    mcs%weights(1,:) = mcs%weights(1,:) / sum(mcs%weights(1,:))
    mcs%integral = 0
    mcs%error    = 0
    mcs%chi2     = 0
    mcs%efficiency = 0
    mcs%accuracy   = 0
  end subroutine mc_state_create_single

  subroutine mc_state_destroy_multi (mcs)
    type(monte_carlo_state), dimension(:), intent(inout) :: mcs
    do mc_process_index=1, mc_number_processes
       call mc_state_destroy_single (mcs(mc_process_index))
    end do
    mc_process_index = mc_number_processes
  end subroutine mc_state_destroy_multi

  subroutine mc_state_destroy_single (mcs)
    type(monte_carlo_state), intent(inout) :: mcs
    call destroy (mcs%beam)
    deallocate (mcs%beam)
    call destroy (mcs%rho_out)
    deallocate(mcs%code, mcs%code_in, mcs%code_out)
    deallocate(mcs%code_zero, mcs%code_zero_in, mcs%code_zero_out)
    deallocate(mcs%hel_state, mcs%hel_state_in, mcs%hel_state_out)
    !!! Intel 9.0 runtime library bug
    !!! deallocate(mcs%col_flow, mcs%acl_flow)
    call destroy (mcs%partons)
    if (process_active (trim(mcs%process_id_current))) then
       call process_destroy(trim(mcs%process_id_current))
    end if
    call destroy(mcs%f)
    call destroy(mcs%cc)
    deallocate(mcs%history)
    deallocate(mcs%histories)
    call vamp_delete_grids (mcs%g)
    call vamp_delete_grids (mcs%g_best)
    call destroy (mcs%eq)
    deallocate(mcs%logtable)
    deallocate(mcs%weights)
  end subroutine mc_state_destroy_single

  subroutine mc_state_write_unit (u, mcs, mci)
    integer, intent(in) :: u
    type(monte_carlo_state), intent(in) :: mcs
    type(monte_carlo_input), intent(in) :: mci
    integer :: pass, it0, it1
    call msg_message (" " // trim(VERSION_STRING), unit=u)
    call write_process &
         & (u, mcs%process_id_current, mcs%code, mcs%n_in, mcs%n_out)
    if (mcs%user_weight_mode /= 0)  then
       write (msg_buffer, "(1x,A,I5)") &
            & " User reweighting function activated: Mode #", &
            & mcs%user_weight_mode
       call msg_message (unit=u)
    end if
    call mc_write_header (u, mcs%n_in, & 
         &  trim(mcs%process_id_current), mcs%process_factor, mcs%checksum)
    it0 = 0
    LEVELS: do pass=1,4
       select case (pass)
       case (1,4)
          it0 = it0 + 1
          it1 = it0
       case (2)
          it0 = it0 + 1
          it1 = it0 + mcs%calls(1,2)-1
       case (3)
          it0 = it0 + mcs%calls(1,2)
          it1 = it0
       end select
       if (it0>mcs%it) exit LEVELS
       call mc_write_result &
            & (u, min(it1,mcs%it)-it0+1, mcs%logtable(it0:min(it1,mcs%it)))
       call mc_write_hline(u)
    end do LEVELS
    if (mcs%show_input) then
       call mc_write_dline(u)
       call msg_message (" Input data:", unit=u)
       call write(u, mci)
    end if
    if (mcs%show_results) then
       call mc_write_dline(u)
       call msg_message (" Total cross section in fb:", unit=u)
       call write_results_namelist (u, mcs)
    end if
    if (associated (mci%slha)) then
       call mc_write_dline(u)
       call msg_message (" Begin SLHA data", unit=u)
       call write (u, mci%slha)
       if (mcs%show_results) then
          call write_results_slha (u, mcs)
          call slha_write_whizard_info (u)
       end if
       call msg_message (" End SLHA data", unit=u)
    end if
    if (mcs%show_phase_space) then
       call mc_write_dline(u)
       call msg_message (" Phase space configuration:", unit=u)
       call write(u, mcs%f)
       call write (u, mcs%eq)
    end if
    if (mcs%show_cuts) then
       call mc_write_dline(u)
       call msg_message (" Cut configuration:", unit=u)
       if (mcs%user_cut_mode /= 0) then
          write (msg_buffer, "(1x,A,I5)") &
               & " User-defined preselection cuts activated: Mode #", &
               & mcs%user_cut_mode
          call msg_message (unit=u)
       end if
       if (cut_number(mcs%cc)>0) then
          call mc_write_dline(u)
          call write(u, mcs%cc)
       end if
    end if
    if (mcs%show_event) then
       call mc_write_dline(u)
       call msg_message (" Event display:", unit=u)
       call mc_write_dline(u)
       call write(u, current_event)
    end if
    if (mcs%actual_integration_done) then
       if (mcs%show_histories) then
          call mc_write_dline(u)
          call msg_message (" Channel histories [vamp]:", unit=u)
          call vamp_write_history(u, mcs%histories)
       end if
       if (mcs%show_weights) then
          call mc_write_dline(u)
          call msg_message (" Weight history [%]:", unit=u)
          call mc_write_weights_unit(u,mcs)
       end if
       if (mcs%show_history) then
          call mc_write_dline(u)
          call msg_message (" Global history [vamp]:", unit=u)
          call vamp_write_history(u, mcs%history)
       end if
    else
       call msg_message (" History information not shown (results read from file).", unit=u)
    end if
  end subroutine mc_state_write_unit

  subroutine mc_state_write_name_multi (filename, mcs, mci)
    character(len=*), intent(in) :: filename
    type(monte_carlo_state), dimension(:), intent(in) :: mcs
    type(monte_carlo_input), intent(in) :: mci
    integer :: u
    character(len=PROCESS_ID_LEN) :: process_id
    u = free_unit()
    do mc_process_index=1, mc_number_processes
       process_id = mcs(mc_process_index)%process_id_current
       open (unit = u, action = 'write', status = 'replace', &
            & file = trim (concat (mcs(mc_process_index)%directory, &
            &                      filename, process_id, "out")))
       call mc_state_write_unit (u, mcs(mc_process_index), mci)
       close (unit=u)
    end do
    mc_process_index = mc_number_processes
  end subroutine mc_state_write_name_multi

  subroutine mc_state_write_name_single (filename, mcs, mci)
    character(len=*), intent(in) :: filename
    type(monte_carlo_state), intent(in) :: mcs
    type(monte_carlo_input), intent(in) :: mci
    integer :: u
    character(len=PROCESS_ID_LEN) :: process_id
    u = free_unit()
    process_id = mcs%process_id_current
    open (unit = u, action = 'write', status = 'replace', &
         & file = trim (concat (mcs%directory, filename, process_id, "out")))
    call mc_state_write_unit (u, mcs, mci)
    close (unit=u)
  end subroutine mc_state_write_name_single

  subroutine mc_write_weights_unit(u,mcs)
    integer, intent(in) :: u
    type(monte_carlo_state), intent(in) :: mcs
    integer :: it, ch
    write(u, '(1x,A6)', advance='no') '! ch |'
    do it=1, mcs%it
       write(u, '(I3)', advance='no') it
    end do
    write(u,*)
    call mc_write_hline(u)
    do ch=1, mcs%f%n_channels
       write(u, '(1x,I4,1x,A1)', advance='no') ch, '|'
       do it=1, mcs%it
          if (mcs%weights(it,ch)==0) exit
          write(u, '(I3)', advance='no') nint(100*mcs%weights(it,ch))
       end do
       write(u,*)
    end do
  end subroutine mc_write_weights_unit

  function mc_checksum (mcs) result (ch)
    type(monte_carlo_state), intent(in) :: mcs
    character(32) :: ch
    integer :: u
    integer(i32), dimension (1) :: mold
    logical :: first = .true.
    if (first) then
       call md5test; first = .false.
    end if
    u = free_unit ()
    open (u, status = "scratch", form = "unformatted")
    call write_parameter (transfer (mcs%process_id, mold))
    call write_parameter (transfer (mcs%polarized_beams, mold))
    call write_parameter (transfer (mcs%structured_beams, mold))
    call write_parameter (transfer (mcs%beam_recoil, mold))
    call write_parameter (transfer (mcs%recoil_conserve_momentum, mold))
    call write_parameter (transfer (mcs%par, mold))
    call write_parameter (transfer (mcs%cm_frame, mold))
    call write_parameter (transfer (mcs%sqrts, mold))
    call write_parameter (transfer (mcs%beam, mold))
    call write_parameter (transfer (mcs%stratified, mold))
    call write_parameter (transfer (mcs%min_bins, mold))
    call write_parameter (transfer (mcs%max_bins, mold))
    call write_parameter (transfer (mcs%min_calls_per_bin, mold))
    call write_parameter (transfer (mcs%min_calls_per_channel, mold))
    call write_parameter (transfer (mcs%azimuthal_dependence, mold))
    call write_parameter (transfer (mcs%f, mold))
    call write_parameter (transfer (mcs%cc, mold))
    rewind (u)
    ch = md5sum (u)
    close (u)
  contains
    subroutine write_parameter (x)
      integer(i32), dimension(:), intent(in) :: x
      integer :: i
      do i = 1, size (x)
         write (u) x(i)
      end do
    end subroutine write_parameter
  end function mc_checksum
  subroutine mc_checksum_error (file)
    character(*), intent(in) :: file
    call msg_message (" Input parameters or phase space setting apparently have changed.")
    call msg_message (" [Set read_"//file//"_force = T if you nevertheless want to proceed.]")
    call msg_fatal (" Checksum mismatch: File doesn't match the current setup.")
  end subroutine mc_checksum_error

  subroutine mc_write_header_unit &
       & (u, n_in, process_id, process_factor, checksum, events)
    integer, intent(in) :: u, n_in
    character(len=*), intent(in), optional :: process_id
    real(kind=default), intent(in), optional :: process_factor
    character(32), intent(in), optional :: checksum
    logical, intent(in), optional :: events
    logical :: evts, wrote_checksum, wrote_factor
    evts = .false.;  if (present(events)) evts = events
    if (present(process_id)) then
       call msg_message (" ", unit=u)
       write (msg_buffer, "(A,1x,A32,1x,A)") &
            & " WHIZARD run for process "//trim(process_id)//":"
       call msg_message (unit=u)
       call mc_write_dline_unit(u)
    end if
    wrote_checksum = .false.
    if (present(checksum)) then
       write (msg_buffer, "(A)") &
            & " Input checksum = " // checksum
       call msg_message (unit=u)
       wrote_checksum = .true.
    end if
    wrote_factor = .false.
    if (present(process_factor)) then
       if (process_factor /= 1._default) then
          write (msg_buffer, "(9x,A,1x,1PE14.7)") "Factor:", process_factor
          call msg_message (unit=u)
          wrote_factor = .true.
       end if
    end if
    if (wrote_checksum .or. wrote_factor)  call mc_write_hline_unit (u)
    if (u /= 6 .or. msg_level > 3) then
       if (.not.evts) then
          call msg_message &
               & (" It      Calls  Integral" // PHYS_UNIT(n_in) // &
               &  " Error" // PHYS_UNIT(n_in) // &
               &  "  Err[%]    Acc  Eff[%]   Chi2 N[It]", unit=u)
       else
          call msg_message &
               & (" It      Events Integral" // PHYS_UNIT(n_in) // &
               &  " Error" // PHYS_UNIT(n_in) // &
               &  "  Err[%]    Acc  Eff[%]   Chi2 N[It]", unit=u)
       end if
    end if
    call mc_write_hline_unit(u)
  end subroutine mc_write_header_unit
  subroutine mc_write_header_summary (u, n_in)
    integer, intent(in) :: u, n_in
    call mc_write_hline (u)
    call msg_message &
         & (" Process ID     Integral" // PHYS_UNIT(n_in) // &
         &  " Error" // PHYS_UNIT(n_in) // &
         &  "  Err[%]        Frac[%]", unit=u)
    call mc_write_hline (u)
  end subroutine mc_write_header_summary

  subroutine mc_write_hline_unit(u)
    integer, intent(in) :: u
    if (u /= 6 .or. msg_level > 3)  call msg_message(repeat('-',77), unit=u)
  end subroutine mc_write_hline_unit
  subroutine mc_write_dline_unit(u)
    integer, intent(in) :: u
    if (u /= 6 .or. msg_level > 3)  call msg_message(repeat('=',77), unit=u)
  end subroutine mc_write_dline_unit
  subroutine mc_write_result_string &
       & (s, it, calls, integral, error, efficiency, accuracy, &
       &  use_efficiency, improved, chi2, iterations)
    character(len=*), intent(inout) :: s
    integer, intent(in) :: it
    integer, intent(in) :: calls
    real(kind=default), intent(in) :: integral, error, efficiency, accuracy
    logical, intent(in), optional :: use_efficiency, improved
    real(kind=default), intent(in), optional :: chi2
    integer, intent(in), optional :: iterations
    character(len=len(s)) :: buffer
    integer :: pos
    real(kind=default) :: abs_error, rel_error
    s = " "; buffer = " "; pos = 1
    call compute_errors (error, integral, abs_error, rel_error)
    write (buffer, '(1x,I3,1x,I10,1x,1PE14.7,1x,1PE9.2,1x,2PF7.2)') &
         & it, calls, integral, abs_error, rel_error
    call append(len_trim(buffer))
    write(buffer,'(1x,0PF7.2)') accuracy
    call append(len_trim(buffer))
    if (present(use_efficiency)) then
       if (.not.use_efficiency.and.present(improved)) then
          if (improved) then
             buffer = "*"
          end if
       end if
    end if
    call append(1)
    write(buffer,'(2PF6.2)') efficiency
    call append(len_trim(buffer))
    if (present(use_efficiency)) then
       if (use_efficiency.and.present(improved)) then
          if (improved) then
             buffer = "*"
          end if
       end if
    end if
    call append(1)
    if (present(chi2)) then
       write(buffer,'(0PF7.2)') chi2
       call append(len_trim(buffer))
       if (present(iterations)) then
          call append(1)
          write(buffer,'(I3)') iterations
          call append(len_trim(buffer))
       end if
    end if
  contains
    subroutine append(p)
      integer, intent(in) :: p
      s(pos+1:pos+p) = buffer
      pos = pos+p
      buffer = " "
    end subroutine append
  end subroutine mc_write_result_string

  subroutine compute_errors (error, integral, abs_error, rel_error)
    real(kind=default), intent(in) :: error, integral
    real(kind=default), intent(out) :: abs_error, rel_error
    if (abs(error) > 1E-99_default) then
       abs_error = error
    else
       abs_error = 0
    end if
    if (integral /= 0) then
       rel_error = abs_error / integral
    else
       rel_error = 0
    end if
  end subroutine compute_errors

  subroutine mc_write_result_unit &
       & (u, it, calls, integral, error, efficiency, accuracy, &
       &  use_efficiency, improved, chi2, iterations)
    integer, intent(in) :: u, it
    integer, intent(in) :: calls
    real(kind=default), intent(in) :: integral, error, efficiency, accuracy
    logical, intent(in), optional :: use_efficiency, improved
    real(kind=default), intent(in), optional :: chi2
    integer, intent(in), optional :: iterations
    character(len=LOGTABLE_LINE_LENGTH) :: s
    call mc_write_result_string(s, it, calls, integral, error, &
         & efficiency, accuracy, use_efficiency, improved, chi2, iterations)
    call msg_result (s, u)
  end subroutine mc_write_result_unit

  subroutine mc_write_summary_unit &
       & (u, process_id, integral, error, total_integral)
    integer, intent(in) :: u
    character(len=*), intent(in) :: process_id
    real(kind=default), intent(in) :: integral, error, total_integral
    character(len=LOGTABLE_LINE_LENGTH) :: s
    character(len=13) :: tag
    real(kind=default) :: abs_error, rel_error, fraction
    tag = process_id
    call compute_errors (error, integral, abs_error, rel_error)
    if (total_integral /= 0) then
       fraction = integral / total_integral
    else
       fraction = 0
    end if
    write(s,'(3x,A13,1x,1PE14.7,1x,1PE9.2,1x,2PF7.2,9x,2PF6.2)') &
         & tag, integral, abs_error, rel_error, fraction
    call msg_result (s, u)
  end subroutine mc_write_summary_unit

  subroutine mc_write_logtable_unit (u, it, s, raw)
    integer, intent(in) :: u, it
    character(len=*), dimension(:), intent(in) :: s
    logical, intent(in), optional :: raw
    integer :: i
    logical :: write_raw
    write_raw = .false.;  if (present (raw))  write_raw = raw
    do i=1, it
       if (write_raw) then
          write (u) s(i)
       else if (len_trim (s(i)) > 0 .or. u /= 6) then
          call msg_result (s(i), u)
       end if
    end do
  end subroutine mc_write_logtable_unit

  subroutine write_results_namelist (u, mcs)
    integer, intent(in) :: u
    type(monte_carlo_state), intent(in) :: mcs
    write (u, *) "&integration_results"
    write (u, *) "integral = ", mcs%integral
    write (u, *) "error    = ", mcs%error
    write (u, *) "chi2     = ", mcs%chi2
    write (u, *) "calls_total = ", mcs%calls(1,3) * mcs%calls(2,3)
    write (u, *) "iterations  = ", mcs%calls(1,3)
    write (u, *) "/"
  end subroutine write_results_namelist
  subroutine write_results_slha (u, mcs)
    integer, intent(in) :: u
    type(monte_carlo_state), intent(in) :: mcs
1 format('#',1x,A)
    write (u, 1)
    write (u, 1) "WHIZARD: The following blocks have been added:"
    call slha_write_process &
         & (u, mcs%code_in(:,1), mcs%code_out(:,1), &
         &  mcs%integral, mcs%error, mcs%chi2)
  end subroutine write_results_slha
  subroutine mc_read_logtable_unit (u, it, s, raw)
    integer, intent(in) :: u, it
    character(len=LOGTABLE_LINE_LENGTH), dimension(:), intent(inout) :: s
    logical, intent(in), optional :: raw
    integer :: i, n_it, n_ignore
    logical :: read_raw
    character(len=LOGTABLE_LINE_LENGTH) :: cdummy
    read_raw = .false.;  if (present (raw))  read_raw = raw
    if (it <= size(s)) then
       n_it = it
       n_ignore = 0
    else
       n_it = size(s)
       n_ignore = it - size(s)
    end if
    if (read_raw) then
       do i=1, n_it
          read (u) s(i)
       end do
       do i=1, n_ignore
          read (u) cdummy
       end do
    else
       do i=1, n_it
          read (u, '(A)') s(i)
       end do
       do i=1, n_ignore
          read (u, *)
       end do
    end if
  end subroutine mc_read_logtable_unit

  function mc_get_efficiency (g) result(efficiency)
    type(vamp_grids), intent(in) :: g
    real(kind=default) :: efficiency
    real(kind=default) :: wsum
    real(kind=default), dimension(size(g%grids)) :: weights
    where(g%num_calls >= 2 .and. g%grids%f_max>0)
       weights = g%weights * g%grids%f_max
    elsewhere
       weights = 0
    end where
    wsum = sum(weights)
    if (wsum/=0) then
       efficiency = dot_product (mc_get_efficiency_vector (g, size(g%grids)), &
            &                    weights) / wsum
    else
       efficiency = 1
    end if
  end function mc_get_efficiency

  function mc_get_efficiency_vector (g, s) result(efficiency)
    type(vamp_grids), intent(in) :: g
    integer, intent(in) :: s
    real(kind=default), dimension(s) :: efficiency
    where(g%num_calls >= 2 .and. g%grids%f_max>0)
       efficiency = g%grids%mu(1) / g%grids%f_max
    elsewhere
       efficiency = 0
    end where
  end function mc_get_efficiency_vector

  function mc_get_accuracy_integer (integral, error, calls) result(a)
    real(kind=default), intent(in) :: integral, error
    integer, intent(in) :: calls
    real(kind=default) :: a
    if (integral/=0) then
       a = error / integral * sqrt(real(calls, kind=default))
    else
       a = 0
    end if
  end function mc_get_accuracy_integer

  function mc_get_accuracy_real (integral, error, calls) result(a)
    real(kind=default), intent(in) :: integral, error, calls
    real(kind=default) :: a
    if (integral/=0) then
       a = error / integral * sqrt(calls)
    else
       a = 0
    end if
  end function mc_get_accuracy_real


end module monte_carlo_states
