! WHIZARD 1.95 Feb 25 2010
! 
! (C) 1999-2008 by 
!     Wolfgang Kilian <wolfgang.kilian@desy.de>
!     Thorsten Ohl <ohl@physik.uni-wuerzburg.de>
!     Juergen Reuter <juergen.reuter@physik.uni-freiburg.de>
!
! WHIZARD is free software; you can redistribute it and/or modify it
! under the terms of the GNU General Public License as published by 
! the Free Software Foundation; either version 2, or (at your option)
! any later version.
!
! WHIZARD is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! This file has been stripped of most comments.  For documentation, refer
! to the source 'whizard.nw'

module mappings

  use kinds, only: default !NODEP!
  use limits, only: MAPPING_SCALE_MIN, TC
  use diagnostics, only: msg_fatal
  use constants, only: pi
  use parameters, only: parameter_set !NODEP!
  use parameters, only: write !NODEP!
  use particles
  use divisions !NODEP!
  use cuts, only: default_cuts

  implicit none
  private

  type, public :: mapping
     private
     integer :: mode
     integer :: particle_code
     real(kind=default) :: mass, width
     real(kind=default) :: a1, a2, a3
     real(kind=default) :: b1, b2, b3
     logical :: variable_limits, a_unknown, b_unknown
  end type mapping


  public :: write
  public :: set
  public :: operator(==)
  public :: apply, get

  integer, parameter, public :: &
       & NO_MAPPING = 0, S_CHANNEL = 1, T_CHANNEL =  2, U_CHANNEL = -2, &
       & RADIATION = 3, COLLINEAR = 4, INFRARED = 5
  integer, parameter, public :: ON_SHELL = -1
  type(mapping), parameter, public :: &
       & mapping_null = &
       & mapping (NO_MAPPING, 0, 0._default, 0._default, &
       &          0._default, 0._default, 0._default, &
       &          0._default, 0._default, 0._default, &
       &          .true., .true., .true.)

  interface write
     module procedure mapping_write_unit
  end interface
  interface set
     module procedure mapping_set_code
     module procedure mapping_set_parameters
  end interface
  interface operator(==)
     module procedure mapping_equal
  end interface
  interface apply
     module procedure mapping_apply_msq
     module procedure mapping_apply_ct
  end interface
  interface get
     module procedure mapping_get_msq
     module procedure mapping_get_ct
  end interface

contains

  subroutine mapping_write_unit (u, map, k)
    integer, intent(in) :: u
    type(mapping), intent(in) :: map
    integer(kind=TC), intent(in), optional :: k
    character(len=9) :: str
    select case(map%mode)
    case(S_CHANNEL); str = "s-channel"
    case(COLLINEAR); str = "collinear"
    case(INFRARED);  str = "infrared "
    case(RADIATION); str = "radiation"
    case(T_CHANNEL); str = "t-channel"
    case(U_CHANNEL); str = "u-channel"
    case(ON_SHELL);  str = "on-shell "
    end select
    if (present(k)) then
       if (map%mode /= NO_MAPPING) &
            & write(u, '(1x,A,I4,A)') &
            &   'Branch #', k, ":  Mapping (" // str // ") for particle " // &
            &   particle_name(map%particle_code)
    else
       write (u, *) "Mapping: " // str // " for particle " // &
            & particle_name (map%particle_code)
       write (u, *) " Code: ", map%particle_code, &
            & " Mass: ", map%mass, " Width: ", map%width
    end if
  end subroutine mapping_write_unit

  subroutine mapping_set_code (map, mode, code)
    type(mapping), intent(inout) :: map
    integer, intent(in) :: mode, code
    map%mode = mode
    map%particle_code = code
  end subroutine mapping_set_code

  subroutine mapping_set_parameters (map, par, cuts, variable_limits, k)
    type(mapping), intent(inout) :: map
    type(parameter_set), intent(in) :: par
    type(default_cuts), intent(in) :: cuts
    logical, intent(in) :: variable_limits
    integer(kind=TC), intent(in), optional :: k
    if (map%mode /= NO_MAPPING) then
       map%mass  = particle_mass (map%particle_code, par)
       map%width = particle_width (map%particle_code, par)
       map%variable_limits = variable_limits
       map%a_unknown = .true.
       map%b_unknown = .true.
       select case (map%mode)
       case (ON_SHELL)
          if (map%mass <= 0) then
             call write (6, par)
             if (present(k)) call write (6, map, k)
             call msg_fatal &
                  & (" On-shell intermediate state must have positive mass")
          end if
       case (S_CHANNEL)
          if (map%mass <= 0) then
             call write (6, par)
             if (present(k)) call write (6, map, k)
             call msg_fatal &
                  & (" S-channel resonance must have positive mass")
          else if (map%width <= 0) then
             call write (6, par)
             if (present(k)) call write (6, map, k)
             call msg_fatal &
                  & (" S-channel resonance must have positive width")
          end if
       case (RADIATION)
          map%width = &
               & max (map%width, cuts%default_energy_cut, MAPPING_SCALE_MIN)
       case (INFRARED, COLLINEAR)
          if (particle_color (map%particle_code) /= 0) then
             map%mass = &
                  & max (map%mass, cuts%default_jet_cut, MAPPING_SCALE_MIN)
          else
             map%mass = &
                  & max (map%mass, cuts%default_mass_cut, MAPPING_SCALE_MIN)
          end if
       case (T_CHANNEL, U_CHANNEL)
          map%mass = &
               & max (map%mass, cuts%default_Q_cut, MAPPING_SCALE_MIN)
       end select
    end if
  end subroutine mapping_set_parameters

  function mapping_equal (m1, m2) result (equal)
    type(mapping), intent(in) :: m1, m2
    logical :: equal
    if (m1%mode == m2%mode) then
       select case (m1%mode)
       case (NO_MAPPING)
          equal = .true.
       case (S_CHANNEL, RADIATION)
          equal = (m1%mass == m2%mass) .and. (m1%width == m2%width)
       case default
          equal = (m1%mass == m2%mass)
       end select
    else
       equal = .false.
    end if
  end function mapping_equal

  subroutine mapping_apply_msq &
       & (map, r, s, msq, msq_min, msq_max, factor)
    type(mapping), intent(inout) :: map
    real(kind=default), intent(in) :: r, s, msq_min, msq_max
    real(kind=default), intent(out) :: msq, factor
    real(kind=default) :: rr, msq0, msq1, tmp, x
    integer :: mode
    x = r
    factor = 1
    if (s==0)  call msg_fatal (" Applying msq mapping for zero energy")
    mode = map%mode
    if (mode == S_CHANNEL) then
       msq0 = map%mass**2
       if (msq0 < msq_min .or. msq0 > msq_max) mode = NO_MAPPING
    end if
    select case(mode)
    case (NO_MAPPING)
       if (map%variable_limits .or. map%a_unknown) then
          map%a1 = 0
          map%a2 = msq_max - msq_min
          map%a3 = map%a2 / s
          map%a_unknown = .false.
       end if
       msq = (1-x) * msq_min + x * msq_max
       factor = factor * map%a3
    case(ON_SHELL)
       msq = map%mass**2
       ! factor = factor   ! should probably be some multiple of 2pi ... ?
    case (S_CHANNEL)
       if (map%variable_limits .or. map%a_unknown) then
          map%a1 = atan((msq_min-msq0)/(map%mass*map%width))/pi
          map%a2 = atan((msq_max-msq0)/(map%mass*map%width))/pi
          map%a3 = (map%a2 - map%a1) * pi * (map%mass * map%width)/s 
          map%a_unknown = .false.
       end if
       rr = (1-x) * map%a1 + x * map%a2
       if (rr > -0.5_default .and. rr < .5_default) then
          tmp = tan (rr * pi)
          msq = map%mass * (map%mass + map%width * tmp)
          factor = factor * map%a3 * (1 + tmp**2) 
       else
          msq = 0
          factor = 0
       end if
    case (COLLINEAR, INFRARED, RADIATION)
       if (map%variable_limits .or. map%a_unknown) then
          if (mode == RADIATION) then
             msq0 = map%width**2
          else
             msq0 = map%mass**2
          end if
          map%a1 = msq0
          map%a2 = log ((msq_max - msq_min)/msq0 + 1)
          map%a3 = map%a2 / s
          map%a_unknown = .false.
       end if
       msq1 = map%a1 * exp (x * map%a2)
       msq = msq1 - map%a1 + msq_min 
       factor = factor * map%a3 * msq1
    case (T_CHANNEL, U_CHANNEL)
       if (map%variable_limits .or. map%a_unknown) then
          msq0 = map%mass**2
          map%a1 = msq0
          map%a2 = 2 * log ((msq_max - msq_min)/(2*msq0) + 1)
          map%a3 = map%a2 / s
          map%a_unknown = .false.
       end if
       if (x < .5_default) then
          msq1 = map%a1 * exp (x * map%a2)
          msq = msq1 - map%a1 + msq_min
       else
          msq1 = map%a1 * exp ((1-x) * map%a2)
          msq = -(msq1 - map%a1) + msq_max
       end if
       factor = factor * map%a3 * msq1
    case default
       call msg_fatal ( " Attempt to apply undefined msq mapping")
    end select
  end subroutine mapping_apply_msq

  subroutine mapping_get_msq &
       & (map, r, s, msq, msq_min, msq_max, factor)
    type(mapping), intent(inout) :: map
    real(kind=default), intent(out) :: r
    real(kind=default), intent(in) :: s, msq, msq_min, msq_max
    real(kind=default), intent(out) :: factor
    real(kind=default) :: msq0, msq1, tmp
    integer :: mode
    factor = 1
    if (s==0) call msg_fatal (" Applying inverse msq mapping for zero energy")
    mode = map%mode
    if (mode == S_CHANNEL) then
       msq0 = map%mass**2
       if (msq0 < msq_min .or. msq0 > msq_max)  mode = NO_MAPPING
    end if
    select case(mode)
    case (NO_MAPPING)
       if (map%variable_limits .or. map%a_unknown) then
          map%a1 = 0
          map%a2 = msq_max - msq_min
          map%a3 = map%a2 / s
          map%a_unknown = .false.
       end if
       if (map%a2 /= 0) then
          r = (msq - msq_min) / map%a2
       else
          r = 0
       end if
       factor = factor * map%a3
    case (ON_SHELL)
       r = 0
       ! factor = factor ! should probably be some multiple of 2pi ...?
    case (S_CHANNEL)
       if (map%variable_limits .or. map%a_unknown) then
          map%a1 = atan((msq_min-msq0)/(map%mass*map%width))/pi
          map%a2 = atan((msq_max-msq0)/(map%mass*map%width))/pi
          map%a3 = (map%a2 - map%a1) * pi * (map%mass * map%width)/s 
          map%a_unknown = .false.
       end if
       tmp = (msq - msq0) / (map%mass * map%width)
       r = (atan(tmp)/pi - map%a1) / (map%a2 - map%a1)
       factor = factor * map%a3 * (1 + tmp**2)
    case (COLLINEAR, INFRARED, RADIATION)
       if (map%variable_limits .or. map%a_unknown) then
          if (mode == RADIATION) then
             msq0 = map%width**2
          else
             msq0 = map%mass**2
          end if
          map%a1 = msq0
          map%a2 = log ((msq_max - msq_min)/msq0 + 1)
          map%a3 = map%a2 / s
          map%a_unknown = .false.
       end if
       msq1 = msq - msq_min + map%a1
       r = log (msq1/map%a1) / map%a2
       factor = factor * msq1 * map%a3
    case (T_CHANNEL, U_CHANNEL)
       if (map%variable_limits .or. map%a_unknown) then
          msq0 = map%mass**2
          map%a1 = msq0
          map%a2 = 2 * log ((msq_max - msq_min)/(2*msq0) + 1)
          map%a3 = map%a2 / s
          map%a_unknown = .false.
       end if
       if (msq < (msq_max + msq_min)/2) then
          msq1 = msq - msq_min + map%a1
          r = log (msq1/map%a1) / map%a2
       else
          msq1 = msq_max - msq + map%a1
          r = 1 - log (msq1/map%a1) / map%a2
       end if
       factor = factor * map%a3 * msq1
    case default
       call msg_fatal (" Attempt to apply undefined (inverse) msq mapping")
    end select
  end subroutine mapping_get_msq

  subroutine mapping_apply_ct (map, sqrts, r, ct, st, factor)
    type(mapping), intent(inout) :: map
    real(kind=default), intent(in) :: sqrts, r
    real(kind=default), intent(out) :: ct, st, factor
    real(kind=default) :: x, ct1, tmp
    factor = 1
    x = r
    select case(map%mode)
    case(NO_MAPPING, S_CHANNEL, INFRARED, RADIATION, ON_SHELL)
       tmp = 2 * (1-x)
       ct = 1 - tmp
       st = sqrt (tmp * (2-tmp))
    case (T_CHANNEL, U_CHANNEL, COLLINEAR)
       if (map%variable_limits .or. map%b_unknown) then
          map%b1 = (map%mass / sqrts)**2
          map%b2 = log ((map%b1 + 1) / map%b1)
          map%b3 = 0
          map%b_unknown = .false.
       end if
       if (x < .5_default) then
          ct1 = map%b1 * exp (2 * x * map%b2)
          ct = ct1 - map%b1 - 1
       else
          ct1 = map%b1 * exp (2 * (1-x) * map%b2)
          ct = -(ct1 - map%b1) + 1
       end if
       if (ct>=-1 .and. ct<=1) then
          st = sqrt (1 - ct**2)
          factor = factor * ct1 * map%b2
       else
          ct = 1;  st = 0;  factor = 0
       end if
    case default
       call msg_fatal (" Attempt to apply undefined ct mapping")
    end select
  end subroutine mapping_apply_ct

  subroutine mapping_get_ct (map, sqrts, r, ct, factor)
    type(mapping), intent(inout) :: map
    real(kind=default), intent(in) :: sqrts
    real(kind=default), intent(out) :: r, factor
    real(kind=default), intent(in) :: ct
    real(kind=default) :: ct1
    factor = 1
    select case(map%mode)
    case(NO_MAPPING, S_CHANNEL, INFRARED, RADIATION, ON_SHELL)
       r = (ct + 1) / 2
    case (T_CHANNEL, U_CHANNEL, COLLINEAR)
       if (map%variable_limits .or. map%b_unknown) then
          map%b1 = (map%mass / sqrts)**2
          map%b2 = log ((map%b1 + 1) / map%b1)
          map%b3 = 0
          map%b_unknown = .false.
       end if
       if (ct < 0) then
          ct1 = ct + map%b1 + 1
          r = log (ct1 / map%b1) / (2 * map%b2)
       else
          ct1 = -ct + map%b1 + 1
          r = 1 - log (ct1 / map%b1) / (2 * map%b2)
       end if
       factor = factor * ct1 * map%b2
    case default
       call msg_fatal (" Attempt to apply undefined (inverse) ct mapping")
    end select
  end subroutine mapping_get_ct


end module mappings
