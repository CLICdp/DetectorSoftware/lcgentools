! WHIZARD 1.95 Feb 25 2010
! 
! (C) 1999-2008 by 
!     Wolfgang Kilian <wolfgang.kilian@desy.de>
!     Thorsten Ohl <ohl@physik.uni-wuerzburg.de>
!     Juergen Reuter <juergen.reuter@physik.uni-freiburg.de>
!
! WHIZARD is free software; you can redistribute it and/or modify it
! under the terms of the GNU General Public License as published by 
! the Free Software Foundation; either version 2, or (at your option)
! any later version.
!
! WHIZARD is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! This file has been stripped of most comments.  For documentation, refer
! to the source 'whizard.nw'

module decay_trees

  use limits, only: TC
  use diagnostics, only: msg_bug
  use permutations, only: permutation, size

  implicit none
  private

  type, public :: decay_tree
     integer :: n_branches, n_externals, n_in
     integer(kind=TC) :: mask, mask_in, mask_out
     type(branch), dimension(:), pointer :: branch
  end type decay_tree

  type, public :: branch
     logical :: set, inverted_decay, inverted_axis
     integer(kind=TC) :: mother, sibling, friend, origin
     integer(kind=TC), dimension(2) :: daughter
     integer :: firstborn
     logical :: has_children, has_friend
  end type branch

  public :: tc_permute
  public :: create, destroy
  public :: write
  public :: decay_level
  public :: tree_from_array
  public :: tree_to_array
  public :: flip_t_to_s_channel
  public :: flipped
  public :: tree_canonicalize

  interface create
     module procedure tree_create
  end interface
  interface destroy
     module procedure tree_destroy
  end interface
  interface write
     module procedure tree_write_unit, branch_write_unit
  end interface
  interface decay_level
     module procedure decay_level_simple
     module procedure decay_level_complement
  end interface

contains

  function tc_permute (k, perm, mask_in) result (pk)
    integer(kind=TC), intent(in) :: k, mask_in
    type(permutation), intent(in) :: perm
    integer(kind=TC) :: pk
    integer :: i
    pk = iand (k, mask_in)
    do i=1, size(perm)
!       if (btest(k,i-1))  pk = ibset (pk, permute(i,perm)-1)
       if (btest(k,i-1))  pk = ibset (pk, perm%p(i)-1)
    end do
  end function tc_permute

  subroutine tree_create(t, n_in, n_out)
    type(decay_tree), intent(out) :: t
    integer, intent(in) :: n_in, n_out
    integer(kind=TC) :: i,n
    integer :: j
    !allocate(t)
    n = 2**(n_in+n_out)-1
    t%n_externals = n_in+n_out
    t%mask = 0
    do i=0, n_in+n_out-1
       t%mask = ibset(t%mask, i)
    end do
    t%n_in = n_in
    t%mask_in = 0
    do i=n_out, n_in+n_out-1
       t%mask_in = ibset(t%mask_in, i)
    end do
    t%mask_out = ieor(t%mask, t%mask_in)
    allocate(t%branch(n))
    do i=1, n
       call branch_create(t%branch(i))
    end do
    t%n_branches  = 0
  contains
    subroutine branch_create(b)
      type(branch), intent(out) :: b
      b%set = .false.
      b%inverted_decay = .false.
      b%inverted_axis  = .false.
      do j=1,2
         b%daughter(j) = 0
      end do
      b%mother  = 0
      b%sibling = 0
      b%friend  = 0
      b%origin  = 0
      b%has_children = .false.
      b%has_friend   = .false.
      b%firstborn = 0
    end subroutine branch_create
  end subroutine tree_create

  subroutine tree_destroy(t)
    type(decay_tree), intent(inout) :: t
    deallocate(t%branch)
    !deallocate(t)
  end subroutine tree_destroy

  subroutine tree_write_unit (u, t)
    integer, intent(in) :: u
    type(decay_tree), intent(in) :: t
    integer(kind=TC) :: k
    write(u,'(1X,A,I2,5X,A,I3)') &
         &'External:', t%n_externals, 'Mask:', t%mask
    write(u,'(1X,A,I2,5X,A,I3)') &
         &'Incoming:', t%n_in, 'Mask:', t%mask_in
    write(u,'(1X,A,I2,5X,A,I3)') &
         &'Branches:', t%n_branches
    do k = size(t%branch), 1, -1
       if (t%branch(k)%set)  call branch_write_unit (u, t%branch(k), k)
    end do
  end subroutine tree_write_unit
  subroutine branch_write_unit (u, b, kval)
    integer, intent(in) :: u
    type(branch), intent(in) :: b
    integer(kind=TC), intent(in), optional :: kval
    integer(kind=TC) :: k
    character(len=6) :: tmp
    character(len=1) :: firstborn(2), sign_decay, sign_axis
    integer :: i
    k = 0;  if (present(kval))  k = kval
    if (b%origin /= 0) then
       write(tmp, '(A,I4,A)') '(', b%origin, ')'
    else
       tmp = ' '
    end if
    do i=1, 2
       if (b%firstborn == i) then
          firstborn(i) = "*"
       else
          firstborn(i) = " "
       end if
    end do
    if (b%inverted_decay) then
       sign_decay = "-"
    else
       sign_decay = "+"
    end if
    if (b%inverted_axis) then
       sign_axis = "-"
    else
       sign_axis = "+"
    end if
    if (b%has_children) then
       if (b%has_friend) then
          write(u,'(1X,A,I4,1x,A,2X,A,I4,A,I4,A,2X,A,3X,A,I4)') &
               &   '*', k, tmp, &
               &   'Daughters: ', &
               &   b%daughter(1), firstborn(1), &
               &   b%daughter(2), firstborn(2), sign_decay, &
               &   'Friend:    ', b%friend
       else
          write(u,'(1X,A,I4,1x,A,2X,A,I4,A,I4,A,2X,A,2X,A)') &
               &   '*', k, tmp, &
               &   'Daughters: ', &
               &   b%daughter(1), firstborn(1), &
               &   b%daughter(2), firstborn(2), sign_decay, &
               &   '(axis '//sign_axis//')'
       end if
    else
       write(u,'(2X,I4,3X,A,I4,I4)') k
    end if
  end subroutine branch_write_unit

  function decay_level_complement (k, mask) result (l)
    integer(kind=TC), intent(in) :: k, mask
    integer :: l
    l = min (decay_level_simple (k), &
         &   decay_level_simple (ieor (k, mask)) + 1)
  end function decay_level_complement
  function decay_level_simple (k) result(l)
    integer(kind=TC), intent(in) :: k
    integer :: l
    integer :: i
    l = 0
    do i=0, bit_size(k)-1
       if (btest(k,i)) l = l+1
    end do
  end function decay_level_simple

  subroutine tree_from_array(t,a)
    type(decay_tree), intent(inout) :: t
    integer(kind=TC), dimension(:), intent(in) :: a
    integer :: i
    integer(kind=TC) :: k
    do i=1, size(a)
       k = a(i)
       if (iand(k, t%mask_in) == t%mask_in)  k = ieor(t%mask, k)
       t%branch(k)%set = .true.
       t%n_branches = t%n_branches+1
    end do
    do i=0, t%n_externals-1
       k = ibset(0,i)
       if (iand(k, t%mask_in) == t%mask_in)  k = ieor(t%mask, k)
       if (t%branch(ieor(t%mask, k))%set) then
          t%branch(ieor(t%mask, k))%set = .false.
          t%branch(k)%set = .true.
       else if (.not.t%branch(k)%set) then
          t%branch(k)%set = .true.
          t%n_branches = t%n_branches+1
       end if
    end do
    if (t%n_branches /= t%n_externals*2-3) then
       call write(6,t)
       call msg_bug &
            & (" Wrong number of branches set in phase space tree")
    end if
    do k=1, size(t%branch)
       if (t%branch(k)%set .and. decay_level(k)/=1) then
          call branch_set_relatives(k)
       end if
    end do
  contains
    subroutine branch_set_relatives(k)
      integer(kind=TC), intent(in) :: k
      integer(kind=TC) :: m,n
      do m=1, k-1
         if(iand(k,m)==m) then
            n = ieor(k,m)
            if ( t%branch(m)%set .and. t%branch(n)%set ) then
               t%branch(k)%daughter(1) = m;  t%branch(k)%daughter(2) = n
               t%branch(m)%mother      = k;  t%branch(n)%mother      = k
               t%branch(m)%sibling     = n;  t%branch(n)%sibling     = m
               t%branch(k)%has_children = .true.
               return
            end if
         end if
      end do
      call write(6, t)
      call msg_bug &
           & (" Missing daughter branch(es) in phase space tree")
    end subroutine branch_set_relatives

  end subroutine tree_from_array

  subroutine tree_to_array(t,a)
    type(decay_tree), intent(in) :: t
    integer(kind=TC), dimension(:), intent(inout) :: a
    integer(kind=TC) :: i,k
    i=0
    do k=1, size(t%branch)
       if (t%branch(k)%set) then
          i = i+1
          a(i) = k
       end if
    end do
  end subroutine tree_to_array
  subroutine flip_t_to_s_channel(t)
    type(decay_tree), intent(inout) :: t
    integer(kind=TC) :: k, f, m, n, d, s
    if (t%n_in == 2) then
       FLIP: do k=3, t%mask-1
          if (.not. t%branch(k)%set) cycle FLIP
          f = iand(k,t%mask_in)
          if (f==0 .or. f==k) cycle FLIP
          m = t%branch(k)%mother
          s = t%branch(k)%sibling
          if (s==0) call find_orphan(s)
          d = t%branch(k)%daughter(1)
          n = ior(d,s)  
          t%branch(k)%set = .false.
          t%branch(n)%set = .true.
          t%branch(n)%origin = k
          t%branch(n)%daughter(1) = d; t%branch(d)%mother  = n
          t%branch(n)%daughter(2) = s; t%branch(s)%mother  = n
          t%branch(n)%has_children = .true.
          t%branch(d)%sibling = s;  t%branch(s)%sibling = d
          t%branch(n)%sibling = f;  t%branch(f)%sibling = n
          t%branch(n)%mother      = m
          t%branch(f)%mother      = m
          if (m/=0) then
             t%branch(m)%daughter(1) = n
             t%branch(m)%daughter(2) = f
          end if
          t%branch(n)%friend = f
          t%branch(n)%has_friend = .true.
          t%branch(n)%firstborn = 2
       end do FLIP
    end if
  contains
    subroutine find_orphan(s)
      integer(kind=TC) :: s
      do s=1, t%mask_out
         if (t%branch(s)%set .and. t%branch(s)%mother==0) return
      end do
      call write(6, t)
      call msg_bug (" Can't flip phase space tree to channel")
    end subroutine find_orphan
  end subroutine flip_t_to_s_channel

  function flipped(t, kt) result(ks)
    type(decay_tree), intent(in) :: t
    integer(kind=TC), intent(in) :: kt
    integer(kind=TC) :: ks
    if (iand(kt,t%mask_in)==0) then
       ks = kt
    else
       ks = t%branch(iand(kt,t%mask_out))%mother
    end if
  end function flipped

  subroutine tree_canonicalize (t)
    type(decay_tree), intent(inout) :: t
    integer :: n_out
    integer(kind=TC) :: k_out
    call branch_canonicalize (t%branch(t%mask_out))
    n_out = t%n_externals - t%n_in
    k_out = t%mask_out
    if (t%branch(k_out)%has_friend &
         & .and. t%branch(k_out)%friend == ibset (0, n_out)) then
       t%branch(k_out)%inverted_axis = .not.t%branch(k_out)%inverted_axis
    end if
    t%branch(k_out)%has_friend = .false.
    t%branch(k_out)%friend = 0
  contains
    recursive subroutine branch_canonicalize (b)
      type(branch), intent(inout) :: b
      integer(kind=TC) :: d1, d2
      if (b%has_children) then
         d1 = b%daughter(1)
         d2 = b%daughter(2)
         if (d1 > d2) then
            b%daughter(1) = d2
            b%daughter(2) = d1
            b%inverted_decay = .not.b%inverted_decay
            if (b%firstborn /= 0)  b%firstborn = 3 - b%firstborn
         end if
         call branch_canonicalize (t%branch(b%daughter(1)))
         call branch_canonicalize (t%branch(b%daughter(2)))
      end if
    end subroutine branch_canonicalize
  end subroutine tree_canonicalize


end module decay_trees
