! WHIZARD 1.95 Feb 25 2010
! 
! (C) 1999-2008 by 
!     Wolfgang Kilian <wolfgang.kilian@desy.de>
!     Thorsten Ohl <ohl@physik.uni-wuerzburg.de>
!     Juergen Reuter <juergen.reuter@physik.uni-freiburg.de>
!
! WHIZARD is free software; you can redistribute it and/or modify it
! under the terms of the GNU General Public License as published by 
! the Free Software Foundation; either version 2, or (at your option)
! any later version.
!
! WHIZARD is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! This file has been stripped of most comments.  For documentation, refer
! to the source 'whizard.nw'
subroutine whizard_set_input &
     & (process_input, integration_input, simulation_input, &
     &  diagnostics_input, parameter_input, beam_input1, beam_input2)
  use whizard, only: set_input => whizard_set_input
  character(len=*), intent(in) :: process_input, integration_input
  character(len=*), intent(in) :: simulation_input, diagnostics_input
  character(len=*), intent(in) :: parameter_input
  character(len=*), intent(in) :: beam_input1, beam_input2
  call set_input &
     & (process_input, integration_input, simulation_input, &
     &  diagnostics_input, parameter_input, beam_input1, beam_input2)
end subroutine whizard_set_input
