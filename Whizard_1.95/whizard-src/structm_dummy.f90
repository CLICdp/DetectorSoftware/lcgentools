! STRUCTM dummy routine, to be removed when PDFLIB is to be linked.
subroutine structm (xx,qq,upv,dnv,usea,dsea,str,chm,bot,top,glu)
  implicit double precision(a-h, o-z)
  implicit integer(i-n)
  stop " Can't happen: STRUCTM dummy routine called"
end subroutine structm
