! WHIZARD 1.95 Feb 25 2010
! 
! (C) 1999-2008 by 
!     Wolfgang Kilian <wolfgang.kilian@desy.de>
!     Thorsten Ohl <ohl@physik.uni-wuerzburg.de>
!     Juergen Reuter <juergen.reuter@physik.uni-freiburg.de>
!
! WHIZARD is free software; you can redistribute it and/or modify it
! under the terms of the GNU General Public License as published by 
! the Free Software Foundation; either version 2, or (at your option)
! any later version.
!
! WHIZARD is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! This file has been stripped of most comments.  For documentation, refer
! to the source 'whizard.nw'

module density_vectors

  use kinds, only: default !NODEP!
  use diagnostics, only: msg_bug

  implicit none
  private

  type, public :: state_density_t
     private
     integer :: n_states
     integer :: n_col, n_hel, n_flv
     real(kind=default), dimension(:,:,:), pointer :: p
     complex(kind=default), dimension(:,:,:,:,:), pointer :: pp
  end type state_density_t


  public :: create, destroy
  public :: read, write
  public :: read_dimensions_raw, write_dimensions_raw
  public :: state_density_vector_to_matrix, state_density_matrix_to_vector
  public :: nullify
  public :: set
  public :: normalize
  public :: compute_state_density
  public :: norm
  public :: select_state

  interface create
     module procedure state_density_create
  end interface
  interface destroy
     module procedure state_density_destroy
  end interface
  interface write
     module procedure state_density_write_unit
  end interface
  interface write
     module procedure state_density_write_raw
  end interface
  interface read
     module procedure state_density_read_raw
  end interface
  interface nullify
     module procedure state_density_nullify
  end interface
  interface set
     module procedure state_density_set_entry
     module procedure state_density_set_matrix_entry
  end interface
  interface normalize
     module procedure state_density_normalize
  end interface
  interface norm
     module procedure state_density_norm
  end interface
  interface
     subroutine process_scatter (p, rho_in, rho_out, proc_id)
       use kinds, only: default !NODEP!
       character(len=*), intent(in) :: proc_id
       real(kind=default), dimension(0:,:), intent(in) :: p
       complex(kind=default), dimension(:,:,:,:), intent(in) :: rho_in
       complex(kind=default), dimension(:,:,:,:), intent(inout) :: rho_out
     end subroutine process_scatter
  end interface
  interface
     subroutine process_scatter_diagonal (p, rho_in, rho_out, proc_id)
       use kinds, only: default !NODEP!
       character(len=*), intent(in) :: proc_id
       real(kind=default), dimension(0:,:), intent(in) :: p
       real(kind=default), dimension(:,:), intent(in) :: rho_in
       real(kind=default), dimension(:,:), intent(inout) :: rho_out
     end subroutine process_scatter_diagonal
  end interface
  interface
     subroutine process_scatter_colored (p, rho_in, rho_out, rho_col_out, proc_id)
       use kinds, only: default !NODEP!
       character(len=*), intent(in) :: proc_id
       real(kind=default), dimension(0:,:), intent(in) :: p
       complex(kind=default), dimension(:,:,:,:), intent(in) :: rho_in
       complex(kind=default), dimension(:,:,:,:), intent(inout) :: rho_out
       complex(kind=default), dimension(:,:,:,:,:), intent(inout) :: rho_col_out
     end subroutine process_scatter_colored
  end interface
  interface
     subroutine process_scatter_diag_colored (p, rho_in, rho_out, proc_id)
       use kinds, only: default !NODEP!
       character(len=*), intent(in) :: proc_id
       real(kind=default), dimension(0:,:), intent(in) :: p
       real(kind=default), dimension(:,:), intent(in) :: rho_in
       real(kind=default), dimension(:,:,:), intent(inout) :: rho_out
     end subroutine process_scatter_diag_colored
  end interface

contains

  subroutine state_density_create (rho, n_col, n_hel, n_flv, diagonal)
    type(state_density_t), intent(inout) :: rho
    integer, intent(in), optional :: n_col, n_hel, n_flv
    logical, intent(in), optional :: diagonal
    logical :: diag
    diag = .true.;  if (present(diagonal))  diag = diagonal
    if (present(n_col))  rho%n_col = n_col
    if (present(n_hel))  rho%n_hel = n_hel
    if (present(n_flv))  rho%n_flv = n_flv
    if (rho%n_col==0)  rho%n_col = 1
    if (rho%n_hel==0)  rho%n_hel = 1
    if (rho%n_flv==0)  rho%n_flv = 1
    rho%n_states = rho%n_col * rho%n_hel * rho%n_flv
    allocate (rho%p (rho%n_col, rho%n_hel, rho%n_flv))
    rho%p = 0
    if (diag) then
       nullify (rho%pp)
    else
       call density_matrix_allocate (rho)
       rho%pp = 0
    end if
  end subroutine state_density_create

  subroutine density_matrix_allocate (rho)
    type(state_density_t), intent(inout) :: rho
    if (rho%n_col <= 1) then
       allocate (rho%pp (0:0, &
            &            rho%n_hel, rho%n_flv, rho%n_hel, rho%n_flv))
    else
       allocate (rho%pp (0:rho%n_col, &
            &            rho%n_hel, rho%n_flv, rho%n_hel, rho%n_flv))
    end if
  end subroutine density_matrix_allocate

  subroutine state_density_destroy (rho)
    type(state_density_t), intent(inout) :: rho
    !XXX return [Old Intel compiler bug]
    if (associated (rho%pp))  deallocate (rho%pp)
    deallocate (rho%p)
    rho%n_col = 0
    rho%n_hel = 0
    rho%n_flv = 0
    rho%n_states = 0
  end subroutine state_density_destroy

  subroutine state_density_write_unit (u, rho)
    integer, intent(in) :: u
    type(state_density_t), intent(in) :: rho
    integer :: f1, f2, h1, h2
    if (associated (rho%pp)) then
       write (u,*) "State density matrix:"
       do f1 = 1, ubound (rho%pp, dim=3)
          do f2 = 1, ubound (rho%pp, dim=5)
             write (u,*)  " Flavor:", f1, f2
             do h1 = 1, ubound (rho%pp, dim=2)
                do h2 = 1, ubound (rho%pp, dim=4)
                   write (u,*)  " Helicity:", h1, h2
                   write (u,*)  rho%pp(0:,h1,f1,h2,f2)
                end do
             end do
          end do
       end do
    end if
    write (u,*) "State density vector:"
    do f1 = 1, ubound(rho%p, dim=3)
       write (u,*)  " Flavor:", f1
       do h1 = 1, ubound(rho%p, dim=2)
          write (u,*)  " Helicity:", h1
          write (u,*)  rho%p(:,h1,f1)
       end do
    end do
  end subroutine state_density_write_unit

  subroutine state_density_write_raw (u, rho, raw, write_matrix)
    integer, intent(in) :: u
    type(state_density_t), intent(in) :: rho
    logical, intent(in) :: raw, write_matrix
    if (raw) then
       write (u) rho%p
       if (write_matrix) then
          if (.not.associated (rho%pp))  call msg_bug &
               & (" Writing events: density matrix not allocated")
          write (u) rho%pp
       end if
    end if
  end subroutine state_density_write_raw

  subroutine state_density_read_raw (u, rho, raw, read_matrix, iostat)
    integer, intent(in) :: u
    type(state_density_t), intent(inout) :: rho
    logical, intent(in) :: raw, read_matrix
    integer, intent(out), optional :: iostat
    if (raw) then
       read (u, iostat=iostat) rho%p
       if (read_matrix) then
          if (.not.associated (rho%pp))  call density_matrix_allocate (rho)
          read (u, iostat=iostat) rho%pp
       end if
    end if
  end subroutine state_density_read_raw

  subroutine write_dimensions_raw (u, rho)
    integer, intent(in) :: u
    type(state_density_t), intent(in) :: rho
    write (u) rho%n_col, rho%n_hel, rho%n_flv
  end subroutine write_dimensions_raw

  subroutine read_dimensions_raw (u, rho, iostat)
    integer, intent(in) :: u
    type(state_density_t), intent(inout) :: rho
    integer, intent(out), optional :: iostat
    read (u, iostat=iostat) rho%n_col, rho%n_hel, rho%n_flv
  end subroutine read_dimensions_raw

  subroutine state_density_vector_to_matrix (rho)
    type(state_density_t), intent(inout) :: rho
    integer :: f, h
    if (.not.associated (rho%pp))  call density_matrix_allocate (rho)
    rho%pp = 0
    do f = 1, rho%n_flv
       do h = 1, rho%n_hel
          if (rho%n_col <= 1) then
             rho%pp(0,h,f,h,f) = rho%p(1,h,f)
          else
             rho%pp(0, h,f,h,f) = sum (rho%p(:,h,f))
             rho%pp(1:,h,f,h,f) = rho%p(:,h,f)
          end if
       end do
    end do
  end subroutine state_density_vector_to_matrix

  subroutine state_density_matrix_to_vector (rho)
    type(state_density_t), intent(inout) :: rho
    integer :: f, h
    real(kind=default) :: pnorm
    if (.not.associated (rho%pp)) then
       call msg_bug (" State density reduction: Spin matrix not allocated")
    end if
    do f = 1, rho%n_flv
       do h = 1, rho%n_hel
          if (rho%n_col <= 1) then
             rho%p(1,h,f) = rho%pp(0,h,f,h,f)
          else
             pnorm = sum (rho%pp(1:,h,f,h,f))
             if (pnorm > 0) then
                rho%p(1:,h,f) = rho%pp(0,h,f,h,f) &
                     & * rho%pp(1:,h,f,h,f) / pnorm
             else
                rho%p(:,h,f) = 0
             end if
          end if
       end do
    end do
  end subroutine state_density_matrix_to_vector

  subroutine state_density_nullify (rho)
    type(state_density_t), intent(inout) :: rho
    rho%p = 0
    if (associated (rho%pp))  rho%pp = 0
  end subroutine state_density_nullify

  subroutine state_density_set_entry (rho, col, hel, flv, val)
    type(state_density_t), intent(inout) :: rho
    integer, intent(in) :: col, hel, flv
    real(kind=default), intent(in) :: val
    rho%p(col,hel,flv) = val
  end subroutine state_density_set_entry

  subroutine state_density_set_matrix_entry &
       & (rho, col, hel1, flv1, hel2, flv2, val)
    type(state_density_t), intent(inout) :: rho
    integer, intent(in) :: col, hel1, hel2, flv1, flv2
    complex(kind=default), intent(in) :: val
    rho%pp(col,hel1,flv1,hel2,flv2) = val
  end subroutine state_density_set_matrix_entry

  subroutine state_density_normalize (rho)
    type(state_density_t), intent(inout) :: rho
    real(kind=default) :: s
    if (any(rho%p/=0)) then
       s = sum(rho%p)
       rho%p = rho%p / s
       if (associated (rho%pp))  rho%pp = rho%pp / s
    end if
  end subroutine state_density_normalize

  subroutine compute_state_density &
       & (p, rho_in, rho_out, norm, process_id, diagonal, colored, ok)
    real(kind=default), dimension(0:,:), intent(in) :: p
    type(state_density_t), intent(in) :: rho_in
    type(state_density_t), intent(inout) :: rho_out
    real(default), intent(out) :: norm
    character(len=*), intent(in) :: process_id
    logical, intent(in) :: diagonal, colored
    logical, intent(out) :: ok
    real(default), parameter :: EPS = 100 * epsilon (1._default)
    if (diagonal) then
       if (colored) then
          call process_scatter_diag_colored &
               & (p, rho_in%p(1,:,:), rho_out%p, process_id)
       else
          call process_scatter_diagonal &
               & (p, rho_in%p(1,:,:), rho_out%p(1,:,:), process_id)
       end if
    else
       if (.not.associated (rho_in%pp))  call msg_bug &
               & (" In-state density matrix not associated in scattering")
       if (.not.associated (rho_out%pp)) call msg_bug &
               & (" Out-state density matrix not associated in scattering")
       if (colored) then
          call process_scatter_colored &
               & (p, rho_in%pp(0,:,:,:,:), rho_out%pp(0,:,:,:,:), &
               &  rho_out%pp(1:,:,:,:,:), process_id)
       else
          call process_scatter &
               & (p, rho_in%pp(0,:,:,:,:), rho_out%pp(0,:,:,:,:), process_id)
       end if
       call state_density_matrix_to_vector (rho_out)
    end if
    norm = state_density_norm (rho_out)
    if (norm /= 0) then
       rho_out%p = rho_out%p / norm
       where (rho_out%p > 0)
          where (rho_out%p < EPS)  rho_out%p = 0
       elsewhere (rho_out%p < 0)
          where (rho_out%p > -EPS) rho_out%p = 0
       end where
       ok = all (rho_out%p >= 0)
    end if
  end subroutine compute_state_density

  function state_density_norm (rho) result (r)
    type(state_density_t), intent(in) :: rho
    real(kind=default) :: r
    r = sum(rho%p)
  end function state_density_norm

  subroutine select_state (rho, r, col, hel, flv)
    type(state_density_t), intent(in) :: rho
    real(kind=default), intent(in) :: r
    integer, intent(out) :: col, hel, flv
    real(kind=default) :: rho_sum, rho_comp
    rho_comp = r
    rho_sum = 0
    LOOP_FLV: do flv=1, rho%n_flv
       LOOP_HEL: do hel=1, rho%n_hel
          LOOP_COL: do col=1, rho%n_col
             rho_sum = rho_sum + rho%p(col,hel,flv)
             if (rho_comp < rho_sum)  exit LOOP_FLV
          end do LOOP_COL
       end do LOOP_HEL
    end do LOOP_FLV
  end subroutine select_state


end module density_vectors
