! WHIZARD 1.95 Feb 25 2010
! 
! (C) 1999-2008 by 
!     Wolfgang Kilian <wolfgang.kilian@desy.de>
!     Thorsten Ohl <ohl@physik.uni-wuerzburg.de>
!     Juergen Reuter <juergen.reuter@physik.uni-freiburg.de>
!
! WHIZARD is free software; you can redistribute it and/or modify it
! under the terms of the GNU General Public License as published by 
! the Free Software Foundation; either version 2, or (at your option)
! any later version.
!
! WHIZARD is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! This file has been stripped of most comments.  For documentation, refer
! to the source 'whizard.nw'

program main

  use limits, only: CMDLINE_ARG_LEN
  use diagnostics, only: msg_message
  use unix_args, only: cmd_option_define, cmd_line_read !NODEP!
  use unix_args, only: cmd_line_destroy !NODEP!
  use whizard
!  use showers

  external :: usage

  ! The option variables
  logical :: opt_help = .false.
  logical :: opt_version = .false.
  logical :: opt_list = .false.
  logical :: opt_directory_set = .false.
  character(len=CMDLINE_ARG_LEN) :: opt_directory = ""
  logical :: opt_filename_set = .false.
  character(len=CMDLINE_ARG_LEN) :: opt_filename = ""
  logical :: opt_input_set = .false.
  character(len=CMDLINE_ARG_LEN) :: opt_input = ""
  logical :: opt_skip_input = .false.
  logical :: opt_parameters_set = .false.
  character(len=CMDLINE_ARG_LEN) :: opt_process = ""
  character(len=CMDLINE_ARG_LEN) :: opt_integration = ""
  character(len=CMDLINE_ARG_LEN) :: opt_simulation = ""
  character(len=CMDLINE_ARG_LEN) :: opt_diagnostics = ""
  character(len=CMDLINE_ARG_LEN) :: opt_parameters = ""
  character(len=CMDLINE_ARG_LEN) :: opt_beam1 = ""
  character(len=CMDLINE_ARG_LEN) :: opt_beam2 = ""

!  call shower_test (10, 0); stop

  ! Define the command-line options
  call cmd_option_define ("-h", opt_help, alt_key="--help")
  call cmd_option_define ("-V", opt_version, alt_key="--version")
  call cmd_option_define ("-l", opt_list, alt_key="--list_processes")
  call cmd_option_define ("-d", opt_directory_set, opt_directory, &
       &                  alt_key="--directory")
  call cmd_option_define ("-f", opt_filename_set, opt_filename, &
       &                  alt_key="--filename")
  call cmd_option_define ("-i", opt_input_set, opt_input, &
       &                  alt_key="--input_file")
  call cmd_option_define ("-n", opt_skip_input, &
       &                  alt_key="--no_input_file")
  call cmd_option_define ("-p", opt_parameters_set, opt_process, &
       &                  alt_key="--process_input")
  call cmd_option_define ("-I", opt_parameters_set, opt_integration, &
       &                  alt_key="--integration_input")
  call cmd_option_define ("-S", opt_parameters_set, opt_simulation, &
       &                  alt_key="--simulation_input")
  call cmd_option_define ("-D", opt_parameters_set, opt_diagnostics, &
       &                  alt_key="--diagnostics_input")
  call cmd_option_define ("-P", opt_parameters_set, opt_parameters, &
       &                  alt_key="--parameter_input")
  call cmd_option_define ("-B1", opt_parameters_set, opt_beam1, &
       &                  alt_key="--beam_input1")
  call cmd_option_define ("-B2", opt_parameters_set, opt_beam2, &
       &                  alt_key="--beam_input2")

  ! Read from the command line (if supported)
  call cmd_line_read (usage)

  ! Execute options
  if (opt_help) then
     call usage
     stop
  end if
  if (opt_version) then
     print "(A)", "WHIZARD 1.95 (Feb 25 2010)"
     stop
  end if
  if (opt_list) then
     call msg_message (" WHIZARD 1.95 (Feb 25 2010)")
     call processes_list (6)
     stop
  end if
  if (opt_directory_set)  call whizard_set_directory (opt_directory)
  if (opt_filename_set)  call whizard_set_filename (opt_filename)
  if (opt_input_set)  call whizard_set_input_file (opt_input)
  if (opt_skip_input)  call whizard_enable_input_file (.false.)
  if (opt_parameters_set) then
     call whizard_set_input &
          & (opt_process, opt_integration, opt_simulation, opt_diagnostics, &
          &  opt_parameters, opt_beam1, opt_beam2)
  end if
  call cmd_line_destroy

  ! Read from input file
  call whizard_read_input

  ! Initialize and integrate (read from input file)
  call whizard_integrate

  ! Generate events (if specified in input file)
  call whizard_generate

  ! Finalize
  call whizard_end

end program main

subroutine usage
  print 1, "Usage: whizard [options]"
  print 1, "Options:"
  print 1, "  -h       --help            display this message and exit"
  print 1, "  -V       --version         show version information and exit"
  print 1, "  -l       --list            list all available processes and exit"
  print 1, "  -d DIR   --directory       set working directory"
  print 1, "  -f NAME  --filename        set default name of input/output files (w/o ext.)"
  print 1, "  -i NAME  --input_file      set name of main input file (w/o extension)"
  print 1, "  -n       --no_input_file   do not read from input file"
  print 1, "  -p STRING  --process_input      set process input parameters"
  print 1, "  -I STRING  --integration_input  set integration input parameters"
  print 1, "  -S STRING  --simulation_input   set simulation input parameters"
  print 1, "  -D STRING  --diagnostics_input  set diagnostics input parameters"
  print 1, "  -P STRING  --parameter_input    set physics input parameters"
  print 1, "  -B1 STRING  --beam_input1       set beam input parameters"
  print 1, "  -B2 STRING  --beam_input2       set beam input parameters"
1 format (A)
end subroutine usage
