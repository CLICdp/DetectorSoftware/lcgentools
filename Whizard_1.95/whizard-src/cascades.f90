! WHIZARD 1.95 Feb 25 2010
! 
! (C) 1999-2008 by 
!     Wolfgang Kilian <wolfgang.kilian@desy.de>
!     Thorsten Ohl <ohl@physik.uni-wuerzburg.de>
!     Juergen Reuter <juergen.reuter@physik.uni-freiburg.de>
!
! WHIZARD is free software; you can redistribute it and/or modify it
! under the terms of the GNU General Public License as published by 
! the Free Software Foundation; either version 2, or (at your option)
! any later version.
!
! WHIZARD is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! This file has been stripped of most comments.  For documentation, refer
! to the source 'whizard.nw'

module cascades

  use kinds, only: default !NODEP!
  use file_utils !NODEP!
  use limits, only: BUFFER_SIZE, MAX_CASCADES, FILENAME_LEN
  use limits, only: TC
  use diagnostics, only: msg_buffer, msg_message, msg_debug
  use diagnostics, only: msg_warning, msg_fatal, msg_bug
  use files, only: file_exists_else_default, concat
  use parser
  use particles
  use events, only: write_process, write_process_tex_form
  use decay_trees, only: decay_level
  use parameters, only: parameter_set !NODEP!
  implicit none
  private

  type, public :: vertex
     integer, dimension(3) :: code
  end type vertex

  type, private :: node_t 
     integer(kind=TC) :: dummy, bincode
     integer :: code, index
     real(kind=default) :: minimal_mass, real_mass, effective_mass
     logical :: resonant, decays, t_channel, needs_cut, is_vector
     logical :: initial, final 
     integer :: mapping
     type(node_t), pointer :: daughter1, daughter2, mother
  end type node_t

  type, public :: cascade
     private
     logical :: active, subdominant, deleted, final
     integer :: depth, hash
     integer :: multiplicity, n_t_channel, n_resonances, n_log_enhanced
     integer :: grove
     integer(kind=TC), dimension(:), pointer :: tree
     integer, dimension(:), pointer :: code, mapping
     logical, dimension(:), pointer :: resonant, log_enhanced, t_channel
     type(node_t), pointer :: node
  end type cascade

  type, public :: phase_space_switches
     integer :: off_shell_lines
     integer :: extra_off_shell_lines
     integer :: splitting_depth
     integer :: exchange_lines
     logical :: show_deleted_channels
     logical :: single_off_shell_decays
     logical :: double_off_shell_decays
     logical :: single_off_shell_branchings
     logical :: double_off_shell_branchings
     logical :: massive_fsr
     real(kind=default) :: threshold_mass
     real(kind=default) :: threshold_mass_t
  end type phase_space_switches

  type(vertex), parameter, public :: NO_VERTEX = vertex((/0,0,0/))
  integer, parameter, public :: &
       & NO_MAPPING = 0, S_CHANNEL = 1, T_CHANNEL =  2, U_CHANNEL = -2, &
       & RADIATION = 3, COLLINEAR = 4, INFRARED = 5

  public :: read
  public :: write
!  public :: scan
  public :: setup_and_read_vertices
  public :: clear_vertices
  public :: write_vertices
  public :: test_vertices
  public :: create, destroy
  public :: cascade_set_write
  public :: cascade_set_init
  public :: cascade_set_clear
  public :: cascade_set_add
  public :: cascade_set_generate
  public :: cascade_set_keystone
  public :: cascade_set_select
  public :: cascade_set_validate
  public :: cascade_set_groves
  public :: phase_space_channels_write
  public :: make_phase_space_file

  type(vertex), dimension(:), allocatable, save :: vertex_set
  integer, save :: n_vertices
  type(cascade), dimension(MAX_CASCADES), save :: cascade_set
  integer, save :: n_cascades, n_cascades_s, n_cascades_t
  integer, save :: n_groves
  logical, save :: single_off_shell_decays
  logical, save :: double_off_shell_decays
  logical, save :: single_off_shell_branchings
  logical, save :: double_off_shell_branchings
  logical, save :: massive_FSR
  integer, save :: splitting_depth, exchange_lines

  interface read
     module procedure vertex_read_unit
  end interface
  interface write
     module procedure vertex_write_unit
  end interface
!  interface scan
!     module procedure vertices_scan_file
!  end interface
  interface write_vertices
     module procedure write_vertices_unit
  end interface
  interface create
     module procedure cascade_create
  end interface
  interface clear
     module procedure cascade_clear
  end interface
  interface destroy
     module procedure cascade_destroy
  end interface

  interface write
     module procedure cascade_write_unit
  end interface
  interface cascade_set_write
     module procedure cascade_set_write_unit
  end interface
  interface cascade_set_add
     module procedure cascade_set_add_single, cascade_set_add_multi
  end interface
  interface cascade_set_generate
     module procedure cascade_set_generate_s
     module procedure cascade_set_generate_t_single
     module procedure cascade_set_generate_t_multi
  end interface
  interface cascade_set_keystone
     module procedure cascade_set_keystone_single, cascade_set_keystone_multi
  end interface
  interface phase_space_channels_write
     module procedure phase_space_channels_write_name
     module procedure phase_space_channels_write_unit
  end interface

contains

  subroutine vertex_read_unit (u, v, ok, iostat)
    integer, intent(in) :: u
    type(vertex), intent(out) :: v
    logical, intent(out), optional :: ok
    integer, intent(out), optional :: iostat
    character(len=BUFFER_SIZE) :: string
    integer :: i, code, ios
    if (present(ok)) ok = .true.
    if (present (iostat))  iostat = 0
    v = NO_VERTEX
    call find_string (u, "vertex", ok=ok)
    if (present(ok)) then
       if (.not.ok) then
          if (present (iostat))  iostat = -1
          return
       end if
    end if
    READ_PARTICLES: do i=1,4
       string = get_string (u, iostat=ios)
       select case (ios)
       case (0)
          code = particle_code (trim(string))
          if (code /= UNKNOWN) then
             if (i < 4) then
                v%code(i) = code
             else
                ok = .false.
             end if
          else
             call put_back_string (string)
             if (i < 4)  ok = .false.
             exit READ_PARTICLES
          end if
       case (:-1)
          if (i < 4)  ok = .false.
          exit READ_PARTICLES 
       case default
          call msg_fatal (" Reading model file: I/O error")
       end select
    end do READ_PARTICLES
  end subroutine vertex_read_unit

  subroutine vertex_write_unit (u, v)
    integer, intent(in) :: u
    type(vertex), intent(in) :: v
    write(u, '(4(1x,A))') "vertex    ", &
         & particle_name(v%code(1)), &
         & particle_name(v%code(2)), &
         & particle_name(v%code(3))
  end subroutine vertex_write_unit
     
  function vertex_string (v) result (string)
    type(vertex), intent(in) :: v
    character(len=BUFFER_SIZE) :: string
    write (string, '(3(1x,A))') &
         & trim (particle_name(v%code(1))), &
         & trim (particle_name(v%code(2))), &
         & trim (particle_name(v%code(3)))
  end function vertex_string

  subroutine vertex_check (v, ok)
    type(vertex), intent(in) :: v
    logical, intent(out) :: ok
    integer, dimension(3) ::  code, multiplicity, charge
    integer :: i
    character(len=BUFFER_SIZE) :: vstr
    do i=1,3
       multiplicity(i) = particle_spin_type (v%code(i))-1
       charge(i) = particle_3charge (v%code(i))
    end do
1   format (1x,A,1x,A)
    vstr = vertex_string (v)
    if (any((v%code)==UNDEFINED)) then
       write (msg_buffer, 1)  "Vertex with unknown particle:  ", trim (vstr)
       call msg_debug
       ok = .false.
    else if (mod(sum(multiplicity),2)/=0) then
       write (msg_buffer, 1)  "Vertex with odd fermion number:", trim (vstr)
       call msg_warning
       ok = .false.
    else if (sum(charge)/=0) then
       write (msg_buffer, 1)  "Vertex with nonzero charge:    ", trim (vstr)
       call msg_warning
       ok = .false.
    else
       code = v%code
       call lookup_vertex (code(1), code(2), code(3))
       if (code(3) == UNDEFINED) then
          ok = .true.
       else
          write (msg_buffer, 1)  "Vertex multiply defined:", trim (vstr)
          call msg_debug
          ok = .false.
       end if
    end if
  end subroutine vertex_check

  subroutine vertices_scan_file (file, count)
    character(len=*), intent(in) :: file
    integer, intent(out), optional :: count
    integer :: u, iostat
    type(vertex) :: v
    logical :: ok
    u = free_unit()
    open (unit=u, file=file, status='old', action='read')
    ok = .true.
    if (present(count)) then
       Count = 0
    else
       call msg_message (" Vertices in "//trim(file)//":")
    end if
    SCAN: do
       call read (u, v, ok, iostat)
       if (iostat /= 0)  exit SCAN
       if (.not.ok)  cycle SCAN
       if (present(count)) then
          count = count + 1
       else
          call write (6, v)
       end if
    end do SCAN
    close (u)
  end subroutine vertices_scan_file

  subroutine vertex_set_read_file (file)
    character(len=*), intent(in) :: file
    integer :: u, i, iostat
    type(vertex) :: v
    logical :: ok
    u = free_unit()
    open (unit=u, file=file, status='old', action='read')
    SCAN: do i=1, size(vertex_set)
       TRY: do
          call read (u, v, ok, iostat)
          if (iostat /= 0)  exit SCAN
          if (.not.ok)  cycle TRY
          call vertex_check (v, ok)
          if (ok) exit TRY
       end do TRY
       vertex_set(i) = v
       n_vertices = i
    end do SCAN
    close (u)
  end subroutine vertex_set_read_file

  subroutine setup_and_read_vertices (prefix, filename)
    character(len=*), intent(in) :: prefix, filename
    character(len=FILENAME_LEN) :: file
    integer :: count
    file = file_exists_else_default (prefix, filename, "mdl")
    if (len_trim (file) == 0) call msg_fatal &
         & (" Model configuration file not found.")
    call msg_message (" Reading vertices from file "//trim(file)//" ...")
    call vertices_scan_file (trim(file), count)
    if (count==0)  call msg_fatal (" Model file: No trilinear vertices found.")
    write (msg_buffer, "(1x,A,2x,I7,1x,A)") &
         & "Model file:", count, "trilinear vertices found."
    call msg_message
    allocate (vertex_set(count))
    n_vertices = 0
    call vertex_set_read_file (trim(file))
    write (msg_buffer, "(1x,A,2x,I7,1x,A)") &
         & "Model file:", n_vertices, "vertices usable for phase space setup."
    call msg_message
  end subroutine setup_and_read_vertices

  subroutine clear_vertices
    deallocate (vertex_set)
    n_vertices = 0
  end subroutine clear_vertices
  subroutine write_vertices_unit (u)
    integer, intent(in) :: u
    integer :: i
    do i=1, n_vertices
       call write (u, vertex_set(i))
    end do
  end subroutine write_vertices_unit

  subroutine lookup_vertex (code1, code2, code3, point)
    integer, intent(in) :: code1, code2
    integer, intent(inout) :: code3
    integer, intent(inout), optional :: point
    integer :: ptr, code
    code = UNDEFINED
    ptr = 1
    if (present (point))  ptr = point
    SCAN: do
       if (ptr > n_vertices) then
          code3 = UNDEFINED
          exit SCAN
       end if
       if (match(1,2).or.match(2,1)) then
          code = vertex_set(ptr)%code(3)
       else if (match(1,3).or.match(3,1)) then
          code = vertex_set(ptr)%code(2)
       else if (match(2,3).or.match(3,2)) then
          code = vertex_set(ptr)%code(1)
       end if
       ptr = ptr+1
       if (code /= UNDEFINED) then
          if (code3 == UNDEFINED) then
             code3 = code
             exit SCAN
          else if (code == code3) then
             exit SCAN
          else
             code = UNDEFINED
          end if
       end if
    end do SCAN
    if (present (point))  point = ptr
  contains
    function match(i,j)
      integer, intent(in) :: i,j
      logical :: match
      match = (code1 == vertex_set(ptr)%code(i)) &
           & .and. (code2 == vertex_set(ptr)%code(j))
    end function match
  end subroutine lookup_vertex

  subroutine test_vertices (code1, code2)
    integer, intent(in) :: code1, code2
    integer :: code3
    write(*,'(3(1x,A),A)') "Particles matching ", &
         & trim(particle_name(code1)), trim(particle_name(code2)), ":"
    SCAN: do
       code3 = UNDEFINED
       call lookup_vertex (code1, code2, code3)
       if (code3/=UNDEFINED) then
          print *, particle_name(code3)
       else
          exit SCAN
       end if
    end do SCAN
  end subroutine test_vertices

  subroutine node_create (node)
    type(node_t), pointer :: node
    node%bincode = 0
    node%code = UNDEFINED
    node%index = 0
    node%minimal_mass = 0
    node%real_mass = 0
    node%effective_mass = 0
    node%resonant = .false.
    node%decays = .false.
    node%t_channel = .false.
    node%is_vector = .false.
    node%initial = .false.
    node%final = .false.
    node%needs_cut = .false.
    node%mapping = NO_MAPPING
    nullify(node%daughter1)
    nullify(node%daughter2)
    nullify(node%mother)
  end subroutine node_create

  subroutine cascade_create (cs, depth)
    type(cascade), intent(inout) :: cs
    integer, intent(in) :: depth
    allocate(cs%node)
    allocate(cs%tree(depth))
    allocate(cs%code(depth))
    allocate(cs%mapping(depth))
    allocate(cs%resonant(depth))
    allocate(cs%log_enhanced(depth))
    allocate(cs%t_channel(depth))
    call clear (cs)
  end subroutine cascade_create

  subroutine cascade_clear (cs)
    type(cascade), intent(inout) :: cs
    cs%active = .false.
    cs%subdominant = .false.
    cs%deleted = .false.
    cs%final = .false.
    cs%depth = 0
    cs%hash = 0
    cs%multiplicity = 0
    cs%n_t_channel = -1
    cs%n_resonances = 0
    cs%n_log_enhanced = 0
    cs%grove = 0
    cs%tree = 0
    cs%code = UNDEFINED
    cs%mapping = NO_MAPPING
    cs%resonant = .false.
    cs%log_enhanced = .false.
    cs%t_channel = .false.
    call node_create (cs%node)
  end subroutine cascade_clear

  subroutine cascade_destroy (cs)
    type(cascade), intent(inout) :: cs
    deallocate(cs%node)
    deallocate(cs%tree)
    deallocate(cs%code)
    deallocate(cs%mapping)
    deallocate(cs%resonant)
    deallocate(cs%log_enhanced)
    deallocate(cs%t_channel)
  end subroutine cascade_destroy

  subroutine cascade_write_unit (u, cs, final, short, init, graph)
    integer, intent(in) :: u
    type(cascade), intent(in) :: cs
    logical, intent(in), optional :: final, short, init, graph
    logical :: short_form, final_only, graph_form
    integer(kind=TC), dimension(cs%depth) :: tree
    integer :: i, d, count
    if (present(init)) then 
       count = 0
       return
    end if
1   format(1x,A,11(1x,I3))
2   format(2x,A,1x,I3,1x,A,1x,A)
    short_form = .false.;  if (present(short)) short_form = short
    final_only = .false.;  if (present(final)) final_only = final
    graph_form = .false.;  if (present(graph)) graph_form = graph
    if (cs%active) then
       if (short_form) then
          if (cs%deleted) then
             call start_comment(u)
          else
             count = count + 1
          end if
          tree = 0
          if (final_only) then
             d = (cs%depth-3)/2
             tree = reduced(cs%tree)
          else
             d = cs%depth
             tree = cs%tree
          end if
          write(u,1,advance='no') 'tree', tree(:d)
          write(u,'(5x)',advance='no')
          call start_comment(u)
          if (cs%deleted) then
             write (u, '(6x)', advance='no')
          else
             write(u,'(1x,I4,A,1x)', advance='no') count, "*"
          end if
          do i=1, cs%depth
             if (any(tree(:d)==cs%tree(i))) write(u,'(1x,A)',advance='no') &
                  & trim(particle_name(cs%code(i)))
          end do
          write(u,*)
          do i=1, cs%depth
             select case (cs%mapping(i))
             case (NO_MAPPING)
             case (S_CHANNEL)
                if (cs%deleted) call start_comment (u)
                write(u,2) ' map', cs%tree(i), 's-channel ', &
                     & trim(particle_name(cs%code(i)))
             case (T_CHANNEL)
                if (cs%deleted) call start_comment (u)
                write(u,2) ' map', cs%tree(i), 't-channel ', &
                     & trim(particle_name(cs%code(i)))
             case (U_CHANNEL)
                if (cs%deleted) call start_comment (u)
                write(u,2) ' map', cs%tree(i), 'u-channel ', &
                     & trim(particle_name(cs%code(i)))
             case (RADIATION)
                if (cs%deleted) call start_comment (u)
                write(u,2) ' map', cs%tree(i), 'radiation ', &
                     & trim(particle_name(cs%code(i)))
             case (COLLINEAR)
                if (cs%deleted) call start_comment (u)
                write(u,2) ' map', cs%tree(i), 'collinear ', &
                     & trim(particle_name(cs%code(i)))
             case (INFRARED)
                if (cs%deleted) call start_comment (u)
                write(u,2) ' map', cs%tree(i), 'infrared  ', &
                     & trim(particle_name(cs%code(i)))
             case default
                call msg_bug (" Impossible mapping mode encountered")
             end select
          end do
       else if (graph_form) then
          call cascade_write_fmfgraph_unit (u, cs)
       else
          write(u,*) 'Cascade #', cs%node%index
          write(u,*) '  Grove:       #', cs%grove
          write(u,*) '  Depth:        ', cs%depth
          write(u,*) '  Multiplicity: ', cs%multiplicity
          write(u,*) '  Resonances:   ', cs%n_resonances
          write(u,*) '  Log-enhanced: ', cs%n_log_enhanced
          write(u,*) '  T-channel:    ', cs%n_t_channel
          write(u,*) '  Tree:         ', cs%tree
          if (final_only) then
             write(u,*) '  Reduced tree: ', reduced(cs%tree)
          end if
          if (cs%node%needs_cut) &
               & write(u,*) '  Zero-mass singularity'
          call node_write_unit (u, cs%node)
       end if
    else if (.not.short_form .and. .not.graph_form) then
       write(u,*) 'Cascade #', cs%node%index, ': deleted'
    end if
  contains
    function reduced (array)
      integer(kind=TC), dimension(:), intent(in) :: array
      integer(kind=TC), dimension(max((size(array)-3)/2, 1)) :: reduced
      integer :: i, j
      j = 1
      do i=1, size(array)
         if (decay_level(array(i)) > 1) then
            reduced(j) = array(i)
            j = j+1
         end if
      end do
    end function reduced
  end subroutine cascade_write_unit

  recursive subroutine node_write_unit (u, node, write_single)
    integer, intent(in) :: u
    type(node_t), pointer :: daughter1, daughter2, mother
    logical, intent(in), optional :: write_single
    type(node_t), pointer :: node
    logical :: single
    single = .true.;  if (present (write_single))  single = write_single
    if (single .or. node%decays)  call entry_write_unit (u, node)
    if (node%decays) then
       write (u, '(1x,A,I1,A)', advance='no') "[", node%mapping, "]"
       daughter1 => node%daughter1
       daughter2 => node%daughter2
       write(u, '(1x,A)', advance='no') '==>'
       call entry_write_unit (u, daughter1)
       call entry_write_unit (u, daughter2)
       write (u,*)
       call node_write_unit (u, daughter1, write_single=.false.)
       call node_write_unit (u, daughter2, write_single=.false.)
       if (associated (node%mother)) then
          call entry_write_unit (u, node)
          mother => node%mother
          write(u, '(1x,A)', advance='no') '<=='
          call entry_write_unit (u, mother)
          write (u,*)
          call node_write_unit (u, mother, write_single=.false.)
       end if
    end if
    if (single .and. .not.node%decays)  write (u,*)
  contains
    subroutine entry_write_unit (u, node)
      integer, intent(in) :: u
      type(node_t), pointer :: node
      write(u, 1, advance='no') &
           & trim(particle_name(node%code)), '(', node%index, &
           & ',', node%effective_mass, ')'
      if (node%resonant) then
         write(u, '(A)', advance='no')  "*"
      end if
      if (node%initial) then
         write(u, '(A)', advance='no') "+"
      else if (.not.node%decays) then
         write(u, '(A)', advance='no') "x"
      else if (node%t_channel) then
         write(u, '(A)', advance='no') "t"
      end if
1     format(5x,A,A,I4,A,1x,1PE8.2,A)
    end subroutine entry_write_unit
  end subroutine node_write_unit

  subroutine start_comment (u)
    integer, intent(in) :: u
    write(u, '(1x,A)', advance='no') '!'
  end subroutine start_comment

  subroutine cascade_set_write_unit (u, final, short, sort, graph)
    integer, intent(in) :: u
    logical, intent(in), optional :: final, short, sort, graph
    integer :: i, grove, n_r, n_t, n_l, mult, fmfcount
    logical :: sorted, final_only, short_form, graph_form, deleted
    final_only = .false.;  if (present(final)) final_only = final
    short_form = .false.;  if (present(short)) short_form = short
    sorted = .false.;  if (present(sort)) sorted = sort
    graph_form = .false.;  if (present(graph)) graph_form = graph
    call write (u, cascade_set(1), init=.true.)
    fmfcount = 0
    SCAN_GROVES: do grove=0, n_groves
       if (short_form .and. sorted .and. &
            & .not.(grove==0 .and. final_only)) then
          call find_cascade_in_grove (grove, i, deleted, n_r, n_l, n_t, mult)
          if (i /= 0) then
             if (deleted) call start_comment (u)
             call start_comment (u)
             write(u,'(1x,A,I2,A)', advance='no') 'Multiplicity =', mult, ","
             select case (n_r)
             case (0);  write(u,'(1x,A)', advance='no') 'no resonances, '
             case (1);  write(u,'(1x,A)', advance='no') ' 1 resonance,  '
             case default
                write(u,'(1x,I2,1x,A)', advance='no') n_r, 'resonances, '
             end select
             write (u, '(1x,I2,1x,A)', advance='no') n_l, 'logs, '
             select case (n_t)
             case (0);  write(u,'(1x,A)') 's-channel graph'
             case (1);  write(u,'(1x,A)') ' 1 t-channel line'
             case default
                write(u,'(1x,I2,1x,A)') n_t, 't-channel lines'
             end select
             if (deleted) call start_comment (u)
             write(u,'(1x,A,I3)') 'grove #', grove
          end if
       else if (graph_form .and. .not.(grove==0 .and. final_only)) then
          call find_cascade_in_grove (grove, i, deleted, n_r, n_l, n_t, mult)
          if (i /= 0 .and. .not.deleted) then
             write (u, *)
             write (u, '(A)') "\vspace{20pt}"
             write (u, '(A)') "\begin{tabular}{l}"
             write (u, '(A,I5,A)') &
               & "\fbox{\bf Grove \boldmath$", grove, "$} \\[10pt]"
             write (u, '(A,I1,A)') "Multiplicity: ", mult, "\\"
             write (u, '(A,I1,A)') "Resonances:   ", n_r, "\\"
             write (u, '(A,I1,A)') "Log-enhanced: ", n_l, "\\"
             write (u, '(A,I1,A)') "t-channel:    ", n_t, ""
             write (u, '(A)') "\end{tabular}"
          end if
       end if
       do i=1, n_cascades
          if ((cascade_set(i)%grove==grove .or. .not.sorted) .and. &
               & (cascade_set(i)%final .or..not.final_only)) then
             call write (u, cascade_set(i), final, short, graph=graph)
             fmfcount = fmfcount + 1
             if (fmfcount >= 250) then
                if (graph_form)  write (u, '(A)') "\clearpage"
                fmfcount = 0
             end if
          end if
       end do
       if (.not.sorted)  exit SCAN_GROVES
       if (.not.graph_form)  write(u,*)
    end do SCAN_GROVES
    if (present(final) .and. .not.graph_form) then
       if (final) then
          write (u,*) "! Number of channels total:   ", &
               & count (cascade_set%active .and. cascade_set%final)
          write (u,*) "! Number of channels kept:    ", &
               & count (cascade_set%active .and. cascade_set%final .and. &
               &        .not. cascade_set%deleted)
          write (u,*) "! Number of dominant channels:", &
               & count (cascade_set%active .and. cascade_set%final .and. &
               &        .not. cascade_set%subdominant)
       else
          write (u,*) "! Number of cascades shown:", &
               & count (cascade_set%active)
       end if
    end if
  contains
    subroutine find_cascade_in_grove (grove, c, deleted, n_r, n_l, n_t, mult)
      integer, intent(in) :: grove
      integer, intent(out) :: c, n_r, n_l, n_t, mult
      logical, intent(out) :: deleted
      integer :: i
      c = 0
      n_r = 0;  n_t = 0;  mult = 0
      FIND_FIRST_MATCH: do i=1, n_cascades
         if (cascade_set(i)%grove==grove) then
            c = i
            deleted = cascade_set(i)%deleted
            n_r = cascade_set(i)%n_resonances
            n_l = cascade_set(i)%n_log_enhanced
            n_t = cascade_set(i)%n_t_channel
            mult = cascade_set(i)%multiplicity
            exit FIND_FIRST_MATCH
         end if
      end do FIND_FIRST_MATCH
    end subroutine find_cascade_in_grove
  end subroutine cascade_set_write_unit

  subroutine cascade_set_init (code, par, threshold_mass)
    integer, dimension(:,:), intent(in) :: code
    type(parameter_set), intent(in) :: par
    real(kind=default), intent(in) :: threshold_mass
    integer :: i, f, n_out, n_flv
    n_cascades = 0
    n_cascades_s = 0
    n_cascades_t = 0
    n_groves = 0
    call cascade_set_clear
    n_out = size(code, dim=1)
    n_flv = size(code, dim=2)
    do i=1, n_out
       do f=1, n_flv
          if (all (code(i,1:f-1) /= code(i,f))) then
             call cascade_set_add &
                  & (ibset(0_TC,i-1), code(i,f), par, threshold_mass, &
                  &  initial=.false.)
          end if
       end do
    end do
  end subroutine cascade_set_init

  subroutine cascade_set_clear
    integer :: i
    cascade_set%active = .false.
    cascade_set%subdominant = .false.
    cascade_set%deleted = .false.
    cascade_set%final = .false.
    cascade_set%depth = 0
    cascade_set%multiplicity = 0
    cascade_set%n_t_channel = -1
    cascade_set%n_resonances = 0
    cascade_set%n_log_enhanced = 0
    cascade_set%grove = 0
    do i=1, n_cascades
       call destroy (cascade_set(i))
    end do
    n_cascades = 0
    n_cascades_s = 0
    n_cascades_t = 0
    n_groves = 0
  end subroutine cascade_set_clear

  subroutine cascade_set_add_multi &
       & (bincode, code, par, threshold_mass, initial, index)
    integer(kind=TC), intent(in) :: bincode
    integer, dimension(:), intent(in) :: code
    type(parameter_set), intent(in) :: par
    real(kind=default), intent(in) :: threshold_mass
    logical, intent(in) :: initial
    integer, dimension(:), intent(out), optional :: index
    integer :: i
    do i = 1, size (code)
       call cascade_set_add_single &
            & (bincode, code(i), par, threshold_mass, initial, index(i))
    end do
  end subroutine cascade_set_add_multi

  subroutine cascade_set_add_single &
       & (bincode, code, par, threshold_mass, initial, index)
    integer(kind=TC), intent(in) :: bincode
    integer, intent(in) :: code
    type(parameter_set), intent(in) :: par
    real(kind=default), intent(in) :: threshold_mass
    logical, intent(in) :: initial
    integer, intent(out), optional :: index
    integer :: hash
    integer, dimension(1) :: tree, codes, mapping
    logical, dimension(1) :: resonant, log_enhanced
    logical :: ok
    integer :: k
    hash = 1
    tree = bincode
    codes = code
    resonant = .false.
    log_enhanced = .false.
    mapping = NO_MAPPING
    call cascade_compare &
         & (hash, tree, codes, resonant, log_enhanced, mapping, ok)
    if (ok) then
       k = n_cascades+1
       if (k>size(cascade_set)) then
          write (msg_buffer, "(1x,A,1x,I6,1x,A)") &
               & "More than ", MAX_CASCADES, &
               & "cascades.  You may increase MAX_CASCADES in limits.f90"
          call msg_fatal
       end if
       call cascade_create (cascade_set(k), 1)
       cascade_set(k)%active = .true.
       cascade_set(k)%depth = 1
       cascade_set(k)%hash = 1
       cascade_set(k)%node%bincode = bincode
       cascade_set(k)%node%code = code
       cascade_set(k)%node%mapping = NO_MAPPING
       cascade_set(k)%node%index = k
       cascade_set(k)%node%minimal_mass = particle_mass (code, par)
       cascade_set(k)%node%real_mass = cascade_set(k)%node%minimal_mass
       if (cascade_set(k)%node%real_mass >= threshold_mass) then
          cascade_set(k)%node%effective_mass = cascade_set(k)%node%real_mass
       else
          cascade_set(k)%node%effective_mass = 0
       end if
       cascade_set(k)%node%resonant = .false.
       cascade_set(k)%node%initial = initial
       cascade_set(k)%node%t_channel = initial
       cascade_set(k)%node%is_vector = particle_spin_type (code) == VECTOR_BOSON
       if (initial) then
          cascade_set(k)%multiplicity = 0
       else
          cascade_set(k)%multiplicity = 1
       end if
       cascade_set(k)%tree(1) = cascade_set(k)%node%bincode
       cascade_set(k)%code(1) = cascade_set(k)%node%code
       cascade_set(k)%mapping(1) = cascade_set(k)%node%mapping
       cascade_set(k)%t_channel(1) = initial
       if (initial) cascade_set(k)%n_t_channel = 0
       if (present(index)) index = k
       n_cascades = n_cascades + 1
    else
       if (present (index)) index = 0
    end if
  end subroutine cascade_set_add_single

  subroutine cascade_copy_final (i, mapping)
    integer, intent(inout) :: i
    integer, intent(in) :: mapping
    integer :: k, d, mapping_old, hash
    integer, dimension(cascade_set(i)%depth) :: tree, code
    integer, dimension(cascade_set(i)%depth) :: mappings_new
    logical, dimension(cascade_set(i)%depth) :: resonant_new
    logical, dimension(cascade_set(i)%depth) :: log_enhanced_new
    integer :: n_resonances_new, n_log_enhanced_new, index_old
    logical :: ok
    d = cascade_set(i)%depth
    mapping_old = cascade_set(i)%node%mapping
    hash = cascade_set(i)%hash
    tree = cascade_set(i)%tree
    code = cascade_set(i)%code
    mappings_new = cascade_set(i)%mapping
    mappings_new(d) = mapping
    resonant_new = cascade_set(i)%resonant
    n_resonances_new = cascade_set(i)%n_resonances
    log_enhanced_new = cascade_set(i)%log_enhanced
    n_log_enhanced_new = cascade_set(i)%n_log_enhanced
    select case (mapping_old)
    case (S_CHANNEL)
       resonant_new(d) = .false.
       n_resonances_new = n_resonances_new - 1
    case (RADIATION, COLLINEAR, INFRARED, T_CHANNEL, U_CHANNEL)
       log_enhanced_new(d) = .false.
       n_log_enhanced_new = n_log_enhanced_new - 1
    end select
    select case (mapping)
    case (S_CHANNEL)
       resonant_new(d) = .true.
       n_resonances_new = n_resonances_new + 1
    case (RADIATION, COLLINEAR, INFRARED, T_CHANNEL, U_CHANNEL)
       log_enhanced_new(d) = .true.
       n_log_enhanced_new = n_log_enhanced_new + 1
    end select
    call cascade_compare (hash, tree, code, &
         & resonant_new, log_enhanced_new, mappings_new, ok, index_old)
    if (ok) then
       k = n_cascades+1
       if (k>size(cascade_set)) then
          write (msg_buffer, "(1x,A,1x,I6,1x,A)") &
               & "More than ", MAX_CASCADES, &
               & "cascades.  You may increase MAX_CASCADES in limits.f90"
          call msg_fatal
       end if
       call cascade_create (cascade_set(k), d)
       cascade_set(k)%active = cascade_set(i)%active
       cascade_set(k)%subdominant = cascade_set(i)%subdominant
       cascade_set(k)%active = cascade_set(i)%active
       cascade_set(k)%active = cascade_set(i)%active
       cascade_set(k)%depth = cascade_set(i)%depth
       cascade_set(k)%hash = hash
       cascade_set(k)%multiplicity = cascade_set(i)%multiplicity
       cascade_set(k)%n_t_channel = cascade_set(i)%n_t_channel
       cascade_set(k)%n_resonances = n_resonances_new
       cascade_set(k)%n_log_enhanced = n_log_enhanced_new
       cascade_set(k)%tree = tree
       cascade_set(k)%code = code
       cascade_set(k)%mapping = mappings_new
       cascade_set(k)%resonant = resonant_new
       cascade_set(k)%log_enhanced = log_enhanced_new
       cascade_set(k)%t_channel = cascade_set(i)%t_channel
       cascade_set(k)%node%bincode = cascade_set(i)%node%bincode
       cascade_set(k)%node%code = cascade_set(i)%node%code
       cascade_set(k)%node%index = k
       cascade_set(k)%node%minimal_mass = cascade_set(i)%node%minimal_mass
       cascade_set(k)%node%real_mass = cascade_set(i)%node%real_mass
       cascade_set(k)%node%effective_mass = cascade_set(i)%node%effective_mass
       cascade_set(k)%node%resonant = cascade_set(i)%node%resonant
       cascade_set(k)%node%decays = cascade_set(i)%node%decays
       cascade_set(k)%node%t_channel = cascade_set(i)%node%t_channel
       cascade_set(k)%node%needs_cut = cascade_set(i)%node%needs_cut
       cascade_set(k)%node%is_vector = cascade_set(i)%node%is_vector
       cascade_set(k)%node%initial = cascade_set(i)%node%initial
       cascade_set(k)%node%final = cascade_set(i)%node%final
       cascade_set(k)%node%daughter1 => cascade_set(i)%node%daughter1
       cascade_set(k)%node%daughter2 => cascade_set(i)%node%daughter2
       cascade_set(k)%node%mother => cascade_set(i)%node%mother
       cascade_set(k)%node%mapping = mapping
       n_cascades = n_cascades + 1
       i = k
    else
       i = index_old
    end if
  end subroutine cascade_copy_final

  subroutine cascade_set_generate_s &
       & (par, threshold_mass, sqrts, off_shell, depth)
    type(parameter_set), intent(in) :: par
    real(kind=default), intent(in) :: threshold_mass, sqrts
    integer, intent(in) :: off_shell
    integer, intent(in), optional :: depth
    integer :: i, j
    integer (kind=TC) :: ic, jc
    i = 0
    OUTER_LOOP: do
       i = i+1;  if (i>n_cascades) exit OUTER_LOOP
       ic = cascade_set(i)%node%bincode
       j = 0
       INNER_LOOP: do
          j = j+1;  if (j>n_cascades) exit INNER_LOOP
          jc = cascade_set(j)%node%bincode
          if (iand (ic,jc) /= 0) cycle INNER_LOOP
          call cascade_match &
               & (i, j, decay=.true., par=par, &
               &  threshold_mass=threshold_mass, sqrts=sqrts, &
               &  depth=depth, is_decay=present(depth), off_shell=off_shell)
       end do INNER_LOOP
    end do OUTER_LOOP
  end subroutine cascade_set_generate_s

  subroutine cascade_set_generate_t_multi &
       & (par, threshold_mass, sqrts, depth, seed, target, off_shell)
    type(parameter_set), intent(in) :: par
    real(kind=default), intent(in) :: threshold_mass, sqrts
    integer, intent(in) :: depth
    integer, intent(in), dimension(:) :: seed, target
    integer, intent(in), optional :: off_shell
    integer :: i, j
    LOOP_SEED: do i = 1, size (seed)
       if (seed(i) == 0) cycle LOOP_SEED
       LOOP_TARGET: do j = 1, size (target)
          if (target(j) == 0) cycle LOOP_TARGET
          call cascade_set_generate_t_single &
               & (par, threshold_mass, sqrts, depth, &
               &  seed(i), target(j), off_shell)
       end do LOOP_TARGET
    end do LOOP_SEED
  end subroutine cascade_set_generate_t_multi

  subroutine cascade_set_generate_t_single &
       & (par, threshold_mass, sqrts, depth, seed, target, off_shell)
    type(parameter_set), intent(in) :: par
    real(kind=default), intent(in) :: threshold_mass, sqrts
    integer, intent(in) :: depth, seed, target
    integer, intent(in), optional :: off_shell
    integer :: i, j
    integer(kind=TC) :: ic, jc, seedc, targetc
    seedc = cascade_set(seed)%node%bincode
    targetc = cascade_set(target)%node%bincode
    i = 0
    OUTER_LOOP: do
       i = i+1;  if (i>n_cascades) exit OUTER_LOOP
       ic = cascade_set(i)%node%bincode
       if (iand (ic,targetc) /= 0) cycle OUTER_LOOP
       j = 0
       INNER_LOOP: do
          j = j+1;  if (j>n_cascades) exit INNER_LOOP
          jc = cascade_set(j)%node%bincode
          if (iand (jc,targetc) /= 0) cycle INNER_LOOP
          if (iand (jc,ic) /= 0) cycle INNER_LOOP
          if (iand (ic,seedc) /= 0) then
             call cascade_match &
                  & (i, j, decay=.false., &
                  &  par=par, threshold_mass=threshold_mass, sqrts=sqrts, &
                  &  depth=depth, off_shell=off_shell)
          else if (iand (jc,seedc) /= 0) then
             call cascade_match &
                  & (j, i, decay=.false., &
                  &  par=par, threshold_mass=threshold_mass, sqrts=sqrts, &
                  &  depth=depth, off_shell=off_shell)
          end if
       end do INNER_LOOP
    end do OUTER_LOOP
  end subroutine cascade_set_generate_t_single

  subroutine cascade_set_keystone_multi &
       & (par, threshold_mass, sqrts, depth, seed, target, off_shell)
    type(parameter_set), intent(in) :: par
    real(kind=default), intent(in) :: threshold_mass, sqrts
    integer, intent(in) :: depth
    integer, dimension(:), intent(in) :: seed, target
    integer, intent(in), optional :: off_shell
    integer :: i, j
    LOOP_SEED: do i = 1, size (seed)
       if (seed(i) == 0) cycle LOOP_SEED
       LOOP_TARGET: do j = 1, size (target)
          if (target(j) == 0) cycle LOOP_TARGET
          call cascade_set_keystone_single &
               & (par, threshold_mass, sqrts, depth, &
               &  seed(i), target(j), off_shell)
       end do LOOP_TARGET
    end do LOOP_SEED
  end subroutine cascade_set_keystone_multi
    
  subroutine cascade_set_keystone_single &
       & (par, threshold_mass, sqrts, depth, seed, target, off_shell)
    type(parameter_set), intent(in) :: par
    real(kind=default), intent(in) :: threshold_mass, sqrts
    integer, intent(in) :: depth, seed, target
    integer, intent(in), optional :: off_shell
    integer :: i, j, k, d12
    integer(kind=TC) :: ic, jc, kc, seedc, targetc
    logical, parameter :: KEYSTONE_AT_TARGET = .true.
    seedc = cascade_set(seed)%node%bincode
    targetc = cascade_set(target)%node%bincode
    i = n_cascades_s
    LOOP_SEED: do
       i = i+1;  if (i>n_cascades_t) exit LOOP_SEED
       ic = cascade_set(i)%node%bincode
       if (iand (ic,seedc) == 0) cycle LOOP_SEED
       j = n_cascades_s
       LOOP_TARGET: do
          if (KEYSTONE_AT_TARGET) then
             j = target
             jc = targetc
          else
             j = j+1;  if (j>n_cascades_t) exit LOOP_TARGET
             jc = cascade_set(j)%node%bincode
             if (iand (jc,targetc) == 0) cycle LOOP_TARGET
          end if
          if (iand (jc,ic) /= 0) cycle LOOP_TARGET
          d12 = cascade_set(i)%depth + cascade_set(j)%depth
          if (d12 > depth) cycle LOOP_TARGET
          k = 0
          LOOP_BRANCH: do
             k = k+1;  if (k>n_cascades_s) exit LOOP_BRANCH
             kc = cascade_set(k)%node%bincode
             if (iand (kc,ic) /= 0) cycle LOOP_BRANCH
             if (iand (kc,jc) /= 0) cycle LOOP_BRANCH
             if (d12 + cascade_set(k)%depth == depth) then
                call cascade_match &
                     & (i, k, third_branch=j, decay=.false., &
                     &  par=par, threshold_mass=threshold_mass, sqrts=sqrts, &
                     &  depth=depth, off_shell=off_shell)
             end if
          end do LOOP_BRANCH
          if (KEYSTONE_AT_TARGET)  exit LOOP_TARGET
       end do LOOP_TARGET
    end do LOOP_SEED
  end subroutine cascade_set_keystone_single

  subroutine cascade_finalize_decay (code, depth)
    integer, intent(in) :: code, depth
    integer :: i, d
    d = depth - 2
    do i = 1, n_cascades
       if (cascade_set(i)%depth == d .and. &
            & cascade_set(i)%node%code == code .and. &
            & cascade_set(i)%node%resonant) then
          cascade_set(i)%final = .true.
          cascade_set(i)%n_t_channel = 0
          cascade_set(i)%resonant(d) = .false.
          cascade_set(i)%n_resonances = cascade_set(i)%n_resonances - 1
          cascade_set(i)%mapping(d) = NO_MAPPING
       end if
    end do
  end subroutine cascade_finalize_decay

  function cascade_disjunct (i, j) result (disjunct)
    integer, intent(in) :: i, j
    logical :: disjunct
    disjunct = (iand (cascade_set(i)%node%bincode, &
         &            cascade_set(j)%node%bincode) == 0)
  end function cascade_disjunct

  subroutine cascade_match &
       & (i, j, decay, par, threshold_mass, sqrts, &
       &  third_branch, depth, off_shell, is_decay)
    integer, intent(in) :: i, j
    logical, intent(in) :: decay
    type(parameter_set), intent(in) :: par
    real(kind=default), intent(in) :: threshold_mass, sqrts
    integer, intent(in), optional :: third_branch, depth, off_shell
    logical, intent(in), optional :: is_decay
    integer :: k, ii, jj, kk
    integer :: point, d1, d2, d3, d, code1, code2, code3, mapping
    logical :: ok, resonant, final, needs_cut, is_final_decay
    real(kind=default) :: minimal_mass, real_mass, effective_mass
    final = present(third_branch)
    d1 = cascade_set(i)%depth
    d2 = cascade_set(j)%depth
    if (final) then
       k = third_branch
       d3 = cascade_set(k)%depth
    else
       k = 0
       d3 = 1
    end if
    is_final_decay = .false.
    d = d1 + d2 + d3
    if (present(depth)) then
       if (final) then
          if (d/=depth) return
       else
          if (d>=depth) return
       end if
       if (present (is_decay)) is_final_decay = is_decay .and. (d == depth - 2)
    end if
    point = 1
    code1 = cascade_set(i)%node%code
    code2 = cascade_set(j)%node%code
    SCAN_VERTICES: do
       if (final) then
          code3 = cascade_set(k)%node%code
       else
          code3 = UNDEFINED
       end if
       call lookup_vertex (code1, code2, code3, point)
       if (code3==UNDEFINED) exit SCAN_VERTICES
       if (decay) then
          call cascade_check_decay &
               & ((/i,j/), code3, par, threshold_mass, sqrts, is_final_decay, &
               &  ok, resonant, needs_cut, mapping, &
               &  minimal_mass, real_mass, effective_mass)
          if (ok) then
             if (resonant) then
                call cascade_merge &
                     & (i, j, antiparticle_code(code3), mapping, &
                     &  resonant, needs_cut, &
                     &  real_mass, real_mass, effective_mass, off_shell)
                call cascade_merge &
                     & (i, j, antiparticle_code(code3), NO_MAPPING, &
                     &  .false., needs_cut, &
                     &  minimal_mass, real_mass, effective_mass, off_shell)
             else
                call cascade_merge &
                     & (i, j, antiparticle_code(code3), mapping, &
                     &  resonant, needs_cut, &
                     &  minimal_mass, real_mass, effective_mass, off_shell)
             end if
          end if
       else if (.not.final) then
          mapping = NO_MAPPING
          if (cascade_set(i)%node%t_channel) then
             call cascade_check_exchange &
                  & ((/i,j/), code3, par, threshold_mass, &
                  &  ok, resonant, needs_cut, mapping, &
                  &  minimal_mass, real_mass, effective_mass)
          else
             call cascade_check_exchange &
                  & ((/j,i/), code3, par, threshold_mass, &
                  &  ok, resonant, needs_cut, mapping, &
                  &  minimal_mass, real_mass, effective_mass)
          end if
          if (ok) then
             if (resonant) then
                call cascade_merge &
                     & (i, j, antiparticle_code(code3), mapping, &
                     &  resonant, needs_cut, &
                     &  real_mass, real_mass, effective_mass, off_shell)
                call cascade_merge &
                     & (i, j, antiparticle_code(code3), NO_MAPPING, &
                     &  .false., needs_cut, &
                     &  minimal_mass, real_mass, effective_mass, off_shell)
             else
                call cascade_merge &
                     & (i, j, antiparticle_code(code3), mapping, &
                     &  resonant, needs_cut, &
                     &  minimal_mass, real_mass, effective_mass, off_shell)
             end if
          end if
       else
          call cascade_check_final (i, j, k, ii, jj, kk, ok)
          if (ok)  call cascade_merge_final (ii, jj, kk, off_shell)
       end if
    end do SCAN_VERTICES
  end subroutine cascade_match

  subroutine cascade_merge &
       & (i, j, code, mapping, resonant, needs_cut, &
       &  minimal_mass, real_mass, effective_mass, off_shell)
    integer, intent(in) :: i, j, code, mapping
    logical, intent(in) :: resonant, needs_cut
    integer, intent(in), optional :: off_shell
    real(kind=default), intent(in) :: minimal_mass, real_mass, effective_mass
    integer :: k, d, d1, d2
    integer :: n_t_channel, n_resonances, n_log_enhanced
    integer :: hash, internal
    integer(kind=TC) :: bc1, bc2, bc
    integer(kind=TC), dimension(:), allocatable :: tree
    integer, dimension(:), allocatable :: codes, mappings
    logical, dimension(:), allocatable :: reson, logs, tchan
    logical :: ok
    ok = .true.
    d1 = cascade_set(i)%depth
    d2 = cascade_set(j)%depth
    d = d1 + d2 + 1
    internal = (d-3)/2
    n_resonances = cascade_set(i)%n_resonances + cascade_set(j)%n_resonances
    n_log_enhanced = &
         & cascade_set(i)%n_log_enhanced + cascade_set(j)%n_log_enhanced
    if (resonant) then
       n_resonances = n_resonances + 1
    end if
    if (present(off_shell)) then
       if (internal - n_resonances - n_log_enhanced > off_shell)  return
    end if
    bc1 = cascade_set(i)%node%bincode
    bc2 = cascade_set(j)%node%bincode
    bc = ior (bc1, bc2)
    allocate(tree(d), codes(d), mappings(d), reson(d), logs(d), tchan(d))
    tree(1:d1)  = cascade_set(i)%tree
    codes(1:d1) = cascade_set(i)%code
    mappings(1:d1) = cascade_set(i)%mapping
    reson(1:d1) = cascade_set(i)%resonant
    logs(1:d1) = cascade_set(i)%log_enhanced
    tchan(1:d1) = cascade_set(i)%t_channel
    tree(d1+1:d1+d2)  = cascade_set(j)%tree
    codes(d1+1:d1+d2) = cascade_set(j)%code
    mappings(d1+1:d1+d2) = cascade_set(j)%mapping
    reson(d1+1:d1+d2) = cascade_set(j)%resonant
    logs(d1+1:d1+d2) = cascade_set(j)%log_enhanced
    tchan(d1+1:d1+d2) = cascade_set(j)%t_channel
    tree(d)  = bc
    codes(d) = code
    mappings(d) = mapping
    reson(d) = resonant
    tchan(d) = any(tchan(:d-1))
    select case (mapping)
    case (COLLINEAR, RADIATION, INFRARED, T_CHANNEL, U_CHANNEL)
       logs(d) = .true.
       n_log_enhanced = n_log_enhanced + 1
    case default
       logs(d) = .false.
    end select
    call tree_sort (tree, codes, mappings, reson, logs, tchan)
    hash = cascade_set(i)%hash * cascade_set(j)%hash
    hash = hash * (bc+847203)
!     if (resonant) &
!          hash = hash * (code + 304981396) !* (mapping + 987804347)
    call cascade_compare (hash, tree, codes, reson, logs, mappings, ok)
    n_t_channel = count(tchan) - 1
    if (n_t_channel > exchange_lines)  ok = .false.
    if (ok) then
       k = n_cascades + 1
       if (k>size(cascade_set)) then
          write (msg_buffer, "(1x,A,1x,I6,1x,A)") &
               & "More than ", MAX_CASCADES, &
               & "cascades.  You may increase MAX_CASCADES in limits.f90"
          call msg_fatal
       end if
       call cascade_create (cascade_set(k), d)
       cascade_set(k)%hash = hash
       if (resonant) then
          cascade_set(k)%multiplicity = 1
       else
          cascade_set(k)%multiplicity = &
               & cascade_set(i)%multiplicity &
               & + cascade_set(j)%multiplicity
       end if
       cascade_set(k)%active = .true.
       cascade_set(k)%depth = d
       cascade_set(k)%final = .false.
       cascade_set(k)%resonant = reson
       cascade_set(k)%n_resonances = n_resonances
       cascade_set(k)%log_enhanced = logs
       cascade_set(k)%n_log_enhanced = n_log_enhanced
       cascade_set(k)%t_channel = tchan
       cascade_set(k)%n_t_channel = n_t_channel
       cascade_set(k)%node%bincode = bc
       cascade_set(k)%node%code = code
       cascade_set(k)%node%index = k
       cascade_set(k)%node%minimal_mass = minimal_mass
       cascade_set(k)%node%real_mass = real_mass
       cascade_set(k)%node%effective_mass = effective_mass
       cascade_set(k)%node%resonant = resonant
       cascade_set(k)%node%needs_cut = needs_cut
       cascade_set(k)%node%t_channel = any(tchan)
       cascade_set(k)%node%is_vector = &
            & particle_spin_type (code) == VECTOR_BOSON
       cascade_set(k)%node%mapping = mapping
       cascade_set(k)%node%final = .false.
       cascade_set(k)%node%decays = .true.
       cascade_set(k)%node%daughter1 => cascade_set(i)%node
       cascade_set(k)%node%daughter2 => cascade_set(j)%node
       cascade_set(k)%tree = tree
       cascade_set(k)%code = codes
       cascade_set(k)%mapping = mappings
       n_cascades = k
    end if
    deallocate(tree, codes, mappings, reson, logs)
  end subroutine cascade_merge

  subroutine cascade_merge_final (i, j, k, off_shell)
    integer, intent(in) :: i, j, k
    integer, intent(in), optional :: off_shell
    integer :: d1, d2, d3, d, n
    integer :: hash, internal
    integer :: n_resonances, n_log_enhanced, n_t_channel
    integer(kind=TC), dimension(:), allocatable :: tree
    integer, dimension(:), allocatable :: codes, mappings
    logical, dimension(:), allocatable :: reson, logs, tchan
    logical :: needs_cut, ok
    ok = .true.
    d1 = cascade_set(i)%depth
    d2 = cascade_set(j)%depth
    d3 = cascade_set(k)%depth
    d = d1 + d2 + d3
    internal = (d-3)/2
    n_resonances = cascade_set(i)%n_resonances &
         &       + cascade_set(j)%n_resonances &
         &       + cascade_set(k)%n_resonances
    n_log_enhanced = cascade_set(i)%n_log_enhanced &
         &         + cascade_set(j)%n_log_enhanced &
         &         + cascade_set(k)%n_log_enhanced
    if (present(off_shell)) then
       if (d1==1 .and. d3==1) then
          if (internal - n_resonances - n_log_enhanced > off_shell)  return
       else
          if (internal - n_resonances - n_log_enhanced > off_shell)  return
       end if
    end if
    allocate(tree(d), codes(d), mappings(d), reson(d), logs(d), tchan(d))
    tree(1:d1)  = cascade_set(i)%tree
    codes(1:d1) = cascade_set(i)%code
    mappings(1:d1) = cascade_set(i)%mapping
    reson(1:d1) = cascade_set(i)%resonant
    logs(1:d1) = cascade_set(i)%log_enhanced
    tchan(1:d1) = cascade_set(i)%t_channel
    tree(d1+1:d1+d2)  = cascade_set(j)%tree
    codes(d1+1:d1+d2) = cascade_set(j)%code
    mappings(d1+1:d1+d2) = cascade_set(j)%mapping
    reson(d1+1:d1+d2) = cascade_set(j)%resonant
    logs(d1+1:d1+d2) = cascade_set(j)%log_enhanced
    tchan(d1+1:d1+d2) = cascade_set(j)%t_channel
    tree(d1+d2+1:d)  = cascade_set(k)%tree
    codes(d1+d2+1:d) = cascade_set(k)%code
    mappings(d1+d2+1:d) = cascade_set(k)%mapping
    reson(d1+d2+1:d) = cascade_set(k)%resonant
    logs(d1+d2+1:d) = cascade_set(k)%log_enhanced
    tchan(d1+d2+1:d) = cascade_set(k)%t_channel
    call tree_sort (tree, codes, mappings, reson, logs, tchan)
    hash = cascade_set(i)%hash * cascade_set(j)%hash * cascade_set(k)%hash
    call cascade_compare (hash, tree, codes, reson, logs, mappings, ok)
    n_t_channel = count(tchan) - 2
    if (n_t_channel > exchange_lines)  ok = .false.
    if (ok) then
       n = n_cascades + 1
       if (n>size(cascade_set)) then
          write (msg_buffer, "(1x,A,1x,I6,1x,A)") &
               & "More than ", MAX_CASCADES, &
               & "cascades.  You may increase MAX_CASCADES in limits.f90"
          call msg_fatal
       end if
       call cascade_create (cascade_set(n), d)
       cascade_set(n)%hash = hash
       needs_cut = cascade_set(i)%node%needs_cut &
            &  .or.cascade_set(j)%node%needs_cut &
            &  .or.cascade_set(k)%node%needs_cut
       cascade_set(n)%multiplicity = cascade_set(i)%multiplicity &
            &                      + cascade_set(j)%multiplicity &
            &                      + cascade_set(k)%multiplicity
       cascade_set(n)%active = .true.
       cascade_set(n)%depth = d
       cascade_set(n)%final = .true.
       cascade_set(n)%resonant = reson
       cascade_set(n)%n_resonances = n_resonances
       cascade_set(n)%log_enhanced = logs
       cascade_set(n)%n_log_enhanced = n_log_enhanced
       cascade_set(n)%t_channel = tchan
       cascade_set(n)%n_t_channel = n_t_channel
       cascade_set(n)%node%bincode = 0
       cascade_set(n)%node%code = KEYSTONE
       cascade_set(n)%node%index = n
       cascade_set(n)%node%minimal_mass = 0
       cascade_set(n)%node%real_mass = 0
       cascade_set(n)%node%effective_mass = 0
       cascade_set(n)%node%resonant = .false.
       cascade_set(n)%node%needs_cut = needs_cut
       cascade_set(n)%node%mapping = NO_MAPPING
       cascade_set(n)%node%initial = .false.
       cascade_set(n)%node%final = .true.
       cascade_set(n)%node%t_channel = .false.
       cascade_set(n)%node%is_vector = .false.
       cascade_set(n)%node%decays = .true.
       cascade_set(n)%node%daughter1 => cascade_set(i)%node
       cascade_set(n)%node%daughter2 => cascade_set(j)%node
       cascade_set(n)%node%mother => cascade_set(k)%node
       cascade_set(n)%tree = tree
       cascade_set(n)%code = codes
       cascade_set(n)%mapping = mappings
       n_cascades = n
    end if
    deallocate(tree, codes, mappings, reson, logs)
  end subroutine cascade_merge_final

  subroutine tree_sort (tree, code, mapping, resonant, log_enhanced, t_channel)
    integer(kind=TC), dimension(:), intent(inout) :: tree
    integer, dimension(:), intent(inout) :: code, mapping
    logical, dimension(:), intent(inout) :: resonant, log_enhanced, t_channel
    integer(kind=TC), dimension(size(tree)) :: tree_tmp
    integer, dimension(size(code)) :: code_tmp, mapping_tmp
    logical, dimension(size(resonant)) :: resonant_tmp, logs_tmp, t_channel_tmp
    integer, dimension(1) :: pos
    integer :: i
    tree_tmp = tree
    code_tmp = code
    mapping_tmp = mapping
    resonant_tmp = resonant
    logs_tmp = log_enhanced
    t_channel_tmp = t_channel
    do i=size(tree),1,-1
       pos = maxloc(tree_tmp)
       tree(i) = tree_tmp(pos(1))
       code(i) = code_tmp(pos(1))
       mapping(i) = mapping_tmp(pos(1))
       resonant(i) = resonant_tmp(pos(1))
       log_enhanced(i) = logs_tmp(pos(1))
       t_channel(i) = t_channel_tmp(pos(1))
       tree_tmp(pos(1)) = 0
    end do
  end subroutine tree_sort

  subroutine cascade_check_decay &
       & (index, newcode, par, threshold_mass, sqrts, is_final_decay, &
       &  ok, resonant, needs_cut, mapping, &
       &  minimal_mass, real_mass, effective_mass)
    integer, dimension(2), intent(in) :: index
    integer, intent(in) :: newcode
    type(parameter_set), intent(in) :: par
    real(kind=default), intent(in) :: threshold_mass, sqrts
    logical, intent(in) :: is_final_decay
    logical, intent(out) :: ok, resonant, needs_cut
    integer, intent(out) :: mapping
    real(kind=default), intent(out) :: minimal_mass, real_mass, effective_mass
    type(node_t), pointer :: node
    integer, dimension(2) :: code
    logical, dimension(2) :: on_shell, quasi_on_shell
    logical, dimension(0:2) :: is_vector
    real(kind=default), dimension(0:2) :: m_min, m_real, m_eff
    real(kind=default) :: width
    integer :: depth
    integer :: i
    do i=1,2
       node => cascade_set(index(i))%node
       code(i) = node%code
       on_shell(i) = node%resonant .or. .not. node%decays
       quasi_on_shell(i) = on_shell(i) .or. node%mapping == RADIATION
       is_vector(i) = node%is_vector
       m_min(i) = node%minimal_mass
       m_real(i) = node%real_mass
       m_eff(i) = node%effective_mass
    end do
    is_vector(0) = particle_spin_type (newcode) == VECTOR_BOSON
    m_min(0) = sum (m_min(1:2))
    m_real(0) = particle_mass (newcode, par)
    width = particle_width (newcode, par)
    if (m_real(0) <= threshold_mass) then
       m_eff(0) = 0
    else
       m_eff(0) = m_real(0)
    end if
    ok = .false.
    resonant = .false.
    needs_cut = .false.
    mapping = NO_MAPPING
    depth = cascade_set(index(1))%depth + cascade_set(index(2))%depth
    ! Potentially resonant cases [sqrts=m_real(0) for on-shell decay]
    if (m_real(0) > m_min(0) .and. m_real(0) <= sqrts) then
       if (is_final_decay .or. width > 0) then
          if (all(on_shell)) then
             resonant = .true.
          else if (single_off_shell_decays) then
             resonant = any(on_shell) .or. double_off_shell_decays
          end if
          if (resonant) then
             ok = .true.
             mapping = S_CHANNEL
          end if
       else
          call warn_decay (newcode)
       end if
    ! Collinear and IR singular cases
    else if (m_real(0) < sqrts) then
       ! Massless splitting
       if (all (m_eff==0) .and. depth <= 2*splitting_depth) then
          ok = .true.
          needs_cut = .true.
          if (is_vector(0)) then
             if (all (is_vector(1:2))) then
                mapping = COLLINEAR   ! three-vector-vertex
             else
                mapping = INFRARED    ! vector splitting into matter
             end if
          else
             if (any (is_vector(1:2))) then
                mapping = COLLINEAR   ! vector radiation off matter
             else
                mapping = INFRARED    ! scalar radiation/splitting
             end if
          end if
       ! IR radiation off massive particle
       else if (m_eff(0) > 0 .and. m_eff(1) > 0 .and. m_eff(2) == 0 &
            &   .and. quasi_on_shell(1) &
            &   .and. abs (m_eff(0)-m_eff(1)) < threshold_mass) then
          ok = .true.
          mapping = RADIATION
          needs_cut = .true.
       else if (m_eff(0) > 0 .and. m_eff(2) > 0 .and. m_eff(1) == 0 &
            &   .and. quasi_on_shell(2) &
            &   .and. abs (m_eff(0)-m_eff(2)) < threshold_mass) then
          ok = .true.
          mapping = RADIATION
          needs_cut = .true.
       end if
    end if
    ! Non-singular cases, including failed resonances
    if (.not.ok .and. massive_FSR) then
       ! Two on-shell particles from a virtual mother
       if (all (on_shell)) then
          ok = .true.
       ! One or two daughters off-shell
       else if (single_off_shell_branchings) then
          ok = any (on_shell) .or. double_off_shell_branchings
       end if
       if (ok) then
          m_eff(0) = max (sum (m_eff(1:2)), m_min(0))
          if (m_eff(0) < threshold_mass)  m_eff(0) = 0
       end if
    end if
    minimal_mass = m_min(0)
    real_mass = m_real(0)
    effective_mass = m_eff(0)
  contains
    subroutine warn_decay (code)
      use limits, only: MAX_WARN_RESONANCE
      integer, intent(in) :: code
      integer :: i
      integer, dimension(MAX_WARN_RESONANCE), save :: warned_code = 0
      LOOP_WARNED: do i = 1, MAX_WARN_RESONANCE
         if (warned_code(i) == 0) then
            warned_code(i) = code
            write (msg_buffer, "(A)") &
                 & " Intermediate decay of zero-width particle " &
                 & // trim (particle_name (code)) &
                 & // " may be possible."
            call msg_warning
            exit LOOP_WARNED
         else if (warned_code(i) == code) then
            exit LOOP_WARNED
         end if
      end do LOOP_WARNED
    end subroutine warn_decay
  end subroutine cascade_check_decay

  subroutine cascade_check_exchange &
       & (index, newcode, par, threshold_mass, &
       &  ok, resonant, needs_cut, mapping, &
       &  minimal_mass, real_mass, effective_mass)
    integer, dimension(2), intent(in) :: index
    integer, intent(in) :: newcode
    type(parameter_set), intent(in) :: par
    real(kind=default), intent(in) :: threshold_mass
    logical, intent(out) :: ok, resonant, needs_cut
    integer, intent(out) :: mapping
    real(kind=default), intent(out) :: minimal_mass, real_mass, effective_mass
    type(node_t), pointer :: node
    integer, dimension(2) :: code
    logical, dimension(2) :: on_shell, quasi_on_shell
    logical, dimension(0:2) :: is_vector
    real(kind=default), dimension(0:2) :: m_min, m_real, m_eff
    logical :: initial
    integer :: i
    ok = .false.
    initial = cascade_set(index(1))%node%initial
    do i=1,2
       node => cascade_set(index(i))%node
       if (node%mapping == T_CHANNEL) return
       code(i) = node%code
       on_shell(i) = node%resonant .or. .not. node%decays
       quasi_on_shell(i) = on_shell(i) .or. node%mapping == RADIATION
       is_vector(i) = node%is_vector
       m_min(i) = node%minimal_mass
       m_real(i) = node%real_mass
       if (node%effective_mass >= threshold_mass) then
          m_eff(i) = node%effective_mass
       else
          m_eff(i) = 0
       end if
    end do
    is_vector(0) = particle_spin_type (newcode) == VECTOR_BOSON
    if (initial) then
       m_min(0) = m_min(2)
    else
       m_min(0) = sum (m_min(1:2))
    end if
    m_real(0) = particle_mass (newcode, par)
    if (m_real(0) <= threshold_mass) then
       m_eff(0) = 0
    else
       m_eff(0) = m_real(0)
    end if
    resonant = .false.
    needs_cut = .false.
    mapping = NO_MAPPING
    ! Allowed decay of beam particle
    if (initial .and. m_real(0) + m_real(2) < m_real(1)) then
       write (msg_buffer, "(1x,A,1x,'->',1x,A,1x,A)") &
            & trim (particle_name (code(1))), &
            & trim (particle_name (newcode)), &
            & trim (particle_name (code(2)))
       call msg_message
       write (msg_buffer, "(1x,'mass(',A,') =',1x,E17.10)") &
            & particle_name (code(1)), m_real(1)
       call msg_message
       write (msg_buffer, "(1x,'mass(',A,') =',1x,E17.10)") &
            & particle_name (newcode), m_real(0)
       call msg_message
       write (msg_buffer, "(1x,'mass(',A,') =',1x,E17.10)") &
            & particle_name (code(2)), m_real(2)
       call msg_message
       call msg_fatal (" Phase space: Initial beam particle can decay")
    ! multiperipheral
    else
       ! Massless splitting
       if (all (m_eff==0)) then
          ok = .true.
          needs_cut = .true.
          mapping = U_CHANNEL
       ! IR radiation off massive particle
       else if (m_eff(1) > 0 .and. m_eff(0) > 0 .and. m_eff(2) == 0 &
            &   .and. quasi_on_shell(1) &
            &   .and. abs (m_eff(1)-m_eff(0)) < threshold_mass) then
          ok = .true.
          mapping = RADIATION
          needs_cut = .true.
       end if
    end if
    ! Here, we keep everything.  The graph may be rejected in cascade_merge
    ! if the number of t-channel lines is too large.
    ok = .true.
!     if (.not.ok) then
!        ! Radiation of an off-shell system
!        if (.not.quasi_on_shell(2)) then
!           ok = massive_fsr
!        ! Everything else will be just kept
!        else
!           ok = .true.
!        end if
!     end if
    minimal_mass = m_min(0)
    real_mass = m_real(0)
    effective_mass = max (m_eff(0), m_eff(2))
    if (effective_mass < threshold_mass)  effective_mass = 0
  end subroutine cascade_check_exchange

  subroutine cascade_check_final (i, j, k, ii, jj, kk, ok)
    integer, intent(in) :: i, j, k
    integer, intent(out) :: ii, jj, kk
    logical, intent(out) :: ok
    type(node_t), pointer :: node_i, node_j, node_k
    integer :: nt_i, nt_k, nt
    integer :: map_i, map_j, map_k
    logical :: vector_i, vector_j, vector_k
    node_i => cascade_set(i)%node
    node_j => cascade_set(j)%node
    node_k => cascade_set(k)%node
    nt_i = cascade_set(i)%n_t_channel
    nt_k = cascade_set(k)%n_t_channel
    nt = nt_i + nt_k
    map_i = node_i%mapping
    map_j = node_j%mapping
    map_k = node_k%mapping
    vector_i = node_i%is_vector .and. .not. node_i%initial
    vector_j = node_j%is_vector .and. node_j%decays
    vector_k = node_k%is_vector .and. .not. node_k%initial
    ii = i;  jj = j;  kk = k
    ok = .true.
    if (map_i == T_CHANNEL .or. map_k == T_CHANNEL) then
       ok = .false.;  return
    end if
    ! s-channel graphs: remove any logarithmic mapping
    if (nt == 0) then
       select case (map_j)
       case (RADIATION, COLLINEAR, INFRARED)
          call cascade_copy_final (jj, NO_MAPPING)
       end select
    ! single exchange: transfer log mapping into t-channel mapping
    else if (nt == 1) then
       if (nt_i == 1) then
          select case (map_i)
          case (RADIATION, COLLINEAR, INFRARED, U_CHANNEL)
             call cascade_copy_final (ii, T_CHANNEL)
          end select
       else
          select case (map_k)
          case (RADIATION, COLLINEAR, INFRARED, U_CHANNEL)
             call cascade_copy_final (kk, T_CHANNEL)
          end select
       end if
    ! multiperipheral, two adjacent vectors
    else if (vector_i .and. vector_k) then
       if      (map_i == COLLINEAR .or. map_i == INFRARED .or. map_i == U_CHANNEL) then
          call cascade_copy_final (ii, T_CHANNEL)
       else if (map_k == COLLINEAR .or. map_k == INFRARED .or. map_k == U_CHANNEL) then
          call cascade_copy_final (kk, T_CHANNEL)
       ! both vectors massive: choose either one
       else if (map_i == RADIATION .and. map_k == RADIATION) then
          call cascade_copy_final (ii, NO_MAPPING)
       end if
    ! multiperipheral, one vector adjacent to the keystone
    else if (vector_i) then
       if      (map_i == COLLINEAR .or. map_i == INFRARED .or. map_i == U_CHANNEL) then
          call cascade_copy_final (ii, T_CHANNEL)
       else if (map_i == RADIATION) then
          call cascade_copy_final (ii, NO_MAPPING)
       end if
    else if (vector_k) then
       if      (map_k == COLLINEAR .or. map_k == INFRARED .or. map_k == U_CHANNEL) then
          call cascade_copy_final (kk, T_CHANNEL)
       else if (map_k == RADIATION) then
          call cascade_copy_final (kk, NO_MAPPING)
       end if
    ! no vector anywhere: same as two vectors
    else if (no_vector_in_t_channel(i) .and. no_vector_in_t_channel(k)) then
       if      (map_i == COLLINEAR .or. map_i == INFRARED .or. map_i == U_CHANNEL) then
          call cascade_copy_final (ii, T_CHANNEL)
       else if (map_k == COLLINEAR .or. map_k == INFRARED .or. map_k == U_CHANNEL) then
          call cascade_copy_final (kk, T_CHANNEL)
       else if (map_i == RADIATION .and. map_k == RADIATION) then
          call cascade_copy_final (ii, NO_MAPPING)
       end if
    ! drop this if the vector exchange is elsewhere in the graph
    else
       ok = .false.
    end if
  end subroutine cascade_check_final

  recursive function no_vector_in_t_channel (i) result (no_vector)
    integer, intent(in) :: i
    logical :: no_vector
    type(node_t), pointer :: node
    node => cascade_set(i)%node
    if (node%t_channel) then
       select case(node%mapping)
       case (COLLINEAR,INFRARED,U_CHANNEL)
          if (node%initial) then
             no_vector = .true.
          else if (node%is_vector) then
             no_vector = .false.
          else
             no_vector = no_vector_in_t_channel (node%daughter1%index)
          end if
       case default
          no_vector = .true.
       end select
    else
       no_vector = .true.
    end if
  end function no_vector_in_t_channel

  subroutine cascade_compare &
       & (hash, tree, code, resonant, log_enhanced, mapping, ok, index_old)
    integer, intent(in) :: hash
    integer(kind=TC), dimension(:), intent(in) :: tree
    integer, dimension(:), intent(in) :: code
    logical, dimension(:), intent(in) :: resonant
    logical, dimension(:), intent(in) :: log_enhanced
    integer, dimension(:), intent(in) :: mapping
    logical, intent(out) :: ok
    integer, intent(out), optional :: index_old
    logical, dimension(size(tree)) :: irrelevant_code
    integer :: k, depth, d
    ok = .true.
    depth = size(tree)
    d = depth - 2
    COMPARE_TREES: do k=1, n_cascades
       if (present (index_old))  index_old = k
       if (.not.cascade_set(k)%active) cycle COMPARE_TREES
       if (hash /= cascade_set(k)%hash) cycle COMPARE_TREES
       if (cascade_set(k)%depth==depth) then
          if (all (cascade_set(k)%tree == tree)) then
             irrelevant_code = .not.(resonant .and. cascade_set(k)%resonant)
             if (.not.cascade_set(k)%final) &
                  & irrelevant_code(depth) = .false.
             if (all (irrelevant_code .or. cascade_set(k)%code==code)) then
                if (all (cascade_set(k)%mapping==mapping)) then
                   ok = .false.
                   exit COMPARE_TREES
                else if (cascade_set(k)%final) then
                   if (cascade_set(k)%n_resonances > count(resonant) .and. &
                        & cascade_set(k)%n_log_enhanced >= count(log_enhanced)) then
                      ok = .false.
                      exit COMPARE_TREES
                   else if (cascade_set(k)%n_resonances < count(resonant) .and. &
                        & cascade_set(k)%n_log_enhanced <= count(log_enhanced)) then
                      cascade_set(k)%active = .false.
                      ok = .true.
                      exit COMPARE_TREES
                   else if (cascade_set(k)%n_log_enhanced > count(log_enhanced) .and. &
                        & cascade_set(k)%n_resonances >= count(resonant)) then
                      ok = .false.
                      exit COMPARE_TREES
                   else if (cascade_set(k)%n_log_enhanced < count(log_enhanced) .and. &
                        & cascade_set(k)%n_resonances <= count(resonant)) then
                      cascade_set(k)%active = .false.
                      ok = .true.
                      exit COMPARE_TREES  
                   end if
                end if
             end if
          end if
       end if
    end do COMPARE_TREES
  end subroutine cascade_compare

  subroutine cascade_set_select (off_shell, extra, remove_deleted)
    integer, intent(in) :: off_shell, extra
    logical, intent(in) :: remove_deleted
    integer :: max_res, max_res_actual
    max_res = (maxval (cascade_set%depth, mask=cascade_set%active) - 3)/2
    max_res_actual = maxval (cascade_set%n_resonances)
    where (cascade_set%final .and. cascade_set%active &
         & .and. cascade_set%n_resonances + cascade_set%n_log_enhanced &
         &       < max_res - off_shell - extra) &
         &   cascade_set%active = .false.
    where (cascade_set%final .and. cascade_set%active &
         & .and. cascade_set%n_resonances + cascade_set%n_log_enhanced &
         &       < max_res - off_shell &
         & .and. cascade_set%n_resonances < max_res_actual) &
         &   cascade_set%deleted = .true.
    where (cascade_set%final .and. cascade_set%active .and. &
         & cascade_set%n_resonances < max_res - off_shell) &
         &   cascade_set%subdominant = .true.
!     if (.not.s_channel) then
!        where (cascade_set%final .and. cascade_set%active .and. &
!             & cascade_set%multiplicity == 1) &
!             &   cascade_set%deleted = .true.
!     end if
    if (remove_deleted) then 
       where (cascade_set%deleted)  cascade_set%active = .false.
    end if
  end subroutine cascade_set_select
         
  subroutine cascade_set_validate
    integer :: i
    do i=1, n_cascades
       if (.not.cascade_set(i)%final) then
          cascade_set(i)%active = .false.
       else if (cascade_set(i)%active) then
          call activate (cascade_set(i)%node)
       end if
    end do
  contains
    recursive subroutine activate (node)
      type(node_t), pointer :: node
      cascade_set(node%index)%active = .true.
      if (node%decays) then
         call activate (node%daughter1)
         call activate (node%daughter2)
         if (associated (node%mother))  call activate (node%mother)
      end if
    end subroutine activate
  end subroutine cascade_set_validate
      
  subroutine cascade_set_groves
    integer :: max_mult, min_mult
    integer :: max_res, min_res
    integer :: max_logs, min_logs
    integer :: max_t_chan, min_t_chan
    integer :: n_r, n_l, n_t, mult
    logical, dimension(:), allocatable :: mask_active
    logical, dimension(:), allocatable :: mask_mult, mask_t, mask_l, mask_r
    n_groves = 0
    allocate(mask_active(n_cascades))
    allocate(mask_mult(n_cascades))
    allocate(mask_t(n_cascades), mask_l(n_cascades), mask_r(n_cascades))
    where (cascade_set(:n_cascades)%active)
       mask_active = cascade_set(:n_cascades)%final
    elsewhere
       mask_active = .false.
    end where
    if (count(mask_active) > 0) then
       min_mult = minval (cascade_set(:n_cascades)%multiplicity, &
            &             mask=mask_active)
       max_mult = maxval (cascade_set(:n_cascades)%multiplicity, &
            &             mask=mask_active)

       SCAN_MULT: do mult = min_mult, max_mult
          where (mask_active)  
             mask_mult = cascade_set(:n_cascades)%multiplicity == mult
          elsewhere
             mask_mult = .false.
          end where
          if (count(mask_mult)>0) then
             max_res = maxval &
                  & (cascade_set(:n_cascades)%n_resonances, mask=mask_mult)
             min_res = minval &
                  & (cascade_set(:n_cascades)%n_resonances, mask=mask_mult)

             SCAN_RES: do n_r=max_res, min_res, -1
                where (mask_mult)
                   mask_r = cascade_set(:n_cascades)%n_resonances == n_r
                elsewhere
                   mask_r = .false.
                end where
                if (count(mask_r)>0) then
                   max_logs = maxval &
                        & (cascade_set(:n_cascades)%n_log_enhanced, mask=mask_r)
                   min_logs = minval &
                        & (cascade_set(:n_cascades)%n_log_enhanced, mask=mask_r)

                   SCAN_L: do n_l=max_logs, min_logs, -1
                      where (mask_r)
                         mask_l = cascade_set(:n_cascades)%n_log_enhanced == n_l
                      elsewhere
                         mask_l = .false.
                      end where
                      if (count(mask_l)>0) then
                         max_t_chan = maxval &
                              & (cascade_set(:n_cascades)%n_t_channel, mask=mask_l)
                         min_t_chan = minval &
                              & (cascade_set(:n_cascades)%n_t_channel, mask=mask_l)

                         SCAN_T: do n_t=max_t_chan, min_t_chan, -1
                            where (mask_l)
                               mask_t = cascade_set(:n_cascades)%n_t_channel == n_t
                            elsewhere
                               mask_t = .false.
                            end where
                            if (count(mask_t)>0) then
                               call cascade_groves_by_resonances (n_r, n_l, n_t, mask_t)
                            end if
                         end do SCAN_T

                      end if
                   end do SCAN_L

                end if
             end do SCAN_RES

          end if
       end do SCAN_MULT

    end if
    deallocate(mask_active, mask_mult, mask_t, mask_r)
  end subroutine cascade_set_groves

  subroutine cascade_groves_by_resonances &
       & (n_resonances, n_log_enhanced, n_t_channel, mask)
    integer, intent(in) :: n_resonances, n_log_enhanced,  n_t_channel
    logical, dimension(:), intent(in) :: mask
    logical, dimension(size(mask)) :: current_mask
    integer, dimension(n_resonances, size(mask)) :: resonances
    integer, dimension(n_log_enhanced, size(mask)) :: logs
    integer, dimension(n_t_channel, size(mask)) :: t_channel
    integer, dimension(n_resonances) :: res
    integer, dimension(n_log_enhanced) :: lg
    integer, dimension(n_t_channel) :: tch
    integer :: i
    logical :: first
    resonances = UNDEFINED
    logs = UNDEFINED
    t_channel = UNDEFINED
    do i=1, size(mask)
       if (mask(i)) then
          call transfer_array (resonances(:,i), &
               & abs(cascade_set(i)%code), mask=cascade_set(i)%resonant)
          call transfer_array (logs(:,i), &
               & abs(cascade_set(i)%code), mask=cascade_set(i)%log_enhanced)
          call transfer_array (t_channel(:,i), &
               & abs(cascade_set(i)%code), mask=cascade_set(i)%t_channel)
       end if
    end do
    current_mask = mask
    SET_GROVES: do
       if (count(current_mask) == 0) exit SET_GROVES
       first = .true.
       do i=1, size(mask)
          if (current_mask(i)) then
             if (first) then
                n_groves = n_groves + 1
                res = resonances(:,i)
                lg = logs(:,i)
                tch = t_channel(:,i)
                cascade_set(i)%grove = n_groves
                current_mask(i) = .false.
                first = .false.
             else if (all (resonances(:,i)==res) .and. &
                  &   all (logs(:,i)==lg) .and. &
                  &   all (t_channel(:,i)==tch)) then
                cascade_set(i)%grove = n_groves
                current_mask(i) = .false.
             end if
          end if
       end do
    end do SET_GROVES
  end subroutine cascade_groves_by_resonances

  subroutine transfer_array (array, code, mask)
    integer, dimension(:), intent(inout) :: array
    integer, dimension(:), intent(in) :: code
    logical, dimension(:), intent(in) :: mask
    logical, dimension(size(mask)) :: current_mask
    integer :: i
    integer, dimension(1) :: pos
    if (any(mask)) then
       current_mask = mask
    else
       return
    end if
    do i=1, size(array)
       pos = maxloc(code, mask=current_mask)
       array(i) = code(pos(1))
       current_mask(pos(1)) = .false.
    end do
  end subroutine transfer_array

  subroutine phase_space_channels_write_name &
       & (directory, filename, process_id, code, n_in, n_out)
    character(len=*), intent(in) :: directory, filename
    character(len=BUFFER_SIZE) :: texfile, fmffile
    character(len=*), intent(in) :: process_id
    integer, dimension(:,:), intent(in) :: code
    integer, intent(in) :: n_in, n_out
    integer :: u
    u = free_unit()
    texfile = concat (directory, trim(filename)//"-channels", &
                 trim(process_id),"tex")
    fmffile = concat ("", trim(filename)//"-channels", &
                 trim(process_id),"fmf")
    open (unit=u, action='write', status='replace', file=trim(texfile))
    call phase_space_channels_write_unit &
         & (u, trim(fmffile), process_id, code, n_in, n_out)
    close (u)
  end subroutine phase_space_channels_write_name

  subroutine phase_space_channels_write_unit &
       & (u, filename, process_id, code, n_in, n_out)
    integer, intent(in) :: u
    character(len=*), intent(in) :: filename
    character(len=*), intent(in) :: process_id
    integer, dimension(:,:), intent(in) :: code
    integer, intent(in) :: n_in, n_out
    write (u, '(A)') "\documentclass[10pt]{article}"
    write (u, '(A)') "\usepackage{feynmp}"
    write (u, '(A)') "\usepackage{color}"
    write (u, *)
    write (u, '(A)') "\textwidth 18.5cm"
    write (u, '(A)') "\evensidemargin -1.5cm"
    write (u, '(A)') "\oddsidemargin -1.5cm"
    write (u, *)
    write (u, '(A)') "\newcommand{\blue}{\color{blue}}"
    write (u, '(A)') "\newcommand{\green}{\color{green}}"
    write (u, '(A)') "\newcommand{\red}{\color{red}}"
    write (u, '(A)') "\newcommand{\magenta}{\color{magenta}}"
    write (u, '(A)') "\newcommand{\cyan}{\color{cyan}}"
    write (u, '(A)') "\newcommand{\sm}{\footnotesize}"
    write (u, '(A)') "\setlength{\parindent}{0pt}"
    write (u, '(A)') "\setlength{\parsep}{20pt}"
    write (u, *)
    write (u, '(A)') "\begin{document}"
    write (u, '(A)') "\begin{fmffile}{" // filename // "}"
    write (u, '(A)') "\fmfcmd{color magenta; magenta = red + blue;}"
    write (u, '(A)') "\fmfcmd{color cyan; cyan = green + blue;}"
    write (u, '(A)') "\begin{fmfshrink}{0.5}"
    write (u, '(A)') "\begin{flushleft}"
    write (u, *)
    write (u, '(A)') "\noindent" // &
         & "\textbf{\large\texttt{WHIZARD} phase space channels}" // &
         & "\hfill\today"
    write (u, *)
    write (u, '(A)') "\vspace{10pt}"
    call write_process_tex_form (u, process_id, code, n_in, n_out)
    write (u, *)
    write (u, '(A)') "\textbf{Color code:} " // &
         & "{\blue resonance,} " // &
         & "{\cyan t-channel,} " // &
         & "{\green radiation,} " // &
         & "{\red infrared,} " // &
         & "{\magenta collinear,} " // &
         & "external/off-shell"
    write (u, *)
    write (u, '(A)') "\vspace{-20pt}"
    call cascade_set_write (u, final=.true., sort=.true., graph=.true.)
    write (u, '(A)') "\end{flushleft}"
    write (u, '(A)') "\end{fmfshrink}"
    write (u, '(A)') "\end{fmffile}"
    write (u, '(A)') "\end{document}"
  end subroutine phase_space_channels_write_unit

  subroutine cascade_write_fmfgraph_unit (u, cs)
    integer, intent(in) :: u
    type(cascade), intent(in) :: cs
    integer, save :: count = 0
    integer(kind=TC) :: mask
    character(len=BUFFER_SIZE) :: left_str, right_str
    if (cs%deleted)  return
    count = count + 1
    mask = 2**((cs%depth+3)/2) - 1
    left_str = ""
    right_str = ""
    write (u, '(A)') "\begin{minipage}{105pt}"
    write (u, '(A)') "\vspace{30pt}"
    write (u, '(A)') "\begin{center}"
    write (u, '(A)') "\begin{fmfgraph*}(55,55)"
    call node_write_fmf_unit (u, cs%node, mask, left_str, right_str)
    write (u, '(A)') "\fmfleft{" // trim(left_str(2:)) // "}"
    write (u, '(A)') "\fmfright{" // trim(right_str(2:)) // "}"
    write (u, '(A)') "\end{fmfgraph*}\\"
    write (u, '(A,I5,A)') "\fbox{$", count, "$}"
    write (u, '(A)') "\end{center}"
    write (u, '(A)') "\end{minipage}"
    write (u, '(A)') "%"
  end subroutine cascade_write_fmfgraph_unit

  recursive subroutine node_write_fmf_unit &
       (u, node, mask, left_str, right_str, reverse)
    integer, intent(in) :: u
    type(node_t), pointer :: node
    integer(kind=TC), intent(in) :: mask
    character(len=*), intent(inout) :: left_str, right_str
    logical, intent(in), optional :: reverse
    logical :: rev
    rev = .false.;  if (present(reverse))  rev = reverse
    if (node%decays) then
       if (.not.rev) then
          call vertex_write (u, node, node%daughter1, mask)
          call vertex_write (u, node, node%daughter2, mask)
       else
          call vertex_write (u, node, node%daughter2, mask, reverse=.true.)
          call vertex_write (u, node, node%daughter1, mask, reverse=.true.)
       end if
       if (node%final) then
          call vertex_write (u, node, node%mother, mask, reverse=.true.)
          write (u, '(A,I3,A)')  "\fmfv{d.shape=square}{v", node%bincode, "}"
       end if
    else
       if (node%initial) then
          call external_write (u, node%bincode, &
               &               antiparticle_code (node%code), left_str)
       else
          call external_write (u, node%bincode, node%code, right_str)
       end if
    end if
  contains
    subroutine vertex_write (u, node, daughter, mask, reverse)
      integer, intent(in) :: u
      type(node_t), pointer :: node, daughter
      integer(kind=TC), intent(in) :: mask
      logical, intent(in), optional :: reverse
      call node_write_fmf_unit &
           & (u, daughter, mask, left_str, right_str, reverse)
      if (daughter%decays) then
         call line_write (u, node%bincode, daughter%bincode, &
              &           daughter%code, mapping=daughter%mapping)
      else
         call line_write (u, node%bincode, daughter%bincode, &
              &           daughter%code)
      end if
    end subroutine vertex_write
    subroutine line_write (u, i1, i2, code, mapping)
      integer, intent(in) :: u
      integer(kind=TC), intent(in) :: i1, i2
      integer, intent(in) :: code
      integer, intent(in), optional :: mapping
      integer :: k1, k2
      character(len=BUFFER_SIZE) :: prt_type
      select case (particle_spin_type (code))
      case (SCALAR);       prt_type = "plain"
      case (FERMION);      prt_type = "fermion"
      case (VECTOR_BOSON); prt_type = "boson"
      case (GRAVITINO);    prt_type = "fermion"
      case (TENSOR);       prt_type = "dbl_wiggly"
      case default;        prt_type = "dashes"
      end select
      if (code > 0) then
         k1 = i1;  k2 = i2
      else
         k1 = i2;  k2 = i1
      end if
      if (present (mapping)) then
         select case (mapping)
         case (S_CHANNEL)
            write (u, '(A,I3,A,I3,A)') "\fmf{" // trim (prt_type) // &
                 & ",f=blue,lab=\sm\blue$" // &
                 & trim (particle_tex_form (code)) // "$}" // &
                 & "{v", k1, ",v", k2, "}"
         case (T_CHANNEL, U_CHANNEL)
            write (u, '(A,I3,A,I3,A)') "\fmf{" // trim (prt_type) // &
                 & ",f=cyan,lab=\sm\cyan$" // &
                 & trim (particle_tex_form (code)) // "$}" // &
                 & "{v", k1, ",v", k2, "}"
         case (RADIATION)
            write (u, '(A,I3,A,I3,A)') "\fmf{" // trim (prt_type) // &
                 & ",f=green,lab=\sm\green$" // &
                 & trim (particle_tex_form (code)) // "$}" // &
                 & "{v", k1, ",v", k2, "}"
         case (COLLINEAR)
            write (u, '(A,I3,A,I3,A)') "\fmf{" // trim (prt_type) // &
                 & ",f=magenta,lab=\sm\magenta$" // &
                 & trim (particle_tex_form (code)) // "$}" // &
                 & "{v", k1, ",v", k2, "}"
         case (INFRARED)
            write (u, '(A,I3,A,I3,A)') "\fmf{" // trim (prt_type) // &
                 & ",f=red,lab=\sm\red$" // &
                 & trim (particle_tex_form (code)) // "$}" // &
                 & "{v", k1, ",v", k2, "}"
         case default
            write (u, '(A,I3,A,I3,A)') "\fmf{" // trim (prt_type) // &
                 & ",f=black}" // &
                 & "{v", k1, ",v", k2, "}"
         end select
      else
         write (u, '(A,I3,A,I3,A)') "\fmf{" // trim (prt_type) // &
                 & "}" // &
                 & "{v", k1, ",v", k2, "}"
      end if
    end subroutine line_write
    subroutine external_write (u, bincode, code, ext_str)
      integer, intent(in) :: u
      integer(kind=TC), intent(in) :: bincode
      integer, intent(in) :: code
      character(len=*), intent(inout) :: ext_str
      character(len=5) :: str
      write (str, '(A2,I3)') ",v", bincode
      ext_str = trim (ext_str) // str
      write (u, '(A,I3,A,I3,A)') "\fmflabel{\sm$"&
           & // trim ( particle_tex_form (code)) &
           & // "\,(", bincode, ")" &
           & // "$}{v", bincode, "}"
    end subroutine external_write
  end subroutine node_write_fmf_unit

  subroutine make_phase_space_file &
       & (unit, prefix, read_model_file, write_phase_space_channels_file, &
       &  process_id, code, n_in, n_out, par, sqrts, &
       &  n_channels, phs)
    integer, intent(in) :: unit
    character(len=*), intent(in) :: prefix, read_model_file
    character(len=*), intent(in) :: write_phase_space_channels_file, process_id
    integer, dimension(:,:), intent(in) :: code
    integer, intent(in) :: n_in, n_out
    type(parameter_set), intent(in) :: par
    real(kind=default), intent(in) :: sqrts
    integer, intent(out) :: n_channels
    type(phase_space_switches), intent(in) :: phs
    integer :: depth, max_off_shell, off_shell_user
    integer, dimension(size(code,2)) :: i1, i2
    real(kind=default) :: threshold_mass, threshold_mass_t
    off_shell_user = phs%off_shell_lines
    single_off_shell_decays = phs%single_off_shell_decays
    double_off_shell_decays = phs%double_off_shell_decays
    single_off_shell_branchings = phs%single_off_shell_branchings
    double_off_shell_branchings = phs%double_off_shell_branchings
    massive_FSR = phs%massive_FSR
    threshold_mass = phs%threshold_mass
    threshold_mass_t = phs%threshold_mass_t
    call setup_and_read_vertices (prefix, read_model_file)
    call msg_message (" Generating phase space channels for process " &
         &            //trim(process_id)// "...")
    max_off_shell = 0
    max_off_shell = max_off_shell + abs(off_shell_user)
    max_off_shell = max_off_shell + abs(phs%extra_off_shell_lines)
    splitting_depth = phs%splitting_depth
    exchange_lines = phs%exchange_lines
    depth = 2*n_out + 1
    LOOP_OFF_SHELL: do
       call cascade_set_init (code(n_in+1:,:), par, threshold_mass)
       if (n_in == 2) then
          call cascade_set_generate &
               & (par, threshold_mass, sqrts, max_off_shell)
          n_cascades_s = n_cascades
          call cascade_set_add &
               & (ibset(0,n_out), antiparticle_code(code(2,:)), par, &
               &  threshold_mass_t, initial=.true., index=i1)
          call cascade_set_add &
               & (ibset(0,n_out+1), antiparticle_code(code(1,:)), par, &
               &  threshold_mass_t, initial=.true., index=i2)
          call cascade_set_generate &
               & (par, threshold_mass_t, sqrts, depth, &
               &  seed=i1, target=i2, off_shell=max_off_shell)
          call cascade_set_generate &
               & (par, threshold_mass_t, sqrts, depth, &
               &  seed=i2, target=i1, off_shell=max_off_shell)
          n_cascades_t = n_cascades
          call cascade_set_keystone &
               & (par, threshold_mass_t, sqrts, depth, &
               &  seed=i1, target=i2, off_shell=max_off_shell)
       else
          call cascade_set_generate &
               & (par, threshold_mass, sqrts, max_off_shell, depth)
          n_cascades_s = n_cascades
          n_cascades_t = n_cascades
          call cascade_finalize_decay (code(1,1), depth)
       end if
       call cascade_set_select (off_shell=off_shell_user, &
            & extra=phs%extra_off_shell_lines, &
            & remove_deleted=.not.phs%show_deleted_channels)
       call cascade_set_validate
       n_channels = count(cascade_set%final .and. cascade_set%active &
            &             .and. .not.cascade_set%deleted)
       if (n_channels > 0) then
          exit LOOP_OFF_SHELL
       else if (max_off_shell < n_in + n_out - 3) then
          write (msg_buffer, "(1x,A,1x,I1)") &
               & "No phase space channels found with off_shell_lines =", &
               & off_shell_user
          call msg_warning
          call msg_message (" Retrying with off_shell_lines increased by one ...")
          call cascade_set_clear
          max_off_shell = max_off_shell + 1
          off_shell_user = off_shell_user + 1
       else
          call msg_fatal (" No valid phase space channels found - missing vertices in model file?")
       end if
    end do LOOP_OFF_SHELL
    call cascade_set_groves
    write (msg_buffer, "(1x,A,1x,I7,1x,A)") &
         & "Phase space:", n_channels, "phase space channels generated."
    call msg_message
    call msg_message (repeat("-",72), unit=unit)
    call msg_message &
         & (" Automatically generated phase space configuration", unit=unit)
    call write_process (unit, process_id, code, n_in, n_out)
    write (unit, '(A,1x,A)') "process", trim(process_id)
    call cascade_set_write (unit, final=.true., short=.true., sort=.true.)
    write (unit, *)
    call msg_message (" Dummy entry", unit=unit)
    write (unit, '(A,1x,A)') "process", trim(process_id)
    call phase_space_channels_write &
         & (prefix, write_phase_space_channels_file, &
         &  process_id, code, n_in, n_out)
    call cascade_set_clear
    call clear_vertices
  end subroutine make_phase_space_file


end module cascades
