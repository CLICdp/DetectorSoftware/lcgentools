! WHIZARD 1.95 Feb 25 2010
! 
! (C) 1999-2008 by 
!     Wolfgang Kilian <wolfgang.kilian@desy.de>
!     Thorsten Ohl <ohl@physik.uni-wuerzburg.de>
!     Juergen Reuter <juergen.reuter@physik.uni-freiburg.de>
!
! WHIZARD is free software; you can redistribute it and/or modify it
! under the terms of the GNU General Public License as published by 
! the Free Software Foundation; either version 2, or (at your option)
! any later version.
!
! WHIZARD is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! This file has been stripped of most comments.  For documentation, refer
! to the source 'whizard.nw'

module analyze

  use kinds, only: default !NODEP!
!  use kinds, only: double !NODEP!
  use limits, only: MAX_EXTERNAL, CUT_MODE_STRLEN, CUT_TEX_STRLEN
  use limits, only: ANALYZE_MAX, HISTOGRAMS_MAX
  use limits, only: HELICITIES_MAX, FLAVORS_MAX, COLOR_FLOWS_MAX
  use limits, only: FILENAME_LEN, BUFFER_SIZE
  use limits, only: TC
  use diagnostics, only: msg_level, msg_buffer
  use diagnostics, only: msg_message, msg_warning, msg_error
  use files, only: file_exists_else_default, concat
  use constants
  use lorentz
  use particles
  use events
  use file_utils !NODEP!
  use parser
!  use particles, only: particle_tex_form
  use cuts
  use histograms !NODEP!
  use monte_carlo_states

  implicit none
  private

  integer, parameter, public :: TEXT_FMT=0, GML_FMT=1

  type, public :: histogram_data
     private
     integer :: mode
     integer(kind=TC), dimension(2) :: code
     type(histogram) :: h
  end type histogram_data
  type, public :: analysis_data
     private
     integer :: n_histograms
     real(kind=default) :: sum_evt, sum_evt_sq, sum_evt_excess, sum_evt_total
     real(kind=default) :: max_weight
     type(cut_array) :: cc
     integer :: n_col, n_hel, n_flv
     integer, dimension(MAX_EXTERNAL, COLOR_FLOWS_MAX) :: col
     integer, dimension(MAX_EXTERNAL, HELICITIES_MAX) :: hel
     integer, dimension(MAX_EXTERNAL, FLAVORS_MAX) :: flv
     type(histogram_data), dimension(HISTOGRAMS_MAX) :: hh
  end type analysis_data
  type, public :: analysis_bundle
     private
     integer :: n_datasets
     type(analysis_data), dimension(ANALYZE_MAX) :: a
  end type analysis_bundle

  public :: create, destroy
  public :: read, write
  public :: apply
  public :: write_grid_drivers
  public :: write_histories_driver

  interface create
     module procedure analysis_bundle_create
     module procedure analysis_data_create
     module procedure histogram_data_create
  end interface
  interface destroy
     module procedure analysis_bundle_destroy
     module procedure analysis_data_destroy
     module procedure histogram_data_destroy
  end interface
  interface read
     module procedure analysis_bundle_read_name
     module procedure analysis_bundle_read_unit
     module procedure analysis_data_read_unit
  end interface
  interface write
     module procedure analysis_data_write_unit
     module procedure analysis_bundle_write_unit
     module procedure analysis_bundles_write_name
  end interface
  interface apply
     module procedure analysis_bundle_apply
     module procedure analysis_data_apply
     module procedure histogram_data_apply
  end interface

contains

  subroutine analysis_bundle_create(aa)
    type(analysis_bundle), intent(out) :: aa
    aa%n_datasets = 0
  end subroutine analysis_bundle_create
  subroutine analysis_bundle_destroy(aa)
    type(analysis_bundle), intent(inout) :: aa
    integer :: i
    do i=1, aa%n_datasets
       call destroy(aa%a(i))
    end do
    aa%n_datasets = 0
  end subroutine analysis_bundle_destroy
  
  subroutine analysis_data_create(a)
    type(analysis_data), intent(out) :: a
    call create(a%cc)
    a%n_histograms = 0
    a%sum_evt = 0
    a%sum_evt_sq = 0
    a%sum_evt_excess = 0
    a%sum_evt_total = 0
    a%max_weight = 0
    a%n_col = 0
    a%n_hel = 0
    a%n_flv = 0
  end subroutine analysis_data_create
  subroutine analysis_data_destroy(a)
    type(analysis_data), intent(inout) :: a
    integer :: i
    do i=1, a%n_histograms
       call destroy(a%hh(i))
    end do
    a%n_histograms = 0
    a%sum_evt = 0
    a%sum_evt_sq = 0
    a%sum_evt_excess = 0
    a%sum_evt_total = 0
    a%max_weight = 0
    a%n_col = 0
    a%n_hel = 0
    a%n_flv = 0
   call destroy(a%cc)
  end subroutine analysis_data_destroy

  subroutine histogram_data_create(h, mode, code, lower, upper, nbin, step)
    type(histogram_data), intent(out) :: h
    integer, intent(in) :: mode
    integer(kind=TC), dimension(2), intent(in) :: code
    real(kind=default), intent(in) :: lower, upper
    integer, intent(in), optional :: nbin
    real(kind=default), intent(in), optional :: step
    real(kind=default) :: x_min, x_max
    if (mode/=NO_CUT) then
       h%mode = mode
       h%code = code
       if (lower <= upper) then
          x_min = lower
          x_max = upper
       else
          x_min = upper
          x_max = lower
       end if
       if (present(nbin)) then
          if (nbin>0) then
             call create_histogram(h%h, x_min, x_max, nbin)
          else
             call create_histogram(h%h, x_min, x_max, 1)
          end if
       else if (present(step)) then
          if (step/=0) then
             call create_histogram(h%h, x_min, x_max, &
                  &                nint((x_max-x_min)/abs(step)))
          else
             call create_histogram(h%h, x_min, x_max, 1)
          end if
       else
          call create_histogram(h%h, x_min, x_max)
       end if
    end if
  end subroutine histogram_data_create

  subroutine histogram_data_destroy(h)
    type(histogram_data), intent(inout) :: h
    h%mode = NO_CUT
    h%code = (/ 0, 0 /)
    call delete_histogram(h%h)
  end subroutine histogram_data_destroy

  subroutine analysis_bundle_read_name (prefix, filename, aa, process_id)
    character(len=*), intent(in) :: prefix, filename
    type(analysis_bundle), intent(inout) :: aa
    character(len=*), intent(in) :: process_id
    character(FILENAME_LEN) :: file
    integer :: unit
    file = file_exists_else_default (prefix, filename, "cut5")
    if (len_trim (file) /= 0) then
       unit = free_unit()
       call msg_message
       call msg_message (" Reading analysis configuration data from file " &
            &            // trim(file))
       open(unit=unit, file=trim(file), status='old', action='read')
       call analysis_bundle_read_unit(unit, aa, process_id)
       if (aa%n_datasets > 1) then
          write (msg_buffer, "(1x,A,1x,I3,1x,A)") &
               & "Found", aa%n_datasets, "analysis configuration datasets."
          call msg_message
       else if (aa%n_datasets == 1) then
          write (msg_buffer, "(1x,A,1x,I3,1x,A)") &
               & "Found", aa%n_datasets, "analysis configuration dataset."
          call msg_message
       else
          call msg_message (" No analysis data found for process " // &
               &            trim(process_id))
          call analysis_bundle_default (aa)
       end if
       call lex_clear
       close(unit)
    else
       call msg_message (" No analysis configuration file found.")
       call analysis_bundle_default (aa)
    end if
  end subroutine analysis_bundle_read_name

  subroutine analysis_bundle_read_unit(unit, aa, process_id)
    integer, intent(in) :: unit
    type(analysis_bundle), intent(inout) :: aa
    character(len=*), intent(in) :: process_id
    logical :: ok
    integer :: iostat, i
    FIND_PROCESS: do
       call find_string(unit, 'process', ok=ok)
       if (.not.ok) exit FIND_PROCESS
       call find_string(unit, process_id, &
            & reject=(/ 'process  ', 'grove    ', 'tree     ', &
            &           'cut      ', 'helicity ', 'flavor   ', 'color    ', &
            &           'histogram', 'and      ' /), &
            & ok=ok)
       if (ok) exit FIND_PROCESS
    end do FIND_PROCESS
    if (.not.ok) return
    i = 1
    SCAN_ANALYSIS_BUNDLE: do
       call create(aa%a(i))
       call analysis_data_read_unit(unit, aa%a(i))
       aa%n_datasets = i
       i = i+1
       if (i > ANALYZE_MAX) exit SCAN_ANALYSIS_BUNDLE
       call find_string(unit, 'and', here=.true., ok=ok, iostat=iostat)
       if (.not.ok) exit SCAN_ANALYSIS_BUNDLE
    end do SCAN_ANALYSIS_BUNDLE
  end subroutine analysis_bundle_read_unit

  subroutine analysis_data_read_unit(unit, a)
    integer, intent(in) :: unit
    type(analysis_data), intent(inout) :: a
    logical :: ok
    integer :: iostat
    integer :: mode
    integer(kind=TC), dimension(2) :: code
    character(len=CUT_MODE_STRLEN) :: str
    real(kind=default) :: lower, upper, step
    integer :: i, nbin
    call read(unit, a%cc, level=5)
    SCAN_HELICITIES: do
       call find_string (unit, 'color', here=.true., ok=ok, iostat=iostat)
       if (ok) then
          a%n_col = a%n_col+1
          do i=1, MAX_EXTERNAL
             a%col(i, a%n_col) = get_integer(unit, ok, iostat)
          end do
       else
          call find_string (unit, 'helicity', here=.true., ok=ok, iostat=iostat)
          if (ok) then
             a%n_hel = a%n_hel+1
             do i=1, MAX_EXTERNAL
                a%hel(i, a%n_hel) = get_integer(unit, ok, iostat)
             end do
          else
             call find_string (unit, 'flavor', here=.true., ok=ok, iostat=iostat)
             if (ok) then
                a%n_flv = a%n_flv+1
                do i=1, MAX_EXTERNAL
                   a%flv(i, a%n_flv) = get_integer(unit, ok, iostat)
                end do
             else
                exit SCAN_HELICITIES
             end if
          end if
       end if
    end do SCAN_HELICITIES
    SCAN_ANALYZE: do
       call find_string(unit, 'histogram', here=.true., ok=ok, iostat=iostat)
       if (.not.ok) exit SCAN_ANALYZE
       i = a%n_histograms+1
       str = get_string_upper_case(unit)
       select case(trim(str))
         case("- ", "--", "---")
            mode = NO_CUT
         case("M","Q")
            mode = M_CUT
         case("LM","LQ")
            mode = LM_CUT
         case("MSQ","QSQ","S","T","U")
            mode = MSQ_CUT
         case("E")
            mode = E_CUT
         case("LE")
            mode = LE_CUT
         case("PT")
            mode = PT_CUT
         case("LPT")
            mode = LPT_CUT
         case("PL")
            mode = PL_CUT
         case("P")
            mode = P_CUT
         case("RAP", "RAPIDITY", "Y")
            mode = RAP_CUT
         case("ETA")
            mode = ETA_CUT
         case("A", "ANGLE", "TH", "THETA")
            mode = A_CUT
         case("CT","COS(TH)","COS(THETA)")
            mode = CT_CUT
         case("AD", "ANGLE(DEG)", "TH(DEG)", "THETA(DEG)")
            mode = AD_CUT
         case("VBF", "VBF_ETA", "HEMI", "HEMISPHERE")
            mode = VBF_CUT
         case("AA", "ANGLE-ABS", "TH-ABS", "THETA-ABS")
            mode = AA_CUT
         case("CTA","COS(TH-ABS)","COS(THETA-ABS)")
            mode = CTA_CUT
         case("AAD", "ANGLE-ABS(DEG)", "TH-ABS(DEG)", "THETA-ABS(DEG)")
            mode = AAD_CUT
         case("PH", "PHI")
            mode = PHI_CUT
         case("PHD", "PHID", "PHI(DEG)")
            mode = PHID_CUT
         case("DPH", "DELTA-PHI")
            mode = DPHI_CUT
         case("DPHD", "DPHID", "DELTA-PHI(DEG)")
            mode = DPHID_CUT
         case("A*", "ANGLE*", "TH*", "THETA*")
            mode = ASTAR_CUT
         case("CT*","COS(TH*)","COS(THETA*)")
            mode = CTSTAR_CUT
         case("AD*", "ANGLE*(DEG)", "TH*(DEG)", "THETA*(DEG)")
            mode = ADSTAR_CUT
         case("DETA", "DELTA-ETA")
            mode = DETA_CUT
         case("DR", "DELTA-R", "CONE")
            mode = CONE_CUT
         case("LDR", "LOG-DELTA-R", "LOG-CONE")
            mode = LOG_CONE_CUT
       case default
          call msg_message (" histogram "//trim(str)//" ...")
          call msg_error (" This is not a valid histogram specifier.  I'll ignore this.")
          mode = NO_CUT
       end select
       call find_string(unit, 'of', here=.true.)
       code(1) = get_integer(unit)
       code(2) = get_integer(unit, ok)
       if (.not.ok) code(2) = 0
       if (mode>0 .and. ok) then
          write (msg_buffer, "(1x,A,1x,I5,1x,I5)") &
               "histogram "//trim(str)//" of", code(1), code(2)
          call msg_message
          call msg_error (" Two integer arguments found, only one expected.  I'll ignore this entry.")
          exit SCAN_ANALYZE
       else if (mode<0 .and. .not.ok) then
          write (msg_buffer, "(1x,A,1x,I5)")  "histogram "//trim(str)//" of", code(1)
          call msg_message
          call msg_error (" One integer argument found, two expected. I'll ignore this entry.")
          exit SCAN_ANALYZE
       end if
       call find_string(unit, 'within', here=.true.)
       lower = get_real(unit)
       upper = get_real(unit)
       call find_string(unit, 'nbin', here=.true., ok=ok)
       if (ok) then
          nbin = get_integer(unit)
          if (mode/=NO_CUT) &
               & call create(a%hh(i), mode, code, lower, upper, nbin=nbin)
       else
          call find_string(unit, 'step', here=.true., ok=ok)
          if (ok) then
             step = get_real(unit)
             if (mode/=NO_CUT) &
                  & call create(a%hh(i), mode, code, lower, upper, step=step)
          else
             if (mode/=NO_CUT) call create(a%hh(i), mode, code, lower, upper)
          end if
       end if
       a%n_histograms = i
    end do SCAN_ANALYZE
  end subroutine analysis_data_read_unit

  subroutine analysis_bundle_default (aa)
    type(analysis_bundle), intent(inout) :: aa
    call create(aa%a(1))
    aa%n_datasets = 1
  end subroutine analysis_bundle_default

  subroutine analysis_bundles_write_name (filename, fmt, aa, mcs)
    character(len=*), intent(in) :: filename
    integer, intent(in) :: fmt
    type(analysis_bundle), dimension(:), intent(in) :: aa
    type(monte_carlo_state), dimension(:), intent(in) :: mcs
    integer :: u
    character(len=BUFFER_SIZE) :: file
    u = free_unit()
    select case (fmt)
    case (TEXT_FMT)
       do mc_process_index=1, mc_number_processes
          file = concat (mcs(mc_process_index)%directory, filename, &
               &         mcs(mc_process_index)%process_id_current, "dat")
          open (unit=u, action='write', status='replace', file=trim(file))
          call analysis_bundle_write_unit &
               & (u, aa(mc_process_index), mcs(mc_process_index))
          close (u)
       end do
       mc_process_index = mc_number_processes
    case(GML_FMT)
       file = concat (mcs(1)%directory, trim(filename)//"-plots", "tex.in")
       open (unit=u, action='write', status='replace', file=trim(file))
       call gml_bundles_write_unit (u, aa, mcs, trim(filename))
       close (u)
    end select
  end subroutine analysis_bundles_write_name

  subroutine analysis_bundle_write_unit (u, aa, mcs, show_histograms)
    integer, intent(in) :: u
    type(analysis_bundle), intent(in) :: aa
    type(monte_carlo_state), intent(in) :: mcs
    logical, intent(in), optional :: show_histograms
    integer :: i
    do i=1, aa%n_datasets
       call write(u, trim(mcs%process_id_current), aa%a(i), &
            &     mcs%it, mcs%integral, mcs%n_in, mcs%n_out, &
            &     mcs%code, &
            &     unweighted=mcs%unweighted, &
            &     show_histograms=show_histograms, &
            &     show_overflow=mcs%show_overflow, &
            &     show_excess=mcs%show_excess.and.mcs%unweighted)
    end do
  end subroutine analysis_bundle_write_unit

  subroutine analysis_data_write_unit &
       & (u, process_id, a, it, integral, n_in, n_out, code, &
       &  unweighted, show_histograms, show_overflow, show_excess)
    integer, intent(in) :: u
    character(len=*), intent(in) :: process_id
    type(analysis_data), intent(in) :: a
    integer, intent(in) :: it
    real(kind=default), intent(in) :: integral
    integer, intent(in) :: n_in, n_out
    integer, intent(in), dimension(:,:) :: code
    logical, intent(in), optional :: &
         & unweighted, show_histograms, show_overflow, show_excess
    real(kind=default) :: eff, integral_cut, error_cut, accuracy
    logical :: show_hist
    integer :: i
    call mc_write_dline(u)
    if (u /= 6) then
       write(u,*) '! WHIZARD 1.95 (Feb 25 2010)'
       call write_process (u, process_id, code, n_in, n_out)
    end if
    if (u /= 6 .or. msg_level > 3)  call msg_message &
         & (" Analysis results for process "//trim(process_id)//":", unit=u)
    if (cut_number(a%cc)>0) then
       if (u /= 6 .or. msg_level > 3) then
          call msg_message (unit=u)
          call msg_message (" Additional cuts:", unit=u)
          call write(u, a%cc)
       end if
    end if
    if (a%n_col>0) then
       if (u /= 6 .or. msg_level > 3) then
          call msg_message (unit=u)
          call msg_message (" Allowed color flows:", unit=u)
          do i=1, a%n_col
             write (msg_buffer, "(12(1x,I3))") a%col(:n_in+n_out,i)
             call msg_message (unit=u)
          end do
       end if
    end if
    if (a%n_hel>0) then
       if (u /= 6 .or. msg_level > 3) then
          call msg_message (unit=u)
          call msg_message (" Allowed helicity combinations:", unit=u)
          do i=1, a%n_hel
             write (msg_buffer, "(12(1x,I2))") a%hel(:n_out,i)
             call msg_message (unit=u)
          end do
       end if
    end if
    if (a%n_flv>0) then
       if (u /= 6 .or. msg_level > 3) then
          call msg_message (unit=u)
          call msg_message (" Allowed flavor combinations:", unit=u)
          do i=1, a%n_flv
             write (msg_buffer, "(12(1x,I3))") a%flv(:n_out,i)
             call msg_message (unit=u)
          end do
       end if
    end if
    show_hist = .true.
    if (present(show_histograms)) show_hist = show_histograms
    if (show_hist .and. a%n_histograms>0) then
       call msg_message (unit=u)
       call msg_message (" Histograms:", unit=u)
       do i=1, a%n_histograms
          call msg_message (unit=u)
          call histogram_data_write_unit &
               & (u, a%hh(i), show_overflow, show_excess)
       end do
    end if
    if (a%sum_evt_total==0) return
    call mc_write_header (u, n_in, events=unweighted)
    if (a%sum_evt_total > 0) then
       eff = a%sum_evt / a%sum_evt_total
       integral_cut = a%sum_evt * integral / a%sum_evt_total
       error_cut = &
            & sqrt(a%sum_evt_sq) * integral / a%sum_evt_total
    else
       eff = 0
       integral_cut = 0
       error_cut = 0
    end if
    accuracy = mc_get_accuracy (integral_cut, error_cut, a%sum_evt)
    call mc_write_result &
         & (u, it, nint(a%sum_evt), integral_cut, error_cut, eff, accuracy)
    if (a%sum_evt>0 .and. a%sum_evt_excess>0) then
       call mc_write_hline(u)
       write(msg_buffer, "(1x,A,1x,F6.1,7x,A,1x,2PF7.2,A,2x,A,0PF6.2)") &
            & "Excess events:", a%sum_evt_excess, &
            & "(", a%sum_evt_excess / a%sum_evt, "% )", &
            & "| Maximal weight:", a%max_weight
       call msg_warning (unit=u)
    end if
  end subroutine analysis_data_write_unit

  subroutine histogram_data_write_unit(u, h, show_overflow, show_excess)
    integer, intent(in) :: u
    type(histogram_data), intent(in) :: h
    logical, intent(in), optional :: show_overflow, show_excess
    character(len=CUT_MODE_STRLEN) :: str
    select case(h%mode)
    case(M_CUT);  str = "M"
    case(LM_CUT);  str = "LM"
    case(MSQ_CUT);  str = "MSQ"
    case(E_CUT);  str = "E "
    case(LE_CUT);  str = "LE"
    case(PT_CUT);  str = "pT"
    case(LPT_CUT);  str = "pT"
    case(PL_CUT);  str = "pL"
    case(P_CUT);  str = "p "
    case(RAP_CUT);  str = "y"
    case(ETA_CUT);  str = "eta"
    case(A_CUT);  str = "theta"
    case(CT_CUT);  str = "cos(theta)"
    case(AD_CUT);  str = "theta(DEG)"
    case(VBF_CUT); str = "eta*eta"
    case(AA_CUT);  str = "theta(abs)"
    case(CTA_CUT);  str = "cos(theta(abs))"
    case(AAD_CUT);  str = "theta(DEG)"
    case(PHI_CUT);  str = "phi"
    case(PHID_CUT);  str = "phi(DEG)"
    case(DPHI_CUT);  str = "delta-phi"
    case(DPHID_CUT);  str = "dphi(DEG)"
    case(ASTAR_CUT);  str = "theta*"
    case(CTSTAR_CUT);  str = "cos(theta*)"
    case(ADSTAR_CUT);  str = "theta*(DEG)"
    case(DETA_CUT);  str = "delta-eta"
    case(CONE_CUT);  str = "delta-r"
    case(LOG_CONE_CUT);  str = "log-delta-r"
    end select
    if (h%mode /= NO_CUT) then
       write(u, '(1x,A,1x,A,1x,A,1x,I3)', advance='no') &
            & 'histogram', trim(str), 'of', h%code(1)
       if (h%code(2)/=0) then
          write(u, '(1x,I3)', advance='no') h%code(2)
       else
          write(u, '(1x,3x)', advance='no')
       end if
       write(u, '(1x,A)', advance='no') 'within'
       write(u, '(2(1x,1PE12.5))', advance='no') h%h%x_min, h%h%x_max
       write(u, '(2x,A,1x,I4)') 'nbin', h%h%n_bins
       call write_histogram(h%h, u, show_overflow, show_excess)
    end if
  end subroutine histogram_data_write_unit

  subroutine gml_bundles_write_unit (u, aa, mcs, name)
    integer, intent(in) :: u
    type(analysis_bundle), dimension(:), intent(in) :: aa
    type(monte_carlo_state), dimension(:), intent(in) :: mcs
    character(len=*), intent(in) :: name
    integer :: i, j
    write(u, '(A)') "\documentclass[12pt]{article}"
    write(u, *)
    write(u, '(A)') "\usepackage{gamelan}"
    write(u, '(A)') "\usepackage{amsmath}"
    write(u, *)
    write(u, '(A)') "\begin{document}"
    write(u, '(A)') "\begin{gmlfile}"
    write(u, *)
    write(u, '(A)') "\begin{gmlcode}"
    write(u, '(A)') "  color col.default, col.bg, col.excess;"
    write(u, '(A)') "  col.default = black;"
    write(u, '(A)') "  col.bg = 0.9white;"
    write(u, '(A)') "  col.excess  = red;"
    write(u, '(A)') "  numeric penscale.default;"
    write(u, '(A)') "  penscale.default = 1;"
    write(u, '(A)') "  picture symbol.default;"
    write(u, '(A)') "  symbol.default = fshape (circle scaled 5)(withcolor col.default);"
    write(u, '(A)') "  boolean show_excess;"
    if (mcs(1)%plot_excess .and. mcs(1)%unweighted) then
       write(u, '(A)') "  show_excess = true;"
    else
       write(u, '(A)') "  show_excess = false;"
    end if
    write(u, '(A)') "\end{gmlcode}"
    write(u, *)
    do j=1, size (aa)
       do i=1, aa(j)%n_datasets
          if (i>1 .or. j>1) write(u, '(A)') "\newpage"
          write(u, '(A)') repeat("%", 72)
          call gml_data_write_unit &
               & (u, aa(j)%a(i), mcs(j), name)
       end do
    end do
    write(u, *)
    write(u, '(A)') "\end{gmlfile}"
    write(u, '(A)') "\end{document}"
  end subroutine gml_bundles_write_unit

  subroutine gml_data_write_unit (u, a, mcs, name)
    integer, intent(in) :: u
    type(analysis_data), intent(in) :: a
    type(monte_carlo_state), intent(in) :: mcs
    character(len=*), intent(in) :: name
    real(kind=default) :: eff, integral_cut, error_cut
    real(kind=default) :: rel_error, rel_error_cut, accuracy
    character(len=32) :: nbin_string, dx_string, x_low_string, x_high_string
    character(len=CUT_TEX_STRLEN) :: str, unit
    integer :: i
    write(u, '(A)') "\noindent\textbf{\large\texttt{WHIZARD} data analysis}" &
         & // "\hfill\today"
    write(u, '(A)') "\begin{flushleft}"
    call write_process_tex_form (u, trim(mcs%process_id_current), &
         &                       mcs%code, mcs%n_in, mcs%n_out)
    write(u, '(A)') "\\"
    if (mcs%user_weight_mode /= 0) then
       write (u, '(1x,A,I5,1x,A)') "Matrix element reweighted by user-defined function \#", mcs%user_weight_mode, "\\"
    end if
    if (mcs%user_cut_mode /= 0) then
       write (u, '(1x,A,I5,1x,A)') "User-defined preselection cuts applied: Mode #", mcs%user_cut_mode, "\\"
    end if
    if (a%n_col>0) then
       write(u, '(A)') "\textbf{Allowed color flows:} "
       write(u, '(A)') "$"
       do i=1, a%n_col
          write(u, *) "(", a%col(:mcs%n_tot,i), ")\ "
       end do
       write(u, '(A)') "$ \\"
    end if
    if (a%n_hel>0) then
       write(u, '(A)') "\textbf{Allowed helicity combinations:} "
       write(u, '(A)') "$"
       do i=1, a%n_hel
          write(u, *) "(", a%hel(:mcs%n_out,i), ")\ "
       end do
       write(u, '(A)') "$ \\"
    end if
    if (a%n_flv>0) then
       write(u, '(A)') "\textbf{Allowed flavor combinations:} "
       write(u, '(A)') "$"
       do i=1, a%n_flv
          write(u, *) "(", a%flv(:mcs%n_out,i), ")\ "
       end do
       write(u, '(A)') "$ \\"
    end if
    write(u, '(A)') "\end{flushleft}"
    write(u, '(A)') "%"
    if (cut_number(a%cc)>0) then
       write(u, '(A)') "\begin{center}"
       call tex_write(u, a%cc)
       write(u, '(A)') "\end{center}"
       write(u, '(A)') "%"
    end if
    if (mcs%n_in==2) then
       write(u, '(A)') '\begin{displaymath}'
       write(u, 10) '\sqrt{s} = ', mcs%sqrts, '\;\textrm{GeV}\qquad'
       write(u, 10) '\int\mathcal{L} = ', &
            & mcs%luminosity, '\;\textrm{fb}^{-1}'
       write(u, '(A)') '\end{displaymath}'
    else
       write(u, '(A)') '\begin{displaymath}'
       write(u, 10) 'M = ', mcs%sqrts, '\;\textrm{GeV}\\'
       write(u, '(A)') '\end{displaymath}'
    end if
    if (a%n_histograms>0) then
       write(u, '(A)') "\begin{center} \unitlength 1mm"
       if (.not.mcs%unweighted)  write(u, '(A)') "{\small [Weighted events]}"
       do i=1, a%n_histograms
          unit = ""
          select case(a%hh(i)%mode)
          case(M_CUT)
             str  = "$M_{\rm inv}$"
             unit = "[\textrm{GeV}]"
          case(LM_CUT)
             str  = "$\log_{10}M_{\rm inv}/\textrm{GeV}$"
          case(MSQ_CUT)
             str  = "$M^2_{\rm inv}$"
             unit = "[$\textrm{GeV}^2$]"
          case(E_CUT)
             str  = "$E$"
             unit = "[\textrm{GeV}]"
          case(LE_CUT)
             str  = "$\log_{10}E/\textrm{GeV}$"
          case(PT_CUT)
             str  = "$p_\perp$"
             unit = "[\textrm{GeV}]"
          case(LPT_CUT)
             str  = "$\log_{10}p_\perp/\textrm{GeV}$"
          case(PL_CUT)
             str = "$p_\parallel$"
             unit = "[\textrm{GeV}]"
          case(P_CUT)
             str  = "$|\vec p\,|$"
             unit = "[\textrm{GeV}]"
          case(RAP_CUT)
             str  = "$y$"
             unit = "(rap.)"
          case(ETA_CUT) 
                str  = "$\eta$"
          case(A_CUT)
             str  = "$\theta$"
             unit = "[\textrm{rad}]"
          case(CT_CUT)
             str  = "$\cos\theta$"
          case(AD_CUT)
             str  = "$\theta$"
             unit = "[\textrm{deg.}]"
          case(VBF_CUT)
             str  = "$\eta\times\eta$"
          case(AA_CUT)
             str  = "$\theta_{\rm abs}$"
             unit = "[\textrm{rad}]"
          case(CTA_CUT)
             str  = "$\cos\theta_{\rm abs}$"
          case(AAD_CUT)
             str  = "$\theta$"
             unit = "[\textrm{deg.}]"
          case(PHI_CUT)
             str  = "$\phi$"
             unit = "[\textrm{rad}]"
          case(PHID_CUT)
             str  = "$\phi$"
             unit = "[\textrm{deg.}]"
          case(DPHI_CUT)
             str  = "$\Delta\phi$"
             unit = "[\textrm{rad}]"
          case(DPHID_CUT)
             str  = "$\Delta\phi$"
             unit = "[\textrm{deg.}]"
          case(ASTAR_CUT)
             str  = "$\theta^*$"
             unit = "[\textrm{rad}]"
          case(CTSTAR_CUT)
             str  = "$\cos\theta^*$"
          case(ADSTAR_CUT)
             str  = "$\theta^*$"
             unit = "[\textrm{deg.}]"
          case(DETA_CUT) 
             str = "$\Delta\eta$"
          case(CONE_CUT)
             str = "$\sqrt{\Delta\eta^2+\Delta\phi^2}$"
          case(LOG_CONE_CUT)
             str = "$\log_{10}\sqrt{\Delta\eta^2+\Delta\phi^2}$"
          end select
          dx_string = " "; x_low_string = " ";  x_high_string = " "
          write(x_low_string, '(G10.3)') a%hh(i)%h%x_min
          x_low_string = adjustl (x_low_string)
          call lower_case (x_low_string)
          write(x_high_string, '(G10.3)') a%hh(i)%h%x_max
          x_high_string = adjustl (x_high_string)
          call lower_case (x_high_string)
          write(nbin_string, '(G10.3)') a%hh(i)%h%n_bins
          nbin_string = adjustl (nbin_string)
          write(dx_string, '(G10.3)') &
               & (a%hh(i)%h%x_max - a%hh(i)%h%x_min) / (2*a%hh(i)%h%n_bins)
          dx_string = adjustl (dx_string)
          call lower_case (dx_string)
          write(u, '(A)', advance='no') '\begin{gmlgraph*}('
          write(u, *) mcs%plot_width, ', ', mcs%plot_height, ')[dat]'
          write(u, '(2x,A)') '%setup(linear,log); ' &
               & //'graphrange (#'//trim(x_low_string) &
               & //',??), (#'//trim(x_high_string)//',??);'
          write(u, '(2x,A)') 'setup(linear,linear); ' &
               & //'graphrange (#'//trim(x_low_string) &
               & //',#0), (#'//trim(x_high_string)//',??);'
          if (mcs%show_overflow) then
             write(u, '(2x,A)') 'endkey "overflow";'
          else
             write(u, '(2x,A)') 'endkey "!";'
          end if
          if (mcs%plot_histograms) then
             write(u, '(2x,A)') 'boolean show_histogram; show_histogram=true;'
          else
             write(u, '(2x,A)') 'boolean show_histogram; show_histogram=false;'
          end if
          if (mcs%plot_curves) then
             write (u, '(2x,A)')  'boolean show_curve; show_curve=true;'
          else
             write (u, '(2x,A)')  'boolean show_curve; show_curve=false;'
          end if
          if (mcs%plot_smoothly) then
             write (u, '(2x,A)')  'boolean smooth_curve; smooth_curve=true;'
          else
             write (u, '(2x,A)')  'boolean smooth_curve; smooth_curve=false;'
          end if
          if (mcs%plot_symbols) then
             write (u, '(2x,A)')  'boolean show_symbol; show_symbol=true;'
          else
             write (u, '(2x,A)')  'boolean show_symbol; show_symbol=false;'
          end if
          if (mcs%plot_errors) then
             write (u, '(2x,A)')  'boolean show_error; show_error=true;'
          else
             write (u, '(2x,A)')  'boolean show_error; show_error=false;'
          end if
          write(u, '(2x,A)') 'fromfile "' &
               & // name // "." // trim(mcs%process_id_current) // '.dat":' 
          if (mcs%show_overflow) then
             write(u, '(4x,A)') 'key "        underflow";'
          else
             write(u, '(4x,A)') 'key " histogram";'
          end if
          write(u, '(4x,A)') 'nbin := #' // trim (nbin_string)//';'
          write(u, '(4x,A)') 'dx := #'//trim(dx_string)//';'
          write(u, '(4x,A)') 'for i withinblock:' 
          write(u, '(6x,A)') 'get x, y, y.d, y.e;'
          if (mcs%normalize_weight .or. mcs%plot_per_bin) then
             write (u, '(6x,A)') 'if show_curve or show_symbol:'
             write (u, '(8x,A)') 'plot(dat)(x,y);'
             write (u, '(8x,A)') 'if show_excess: ' // &
                  & 'plot(dat.e)(x, y plus y.e); fi'
             write (u, '(6x,A)') 'fi'
             write (u, '(6x,A)') 'if show_histogram: ' 
             write (u, '(8x,A)') 'plot(dat.h)(x,y) hbar dx;'
             write (u, '(8x,A)') 'if show_excess: ' // &
                  & 'plot(dat.he)(x, y plus y.e) hbar dx; fi'
             write (u, '(6x,A)') 'fi'
             write (u, '(6x,A)') 'if show_error: ' 
             write (u, '(8x,A)') 'plot(dat.err)(x,y) vbar y.d;'
             write (u, '(6x,A)') 'fi'
          else
             write (u, '(6x,A)') 'if show_curve or show_symbol:'
             write (u, '(8x,A)') 'plot(dat)(x,y times nbin);'
             write (u, '(8x,A)') 'if show_excess: ' // &
                  & 'plot(dat.e)(x, (y plus y.e) times nbin); fi'
             write (u, '(6x,A)') 'fi'
             write (u, '(6x,A)') 'if show_histogram: ' 
             write (u, '(8x,A)') 'plot(dat.h)(x,y times nbin) hbar dx;'
             write (u, '(8x,A)') 'if show_excess: ' // &
                  & 'plot(dat.he)(x, (y plus y.e) times nbin) hbar dx; fi'
             write (u, '(6x,A)') 'fi'
             write (u, '(6x,A)') 'if show_error: ' 
             write (u, '(8x,A)') 'plot(dat.err)(x,y times nbin) ' // &
                  & 'vbar (y.d times nbin);'
             write (u, '(6x,A)') 'fi'
          end if
          write(u, '(4x,A)') 'endfor'
          write(u, '(2x,A)') 'endfrom'
          write(u, '(2x,A)') 'if show_histogram:'
          write(u, '(4x,A)') 'calculate dat.base(dat.h)(x,#0);'
          write(u, '(4x,A)') 'fill piecewise from(dat.h, dat.base/\) '// &
               & 'withcolor col.bg outlined;'
          write(u, '(4x,A)') 'if show_excess: ' // &
               & 'fill piecewise from(dat.he, dat.h/\) '// &
               & 'withcolor col.excess outlined; fi'
          write (u, '(2x,A)') 'fi'
          write (u, '(2x,A)') 'if show_curve:'
          write (u, '(4x,A)') 'if show_excess: ' // &
               'draw from (dat.e) if smooth_curve: linked smoothly fi withpenscale penscale.default withcolor col.excess; fi'
          write (u, '(4x,A)') &
               'draw from (dat) if smooth_curve: linked smoothly fi withpenscale penscale.default withcolor col.default;'
          write (u, '(2x,A)') 'fi'
          write (u, '(2x,A)') 'if show_symbol:'
          write (u, '(4x,A)') 'if show_excess: ' // &
               'phantom from (dat.e) withsymbol (symbol.default col.excess); fi'
          write (u, '(4x,A)') &
               'phantom from (dat) withsymbol symbol.default;'
          write (u, '(2x,A)') 'fi'
          write (u, '(2x,A)') 'if show_error:'
          write (u, '(4x,A)') &
               'draw piecewise from (dat.err) withpenscale penscale.default withcolor col.default;'
          write (u, '(2x,A)') 'fi'
          if (mcs%normalize_weight) then
             write(u, '(2x,A)') 'label.ulft(<'//'<\#evt/bin>'//'>, out);'
          else if (mcs%plot_per_bin) then
             write(u, '(2x,A)') 'label.ulft(<'//'<$d\sigma\,[{\rm fb}]$/bin>'//'>, out);'
          else
             write(u, '(2x,A)') &
                  'label.ulft(<'//'<$d\sigma\,[{\rm fb}]/d$' // &
                  trim(str) // '>'//'>, out);'
          end if
          write(u, '(2x,A)', advance='no') 'label.bot(<'//'<'
          if (a%hh(i)%code(2)==0) then
             write(u, '(1x,A,1x,A,1x,A,1x,I3,A)', advance='no') &
                  & trim(str), trim(unit), 'of $(', a%hh(i)%code(1), ')$'
          else
             write(u, '(1x,A,1x,A,1x,A,1x,I3,A,I3,A)', advance='no') &
                  & trim(str), trim(unit), 'of $(', a%hh(i)%code(1), ', ', &
                  & a%hh(i)%code(2), ')$'
          end if
          write(u, '(A)') '>'//'>, out);'
          write(u, '(A)') '\end{gmlgraph*}'
       end do
       write(u, '(A)') "\end{center}"
    end if
    if (mcs%integral/=0) then
       rel_error = mcs%error/mcs%integral
    else
       rel_error = 0
    end if
    if (a%sum_evt_total/=0) then
       eff = a%sum_evt / a%sum_evt_total
       integral_cut = mcs%integral * eff
       error_cut = sqrt(a%sum_evt_sq) * mcs%integral / a%sum_evt_total
       if (integral_cut/=0) then
          rel_error_cut = error_cut / integral_cut
       else
          rel_error_cut = 0
       end if
    else
       eff = 1
       integral_cut = 0
       error_cut = 0
       rel_error_cut = 0
    end if
    accuracy = mc_get_accuracy(integral_cut, error_cut, a%sum_evt)
    write(u, '(A)') "%"
10  format(2x,A,G11.4,A)
11  format(2x,A,G12.5,A,G10.3,A/,2x,A,2PF7.2,A)
    if (mcs%n_in==2) then
!        write(u, '(A)') '\begin{displaymath}'
!        write(u, 10) '\sqrt{s} = ', mcs%sqrts, '\;\textrm{GeV}\qquad'
!        write(u, 10) '\int\mathcal{L} = ', &
!             & mcs%luminosity, '\;\textrm{fb}^{-1}'
!        write(u, '(A)') '\end{displaymath}'
       write(u, '(A)') '\begin{align*}'
       write(u, 11) &
            & '\sigma_{\rm tot} &= ', mcs%integral, &
            & ' \quad \pm ', mcs%error, '\;\textrm{fb}', &
            & ' \quad [\pm ', rel_error, '\;\%]'
       write(u, *) '& n_{\rm evt,\ tot} &= ', nint (a%sum_evt_total)
       write(u, *) '\\'
       write(u, 11) &
            & '\sigma_{\rm cut} &= ', integral_cut, &
            & ' \quad \pm ', error_cut, '\;\textrm{fb}', &
            & ' \quad [\pm ', rel_error_cut, '\;\%]'
       write(u, *) '& n_{\rm evt,\ cut} &= ', nint (a%sum_evt), ' \quad '
       write(u, 12) '[ ', 100*eff, ' \;\%]'
       write(u, '(A)') '\end{align*}'
    else
       write(u, '(A)') '\begin{align*}'
!       write(u, 10) 'M &=& ', mcs%sqrts, '\;\textrm{GeV}\\'
       write(u, 11) &
            & '\Gamma &=& ', mcs%integral, &
            & ' \quad \pm ', mcs%error, '\;\textrm{GeV}', &
            & ' \quad [\pm ', rel_error, '\;\%]'
       write(u, *) '& n_{\rm evt,\ tot} &= ', nint (a%sum_evt_total)
       write(u, *) '\\'
       write(u, 11) &
            & '\Gamma_{\rm cut} &=& ', integral_cut, &
            & ' \quad \pm ', error_cut, '\;\textrm{GeV}', &
            & ' \quad [\pm ', rel_error_cut, '\;\%]\\'
       write(u, *) '& n_{\rm evt,\ cut} &= ', nint (a%sum_evt), ' \quad '
       write(u, 12) '[ ', 100*eff, ' \;\%]'
       write(u, '(A)') '\end{align*}'
    end if
12  format(2x,A,F7.2,A)
    write(u, '(A)') "%"
    write(u, '(A)') "%"
!     if (a%sum_evt_excess>0) then
!        call mc_write_hline(u)
!        write(u,'(1x,A,1x,F6.1,7x,A,1x,2PF7.2,1x,A)') &
!             & '! Excess events:', a%sum_evt_excess, &
!             & '(Error[%]:', a%sum_evt_excess / a%sum_evt, ')'
!     end if
!     write(u, '(A)') "\end{verbatim}"
  contains
      subroutine lower_case (string)
        character(len=*), intent(inout) :: string
        integer :: i
        i = index (string, "E")
        if (i>0)  string(i:i) = "e"
      end subroutine lower_case
  end subroutine gml_data_write_unit

  subroutine analysis_bundle_apply (aa, evt, weight, excess)
    type(analysis_bundle), intent(inout) :: aa
    type(event), intent(in) :: evt
    real(kind=default), intent(in), optional :: weight, excess
    integer :: i
    do i=1, aa%n_datasets
       call apply (aa%a(i), evt, weight, excess)
    end do
  end subroutine analysis_bundle_apply

  subroutine analysis_data_apply (a, evt, weight, excess)
    type(analysis_data), intent(inout) :: a
    type(event), intent(in) :: evt
    real(kind=default), intent(in), optional :: weight, excess
    real(kind=default) :: wgt
    logical :: ok
    integer :: i, j
    if (present (weight)) then
       a%sum_evt_total = a%sum_evt_total + weight
    else
       a%sum_evt_total = a%sum_evt_total + 1
    end if
    call apply(a%cc, evt, ok)
    if (.not.ok) return
    CHECK_HELICITY: do i=1, a%n_hel
       ok = all (a%hel(:evt%n_out,i) == evt%hel_out)
       if (ok) exit CHECK_HELICITY
    end do CHECK_HELICITY
    if (.not.ok) return
    CHECK_FLAVOR: do i=1, a%n_flv
       ok = .true.
       do j=1, evt%n_out
          if (a%flv(j,i) /= particle_code(evt%prt(j))) ok = .false.
       end do
       if (ok) exit CHECK_FLAVOR
    end do CHECK_FLAVOR
    if (.not.ok) return
    CHECK_COLOR: do i=1, a%n_col
       ok = all (a%col(:evt%n_in,i) == evt%anticolor_flow(:evt%n_in)) &
            & .and. all (a%col(evt%n_in+1:evt%n_tot,i) &
            &            == evt%color_flow(evt%n_in+1:))
       if (ok) exit CHECK_COLOR
    end do CHECK_COLOR
    if (.not.ok) return
    if (present(weight)) then
       wgt = weight * evt%mc_function_ratio
    else
       wgt = evt%mc_function_ratio
       if (present(excess)) then
          a%sum_evt_excess = a%sum_evt_excess + excess
          if (excess > a%max_weight)  a%max_weight = 1 + excess
       end if
    end if
    a%sum_evt = a%sum_evt + wgt
    a%sum_evt_sq = a%sum_evt_sq + wgt**2
    do i=1, a%n_histograms
       call histogram_data_apply (a%hh(i), evt, wgt, excess)
    end do
  end subroutine analysis_data_apply

  subroutine histogram_data_apply (h, evt, weight, excess)
    type(histogram_data), intent(inout) :: h
    type(event), intent(in) :: evt
    real(kind=default), intent(in), optional :: weight, excess
    real(kind=default) :: value
    type(four_momentum), dimension(2) :: p
    integer :: i
    do i=1,2
       if (h%code(i)/=0) call calculate_momentum_sum (p(i), evt, h%code(i))
    end do
    select case(h%mode)
    case(M_CUT)
       value = p(1)**1
    case(LM_CUT)
       value = abs(p(1)**1)
       if (value/=0) then
          value = log10(value)
       else
          value = -huge(1._default)
       end if
    case(MSQ_CUT)
       value = p(1)**2
    case(E_CUT)
       value = energy(p(1))
    case(LE_CUT)
       value = energy(p(1))
       if (value>0) then
          value = log10(value)
       else
          value = -huge(1._default)
       end if
    case(PT_CUT)
       value = transverse_momentum(p(1))
    case(LPT_CUT)
       value = transverse_momentum(p(1))
       if (value>0) then
          value = log10(value)
       else
          value = -huge(1._default)
       end if
    case(PL_CUT)
       value = longitudinal_momentum(p(1))
    case(P_CUT)
       value = space_part(p(1))**1
    case(RAP_CUT)
       value = rapidity (p(1))
    case(ETA_CUT)
       value = pseudorapidity (p(1))
    case(A_CUT)
       value = angle (p(1), p(2))
    case(CT_CUT)
       value = angle_ct (p(1), p(2))
    case(AD_CUT)
       value = angle (p(1), p(2)) / degree
       if (value>180)  value = 180
    case(VBF_CUT)
       value = pseudorapidity (p(1)) * pseudorapidity (p(2))
    case(AA_CUT)
       value = polar_angle (p(1))
    case(CTA_CUT)
       value = polar_angle_ct (p(1))
    case(AAD_CUT)
       value = polar_angle (p(1)) / degree
       if (value>180)  value = 180
    case(PHI_CUT)
       value = azimuthal_angle (p(1))
    case(PHID_CUT)
       value = azimuthal_angle (p(1)) / degree
       if (value>360) then
          value = 360
       else if (value<0) then
          value = 0
       end if
    case(DPHI_CUT)
       value = azimuthal_distance (p(1), p(2))
    case(DPHID_CUT)
       value = azimuthal_distance (p(1), p(2)) / degree
       if (value>180) then
          value = 180
       else if (value<-180) then
          value = -180
       end if
    case(ASTAR_CUT)
       if (invariant_mass(p(2)) > 0) then
          value = angle (space_part (boost(-p(2), invariant_mass(p(2))) &
               &         * p(1)), &
               &         space_part(p(2)))
       else
          value = 0
       end if
    case(CTSTAR_CUT)
       if (invariant_mass(p(2)) > 0) then
          value = angle_ct (space_part(boost(-p(2), invariant_mass(p(2))) &
               &            * p(1)), &
               &            space_part(p(2)))
       else
          value = 0
       end if
    case(ADSTAR_CUT)
       if (invariant_mass(p(2)) > 0) then
          value = angle (space_part(boost(-p(2), invariant_mass(p(2))) &
               &         * p(1)), &
               &         space_part(p(2))) / degree
          if (value>180)  value = 180
       else
          value = 0
       end if
    case(DETA_CUT)
       value = pseudorapidity_distance (p(1), p(2))
    case(CONE_CUT)
       value = eta_phi_distance (p(1), p(2))
    case(LOG_CONE_CUT)
       value = log10 (eta_phi_distance (p(1), p(2)))
    end select
    call fill_histogram (h%h, value, weight, excess)
  end subroutine histogram_data_apply

  subroutine write_grid_drivers (filename, mcs)
    character(len=*), intent(in) :: filename
    type(monte_carlo_state), dimension(:), intent(in) :: mcs
    integer :: u, i
    do i=1, size (mcs)
       call write_grid_driver (filename, mcs(i))
    end do
    u = free_unit ()
    open (u, file = trim(filename) // "-grids.tex", &
         & action = "write", status = "replace")
    write (u, '(A)') "\documentclass[12pt]{article}"
    write (u, *)
    write (u, '(A)') "\usepackage{gamelan}"
    write (u, '(A)') "\renewcommand{\textfraction}{0}"
    write (u, *)
    write (u, '(A)') "\begin{document}"
    do i=1, size (mcs)
       write (u, '(A,A,A)') "\include{" &
            & // trim(filename) // "-grids." &
            & // trim(mcs(i)%process_id_current), "}"
    end do
    write (u, '(A)') "\end{document}"
    close (u)
  end subroutine write_grid_drivers
  subroutine write_grid_driver (filename, mcs)
    character(len=*), intent(in) :: filename
    type(monte_carlo_state), intent(in) :: mcs
    integer :: u, i, n, d, ch, mode
    integer :: n_ch, n_ch_show
    integer :: n_dim_masses, n_dim_angles, n_dim_momenta
    integer :: n_dim_extra, n_dim_integration
    integer, parameter :: MSQ = 1, PHI = 2, THETA = 3, STRFUN = 4
    logical, dimension(mcs%f%n_channels) :: show_ch
    n_ch = mcs%f%n_channels
    call scan_string (trim(mcs%plot_grids_channels))
    n_dim_masses  = mcs%f%n_dim_masses
    n_dim_angles  = mcs%f%n_dim_angles
    n_dim_momenta = mcs%f%n_dim_momenta
    n_dim_extra   = mcs%f%n_dim_extra
    n_dim_integration = mcs%f%n_dim_integration
    u = free_unit ()
    open (u, &
         & file = trim(filename) // "-grids." // trim(mcs%process_id_current) &
         &        //".tex",&
         & action = "write", status = "replace")
    write (u, '(A)') "%\documentclass[12pt]{article}"
    write (u, *)
    write (u, '(A)') "%\usepackage{gamelan}"
    write (u, '(A)') "%\renewcommand{\textfraction}{0}"
    write (u, *)
    write (u, '(A)') "%\begin{document}"
    write (u, '(A)') "\begin{gmlfile}[" // trim(filename) &
         & // "-grids." // trim(mcs%process_id_current) // "]"
    write (u, '(A)') "\noindent\textbf{\large\texttt{WHIZARD} grid analysis}" &
         & // "\hfill\today"
    write (u, '(A)') "\begin{flushleft}"
    write (u, '(A)') "\textbf{Process:} "
    write (u, '(2x,A)', advance='no') "\verb|" &
         & // trim(mcs%process_id_current) // "| ("
    write (u, '(A)', advance='no') "$"
    do i=1, mcs%n_tot
       write (u, '(1x,A)', advance='no') trim(particle_tex_form(mcs%code(i,1)))
       if (i==mcs%n_in) write(u, '(1x,A)', advance='no') "\to"
    end do
    write (u, '(A)') "$) \\"
    write (u, '(A,I6,A)') "Channels total:", n_ch, "\\"
    write (u, '(A,I6,A)') "Integration dimension:", n_dim_integration
    write (u, '(A,I6,A)') " (masses: ", n_dim_masses, ", "
    write (u, '(A,I6,A)') " angles: ", n_dim_angles, ", "
    write (u, '(A,I6,A)') " strfun: ", n_dim_extra, ")\\"
    write (u, '(A)') "\begin{gmlcode}"
    write (u, '(2x,A,I6,A)') 'n_dim_integration := ', n_dim_integration, ';'
    write (u, '(2x,A,F17.10,A)') 'min_logplot := #', mcs%plot_grids_logscale, ';'
    write (u, '(2x,A)') 'boolean logplot[];'
    write (u, '(2x,A)') 'fromfile "' // trim(filename) // '.' &
         &          // trim(mcs%process_id_current) // '.grb' // '":'
    write (u, '(2x,A)') '  endkey "end";'
    write (u, '(2x,A,I6,A)') '  for ch = 1 upto ', n_ch_show, ':'
    write (u, '(2x,A)') '    numeric w_div[];'
    write (u, '(2x,A)') '    key " begin g%num_div";'
    write (u, '(2x,A)') '    for i withinblock:'
    write (u, '(2x,A)') '      get k,n_div;  w_div[i]:=one over n_div;'
    write (u, '(2x,A)') '    endfor;'
    write (u, '(2x,A)') '    for dm = 1 upto n_dim_integration:'
    write (u, '(2x,A)') '      n := (ch-1)*n_dim_integration + dm;'
    write (u, '(2x,A)') '      numeric x[];'
    write (u, '(2x,A)') '      key " begin d%x";'
    write (u, '(2x,A)') '      for i withinblock:  get k,x[i-1];  endfor;'
    write (u, '(2x,A)') '      logplot[n] := false;'
    write (u, '(2x,A)') '      i := 0;'
    write (u, '(2x,A)') '      forever:'
    write (u, '(2x,A)') '        i := i+1;  exitif unknown x[i];'
    write (u, '(2x,A)') '        h := w_div[dm] over (x[i] minus x[i-1]);'
    write (u, '(2x,A)') '        if h greater min_logplot: logplot[n] := true; fi'
    write (u, '(2x,A)') '        plot(grd[n])(#(i-1),h),(#i,h);'
    write (u, '(2x,A)') '      endfor;'
    write (u, '(2x,A)') '    endfor;'
    write (u, '(2x,A)') '  endfor;'
    write (u, '(2x,A)') 'endfrom;'
    write (u, '(A)') "\end{gmlcode}"
    write (u, '(A)') "\unitlength1mm"
    ch = 0
    do n=1, n_ch_show * n_dim_integration
       if (mod (n, n_dim_integration) == 1) then
          do
             ch = ch + 1;  if (show_ch(ch) .or. ch > n_ch)  exit
          end do
       end if
       write (u, *)
       d = mod (n-1, n_dim_integration) + 1
       if (d==1 .or. d==n_dim_masses+1)  write (u, '(A)')  "\clearpage"
       if (d <= n_dim_masses) then
          i = d
          mode = MSQ
       else if (d <= n_dim_momenta) then
          i = (d-n_dim_masses-1)/2 + 1
          select case (mod(d-n_dim_masses,2))
          case (1);  mode = PHI
          case (0);  mode = THETA
          end select
       else
          i = d - n_dim_momenta
          mode = STRFUN
       end if
       select case (mode)
       case (MSQ, PHI, STRFUN);  write (u, '(A)') "\begin{figure}[ht]"
       end select
       write (u, '(A)') "\begin{flushleft}"
       write (u, '(A,I6,A)') "\textbf{Channel \#}", ch, ": "
       write (u, '(A,I3,A)') "Integration dimension \#", d, ","
       select case (mode)
       case (MSQ)
          write (u, '(A,I3,A)') "Invariant mass squared \#", d, "\\"
       case (PHI)
          write (u, '(A,I3,A)') "Azimuthal angle \#", i, "\\"             
       case (THETA)
          write (u, '(A,I3,A)') "Polar angle \#", i, "\\"             
       case (STRFUN)
          write (u, '(A,I3,A)') "Structure function \#", i, "\\"
       end select
       write (u, '(A)') "\begin{gmlgraph}(115,70)"
       write (u, '(2x,A,I4,A)') "n := ", n, ";"
       write (u, '(2x,A)') "if logplot[n]: setup(linear,log); fi"
       write (u, '(2x,A)') "draw from (grd[n]);"
       write (u, '(A)') "\end{gmlgraph}"
       write (u, '(A)') "\end{flushleft}"
       select case (mode)
       case (MSQ, THETA, STRFUN);  write (u, '(A)') "\end{figure}"
       end select
    end do
    write(u, *)
    write(u, '(A)') "\end{flushleft}"
    write(u, '(A)') "\end{gmlfile}"
    write(u, '(A)') "%\end{document}"
    close (u)
  contains
    subroutine scan_string (string)
      character(len=*), intent(in) :: string
      integer :: u, iostat, ch
      if (trim (adjustr (string)) == "all") then
         show_ch = .true.
      else
         show_ch = .false.
         u = free_unit ()
         open (u, status="scratch")
         write (u, "(A)")  string
         rewind (u)
         SCAN_TOKENS: do
            ch = get_integer (u, iostat=iostat)
            if (iostat /= 0)  exit SCAN_TOKENS
            if (ch > 0 .and. ch <= n_ch)  show_ch(ch) = .true.
         end do SCAN_TOKENS
         close (u)
      end if
      n_ch_show = count (show_ch)
    end subroutine scan_string
  end subroutine write_grid_driver

  subroutine write_histories_driver (filename, mcs)
    character(len=*), intent(in) :: filename
    type(monte_carlo_state), dimension(:), intent(in) :: mcs
    integer :: u
    if (mcs(1)%plot_history) then
       u = free_unit ()
       open (u, file = trim(filename) // "-history.tex", &
            & action = "write", status = "replace")
       call gml_histories_write_unit (u, mcs, filename)
       close (u)
    end if
  end subroutine write_histories_driver

  subroutine gml_histories_write_unit (u, mcs, name)
    integer, intent(in) :: u
    type(monte_carlo_state), dimension(:), intent(in) :: mcs
    character(len=*), intent(in) :: name
    integer :: j
    write(u, '(A)') "\documentclass[12pt]{article}"
    write(u, *)
    write(u, '(A)') "\usepackage{gamelan}"
    write(u, '(A)') "\usepackage{amsmath}"
    write(u, *)
    write(u, '(A)') "\begin{document}"
    write(u, '(A)') "\begin{gmlfile}"
    write(u, *)
    write(u, '(A)') "\begin{gmlcode}"
    write(u, '(A)') "  color col.default;"
    write(u, '(A)') "  col.default = 0.9white;"
    write(u, '(A)') "  picture dot; dot = image (fill circle scaled 1.5mm);"
    write(u, '(A)') "\end{gmlcode}"
    write(u, *)
    do j=1, size (mcs)
       if (j>1) write(u, '(A)') "\newpage"
       write(u, '(A)') repeat("%", 72)
       call gml_history_write_unit (u, mcs(j), name)
    end do
    write(u, *)
    write(u, '(A)') "\end{gmlfile}"
    write(u, '(A)') "\end{document}"
  end subroutine gml_histories_write_unit

  subroutine gml_history_write_unit (u, mcs, name)
    integer, intent(in) :: u
    type(monte_carlo_state), intent(in) :: mcs
    character(len=*), intent(in) :: name
    real(kind=default) :: rel_error
    write(u, '(A)') "\noindent\textbf{\large\texttt{WHIZARD} " &
         & // "integration results}\hfill\today"
    write(u, '(A)') "\begin{flushleft}"
    call write_process_tex_form (u, trim(mcs%process_id_current), &
         &                       mcs%code, mcs%n_in, mcs%n_out)
    write(u, '(A)') "\\"
    if (mcs%user_weight_mode /= 0) then
       write (u, '(1x,A,I5,1x,A)') "Matrix element reweighted by user-defined function \#", mcs%user_weight_mode, "\\"
    end if
    if (mcs%user_cut_mode /= 0) then
       write (u, '(1x,A,I5,1x,A)') "User-defined preselection cuts applied: Mode #", mcs%user_cut_mode, "\\"
    end if
    write(u, '(A)') "\end{flushleft}"
    write(u, '(A)') "%"
    if (mcs%n_in==2) then
       write(u, '(A)') '\begin{displaymath}'
       write(u, 10) '\sqrt{s} = ', mcs%sqrts, '\;\textrm{GeV}\qquad'
       write(u, '(A)') '\end{displaymath}'
    else
       write(u, '(A)') '\begin{displaymath}'
       write(u, 10) 'M = ', mcs%sqrts, '\;\textrm{GeV}\\'
       write(u, '(A)') '\end{displaymath}'
    end if
    write(u, '(A)') "\begin{center} \unitlength 1mm"
    write(u, '(A)', advance='no') '\begin{gmlgraph*}('
    write(u, *) mcs%plot_width, ', ', mcs%plot_height, ')[dat]'
    write(u, '(A)') '  comment "!";'
    write(u, '(A)') '  endkey "&process_input";'
    write(u, '(A)') '  sigma.min := sigma.max := zero;'
    write(u, '(A)') '  integral.lo := integral.hi := zero;'
    write(u, '(A)') '  it := one;'
    write(u, '(2x,A)') 'fromfile "' &
         & // name // "." // trim(mcs%process_id_current) // '.out":' 
    write(u, '(4x,A)') 'for i withinblock:' 
    write(u, '(6x,A)') 'get it, calls, integral, error;'
    write(u, '(6x,A)') 'plot(dat)(it,integral) vbar error;'
    write(u, '(6x,A)') 'integral.lo := integral minus error;'
    write(u, '(6x,A)') 'integral.hi := integral plus error;'
    write(u, '(6x,A)') 'sigma.min :='
    write(u, '(6x,A)') '  if sigma.min = zero: integral.lo'
    write(u, '(6x,A)') '  elseif sigma.min greater integral.lo: integral.lo'
    write(u, '(6x,A)') '  else: sigma.min'
    write(u, '(6x,A)') '  fi;'
    write(u, '(6x,A)') 'sigma.max :='
    write(u, '(6x,A)') '  if sigma.max less integral.hi: integral.hi'
    write(u, '(6x,A)') '  else: sigma.max'
    write(u, '(6x,A)') '  fi;'
    write(u, '(4x,A)') 'endfor'
    write(u, '(2x,A)') 'endfrom'
    write(u, '(2x,A)') 'if sigma.max greater zero:'
    write(u, '(2x,A)') '  sigma.max := #1.05 times sigma.max;'
    write(u, '(2x,A)') '  sigma.min := #0.95 times sigma.min;'
    write(u, '(2x,A)') 'else:'
    write(u, '(2x,A)') '  sigma.max := #1;'
    write(u, '(2x,A)') '  sigma.min := #-1;'
    write(u, '(2x,A)') 'fi'
    write(u, '(2x,A)') 'sigma.lo := integral.lo;'
    write(u, '(2x,A)') 'sigma.hi := integral.hi;'
    write(u, '(2x,A)') 'it.lo := zero;'
    write(u, '(2x,A)') 'it.hi := it plus one;'
    write(u, '(2x,A)') 'setup(linear,linear); graphrange (#0.5,sigma.min), (it plus #0.5,sigma.max);'
    write(u, '(2x,A)') 'plot(dat.band) (it.lo, sigma.lo), (it.hi, sigma.lo);'
    write(u, '(2x,A)') 'plot(dat.band) (it.hi, sigma.hi), (it.lo, sigma.hi);'
    write(u, '(2x,A)') 'fill from (dat.band) withcolor col.default;'
    write(u, '(2x,A)') 'draw piecewise from(dat) withsymbol dot withticks;'
    if (mcs%n_in==2) then
       write(u, '(2x,A)') 'label.ulft(<'//'<$\sigma$ [fb]>'//'>, out);'
    else
       write(u, '(2x,A)') 'label.ulft(<'//'<$\Gamma$ [GeV]>'//'>, out);'
    end if
    write(u, '(2x,A)') 'label.bot(<'//'<Iteration>'//'>, out);'
    write(u, '(A)') '\end{gmlgraph*}'
    write(u, '(A)') "\end{center}"
    write(u, '(A)') "%"
10  format(2x,A,G11.4,A)
11  format(2x,A,G12.5,A,G10.3,A/,2x,A,2PF7.2,A)
    if (mcs%integral/=0) then
       rel_error = mcs%error/mcs%integral
    else
       rel_error = 0
    end if
    if (mcs%n_in==2) then
       write(u, '(A)') '\begin{align*}'
       write(u, 11) &
            & '\sigma_{\rm tot} &= ', mcs%integral, &
            & ' \quad \pm ', mcs%error, '\;\textrm{fb}', &
            & ' \quad [\pm ', rel_error, '\;\%]'
       write(u, '(A)') '\end{align*}'
    else
       write(u, '(A)') '\begin{align*}'
       write(u, 11) &
            & '\Gamma &=& ', mcs%integral, &
            & ' \quad \pm ', mcs%error, '\;\textrm{GeV}', &
            & ' \quad [\pm ', rel_error, '\;\%]'
       write(u, '(A)') '\end{align*}'
    end if
12  format(2x,A,F7.2,A)
    write(u, '(A)') "%"
    write(u, '(A)') "%"
  end subroutine gml_history_write_unit


end module analyze
