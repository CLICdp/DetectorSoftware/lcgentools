! WHIZARD 1.95 Feb 25 2010
! 
! (C) 1999-2008 by 
!     Wolfgang Kilian <wolfgang.kilian@desy.de>
!     Thorsten Ohl <ohl@physik.uni-wuerzburg.de>
!     Juergen Reuter <juergen.reuter@physik.uni-freiburg.de>
!
! WHIZARD is free software; you can redistribute it and/or modify it
! under the terms of the GNU General Public License as published by 
! the Free Software Foundation; either version 2, or (at your option)
! any later version.
!
! WHIZARD is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! This file has been stripped of most comments.  For documentation, refer
! to the source 'whizard.nw'
!
! Replacement for the CIRCE2 subroutines called by WHIZARD
! This file can be linked if CIRCE2 is absent or disabled.

subroutine cir2ld (file, design, roots, ierror)
  implicit none
  character*(*) file, design
  double precision roots
  integer ierror
  stop "The CIRCE2 module is absent.  Please recompile with CIRCE2 enabled."
end subroutine cir2ld

double precision function cir2dn (p1, h1, p2, h2, yy1, yy2)
  implicit none
  integer p1, h1, p2, h2
  double precision yy1, yy2
  cir2dn = 0
  stop "The CIRCE2 module is absent.  Please recompile with CIRCE2 enabled."
end function cir2dn

double precision function cir2lm (p1, h1, p2, h2)
  implicit none
  integer p1, h1, p2, h2
  cir2lm = 0
  stop "The CIRCE2 module is absent.  Please recompile with CIRCE2 enabled."
end function cir2lm

subroutine cir2ch (p1, h1, p2, h2, rng)
  implicit none
  integer p1, h1, p2, h2
  external rng
  p1 = 0
  h1 = 0
  p2 = 0
  h2 = 0
end subroutine cir2ch
  
subroutine cir2gn (p1, h1, p2, h2, x1, x2, rng)
  implicit none
  integer p1, h1, p2, h2
  double precision x1, x2
  external rng
  x1 = 0
  x2 = 0
end subroutine cir2gn

