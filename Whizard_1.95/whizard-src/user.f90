!========================================================================
! File: user.f90.default
!
! This is a template for user-defined functions to be called by WHIZARD.
! Copy this code to 'user.f90' and modify it according to your needs.
! The code will automatically be compiled and included in the WHIZARD
! executable. 
!
! There are four places where user code can be inserted:
! 1) User-defined beam spectra
! 2) User-defined structure functions
! 3) User-defined cut algorithms
! 4) User-defined reweighting functions for the squared matrix element
! 5) User-defined fragmentation
!========================================================================

module user

  use kinds, only: default, double, single !NODEP!
  use kinds, only: i32, i64 !NODEP!
  use file_utils, only: free_unit !NODEP!
  use limits, only: PYTHIA_PARAMETERS_LEN
  use lorentz
  use particles
  use diagnostics, only: msg_level
  implicit none
  private

  public :: spectrum_single, spectrum_double
  public :: spectrum_prt_out, spectrum_beam_remnant

  public :: strfun_single, strfun_double
  public :: strfun_prt_out, strfun_beam_remnant

  public :: pdf_init, pdf_strfun_array

  public :: cut
  public :: weight

  public :: fragment_init, fragment_call, fragment_end

  integer(kind=default) :: ncall_spectrum=0
  integer(kind=default) :: ncall_spectrum_overflow=0

  !character(len=100) :: lumi_ee_file='/tmp/lumi_linker_directory/lumi_ee_linker_000'
  !character(len=100) :: lumi_eg_file='/tmp/lumi_linker_directory/lumi_eg_linker_000'
  !character(len=100) :: lumi_ge_file='/tmp/lumi_linker_directory/lumi_ge_linker_000'
  !character(len=100) :: lumi_gg_file='/tmp/lumi_linker_directory/lumi_gg_linker_000'
  !character(len=100) :: lumi_file='/tmp/lumi_linker_directory/lumi_linker_000'
  !character(len=100) :: photons_beam1_file='/tmp/lumi_linker_directory/photons_beam1_linker_000'
  !character(len=100) :: photons_beam2_file='/tmp/lumi_linker_directory/photons_beam2_linker_000'
  !character(len=100) :: ebeam_in_file='/tmp/lumi_linker_directory/ebeam_in_linker_000'
  !character(len=100) :: pbeam_in_file='/tmp/lumi_linker_directory/pbeam_in_linker_000'

  character(len=200) :: lumi_ee_file
  character(len=200) :: lumi_eg_file
  character(len=200) :: lumi_ge_file
  character(len=200) :: lumi_gg_file
  character(len=200) :: lumi_file
  character(len=200) :: photons_beam1_file
  character(len=200) :: photons_beam2_file
  character(len=200) :: ebeam_in_file
  character(len=200) :: pbeam_in_file

  real(kind=double) :: yy_electron_peak
  real(kind=double) :: photons_beam1_factor
  real(kind=double) :: photons_beam2_factor

  integer :: ndiv_eg_lumi
  real(kind=double), dimension(:,:), pointer :: yl_eg_lumi
  real(kind=double), dimension(:,:), pointer :: yu_eg_lumi
  real(kind=double), dimension(:,:), pointer :: rho_single_eg_lumi
  real(kind=double), dimension(:,:), pointer :: rho_corr_eg_lumi
  real(kind=double), dimension(2) :: avg_energy_eg_lumi

  integer :: ndiv_ge_lumi
  real(kind=double), dimension(:,:), pointer :: yl_ge_lumi
  real(kind=double), dimension(:,:), pointer :: yu_ge_lumi
  real(kind=double), dimension(:,:), pointer :: rho_single_ge_lumi
  real(kind=double), dimension(:,:), pointer :: rho_corr_ge_lumi
  real(kind=double), dimension(2) :: avg_energy_ge_lumi

  integer :: ndiv_gg_lumi
  real(kind=double), dimension(:,:), pointer :: yl_gg_lumi
  real(kind=double), dimension(:,:), pointer :: yu_gg_lumi
  real(kind=double), dimension(:,:), pointer :: rho_single_gg_lumi
  real(kind=double), dimension(:,:), pointer :: rho_corr_gg_lumi
  real(kind=double), dimension(2) :: avg_energy_gg_lumi

  integer :: ndiv_lumi
  real(kind=double), dimension(:,:), pointer :: yl_lumi
  real(kind=double), dimension(:,:), pointer :: yu_lumi
  real(kind=double), dimension(:,:), pointer :: rho_single_lumi
  real(kind=double), dimension(:,:), pointer :: rho_corr_lumi
  real(kind=double), dimension(2) :: avg_energy_lumi

  integer :: ndiv_photons_beam1
  real(kind=double), dimension(:), pointer :: yl_photons_beam1
  real(kind=double), dimension(:), pointer :: yu_photons_beam1
  real(kind=double), dimension(:), pointer :: rho_single_photons_beam1
  real(kind=double) :: avg_energy_photons_beam1

  integer :: ndiv_photons_beam2
  real(kind=double), dimension(:), pointer :: yl_photons_beam2
  real(kind=double), dimension(:), pointer :: yu_photons_beam2
  real(kind=double), dimension(:), pointer :: rho_single_photons_beam2
  real(kind=double) :: avg_energy_photons_beam2

  integer :: ndiv_ebeam_in
  real(kind=double), dimension(:), pointer :: yl_ebeam_in
  real(kind=double), dimension(:), pointer :: yu_ebeam_in
  real(kind=double), dimension(:), pointer :: rho_single_ebeam_in
  real(kind=double) :: avg_energy_ebeam_in

  integer :: ndiv_pbeam_in
  real(kind=double), dimension(:), pointer :: yl_pbeam_in
  real(kind=double), dimension(:), pointer :: yu_pbeam_in
  real(kind=double), dimension(:), pointer :: rho_single_pbeam_in
  real(kind=double) :: avg_energy_pbeam_in

  ! module variables used by the structure function part
  integer :: pdf_code_in, pdf_ngroup, pdf_nset, pdf_nfl, pdf_lo
  real(kind=double) :: pdf_QCDL4, pdf_top_mass

  logical :: old_style_lumi_linker

contains

  !========================================================================

  ! While the physical meaning of a 'spectrum' and a 'structure function' 
  ! is different, the implementation is exactly analogous.
  ! The difference is that a user spectrum will be applied BEFORE all built-in
  ! spectra and structure functions (if any), while a user structure functions
  ! will be applied AFTER all built-ins.
  !
  ! Four functions have to be coded by the user:
  !   spectrum_single, spectrum_double, spectrum_prt_out, spectrum_beam_remnant
  ! Except for 'spectrum_single', the templates below can be left unchanged,
  ! if the default behavior is appropriate.
  !
  ! The user spectrum functions take two parameters which can be set in
  ! the WHIZARD input file:
  !   sqrts  : default real (if not set, the total c.m. energy of the process)
  !   mode   : integer      (to distinguish among different user spectra)
  ! In the input file, these are called USER_spectrum_sqrts and 
  ! USER_spectrum_mode, resp. USER_strfun_sqrts and USER_strfun_mode

  !------------------------------------------------------------------------
  ! Return the outgoing particle PDG code, given the incoming particle code
  ! and the mode.  Note that named constants for PDG codes are defined in the
  ! 'particles.f90' module and are available here.
  ! For a beam spectrum, this is usually trivial.

  function spectrum_prt_out (prt_in, mode) result (prt_out)
    integer, intent(in) :: prt_in, mode
    integer :: prt_out
    select case (mode)
    case default
       prt_out = prt_in
    end select
  end function spectrum_prt_out

  !------------------------------------------------------------------------
  ! Return the PDG code of the beam remnant, given the incoming particle code
  ! and the mode.
  ! For a beam spectrum, normally there is no beam remnant, and we return
  ! UNDEFINED (=0)

  function spectrum_beam_remnant (prt_in, mode) result (prt_out)
    integer, intent(in) :: prt_in, mode
    integer :: prt_out
    check_prt_in: if(prt_in.eq.22) then
       check_mode: if(mode.gt.0) then
          prt_out=11
       else check_mode
          prt_out=-11
       end if check_mode
    else check_prt_in
       prt_out = UNDEFINED
    end if check_prt_in
    if (msg_level > 2 ) print *, " spectrum_beam_remnant prt_in,mode,prt_out=", prt_in,mode,prt_out
  end function spectrum_beam_remnant

  !-----------------------------------------------------------------------
  ! The correlated spectrum for a beam pair.
  ! Given the two input parameters x(1), x(2), which are uniformly distributed
  ! between 0 and 1, the spectrum function returns output x values together
  ! with 'factor', which is the spectrum times the Jacobian of the x mapping,
  ! if any.
  !
  ! The numbers returned may also depend on the helicities of the incoming
  ! and outgoing particles.  These are specified in the form of complex density
  ! matrices rho_in(:,:,i) and rho_out(:,:,i), where i is the beam index.  
  !
  ! Each density matrix is a two-dimensional array, where the diagonal elements 
  ! should be real, and the trace should be normalized to unity.  For states
  ! with definite helicity, only the diagonal elements are set.  Off-diagonal
  ! elements refer to superpositions of helicity states, e.g., transversal
  ! polarization of fermions. 
  ! The norm of the spectrum, i.e., the spectrum averaged over initial
  ! helicities and summed over outgoing helicities, should be returned 
  ! as the overall 'factor'.
  ! The non-zero entries in the density matrices are defined as +-1 for fermions,
  ! +-1 for massless vector bosons, +-1 and 0 for massive vector bosons, and 0 
  ! for scalars.  Note that the correct assignment is not checked.  
  !
  ! If the spectrum does not depend on polarization, one should set 
  !   rho_out = rho_in   if the in- and out-particles are identical, resp. 
  !   rho_out = unpolarized_density_matrix (prt_out)   if they are not.
  ! The function 'unpolarized_density_matrix' is defined in the module 
  ! 'particles', as are the PDG codes of the particles.
  !
  ! For factorized spectra, the code below is appropriate.  Modify it if
  ! spectra are correlated, or if you want to improve the performance by 
  ! mapping the unit square onto itself.

  subroutine spectrum_double (factor, x, prt_in, sqrts, rho_in, rho_out, mode)
    real(kind=default), intent(out) :: factor
    real(kind=default), dimension(2), intent(inout) :: x
    integer, dimension(2), intent(in) :: prt_in
    real(kind=default), dimension(2), intent(in) :: sqrts
    complex(kind=default), dimension(-2:2,-2:2,2), intent(in) :: rho_in
    complex(kind=default), dimension(-2:2,-2:2,2), intent(out) :: rho_out
    integer, dimension(2), intent(in) :: mode
    real(kind=default), dimension(2) :: x_in
    integer, dimension(2) :: ii
    real(kind=default), dimension(2) :: factor_single
    integer :: i
    check_ncall_overflow: if(ncall_spectrum.lt.huge(i64)) then
       ncall_spectrum=ncall_spectrum+1
    elseif(ncall_spectrum_overflow.eq.0) then check_ncall_overflow
       ncall_spectrum_overflow=1
       if (msg_level > 0 ) print *, " encountered ncall_spectrum_overflow;  ncall_spectrum,huge(0)=", ncall_spectrum,huge(0)
    end if check_ncall_overflow
    if(ncall_spectrum.eq.1) call spectrum_ini(mode(1))
    x_in=x
    check_prt_in: if(prt_in(1).eq.11.and.prt_in(2).eq.-11) then
       rho_out=rho_in
       ii=min(ndiv_lumi,int(x_in*dble(ndiv_lumi))+1)
       x(1)=yl_lumi(1,ii(1))+(x_in(1)*dble(ndiv_lumi)-dble(ii(1)-1))*(yu_lumi(1,ii(1))-yl_lumi(1,ii(1)))
       x(2)=yl_lumi(2,ii(2))+(x_in(2)*dble(ndiv_lumi)-dble(ii(2)-1))*(yu_lumi(2,ii(2))-yl_lumi(2,ii(2)))
       x=x/avg_energy_ebeam_in
       factor=rho_corr_lumi(ii(1),ii(2))/rho_single_lumi(1,ii(1))/rho_single_lumi(2,ii(2))
       check_ncall_print_ee: if(ncall_spectrum.le.100 .and. msg_level > 3 ) then
          print *, "  "
          print *, " ncall,ii,prt_in,x_in,x,factor=", ncall_spectrum,ii,prt_in,x_in,x,factor
          print *, " ncall,yl_lumi(1,ii(1)),yu_lumi(1,ii(1))-yl_lumi(1,ii(1)),x_in(1)*dble(ndiv_lumi)-dble(ii(1)-1)=", ncall_spectrum,yl_lumi(1,ii(1)),yu_lumi(1,ii(1))-yl_lumi(1,ii(1)),x_in(1)*dble(ndiv_lumi)-dble(ii(1)-1)
          print *, " ncall,yl_lumi(2,ii(2)),yu_lumi(2,ii(2))-yl_lumi(2,ii(2)),x_in(2)*dble(ndiv_lumi)-dble(ii(2)-1)=", ncall_spectrum,yl_lumi(2,ii(2)),yu_lumi(2,ii(2))-yl_lumi(2,ii(2)),x_in(2)*dble(ndiv_lumi)-dble(ii(2)-1)
       end if check_ncall_print_ee
    else if((.not.old_style_lumi_linker).and.prt_in(1).eq.11.and.prt_in(2).eq.22) then check_prt_in
       rho_out=rho_in
       ii=min(ndiv_eg_lumi,int(x_in*dble(ndiv_eg_lumi))+1)
       x(1)=yl_eg_lumi(1,ii(1))+(x_in(1)*dble(ndiv_eg_lumi)-dble(ii(1)-1))*(yu_eg_lumi(1,ii(1))-yl_eg_lumi(1,ii(1)))
       x(2)=yl_eg_lumi(2,ii(2))+(x_in(2)*dble(ndiv_eg_lumi)-dble(ii(2)-1))*(yu_eg_lumi(2,ii(2))-yl_eg_lumi(2,ii(2)))
       x=x/avg_energy_ebeam_in
       factor=rho_corr_eg_lumi(ii(1),ii(2))/rho_single_eg_lumi(1,ii(1))/rho_single_eg_lumi(2,ii(2))
    else if((.not.old_style_lumi_linker).and.prt_in(1).eq.22.and.prt_in(2).eq.-11) then check_prt_in
       rho_out=rho_in
       ii=min(ndiv_ge_lumi,int(x_in*dble(ndiv_ge_lumi))+1)
       x(1)=yl_ge_lumi(1,ii(1))+(x_in(1)*dble(ndiv_ge_lumi)-dble(ii(1)-1))*(yu_ge_lumi(1,ii(1))-yl_ge_lumi(1,ii(1)))
       x(2)=yl_ge_lumi(2,ii(2))+(x_in(2)*dble(ndiv_ge_lumi)-dble(ii(2)-1))*(yu_ge_lumi(2,ii(2))-yl_ge_lumi(2,ii(2)))
       x=x/avg_energy_ebeam_in
       factor=rho_corr_ge_lumi(ii(1),ii(2))/rho_single_ge_lumi(1,ii(1))/rho_single_ge_lumi(2,ii(2))
    else if((.not.old_style_lumi_linker).and.prt_in(1).eq.22.and.prt_in(2).eq.22) then check_prt_in
       rho_out=rho_in
       ii=min(ndiv_gg_lumi,int(x_in*dble(ndiv_gg_lumi))+1)
       x(1)=yl_gg_lumi(1,ii(1))+(x_in(1)*dble(ndiv_gg_lumi)-dble(ii(1)-1))*(yu_gg_lumi(1,ii(1))-yl_gg_lumi(1,ii(1)))
       x(2)=yl_gg_lumi(2,ii(2))+(x_in(2)*dble(ndiv_gg_lumi)-dble(ii(2)-1))*(yu_gg_lumi(2,ii(2))-yl_gg_lumi(2,ii(2)))
       x=x/avg_energy_ebeam_in
       factor=rho_corr_gg_lumi(ii(1),ii(2))/rho_single_gg_lumi(1,ii(1))/rho_single_gg_lumi(2,ii(2))
    else check_prt_in
       factor=1.d0
       loop_singles: do i=1,2
          call spectrum_single(factor_single(i),x(i),prt_in(i),sqrts(i),rho_in(:,:,i),rho_out(:,:,i),mode(i))
          factor=factor*factor_single(i)
       end do loop_singles
       check_ncall_print_other: if(ncall_spectrum.le.100 .and. msg_level > 3 ) then
          print *, "  "
          print *, " ncall,ii,prt_in(1:2),x_in(1:2),x(1:2),factor_single(1:2),factor=", ncall_spectrum,ii,prt_in,x_in,x,factor_single,factor
       end if check_ncall_print_other
    end if check_prt_in
    if(ncall_spectrum.le.100) print *, " exit from spectrum_double ncall_spectrum= ",ncall_spectrum

  end subroutine spectrum_double

  !-----------------------------------------------------------------------
  ! The spectrum for a single beam
  ! Given the input parameter x, which is initially uniformly distributed
  ! between 0 and 1, the spectrum function returns an output x value together
  ! with 'factor', which is the unpolarized spectrum times the Jacobian of the
  ! x mapping, if any.
  ! The outgoing density matrix must also be set.  See the comment for
  ! spectrum_double.
  !
  ! The code below describes a trivial spectrum which is unity over the
  ! whole kinematical range.  Modify this according to your needs.
  ! For correlated spectra, leave the code below as is and modify the subroutine
  ! spectrum_double instead.

  subroutine spectrum_single (factor, x, prt_in, sqrts, rho_in, rho_out, mode)
    real(kind=default), intent(out) :: factor
    real(kind=default), intent(inout) :: x
    integer, intent(in) :: prt_in
    real(kind=default), intent(in) :: sqrts
    complex(kind=default), dimension(-2:2,-2:2), intent(in) :: rho_in
    complex(kind=default), dimension(-2:2,-2:2), intent(out) :: rho_out
    integer, intent(in) :: mode
    real(kind=default) :: x_in
    integer :: ii
    check_ncall_overflow: if(ncall_spectrum.lt.huge(i64)) then
       ncall_spectrum=ncall_spectrum+1
    elseif(ncall_spectrum_overflow.eq.0) then check_ncall_overflow
       ncall_spectrum_overflow=1
       if (msg_level > 0 ) print *, " encountered ncall_spectrum_overflow;  ncall_spectrum,huge(0)=", ncall_spectrum,huge(0)
    end if check_ncall_overflow
    if(ncall_spectrum.eq.1) call spectrum_ini(mode)
    rho_out=rho_in
    x_in=x
    check_prt_in: if(abs(prt_in).eq.11) then
       ii=min(ndiv_lumi,int(x_in*dble(ndiv_lumi))+1)
       check_mode_sign_lumi: if(mode.gt.0) then
          x=yl_lumi(1,ii)+(x_in*dble(ndiv_lumi)-dble(ii-1))*(yu_lumi(1,ii)-yl_lumi(1,ii))
          x=x/avg_energy_ebeam_in
       else check_mode_sign_lumi
          x=yl_lumi(2,ii)+(x_in*dble(ndiv_lumi)-dble(ii-1))*(yu_lumi(2,ii)-yl_lumi(2,ii))
          x=x/avg_energy_ebeam_in
       end if check_mode_sign_lumi
       factor=1.d0
       check_ncall_print_lumi: if(ncall_spectrum.le.100 .and. msg_level > 3 ) then
          print *, "  "
          print *, " ncall,prt_in,mode,ii,x_in,x,factor=", ncall_spectrum,prt_in,mode,ii,x_in,x,factor
          check_mode_print: if(mode.gt.0) then
             print *, " ncall,yl_lumi(1,ii),yu_lumi(1,ii)-yl_lumi(1,ii),x_in*dble(ndiv_lumi)-dble(ii-1)=", ncall_spectrum,yl_lumi(1,ii),yu_lumi(1,ii)-yl_lumi(1,ii),x_in*dble(ndiv_lumi)-dble(ii-1)
          else check_mode_print
             print *, " ncall,yl_lumi(2,ii),yu_lumi(2,ii)-yl_lumi(2,ii),x_in*dble(ndiv_lumi)-dble(ii-1)=", ncall_spectrum,yl_lumi(2,ii),yu_lumi(2,ii)-yl_lumi(2,ii),x_in*dble(ndiv_lumi)-dble(ii-1)
          end if check_mode_print
       end if check_ncall_print_lumi
    else check_prt_in
       check_mode_sign_photons: if(mode.gt.0) then
          ii=min(ndiv_photons_beam1,int(x_in*dble(ndiv_photons_beam1))+1)
          x=yl_photons_beam1(ii)+(x_in*dble(ndiv_photons_beam1)-dble(ii-1))*(yu_photons_beam1(ii)-yl_photons_beam1(ii))
          x=x/avg_energy_ebeam_in
          factor=photons_beam1_factor
       else check_mode_sign_photons
          ii=min(ndiv_photons_beam2,int(x_in*dble(ndiv_photons_beam2))+1)
          x=yl_photons_beam2(ii)+(x_in*dble(ndiv_photons_beam2)-dble(ii-1))*(yu_photons_beam2(ii)-yl_photons_beam2(ii))
          x=x/avg_energy_ebeam_in
          factor=photons_beam2_factor
       end if check_mode_sign_photons
       check_ncall_print_photons: if(ncall_spectrum.le.100 .and. msg_level > 3) then
          print *, "  "
          print *, " ncall,prt_in,mode,ii,x_in,x,factor=", ncall_spectrum,prt_in,mode,ii,x_in,x,factor
          check_mode_print_photons: if(mode.gt.0) then
             print *, " ncall,yl_photons_beam1(ii),yu_photons_beam1(ii)-yl_photons_beam1(ii),x_in*dble(ndiv_photons_beam1)-dble(ii-1)=",  &
                  &   ncall_spectrum,yl_photons_beam1(ii),yu_photons_beam1(ii)-yl_photons_beam1(ii),x_in*dble(ndiv_photons_beam1)-dble(ii-1)
          else check_mode_print_photons
             print *, " ncall,yl_photons_beam2(ii),yu_photons_beam2(ii)-yl_photons_beam2(ii),x_in*dble(ndiv_photons_beam2)-dble(ii-1)=",  &
                  &   ncall_spectrum,yl_photons_beam2(ii),yu_photons_beam2(ii)-yl_photons_beam2(ii),x_in*dble(ndiv_photons_beam2)-dble(ii-1)
          end if check_mode_print_photons
       end if check_ncall_print_photons
    end if check_prt_in
  end subroutine spectrum_single

  !========================================================================

  ! The structure function definitions are analogous to spectrum definitions
  !
  ! Four functions have to be coded by the user:
  !   strfun_single, strfun_double, strfun_prt_out, strfun_beam_remnant
  ! Except for 'strfun_single', the templates below can be left unchanged,
  ! if the default behavior is appropriate.
  !
  ! The user functions take two parameters which can be set in
  ! the WHIZARD input file:
  !   sqrts  : default real (if not set, the total c.m. energy of the process)
  !   mode   : integer      (to distinguish among different user spectra)
  ! In the input file, these are called USER_strfun_sqrts and 
  ! USER_strfun_mode, resp. USER_strfun_sqrts and USER_strfun_mode

  !------------------------------------------------------------------------
  ! Return the outgoing particle PDG code, given the incoming particle code
  ! and the mode.  Note that named constants for PDG codes are defined in the
  ! 'particles.f90' module and are available here.
  ! Default: incoming=outgoing (like, e.g., ISR)

  function strfun_prt_out (prt_in, mode) result (prt_out)
    integer, intent(in) :: prt_in, mode
    integer :: prt_out
    select case (mode)
    case default
       prt_out = prt_in
    end select
  end function strfun_prt_out

  !------------------------------------------------------------------------
  ! Return the PDG code of the beam remnant, given the incoming particle code
  ! and the mode.
  ! Insert something sensible here if you want to have it in the event record.

  function strfun_beam_remnant (prt_in, mode) result (prt_out)
    integer, intent(in) :: prt_in, mode
    integer :: prt_out
    select case (mode)
    case default
       prt_out = UNDEFINED
    end select
  end function strfun_beam_remnant

  !-----------------------------------------------------------------------
  ! The correlated structure functions for a beam pair.
  ! Given the two input parameters x(1), x(2), which are uniformly distributed
  ! between 0 and 1, the strfun function returns output x values together
  ! with 'factor', which is the strfun times the Jacobian of the x mapping,
  ! if any.
  ! For the helicity treatment, see the comments for spectrum_double above.
  !
  ! Given the fact that structure functions are normally factorized, this may
  ! be left as is.  However, you can improve the performance by 
  ! mapping the unit square.  Mapping should take place only if the last
  ! argument 'map' is .true.  If 'map' is .false., this function should return
  ! the structure-function f(x) without modifying the x values

  subroutine strfun_double &
       & (factor, x, prt_in, sqrts, rho_in, rho_out, mode, map)
    real(kind=default), intent(out) :: factor
    real(kind=default), dimension(2), intent(inout) :: x
    integer, dimension(2), intent(in) :: prt_in
    real(kind=default), dimension(2), intent(in) :: sqrts
    complex(kind=default), dimension(-2:2,-2:2,2), intent(in) :: rho_in
    complex(kind=default), dimension(-2:2,-2:2,2), intent(out) :: rho_out
    integer, dimension(2), intent(in) :: mode
    logical, intent(in) :: map
    real(kind=default), dimension(2) :: f
    integer :: i
    factor = 1
    do i = 1, 2
       call strfun_single (f(i), x(i), prt_in(i), sqrts(i), &
            &              rho_in(:,:,i), rho_out(:,:,i), mode(i), map)
       factor = factor * f(i)
    end do
  end subroutine strfun_double

  !-----------------------------------------------------------------------
  ! The structure function for a single beam/parton
  ! Given the input parameter x, which is initially uniformly distributed
  ! between 0 and 1, the strfun function returns an output x value together
  ! with 'factor', which is the strfun times the Jacobian of the x mapping,
  ! if any.
  !
  ! The code below describes a trivial structure function which is unity over the
  ! whole kinematical range.  Modify this according to your needs.
  !
  ! If the structure function is strongly peaked (normally at x=0 or x=1) or
  ! even infinite there (it still has to be integrable), you may apply a mapping,
  ! so the input and output values of x differ.  Mapping should take place only 
  ! if the last argument 'map' is .true.  If 'map' is .false., this function 
  ! should return the structure-function f(x) without modifying the x value

  subroutine strfun_single &
       & (factor, x, prt_in, sqrts, rho_in, rho_out, mode, map)
    real(kind=default), intent(out) :: factor
    real(kind=default), intent(inout) :: x
    integer, intent(in) :: prt_in
    real(kind=default), intent(in) :: sqrts
    complex(kind=default), dimension(-2:2,-2:2), intent(in) :: rho_in
    complex(kind=default), dimension(-2:2,-2:2), intent(out) :: rho_out
    integer, intent(in) :: mode
    logical, intent(in) :: map
    integer :: prt_out
    prt_out = spectrum_prt_out (prt_in, mode)
    select case (mode)
    case default      ! Trivial function without polarization dependence
       factor = 1
       if (prt_in == prt_out) then
          rho_out = rho_in
       else
          rho_out = unpolarized_density_matrix (prt_out)
       end if
    end select
  end subroutine strfun_single

  !=======================================================================
  ! This interface allow for inserting user-defined parton distribution 
  ! functions or PDFs which are not (yet) contained in the standard PDFlib

  ! This interface will be selected if PDF_ngroup is negative in the
  ! WHIZARD input file, while a positive value will select the corresponding
  ! PDFLIB structure functions.

  ! Available parameters and module variables:
  !   code_in     : PDG code of the beam particle (e.g., PROTON)
  !   ngroup      : Absolute value of PDF_ngroup
  !   nset        : Value of PDF_nset
  !   nfl         : Number of active flavors (PDF_nfl)
  !   lo          : QCD order (PDF_lo)
  !   QCDL4       : Value of Lambda(QCD)_4 (PDF_QCDL4)
  !   top_mass    : Top mass value to be used

  ! Initialization call:
  subroutine pdf_init (pdg_code_in, ngroup, nset, nfl, lo, QCDL4, top_mass)
    integer, intent(in) :: pdg_code_in, ngroup, nset, nfl, lo
    real(kind=double), intent(in) :: QCDL4, top_mass
    pdf_code_in  = pdg_code_in
    pdf_ngroup   = ngroup
    pdf_nset     = nset
    pdf_nfl      = nfl
    pdf_lo       = lo
    pdf_QCDL4    = QCDL4
    pdf_top_mass = top_mass
    ! More initialization here
  end subroutine pdf_init

  ! Structure function call:
  ! Input parameters:
  !   x           : Bjorken x value
  !   scale       : Q scale (set by PDF_running_scale or PDF_scale)
  ! Output:
  !   dxpdf       : array of PDF values [x*f(x)]
  !               : (tbar,bbar,cbar,sbar,ubar,dbar,g,d,u,s,c,b,t)
  !               :   -6   -5   -4   -3   -2   -1  0 1 2 3 4 5 6
  subroutine pdf_strfun_array (x, scale, dxpdf)
    real(kind=double), intent(in) :: x, scale
    real(kind=double), dimension(-6:6), intent(out) :: dxpdf
    dxpdf = 0   ! Modify this as needed
  end subroutine pdf_strfun_array

  !=======================================================================

  ! This minimal cut function accepts everything

  function cut (p, code, mode) result (accept)
    type(four_momentum), dimension(:), intent(in) :: p
    integer, dimension(:), intent(in) :: code
    integer, intent(in) :: mode
    logical :: accept
    select case (mode)
    case default
       accept = .true.
    end select
  end function cut

  ! ! This is an example of a cut function which simulates a 'trigger' at
  ! !   a hadron collider detector.  It may be adapted to your special needs,
  ! !   placed instead of the no-op routine above, and activated by setting
  ! !   user_cut_mode nonzero in the input file.  The value of user_cut_mode
  ! !   is transferred as the mode argument of the cut function.
  ! ! The routine is written such that it scans the particle codes, therefore
  ! !   it applies to any process.  This is recommended practice, although it
  ! !   is possible to write process-dependent code by explicitly adressing
  ! !   particle indices.
  ! ! In this sample cut function, the event is accepted if 
  ! !  (1) all partons are 0.4 units in eta-phi space apart from each other
  ! !  (2) There is at least
  ! !     - a lepton with pT > 20 or
  ! !     - missing pT > 50
  ! !     - a b quark with pT > 80 or
  ! !     - a jet (quark/gluon) with pT > 100 GeV
  ! ! Note that kinematical functions are available in the module lorentz.f90
  ! !   while the PDG codes E_LEPTON etc. and functions such as [[visible]]
  ! !   are defined in particles.f90

  !   function cut (p, code, mode) result (accept)
  !     ! function arguments and result
  !     type(four_momentum), dimension(:), intent(in) :: p
  !     integer, dimension(:), intent(in) :: code
  !     integer, intent(in) :: mode
  !     logical :: accept
  !     ! local variables
  !     integer :: n, i, j
  !     logical, dimension (size(code)) :: is_visible
  !     ! number of particles
  !     n = size (code)
  !     ! flag the visible particles in a logical array
  !     is_visible = visible (code)
  !     ! There is one nontrivial set of cuts for mode=1
  !     select case (mode)
  !     case (1)
  !        ! check if all particles (except invisible ones) are separated
  !        accept = .true.
  !        do i=1, n
  !           if (is_visible(i)) then
  !              do j=i+1, n
  !                 if (is_visible(j) .and. eta_phi_distance (p(i), p(j)) < 0.4) &
  !                      & accept = .false.
  !              end do
  !           end if
  !        end do
  !        if (.not.accept) return   ! No need to check further
  !        ! Check for particles with minimum pT
  !        accept = .false.
  !        do i=1, n
  !           ! Don't distinguish particle/antiparticle
  !           select case (abs(code(i)))
  !           case (E_LEPTON,MU_LEPTON,TAU_LEPTON)
  !              if (transverse_momentum (p(i)) > 20)  accept = .true.
  !           case (B_QUARK)
  !              if (transverse_momentum (p(i)) > 80)  accept = .true.
  !           case (GLUON, GLUON2, U_QUARK, D_QUARK, S_QUARK, C_QUARK)
  !              if (transverse_momentum (p(i)) > 100)  accept = .true.
  !           end select
  !        end do
  !        if (accept) return        ! No need to check further
  !        ! Check the total visible and invisible tranverse momentum
  !        if (transverse_momentum (sum (p, mask=is_visible)) > 50) then
  !           accept = .true.
  !           !     else if (transverse_momentum (sum (p, mask=.not.is_visible)) > 80) then
  !           !        accept = .true.
  !        end if
  !     case default
  !        ! No operation for other modes
  !        accept = .true.
  !     end select
  !   end function cut


  !========================================================================

  ! This weight function returns unity, i.e. no reweighting.
  ! Change this code if you need to reweight the matrix element according 
  !   to a known (positive-semidefinite) function of the particle momenta
  !   and activate it by setting user_weight_mode nonzero (the mode) in the
  !   input file. 
  ! You may use the kinematical functions, operators and particle codes from the 
  !   lorentz.f90 and particles.f90 modules for that purpose.

  function weight (p, code, mode)
    type(four_momentum), dimension(:), intent(in) :: p
    integer, dimension(:), intent(in) :: code
    integer, intent(in) :: mode
    real(kind=default) :: weight
    select case (mode)
    case default
       weight = 1
    end select
  end function weight

  !========================================================================

  ! A standard interface for fragmentation has been defined in
  !   E. Boos et al., Proc. Les Houches 2001, hep-ph/0109068
  ! The interface consists of two common blocks, HEPRUP and HEPEUP
  ! which are used to transfer all relevant information from the
  ! Monte Carlo generator to the fragmentation program.

  ! This subroutine can be modified for initializing user fragmentation.
  ! The fragmentation method is determined by the input variable
  ! user_fragmentation_mode.  It can be used to select among different versions
  ! of user fragmentation.
  ! For convenience, the process energy and the random generator seed are given.
  ! The PYTHIA parameters string is also made accessible here and can thus be
  ! abused for transferring any further information if PYTHIA is not invoked
  ! otherwise.
  ! You can assume that the common block HEPRUP has been filled.
  ! Use HEPRUP to access all relevant information.

  subroutine fragment_init (mode, sqrts, seed, pythia_parameters)
    integer, intent(in) :: mode
    real(kind=default), intent(in) :: sqrts
    integer, intent(in) :: seed
    character(*), intent(in) :: pythia_parameters
    select case (mode)
    case default
       ! (insert code here)
       call ilc_fragment_init(seed,pythia_parameters,sqrts)
    end select
  end subroutine fragment_init

  ! This subroutine can be modified for executing user fragmentation of
  ! a generated event.
  ! You may access the event counter as a 64-bit integer.
  ! You can assume that the common block HEPEUP has been filled.
  ! Use HEPEUP to access all relevant information.
  ! The result of fragmentation should be stored in the standard HEPEVT common
  ! block.

  subroutine fragment_call (mode, event_count)
    integer, intent(in) :: mode
    integer(i64), intent(inout) :: event_count
    select case (mode)
    case default
       ! (insert code here)
       call ilc_fragment_call(event_count)
    end select
  end subroutine fragment_call

  ! This subroutine can be modified for finalizing user fragmentation.
  ! You may access the event counter (64-bit integer) and the integral.

  subroutine fragment_end (mode, event_count, integral)
    integer, intent(in) :: mode
    integer(i64), intent(in) :: event_count
    real(default), intent(in) :: integral
    select case (mode)
    case default
       ! (insert code here)
       call ilc_fragment_print(event_count,integral)
    end select
  end subroutine fragment_end

  subroutine spectrum_ini (mode)
    integer, intent(in) :: mode
    integer :: iu
    integer, dimension(1) :: i1
    integer  :: i
    integer  :: ios

    real(kind=double), dimension(:,:), pointer :: yl_lumi_loc
    real(kind=double), dimension(:,:), pointer :: yu_lumi_loc
    real(kind=double), dimension(:,:), pointer :: rho_single_lumi_loc
    real(kind=double), dimension(:,:), pointer :: rho_corr_lumi_loc
    real(kind=double), dimension(2) :: avg_energy_lumi_loc


    call getenv("LUMI_EE_LINKER",lumi_ee_file)
    call getenv("LUMI_EG_LINKER",lumi_eg_file)
    call getenv("LUMI_GE_LINKER",lumi_ge_file)
    call getenv("LUMI_GG_LINKER",lumi_gg_file)
    call getenv("LUMI_LINKER",lumi_file)
    call getenv("PHOTONS_B1",photons_beam1_file)
    call getenv("PHOTONS_B2",photons_beam2_file)
    call getenv("EBEAM",ebeam_in_file)
    call getenv("PBEAM",pbeam_in_file)
    lumi_file=adjustr(lumi_file)
    write(lumi_file(198:200),FMT='(i3.3)') abs(mode)
    lumi_file=adjustl(lumi_file)
    if ( msg_level > 0 ) print *, " lumi_file=", lumi_file

    lumi_ee_file=adjustr(lumi_ee_file)
    write(lumi_ee_file(198:200),FMT='(i3.3)') abs(mode)
    lumi_ee_file=adjustl(lumi_ee_file)
    print *, " lumi_ee_file=", lumi_ee_file

    lumi_eg_file=adjustr(lumi_eg_file)
    write(lumi_eg_file(198:200),FMT='(i3.3)') abs(mode)
    lumi_eg_file=adjustl(lumi_eg_file)
    print *, " lumi_eg_file=", lumi_eg_file

    lumi_ge_file=adjustr(lumi_ge_file)
    write(lumi_ge_file(198:200),FMT='(i3.3)') abs(mode)
    lumi_ge_file=adjustl(lumi_ge_file)
    print *, " lumi_ge_file=", lumi_ge_file

    lumi_gg_file=adjustr(lumi_gg_file)
    write(lumi_gg_file(198:200),FMT='(i3.3)') abs(mode)
    lumi_gg_file=adjustl(lumi_gg_file)
    print *, " lumi_gg_file=", lumi_gg_file


    iu=free_unit()
    open(unit=iu,file=lumi_file,action='read',status='old',form='unformatted',iostat=ios)
    check_old_or_new_style: if(ios.eq.0) then
       old_style_lumi_linker=.true.
    else check_old_or_new_style
       old_style_lumi_linker=.false.
       close(iu)
       open(unit=iu,file=lumi_ee_file,action='read',status='old',form='unformatted')
    end if check_old_or_new_style
    read(iu) ndiv_lumi
    allocate (rho_corr_lumi(ndiv_lumi,ndiv_lumi))
    allocate (rho_single_lumi(2,ndiv_lumi))
    allocate (yl_lumi(2,ndiv_lumi))
    allocate (yu_lumi(2,ndiv_lumi))
    allocate (rho_corr_lumi_loc(ndiv_lumi,ndiv_lumi))
    allocate (rho_single_lumi_loc(2,ndiv_lumi))
    allocate (yl_lumi_loc(2,ndiv_lumi))
    allocate (yu_lumi_loc(2,ndiv_lumi))
    read(iu) yl_lumi_loc,yu_lumi_loc,rho_single_lumi_loc,rho_corr_lumi_loc,avg_energy_lumi_loc
    close(iu)
    check_mode_5: if(abs(mode).ne.5) then
       yl_lumi=yl_lumi_loc
       yu_lumi=yu_lumi_loc
       rho_single_lumi=rho_single_lumi_loc
       rho_corr_lumi=rho_corr_lumi_loc
       avg_energy_lumi=avg_energy_lumi_loc
    else check_mode_5

       yl_lumi(1,:)=yl_lumi_loc(2,:)
       yu_lumi(1,:)=yu_lumi_loc(2,:)
       rho_single_lumi(1,:)=rho_single_lumi_loc(2,:)
       avg_energy_lumi(1)=avg_energy_lumi_loc(2)  

       yl_lumi(2,:)=yl_lumi_loc(1,:)
       yu_lumi(2,:)=yu_lumi_loc(1,:)
       rho_single_lumi(2,:)=rho_single_lumi_loc(1,:)
       avg_energy_lumi(2)=avg_energy_lumi_loc(1)  

       rho_corr_lumi=transpose(rho_corr_lumi_loc)

    end if check_mode_5
    i1=maxloc(rho_single_lumi(1,:))
    yy_electron_peak=0.5d0*(yl_lumi(1,i1(1))+yu_lumi(1,i1(1)))
    if ( msg_level > 3 ) then
       print *, " ndiv_lumi,avg_energy_lumi=", ndiv_lumi,avg_energy_lumi
       loop_i_lumi: do i=1,ndiv_lumi
          print *, " i,yl_lumi(:,i),yu_lumi(:,i),rho_single_lumi(:,i),rho_corr_lumi(i,i)=", i,yl_lumi(:,i),yu_lumi(:,i),rho_single_lumi(:,i),rho_corr_lumi(i,i)
       end do loop_i_lumi
       print *, " i1,yy_electron_peak=", i1,yy_electron_peak
    endif

    ebeam_in_file=adjustr(ebeam_in_file)
    write(ebeam_in_file(198:200),FMT='(i3.3)') abs(mode)
    ebeam_in_file=adjustl(ebeam_in_file)
    if ( msg_level > 0 ) print *, " ebeam_in_file=", ebeam_in_file
    open(unit=iu,file=ebeam_in_file,action='read',status='old',form='unformatted')
    read(iu) ndiv_ebeam_in
    allocate (rho_single_ebeam_in(ndiv_ebeam_in))
    allocate (yl_ebeam_in(ndiv_ebeam_in))
    allocate (yu_ebeam_in(ndiv_ebeam_in))
    read(iu) yl_ebeam_in,yu_ebeam_in,rho_single_ebeam_in,avg_energy_ebeam_in
    close(iu)
    print *, " ndiv_ebeam_in,avg_energy_ebeam_in=", ndiv_ebeam_in,avg_energy_ebeam_in
    loop_i_ebeam_in: do i=1,ndiv_ebeam_in
       print *, " i,yl_ebeam_in(i),yu_ebeam_in(i),rho_single_ebeam_in(i)=", i,yl_ebeam_in(i),yu_ebeam_in(i),rho_single_ebeam_in(i)
    end do loop_i_ebeam_in

    check_old_or_new_style_photons: if(old_style_lumi_linker) then

       pbeam_in_file=adjustr(pbeam_in_file)
       write(pbeam_in_file(198:200),FMT='(i3.3)') abs(mode)
       pbeam_in_file=adjustl(pbeam_in_file)
       print *, " pbeam_in_file=", pbeam_in_file
       open(unit=iu,file=pbeam_in_file,action='read',status='old',form='unformatted')
       read(iu) ndiv_pbeam_in
       allocate (rho_single_pbeam_in(ndiv_pbeam_in))
       allocate (yl_pbeam_in(ndiv_pbeam_in))
       allocate (yu_pbeam_in(ndiv_pbeam_in))
       read(iu) yl_pbeam_in,yu_pbeam_in,rho_single_pbeam_in,avg_energy_pbeam_in
       close(iu)
       print *, " ndiv_pbeam_in,avg_energy_pbeam_in=", ndiv_pbeam_in,avg_energy_pbeam_in
       loop_i_pbeam_in: do i=1,ndiv_pbeam_in
          print *, " i,yl_pbeam_in(i),yu_pbeam_in(i),rho_single_pbeam_in(i)=", i,yl_pbeam_in(i),yu_pbeam_in(i),rho_single_pbeam_in(i)
       end do loop_i_pbeam_in

       photons_beam1_file=adjustr(photons_beam1_file)
       write(photons_beam1_file(198:200),FMT='(i3.3)') abs(mode)
       photons_beam1_file=adjustl(photons_beam1_file)
       print *, " photons_beam1_file=", photons_beam1_file
       open(unit=iu,file=photons_beam1_file,action='read',status='old',form='unformatted')
       read(iu) ndiv_photons_beam1
       allocate (rho_single_photons_beam1(ndiv_photons_beam1))
       allocate (yl_photons_beam1(ndiv_photons_beam1))
       allocate (yu_photons_beam1(ndiv_photons_beam1))
       read(iu) yl_photons_beam1,yu_photons_beam1,rho_single_photons_beam1,avg_energy_photons_beam1
       close(iu)
       if ( msg_level > 3 ) then
          print *, " ndiv_photons_beam1,avg_energy_photons_beam1=", ndiv_photons_beam1,avg_energy_photons_beam1
          loop_i_photons_beam1: do i=1,ndiv_photons_beam1
             print *, " i,yl_photons_beam1(i),yu_photons_beam1(i),rho_single_photons_beam1(i)=", i,yl_photons_beam1(i),yu_photons_beam1(i),rho_single_photons_beam1(i)
          end do loop_i_photons_beam1
       endif

       photons_beam2_file=adjustr(photons_beam2_file)
       write(photons_beam2_file(198:200),FMT='(i3.3)') abs(mode)
       photons_beam2_file=adjustl(photons_beam2_file)
       if ( msg_level > 0 ) print *, " photons_beam2_file=", photons_beam2_file
       open(unit=iu,file=photons_beam2_file,action='read',status='old',form='unformatted')
       read(iu) ndiv_photons_beam2
       allocate (rho_single_photons_beam2(ndiv_photons_beam2))
       allocate (yl_photons_beam2(ndiv_photons_beam2))
       allocate (yu_photons_beam2(ndiv_photons_beam2))
       read(iu) yl_photons_beam2,yu_photons_beam2,rho_single_photons_beam2,avg_energy_photons_beam2
       close(iu)
       if ( msg_level > 3 ) then
          print *, " ndiv_photons_beam2,avg_energy_photons_beam2=", ndiv_photons_beam2,avg_energy_photons_beam2
          loop_i_photons_beam2: do i=1,ndiv_photons_beam2
             print *, " i,yl_photons_beam2(i),yu_photons_beam2(i),rho_single_photons_beam2(i)=", i,yl_photons_beam2(i),yu_photons_beam2(i),rho_single_photons_beam2(i)
          end do loop_i_photons_beam2
       endif


       photons_beam1_factor=(avg_energy_ebeam_in-avg_energy_lumi(1))/avg_energy_photons_beam1
       photons_beam2_factor=(avg_energy_pbeam_in-avg_energy_lumi(2))/avg_energy_photons_beam2

       print *, " photons_beam1_factor,photons_beam2_factor=", photons_beam1_factor,photons_beam2_factor

    else check_old_or_new_style_photons

       open(unit=iu,file=lumi_eg_file,action='read',status='old',form='unformatted')
       read(iu) ndiv_eg_lumi
       allocate (rho_corr_eg_lumi(ndiv_eg_lumi,ndiv_eg_lumi))
       allocate (rho_single_eg_lumi(2,ndiv_eg_lumi))
       allocate (yl_eg_lumi(2,ndiv_eg_lumi))
       allocate (yu_eg_lumi(2,ndiv_eg_lumi))
       read(iu) yl_eg_lumi,yu_eg_lumi,rho_single_eg_lumi,rho_corr_eg_lumi,avg_energy_eg_lumi
       close(iu)

       open(unit=iu,file=lumi_ge_file,action='read',status='old',form='unformatted')
       read(iu) ndiv_ge_lumi
       allocate (rho_corr_ge_lumi(ndiv_ge_lumi,ndiv_ge_lumi))
       allocate (rho_single_ge_lumi(2,ndiv_ge_lumi))
       allocate (yl_ge_lumi(2,ndiv_ge_lumi))
       allocate (yu_ge_lumi(2,ndiv_ge_lumi))
       read(iu) yl_ge_lumi,yu_ge_lumi,rho_single_ge_lumi,rho_corr_ge_lumi,avg_energy_ge_lumi
       close(iu)

       open(unit=iu,file=lumi_gg_file,action='read',status='old',form='unformatted')
       read(iu) ndiv_gg_lumi
       allocate (rho_corr_gg_lumi(ndiv_gg_lumi,ndiv_gg_lumi))
       allocate (rho_single_gg_lumi(2,ndiv_gg_lumi))
       allocate (yl_gg_lumi(2,ndiv_gg_lumi))
       allocate (yu_gg_lumi(2,ndiv_gg_lumi))
       read(iu) yl_gg_lumi,yu_gg_lumi,rho_single_gg_lumi,rho_corr_gg_lumi,avg_energy_gg_lumi
       close(iu)

    end if check_old_or_new_style_photons


  end subroutine spectrum_ini

  !=======================================================================

end module user
