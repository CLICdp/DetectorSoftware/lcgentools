! WHIZARD 1.95 Feb 25 2010
! 
! (C) 1999-2008 by 
!     Wolfgang Kilian <wolfgang.kilian@desy.de>
!     Thorsten Ohl <ohl@physik.uni-wuerzburg.de>
!     Juergen Reuter <juergen.reuter@physik.uni-freiburg.de>
!
! WHIZARD is free software; you can redistribute it and/or modify it
! under the terms of the GNU General Public License as published by 
! the Free Software Foundation; either version 2, or (at your option)
! any later version.
!
! WHIZARD is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! This file has been stripped of most comments.  For documentation, refer
! to the source 'whizard.nw'

! The original of this module has been contributed by Sebastian Schmidt.

module showers

  use kinds, only: default !NODEP!
  use kinds, only: double !NODEP!
  use limits, only: SHOWER_MAX_TRY
  use limits, only: SHOWER_MAX_TRY_BRANCHING, SHOWER_MAX_TRY_PT
  use limits, only: SHOWER_MAX_LEVELS
  use diagnostics, only: msg_bug, msg_fatal, msg_error
  use constants, only: twopi
  use lorentz, only: three_momentum, four_momentum
  use lorentz, only: three_momentum_moving, four_momentum_moving
  use lorentz, only: array, space_part, energy
  use lorentz, only: sum, operator(-), operator(*), operator(**)
  use lorentz, only: lorentz_transformation_t => lorentz_transformation
  use lorentz, only: boost, inverse, write
  ! prepare for 2.0 naming conventions:
  use particles, only: COMPOSITE, GLUON, D_QUARK, T_QUARK, PHOTON
  use particles, only: particle_t => particle
  use particles, only: particle_new
  use particles, only: particle_get_code => particle_code
  use particles, only: particle_color
  use particles, only: particle_four_momentum, particle_three_momentum
  use particles, only: particle_energy, particle_absolute_momentum
  use particles, only: particle_mass, particle_set_mass
  use particles, only: particle_set_momentum, particle_set_energy
  use tao_random_numbers !NODEP!

  implicit none
  private

  public :: shower_parameters_t
  public :: shower_parameters_init
  public :: shower_t
  public :: shower_has_particles
  public :: shower_final
  public :: shower_write
  public :: shower_get_n_total
  public :: shower_get_n_final
  public :: shower_get_n_gluons
  public :: shower_get_n_quarks
  public :: shower_get_n_photons
  public :: shower_get_n_colors
  public :: pair_shower_generate 
  public :: shower_get_initiator
  public :: shower_get_final_particles
  public :: shower_test

  logical, parameter :: DEBUG = .false.

  type :: shower_parameters_t
     logical :: shower_on = .false.
     integer :: nf = 5
     logical :: running_alpha_s = .true.
     real(default) :: alpha_s = 0.2_default
     real(default) :: lambda = 0.29_default
     real(default) :: t_min = 1._default
     real(default) :: pt2min = 0
     real(default), dimension(5) :: mass = (/ &
          0.330_default, &
          0.330_default, &
          0.500_default, &
          1.5_default, &
          4.8_default &
          /)
     real(default), dimension(5) :: mass_squared = 0
  end type shower_parameters_t

  type :: shower_particle_t
     private
     integer :: nr
     type(particle_t) :: p
     real(default) :: t , z
     real(default) :: tini, pxini, pyini, pzini, Eini
     integer :: col = 0, acl = 0
     logical :: simulated = .false.
     type(shower_particle_t), pointer :: parent => null ()
     type(shower_particle_t), pointer :: child1 => null ()
     type(shower_particle_t), pointer :: child2 => null ()
  end type shower_particle_t

  type :: particle_pointer_t
     type(shower_particle_t), pointer :: p => null ()
  end type particle_pointer_t

  type :: shower_level_t
     private
     type(particle_pointer_t), dimension(:), allocatable :: prt
     integer :: n_entries = 0
     type(shower_level_t), pointer :: next => null ()
  end type shower_level_t

  type :: shower_t
     private
     type(shower_level_t), pointer :: first => null (), last => null ()
     integer :: n_total, n_final, n_gluons, n_quarks, n_photons
     integer :: n_colors
     type(lorentz_transformation_t) :: L
  end type shower_t


  interface particle_shower_mass
     module procedure particle_shower_mass_code
     module procedure particle_shower_mass_prt
  end interface

  interface particle_shower_mass_squared
     module procedure particle_shower_mass_squared_code
     module procedure particle_shower_mass_squared_prt
  end interface


contains

  subroutine shower_parameters_init (spar)
    type(shower_parameters_t), intent(inout) :: spar
    spar%pt2min = max (0.5*spar%t_min, 1.1*spar%lambda) ** 2
    spar%mass_squared = spar%mass ** 2
  end subroutine shower_parameters_init

  function particle_shower_mass_code (code, spar) result (m)
    real(default) :: m
    integer, intent(in) :: code
    type(shower_parameters_t), intent(in) :: spar
    integer :: i
    i = abs (code)
    select case (i)
    case (1:5)
       m = spar%mass(i)
    case default
       m = 0
    end select
  end function particle_shower_mass_code

  function particle_shower_mass_prt (prt, spar) result (m)
    real(default) :: m
    type(shower_particle_t), intent(in) :: prt
    type(shower_parameters_t), intent(in) :: spar
    m = particle_shower_mass (particle_get_code (prt%p), spar)
  end function particle_shower_mass_prt

  function particle_shower_mass_squared_code (code, spar) result (m)
    real(default) :: m
    integer, intent(in) :: code
    type(shower_parameters_t), intent(in) :: spar
    integer :: i
    i = abs (code)
    select case (i)
    case (1:5)
       m = spar%mass_squared(i)
    case default
       m = 0
    end select
  end function particle_shower_mass_squared_code

  function particle_shower_mass_squared_prt (prt, spar) result (m)
    real(default) :: m
    type(shower_particle_t), intent(in) :: prt
    type(shower_parameters_t), intent(in) :: spar
    m = particle_shower_mass_squared (particle_get_code (prt%p), spar)
  end function particle_shower_mass_squared_prt

  function random_number (rng) result (x)
    real(default) :: x
    type(tao_random_state), intent(inout) :: rng
    real(double) :: r
    call tao_random_number (rng, r)
    x = r
  end function random_number

  function random_0_or_1 (rng) result (i)
    integer :: i
    type(tao_random_state), intent(inout) :: rng
    real(double) :: r
    call tao_random_number (rng, r)
    if (r < 0.5_double) then
       i = 0
    else
       i = 1
    end if
  end function random_0_or_1
  
  function particle_is_final (prt) result (is_final)
    logical :: is_final
    type(shower_particle_t), intent(in) :: prt
    is_final = (.not. associated (prt%child1))
  end function particle_is_final

  function particle_is_branched (prt) result (is_branched)
    logical :: is_branched
    type(shower_particle_t), intent(in) :: prt
    if (associated (prt%child1)) then
      is_branched = (prt%child1%nr /= prt%child2%nr)
    else
       is_branched = .false.
    end if
  end function particle_is_branched

  subroutine particle_set_simulated (prt, sim)
    type(shower_particle_t), intent(inout) :: prt
    logical, intent(in), optional :: sim
    if (present (sim)) then
       prt%simulated = sim
    else
       prt%simulated = .true.
    end if
  end subroutine particle_set_simulated

  function particle_is_simulated (prt) result (is_simulated)
    logical :: is_simulated
    type(shower_particle_t), intent(in) :: prt
    is_simulated = prt%simulated
  end function particle_is_simulated

  subroutine particle_set_parent (prt, parent)
    type(shower_particle_t), intent(inout) :: prt
    type(shower_particle_t), intent(in), target :: parent
    type(shower_particle_t), pointer :: parent_pointer
    prt%parent => parent
  end subroutine particle_set_parent

  function particle_get_parent (prt) result (parent)
    type(shower_particle_t) :: parent
    type(shower_particle_t), intent(in) :: prt
    parent = prt%parent
  end function particle_get_parent

  subroutine particle_set_child (prt, child, i)
    type(shower_particle_t), intent(inout) :: prt
    type(shower_particle_t), intent(in), target :: child
    integer, intent(in) :: i
    select case (i)
    case (1);  prt%child1 => child
    case (2);  prt%child2 => child
    end select
  end subroutine particle_set_child

  function particle_get_child (prt, i) result (child)
    type(shower_particle_t) :: child
    type(shower_particle_t), intent(in) :: prt
    integer, intent(in) :: i
    select case (i)
    case (1);  child = prt%child1
    case (2);  child = prt%child2
    end select
  end function particle_get_child

  function particle_is_quark (prt) result (is_quark)
    logical :: is_quark
    type(shower_particle_t), intent(in) ::prt
    is_quark = abs (particle_get_code (prt%p)) <= 6
  end function particle_is_quark

  function particle_is_gluon (prt) result (is_gluon)
    logical :: is_gluon
    type(shower_particle_t), intent(in) :: prt
    is_gluon = particle_get_code (prt%p) == GLUON
  end function particle_is_gluon

  function particle_get_costheta (prt) result (ct)
    real(default) :: ct
    type(shower_particle_t), intent(in) :: prt
    real(default) :: E2, t, z, zbar
    E2 = particle_energy (prt%p) ** 2
    t = prt%t
    z = prt%z
    zbar = 1 - z
    ct = 1 - t / (2 * z * zbar * E2)
  end function particle_get_costheta

  function particle_get_costheta_correct (prt) result (ct)
    real(default) :: ct
    type(shower_particle_t), intent(in) :: prt
    real(default) :: E2, t, t1, t2, z, zbar
    if (particle_is_branched (prt)) then
       if (particle_is_simulated (prt%child1) &
            .and. particle_is_simulated (prt%child2)) then
          E2 = particle_energy (prt%p) ** 2
          t = prt%t
          t1 = prt%child1%t
          t2 = prt%child2%t
          z = prt%z
          zbar = 1 - z
          ct = (t - t1 - t2 - 2 * z * zbar * E2 ) &
               / (-2 * sqrt (z**2 * E2 - t1) * sqrt (zbar**2 * E2 - t2))
       else
          ct = particle_get_costheta (prt)
       end if
    else
       ct = particle_get_costheta (prt)
    end if
  end function particle_get_costheta_correct

  function particle_get_beta (prt) result (beta)
    real(default) :: beta
    type(shower_particle_t), intent(in) :: prt
    beta = sqrt (max (0._default, 1 - prt%t / particle_energy (prt%p)**2))
  end function particle_get_beta

  recursive subroutine particle_apply_z (prt, newz)
    type(shower_particle_t), intent(inout) :: prt
    real(default), intent(in) :: newz
    real(default) :: E
!    if (DEBUG) print *, "old z:", prt%z , " new z: ", newz
    prt%z = newz
    if (associated (prt%child1) .and. associated (prt%child2)) then
       E = particle_energy (prt%p)
       call particle_set_energy (prt%child1%p, newz * E)
       call particle_apply_z (prt%child1, prt%child1%z)
       call particle_set_energy (prt%child2%p, (1-newz) * E)
       call particle_apply_z (prt%child2, prt%child2%z)
    end if
  end subroutine particle_apply_z

  subroutine particle_add_child (prt, code, E, t)
    type(shower_particle_t), intent(inout) :: prt
    integer, intent(in) :: code
    real(default), intent(in) :: E,t
    type(shower_particle_t) , pointer :: child

    if (DEBUG) print *, "adding child for particle ", prt%nr
    allocate(child)
    child%nr = 0
    child%p = particle_new (code, E)
    child%t = t
    call particle_set_parent (child, prt)
    if (.not. associated (prt%child1)) then
       child%nr = 1        ! nr is only temporary, will be corrected
       prt%child1 => child
    else if (.not. associated(prt%child2)) then
       child%nr = 2        ! nr needs to be different -> particle_is_branched
       prt%child2=>child
    else
       call msg_bug (" Error in particle_add_child")
    end if
  end subroutine particle_add_child

  subroutine particle_write (prt, unit, L)
    type(shower_particle_t), intent(in) :: prt
    integer, intent(in) :: unit
    type(lorentz_transformation_t), intent(in), optional :: L
    type(four_momentum) :: p
    if (present (L)) then
       p = L * particle_four_momentum (prt%p)
    else
       p = particle_four_momentum (prt%p)
    end if
100 format (1x, I3)
    write (unit, 100, advance = "no") prt%nr
    if (particle_is_final (prt)) then
110    format (1x,' ', I3, ' ')
       write (unit, 110, advance="no") particle_get_code (prt%p)
    else
111    format (1x,'(', I3, ')')
       write (unit, 111, advance="no") particle_get_code (prt%p)
    end if
    if ( associated(prt%parent) ) then
101 format (I5)
       write (unit,101, advance="no") prt%parent%nr
    else
102 format (5x)
       write (unit,102, advance="no")
    end if
112 format (1x,I3)
113 format (4x)
    if (prt%col /= 0) then
       write (unit, 112, advance="no") prt%col
    else
       write (unit, 113, advance="no")
    end if
    if (prt%acl /= 0) then
       write (unit, 112, advance="no") prt%acl
    else
       write (unit, 113, advance="no")
    end if
103 format (1x, F9.3, F9.3, F9.3, F10.3, F14.5, F14.5)
    write (unit, 103, advance="no") &
         array (space_part (p)), energy (p), p**2, prt%t
    if (particle_is_branched (prt)) then
104    format (1x, F8.5, F8.5, F8.5,1x, A1)
       write (unit, 104, advance="no") &
            prt%z, &
            particle_get_costheta (prt), &
            particle_get_costheta_correct (prt), &
            'b'
    else
105    format (27x)
       write (unit, 105, advance="no")
    end if
106 format(A1)
    if (particle_is_final (prt)) then
       write (unit, 106, advance="no") "f"
    else
       write (unit, 106, advance="no") " "
    end if
    if (particle_is_simulated (prt)) then
       write (unit, 106, advance="no") "s"
    else 
       write (unit, 106, advance="no") " "
    end if
    if (associated (prt%child1)) then
107    format ("  C:", I3, I3)
       write (unit, 107, advance="no") prt%child1%nr, prt%child2%nr
    end if  
    write(unit,*)
  end subroutine particle_write

  subroutine shower_level_create (shl, length)
    type(shower_level_t), pointer :: shl
    integer, intent(in) :: length
    integer i
    allocate (shl)
    allocate (shl%prt (length))
!    do i=1,length
!      shl%prt(i)%p = null ()
!    end do
    shl%n_entries = 0
  end subroutine shower_level_create

  subroutine shower_level_destroy (shl)
    type(shower_level_t), pointer :: shl
    type(shower_particle_t), pointer :: act
    integer i
    do i=1, shl%n_entries
       deallocate (shl%prt(i)%p)
    end do
    deallocate (shl)
  end subroutine shower_level_destroy

  subroutine shower_level_add_prt (shl, prt)
    type(shower_level_t), intent(inout) :: shl
    type(particle_pointer_t), intent(in) :: prt
    if (shl%n_entries < size (shl%prt)) then
       shl%n_entries = shl%n_entries + 1
       shl%prt(shl%n_entries) = prt
    else
       call msg_bug (" Allocated number of particles in shower level exceeded")
    end if
  end subroutine shower_level_add_prt

  subroutine shower_level_write (shl, unit, L)
    type(shower_level_t), intent(in) :: shl
    integer, intent(in) :: unit
    type(lorentz_transformation_t), intent(in), optional :: L
    integer :: i
    write (unit, *) "----"
    do i = 1, shl%n_entries
      call particle_write (shl%prt(i)%p, unit, L)
    end do
  end subroutine shower_level_write
  
  subroutine shower_init (shower, prt)
    type(shower_t), intent(out) :: shower
    type(particle_t), dimension(2), intent(in) :: prt
    type(three_momentum), dimension(2) :: p3
    type(four_momentum), dimension(2) :: p4
    real(default), dimension(2) :: p_abs
    integer, dimension(2) :: code
    type(three_momentum) :: p_cm
    real(default) :: E_cm, m_cm
    type(shower_level_t), pointer :: shl
    type(particle_pointer_t) :: particle_ptr
    integer :: i
    shower%n_total = 0
    shower%n_final = 0
    shower%n_gluons = 0
    shower%n_quarks = 0
    shower%n_photons = 0
    shower%n_colors = 0
    code = particle_get_code (prt)
    p3 = particle_three_momentum (prt)
    p_abs = particle_absolute_momentum (prt)
    p_cm = sum (p3)
    E_cm = sum (p_abs)
    m_cm = sqrt (max (0._default, E_cm**2 - p_cm**2))
    if (m_cm > 0) then
       shower%L = boost (p_cm, m_cm)
    else
       call msg_fatal (" shower_init: zero invariant mass of initial pair")
    end if
    p4(1) = inverse (shower%L) * four_momentum_moving (p_abs(1), p3(1))
    p4(2) = four_momentum_moving (energy (p4(1)), - space_part (p4(1)))
    call shower_level_create(shl, 2)
    do i = 1, 2
       allocate (particle_ptr%p)
       particle_ptr%p%nr = i
       particle_ptr%p%p = particle_new (code(i), 0._default, p4(i))
       call shower_level_add_prt (shl, particle_ptr)
    end do
    call shower_add_level (shower, shl)
  end subroutine shower_init

  subroutine shower_add_level (shower, shl)
    type(shower_t), intent(inout) :: shower
    type(shower_level_t), pointer :: shl
    if (associated (shower%last)) then
       shower%last%next => shl
    else
       shower%first => shl
    end if
    if (DEBUG) print *, " adding level with " ,shl%n_entries, " entries"
    shower%last => shl
  end subroutine shower_add_level

  function shower_has_particles (shower) result (has_particles)
    logical :: has_particles
    type(shower_t), intent(in) :: shower
    has_particles = associated (shower%first)
  end function shower_has_particles

  subroutine shower_final (shower)
    type(shower_t), intent(inout) :: shower
    type(shower_level_t), pointer :: current
    do while (associated (shower%first))
       current => shower%first
       shower%first => current%next
       call shower_level_destroy (current)
    end do
    shower%last => null ()
    shower%n_total = 0
    shower%n_final = 0
    shower%n_gluons = 0
    shower%n_quarks = 0
    shower%n_photons = 0
    shower%n_colors = 0
  end subroutine shower_final

  subroutine shower_write (shower, unit, in_cm_system)
    type(shower_t), intent(in) :: shower
    integer, intent(in) :: unit
    logical, intent(in), optional :: in_cm_system
    logical :: cm
    type(shower_level_t), pointer :: current
    cm = .true.;  if (present (in_cm_system))  cm = in_cm_system
    if (cm) then
       write (unit, *) "Parton shower (c.m. system):"
    else
       write (unit, *) "Parton shower (lab system):"
    end if
    call write (unit, shower%L)
    write (unit, *) "Particle list:"
    write (unit, "(A)") "   #   ID parent col acl    " &
         // "px       py       pz         E          p^2            " &
         // "t         z      ct      ct*    BFS  children"
    current => shower%first
    do while (associated (current))
!       write (unit, *) "next shower level with " , current%n_entries , " entries"
       if (cm) then
          call shower_level_write (current, unit)
       else
          call shower_level_write (current, unit, shower%L)
       end if
       current => current%next
    end do
120 format (1x,A,1x,I3,3x)
    write (unit, 120, advance="no") "n_total =", shower%n_total
    write (unit, 120, advance="no") "n_final =", shower%n_final
    write (unit, 120, advance="no") "n_gluons =", shower%n_gluons
    write (unit, 120, advance="no") "n_quarks =", shower%n_quarks
    write (unit, 120, advance="no") "n_photons =", shower%n_photons
    write (unit, 120, advance="no") "n_colors =", shower%n_colors
  end subroutine shower_write

  function shower_get_n_total (shower) result (n_total)
    integer :: n_total
    type(shower_t), intent(in) :: shower
    n_total = shower%n_total
  end function shower_get_n_total

  function shower_get_n_final (shower) result (n_final)
    integer :: n_final
    type(shower_t), intent(in) :: shower
    n_final = shower%n_final
  end function shower_get_n_final

  function shower_get_n_gluons (shower) result (n_gluons)
    integer :: n_gluons
    type(shower_t), intent(in) :: shower
    n_gluons = shower%n_gluons
  end function shower_get_n_gluons

  function shower_get_n_quarks (shower) result (n_quarks)
    integer :: n_quarks
    type(shower_t), intent(in) :: shower
    n_quarks = shower%n_quarks
  end function shower_get_n_quarks

  function shower_get_n_photons (shower) result (n_photons)
    integer :: n_photons
    type(shower_t), intent(in) :: shower
    n_photons = shower%n_photons
  end function shower_get_n_photons

  function shower_get_n_colors (shower) result (n_colors)
    integer :: n_colors
    type(shower_t), intent(in) :: shower
    n_colors = shower%n_colors
  end function shower_get_n_colors

  subroutine particle_next_tz (prt, gtoqq, rng, spar)
    type(shower_particle_t), intent(inout) :: prt
    integer, intent(inout) ::  gtoqq
    type(tao_random_state), intent(inout) :: rng
    type(shower_parameters_t), intent(in) :: spar
    real(default) :: limit
    real(default) :: fbr, pt2app, delta_z
    real(default) :: tmax, tmin, tt, zz, b0, zufall, pmq, pmq0, tmp
    integer :: iff

    gtoqq = 0
    tmax = min (prt%t, particle_energy (prt%p)**2)
    tmin = spar%t_min
    tt = tmax
    zz = 0
    if (DEBUG) print *, "particle_next_tz for particle " , prt%nr
    if (DEBUG) call particle_write (prt, 6)
    limit = 0.5 * (1 - sqrt (max (0._default, 1 - spar%t_min / prt%t)))
    fbr = 0
    if (particle_is_quark (prt)) then
       fbr = 6._default * log ((1-limit)/limit) + 0.5_default * spar%nf
    end if
    if (particle_is_gluon (prt)) then
       fbr = (8._default / 3) * log ((1-limit)/limit)
    end if

    TRY_T_AND_Z: do
! Shift m^2 for evolution in t=t-m^2
       tmax = tt - particle_shower_mass_squared (prt, spar)

! Select mass for daughter in QCD evolution
       if (spar%running_alpha_s) then
          b0 = 27._default / 6
          iff = 4
          do while (iff <= spar%nf)
             if (prt%t > 4 * particle_shower_mass_squared (iff, spar) &
                         + spar%t_min) then
                b0 = (33._default - 2 * iff) / 6
             end if
             iff = iff+1
          end do
          tt = tmax * exp (max (-50._default, &
                                log (spar%pt2min / (spar%lambda**2)) &
                                * b0 * log (random_number (rng)) / fbr))
       else
          tt = tmax * exp (max (-50._default, &
                                log (random_number (rng)) * twopi &
                                / (spar%alpha_s * fbr)))
       end if
       tt = tt + particle_shower_mass_squared (prt, spar)
! check whether daughter mass is below cutoff
       if (tt < spar%t_min + particle_shower_mass_squared (prt, spar)) then
          if (DEBUG) print *, "particle on mass-shell"
          prt%t = particle_shower_mass_squared (prt, spar)
          exit TRY_T_AND_Z
       end if
! select z-value of branching q->qg, g->gg, g->qqbar
       if (particle_is_quark (prt)) then
          zz = 1 - (1-limit) * (limit/(1-limit))**random_number (rng)
          if (1 + zz**2 < 2*random_number (rng)) then
             cycle TRY_T_AND_Z
          end if
       else
          if (0.5 * spar%nf < fbr * random_number (rng)) then
! g -> gg
             gtoqq = 0
             zz = (1-limit) * (limit/(1-limit))**random_number (rng)
             if (zz > 0.5_default) then
                zz = 1 - zz
             end if
             if ((1 - zz * (1-zz))**2 < random_number (rng))  cycle TRY_T_AND_Z
          else
! g -> qqbar
             gtoqq = 1 + int (spar%nf * random_number (rng))
             pmq = (4 * particle_shower_mass (gtoqq, spar)**2 + spar%t_min) &
                   / tt
             if (pmq > 1)  cycle TRY_T_AND_Z
             zz = random_number (rng)
             if (zz**2 + (1-zz)**2 < random_number (rng))  cycle TRY_T_AND_Z
             if (zz < limit .or. zz > 1-limit)  cycle TRY_T_AND_Z
             pmq0 = 4 * sqrt (0.25 * spar%t_min) / tt
             if ((1 + 0.5*pmq) * sqrt (1 - pmq) &
                  < random_number (rng) * (1 + 0.5 * pmq0) * sqrt (1 - pmq0)) &
                  cycle TRY_T_AND_Z
          end if
       end if
! correct to alpha_S(pT^2)
       if (spar%running_alpha_s) then
          pt2app = zz * (1-zz) * tt
          if (pt2app < spar%pt2min)  cycle TRY_T_AND_Z
          if ((log (spar%pt2min / spar%lambda**2) &
               / log (pt2app / spar%lambda**2)) < random_number (rng)) &
               cycle TRY_T_AND_Z
       end if
! Check if z consistent with chosen m
       delta_z = sqrt (max (0._default, 1 - tt / particle_energy (prt%p)**2))
       if (zz < 0.5 * (1 - delta_z) .or. zz > 0.5 * (1 + delta_z)) &
            cycle TRY_T_AND_Z

       prt%t=tt
       prt%z=zz
! impose angular ordering by rejection of nonordered emission
       if (associated (prt%parent)) then
          if (associated (prt%parent%parent)) then  ! don't check for first partons
             if (particle_get_costheta (prt%parent) &
                  > particle_get_costheta (prt)) then
!                print *, "AO failed for t=" , tt
                cycle TRY_T_AND_Z
             end if
          end if
       end if
       exit TRY_T_AND_Z
    end do TRY_T_AND_Z

    if (DEBUG) print *, "tt: " , tt, "prt: " , prt%t
    if (DEBUG) call particle_write (prt, 6)
  end subroutine particle_next_tz

  subroutine particle_simulate_children (prt, rng, spar, ok)
    type(shower_particle_t), intent(inout) :: prt
    type(tao_random_state), intent(inout) :: rng
    type(shower_parameters_t), intent(in) :: spar
    logical, intent(out) :: ok
    integer :: gtoqq(2)
    real(default) :: tini(2)
    integer :: daughter, looppt, loopbranch
    logical :: correct
    real(default) :: p1betrag, p2betrag
    type(shower_particle_t), pointer :: daughterprt
    
    if (DEBUG) print *, " simulate_children for particle " , prt%nr
    tini(1) = prt%child1%t
    tini(2) = prt%child2%t
    gtoqq(1) = 0
    gtoqq(2) = 0
    daughter = 0
    loopbranch = 0
    
    ok = .true.
    correct = .true.
    TRY_BRANCHING: do
!       print *, "new cycle"
       loopbranch = loopbranch + 1
       if (loopbranch > SHOWER_MAX_TRY_BRANCHING) then
!            call msg_fatal (" Shower: Branching failed after 100 tries")
          ok = .false.;  return
       end if
       if ((prt%child1%t == prt%t) .and. (prt%child2%t == prt%t)) then
          daughter = 1 + random_0_or_1 (rng)
       else
          if (daughter==0) then
             if (prt%child1%t &
                    - particle_shower_mass_squared (prt%child1, spar) &
                  > prt%child2%t &
                    - particle_shower_mass_squared (prt%child2, spar)) then
                daughter = 1
             else
                daughter = 2
             end if
          end if
       end if
       if (daughter == 1) then
          daughterprt => prt%child1
       else
          daughterprt => prt%child2
       end if
       call particle_next_tz (daughterprt, gtoqq(daughter), rng, spar)
! check if chosen multiplet m1, m2, z1, z2 is physical
       if (sqrt (prt%child1%t) + sqrt (prt%child2%t) >= sqrt(prt%t)) then
          daughter = 0
          if (DEBUG) print *, "sqrt(t) failed", &
               prt%child1%t , prt%child2%t , prt%t
          cycle TRY_BRANCHING
       end if
       if (associated (prt%parent)) then
          if (associated (prt%parent%parent)) then
! Shuffling
             call particle_apply_z (prt, &
                  0.5 * (1 + (prt%child1%t - prt%child2%t) / prt%t &
                           + (2 * prt%z - 1) &
                             * sqrt ((prt%t - prt%child1%t - prt%child2%t)**2 &
                                     - 4 * prt%child1%t * prt%child2%t) &
                             / prt%t))
             if (DEBUG) print *, "applying shuffling"
             if (DEBUG) call particle_write (prt, 6)
             if (DEBUG) call particle_write (prt%child1, 6)
             if (DEBUG) call particle_write (prt%child2, 6)
             if (prt%child1%t &
                  > particle_shower_mass_squared (prt%child1, spar) &
                    + 0.25 * spar%t_min) then
                if (prt%child1%z < 0.5 * (1 - particle_get_beta(prt%child1)) &
                     .or. & 
                    prt%child1%z > 0.5 * (1 + particle_get_beta(prt%child1))) &
                    then 
                   if (DEBUG) print *, "z(1) failed with ", prt%child1%t
!                   daughter=1
                   cycle TRY_BRANCHING
                end if
             end if
             if (prt%child2%t &
                  > particle_shower_mass_squared (prt%child2, spar) &
                    + 0.25 * spar%t_min) then
                if (prt%child2%z < 0.5 * (1 - particle_get_beta(prt%child2)) &
                     .or. & 
                    prt%child2%z > 0.5 * (1 + particle_get_beta(prt%child2))) &
                    then
                   if(DEBUG) print *, "z(2) failed"
!                   daughter=2
                   cycle TRY_BRANCHING
                end if
             end if
             looppt = 0
             correct = .true.
             TRY_PT: do
                if (prt%child1%t &
                     <= particle_shower_mass_squared (prt%child1, spar) &
                    .and. &
                    prt%child2%t &
                     <= particle_shower_mass_squared (prt%child2, spar)) then
                   exit TRY_PT
                end if
                looppt = looppt + 1
                p1betrag = sqrt (max (0._default, &
                                      particle_energy (prt%child1%p) ** 2 &
                                      - prt%child1%t))
                p2betrag = sqrt (max (0._default, &
                                      particle_energy (prt%child2%p) ** 2 &
                                      - prt%child2%t))
                if (p1betrag == 0 .or. p2betrag == 0) then
                   correct = .false.
                else
                   correct = &
                        p1betrag + p2betrag &
                          >= particle_absolute_momentum (prt%p) & 
                        .and. &
                        particle_absolute_momentum (prt%p) &
                          >= abs (p1betrag - p2betrag)
                   if (DEBUG) print *, "correct: " , &
                        correct, p1betrag, p2betrag, &
                        particle_absolute_momentum (prt%p)
                end if
                if (.not. correct) then
                   call particle_apply_z (prt, 0.05 + 0.9 * prt%z)
                end if
                if (looppt >= SHOWER_MAX_TRY_PT .or. correct) then
                   exit TRY_PT
                end if
             end do TRY_PT
             if (correct .eqv. .false.) then
                prt%child1%t = tini(1)
                prt%child2%t = tini(2)
                if (DEBUG) print *, &
                     "correct failed, resetting ts.........................."
                cycle TRY_BRANCHING
             end if
          end if
       end if
       exit TRY_BRANCHING
    end do TRY_BRANCHING
! add children
    LOOP_DAUGHTERS: do daughter = 1, 2
       if (daughter==1) then
          daughterprt => prt%child1
       else
          daughterprt => prt%child2
       end if
!       print *, "daughter: " , daughterprt%nr
       call particle_set_simulated (daughterprt)
       if (daughterprt%t &
            < particle_shower_mass_squared (daughterprt, spar) &
              + 0.25*spar%t_min) then
          cycle LOOP_DAUGHTERS
       end if
       if (particle_is_quark (daughterprt)) then
          ! q -> qg
          call particle_add_child &
               (daughterprt, &
                particle_get_code (daughterprt%p), &
                daughterprt%z * particle_energy (daughterprt%p), &
                daughterprt%t)
          call particle_add_child &
               (daughterprt, &
                GLUON, &
                (1 - daughterprt%z) * particle_energy (daughterprt%p), &
                daughterprt%t)
       else
          if (gtoqq (daughter) > 0) then
             call particle_add_child &
                  (daughterprt, &
                   gtoqq(daughter), &
                   daughterprt%z * particle_energy (daughterprt%p), &
                   daughterprt%t)
             call particle_add_child &
                  (daughterprt, &
                   -gtoqq(daughter), &
                   (1 - daughterprt%z) * particle_energy (daughterprt%p), &
                   daughterprt%t)
          else
             call particle_add_child &
                  (daughterprt, &
                   GLUON, &
                   daughterprt%z * particle_energy (daughterprt%p), &
                   daughterprt%t)
             call particle_add_child &
                  (daughterprt, &
                   GLUON, &
                   (1 - daughterprt%z) * particle_energy (daughterprt%p), &
                   daughterprt%t)
          end if
      end if
   end do LOOP_DAUGHTERS
  end subroutine particle_simulate_children

  subroutine particle_generate_ps (prt, rng)
    type(shower_particle_t), intent(inout) :: prt
    type(tao_random_state), intent(inout) :: rng
    real(default), dimension(1:3, 1:3) :: directions
    integer :: i,j
    real(default) :: E, E1, t1, t2, factor
    real(default) :: scprodukt, pbetrag, p1betrag, p2betrag, x, pTbetrag, phi
    real(default), dimension(3) :: momentum
 
    if (DEBUG) print *, " generate_ps for particle " , prt%nr
    if(.not. (associated(prt%child1) .and. associated(prt%child2))) then
       call msg_error ("no children for generate_ps")
       return
    end if
    if (particle_absolute_momentum (prt%p) == 0) then
       ! Workaround for first branching
       E = particle_energy (prt%p)
       t1 = prt%child1%t
       t2 = prt%child2%t
       call particle_apply_z (prt, (1 + (t1-t2) / E**2) / 2)
       E1 = particle_energy (prt%child1%p)
!       pz = (1 - 2 * random_0_or_1 (rng)) * sqrt (E1**2 - t1)
       factor = sqrt (E1**2 - t1) / particle_absolute_momentum (prt%parent%p)
       call particle_set_momentum (prt%child1%p, &
            particle_three_momentum (prt%parent%p) * factor)
       call particle_set_mass (prt%child1%p, sqrt (prt%child1%t))
       call particle_set_momentum (prt%child2%p, &
            particle_three_momentum (prt%parent%p) * (-factor))
       call particle_set_mass (prt%child2%p, sqrt (prt%child2%t))
    else
       directions(1,:) = &
            array (particle_three_momentum (prt%p)) &
            / particle_absolute_momentum (prt%p)
       do i = 1, 3
          do j = 2, 3
             directions(j,i) = random_number (rng)
          end do
       end do
       do i = 2,3
          scprodukt = 0
          do j = 1, i-1
             scprodukt = dot_product (directions(i,:), directions(j,:))
             directions(i,:) = directions(i,:) - directions(j,:) * scprodukt
          end do
          scprodukt = dot_product (directions(i,:), directions(i,:))
          directions(i,:) = directions(i,:) / sqrt (scprodukt)
       end do
! enforce righthanded system
       if ((  directions(1,1) * (  directions(2,2) * directions(3,3) &
                                 - directions(2,3) * directions(3,2)) &
            + directions(1,2) * (  directions(2,3) * directions(3,1) &
                                 - directions(2,1) * directions(3,3)) &
            + directions(1,3) * (  directions(2,1) * directions(3,2) &
                                 - directions(2,2) * directions(3,1))) &
            < 0) then
          directions(3,:) = - directions(3,:)
       end if
       pbetrag = particle_absolute_momentum (prt%p)
       if ((particle_energy (prt%child1%p)**2 - prt%child1%t < 0) &
            .or. &
           (particle_energy (prt%child2%p)**2 - prt%child2%t < 0)) then
          if (DEBUG) print *, "err: error at generate_ps(), E^2 < t"
          return
       end if
       p1betrag = sqrt (particle_energy (prt%child1%p)**2 - prt%child1%t)
       p2betrag = sqrt (particle_energy (prt%child2%p)**2 - prt%child2%t)
       x = (pbetrag**2 + p1betrag**2 - p2betrag**2) / (2 * pbetrag)
       if (particle_absolute_momentum (prt%p) > p1betrag + p2betrag &
           .or. &
           particle_absolute_momentum (prt%p) < abs (p1betrag - p2betrag)) then
          if (DEBUG) print *, &
               "error in generate_ps, triangle inequality violated: ", &
               particle_absolute_momentum (prt%p)," ",p1betrag," ",p2betrag
          return
       end if
       pTbetrag = sqrt (p1betrag**2 - x**2)
       phi = twopi * random_number (rng)
       momentum = x * directions(1,:) &
            + pTbetrag * (  cos (phi) * directions(2,:) &
                          + sin (phi) * directions(3,:))
       call particle_set_momentum &
            (prt%child1%p, three_momentum_moving (momentum))
       call particle_set_mass (prt%child1%p, sqrt (prt%child1%t))
       momentum = (particle_absolute_momentum (prt%p) - x) * directions(1,:) &
            - pTbetrag * (  cos (phi) * directions(2,:) &
                          + sin (phi) * directions(3,:))
       call particle_set_momentum &
            (prt%child2%p, three_momentum_moving (momentum))
       call particle_set_mass (prt%child2%p, sqrt (prt%child2%t))
    end if
  end subroutine particle_generate_ps

  subroutine shower_generate (shower, rng, spar, ok)
    type(shower_t), intent(inout) :: shower
    type(tao_random_state), intent(inout) :: rng
    type(shower_parameters_t), intent(in) :: spar
    logical, intent(out) :: ok
    type(shower_level_t), pointer :: current
    type(particle_pointer_t) :: particle_ptr
    type(shower_level_t), pointer :: shl
    real(default), dimension(4) ::  pini
    integer :: i,j, n_children, number, n_loops

    if (.not. associated (shower%first)) &
         call msg_bug ("No particles in shower_generate")
    if (DEBUG) print *, "simulate"
! Define imagined single initiator of shower 
    allocate (particle_ptr%p)
    pini = 0
    do j = 1, shower%first%n_entries
       pini = pini + array (particle_four_momentum (shower%first%prt(j)%p%p))
       call particle_set_simulated (shower%first%prt(j)%p)
       call particle_set_child (shower%first%prt(j)%p, particle_ptr%p, 1)
       call particle_set_child (shower%first%prt(j)%p, particle_ptr%p, 2)
    end do
    particle_ptr%p%nr = shower%first%n_entries + 1
    particle_ptr%p%z = 0.5
    particle_ptr%p%p = particle_new (COMPOSITE, four_momentum_moving (pini))
    particle_ptr%p%t = particle_mass (particle_ptr%p%p)**2
    call particle_set_simulated (particle_ptr%p)
    call particle_set_parent (particle_ptr%p, shower%first%prt(1)%p)
    call shower_level_create (shl, 1)
    call shower_level_add_prt (shl, particle_ptr)
    call shower_add_level (shower, shl)
! reinsert initial particles as children of imagined mother
    call shower_level_create (shl, shower%first%n_entries)
    do i = 1, shower%first%n_entries
!       print *, "i: ", i
      allocate (particle_ptr%p)
      if (i == 1) then
         call particle_set_child &
              (shower%first%next%prt(1)%p, particle_ptr%p, 1)
      end if
      if (i == shower%first%n_entries) then
         call particle_set_child &
              (shower%first%next%prt(1)%p, particle_ptr%p, 2)
      end if
      particle_ptr%p%p = shower%first%prt(i)%p%p
      particle_ptr%p%nr = shower%first%next%prt(1)%p%nr + i
      particle_ptr%p%t = shower%first%next%prt(1)%p%t
      call particle_set_parent (particle_ptr%p, shower%first%next%prt(1)%p)
      call shower_level_add_prt (shl, particle_ptr)
    end do
    call shower_add_level (shower, shl)

!    print *, "vor particle_simulate_children"
!    call shower_write (shower, 6)
    
    n_loops = 0
    current => shower%first%next
    do while (associated (current).and. n_loops < SHOWER_MAX_LEVELS)
       if (DEBUG) print *, " new row in shower_generate"
       if (DEBUG) call shower_write (shower, 6)
       n_loops = n_loops + 1
       n_children = 0
       do i = 1, current%n_entries
!          print *, "n_entries: ", current%n_entries
          if (particle_is_branched (current%prt(i)%p)) then
              call particle_simulate_children (current%prt(i)%p, rng, spar, ok)
              if (.not. ok) then
                 call shower_final (shower)
                 return
              end if
              call particle_generate_ps (current%prt(i)%p, rng)
              if (particle_is_branched (current%prt(i)%p%child1)) &
                   n_children = n_children+2
              if (particle_is_branched (current%prt(i)%p%child2)) &
                   n_children = n_children+2
          end if
       end do
! collect children and insert in shower_levels, renumber them
!       print *, " n_children: ", n_children
       if (n_children > 0) then
         call shower_level_create (shl, n_children)
         number = current%next%prt(1)%p%nr + current%next%n_entries - 1
         do i = 1, current%n_entries
           if (particle_is_branched (current%prt(i)%p)) then
             if (particle_is_branched (current%prt(i)%p%child1)) then
               particle_ptr%p => current%prt(i)%p%child1%child1
               number = number + 1
               particle_ptr%p%nr = number
               call shower_level_add_prt (shl, particle_ptr)
               particle_ptr%p => current%prt(i)%p%child1%child2
               number = number + 1
               particle_ptr%p%nr = number
               call shower_level_add_prt (shl, particle_ptr)
             end if
             if (particle_is_branched (current%prt(i)%p%child2)) then
               particle_ptr%p => current%prt(i)%p%child2%child1
               number = number + 1
               particle_ptr%p%nr = number
               call shower_level_add_prt (shl, particle_ptr)
               particle_ptr%p => current%prt(i)%p%child2%child2
               number = number + 1
               particle_ptr%p%nr = number
               call shower_level_add_prt (shl, particle_ptr)
             end if
           end if
         end do
         call shower_add_level (shower, shl)
       end if
       current => current%next
    end do
  end subroutine shower_generate

  subroutine shower_count_particles (shower)
    type(shower_t), intent(inout) :: shower
    type(shower_level_t), pointer :: current
    integer :: i
    current => shower%first
    do while (associated (current))
       do i = 1, current%n_entries
          shower%n_total = shower%n_total + 1
          if (particle_is_final (current%prt(i)%p)) then
             shower%n_final = shower%n_final + 1
             select case (abs (particle_get_code (current%prt(i)%p%p)))
             case (GLUON)
                shower%n_gluons = shower%n_gluons + 1
             case (D_QUARK:T_QUARK)
                shower%n_quarks = shower%n_quarks + 1
             case (PHOTON)
                shower%n_photons = shower%n_photons + 1
             end select
          end if
       end do
       current => current%next
    end do
  end subroutine shower_count_particles

  subroutine shower_assign_colors (shower, col0)
    type(shower_t), intent(inout) :: shower
    integer, intent(in), optional :: col0
    type(shower_level_t), pointer :: current
    type(shower_particle_t), pointer :: prt, prt1, prt2
    integer :: i, c0, c, col, col1, col2, n, n1, n2

    c0 = 0;  if (present (col0))  c0 = col0
    c = c0 + 1

    ! First level = quark and antiquark #1, #2
    current => shower%first
    prt => current%prt(1)%p
    select case (particle_color (particle_get_code (prt%p)))
    case (3)
       prt1 => current%prt(1)%p
       prt2 => current%prt(2)%p
    case (-3)
       prt1 => current%prt(2)%p
       prt2 => current%prt(1)%p
    end select
    prt1%col = c
    prt2%acl = c

    ! Second level = fictitious singlet gluon #3
    current => current%next
    prt => current%prt(1)%p
    prt%col = c
    prt%acl = c

    ! Third level = same as first level #4, #5
    current => current%next
    prt => current%prt(1)%p
    select case (particle_color (particle_get_code (prt%p)))
    case (3)
       prt1 => current%prt(1)%p
       prt2 => current%prt(2)%p
    case (-3)
       prt1 => current%prt(2)%p
       prt2 => current%prt(1)%p
    end select
    prt1%col = c
    prt2%acl = c

    ! Further levels = branchings
    current => current%next
    do while (associated (current))
       do i = 1, current%n_entries, 2
          prt  => current%prt(i)%p%parent
          prt1 => current%prt(i)%p
          prt2 => current%prt(i+1)%p
          col  = particle_color (particle_get_code (prt%p))
          col1 = particle_color (particle_get_code (prt1%p))
          col2 = particle_color (particle_get_code (prt2%p))
          n  = prt%nr
          n1 = prt1%nr
          n2 = prt2%nr
          select case (col)
          case (8)
             select case (col1)
             case (8) 
                select case (col2)
                case (8)                              ! G -> G G
                   prt1%col = prt%col
                   prt2%acl = prt%acl
                   c = c + 1
                   prt1%acl = c
                   prt2%col = c
                case (0)                              ! G -> G H
                   prt1%col = prt%col
                   prt1%acl = prt%acl
                end select
             case (3)                                 ! G -> q qbar
                prt1%col = prt%col
                prt2%acl = prt%acl
             case (-3)                                ! G -> qbar q
                prt1%acl = prt%acl
                prt2%col = prt%col
             case (0)                                 ! G -> H G
                prt2%col = prt%col
                prt2%acl = prt%acl
             end select
          case (3)
             select case (col1)
             case (8)
                select case (col1)
                case (8)                               ! q -> G q
                   prt1%col = prt%col
                   c = c + 1
                   prt1%acl = c
                   prt2%col = c
                case (0)                               ! q -> A q
                   prt2%col = prt%col
                end select
             case (3)
                select case (col2)
                case (8)                               ! q -> q G
                   prt2%col = prt%col
                   c = c + 1
                   prt2%acl = c
                   prt1%col = c
                case (0)                               ! q -> q A
                   prt1%col = prt%col 
                end select
             end select
          case (-3)
             select case (col1)
             case (8)
                select case (col1)
                case (8)                              ! qbar -> G qbar
                   prt1%acl = prt%acl
                   c = c + 1
                   prt1%col = c
                   prt2%acl = c
                case (0)                              ! qbar -> A qbar
                   prt2%acl = prt%acl
                end select
             case (-3)
                select case (col2)
                case (8)                              ! qbar -> qbar G
                   prt2%acl = prt%acl
                   c = c + 1
                   prt2%col = c
                   prt1%acl = c
                case (0)                              ! qbar -> qbar A
                   prt1%acl = prt%acl
                end select
             end select
          case (0)
             c = c + 1
             select case (col1)
             case (3)                                  ! A -> q qbar      
                prt1%col = c
                prt2%acl = c
             case (-3)                                 ! A -> qbar q
                prt1%acl = c
                prt2%col = c
             case (8)                                  ! H -> g g
                c = c + 1
                prt1%col = c
                prt2%acl = c
                c = c + 1
                prt1%acl = c
                prt2%col = c
             end select
          end select
       end do
       current => current%next
    end do
    shower%n_colors = c - c0
  end subroutine shower_assign_colors
       
  subroutine pair_shower_generate (shower, prt, rng, spar)
    type(shower_t), target, intent(inout) :: shower
    type(particle_t), dimension(2), intent(in) :: prt
    type(tao_random_state), intent(inout) :: rng
    type(shower_parameters_t), intent(in) :: spar
    logical :: ok
    integer :: try, i
    TRY_SHOWER: do try = 1, SHOWER_MAX_TRY
       call shower_init (shower, prt)
       call shower_generate (shower, rng, spar, ok)
       if (ok)  exit TRY_SHOWER
       call shower_final (shower)
    end do TRY_SHOWER
    if (ok) then
       call shower_count_particles (shower)
       call shower_assign_colors (shower)
    else
       call msg_fatal (" Parton shower failed after 10 tries")
    end if
  end subroutine pair_shower_generate

  subroutine shower_get_initiator (shower, prt)
    type(shower_t), intent(in) :: shower
    type(particle_t), intent(out) :: prt
    prt = shower%first%next%prt(1)%p%p
  end subroutine shower_get_initiator

  subroutine shower_get_final_particles (shower, prt, col, acl)
    type(shower_t), intent(in) :: shower
    type(particle_t), dimension(:), pointer :: prt
    integer, dimension(:), pointer :: col, acl
    type(shower_level_t), pointer :: current
    type(shower_particle_t), pointer :: prt_i
    integer :: i, k
    allocate (prt(shower%n_final), col(shower%n_final), acl(shower%n_final))
    k = 0
    current => shower%first
    do while (associated (current))
       do i = 1, current%n_entries
          prt_i => current%prt(i)%p
          if (particle_is_final (prt_i)) then
             k = k + 1
             prt(k) = prt_i%p
             col(k) = prt_i%col
             acl(k) = prt_i%acl
          end if
       end do
       current => current%next
    end do
  end subroutine shower_get_final_particles

  subroutine shower_test (n_events, seed)
    integer, intent(in) :: n_events
    integer, intent(in), optional :: seed
    type(shower_parameters_t) :: spar
    type(shower_t) :: shower
    type(tao_random_state) :: rng
    type(particle_t), dimension(2) :: prt
    integer :: npartons, nquarks
    integer :: s, i
    logical :: ok
    if (present (seed)) then
       s = seed
    else
       call system_clock (s)
    end if
    call tao_random_create (rng, s)
    call shower_parameters_init (spar)
    prt(1) = particle_new ( 1,  500._default, 3)
    prt(2) = particle_new (-1, -500._default, 3)
    npartons = 0
    nquarks = 0
    do i = 1, n_events
       call shower_init (shower, prt)
       call shower_generate (shower, rng, spar, ok)
       if (.not. ok) call msg_error (" Parton shower failed")
       if (i == n_events)  call shower_write (shower, 6) 
       npartons = npartons + shower_get_n_final (shower)
       nquarks = nquarks + shower_get_n_quarks (shower)  
       call shower_final (shower)
    end do
    print *, "average number of partons: ", real (npartons, default) / n_events
    print *, "average number of quarks: " , real (nquarks, default) / n_events
    call tao_random_destroy (rng)
  end subroutine shower_test


end module showers
