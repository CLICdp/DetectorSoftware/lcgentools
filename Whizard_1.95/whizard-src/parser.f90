! WHIZARD 1.95 Feb 25 2010
! 
! (C) 1999-2008 by 
!     Wolfgang Kilian <wolfgang.kilian@desy.de>
!     Thorsten Ohl <ohl@physik.uni-wuerzburg.de>
!     Juergen Reuter <juergen.reuter@physik.uni-freiburg.de>
!
! WHIZARD is free software; you can redistribute it and/or modify it
! under the terms of the GNU General Public License as published by 
! the Free Software Foundation; either version 2, or (at your option)
! any later version.
!
! WHIZARD is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! This file has been stripped of most comments.  For documentation, refer
! to the source 'whizard.nw'

module parser

  use kinds, only: default !NODEP!
  use limits, only: BUFFER_SIZE
  use lexer, only: lex, lex_back, lex_clear
  use diagnostics, only: msg_error, msg_message

  implicit none
  private

  public :: get_string, put_back_string, get_string_upper_case
  public :: get_integer
  public :: get_real
  public :: find_string
  public :: find_integer
  public :: lex_clear

contains

  function get_string(unit, iostat) result(string)
    integer, intent(in) :: unit
    character(len=BUFFER_SIZE) :: string
    integer, intent(out), optional :: iostat
    integer :: ios
    call lex(unit, string, iostat=ios)
    if (present(iostat)) then
       iostat = ios
    else
       if (ios > 0) then
          call msg_error (" I/O error occured while reading from file.")
          string = " "
       end if
       if (ios < 0) then
          call msg_error (" Unexpected end of file.")
          string = " "
       end if
    end if
  end function get_string

  subroutine put_back_string (string)
    character(len=BUFFER_SIZE), intent(in) :: string
    call lex_back (string)
  end subroutine put_back_string

  function get_string_upper_case(unit, iostat) result(string)
    integer, intent(in) :: unit
    character(len=BUFFER_SIZE) :: string
    integer, intent(out), optional :: iostat
    integer :: i
    string = get_string(unit, iostat)
    do i=1, len(string)
       if (lge(string(i:i), 'a'))  string(i:i) = achar(iachar(string(i:i))-32)
    end do
  end function get_string_upper_case

  function get_integer(unit, ok, iostat) result(i)
    integer, intent(in) :: unit
    logical, intent(out), optional :: ok
    integer, intent(out), optional :: iostat
    integer :: i
    character(len=BUFFER_SIZE) :: string
    if (present(ok)) ok = .false.
    string = get_string(unit, iostat)
    if (present(iostat)) then
       if (iostat /= 0) return
    end if
    call check_integer(string, ok, i)
    if (present(ok)) then
       if (.not.ok) call lex_back(string)
    end if
  end function get_integer

  subroutine check_integer(string, ok, i)
    character(len=*), intent(in) :: string
    integer, intent(out), optional :: i
    logical, intent(out), optional :: ok
    integer :: iostat, j
    if (verify(string, ' +-1234567890') == 0) then
       read(unit=string, fmt=*, iostat=iostat) j
    else
       iostat = 1
    end if
    if (present(ok)) then
       ok = (iostat == 0)
    else if (iostat /= 0) then
       call msg_message (string)
       call msg_error (" This string should read as an integer.  I'll insert 0.")
    end if
    if (present(i)) i = j
  end subroutine check_integer

  function get_real(unit, ok, iostat) result(x)
    integer, intent(in) :: unit
    logical, intent(out), optional :: ok
    integer, intent(out), optional :: iostat
    real(kind=default) :: x
    character(len=BUFFER_SIZE) :: string
    if (present(ok)) ok = .false.
    string = get_string(unit, iostat)
    if (present(iostat)) then
       if (iostat /= 0) return
    end if
    call check_real(string, ok, x)
    if (present(ok)) then
       if (.not.ok) call lex_back(string)
    end if
  end function get_real

  subroutine check_real(string, ok, x)
    character(len=*), intent(in) :: string
    real(kind=default), intent(out), optional :: x
    logical, intent(out), optional :: ok
    integer :: iostat
    real(kind=default) :: y
    if (verify(string, ' +-1234567890.eEdDqQ') == 0) then
       read(unit=string, fmt=*, iostat=iostat) y
    else
       iostat = 1
    end if
    if (present(ok)) then
       ok = (iostat == 0)
    else if (iostat /= 0) then
       call msg_message (string)
       call msg_error (" This string should read as a real number.  I'll insert 0.")
    end if
    if (present(x)) x = y
  end subroutine check_real

  subroutine find_string(unit, string, reject, here, ok, iostat)
    integer, intent(in) :: unit
    character(len=*), intent(in) :: string
    character(len=*), dimension(:), intent(in), optional :: reject
    logical, intent(in), optional :: here
    logical, intent(out), optional :: ok
    integer, intent(out), optional :: iostat
    character(len=BUFFER_SIZE) :: token
    logical :: search_here, is_integer
    integer :: ios, i
    search_here = .false.;  if (present(here)) search_here = here
    if (present(iostat)) iostat = 0
    SEARCH: do
       if (present(ok)) then
          ok = .false.
          token = get_string(unit, iostat=ios)
          if (ios /= 0) then
             if (present(iostat)) iostat = ios
             exit SEARCH
          end if
          if (trim(token) == string) then
             ok = .true.
             exit SEARCH
          end if
          if (present(reject)) then
             do i=1, size(reject)
                if (trim(token) == trim(reject(i))) then
                   call lex_back(token)
                   ok = .false.
                   exit SEARCH
                end if
             end do
          end if
          if (search_here) then
             call check_integer(trim(token), ok=ok)
             if (.not.ok) then
                call lex_back(token)
                exit SEARCH
             end if
          end if
       else
          token = get_string(unit)
          if (trim(token) == string) exit SEARCH
          if (present(reject)) then
             do i=1, size(reject)
                if (trim(token) == trim(reject(i))) then
                   call msg_message (token)
                   call msg_error (" Did not expect to find this keyword here.  I'll stop parsing this file.")
                   ok = .false.
                   exit SEARCH
                end if
             end do
          end if
          if (search_here) then
             call check_integer(trim(token), ok=is_integer)
             if (.not.is_integer) then
                call msg_message (token)
                call msg_error (" This should have been an integer. I'll stop parsing this file.")
                ok = .false.
                exit SEARCH
             end if
          end if
       end if
    end do SEARCH
  end subroutine find_string

  subroutine find_integer(unit, i, here, ok, iostat)
    integer, intent(in) :: unit, i
    logical, intent(in), optional :: here
    logical, intent(out), optional :: ok
    integer, intent(out), optional :: iostat
    character(len=BUFFER_SIZE) :: string
    write(unit=string, fmt=*) i
    call find_string(unit, trim(adjustl(string)), &
         &           here=here, ok=ok, iostat=iostat)
  end subroutine find_integer


end module parser
