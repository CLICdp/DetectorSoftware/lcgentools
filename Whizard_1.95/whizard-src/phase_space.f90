! WHIZARD 1.95 Feb 25 2010
! 
! (C) 1999-2008 by 
!     Wolfgang Kilian <wolfgang.kilian@desy.de>
!     Thorsten Ohl <ohl@physik.uni-wuerzburg.de>
!     Juergen Reuter <juergen.reuter@physik.uni-freiburg.de>
!
! WHIZARD is free software; you can redistribute it and/or modify it
! under the terms of the GNU General Public License as published by 
! the Free Software Foundation; either version 2, or (at your option)
! any later version.
!
! WHIZARD is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! This file has been stripped of most comments.  For documentation, refer
! to the source 'whizard.nw'

module phase_space

  use kinds, only: default !NODEP!
  use limits, only: TC
  use lorentz
  use constants
  use particles
  use decay_trees, only: decay_tree, branch
  use mappings
  use decay_forests
  use events
!  use vamp_rest, only: division, vamp_grid, vamp_grids !NODEP!

  implicit none
  private

  public :: event_fill

contains

  subroutine event_fill (evt, r, f, factor, volume, chan, active, ok)
    type(event), intent(inout) :: evt
    real(kind=default), dimension(:,:), intent(inout) :: r
    type(decay_forest), intent(inout) :: f
    real(kind=default), dimension(:), intent(out) :: factor
    real(kind=default), intent(out) :: volume
    integer, intent(in) :: chan
    logical, dimension(:), intent(in) :: active
    logical, intent(out) :: ok
    type(particle), dimension(:), allocatable :: prt
    integer :: i,n1,n2
    n1 = f%n_dim_masses
    n2 = f%n_dim_angles
    allocate(prt(f%n_branches_tot))
    call setup_externals(prt, evt%prt(1:))
    if (evt%scattering) then
       do i=1,2
          prt(2**(f%n_externals-i)) = evt%prt(-i)
       end do
       prt(2**(f%n_externals-2)-1)  = evt%prt(0)
    else
       prt(2**(f%n_externals-1)-1)  = evt%prt(0)
    end if
    call fill_decay_tree (prt, &
         &                factor(chan), volume, &
         &                f%tree(chan), f%map(:,chan), &
         &                f%mass_sum, evt%sqrts_hat, &
         &                r(1:n1,chan), r(n1+1:n1+n2,chan), ok)
    if (.not.ok) return
    do i=1, f%n_channels
       if (i/=chan .and. active(i)) then
          call combine_particles(prt, f%tree(i))
          call extract_decay_tree (prt, &
               &                   factor(i), &
               &                   f%tree(i), f%map(:,i), &
               &                   f%mass_sum, evt%sqrts_hat, &
               &                   r(1:n1,i), r(n1+1:n1+n2,i))
       end if
    end do
    call extract_externals(prt, evt%prt(1:))
    deallocate(prt)
    if (.not. evt%cm_is_lab_frame) then
       if (evt%scattering) then
          do i = -2, -1
             evt%prt(i) = evt%L0 * evt%prt(i)
          end do
       end if
       do i = 0, evt%n_out
          evt%prt(i) = evt%L0 * evt%prt(i)
       end do
    end if
  end subroutine event_fill

  subroutine setup_externals(prt, external)
    type(particle), dimension(:), intent(out) :: prt
    type(particle), dimension(:), intent(in) :: external
    integer :: i
    prt = particle_null
    do i=1, ubound(external,1)
       prt(ibset(0,i-1)) = external(i)
    end do
  end subroutine setup_externals

  subroutine extract_externals(prt, external)
    type(particle), dimension(:), intent(in) :: prt
    type(particle), dimension(1:), intent(out) :: external
    integer :: i
    do i=1, ubound(external,1)
       external(i) = prt(ibset(0,i-1))
    end do
  end subroutine extract_externals

  subroutine fill_decay_tree (prt, factor, volume, &
       &                      t, map, mass_sum, sqrts, r1, r2, ok)
    type(particle), dimension(:), intent(inout) :: prt
    real(kind=default), intent(out) :: factor, volume
    type(decay_tree), intent(in) :: t
    type(mapping), dimension(:), intent(inout) :: map
    real(kind=default), dimension(:), intent(in) :: mass_sum
    real(kind=default), intent(in) :: sqrts
    real(kind=default), dimension(:), intent(in) :: r1, r2
    logical, intent(out) :: ok
    real(kind=default), dimension(t%mask_out) :: decay_p
    call set_msq &
         & (prt, factor, volume, decay_p, t, map, &
         &  mass_sum, sqrts, r1, ok)
    if (ok) call set_angles(prt, factor, decay_p, sqrts, t, map, r2)
  end subroutine fill_decay_tree

  subroutine set_msq (prt, factor, volume, decay_p, t, map, &
       &              mass_sum, sqrts, r, ok)
    type(particle), dimension(:), intent(inout) :: prt
    real(kind=default), intent(out) :: factor, volume
    real(kind=default), dimension(:), intent(out) :: decay_p
    type(decay_tree), intent(in) :: t
    type(mapping), dimension(:), intent(inout) :: map
    real(kind=default), dimension(:), intent(in) :: mass_sum
    real(kind=default), intent(in) :: sqrts
    real(kind=default), dimension(:), intent(in) :: r
    logical, intent(out) :: ok
    integer :: ix
    integer(kind=TC) :: k
    real(kind=default) :: m_tot, m1, m2
    ok =.true.
    ix = 1
    k  = t%mask_out
    m_tot = mass_sum(k)
    decay_p(k) = 0.
    if (m_tot < sqrts .or. k==1) then
       if (t%branch(k)%has_children) then
          call set_msq_x (t%branch(k), k, factor, volume, .true.)
       else
          factor = 1
          volume = 1
       end if
    else
       ok = .false.
    end if
  contains
    recursive subroutine set_msq_x (b, k, factor, volume, initial)
      type(branch), intent(in) :: b
      integer(kind=TC), intent(in) :: k
      real(kind=default), intent(out) :: factor, volume
      logical, intent(in) :: initial
      real(kind=default) :: msq, m, m_min, m_max, lda, rlda
      integer(kind=TC) :: k1,k2
      real(kind=default) :: f1,f2,v1,v2
      k1 = b%daughter(1);  k2 = b%daughter(2)
      if (t%branch(k1)%has_children) then
         call set_msq_x (t%branch(k1), k1, f1, v1, .false.)
         if (.not.ok) return
      else
         f1 = 1;  v1 = 1
      end if
      if (t%branch(k2)%has_children) then
         call set_msq_x (t%branch(k2), k2, f2, v2, .false.)
         if (.not.ok) return
      else
         f2 = 1;  v2 = 1
      end if
      m_min = mass_sum(k)
      if (initial) then
         msq = sqrts**2
         m = sqrts
         m_max = sqrts
         factor = f1 * f2
         volume = v1 * v2 / (4*twopi5)
      else
         m_max = sqrts - m_tot + m_min
         call apply (map(k), r(ix), sqrts**2, &
              &      msq, m_min**2, m_max**2, factor); ix=ix+1
         if (msq>=0) then
            m = sqrt(msq)
            factor = f1 * f2 * factor
            volume = v1 * v2 * sqrts**2 /(4*twopi2)
            prt(k) = particle_new (COMPOSITE, m)
         else
            ok = .false.
         end if
      end if
      if (ok) then
         m1 = particle_mass(prt(k1));  m2 = particle_mass(prt(k2))
         lda = lambda(msq, m1**2, m2**2)
         if (lda>0 .and. m>m1+m2 .and. m<=m_max) then
            rlda = sqrt(lda)
            decay_p(k1) = rlda/(2*m)
            decay_p(k2) = -decay_p(k1)
            factor = rlda/msq * factor
         else
            ok = .false.
         end if
      end if
    end subroutine set_msq_x
  end subroutine set_msq

  subroutine set_angles (prt, factor, decay_p, sqrts, t, map, r)
    type(particle), dimension(:), intent(inout) :: prt
    real(kind=default), intent(inout) :: factor
    real(kind=default), dimension(:), intent(in) :: decay_p
    real(kind=default), intent(in) :: sqrts
    type(decay_tree), intent(in) :: t
    type(mapping), dimension(:), intent(inout) :: map
    real(kind=default), dimension(:), intent(in) :: r
    integer :: ix
    integer(kind=TC) :: k
    ix = 1
    k  = t%mask_out
    call set_angles_x (t%branch(k), k)
  contains
    recursive subroutine set_angles_x (b, k, L0)
      type(branch), intent(in) :: b
      integer(kind=TC), intent(in) :: k
      type(lorentz_transformation), intent(in), optional :: L0
      real(kind=default) :: m, ct, st, phi, f, E, p, bg
      type(lorentz_transformation) :: L, LL
      integer(kind=TC) :: k1, k2
      type(three_momentum) :: axis
      p = decay_p(k)
      m = particle_mass(prt(k))
      E = sqrt(m**2 + p**2)
      if (present (L0)) then
         call set(prt(k), L0 * four_momentum_moving(E,p,3) )
      else
         call set (prt(k), four_momentum_moving(E,p,3))
      end if
      if (b%has_children) then
         k1 = b%daughter(1);  k2 = b%daughter(2)
         if (m>0) then
            bg = p/m
         else
            bg = 0
         end if
         phi = r(ix)*twopi;  ix=ix+1
         call apply (map(k), sqrts, r(ix), ct, st, f)
         ix=ix+1
         factor = factor * f
         if (.not. b%has_friend) then
!            L = boost(bg,3) * rotation(phi,3) * rotation(ct,st,2)
            L = LT_compose_r2_r3_b3 (ct, st, cos(phi), sin(phi), bg)
         else
            LL = boost(-bg,3);  if (present (L0))  LL = LL * inverse(L0)
            axis = space_part( &
                 & LL * particle_four_momentum(prt(t%branch(k)%friend)) )
            L = boost(bg,3) * rotation(three_momentum_canonical(3), axis) &
!                 & * rotation(phi,3) * rotation(ct,st,2)
                 & * LT_compose_r2_r3_b3 (ct, st, cos(phi), sin(phi), 0._default)
         end if
         if (present (L0))  L = L0 * L
         call set_angles_x (t%branch(k1), k1, L)
         call set_angles_x (t%branch(k2), k2, L)
      end if
    end subroutine set_angles_x
  end subroutine set_angles

  subroutine combine_particles (prt, t)
    type(particle), dimension(:), intent(inout) :: prt
    type(decay_tree), intent(in) :: t
    call combine_particles_x (t%mask_out)
  contains
    recursive subroutine combine_particles_x(k)
      integer(kind=TC), intent(in) :: k
      integer :: k1, k2
      if (t%branch(k)%has_children) then
         k1 = t%branch(k)%daughter(1);  k2 = t%branch(k)%daughter(2)
         call combine_particles_x (k1)
         call combine_particles_x (k2)
         if (particle_code(prt(k))==0) then
            prt(k) = prt(k1) + prt(k2)
         end if
      end if
    end subroutine combine_particles_x
  end subroutine combine_particles

  subroutine extract_decay_tree (prt, factor, &
       &                         t, map, mass_sum, sqrts, r1, r2)
    type(particle), dimension(:), intent(in) :: prt
    real(kind=default), intent(out) :: factor
    type(decay_tree), intent(in) :: t
    type(mapping), dimension(:), intent(inout) :: map
    real(kind=default), dimension(:), intent(in) :: mass_sum
    real(kind=default), intent(in) :: sqrts
    real(kind=default), dimension(:), intent(out) :: r1, r2
    real(kind=default), dimension(t%mask_out) :: decay_p
    call get_msq (prt, factor, decay_p, t, map, mass_sum, sqrts, r1)
    call get_angles (prt, factor, decay_p, sqrts, t, map, r2)
  end subroutine extract_decay_tree

  subroutine get_msq &
       & (prt, factor, decay_p, t, map, mass_sum, sqrts, r)
    type(particle), dimension(:), intent(in) :: prt
    real(kind=default), intent(out) :: factor
    real(kind=default), dimension(:), intent(out) :: decay_p
    type(decay_tree), intent(in) :: t
    type(mapping), dimension(:), intent(inout) :: map
    real(kind=default), dimension(:), intent(in) :: mass_sum
    real(kind=default), intent(in) :: sqrts
    real(kind=default), dimension(:), intent(inout) :: r
    integer :: ix
    integer(kind=TC) :: k
    real(kind=default) :: m_tot
    ix = 1
    k  = t%mask_out
    m_tot = mass_sum(k)
    decay_p(k) = 0.
    if (t%branch(k)%has_children) then
       call get_msq_x (t%branch(k), k, factor, .true.)
    else
       factor = 1
    end if
  contains
    recursive subroutine get_msq_x (b, k, factor, initial)
      type(branch), intent(in) :: b
      integer(kind=TC), intent(in) :: k
      real(kind=default), intent(out) :: factor
      logical, intent(in) :: initial
      real(kind=default) :: msq, m, m_min, m_max, lda, rlda
      integer(kind=TC) :: k1, k2
      real(kind=default) :: f1, f2, m1, m2
      k1 = b%daughter(1);  k2 = b%daughter(2)
      if (t%branch(k1)%has_children) then
         call get_msq_x (t%branch(k1), k1, f1, .false.)
      else
         f1 = 1
      end if
      if (t%branch(k2)%has_children) then
         call get_msq_x (t%branch(k2), k2, f2, .false.)
      else
         f2 = 1
      end if
      m_min = mass_sum(k)
      m_max = sqrts - m_tot + m_min
      m = particle_mass(prt(k));  msq = m**2
      if (initial) then
         factor = f1 * f2
      else
         call get (map(k), r(ix), sqrts**2, &
              &    msq, m_min**2, m_max**2, factor);  ix=ix+1
         factor = f1 * f2 * factor
      end if
      m1 = particle_mass(prt(k1));  m2 = particle_mass(prt(k2))  
      lda = lambda(msq, m1**2, m2**2)
      if (lda>0) then
         rlda = sqrt(lda)
         decay_p(k1) = rlda/(2*m)
         decay_p(k2) = -decay_p(k1)
         factor = rlda/msq * factor
      else
         decay_p(k1) = 0
         decay_p(k2) = 0
         factor = 0
      end if
    end subroutine get_msq_x
  end subroutine get_msq

  subroutine get_angles (prt, factor, decay_p, sqrts, t, map, r)
    type(particle), dimension(:), intent(in) :: prt
    real(kind=default), intent(inout) :: factor
    real(kind=default), dimension(:), intent(in) :: decay_p
    real(kind=default), intent(in) :: sqrts
    type(decay_tree), intent(in) :: t
    type(mapping), dimension(:), intent(inout) :: map
    real(kind=default), dimension(:), intent(out) :: r
    integer :: ix
    integer(kind=TC) :: k
    ix = 1
    k  = t%mask_out
    if (t%branch(k)%has_children)  call get_angles_x (t%branch(k), k)
  contains
    recursive subroutine get_angles_x (b, k, ct0, st0, phi0, L0)
      type(branch), intent(in) :: b
      integer(kind=TC), intent(in) :: k
      real(kind=default), intent(in), optional :: ct0, st0, phi0
      type(lorentz_transformation), intent(in), optional :: L0
      real(kind=default) :: cp0, sp0, m, ct, st, phi, bg, f
      type(lorentz_transformation) :: L, LL
      type(four_momentum) :: p1, pf
      type(three_momentum) :: n, axis
      integer(kind=TC) :: k1,k2,kf
      logical :: has_friend, need_L
      k1 = b%daughter(1);  k2 = b%daughter(2)
      kf = b%friend;  has_friend = b%has_friend
      if (present(L0)) then
         p1 = L0 * particle_four_momentum (prt(k1))
         if (has_friend)  pf = L0 * particle_four_momentum (prt(kf))
      else
         p1 = particle_four_momentum (prt(k1))
         if (has_friend)  pf = particle_four_momentum (prt(kf))
      end if
      if (present(phi0)) then
         cp0 = cos(phi0);  sp0 = sin(phi0)
      end if
      m = particle_mass(prt(k))
      if (m>0) then
         bg = decay_p(k) / m
      else
         bg = 0
      end if
      if (has_friend) then
         if (present(phi0)) then
            axis = axis_from_p_r3_r2_b3 (pf, cp0, -sp0, ct0, -st0, -bg)
            LL = rotation (axis, three_momentum_canonical(3)) &
                 & * LT_compose_r3_r2_b3 (cp0, -sp0, ct0, -st0, -bg)
         else
            axis = axis_from_p_b3 (pf, -bg)
            LL = rotation (axis, three_momentum_canonical(3))
            if (bg/=0)  LL = LL * boost(-bg, 3)
         end if
         n = space_part(LL * p1)
      else if (present(phi0)) then
         n = axis_from_p_r3_r2_b3 (p1, cp0, -sp0, ct0, -st0, -bg)
      else
         n = axis_from_p_b3 (p1, -bg)
      end if
      phi = azimuthal_angle(n)
      r(ix) = phi/twopi;  ix=ix+1
      ct = polar_angle_ct(n);  st = sqrt(1-ct**2)
      call get (map(k), sqrts, r(ix), ct, f);  ix=ix+1
      factor = factor * f
      if (t%branch(k1)%has_children .or. t%branch(k2)%has_children) then
         need_L = .true.
         if (has_friend) then
            if (present(L0)) then
               L = LL * L0
            else
               L = LL
            end if
         else if (present(L0)) then
            L = LT_compose_r3_r2_b3 (cp0, -sp0, ct0, -st0, -bg) * L0
         else if (present(phi0)) then
            L = LT_compose_r3_r2_b3 (cp0, -sp0, ct0, -st0, -bg)
         else if (bg/=0) then
            L = boost(-bg, 3)
         else
            need_L = .false.
         end if
         if (need_L) then
            if (t%branch(k1)%has_children) &
                 & call get_angles_x (t%branch(k1), k1, ct, st, phi, L)
            if (t%branch(k2)%has_children) &
                 & call get_angles_x (t%branch(k2), k2, ct, st, phi, L)
         else
            if (t%branch(k1)%has_children) &
                 & call get_angles_x (t%branch(k1), k1, ct, st, phi)
            if (t%branch(k2)%has_children) &
                 & call get_angles_x (t%branch(k2), k2, ct, st, phi)
         end if
      end if
    end subroutine get_angles_x
  end subroutine get_angles


end module phase_space
