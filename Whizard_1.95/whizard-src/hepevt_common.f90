! WHIZARD 1.95 Feb 25 2010
! 
! (C) 1999-2008 by 
!     Wolfgang Kilian <wolfgang.kilian@desy.de>
!     Thorsten Ohl <ohl@physik.uni-wuerzburg.de>
!     Juergen Reuter <juergen.reuter@physik.uni-freiburg.de>
!
! WHIZARD is free software; you can redistribute it and/or modify it
! under the terms of the GNU General Public License as published by 
! the Free Software Foundation; either version 2, or (at your option)
! any later version.
!
! WHIZARD is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! This file has been stripped of most comments.  For documentation, refer
! to the source 'whizard.nw'

! For the LEP Monte Carlos, a standard common block has been proposed
! in AKV89.  We strongly recommend its use.  (The description is an
! abbreviated transcription of AKV89, Vol. 3, pp. 327-330).
!
!
! NMXHEP is the maximum number of entries:
!
!
! NEVHEP is normally the event number, but may take special
! values as follows:
!
!    0   the program does not keep track of event numbers.
!   -1   a special initialization record.
!   -2   a special final record.
!
!
! NHEP holds the number of entries for this event.
!
!
! The entry ISTHEP(N) gives the status code for the Nth entry,
! with the following semantics:
!    0       a null entry.
!    1       an existing entry, which has not decayed or fragmented.
!    2       a decayed or fragmented entry, which is retained for
!            event history information.
!    3       documentation line.
!    4- 10   reserved for future standards.
!   11-200   at the disposal of each model builder.
!  201-      at the disposal of users.
!
!
! The Particle Data Group has proposed standard particle codes,
! which are to be stored in IDHEP(N).
!
!
! JMOHEP(1,N) points to the Nth entry's mother, if any.
! It is set to zero for initial entries.
! JMOHEP(2,N) points to the second mother, if any.
!
!
! JDAHEP(1,N) and JDAHEP(2,N) point to the Nth entry's first and
! last daughter, if any.  These are zero for entries which have not
! yet decayed.  The other daughters are stored in between these two.
!
!
! In PHEP we store the momentum of the particle, more specifically
! this means that PHEP(1,N), PHEP(2,N), and PHEP(3,N) contain the
! momentum in the x, y, and z direction (as defined by the machine
! people), measured in GeV/c.  PHEP(4,N) contains the energy in GeV
! and PHEP(5,N) the mass in GeV/c**2.  The latter may be negative for
! spacelike partons.
!
!
! Finally VHEP is the place to store the position of the production
! vertex.  VHEP(1,N), VHEP(2,N), and VHEP(3,N) contain the x, y,
! and z coordinate (as defined by the machine people), measured in mm.
! VHEP(4,N) contains the production time in mm/c.
!
!
! As an amendment to the proposed standard common block HEPEVT, we
! also have a polarisation common block HEPSPN, as described in
! AKV89.  SHEP(1,N), SHEP(2,N), and SHEP(3,N) give the x, y, and z
! component of the spinvector s of a fermion in the fermions
! restframe.
!
! By convention, SHEP(4,N) is always 1.

module hepevt_common

  use kinds, only: default !NODEP!
  use kinds, only: double, i32, i64 !NODEP!
  use diagnostics, only: msg_fatal
  use limits, only: PB_PER_FB_EXPONENT
  use lorentz
  use particles
  use showers, only: shower_has_particles, shower_get_n_colors
  use showers, only: shower_get_initiator, shower_get_final_particles
  use events
  use jetset_interface !NODEP!
  use stdhep_interface !NODEP!

  implicit none
  public

  public :: hepevt_fill
  public :: hepevt_write
  public :: heprup_fill
  public :: heprup_write
  public :: hepeup_fill
  public ::  hepeup_write
  public :: lhef_write_init
  public :: lhef_write_heprup
  public :: lhef_write_hepeup
  public :: lhef_write_final

  integer, parameter :: nmxhep = 4000
  integer :: nevhep, nhep
  integer, dimension(nmxhep) :: isthep, idhep
  integer, dimension(2, nmxhep) :: jmohep, jdahep
  real(kind=double), dimension(5, nmxhep) :: phep
  real(kind=double), dimension(4, nmxhep) :: vhep
  common /HEPEVT/ nevhep, nhep, isthep, idhep, &
       & jmohep, jdahep, phep, vhep

  integer, parameter :: MAXPUP=100
  integer, dimension(2) :: idbmup, pdfgup, pdfsup 
  integer :: idwtup, nprup
  integer, dimension(MAXPUP) :: lprup
  real(kind=double), dimension(2) :: ebmup
  real(kind=double), dimension(MAXPUP) :: xsecup, xerrup, xmaxup
  common /HEPRUP/ idbmup, ebmup, pdfgup, pdfsup, &
       & idwtup, nprup, xsecup, xerrup, xmaxup, lprup

integer, parameter :: MAXNUP = 500
integer :: nup, idprup
integer, dimension(MAXNUP) :: idup, istup
integer, dimension(2,MAXNUP) :: mothup, icolup
real(kind=double) :: xwgtup, scalup, aqedup, aqcdup
real(kind=double), dimension(5,MAXNUP) :: pup
real(kind=double), dimension(MAXNUP) :: vtimup, spinup
integer, dimension(MAXNUP) :: ispinup
common /HEPEUP/ nup, idprup, xwgtup, scalup, aqedup, aqcdup, &
&       idup, istup, mothup, icolup, pup, vtimup, spinup


  interface hepevt_write
     module procedure hepevt_write_unit
  end interface
  interface heprup_write
     module procedure heprup_write_unit
  end interface
  interface hepeup_write
     module procedure hepeup_write_unit
  end interface

contains

  subroutine hepevt_fill (evt)
    type(event), intent(in) :: evt
    integer :: i, j, offset
    integer(i32), parameter :: huge32 = huge (0_i32)
    real(kind=double), dimension(0:3) :: p
    nevhep = mod (evt%count, int (huge32, i64))
    offset = 0
    nhep = event_n_entries (evt)
    if (evt%keep_initials) then
       do j = 1, evt%n_in
          i = offset + j
          isthep(i) = 2
          idhep(i) = particle_code(evt%beam_particle(j))
          jmohep(:, i) = 0
          jdahep(1, i) = evt%n_in + 1
          jdahep(2, i) = 2*evt%n_in + evt%n_beam_remnants
          p = array(particle_four_momentum(evt%beam_particle(j)))
          phep(1:3, i) = p(1:3)
          phep(4, i) = p(0)
          phep(5, i) = particle_mass(evt%beam_particle(j))
          vhep(:, i) = 0
       end do
       offset = offset + evt%n_in
       do j = 1, evt%n_in
          i = offset + j
          isthep(i) = 2
          idhep(i) = particle_code(evt%prt(-j))
          jmohep(1, i) = 1
          jmohep(2, i) = evt%n_in
          jdahep(1, i) = 2*evt%n_in + evt%n_beam_remnants + 1
          jdahep(2, i) = evt%n_in + evt%n_tot + evt%n_beam_remnants
          p = array(particle_four_momentum(evt%prt(-j)))
          phep(1:3, i) = p(1:3)
          phep(4, i) = p(0)
          phep(5, i) = particle_mass(evt%prt(-j))
          vhep(:, i) = 0
       end do
       offset = offset + evt%n_in
    end if
    do j = 1, evt%n_beam_remnants
       i = offset + j
       isthep(i) = 1
       idhep(i) = particle_code(evt%beam_remnant(j))
       if (evt%keep_initials) then
          jmohep(1, i) = 1
          jmohep(2, i) = evt%n_in
       else
          jmohep(:, i) = 0
       end if
       jdahep(:, i) = 0
       p = array(particle_four_momentum(evt%beam_remnant(j)))
       phep(1:3, i) = p(1:3)
       phep(4, i) = p(0)
       phep(5, i) = particle_mass(evt%beam_remnant(j))
       vhep(:, i) = 0
    end do
    offset = offset + evt%n_beam_remnants
    do j = 1, evt%n_out
       i = offset + j
       isthep(i) = 1
       idhep(i) = particle_code(evt%prt(j))
       if (evt%keep_initials) then
          jmohep(1, i) = evt%n_in + 1
          jmohep(2, i) = 2*evt%n_in
       else
          jmohep(:, i) = 0
       end if
       jdahep(:, i) = 0
       p = array(particle_four_momentum(evt%prt(j)))
       phep(1:3, i) = p(1:3)
       phep(4, i) = p(0)
       phep(5, i) = particle_mass(evt%prt(j))
       vhep(:, i) = 0
    end do
  end subroutine hepevt_fill

  subroutine hepevt_write_unit (u, fmt)
    integer, intent(in) :: u
    integer, intent(in), optional :: fmt
    integer :: mode, i, j
    mode = HEPEVT_FMT;  if (present(fmt)) mode = fmt
    if (nevhep < 0) return
    select case(mode)
    case (HEPEVT_FMT)
       write(u,11) nhep
       do i=1, nhep
          write (u,13) isthep(i), idhep(i), &
               & (jmohep(j,i), j=1,2), (jdahep(j,i), j=1,2)
          write (u,12) (phep(j,i), j=1,5)
          write (u,12) (vhep(j,i), j=1,min(5,ubound(vhep,1)))
       end do       
    case(SHORT_FMT)
       write(u,11) nhep
       do i=1, nhep
          write (u,13) idhep(i)
          write (u,12) (phep(j,i), j=1,5)
       end do
    case (STDHEP_FMT)
       call stdhep_write (STDHEP_HEPEVT)       
    case (STDHEP_UP_FMT)
       call stdhep_write (STDHEP_HEPEUP)       
    case(STDHEP_FMT4)
       call stdhep_write (STDHEP_HEPEV4)
    case (JETSET_FMT1, JETSET_FMT2, JETSET_FMT3)
       call jetset_write (u,mode-10)
    case(ATHENA_FMT)
       write(u,11) nevhep, nhep
       do i=1, nhep
          write (u,13) i, isthep(i), idhep(i), jmohep(1,i), &
               & jmohep(2,i), jdahep(1,i), jdahep(2,i)
          write (u,12) phep(1,i), phep(2,i), &
               & phep(3,i), phep(4,i), phep(5,i)
          write (u,*) vhep(1,i), vhep(2,i), &
               & vhep(3,i), vhep(4,i)
       end do
    case(LHA_FMT)
       write (u,16) nup, idprup, xwgtup, scalup, aqedup, aqcdup
       write (u,17) idup(:nup)
       write (u,17) mothup(1,:nup)
       write (u,17) mothup(2,:nup)
       write (u,17) icolup(1,:nup)
       write (u,17) icolup(2,:nup)
       write (u,17) istup(:nup)
       write (u,17) ispinup(:nup)
       do i=1, nup
          write (u,18) i, pup((/4,1,2,3/),i)
       end do
    case default
       call msg_fatal (" Invalid format selected for HEPEVT contents output")
    end select
    11 format(10I5)
    12 format(10F17.10)
    13 format(I9,I9,5I5)
    14 format(ES17.10,F17.10)
    15 format(3I5,1X,F17.10)
    16 format(2(1x,I5),1x,F17.10,3(1x,F13.6))
    17 format(500(1x,I5))
    18 format(1x,I5,4(1x,F17.10))
  end subroutine hepevt_write_unit

  subroutine heprup_fill &
       & (pro_id, pro_max, beam_code, beam_energy, PDF_ngroup, PDF_nset, integral, error)
    integer, intent(in) :: pro_id
    integer, intent(in) :: pro_max
    integer, dimension(2), intent(in) :: beam_code
    real(kind=default), dimension(2), intent(in) :: beam_energy
    integer, dimension(2), intent(in) :: PDF_ngroup, PDF_nset
    real(kind=default), intent(in) :: integral, error
    integer :: i
    idbmup = beam_code
    ebmup = beam_energy
    if (any (PDF_ngroup == 0 .or. PDF_nset == 0)) then
       pdfgup = -1
       pdfsup = -1
    else
       pdfgup = PDF_ngroup
       pdfsup = PDF_nset
    end if
    idwtup = 3
    nprup = pro_max
    xsecup(pro_id) = integral / 10._double**PB_PER_FB_EXPONENT
    xerrup(pro_id) = error / 10._double**PB_PER_FB_EXPONENT
    xmaxup(pro_id) = 1
    lprup(pro_id) = pro_id
  end subroutine heprup_fill

  subroutine heprup_write_unit (u)
    integer, intent(in) :: u
    write(u,*) "HEPRUP contents:"
    write(u,*) " IDBMUP =", idbmup
    write(u,*) " EBMUP  =", ebmup
    write(u,*) " PDFGUP =", pdfgup
    write(u,*) " PDFSUP =", pdfsup
    write(u,*) " IDWTUP =", idwtup
    write(u,*) " NPRUP  =", nprup
    write(u,*) " XSECUP =", xsecup(1)
    write(u,*) " XERRUP =", xerrup(1)
    write(u,*) " XMAXUP =", xmaxup(1)
    write(u,*) " LPRUP  =", lprup(1)
  end subroutine heprup_write_unit

  subroutine hepeup_fill (evt, pro_id)
    integer, intent(in) :: pro_id
    type(event), intent(in) :: evt
    integer, dimension(:), pointer :: col, acl
    type(particle) :: prt0
    type(particle), dimension(:), pointer :: prt
    integer :: col_offset, i, ii, j, k
    idprup = pro_id
    allocate (col(evt%n_tot), acl(evt%n_tot))
    call translate_color_info (col, acl, evt%color_flow, evt%anticolor_flow)
    ii = 0
    LOOP_INCOMING: do i = 1, 2
       j = -i
       ii = ii + 1
       idup(ii) = particle_code (evt%prt(j))
       istup(ii) = -1
       mothup(:,ii) = 0
       icolup(1,ii) = acl(i)
       icolup(2,ii) = col(i)
       pup((/4,1,2,3/),ii) = array (particle_four_momentum (evt%prt(j)))
       pup(5,ii) = particle_mass (evt%prt(j))
       vtimup(ii) = 0
       ispinup(ii) = 9
       spinup(ii) = ispinup(ii)
    end do LOOP_INCOMING
    ii = 2
    LOOP_OUTGOING: do i = 3, evt%n_tot
       j = i - 2
       if (associated (evt%shower_index)) then
          if (evt%shower_index(j) /= 0)  cycle LOOP_OUTGOING
       end if
       ii = ii + 1
       idup(ii) = particle_code (evt%prt(j))
       istup(ii) = 1
       mothup(:,ii) = (/ 1, 2 /)
       icolup(1,ii) = col(i)
       icolup(2,ii) = acl(i)
       pup((/4,1,2,3/),ii) = array (particle_four_momentum (evt%prt(j)))
       pup(5,ii) = particle_mass (evt%prt(j))
       vtimup(ii) = 0
       if (associated (evt%hel_out)) then
          ispinup(ii) = evt%hel_out(j)
       else
          ispinup(ii) = 9
       end if
       spinup(ii) = ispinup(ii)
    end do LOOP_OUTGOING
    col_offset = maxval (col)
    deallocate (col, acl)
    if (associated (evt%shower)) then
       LOOP_SHOWERS: do k = 1, size (evt%shower)
          if (shower_has_particles (evt%shower(k))) then
             ii = ii + 1
             call shower_get_initiator (evt%shower(k), prt0)
             idup(ii) = 0
             istup(ii) = 2
             mothup(:,ii) = (/ 1, 2 /)
             icolup(:,ii) = 0
             pup((/4,1,2,3/),ii) = array (particle_four_momentum (prt0))
             pup(5,ii) = particle_mass (prt0)
             vtimup(ii) = 0
             ispinup(ii) = 9
             spinup(ii) = ispinup(i)
             j = ii
             call shower_get_final_particles (evt%shower(k), prt, col, acl)
             do i = 1, size (prt)
                ii = ii + 1
                idup(ii) = particle_code (prt(i))
                istup(ii) = 1
                mothup(:,ii) = j
                icolup(1,ii) = col(i) + col_offset
                icolup(2,ii) = acl(i) + col_offset
                pup((/4,1,2,3/),ii) = array (particle_four_momentum (prt(i)))
                pup(5,ii) = particle_mass (prt(i))
                vtimup(ii) = 0
                ispinup(ii) = 9
                spinup(ii) = ispinup(ii)
             end do
             col_offset = col_offset + shower_get_n_colors (evt%shower(k))
             deallocate (prt, col, acl)
          end if
       end do LOOP_SHOWERS
    end if
    nup = ii
    xwgtup = 1
    scalup = evt%energy_scale
    aqedup = -1
    aqcdup = -1
!    call hepeup_write (6)
  contains
    subroutine translate_color_info &
         & (color_index, anticolor_index, color_flow, anticolor_flow)
      integer, dimension(:), intent(out) :: color_index, anticolor_index
      integer, dimension(:), intent(in) :: color_flow, anticolor_flow
      integer :: i, cl
      cl = MAXNUP
      color_index = 0
      anticolor_index = 0
      do i = 1, size (color_flow)
         if (color_index(i) == 0) then
            if (color_flow(i) /= 0) then
               cl = cl + 1
               color_index(i) = cl
               anticolor_index(color_flow(i)) = cl
            end if
         end if
         if (anticolor_index(i) == 0) then
            if (anticolor_flow(i) /= 0) then
               cl = cl + 1
               anticolor_index(i) = cl
               color_index(anticolor_flow(i)) = cl
            end if
         end if
      end do
    end subroutine translate_color_info

  end subroutine hepeup_fill

  subroutine hepeup_write_unit (u)
    integer, intent(in) :: u
    integer :: i
    write(u,*) "HEPEUP contents:"
    write(u,*) " NUP    =", nup
    write(u,*) " IDPRUP =", idprup
    write(u,*) " XWGTUP =", xwgtup
    write(u,*) " SCALUP =", scalup
    write(u,*) " AQEDUP =", aqedup
    write(u,*) " AQCDUP =", aqcdup
    do i = 1, nup
       write(u,*) " Particle", i
       write(u,*) "   IDUP     =", idup(i)
       write(u,*) "   ISTUP    =", istup(i)
       write(u,*) "   MOTHUP   =", mothup(:,i)
       write(u,*) "   ICOLUP   =", icolup(:,i)
       write(u,*) "   PUP(1:3) =", pup(1:3,i)
       write(u,*) "   PUP(4,5) =", pup(4:5,i)
       write(u,*) "   VTIMUP   =", vtimup(i)
       write(u,*) "   SPINUP   =", spinup(i)
    end do
  end subroutine hepeup_write_unit

  subroutine lhef_write_init (unit, process_id)
    integer, intent(in) :: unit
    integer :: i
    character(*), dimension(:), intent(in) :: process_id
    write (unit, "(A)")  '<LesHouchesEvents version="1.0">'
    write (unit, "(A)")  '<header>'
    write (unit, "(2x,A)")  '<generator>'
    write (unit, "(4x,A)")  &
         '<name version="1.95" date="Feb 25 2010">' // &
         'WHIZARD</name>'
    do i = 1, size (process_id)
       write (unit, "(4x,A)") &
            '<process_id>' // trim (adjustl (process_id(i))) &
            // '</process_id>'
    end do
    write (unit, "(2x,A)")  '</generator>'
    write (unit, "(A)")  '</header>'
  end subroutine lhef_write_init

  subroutine lhef_write_heprup (unit, process_id)
    character(*), dimension(:), intent(in) :: process_id
    integer, intent(in) :: unit
    integer :: i
    write (unit, "(A)") '<init>'
    write (unit, "(2(2x,I10),2(2x,G22.15E3),8x,6(2x,I10))") &
         idbmup(1:2), ebmup(1:2), &
         pdfgup(1:2), pdfsup(1:2), &
         idwtup, nprup
    do i = 1, size (process_id)
       write (unit, "(3(2x,G22.15E3),8x,1(2x,I10))") &
            xsecup(i), xerrup(i), xmaxup(i), lprup(i)
    end do
    write (unit, "(A)") '</init>'
  end subroutine lhef_write_heprup

  subroutine lhef_write_hepeup (unit, pro_id)
    integer, intent(in) :: unit, pro_id
    integer :: i
    idprup = pro_id
    write (unit, "(A)") '<event>'
    write (unit, "(2(2x,I10),2(2x,G22.15E3),8x,2(2x,G22.15E3))") &
         nup, idprup, xwgtup, scalup, aqedup, aqcdup
    do i = 1, nup
       write (unit, "(6(2x,I10),8x,3(2x,G22.15E3),8x,2(2x,G22.15E3),32x,2(2x,G22.15E3))") &
            idup(i), istup(i), &
            mothup(1:2,i), icolup(1:2,i), &
            pup(1:5,i), vtimup(i), spinup(i)
    end do
    write (unit, "(A)") '</event>'
  end subroutine lhef_write_hepeup

  subroutine lhef_write_final (unit)
    integer, intent(in) :: unit
    write (unit, "(A)")  '</LesHouchesEvents>'
  end subroutine lhef_write_final


end module hepevt_common
