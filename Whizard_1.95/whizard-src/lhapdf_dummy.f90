! WHIZARD 1.95 Feb 25 2010
! 
! (C) 1999-2008 by 
!     Wolfgang Kilian <wolfgang.kilian@desy.de>
!     Thorsten Ohl <ohl@physik.uni-wuerzburg.de>
!     Juergen Reuter <juergen.reuter@physik.uni-freiburg.de>
!
! WHIZARD is free software; you can redistribute it and/or modify it
! under the terms of the GNU General Public License as published by 
! the Free Software Foundation; either version 2, or (at your option)
! any later version.
!
! WHIZARD is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! This file has been stripped of most comments.  For documentation, refer
! to the source 'whizard.nw'

module lhapdf_interface

  use kinds, only: default !NODEP!
  use limits, only: PDFSET_CHAR_LEN
  use diagnostics, only: msg_fatal

  implicit none
  private

  public :: lhapdflib_init, lhapdflib_strfun_array

  integer, parameter :: TQUARK = 6

contains

  subroutine lhapdflib_init (pdg_code_in, file, set, first)
    integer, intent(in) :: pdg_code_in, set
    character(len=PDFSET_CHAR_LEN), intent(in) :: file
    logical, intent(in), optional :: first
    call msg_fatal &
         (" LHAPDF not linked.  LHAPDF structure functions are unavailable.")
  end subroutine lhapdflib_init

  subroutine lhapdflib_strfun_array (pdg_code_in, set, x, scale, rho)
    integer, intent(in) :: pdg_code_in, set
    real(kind=default), intent(in) :: x, scale
    real(kind=default), dimension(-TQUARK:TQUARK), intent(out) :: rho
    rho = 0
    call msg_fatal &
         (" LHAPDF not linked.  LHAPDF structure functions are unavailable.")
  end subroutine lhapdflib_strfun_array

end module lhapdf_interface

