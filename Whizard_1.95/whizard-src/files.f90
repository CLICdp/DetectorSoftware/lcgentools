! WHIZARD 1.95 Feb 25 2010
! 
! (C) 1999-2008 by 
!     Wolfgang Kilian <wolfgang.kilian@desy.de>
!     Thorsten Ohl <ohl@physik.uni-wuerzburg.de>
!     Juergen Reuter <juergen.reuter@physik.uni-freiburg.de>
!
! WHIZARD is free software; you can redistribute it and/or modify it
! under the terms of the GNU General Public License as published by 
! the Free Software Foundation; either version 2, or (at your option)
! any later version.
!
! WHIZARD is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! This file has been stripped of most comments.  For documentation, refer
! to the source 'whizard.nw'

module files

  use limits, only: DEFAULT_FILENAME, FILENAME_LEN, BUFFER_SIZE
  use diagnostics, only: msg_bug
  
  implicit none
  private

  type, public :: line_t
     private
     type(line_t), pointer :: previous, next
     character(len=BUFFER_SIZE) :: string
  end type line_t

  type, public :: line_list
     private
     type (line_t), pointer :: first, last
  end type line_list

  public :: choose_filename
  public :: concat
  public :: file_exists_else_default
  public :: create, destroy, read, write
  public :: line_string
  public :: previous, next
  public :: append
  public :: first_line, last_line
  public :: line_list_nth

  interface concat
     module procedure concat_two, concat_three
  end interface
  interface create
     module procedure line_create
  end interface
  interface destroy
     module procedure line_destroy
  end interface
  interface read
     module procedure line_read_unit
  end interface
  interface write
     module procedure line_write_unit
  end interface
  interface previous
     module procedure line_previous
  end interface
  interface next
     module procedure line_next
  end interface
  interface create
     module procedure line_list_create
  end interface
  interface reset
     module procedure line_list_create
  end interface
  interface destroy
     module procedure line_list_destroy
  end interface
  interface read
     module procedure line_list_read_unit
  end interface
  interface write
     module procedure line_list_write_unit
  end interface
  interface append
     module procedure line_list_append_string, line_list_append_line
     module procedure line_list_append_list
  end interface

contains

  function choose_filename (file, default_file) result (chosen_file)
    character(len=*), intent(in) :: file, default_file
    character(len=FILENAME_LEN) :: chosen_file
    if (len_trim (file) == 0) then
       chosen_file = default_file
    else
       chosen_file = file
    end if
  end function choose_filename

  function concat_two (prefix, filename, extension) result (file)
    character(len=*) :: prefix, filename, extension
    character(len=FILENAME_LEN) :: file
    file = trim(filename)//"."//trim(extension)
    if (prefix /= "" .and. filename(1:1) /= "/" .and. filename(1:2) /= "./") &
         & file = trim(prefix) // "/" // file
  end function concat_two

  function concat_three (prefix, filename, process_id, extension) result (file)
    character(len=*) :: prefix, filename, process_id, extension
    character(len=FILENAME_LEN) :: file
    file = trim(filename)//"."//trim(process_id)//"."//trim(extension)
    if (prefix /= "" .and. filename(1:1) /= "/" .and. filename(1:2) /= "./") &
         & file = trim(prefix) // "/" // file
  end function concat_three

  function file_exists_else_default (prefix, filename, extension) result (file)
    character(len=*) :: prefix, filename, extension
    character(len=FILENAME_LEN) :: file
    logical :: exist
    file = concat (prefix, filename, extension)
    inquire (file=trim(file), exist=exist)
    if (.not.exist) then
       file = concat (prefix, DEFAULT_FILENAME, extension)
       inquire (file=trim(file), exist=exist)
       if (.not.exist)  then
          file = ""
       end if
    end if
  end function file_exists_else_default

  subroutine line_create (line, string, previous)
    type(line_t), pointer :: line
    character(len=*), intent(in) :: string
    type(line_t), pointer, optional :: previous
    if (associated (line))  call msg_bug &
         & (" Attempt to allocate line pointer which was already allocated")
    allocate (line)
    line%string = string
    if (present(previous)) then
       if (associated (previous))  previous%next => line
       line%previous => previous
    else
       nullify (line%previous)
    end if
    nullify (line%next)
  end subroutine line_create

  subroutine line_destroy (line)
    type(line_t), pointer :: line, next
    if (associated (line)) then
       if (associated (line%previous)) &
            & line%previous%next => line%next
       if (associated (line%next)) &
            & line%next%previous => line%previous
       next => line%next
       deallocate (line)
       line => next
    end if
  end subroutine line_destroy

  function line_string (line)
    type(line_t), pointer :: line
    character(len=BUFFER_SIZE) :: line_string
    line_string = line%string
  end function line_string

  function line_previous (line)
    type(line_t), pointer :: line
    type(line_t), pointer :: line_previous
    line_previous => line%previous
  end function line_previous

  function line_next (line)
    type(line_t), pointer :: line
    type(line_t), pointer :: line_next
    line_next => line%next
  end function line_next

  subroutine line_read_unit (unit, line, iostat)
    integer, intent(in) :: unit
    type(line_t), pointer :: line, previous
    integer, intent(out) :: iostat
    character(len=BUFFER_SIZE) :: string
    read (unit=unit, fmt='(A)', iostat=iostat) string
    if (iostat == 0) then
       previous => line
       nullify (line)
       call create (line, string, previous=previous)
    end if
  end subroutine line_read_unit

  subroutine line_write_unit (unit, line)
    integer, intent(in) :: unit
    type(line_t), pointer :: line
    if (associated (line)) then
       write (unit, '(A)')  trim (line_string (line))
       line => next (line)
    end if
  end subroutine line_write_unit

  subroutine line_list_create (ll)
    type(line_list), intent(inout) :: ll
    nullify (ll%first)
    nullify (ll%last)
  end subroutine line_list_create

  subroutine line_list_destroy (ll)
    type(line_list), intent(inout) :: ll
    do while (associated(ll%first))
       call destroy (ll%first)
    end do
    nullify (ll%last)
  end subroutine line_list_destroy

  subroutine line_list_read_unit (unit, ll, iostat)
    integer, intent(in) :: unit
    type(line_list), intent(inout) :: ll
    integer, intent(out), optional :: iostat
    integer :: ios
    call create (ll)
    nullify (ll%last)
    READLINE: do
       call read (unit, ll%last, ios)
       if (ios /= 0) exit READLINE
       if (.not.associated (ll%last%previous))  ll%first => ll%last
    end do READLINE
    if (present(iostat))  iostat = ios
  end subroutine line_list_read_unit

  subroutine line_list_write_unit (unit, ll)
    integer, intent(in) :: unit
    type(line_list), intent(in) :: ll
    type(line_t), pointer :: line
    line => ll%first
    do while (associated (line))
       call write (unit, line)
    end do
  end subroutine line_list_write_unit

  subroutine line_list_append_string (ll, string)
    type(line_list), intent(inout) :: ll
    character(len=*), intent(in) :: string
    type(line_t), pointer :: line, line_previous
    !!! gfortran 4.3.0 workaround, which does not understand
    !!!                            functions on pointers
    !!! type(line_t), pointer :: line
    nullify (line)
    call create (line, string, ll%last)
    ll%last => line
    line_previous => line%previous
    if (.not.associated (line_previous)) ll%first => ll%last
    !!! gfortran 4.3.0 workaround, which does not understand
    !!!                            functions on pointers
    !!! if (.not.associated (previous (line)))  ll%first => ll%last
  end subroutine line_list_append_string

  subroutine line_list_append_line (ll, line)
    type(line_list), intent(inout) :: ll
    type(line_t), pointer :: line
    call line_list_append_string (ll, line_string (line))
  end subroutine line_list_append_line

  subroutine line_list_append_list (ll, al)
    type(line_list), intent(inout) :: ll
    type(line_list), intent(in) :: al
    type(line_t), pointer :: line
    line => al%first
    do while (associated (line))
       call line_list_append_line (ll, line)
       line => next (line)
    end do
  end subroutine line_list_append_list

  function first_line (ll) result (first)
    type(line_list), intent(in) :: ll
    type(line_t), pointer :: first
    first => ll%first
  end function first_line

  function last_line (ll) result (last)
    type(line_list), intent(in) :: ll
    type(line_t), pointer :: last
    last => ll%last
  end function last_line

  function line_list_nth (ll, n) result (alias)
    type(line_list), intent(in) :: ll
    integer, intent(in) :: n
    type(line_list) :: alias
    integer :: i
    call create (alias)
    i = 1
    alias%first => ll%first
    SCAN: do while (associated (alias%first))
       if (i >= n) then
          alias%last => ll%last
          exit SCAN
       else
          i = i + 1
          alias%first => alias%first%next
       end if
    end do SCAN
  end function line_list_nth


end module files
