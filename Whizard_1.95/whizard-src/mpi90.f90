! WHIZARD 1.95 Feb 25 2010
! 
! (C) 1999-2008 by 
!     Wolfgang Kilian <wolfgang.kilian@desy.de>
!     Thorsten Ohl <ohl@physik.uni-wuerzburg.de>
!     Juergen Reuter <juergen.reuter@physik.uni-freiburg.de>
!
! WHIZARD is free software; you can redistribute it and/or modify it
! under the terms of the GNU General Public License as published by 
! the Free Software Foundation; either version 2, or (at your option)
! any later version.
!
! WHIZARD is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! This file has been stripped of most comments.  For documentation, refer
! to the source 'whizard.nw'

module mpi90

  use kinds, only: double !NODEP!
  use kinds, only: quad_or_single !NODEP!
  use vamp_rest, only: vamp_grids !NODEP!

  implicit none
  private

  integer, parameter, public :: MPI_SUCCESS = 0

  public :: mpi90_init, mpi90_finalize
  public :: mpi90_abort, mpi90_print_error
  public :: mpi90_size, mpi90_rank
  public :: mpi90_send
  public :: mpi90_receive, mpi90_receive_pointer
  public :: mpi90_broadcast
  public :: vamp_broadcast_grids

  type, public :: mpi90_status
     integer :: count, source, tag, error
  end type mpi90_status

  type(mpi90_status), parameter :: MPI_DUMMY_STATUS = mpi90_status(0, 0, 0, 0)

  interface mpi90_send
     module procedure &
          mpi90_send_integer, mpi90_send_double, &
          mpi90_send_integer_array, mpi90_send_double_array, &
          mpi90_send_integer_array2, mpi90_send_double_array2
  end interface
  interface mpi90_receive
     module procedure &
          mpi90_receive_integer, mpi90_receive_double, &
          mpi90_receive_integer_array, mpi90_receive_double_array, &
          mpi90_receive_integer_array2, mpi90_receive_double_array2
  end interface
  interface mpi90_receive_pointer
     module procedure &
          mpi90_receive_integer_pointer, mpi90_receive_double_pointer
  end interface
  interface mpi90_broadcast
     module procedure &
          mpi90_broadcast_integer, mpi90_broadcast_integer_array, &
          mpi90_broadcast_integer_array2, mpi90_broadcast_integer_array3, &
          mpi90_broadcast_double, mpi90_broadcast_double_array, &
          mpi90_broadcast_double_array2, mpi90_broadcast_double_array3, &
          mpi90_broadcast_quad, mpi90_broadcast_quad_array, &
          mpi90_broadcast_quad_array2, mpi90_broadcast_quad_array3, &
          mpi90_broadcast_logical, mpi90_broadcast_logical_array, &
          mpi90_broadcast_logical_array2, mpi90_broadcast_logical_array3
  end interface

contains

  subroutine mpi90_init (error)
    integer, intent(out), optional :: error
    if (present(error)) error = MPI_SUCCESS
  end subroutine mpi90_init
  subroutine mpi90_finalize (error)
    integer, intent(out), optional :: error
    if (present(error)) error = MPI_SUCCESS
  end subroutine mpi90_finalize
  subroutine mpi90_abort (code, domain, error)
    integer, intent(in), optional :: code, domain
    integer, intent(out), optional :: error
    if (present(error)) error = MPI_SUCCESS
    stop 'MPI90 abort called in serial mode'
  end subroutine mpi90_abort
  subroutine mpi90_print_error (error, msg)
    integer, intent(in) :: error
    character(len=*), optional :: msg
    if (error/=0) then
       print *, 'Error =', error
       stop 'MPI90 print-error called in serial mode'
    else if (present(msg)) then
       print *, trim(msg)
       stop 'MPI90 print-error called in serial mode'
    end if
  end subroutine mpi90_print_error

  subroutine mpi90_size (sz, domain, error)
    integer, intent(out) :: sz
    integer, intent(in), optional :: domain
    integer, intent(out), optional :: error
    sz = 1
    if (present(error)) error = MPI_SUCCESS
  end subroutine mpi90_size
  subroutine mpi90_rank (rank, domain, error)
    integer, intent(out) :: rank
    integer, intent(in), optional :: domain
    integer, intent(out), optional :: error
    rank = 0
    if (present(error)) error = MPI_SUCCESS
  end subroutine mpi90_rank

  subroutine mpi90_send_integer (value, target, tag, domain, error)
    integer, intent(in) :: value
    integer, intent(in) :: target, tag
    integer, intent(in), optional :: domain
    integer, intent(out), optional :: error
    if (present(error)) error = MPI_SUCCESS
  end subroutine mpi90_send_integer
  subroutine mpi90_send_double (value, target, tag, domain, error)
    real(kind=double), intent(in) :: value
    integer, intent(in) :: target, tag
    integer, intent(in), optional :: domain
    integer, intent(out), optional :: error
    if (present(error)) error = MPI_SUCCESS
  end subroutine mpi90_send_double
  subroutine mpi90_send_integer_array (buffer, target, tag, domain, error)
    integer, dimension(:), intent(in) :: buffer
    integer, intent(in) ::  target, tag
    integer, intent(in), optional :: domain
    integer, intent(out), optional :: error
    if (present(error)) error = MPI_SUCCESS
  end subroutine mpi90_send_integer_array
  subroutine mpi90_send_double_array (buffer, target, tag, domain, error)
    real(kind=double), dimension(:), intent(in) :: buffer
    integer, intent(in) :: target, tag
    integer, intent(in), optional :: domain
    integer, intent(out), optional :: error
    if (present(error)) error = MPI_SUCCESS
  end subroutine mpi90_send_double_array
  subroutine mpi90_send_integer_array2 (value, target, tag, domain, error)
    integer, dimension(:,:), intent(in) :: value
    integer, intent(in) :: target, tag
    integer, intent(in), optional :: domain
    integer, intent(out), optional :: error
    if (present(error)) error = MPI_SUCCESS
  end subroutine mpi90_send_integer_array2
  subroutine mpi90_send_double_array2 (value, target, tag, domain, error)
    real(kind=double), dimension(:,:), intent(in) :: value
    integer, intent(in) :: target, tag
    integer, intent(in), optional :: domain
    integer, intent(out), optional :: error
    if (present(error)) error = MPI_SUCCESS
  end subroutine mpi90_send_double_array2

  subroutine mpi90_receive_error
    stop 'MPI90 receive called in serial mode'
  end subroutine mpi90_receive_error
  subroutine mpi90_receive_integer (value, source, tag, domain, status, error)
    integer, intent(out) :: value
    integer, intent(in), optional :: source, tag, domain
    type(mpi90_status), intent(out), optional :: status
    integer, intent(out), optional :: error
    value = 0
    if (present(status)) status = MPI_DUMMY_STATUS
    if (present(error)) error = 0
    call mpi90_receive_error
  end subroutine mpi90_receive_integer
  subroutine mpi90_receive_double (value, source, tag, domain, status, error)
    real(kind=double), intent(out) :: value
    integer, intent(in), optional :: source, tag, domain
    type(mpi90_status), intent(out), optional :: status
    integer, intent(out), optional :: error
    value = 0
    if (present(status)) status = MPI_DUMMY_STATUS
    if (present(error)) error = 0
    call mpi90_receive_error
  end subroutine mpi90_receive_double
  subroutine mpi90_receive_integer_array &
       (buffer, source, tag, domain, status, error)
    integer, dimension(:), intent(out) :: buffer
    integer, intent(in), optional :: source, tag, domain
    type(mpi90_status), intent(out), optional :: status
    integer, intent(out), optional :: error
    buffer = 0
    if (present(status)) status = MPI_DUMMY_STATUS
    if (present(error)) error = 0
    call mpi90_receive_error
  end subroutine mpi90_receive_integer_array
  subroutine mpi90_receive_double_array &
       (buffer, source, tag, domain, status, error)
    real(kind=double), dimension(:), intent(out) :: buffer
    integer, intent(in), optional :: source, tag, domain
    type(mpi90_status), intent(out), optional :: status
    integer, intent(out), optional :: error
    buffer = 0
    if (present(status)) status = MPI_DUMMY_STATUS
    if (present(error)) error = 0
    call mpi90_receive_error
  end subroutine mpi90_receive_double_array
  subroutine mpi90_receive_integer_array2 &
       (value, source, tag, domain, status, error)
    integer, dimension(:,:), intent(out) :: value
    integer, intent(in), optional :: source, tag, domain
    type(mpi90_status), intent(out), optional :: status
    integer, intent(out), optional :: error
    value = 0
    if (present(status)) status = MPI_DUMMY_STATUS
    if (present(error)) error = 0
    call mpi90_receive_error
  end subroutine mpi90_receive_integer_array2
  subroutine mpi90_receive_double_array2 &
       (value, source, tag, domain, status, error)
    real(kind=double), dimension(:,:), intent(out) :: value
    integer, intent(in), optional :: source, tag, domain
    type(mpi90_status), intent(out), optional :: status
    integer, intent(out), optional :: error
    value = 0
    if (present(status)) status = MPI_DUMMY_STATUS
    if (present(error)) error = 0
    call mpi90_receive_error
  end subroutine mpi90_receive_double_array2
  subroutine mpi90_receive_integer_pointer &
       (buffer, source, tag, domain, status, error)
    integer, dimension(:), pointer :: buffer
    integer, intent(in), optional :: source, tag, domain
    type(mpi90_status), intent(out), optional :: status
    integer, intent(out), optional :: error
    if (present(status)) status = MPI_DUMMY_STATUS
    if (present(error)) error = 0
    call mpi90_receive_error
  end subroutine mpi90_receive_integer_pointer
  subroutine mpi90_receive_double_pointer &
       (buffer, source, tag, domain, status, error)
    real(kind=double), dimension(:), pointer :: buffer
    integer, intent(in), optional :: source, tag, domain
    type(mpi90_status), intent(out), optional :: status
    integer, intent(out), optional :: error
    if (present(status)) status = MPI_DUMMY_STATUS
    if (present(error)) error = 0
    call mpi90_receive_error
  end subroutine mpi90_receive_double_pointer

  subroutine mpi90_broadcast_integer (value, root, domain, error)
    integer, intent(inout) :: value
    integer, intent(in) :: root
    integer, intent(in), optional :: domain
    integer, intent(out), optional :: error
    if (present(error)) error = MPI_SUCCESS
  end subroutine mpi90_broadcast_integer
  subroutine mpi90_broadcast_double (value, root, domain, error)
    real(kind=double), intent(inout) :: value
    integer, intent(in) :: root
    integer, intent(in), optional :: domain
    integer, intent(out), optional :: error
    if (present(error)) error = MPI_SUCCESS
  end subroutine mpi90_broadcast_double
  subroutine mpi90_broadcast_quad (value, root, domain, error)
    real(kind=quad_or_single), intent(inout) :: value
    integer, intent(in) :: root
    integer, intent(in), optional :: domain
    integer, intent(out), optional :: error
    if (present(error)) error = MPI_SUCCESS
  end subroutine mpi90_broadcast_quad
  subroutine mpi90_broadcast_logical (value, root, domain, error)
    logical, intent(inout) :: value
    integer, intent(in) :: root
    integer, intent(in), optional :: domain
    integer, intent(out), optional :: error
    if (present(error)) error = MPI_SUCCESS
  end subroutine mpi90_broadcast_logical
  subroutine mpi90_broadcast_integer_array (buffer, root, domain, error)
    integer, dimension(:), intent(inout) :: buffer
    integer, intent(in) ::  root
    integer, intent(in), optional :: domain
    integer, intent(out), optional :: error
    if (present(error)) error = MPI_SUCCESS
  end subroutine mpi90_broadcast_integer_array
  subroutine mpi90_broadcast_double_array (buffer, root, domain, error)
    real(kind=double), dimension(:), intent(inout) :: buffer
    integer, intent(in) :: root
    integer, intent(in), optional :: domain
    integer, intent(out), optional :: error
    if (present(error)) error = MPI_SUCCESS
  end subroutine mpi90_broadcast_double_array
  subroutine mpi90_broadcast_quad_array (buffer, root, domain, error)
    real(kind=quad_or_single), dimension(:), intent(inout) :: buffer
    integer, intent(in) :: root
    integer, intent(in), optional :: domain
    integer, intent(out), optional :: error
    if (present(error)) error = MPI_SUCCESS
  end subroutine mpi90_broadcast_quad_array
  subroutine mpi90_broadcast_logical_array (buffer, root, domain, error)
    logical, dimension(:), intent(inout) :: buffer
    integer, intent(in) :: root
    integer, intent(in), optional :: domain
    integer, intent(out), optional :: error
    if (present(error)) error = MPI_SUCCESS
  end subroutine mpi90_broadcast_logical_array
  subroutine mpi90_broadcast_integer_array2 (value, root, domain, error)
    integer, dimension(:,:), intent(inout) :: value
    integer, intent(in) :: root
    integer, intent(in), optional :: domain
    integer, intent(out), optional :: error
    if (present(error)) error = MPI_SUCCESS
  end subroutine mpi90_broadcast_integer_array2
  subroutine mpi90_broadcast_double_array2 (value, root, domain, error)
    real(kind=double), dimension(:,:), intent(inout) :: value
    integer, intent(in) :: root
    integer, intent(in), optional :: domain
    integer, intent(out), optional :: error
    if (present(error)) error = MPI_SUCCESS
  end subroutine mpi90_broadcast_double_array2
  subroutine mpi90_broadcast_quad_array2 (value, root, domain, error)
    real(kind=quad_or_single), dimension(:,:), intent(inout) :: value
    integer, intent(in) :: root
    integer, intent(in), optional :: domain
    integer, intent(out), optional :: error
    if (present(error)) error = MPI_SUCCESS
  end subroutine mpi90_broadcast_quad_array2
  subroutine mpi90_broadcast_logical_array2 (value, root, domain, error)
    logical, dimension(:,:), intent(inout) :: value
    integer, intent(in) :: root
    integer, intent(in), optional :: domain
    integer, intent(out), optional :: error
    if (present(error)) error = MPI_SUCCESS
  end subroutine mpi90_broadcast_logical_array2
  subroutine mpi90_broadcast_integer_array3 (value, root, domain, error)
    integer, dimension(:,:,:), intent(inout) :: value
    integer, intent(in) :: root
    integer, intent(in), optional :: domain
    integer, intent(out), optional :: error
    if (present(error)) error = MPI_SUCCESS
  end subroutine mpi90_broadcast_integer_array3
  subroutine mpi90_broadcast_double_array3 (value, root, domain, error)
    real(kind=double), dimension(:,:,:), intent(inout) :: value
    integer, intent(in) :: root
    integer, intent(in), optional :: domain
    integer, intent(out), optional :: error
    if (present(error)) error = MPI_SUCCESS
  end subroutine mpi90_broadcast_double_array3
  subroutine mpi90_broadcast_quad_array3 (value, root, domain, error)
    real(kind=quad_or_single), dimension(:,:,:), intent(inout) :: value
    integer, intent(in) :: root
    integer, intent(in), optional :: domain
    integer, intent(out), optional :: error
    if (present(error)) error = MPI_SUCCESS
  end subroutine mpi90_broadcast_quad_array3
  subroutine mpi90_broadcast_logical_array3 (value, root, domain, error)
    logical, dimension(:,:,:), intent(inout) :: value
    integer, intent(in) :: root
    integer, intent(in), optional :: domain
    integer, intent(out), optional :: error
    if (present(error)) error = MPI_SUCCESS
  end subroutine mpi90_broadcast_logical_array3

  subroutine vamp_broadcast_grids(g, root, domain, error)
    type(vamp_grids), intent(inout) :: g
    integer, intent(in) :: root
    integer, intent(in), optional :: domain
    integer, intent(out), optional :: error
    if (present(error)) error = MPI_SUCCESS
  end subroutine vamp_broadcast_grids


end module mpi90
