! WHIZARD 1.95 Feb 25 2010
! 
! (C) 1999-2008 by 
!     Wolfgang Kilian <wolfgang.kilian@desy.de>
!     Thorsten Ohl <ohl@physik.uni-wuerzburg.de>
!     Juergen Reuter <juergen.reuter@physik.uni-freiburg.de>
!
! WHIZARD is free software; you can redistribute it and/or modify it
! under the terms of the GNU General Public License as published by 
! the Free Software Foundation; either version 2, or (at your option)
! any later version.
!
! WHIZARD is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! This file has been stripped of most comments.  For documentation, refer
! to the source 'whizard.nw'

module partons

  use kinds, only: default !NODEP!
  use constants, only: twopi, twopi3
  use diagnostics, only: msg_buffer, msg_message, msg_fatal, msg_bug
  use lorentz
  use particles, only: UNDEFINED
  use particles, only: T_QUARK, GLUON, GLUON2, PHOTON, HADRON_REMNANT
  use particles, only: particle_multiplicity, particle_name, is_antiparticle
  use particles, only: particle, particle_null, particle_new, write
  use particles, only: particle_four_momentum
  use particles, only: unpolarized_density_matrix
  use density_vectors, only: state_density_t, create, destroy, write, set
  use density_vectors, only: state_density_matrix_to_vector
  use beams, only: beam_data_t
  use beams, only: read_FILE_beam_event
  use user, only: strfun_USER_spectrum_double => spectrum_double
  use user, only: strfun_USER_spectrum_single => spectrum_single
  use beams, only: strfun_SCAN_spectrum
  use beams, only: strfun_CIRCE_map, strfun_CIRCE, generate_CIRCE
  use beams, only: strfun_CIRCE2_polarized, strfun_CIRCE2_unpolarized
  use beams, only: generate_CIRCE2
  use beams, only: strfun_ISR
  use beams, only: strfun_EPA
  use pdflib_interface, only: pdflib_init, pdflib_strfun_array !NODEP!
  use lhapdf_interface, only: lhapdflib_init, lhapdflib_strfun_array !NODEP!
  use beams, only: strfun_EWA
  use user, only: strfun_USER_strfun_double => strfun_double
  use user, only: strfun_USER_strfun_single => strfun_single

  implicit none
  private

  type, public :: parton_state
     real(kind=default) :: sqrts, energy_scale
     integer, dimension(:), allocatable :: beam_code, code
     logical :: fixed_energy, cm_frame, single_particle
     type(lorentz_transformation) :: L_cm
     logical :: polarized_beams, polarized_me
     logical :: generic_polarization, correlated_polarization
     logical, dimension(:), allocatable :: vector_polarization
     complex(kind=default), dimension(:,:,:), allocatable :: rho_hel
     real(kind=default) :: strfun_factor
     real(kind=default), dimension(:), allocatable :: x
     real(kind=default), dimension(:,:), allocatable :: PDF_rho_flv
     real(kind=default) :: x_fixed
     integer :: n_strfun, strfun_count
     integer :: n_in, n_col, n_hel, n_flv
     type(state_density_t), pointer :: rho
     logical :: beam_recoil, conserve_momentum
     type(four_momentum), dimension(:), allocatable :: recoil_momentum
     type(particle), dimension(:), pointer :: beam_remnant
     integer :: n_beam_remnants
  end type parton_state


  public :: create, destroy
  public :: write
  public :: structure_functions

  interface create
     module procedure parton_state_create
  end interface
  interface destroy
     module procedure parton_state_destroy
  end interface
  interface write
     module procedure parton_state_write_unit
  end interface
  interface set_beam_remnant
     module procedure set_beam_remnant_collinear
     module procedure set_beam_remnant_pt
  end interface

contains

  subroutine parton_state_create &
       & (ps, polarized_beams, polarized_me, vector_polarization, &
       &  beam_recoil, conserve_momentum, single_particle, &
       &  n_in, sqrts, beam, n_col, n_hel, n_flv)
    type(parton_state), intent(inout) :: ps
    logical, intent(in) :: polarized_beams, polarized_me
    logical, dimension(:), intent(in) :: vector_polarization
    logical, intent(in) :: beam_recoil, conserve_momentum, single_particle
    integer, intent(in) :: n_in
    real(kind=default), intent(in) :: sqrts
    type(beam_data_t), intent(in), dimension(:) :: beam
    integer, intent(in) :: n_col, n_hel, n_flv
    type(four_momentum), dimension(2) :: p
    integer :: i
    ps%n_in = n_in
    allocate (ps%beam_code(n_in), ps%code(n_in))
    ps%beam_code = 0
    ps%code = 0
    ps%fixed_energy = all (beam%fixed_energy)
    ps%sqrts = sqrts
    ps%cm_frame = all (beam%cm_frame)
    if (ps%cm_frame) then
       ps%L_cm = unit_lorentz_transformation
    else if (n_in == 1) then
       ps%L_cm = boost (three_momentum_moving &
            &              (beam(1)%momentum * beam(1)%direction), &
            &           sqrts)
    else
       do i = 1, 2
          p(i) = four_momentum_moving &
               &    (beam(i)%energy, &
               &     three_momentum_moving &
               &        (beam(i)%momentum * beam(i)%direction))
       end do
       ps%L_cm = transformation (3, p, ps%sqrts)
    end if
    ps%polarized_beams = polarized_beams
    ps%polarized_me = polarized_me
    ps%generic_polarization = any (vector_polarization)
    allocate (ps%vector_polarization(n_in))
    ps%vector_polarization = vector_polarization
    ps%correlated_polarization = .false.
    ps%energy_scale = 0
    ps%code = beam%particle_code
    ps%strfun_factor = 0
    allocate (ps%x(n_in))
    ps%x = 0
    ps%n_strfun = sum (beam%n_strfun)
    ps%strfun_count = 0
    ps%single_particle = single_particle
    ps%x_fixed = 0
    allocate (ps%rho_hel(-2:2,-2:2,n_in))
    ps%rho_hel = 0
    allocate (ps%PDF_rho_flv(-T_QUARK:T_QUARK,n_in))
    ps%PDF_rho_flv = 0
    ps%n_col = n_col
    ps%n_hel = n_hel
    ps%n_flv = n_flv
    if (n_col > 1)  call msg_bug &
         & (" Nontrivial initial color distribution.")
    allocate (ps%rho)
    call create (ps%rho, ps%n_col, ps%n_hel, ps%n_flv, &
         &       diagonal=.not.ps%generic_polarization)
    ps%beam_recoil = beam_recoil
    ps%conserve_momentum = conserve_momentum
    allocate (ps%recoil_momentum(n_in))
    ps%recoil_momentum = four_momentum_null
    allocate (ps%beam_remnant(sum(beam%n_strfun)))
    ps%beam_remnant = particle_null
    ps%n_beam_remnants = 0
  end subroutine parton_state_create

  subroutine parton_state_destroy (ps)
    type(parton_state), intent(inout) :: ps
    !XXX return [Old Intel compiler bug]
    call destroy (ps%rho)
    deallocate (ps%rho)
    deallocate (ps%recoil_momentum)
    deallocate (ps%beam_remnant)
    deallocate (ps%beam_code, ps%code)
    deallocate (ps%vector_polarization)
    deallocate (ps%rho_hel)
    deallocate (ps%x)
    deallocate (ps%PDF_rho_flv)
  end subroutine parton_state_destroy

  subroutine parton_state_write_unit (u, ps)
    integer, intent(in) :: u
    type(parton_state), intent(in) :: ps
    integer :: i,j
    write (u,*) "Partonic initial state:"
    write (u,*) "N_incoming       = ", ps%n_in
    write (u,*) "PDG codes (beam) = ", ps%beam_code
    write (u,*) "PDG codes        = ", ps%code
    write (u,*) "polarized beams  = ", ps%polarized_beams
    write (u,*) "polarized ME     = ", ps%polarized_me
    write (u,*) "sqrt(s)          = ", ps%sqrts
    write (u,*) "energy scale     = ", ps%energy_scale
    write (u,*) "single particle  = ", ps%single_particle
    write (u,*) "single prt: x0   = ", ps%x_fixed
    write (u,*) "#strfun: i, n    = ", ps%strfun_count, ps%n_strfun
    write (u,*) "strf.-factor     = ", ps%strfun_factor
    write (u,*) "x(1,2)           = ", ps%x
    write (u,*) "N_col/hel/flv    = ", ps%n_col, ps%n_hel, ps%n_flv
    write (u,*) "C.M. boost:"
    call write (u, ps%L_cm)
    if (ps%n_in == 2) then
       write (u,*) "PDF flavor probabilities:"
       call write_pdf_rho_flv (1, ps%PDF_rho_flv(:,1))
       call write_pdf_rho_flv (2, ps%PDF_rho_flv(:,2))
    end if
    write (u,*) "Helicity density matrices:"
    do i = 1, ps%n_in
       do j = -2, 2
          write (u,*) ps%rho_hel(j,-2,i), ps%rho_hel(j,-1,i), ps%rho_hel(j,0,i), &
             ps%rho_hel(j,1,i), ps%rho_hel(j,2,i)
       end do
    end do
    write (u,*) "State density:"
    call write (u, ps%rho)
    if (ps%n_beam_remnants > 0) then
       write (u,*) "Beam remnant(s):"
       do j=1, ps%n_beam_remnants
          call write (u, ps%beam_remnant(j))
       end do
    end if
  contains
    subroutine write_pdf_rho_flv (i, rho)
      integer, intent(in) :: i
      real(kind=default), dimension(-T_QUARK:T_QUARK), intent(in) :: rho
      write (u,"(1x,A,I1,A)")  "Beam ", i, ":"
      write (u,11) "d:", rho(1), "u:", rho(2), "s:", rho(3), &
           &       "c:", rho(4), "b:", rho(5), "t:", rho(6)
      write (u,11) "D:", rho(-1), "U:", rho(-2), "S:", rho(-3), &
           &       "C:", rho(-4), "B:", rho(-5), "T:", rho(-6)
      write (u,11) "g:", rho(0)
11    format (6(1x,A,1PE9.3))
    end subroutine write_pdf_rho_flv
  end subroutine parton_state_write_unit

  subroutine structure_functions &
       & (ps, beam, r, hel_state_in, flv_state_in, mass_out, &
       &  mc_random_number)
    type(parton_state), intent(inout) :: ps
    type(beam_data_t), dimension(:), intent(in) :: beam
    real(kind=default), dimension(:), intent(in) :: r
    integer, dimension(:,:), intent(in) :: hel_state_in, flv_state_in
    real(kind=default), intent(in) :: mass_out
    interface
       subroutine mc_random_number (r)
         use kinds, only: double !NODEP!
         real(kind=double) :: r
       end subroutine mc_random_number
    end interface
    integer :: r_pointer
    r_pointer = 1
    ps%beam_code = beam%particle_code
    ps%code = ps%beam_code
    ps%strfun_factor = 1
    ps%x = 1
    ps%n_beam_remnants = 0
    ps%beam_remnant = particle_null
    ps%recoil_momentum = four_momentum_null
    ps%strfun_count = 0
    ps%x_fixed = (mass_out / ps%sqrts)**2
    call init_beam_polarization (ps, beam)
    if (ps%n_in == 2) then
       call apply_FILE_events (ps, beam, r, r_pointer)
       call apply_SCAN_spectrum (ps, beam, r, r_pointer)
       call apply_USER_spectrum (ps, beam, r, r_pointer)
       call apply_CIRCE (ps, beam, r, r_pointer, mc_random_number)
       call apply_CIRCE2 (ps, beam, r, r_pointer, mc_random_number)
       call apply_ISR (ps, beam, r, r_pointer)
       call apply_EPA (ps, beam, r, r_pointer)
       call apply_PDF (ps, beam, r, r_pointer)
       call apply_LHAPDF (ps, beam, r, r_pointer)
       call apply_EWA (ps, beam, r, r_pointer)
       call apply_USER_strfun (ps, beam, r, r_pointer)
    end if
    call compose_density (ps, beam, hel_state_in, flv_state_in)
    call find_energy_scale (ps, beam)
  end subroutine structure_functions

  subroutine count_strfun (ps, bypass)
    type(parton_state), intent(inout) :: ps
    logical, intent(out), optional :: bypass
    ps%strfun_count = ps%strfun_count + 1
    if (ps%single_particle .and. ps%strfun_count == ps%n_strfun) then
       if (present (bypass)) then
          bypass = .true.
       else
          call msg_fatal (" Single-particle production not implemented for this combination of structure functions")
       end if
    else if (ps%strfun_count <= ps%n_strfun) then
       if (present (bypass))  bypass = .false.
    else
       call msg_bug (" Structure function counter mismatch")
    end if
  end subroutine count_strfun

  subroutine strfun_bypass_x (x, x_in, ps)
    real(kind=default), intent(out) :: x
    real(kind=default), intent(in) :: x_in
    type(parton_state), intent(inout) :: ps
    real(kind=default) :: x0
    x0 = x_in
    if (x0 > ps%x_fixed) then
       x = ps%x_fixed / x0
       ps%strfun_factor = ps%strfun_factor / (twopi3 * x0 * ps%sqrts**2)
    else
       x = ps%x_fixed
       ps%strfun_factor = 0
    end if
  end subroutine strfun_bypass_x

  subroutine init_beam_polarization (ps, beam)
    type(parton_state), intent(inout) :: ps
    type(beam_data_t), dimension(:), intent(in) :: beam
    real(kind=default), dimension(-2:2,ps%n_in) :: rho_vector
    real(kind=default), dimension(5) :: pol
    integer :: i, j
    ps%rho_hel = 0
    if (ps%polarized_me) then
       if (ps%polarized_beams .and. ps%generic_polarization) then
          do i=1, ps%n_in
             pol = beam(i)%polarization
             if (ps%vector_polarization(i)) then
                call beam_vector_polarization (ps%rho_hel(:,:,i), &
                     & beam(i)%particle_code, pol)
             else
                call beam_helicity_basis (rho_vector(:,i), &
                     & beam(i)%particle_code, pol, &
                     & ps%polarized_beams)
                do j = -2, 2
                   ps%rho_hel(j,j,i) = rho_vector(j,i)
                end do
             end if
          end do
       else
          do i=1, ps%n_in
             pol = beam(i)%polarization
             call beam_helicity_basis (rho_vector(:,i), &
                  & beam(i)%particle_code, pol, &
                  & ps%polarized_beams)
             do j = -2, 2
                ps%rho_hel(j,j,i) = rho_vector(j,i)
             end do
          end do
       end if
    else
       ps%rho_hel(0,0,:) = 1
    end if
  contains
    subroutine beam_helicity_basis (rho, code, pol, polarized)
      real(kind=default), dimension(-2:2), intent(out) :: rho
      integer, intent(in) :: code
      real(kind=default), dimension(:), intent(in) :: pol
      logical, intent(in) :: polarized
      integer :: mult
      real(kind=default), dimension(max(5,size(pol))) :: pol_tmp
      rho = 0
      mult = particle_multiplicity (code)
      if (polarized .and. mult>0) then
         pol_tmp = pol(:mult) + (1-sum(pol(:mult)))/mult
      else if (mult>0) then
         pol_tmp = 1._default / mult
      else
         pol_tmp = 0
      end if
      select case(mult)
      case (1)
         rho(0) = 1
      case (2)
         rho(-1) = pol_tmp(1)
         rho(1)  = pol_tmp(2)
      case (3)
         rho(-1:1) = pol_tmp(1:3)
      case (4) 
         rho(-2) = pol_tmp(1)
         rho(-1) = pol_tmp(2)
         rho(1)  = pol_tmp(3)
         rho(2)  = pol_tmp(4)
      case (5) 
         rho(-2:2) = pol_tmp(1:5)
      case default
         write (msg_buffer, "(1x,A,1x,I7,1x,A)") &
              & "Particle:", code, particle_name (code)
         call msg_message
         call msg_message (" Incoming beam: particle spin is undefined.")
         call msg_fatal (" Beam setup failed.")
      end select
    end subroutine beam_helicity_basis
    subroutine beam_vector_polarization (rho, code, polarization)
      complex(kind=default), dimension(-2:2,-2:2), intent(out) :: rho
      integer, intent(in) :: code
      real(kind=default), dimension(:), intent(in) :: polarization
      real(kind=default), dimension(5) :: pol
      integer :: mult
      pol = polarization
      rho = 0
      mult = particle_multiplicity (code)
      if (mult>2) then
      end if
      select case(mult)
      case (1)
         rho(0,0) = 1
      case (2)
         if (sum (pol**2) > 1)  pol = pol / sqrt (sum (pol**2))
         rho( 1, 1) = (1 + pol(3)) / 2
         if (is_antiparticle (code)) then
            rho( 1,-1) = cmplx(pol(1), pol(2),kind=default) / 2
            rho(-1, 1) = cmplx(pol(1),-pol(2),kind=default) / 2
         else
            rho( 1,-1) = cmplx(pol(1),-pol(2),kind=default) / 2
            rho(-1, 1) = cmplx(pol(1), pol(2),kind=default) / 2
         end if
         rho(-1,-1) = (1 - pol(3)) / 2
      case default
         write (msg_buffer, "(1x,A,1x,I7,1x,A)") &
              & "Beam particle:", code, particle_name (code)
         call msg_message
         call msg_message &
              & (" Generic polarization for spin > 1/2 not implemented yet")
         call msg_fatal (" Beam setup failed.")
      end select
    end subroutine beam_vector_polarization
  end subroutine init_beam_polarization

  subroutine average_beam_polarization (ps, prt_in, prt_out, i)
    type(parton_state), intent(inout) :: ps
    integer, intent(in) :: prt_in, prt_out, i
    if (ps%polarized_me .and. prt_in /= prt_out) then
       ps%rho_hel(:,:,i) = unpolarized_density_matrix (prt_out)
    end if
  end subroutine average_beam_polarization

  subroutine set_beam_remnant_collinear (ps, code, x, i, mass, ok)
    type(parton_state), intent(inout) :: ps
    real(kind=default), intent(in) :: x
    integer, intent(in) :: code, i
    real(kind=default), intent(in), optional :: mass
    logical, intent(out), optional :: ok
    integer :: n
    real(kind=default) :: E, p
    n = ps%n_beam_remnants + 1
    if (n > size(ps%beam_remnant)) &
         & call msg_bug (" Number of allowed beam remnants exceeded")
    E = max ((1-x) * ps%x(i) * ps%sqrts / 2, 0._default)
    call get_beam_momentum (p, E, i, mass, ok)
    ps%beam_remnant(n) = particle_new (code, p, 3) 
    ps%n_beam_remnants = n
  end subroutine set_beam_remnant_collinear

  subroutine set_beam_remnant_pt (ps, code, x, i, ct, st, phi, mass, ok)
    type(parton_state), intent(inout) :: ps
    real(kind=default), intent(in) :: x
    integer, intent(in) :: code, i
    real(kind=default), intent(in) :: ct, st, phi
    real(kind=default), intent(in), optional :: mass
    logical, intent(out), optional :: ok
    integer :: n
    real(kind=default) :: E, p, pt, cphi, sphi
    type(four_momentum) :: pvec
    n = ps%n_beam_remnants + 1
    if (n > size(ps%beam_remnant)) &
         & call msg_bug (" Number of allowed beam remnants exceeded")
    E = max ((1-x) * ps%x(i) * ps%sqrts / 2, 0._default)
    cphi = cos (phi)
    sphi = sin (phi)
    call get_beam_momentum (p, E, i, mass, ok)
    pt = p * st
    pvec = four_momentum_moving ((/ E, pt * cphi, pt * sphi, p * ct /))
    if (present (mass)) then
       ps%beam_remnant(n) = particle_new (code, mass, pvec) 
    else
       ps%beam_remnant(n) = particle_new (code, 0._default, pvec) 
    end if
    ps%n_beam_remnants = n
    ps%recoil_momentum(i) = ps%recoil_momentum(i) &
         & + particle_four_momentum (ps%beam_remnant(n))
  end subroutine set_beam_remnant_pt

  subroutine get_beam_momentum (p, E, i, mass, ok)
    real(kind=default), intent(out) :: p
    real(kind=default), intent(in) :: E
    integer, intent(in) :: i
    real(kind=default), intent(in), optional :: mass
    logical, intent(out), optional :: ok
    if (present (mass)) then
       if (E >= mass) then
          if (i==1) then
             p =  sqrt (E**2 - mass**2)
          else
             p = -sqrt (E**2 - mass**2)
          end if
          if (present (ok)) ok = .true.
       else
          p = 0
          if (present (ok)) ok = .false.
       end if
    else
       if (i==1) then
          p = E
       else
          p = -E
       end if
       if (present (ok)) ok = .true.
    end if
  end subroutine get_beam_momentum

  subroutine apply_FILE_events (ps, beam, r, r_pointer)
    type(parton_state), intent(inout) :: ps
    type(beam_data_t), dimension(2), intent(in) :: beam
    real(kind=default), dimension(:), intent(in) :: r
    integer, intent(inout) :: r_pointer
    integer :: i
    real(kind=default), dimension(2) :: x
!    complex(kind=default), dimension(-1:1,-1:1,2) :: rho_hel
    if (all (beam%FILE_events_on)) then
       call count_strfun (ps)
       call count_strfun (ps)
       r_pointer = r_pointer + 2
       if (beam(1)%FILE_events_unit == beam(2)%FILE_events_unit) then
          call read_FILE_beam_event (beam(1)%FILE_events_unit, x(1), x(2))
       else
          do i = 1, 2
             call read_FILE_beam_event (beam(i)%FILE_events_unit, x(i))
          end do
       end if
       where (beam%FILE_events_energies)
          ps%x = ps%x * x / (ps%sqrts / 2)
       elsewhere
          ps%x = ps%x * x
       end where
!       ps%rho_hel = rho_hel
       ps%code = beam%FILE_events_prt_out
    else
       do i = 1, 2
          if (beam(i)%FILE_events_on) then
             call count_strfun (ps)
             r_pointer = r_pointer + 1
             call read_FILE_beam_event (beam(i)%FILE_events_unit, x(i))
             ps%x(i) = ps%x(i) * x(i)
!             ps%rho_hel(:,:,i) = rho_hel(:,:,i)
             ps%code(i) = beam(i)%FILE_events_prt_out
          end if
       end do
    end if
  end subroutine apply_FILE_events

  subroutine apply_USER_spectrum (ps, beam, r, r_pointer)
    type(parton_state), intent(inout) :: ps
    type(beam_data_t), dimension(2), intent(in) :: beam
    real(kind=default), dimension(:), intent(in) :: r
    integer, intent(inout) :: r_pointer
    integer :: i
    real(kind=default) :: factor
    real(kind=default), dimension(2) :: x
    complex(kind=default), dimension(-2:2,-2:2,2) :: rho_hel
    if (all (beam%USER_spectrum_on)) then
       call count_strfun (ps)
       call count_strfun (ps)
       x = r(r_pointer:r_pointer+1)
       r_pointer = r_pointer + 2
       call strfun_USER_spectrum_double &
            & (factor, x, beam%USER_spectrum_prt_in, &
            &  beam%USER_spectrum_sqrts, ps%rho_hel, rho_hel, &
            &  beam%USER_spectrum_mode)
       do i = 1, 2
          if (beam(i)%USER_spectrum_remnant /= UNDEFINED) then
             call set_beam_remnant &
                  & (ps, beam(i)%USER_spectrum_remnant, x(i), i)
          end if
       end do
       ps%strfun_factor = ps%strfun_factor * factor
       ps%x = ps%x * x
       ps%rho_hel = rho_hel
       ps%code = beam%USER_spectrum_prt_out
    else
       do i = 1, 2
          if (beam(i)%USER_spectrum_on) then
             call count_strfun (ps)
             x(i) = r(r_pointer)
             r_pointer = r_pointer + 1
             call strfun_USER_spectrum_single &
                  & (factor, x(i), beam(i)%USER_spectrum_prt_in, &
                  &  beam(i)%USER_spectrum_sqrts, &
                  &  ps%rho_hel(:,:,i), rho_hel(:,:,i), &
                  &  beam(i)%USER_spectrum_mode)
             if (beam(i)%USER_spectrum_remnant /= UNDEFINED) then
                call set_beam_remnant &
                     & (ps, beam(i)%USER_spectrum_remnant, x(i), i)
             end if
             ps%strfun_factor = ps%strfun_factor * factor
             ps%x(i) = ps%x(i) * x(i)
             ps%rho_hel(:,:,i) = rho_hel(:,:,i)
             ps%code(i) = beam(i)%USER_spectrum_prt_out
          end if
       end do
    end if
  end subroutine apply_USER_spectrum

  subroutine apply_SCAN_spectrum (ps, beam, r, r_pointer)
    type(parton_state), intent(inout) :: ps
    type(beam_data_t), dimension(2), intent(in) :: beam
    real(kind=default), dimension(:), intent(in) :: r
    integer, intent(inout) :: r_pointer
    integer :: i
    real(kind=default) :: factor
    real(kind=default), dimension(2) :: x
    do i = 1, 2
       if (beam(i)%SCAN_spectrum_on) then
          call count_strfun (ps)
          x(i) = r(r_pointer)
          r_pointer = r_pointer + 1
          call strfun_SCAN_spectrum &
               & (factor, x(i), beam(i)%SCAN_spectrum_sqrts, &
               &  beam(i)%SCAN_spectrum_E0, beam(i)%SCAN_spectrum_E1)
          ps%strfun_factor = ps%strfun_factor * factor
          ps%code(i) = beam(i)%SCAN_spectrum_prt_out
       else
          x(i) = 1
       end if
    end do
    ps%x = ps%x * sqrt (product (x))
  end subroutine apply_SCAN_spectrum

  subroutine apply_CIRCE (ps, beam, r, r_pointer, mc_random_number)
    type(parton_state), intent(inout) :: ps
    type(beam_data_t), dimension(2), intent(in) :: beam
    real(kind=default), dimension(:), intent(in) :: r
    integer, intent(inout) :: r_pointer
    interface
       subroutine mc_random_number (r)
         use kinds, only: double !NODEP!
         real(kind=double) :: r
       end subroutine mc_random_number
    end interface
    real(kind=default), dimension(2) :: x
    logical :: bypass, map
    integer :: i
    if (all (beam%CIRCE_on)) then
       if (all (beam%CIRCE_generate)) then
          call count_strfun (ps)
          call count_strfun (ps)
          call generate_CIRCE (x, beam%CIRCE_prt_out, mc_random_number)
          r_pointer = r_pointer + 2
       else
          call count_strfun (ps)
          call count_strfun (ps, bypass)
          if (bypass) then
             x(1) = r(r_pointer)
             r_pointer = r_pointer + 1
             call strfun_bypass_x (x(2), x(1), ps)
             map = .false.
          else
             x = r(r_pointer:r_pointer+1)
             r_pointer = r_pointer + 2
             map = all (beam%CIRCE_map)
          end if
          if (map) then
             call strfun_CIRCE_map (ps%strfun_factor, x(1), x(2), &
                  &  beam(1)%CIRCE_beta, beam(1)%CIRCE_gamma, &
                  &  beam(2)%CIRCE_beta, beam(2)%CIRCE_gamma, &
                  &  beam(1)%CIRCE_prt_out, beam(2)%CIRCE_prt_out)
          else
             call strfun_CIRCE (ps%strfun_factor, x(1), x(2), &
                  &  beam(1)%CIRCE_prt_out, beam(2)%CIRCE_prt_out)
          end if
       end if
       do i=1, 2
          if (beam(i)%CIRCE_prt_out == PHOTON) then
             call set_beam_remnant (ps, beam(i)%CIRCE_prt_in, x(i), i)
             call average_beam_polarization &
                  & (ps, beam(i)%CIRCE_prt_in, beam(i)%CIRCE_prt_out, i)
          else
             call set_beam_remnant (ps, PHOTON, x(i), i)
          end if
       end do
       ps%x = ps%x * x
       ps%code = beam%CIRCE_prt_out
    end if
  end subroutine apply_CIRCE

  subroutine apply_CIRCE2 (ps, beam, r, r_pointer, mc_random_number)
    type(parton_state), intent(inout) :: ps
    type(beam_data_t), dimension(2), intent(in) :: beam
    real(kind=default), dimension(:), intent(in) :: r
    integer, intent(inout) :: r_pointer
    interface
       subroutine mc_random_number (r)
         use kinds, only: double !NODEP!
         real(kind=double) :: r
       end subroutine mc_random_number
    end interface
    real(kind=default) :: factor
    real(kind=default), dimension(2) :: x
    logical :: bypass
    integer, dimension(2) :: map
    integer :: i
    if (all (beam%CIRCE2_on)) then
       factor = 1
       if (all (beam%CIRCE2_generate)) then
          call count_strfun (ps)
          call count_strfun (ps)
          x = r(r_pointer:r_pointer+1)
          r_pointer = r_pointer + 2
          if (all (beam%CIRCE2_polarized)) then
             call generate_CIRCE2 (x, beam%CIRCE2_prt_out, &
                  &                mc_random_number, ps%rho_hel)
             ps%correlated_polarization = .false.
             ps%generic_polarization = .false.
          else
             call generate_CIRCE2 (x, beam%CIRCE2_prt_out, mc_random_number)
             do i = 1, 2
                call average_beam_polarization &
                     & (ps, beam(i)%CIRCE2_prt_in, beam(i)%CIRCE2_prt_out, i)
             end do
          end if
       else
          call count_strfun (ps)
          call count_strfun (ps, bypass)
          if (bypass) then
             x(1) = r(r_pointer)
             r_pointer = r_pointer + 1
             call strfun_bypass_x (x(2), x(1), ps)
             map = -1
          else
             x = r(r_pointer:r_pointer+1)
             r_pointer = r_pointer + 2
             map = beam%CIRCE2_map
          end if
          if (all (beam%CIRCE2_polarized)) then
             call strfun_CIRCE2_polarized &
                  & (factor, x(1), x(2), ps%rho_hel, beam%CIRCE2_prt_out, &
                  &  map, beam%CIRCE2_power)
             ps%correlated_polarization = .true.
             ps%generic_polarization = .false.
          else
             call strfun_CIRCE2_unpolarized &
                  & (factor, x(1), x(2), beam%CIRCE2_prt_out, &
                  &  map, beam%CIRCE2_power)
             do i = 1, 2
                call average_beam_polarization &
                     & (ps, beam(i)%CIRCE2_prt_in, beam(i)%CIRCE2_prt_out, i)
             end do
          end if
       end if
       ps%strfun_factor = ps%strfun_factor * factor
       ps%x = ps%x * x
       do i=1, 2
          if (beam(i)%CIRCE2_prt_out == PHOTON &
               & .and. beam(i)%CIRCE2_prt_in /= PHOTON) then
             call set_beam_remnant (ps, beam(i)%CIRCE2_prt_in, x(i), i)
          else if (beam(i)%CIRCE2_prt_out /= PHOTON) then
             call set_beam_remnant (ps, PHOTON, x(i), i)
          end if
       end do
       ps%code = beam%CIRCE2_prt_out
    end if
  end subroutine apply_CIRCE2

  subroutine apply_ISR (ps, beam, r, r_pointer)
    type(parton_state), intent(inout) :: ps
    type(beam_data_t), dimension(2), intent(in) :: beam
    real(kind=default), dimension(:), intent(in) :: r
    integer, intent(inout) :: r_pointer
    logical :: bypass, map
    integer :: i
    real(kind=default), dimension(2) :: x
    real(kind=default) :: xbar, y, m2, q2, tmp, st2, st, ct, phi
    x = 1
    do i=1, 2
       if (beam(i)%ISR_on) then
          call count_strfun (ps, bypass)
          if (bypass) then
             call strfun_bypass_x (x(i), x(1), ps)
             map = .false.
          else
             x(i) = r(r_pointer)
             r_pointer = r_pointer + 1
             map = beam(i)%ISR_map
          end if
          call strfun_ISR &
               & (ps%strfun_factor, x(i), beam(i)%ISR_power, &
               &  beam(i)%ISR_LLA_order, map)
          if (ps%beam_recoil) then
             y = r(r_pointer)
             r_pointer = r_pointer + 1
             xbar = 1 - x(i)
             if (xbar > 0) then
                m2 = beam(i)%ISR_m_in**2
                q2 = m2 * (1 - xbar * exp (y * beam(i)%ISR_log))
                tmp = (2 / xbar) * (q2 - x(i)*m2) / beam(i)%ISR_Q_max**2
             else
                tmp = 0
             end if
             if (tmp >= 0) then
                st = 0
                ct = 1
             else
                st2 = -tmp * (2 + tmp)
                if (st2 < 0) then
                   st = 0
                   ct = -1
                else if (st2 > 1) then
                   st = 1
                   ct = 0
                else
                   st = sqrt (st2)
                   ct = 1 + tmp
                end if
             end if
             y = r(r_pointer)
             r_pointer = r_pointer + 1
             phi = twopi * y
             call set_beam_remnant (ps, PHOTON, x(i), i, ct, st, phi)
          else
             call set_beam_remnant (ps, PHOTON, x(i), i)
          end if
       end if
    end do
    ps%x = ps%x * x
  end subroutine apply_ISR

  subroutine apply_EPA (ps, beam, r, r_pointer)
    type(parton_state), intent(inout) :: ps
    type(beam_data_t), dimension(2), intent(in) :: beam
    real(kind=default), dimension(:), intent(in) :: r
    integer, intent(inout) :: r_pointer
    real(kind=default), dimension(2) :: x
    real(kind=default) :: xbar, y, m2, q2, tmp, st2, st, ct, phi
    logical, dimension(2) :: bypass
    logical :: ok
    integer :: i
    x = 1
    do i=1, 2
       if (beam(i)%EPA_on) then
          call count_strfun (ps, bypass(i))
          if (bypass(i)) then
             call strfun_bypass_x (x(i), x(1), ps)
          else
             x(i) = r(r_pointer)
             r_pointer = r_pointer + 1
          end if
       end if
    end do
    if (all (beam%EPA_on) .and. .not. any (bypass)) &
         & call map_unit_square (x, ps%strfun_factor)
    do i=1, 2
       if (beam(i)%EPA_on) then
          call strfun_EPA (x(i), ps%strfun_factor, beam(i)%EPA_map, &
               &           bypass(i), &
               &           beam(i)%EPA_x0, beam(i)%EPA_x1, &
               &           beam(i)%EPA_c0, beam(i)%EPA_c1, &
               &           beam(i)%EPA_msq, &
               &           beam(i)%EPA_q_max**2, beam(i)%EPA_s, &
               &           ok)
          if (ps%beam_recoil) then
             y = r(r_pointer)
             r_pointer = r_pointer + 1
             xbar = 1 - x(i)
             if (xbar > 0) then
                m2 = beam(i)%EPA_m_in**2
                q2 = m2 * (1 - xbar * exp (y * beam(i)%EPA_log))
                tmp = (2 / xbar) * (q2 - x(i)*m2) / beam(i)%EPA_Q_max**2
             else
                tmp = 0
             end if
             if (tmp >= 0) then
                st = 0
                ct = 1
             else
                st2 = -tmp * (2 + tmp)
                if (st2 < 0) then
                   st = 0
                   ct = -1
                else if (st2 > 1) then
                   st = 1
                   ct = 0
                else
                   st = sqrt (st2)
                   ct = 1 + tmp
                end if
             end if
             y = r(r_pointer)
             r_pointer = r_pointer + 1
             phi = twopi * y
             call set_beam_remnant (ps, beam(i)%EPA_prt_in, x(i), i, ct, st, phi)
          else
             call set_beam_remnant (ps, beam(i)%EPA_prt_in, x(i), i)
          end if
          ps%x(i) = ps%x(i) * x(i)
          call average_beam_polarization &
               & (ps, beam(i)%EPA_prt_in, beam(i)%EPA_prt_out, i)
          if (.not.ok)  ps%strfun_factor = 0
       end if
    end do
  end subroutine apply_EPA

  subroutine apply_PDF (ps, beam, r, r_pointer)
    type(parton_state), intent(inout) :: ps
    type(beam_data_t), dimension(2), intent(in) :: beam
    real(kind=default), dimension(:), intent(in) :: r
    integer, intent(inout) :: r_pointer
    real(kind=default), dimension(2) :: x, scale
    logical, dimension(2) :: bypass
    integer :: i
    x = 1
    do i=1, 2
       if (beam(i)%PDF_on) then
          call count_strfun (ps, bypass(i))
          if (bypass(i)) then
             call strfun_bypass_x (x(i), x(1), ps)
          else
             x(i) = r(r_pointer)
             r_pointer = r_pointer + 1
          end if
       end if
    end do
    if (all (beam%PDF_on) .and..not.any(bypass)) &
         & call map_unit_square (x, ps%strfun_factor, maxval (beam%PDF_power))
    do i=1, 2
       if (beam(i)%PDF_on) then
          if (beam(i)%PDF_running_scale) then
             scale(i) = ps%sqrts * sqrt (x(1)*x(2))
          else
             scale(i) = beam(i)%PDF_scale
          end if
          if (beam(1)%PDF_prt_in /= beam(2)%PDF_prt_in) then
             call pdflib_init &
                  & (beam(i)%PDF_prt_in, beam(i)%PDF_ngroup, &
                  &  beam(i)%PDF_nset, beam(i)%PDF_nfl, beam(i)%PDF_lo, &
                  &  beam(i)%PDF_QCDL4, beam(i)%PDF_mtop)
          end if
          call pdflib_strfun_array &    
               & (x(i), scale(i), ps%PDF_rho_flv (:,i))
          ps%code(i) = beam(i)%PDF_prt_out
          call set_beam_remnant (ps, HADRON_REMNANT, x(i), i)
          call average_beam_polarization &
               & (ps, beam(i)%PDF_prt_in, beam(i)%PDF_prt_out, i)
          ps%x(i) = ps%x(i) * x(i)
       end if
    end do
  end subroutine apply_PDF

  subroutine apply_LHAPDF (ps, beam, r, r_pointer)
    type(parton_state), intent(inout) :: ps
    type(beam_data_t), dimension(2), intent(in) :: beam
    real(kind=default), dimension(:), intent(in) :: r
    integer, intent(inout) :: r_pointer
    real(kind=default), dimension(2) :: x, scale
    logical, dimension(2) :: bypass
    integer :: i
    x = 1
    do i=1, 2
       if (beam(i)%LHAPDF_on) then
          call count_strfun (ps, bypass(i))
          if (bypass(i)) then
             call strfun_bypass_x (x(i), x(1), ps)
          else
             x(i) = r(r_pointer)
             r_pointer = r_pointer + 1
          end if
       end if
    end do
    if (all (beam%LHAPDF_on) .and..not.any(bypass)) &
         & call map_unit_square (x, ps%strfun_factor, maxval (beam%PDF_power))
    do i=1, 2
       if (beam(i)%LHAPDF_on) then
          if (beam(i)%PDF_running_scale) then
             scale(i) = ps%sqrts * sqrt (x(1)*x(2))
          else
             scale(i) = beam(i)%PDF_scale
          end if
          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          !!! JR: This code is deprecated 
          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          !!! if (beam(1)%PDF_prt_in /= beam(2)%PDF_prt_in) then
          !!!    call lhapdflib_init (beam(i)%PDF_prt_in, beam(i)%LHAPDF_file, &
          !!!         &         beam(i)%LHAPDF_set, first=.true.)   
          !!! end if
          call lhapdflib_strfun_array (beam(i)%PDF_prt_in, &
                  & beam(i)%LHAPDF_set, x(i) , scale(i), ps%PDF_rho_flv (:,i))
          ps%code(i) = beam(i)%PDF_prt_out
          call set_beam_remnant (ps, HADRON_REMNANT, x(i), i)
          call average_beam_polarization &
               & (ps, beam(i)%PDF_prt_in, beam(i)%PDF_prt_out, i)
          ps%x(i) = ps%x(i) * x(i)
       end if
    end do
  end subroutine apply_LHAPDF

  subroutine apply_EWA (ps, beam, r, r_pointer)
    type(parton_state), intent(inout) :: ps
    type(beam_data_t), dimension(2), intent(in) :: beam
    real(kind=default), dimension(:), intent(in) :: r
    integer, intent(inout) :: r_pointer
    real(kind=default), dimension(2) :: x
    real(kind=default) :: xbar, y, m2, q2, tmp, st2, st, ct, phi
    real(kind=default) :: fp, fm, fL
    logical, dimension(2) :: bypass
    logical :: ok
    integer :: i
    x = 1
    do i = 1, 2
       if (beam(i)%EWA_on) then
          call count_strfun (ps, bypass(i))
          if (bypass(i)) then
             call strfun_bypass_x (x(i), x(1), ps)
          else
             x(i) = r(r_pointer)
             r_pointer = r_pointer + 1
          end if
       end if
    end do
    if (all (beam%EWA_on) .and. .not. any (bypass)) &
         & call map_unit_square (x, ps%strfun_factor)
    do i=1, 2
       if (beam(i)%EWA_on) then
          if (.not. ps%beam_recoil) then
             call strfun_EWA &
               &          (x(i), ps%strfun_factor, fp, fm, fL, &
               &           beam(i)%EWA_map, &
               &           bypass(i), &
               &           beam(i)%EWA_x0, beam(i)%EWA_x1, &
               &           beam(i)%EWA_pt_max**2, &
               &           beam(i)%EWA_cv, beam(i)%EWA_ca, beam(i)%EWA_coeff, &
               &           beam(i)%EWA_mass**2, ps%sqrts, &
               &           ok)
             call set_beam_remnant (ps, beam(i)%EWA_remnant, x(i), i)
          else
             call msg_fatal ("pT dependence of EWA not implemented yet")
!              y = r(r_pointer)
!              r_pointer = r_pointer + 1
!              xbar = 1 - x(i)
!              if (xbar > 0) then
!                 m2 = beam(i)%EWA_m_in**2
!                 q2 = m2 * (1 - xbar * exp (y * beam(i)%EWA_log))
!                 tmp = (2 / xbar) * (q2 - x(i)*m2) / beam(i)%EWA_pT_max**2
!              else
!                 tmp = 0
!              end if
!              if (tmp >= 0) then
!                 st = 0
!                 ct = 1
!              else
!                 st2 = -tmp * (2 + tmp)
!                 if (st2 < 0) then
!                    st = 0
!                    ct = -1
!                 else if (st2 > 1) then
!                    st = 1
!                    ct = 0
!                 else
!                    st = sqrt (st2)
!                    ct = 1 + tmp
!                 end if
!              end if
!              y = r(r_pointer)
!              r_pointer = r_pointer + 1
!              phi = twopi * y
!              call set_beam_remnant (ps, beam(i)%EWA_prt_in, x(i), i, ct, st, phi)
          end if
          ps%x(i) = ps%x(i) * x(i)
          if (ps%polarized_me) then
             ps%rho_hel(:,:,i) = 0
             ps%rho_hel(-1,-1,i) = fm
             ps%rho_hel( 0, 0,i) = fL
             ps%rho_hel( 1, 1,i) = fp
          else
             ps%rho_hel(:,:,i) = &
                  unpolarized_density_matrix (beam(i)%EWA_prt_out)
          end if
          if (.not.ok)  ps%strfun_factor = 0
       end if
    end do
  end subroutine apply_EWA

  subroutine apply_USER_strfun (ps, beam, r, r_pointer)
    type(parton_state), intent(inout) :: ps
    type(beam_data_t), dimension(2), intent(in) :: beam
    real(kind=default), dimension(:), intent(in) :: r
    integer, intent(inout) :: r_pointer
    integer :: i
    logical :: bypass, map
    real(kind=default) :: factor
    real(kind=default), dimension(2) :: x
    complex(kind=default), dimension(-2:2,-2:2,2) :: rho_hel
    if (all (beam%USER_strfun_on)) then
       call count_strfun (ps)
       call count_strfun (ps, bypass)
       if (bypass) then
          x(1) = r(r_pointer)
          r_pointer = r_pointer + 1
          call strfun_bypass_x (x(2), x(1), ps)
          map = .false.
       else
          x = r(r_pointer:r_pointer+1)
          r_pointer = r_pointer + 2
          map = .true.
       end if
       call strfun_USER_strfun_double &
            & (factor, x, beam%USER_strfun_prt_in, &
            &  beam%USER_strfun_sqrts, ps%rho_hel, rho_hel, &
            &  beam%USER_strfun_mode, map)
       do i = 1, 2
          if (beam(i)%USER_strfun_remnant /= UNDEFINED) then
             call set_beam_remnant &
                  & (ps, beam(i)%USER_strfun_remnant, x(i), i)
          end if
       end do
       ps%strfun_factor = ps%strfun_factor * factor
       ps%x = ps%x * x
       ps%rho_hel = rho_hel
       ps%code = beam%USER_strfun_prt_out
    else
       x = 1
       do i = 1, 2
          if (beam(i)%USER_strfun_on) then
             call count_strfun (ps, bypass)
             if (bypass) then
                call strfun_bypass_x (x(i), x(1), ps)
                map = .false.
             else
                x(i) = r(r_pointer)
                r_pointer = r_pointer + 1
                map = .true.
             end if
             call strfun_USER_strfun_single &
                  & (factor, x(i), beam(i)%USER_strfun_prt_in, &
                  &  beam(i)%USER_strfun_sqrts, &
                  &  ps%rho_hel(:,:,i), rho_hel(:,:,i), &
                  &  beam(i)%USER_strfun_mode, map)
             if (beam(i)%USER_strfun_remnant /= UNDEFINED) then
                call set_beam_remnant &
                     & (ps, beam(i)%USER_strfun_remnant, x(i), i)
             end if
             ps%strfun_factor = ps%strfun_factor * factor
             ps%x(i) = ps%x(i) * x(i)
             ps%rho_hel(:,:,i) = rho_hel(:,:,i)
             ps%code(i) = beam(i)%USER_strfun_prt_out
          end if
       end do
    end if
  end subroutine apply_USER_strfun

  subroutine map_unit_square (x, factor, power)
    real(kind=default), dimension(2), intent(inout) :: x
    real(kind=default), intent(inout) :: factor
    real(kind=default), intent(in), optional :: power
    real(kind=default) :: xx, yy
    xx = x(1)
    yy = x(2)
    if (present(power)) then
       if (x(1) > 0 .and. power > 1) then
          xx = x(1)**power
          factor = factor * power * xx / x(1)
       end if
    end if
    if (xx /= 0) then
       x(1) = xx ** yy
       x(2) = xx / x(1)
       factor = factor * abs (log (xx))
    else
       x = 0
    end if
  end subroutine map_unit_square

  subroutine compose_density (ps, beam, hel_state_in, flv_state_in)
    type(parton_state), intent(inout) :: ps
    type(beam_data_t), dimension(:), intent(in) :: beam
    integer, dimension(:,:), intent(in) :: hel_state_in, flv_state_in
    integer :: i_hel, j_hel, i_flv, i
    integer, dimension(ps%n_in) :: hel1, hel2, flv
    real(kind=default) :: rval, factor
    complex(kind=default) :: cval
    do i_flv = 1, ps%n_flv
       flv = flv_state_in (:,i_flv)
       factor = ps%strfun_factor
       if (ps%n_in == 2) then
          do i=1, 2
             if ((beam(i)%PDF_on).or.(beam(i)%LHAPDF_on)) then
                select case (flv(i))
                case (GLUON, GLUON2)
                   flv(i) = 0
                end select
                factor = factor * ps%PDF_rho_flv(flv(i),i)
             end if
          end do
       end if
       if (ps%polarized_me) then
          if (ps%correlated_polarization) then
             if (ps%n_in==1) &
                  & call msg_bug (" Correlated polarization for decay")
             do i_hel = 1, ps%n_hel
                hel1 = hel_state_in (:,i_hel)
                rval = factor * &
                     & ps%rho_hel(hel1(1),hel1(2),1)
                call set (ps%rho, 1, i_hel, i_flv, rval)
             end do  
          else if (ps%generic_polarization) then
             do i_hel = 1, ps%n_hel
                hel1 = hel_state_in (:,i_hel)
                do j_hel = 1, ps%n_hel
                   hel2 = hel_state_in (:, j_hel)
                   cval = factor * ps%rho_hel(hel1(1),hel2(1),1)
                   if (ps%n_in == 2) then
                      cval = cval * ps%rho_hel(hel1(2),hel2(2),2)
                   end if
                   call set (ps%rho, 0, i_hel, i_flv, j_hel, i_flv, cval)
                end do
             end do
          else
             do i_hel = 1, ps%n_hel
                hel1 = hel_state_in (:,i_hel)
                rval = factor * ps%rho_hel(hel1(1),hel1(1),1)
                if (ps%n_in == 2) then
                   rval = rval * ps%rho_hel(hel1(2),hel1(2),2)
                end if
                call set (ps%rho, 1, i_hel, i_flv, rval)
             end do
          end if
       else
          rval = factor
          call set (ps%rho, 1, 1, i_flv, rval)
       end if
    end do
    if (ps%generic_polarization)  call state_density_matrix_to_vector (ps%rho)
  end subroutine compose_density

  subroutine find_energy_scale (ps, beam)
    type(parton_state), intent(inout) :: ps
    type(beam_data_t), dimension(:), intent(in) :: beam
    real(kind=default), dimension(size(beam)) :: energy_scale
    integer :: i
    do i = 1, size(beam)
       if (((beam(i)%PDF_on).or.(beam(i)%LHAPDF_on)) & 
         .and. .not. beam(i)%PDF_running_scale) then
          energy_scale(i) = beam(i)%PDF_scale
       else
          energy_scale(i) = ps%sqrts * sqrt (product (ps%x))
       end if
    end do
    ps%energy_scale = minval (energy_scale)
  end subroutine find_energy_scale


end module partons

