! STRUCTP dummy routine, to be removed when PDFLIB is to be linked.
subroutine structp (xx,qq2,p2,ip2,upv,dnv,usea,dsea,str,chm,bot,top,glu)
  implicit double precision(a-h, o-z)
  implicit integer(i-n)
  stop " Can't happen: STRUCTP dummy routine called"
end subroutine structp
