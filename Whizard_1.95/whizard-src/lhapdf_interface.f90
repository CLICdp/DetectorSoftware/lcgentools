! WHIZARD 1.95 Feb 25 2010
! 
! (C) 1999-2008 by 
!     Wolfgang Kilian <wolfgang.kilian@desy.de>
!     Thorsten Ohl <ohl@physik.uni-wuerzburg.de>
!     Juergen Reuter <juergen.reuter@physik.uni-freiburg.de>
!
! WHIZARD is free software; you can redistribute it and/or modify it
! under the terms of the GNU General Public License as published by 
! the Free Software Foundation; either version 2, or (at your option)
! any later version.
!
! WHIZARD is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! This file has been stripped of most comments.  For documentation, refer
! to the source 'whizard.nw'

module lhapdf_interface

  use kinds, only: default !NODEP!
  use kinds, only: double !NODEP!
  use limits, only: PDFSET_CHAR_LEN
  use diagnostics, only: msg_fatal

  implicit none
  private

  public :: lhapdflib_init, lhapdflib_strfun_array
 
  integer, parameter :: TQUARK = 6, GLUON1 = 9, GLUON2 = 21
  integer, parameter :: PROTON = 2212, ANTIPROTON = -PROTON, PHOTON = 22, &
        PION = 211, ANTIPION = -PION

  logical, save :: pdf_invert = .false.
  logical, save :: pdf_user = .false.

contains

  subroutine lhapdflib_init &
       & (pdg_code_in, file, set, first)
    integer, intent(in) :: pdg_code_in, set
    character(len=PDFSET_CHAR_LEN) :: file
    logical, intent(in), optional :: first
    real(kind=double), parameter :: x_dummy = 0.5_double
    real(kind=double), parameter :: scale_dummy = 100._double
    real(kind=double), dimension(-TQUARK:TQUARK) :: dxpdf_dummy
    select case (pdg_code_in)
    case (PROTON,ANTIPROTON,PHOTON,PION,ANTIPION)
    case default
       call msg_fatal &
            & (" LHAPDF only works for p, pbar, (anti-)pion, photon")
    end select
    call InitPDFsetByName (file)        
    call InitPDF (set)
    if (present(first)) then
       if (first) call & 
            lha_pftopdg (pdg_code_in, x_dummy, scale_dummy, dxpdf_dummy)
    end if
  contains
    subroutine lha_pftopdg (pdg_code_in, xdum, scaldum, dxpdfdum)
      integer, intent(in) :: pdg_code_in
      real(kind=double), intent(in) :: xdum 
      real(kind=double), intent(in) :: scaldum 
      real(kind=double), dimension(-TQUARK:TQUARK) :: dxpdfdum, f
      select case (pdg_code_in)
      case (PROTON,ANTIPROTON,PION,ANTIPION)
         call evolvePDF (xdum, scaldum, f)         
      case (PHOTON)
         call evolvePDFp (xdum, scaldum, 0.0_double, 0, f)
      case default 
         call msg_fatal &
              & (" No structure function available. ")
      end select
      dxpdfdum(0) = f(0)
      dxpdfdum(-2:2) = f(-2:2)
      dxpdfdum(3:6) = f(3:6)
      dxpdfdum(-3:-6) = f(-3:-6)
    end subroutine lha_pftopdg
  end subroutine lhapdflib_init

  subroutine lhapdflib_strfun_array (pdg_code_in, set, x, scale, rho)
    integer, intent(in) :: pdg_code_in, set     
    real(kind=default), intent(in) :: x, scale
    real(kind=default) :: xdum, scaledum
    real(kind=default) :: xmin, xmax, q2min, q2max
    real(kind=default), dimension(-TQUARK:TQUARK), intent(out) :: rho
    real(kind=double), dimension(-TQUARK:TQUARK) :: dxpdf
    select case (pdg_code_in)
    case (PROTON)
       pdf_invert = .false.
    case (ANTIPROTON)
       pdf_invert = .true.
    case (PHOTON)
       pdf_invert = .false.
    case (PION)
       pdf_invert = .false.
    case (ANTIPION) 
       pdf_invert = .true.
    case default
       call msg_fatal &
            & (" LHAPDF only works for p, pbar, (anti-)pion, photon")
    end select
    call GetMinMax (set,xmin,xmax,q2min,q2max)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!! Calling the PDFs at their extremal points when
    !!! calling for values outside their range of validity
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    if (scale**2 .ge. q2max) then
       scaledum = sqrt(q2max)
    elseif (scale**2 .le. q2min) then
       scaledum = sqrt(q2min)
    else
       scaledum = scale
    end if
    if (x .ge. xmax) then
       xdum = xmax
    elseif (x .le. xmin) then
       xdum = xmin
    else
       xdum = x
    end if
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    select case (abs (pdg_code_in))
    case (PROTON, PION)
       call evolvePDF (xdum, scaledum, dxpdf)         
    case (PHOTON)
       call evolvePDFp (xdum, scaledum, 0.0_double, 0, dxpdf)
    end select
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!! First version: just nullify the PDFs when getting
    !!! outside their allowed ranges
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!! if (x .ge. xmin .and. x .le. xmax .and. scale**2 .ge. &
    !!!      & q2min .and. scale**2 .le. q2max) then
    !!!    select case (abs (pdg_code_in))
    !!!    case (PROTON, PION)
    !!!       call evolvePDF (x, scale, dxpdf)     
    !!!    case (PHOTON)
    !!!       call evolvePDFp (x, scale, 0.0_double, 0, dxpdf)
    !!!    end select
    !!! else 
    !!!    dxpdf = 0
    !!! end if
    !!! Here we catch negatively running sea quark densities for some of 
    !!! the .LHpdf files in LHAPDF
    dxpdf = max (dxpdf,0.0_default)
    if (.not.pdf_invert) then
       rho = dxpdf / x
    else
       rho = dxpdf(TQUARK:-TQUARK:-1) / x
    end if
  end subroutine lhapdflib_strfun_array

end module lhapdf_interface
