! WHIZARD 1.95 Feb 25 2010
! 
! (C) 1999-2008 by 
!     Wolfgang Kilian <wolfgang.kilian@desy.de>
!     Thorsten Ohl <ohl@physik.uni-wuerzburg.de>
!     Juergen Reuter <juergen.reuter@physik.uni-freiburg.de>
!
! WHIZARD is free software; you can redistribute it and/or modify it
! under the terms of the GNU General Public License as published by 
! the Free Software Foundation; either version 2, or (at your option)
! any later version.
!
! WHIZARD is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! This file has been stripped of most comments.  For documentation, refer
! to the source 'whizard.nw'

module decay_forests

  use kinds, only: default !NODEP!
  use limits, only: MAX_EXTERNAL, MAX_CHANNELS
  use limits, only: BUFFER_SIZE, DEFAULT_FILENAME, FILENAME_LEN
  use limits, only: TC, PRT_NAME_LEN
  use mpi90, only: mpi90_broadcast !NODEP!
  use files, only: concat
  use particles
  use mappings
  use cuts, only: default_cuts
  use file_utils !NODEP!
  use parser
  use cascades, only: make_phase_space_file, phase_space_switches
  use permutations, only: permutation_list, permutation
  use permutations, only: create, destroy, len, size, write
  use permutations, only: make_permutations, find_permutation
  use permutations, only: extract, permutation_ok
  use diagnostics, only: msg_buffer
  use diagnostics, only: msg_bug, msg_fatal, msg_error, msg_message
  use decay_trees
  use parameters, only: parameter_set !NODEP!

  implicit none
  private

  type, public :: decay_forest
     integer :: n_externals, n_internals, n_in, n_out
     integer :: n_channels_max, n_channels, n_groves
     integer :: n_dim_integration
     integer :: n_dim_momenta, n_dim_masses, n_dim_angles
     integer :: n_dim_extra, n_dim_strfun, n_dim_recoil, n_dim_dummy
     integer, dimension(2) :: generated_externally
     integer(kind=TC) :: n_branches_tot, n_branches_decay
     real(kind=default), dimension(:), pointer :: mass_sum
     type(decay_tree), dimension(:), pointer :: tree
     type(mapping), dimension(:,:), pointer :: map
     type(decay_grove), dimension(:), pointer :: grove
     type(permutation_list) :: perm_list
     type(equivalences), pointer :: eq
     integer :: n_eq
  end type decay_forest
  type, public :: decay_grove
     integer :: first, last
  end type decay_grove
  type, public :: equivalences
     integer :: left, right
     integer :: perm_index
     type(permutation) :: msq_perm, angle_perm
     logical, dimension(:), pointer :: angle_sig
     type(equivalences), pointer :: next
  end type equivalences

  public :: create, destroy
  public :: write
  public :: forest_fill_tmp_file, forest_append_tmp_file
  public :: forest_count
  public :: forest_read
  public :: add
  public :: map
  public :: mpi90_broadcast
  public :: forest_set_strfun
  public :: forest_prepare
  public :: forest_set_equivalences

  interface create
     module procedure forest_create
  end interface
  interface destroy
     module procedure forest_destroy
  end interface
  interface create
     module procedure equivalence_create
  end interface
  interface destroy
     module procedure equivalences_destroy
  end interface
  interface write
     module procedure forest_write_unit
  end interface
  interface write
     module procedure equivalences_write_unit
  end interface
  interface add
     module procedure forest_add_tree
  end interface
  interface map
     module procedure forest_set_mapping
  end interface
  interface mpi90_broadcast
     module procedure forest_broadcast
  end interface

contains

  subroutine forest_create (f, n_in, n_out, n_channels_max)
    type(decay_forest), intent(out) :: f
    integer, intent(in) :: n_in, n_out, n_channels_max
    !allocate(f)
!     if (n_out < 2) call msg_fatal &
!          & (" Single particle production processes not supported yet")
    f%n_in = n_in
    f%n_out = n_out
    f%n_externals = n_in + n_out
    f%n_channels_max  = n_channels_max
    f%n_channels  = 0
    f%n_groves = 0
    f%n_internals = f%n_externals-3
    f%n_branches_tot = 2**f%n_externals-1
    f%n_branches_decay = 2**(n_out)-1
    if (n_out >= 2) then
       f%n_dim_masses = n_out - 2
       f%n_dim_angles = 2*n_out - 2
    else if (n_out == 1) then
       f%n_dim_masses = 0
       f%n_dim_angles = 0
    else
       call msg_bug (" Number of outgoing particles <= 0")
    end if
    f%n_dim_strfun = 0
    f%n_dim_dummy  = 0
    f%n_dim_recoil = 0
    f%n_dim_momenta     = f%n_dim_masses + f%n_dim_angles
    f%n_dim_extra       = f%n_dim_strfun + f%n_dim_recoil + f%n_dim_dummy
    f%n_dim_integration = f%n_dim_momenta + f%n_dim_extra
    f%generated_externally = 0
    allocate(f%mass_sum(f%n_branches_tot))
    allocate(f%tree(f%n_channels_max))
    allocate(f%map(f%n_branches_tot, f%n_channels_max))
    f%map = mapping_null
    allocate(f%grove(f%n_channels_max))
    call create (f%perm_list, 0, n_out)
    nullify (f%eq)
  end subroutine forest_create

  subroutine forest_destroy (f)
    type(decay_forest), intent(inout) :: f
    integer :: ch
    call destroy (f%perm_list)
    deallocate (f%grove)
    deallocate (f%map)
    do ch=1, f%n_channels
       call destroy (f%tree(ch))
    end do
    deallocate (f%tree)
    deallocate (f%mass_sum)
    if (f%n_channels > 0)  call destroy (f%eq)
    !deallocate (f)
  end subroutine forest_destroy

  subroutine equivalence_create (eq, left, right, perm_index)
    type(equivalences), pointer :: eq, new
    integer, intent(in) :: left, right, perm_index
    allocate (new)
    new%left = left
    new%right = right
    new%perm_index = perm_index
    call create (new%msq_perm, 0)
    call create (new%angle_perm, 0)
    allocate (new%angle_sig(0))
    nullify (new%next)
    eq => new
  end subroutine equivalence_create

  recursive subroutine equivalences_destroy (eq)
    type(equivalences), pointer :: eq
    if (associated(eq%next))  call equivalences_destroy (eq%next)
    call destroy (eq%msq_perm)
    call destroy (eq%angle_perm)
    deallocate (eq%angle_sig)
    deallocate (eq)
  end subroutine equivalences_destroy

  subroutine forest_write_unit(u,f)
    integer, intent(in) :: u
    type(decay_forest), intent(in) :: f
    integer(kind=TC) :: k
    integer :: ch, igrove
    write(u,'(1x,A,1x,I3)') 'Overall dimension:    ', f%n_dim_integration
    write(u,'(1x,A,1x,I3)') 'Phase space dimension:', f%n_dim_momenta
    write(u,'(1x,A)') 'Phase space channels:'
    igrove = 1
    do ch=1, f%n_channels
       if (igrove <= f%n_groves) then
          if (ch == f%grove(igrove)%first) &
               & write(u, '(1x,A,I3)') 'Decay grove #', igrove
       end if
       write(u,'(1x,A,I3)') 'Decay tree #', ch
       call write(u,f%tree(ch))
       do k=1, f%n_branches_tot
          call write(u, f%map(k,ch), k)
       end do
       if (igrove <= f%n_groves) then
          if (ch == f%grove(igrove)%last) then
             write(u, '(1x,A)') 'End of grove'
             igrove = igrove+1
          end if
       end if
       write(u,*)
    end do
    call write (u, f%eq, f%perm_list)
  end subroutine forest_write_unit

  recursive subroutine equivalences_write_unit (u, eq, pl)
    integer, intent(in) :: u
    type(equivalences), pointer :: eq
    type(permutation_list), intent(in) :: pl
    type(permutation) :: perm
    integer :: i
    if (associated(eq)) then
       write (u, "(1x,A,1x,I5,1x,I5,5x,A)", advance="no") &
            "Equivalence:", eq%left, eq%right, "Final state permutation:"
       call extract (perm, pl, eq%perm_index)
       call write (u, perm)
       call destroy (perm)
       write (u, "(1x,12x,1x,A,1x)", advance="no") "       msq permutation:  "
       call write (u, eq%msq_perm)
       write (u, "(1x,12x,1x,A,1x)", advance="no") "       angle permutation:"
       call write (u, eq%angle_perm)
       write (u, "(1x,12x,1x,26x)", advance="no")
       do i=1, size(eq%angle_sig)
          if (eq%angle_sig(i)) then
             write (u, "(1x,A)", advance="no") "+"
          else
             write (u, "(1x,A)", advance="no") "-"
          end if
       end do
       write (u, *)
       call equivalences_write_unit (u, eq%next, pl)
    end if
  end subroutine equivalences_write_unit

  subroutine forest_fill_tmp_file (prefix, filename, unit)
    character(len=*), intent(in) :: prefix, filename
    integer, intent(in) :: unit
    call copy_file (trim (concat (prefix, filename, "phs")), unit)
    if (trim(filename) /= trim(DEFAULT_FILENAME))  &
         & call copy_file (concat (prefix, DEFAULT_FILENAME, "phs"), unit)
    call copy_file (trim (concat (prefix, filename, "phx")), unit)
  end subroutine forest_fill_tmp_file

  subroutine forest_append_tmp_file (unit, prefix, filename)
    integer, intent(in) :: unit
    character(len=*), intent(in) :: prefix, filename
    character(len=FILENAME_LEN) :: file
    logical :: exist, ok
    integer :: u_out
    file = concat (prefix, filename, "phx")
    u_out = free_unit ()
    inquire (file = trim(file), exist=exist)
    if (exist) then
       open (u_out, file = trim(file), action = "write", &
            & status = "old", position = "append")
    else
       open (u_out, file = trim(file), action="write")
    end if
    rewind (unit)
    call copy_unit (unit, u_out, ok)
    close (u_out)
    if (ok)  call msg_message &
         & (" Wrote phase space configurations to file "//trim(file))
  end subroutine forest_append_tmp_file

  subroutine copy_file (file, unit)
    character(len=*), intent(in) :: file
    integer, intent(in) :: unit
    integer :: u_in
    logical :: exist
    inquire (file=file, exist=exist)
    if (exist) then
       call msg_message &
            & (" Reading phase space configurations from file "//file)
       u_in = free_unit ()
       open (u_in, file=file, action="read")
       call copy_unit (u_in, unit)
       close (u_in)
    end if
  end subroutine copy_file

  subroutine copy_unit (u_in, u_out, ok)
    integer, intent(in) :: u_in, u_out
    logical, intent(out), optional :: ok
    character(len=BUFFER_SIZE) :: buffer
    integer :: iostat
    if (present(ok))  ok = .false.
    READLINE: do
       read (unit=u_in, fmt="(A)", iostat=iostat) buffer
       if (iostat /= 0)  exit READLINE
       write (unit=u_out, fmt="(A)")  trim(buffer)
       if (present(ok))  ok = .true.
    end do READLINE
  end subroutine copy_unit

  subroutine forest_count &
       & (unit_old, unit_new, prefix, read_model_file, &
       &  write_phase_space_channels_file, process_id, code, n_in, n_out, &
       &  par, sqrts, &
       &  generate, phs, n_channels, new_phase_space)
    integer, intent(in) :: unit_old, unit_new
    character(len=*), intent(in) :: prefix, read_model_file
    character(len=*), intent(in) :: write_phase_space_channels_file, process_id
    integer, dimension(:,:), intent(in) :: code
    integer, intent(in) :: n_in, n_out
    type(parameter_set), intent(in) :: par
    real(kind=default), intent(in) :: sqrts
    logical, intent(in) :: generate
    type(phase_space_switches), intent(in) :: phs
    integer, intent(out) :: n_channels
    logical, intent(out) :: new_phase_space
    rewind (unit_old)
    call forest_count_unit (unit_old, process_id, n_channels)
    if (n_channels == 0) then
       if (generate) then
          call make_phase_space_file &
               & (unit_new, prefix, read_model_file, &
               &  write_phase_space_channels_file, process_id, &
               &  code, n_in, n_out, par, sqrts, n_channels, phs)
          new_phase_space = .true.
       else 
          call msg_message (" No phase space configuration found for process " &
               &            // process_id)
          call msg_message (" If you want me to generate a phase space configuration,")
          call msg_message (" set generate_phase_space = T in the input file.")
          call msg_fatal (" Phase space configuration failed.")
       end if
    else
       write (msg_buffer, "(1x,I5,1x,A)") n_channels, &
            & "phase space channels found for process "//trim(process_id)
       call msg_message
       new_phase_space = .false.
    end if
  end subroutine forest_count

  subroutine forest_count_unit (unit, process_id, n_channels)
    integer, intent(in) :: unit
    character(len=*), intent(in) :: process_id
    integer, intent(out) :: n_channels
    integer :: iostat
    integer :: ch
    logical :: ok
    character(len=BUFFER_SIZE) :: string
    n_channels = 0
    FIND_PROCESS: do
       call find_string(unit, 'process', ok=ok)
       if (.not.ok) exit FIND_PROCESS
       call find_string(unit, process_id, &
            & reject=(/ 'process  ', 'grove    ', 'tree     ', &
            &           'cut      ', 'helicity ', 'flavor   ', 'color    ', &
            &           'histogram', 'and      ' /), &
            & ok=ok)
       if (ok) exit FIND_PROCESS
    end do FIND_PROCESS
    if (.not.ok) then
!        call msg_warning (" No entry found for process "//trim(process_id)//".")
       call lex_clear
       return
    end if
    SCAN_TREES: do ch=1, MAX_CHANNELS
       call find_string(unit, 'tree', reject=(/'process'/), ok=ok, &
            &           iostat=iostat)
       if (.not.ok) exit SCAN_TREES
       n_channels = n_channels+1
       SCAN_MAPPINGS: do
          call find_string(unit, 'map', here=.true., ok=ok, &
               &          iostat=iostat)
          if (iostat < 0) exit SCAN_TREES
          if (.not.ok) exit SCAN_MAPPINGS
          string = get_string(unit)
          string = get_string(unit)
          string = get_string(unit)
       end do SCAN_MAPPINGS
    end do SCAN_TREES
    call lex_clear
  end subroutine forest_count_unit

  subroutine forest_read (unit, f, process_id)
    integer, intent(in) :: unit
    type(decay_forest), intent(inout) :: f
    character(len=*), intent(in) :: process_id
    integer :: iostat
    integer, dimension(2*MAX_EXTERNAL) :: a
    integer :: i, ch, code, mode
    integer(kind=TC) :: k
    logical :: ok, within_grove
    character(len=BUFFER_SIZE) :: string
    character(len=PRT_NAME_LEN) :: name
    type(mapping), dimension(f%n_branches_tot) :: default_map
    default_map = mapping_null
    FIND_PROCESS: do
       call find_string(unit, 'process', ok=ok)
       if (.not.ok) exit FIND_PROCESS
       call find_string(unit, process_id, &
            & reject=(/ 'process  ', 'grove    ', 'tree     ', &
            &           'cut      ', 'helicity ', 'flavor   ', 'color    ', &
            &           'histogram', 'and      ' /), &
            & ok=ok)
       if (ok) exit FIND_PROCESS
    end do FIND_PROCESS
    if (.not.ok)  call msg_bug &
         & (" Process entry not found in phase space configuration.")
    SCAN_DEFAULT_MAPPINGS: do
       call find_string(unit, 'map', reject=(/'process', 'grove  ', 'tree   '/), &
            &           ok=ok, &
            &           iostat=iostat)
       if (.not.ok) exit SCAN_DEFAULT_MAPPINGS
       k = get_integer(unit)
       string = get_string(unit)
       select case(string(1:1))
       case('s','S');  mode = S_CHANNEL
       case('t','T');  mode = T_CHANNEL
       case('u','U');  mode = U_CHANNEL
       case('c','C');  mode = COLLINEAR
       case('i','I');  mode = INFRARED
       case('r','R');  mode = RADIATION
       case('o','O');  mode = ON_SHELL
       case('n','-');  mode = NO_MAPPING
       case default
          call msg_message (" map "//trim(string)//" ...")
          call msg_error (" This is not a valid mapping specifier.  I'll ignore this.")
          mode = NO_MAPPING
       end select
       name = get_string(unit)
       code = particle_code(name)
       if (mode/=NO_MAPPING .and. code==UNDEFINED) then
          call msg_message (" mapping for particle "//name//":")
          call msg_error (" This particle is undefined.  I'll ignore the mapping.")
          mode = NO_MAPPING
       end if
       call set (default_map(k), mode=mode, code=code)
    end do SCAN_DEFAULT_MAPPINGS
    within_grove = .false.
    SCAN_TREES: do ch=1, MAX_CHANNELS
       call find_string(unit, 'grove', reject=(/'process', 'tree   '/), &
            &           ok=ok, iostat=iostat)
       if (ok .or. ch > f%n_channels_max) then
          call find_string(unit, 'end', reject=(/'process', 'tree   '/), &
               &           ok=ok, iostat=iostat)
          if (ok .or. ch > f%n_channels_max) then
             within_grove = .false.
          else
             f%n_groves = f%n_groves + 1
             f%grove(f%n_groves)%first = ch
             within_grove = .true.
          end if
       end if
       if (within_grove) f%grove(f%n_groves)%last = ch
       call find_string(unit, 'tree', reject=(/'process'/), ok=ok, &
            &           iostat=iostat)
       if (.not.ok) exit SCAN_TREES
       SET_TREE_CODES: do i=1, size(a)
          a(i) = get_integer(unit, ok=ok)
          if (.not.ok) exit SET_TREE_CODES
       end do SET_TREE_CODES
       call forest_add_tree(f, a(:i-1), default_map)
       SCAN_MAPPINGS: do
          call find_string(unit, 'map', here=.true., ok=ok, &
               &          iostat=iostat)
          if (iostat < 0) exit SCAN_TREES
          if (.not.ok) exit SCAN_MAPPINGS
          k = get_integer(unit)
          string = get_string(unit)
          select case(string(1:1))
          case('s','S');  mode = S_CHANNEL
          case('t','T');  mode = T_CHANNEL
          case('u','U');  mode = U_CHANNEL
          case('c','C');  mode = COLLINEAR
          case('i','I');  mode = INFRARED
          case('r','R');  mode = RADIATION
          case('o','O');  mode = ON_SHELL
          case('n','-');  mode = NO_MAPPING
          case default
             call msg_message (" map "//trim(string)//" ...")
             call msg_error (" This is not a valid mapping specifier.  I'll ignore this.")
             mode = NO_MAPPING
          end select
          name = get_string(unit)
          code = particle_code(name)
          if (mode/=NO_MAPPING .and. code==UNDEFINED) then
             call msg_message (" mapping for particle "//name//":")
             call msg_error (" This particle is undefined.  I'll ignore the mapping.")
             mode = NO_MAPPING
          end if
          call forest_set_mapping(f, k, mode, code, ch)
       end do SCAN_MAPPINGS
    end do SCAN_TREES
    call lex_clear
  end subroutine forest_read

  subroutine forest_add_tree(f, a, default_map)
    type(decay_forest), intent(inout) :: f
    integer(kind=TC), dimension(:), intent(in) :: a
    type(mapping), dimension(:), intent(in), optional :: default_map
    integer :: i,ch
    ch = f%n_channels+1
    if (ch>f%n_channels_max)  then
       call msg_fatal(" Can't happen: Maximum number of phase space channels exceeded in decay forest.")
    end if
    call create(f%tree(ch), f%n_in, f%n_out)
    call tree_from_array(f%tree(ch), a)
    call flip_t_to_s_channel(f%tree(ch))
    if (present(default_map)) then
       do i=1, size(a)
          f%map(flipped(f%tree(ch),a(i)), ch) = default_map(a(i))
       end do
    end if
    f%n_channels = ch
  end subroutine forest_add_tree

  subroutine forest_set_mapping(f, k, mode, code, channel)
    type(decay_forest), intent(inout) :: f
    integer(kind=TC), intent(in) :: k
    integer, intent(in) :: mode, code
    integer, intent(in), optional :: channel
    integer :: ch
    ch = f%n_channels;  if (present(channel)) ch = channel
    if (ch>f%n_channels) then
       write (msg_buffer, "(1x,A,1x,I5,1x,A,1x,I5)") &
            & "ch =", ch, "n_channels =", f%n_channels
       call msg_message
       call msg_error ("Mapping for absent phase space channel: Ignored.")
    else
       call set (f%map(flipped(f%tree(ch),k),ch), mode=mode, code=code)
    end if
  end subroutine forest_set_mapping

  subroutine forest_broadcast(f, root, domain, error)
    type(decay_forest), intent(inout) :: f
    integer, intent(in) :: root
    integer, intent(in), optional :: domain
    integer, intent(out), optional :: error
!     integer, dimension(:), allocatable :: buffer
!     allocate(buffer(size(transfer(f, buffer))))
!     buffer = transfer(f, buffer)
!     call mpi90_broadcast(buffer, root, domain, error)
!     f = transfer(buffer, f)
!     deallocate(buffer)
    if (present(error))  error = 0
  end subroutine forest_broadcast

  subroutine forest_set_strfun &
       & (f, n_dim_strfun, n_dim_recoil, generated_externally)
    type(decay_forest), intent(inout) :: f
    integer, intent(in) :: n_dim_strfun, n_dim_recoil
    integer, dimension(2), intent(in) :: generated_externally
    f%generated_externally = generated_externally
    if (f%n_out > 1) then
       f%n_dim_strfun = n_dim_strfun
       f%n_dim_dummy  = 0
    else if (n_dim_strfun > 1) then
       f%n_dim_strfun = n_dim_strfun - 1
       f%n_dim_dummy  = 0
    else if (n_dim_strfun == 1) then
       f%n_dim_strfun = 0
       f%n_dim_dummy  = 1
    else
       call msg_fatal (" Can't calculate single-particle production without structure functions")
    end if
    f%n_dim_recoil = n_dim_recoil
    f%n_dim_extra       = f%n_dim_strfun + f%n_dim_recoil + f%n_dim_dummy
    f%n_dim_integration = f%n_dim_momenta + f%n_dim_extra
  end subroutine forest_set_strfun

  subroutine forest_prepare (f, code, par, cuts, use_equivalences)
    type(decay_forest), intent(inout) :: f
    integer, dimension(:), intent(in) :: code
    type(parameter_set), intent(in) :: par
    type(default_cuts), intent(in) :: cuts
    logical, intent(in) :: use_equivalences
    logical :: variable_limits
    integer :: independent
    call forest_canonicalize (f%tree)
    call set_mass_sum (f%mass_sum, f%n_branches_decay, code, par)
    variable_limits = f%n_dim_strfun > 0
    call set_map_parameters &
         & (f%map, f%n_branches_decay, f%n_channels, &
         &  par, cuts, variable_limits)
    call msg_message (" Scanning phase space channels for equivalences ...")
    call destroy (f%perm_list)
    call make_permutations (f%perm_list, code)
    independent = f%n_channels / len (f%perm_list)
!     write (msg_buffer, "(1x,A,1x,I5,1x,A)") &
!          & "Phase space:", independent, "channels are inequivalent."
!     call msg_message
    if (f%n_channels > 0) then
       call set_equivalences &
            & (f%eq, f%n_eq, f%tree, f%map, f%perm_list, &
            &  f%n_channels, f%n_groves, f%grove, use_equivalences)
    end if
    write (msg_buffer, "(1x,A,1x,I7,1x,A)") &
         & "Phase space:", f%n_eq, "equivalence relations found."
    call msg_message
  end subroutine forest_prepare

  subroutine forest_canonicalize (t)
    type(decay_tree), dimension(:), intent(inout) :: t
    integer :: ch
    do ch=1, size (t)
       call tree_canonicalize (t(ch))
    end do
  end subroutine forest_canonicalize

  subroutine set_mass_sum (mass_sum, n_branches_decay, code, par)
    real(kind=default), dimension(:), intent(inout) :: mass_sum
    integer, intent(in) :: n_branches_decay
    integer, dimension(:), intent(in) :: code
    type(parameter_set), intent(in) :: par
    integer(kind=TC) :: k
    integer :: i
    mass_sum = 0
    do k=1, n_branches_decay
       do i=0, ubound(code,1)-1
          if (btest(k,i)) then
             if (ibclr(k,i)==0) then
                mass_sum(k) = particle_mass(code(i+1), par)
             else
                mass_sum(k) = mass_sum(ibclr(k,i)) + mass_sum(ibset(0,i))
             end if
             exit
          end if
       end do
    end do
  end subroutine set_mass_sum
  
  subroutine set_map_parameters &
       & (map, n_branches_decay, n_channels, par, cuts, variable_limits)
    type(mapping), dimension(:,:), intent(inout) :: map
    integer, intent(in) :: n_branches_decay, n_channels
    type(parameter_set), intent(in) :: par
    type(default_cuts), intent(in) :: cuts
    logical, intent(in) :: variable_limits
    integer(kind=TC) :: k
    integer :: ch
    do k=1, n_branches_decay
       do ch=1, n_channels
          call set (map(k,ch), par, cuts, variable_limits, k)
       end do
    end do
  end subroutine set_map_parameters

  subroutine set_equivalences &
       & (eq_root, n_eq, tree, map, pl, &
       &  n_channels, n_groves, grove, use_equivalences)
    type(equivalences), pointer :: eq_root
    integer, intent(out) :: n_eq
    type(decay_tree), dimension(:), intent(in) :: tree
    type(mapping), dimension(:,:), intent(in) :: map
    type(permutation_list), intent(in) :: pl
    type(permutation) :: perm
    integer, intent(in) :: n_channels, n_groves
    type(decay_grove), dimension(:), intent(in) :: grove
    logical, intent(in) :: use_equivalences
    integer :: ch, cc, i, g
    type(equivalences), pointer :: eq, eq_last
    logical :: equivalent
    n_eq = 0
    nullify (eq_root)
    if (use_equivalences) then
       do g=1, n_groves
          do ch=grove(g)%first, grove(g)%last
             do cc=grove(g)%first, grove(g)%last
                do i=1, len(pl)
                   call extract (perm, pl, i)
                   equivalent = tree_permuted_equal &
                        & (tree(ch), map(:,ch), tree(cc), map(:,cc), perm)
                   call destroy (perm)
                   if (equivalent) then
                      call create (eq, ch, cc, i)
                      if (.not.associated (eq_root)) then
                         eq_root => eq
                         eq_last => eq_root
                         n_eq = 1
                      else
                         eq_last%next => eq
                         eq_last => eq_last%next
                         n_eq = n_eq + 1
                      end if
                   end if
                end do
             end do
          end do
       end do
    else
       do ch=1, n_channels
          call create (eq, ch, ch, 1)
          if (.not.associated (eq_root)) then
             eq_root => eq
             eq_last => eq_root
             n_eq = 1
          else
             eq_last%next => eq
             eq_last => eq_last%next
             n_eq = n_eq + 1
          end if
       end do
    end if
  end subroutine set_equivalences
  
  function tree_permuted_equal (t1, m1, t2, m2, perm) result (is_equal)
    type(decay_tree), intent(in) :: t1, t2
    type(mapping), dimension(:), intent(in) :: m1, m2
    type(permutation), intent(in) :: perm
    logical :: equal, is_equal
    integer(kind=TC) :: k1, k2, mask_in
    k1 = t1%mask_out
    k2 = t2%mask_out
    mask_in = t1%mask_in
    equal = .true.
    call check (t1%branch(k1), t2%branch(k2), k1, k2)
    is_equal = equal
  contains
    recursive subroutine check (b1, b2, k1, k2)
      type(branch), intent(in) :: b1, b2
      integer(kind=TC), intent(in) :: k1, k2
      integer(kind=TC), dimension(2) :: d1, d2, pd2
      integer :: i
      if (.not.b1%has_friend .and. .not.b2%has_friend) then
         equal = .true.
      else if (b1%has_friend .and. b2%has_friend) then
         equal = (b1%friend == tc_permute (b2%friend, perm, mask_in))
      end if
      if (equal) then
         if (b1%has_children .and. b2%has_children) then
            d1 = b1%daughter
            d2 = b2%daughter
            do i=1, 2
               pd2(i) = tc_permute (d2(i), perm, mask_in)
            end do
            if (d1(1)==pd2(1) .and. d1(2)==pd2(2)) then
               equal = (b1%firstborn == b2%firstborn)
               if (equal) call check &
                    &     (t1%branch(d1(1)), t2%branch(d2(1)), d1(1), d2(1))
               if (equal) call check &
                    &     (t1%branch(d1(2)), t2%branch(d2(2)), d1(2), d2(2))
            else if (d1(1)==pd2(2) .and. d1(2)==pd2(1)) then
               equal = ( (b1%firstborn == 0 .and. b2%firstborn == 0) &
                    &   .or. (b1%firstborn == 3 - b2%firstborn) )
               if (equal) call check &
                    &     (t1%branch(d1(1)), t2%branch(d2(2)), d1(1), d2(2))
               if (equal) call check &
                    &     (t1%branch(d1(2)), t2%branch(d2(1)), d1(2), d2(1))
            else
               equal = .false.
            end if
         end if
      end if
      if (equal) then
         equal = (m1(k1) == m2(k2))
      end if
    end subroutine check
  end function tree_permuted_equal

  subroutine forest_set_equivalences (f)
    type(decay_forest), intent(inout) :: f
    call set_eq (f%eq)
!    call write (6, f%eq, f%perm_list)
  contains
    recursive subroutine set_eq (eq)
      type(equivalences), pointer :: eq
      if (associated (eq)) then
         call set_msq_equivalence &
              & (eq, f%tree, f%perm_list, f%n_dim_masses)
         call set_angle_equivalence &
              & (eq, f%tree, f%perm_list, f%n_dim_angles)
         call set_eq (eq%next)
      end if
    end subroutine set_eq
  end subroutine forest_set_equivalences

  subroutine set_msq_equivalence (eq, tree, perm_list, n_dim)
    type(equivalences), intent(inout) :: eq
    type(decay_tree), dimension(:), intent(in) :: tree
    type(permutation_list), intent(in) :: perm_list
    integer, intent(in) :: n_dim
    type(decay_tree) :: t1, t2
    type(permutation) :: perm
    integer :: i
    integer(kind=TC), dimension(n_dim) :: dim1, dim2
    integer(kind=TC) :: mask_in
    t1 = tree(eq%left)
    t2 = tree(eq%right)
    mask_in = t1%mask_in
    i = 0
    call create (perm, size(perm_list))
    call scan (t1, t1%branch(t1%mask_out), t1%mask_out, dim1)
    call destroy (perm)
    i = 0
    call extract (perm, perm_list, eq%perm_index)
    call scan (t2, t2%branch(t2%mask_out), t2%mask_out, dim2)
    call destroy (perm)
    call destroy (eq%msq_perm)
    call create (eq%msq_perm, n_dim)
    call find_permutation (eq%msq_perm, dim1, dim2)
    if (.not.permutation_ok (eq%msq_perm)) then
       call write (6, eq%msq_perm)
       call msg_bug (" Invalid permutation generated")
    end if
  contains
    recursive subroutine scan (t, b, k, dim)
      type(decay_tree), intent(in) :: t
      type(branch), intent(in) :: b
      integer(kind=TC), intent(in) :: k
      integer, dimension(:), intent(inout) :: dim
      if (b%has_children) then
         call scan (t, t%branch(b%daughter(1)), b%daughter(1), dim)
         call scan (t, t%branch(b%daughter(2)), b%daughter(2), dim)
         i = i + 1
         if (i <= n_dim)  dim(i) = tc_permute (k, perm, mask_in)
      end if
    end subroutine scan
  end subroutine set_msq_equivalence

  subroutine set_angle_equivalence (eq, tree, perm_list, n_dim)
    type(equivalences), intent(inout) :: eq
    type(decay_tree), dimension(:), intent(in) :: tree
    type(permutation_list), intent(in) :: perm_list
    integer, intent(in) :: n_dim
    type(decay_tree) :: t1, t2
    type(permutation) :: perm
    integer(kind=TC) :: mask_in
    integer :: i
    integer(kind=TC), dimension(n_dim) :: dim1, dim2
    t1 = tree(eq%left)
    t2 = tree(eq%right)
    mask_in = t1%mask_in
    i = 0
    deallocate (eq%angle_sig)
    allocate (eq%angle_sig(n_dim))
    call create (perm, size(perm_list))
    call scan (t1, t1%branch(t1%mask_out), t1%mask_out, dim1)
    call destroy (perm)
    if (.not. all(eq%angle_sig))  call msg_bug &
         & ("Recording equivalences: non-canonical tree encountered")
    i = 0
    call extract (perm, perm_list, eq%perm_index)
    call scan (t2, t2%branch(t2%mask_out), t2%mask_out, dim2)
    call destroy (perm)
    call destroy (eq%angle_perm)
    call create (eq%angle_perm, n_dim)
    call find_permutation (eq%angle_perm, dim1, dim2)
    if (.not.permutation_ok (eq%angle_perm)) then
       call write (6, eq%angle_perm)
       call msg_bug (" Invalid permutation generated")
    end if
  contains
    recursive subroutine scan (t, b, k, dim)
      type(decay_tree), intent(in) :: t
      type(branch), intent(in) :: b
      integer(kind=TC), intent(in) :: k
      integer, dimension(:), intent(inout) :: dim
      integer(kind=TC) :: k1, k2
      logical :: sig
      if (b%has_children) then
         k1 = b%daughter(1)
         k2 = b%daughter(2)
         sig = (tc_permute(k1, perm, mask_in) < tc_permute(k2, perm, mask_in))
         i = i + 1
         if (i <= n_dim) then
            dim(i) = tc_permute (k, perm, mask_in)
            eq%angle_sig(i) = sig
         end if
         i = i + 1
         if (i <= n_dim)  then
            dim(i) = - tc_permute (k, perm, mask_in)
            eq%angle_sig(i) = sig
         end if
         call scan (t, t%branch(k1), k1, dim)
         call scan (t, t%branch(k2), k2, dim)
      end if
    end subroutine scan
  end subroutine set_angle_equivalence


end module decay_forests
