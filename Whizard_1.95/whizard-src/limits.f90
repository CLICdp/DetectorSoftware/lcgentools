! WHIZARD 1.95 Feb 25 2010
! 
! (C) 1999-2008 by 
!     Wolfgang Kilian <wolfgang.kilian@desy.de>
!     Thorsten Ohl <ohl@physik.uni-wuerzburg.de>
!     Juergen Reuter <juergen.reuter@physik.uni-freiburg.de>
!
! WHIZARD is free software; you can redistribute it and/or modify it
! under the terms of the GNU General Public License as published by 
! the Free Software Foundation; either version 2, or (at your option)
! any later version.
!
! WHIZARD is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! This file has been stripped of most comments.  For documentation, refer
! to the source 'whizard.nw'

module limits

  use kinds, only: default !NODEP!

  implicit none
  private

  integer, parameter, public :: VERSION_STRLEN = 255
  character(len=VERSION_STRLEN), parameter, public :: &
       & VERSION_STRING = "WHIZARD version 1.95 (Feb 25 2010)"
  integer, parameter, public :: WHIZARD_ROOT = 0
  integer, parameter, public :: BUFFER_SIZE = 255
  integer, parameter, public :: MAX_ERRORS = 10
  integer, parameter, public :: FILENAME_LEN = 256
  character(len=*), parameter, public :: DEFAULT_FILENAME = "whizard"
  character, parameter, public :: CR = achar (13)
  character(len=*), parameter, public :: SEPARATORS = ' ,;'
  character(len=*), parameter, public :: COMMENT_CHARS   = '!#'
  character, parameter, public :: BLANK = ' ',  TAB = achar(9)
  integer, parameter, public :: SIGINT = 2, SIGTERM = 15, SIGXCPU = 24
  integer, parameter, public :: CMDLINE_OPTKEY_LEN = 50
  integer, parameter, public :: CMDLINE_ARG_LEN = 1000
  integer, parameter, public :: MAX_CMDLINE_OPTIONS = 20
  integer, parameter, public :: PRT_NAME_LEN = 16
  real(kind=default), parameter, public :: KIREPS = 1E-6_default
  integer, parameter, public :: CIRCE2_MAX_TRIES = 100000
  integer, parameter, public :: LHAPDF_FILENAME_LEN = 20
  integer, parameter, public :: SHOWER_MAX_TRY_BRANCHING = 100
  integer, parameter, public :: SHOWER_MAX_TRY_PT = 10
  integer, parameter, public :: SHOWER_MAX_LEVELS = 50
  integer, parameter, public :: SHOWER_MAX_TRY = 10
  integer, parameter, public :: STDHEP_BYTES_PER_FILE = 5008
  integer, parameter, public :: STDHEP_BYTES_PER_EVENT = 168
  integer, parameter, public :: STDHEP_BYTES_PER_ENTRY = 96
  integer, parameter, public :: MAX_CASCADES = 500000
  integer, parameter, public :: MAX_WARN_RESONANCE = 50
  integer, parameter, public :: MAX_EXTERNAL = 8
  integer, parameter, public :: TC = selected_int_kind(MAX_EXTERNAL)
  real(kind=default), parameter, public :: MAPPING_SCALE_MIN = 1.E-6_default
  real(kind=default), parameter, public :: &
       & CT_MAPPING_DEFAULT_SCALE = 1._default
  integer, parameter, public :: CUT_WINDOWS_MAX = 4
  integer, parameter, public :: CUTS_MAX = 64
  integer, parameter, public :: CUT_TEX_STRLEN = 80
  integer, parameter, public :: CUT_MODE_STRLEN = 16
  integer, parameter, public :: MAX_CHANNELS = 10000 !1024
  integer, parameter, public :: PDFSET_ARRAY_SIZE = 20
  integer, parameter, public :: PDFSET_CHAR_LEN = 20
  integer, parameter, public :: PYTHIA_EXTERNAL_PROCESS = 199
  integer, parameter, public :: MB_PER_FB_EXPONENT = 12
  integer, parameter, public :: PB_PER_FB_EXPONENT =  3
  integer, parameter, public :: PYTHIA_PARAMETERS_LEN = 1000
!  integer, parameter, public :: HERWIG_EXTERNAL_PROCESS = 199
  integer, parameter, public :: HERWIG_PARAMETERS_LEN = 1000
  integer, parameter, public :: SLHA_BLOCK_NAME_LEN = 20
  integer, parameter, public :: PROCESS_ID_LEN = 16
!  integer, parameter, public :: NUMBER_PROCESSES_MAX  = 100
  integer, parameter, public :: LOGTABLE_LINE_LENGTH = 79
  integer, parameter, public :: GRID_EXTENSION_LENGTH = 3
  integer, parameter, public :: HISTOGRAMS_MAX = 64
  integer, parameter, public :: HELICITIES_MAX = 64
  integer, parameter, public :: FLAVORS_MAX = 64
  integer, parameter, public :: COLOR_FLOWS_MAX = 64
  integer, parameter, public :: ANALYZE_MAX = 32
  integer, parameter, public :: PYTHIA_BEAM_STRLEN = 12
  integer, parameter, public :: WEIGHT_HISTOGRAMS_NBIN = 20

end module limits
