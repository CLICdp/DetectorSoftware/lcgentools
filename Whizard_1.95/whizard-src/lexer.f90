! WHIZARD 1.95 Feb 25 2010
! 
! (C) 1999-2008 by 
!     Wolfgang Kilian <wolfgang.kilian@desy.de>
!     Thorsten Ohl <ohl@physik.uni-wuerzburg.de>
!     Juergen Reuter <juergen.reuter@physik.uni-freiburg.de>
!
! WHIZARD is free software; you can redistribute it and/or modify it
! under the terms of the GNU General Public License as published by 
! the Free Software Foundation; either version 2, or (at your option)
! any later version.
!
! WHIZARD is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! This file has been stripped of most comments.  For documentation, refer
! to the source 'whizard.nw'

module lexer

  use limits, only: BUFFER_SIZE
  use limits, only: BLANK, TAB, SEPARATORS, COMMENT_CHARS
  use diagnostics, only: msg_message, msg_error

  implicit none
  private

  public :: lex
  public :: lex_back
  public :: lex_clear

  character(len=BUFFER_SIZE),   save :: current_token = BLANK
  character(len=BUFFER_SIZE+1), save :: buffer = BLANK

contains

  subroutine lex (unit, token, iostat)
    integer, intent(in) :: unit
    character(len=*), intent(out) :: token
    integer, intent(out) :: iostat
    iostat=0
    GET_TOKEN: do
       if (current_token /= BLANK) exit GET_TOKEN
       READLINE: do
          if (buffer /= BLANK) exit READLINE
          read(unit=unit, fmt='(A)', iostat=iostat) buffer
          if (iostat /= 0) exit GET_TOKEN
          call tabs_to_blanks(buffer)
          buffer = adjustl(buffer)
       end do READLINE
       call chop_token(current_token, buffer)
    end do GET_TOKEN
    token = current_token
    current_token = BLANK
  end subroutine lex

  subroutine lex_back(token)
    character(len=*), intent(in) :: token
    current_token = token
  end subroutine lex_back

  subroutine lex_clear
    current_token = BLANK
    buffer = BLANK
  end subroutine lex_clear

  subroutine tabs_to_blanks(buffer)
    character(len=*), intent(inout) :: buffer
    integer :: pos
    SCAN_BUFFER: do
       pos = scan(buffer, TAB)
       if (pos==0) exit SCAN_BUFFER
       buffer(pos:pos) = BLANK
    end do SCAN_BUFFER
  end subroutine tabs_to_blanks

  subroutine chop_token(token, buffer)
    character(len=*), intent(out) :: token
    character(len=*), intent(inout) :: buffer
    integer :: pos
    pos = scan(buffer, SEPARATORS)
    select case(pos)
    case(0)
       call msg_message (buffer)
       call msg_error (" This token exceeds the maximum buffer length.  I'll ignore it.")
    case(1)
       token  = BLANK
    case default
       token  = buffer(:pos-1)
       buffer = adjustl(buffer(pos+1:))
    end select
    if (verify(token(1:1), COMMENT_CHARS)==0) then
       token = BLANK
       buffer = BLANK
    end if
  end subroutine chop_token


end module lexer
