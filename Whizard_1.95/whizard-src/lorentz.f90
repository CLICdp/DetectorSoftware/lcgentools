! WHIZARD 1.95 Feb 25 2010
! 
! (C) 1999-2008 by 
!     Wolfgang Kilian <wolfgang.kilian@desy.de>
!     Thorsten Ohl <ohl@physik.uni-wuerzburg.de>
!     Juergen Reuter <juergen.reuter@physik.uni-freiburg.de>
!
! WHIZARD is free software; you can redistribute it and/or modify it
! under the terms of the GNU General Public License as published by 
! the Free Software Foundation; either version 2, or (at your option)
! any later version.
!
! WHIZARD is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! This file has been stripped of most comments.  For documentation, refer
! to the source 'whizard.nw'

module lorentz

  use kinds, only: default !NODEP!
  use constants, only: pi, twopi

  implicit none
  private

  type, public :: three_momentum
     private
     real(kind=default), dimension(3) :: p
  end type three_momentum
  type, public :: four_momentum
     private
     real(kind=default), dimension(0:3) :: p
  end type four_momentum
  type, public :: lorentz_transformation
     private
     real(kind=default), dimension(0:3, 0:3) :: L
  end type lorentz_transformation

  public :: write
  public :: three_momentum_canonical
  public :: three_momentum_moving
  public :: operator(+), operator(-)
  public :: operator(*), operator(/)
  public :: cross_product
  public :: operator(**)
  public :: sum
  public :: component
  public :: array
  public :: four_momentum_canonical
  public :: four_momentum_at_rest
  public :: four_momentum_moving
  public :: space_part
  public :: azimuthal_angle
  public :: azimuthal_distance
  public :: polar_angle
  public :: polar_angle_ct
  public :: angle
  public :: angle_ct
  public :: transverse_momentum
  public :: longitudinal_momentum
  public :: absolute_momentum
  public :: energy
  public :: invariant_mass
  public :: rapidity
  public :: pseudorapidity
  public :: pseudorapidity_distance
  public :: eta_phi_distance
  public :: inverse
  public :: boost
  public :: rotation
  public :: transformation
  public :: LT_compose_r3_r2_b3
  public :: LT_compose_r2_r3_b3
  public :: axis_from_p_r3_r2_b3, axis_from_p_b3
  public :: lambda

  type(three_momentum), parameter, public :: three_momentum_null &
       & = three_momentum((/ 0._default, 0._default, 0._default /))
  type(four_momentum), parameter, public :: four_momentum_null &
       & = four_momentum((/ 0._default, 0._default, 0._default, 0._default /))
  type(lorentz_transformation), parameter, public :: &
       & unit_lorentz_transformation = &
       & lorentz_transformation( &
       & reshape( source = (/ 1._default, 0._default, 0._default, 0._default, &
       &                      0._default, 1._default, 0._default, 0._default, &
       &                      0._default, 0._default, 1._default, 0._default, &
       &                      0._default, 0._default, 0._default, 1._default /),&
       &          shape = (/ 4,4 /) ) )
  type(lorentz_transformation), parameter, public :: &
       & lorentz_space_reflection = &
       & lorentz_transformation( &
       & reshape( source = (/ 1._default, 0._default, 0._default, 0._default, &
       &                      0._default,-1._default, 0._default, 0._default, &
       &                      0._default, 0._default,-1._default, 0._default, &
       &                      0._default, 0._default, 0._default,-1._default /),&
       &          shape = (/ 4,4 /) ) )

  integer, dimension(3,3), parameter :: delta_three = &
       & reshape( source = (/ 1,0,0, 0,1,0, 0,0,1 /), &
       &          shape  = (/3,3/) )
  integer, dimension(3,3,3), parameter :: epsilon_three = &
       & reshape( source = (/ 0, 0,0,  0,0,-1,   0,1,0,&
       &                      0, 0,1,  0,0, 0,  -1,0,0,&
       &                      0,-1,0,  1,0, 0,   0,0,0 /),&
       &          shape = (/3,3,3/) )

  interface write
     module procedure write_three_momentum_unit
  end interface
  interface three_momentum_moving
     module procedure three_momentum_moving_canonical
     module procedure three_momentum_moving_generic
  end interface
  interface operator(+)
     module procedure add_three
  end interface
  interface operator(-)
     module procedure diff_three
  end interface
  interface operator(*)
     module procedure prod_integer_three, prod_three_integer
     module procedure prod_real_three, prod_three_real
  end interface
  interface operator(/)
     module procedure div_three_real, div_three_integer
  end interface
  interface operator(*)
     module procedure prod_three
  end interface
  interface operator(**)
     module procedure power_three
  end interface
  interface operator(-)
     module procedure negate_three
  end interface
  interface sum
     module procedure sum_three
  end interface
  interface component
     module procedure component_three
  end interface
  interface array
     module procedure array_from_three_momentum
  end interface
  interface write
     module procedure write_four_momentum_unit
  end interface
  interface four_momentum_moving
     module procedure four_momentum_moving_canonical
     module procedure four_momentum_moving_three
     module procedure four_momentum_moving_generic
  end interface
  interface operator(+)
     module procedure add_four
  end interface
  interface operator(-)
     module procedure diff_four
  end interface
  interface operator(*)
     module procedure prod_real_four, prod_four_real
     module procedure prod_integer_four, prod_four_integer
  end interface
  interface operator(/)
     module procedure div_four_real
     module procedure div_four_integer
  end interface
  interface operator(*)
     module procedure prod_four
  end interface
  interface operator(**)
     module procedure power_four
  end interface
  interface operator(-)
     module procedure negate_four
  end interface
  interface sum
     module procedure sum_four
  end interface
  interface component
     module procedure component_four
  end interface
  interface array
     module procedure array_from_four_momentum
  end interface
  interface azimuthal_angle
     module procedure azimuthal_angle_three
     module procedure azimuthal_angle_four
  end interface
  interface azimuthal_distance
     module procedure azimuthal_distance_three
     module procedure azimuthal_distance_four
  end interface
  interface polar_angle
     module procedure polar_angle_three
     module procedure polar_angle_four
  end interface
  interface polar_angle_ct
     module procedure polar_angle_ct_three
     module procedure polar_angle_ct_four
  end interface
  interface angle
     module procedure angle_three
     module procedure angle_four
  end interface
  interface angle_ct
     module procedure angle_ct_three
     module procedure angle_ct_four
  end interface
  interface energy
     module procedure energy_from_four_momentum
     module procedure energy_from_three_momentum
     module procedure energy_from_momentum
  end interface
  interface write
     module procedure write_lorentz_trafo_unit
  end interface
  interface inverse
     module procedure inverse_lorentz_transformation
  end interface
  interface array
     module procedure array_from_lorentz_trafo
  end interface
  interface boost
     module procedure boost_from_rest_frame
     module procedure boost_from_rest_frame_three
     module procedure boost_generic
     module procedure boost_canonical
  end interface
  interface rotation
     module procedure rotation_generic
     module procedure rotation_canonical
     module procedure rotation_generic_cs
     module procedure rotation_canonical_cs
     module procedure rotation_rec_generic
     module procedure rotation_rec_canonical
  end interface
  interface transformation
     module procedure transformation_rec_generic
     module procedure transformation_rec_canonical
  end interface
  interface operator(*)
     module procedure prod_LT_four
     module procedure prod_LT_LT
     module procedure prod_four_LT
  end interface

contains

  subroutine write_three_momentum_unit(u,p)
    integer, intent(in) :: u
    type(three_momentum), intent(in) :: p
    write(u,*) 'P = ', p%p
  end subroutine write_three_momentum_unit
  function three_momentum_canonical(k) result(p)
    integer, intent(in) :: k
    type(three_momentum) :: p
    p = three_momentum_null
    p%p(k) = 1
  end function three_momentum_canonical
  function three_momentum_moving_canonical(p,k) result(q)
    real(kind=default), intent(in) :: p
    integer, intent(in) :: k
    type(three_momentum) :: q
    q = three_momentum_null
    q%p(k) = p
  end function three_momentum_moving_canonical
  function three_momentum_moving_generic(p) result(q)
    real(kind=default), dimension(3), intent(in) :: p
    type(three_momentum) :: q
    q%p = p
  end function three_momentum_moving_generic
  function add_three(p,q) result(r)
    type(three_momentum), intent(in) :: p,q
    type(three_momentum) :: r
    r%p = p%p + q%p
  end function add_three
  function diff_three(p,q) result(r)
    type(three_momentum), intent(in) :: p,q
    type(three_momentum) :: r
    r%p = p%p - q%p
  end function diff_three
  function prod_real_three(s,p) result(q)
    real(kind=default), intent(in) :: s
    type(three_momentum), intent(in) :: p
    type(three_momentum) :: q
    q%p = s * p%p
  end function prod_real_three
  function prod_three_real(p,s) result(q)
    real(kind=default), intent(in) :: s
    type(three_momentum), intent(in) :: p
    type(three_momentum) :: q
    q%p = s * p%p
  end function prod_three_real
  function div_three_real(p,s) result(q)
    real(kind=default), intent(in) :: s
    type(three_momentum), intent(in) :: p
    type(three_momentum) :: q
    q%p = p%p/s
  end function div_three_real
  function prod_integer_three(s,p) result(q)
    integer, intent(in) :: s
    type(three_momentum), intent(in) :: p
    type(three_momentum) :: q
    q%p = s * p%p
  end function prod_integer_three
  function prod_three_integer(p,s) result(q)
    integer, intent(in) :: s
    type(three_momentum), intent(in) :: p
    type(three_momentum) :: q
    q%p = s * p%p
  end function prod_three_integer
  function div_three_integer(p,s) result(q)
    integer, intent(in) :: s
    type(three_momentum), intent(in) :: p
    type(three_momentum) :: q
    q%p = p%p/s
  end function div_three_integer
  function prod_three(p,q) result(s)
    type(three_momentum), intent(in) :: p,q
    real(kind=default) :: s
    s = dot_product(p%p, q%p)
  end function prod_three
  function cross_product(p,q) result(r)
    type(three_momentum), intent(in) :: p,q
    type(three_momentum) :: r
    integer :: i
    do i=1,3
       r%p(i) = dot_product(p%p, matmul(epsilon_three(i,:,:), q%p))
    end do
  end function cross_product
  function power_three(p,e) result(s)
    type(three_momentum), intent(in) :: p
    integer, intent(in) :: e
    real(kind=default) :: s
    s = dot_product(p%p, p%p)
    if (e/=2) then
       if (mod(e,2)==0) then
          s = s**(e/2)
       else
          s = sqrt(s)**e
       end if
    end if
  end function power_three
  function negate_three(p) result(q)
    type(three_momentum), intent(in) :: p
    type(three_momentum) :: q
    q%p = -p%p
  end function negate_three
  function sum_three (p) result (q)
    type(three_momentum), dimension(:), intent(in) :: p
    type(three_momentum) :: q
    integer :: i
    do i=1, 3
       q%p(i) = sum (p%p(i))
    end do
  end function sum_three
!   function sum_three_mask (p, mask) result (q)
!     type(three_momentum), dimension(:), intent(in) :: p
!     logical, dimension(:), intent(in) :: mask
!     type(three_momentum) :: q
!     integer :: i
!     do i=1, 3
!        q%p(i) = sum (p%p(i), mask=mask)
!     end do
!   end function sum_three_mask
  function component_three(p,k) result(c)
    type(three_momentum), intent(in) :: p
    integer, intent(in) :: k
    real(kind=default) :: c
    c = p%p(k)
  end function component_three

  function array_from_three_momentum(p) result(a)
    type(three_momentum), intent(in) :: p
    real(kind=default), dimension(3) :: a
    a = p%p
  end function array_from_three_momentum
  subroutine write_four_momentum_unit(u,p)
    integer, intent(in) :: u
    type(four_momentum), intent(in) :: p
    write(u,*) 'E = ', p%p(0)
    write(u,*) 'P = ', p%p(1:)
  end subroutine write_four_momentum_unit
  function four_momentum_canonical(k) result(p)
    integer, intent(in) :: k
    type(four_momentum) :: p
    p = four_momentum_null
    p%p(k) = 1
  end function four_momentum_canonical
  function four_momentum_at_rest(m) result(p)
    real(kind=default), intent(in) :: m
    type(four_momentum) :: p
    p = four_momentum((/ m, 0._default, 0._default, 0._default /))
  end function four_momentum_at_rest

  function four_momentum_moving_canonical(E,p,k) result(q)
    real(kind=default), intent(in) :: E, p
    integer, intent(in) :: k
    type(four_momentum) :: q
    q = four_momentum_at_rest(E)
    q%p(k) = p
  end function four_momentum_moving_canonical
  function four_momentum_moving_three(E,p) result(q)
    real(kind=default), intent(in) :: E
    type(three_momentum), intent(in) :: p
    type(four_momentum) :: q
    q%p(0) = E
    q%p(1:) = p%p
  end function four_momentum_moving_three
  function four_momentum_moving_generic(p) result(q)
    real(kind=default), dimension(0:3), intent(in) :: p
    type(four_momentum) :: q
    q%p = p
  end function four_momentum_moving_generic
  function add_four(p,q) result(r)
    type(four_momentum), intent(in) :: p,q
    type(four_momentum) :: r
    r%p = p%p + q%p
  end function add_four
  function diff_four(p,q) result(r)
    type(four_momentum), intent(in) :: p,q
    type(four_momentum) :: r
    r%p = p%p - q%p
  end function diff_four
  function prod_real_four(s,p) result(q)
    real(kind=default), intent(in) :: s
    type(four_momentum), intent(in) :: p
    type(four_momentum) :: q
    q%p = s * p%p
  end function prod_real_four
  function prod_four_real(p,s) result(q)
    real(kind=default), intent(in) :: s
    type(four_momentum), intent(in) :: p
    type(four_momentum) :: q
    q%p = s * p%p
  end function prod_four_real
  function div_four_real(p,s) result(q)
    real(kind=default), intent(in) :: s
    type(four_momentum), intent(in) :: p
    type(four_momentum) :: q
    q%p = p%p/s
  end function div_four_real
  function prod_integer_four(s,p) result(q)
    integer, intent(in) :: s
    type(four_momentum), intent(in) :: p
    type(four_momentum) :: q
    q%p = s * p%p
  end function prod_integer_four
  function prod_four_integer(p,s) result(q)
    integer, intent(in) :: s
    type(four_momentum), intent(in) :: p
    type(four_momentum) :: q
    q%p = s * p%p
  end function prod_four_integer
  function div_four_integer(p,s) result(q)
    integer, intent(in) :: s
    type(four_momentum), intent(in) :: p
    type(four_momentum) :: q
    q%p = p%p/s
  end function div_four_integer
  function prod_four(p,q) result(s)
    type(four_momentum), intent(in) :: p,q
    real(kind=default) :: s
    s = p%p(0)*q%p(0) - dot_product(p%p(1:), q%p(1:))
  end function prod_four
  function power_four(p,e) result(s)
    type(four_momentum), intent(in) :: p
    integer, intent(in) :: e
    real(kind=default) :: s
    s = p*p
    if (e/=2) then
       if (mod(e,2)==0) then
          s = s**(e/2)
       elseif (s>=0) then
          s = sqrt(s)**e
       else
          s = -(sqrt(abs(s))**e)
       end if
    end if
  end function power_four
  function negate_four(p) result(q)
    type(four_momentum), intent(in) :: p
    type(four_momentum) :: q
    q%p = -p%p
  end function negate_four
  function sum_four (p) result (q)
    type(four_momentum), dimension(:), intent(in) :: p
    type(four_momentum) :: q
    integer :: i
    do i=0, 3
       q%p(i) = sum (p%p(i))
    end do
  end function sum_four
!   function sum_four_mask (p, mask) result (q)
!     type(four_momentum), dimension(:), intent(in) :: p
!     logical, dimension(:), intent(in) :: mask
!     type(four_momentum) :: q
!     integer :: i
!     do i=0, 3
!        q%p(i) = sum (p%p(i), mask=mask)
!     end do
!   end function sum_four_mask
  function space_part(p) result(q)
    type(four_momentum), intent(in) :: p
    type(three_momentum) :: q
    q%p = p%p(1:)
  end function space_part

  function component_four(p,k) result(c)
    type(four_momentum), intent(in) :: p
    integer, intent(in) :: k
    real(kind=default) :: c
    c = p%p(k)
  end function component_four

  function array_from_four_momentum(p) result(a)
    type(four_momentum), intent(in) :: p
    real(kind=default), dimension(0:3) :: a
    a = p%p
  end function array_from_four_momentum

  function azimuthal_angle_three (p) result (phi)
    type(three_momentum), intent(in) :: p
    real(kind=default) :: phi
    if (any(p%p(1:2)/=0)) then
       phi = atan2(p%p(2), p%p(1))
       if (phi < 0) phi = phi + twopi
    else
       phi = 0
    end if
  end function azimuthal_angle_three
  function azimuthal_angle_four (p) result (phi)
    type(four_momentum), intent(in) :: p
    real(kind=default) :: phi
    phi = azimuthal_angle (space_part (p))
  end function azimuthal_angle_four

  function azimuthal_distance_three (p, q) result (dphi)
    type(three_momentum), intent(in) :: p,q
    real(kind=default) :: dphi
    dphi = azimuthal_angle (q) - azimuthal_angle (p)
    if (dphi <= -pi) then
       dphi = dphi + twopi
    else if (dphi > pi) then
       dphi = dphi - twopi
    end if
  end function azimuthal_distance_three
  function azimuthal_distance_four (p, q) result (dphi)
    type(four_momentum), intent(in) :: p,q
    real(kind=default) :: dphi
    dphi = azimuthal_distance (space_part(p), space_part(q))
  end function azimuthal_distance_four

  function polar_angle_three (p) result (theta)
    type(three_momentum), intent(in) :: p
    real(kind=default) :: theta
    if (any(p%p/=0)) then
       theta = atan2 (sqrt(p%p(1)**2 + p%p(2)**2), p%p(3))
    else
       theta = 0
    end if
  end function polar_angle_three
  function polar_angle_four (p) result (theta)
    type(four_momentum), intent(in) :: p
    real(kind=default) :: theta
    theta = polar_angle (space_part (p))
  end function polar_angle_four

  function polar_angle_ct_three (p) result (ct)
    type(three_momentum), intent(in) :: p
    real(kind=default) :: ct
    if (any(p%p/=0)) then
       ct = p%p(3) / p**1
    else
       ct = 1
    end if
  end function polar_angle_ct_three
  function polar_angle_ct_four (p) result (ct)
    type(four_momentum), intent(in) :: p
    real(kind=default) :: ct
    ct = polar_angle_ct (space_part (p))
  end function polar_angle_ct_four

  function angle_three (p, q) result (theta)
    type(three_momentum), intent(in) :: p, q
    real(kind=default) :: theta
    theta = acos(angle_ct(p,q))
  end function angle_three
  function angle_four (p, q) result (theta)
    type(four_momentum), intent(in) :: p, q
    real(kind=default) :: theta
    theta = angle (space_part(p), space_part(q))
  end function angle_four

  function angle_ct_three (p, q) result (ct)
    type(three_momentum), intent(in) :: p, q
    real(kind=default) :: ct
    if (any(p%p/=0).and.any(q%p/=0)) then
       ct = p*q / (p**1 * q**1)
       if (ct>1) then
          ct = 1
       else if (ct<-1) then
          ct = -1
       end if
    else
       ct = 1
    end if
  end function angle_ct_three
  function angle_ct_four (p, q) result (ct)
    type(four_momentum), intent(in) :: p, q
    real(kind=default) :: ct
    ct = angle_ct (space_part(p), space_part(q))
  end function angle_ct_four

  function transverse_momentum(p) result(pT)
    type(four_momentum), intent(in) :: p
    real(kind=default) :: pT
    pT = sqrt(p%p(1)**2 + p%p(2)**2)
  end function transverse_momentum

  function longitudinal_momentum(p) result(pL)
    type(four_momentum), intent(in) :: p
    real(kind=default) :: pL
    pL = p%p(3)
  end function longitudinal_momentum

  function absolute_momentum(p) result(pabs)
    type(four_momentum), intent(in) :: p
    real(kind=default) :: pabs
    pabs = space_part (p) ** 1
  end function absolute_momentum

  function energy_from_four_momentum (p) result (E)
    type(four_momentum), intent(in) :: p
    real(kind=default) :: E
    E = p%p(0)
  end function energy_from_four_momentum

  function energy_from_three_momentum (p, mass) result (E)
    type(three_momentum), intent(in) :: p
    real(kind=default), intent(in), optional :: mass
    real(kind=default) :: E
    if (present (mass)) then
       E = sqrt (p**2 + mass**2)
    else
       E = p**1
    end if
  end function energy_from_three_momentum

  function energy_from_momentum (p, mass) result (E)
    real(kind=default), intent(in) :: p
    real(kind=default), intent(in), optional :: mass
    real(kind=default) :: E
    if (present (mass)) then
       E = sqrt (p**2 + mass**2)
    else
       E = abs (p)
    end if
  end function energy_from_momentum

  function invariant_mass (p) result (m)
    type(four_momentum), intent(in) :: p
    real(kind=default) :: m
    real(kind=default) :: msq
    msq = p*p
    if (msq > 0) then
       m = sqrt (msq)
    else
       m = 0
    end if
  end function invariant_mass
  function rapidity (p) result (y)
    type(four_momentum), intent(in) :: p
    real(kind=default) :: y
    y = .5 * log( (energy(p) + longitudinal_momentum(p)) &
         &       /(energy(p) - longitudinal_momentum(p)))
  end function rapidity
  function pseudorapidity (p) result (eta)
    type(four_momentum), intent(in) :: p
    real(kind=default) :: eta
    eta = -log( tan(.5 * polar_angle(p)))
  end function pseudorapidity
  function pseudorapidity_distance (p, q) result (deta)
    type(four_momentum), intent(in) :: p, q
    real(kind=default) :: deta
    deta = pseudorapidity (q) - pseudorapidity (p)
  end function pseudorapidity_distance
  function eta_phi_distance (p, q) result (dr)
    type(four_momentum), intent(in) :: p, q
    real(kind=default) :: dr
    dr = sqrt (pseudorapidity_distance(p,q)**2 + azimuthal_distance(p,q)**2)
  end function eta_phi_distance

  subroutine write_lorentz_trafo_unit(u,L)
    integer, intent(in) :: u
    type(lorentz_transformation), intent(in) :: L
    integer :: i
    write(u,*) 'Lorentz transformation:'
    write(u,*) 'L00:'
    write(u,*) L%L(0,0)
    write(u,*) 'L0j:', L%L(0,1:)
    write(u,*) 'Li0, Lij:'
    do i=1,3
       write(u,*) L%L(i,0)
       write(u,*) '    ', L%L(i,1:)
    end do
  end subroutine write_lorentz_trafo_unit

  function inverse_lorentz_transformation(L) result(IL)
    type(lorentz_transformation), intent(in) :: L
    type(lorentz_transformation) :: IL
    IL%L(0,0) = L%L(0,0)
    IL%L(0,1:) = -L%L(1:,0)
    IL%L(1:,0) = -L%L(0,1:)
    IL%L(1:,1:) = transpose(L%L(1:,1:))
  end function inverse_lorentz_transformation

  function array_from_lorentz_trafo(L) result(a)
    type(lorentz_transformation), intent(in) :: L
    real(kind=default), dimension(0:3,0:3) :: a
    a = L%L
  end function array_from_lorentz_trafo
  function boost_from_rest_frame (p, m) result (L)
    type(four_momentum), intent(in) :: p
    real(kind=default) :: m
    type(lorentz_transformation) :: L
    L = boost_from_rest_frame_three (space_part(p), m)
  end function boost_from_rest_frame
  function boost_from_rest_frame_three (p, m) result (L)
    type(three_momentum), intent(in) :: p
    real(kind=default) :: m
    type(lorentz_transformation) :: L
    type(three_momentum) :: beta_gamma
    real(kind=default) :: bg2, g, c
    integer :: i,j
    if (m /= 0) then
       beta_gamma = p / m
       bg2 = beta_gamma**2
    else
       bg2 = 0
    end if
    if (bg2 /= 0) then
       g = sqrt(1 + bg2);  c = (g-1)/bg2
       L%L(0,0)  = g
       L%L(0,1:) = beta_gamma%p
       L%L(1:,0) = L%L(0,1:)
       do i=1,3
          do j=1,3
             L%L(i,j) = delta_three(i,j) + c*beta_gamma%p(i)*beta_gamma%p(j)
          end do
       end do
    else
       L = unit_lorentz_transformation
    end if
  end function boost_from_rest_frame_three
  function boost_canonical (beta_gamma,k) result (L)
    real(kind=default), intent(in) :: beta_gamma
    integer, intent(in) :: k
    type(lorentz_transformation) :: L
    real(kind=default) :: g
    g = sqrt(1 + beta_gamma**2)
    L = unit_lorentz_transformation
    L%L(0,0) = g
    L%L(0,k) = beta_gamma
    L%L(k,0) = L%L(0,k)
    L%L(k,k) = L%L(0,0)
  end function boost_canonical
  function boost_generic (beta_gamma, axis) result (L)
    real(kind=default), intent(in) :: beta_gamma
    type(three_momentum), intent(in) :: axis
    type(lorentz_transformation) :: L
    if (any(axis%p/=0)) then
       L = boost_from_rest_frame_three (beta_gamma * axis, axis**1)
    else
       L = unit_lorentz_transformation
    end if
  end function boost_generic
  function rotation_generic_cs(cp, sp, n) result(R)
    real(kind=default), intent(in) :: cp, sp
    real(kind=default), dimension(3), intent(in) :: n
    type(lorentz_transformation) :: R
    integer :: i,j
    R = unit_lorentz_transformation
    do i=1,3
       do j=1,3
          R%L(i,j) = cp*delta_three(i,j) + (1-cp)*n(i)*n(j)  &
               &   - sp*dot_product(epsilon_three(i,j,:), n)
       end do
    end do
  end function rotation_generic_cs
  function rotation_generic(axis) result(R)
    type(three_momentum), intent(in) :: axis
    type(lorentz_transformation) :: R
    real(kind=default) :: phi
    if (any(axis%p/=0)) then
       phi = abs(axis**1)
       R = rotation_generic_cs(cos(phi), sin(phi), axis%p/phi)
    else
       R = unit_lorentz_transformation
    end if
  end function rotation_generic
  function rotation_canonical_cs(cp, sp, k) result(R)
    real(kind=default), intent(in) :: cp, sp
    integer, intent(in) :: k
    type(lorentz_transformation) :: R
    integer :: i,j
    R = unit_lorentz_transformation
    do i=1,3
       do j=1,3
          R%L(i,j) = -sp*epsilon_three(i,j,k)
       end do
       R%L(i,i) = cp
    end do
    R%L(k,k) = 1
  end function rotation_canonical_cs
  function rotation_canonical(phi,k) result(R)
    real(kind=default), intent(in) :: phi
    integer, intent(in) :: k
    type(lorentz_transformation) :: R
    R = rotation_canonical_cs(cos(phi), sin(phi), k)
  end function rotation_canonical

  function rotation_rec_generic(p, q) result(R)
    type(three_momentum), intent(in) :: p, q
    type(lorentz_transformation) :: R
    type(three_momentum) :: a, b, ab
    real(kind=default) :: ct, st
    if (any(p%p/=0) .and. any(q%p/=0)) then
       a = p/p**1;  b = q/q**1
       ab = cross_product(a,b)
       ct = a*b;  st = ab**1
       if (st/=0) then
          R = rotation_generic_cs(ct, st, ab%p/st)
       else if (ct<0) then
          R = lorentz_space_reflection
       else
          R = unit_lorentz_transformation
       end if
    else
       R = unit_lorentz_transformation
    end if
  end function rotation_rec_generic
  function rotation_rec_canonical (k, p) result (R)
    integer, intent(in) :: k
    type(three_momentum), intent(in) :: p
    type(lorentz_transformation) :: R
    type(three_momentum) :: b, ab
    real(kind=default) :: ct, st
    integer :: i, j
    if (any(p%p/=0)) then
       b = p/p**1
       ab%p = 0
       do i = 1, 3
          do j = 1, 3
             ab%p(j) = ab%p(j) + b%p(i) * epsilon_three(i,j,k)
          end do
       end do
       ct = b%p(k);  st = ab**1
       if (st/=0) then
          R = rotation_generic_cs(ct, st, ab%p/st)
       else if (ct<0) then
          R = lorentz_space_reflection
       else
          R = unit_lorentz_transformation
       end if
    else
       R = unit_lorentz_transformation
    end if
  end function rotation_rec_canonical
  function transformation_rec_generic (axis, p, m) result (L)
    type(three_momentum), intent(in) :: axis
    type(four_momentum), dimension(2), intent(in) :: p
    real(kind=default), intent(in) :: m
    type(lorentz_transformation) :: L
    L = boost (sum (p), m)
    L = L * rotation (axis, space_part (inverse (L) * p(1)))
  end function transformation_rec_generic
  function transformation_rec_canonical (k, p, m) result (L)
    integer, intent(in) :: k
    type(four_momentum), dimension(2), intent(in) :: p
    real(kind=default), intent(in) :: m
    type(lorentz_transformation) :: L
    L = boost (sum (p), m)
    L = L * rotation (k, space_part (inverse (L) * p(1)))
  end function transformation_rec_canonical
  function prod_LT_four(L,p) result(np)
    type(lorentz_transformation), intent(in) :: L
    type(four_momentum), intent(in) :: p
    type(four_momentum) :: np
    np%p = matmul(L%L, p%p)
  end function prod_LT_four
  function prod_LT_LT(L1,L2) result(NL)
    type(lorentz_transformation), intent(in) :: L1,L2
    type(lorentz_transformation) :: NL
    NL%L = matmul(L1%L, L2%L)
  end function prod_LT_LT
  function prod_four_LT(p,L) result(np)
    type(four_momentum), intent(in) :: p
    type(lorentz_transformation), intent(in) :: L
    type(four_momentum) :: np
    np%p = matmul(p%p, L%L)
  end function prod_four_LT

  function LT_compose_r3_r2_b3 (cp, sp, ct, st, beta_gamma) result (L)
    real(kind=default), intent(in) :: cp, sp, ct, st, beta_gamma
    type(lorentz_transformation) :: L
    real(kind=default) :: gamma
    if (beta_gamma==0) then
       L%L(0,0)  = 1
       L%L(1:,0) = 0
       L%L(0,1:) = 0
       L%L(1,1:) = (/  ct*cp, -ct*sp, st /)
       L%L(2,1:) = (/     sp,     cp,  0._default /)
       L%L(3,1:) = (/ -st*cp,  st*sp, ct /)
    else
       gamma = sqrt(1 + beta_gamma**2)
       L%L(0,0)  = gamma
       L%L(1,0)  = 0
       L%L(2,0)  = 0
       L%L(3,0)  = beta_gamma
       L%L(0,1:) = beta_gamma * (/ -st*cp,  st*sp, ct /)
       L%L(1,1:) =              (/  ct*cp, -ct*sp, st /)
       L%L(2,1:) =              (/     sp,     cp, 0._default /)
       L%L(3,1:) = gamma      * (/ -st*cp,  st*sp, ct /)
    end if
  end function LT_compose_r3_r2_b3

  function LT_compose_r2_r3_b3 (ct, st, cp, sp, beta_gamma) result (L)
    real(kind=default), intent(in) :: ct, st, cp, sp, beta_gamma
    type(lorentz_transformation) :: L
    real(kind=default) :: gamma
    if (beta_gamma==0) then
       L%L(0,0)  = 1
       L%L(1:,0) = 0
       L%L(0,1:) = 0
       L%L(1,1:) = (/  ct*cp,    -sp,     st*cp /)
       L%L(2,1:) = (/  ct*sp,     cp,     st*sp /)
       L%L(3,1:) = (/ -st   , 0._default, ct    /)
    else
       gamma = sqrt(1 + beta_gamma**2)
       L%L(0,0)  = gamma
       L%L(1,0)  = 0
       L%L(2,0)  = 0
       L%L(3,0)  = beta_gamma
       L%L(0,1:) = beta_gamma * (/ -st   , 0._default, ct    /)
       L%L(1,1:) =              (/  ct*cp,    -sp,     st*cp /)
       L%L(2,1:) =              (/  ct*sp,     cp,     st*sp /)
       L%L(3,1:) = gamma      * (/ -st   , 0._default, ct    /)
    end if
  end function LT_compose_r2_r3_b3

  function axis_from_p_r3_r2_b3 (p, cp, sp, ct, st, beta_gamma) result (n)
    type(four_momentum), intent(in) :: p
    real(kind=default), intent(in) :: cp, sp, ct, st, beta_gamma
    type(three_momentum) :: n
    real(kind=default) :: gamma, px, py
    px = cp * p%p(1) - sp * p%p(2)
    py = sp * p%p(1) + cp * p%p(2)
    n%p(1) =  ct * px + st * p%p(3)
    n%p(2) = py
    n%p(3) = -st * px + ct * p%p(3) 
    if (beta_gamma/=0) then
       gamma = sqrt(1 + beta_gamma**2)
       n%p(3) = n%p(3) * gamma + p%p(0) * beta_gamma
    end if
  end function axis_from_p_r3_r2_b3

  function axis_from_p_b3 (p, beta_gamma) result (n)
    type(four_momentum), intent(in) :: p
    real(kind=default), intent(in) :: beta_gamma
    type(three_momentum) :: n
    real(kind=default) :: gamma
    n%p = p%p(1:3)
    if (beta_gamma/=0) then
       gamma = sqrt(1 + beta_gamma**2)
       n%p(3) = n%p(3) * gamma + p%p(0) * beta_gamma
    end if
  end function axis_from_p_b3

  function lambda(m1sq,m2sq,m3sq)
    real(kind=default), intent(in) :: m1sq,m2sq,m3sq
    real(kind=default) :: lambda
    lambda = (m1sq - m2sq - m3sq)**2 - 4*m2sq*m3sq
  end function lambda

end module lorentz
