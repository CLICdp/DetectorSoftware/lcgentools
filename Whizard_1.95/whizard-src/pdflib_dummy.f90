! WHIZARD 1.95 Feb 25 2010
! 
! (C) 1999-2008 by 
!     Wolfgang Kilian <wolfgang.kilian@desy.de>
!     Thorsten Ohl <ohl@physik.uni-wuerzburg.de>
!     Juergen Reuter <juergen.reuter@physik.uni-freiburg.de>
!
! WHIZARD is free software; you can redistribute it and/or modify it
! under the terms of the GNU General Public License as published by 
! the Free Software Foundation; either version 2, or (at your option)
! any later version.
!
! WHIZARD is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! This file has been stripped of most comments.  For documentation, refer
! to the source 'whizard.nw'

module pdflib_interface

  use kinds, only: default !NODEP!
  use diagnostics, only: msg_fatal
  use kinds, only: double !NODEP!
  use user, only: PDF_user_init => PDF_init
  use user, only: PDF_user_strfun_array => PDF_strfun_array

  implicit none
  private

  public :: pdflib_init, pdflib_strfun_array

  integer, parameter :: TQUARK = 6
  integer, parameter :: PROTON = 2212, ANTIPROTON = -PROTON, PHOTON = 22, &
        PION = 211, ANTIPION = -PION 

  integer, parameter, public :: PDFSET_CHAR_LEN = 20

  logical, save :: pdf_invert = .false.
  logical, save :: pdf_user = .false.

contains

  subroutine pdflib_init &
       & (pdg_code_in, ngroup, nset, nfl, lo, QCDL4, top_mass, first)
    integer, intent(in) :: pdg_code_in, ngroup, nset, nfl, lo
    real(kind=default), intent(in) :: QCDL4, top_mass
    logical, intent(in), optional :: first
    integer :: nptype
    select case (pdg_code_in)
    case (PROTON)
       nptype = 1
       pdf_invert = .false.
    case (ANTIPROTON)
       nptype = 1
       pdf_invert = .true.
    case (PHOTON)
       nptype = 3
       pdf_invert = .false.
    case default
       call msg_fatal &
            & (" PDF called for beam different from (anti)proton or photon")
    end select
    if (ngroup < 0) then
       pdf_user = .true.
       call PDF_user_init &
            & (pdg_code_in, abs(ngroup), nset, nfl, lo, &
            &  real(QCDL4,kind=double), real(top_mass,kind=double))
    else
       pdf_user = .false.
       call msg_fatal &
            & (" PDFlib not linked: PDFlib structure functions unavailable.")
    end if
  end subroutine pdflib_init

  subroutine pdflib_strfun_array (x, scale, rho)
    real(kind=default), intent(in) :: x, scale
    real(kind=default), dimension(-TQUARK:TQUARK), intent(out) :: rho
    real(kind=double), dimension(-TQUARK:TQUARK) :: dxpdf
    real(kind=double) :: xd
    xd = min (max (real (x, kind=double), tiny(xd)), 1.0_double - epsilon(xd))
    if (pdf_user) then
       call PDF_user_strfun_array &
            & (real(x,kind=double), real(scale,kind=double), dxpdf)
    else
       dxpdf = 0
    end if
    if (.not.pdf_invert) then
       rho = dxpdf / xd
    else
       rho = dxpdf(TQUARK:-TQUARK:-1) / xd
    end if
  end subroutine pdflib_strfun_array

end module pdflib_interface

