! WHIZARD 1.95 Feb 25 2010
! 
! (C) 1999-2008 by 
!     Wolfgang Kilian <wolfgang.kilian@desy.de>
!     Thorsten Ohl <ohl@physik.uni-wuerzburg.de>
!     Juergen Reuter <juergen.reuter@physik.uni-freiburg.de>
!
! WHIZARD is free software; you can redistribute it and/or modify it
! under the terms of the GNU General Public License as published by 
! the Free Software Foundation; either version 2, or (at your option)
! any later version.
!
! WHIZARD is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! This file has been stripped of most comments.  For documentation, refer
! to the source 'whizard.nw'

module permutations

  implicit none
  private

  type, public :: permutation
!     private
     integer, dimension(:), pointer :: p
     integer :: size
  end type permutation
  type, public :: permutation_list
     private
     type(permutation), dimension(:), pointer :: p
     integer :: length, size
  end type permutation_list

  public :: create, destroy
  public :: assignment(=)
  public :: write
  public :: size, len
  public :: extract
  public :: permute
  public :: permutation_ok
  public :: find_permutation
  public :: make_permutations
  public :: factorial

  interface create
     module procedure permutation_create, permutation_list_create
  end interface
  interface destroy
     module procedure permutation_destroy, permutation_list_destroy
  end interface
  interface assignment (=)
     module procedure permutation_assign
     module procedure permutation_list_assign
  end interface
  interface write
     module procedure permutation_write_unit, permutation_list_write_unit
  end interface
  interface size
     module procedure permutation_size, permutation_list_size
  end interface
  interface len
     module procedure permutation_list_length
  end interface
  interface extract
     module procedure permutation_list_extract
  end interface

contains

  subroutine permutation_create (p, size)
    type(permutation), intent(inout) :: p
    integer, intent(in) :: size
    integer :: i
    allocate (p%p(size))
    do i=1, size
       p%p(i) = i
    end do
    p%size = size
  end subroutine permutation_create

  subroutine permutation_destroy (p)
    type(permutation), intent(inout) :: p
    deallocate (p%p)
    p%size = 0
  end subroutine permutation_destroy

  subroutine permutation_list_create (p, length, size)
    type(permutation_list), intent(inout) :: p
    integer, intent(in) :: length, size
    integer :: i
    allocate (p%p(length))
    do i=1, length
       call create (p%p(i), size)
    end do
    p%length = length
    p%size = size
  end subroutine permutation_list_create

  subroutine permutation_list_destroy (p)
    type(permutation_list), intent(inout) :: p
    integer :: i
    do i=1, p%length
       call destroy (p%p(i))
    end do
    deallocate (p%p)
    p%length = 0
    p%size = 0
  end subroutine permutation_list_destroy

   subroutine permutation_assign (p, p_in)
    type(permutation), intent(inout) :: p
    type(permutation), intent(in) :: p_in
    p%p = p_in%p
  end subroutine permutation_assign

   subroutine permutation_list_assign (pl, p_in)
    type(permutation_list), intent(inout) :: pl
    type(permutation), intent(in) :: p_in
    integer :: i
    do i=1, pl%length
       pl%p(i) = p_in
    end do
  end subroutine permutation_list_assign

  subroutine permutation_write_unit (u, p)
    integer, intent(in) :: u
    type(permutation), intent (in) :: p
    integer :: i
    do i=1, p%size
       if (p%size<10) then
          write (u,"(1x,I1)", advance="no") p%p(i)
       else
          write (u,"(1x,I3)", advance="no") p%p(i)
       end if
    end do
    write (u, *)
  end subroutine permutation_write_unit

  subroutine permutation_list_write_unit (u, p)
    integer, intent(in) :: u
    type(permutation_list), intent(in) :: p
    integer :: i
    write (u,*) "Permutation list:"
    do i=1, len(p)
       call write (u, p%p(i))
    end do
  end subroutine permutation_list_write_unit

  function permutation_size (perm) result (s)
    type(permutation), intent(in) :: perm
    integer :: s
    s = perm%size
  end function permutation_size

  function permutation_list_size (perm) result (s)
    type(permutation_list), intent(in) :: perm
    integer :: s
    s = perm%size
  end function permutation_list_size

  function permutation_list_length (perm) result (l)
    type(permutation_list), intent(in) :: perm
    integer :: l
    l = perm%length
  end function permutation_list_length

  subroutine permutation_list_extract (p, pl, i)
    type(permutation), intent(out) :: p
    type(permutation_list), intent(in) :: pl
    integer, intent(in) :: i
    p%size = size (pl)
    allocate (p%p(p%size))
    if (i>0 .and. i<=len(pl)) then
       p%p = pl%p(i)%p
    else
       p%p = 0
    end if
  end subroutine permutation_list_extract

  function permute (i, p) result (j)
    integer, intent(in) :: i
    type(permutation), intent(in) :: p
    integer :: j
    if (i>0 .and. i<=size(p)) then
       j = p%p(i)
    else
       j = 0
    end if
  end function permute

  function permutation_ok (perm) result (ok)
    type(permutation), intent(in) :: perm
    logical :: ok
    integer :: i
    logical, dimension(perm%size) :: set
    ok = .true.
    set = .false.
    do i=1, perm%size
       ok = (perm%p(i)>0 .and. perm%p(i)<=perm%size)
       if (.not.ok) return
       set(perm%p(i)) = .true.
    end do
    ok = all (set)
  end function permutation_ok

  subroutine find_permutation (perm, a1, a2)
    type(permutation), intent(inout) :: perm
    integer, dimension(:), intent(in) :: a1, a2
    integer :: i, j
    do i=1, size (a1)
       do j=1, size (a2)
          if (a1(i) == a2(j)) then
             perm%p(i) = j
             exit
          end if
          perm%p(i) = 0
       end do
    end do
  end subroutine find_permutation

  subroutine make_permutations (p, code)
    type(permutation_list), intent(inout) :: p
    integer, dimension(:), intent(in) :: code
    logical, dimension(size(code)) :: mask
    logical, dimension(:,:), allocatable :: imask
    integer, dimension(:), allocatable :: n_i
    type(permutation) :: p_init
    type(permutation_list) :: p_tmp
    integer :: psize, i, j, k, n_different, n, nn_k
    psize = size (code)
    mask = .true.
    n_different = 0
    do i=1, psize
       if (mask(i)) then
          n_different = n_different + 1
          mask = mask .and. (code /= code(i))
       end if
    end do
    allocate (imask(psize, n_different), n_i(n_different))
    mask = .true.
    k = 0
    do i=1, psize
       if (mask(i)) then
          k = k + 1
          imask(:,k) = (code == code(i))
          n_i(k) = factorial (count(imask(:,k)))
          mask = mask .and. (code /= code(i))
       end if
    end do
    n = product (n_i)
    call create (p, n, psize)
    call create (p_init, psize)
    p%p(1) = p_init
    nn_k = 1
    do k = 1, n_different
       call create (p_tmp, n_i(k), psize)
       do i = nn_k, 1, -1
          call make_permutations_s (p_tmp, imask(:,k), p%p(i))
          do j = n_i(k), 1, -1
             p%p((i-1)*n_i(k) + j) = p_tmp%p(j)
          end do
       end do
       call destroy (p_tmp)
       nn_k = nn_k * n_i(k)
    end do
    call destroy (p_init)
    deallocate (imask, n_i)
  end subroutine make_permutations

  subroutine make_permutations_s (p_list, mask, p_init)
    type(permutation_list), intent(inout) :: p_list
    logical, dimension(:), intent(in) :: mask
    type(permutation), intent(in) :: p_init
    integer :: plen
    integer :: i, ii, j, fac_i, k, x
    integer, dimension(:), allocatable :: index 
    plen = len (p_list)
    allocate (index(count(mask)))
    ii = 0
    do i = 1, size (p_list)
       if (mask(i)) then
          ii = ii + 1
          index(ii) = i
       end if
    end do
    p_list = p_init
    ii = 0
    fac_i = 1
    do i = 1, size (p_list)
       if (mask(i)) then
          ii = ii + 1
          fac_i = fac_i * ii
          x = permute (i, p_init)
          do j = 1, plen
             k = ii - mod (((j-1)*fac_i)/plen, ii)
             call insert (p_list%p(j), x, k, ii, index)
          end do
       end if
    end do
    deallocate (index)
  contains
    subroutine insert (p, x, k, n, index)
      type(permutation), intent(inout) :: p
      integer, intent(in) :: x, k, n
      integer, dimension(:), intent(in) :: index
      integer :: i
      do i = n, k+1, -1
         p%p(index(i)) = p%p(index(i-1))
      end do
      p%p(index(k)) = x
    end subroutine insert
  end subroutine make_permutations_s

  function factorial (n) result (f)
    integer, intent(in) :: n
    integer :: f
    integer :: i
    f = 1
    do i=2, abs(n)
       f = f*i
    end do
  end function factorial


end module permutations
