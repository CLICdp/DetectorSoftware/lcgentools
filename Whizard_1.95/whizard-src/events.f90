! WHIZARD 1.95 Feb 25 2010
! 
! (C) 1999-2008 by 
!     Wolfgang Kilian <wolfgang.kilian@desy.de>
!     Thorsten Ohl <ohl@physik.uni-wuerzburg.de>
!     Juergen Reuter <juergen.reuter@physik.uni-freiburg.de>
!
! WHIZARD is free software; you can redistribute it and/or modify it
! under the terms of the GNU General Public License as published by 
! the Free Software Foundation; either version 2, or (at your option)
! any later version.
!
! WHIZARD is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! This file has been stripped of most comments.  For documentation, refer
! to the source 'whizard.nw'

module events

  use kinds, only: default !NODEP!
  use kinds, only: i64 !NODEP!
  use limits, only: BUFFER_SIZE, PROCESS_ID_LEN
  use limits, only: STDHEP_BYTES_PER_EVENT, STDHEP_BYTES_PER_ENTRY
  use diagnostics, only: msg_buffer, msg_message, msg_fatal, msg_bug
  use constants
  use lorentz
  use parameters, only: parameter_set !NODEP!
!  use polarization
  use beams, only: beam_sign, beam_data_t
  use particles
  use density_vectors, only: state_density_t, create, destroy
  use density_vectors, only: read, write
  use density_vectors, only: read_dimensions_raw, write_dimensions_raw
  use partons, only: parton_state
  use jetset_interface !NODEP!
  use stdhep_interface !NODEP!
  use showers, only: shower_t, shower_has_particles, shower_final
  use showers, only: shower_write

  implicit none
  private

  integer, parameter, public :: SCREEN_FMT=0, HEPEVT_FMT=1, RAW_FMT=-1
  integer, parameter, public :: SHORT_FMT=2, LONG_FMT=3
  integer, parameter, public :: ATHENA_FMT=4, LHA_FMT=5, LHEF_FMT=6
  integer, parameter, public :: JETSET_FMT1=11, JETSET_FMT2=12, JETSET_FMT3=13
  integer, parameter, public :: STDHEP_FMT=20, STDHEP_UP_FMT=21, STDHEP_FMT4=24

  type, public :: event
     character(len=PROCESS_ID_LEN) :: process_id
     integer :: process_index
     integer(i64) :: count, count_internal, count_file, count_bytes
     integer :: n_in, n_out, n_tot
     logical :: scattering
     real(kind=default) :: sqrts, sqrts_hat, s_hat
     real(kind=default) :: flux_factor, phase_space_factor, energy_scale
     type(particle), dimension(:), allocatable :: beam_particle
     type(state_density_t), pointer :: rho_in => null ()
     logical :: rho_in_allocated
     type(particle), dimension(:), pointer :: beam_remnant
     integer :: n_beam_remnants
     logical :: beam_remnants_allocated
     real(kind=default), dimension(:), allocatable :: x_parton
     type(particle), dimension(:), pointer :: prt => null ()
     logical :: keep_initials
     real(kind=default) :: mc_function_value, mc_function_ratio
     real(kind=default), dimension(:,:,:), pointer :: rho_out
     integer :: n_col_out, n_hel_out, n_flv_out
     integer, dimension(:), pointer :: color_flow => null()
     integer, dimension(:), pointer :: anticolor_flow => null ()
     integer, dimension(:), pointer :: hel_out => null()
     logical :: cm_is_lab_frame, beam_recoil, conserve_momentum
     type(lorentz_transformation) :: L0
     integer, dimension(:), pointer :: shower_index => null ()
     type(shower_t), dimension(:), pointer :: shower => null ()
  end type event

  public :: create, destroy
  public :: destroy_rho_in
  public :: event_reset_shower
  public :: read, write
  public :: event_n_entries
  public :: write_process, write_process_tex_form
  public :: event_set_beams
  public :: event_in_state_from_partons
  public :: set_out_state
  public :: get
!  public :: merge

  interface create
     module procedure event_create
  end interface
  interface destroy
     module procedure event_destroy
  end interface
  interface write
     module procedure event_write_unit
     module procedure event_write_formatted
  end interface
  interface read
     module procedure event_read_unit
  end interface
  interface write_process
     module procedure write_process_unit
  end interface
  interface write_process_tex_form
     module procedure write_process_tex_form_unit
  end interface
  interface set_out_state
     module procedure event_out_state_codes
     module procedure event_out_state_rho
  end interface
  interface get
     module procedure event_get_particle
  end interface
!  interface merge
!     module procedure event_apply_decay
!  end interface

contains

  subroutine event_create (evt, n_in, n_out, n_col_out, n_hel_out, n_flv_out)
    type(event), intent(out) :: evt
    integer, intent(in) :: n_in, n_out
    integer, intent(in) :: n_col_out, n_hel_out, n_flv_out
    !allocate(evt)
    evt%process_id = " "
    evt%process_index = 0
    evt%count = 0
    evt%count_internal = 0
    evt%count_file = 0
    evt%count_bytes = 0
    evt%scattering = (n_in==2)
    evt%n_in = n_in
    evt%n_out = n_out
    evt%n_tot = n_in + n_out
    if (evt%scattering) then
       allocate(evt%prt(-n_in:n_out))
    else
       allocate(evt%prt(0:n_out))
    end if
    evt%prt = particle_null
    allocate (evt%beam_particle(n_in), evt%x_parton(n_in))
    evt%beam_particle = particle_null
    evt%x_parton = 0
    nullify (evt%rho_in)
    evt%rho_in_allocated = .false.
    nullify (evt%beam_remnant)
    evt%beam_remnants_allocated = .false.
    evt%keep_initials = .false.
    evt%n_beam_remnants = 0
    evt%n_col_out = n_col_out
    evt%n_hel_out = n_hel_out
    evt%n_flv_out = n_flv_out
    allocate (evt%rho_out(n_col_out, n_hel_out, n_flv_out))
    evt%rho_out = 0
    evt%mc_function_value = 0
    evt%mc_function_ratio = 0
    allocate(evt%hel_out(n_out))
    evt%hel_out = 0
    allocate(evt%color_flow(evt%n_tot), evt%anticolor_flow(evt%n_tot))
    evt%color_flow = 0
    evt%anticolor_flow = 0
    evt%cm_is_lab_frame = .true.
    evt%beam_recoil = .false.
    evt%conserve_momentum = .false.
    evt%L0 = unit_lorentz_transformation
  end subroutine event_create

  subroutine event_destroy (evt)
    type(event), intent(inout) :: evt
    deallocate (evt%color_flow, evt%anticolor_flow)
    deallocate (evt%hel_out)
    deallocate (evt%rho_out)
    call destroy_rho_in (evt)
    deallocate (evt%x_parton)
    deallocate (evt%beam_particle)
    deallocate (evt%prt)
    call event_reset_shower (evt)
    !deallocate(evt)
  end subroutine event_destroy

  subroutine destroy_rho_in (evt)
    type(event), intent(inout) :: evt
    if (evt%rho_in_allocated) then
       call destroy (evt%rho_in)
       deallocate (evt%rho_in)
       evt%rho_in_allocated = .false.
    end if
    nullify (evt%rho_in)
    if (evt%beam_remnants_allocated) then
       deallocate (evt%beam_remnant)
       evt%beam_remnants_allocated = .false.
       evt%n_beam_remnants = 0
    end if
    nullify (evt%beam_remnant)
  end subroutine destroy_rho_in

  subroutine event_reset_shower (evt)
    type(event), intent(inout) :: evt
    integer :: i
    if (associated (evt%shower_index))  deallocate (evt%shower_index)
    if (associated (evt%shower)) then
       do i = 1, size (evt%shower)
          call shower_final (evt%shower(i))
       end do
       deallocate (evt%shower)
    end if
  end subroutine event_reset_shower

  function event_n_entries (evt) result (n)
    type(event), intent(in) :: evt
    integer :: n
    if (evt%keep_initials) then
       n = evt%n_in + evt%n_tot + evt%n_beam_remnants
    else
       n = evt%n_out + evt%n_beam_remnants
    end if
  end function event_n_entries

  subroutine event_write_unit (u, evt)
    integer, intent(in) :: u
    type(event), intent(inout) :: evt
    call write (u, evt, .false., 1._default, SCREEN_FMT)
  end subroutine event_write_unit

  subroutine event_write_formatted &
       & (u, evt, generic_polarization, weight, fmt, probability, channel)
    integer, intent(in) :: u
    type(event), intent(inout) :: evt
    logical :: generic_polarization
    real(kind=default), intent(in) :: weight
    integer, intent(in) :: fmt
    real(kind=default), intent(in), optional :: probability
    integer, intent(in), optional :: channel
    integer :: mode, i, j, k, c, f
    real(kind=default), dimension(0:3) :: p
    mode = fmt
    select case(mode)
    case(SCREEN_FMT)
       write(u,*) '============================================================================'
       write(u,*) 'Event listing:'
       write(u,*) 'process ID      = ', evt%process_index, &
            & "("//trim(evt%process_id)//")"
       write(u,*) '----------------------------------------------------------------------------'
       write(u,*) 'count           = ', evt%count
       write(u,*) 'count_internal  = ', evt%count_internal
       write(u,*) 'count_file      = ', evt%count_file
       write(u,*) 'count_bytes     = ', evt%count_bytes
       write(u,*) 'n_in, n_out, n_tot  = ', evt%n_in, evt%n_out, evt%n_tot
       write(u,*) 'scattering          = ', evt%scattering
       write(u,*) 'sqrts               =', evt%sqrts
       write(u,*) 'sqrts_hat           =', evt%sqrts_hat
       write(u,*) 's_hat               =', evt%s_hat
       if (allocated (evt%x_parton)) then
          write(u,*) 'x_parton            =', evt%x_parton
       else
          write(u,*) 'x_parton = [not allocated]'
       end if
       write(u,*) 'flux factor         =', evt%flux_factor
       write(u,*) 'phs factor          =', evt%phase_space_factor
       write(u,*) 'energy_scale        =', evt%energy_scale
       write(u,*) 'cm_is_lab_frame     =', evt%cm_is_lab_frame
       write(u,*) 'beam_recoil         =', evt%beam_recoil
       write(u,*) 'conserve_momentum   =', evt%conserve_momentum
       call write (u, evt%L0)
       write(u,*)
       write(u,*) 'mc_function value   =', evt%mc_function_value
       write(u,*) 'mc_function ratio   =', evt%mc_function_ratio
       write(u,*) 'weight              =', weight
       write(u,*)
       write(u,*) 'Beam particles:'
       if (allocated (evt%beam_particle)) then
          do i=1, size (evt%beam_particle)
             call write (u, evt%beam_particle(i))
          end do
       else
          write(u,*) '  [not allocated]'
       end if
       write(u,*)
       write(u,*) 'beam_remnants_allocated =', evt%beam_remnants_allocated
       write(u,*) 'n_beam_remnants         =', evt%n_beam_remnants
       write(u,*) "Beam remnants:"
       if (evt%beam_remnants_allocated) then
          if (size (evt%beam_remnant) > 0) then
             do i=1, size (evt%beam_remnant)
                call write (u, evt%beam_remnant(i))
             end do
          else
             write(u,*) '  [none]'
          end if
       else
          write(u,*) '  [not allocated]'
       end if
       write(u,*)
       write(u,*) 'In-state particles:'
       if (associated (evt%prt)) then
          write(u,*) 'Particle 0:'
          call write(u,evt%prt(0))
          if (evt%scattering) then
             write(u,*) 'Particles -1,-2:'
             do i=1,2
                call write(u,evt%prt(-i))
             end do
          end if
       else
          write(u,*) '  [not allocated]'
       end if
       write(u,*)
       write(u,*) 'In-state probability density:'
       write(u,*) 'rho_in_allocated =', evt%rho_in_allocated
       if (associated(evt%rho_in)) then
          call write (u, evt%rho_in)
       else
          write(u,*) '  [not allocated]'
       end if
       write(u,*)
       write(u,*) 'Out-state particles:'
       write(u,*) 'n_flv_out =', evt%n_flv_out
       if (associated (evt%prt)) then
          do i=1, ubound (evt%prt, 1)
             if (associated (evt%shower_index)) then
                if (evt%shower_index(i) /= 0) &
                     write (u,"(1x,A)",advance="no") "(Showered)"
             end if
             call write(u,evt%prt(i))
          end do
       else
          write(u,*) '  [not allocated]'
       end if
       write(u,*)
       write(u,*) 'Color flow:'
       write(u,*) 'n_col_out =', evt%n_col_out
       if (associated (evt%color_flow) .and. associated (evt%anticolor_flow)) then
          write(u,*) 'Color flow:     ', evt%anticolor_flow(:evt%n_in), &
               & "->", evt%color_flow(evt%n_in+1:)
          write(u,*) 'Anticolor flow: ', evt%color_flow(:evt%n_in), &
               & "<-", evt%anticolor_flow(evt%n_in+1:)
       else
          write(u,*) 'Color flow:       [not completely allocated]'
       end if
       write(u,*)
       write(u,*) 'Helicity state:'
       write(u,*) 'n_hel_out =', evt%n_hel_out
       if (associated (evt%hel_out)) then
          write(u,*) 'Helicity vector:', evt%hel_out
       else
          write(u,*) 'Helicity vector:  [not allocated]'
       end if
       write(u,*)
       write(u,*) 'Out-state density:'
       if (associated (evt%rho_out)) then
          if (any (evt%rho_out /= 0)) then
             do f = 1, evt%n_flv_out
                if (evt%n_flv_out > 1)  write (u,*) "Flavor state:", f
                if (any (evt%rho_out(:,:,f) /= 0)) then
                   do c = 1, evt%n_col_out
                      if (evt%n_col_out > 1)  write (u,*) "Color state:", c
                      if (any (evt%rho_out(c,:,f) /= 0)) then
                         write (u, *)  evt%rho_out(c,:,f)
                      else
                         write (u, *)  0
                      end if
                   end do
                else
                   write (u,*)  0
                end if
             end do
          else
             write (u,*) 0
          end if
       else
          write(u,*) '  [not allocated]'
       end if
       write(u,*) '----------------------------------------------------------------------------'
       write(u,*) 'Parton showers:'
       if (associated (evt%shower)) then
          do i = 1, size (evt%shower)
             if (shower_has_particles (evt%shower(i))) then
                write(u,*) '----------------------------------------------------------------------------'
                write(u,"(1x,A,I2)") 'Particle #', i
                call shower_write (evt%shower(i), u, in_cm_system = .false.)
             end if
          end do
       end if
       write(u,*) '============================================================================'
    case(HEPEVT_FMT)
       write(u,15) event_n_entries (evt), evt%n_out, evt%n_beam_remnants, weight
       if (evt%keep_initials) then
          do i=1, evt%n_in
             p = array(particle_four_momentum(evt%beam_particle(i)))
             write (u,13) 2, particle_code(evt%beam_particle(i)), &
                  & (0, j=1,2), &
                  & evt%n_in+1, 2*evt%n_in+evt%n_beam_remnants, 0
             write (u,12) p(1:3), p(0), particle_mass(evt%beam_particle(i))
             write (u,12) (0._default, j=1,5)
          end do
          do i=1, evt%n_in
             p = array(particle_four_momentum(evt%prt(-i)))
             write (u,13) 2, particle_code(evt%prt(-i)), &
                  & evt%n_in+1, 2*evt%n_in, &
                  & 2*evt%n_in+evt%n_beam_remnants, &
                  & evt%n_in+evt%n_tot+evt%n_beam_remnants, 0
             write (u,12) p(1:3), p(0), particle_mass(evt%prt(-i))
             write (u,12) (0._default, j=1,5)
          end do
       end if
       do i=1, evt%n_beam_remnants
          p = array(particle_four_momentum(evt%beam_remnant(i)))
          write (u,13) 1, particle_code(evt%beam_remnant(i)), &
               & evt%n_in+1, 2*evt%n_in, (0, j=1,2), 0
          write (u,12) p(1:3), p(0), particle_mass(evt%beam_remnant(i))
          write (u,12) (0._default, j=1,5)
       end do
       do i=1, evt%n_out
          p = array(particle_four_momentum(evt%prt(i)))
          write (u,13) 1, particle_code(evt%prt(i)), &
               & evt%n_in+1, 2*evt%n_in, (0, j=1,2), evt%hel_out(i)
          write (u,12) p(1:3), p(0), particle_mass(evt%prt(i))
          write (u,12) (0._default, j=1,5)
       end do
       evt%count_bytes = evt%count_bytes + 34 + event_n_entries (evt) * 216
    case(SHORT_FMT)
       write(u,15) evt%n_out + evt%n_beam_remnants, evt%n_out, &
            & evt%n_beam_remnants, weight
       do i=1, evt%n_beam_remnants
          p = array(particle_four_momentum(evt%beam_remnant(i)))
          write (u,13) particle_code(evt%beam_remnant(i)), 0
          write (u,12) p(1:3), p(0), particle_mass(evt%beam_remnant(i))
       end do
       do i=1, evt%n_out
          p = array(particle_four_momentum(evt%prt(i)))
          write (u,13) particle_code(evt%prt(i)), evt%hel_out(i)
          write (u,12) p(1:3), p(0), particle_mass(evt%prt(i))
       end do
       evt%count_bytes = evt%count_bytes + 34 + (evt%n_out+evt%n_beam_remnants) * 105
    case(LONG_FMT)
       write (u,15) evt%n_out + evt%n_beam_remnants, evt%n_out, &
            & evt%n_beam_remnants, weight
       do i=1, evt%n_beam_remnants
          p = array(particle_four_momentum(evt%beam_remnant(i)))
          write (u,13) particle_code(evt%beam_remnant(i)), 0
          write (u,12) p(1:3), p(0), particle_mass(evt%beam_remnant(i))
       end do
       do i=1, evt%n_out
          p = array(particle_four_momentum(evt%prt(i)))
          write (u,13) particle_code(evt%prt(i)), evt%hel_out(i)
          write (u,12) p(1:3), p(0), particle_mass(evt%prt(i))
       end do
       write (u, 14) evt%mc_function_value, evt%mc_function_ratio
       evt%count_bytes = evt%count_bytes + 69 + (evt%n_out+evt%n_beam_remnants) * 105
    case(ATHENA_FMT)
       write(u,11) evt%count, event_n_entries (evt)
       k = 0
       if (evt%keep_initials) then
          do i=1, evt%n_in
             k = k + 1
             p = array(particle_four_momentum(evt%beam_particle(i)))
             write (u,13) k, 2, particle_code(evt%beam_particle(i)), &
                  & (0, j=1,2), &
                  & evt%n_in+1, 2*evt%n_in+evt%n_beam_remnants
             write (u,12) p(1:3), p(0), particle_mass(evt%beam_particle(i))
             write (u,12) (0._default, j=1,4)
          end do
          do i=1, evt%n_in
             k = k + 1
             p = array(particle_four_momentum(evt%prt(-i)))
             write (u,13) k, 2, particle_code(evt%prt(-i)), &
                  & evt%n_in+1, 2*evt%n_in, &
                  & 2*evt%n_in+evt%n_beam_remnants, &
                  & evt%n_in+evt%n_tot+evt%n_beam_remnants
             write (u,12) p(1:3), p(0), particle_mass(evt%prt(-i))
             write (u,12) (0._default, j=1,4)
          end do
       end if
       do i=1, evt%n_beam_remnants
          k = k + 1
          p = array(particle_four_momentum(evt%beam_remnant(i)))
          write (u,13) k, 1, particle_code(evt%beam_remnant(i)), &
               & evt%n_in+1, 2*evt%n_in, (0, j=1,2)
          write (u,12) p(1:3), p(0), particle_mass(evt%beam_remnant(i))
          write (u,12) (0._default, j=1,4)
       end do
       do i=1, evt%n_out
          k = k + 1
          p = array(particle_four_momentum(evt%prt(i)))
          write (u,13) k, 1, particle_code(evt%prt(i)), &
               & evt%n_in+1, 2*evt%n_in, (0, j=1,2)
          write (u,12) p(1:3), p(0), particle_mass(evt%prt(i))
          write (u,12) (0._default, j=1,4)
       end do
       !!!! revise this:
       evt%count_bytes = evt%count_bytes + 34 + event_n_entries (evt) * 216
    case(LHA_FMT)
       call msg_message (" To activate LHA output format, set fragment = T") 
       call msg_fatal (" Invalid event output format")
    case(RAW_FMT)
       write(u) evt%process_id, evt%process_index, evt%n_out, evt%n_beam_remnants
       write(u) evt%sqrts_hat, evt%energy_scale
       j=0;  if (evt%scattering) j=2
       do i=0,j
          write(u)  particle_code(evt%prt(-i)), &
               & array(particle_four_momentum(evt%prt(-i))), &
               & particle_mass(evt%prt(-i)), &
               & 0
          if (i==0) then
             if (evt%scattering) then
                write(u) 0, 0
             else
                write(u) evt%color_flow(1), evt%anticolor_flow(1)
             end if
          else
             write(u) evt%color_flow(i), evt%anticolor_flow(i)
          end if
       end do
       do i=1, evt%n_out
          write(u) particle_code(evt%prt(i)), &
               & array(particle_four_momentum(evt%prt(i))), &
               & particle_mass(evt%prt(i)), &
               & evt%hel_out(i)
          write(u) evt%color_flow(evt%n_in+i), evt%anticolor_flow(evt%n_in+i)
       end do
       do i=1, evt%n_beam_remnants
          write(u) particle_code(evt%beam_remnant(i)), &
               & array(particle_four_momentum(evt%beam_remnant(i))), &
               & particle_mass(evt%beam_remnant(i)), &
               & 0
          write(u) 0, 0
       end do
       if (present(probability).and.present(channel)) then
          write(u) weight, channel, probability
       else
          write(u) weight
       end if
       if (associated (evt%rho_in)) then
          call write_dimensions_raw (u, evt%rho_in)
          call write (u, evt%rho_in, raw=.true., write_matrix=generic_polarization)
       else
          call msg_bug (" Writing event: in-state density not allocated.")
       end if
       write(u)  evt%flux_factor, evt%phase_space_factor, evt%mc_function_value
    case(STDHEP_FMT)
       call stdhep_write (STDHEP_HEPEVT)
       evt%count_bytes = evt%count_bytes + STDHEP_BYTES_PER_EVENT &
            & + event_n_entries (evt) * STDHEP_BYTES_PER_ENTRY
    case(STDHEP_UP_FMT)
       call stdhep_write (STDHEP_HEPEUP)
       evt%count_bytes = evt%count_bytes + STDHEP_BYTES_PER_EVENT &
            & + event_n_entries (evt) * STDHEP_BYTES_PER_ENTRY
    case(STDHEP_FMT4)
       call stdhep_write (STDHEP_HEPEV4)
       evt%count_bytes = evt%count_bytes + STDHEP_BYTES_PER_EVENT &
            & + event_n_entries (evt) * STDHEP_BYTES_PER_ENTRY
    case (JETSET_FMT1, JETSET_FMT2, JETSET_FMT3)
       
    case default
       call msg_fatal (" Invalid event output format")
    end select
  11 format(10I5)
  12 format(10F17.10)
  13 format(I9,I9,5I5)
  14 format(ES17.10,F17.10)
  15 format(3I5,1X,F17.10)
  16 format(2(1x,I5),1x,F17.10,3(1x,F13.6))
  17 format(500(1x,I5))
  18 format(1x,I5,4(1x,F17.10))
  end subroutine event_write_formatted

  subroutine event_read_unit &
       & (u, evt, generic_polarization, compatibility_142, &
       &  weight, &
       &  fmt, probability, channel, iostat)
    integer, intent(in) :: u
    type(event), intent(inout) :: evt
    logical, intent(in) :: generic_polarization, compatibility_142
    real(kind=default), intent(out) :: weight
    integer, intent(in), optional :: fmt
    real(kind=default), intent(out), optional :: probability
    integer, intent(out), optional :: channel, iostat
    integer :: mode, i, j, code, idummy
    real(kind=default) :: mass
    real(kind=default), dimension(0:3) :: p
    mode = SCREEN_FMT;  if (present(fmt)) mode = fmt
    call destroy_rho_in (evt)
    select case(mode)
    case(RAW_FMT)
       iostat = 0
       read(u, iostat=iostat) &
            & evt%process_id, evt%process_index, evt%n_out, evt%n_beam_remnants
       if (iostat/=0) return
       if (compatibility_142) then
          evt%sqrts_hat    = evt%sqrts
          evt%energy_scale = evt%sqrts
       else
          read(u, iostat=iostat) evt%sqrts_hat, evt%energy_scale
          if (iostat/=0) return
       end if
       evt%s_hat = evt%sqrts_hat**2
       evt%n_tot = evt%n_in + evt%n_out
       j=0;  if (evt%scattering) j=2
       do i=0,j
          read(u, iostat=iostat) code, p, mass
          if (iostat/=0) return
          evt%prt(-i) = particle_new(code, mass, &
               & four_momentum_moving(p(0), three_momentum_moving(p(1:3))))
          if (i==0) then
             if (evt%scattering) then
                read(u, iostat=iostat) idummy, idummy
             else
                read(u, iostat=iostat) evt%color_flow(1), evt%anticolor_flow(1)
             end if
          else
             read(u, iostat=iostat) evt%color_flow(i), evt%anticolor_flow(i)
          end if
       end do
       do i=1, evt%n_out
          read(u, iostat=iostat) code, p, mass, evt%hel_out(i)
          evt%prt(i) = particle_new(code, mass, &
               & four_momentum_moving(p(0), three_momentum_moving(p(1:3))))
          read(u, iostat=iostat) &
               & evt%color_flow(evt%n_in+i), evt%anticolor_flow(evt%n_in+i)
       end do
       allocate (evt%beam_remnant(evt%n_beam_remnants))
       evt%beam_remnants_allocated = .true.
       do i=1, evt%n_beam_remnants
          read(u, iostat=iostat) code, p, mass
          evt%beam_remnant(i) = particle_new(code, mass, &
               & four_momentum_moving(p(0), three_momentum_moving(p(1:3))))
          read(u, iostat=iostat) idummy, idummy
       end do
       if (present(probability).and.present(channel)) then
          read(u, iostat=iostat) weight, channel, probability
       else
          read(u, iostat=iostat) weight
       end if
       if (associated (evt%rho_in)) &
            & call msg_fatal (" Reading event: In-state density not properly deallocated.")
       allocate (evt%rho_in)
       evt%rho_in_allocated = .true.
       call read_dimensions_raw (u, evt%rho_in, iostat=iostat)
       call create (evt%rho_in, diagonal=.not.generic_polarization)
       call read (u, evt%rho_in, raw=.true., read_matrix=generic_polarization, &
            & iostat=iostat)
       read(u, iostat=iostat) &
            & evt%flux_factor, evt%phase_space_factor, evt%mc_function_value
       evt%mc_function_ratio = 1
    case default
       call msg_fatal (" Can read events in raw format only")
    end select
  11 format(10I5)
  12 format(10F17.10)
  13 format(I9,I9,5I5)
  14 format(ES17.10,F17.10)
  15 format(3I5,1X,F17.10)
  16 format(2(1x,I5),1x,F17.10,3(1x,F13.6))
  17 format(500(1x,I5))
  18 format(1x,I5,4(1x,F17.10))
  end subroutine event_read_unit

  subroutine write_process_unit (u, process_id, code, n_in, n_out)
    integer, intent(in) :: u
    character(len=*), intent(in) :: process_id
    integer, dimension(:,:), intent(in) :: code
    integer, intent(in) :: n_in, n_out
    integer :: i, j, pos
    character(len=BUFFER_SIZE) :: tmp_buffer
    call msg_message (" Process " // trim(process_id) // ":", unit=u)
    do j = 1, size (code, 2)
       tmp_buffer = " "
       pos = 1
       do i=1, n_in
          call write_prt (pos, code(i,j), ibset(0,n_in+n_out-i))
       end do
       msg_buffer (pos+1:) = " ->"
       tmp_buffer (pos+1:) = " ->"
       pos = pos + 3
       do i=1, n_out
          call write_prt (pos, code(n_in+i,j), ibset(0,i-1))
       end do
       call msg_message (unit=u)
    end do
    msg_buffer = tmp_buffer
    call msg_message (unit=u)
  contains
    subroutine write_prt (pos, code, bincode)
      integer, intent(inout) :: pos
      integer, intent(in) :: code, bincode
      character(len=9) :: fmt
      integer :: len
      len = max (len_trim(particle_name(code)), 3)
      write(fmt, '(A5,I2,A2)') "(1x,A", len, ")"
      write(msg_buffer(pos+1:), fmt) trim(particle_name(code))
      write(fmt, '(A5,I2,A2)') "(1x,I", len, ")"
      write(tmp_buffer(pos+1:), fmt) bincode
      pos = pos + len + 1
    end subroutine write_prt
  end subroutine write_process_unit

  subroutine write_process_tex_form_unit (u, process_id, code, n_in, n_out)
    integer, intent(in) :: u
    character(len=*), intent(in) :: process_id
    integer, dimension(:,:), intent(in) :: code
    integer, intent(in) :: n_in, n_out
    integer :: i,j
    write(u, '(A)') "\textbf{Process:} "
    write(u, '(2x,A)', advance='no') "\verb|" &
         & // trim(process_id) // "| ("
    do j = 1, size (code, 2)
       if (j > 1)  write(u, '(A)', advance='no') ", "
       write(u, '(A)', advance='no') "$"
       do i=1, n_in + n_out
          write(u, '(1x,A)', advance='no') trim(particle_tex_form(code(i,j)))
          if (i==n_in) write(u, '(1x,A)', advance='no') "\to"
       end do
       write (u, '(A)')  "$)"
    end do
  end subroutine write_process_tex_form_unit

  subroutine event_set_beams (evt, beam, sqrts, par, ok)
    type(event), intent(inout) :: evt
    type(beam_data_t), dimension(:), intent(in) :: beam
    real(kind=default), intent(in) :: sqrts
    type(parameter_set), intent(in) :: par
    logical, intent(out), optional :: ok
    real(kind=default), dimension(evt%n_in) :: mass, E
    real(kind=default) :: pp, lda
    integer :: i
    if (present (ok))  ok = .true.
    evt%sqrts = sqrts
    mass = particle_mass (beam%particle_code, par)
    if (evt%scattering) then
       lda = lambda (evt%sqrts**2, mass(1)**2, mass(2)**2)
       if (lda <= 0) then
          if (present (ok))  ok = .false.
          return
       end if
       pp = sqrt (lda)
       do i = 1, 2
          E(i) = (sqrts + beam_sign(i) * (mass(1)**2-mass(2)**2)/sqrts) / 2
          evt%beam_particle(i) = particle_new ( &
               & code = beam(i)%particle_code, &
               & mass = mass(i), &
               & p = four_momentum_moving ( E(i),  &
               &                            beam_sign(i) * pp/(2*sqrts), 3) )
       end do
       evt%flux_factor = conv * twopi4 / (2*pp)
    else
       evt%beam_particle(1) = particle_new ( &
            & code = beam(1)%particle_code, &
            & mass = mass(1), &
            & p = four_momentum_at_rest (mass(1)))
       evt%flux_factor = twopi4 / (2*mass(1))
    end if
  end subroutine event_set_beams

  subroutine event_in_state_from_partons &
       & (evt, ps, par, keep_initials, keep_beam_remnants, ok)
    type(event), intent(inout) :: evt
    type(parton_state), intent(in) :: ps
    type(parameter_set), intent(in) :: par
    logical, intent(in) :: keep_initials, keep_beam_remnants
    logical, intent(out) :: ok
    integer :: i
    real(kind=default) :: pp, rap, sqrts, sh, sqsh, lda
    real(kind=default), dimension(2) :: mass, E
    type(four_momentum), dimension(2) :: p_parton
    type(four_momentum) :: p_hat
    ok = .true.
    sqrts = evt%sqrts
    evt%x_parton = ps%x
    if (ps%fixed_energy) then
       sh = sqrts**2
       sqsh = sqrts
    else
       sh   = sqrts**2 * product (evt%x_parton)
       sqsh = sqrt (sh)
    end if
    evt%s_hat     = sh
    evt%sqrts_hat = sqsh
    evt%energy_scale = ps%energy_scale
    evt%rho_in => ps%rho
    evt%keep_initials = keep_initials
    if (ps%fixed_energy .or. all (ps%x == 1)) then
       evt%cm_is_lab_frame = ps%cm_frame
       evt%L0 = ps%L_cm
    else if (ps%beam_recoil) then
       evt%cm_is_lab_frame = .false.
    else if (all(ps%x>0)) then
       evt%cm_is_lab_frame = .false.
       rap = log (ps%x(1) / ps%x(2)) / 2
       evt%L0 = boost (sinh(rap), 3)
       if (.not. ps%cm_frame)  evt%L0 = ps%L_cm * evt%L0
    else
       ok = .false.; return
    end if
    if (evt%scattering) then
       if (keep_beam_remnants) then
          evt%n_beam_remnants = ps%n_beam_remnants
          evt%beam_remnant => ps%beam_remnant
          if (.not. ps%cm_frame) then
             do i = 1, evt%n_beam_remnants
                evt%beam_remnant(i) = ps%L_cm * evt%beam_remnant(i)
             end do
          end if
       else
          evt%n_beam_remnants = 0
          nullify (evt%beam_remnant)
       end if
       if (ps%fixed_energy) then
          evt%prt((/-1,-2/)) = evt%beam_particle     ! [no strfun]
          evt%prt(0) = particle_new (COMPOSITE, sqsh)
       else                                          ! [with strfun]
          mass(1:2) = particle_mass (ps%code, par)
          if (ps%beam_recoil) then                ! [recoil pt]
             if (ps%conserve_momentum) then       ! [keep momentum balance]
                do i = 1, 2
                   p_parton(i) = particle_four_momentum (evt%beam_particle(i)) &
                        &     - ps%recoil_momentum(i)
                end do
             else                                 ! [keep energy balance]
                do i = 1, 2
                   p_parton(i) = &
                        & (ps%x(i) + energy (ps%recoil_momentum(i)) / &
                        &            particle_energy (evt%beam_particle(i))) &
                        & * particle_four_momentum (evt%beam_particle(i)) &
                        & - ps%recoil_momentum(i)
                end do
             end if
             p_hat = p_parton(1) + p_parton(2)
             sh = p_hat**2
             sqsh = sqrt(max(0._default,sh))
             evt%s_hat = sh
             evt%sqrts_hat = sqsh
             evt%L0 = transformation (3, p_parton, sqsh)
             if (.not. ps%cm_frame)  evt%L0 = ps%L_cm * evt%L0
          end if
          lda = lambda (evt%s_hat, mass(1)**2, mass(2)**2)
          if (lda <= 0) then
             ok = .false.; return
          end if
          pp = sqrt (lda)
          E(1) = (sqsh + (mass(1)**2-mass(2)**2)/sqsh) / 2
          E(2) = (sqsh - (mass(1)**2-mass(2)**2)/sqsh) / 2
          evt%prt(-1) = particle_new ( &
               & code = ps%code(1), &
               & mass = mass(1), &
               & p = four_momentum_moving ( E(1),  pp/(2*sqsh), 3) )
          evt%prt(-2) = particle_new ( &
               & code = ps%code(2), &
               & mass = mass(2), &
               & p = four_momentum_moving ( E(2), -pp/(2*sqsh), 3) )
          evt%prt(0) = particle_new (COMPOSITE, sqsh)
          evt%flux_factor = conv * twopi4 / (2*pp)
       end if
    else                                                ! [particle decay]
       evt%prt(0) = evt%beam_particle(1)
    end if
    if (.not. ps%cm_frame) then
       do i = 1, ps%n_in
          evt%beam_particle(i) = ps%L_cm * evt%beam_particle(i)
       end do
    end if
    evt%beam_recoil = ps%beam_recoil
    evt%conserve_momentum = ps%conserve_momentum
    if (particle_mass (evt%prt(0)) <= 0) then
       ok = .false.;  return
    end if
  end subroutine event_in_state_from_partons

  subroutine event_out_state_codes(evt, code, par)
    type(event), intent(inout) :: evt
    integer, dimension(:), intent(in) :: code
    type(parameter_set), intent(in) :: par
    integer :: i
    evt%n_out = size (code)
    evt%n_tot = evt%n_in + evt%n_out
    do i=1, evt%n_out
       evt%prt(i) = particle_new( code(i), par)
    end do
  end subroutine event_out_state_codes

  subroutine event_out_state_rho (evt, rho_out, n_col, n_hel, n_flv)
    type(event), intent(inout) :: evt
    real(kind=default), dimension(:,:,:), intent(in) :: rho_out
    integer, intent(in) :: n_col, n_hel, n_flv
    evt%rho_out = 0
    evt%n_col_out = n_col
    evt%n_hel_out = n_hel
    evt%n_flv_out = n_flv
    evt%rho_out(:n_col,:n_hel,:n_flv) = rho_out
  end subroutine event_out_state_rho

  subroutine event_get_particle(evt, prt, code)
    type(event), intent(in) :: evt
    type(particle), intent(out) :: prt
    integer, intent(in) :: code
    integer :: i
    prt = particle_null
    do i=1, evt%n_out
       if (particle_code(evt%prt(i)) == code) then
          prt = evt%prt(i)
          exit
       end if
    end do
  end subroutine event_get_particle
  subroutine event_apply_decay(evt, evt_daughter)
    type(event), intent(inout) :: evt
    type(event), intent(in) :: evt_daughter
    type(particle), dimension(:), pointer :: prt
    integer :: i,n
    n = evt%n_out
    do i=1, n
       if (particle_code(evt%prt(i)) == particle_code(evt_daughter%prt(0))) &
            & then
          n = n + evt_daughter%n_out - 1
          if (evt%scattering) then
             allocate(prt(-2:n))
             prt(-2:-1) = evt%prt(-2:-1)
          else
             allocate(prt(0:n))
          end if
          prt(0:evt%n_out)  = evt%prt(0:)
          prt(i)                  = evt_daughter%prt(1)
          prt(evt%n_out+1:) = evt_daughter%prt(2:)
          deallocate(evt%prt)
          evt%prt => prt
          evt%n_out = n
          exit
       end if
    end do
  end subroutine event_apply_decay


end module events
