! WHIZARD 1.95 Feb 25 2010
! 
! (C) 1999-2008 by 
!     Wolfgang Kilian <wolfgang.kilian@desy.de>
!     Thorsten Ohl <ohl@physik.uni-wuerzburg.de>
!     Juergen Reuter <juergen.reuter@physik.uni-freiburg.de>
!
! WHIZARD is free software; you can redistribute it and/or modify it
! under the terms of the GNU General Public License as published by 
! the Free Software Foundation; either version 2, or (at your option)
! any later version.
!
! WHIZARD is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! This file has been stripped of most comments.  For documentation, refer
! to the source 'whizard.nw'

module constants

  use kinds, only: default !NODEP!

  implicit none
  private

  real(kind=default), parameter, public :: &
       &  pi = 3.1415926535897932384626433832795028841972_default

  real(kind=default), parameter, public :: &
       &  twopi = 2*pi, &
       &  twopi2 = twopi**2,  twopi3 = twopi**3,  twopi4 = twopi**4, &
       &  twopi5 = twopi**5,  twopi6 = twopi**6

  real(kind=default), parameter, public :: &
       &  degree = pi/180

  real(kind=default), parameter, public :: &
       &  conv = 0.38937966e12_default

end module constants
