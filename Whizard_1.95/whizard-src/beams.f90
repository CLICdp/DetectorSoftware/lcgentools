! WHIZARD 1.95 Feb 25 2010
! 
! (C) 1999-2008 by 
!     Wolfgang Kilian <wolfgang.kilian@desy.de>
!     Thorsten Ohl <ohl@physik.uni-wuerzburg.de>
!     Juergen Reuter <juergen.reuter@physik.uni-freiburg.de>
!
! WHIZARD is free software; you can redistribute it and/or modify it
! under the terms of the GNU General Public License as published by 
! the Free Software Foundation; either version 2, or (at your option)
! any later version.
!
! WHIZARD is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! This file has been stripped of most comments.  For documentation, refer
! to the source 'whizard.nw'

module beams

  use kinds, only: default !NODEP!
  use kinds, only: double !NODEP!
  use file_utils !NODEP!
  use mpi90, only: mpi90_broadcast !NODEP!
  use files, only: file_exists_else_default
  use constants
  use parameters !NODEP!
  use particles
  use user, only: spectrum_USER_prt_out => spectrum_prt_out
  use user, only: spectrum_USER_beam_remnant => spectrum_beam_remnant
  use user, only: strfun_USER_prt_out => strfun_prt_out
  use user, only: strfun_USER_beam_remnant => strfun_beam_remnant
  use pdflib_interface !NODEP!
  use lhapdf_interface !NODEP!
  use diagnostics, only: msg_buffer
  use diagnostics, only: msg_message, msg_warning, msg_error
  use diagnostics, only: msg_fatal, msg_bug
  use limits, only: KIREPS, CIRCE2_MAX_TRIES
  use limits, only: PRT_NAME_LEN
  use limits, only: FILENAME_LEN, BLANK, COMMENT_CHARS, BUFFER_SIZE
  use lorentz

  implicit none
  private

  type, public :: beam_data_t
     logical :: cm_frame
     real(kind=default) :: energy, momentum, angle
     real(kind=default), dimension(3) :: direction
     logical :: vector_polarization
     real(kind=default), dimension(5) :: polarization
     logical :: fixed_energy
     integer :: n_strfun, n_recoil
     character(len=PRT_NAME_LEN) :: particle_name
     integer :: particle_code
     integer :: generated_externally
     logical :: FILE_events_on, FILE_events_energies
     character(len=FILENAME_LEN) :: FILE_events_file
     integer :: FILE_events_prt_in, FILE_events_prt_out, FILE_events_unit
     logical :: USER_spectrum_on
     integer :: USER_spectrum_prt_in, USER_spectrum_prt_out, USER_spectrum_remnant
     real(kind=default) :: USER_spectrum_sqrts
     integer :: USER_spectrum_mode
     logical :: SCAN_spectrum_on
     integer :: SCAN_spectrum_prt_in, SCAN_spectrum_prt_out
     real(kind=default) :: SCAN_spectrum_sqrts
     real(kind=default) :: SCAN_spectrum_E0, SCAN_spectrum_E1
     logical :: CIRCE_on, CIRCE_generate, CIRCE_map
     real(kind=default) :: CIRCE_beta, CIRCE_gamma
     real(kind=default) :: CIRCE_sqrts
     integer :: CIRCE_prt_in, CIRCE_prt_out
     integer :: CIRCE_ver, CIRCE_rev, CIRCE_acc, CIRCE_chat
     logical :: CIRCE2_on, CIRCE2_generate, CIRCE2_verbose, CIRCE2_polarized
     integer :: CIRCE2_prt_in, CIRCE2_prt_out
     integer :: CIRCE2_map
     character(len=FILENAME_LEN) :: CIRCE2_file, CIRCE2_design
     real(kind=default) :: CIRCE2_sqrts, CIRCE2_power
     integer :: CIRCE2_error
     logical :: ISR_on
     real(kind=default) :: ISR_alpha, ISR_m_in, ISR_Q_max
     real(kind=default) :: ISR_power, ISR_log
     integer :: ISR_prt_in, ISR_prt_out, ISR_LLA_order
     logical :: ISR_map
     logical :: EPA_on, EPA_map
     real(kind=default) :: EPA_alpha, EPA_mX, EPA_Q_max, EPA_x0, EPA_x1, EPA_m_in
     real(kind=default) :: EPA_msq, EPA_s, EPA_c0, EPA_c1, EPA_log
     integer :: EPA_prt_in, EPA_prt_out
     logical :: PDF_on
     integer :: PDF_prt_in, PDF_prt_out
     integer :: PDF_ngroup, PDF_nset, PDF_nfl, PDF_lo
     logical :: PDF_running_scale
     real(kind=default) :: PDF_scale, PDF_QCDL4, PDF_mtop, PDF_power
     logical :: LHAPDF_on = .true.
     integer :: LHAPDF_set
     character(len=FILENAME_LEN) :: LHAPDF_file
     logical :: EWA_on, EWA_map
     real(kind=default) :: EWA_pT_max, EWA_x0, EWA_x1
     real(kind=default) :: EWA_coeff, EWA_cv, EWA_ca, EWA_mass
     integer :: EWA_prt_in, EWA_prt_out, EWA_remnant
     logical :: USER_strfun_on
     integer :: USER_strfun_prt_in, USER_strfun_prt_out, USER_strfun_remnant
     real(kind=default) :: USER_strfun_sqrts
     integer :: USER_strfun_mode
  end type beam_data_t

  public :: create, destroy
  public :: beam_setup
  public :: beam_sign
  public :: read, write
  public :: beams_pythia_format
  public :: mpi90_broadcast
  public :: read_FILE_beam_event
  public :: strfun_SCAN_spectrum
  public :: generate_CIRCE
  public :: strfun_CIRCE, strfun_CIRCE_map
  public :: generate_CIRCE2
  public :: strfun_CIRCE2_unpolarized, strfun_CIRCE2_polarized
  public :: strfun_ISR
  public :: strfun_EPA
  public :: strfun_EWA

  interface create
     module procedure beam_create_single
     module procedure beam_create_multi
  end interface
  interface destroy
     module procedure beam_destroy_single
     module procedure beam_destroy_multi
  end interface
  interface read
     module procedure read_beam_parameters_unit
     module procedure read_beam_parameters_name
  end interface
  interface write
     module procedure write_beam_parameters_unit
     module procedure write_beam_parameters_name
  end interface
  interface mpi90_broadcast
     module procedure beam_par_broadcast
  end interface

contains

  subroutine beam_create_multi (beam)
    type(beam_data_t), dimension(:), intent(out) :: beam
    integer :: i
    do i=1, size(beam)
       call beam_create_single (beam(i))
    end do
  end subroutine beam_create_multi
  subroutine beam_create_single (beam)
    type(beam_data_t), intent(out) :: beam
    beam%cm_frame = .true.
    beam%energy = 0
    beam%momentum = 0
    beam%angle = 0
    beam%direction = 0
    beam%vector_polarization = .false.
    beam%polarization = 0
    beam%fixed_energy = .true.
    beam%n_strfun = 0
    beam%n_recoil = 0
    beam%particle_name = "UNDEFINED"
    beam%particle_code = UNDEFINED
    beam%generated_externally = 0
    beam%FILE_events_on = .false.
    beam%FILE_events_file = ""
    beam%FILE_events_energies = .true.
    beam%FILE_events_unit = 0
    beam%USER_spectrum_on = .false.
    beam%USER_spectrum_sqrts = 0
    beam%USER_spectrum_mode = 0
    beam%SCAN_spectrum_on = .false.
    beam%SCAN_spectrum_sqrts = 0
    beam%SCAN_spectrum_E0 = 0
    beam%SCAN_spectrum_E1 = 0
    beam%CIRCE_on = .false.
    beam%CIRCE_generate = .true.
    beam%CIRCE_map = .true.
    beam%CIRCE_beta = 0
    beam%CIRCE_gamma = 0
    beam%CIRCE_sqrts = 0
    beam%CIRCE_prt_in = 0
    beam%CIRCE_prt_out = 0
    beam%CIRCE_ver = 0
    beam%CIRCE_rev = 0
    beam%CIRCE_acc = 0
    beam%CIRCE_chat = 0
    beam%CIRCE2_on = .false.
    beam%CIRCE2_generate = .true.
    beam%CIRCE2_verbose = .true.
    beam%CIRCE2_prt_in = 0
    beam%CIRCE2_prt_out = 0
    beam%CIRCE2_file = ""
    beam%CIRCE2_design = "*"
    beam%CIRCE2_polarized = .true.
    beam%CIRCE2_map = -2
    beam%CIRCE2_power = 2._default
    beam%CIRCE2_error = 0
    beam%ISR_on = .false.
    beam%ISR_alpha = 0
    beam%ISR_m_in = 0
    beam%ISR_Q_max = 0
    beam%ISR_power = 0
    beam%ISR_log = 0
    beam%ISR_prt_in = 0
    beam%ISR_prt_out = 0
    beam%ISR_LLA_order = 3
    beam%ISR_map = .true.
    beam%EPA_on = .false.
    beam%EPA_map = .true.
    beam%EPA_alpha = 0
    beam%EPA_mX = 0
    beam%EPA_Q_max = 0
    beam%EPA_x0 = 0
    beam%EPA_x1 = 0
    beam%EPA_m_in = 0
    beam%EPA_msq = 0
    beam%EPA_s = 0
    beam%EPA_c0 = 0
    beam%EPA_c1 = 0
    beam%EPA_log = 0
    beam%EPA_prt_in = 0
    beam%EPA_prt_out = 0
    beam%PDF_on = .false.
    beam%PDF_prt_in = 0
    beam%PDF_prt_out = 0
    beam%PDF_ngroup = 0
    beam%PDF_nset = 0
    beam%PDF_nfl = 0
    beam%PDF_lo = 0
    beam%PDF_running_scale = .false.
    beam%PDF_scale = 0
    beam%PDF_QCDL4 = 0
    beam%PDF_mtop = 0
    beam%PDF_power = 2._default
    beam%LHAPDF_on = .false.
    beam%LHAPDF_file = ""
    beam%LHAPDF_set = 0
    beam%EWA_on = .false.
    beam%EWA_map = .true.
    beam%EWA_pT_max = 0
    beam%EWA_x0 = 0
    beam%EWA_x1 = 0
    beam%EWA_coeff = 0
    beam%EWA_cv = 0
    beam%EWA_ca = 0
    beam%EWA_mass = 0
    beam%EWA_prt_in = 0
    beam%EWA_prt_out = 0
    beam%EWA_remnant = 0
    beam%USER_strfun_on = .false.
    beam%USER_strfun_sqrts = 0
    beam%USER_strfun_mode = 0
  end subroutine beam_create_single

  subroutine beam_destroy_multi(beam)
    type(beam_data_t), dimension(:), intent(inout) :: beam
    integer :: i
    do i=1, size(beam)
       call beam_destroy_single(beam(i))
    end do
  end subroutine beam_destroy_multi
  subroutine beam_destroy_single(beam)
    type(beam_data_t), intent(inout) :: beam
    call close_beam_event_file (beam%FILE_events_unit)
    beam%FILE_events_unit = 0
  end subroutine beam_destroy_single

  subroutine beam_setup (beam, sqrts, &
       &                 sqrts_in, par, code, &
       &                 cm_frame, polarized, structured, &
       &                 beam_recoil, conserve_momentum, &
       &                 beam_user, prefix)
    type(beam_data_t), dimension(:), intent(inout) :: beam
    real(kind=default), intent(out) :: sqrts
    real(kind=default), intent(in) :: sqrts_in
    type(parameter_set), intent(in) :: par
    integer, dimension(:), intent(in) :: code
    logical, intent(in) :: cm_frame, polarized, structured
    logical, intent(in) :: beam_recoil, conserve_momentum
    type(beam_data_t), dimension(:), intent(in) :: beam_user
    character(len=FILENAME_LEN), intent(in) :: prefix
    call setup_beam_particles (beam, beam_user, code)
    beam%cm_frame = cm_frame
    call setup_beam_directions (beam, beam_user)
    call setup_beam_energies (beam, beam_user, sqrts, sqrts_in)
    if (polarized) then
       call setup_beam_polarization (beam, beam_user)
    end if
    if (structured .and. size (beam) == 2) then
       call setup_beam_structure_functions &
            & (beam, beam_user, par, code, beam_recoil, prefix)
    end if

  contains

    subroutine setup_beam_particles (beam, beam_user, code)
      type(beam_data_t), dimension(:), intent(inout) :: beam
      type(beam_data_t), dimension(:), intent(in) :: beam_user
      integer, dimension(:), intent(in) :: code
      integer :: i
      if (.not.structured) then
         beam%particle_code = code
      else if (size (beam) == 1) then
         call msg_warning (" I'll ignore the beam structure -- meaningless for decays")
         beam%particle_code = code
      else  ! size(beam)=2
         beam%particle_code = beam_user%particle_code
         do i = 1, 2
            if (beam(i)%particle_code == UNDEFINED) then
               if (beam_user(i)%particle_name /= "UNDEFINED") then
                  beam(i)%particle_code = &
                       & particle_code (beam_user(i)%particle_name)
               else
                  call msg_fatal (" Beam input: Incoming particle unspecified.")
               end if
            else if (beam_user(i)%particle_name /= "UNDEFINED") then
               call msg_error (" Beam input: Inconsistent particle specifications.  I'll use the code value.")
            end if
            beam(i)%particle_name = particle_name (beam(i)%particle_code)
         end do
      end if
    end subroutine setup_beam_particles

    subroutine setup_beam_directions (beam, beam_user)
      type(beam_data_t), dimension(:), intent(inout) :: beam
      type(beam_data_t), dimension(:), intent(in) :: beam_user
      integer :: i
      if (all (beam%cm_frame)) then
         do i = 1, size (beam)
            beam(i)%direction = beam_sign (i) &
                 &              * array (three_momentum_canonical (3))
            beam(i)%angle = 0
         end do
      else
         do i = 1, size (beam)
            if (any (beam_user(i)%direction /= 0)) then
               beam(i)%direction = beam_user(i)%direction &
                    &              / sum (beam_user(i)%direction**2)
               beam(i)%angle = angle (three_momentum_moving (beam(i)%direction), &
                    &                 beam_sign (i) * three_momentum_canonical (3))
            else
               beam(i)%angle = beam_user(i)%angle
               beam(i)%direction =  beam_sign (i) &
                    & * (/ sin(beam(i)%angle), 0._default, cos(beam(i)%angle) /)
            end if
         end do
      end if
    end subroutine setup_beam_directions

    subroutine setup_beam_energies (beam, beam_user, sqrts, sqrts_in)
      type(beam_data_t), dimension(:), intent(inout) :: beam
      type(beam_data_t), dimension(:), intent(in) :: beam_user
      real(kind=default), intent(out) :: sqrts
      real(kind=default), intent(in) :: sqrts_in
      integer, dimension(size(beam)) :: code
      real(kind=default), dimension(size(beam)) :: mass
      integer :: i
      code = beam%particle_code
      mass = particle_mass (code, par)
      if (size (beam) == 1) then
         if (sqrts_in /= 0)  call msg_warning (" Beam setup: sqrts value ignored (decay process)")
         sqrts = mass(1)
         if (beam(1)%cm_frame) then
            beam(1)%energy = mass(1)
            beam(1)%momentum = 0
         else
            if (beam_user(1)%energy >= mass(1)) then
               beam(1)%energy = beam_user(1)%energy
            else if (beam_user(1)%energy == 0) then
               beam(1)%energy = mass(1)
            else
               call msg_error (" Beam setup: energy value lower than mass of decaying particle")
               beam(1)%energy = mass(1)
            end if
            beam(1)%momentum = sqrt (beam(1)%energy**2 - mass(1)**2)
            write (msg_buffer, '(1x,A,1x,1PG12.5,1x,A)') &
                 & "Energy of decaying particle set to", beam(1)%energy, "GeV"
            call msg_message
         end if
      else
         if (all (beam%cm_frame)) then
            if (sqrts_in >= sum (mass)) then
               sqrts = sqrts_in
            else
               call msg_error (" Beam setup: sqrts value lower than sum of beam particle masses")
               sqrts = sum (mass)
            end if
            if (sqrts > 0) then
               beam(1)%energy = max (mass(1), &
                    &                (sqrts + (mass(1)**2-mass(2)**2)/sqrts) / 2)
               beam(2)%energy = max (mass(2), &
                    &                (sqrts - (mass(1)**2-mass(2)**2)/sqrts) / 2)
               beam%momentum = sqrt (beam%energy**2 - mass**2)
            else
               beam%energy = 0
               beam%momentum = 0
            end if
         else
            do i = 1, 2
               if (beam_user(i)%energy >= mass(i)) then
                  beam(i)%energy = beam_user(i)%energy
               else if (beam_user(i)%energy == 0) then
                  beam(i)%energy = mass(i)
               else
                  call msg_error (" Beam setup: energy value lower than mass of beam particle")
                  beam(i)%energy = mass(i)
               end if
               write (msg_buffer, '(1x,A,1x,I1,1x,A,1x,1PG12.5,1x,A)') &
                    & "Energy of beam", i, "set to", beam(i)%energy, "GeV"
               call msg_message
            end do
            beam%momentum = sqrt (beam%energy**2 - mass**2)
            sqrts = sqrt (max (0._default, &
                 &             sum (mass**2) &
                 &             + 2 * product (beam%energy) &
                 &             - 2 * product (beam%momentum) &
                 &                 * dot_product (beam(1)%direction, &
                 &                                beam(2)%direction)))
         end if
         write (msg_buffer, '(1x,A,1x,1PG12.5,1x,A)') &
              & "Process energy set to  ", sqrts, "GeV"
         call msg_message
      end if
    end subroutine setup_beam_energies

    subroutine setup_beam_polarization (beam, beam_user)
      type(beam_data_t), dimension(:), intent(inout) :: beam
      type(beam_data_t), dimension(:), intent(in) :: beam_user
      integer :: i
      do i = 1, size (beam)
         beam(i)%vector_polarization = beam_user(i)%vector_polarization
         beam(i)%polarization = beam_user(i)%polarization
      end do
    end subroutine setup_beam_polarization

    subroutine setup_beam_structure_functions &
         & (beam, beam_user, par, code, beam_recoil, prefix)
      type(beam_data_t), dimension(:), intent(inout) :: beam
      type(beam_data_t), dimension(:), intent(in) :: beam_user
      type(parameter_set), intent(in) :: par
      integer, dimension(:), intent(in) :: code
      logical, intent(in) :: beam_recoil
      character(len=FILENAME_LEN), intent(in) :: prefix
      integer :: i, current_code, strfun_count
      strfun_count = 0
      do i = 1, 2
         current_code = beam(i)%particle_code
         if (beam_user(i)%FILE_events_on) then
            beam(i)%FILE_events_on = .true.
            beam(i)%FILE_events_energies = beam_user(i)%FILE_events_energies
            beam(i)%FILE_events_prt_in = current_code
            beam(i)%FILE_events_prt_out = current_code
            beam(i)%fixed_energy = .false.
            beam(i)%n_strfun = beam(i)%n_strfun + 1
            strfun_count = strfun_count + 1
            beam(i)%generated_externally = strfun_count
            beam(i)%FILE_events_file = beam_user(i)%FILE_events_file
         end if
         if (beam_user(i)%USER_spectrum_on) then
            beam(i)%USER_spectrum_on = .true.
            beam(i)%USER_spectrum_mode = beam_user(i)%USER_spectrum_mode
            beam(i)%USER_spectrum_prt_in = current_code
            beam(i)%USER_spectrum_prt_out = &
                 & spectrum_USER_prt_out (current_code, beam(i)%USER_spectrum_mode)
            beam(i)%USER_spectrum_remnant = &
                 & spectrum_USER_beam_remnant (current_code, beam(i)%USER_spectrum_mode)
            current_code = beam(i)%USER_spectrum_prt_out
            beam(i)%fixed_energy = .false.
            beam(i)%n_strfun = beam(i)%n_strfun + 1
            strfun_count = strfun_count + 1
            beam(i)%USER_spectrum_sqrts = beam_user(i)%USER_spectrum_sqrts
            if (beam(i)%USER_spectrum_sqrts == 0)  beam(i)%USER_spectrum_sqrts = sqrts
         end if
         if (beam_user(i)%SCAN_spectrum_on) then
            beam(i)%SCAN_spectrum_on = .true.
            beam(i)%SCAN_spectrum_prt_in = current_code
            beam(i)%SCAN_spectrum_prt_out = current_code
            beam(i)%fixed_energy = .false.
            beam(i)%n_strfun = beam(i)%n_strfun + 1
            strfun_count = strfun_count + 1
            beam(i)%SCAN_spectrum_sqrts = sqrts
            beam(i)%SCAN_spectrum_E0 = beam_user(i)%SCAN_spectrum_E0
            beam(i)%SCAN_spectrum_E1 = beam_user(i)%SCAN_spectrum_E1
            if (beam(i)%SCAN_spectrum_E1 == 0) beam(i)%SCAN_spectrum_E1 = sqrts
            if (beam(i)%SCAN_spectrum_E0 < 0 .or. &
                 beam(i)%SCAN_spectrum_E0 >= beam(i)%SCAN_spectrum_E1 .or. &
                 beam(i)%SCAN_spectrum_E1 > sqrts) then
               call msg_fatal (" SCAN spectrum: energy limits out of range")
            end if
         end if
         if (beam_user(i)%CIRCE_on .and. abs(current_code)==ELECTRON) then
            if (beam_user(i)%EPA_on) then
               beam(i)%CIRCE_on = .true.
               beam(i)%CIRCE_prt_in = current_code
               beam(i)%CIRCE_prt_out = current_code
            else if (beam_user(i)%PDF_on) then
               beam(i)%CIRCE_on = .true.
               beam(i)%CIRCE_prt_in = current_code
               beam(i)%CIRCE_prt_out = PHOTON
            else if (code(i)==PHOTON) then
               beam(i)%CIRCE_on = .true.
               beam(i)%CIRCE_prt_in = current_code
               beam(i)%CIRCE_prt_out = PHOTON
            else if (code(i)==current_code) then
               beam(i)%CIRCE_on = .true.
               beam(i)%CIRCE_prt_in = current_code
               beam(i)%CIRCE_prt_out = current_code
            end if
         end if
         if (beam(i)%CIRCE_on) then
            current_code = beam(i)%CIRCE_prt_out
            beam(i)%fixed_energy = .false.
            beam(i)%n_strfun = beam(i)%n_strfun+1
            strfun_count = strfun_count + 1
            beam(i)%CIRCE_generate = beam_user(i)%CIRCE_generate
            if (beam(i)%CIRCE_generate)  beam(i)%generated_externally = strfun_count
            beam(i)%CIRCE_map = beam_user(i)%CIRCE_map
            beam(i)%CIRCE_sqrts = beam_user(i)%CIRCE_sqrts
            if (beam(i)%CIRCE_sqrts ==0) &
                 & beam(i)%CIRCE_sqrts = sqrts
            if (beam(i)%CIRCE_sqrts<=0) &
                 & call msg_fatal(" CIRCE energy scale CIRCE_sqrts must be positive")
            beam(i)%CIRCE_acc = beam_user(i)%CIRCE_acc
            beam(i)%CIRCE_ver = beam_user(i)%CIRCE_ver
            beam(i)%CIRCE_rev = beam_user(i)%CIRCE_rev
            beam(i)%CIRCE_chat = beam_user(i)%CIRCE_chat
         end if
         if (beam_user(i)%CIRCE2_on) then
            if (beam_user(i)%EPA_on .and. abs(current_code)==ELECTRON) then
               beam(i)%CIRCE2_on = .true.
               beam(i)%CIRCE2_prt_in = current_code
               beam(i)%CIRCE2_prt_out = current_code
            else if (beam_user(i)%PDF_on) then
               beam(i)%CIRCE2_on = .true.
               beam(i)%CIRCE2_prt_in = current_code
               beam(i)%CIRCE2_prt_out = PHOTON
            else if (code(i)==PHOTON) then
               beam(i)%CIRCE2_on = .true.
               beam(i)%CIRCE2_prt_in = current_code
               beam(i)%CIRCE2_prt_out = PHOTON
            else if (code(i)==current_code) then
               beam(i)%CIRCE2_on = .true.
               beam(i)%CIRCE2_prt_in = current_code
               beam(i)%CIRCE2_prt_out = current_code
            end if
         end if
         if (beam(i)%CIRCE2_on) then
            current_code = beam(i)%CIRCE2_prt_out
            beam(i)%fixed_energy = .false.
            beam(i)%n_strfun = beam(i)%n_strfun+1
            strfun_count = strfun_count + 1
            beam(i)%CIRCE2_file = beam_user(i)%CIRCE2_file
            beam(i)%CIRCE2_design = beam_user(i)%CIRCE2_design
            beam(i)%CIRCE2_polarized = beam_user(i)%CIRCE2_polarized
            beam(i)%CIRCE2_verbose = beam_user(i)%CIRCE2_verbose
            beam(i)%CIRCE2_generate = beam_user(i)%CIRCE2_generate
            if (.not.beam(i)%CIRCE2_generate) then
               select case (beam_user(i)%CIRCE2_map)
               case (-1,0,1)
                  beam(i)%CIRCE2_map = beam_user(i)%CIRCE2_map
                  beam(i)%CIRCE2_power = beam_user(i)%CIRCE2_power
               case default
                  select case (current_code)
                  case (PHOTON)
                     beam(i)%CIRCE2_map = 0
                     beam(i)%CIRCE2_power = 3._default
                  case (ELECTRON)
                     beam(i)%CIRCE2_map = 1
                     beam(i)%CIRCE2_power = 12._default
                  case default
                     beam(i)%CIRCE2_map = -1
                  end select
               end select
            end if
         end if
         if (beam_user(i)%ISR_on .and. particle_charge(current_code)/=0) then
            beam(i)%ISR_on = .true.
            beam(i)%ISR_prt_in = current_code
            beam(i)%ISR_prt_out = current_code
         end if
         if (beam(i)%ISR_on) then      
            beam(i)%fixed_energy = .false.
            beam(i)%n_strfun = beam(i)%n_strfun + 1
            if (beam_recoil)  beam(i)%n_recoil = beam(i)%n_recoil + 2
            strfun_count = strfun_count + 1
            beam(i)%ISR_alpha = beam_user(i)%ISR_alpha
            if (beam(i)%ISR_alpha == 0) &
                 & beam(i)%ISR_alpha = QED_charge(par)**2 / (4*pi)
            if (beam(i)%ISR_alpha <= 0) &
                 & call msg_fatal (" Coupling ISR_alpha must be positive.")
            beam(i)%ISR_m_in  = beam_user(i)%ISR_m_in
            if (beam(i)%ISR_m_in  == 0) &
                 & beam(i)%ISR_m_in  = particle_mass(beam(i)%ISR_prt_in, par)
            if (beam(i)%ISR_m_in <= 0) &
                 & call msg_fatal (" Incoming particle mass ISR_m_in must be positive.")
            beam(i)%ISR_Q_max = beam_user(i)%ISR_Q_max
            if (beam(i)%ISR_Q_max == 0) &
                 & beam(i)%ISR_Q_max = sqrts
            if (beam(i)%ISR_Q_max <= 0) &
                 & call msg_fatal (" Energy scale ISR_Q_max must be positive.")
            if (beam(i)%ISR_Q_max <= beam(i)%ISR_m_in) &
                 & call msg_fatal (" ISR_m_in must be smaller than ISR_Q_max.")
            beam(i)%ISR_log = log (1 + (beam(i)%ISR_Q_max / beam(i)%ISR_m_in)**2)
            beam(i)%ISR_power = &
                 & beam(i)%ISR_alpha/pi &
                 &  * particle_charge(beam(i)%ISR_prt_in)**2 &
                 &  * (2 * log(beam(i)%ISR_Q_max / beam(i)%ISR_m_in) - 1)
            if (beam(i)%ISR_power>1) then
               write (msg_buffer, "(1x,G10.3)")  beam(i)%ISR_power
               call msg_message
               call msg_fatal (" ISR: Expansion parameter is too large, LLA fails.")
            end if
            select case (beam_user(i)%ISR_LLA_order)
            case (0:3)
               beam(i)%ISR_LLA_order = beam_user(i)%ISR_LLA_order
            case default
               call msg_message (" ISR_LLA_order must be between 0 and 3.")
               call msg_error (" Illegal ISR_LLA_order replaced by default (3).")
            end select
            beam(i)%ISR_map = beam_user(i)%ISR_map
         end if
         if (beam_user(i)%EPA_on .and. particle_charge(current_code)/=0) then
            beam(i)%EPA_on = .true.
            beam(i)%EPA_prt_in = current_code
            beam(i)%EPA_prt_out = PHOTON
            current_code = beam(i)%EPA_prt_out
         end if
         if (beam(i)%EPA_on) then
            beam(i)%fixed_energy = .false.
            beam(i)%n_strfun = beam(i)%n_strfun + 1
            if (beam_recoil)  beam(i)%n_recoil = beam(i)%n_recoil + 2
            strfun_count = strfun_count + 1
            beam(i)%EPA_map = beam_user(i)%EPA_map
            beam(i)%EPA_m_in = beam_user(i)%EPA_m_in
            if (beam(i)%EPA_m_in == 0) &
                 beam(i)%EPA_m_in = particle_mass (beam(i)%EPA_prt_in, par)
            beam(i)%EPA_msq = beam(i)%EPA_m_in**2
            if (beam(i)%EPA_msq == 0) &
                 & call msg_fatal (" EPA: Incoming particle mass must be nonzero.")
            beam(i)%EPA_alpha = beam_user(i)%EPA_alpha
            if (beam(i)%EPA_alpha == 0) &
                 & beam(i)%EPA_alpha = QED_charge(par)**2 / (4*pi)
            if (beam(i)%EPA_alpha <= 0) &
                 & call msg_fatal (" Coupling EPA_alpha must be positive.")
            if (beam(i)%EPA_map) then
               beam(i)%EPA_mX  = beam_user(i)%EPA_mX
               if (beam(i)%EPA_mX <= 0) &
                    & call msg_fatal (" Minimum produced mass EPA_mX must be positive.")
               if (beam(i)%EPA_mX >= sqrts) &
                    & call msg_fatal (" EPA_mX must be smaller than sqrt(s).")
            end if
            beam(i)%EPA_Q_max = beam_user(i)%EPA_Q_max
            if (beam(i)%EPA_Q_max <=0) &
                 & call msg_fatal (" Cutoff EPA_Q_max must be positive.")
            if (beam(i)%EPA_Q_max > sqrts) &
                 & call msg_fatal (" Cutoff EPA_Q_max must not be larger than sqrt(s).")
            beam(i)%EPA_log = log (1 + (beam(i)%EPA_Q_max / beam(i)%EPA_m_in)**2)
            beam(i)%EPA_x0 = beam_user(i)%EPA_x0
            if (beam(i)%EPA_map) then
               if (beam(i)%EPA_x0 < (beam(i)%EPA_mX / sqrts)**2) &
                    & beam(i)%EPA_x0 = (beam(i)%EPA_mX / sqrts)**2
            else
               if (beam(i)%EPA_x0 < 0) &
                    & beam(i)%EPA_x0 = 0
            end if
            beam(i)%EPA_x1 = beam_user(i)%EPA_x1
            if (beam(i)%EPA_x1 == 0) &
                 & beam(i)%EPA_x1 = 1
            if (beam(i)%EPA_x1 > 1) then
               call msg_error (" EPA_x1 must not be larger than 1.  I'll set it to 1.")
               beam(i)%EPA_x1 = 1
            end if
            if (beam(i)%EPA_x1 <= beam(i)%EPA_x0) then
               call msg_error (" EPA_x0 must be smaller than EPA_x1.  I'll set EPA_x0 to 0.")
               beam(i)%EPA_x0 = 0
            end if
            beam(i)%EPA_s = sqrts**2
            if (beam(i)%EPA_map) then
               beam(i)%EPA_c0 = beam(i)%EPA_alpha/pi &
                    & * particle_charge(beam(i)%EPA_prt_in)**2 &
                    & * log(beam(i)%EPA_x0) &
                    & * log(beam_user(i)%EPA_Q_max**2 / beam(i)%EPA_msq &
                    &       / beam(i)%EPA_x0)
               beam(i)%EPA_c1 = beam(i)%EPA_alpha/pi &
                    & * particle_charge(beam(i)%EPA_prt_in)**2 &
                    & * log(beam(i)%EPA_x1) &
                    & * log(beam_user(i)%EPA_Q_max**2 / beam(i)%EPA_msq &
                    &       / beam(i)%EPA_x1)
               if (beam(i)%EPA_c1 <= beam(i)%EPA_c0) &
                    & call msg_fatal (" EPA_Q_max is too small.")
            else
               beam(i)%EPA_c0 = beam(i)%EPA_alpha/pi &
                    & * particle_charge(beam(i)%EPA_prt_in)**2 &
                    & * beam(i)%EPA_x0
               beam(i)%EPA_c1 = beam(i)%EPA_alpha/pi &
                    & * particle_charge(beam(i)%EPA_prt_in)**2 &
                    & * beam(i)%EPA_x1
            end if
         end if
         if (beam_user(i)%PDF_on) then
            select case (abs(current_code))
            case (PROTON, PHOTON)
               select case (abs(code(i)))
               case (D_QUARK:T_QUARK, GLUON, GLUON2)
                  beam(i)%PDF_on = .true.
                  beam(i)%PDF_prt_in = current_code
                  beam(i)%PDF_prt_out = code(i)
                  current_code = beam(i)%PDF_prt_out
               end select
            end select
         end if
         if (beam(i)%PDF_on) then
            beam(i)%fixed_energy = .false.
            beam(i)%n_strfun = beam(i)%n_strfun + 1
            strfun_count = strfun_count + 1
            beam(i)%PDF_ngroup = beam_user(i)%PDF_ngroup
            beam(i)%PDF_nset = beam_user(i)%PDF_nset
            beam(i)%PDF_nfl = beam_user(i)%PDF_nfl
            beam(i)%PDF_lo = beam_user(i)%PDF_lo
            beam(i)%PDF_QCDL4 = beam_user(i)%PDF_QCDL4
            beam(i)%PDF_mtop = particle_mass (T_QUARK, par)
            beam(i)%PDF_running_scale = beam_user(i)%PDF_running_scale
            beam(i)%PDF_power = beam_user(i)%PDF_power
            if (.not.beam(i)%PDF_running_scale) then
               beam(i)%PDF_scale = beam_user(i)%PDF_scale
               if (beam(i)%PDF_scale <= 0) then
                  call msg_message &
                       & (" Missing PDF scale.  You have two choices:")
                  call msg_message &
                       & (" (1) PDF_running_scale=T, (2) PDF_scale > 0")
                  call msg_fatal (" PDF setup failed.")
               end if
            end if   
            call pdflib_init (beam(i)%PDF_prt_in, beam(i)%PDF_ngroup, &
                 &            beam(i)%PDF_nset, beam(i)%PDF_nfl, beam(i)%PDF_lo, &
                 &            beam(i)%PDF_QCDL4, beam(i)%PDF_mtop, first=.true.)   
            if (polarized) &
                 & call msg_warning (" Beam polarization is ignored for hadron-initiated processes.")
         end if
         if (beam_user(i)%LHAPDF_on) then
            if (beam_user(i)%PDF_on)  call msg_fatal &
                 (" PDFlib and LHAPDF cannot be switched on for a beam simultaneously")
            select case (abs(current_code))
            case (PROTON, PHOTON)
               select case (abs(code(i)))
               case (D_QUARK:T_QUARK, GLUON, GLUON2)
                  beam(i)%LHAPDF_on = .true.
                  beam(i)%PDF_prt_in = current_code
                  beam(i)%PDF_prt_out = code(i)
                  current_code = beam(i)%PDF_prt_out
               end select
            end select
         end if
         if (beam(i)%LHAPDF_on) then
            beam(i)%fixed_energy = .false.
            beam(i)%n_strfun = beam(i)%n_strfun + 1
            strfun_count = strfun_count + 1
            beam(i)%PDF_running_scale = beam_user(i)%PDF_running_scale
            beam(i)%PDF_power = beam_user(i)%PDF_power
            beam(i)%LHAPDF_on = beam_user(i)%LHAPDF_on
            beam(i)%LHAPDF_file = beam_user(i)%LHAPDF_file
            beam(i)%LHAPDF_set = beam_user(i)%LHAPDF_set
            if (.not.beam(i)%PDF_running_scale) then
               beam(i)%PDF_scale = beam_user(i)%PDF_scale
               if (beam(i)%PDF_scale <= 0) then
                  call msg_message &
                       & (" Missing PDF scale.  You have two choices:")
                  call msg_message &
                       & (" (1) PDF_running_scale=T, (2) PDF_scale > 0")
                  call msg_fatal (" PDF setup failed.")
               end if
            end if   
            call lhapdflib_init (beam(i)%PDF_prt_in, beam(i)%LHAPDF_file, &
                 &            beam(i)%LHAPDF_set, first=.true.)   
            if (polarized) &
                 & call msg_warning (" Beam polarization is ignored for hadron-initiated processes.")
         end if
         if (beam_user(i)%EWA_on .and. particle_spin_type(current_code)==FERMION) then
            select case (code(i))
            case (W_BOSON)
               if (particle_isospin (current_code) == sign (1, current_code)) then
                  beam(i)%EWA_on = .true.
                  beam(i)%EWA_prt_in = current_code
                  beam(i)%EWA_prt_out = W_BOSON
                  beam(i)%EWA_remnant = current_code - 1
               end if
            case (-W_BOSON)
               if (particle_isospin (current_code) == -sign (1, current_code)) then
                  beam(i)%EWA_on = .true.
                  beam(i)%EWA_prt_in = current_code
                  beam(i)%EWA_prt_out = -W_BOSON
                  beam(i)%EWA_remnant = current_code + 1
               end if
            case (Z_BOSON)
               if (particle_isospin (current_code) /= 0 &
                    .or. particle_charge (current_code) /= 0) then
                  beam(i)%EWA_on = .true.
                  beam(i)%EWA_prt_in = current_code
                  beam(i)%EWA_prt_out = Z_BOSON
                  beam(i)%EWA_remnant = current_code
               end if
            end select
         end if
         if (beam(i)%EWA_on) then
            current_code = beam(i)%EWA_prt_out
            select case (abs (code(i)))
            case (W_BOSON)
               beam(i)%EWA_cv = QED_charge(par)/(2 * sqrt(2._default) * SM_sw(par))
               beam(i)%EWA_ca = QED_charge(par)/(2 * sqrt(2._default) * SM_sw(par)) &
                    * sign (1, beam(i)%EWA_remnant)
            case (Z_BOSON)
               beam(i)%EWA_cv = QED_charge(par)/(2 * SM_sw(par) * SM_cw(par)) &
                    * (particle_isospin (beam(i)%EWA_prt_in)/2._default &
                       - 2 * particle_charge (beam(i)%EWA_prt_in) * SM_sw(par)**2)
               beam(i)%EWA_ca = QED_charge(par)/(2 * SM_sw(par) * SM_cw(par)) &
                    * (particle_isospin (beam(i)%EWA_prt_in)/2._default) &
                    * sign (1, beam(i)%EWA_remnant)
            end select
            beam(i)%EWA_coeff = 1 / (8 * pi**2)
            beam(i)%fixed_energy = .false.
            beam(i)%n_strfun = beam(i)%n_strfun + 1
            if (beam_recoil)  beam(i)%n_recoil = beam(i)%n_recoil + 2
            strfun_count = strfun_count + 1
            beam(i)%EWA_map = beam_user(i)%EWA_map
            beam(i)%EWA_pT_max = beam_user(i)%EWA_pT_max
            if (beam(i)%EWA_pT_max <=0) beam(i)%EWA_pt_max = sqrts
            beam(i)%EWA_x0 = beam_user(i)%EWA_x0
            beam(i)%EWA_mass = particle_mass (current_code, par)
            if (beam(i)%EWA_x0 < (beam(i)%EWA_mass / sqrts)**2) then
               if (beam(i)%EWA_x0 /= 0) &
                    call msg_error (" EWA_x0 must not be less than mV^2/s")
               beam(i)%EWA_x0 = (beam(i)%EWA_mass / sqrts)**2
            end if
            beam(i)%EWA_x1 = beam_user(i)%EWA_x1
            if (beam(i)%EWA_x1 == 0)  beam(i)%EWA_x1 = 1
            if (beam(i)%EWA_x1 > 1) then
               call msg_error (" EWA_x1 must not be larger than 1.  I'll set it to 1.")
               beam(i)%EWA_x1 = 1
            end if
            if (beam(i)%EWA_x1 <= beam(i)%EWA_x0) then
               call msg_fatal (" EWA_x0 must be smaller than EWA_x1.")
            end if
         end if
         if (beam_user(i)%USER_strfun_on) then
            beam(i)%USER_strfun_on = .true.
            beam(i)%USER_strfun_mode = beam_user(i)%USER_strfun_mode
            beam(i)%USER_strfun_prt_in = current_code
            beam(i)%USER_strfun_prt_out = &
                 & strfun_USER_prt_out (current_code, beam(i)%USER_strfun_mode)
            beam(i)%USER_strfun_remnant = &
                 & strfun_USER_beam_remnant (current_code, beam(i)%USER_strfun_mode)
            current_code = beam(i)%USER_strfun_prt_out
            beam(i)%fixed_energy = .false.
            beam(i)%n_strfun = beam(i)%n_strfun + 1
            strfun_count = strfun_count + 1
            beam(i)%USER_strfun_sqrts = beam_user(i)%USER_strfun_sqrts
            if (beam(i)%USER_strfun_sqrts == 0)  beam(i)%USER_strfun_sqrts = sqrts
         end if
         if (current_code /= code(i)) then
            write (msg_buffer, "(1x,A,1x,I7,1x,A)") &
                 & "Beam particle       = ", beam(i)%particle_code, &
                 & particle_name (beam(i)%particle_code)
            call msg_message
            write (msg_buffer, "(1x,A,1x,I7,1x,A)") &
                 & "Parton              = ", current_code, &
                 & particle_name (current_code)
            call msg_message
            write (msg_buffer, "(1x,A,1x,I7,1x,A)") &
                 & "Process in-particle = ", code(i), &
                 & particle_name (code(i))
            call msg_message
            call msg_fatal (" Beam setup does not match process.")
         else if (beam(i)%n_strfun==0) then
            write (msg_buffer, "(1x,A,1x,I1,A)") &
                 & "No structure functions selected for beam", i, "."
            call msg_message
         else
            write (msg_buffer, "(1x,A,1x,I1,A)") &
                 & "Active structure functions for beam", i, ":"
            call msg_message
            if (beam(i)%FILE_events_on)  call strfun_show &
                 & ("FILE events", beam(i)%FILE_events_prt_in, &
                 &  beam(i)%FILE_events_prt_out, generate=.true.)
            if (beam(i)%USER_spectrum_on)  call strfun_show &
                 & ("USER spectrum", beam(i)%USER_spectrum_prt_in, &
                 &  beam(i)%USER_spectrum_prt_out)
            if (beam(i)%SCAN_spectrum_on)  call strfun_show &
                 & ("SCAN spectrum", beam(i)%SCAN_spectrum_prt_in, &
                 &  beam(i)%SCAN_spectrum_prt_out)
            if (beam(i)%CIRCE_on)  call strfun_show &
                 & ("CIRCE", beam(i)%CIRCE_prt_in, beam(i)%CIRCE_prt_out, &
                 &  beam(i)%CIRCE_generate)
            if (beam(i)%CIRCE2_on)  call strfun_show &
                 & ("CIRCE2", beam(i)%CIRCE2_prt_in, beam(i)%CIRCE2_prt_out, &
                 &  beam(i)%CIRCE2_generate)
            if (beam(i)%ISR_on)  call strfun_show &
                    & ("ISR", beam(i)%ISR_prt_in, beam(i)%ISR_prt_out)
            if (beam(i)%EPA_on)  call strfun_show &
                    & ("EPA", beam(i)%EPA_prt_in, beam(i)%EPA_prt_out)
            if (beam(i)%PDF_on)  call strfun_show &
                    & ("PDF", beam(i)%PDF_prt_in, beam(i)%PDF_prt_out)
            if (beam(i)%LHAPDF_on)  call strfun_show &
                    & ("LHAPDF", beam(i)%PDF_prt_in, beam(i)%PDF_prt_out)
            if (beam(i)%EWA_on)  call strfun_show &
                    & ("EWA", beam(i)%EWA_prt_in, beam(i)%EWA_prt_out)
            if (beam(i)%USER_strfun_on)  call strfun_show &
                 & ("USER strfun", beam(i)%USER_strfun_prt_in, beam(i)%USER_strfun_prt_out)
         end if
      end do
      if (any (beam%FILE_events_on)) then
         if (all (beam%FILE_events_on) .and. &
              & beam(1)%FILE_events_file == beam(2)%FILE_events_file) then
            call open_beam_event_file (prefix, &
                 & trim(beam(1)%FILE_events_file), beam(1)%FILE_events_unit)
            beam(2)%FILE_events_unit = beam(1)%FILE_events_unit
         else
            do i = 1, 2
               if (beam(i)%FILE_events_on) then
                  call open_beam_event_file (prefix, &
                       & trim(beam(i)%FILE_events_file), beam(i)%FILE_events_unit)
               end if
            end do
         end if
         if (sqrts <= 0)  call msg_error &
              & (" FILE events: Nominal collider energy sqrts must be positive")
         if (polarized) then
            do i=1, 2
               if (any (beam(i)%polarization /= 0)) then
                  call msg_warning (" FILE events: Beam polarization will be assumed constant.")
               end if
            end do
         end if
      end if
      if (all (beam%SCAN_spectrum_on)) then
         call msg_fatal (" SCAN spectrum applies to one beam only.")
      end if
      if (any (beam%CIRCE_on)) then
         if (.not.all (beam%CIRCE_on)) then
            call msg_fatal (" CIRCE must be switched on for both beams, if at all.")
         else 
            if (beam(1)%CIRCE_sqrts /= beam(2)%CIRCE_sqrts &
                 & .or. beam(1)%CIRCE_ver /= beam(2)%CIRCE_ver &
                 & .or. beam(1)%CIRCE_rev /= beam(2)%CIRCE_rev &
                 & .or. beam(1)%CIRCE_acc /= beam(2)%CIRCE_acc &
                 & .or. beam(1)%CIRCE_chat/= beam(2)%CIRCE_chat) then
               call msg_error (" CIRCE parameters must coincide for both beams.  I'll use the first set.")
               beam(2)%CIRCE_sqrts = beam(1)%CIRCE_sqrts
               beam(2)%CIRCE_ver = beam(1)%CIRCE_ver
               beam(2)%CIRCE_rev = beam(1)%CIRCE_rev
               beam(2)%CIRCE_acc = beam(1)%CIRCE_acc
               beam(2)%CIRCE_chat = beam(1)%CIRCE_chat
            end if
            call circes (0.d0, 0.d0, dble(beam(1)%CIRCE_sqrts), &
                 & beam(1)%CIRCE_acc, &
                 & beam(1)%CIRCE_ver, beam(1)%CIRCE_rev, beam(1)%CIRCE_chat)
            call map_CIRCE_set_powers &
                 & (beam(1)%CIRCE_beta, beam(1)%CIRCE_gamma, code(1))
            call map_CIRCE_set_powers &
                 & (beam(2)%CIRCE_beta, beam(2)%CIRCE_gamma, code(2))
         end if
         if (beam(1)%CIRCE_generate .neqv. beam(2)%CIRCE_generate) then
            call msg_error ("CIRCE mode must coincide for both beams. I'll use generate mode.")
            beam%CIRCE_generate = .true.
         end if
         if (polarized) then
            do i=1, 2
               if (any (beam(i)%polarization /= 0)) then
                  if (beam(i)%CIRCE_prt_out == PHOTON) then
                     call msg_error (" CIRCE: Beam polarization will be ignored for photon emission.")
                  else
                     call msg_warning (" CIRCE: Beamstrahlung effect on polarization will be ignored.")
                  end if
               end if
            end do
         end if
      end if
      if (any (beam%CIRCE2_on)) then
         if (.not.all (beam%CIRCE2_on)) then
            call msg_fatal (" CIRCE2 must be switched on for both beams, if at all.")
         else 
            if (beam(1)%CIRCE2_verbose .neqv. beam(2)%CIRCE2_verbose &
                 & .or. beam(1)%CIRCE2_file /= beam(2)%CIRCE2_file &
                 & .or. beam(1)%CIRCE2_design /= beam(2)%CIRCE2_design ) then
               call msg_error (" CIRCE2 parameters must coincide for both beams.  I'll use the first set.")
            end if
            if (beam(1)%CIRCE2_verbose) then
               beam%CIRCE2_error = 1
            else
               beam%CIRCE2_error = 0
            end if
            call cir2ld (trim (beam(1)%CIRCE2_file), trim (beam(1)%CIRCE2_design), &
                 &       sqrts, beam(1)%CIRCE2_error)
            if (beam(1)%CIRCE2_generate .neqv. beam(2)%CIRCE2_generate) then
               call msg_error (" CIRCE2 mode must coincide for both beams. I'll use generate mode.")
               beam%CIRCE2_generate = .true.
            end if
            do i=1, 2
               if (.not.beam(i)%CIRCE2_generate) then
                  select case (beam(i)%CIRCE2_map)
                  case (-1)
                  case (0,1)
                     if (beam(i)%CIRCE2_power <= 0) then
                        call msg_error ("CIRCE2: CIRCE2_power must be greater than zero")
                        beam(i)%CIRCE2_map = -1
                     end if
                  case default
                     call msg_error ("CIRCE2: illegal value for CIRCE2_map (allowed: -1,0,1)")
                     beam(i)%CIRCE2_map = -1
                  end select
               end if
            end do
            if (beam(1)%CIRCE2_error < 0)  beam%CIRCE2_on = .false.
            select case (beam(1)%CIRCE2_error)
            case (-1)
               call msg_fatal (" CIRCE2 parameter file not found.")
            case (-2)
               call msg_fatal (" CIRCE2 beam energy does not match.")
            case (-3)
               call msg_fatal (" CIRCE2 parameter file format error.")
            case (-4)
               call msg_fatal (" CIRCE2 parameter file too long.")
            end select
         end if
      end if
      do i=1, 2
         if (beam(i)%ISR_on .and. polarized) then
            if (any (beam(i)%polarization /= 0)) then
               call msg_warning (" ISR: Effect on beam polarization will be ignored.")
            end if
         end if
         if (all (beam%ISR_on) .and. beam(1)%ISR_LLA_order /= beam(2)%ISR_LLA_order) then
            call msg_warning (" ISR: LLA order does not coincide for both beams.")
         end if
      end do
      do i=1, 2
         if (beam(i)%EPA_on .and. polarized) then
            if (any (beam(i)%polarization /= 0)) then
               call msg_error (" EPA: Beam polarization will be ignored.")
            end if
         end if
      end do
      do i=1, 2
         if (beam(i)%PDF_on .and. polarized) then
            if (any (beam(i)%polarization /= 0)) then
               call msg_error (" PDF: Beam polarization will be ignored.")
            end if
         end if
      end do
      do i=1, 2
         if (beam(i)%LHAPDF_on .and. polarized) then
            if (any (beam(i)%polarization /= 0)) then
               call msg_error (" LHAPDF: Beam polarization will be ignored.")
            end if
         end if
      end do
      do i=1, 2
         if (beam(i)%EWA_on .and. polarized) then
            if (any (beam(i)%polarization /= 0)) then
               call msg_error (" EWA: Beam polarization will be ignored.")
            end if
         end if
      end do
    end subroutine setup_beam_structure_functions

    subroutine strfun_show (str, code_in, code_out, generate)
      character(len=*), intent(in) :: str
      integer, intent(in) :: code_in, code_out
      logical, intent(in), optional :: generate
      character(len=15) :: string
      logical :: gen
      gen = .false.
      if (present (generate))  gen = generate
      string = str//":"
      if (gen) then
         write (msg_buffer, "(3x,A15,1x,A,1x,A,1x,A)") &
              & string, trim (particle_name (code_in)), &
              & "->", trim (particle_name (code_out) // " (generator)")
      else
         write (msg_buffer, "(3x,A15,1x,A,1x,A,1x,A)") &
              & string, trim (particle_name (code_in)), &
              & "->", trim (particle_name (code_out))
      end if
      call msg_message
    end subroutine strfun_show

  end subroutine beam_setup

  function beam_sign (i) result (s)
    integer, intent(in) :: i
    integer :: s
    select case (i)
    case (1);  s =  1
    case (2);  s = -1
    case default
       call msg_bug (" Function beam_sign called for argument other than 1,2")
    end select
  end function beam_sign

  subroutine read_beam_parameters_unit (unit, beam)
    integer, intent(in) :: unit
    type(beam_data_t), dimension(:), intent(inout) :: beam
    integer :: i
    character(len=PRT_NAME_LEN) :: particle_name
    integer :: particle_code
    real(kind=default) :: energy, angle
    real(kind=default), dimension(3) :: direction
    real(kind=default), dimension(5) :: polarization
    logical :: vector_polarization
    logical :: FILE_events_on, FILE_events_energies
    character(len=FILENAME_LEN) :: FILE_events_file
    integer :: FILE_events_prt_in, FILE_events_prt_out, FILE_events_unit
    logical :: USER_spectrum_on
    integer :: USER_spectrum_prt_in, USER_spectrum_prt_out, USER_spectrum_remnant
    real(kind=default) :: USER_spectrum_sqrts
    integer :: USER_spectrum_mode
    logical :: SCAN_spectrum_on
    integer :: SCAN_spectrum_prt_in, SCAN_spectrum_prt_out
    real(kind=default) :: SCAN_spectrum_sqrts
    real(kind=default) :: SCAN_spectrum_E0, SCAN_spectrum_E1
    logical :: CIRCE_on, CIRCE_generate, CIRCE_map
    real(kind=default) :: CIRCE_beta, CIRCE_gamma
    real(kind=default) :: CIRCE_sqrts
    integer :: CIRCE_prt_in, CIRCE_prt_out
    integer :: CIRCE_ver, CIRCE_rev, CIRCE_acc, CIRCE_chat
    logical :: CIRCE2_on, CIRCE2_generate, CIRCE2_verbose, CIRCE2_polarized
    integer :: CIRCE2_prt_in, CIRCE2_prt_out
    integer :: CIRCE2_map
    character(len=FILENAME_LEN) :: CIRCE2_file, CIRCE2_design
    real(kind=default) :: CIRCE2_sqrts, CIRCE2_power
    integer :: CIRCE2_error
    logical :: ISR_on
    real(kind=default) :: ISR_alpha, ISR_m_in, ISR_Q_max
    real(kind=default) :: ISR_power, ISR_log
    integer :: ISR_prt_in, ISR_prt_out, ISR_LLA_order
    logical :: ISR_map
    logical :: EPA_on, EPA_map
    real(kind=default) :: EPA_alpha, EPA_mX, EPA_Q_max, EPA_x0, EPA_x1, EPA_m_in
    real(kind=default) :: EPA_msq, EPA_s, EPA_c0, EPA_c1, EPA_log
    integer :: EPA_prt_in, EPA_prt_out
    logical :: PDF_on
    integer :: PDF_prt_in, PDF_prt_out
    integer :: PDF_ngroup, PDF_nset, PDF_nfl, PDF_lo
    logical :: PDF_running_scale
    real(kind=default) :: PDF_scale, PDF_QCDL4, PDF_mtop, PDF_power
    logical :: LHAPDF_on = .true.
    integer :: LHAPDF_set
    character(len=FILENAME_LEN) :: LHAPDF_file
    logical :: EWA_on, EWA_map
    real(kind=default) :: EWA_pT_max, EWA_x0, EWA_x1
    real(kind=default) :: EWA_coeff, EWA_cv, EWA_ca, EWA_mass
    integer :: EWA_prt_in, EWA_prt_out, EWA_remnant
    logical :: USER_strfun_on
    integer :: USER_strfun_prt_in, USER_strfun_prt_out, USER_strfun_remnant
    real(kind=default) :: USER_strfun_sqrts
    integer :: USER_strfun_mode
    namelist /beam_input/ particle_name, particle_code
    namelist /beam_input/ energy, angle, direction
    namelist /beam_input/ vector_polarization, polarization
    namelist /beam_input/ FILE_events_on, FILE_events_energies, FILE_events_file
    namelist /beam_input/ USER_spectrum_on, USER_spectrum_sqrts, USER_spectrum_mode
    namelist /beam_input/ SCAN_spectrum_on, SCAN_spectrum_E0, SCAN_spectrum_E1
    namelist /beam_input/ CIRCE_on, CIRCE_generate, CIRCE_map, &
    !     &                CIRCE_beta, CIRCE_gamma, &
         &                CIRCE_sqrts, CIRCE_ver, &
         &                CIRCE_rev, CIRCE_acc, CIRCE_chat
    namelist /beam_input/ CIRCE2_on, CIRCE2_generate, CIRCE2_verbose, &
         &                CIRCE2_file, CIRCE2_design, CIRCE2_polarized, &
         &                CIRCE2_map, CIRCE2_power
    namelist /beam_input/ ISR_on, ISR_alpha, ISR_m_in, ISR_Q_max, ISR_LLA_order, &
         & ISR_map
    namelist /beam_input/ EPA_on, EPA_map
    namelist /beam_input/ EPA_alpha, EPA_mX
    namelist /beam_input/ EPA_Q_max, EPA_x0, EPA_x1, EPA_m_in
    namelist /beam_input/ PDF_on, PDF_ngroup, PDF_nset, PDF_nfl, PDF_lo
    namelist /beam_input/ PDF_running_scale, PDF_scale, PDF_QCDL4, PDF_power
    namelist /beam_input/ LHAPDF_on, LHAPDF_file, LHAPDF_set
    namelist /beam_input/ EWA_on, EWA_map
    namelist /beam_input/ EWA_pT_max, EWA_x0, EWA_x1
    namelist /beam_input/ USER_strfun_on, USER_strfun_sqrts, USER_strfun_mode
    do i = 1, size (beam)
       particle_name = beam(i)%particle_name
       particle_code = beam(i)%particle_code
       energy = beam(i)%energy
       angle = beam(i)%angle
       direction = beam(i)%direction
       vector_polarization = beam(i)%vector_polarization
       polarization = beam(i)%polarization
       FILE_events_on = beam(i)%FILE_events_on
       FILE_events_file = beam(i)%FILE_events_file
       FILE_events_energies = beam(i)%FILE_events_energies
       USER_spectrum_on = beam(i)%USER_spectrum_on
       USER_spectrum_sqrts = beam(i)%USER_spectrum_sqrts
       USER_spectrum_mode = beam(i)%USER_spectrum_mode
       SCAN_spectrum_on = beam(i)%SCAN_spectrum_on
       SCAN_spectrum_E0 = beam(i)%SCAN_spectrum_E0
       SCAN_spectrum_E1 = beam(i)%SCAN_spectrum_E1
       CIRCE_on       = beam(i)%CIRCE_on
       CIRCE_generate = beam(i)%CIRCE_generate
       CIRCE_map      = beam(i)%CIRCE_map
       !CIRCE_beta    = beam(i)%CIRCE_beta
       !CIRCE_gamma   = beam(i)%CIRCE_gamma
       CIRCE_sqrts    = beam(i)%CIRCE_sqrts
       CIRCE_ver      = beam(i)%CIRCE_ver
       CIRCE_rev      = beam(i)%CIRCE_rev
       CIRCE_acc      = beam(i)%CIRCE_acc
       CIRCE_chat     = beam(i)%CIRCE_chat
       CIRCE2_on        = beam(i)%CIRCE2_on
       CIRCE2_generate  = beam(i)%CIRCE2_generate
       CIRCE2_verbose   = beam(i)%CIRCE2_verbose
       CIRCE2_file      = beam(i)%CIRCE2_file
       CIRCE2_design    = beam(i)%CIRCE2_design
       CIRCE2_polarized = beam(i)%CIRCE2_polarized
       CIRCE2_map       = beam(i)%CIRCE2_map
       CIRCE2_power     = beam(i)%CIRCE2_power
       ISR_on = beam(i)%ISR_on
       ISR_alpha = beam(i)%ISR_alpha
       ISR_m_in = beam(i)%ISR_m_in
       ISR_Q_max = beam(i)%ISR_Q_max
       ISR_LLA_order = beam(i)%ISR_LLA_order
       ISR_map = beam(i)%ISR_map
       EPA_on = beam(i)%EPA_on
       EPA_map = beam(i)%EPA_map
       EPA_alpha = beam(i)%EPA_alpha
       EPA_mX = beam(i)%EPA_mX
       EPA_Q_max = beam(i)%EPA_Q_max
       EPA_x0 = beam(i)%EPA_x0
       EPA_x1 = beam(i)%EPA_x1
       EPA_m_in = beam(i)%EPA_m_in
       PDF_on = beam(i)%PDF_on
       PDF_ngroup = beam(i)%PDF_ngroup
       PDF_nset = beam(i)%PDF_nset
       PDF_nfl = beam(i)%PDF_nfl
       PDF_lo = beam(i)%PDF_lo
       PDF_running_scale = beam(i)%PDF_running_scale
       PDF_scale = beam(i)%PDF_scale
       PDF_QCDL4 = beam(i)%PDF_QCDL4
       PDF_power = beam(i)%PDF_power
       LHAPDF_on = beam(i)%LHAPDF_on
       LHAPDF_file = beam(i)%LHAPDF_file
       LHAPDF_set = beam(i)%LHAPDF_set
       EWA_on = beam(i)%EWA_on
       EWA_map = beam(i)%EWA_map
       EWA_pT_max = beam(i)%EWA_pT_max
       EWA_x0 = beam(i)%EWA_x0
       EWA_x1 = beam(i)%EWA_x1
       USER_strfun_on = beam(i)%USER_strfun_on
       USER_strfun_sqrts = beam(i)%USER_strfun_sqrts
       USER_strfun_mode = beam(i)%USER_strfun_mode
       read(unit, nml=beam_input)
       beam(i)%particle_name = particle_name
       beam(i)%particle_code = particle_code
       beam(i)%energy = energy
       beam(i)%angle = angle
       beam(i)%direction = direction
       beam(i)%vector_polarization = vector_polarization
       beam(i)%polarization = polarization
       beam(i)%FILE_events_on = FILE_events_on
       beam(i)%FILE_events_file = FILE_events_file
       beam(i)%FILE_events_energies = FILE_events_energies
       beam(i)%USER_spectrum_on = USER_spectrum_on
       beam(i)%USER_spectrum_sqrts = USER_spectrum_sqrts
       beam(i)%USER_spectrum_mode = USER_spectrum_mode
       beam(i)%SCAN_spectrum_on = SCAN_spectrum_on
       beam(i)%SCAN_spectrum_E0 = SCAN_spectrum_E0
       beam(i)%SCAN_spectrum_E1 = SCAN_spectrum_E1
       beam(i)%CIRCE_on       = CIRCE_on
       beam(i)%CIRCE_generate = CIRCE_generate
       beam(i)%CIRCE_map      = CIRCE_map
       !beam(i)%CIRCE_beta     = CIRCE_beta
       !beam(i)%CIRCE_gamma    = CIRCE_gamma
       beam(i)%CIRCE_sqrts    = CIRCE_sqrts
       beam(i)%CIRCE_ver      = CIRCE_ver
       beam(i)%CIRCE_rev      = CIRCE_rev
       beam(i)%CIRCE_acc      = CIRCE_acc
       beam(i)%CIRCE_chat     = CIRCE_chat
       beam(i)%CIRCE2_on        = CIRCE2_on
       beam(i)%CIRCE2_generate  = CIRCE2_generate
       beam(i)%CIRCE2_verbose   = CIRCE2_verbose
       beam(i)%CIRCE2_file      = CIRCE2_file
       beam(i)%CIRCE2_design    = CIRCE2_design
       beam(i)%CIRCE2_polarized = CIRCE2_polarized
       beam(i)%CIRCE2_map       = CIRCE2_map
       beam(i)%CIRCE2_power     = CIRCE2_power
       beam(i)%ISR_on = ISR_on
       beam(i)%ISR_alpha = ISR_alpha
       beam(i)%ISR_m_in = ISR_m_in
       beam(i)%ISR_Q_max = ISR_Q_max
       beam(i)%ISR_LLA_order = ISR_LLA_order
       beam(i)%ISR_map = ISR_map
       beam(i)%EPA_on = EPA_on
       beam(i)%EPA_map = EPA_map
       beam(i)%EPA_alpha = EPA_alpha
       beam(i)%EPA_mX = EPA_mX
       beam(i)%EPA_Q_max = EPA_Q_max
       beam(i)%EPA_x0 = EPA_x0
       beam(i)%EPA_x1 = EPA_x1
       beam(i)%EPA_m_in = EPA_m_in
       beam(i)%PDF_on = PDF_on
       beam(i)%PDF_ngroup = PDF_ngroup
       beam(i)%PDF_nset = PDF_nset
       beam(i)%PDF_nfl = PDF_nfl
       beam(i)%PDF_lo = PDF_lo
       beam(i)%PDF_running_scale = PDF_running_scale 
       beam(i)%PDF_scale = PDF_scale 
       beam(i)%PDF_QCDL4 = PDF_QCDL4
       beam(i)%PDF_power = PDF_power
       beam(i)%LHAPDF_on = LHAPDF_on
       beam(i)%LHAPDF_file = LHAPDF_file
       beam(i)%LHAPDF_set = LHAPDF_set
       beam(i)%EWA_on = EWA_on
       beam(i)%EWA_map = EWA_map
       beam(i)%EWA_pT_max = EWA_pT_max
       beam(i)%EWA_x0 = EWA_x0
       beam(i)%EWA_x1 = EWA_x1
       beam(i)%USER_strfun_on = USER_strfun_on
       beam(i)%USER_strfun_sqrts = USER_strfun_sqrts
       beam(i)%USER_strfun_mode = USER_strfun_mode
    end do
  end subroutine read_beam_parameters_unit

  subroutine write_beam_parameters_unit(unit, beam)
    integer, intent(in) :: unit
    type(beam_data_t), dimension(:), intent(in) :: beam
    integer :: i
    do i = 1, size (beam)
       write (unit, *) "&beam_input"
       write (unit, *) "particle_name  = ", beam(i)%particle_name
       write (unit, *) "particle_code  = ", beam(i)%particle_code
       write (unit, *) "energy         = ", beam(i)%energy
       write (unit, *) "angle          = ", beam(i)%angle
       write (unit, *) "direction      = ", beam(i)%direction
       write (unit, *) "vector_polarization = ", beam(i)%vector_polarization
       write (unit, *) "polarization = "
       write (unit, *)    beam(i)%polarization
       write(unit, *) 'FILE_events_on       = ', beam(i)%FILE_events_on
       write(unit, *) 'FILE_events_file     = "', trim (beam(i)%FILE_events_file), '"'
       write(unit, *) 'FILE_events_energies = ', beam(i)%FILE_events_energies
       write(unit, *) 'USER_spectrum_on    = ', beam(i)%USER_spectrum_on
       write(unit, *) 'USER_spectrum_sqrts = ', beam(i)%USER_spectrum_sqrts
       write(unit, *) 'USER_spectrum_mode  = ', beam(i)%USER_spectrum_mode
       write(unit, *) 'SCAN_spectrum_on    = ', beam(i)%SCAN_spectrum_on
       write(unit, *) 'SCAN_spectrum_E0    = ', beam(i)%SCAN_spectrum_E0
       write(unit, *) 'SCAN_spectrum_E1    = ', beam(i)%SCAN_spectrum_E1
       write(unit, *) 'CIRCE_on       = ', beam(i)%CIRCE_on
       write(unit, *) 'CIRCE_generate = ', beam(i)%CIRCE_map
       write(unit, *) 'CIRCE_map      = ', beam(i)%CIRCE_map
       !write(unit, *) 'CIRCE_beta     = ', beam(i)%CIRCE_beta
       !write(unit, *) 'CIRCE_gamma    = ', beam(i)%CIRCE_gamma
       write(unit, *) 'CIRCE_sqrts    = ', beam(i)%CIRCE_sqrts
       write(unit, *) 'CIRCE_ver      = ', beam(i)%CIRCE_ver
       write(unit, *) 'CIRCE_rev      = ', beam(i)%CIRCE_rev
       write(unit, *) 'CIRCE_acc      = ', beam(i)%CIRCE_acc
       write(unit, *) 'CIRCE_chat     = ', beam(i)%CIRCE_chat
       write(unit, *) 'CIRCE2_on        = ', beam(i)%CIRCE2_on
       write(unit, *) 'CIRCE2_generate  = ', beam(i)%CIRCE2_generate
       write(unit, *) 'CIRCE2_verbose   = ', beam(i)%CIRCE2_verbose
       write(unit, *) 'CIRCE2_file      = ', '"', trim (beam(i)%CIRCE2_file), '"'
       write(unit, *) 'CIRCE2_design    = ', '"', trim (beam(i)%CIRCE2_design), '"'
       write(unit, *) 'CIRCE2_polarized = ', beam(i)%CIRCE2_polarized
       write(unit, *) 'CIRCE2_map       = ', beam(i)%CIRCE2_map
       write(unit, *) 'CIRCE2_power     = ', beam(i)%CIRCE2_power
       write(unit, *) 'ISR_on        = ', beam(i)%ISR_on
       write(unit, *) 'ISR_alpha     = ', beam(i)%ISR_alpha
       write(unit, *) 'ISR_m_in      = ', beam(i)%ISR_m_in
       write(unit, *) 'ISR_Q_max     = ', beam(i)%ISR_Q_max
       write(unit, *) 'ISR_LLA_order = ', beam(i)%ISR_LLA_order
       write(unit, *) 'ISR_map       = ', beam(i)%ISR_map
       write(unit, *) 'EPA_on       = ', beam(i)%EPA_on
       write(unit, *) 'EPA_map      = ', beam(i)%EPA_map
       write(unit, *) 'EPA_alpha    = ', beam(i)%EPA_alpha
       write(unit, *) 'EPA_m_in     = ', beam(i)%EPA_m_in
       write(unit, *) 'EPA_mX       = ', beam(i)%EPA_mX
       write(unit, *) 'EPA_Q_max    = ', beam(i)%EPA_Q_max
       write(unit, *) 'EPA_x0       = ', beam(i)%EPA_x0  
       write(unit, *) 'EPA_x1       = ', beam(i)%EPA_x1
       write(unit, *) 'PDF_on            = ', beam(i)%PDF_on
       write(unit, *) 'PDF_ngroup        = ', beam(i)%PDF_ngroup
       write(unit, *) 'PDF_nset          = ', beam(i)%PDF_nset
       write(unit, *) 'PDF_nfl           = ', beam(i)%PDF_nfl
       write(unit, *) 'PDF_lo            = ', beam(i)%PDF_lo
       write(unit, *) 'PDF_running_scale = ', beam(i)%PDF_running_scale
       write(unit, *) 'PDF_scale         = ', beam(i)%PDF_scale
       write(unit, *) 'PDF_QCDL4         = ', beam(i)%PDF_QCDL4
       write(unit, *) 'PDF_power         = ', beam(i)%PDF_power
       write(unit, *) 'LHAPDF_on         = ', beam(i)%LHAPDF_on
       write(unit, *) 'LHAPDF_file       = "', trim (beam(i)%LHAPDF_file), '"'
       write(unit, *) 'LHAPDF_set        = ', beam(i)%LHAPDF_set
       write(unit, *) 'EWA_on       = ', beam(i)%EWA_on
       write(unit, *) 'EWA_map      = ', beam(i)%EWA_map
       write(unit, *) 'EWA_pT_max    = ', beam(i)%EWA_pT_max
       write(unit, *) 'EWA_x0       = ', beam(i)%EWA_x0  
       write(unit, *) 'EWA_x1       = ', beam(i)%EWA_x1
       write(unit, *) 'USER_strfun_on    = ', beam(i)%USER_strfun_on
       write(unit, *) 'USER_strfun_sqrts = ', beam(i)%USER_strfun_sqrts
       write(unit, *) 'USER_strfun_mode  = ', beam(i)%USER_strfun_mode
       write (unit, *) "/"
    end do
  end subroutine write_beam_parameters_unit

  subroutine read_beam_parameters_name(name, beam)
    character(len=*), intent(in) :: name
    type(beam_data_t), dimension(2), intent(inout) :: beam
    integer :: unit
    unit = free_unit ()
    open (unit=unit, action='read', status='old', file=name)
    call read_beam_parameters_unit (unit, beam)
    close (unit=unit)
  end subroutine read_beam_parameters_name

  subroutine write_beam_parameters_name (name, beam)
    character(len=*), intent(in) :: name
    type(beam_data_t), dimension(:), intent(in) :: beam
    integer :: unit
    unit = free_unit ()
    open (unit=unit, action='write', status='replace', file=name)
    call write_beam_parameters_unit (unit, beam)
    close (unit=unit)
  end subroutine write_beam_parameters_name

  subroutine beams_pythia_format (beam, code_in, beam_str, target_str)
    type(beam_data_t), dimension(:), intent(in) :: beam
    integer, dimension(:), intent(in) :: code_in
    character(len=*), intent(inout) :: beam_str, target_str
    character(len=max(len(beam_str), len(target_str))), dimension(2) :: str
    integer :: code, i
    do i = 1, size (beam)
       if (beam(i)%fixed_energy) then
          code = code_in(i)
       else
          code = beam(i)%particle_code
       end if
       select case (code)
       case (ELECTRON);      str(i) = "e-"
       case (-ELECTRON);     str(i) = "e+"
       case (E_NEUTRINO);    str(i) = "nu_e"
       case (-E_NEUTRINO);   str(i) = "nu_e~"
       case (MU_LEPTON);     str(i) = "mu-"
       case (-MU_LEPTON);    str(i) = "mu+"
       case (MU_NEUTRINO);   str(i) = "nu_mu"
       case (-MU_NEUTRINO);  str(i) = "nu_mu~"
       case (TAU_LEPTON);    str(i) = "tau-"
       case (-TAU_LEPTON);   str(i) = "tau+"
       case (TAU_NEUTRINO);  str(i) = "nu_tau"
       case (-TAU_NEUTRINO); str(i) = "nu_tau~"
       case (PHOTON);        str(i) = "gamma"
       case (PROTON);        str(i) = "p+"
       case (-PROTON);       str(i) = "p~-"
       case default
          call msg_message (" Incoming particle "//trim(particle_name(code)) &
               &            //" is not recognized by PYTHIA.")
          call msg_fatal (" PYTHIA fragmentation is not possible.")
       end select
    end do
    beam_str = str(1)
    if (size (beam) == 2) then
       target_str = str(2)
    else
       target_str = ""
    end if
  end subroutine beams_pythia_format

  subroutine beam_par_broadcast (beam, root, domain, error)
    type(beam_data_t), dimension(:), intent(inout) :: beam
    integer, intent(in) :: root
    integer, intent(in), optional :: domain
    integer, intent(out), optional :: error
    integer :: i
    do i = 1, size (beam)
       call mpi90_broadcast(beam(i)%vector_polarization, root, domain, error)
       call mpi90_broadcast(beam(i)%polarization, root, domain, error)
!       call mpi90_broadcast(beam(i)%particle_name, root, domain, error)
       call mpi90_broadcast(beam(i)%particle_code, root, domain, error)
       call mpi90_broadcast(beam(i)%FILE_events_on, root, domain, error)
       !call mpi90_broadcast(beam(i)%FILE_events_file, root, domain, error)
       call mpi90_broadcast(beam(i)%FILE_events_energies, root, domain, error)
       call mpi90_broadcast(beam(i)%USER_spectrum_on, root, domain, error)
       call mpi90_broadcast(beam(i)%USER_spectrum_sqrts, root, domain, error)
       call mpi90_broadcast(beam(i)%USER_spectrum_mode, root, domain, error)
       call mpi90_broadcast(beam(i)%SCAN_spectrum_on, root, domain, error)
       call mpi90_broadcast(beam(i)%SCAN_spectrum_E0, root, domain, error)
       call mpi90_broadcast(beam(i)%SCAN_spectrum_E1, root, domain, error)
       call mpi90_broadcast(beam(i)%CIRCE_on, root, domain, error)
       call mpi90_broadcast(beam(i)%CIRCE_generate, root, domain, error)
       call mpi90_broadcast(beam(i)%CIRCE_map, root, domain, error)
       !call mpi90_broadcast(beam(i)%CIRCE_beta, root, domain, error)
       !call mpi90_broadcast(beam(i)%CIRCE_gamma, root, domain, error)
       call mpi90_broadcast(beam(i)%CIRCE_sqrts, root, domain, error)
       call mpi90_broadcast(beam(i)%CIRCE_ver, root, domain, error)
       call mpi90_broadcast(beam(i)%CIRCE_rev, root, domain, error)
       call mpi90_broadcast(beam(i)%CIRCE_acc, root, domain, error)
       call mpi90_broadcast(beam(i)%CIRCE_chat, root, domain, error)
       call mpi90_broadcast(beam(i)%CIRCE2_on, root, domain, error)
       call mpi90_broadcast(beam(i)%CIRCE2_generate, root, domain, error)
       call mpi90_broadcast(beam(i)%CIRCE2_verbose, root, domain, error)
       !call mpi90_broadcast(beam(i)%CIRCE2_file, root, domain, error)
       !call mpi90_broadcast(beam(i)%CIRCE2_design, root, domain, error)
       call mpi90_broadcast(beam(i)%CIRCE2_polarized, root, domain, error)
       call mpi90_broadcast(beam(i)%CIRCE2_map, root, domain, error)
       call mpi90_broadcast(beam(i)%CIRCE2_power, root, domain, error)
       call mpi90_broadcast(beam(i)%ISR_on, root, domain, error)
       call mpi90_broadcast(beam(i)%ISR_alpha, root, domain, error)
       call mpi90_broadcast(beam(i)%ISR_m_in, root, domain, error)
       call mpi90_broadcast(beam(i)%ISR_Q_max, root, domain, error)
       call mpi90_broadcast(beam(i)%ISR_LLA_order, root, domain, error)
       call mpi90_broadcast(beam(i)%ISR_map, root, domain, error)
       call mpi90_broadcast(beam(i)%EPA_on, root, domain, error)
       call mpi90_broadcast(beam(i)%EPA_map, root, domain, error)
       call mpi90_broadcast(beam(i)%EPA_alpha, root, domain, error)
       call mpi90_broadcast(beam(i)%EPA_mX, root, domain, error)
       call mpi90_broadcast(beam(i)%EPA_Q_max, root, domain, error)
       call mpi90_broadcast(beam(i)%EPA_x0, root, domain, error)
       call mpi90_broadcast(beam(i)%EPA_x1, root, domain, error)
       call mpi90_broadcast(beam(i)%EPA_m_in, root, domain, error)
       ! call mpi90_broadcast(beam(i)%PDF_on, root, domain, error)
       ! call mpi90_broadcast(beam(i)%PDF_ngroup, root, domain, error)
       ! call mpi90_broadcast(beam(i)%PDF_nset, root, domain, error)
       ! call mpi90_broadcast(beam(i)%PDF_nfl, root, domain, error)
       ! call mpi90_broadcast(beam(i)%PDF_lo, root, domain, error)
       ! call mpi90_broadcast(beam(i)%PDF_running_scale, root, domain, error)
       ! call mpi90_broadcast(beam(i)%PDF_scale, root, domain, error)
       ! call mpi90_broadcast(beam(i)%PDF_QCDL4, root, domain, error)
       ! call mpi90_broadcast(beam(i)%PDF_power, root, domain, error)
       ! call mpi90_broadcast(beam(i)%PDF_on, root, domain, error)
       ! call mpi90_broadcast(beam(i)%PDF_ngroup, root, domain, error)
       ! call mpi90_broadcast(beam(i)%PDF_nset, root, domain, error)
       ! call mpi90_broadcast(beam(i)%PDF_nfl, root, domain, error)
       ! call mpi90_broadcast(beam(i)%PDF_lo, root, domain, error)
       ! call mpi90_broadcast(beam(i)%PDF_running_scale, root, domain, error)
       ! call mpi90_broadcast(beam(i)%PDF_scale, root, domain, error)
       ! call mpi90_broadcast(beam(i)%PDF_QCDL4, root, domain, error)
       ! call mpi90_broadcast(beam(i)%PDF_power, root, domain, error)
       ! call mpi90_broadcast(beam(i)%LHAPDF, root, domain, error)
       ! call mpi90_broadcast(beam(i)%LHAPDF_file, root, domain, error)
       ! call mpi90_broadcast(beam(i)%LHAPDF_set, root, domain, error)
       call mpi90_broadcast(beam(i)%EWA_on, root, domain, error)
       call mpi90_broadcast(beam(i)%EWA_map, root, domain, error)
       call mpi90_broadcast(beam(i)%EWA_pT_max, root, domain, error)
       call mpi90_broadcast(beam(i)%EWA_x0, root, domain, error)
       call mpi90_broadcast(beam(i)%EWA_x1, root, domain, error)
       call mpi90_broadcast(beam(i)%USER_strfun_on, root, domain, error)
       call mpi90_broadcast(beam(i)%USER_strfun_sqrts, root, domain, error)
       call mpi90_broadcast(beam(i)%USER_strfun_mode, root, domain, error)
    end do
  end subroutine beam_par_broadcast

  subroutine open_beam_event_file (prefix, file, unit)
    character(len=*), intent(in) :: prefix, file
    integer, intent(out) :: unit
    character(len=FILENAME_LEN) :: filename
    logical :: exist
    filename = file_exists_else_default (prefix, file, "bev")
    inquire (file = trim(filename), exist = exist)
    if (exist) then
       call msg_message &
            & (" Reading beam events from file " //trim(filename)// "...")
       unit = free_unit ()
       open (unit, file = trim(filename), action = "read")
    else
       call msg_fatal (' Beam events file "' //trim(file)// '" not found')
    end if
  end subroutine open_beam_event_file

  subroutine close_beam_event_file (unit)
    integer, intent(in) :: unit
    logical :: opened
    if (unit > 0) then
       inquire (unit = unit, opened = opened)
       if (opened)  close (unit)
    end if
  end subroutine close_beam_event_file

  subroutine read_FILE_beam_event (unit, x1, x2)
    integer, intent(in) :: unit
    real(kind=default), intent(out) :: x1
    real(kind=default), intent(out), optional :: x2
    character(len=BUFFER_SIZE) :: buffer
    integer :: iostat, pos
    READLINE: do
       read (unit, "(A)", iostat = iostat)  buffer
       select case (iostat)
       case (0)
          pos = verify (buffer, BLANK)
          if (pos > 0) then
             if (verify (buffer(pos:pos), COMMENT_CHARS) /= 0) then
                if (present (x2)) then
                   read (buffer, *, iostat = iostat)  x1, x2
                else
                   read (buffer, *, iostat = iostat)  x1
                end if
                if (iostat == 0) then
                   exit READLINE
                else
                   call msg_message (trim (buffer))
                   call msg_error (" Reading beam events from file: Syntax error (line ignored)")
                end if
             end if
          end if
       case (:-1)
          call msg_warning (" Reading beam events from file: File end encountered (rewinding)")
          rewind (unit)
       case default
          call msg_fatal (" Reading beam events from file: I/O error")
       end select
    end do READLINE
  end subroutine read_FILE_beam_event

  subroutine strfun_SCAN_spectrum (f, x, sqrts, E0, E1)
    real(kind=default), intent(inout) :: x, f
    real(kind=default), intent(in) :: sqrts, E0, E1
    x = ((E1 * x + E0 * (1-x)) / sqrts)**2
    f = (E1 - E0) / sqrts
  end subroutine strfun_SCAN_spectrum

  subroutine generate_CIRCE (r, code, mc_random_number)
    real(kind=default), dimension(2), intent(out) :: r
    integer, dimension(2), intent(in) :: code
    interface
       subroutine mc_random_number (r)
         use kinds, only: double !NODEP!
         real(kind=double) :: r
       end subroutine mc_random_number
    end interface
    real(kind=double), dimension(2) :: x
    select case (code(1))
    case (ELECTRON)
       select case (code(2))
       case (-ELECTRON);  call gircee (x(1), x(2), mc_random_number)
       case (PHOTON);     call girceg (x(1), x(2), mc_random_number)
       case default; call error
       end select
    case (PHOTON)
       select case (code(2))
       case (ELECTRON);   call girceg (x(2), x(1), mc_random_number)
       case (PHOTON);     call gircgg (x(1), x(2), mc_random_number)
       case (-ELECTRON);  call girceg (x(2), x(1), mc_random_number)
       case default; call error
       end select
    case (-ELECTRON)
       select case (code(2))
       case (ELECTRON);   call gircee (x(2), x(1), mc_random_number)
       case (PHOTON);     call girceg (x(1), x(2), mc_random_number)
       case default; call error
       end select
    case default; call error
    end select
    r = x
  contains
    subroutine error
      call msg_bug ("CIRCE2: Wrong particle combination")
    end subroutine error
  end subroutine generate_CIRCE

  subroutine strfun_CIRCE (f, x1, x2, p1, p2)
    real(kind=default), intent(inout) :: f, x1, x2
    integer, intent(in) :: p1, p2
    double precision :: kirke
    external kirke
    if (x1 /= 0 .and. x2 /= 0) then
       f = f * kirke (dble(x1), dble(x2), p1, p2)
    else
       f = 0
    end if
  end subroutine strfun_CIRCE

  function map_CIRCE_jacobian (x, x0, a, power, invert) result (dy_dx)
    real(kind=default), intent(in) :: x, x0, a, power
    logical, intent(in) :: invert
    real(kind=default) :: dy_dx
    if (x<=0) then
       dy_dx = a
    else if (invert) then
       if (x<x0) then
          dy_dx = a * (1-x)**power
       else
          dy_dx = a * (1-x0)**power
       end if
    else
       if (x<x0) then
          dy_dx = a * x**power
       else
          dy_dx = a * x0**power
       end if
    end if
  end function map_CIRCE_jacobian

  function map_CIRCE_x (y, y0, x0, a, power, invert) result (x)
    real(kind=default), intent(in) :: y, y0, x0, a, power
    logical, intent(in) :: invert
    real(kind=default) :: x
    if (y<=0) then
       x = 0
    else if (y>1) then
       x = 1
    else if (invert) then
       if (y<y0) then
          x = 1 - (1 - (1+power)*y/a)**(1/(1+power))
       else
          x = x0 + (y-y0)/(a*(1-x0)**power)
       end if
    else
       if (y<y0) then
          x = ((1+power)*y/a)**(1/(1+power))
       else
          x = x0 + (y-y0)/(a*x0**power)
       end if
    end if
  end function map_CIRCE_x

  subroutine map_CIRCE_set_const (a, y0, x0, power, invert)
    real(kind=default), intent(out) :: a, y0
    real(kind=default), intent(in) :: x0, power
    logical, intent(in) :: invert
    real(kind=default) :: tmp
    if (invert) then
       tmp = (1 - (1-x0)**(1+power)) / (1+power)
       a = 1 / (tmp + (1-x0)**(1+power))
    else
       tmp = x0**(1+power) / (1+power)
       a = 1 / (tmp + (1-x0) * x0**power)
    end if
    y0 = a * tmp
  end subroutine map_CIRCE_set_const

  subroutine strfun_CIRCE_map_s (f, x, x0, beta, gamma)
    real(kind=default), intent(inout) :: f, x
    real(kind=default), intent(in) :: x0, beta, gamma
    real(kind=default) :: y, z, ay, az, y0, z0
    call map_CIRCE_set_const (ay, y0, x0, gamma, invert=.true.)
    call map_CIRCE_set_const (az, z0, y0, beta, invert=.false.)
    z = x
    y = map_CIRCE_x (z, z0, y0, az, beta, invert=.false.)
!    y = z
    x = map_CIRCE_x (y, y0, x0, ay, gamma, invert=.true.)
    f = f / map_CIRCE_jacobian (x, x0, ay, gamma, invert=.true.)
    f = f / map_CIRCE_jacobian (y, y0, az, beta, invert=.false.)
  end subroutine strfun_CIRCE_map_s
    
  subroutine strfun_CIRCE_map_p (f, x, eps, beta, gamma, p)
    real(kind=default), intent(inout) :: f, x
    real(kind=default), intent(in) :: eps, beta, gamma
    integer, intent(in) :: p
    real(kind=default) :: xtmp
    if (p==PHOTON) then
       xtmp = 1 - x
       call strfun_CIRCE_map_s (f, xtmp, 1-eps, beta, gamma)
       x = 1 - xtmp
    else
       call strfun_CIRCE_map_s (f, x, 1-eps, beta, gamma)
    end if
  end subroutine strfun_CIRCE_map_p

  subroutine map_CIRCE_set_powers (beta, gamma, p)
    real(kind=default), intent(out) :: beta, gamma
    integer, intent(in) :: p
    double precision x1m, x2m, roots
    common /circom/ x1m, x2m, roots
    double precision lumi
    common /circom/ lumi
    double precision a1(0:7)
    common /circom/ a1
    double precision elect0, gamma0
    common /circom/ elect0, gamma0
    integer acc, ver, rev, chat
    common /circom/ acc, ver, rev, chat
    integer magic
    common /circom/ magic
    integer e, r, ehi, elo
    common /circom/ e, r, ehi, elo
    save /circom/
    if (abs(p)==ELECTRON) then
       beta = a1(2)
       gamma = a1(3)
    else if (abs(p)==PHOTON) then
       beta = a1(6)
       gamma = a1(5)
    end if
  end subroutine map_CIRCE_set_powers

  subroutine strfun_CIRCE_map (f, x1, x2, beta1, gamma1, beta2, gamma2, p1, p2)
    real(kind=default), intent(inout) :: f, x1, x2
    real(kind=default), intent(in) :: beta1, gamma1
    real(kind=default), intent(in) :: beta2, gamma2
    integer, intent(in) :: p1, p2
    real(kind=default), parameter :: eps = KIREPS
    call strfun_CIRCE_map_p (f, x1, eps, beta1, gamma1, p1)
    call strfun_CIRCE_map_p (f, x2, eps, beta2, gamma2, p2)
    call strfun_CIRCE (f, x1, x2, p1, p2)
  end subroutine strfun_CIRCE_map

  subroutine generate_CIRCE2 (x, code, mc_random_number, rho_hel)
    real(kind=default), dimension(2), intent(out) :: x
    integer, dimension(2), intent(in) :: code
    interface
       subroutine mc_random_number (r)
         use kinds, only: double !NODEP!
         real(kind=double) :: r
       end subroutine mc_random_number
    end interface
    complex(kind=default), dimension(-2:2,-2:2,2), intent(inout), optional :: &
         & rho_hel
    integer, dimension(2) :: code_try
    integer, dimension(2) :: h
    integer :: i
    GET_CHANNEL: do i = 1, CIRCE2_MAX_TRIES
       call cir2ch (code_try(1), h(1), code_try(2), h(2), mc_random_number)
       if (all (code_try == code))  exit GET_CHANNEL
    end do GET_CHANNEL
    if (i >= CIRCE2_MAX_TRIES)  call msg_fatal &
         & ("CIRCE2: Could not generate requested particle combination")
    call cir2gn (code(1), h(1), code(2), h(2), x(1), x(2), mc_random_number)
    if (present (rho_hel)) then
       rho_hel = 0
       do i = 1, 2
          rho_hel(h(i),h(i),i) = 1
       end do
    end if
  end subroutine generate_CIRCE2

  subroutine strfun_CIRCE2_unpolarized (f, x1, x2, code, map, power)
    real(kind=default), intent(inout) :: f, x1, x2
    integer, dimension(2), intent(in) :: code, map
    real(kind=default), dimension(2), intent(in) :: power
    real(kind=default) :: y1, y2
    double precision :: cir2dn
    external cir2dn
    call power_mapping (f, y1, x1, map(1), power(1))
    call power_mapping (f, y2, x2, map(2), power(2))
    f = f * cir2dn (code(1), 0, code(2), 0, dble(y1), dble(y2))
    x1 = y1
    x2 = y2
  end subroutine strfun_CIRCE2_unpolarized

  subroutine strfun_CIRCE2_polarized (f, x1, x2, rho_hel, code, map, power)
    real(kind=default), intent(inout) :: f, x1, x2
    complex(kind=default), dimension(-2:2,-2:2,2), intent(inout) :: rho_hel
    integer, dimension(2), intent(in) :: code, map
    real(kind=default), dimension(2), intent(in) :: power
    real(kind=default) :: y1, y2, cir2lm_total
    integer :: h1, h2
    double precision :: cir2dn, cir2lm
    external cir2dn, cir2lm
    real(kind=default), dimension(-2:2,-2:2) :: rho_tmp
    rho_tmp = 0
    call power_mapping (f, y1, x1, map(1), power(1))
    call power_mapping (f, y2, x2, map(2), power(2))
    cir2lm_total = cir2lm (code(1), 0, code(2), 0)
    if (cir2lm_total <= 0) then
       f = 0
       return
    end if
    do h1 = -1, 1, 2
       do h2 = -1, 1, 2
          rho_tmp(h1,h2) = 4 * &
               & rho_hel(h1,h1,1) * rho_hel(h2,h2,2) &
               & * cir2dn (code(1), h1, code(2), h2, dble(y1), dble(y2)) &
               & * cir2lm (code(1), h1, code(2), h2) / cir2lm_total
       end do
    end do
    where (rho_tmp < 0)
       rho_tmp = 0
    end where
    rho_hel(-2:2,-2:2,1) = rho_tmp(-2:2,-2:2)
    rho_hel(-2:2,-2:2,2) = 0
    f = f
    x1 = y1
    x2 = y2
  end subroutine strfun_CIRCE2_polarized

  subroutine power_mapping (f, y, x, map, power)
    real(kind=default), intent(inout) :: f
    real(kind=default), intent(out) :: y
    real(kind=default), intent(in) :: x
    integer, intent(in) :: map
    real(kind=default), intent(in) :: power
    real(kind=default) :: xbar, ybar
    select case (map)
    case (0)
       if (x <= tiny(1._default)) then
          y = 0
          f = 0
       else
          y = x ** power
          f = f * power * y / x
       end if
    case (1)
       xbar = 1 - x
       if (xbar <= tiny(1._default)) then
          ybar = 0
          f = 0
       else
          ybar = xbar ** power
          f = f * power * ybar / xbar
       end if
       y = 1 - ybar
    case default
       y = x
    end select
  end subroutine power_mapping

  subroutine strfun_ISR (factor, x, eps, LLA_order, map)
    real(kind=default), intent(inout) :: factor, x
    real(kind=default), intent(in) :: eps
    integer, intent(in) :: LLA_order
    logical, intent(in) :: map
    real(kind=default) :: f
    real(kind=default) :: xx, xxbar, xbar, log_x, log_xbar, x_2
    real(kind=default), parameter :: &
         & xmin = 0.00714053329734592839549810275603_default
    real(kind=default), parameter :: &
         & zeta3 = 1.20205690315959428539973816151_default
    real(kind=default), parameter :: &
         & g1 = 3._default / 4._default, &
         & g2 = (27 - 8*pi**2) / 96._default, &
         & g3 = (27 - 24*pi**2 + 128*zeta3) / 384._default
    if (map) then
       xx = x
       xxbar = 1 - xx
       if (xxbar < tiny(1._default)**eps) then
          xbar = 0
       else
          xbar = xxbar**(1/eps)
       end if
       x = 1 - xbar
    else
       xbar = 1 - x
       factor = factor * eps * xbar**(eps-1)
    end if
    if (LLA_order > 0) then
       f = 1 + g1 * eps
       x_2 = x*x
       if (xxbar>0)  f = f - (1-x_2) / (2 * xxbar)
       if (LLA_order > 1) then
          f = f + g2 * eps**2
          if (xxbar>0 .and. xbar>0 .and. x>xmin) then
             log_x = log(x)
             log_xbar = log(xbar)
             f = f - ((1+3*x_2)*log_x + xbar * (4*(1+x)*log_xbar + 5 + x)) &
                  & / ( 8 * xxbar) * eps
          end if
          if (LLA_order > 2) then
             f = f + g3 * eps**3
             if (xxbar>0 .and. xbar>0 .and. x>xmin) then
                f = f - ((1+x) * xbar &
                     &     * (6 * Li2(x) + 12 * log_xbar**2 - 3 * pi**2) &
                     &   + 1.5_default * (1 + 8*x + 3*x_2) * log_x &
                     &   + 6 * (x+5) * xbar * log_xbar &
                     &   + 12 * (1+x_2) * log_x * log_xbar &
                     &   - (1 + 7*x_2) * log_x**2 / 2 &
                     &   + (39 - 24*x - 15*x_2) / 4) &
                     &  / ( 48 * xxbar) * eps**2
             end if
          end if
       end if
       factor = factor * f
    end if
  end subroutine strfun_ISR

  function Li2 (x)
    use kinds, only: double !NODEP!
    real(kind=default), intent(in) :: x
    real(kind=default) :: Li2
    Li2 = real( Li2_double (real(x, kind=double)), kind=default)
  end function Li2

  function Li2_double (x)  result (Li2)
    use kinds, only: double !NODEP!
    real(kind=double), intent(in) :: x
    real(kind=double) :: Li2
    real(kind=double), parameter :: pi2_6 = pi**2/6
    if (abs(1-x) < 1.E-13_double) then
       Li2 = pi2_6
    else if (abs(1-x) <  0.5_double) then
       Li2 = pi2_6 - log(1-x) * log(x) - Li2_restricted (1-x)
    else if (abs(x).gt.1.d0) then
       call msg_bug (" Dilogarithm called outside of defined range.")
!       Li2 = -pi2_6 - 0.5_default * log(-x) * log(-x) - Li2_restricted (1/x)
    else
       Li2 = Li2_restricted (x)
    end if
  contains
    function Li2_restricted (x) result (Li2)
      real(kind=double), intent(in) :: x
      real(kind=double) :: Li2
      real(kind=double) :: tmp, z, z2
      z = - log (1-x)
      z2 = z**2
! Horner's rule for the powers z^3 through z^19
      tmp = 43867._double/798._double
      tmp = tmp * z2 /342._double - 3617._double/510._double
      tmp = tmp * z2 /272._double + 7._double/6._double
      tmp = tmp * z2 /210._double - 691._double/2730._double
      tmp = tmp * z2 /156._double + 5._double/66._double
      tmp = tmp * z2 /110._double - 1._double/30._double
      tmp = tmp * z2 / 72._double + 1._double/42._double
      tmp = tmp * z2 / 42._double - 1._double/30._double
      tmp = tmp * z2 / 20._double + 1._double/6._double
! The first three terms of the power series
      Li2 = z2 * z * tmp / 6._double - 0.25_double * z2 + z
    end function Li2_restricted
  end function Li2_double

  subroutine strfun_EPA (x, f, map, bypass, x0, x1, c0, c1, msq, qmaxsq, s, ok)
    real(kind=default), intent(inout) :: x, f
    logical, intent(in) :: map, bypass
    real(kind=default), intent(in) :: x0, x1, c0, c1, msq, qmaxsq, s
    logical, intent(out) :: ok
    real(kind=default) :: l, d, lx, lx0, lx1, qminsq
    real(kind=default) :: xbar, tmp
    ok = .true.
    if (map) then
       L = log(Qmaxsq/msq)
       lx0 = log(x0);  lx1 = log(x1)
       D = L**2 - 4*(x * lx1 * (L-lx1) + (1-x) * lx0 * (L-lx0))
       if (D <= 0) then
          ok = .false.; return
       end if
       lx = (L - sqrt(D))/2
       x = exp(lx)
       tmp = L - 2*lx
       if (tmp <= 0) then
          ok = .false.; return
       end if
       f = f / tmp
    else if (.not.bypass) then
       x = x * x1 + (1-x) * x0
       if (x <= 0) then
          ok = .false.; return
       end if
       f = f / x
    else
       if (x < x0 .or. x1 < x) then
          ok = .false.; return
       end if
    end if
    xbar = 1 - x
    if (xbar <= 0) then
       ok = .false.; return
    end if
    Qminsq = msq * x**2 / xbar
    if (Qminsq >= Qmaxsq) then
       ok = .false.; return
    end if
    f = f  * (  (1-x+x**2/2)*log(Qmaxsq/Qminsq) &
         &    - (1-x/2)**2 * log((x**2 + 4*Qmaxsq/s) / (x**2 + 4*Qminsq/s)) &
         &    - (1-x) * (1 - Qminsq/Qmaxsq) ) &
         & * ( c1 - c0 ) 
  end subroutine strfun_EPA

  subroutine strfun_EWA &
       (x, f, fp, fm, fL, &
       map, bypass, x0, x1, pt2max, cv, ca, coeff, msq, sqrts, ok)
    real(kind=default), intent(inout) :: x, f
    real(kind=default), intent(out) :: fp, fm, fL
    logical, intent(in) :: map, bypass
    real(kind=default), intent(in) :: x0, x1, pt2max, cv, ca, coeff, msq, sqrts
    logical, intent(out) :: ok
    real(kind=default) :: xbar, c1, c2, fsum, lx0, lx1, lx, pt2
    ok = .true.
    if (map) then
       lx0 = log(x0);  lx1 = log(x1)
       lx = lx1 * x + lx0 * (1-x)
       x = exp(lx)
       f = f * coeff * (lx1 - lx0)
    else if (.not.bypass) then
       x = x1 * x + x0 * (1-x)
       if (x <= 0) then
          ok = .false.; return
       end if
       f = f * coeff / x * (x1 - x0)
    else
       if (x < x0 .or. x1 < x) then
          ok = .false.; return
       end if
    end if
    xbar = 1 - x
    if (xbar <= 0) then
       ok = .false.; return
    end if
    pt2 = min (pt2max, (xbar * sqrts / 2)**2)
    c1 = log (1 + pt2 / (xbar * msq))
    c2 = 1 / (1 + (xbar * msq) / pt2)
    fm = ((cv + ca)**2 + ((cv - ca) * xbar)**2) / 2 * (c1 - c2)
    fp = ((cv - ca)**2 + ((cv + ca) * xbar)**2) / 2 * (c1 - c2)
    fL = (cv**2 + ca**2) * 2 * xbar * c2
    fsum = fp + fm + fL
    f = f * fsum
    if (fsum /= 0) then
       fp = fp / fsum
       fm = fm / fsum
       fL = fL / fsum
    end if
  end subroutine strfun_EWA


end module beams
