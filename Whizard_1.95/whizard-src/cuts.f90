! WHIZARD 1.95 Feb 25 2010
! 
! (C) 1999-2008 by 
!     Wolfgang Kilian <wolfgang.kilian@desy.de>
!     Thorsten Ohl <ohl@physik.uni-wuerzburg.de>
!     Juergen Reuter <juergen.reuter@physik.uni-freiburg.de>
!
! WHIZARD is free software; you can redistribute it and/or modify it
! under the terms of the GNU General Public License as published by 
! the Free Software Foundation; either version 2, or (at your option)
! any later version.
!
! WHIZARD is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! This file has been stripped of most comments.  For documentation, refer
! to the source 'whizard.nw'

module cuts

  use kinds, only: default !NODEP!
  use limits, only: CUT_WINDOWS_MAX, CUTS_MAX, CUT_MODE_STRLEN, CUT_TEX_STRLEN
  use limits, only: FILENAME_LEN
  use limits, only: TC
  use mpi90, only: mpi90_broadcast !NODEP!
  use files, only: concat
  use user, only: user_cut => cut
  use constants
  use lorentz
  use particles
  use events
  use file_utils !NODEP!
  use diagnostics, only: msg_buffer
  use diagnostics, only: msg_result, msg_message, msg_error
  use parser
  use parameters, only: parameter_set !NODEP!

  implicit none
  private

  integer, parameter, public :: NO_CUT = 0
  integer, parameter, public :: M_CUT = 1
  integer, parameter, public :: LM_CUT = 6
  integer, parameter, public :: MSQ_CUT = 8
  integer, parameter, public :: E_CUT = 2
  integer, parameter, public :: LE_CUT = 7
  integer, parameter, public :: PT_CUT = 3
  integer, parameter, public :: LPT_CUT = 13
  integer, parameter, public :: PL_CUT = 4
  integer, parameter, public :: P_CUT = 5
  integer, parameter, public :: RAP_CUT = 9
  integer, parameter, public :: ETA_CUT = 10
  integer, parameter, public :: A_CUT = -1
  integer, parameter, public :: CT_CUT = -2
  integer, parameter, public :: AD_CUT = -3
  integer, parameter, public :: VBF_CUT = -14
  integer, parameter, public :: AA_CUT = 21
  integer, parameter, public :: CTA_CUT = 20
  integer, parameter, public :: AAD_CUT = 22
  integer, parameter, public :: PHI_CUT = 11
  integer, parameter, public :: PHID_CUT = 12
  integer, parameter, public :: DPHI_CUT = -4
  integer, parameter, public :: DPHID_CUT = -5
  integer, parameter, public :: ASTAR_CUT = -11
  integer, parameter, public :: CTSTAR_CUT = -12
  integer, parameter, public :: ADSTAR_CUT = -13
  integer, parameter, public :: DETA_CUT = -10
  integer, parameter, public :: CONE_CUT = -20
  integer, parameter, public :: LOG_CONE_CUT = -21

  type, public :: cut
     private
     integer :: mode, level
     integer(kind=TC), dimension(2) :: code
     integer :: n_windows
     real(kind=default), dimension(CUT_WINDOWS_MAX) :: lower
     real(kind=default), dimension(CUT_WINDOWS_MAX) :: upper
  end type cut
  type, public :: cut_array
     private
     integer :: n_cuts
     type(cut), dimension(CUTS_MAX) :: c
     integer :: user_cut_mode
  end type cut_array
  type, public :: default_cuts
     real(kind=default) :: default_jet_cut
     real(kind=default) :: default_mass_cut
     real(kind=default) :: default_energy_cut
     real(kind=default) :: default_q_cut
  end type default_cuts


  public :: cut_number
  public :: create, destroy
  public :: read, write
  public :: tex_write
  public :: mpi90_broadcast
  public :: add
  public :: make_cuts
  public :: set_user_cut
  public :: apply
  public :: calculate_momentum_sum

  interface create
     module procedure cut_array_create
  end interface
  interface destroy
     module procedure cut_array_destroy
  end interface
  interface read
     module procedure cut_array_read_unit
     module procedure cut_array_read_name
  end interface
  interface write
     module procedure cut_write_unit
     module procedure cut_array_write_unit
  end interface
  interface tex_write
     module procedure cut_array_tex_write_unit
     module procedure cut_tex_write_unit
  end interface
  interface mpi90_broadcast
     module procedure cut_array_broadcast
  end interface
  interface add
     module procedure cut_add_multi
     module procedure cut_add_single
  end interface
  interface apply
     module procedure cut_array_apply
     module procedure cut_apply
  end interface

contains

  function cut_number(cc) result(n)
    type(cut_array), intent(in) :: cc
    integer :: n
    n = cc%n_cuts
  end function cut_number

  subroutine cut_array_create(cc)
    type(cut_array), intent(out) :: cc
    cc%n_cuts = 0
  end subroutine cut_array_create
  subroutine cut_array_destroy(cc)
    type(cut_array), intent(inout) :: cc
    cc%n_cuts = 0
  end subroutine cut_array_destroy

  subroutine cut_array_read_name(name, cc, process_id, level)
    character(len=*), intent(in) :: name
    type(cut_array), intent(inout) :: cc
    character(len=*), intent(in) :: process_id
    integer, intent(in) :: level
    integer :: unit
    logical :: ok
    ok = .false.
    unit = free_unit()
    call msg_message (" Reading cut configuration data from file " // name)
    open(unit=unit, file=name, status='old', action='read')
      FIND_PROCESS: do
         call find_string(unit, 'process', ok=ok)
         if (.not.ok) exit FIND_PROCESS
         call find_string(unit, process_id, &
              & reject=(/ 'process  ', 'grove    ', 'tree     ', &
              &           'cut      ', 'helicity ', 'flavor   ', 'color    ', &
              &           'histogram', 'and      ' /), &
              & ok=ok)
         if (ok) exit FIND_PROCESS
      end do FIND_PROCESS
    if (ok) then
       if (level==1 .and. cc%n_cuts > 0)  call msg_message &
            & (" Replacing default cuts by user-defined cuts.")
       cc%n_cuts = 0
       call cut_array_read_unit(unit, cc, level)
    else
       call msg_message (" No cut data found for process " // process_id)
    end if
    call lex_clear
    close(unit)
    if (level==1 .and. .not.ok .and. cc%n_cuts > 0) then
       call msg_message (" Using default cuts.")
       call cut_array_write_unit(6, cc)
    end if
  end subroutine cut_array_read_name

  subroutine cut_array_read_unit(unit, cc, level)
    integer, intent(in) :: unit
    type(cut_array), intent(inout) :: cc
    integer, intent(in), optional :: level
    logical :: ok
    integer :: iostat
    integer :: mode
    integer(kind=TC), dimension(2) :: code
    character(len=CUT_MODE_STRLEN) :: str
    real(kind=default) :: lower, upper
    logical :: new
    SCAN_CUTS: do
       call find_string(unit, 'cut', &
            & reject=(/ 'process  ', 'color    ', 'helicity ', 'flavor   ', &
            &           'histogram', &
            &           'and      ' /),&
            & ok=ok, iostat=iostat)
       if (.not.ok) exit SCAN_CUTS
       str = get_string_upper_case(unit)
       select case(trim(str))
         case("- ", "--", "---")
            mode = NO_CUT
         case("M","Q")
            mode = M_CUT
         case("LM","LQ")
            mode = LM_CUT
         case("MSQ","QSQ","S","T","U")
            mode = MSQ_CUT
         case("E")
            mode = E_CUT
         case("LE")
            mode = LE_CUT
         case("PT")
            mode = PT_CUT
         case("LPT")
            mode = LPT_CUT
         case("PL")
            mode = PL_CUT
         case("P")
            mode = P_CUT
         case("RAP", "RAPIDITY", "Y")
            mode = RAP_CUT
         case("ETA")
            mode = ETA_CUT
         case("A", "ANGLE", "TH", "THETA")
            mode = A_CUT
         case("CT","COS(TH)","COS(THETA)")
            mode = CT_CUT
         case("AD", "ANGLE(DEG)", "TH(DEG)", "THETA(DEG)")
            mode = AD_CUT
         case("VBF", "VBF_ETA", "HEMI", "HEMISPHERE")
            mode = VBF_CUT
         case("AA", "ANGLE-ABS", "TH-ABS", "THETA-ABS")
            mode = AA_CUT
         case("CTA","COS(TH-ABS)","COS(THETA-ABS)")
            mode = CTA_CUT
         case("AAD", "ANGLE-ABS(DEG)", "TH-ABS(DEG)", "THETA-ABS(DEG)")
            mode = AAD_CUT
         case("PH", "PHI")
            mode = PHI_CUT
         case("PHD", "PHID", "PHI(DEG)")
            mode = PHID_CUT
         case("DPH", "DELTA-PHI")
            mode = DPHI_CUT
         case("DPHD", "DPHID", "DELTA-PHI(DEG)")
            mode = DPHID_CUT
         case("A*", "ANGLE*", "TH*", "THETA*")
            mode = ASTAR_CUT
         case("CT*","COS(TH*)","COS(THETA*)")
            mode = CTSTAR_CUT
         case("AD*", "ANGLE*(DEG)", "TH*(DEG)", "THETA*(DEG)")
            mode = ADSTAR_CUT
         case("DETA", "DELTA-ETA")
            mode = DETA_CUT
         case("DR", "DELTA-R", "CONE")
            mode = CONE_CUT
         case("LDR", "LOG-DELTA-R", "LOG-CONE")
            mode = LOG_CONE_CUT
       case default
          call msg_message (" cut "//trim(str)//" ...")
          call msg_error (" This is not a valid cut specifier.  I'll ignore the cut.")
          mode = NO_CUT
       end select
       if (mode==NO_CUT) exit SCAN_CUTS
       call find_string(unit, 'of', here=.true.)
       code(1) = get_integer(unit)
       code(2) = get_integer(unit, ok)
       if (.not.ok)  code(2) = 0
       if (mode>0 .and. ok) then
          write (msg_buffer, "(1x,A,1x,I5,1x,I5)") &
               "cut "//trim(str)//" of", code(1), code(2)
          call msg_message
          call msg_error (" Two integer arguments found, only one expected.  I'll ignore the cut.")
          exit SCAN_CUTS
       else if (mode<0 .and. .not.ok) then
          write (msg_buffer, "(1x,A,1x,I5)")  "cut "//trim(str)//" of", code(1)
          call msg_message
          call msg_error (" One integer argument found, two expected. I'll ignore the cut.")
          exit SCAN_CUTS
       end if
       new = .true.
       call find_string(unit, 'within', here=.true.)
       SCAN_CUT_WINDOWS: do
          lower = get_real(unit)
          upper = get_real(unit)
          if (mode/=NO_CUT) call add(cc, mode, code, lower, upper, new, level)
          call find_string(unit, 'or', here=.true., ok=ok, &
               &           iostat=iostat)
          if (.not.ok) exit SCAN_CUT_WINDOWS
          new = .false.
       end do SCAN_CUT_WINDOWS
    end do SCAN_CUTS
  end subroutine cut_array_read_unit

  subroutine cut_array_write_unit(u, cc, level)
    integer, intent(in) :: u
    type(cut_array), intent(in) :: cc
    integer, intent(in), optional :: level
    integer :: i
    integer :: lv
    lv = 0
    do i=1, cc%n_cuts
       if (present(level)) then
          if (cc%c(i)%level == level) call cut_write_unit(u, cc%c(i))
       else
          if (cc%c(i)%level /= lv) then
             lv = cc%c(i)%level
             write (msg_buffer, "(1x,A,1x,I1)") "integration pass", lv
             call msg_message (unit=u)
          end if
          call cut_write_unit(u, cc%c(i))
       end if
    end do
  end subroutine cut_array_write_unit

  subroutine cut_write_unit(u, c)
    integer, intent(in) :: u
    type(cut), intent(in) :: c
    character(len=CUT_MODE_STRLEN) :: str
    integer :: i, pos
    select case(c%mode)
    case(M_CUT);  str = "M"
    case(LM_CUT);  str = "LM"
    case(MSQ_CUT);  str = "MSQ"
    case(E_CUT);  str = "E "
    case(LE_CUT);  str = "LE"
    case(PT_CUT);  str = "pT"
    case(LPT_CUT);  str = "pT"
    case(PL_CUT);  str = "pL"
    case(P_CUT);  str = "p "
    case(RAP_CUT);  str = "y"
    case(ETA_CUT);  str = "eta"
    case(A_CUT);  str = "theta"
    case(CT_CUT);  str = "cos(theta)"
    case(AD_CUT);  str = "theta(DEG)"
    case(VBF_CUT); str = "eta*eta"
    case(AA_CUT);  str = "theta(abs)"
    case(CTA_CUT);  str = "cos(theta(abs))"
    case(AAD_CUT);  str = "theta(DEG)"
    case(PHI_CUT);  str = "phi"
    case(PHID_CUT);  str = "phi(DEG)"
    case(DPHI_CUT);  str = "delta-phi"
    case(DPHID_CUT);  str = "dphi(DEG)"
    case(ASTAR_CUT);  str = "theta*"
    case(CTSTAR_CUT);  str = "cos(theta*)"
    case(ADSTAR_CUT);  str = "theta*(DEG)"
    case(DETA_CUT);  str = "delta-eta"
    case(CONE_CUT);  str = "delta-r"
    case(LOG_CONE_CUT);  str = "log-delta-r"
    end select
    if (c%mode /= NO_CUT) then
       msg_buffer = " cut " // trim(str) // " of"
       pos = len_trim (msg_buffer)
       write (msg_buffer(pos+1:), "(1x,I4)")  c%code(1)
       pos = pos + 5
       if (c%code(2)/=0) write (msg_buffer(pos+1:), "(1x,I4)")  c%code(2)
       pos = pos + 5
       msg_buffer (pos+1:) = " within"
       pos = len_trim (msg_buffer)
       do i=1, c%n_windows
          if (i>1) then
             msg_buffer(pos+1:) = " or"
             pos = len_trim (msg_buffer)
          end if
          write (msg_buffer(pos+1:), "(2(1x,1PE12.5))") c%lower(i), c%upper(i)
          pos = len_trim (msg_buffer)
       end do
       call msg_result (unit=u)
    end if
  end subroutine cut_write_unit

  subroutine cut_array_tex_write_unit(u, cc)
    integer, intent(in) :: u
    type(cut_array), intent(in) :: cc
    integer :: i
    integer :: level
    level = 0
    write(u, '(A)') "\begin{tabular}{l|cc}"
    write(u, '(A)') "\textbf{Cut} & Lower bound & Upper bound\\"
    level = 0
    do i=1, cc%n_cuts
       if (cc%c(i)%level /= level) then
          level = cc%c(i)%level
          write(u, '(A)') "\hline"
       end if
       call cut_tex_write_unit(u, cc%c(i))
    end do
    write(u, '(A)') "\end{tabular}"
  end subroutine cut_array_tex_write_unit

  subroutine cut_tex_write_unit(u, c)
    integer, intent(in) :: u
    type(cut), intent(in) :: c
    character(len=CUT_TEX_STRLEN) :: str, unit
    integer :: i
    unit = ""
    select case(c%mode)
    case(M_CUT)
       str  = "$M_{\rm inv}$"
       unit = "[\textrm{GeV}]"
    case(LM_CUT)
       str  = "$\log_{10}M_{\rm inv}/\textrm{GeV}$"
    case(MSQ_CUT)
       str  = "$M^2_{\rm inv}$"
       unit = "[$\textrm{GeV}^2$]"
    case(E_CUT)
       str  = "$E$"
       unit = "[\textrm{GeV}]"
    case(LE_CUT)
       str  = "$\log_{10}E/\textrm{GeV}$"
    case(PT_CUT)
       str  = "$p_\perp$"
       unit = "[\textrm{GeV}]"
    case(LPT_CUT)
       str  = "$\log_{10}p_\perp/\textrm{GeV}$"
    case(PL_CUT)
       str = "$p_\parallel$"
       unit = "[\textrm{GeV}]"
    case(P_CUT)
       str  = "$|\vec p\,|$"
       unit = "[\textrm{GeV}]"
    case(RAP_CUT)
       str  = "$y$"
       unit = "(rap.)"
    case(ETA_CUT) 
          str  = "$\eta$"
    case(A_CUT)
       str  = "$\theta$"
       unit = "[\textrm{rad}]"
    case(CT_CUT)
       str  = "$\cos\theta$"
    case(AD_CUT)
       str  = "$\theta$"
       unit = "[\textrm{deg.}]"
    case(VBF_CUT)
       str  = "$\eta\times\eta$"
    case(AA_CUT)
       str  = "$\theta_{\rm abs}$"
       unit = "[\textrm{rad}]"
    case(CTA_CUT)
       str  = "$\cos\theta_{\rm abs}$"
    case(AAD_CUT)
       str  = "$\theta$"
       unit = "[\textrm{deg.}]"
    case(PHI_CUT)
       str  = "$\phi$"
       unit = "[\textrm{rad}]"
    case(PHID_CUT)
       str  = "$\phi$"
       unit = "[\textrm{deg.}]"
    case(DPHI_CUT)
       str  = "$\Delta\phi$"
       unit = "[\textrm{rad}]"
    case(DPHID_CUT)
       str  = "$\Delta\phi$"
       unit = "[\textrm{deg.}]"
    case(ASTAR_CUT)
       str  = "$\theta^*$"
       unit = "[\textrm{rad}]"
    case(CTSTAR_CUT)
       str  = "$\cos\theta^*$"
    case(ADSTAR_CUT)
       str  = "$\theta^*$"
       unit = "[\textrm{deg.}]"
    case(DETA_CUT) 
       str = "$\Delta\eta$"
    case(CONE_CUT)
       str = "$\sqrt{\Delta\eta^2+\Delta\phi^2}$"
    case(LOG_CONE_CUT)
       str = "$\log_{10}\sqrt{\Delta\eta^2+\Delta\phi^2}$"
    end select
    if (c%mode /= NO_CUT) then
       if (c%code(2)==0) then
          write(u, '(2x,A,1x,A,1x,A,1x,I4,A)', advance='no') &
               & trim(str), trim(unit), ' of $(', c%code(1), ')$'
       else
          write(u, '(2x,A,1x,A,1x,A,1x,I4,A,I4,A)', advance='no') &
               & trim(str), trim(unit), ' of $(', c%code(1), &
               & ', ', c%code(2), ')$'
       end if
       write(u, '(4x,A)', advance='no') ' & '
       do i=1, c%n_windows
          if (i>1) write(u, '(A)', advance='no') '\hfill or & '
          write(u, '(A,G12.5,A,G12.5,A)', advance='no') &
               & '$', c%lower(i), '$ & $', c%upper(i), '$\\'
       end do
    end if
    write(u,*)
  end subroutine cut_tex_write_unit

  subroutine cut_array_broadcast(cc, root, domain, error)
    type(cut_array), intent(inout) :: cc
    integer, intent(in) :: root
    integer, intent(in), optional :: domain
    integer, intent(out), optional :: error
!     integer, dimension(:), allocatable :: buffer
!     allocate(buffer(size(transfer(cc, buffer))))
!     buffer = transfer(cc, buffer)
!     call mpi90_broadcast(buffer, root, domain, error)
!     cc = transfer(buffer, cc)
!     deallocate(buffer)
    if (present(error))  error = 0
  end subroutine cut_array_broadcast

  subroutine cut_add_multi(cc, mode, code, lower, upper, new, level)
    type(cut_array), intent(inout) :: cc
    integer, intent(in) :: mode
    integer(kind=TC), dimension(2), intent(in) :: code
    real(kind=default), intent(in) :: lower, upper
    logical, intent(in) :: new
    integer, intent(in), optional :: level
    if (new) then
       if (cc%n_cuts >= CUTS_MAX) then
          write (msg_buffer, "(1x,A,1x,I3,1x,A)") &
               & "I can't handle more than", CUTS_MAX, "cuts.  I'll ignore the rest."
          call msg_error
          return
       end if
       cc%n_cuts = cc%n_cuts+1
       cc%c(cc%n_cuts)%n_windows = 0
    end if
    call cut_add_single(cc%c(cc%n_cuts), mode, code, lower, upper, level)
  end subroutine cut_add_multi

  subroutine cut_add_if_new (cc, mode, code, lower, upper)
    type(cut_array), intent(inout) :: cc
    integer, intent(in) :: mode
    integer(kind=TC), dimension(2), intent(in) :: code
    real(kind=default), intent(in) :: lower, upper
    integer :: i
    logical :: exists
    exists = .false.
    SCAN_CUTS: do i = 1, cc%n_cuts
       if (cc%c(i)%mode == mode .and. all (cc%c(i)%code == code)) then
          exists = .true.
          exit SCAN_CUTS
       end if
    end do SCAN_CUTS
    if (.not. exists) &
         call cut_add_multi (cc, mode, code, lower, upper, .true., 0)
  end subroutine cut_add_if_new

  subroutine cut_add_single(c, mode, code, lower, upper, level)
    type(cut), intent(inout) :: c
    integer, intent(in) :: mode
    integer(kind=TC), dimension(2), intent(in) :: code
    real(kind=default), intent(in) :: lower, upper
    integer, intent(in), optional :: level
    if (c%n_windows >= CUT_WINDOWS_MAX) then
       call write(6,c)
       write (msg_buffer, "(1x,A,1x,I2,1x,A)") &
            & "There can only be", CUT_WINDOWS_MAX, "windows for this cut.  I'll ignore the rest."
       call msg_error
       return
    end if
    c%mode = mode
    c%code = code
    c%n_windows = c%n_windows+1
    if (lower <= upper) then
       c%lower(c%n_windows) = lower
       c%upper(c%n_windows) = upper
    else
       c%lower(c%n_windows) = upper
       c%upper(c%n_windows) = lower
    end if
    if (present(level)) then
       c%level = level
    else
       c%level = 0
    end if
  end subroutine cut_add_single

  subroutine make_cuts &
       & (prefix, filename, process_id, cc, code, &
       &  n_in, n_out, n_strfun, par, cuts)
    character(len=*), intent(in) :: prefix, filename, process_id
    type(cut_array), intent(inout) :: cc
    integer, dimension(:,:), intent(in) :: code
    integer, intent(in) :: n_in, n_out, n_strfun
    type(parameter_set), intent(in) :: par
    type(default_cuts), intent(in) :: cuts
    character(len=FILENAME_LEN) :: file
    integer(kind=TC), dimension(size(code)) :: bincode
    logical, dimension(size(code)) :: charged, colored, massive
    logical :: q_cut
    integer :: f, i, j, n, n_cuts0, u
    real(kind=default), parameter :: INFINITY = 1.E99_default
    n_cuts0 = cc%n_cuts
    n = size (code, 1)
    do f = 1, size (code, 2)
       do i=1, n 
          massive(i) = particle_mass(code(i,f), par) /= 0
          charged(i) = particle_3charge(code(i,f)) /= 0
          colored(i) = particle_color(code(i,f)) /= 0
       end do
       do i=1, n_out
          bincode(n_in+i) = ibset(0, i-1)
       end do
       do i=1, n_in
          bincode(i) = ibset(0, n-i)
       end do
       ! s-channel cuts: invariant mass, energy
       if (n_strfun > 0 .or. n_out > 2) then
          do i=n_in+1, n
             do j=i+1, n
                if (.not.massive(i) .and. .not.massive(j)) then
                   if (colored(i) .and. colored(j)) then
                      call cut_add_if_new &
                           (cc, M_CUT, (/ior(bincode(i),bincode(j)), 0/), &
                            cuts%default_jet_cut, INFINITY)
                   else if (charged(i) .and. code(i,f)== -code(j,f)) then
                      call cut_add_if_new &
                           (cc, M_CUT, (/ior(bincode(i),bincode(j)), 0/), &
                            cuts%default_mass_cut, INFINITY)
                   else if (charged(i) .and. code(j,f)==PHOTON) then
                      call cut_add_if_new &
                           (cc, M_CUT, (/ior(bincode(i),bincode(j)), 0/), &
                            cuts%default_mass_cut, INFINITY)
                   else if (charged(j) .and. code(i,f)==PHOTON) then
                      call cut_add_if_new &
                           (cc, M_CUT, (/ior(bincode(i),bincode(j)), 0/), &
                            cuts%default_mass_cut, INFINITY)
                   end if
                end if
             end do
          end do
          do i=n_in+1, n
             if (code(i,f)==PHOTON .and. any(charged .and. massive)) then
                call cut_add_if_new &
                     (cc, E_CUT, (/bincode(i), 0/), &
                      cuts%default_energy_cut, INFINITY)
             else if (code(i,f)==GLUON .or. code(i,f)==GLUON2 .and. &
                  &   any(colored .and. massive)) then
                call cut_add_if_new &
                     (cc, E_CUT, (/bincode(i), 0/), &
                      cuts%default_energy_cut, INFINITY)
             end if
          end do
       end if
       ! t-channel cuts: q value
       if (n_in == 2) then
          if (all (charged(:n_in) .or. code(:n_in,f)==PHOTON &
               .or. colored(:n_in))) then
             do i=1, n_in
                do j=n_in+1, n
                   q_cut = .false.
                   if (code(i,f)==code(j,f)) then
                      q_cut = .true.
                   else if (code(i,f)==PHOTON) then
                      q_cut = .not.massive(j) .and. charged(j)
                   else if (code(j,f)==PHOTON) then
                      q_cut = .not.massive(i) .and. charged(i)
                   else if (code(i,f)==GLUON .or. code(i,f)==GLUON2) then
                      q_cut = .not.massive(j) .and. colored(j)
                   else if (code(j,f)==GLUON .or. code(j,f)==GLUON2) then
                      q_cut = .not.massive(i) .and. colored(i)
                   end if
                   if (q_cut) then
                      call cut_add_if_new &
                           (cc, M_CUT, (/ior(bincode(i),bincode(j)), 0/), &
                            -INFINITY, -cuts%default_Q_cut)
                   end if
                end do
             end do
          end if
       end if
       if (cc%n_cuts > n_cuts0) then
          if (n_in == 1) then
             call msg_message &
                  & (" Note: This partial width may be infinite without cuts.")
          else
             call msg_message &
                  & (" Note: This cross section may be infinite without cuts.")
          end if
          u = free_unit ()
          file = concat (prefix, filename, process_id, "cut0")
          open (u, file=trim(file), action="write", status="replace")
          call msg_message (" Automatically generated set of cuts", unit=u)
          call write_process (u, process_id, code, n_in, n_out)
          write (u, '(1x,A,1x,A)') "process", process_id
          call write (u, cc)
          close (u)
          call msg_message (" Wrote default cut configuration file "//trim(file))
       end if
    end do
  end subroutine make_cuts
    
  subroutine set_user_cut (cc, mode)
    type(cut_array), intent(inout) :: cc
    integer, intent(in) :: mode
    cc%user_cut_mode = mode
  end subroutine set_user_cut

  subroutine cut_array_apply (cc, evt, ok)
    type(cut_array), intent(in) :: cc
    type(event), intent(in) :: evt
    logical, intent(out) :: ok
    integer :: i
    if (cc%user_cut_mode /= 0) then
       ok = user_cut (particle_four_momentum (evt%prt(1:)), &
            &         particle_code (evt%prt(1:)), &
            &         cc%user_cut_mode)
    else
       ok = .true.
    end if
    if (.not.ok) return
    do i=1, cc%n_cuts
       call cut_apply (cc%c(i), evt, ok)
       if (.not.ok) return
    end do
  end subroutine cut_array_apply

  subroutine cut_apply(c, evt, ok)
    type(cut), intent(in) :: c
    type(event), intent(in) :: evt
    logical, intent(out) :: ok
    type(four_momentum), dimension(2) :: p
    integer :: i
    ok = .true.
    do i=1, 2
       if (c%code(i)/=0) call calculate_momentum_sum(p(i), evt, c%code(i))
    end do
    do i=1, c%n_windows
       call cut_apply_window(c%mode, c%lower(i), c%upper(i), p, ok)
       if (ok) exit
    end do
  end subroutine cut_apply

  subroutine cut_apply_window(mode, lower, upper, p, ok)
    integer, intent(in) :: mode
    real(kind=default), intent(in) :: lower, upper
    type(four_momentum), dimension(2), intent(in) :: p
    logical, intent(out) :: ok
    real(kind=default) :: value
    value = 0._default
    select case(mode)
    case(M_CUT)
       value = p(1)**1
    case(LM_CUT)
       value = abs(p(1)**1)
       if (value/=0) then
          value = log10(value)
       else
          value = -huge(1._default)
       end if
    case(MSQ_CUT)
       value = p(1)**2
    case(E_CUT)
       value = energy(p(1))
    case(LE_CUT)
       value = energy(p(1))
       if (value>0) then
          value = log10(value)
       else
          value = -huge(1._default)
       end if
    case(PT_CUT)
       value = transverse_momentum(p(1))
    case(LPT_CUT)
       value = transverse_momentum(p(1))
       if (value>0) then
          value = log10(value)
       else
          value = -huge(1._default)
       end if
    case(PL_CUT)
       value = longitudinal_momentum(p(1))
    case(P_CUT)
       value = space_part(p(1))**1
    case(RAP_CUT)
       value = rapidity (p(1))
    case(ETA_CUT)
       value = pseudorapidity (p(1))
    case(A_CUT)
       value = angle (p(1), p(2))
    case(CT_CUT)
       value = angle_ct (p(1), p(2))
    case(AD_CUT)
       value = angle (p(1), p(2)) / degree
       if (value>180)  value = 180
    case(VBF_CUT)
       value = pseudorapidity (p(1)) * pseudorapidity (p(2))
    case(AA_CUT)
       value = polar_angle (p(1))
    case(CTA_CUT)
       value = polar_angle_ct (p(1))
    case(AAD_CUT)
       value = polar_angle (p(1)) / degree
       if (value>180)  value = 180
    case(PHI_CUT)
       value = azimuthal_angle (p(1))
    case(PHID_CUT)
       value = azimuthal_angle (p(1)) / degree
       if (value>360) then
          value = 360
       else if (value<0) then
          value = 0
       end if
    case(DPHI_CUT)
       value = azimuthal_distance (p(1), p(2))
    case(DPHID_CUT)
       value = azimuthal_distance (p(1), p(2)) / degree
       if (value>180) then
          value = 180
       else if (value<-180) then
          value = -180
       end if
    case(ASTAR_CUT)
       if (invariant_mass(p(2)) > 0) then
          value = angle (space_part (boost(-p(2), invariant_mass(p(2))) &
               &         * p(1)), &
               &         space_part(p(2)))
       else
          value = 0
       end if
    case(CTSTAR_CUT)
       if (invariant_mass(p(2)) > 0) then
          value = angle_ct (space_part(boost(-p(2), invariant_mass(p(2))) &
               &            * p(1)), &
               &            space_part(p(2)))
       else
          value = 0
       end if
    case(ADSTAR_CUT)
       if (invariant_mass(p(2)) > 0) then
          value = angle (space_part(boost(-p(2), invariant_mass(p(2))) &
               &         * p(1)), &
               &         space_part(p(2))) / degree
          if (value>180)  value = 180
       else
          value = 0
       end if
    case(DETA_CUT)
       value = pseudorapidity_distance (p(1), p(2))
    case(CONE_CUT)
       value = eta_phi_distance (p(1), p(2))
    case(LOG_CONE_CUT)
       value = log10 (eta_phi_distance (p(1), p(2)))
    end select
    ok = (lower <= value .and. value <= upper)
  end subroutine cut_apply_window

  subroutine calculate_momentum_sum(p, evt, code)
    type(four_momentum), intent(out) :: p
    type(event), intent(in) :: evt
    integer(kind=TC) :: code, abs_code
    integer :: i
    p = four_momentum_null
    abs_code = abs (code)
    do i=1, evt%n_out
       if (btest(abs_code, i-1)) p = p + particle_four_momentum(evt%prt(i))
    end do       
    if (evt%scattering) then
       do i=1, 2
          if (btest(abs_code, evt%n_out+2-i)) then
             if (code > 0) then
                p = p - particle_four_momentum (evt%prt(-i))
             else
                p = p - particle_four_momentum (evt%beam_particle(i))
             end if
          end if
       end do
    else
       if (btest(abs_code, evt%n_out)) then
          p = p - particle_four_momentum(evt%prt(0))
       end if
    end if
  end subroutine calculate_momentum_sum


end module cuts
