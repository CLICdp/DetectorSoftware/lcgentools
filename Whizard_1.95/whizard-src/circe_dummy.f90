! WHIZARD 1.95 Feb 25 2010
! 
! (C) 1999-2008 by 
!     Wolfgang Kilian <wolfgang.kilian@desy.de>
!     Thorsten Ohl <ohl@physik.uni-wuerzburg.de>
!     Juergen Reuter <juergen.reuter@physik.uni-freiburg.de>
!
! WHIZARD is free software; you can redistribute it and/or modify it
! under the terms of the GNU General Public License as published by 
! the Free Software Foundation; either version 2, or (at your option)
! any later version.
!
! WHIZARD is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! This file has been stripped of most comments.  For documentation, refer
! to the source 'whizard.nw'
!
! Replacement for the CIRCE subroutines called by WHIZARD
! This file can be linked if CIRCE is absent or disabled.

subroutine circes (xx1m, xx2m, xroots, xacc, xver, xrev, xchat)
  double precision :: xx1m, xx2m, xroots
  integer :: xacc, xver, xrev, xchat
  stop "The CIRCE module is absent.  Please recompile with CIRCE enabled."
end subroutine circes

double precision function kirke (x1, x2, p1, p2)
  implicit none
  double precision x1, x2
  integer p1, p2
  kirke = 0.d0
end function kirke

subroutine gircee (x1, x2, rng)
  implicit none
  double precision x1, x2
  external rng
  entry girceg
  entry gircgg
  x1 = 0.d0
  x2 = 0.d0
end subroutine gircee

