! WHIZARD 1.95 Feb 25 2010
! 
! (C) 1999-2008 by 
!     Wolfgang Kilian <wolfgang.kilian@desy.de>
!     Thorsten Ohl <ohl@physik.uni-wuerzburg.de>
!     Juergen Reuter <juergen.reuter@physik.uni-freiburg.de>
!
! WHIZARD is free software; you can redistribute it and/or modify it
! under the terms of the GNU General Public License as published by 
! the Free Software Foundation; either version 2, or (at your option)
! any later version.
!
! WHIZARD is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! This file has been stripped of most comments.  For documentation, refer
! to the source 'whizard.nw'

module jetset_interface

  use kinds, only: default !NODEP!
  use kinds, only: double !NODEP!
  use file_utils, only: free_unit !NODEP!
  use diagnostics, only: msg_buffer, msg_message, msg_error
  use limits, only: BUFFER_SIZE, PYTHIA_PARAMETERS_LEN
  use parser, only: get_integer, lex_clear

  implicit none
  private

  integer, parameter, public :: PYGIVE_LEN = 100

  type, public :: pythia_init_data
     integer :: n_external
     character(len=BUFFER_SIZE) :: process_id, pyproc, beam, target
     logical :: fixed_energy, show_pymaxi
     real(kind=default) :: sqrts, integral, error
     character(len=PYTHIA_PARAMETERS_LEN) :: chin
  end type pythia_init_data


  public :: jetset_init, pythia_init
  public :: jetset_fragment
  public :: pythia_fragment
  public :: pythia_up_fill
  public :: jetset_open_write, jetset_close
  public :: jetset_write
  public :: jetset_suppress_banner
  public :: pythia_print_statistics

  logical, public :: new_event_needed_pythia = .true.
  integer, public :: number_events_whizard = 0
  integer, public :: number_events_pythia = 0
  integer, dimension(2), public :: number_events_written = 0
!  integer :: mstp61, mstp71

  save

contains

  subroutine jetset_init (chin)
    character(len=*), intent(in) :: chin
    common /PYPARS/ mstp(200),parp(200),msti(200),pari(200)
    integer :: mstp, msti
    real(kind=double) :: parp, pari
    call msg_message (" Initializing PYTHIA ...")
    call pyinit ("NONE", "", "", 0._double)
    mstp(122)=0       ! Second pyinit call silent
    call pyinit ("CMS", "e+", "e-", 5000._double)
    mstp(122)=1       ! Now print it again
    if (chin /= ' ') then
       call call_pygive (chin)
    end if
  end subroutine jetset_init

  subroutine jetset_fragment (color_flow, anticolor_flow)
    integer, dimension(:), intent(in) :: color_flow, anticolor_flow
    integer, dimension(size(color_flow)) :: cflow, aflow
    integer :: i
    call pyhepc (2)
    cflow = color_flow
    aflow = anticolor_flow
    do i=1, size(cflow)
       if (cflow(i)/=0 .and. aflow(i)==0) then
          call make_color_string (i, cflow, aflow)
       end if
    end do
    do i=1, size(cflow)
       if (aflow(i)/=0 .and. cflow(i)==0) then
          call make_color_string (i, aflow, cflow)
       end if
    end do
    do i=1, size(cflow)
       if (cflow(i)/=0 .and. aflow(i)/=0) then
          call make_color_string (i, cflow, aflow)
       end if
    end do
    call pyexec ()
    call pyhepc (1)
  contains
  subroutine make_color_string (i0, flow1, flow2)
    integer, intent(in) :: i0
    integer, dimension(:), intent(inout) :: flow1, flow2
    integer, dimension(size(flow1)) :: ijoin
    integer :: njoin
    integer :: i1, i2, j
    i1 = i0
    ijoin = 0
    njoin = 0
    do j=1, size(ijoin)
       if (i1 <= 2) exit
       ijoin(j) = i1 - 2
       njoin = njoin + 1
       i2 = i1
       i1 = flow1(i2)
       flow1(i2) = 0
       flow2(i2) = 0
    end do
    if (njoin > 1)  call pyjoin (njoin, ijoin)
  end subroutine make_color_string
  end subroutine jetset_fragment

  subroutine set_processes (pyproc)
    character(len=*), intent(in) :: pyproc
    integer :: u, iostat, process
    character(len=BUFFER_SIZE) :: buffer
    u = free_unit ()
    open (u, status="scratch")
    write (u, "(A)") pyproc
    rewind (u)
    SCAN_TOKENS: do
       process = get_integer (u, iostat=iostat)
       if (iostat /= 0)  exit SCAN_TOKENS
       buffer = " "
       write (buffer, "(I4)")  process
       call pygive ("MSUB("//trim(buffer)//") = 1")
    end do SCAN_TOKENS
    close (u, status="delete")
    call lex_clear
  end subroutine set_processes

  subroutine jetset_open_write (unit, file)
    integer, intent(in) :: unit
    character (len=*), intent(in) :: file
    external F77OPN
    call F77OPN (unit, trim(file))
    number_events_written = 0
  end subroutine jetset_open_write

  subroutine jetset_close (unit)
    integer, intent(in) :: unit
    external F77CLS
    call F77CLS (unit)
  end subroutine jetset_close

  subroutine jetset_write (unit, mlist)
    integer, intent(in) :: unit, mlist
    common /PYDAT1/ mstu(200), paru(200), mstj(200), parj(200)
    integer :: mstu, mstj
    real(kind=double) :: paru, parj
    integer :: mstu11, nev
    character(len=BUFFER_SIZE) :: buffer
    external F77WCH
    call pyhepc (2)
    mstu11 = mstu(11)
    mstu(11) = unit
    buffer = " "
    if (unit==6) then
       number_events_written(1) = number_events_written(1) + 1
       nev = number_events_written(1)
    else
       number_events_written(2) = number_events_written(2) + 1
       nev = number_events_written(2)
    end if
    if (new_event_needed_pythia) then
       write (buffer, "(1x,A,1x,I12,3x,A)") "*** Event #", nev, "(WHIZARD):"
    else
       write (buffer, "(1x,A,1x,I12,3x,A)") "*** Event #", nev, "(PYTHIA):"
    end if
    call F77WCH (unit, " ")
    call F77WCH (unit, " ")
    call F77WCH (unit, " ")
    call F77WCH (unit, trim(buffer))
    call jetset_suppress_banner
    call pylist (mlist)
    mstu(11) = mstu11
  end subroutine jetset_write

  subroutine jetset_suppress_banner
    common /PYDAT1/ mstu(200), paru(200), mstj(200), parj(200)
    integer :: mstu, mstj
    real(kind=double) :: paru, parj
    mstu(12) = 0
  end subroutine jetset_suppress_banner

  subroutine call_pygive (string)
    character(len=*), intent(in) :: string
    character(len=PYGIVE_LEN) :: chin
    integer :: pos1, pos2
    pos1 = 1
    do
       pos2 = scan (string(pos1:), ";")
       if (pos2 == 0) then
          chin = string(pos1:)
          call pygive (chin)
       else if (pos2 > PYGIVE_LEN) then
          write (msg_buffer, "(1x,A)") '"'//string(pos1:pos2)//'"'
          call msg_message
          call msg_error &
               & (" Substring of pythia_parameters too long, will be ignored")
          chin = ""
          pos1 = pos1 + pos2 + 1
       else
          chin = string(pos1:pos1+pos2-2)
          call pygive (chin)
          pos1 = pos1 + pos2 + 1
       end if
       if (pos2 == 0) exit
    end do
  end subroutine call_pygive

  subroutine pythia_print_statistics
    call pystat (1)
  end subroutine pythia_print_statistics


  subroutine pythia_up_fill
  end subroutine pythia_up_fill

  subroutine pythia_init (pydat)
    type(pythia_init_data), intent(in) :: pydat
    call msg_message 
    call msg_message (" Initializing PYTHIA ...")
    call set_processes (pydat%pyproc)
    call pyinit ("USER", "", "", 0._double)
    call pygive ("MSTP(11)=0; MSTP(61)=0; MSTP(71)=0") ! Suppress ISR/FSR
    if (pydat%chin /= ' ') call call_pygive (pydat%chin)   ! User intervention
    new_event_needed_pythia = .true.
    number_events_whizard = 0
    number_events_pythia = 0
  end subroutine pythia_init

  subroutine pythia_fragment &
       & (m_dummy, p_dummy, code_dummy, color_flow_dummy, anticolor_flow_dummy)
    real(kind=default), dimension(:), intent(in) :: m_dummy
    real(kind=default), dimension(:,0:), intent(in) :: p_dummy
    integer, dimension(:), intent(in) :: code_dummy
    integer, dimension(:), intent(in) :: color_flow_dummy, anticolor_flow_dummy
    new_event_needed_pythia = .false.
    call pyevnt ()
    call pyhepc (1)
    if (new_event_needed_pythia) then
       number_events_whizard = number_events_whizard + 1
    else
       number_events_pythia = number_events_pythia + 1
    end if
  end subroutine pythia_fragment


end module jetset_interface

subroutine upevnt
  use jetset_interface, only: new_event_needed_pythia !NODEP!
  new_event_needed_pythia = .true.
end subroutine upevnt
