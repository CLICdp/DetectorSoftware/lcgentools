! WHIZARD 1.95 Feb 25 2010
! 
! (C) 1999-2008 by 
!     Wolfgang Kilian <wolfgang.kilian@desy.de>
!     Thorsten Ohl <ohl@physik.uni-wuerzburg.de>
!     Juergen Reuter <juergen.reuter@physik.uni-freiburg.de>
!
! WHIZARD is free software; you can redistribute it and/or modify it
! under the terms of the GNU General Public License as published by 
! the Free Software Foundation; either version 2, or (at your option)
! any later version.
!
! WHIZARD is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! This file has been stripped of most comments.  For documentation, refer
! to the source 'whizard.nw'

module slha_interface

  use kinds, only: default !NODEP!
  use file_utils, only: free_unit !NODEP!
  use limits, only: BLANK, COMMENT_CHARS
  use limits, only: FILENAME_LEN, BUFFER_SIZE, SLHA_BLOCK_NAME_LEN
  use diagnostics, only: msg_buffer, msg_message
  use diagnostics, only: msg_warning, msg_error, msg_fatal, msg_bug
  use diagnostics, only: msg_listing, WARNING, ERROR
  use files, only: file_exists_else_default
  use files, only: line_t, line_string, next
  use files, only: line_list, create, destroy, read, write, append
  use files, only: first_line, line_list_nth
  use constants, only: pi
  use parameters, only: parameter_set !NODEP!
  use particles

  implicit none
  private

  public :: create, destroy
  public :: read, write
  public :: slha_set, parameters_set
  public :: contains_whizard_input
  public :: write_whizard_input
  public :: slha_write_process
  public :: slha_write_whizard_info

  integer, parameter :: N_BLOCKS_RECOGNIZED = 32
  character(len=SLHA_BLOCK_NAME_LEN), dimension(0:N_BLOCKS_RECOGNIZED), &
       & parameter :: slha_block_type = (/ &
       &  "EXTRA BLOCKS ", &
       &  "MODSEL       ", &
       &  "SMINPUTS     ", "MINPAR       ", "EXTPAR       ", &
       &  "MASS         ", &
       &  "ALPHA        ", "HMIX         ", &
       &  "STOPMIX      ", "SBOTMIX      ", "STAUMIX      ", &
       &  "UMIX         ", "VMIX         ", "NMIX         ", &
       &  "GAUGE        ", "MSOFT        ", &
       &  "AU           ", "AD           ", "AE           ", &
       &  "YU           ", "YD           ", "YE           ", &
       &  "SPINFO       ", &
       &  "DECAY WIDTHS ", "DCINFO       ", &
       &  "WHIZARD_INPUT", &
       &  "CSINFO       ", &
       &  "NMHMIX       ", "NMAMIX       ", "NMNMIX       ", & 
       &  "NMSSMRUN     ", &     
       &  "VCKMIN       ", &     
       &  "QEXTPAR      " &           
       &  /)
  logical, dimension(0:N_BLOCKS_RECOGNIZED), &
       & parameter :: slha_block_used = (/ &
       &  .true., &
       &  .true., &
       &  .true., .true., .false., &
       &  .true., &
       &  .true., .true., &
       &  .true., .true., .true., &
       &  .true., .true., .true., &
       &  .false., .false., &
       &  .true., .true., .true., &
       &  .false., .false., .false., &
       &  .true., &
       &  .true., .true., &
       &  .true., &
       &  .false., &
       &  .true., .true., .true., &
       &  .true., &
       &  .true., &
       &  .false. &
       &  /)
  logical, dimension(0:N_BLOCKS_RECOGNIZED), &
       & parameter :: slha_block_allowed_twice = (/ &
       &  .true., &
       &  .false., &
       &  .false., .false., .false., &
       &  .false., &
       &  .false., .true., &
       &  .false., .false., .false., &
       &  .false., .false., .false., &
       &  .true., .true., &
       &  .true., .true., .true., &
       &  .true., .true., .true., &
       &  .false., &
       &  .true., .false., &
       &  .false., &
       &  .false., &
       &  .false., .false., .false., &
       &  .false., &
       &  .false., &
       &  .false. &
       &  /)
  logical, dimension(0:N_BLOCKS_RECOGNIZED), &
       & parameter :: slha_block_mandatory_mssm = (/ &
       &  .false., &
       &  .true., &
       &  .true., .false., .false., &
       &  .true., &
       &  .true., .true., &
       &  .true., .true., .true., &
       &  .true., .true., .true., &
       &  .false., .false., &
       &  .true., .true., .true., &
       &  .false., .false., .false., &
       &  .false., &
       &  .false., .false., &
       &  .false., &
       &  .false., &
       &  .false., .false., .false., &
       &  .false., &
       &  .false., &
       &  .false. &
       &  /)
    logical, dimension(0:N_BLOCKS_RECOGNIZED), &
       & parameter :: slha_block_mandatory_nmssm = (/ &
       &  .false., &
       &  .true., &
       &  .true., .false., .false., &
       &  .true., &
       &  .true., .true., &
       &  .true., .true., .true., &
       &  .true., .true., .false., &
       &  .false., .false., &
       &  .true., .true., .true., &
       &  .false., .false., .false., &
       &  .false., &
       &  .false., .false., &
       &  .false., &
       &  .false., &
       &  .true.,.true., .true., &                                        
       &  .true., &                                                       
       &  .false., &                                                      
       &  .false. &
       &  /)                                                      
  logical, dimension(0:N_BLOCKS_RECOGNIZED), &
       & parameter :: slha_block_mandatory_sm = (/ &
       &  .false., &
       &  .true., &
       &  .true., .false., .false., &
       &  .true., &
       &  .false., .false., &
       &  .false., .false., .false., &
       &  .false., .false., .false., &
       &  .false., .false., &
       &  .false., .false., .false., &
       &  .false., .false., .false., &
       &  .false., &
       &  .false., .false., &
       &  .false., &
       &  .false., &
       &  .false., .false., .false., &
       &  .false., &
       &  .false., &
       &  .false. &
       &  /)
  integer, parameter :: IS_EMPTY = 0, IS_DATA = 1, IS_BLOCK = 2, IS_DECAY = 3
  integer, parameter :: SLHA_COMMENT_LEN = 30

  type :: slha_modsel
     integer :: mtype = 0
     integer :: Q_grid_size = 0
     integer :: particle_content = 0
     real(kind=default) :: Q_max = 0
     integer :: R_par_viol = 0                                    
     integer :: CP_viol = 0
     integer :: flavor_viol = 0 
  end type slha_modsel

  type :: slha_sminputs
     real(kind=default) :: alpha_em_inv = 0
     real(kind=default) :: GF           = 0
     real(kind=default) :: alpha_s      = 0
     real(kind=default) :: mz   = 0
     real(kind=default) :: mb   = 0
     real(kind=default) :: mtop = 0
     real(kind=default) :: mtau = 0
     real(kind=default) :: mnu_3 = 0
     real(kind=default) :: me = 0                                 
     real(kind=default) :: mnu_1 = 0
     real(kind=default) :: mmu = 0
     real(kind=default) :: mnu_2 = 0
     real(kind=default) :: md = 0
     real(kind=default) :: mu = 0
     real(kind=default) :: ms = 0
     real(kind=default) :: mc = 0
  end type slha_sminputs

  type :: slha_vckmin           
     real(kind=default) :: lambda_ckm = 0
     real(kind=default) :: A_ckm = 0
     real(kind=default) :: rho_bar_ckm = 0
     real(kind=default) :: eta_bar_ckm = 0
  end type slha_vckmin

  type :: minpar_MSSM
     real(kind=default) :: tanb = 0
  end type minpar_MSSM
  type :: minpar_SUGRA
     real(kind=default) :: m_zero = 0
     real(kind=default) :: m_half = 0
     real(kind=default) :: tanb   = 0
     real(kind=default) :: sgn_mu = 0
     real(kind=default) :: A0     = 0
  end type minpar_SUGRA
  type :: minpar_GMSB
     real(kind=default) :: Lambda = 0
     real(kind=default) :: M_mes  = 0
     real(kind=default) :: tanb   = 0
     real(kind=default) :: sgn_mu = 0
     real(kind=default) :: N_5    = 0
     real(kind=default) :: c_grav = 0
  end type minpar_GMSB
  type :: minpar_AMSB
     real(kind=default) :: m_zero = 0
     real(kind=default) :: m_grav = 0
     real(kind=default) :: tanb   = 0
     real(kind=default) :: sgn_mu = 0
  end type minpar_AMSB
  type :: slha_minpar
     integer :: mtype = 0
     real(kind=default) :: Q_max = 0
     type(minpar_MSSM), pointer :: MSSM   => null ()
     type(minpar_SUGRA), pointer :: SUGRA => null ()
     type(minpar_GMSB), pointer :: GMSB   => null ()
     type(minpar_AMSB), pointer :: AMSB   => null ()
  end type slha_minpar

  type :: slha_mass
include "slha_def_mass.inc"
     logical :: dummy   ! in case the include file is empty
  end type slha_mass

  type :: slha_alpha
     real(kind=default) :: al_h = 0
  end type slha_alpha

  type :: slha_hmix
     real(kind=default) :: Q      = 0
     real(kind=default) :: mu_h   = 0
     real(kind=default) :: tanb_h = 0
     real(kind=default) :: v      = 0
     real(kind=default) :: MAsq   = 0
  end type slha_hmix

  type :: slha_nmssmrun
     real(kind=default) :: Q = 0
     real(kind=default) :: ls = 0
     real(kind=default) :: ks = 0
     real(kind=default) :: a_ls   = 0
     real(kind=default) :: a_ks = 0
     real(kind=default) :: nmu = 0
  end type slha_nmssmrun

  type :: slha_mix
     real(kind=default) :: Q = 0
     real(kind=default), dimension(:,:), pointer :: c => null ()
  end type slha_mix

  type :: slha_decay
include "slha_def_decay.inc"
     logical :: dummy   ! in case the include file is empty
  end type slha_decay


  type :: slha_block
     character(len=SLHA_BLOCK_NAME_LEN) :: block_type
     type(line_list) :: contents
     logical :: text_read
     type(slha_modsel), pointer :: modsel
     type(slha_sminputs), pointer :: sminputs
     type(slha_vckmin), pointer :: vckmin
     type(slha_minpar), pointer :: minpar
     type(slha_mass), pointer :: mass
     type(slha_alpha), pointer :: alpha
     type(slha_hmix), pointer :: hmix
     type(slha_nmssmrun), pointer :: nmssmrun
     type(slha_decay), pointer :: decay
     type(slha_mix), pointer :: mix
  end type slha_block

  type, public :: slha_data
     private
     logical :: ignore_errors, rewrite_input
     type(line_list) :: file_contents, header
     type(slha_block) :: modsel
     type(slha_block) :: sminputs
     type(slha_block) :: vckmin
     type(slha_block) :: minpar
     type(slha_block) :: mass
     type(slha_block) :: alpha
     type(slha_block) :: hmix
     type(slha_block) :: nmssmrun
     type(slha_block) :: Au
     type(slha_block) :: Ad
     type(slha_block) :: Ae
     type(slha_block) :: stopmix
     type(slha_block) :: sbotmix
     type(slha_block) :: staumix
     type(slha_block) :: Umix
     type(slha_block) :: Vmix
     type(slha_block) :: nmix
     type(slha_block) :: nmhmix
     type(slha_block) :: nmamix
     type(slha_block) :: nmnmix
     type(slha_block) :: spinfo
     type(slha_block) :: dcinfo
     type(slha_block) :: decay
     type(slha_block) :: whizard_input
     type(slha_block) :: extra_blocks
  end type slha_data

  integer, parameter, public :: MSSM=0, SUGRA=1, GMSB=2, AMSB=3

  interface create
     module procedure slha_create
  end interface
  interface destroy
     module procedure slha_destroy
  end interface
  interface read
     module procedure slha_read_unit, slha_read_name
  end interface
  interface write
     module procedure slha_write_unit, slha_write_name
  end interface
  interface set_value
     module procedure set_scalar_value, set_matrix_value
  end interface
  interface get_value
     module procedure get_scalar_value, get_matrix_value
  end interface
  interface string_value
     module procedure string_value_scalar, string_value_matrix
  end interface
  interface header_string
     module procedure header_name, header_with_value
  end interface
  interface write_whizard_input
     module procedure write_whizard_input_name, write_whizard_input_unit
  end interface

contains

  subroutine slha_message (string)
    character(len=*), intent(in) :: string
    call msg_message (" SLHA: " // string)
  end subroutine slha_message

  subroutine slha_warning (string)
    character(len=*), intent(in) :: string
    call msg_warning (" SLHA: " // string)
  end subroutine slha_warning

  subroutine slha_error (string)
    character(len=*), intent(in) :: string
    call msg_error (" SLHA: " // string)
  end subroutine slha_error

  subroutine slha_fatal (string)
    character(len=*), intent(in) :: string
    call msg_fatal (" SLHA: " // string)
  end subroutine slha_fatal

  subroutine slha_bug (string)
    character(len=*), intent(in) :: string
    call msg_bug (" SLHA input: " // string)
  end subroutine slha_bug

  subroutine slha_print_line (line)
    type(line_t), pointer :: line
    if (associated (line))  call msg_message (trim (line_string (line)))
  end subroutine slha_print_line

  subroutine slha_create (slha)
    type(slha_data), intent(inout) :: slha
    type(slha_block), pointer :: b
    integer :: i
    slha%ignore_errors = .false.
    slha%rewrite_input = .false.
    call create (slha%file_contents)
    call create (slha%header)
    do i = 0, n_blocks_recognized
       b => block_pointer (slha, slha_block_type(i))
       if (associated (b)) then
          call slha_block_create (b, slha_block_type(i))
       end if
    end do
  end subroutine slha_create

  subroutine slha_destroy (slha)
    type(slha_data), intent(inout) :: slha
    type(slha_block), pointer :: b
    integer :: i
    call destroy (slha%file_contents)
    call destroy (slha%header)
    do i = 0, n_blocks_recognized
       b => block_pointer (slha, slha_block_type(i))
       if (associated (b)) then
          call slha_block_destroy (b)
       end if
    end do
  end subroutine slha_destroy

  subroutine slha_block_create (b, block_type)
    type(slha_block), pointer :: b
    character(len=*), intent(in) :: block_type
    if (associated (b)) then
       b%block_type = block_type
       call create (b%contents)
       b%text_read = .false.
       nullify (b%modsel)
       nullify (b%sminputs)
       nullify (b%vckmin)
       nullify (b%minpar)
       nullify (b%mass)
       nullify (b%alpha)
       nullify (b%hmix)
       nullify (b%nmssmrun)
       nullify (b%mix)
       nullify (b%mix)
       nullify (b%decay)
       select case (block_type)
       case ("MODSEL")
          allocate (b%modsel)
          call init_modsel (b%modsel)
       case ("SMINPUTS")
          allocate (b%sminputs)
       case ("VCKMIN")
          allocate (b%vckmin)
       case ("MINPAR")
          allocate (b%minpar)
          call minpar_create (b%minpar)
       case ("MASS")
          allocate (b%mass)
       case ("ALPHA")
          allocate (b%alpha)
       case ("HMIX")
          allocate (b%hmix)
       case ("NMSSMRUN")
          allocate (b%nmssmrun)
       case ("AU", "AD", "AE")
          allocate (b%mix)
          call create_mix (b%mix, 3, 3)
       case ("STOPMIX", "SBOTMIX", "STAUMIX", "UMIX", "VMIX")
          allocate (b%mix)
          call create_mix (b%mix, 1, 2)
       case ("NMIX")
          allocate (b%mix)
          call create_mix (b%mix, 1, 4)
       case ("NMHMIX", "NMAMIX")                       
          allocate (b%mix)
          call create_mix (b%mix, 1, 3)
       case ("NMNMIX")                               
          allocate (b%mix)
          call create_mix (b%mix, 1, 5)
       case ("DECAY WIDTHS")
          allocate (b%decay)
       end select
    end if
  end subroutine slha_block_create

  subroutine slha_block_destroy (b)
    type(slha_block), pointer :: b
    if (associated (b)) then
       b%block_type = ""
       call destroy (b%contents)
       if (associated (b%modsel))  deallocate (b%modsel)
       if (associated (b%sminputs))  deallocate (b%sminputs)
       if (associated (b%vckmin))  deallocate (b%vckmin)
       if (associated (b%minpar)) then
          call minpar_destroy (b%minpar)
          deallocate (b%minpar)
       end if
       if (associated (b%mass))  deallocate (b%mass)
       if (associated (b%alpha))  deallocate (b%alpha)
       if (associated (b%hmix))  deallocate (b%hmix)
       if (associated (b%nmssmrun))  deallocate (b%nmssmrun)
       if (associated (b%mix)) then
          call destroy_mix (b%mix)
          deallocate (b%mix)
       end if
       if (associated (b%mix)) then
          call destroy_mix (b%mix)
          deallocate (b%mix)
       end if
       if (associated (b%decay)) then
          deallocate (b%decay)
       end if
    end if
  end subroutine slha_block_destroy

  function block_pointer (slha, block_type) result (b)
    type(slha_data), intent(in), target :: slha
    character(len=*), intent(in) :: block_type
    type(slha_block), pointer :: b
    select case (block_type)
    case ("MODSEL");  b => slha%modsel
    case ("SMINPUTS");  b => slha%sminputs
    case ("VCKMIN");  b => slha%vckmin
    case ("MINPAR");  b => slha%minpar
    case ("MASS");  b => slha%mass
    case ("ALPHA");  b => slha%alpha
    case ("HMIX");  b => slha%hmix
    case ("NMSSMRUN");  b => slha%nmssmrun
    case ("AU");  b => slha%Au
    case ("AD");  b => slha%Ad
    case ("AE");  b => slha%Ae
    case ("STOPMIX");  b => slha%stopmix
    case ("SBOTMIX");  b => slha%sbotmix
    case ("STAUMIX");  b => slha%staumix
    case ("UMIX");  b => slha%Umix
    case ("VMIX");  b => slha%Vmix
    case ("NMIX");  b => slha%nmix
    case ("NMHMIX");  b => slha%nmhmix
    case ("NMAMIX");  b => slha%nmamix
    case ("NMNMIX");  b => slha%nmnmix
    case ("SPINFO");  b => slha%spinfo
    case ("DCINFO");  b => slha%dcinfo
    case ("DECAY WIDTHS");  b => slha%decay
    case ("WHIZARD_INPUT");  b => slha%whizard_input
    case ("EXTRA BLOCKS");  b => slha%extra_blocks
    case default
       nullify (b)
    end select
  end function block_pointer

  function block_known (block_type)
    character(len=*), intent(in) :: block_type
    logical :: block_known
    integer :: i
    block_known = .false.
    SCAN: do i = 1, N_BLOCKS_RECOGNIZED
       if (trim (block_type) == trim (slha_block_type(i))) then
          block_known = .true.
          exit SCAN
       end if
    end do SCAN
  end function block_known

  function block_used (block_type)
    character(len=*), intent(in) :: block_type
    logical :: block_used
    integer :: i
    block_used = .false.
    SCAN: do i = 1, N_BLOCKS_RECOGNIZED
       if (trim (block_type) == trim (slha_block_type(i))) then
          block_used = slha_block_used(i)
          exit SCAN
       end if
    end do SCAN
  end function block_used

  function block_allowed_twice (block_type)
    character(len=*), intent(in) :: block_type
    logical :: block_allowed_twice
    integer :: i
    block_allowed_twice = .false.
    SCAN: do i = 1, N_BLOCKS_RECOGNIZED
       if (trim (block_type) == trim (slha_block_type(i))) then
          block_allowed_twice = slha_block_allowed_twice(i)
          exit SCAN
       end if
    end do SCAN
  end function block_allowed_twice

  function block_exists (slha, block_type)
    type(slha_data), intent(in) :: slha
    character(len=*), intent(in) :: block_type
    logical :: block_exists
    type(slha_block), pointer :: b
    b => block_pointer (slha, block_type)
    if (associated (b)) then
       block_exists = b%text_read
    else
       block_exists = .false.
    end if
  end function block_exists

  subroutine slha_read_name (prefix, filename, slha, ignore_errors, rewrite_input)
    character(len=*), intent(in) :: prefix, filename
    type(slha_data), intent(inout) :: slha
    logical, intent(in), optional :: ignore_errors, rewrite_input
    character(len=FILENAME_LEN) :: file
    integer :: u
    file = file_exists_else_default (prefix, filename, "in")
    if (len_trim (file) /= 0) then
       call msg_message (" Reading SLHA data from file "//trim(file))
       u = free_unit ()
       open(u, file=trim(file), action='read', status='old')
       call slha_read_unit (u, slha, ignore_errors, rewrite_input)
       close (u)
    else
       call msg_warning (" SLHA input file not found")
    end if
  end subroutine slha_read_name

  subroutine slha_write_name (name, slha)
    character(len=*) :: name
    type(slha_data), intent(in) :: slha
    integer :: u
    u = free_unit()
    open (u, file=name, action='write', status='replace')
    call slha_write_unit (u, slha)
    close (u)
  end subroutine slha_write_name

  subroutine slha_read_unit (unit, slha, ignore_errors, rewrite_input)
    integer, intent(in) :: unit
    type(slha_data), intent(inout) :: slha
    logical, intent(in), optional :: ignore_errors, rewrite_input
    call read (unit, slha%file_contents)
    if (present (ignore_errors))  slha%ignore_errors = ignore_errors
    if (present (rewrite_input))  slha%rewrite_input = rewrite_input
    call slha_fill_blocks (slha)
    call slha_reset_file_contents (slha)
    call slha_recover_file_contents (slha)
  end subroutine slha_read_unit

  subroutine slha_write_unit (unit, slha)
    integer, intent(in) :: unit
    type(slha_data), intent(in) :: slha
    call write (unit, slha%file_contents)
  end subroutine slha_write_unit

  subroutine slha_read_file_contents (unit, slha)
    integer, intent(in) :: unit
    type(slha_data), intent(inout) :: slha
    call read (unit, slha%file_contents)
  end subroutine slha_read_file_contents

  subroutine slha_fill_blocks (slha)
    type(slha_data), intent(inout) :: slha
    type(line_t), pointer :: line
    character(len=5) :: string_tmp
    character(len=SLHA_BLOCK_NAME_LEN) :: block_type
    type(slha_block), pointer :: b
    integer :: i
    logical :: within_block
    line => first_line (slha%file_contents)
    within_block = .false.
    do while (associated (line))
       select case (line_type (line))
       case (IS_EMPTY)
       case (IS_DATA)
          call slha_print_line (line)
          if (.not.within_block) then
             string_tmp = upper_case (adjustl (line_string (line)))
             select case (string_tmp)
             case ('BLOCK','DECAY')
                call slha_error ("BLOCK or DECAY statement not in first column (block ignored)")
             case default
                call slha_error ("Data line before first BLOCK or DECAY (data ignored)")
             end select
          else
             call slha_bug ("Data line in wrong place -- missed block declaration?")
          end if
       case (IS_BLOCK)
          within_block = .true.
          block_type = block_name (line)
          if (block_known (block_type)) then
             if (block_used (block_type)) then
                if (.not. block_exists (slha, block_type)) then
                   b => block_pointer (slha, block_type)
                   call read_text_block (b, line)
                else if (block_allowed_twice (block_type)) then
                   call slha_warning ('Ignoring extra occurence of block"' // trim(block_type) // '"')
                   call ignore_block (slha, line)
                else
                   call slha_error ('Extra occurence of block"' // trim(block_type) // '" (ignored)')
                   call ignore_block (slha, line)
                end if
             else
                call ignore_block (slha, line)
             end if
          else
             call slha_warning ('Block "'//trim(block_type)//'" not recognized (ignored)')
             call ignore_block (slha, line)
          end if
       case (IS_DECAY)
          b => block_pointer (slha, "DECAY WIDTHS")
          call read_decay (b, line)
          call read_text_block (b, line)
       case default
          call slha_print_line (line)
          call slha_bug ("Inconsistent line type encountered")
       end select
       if (.not.within_block)  call append (slha%header, line)
       line => next (line)
    end do
    do i = 1, N_BLOCKS_RECOGNIZED
       if (slha_block_used(i)) then
          call handle_block (i, block_pointer (slha, slha_block_type(i)), slha)
       end if
    end do
    call handle_block (0, block_pointer (slha, slha_block_type(0)), slha)
  end subroutine slha_fill_blocks

  subroutine slha_reset_file_contents (slha)
    type(slha_data), intent(inout) :: slha
    call destroy (slha%file_contents)
    call create (slha%file_contents)
  end subroutine slha_reset_file_contents

  subroutine slha_recover_file_contents (slha)
    type(slha_data), intent(inout) :: slha
    integer :: i
    call append (slha%file_contents, slha%header)
    do i = 1, N_BLOCKS_RECOGNIZED
       if (slha_block_used(i)) then
          call append_block_contents (slha, slha_block_type(i))
       end if
    end do
    if (block_exists (slha, slha_block_type(0))) then
       call append_line (slha, "#")
       call append_line (slha, &
            &       "# WHIZARD: The following blocks have not been used:")
       call append_block_contents (slha, slha_block_type(0))
    end if
  end subroutine slha_recover_file_contents

  subroutine append_block_contents (slha, block_type)
    type(slha_data), intent(inout) :: slha
    character(len=*), intent(in) :: block_type
    type(slha_block), pointer :: b
    b => block_pointer (slha, block_type)
    if (associated (b)) then
       call append_line_list (slha, b%contents)
    else
       call slha_bug ("Attempt to append block with null pointer")
    end if
  end subroutine append_block_contents

  subroutine append_line_list (slha, contents)
    type(slha_data), intent(inout) :: slha
    type(line_list), intent(in) :: contents
    call append (slha%file_contents, contents)
  end subroutine append_line_list

  subroutine append_line (slha, string)
    type(slha_data), intent(inout) :: slha
    character(len=*), intent(in) :: string
    call append (slha%file_contents, trim(string))
  end subroutine append_line

  function line_type (line)
    type(line_t), pointer :: line
    integer :: line_type
    character(len=BUFFER_SIZE) :: string
    if (associated (line)) then
       string = upper_case (remove_comment (line_string (line)))
       select case (string(1:1))
       case (BLANK)
          if (verify (string, BLANK) == 0) then
             line_type = IS_EMPTY
          else
             line_type = IS_DATA
          end if
       case default
          select case (string(1:6))
          case ('BLOCK ')
             line_type = IS_BLOCK
          case ('DECAY ')
             line_type = IS_DECAY
          case default
             call slha_print_line (line)
             call slha_error ("Line is not recognized and will be ignored")
             line_type = IS_EMPTY
          end select
       end select
    end if
  end function line_type

  function block_name (line) result (string)
    type(line_t), pointer :: line
    character(len=SLHA_BLOCK_NAME_LEN) :: string
    integer :: pos
    string = line_string (line)
    string = upper_case (remove_comment (adjustl (string(6:))))
    pos = scan (string, BLANK)
    if (pos /= 0) then
       string = string(1:pos-1)
    end if
  end function block_name

  function remove_comment (string) result (new_string)
    character(len=*), intent(in) :: string
    character(len=len(string)) :: new_string
    integer :: pos
    pos = scan (string, COMMENT_CHARS)
    if (pos==0) then
       new_string = string
    else
       new_string = string(1:pos-1)
    end if
  end function remove_comment

  function upper_case (string) result (new_string)
    character(len=*), intent(in) :: string
    character(len=len(string)) :: new_string
    integer :: pos, code
    integer, parameter :: offset = iachar('A')-iachar('a')
    do pos=1, len(string)
       code = iachar (string(pos:pos))
       select case (code)
       case (iachar('a'):iachar('z'))
          new_string(pos:pos) = achar (code + offset)
       case default
          new_string(pos:pos) = string(pos:pos)
       end select
    end do
  end function upper_case

  subroutine slha_set (slha, par)
    type(slha_data), intent(inout), target :: slha
    type(parameter_set), intent(in) :: par
!   real(kind=default) :: tmp
    type(slha_modsel), pointer :: modsel
    type(slha_sminputs), pointer :: sminputs
    type(slha_vckmin), pointer :: vckmin
    type(slha_minpar), pointer :: minpar
    type(slha_mass), pointer :: mass
    type(slha_alpha), pointer :: alpha
    type(slha_hmix), pointer :: hmix
    type(slha_nmssmrun), pointer :: nmssmrun
    type(slha_decay), pointer :: decay
    type(slha_mix), pointer :: Au, Ad, Ae
    type(slha_mix), pointer :: stopmix, sbotmix, staumix, Umix, Vmix, nmix, nmhmix, nmamix, nmnmix
    modsel => slha%modsel%modsel
    sminputs => slha%sminputs%sminputs
    vckmin => slha%vckmin%vckmin
    minpar => slha%minpar%minpar
    mass => slha%mass%mass
    alpha => slha%alpha%alpha
    hmix => slha%hmix%hmix
    nmssmrun => slha%nmssmrun%nmssmrun
    Au => slha%Au%mix
    Ad => slha%Ad%mix
    Ae => slha%Ae%mix
    stopmix => slha%stopmix%mix
    sbotmix => slha%sbotmix%mix
    staumix => slha%staumix%mix
    Umix => slha%Umix%mix
    Vmix => slha%Vmix%mix
    nmix => slha%nmix%mix
    nmhmix => slha%nmhmix%mix
    nmamix => slha%nmamix%mix
    nmnmix => slha%nmnmix%mix
    decay => slha%decay%decay
include "slha_initialize.inc"
  end subroutine slha_set

  subroutine parameters_set (par, slha)
    type(parameter_set), intent(inout) :: par
    type(slha_data), intent(in), target :: slha
    real(kind=default) :: tmp
    type(slha_modsel), pointer :: modsel
    type(slha_sminputs), pointer :: sminputs
    type(slha_vckmin), pointer :: vckmin
    type(slha_minpar), pointer :: minpar
    type(slha_mass), pointer :: mass
    type(slha_alpha), pointer :: alpha
    type(slha_hmix), pointer :: hmix
    type(slha_nmssmrun), pointer :: nmssmrun
    type(slha_decay), pointer :: decay
    type(slha_mix), pointer :: Au, Ad, Ae
    type(slha_mix), pointer :: stopmix, sbotmix, staumix, Umix, Vmix, nmix, nmhmix, nmamix, nmnmix
    modsel => slha%modsel%modsel
    sminputs => slha%sminputs%sminputs
    vckmin => slha%vckmin%vckmin
    minpar => slha%minpar%minpar
    mass => slha%mass%mass
    alpha => slha%alpha%alpha
    hmix => slha%hmix%hmix
    nmssmrun => slha%nmssmrun%nmssmrun
    Au => slha%Au%mix
    Ad => slha%Ad%mix
    Ae => slha%Ae%mix
    stopmix => slha%stopmix%mix
    sbotmix => slha%sbotmix%mix
    staumix => slha%staumix%mix
    Umix => slha%Umix%mix
    Vmix => slha%Vmix%mix
    nmix => slha%nmix%mix
    nmhmix => slha%nmhmix%mix
    nmamix => slha%nmamix%mix
    nmnmix => slha%nmnmix%mix
    decay => slha%decay%decay
include "slha_parameters.inc"
  end subroutine parameters_set

  subroutine set_scalar_value (b, i, rval, ival, ok)
    type(slha_block), intent(inout) :: b
    integer, intent(in) :: i
    real(kind=default), intent(in) :: rval
    integer, intent(in) :: ival
    logical, intent(out) :: ok
    if (is_real (b, i)) then
       select case (b%block_type)
       case ("MODSEL");     call modsel_set_value (b%modsel, i, ok, rval=rval)
       case ("SMINPUTS");   call sminputs_set_value (b%sminputs, i, ok, rval)
       case ("VCKMIN");   call vckmin_set_value (b%vckmin, i, ok, rval)
       case ("MINPAR");     call minpar_set_value (b%minpar, i, ok, rval)
       case ("MASS");       call mass_set_value (b%mass, i, ok, rval)
       case ("ALPHA");      call alpha_set_value (b%alpha, ok, rval)
       case ("HMIX");       call hmix_set_value (b%hmix, i, ok, rval)
       case ("NMSSMRUN");       call nmssmrun_set_value (b%nmssmrun, i, ok, rval)
       case ("AU", "AD", "AE")
          call mix_set_value (b%mix, rval, i, ok=ok)
       case ("DECAY WIDTHS");  call decay_set_value (b%decay, i, ok, rval)
       case default
          ok = .false.
       end select
    else if (is_integer (b, i)) then
       select case (b%block_type)
       case ("MODSEL");     call modsel_set_value (b%modsel, i, ok, ival=ival)
       case default
          ok = .false.
       end select
    else
       ok = .false.
    end if
  end subroutine set_scalar_value

  subroutine get_scalar_value (b, i, rval, ival, ok)
    type(slha_block), intent(in) :: b
    integer, intent(in) :: i
    real(kind=default), intent(out) :: rval
    integer, intent(out) :: ival
    logical, intent(out) :: ok
    ival = 0;  rval = 0
    if (is_real (b, i)) then
       select case (b%block_type)
       case ("MODSEL");     call modsel_get_value (b%modsel, i, ok, rval=rval)
       case ("SMINPUTS");   call sminputs_get_value (b%sminputs, i, ok, rval)
       case ("VCKMIN");   call vckmin_get_value (b%vckmin, i, ok, rval)
       case ("MINPAR");     call minpar_get_value (b%minpar, i, ok, rval)
       case ("MASS");       call mass_get_value (b%mass, i, ok, rval)
       case ("ALPHA");      call alpha_get_value (b%alpha, ok, rval)
       case ("HMIX");       call hmix_get_value (b%hmix, i, ok, rval)
       case ("NMSSMRUN");       call nmssmrun_get_value (b%nmssmrun, i, ok, rval)
       case ("AU", "AD", "AE")
          call mix_get_value (b%mix, rval, i, ok=ok)
       case ("DECAY WIDTHS");  call decay_get_value (b%decay, i, ok, rval)
       case default     
          ok = .false.
       end select
    else if (is_integer (b, i)) then
       select case (b%block_type)
       case ("MODSEL");     call modsel_get_value (b%modsel, i, ok, ival=ival)
       case default     
          ok = .false.
       end select
    else
       ok = .false.
    end if
  end subroutine get_scalar_value

  function string_value_scalar (b, i, comment, ok) result (string)
    character(len=BUFFER_SIZE) :: string
    type(slha_block), intent(in) :: b
    integer, intent(in) :: i
    character(len=*), intent(in), optional :: comment
    logical, intent(out) :: ok
    real(kind=default) :: rval
    integer :: ival
    character(len=BUFFER_SIZE) :: fmt, text
    fmt = slha_line_format (b,i)
    text = comment_string (b,i)
    call get_value (b, i, rval, ival, ok)
    string = ""
    if (present (comment))  text = trim (text) // trim (comment)
    if (ok) then
       if (has_index (b)) then
          if (is_integer (b,i)) then
             write (string, trim(fmt))  i, ival, trim(text)
          else if (is_real (b,i)) then
             write (string, trim(fmt))  i, rval, trim(text)
          end if
       else
          if (is_integer (b,i)) then
             write (string, trim(fmt))  ival, trim(text)
          else if (is_real (b,i)) then
             write (string, trim(fmt))  rval, trim(text)
          end if
       end if
    end if
  end function string_value_scalar
    
  subroutine set_matrix_value (b, i, j, rval, ival, ok)
    type(slha_block), intent(inout) :: b
    integer, intent(in) :: i, j
    real(kind=default), intent(in) :: rval
    integer, intent(in) :: ival
    logical, intent(out), optional :: ok
    if (present (ok))  ok = .true.
    select case (b%block_type)
    case ("AU", "AD", "AE")
       call mix_set_value (b%mix, rval, i, j, ok=ok)
    case ("STOPMIX", "SBOTMIX", "STAUMIX", "UMIX", "VMIX", "NMIX", "NMHMIX", "NMAMIX", "NMNMIX")
       call mix_set_value (b%mix, rval, i, j, ok)
    case default
       if (present (ok))  ok = .false.
    end select
  end subroutine set_matrix_value

  subroutine get_matrix_value (b, i, j, rval, ival, ok)
    type(slha_block), intent(in) :: b
    integer, intent(in) :: i, j
    real(kind=default), intent(out) :: rval
    integer, intent(out) :: ival
    logical, intent(out), optional :: ok
    rval = 0;  ival = 0
    if (present (ok))  ok = .true.
    select case (b%block_type)
    case ("AU", "AD", "AE")
       call mix_get_value (b%mix, rval, i, j, ok=ok)
    case ("STOPMIX", "SBOTMIX", "STAUMIX", "UMIX", "VMIX", "NMIX", "NMHMIX", "NMAMIX", "NMNMIX")
       call mix_get_value (b%mix, rval, i, j, ok)
    case default        
       if (present (ok))  ok = .false.
    end select
  end subroutine get_matrix_value

  function string_value_matrix (b, i, j, comment, ok) result (string)
    character(len=BUFFER_SIZE) :: string
    type(slha_block), intent(in) :: b
    integer, intent(in) :: i, j
    character(len=*), intent(in), optional :: comment
    logical, intent(out), optional :: ok
    real(kind=default) :: rval
    integer :: ival
    character(len=BUFFER_SIZE) :: fmt, text
    fmt = slha_line_format (b,i)
    text = comment_string (b,i)
    call get_value (b, i, j, rval, ival, ok)
    string = ""
    if (present (comment))  text = trim (text) // trim (comment)
    if (ok) then
       if (is_integer (b,i)) then
          write (string, trim (fmt)) i, j, ival, trim(text)
       else if (is_real (b,i)) then
          write (string, trim (fmt)) i, j, rval, trim(text)
       end if
    end if
  end function string_value_matrix
    
  function header_name (b) result (string)
    type(slha_block), intent(in) :: b
    character(len=BUFFER_SIZE) :: string
    write (string, trim (slha_header_format (b))) &
         & b%block_type, comment_string (b, 0)
  end function header_name

  function header_with_value (b, value) result (string)
    type(slha_block), intent(in) :: b
    real(kind=default), intent(in) :: value
    character(len=BUFFER_SIZE) :: string
    write (string, trim (slha_header_format (b))) &
         & b%block_type, value, comment_string (b, 0)
  end function header_with_value

  function header_decay (b, code, value) result (string)
    type(slha_block), intent(in) :: b
    integer, intent(in) :: code 
    real(kind=default), intent(in) :: value
    character(len=BUFFER_SIZE) :: string
    write (string, trim (slha_header_format (b))) &
         & code, value, trim (particle_name (code))
  end function header_decay

  function slha_header_format (b) result (fmt)
    type(slha_block), intent(in), optional :: b
    character(len=BUFFER_SIZE) :: fmt
    if (present (b)) then
       select case (b%block_type)
       case ("NMSSMRUN", "HMIX", "GAUGE", "MSOFT", "AU", "AD", "AE", "YU", "YD", "YE")
          fmt = "('BLOCK', 1x, A, 1x, 'Q=', 1x, 1P, E16.8, 3x, '#', 1x, A)"
       case ("DECAY WIDTHS")
          fmt = "('DECAY', 1x, I9, 3x, 1P, E16.8, 3x, '#', 1x, A)"
       case default
          fmt = "('BLOCK', 1x, A, 3x, '#', 1x, A)"
       end select
    else
       fmt = "('BLOCK', 1x, A, 3x, '#', 1x, A)"
    end if
  end function slha_header_format

  function slha_line_format (b, i) result (fmt)
    type(slha_block), intent(in) :: b
    integer, intent(in) :: i
    character(len=BUFFER_SIZE) :: fmt
    if (is_real (b, i)) then
       select case (b%block_type)
       case ("MODSEL");    fmt = "(1x,I4,2x,1P,E16.8,2x,'#',1x,A)"
       case ("SMINPUTS");  fmt = "(1x,I4,2x,1P,E16.8,2x,'#',1x,A)"
       case ("VCKMIN");  fmt = "(1x,I4,2x,1P,E16.8,2x,'#',1x,A)"
       case ("MINPAR");    fmt = "(1x,I4,2x,1P,E16.8,2x,'#',1x,A)"
       case ("MASS");      fmt = "(1x,I9,3x,1P,E16.8,3x,'#',1x,A)"
       case ("ALPHA");     fmt = "(9x,1P,E16.8,3x,'#',1x,A)"
       case ("HMIX");      fmt = "(1x,I5,3x,1P,E16.8,3x,'#',1x,A)"
       case ("NMSSMRUN");      fmt = "(1x,I5,3x,1P,E16.8,3x,'#',1x,A)"
       case ("AU", "AD", "AE")
          fmt = "(1x,I2,1x,I2,3x,1P,E16.8,3x,'#',1x,A)"
       case ("STOPMIX", "SBOTMIX", "STAUMIX", "UMIX", "VMIX", "NMIX", "NMHMIX", "NMAMIX", "NMNMIX")
          fmt = "(1x,I2,1x,I2,3x,1P,E16.8,3x,'#',1x,A)"
       case default;  fmt = "('#',1x,A)"
       end select
    else if (is_integer (b, i)) then
       select case (b%block_type)
       case ("MODSEL");    fmt = "(1x,I4,5x,I3,12x,'#',1x,A)"
       case default;  fmt = "('#',1x,A)"
       end select
    else
       fmt = "('#',1x,A)"
    end if
  end function slha_line_format

  function has_index (b)
    type(slha_block), intent(in) :: b
    logical :: has_index
    select case (b%block_type)
    case ("ALPHA");     has_index = .false.
    case default
       has_index = .true.
    end select
  end function has_index

  function is_integer (b, index)
    type(slha_block), intent(in) :: b
    integer, intent(in) :: index
    logical :: is_integer
    select case (b%block_type)
    case ("MODSEL")
       select case (index)
       case (1,2,3,4,5,6,11,21);  is_integer = .true.
       case default;        is_integer = .false.
       end select
    case default
       is_integer = .false.
    end select
  end function is_integer

  function is_real (b, index)
    type(slha_block), intent(in) :: b
    integer, intent(in) :: index
    logical :: is_real
    select case (b%block_type)
    case ("MODSEL")
       select case (index)
       case (12);     is_real = .true.
       case default;  is_real = .false.
       end select
    case ("SMINPUTS");  is_real = .true.
    case ("VCKMIN");  is_real = .true.
    case ("MINPAR");    is_real = .true.
    case ("MASS");      is_real = .true.
    case ("ALPHA");     is_real = .true.
    case ("HMIX");      is_real = .true.
    case ("NMSSMRUN");      is_real = .true.
    case ("AU", "AD", "AE")
       is_real = .true.
    case ("STOPMIX", "SBOTMIX", "STAUMIX", "UMIX", "VMIX", "NMIX", "NMHMIX", "NMAMIX", "NMNMIX")
       is_real = .true.
    case ("DECAY WIDTHS");  is_real = .true.
    case default
       is_real = .false.
    end select
  end function is_real

  subroutine read_text_block (b, line)
    type(slha_block), pointer :: b
    type(line_t), pointer :: line, previous
    if (.not.associated (b)) &
         & call slha_bug ("Null block pointer while reading block")
    if (.not.associated (line)) &
         & call slha_bug ("Null line pointer while reading block")
    call append (b%contents, line)
    READLINE: do
       previous => line
       line => next (line)
       if (.not.associated(line))  exit READLINE
       select case (line_type (line))
       case (IS_DATA, IS_EMPTY)
       case default
          exit READLINE
       end select
       call append (b%contents, line)
    end do READLINE
    line => previous
    b%text_read = .true.
  end subroutine read_text_block

  subroutine ignore_block (slha, line)
    type(slha_data), intent(inout) :: slha
    type(line_t), pointer :: line
    character(len=*), parameter :: block_type = slha_block_type(0)
    call read_text_block (block_pointer (slha, block_type), line)
  end subroutine ignore_block

  subroutine handle_block (i, b, slha)
    integer, intent(in) :: i
    type(slha_block), pointer :: b
    type(slha_data), intent(inout) :: slha
    logical :: mandatory
    if (.not.associated (b)) &
         & call slha_bug ("Attempt to parse block with null pointer")
    if (block_exists (slha, b%block_type)) then
       select case (b%block_type)
       case ('MODSEL');     call handle_block_scalar (b)
       case ('SMINPUTS');   call handle_block_scalar (b)
       case ('VCKMIN');   call handle_block_scalar (b)
       case ('MINPAR');
          call minpar_reset (b%minpar, slha%modsel%modsel)
          call handle_block_scalar (b)
       case ('MASS');       call handle_block_scalar (b)
       case ('ALPHA');      call handle_block_scalar (b)
       case ('HMIX');       call handle_block_scalar (b)
       case ('NMSSMRUN');       call handle_block_scalar (b)
       case ("AU", "AD", "AE")
          call handle_block_matrix (b)
       case ("STOPMIX", "SBOTMIX", "STAUMIX", "UMIX", "VMIX", "NMIX", "NMHMIX", "NMAMIX", "NMNMIX")
          call handle_block_matrix (b)
       case ('SPINFO');     
          call handle_info_block (b, "Spectrum calculator", slha%ignore_errors)
       case ('DCINFO');     
          call handle_info_block (b, "Decay package", slha%ignore_errors)
       case ("DECAY WIDTHS")
       case ('WHIZARD_INPUT')
       case ('EXTRA BLOCKS')
       case default
          call slha_bug ('Attempt to read unused block "'//trim(b%block_type)//'"')
       end select
    else if (i==1) then
       if (slha%ignore_errors) then
          call slha_error ('Block "' // trim(b%block_type) // '" is missing')
       else
          call slha_fatal ('Block "' // trim(b%block_type) // '" is missing')
       end if
    else
       if (block_exists (slha, "MODSEL")) then
          select case (slha%modsel%modsel%mtype)
          case (:-1);   mandatory = slha_block_mandatory_sm(i)
          case (4)
               call slha_fatal ('R parity violating MSSM not yet supported')
          case (5)
               call slha_fatal ('CP violating MSSM not yet supported')         
          case (6)
               call slha_fatal ('MSSM with flavor violation not yet supported')
          case default; 
               select case (slha%modsel%modsel%particle_content) 
               case (1); mandatory = slha_block_mandatory_nmssm(i)
               case default; mandatory = slha_block_mandatory_mssm(i)
               end select
          end select
       else if (slha%ignore_errors) then
          mandatory = slha_block_mandatory_sm(i)
       else
          call slha_bug ('Block MODSEL unset while handling other blocks')
       end if
       if (mandatory) then
          call slha_error ('Missing block "' // trim(b%block_type) // '" -- using default values')
       end if
    end if
    if (slha%rewrite_input) then
       select case (b%block_type)
       case ('MODSEL');     call rewrite_block_text (b, 12)
       case ('SMINPUTS');   call rewrite_block_text (b, 24)
       case ('VCKMIN');   call rewrite_block_text (b, 7)
       case ('MINPAR');     call rewrite_block_text (b, 6)
       case ('MASS');       call rewrite_block_mass (b)
       case ('ALPHA');      call rewrite_block_text (b, 1)
       case ('HMIX');       call rewrite_block_text (b, 4, value = b%hmix%Q)
       case ('NMSSMRUN');       call rewrite_block_text (b, 6, value = b%nmssmrun%Q)
       case ("AU", "AD", "AE")
          call rewrite_block_mix (b)
       case ("STOPMIX", "SBOTMIX", "STAUMIX", "UMIX", "VMIX", "NMIX", "NMHMIX", "NMAMIX", "NMNMIX")
          call rewrite_block_mix (b)
       case ("DECAY WIDTHS");   call rewrite_block_decay (b)
       end select
    end if
  end subroutine handle_block

  subroutine handle_block_scalar (b)
    type(slha_block), pointer :: b
    integer :: index
    type(line_t), pointer :: line
    integer :: ival
    real(kind=default) :: rval
    character(len=BUFFER_SIZE) :: string
    integer :: iostat
    logical :: ok
    line => first_line (b%contents)
    call read_Q_value (line, rval, ok)
    call set_value (b, 0, rval, ival, ok)
    READLINE: do
       line => next (line)
       if (.not.associated(line))  exit READLINE
       select case (line_type (line))
       case (IS_DATA)
          string = remove_comment (line_string (line))
          ival = 0;  rval = 0
          if (has_index (b)) then
             read (string, *, iostat=iostat)  index
             if (iostat==0) then
                if (is_integer (b, index)) then
                   read (string, *, iostat=iostat)  index, ival
                else if (is_real (b, index)) then
                   read (string, *, iostat=iostat)  index, rval
                end if
             end if
          else if (is_integer (b, 1)) then
             read (string, *, iostat=iostat)  ival
          else if (is_real (b, 1)) then
             read (string, *, iostat=iostat)  rval
          end if
          if (iostat == 0) then
             call set_value (b, index, rval, ival, ok)
          else
             call slha_print_line (line)
             call slha_block_syntax_error (b%block_type)
          end if
       case (IS_EMPTY)
       case default
          exit READLINE
       end select
    end do READLINE
  end subroutine handle_block_scalar

  subroutine handle_block_matrix (b)
    type(slha_block), pointer :: b
    integer :: i, j
    type(line_t), pointer :: line
    integer :: ival
    real(kind=default) :: rval
    character(len=BUFFER_SIZE) :: string
    integer :: iostat
    logical :: ok
    line => first_line (b%contents)
    call read_Q_value (line, rval, ok)
    call set_value (b, 0, rval, ival, ok)
    READLINE: do
       line => next (line)
       if (.not.associated(line))  exit READLINE
       select case (line_type (line))
       case (IS_DATA)
          string = remove_comment (line_string (line))
          ival = 0;  rval = 0
          read (string, *, iostat=iostat)  i, j, rval
          if (iostat == 0) then
             call set_value (b, i, j, rval, ival, ok)
          else
             call slha_print_line (line)
             call slha_block_syntax_error (b%block_type)
          end if
       case (IS_EMPTY)
       case default
          exit READLINE
       end select
    end do READLINE
  end subroutine handle_block_matrix

  subroutine slha_block_syntax_error (block_type)
    character(len=*), intent(in) :: block_type
    call slha_error ('Block "'//trim(block_type)//'": syntax error in data line')
  end subroutine slha_block_syntax_error

  subroutine read_Q_value (line, value, ok)
    type(line_t), pointer :: line
    real(kind=default), intent(out) :: value
    logical, intent(out) :: ok
    character(len=BUFFER_SIZE) :: string
    integer :: pos, iostat
    value = 0
    ok = .false.
    string = remove_comment (line_string (line))
    string = adjustl (string(6:))
    pos = scan (string, BLANK)
    if (pos /= 0) then
       string = adjustl (string(pos:))
       if (verify (string(1:1), "Qq") == 0) then
          string = adjustl (string(2:))
          if (string(1:1) == "=") then
             string = adjustl (string(2:))
             read (string, *, iostat=iostat)  value
             if (iostat == 0) then
                ok = .true.
             else
                call slha_print_line (line)
                call slha_error ("Syntax error in block header")
             end if
          end if
       end if
    end if
  end subroutine read_Q_value

  subroutine rewrite_block_text (b, n, value)
    type(slha_block), pointer :: b
    integer, intent(in) :: n
    real(kind=default), intent(in), optional :: value
    integer :: i
    logical :: ok
    character(len=BUFFER_SIZE) :: string
    call destroy (b%contents)
    call create (b%contents)
    if (present (value)) then
       call append (b%contents, trim (header_with_value (b, value)))
    else
       call append (b%contents, trim (header_name (b)))
    end if
    do i = 1, n
       string = string_value (b, i, ok=ok)
       if (ok)  call append (b%contents, trim (string))
    end do
  end subroutine rewrite_block_text
       
  function comment_string (b, i) result (string)
    character(len=SLHA_COMMENT_LEN) :: string
    type(slha_block), intent(in) :: b
    integer, intent(in) :: i
    string = ""
    select case (b%block_type)
    case ("MODSEL");    string = modsel_comment (i)
    case ("SMINPUTS");  string = sminputs_comment (i)
    case ("VCKMIN");  string = vckmin_comment (i)
    case ("MINPAR");    string = minpar_comment (b, i)
    case ("MASS");      string = mass_comment (i)
    case ("ALPHA");     string = alpha_comment (i)
    case ("HMIX");      string = hmix_comment (i)
    case ("NMSSMRUN");      string = nmssmrun_comment (i)
    case ("AU");        if (i==0)  string = "Up squark mixing matrix"
    case ("AD");        if (i==0)  string = "Down squark mixing matrix"
    case ("AE");        if (i==0)  string = "Slepton mixing matrix"
    case ("STOPMIX");   if (i==0)  string = "Stop mixing matrix"
    case ("SBOTMIX");   if (i==0)  string = "Sbottom mixing matrix"
    case ("STAUMIX");   if (i==0)  string = "Stau mixing matrix"
    case ("UMIX");      if (i==0)  string = "Chargino left mixing matrix"
    case ("VMIX");      if (i==0)  string = "Chargino right mixing matrix"
    case ("NMIX");      if (i==0)  string = "Neutralino mixing matrix"
    case ("NMHMIX");    if (i==0)  string = "NMSSM CP-even Higgs mixing matrix"
    case ("NMAMIX");    if (i==0)  string = "NMSSM CP-odd Higgs mixing matrix"
    case ("NMNMIX");    if (i==0)  string = "NMSSM Neutralino mixing matrix"
    end select
  end function comment_string

  function modsel_comment (i) result (string)
    integer, intent(in) :: i
    character(len=SLHA_COMMENT_LEN) :: string
    select case (i)
    case (0);   string = "Model selection"
    case (1);   string = "Selected model"
    case (3);   string = "Particle content"
    case (4);   string = "R-parity violation"
    case (5);   string = "CP violation"
    case (6);   string = "flavor violation"
    case (11);  string = "Number of Q values"
    case (12);  string = "Max. Q value"
    case default;  string = ""
    end select
  end function modsel_comment

  subroutine init_modsel (modsel)
    type(slha_modsel), intent(inout) :: modsel
    modsel%mtype = 1
    modsel%Q_grid_size = 1
    modsel%particle_content = 0
    modsel%R_par_viol = 0
    modsel%CP_viol = 0
    modsel%flavor_viol = 0
    modsel%Q_max = 0
  end subroutine init_modsel

  subroutine modsel_set_value (modsel, i, ok, rval, ival)
    type(slha_modsel), intent(inout) :: modsel
    logical, intent(out) :: ok
    real(kind=default), intent(in), optional :: rval
    integer, intent(in), optional :: ival
    integer, intent(in) :: i
    ok = .true.
    if (present (ival)) then
       select case (i)
       case (1);   modsel%mtype = ival
       case (3);   modsel%particle_content = ival
       case (2);   modsel%Q_grid_size = ival  ! deprecated
       case (11);  modsel%Q_grid_size = ival
       case (4);   modsel%R_par_viol = ival
       case (5);   modsel%CP_viol = ival
       case (6);   modsel%flavor_viol = ival
       case default
          ok = .false.
       end select
    else if (present(rval)) then
       select case (i)
       case (12);  modsel%Q_max = rval
       case default
          ok = .false.
       end select
    else
       ok = .false.
    end if
  end subroutine modsel_set_value

  subroutine modsel_get_value (modsel, i, ok, rval, ival)
    type(slha_modsel), intent(in) :: modsel
    integer, intent(in) :: i
    logical, intent(out) :: ok
    real(kind=default), intent(out), optional :: rval
    integer, intent(out), optional :: ival
    ok = .true.
    if (present (ival)) then
       select case (i)
       case (1);  ival = modsel%mtype
       case (3);  ival = modsel%particle_content
!       case(11);  ival = modsel%Q_grid_size
       case (4);  ival = modsel%R_par_viol
       case (5);  ival = modsel%CP_viol
       case (6);  ival = modsel%flavor_viol
       case default
          ival = 0
          ok = .false.
       end select
    else if (present(rval)) then
       select case (i)
!       case(12);  rval = modsel%Q_max
       case default
          rval = 0
          ok = .false.
       end select
    else
       ok = .false.
    end if
  end subroutine modsel_get_value

  function sminputs_comment (i) result (string)
    integer, intent(in) :: i
    character(len=SLHA_COMMENT_LEN) :: string
    select case (i)
    case (0);  string = "SM parameters"
    case (1);  string = "1/alpha_em(MZ)"
    case (2);  string = "GF"
    case (3);  string = "alpha_s(MZ)"
    case (4);  string = "MZ"
    case (5);  string = "mb"
    case (6);  string = "mtop"
    case (7);  string = "mtau"
    case (8);  string = "mnu_3" 
    case (11); string = "me"
    case (12); string = "mnu_1"
    case (13); string = "mmu"
    case (14); string = "mnu_2"
    case (21); string = "md"
    case (22); string = "mu"
    case (23); string = "ms"
    case (24); string = "mc"
    case default;  string = ""
    end select
  end function sminputs_comment

  subroutine sminputs_set_value (sminputs, i, ok, value)
    type(slha_sminputs), intent(inout) :: sminputs
    integer, intent(in) :: i
    logical, intent(out) :: ok
    real(kind=default), intent(in) :: value
    ok = .true.
    select case (i)
    case (1);  sminputs%alpha_em_inv = value
    case (2);  sminputs%GF = value
    case (3);  sminputs%alpha_s = value
    case (4);  sminputs%mz = value
    case (5);  sminputs%mb = value
    case (6);  sminputs%mtop = value
    case (7);  sminputs%mtau = value
    case (8);  sminputs%mnu_3 = value               
    case (11); sminputs%me = value
    case (12); sminputs%mnu_1 = value
    case (13); sminputs%mmu = value
    case (14); sminputs%mnu_2 = value
    case (21); sminputs%md = value
    case (22); sminputs%mu = value
    case (23); sminputs%ms = value
    case (24); sminputs%mc = value
    case default
       ok = .false.
    end select
  end subroutine sminputs_set_value

  subroutine sminputs_get_value (sminputs, i, ok, value)
    type(slha_sminputs), intent(in) :: sminputs
    integer, intent(in) :: i
    logical, intent(out) :: ok
    real(kind=default), intent(out) :: value
    ok = .true.
    select case (i)
    case (1);  value = sminputs%alpha_em_inv
    case (2);  value = sminputs%GF
    case (3);  value = sminputs%alpha_s
    case (4);  value = sminputs%mz
    case (5);  value = sminputs%mb
    case (6);  value = sminputs%mtop
    case (7);  value = sminputs%mtau
    case (8);  value = sminputs%mnu_3                           
    case (11); value = sminputs%me 
    case (12); value = sminputs%mnu_1
    case (13); value = sminputs%mmu 
    case (14); value = sminputs%mnu_2
    case (21); value = sminputs%md 
    case (22); value = sminputs%mu 
    case (23); value = sminputs%ms 
    case (24); value = sminputs%mc 
    case default
       value = 0
       ok = .false.
    end select
  end subroutine sminputs_get_value

  function vckmin_comment (i) result (string)           
    integer, intent(in) :: i
    character(len=SLHA_COMMENT_LEN) :: string
    select case (i)
    case (0);  string = "Vckm parameters"
    case (1);  string = "lambda_ckm"
    case (2);  string = "A_ckm"
    case (3);  string = "rho_bar_ckm"
    case (4);  string = "eta_bar_ckm"
    case default;  string = ""
    end select
  end function vckmin_comment

 subroutine vckmin_set_value (vckmin, i, ok,value)
    type(slha_vckmin), intent(inout) :: vckmin
    integer, intent(in) :: i
    logical, intent(out) :: ok
    real(kind=default), intent(in) :: value
    ok = .true.
    select case (i)
    case (1);  vckmin%lambda_ckm = value
    case (2);  vckmin%A_ckm = value
    case (3);  vckmin%rho_bar_ckm = value
    case (4);  vckmin%eta_bar_ckm = value
    case default
       ok = .false.
    end select
  end subroutine vckmin_set_value

  subroutine vckmin_get_value (vckmin, i, ok, value)            
    type(slha_vckmin), intent(in) :: vckmin
    integer, intent(in) :: i
    logical, intent(out) :: ok
    real(kind=default), intent(out) :: value
    ok = .true.
    select case (i)
    case (1);  value = vckmin%lambda_ckm
    case (2);  value = vckmin%A_ckm
    case (3);  value = vckmin%rho_bar_ckm
    case (4);  value = vckmin%eta_bar_ckm
    case default
       value = 0
       ok = .false.
    end select
  end subroutine vckmin_get_value

  function minpar_comment (b, i) result (string)
    type(slha_block), intent(in) :: b
    integer, intent(in) :: i
    character(len=SLHA_COMMENT_LEN) :: string
    select case (b%minpar%mtype)
    case (MSSM);
       select case (i)
       case (0);  string = "Generic MSSM parameters"
       case (3);  string = "tan(beta)"
       case default;  string = ""
       end select
    case (SUGRA)
       select case (i)
       case (0);  string = "mSUGRA parameters"
       case (1);  string = "m_0"
       case (2);  string = "m_1/2"
       case (3);  string = "tan(beta)"
       case (4);  string = "sgn(mu)"
       case (5);  string = "A"
       case default;  string = ""
       end select
    case (GMSB)
       select case (i)
       case (0);  string = "mGMSB parameters"
       case (1);  string = "Lambda"
       case (2);  string = "M_mes"
       case (3);  string = "tan(beta)"
       case (4);  string = "sgn(mu)"
       case (5);  string = "N_5"
       case (6);  string = "c_grav"
       case default;  string = ""
       end select
    case (AMSB)
       select case (i)
       case (0);  string = "mAMSB parameters"
       case (1);  string = "m_0"
       case (2);  string = "m_3/2"
       case (3);  string = "tan(beta)"
       case (4);  string = "sgn(mu)"
       case default;  string = ""
       end select
    case default
       select case (i)
       case (0);  string = "Model parameters"
       case default;  string = ""
       end select
    end select
  end function minpar_comment

  subroutine minpar_create (minpar, model)
    type(slha_minpar), intent(inout) :: minpar
    integer, intent(in), optional :: model
    minpar%Q_max = 0
    nullify (minpar%MSSM)
    nullify (minpar%SUGRA)
    nullify (minpar%GMSB)
    nullify (minpar%AMSB)
    if (present (model)) then
       minpar%mtype = model
       select case (model)
       case (:-1)
       case (MSSM);   allocate (minpar%MSSM)
       case (SUGRA);  allocate (minpar%SUGRA)
       case (GMSB);   allocate (minpar%GMSB)
       case (AMSB);   allocate (minpar%AMSB)
       case default
          allocate (minpar%MSSM)
          minpar%MSSM%tanb = 1
       end select
    else
       minpar%mtype = -1
    end if
  end subroutine minpar_create

  subroutine minpar_destroy (minpar)
    type(slha_minpar), intent(inout) :: minpar
    if (associated (minpar%MSSM))   deallocate (minpar%MSSM)
    if (associated (minpar%SUGRA))  deallocate (minpar%SUGRA)
    if (associated (minpar%GMSB))   deallocate (minpar%GMSB)
    if (associated (minpar%AMSB))   deallocate (minpar%AMSB)
  end subroutine minpar_destroy

  subroutine minpar_reset (minpar, modsel)
    type(slha_minpar), pointer :: minpar
    type(slha_modsel), pointer :: modsel
    if (associated (minpar) .and. associated (modsel)) then
       if (minpar%mtype /= modsel%mtype) then
          call minpar_destroy (minpar)
          call minpar_create (minpar, modsel%mtype)
       end if
    else
       call slha_bug ("Attempt to access MINPAR/MODSEL: null pointer found")
    end if
  end subroutine minpar_reset

  subroutine minpar_set_value (minpar, i, ok, value)
    type(slha_minpar), intent(inout) :: minpar
    integer, intent(in) :: i
    logical, intent(out) :: ok
    real(kind=default), intent(in) :: value
    ok = .true.
    if (i==0) then
       minpar%Q_max = value
    else
       select case (minpar%mtype)
       case (:-1)
       case (SUGRA)
          select case (i)
          case (1);  minpar%SUGRA%m_zero = value
          case (2);  minpar%SUGRA%m_half = value
          case (3);  minpar%SUGRA%tanb = value
          case (4);  minpar%SUGRA%sgn_mu = value
          case (5);  minpar%SUGRA%A0 = value
          case default
             ok = .false.
          end select
       case (GMSB)
          select case (i)
          case (1);  minpar%GMSB%Lambda = value
          case (2);  minpar%GMSB%M_mes = value
          case (3);  minpar%GMSB%tanb = value
          case (4);  minpar%GMSB%sgn_mu = value
          case (5);  minpar%GMSB%N_5 = value
          case (6);  minpar%GMSB%c_grav = value
          case default
             ok = .false.
          end select
       case (AMSB)
          select case (i)
          case (1);  minpar%AMSB%m_zero = value
          case (2);  minpar%AMSB%m_grav = value
          case (3);  minpar%AMSB%tanb = value
          case (4);  minpar%AMSB%sgn_mu = value
          case default
             ok = .false.
          end select
       case default
          select case (i)
          case (3);  minpar%MSSM%tanb = value
          case default
             ok = .false.
          end select
       end select
    end if
  end subroutine minpar_set_value

  subroutine minpar_get_value (minpar, i, ok, value)
    type(slha_minpar), intent(in) :: minpar
    integer, intent(in) :: i
    logical, intent(out) :: ok
    real(kind=default), intent(out) :: value
    ok = .true.
    if (i==0) then
       value = minpar%Q_max
    else
       select case (minpar%mtype)
       case (:-1)
          value = 0
          ok = .false.
       case (SUGRA)
          select case (i)
          case (1);  value = minpar%SUGRA%m_zero
          case (2);  value = minpar%SUGRA%m_half
          case (3);  value = minpar%SUGRA%tanb
          case (4);  value = minpar%SUGRA%sgn_mu
          case (5);  value = minpar%SUGRA%A0
          case default
             value = 0
             ok = .false.
          end select
       case (GMSB)
          select case (i)
          case (1);  value = minpar%GMSB%Lambda
          case (2);  value = minpar%GMSB%M_mes
          case (3);  value = minpar%GMSB%tanb
          case (4);  value = minpar%GMSB%sgn_mu
          case (5);  value = minpar%GMSB%N_5
          case (6);  value = minpar%GMSB%c_grav
          case default
             value = 0
             ok = .false.
          end select
       case (AMSB)
          select case (i)
          case (1);  value = minpar%AMSB%m_zero
          case (2);  value = minpar%AMSB%m_grav
          case (3);  value = minpar%AMSB%tanb
          case (4);  value = minpar%AMSB%sgn_mu
          case default
             value = 0
             ok = .false.
          end select
       case default
          select case (i)
          case (3);  value = minpar%MSSM%tanb
          case default
             value = 0
             ok = .false.
          end select
       end select
    end if
  end subroutine minpar_get_value

  function mass_comment (code) result (string)
    integer, intent(in) :: code
    character(len=SLHA_COMMENT_LEN) :: string
    select case (code)
    case (0);  string = "MSSM mass parameters"
    case default;  string = particle_name (code)
    end select
  end function mass_comment

  subroutine rewrite_block_mass (b)
    type(slha_block), pointer :: b
    call destroy (b%contents)
    call create (b%contents)
    call append (b%contents, trim (header_name (b)))
include "slha_prt.inc"
  contains
    subroutine handle (code)
      integer, intent(in) :: code
      logical :: ok
      character(len=BUFFER_SIZE) :: string
      string = string_value (b, code, ok=ok)
      if (ok)  call append (b%contents, trim (string))
    end subroutine handle
  end subroutine rewrite_block_mass

  subroutine mass_set_value (mass, i, ok, value)
    type(slha_mass), intent(inout) :: mass
    integer, intent(in) :: i
    logical, intent(out) :: ok
    real(kind=default), intent(in) :: value
    ok = .true.
    select case (i)
include "slha_set_mass.inc"
    case default
       ok = .false.
    end select
  end subroutine mass_set_value

  subroutine mass_get_value (mass, i, ok, value)
    type(slha_mass), intent(in) :: mass
    integer, intent(in) :: i
    logical, intent(out) :: ok
    real(kind=default), intent(out) :: value
    ok = .true.
    select case (i)
include "slha_get_mass.inc"
    case default
       value = 0
       ok = .false.
    end select
  end subroutine mass_get_value

  function alpha_comment (i) result (string)
    integer, intent(in) :: i
    character(len=SLHA_COMMENT_LEN) :: string
    select case (i)
    case (0);  string = "Higgs mixing angle"
    case (1);  string = "alpha"
    case default;  string = ""
    end select
  end function alpha_comment

  subroutine alpha_set_value (alpha, ok, value)
    type(slha_alpha), intent(inout) :: alpha
    logical, intent(out) :: ok
    real(kind=default), intent(in) :: value
    alpha%al_h = value
    ok = .true.
  end subroutine alpha_set_value

  subroutine alpha_get_value (alpha, ok, value)
    type(slha_alpha), intent(in) :: alpha
    logical, intent(out) :: ok
    real(kind=default), intent(out) :: value
    value = alpha%al_h
    ok = .true.
  end subroutine alpha_get_value

  function hmix_comment (i) result (string)
    integer, intent(in) :: i
    character(len=SLHA_COMMENT_LEN) :: string
    select case (i)
    case (0);  string = "Higgs sector parameters"
    case (1);  string = "mu Parameter"
    case (2);  string = "tanb"
    case (3);  string = "v (Higgs VEV)"
    case (4);  string = "mA^2"
    case default;  string = ""
    end select
  end function hmix_comment

  subroutine hmix_set_value (hmix, i, ok, value)
    type(slha_hmix), intent(inout) :: hmix
    integer, intent(in) :: i
    logical, intent(out) :: ok
    real(kind=default), intent(in) :: value
    ok = .true.
    select case (i)
    case (0);  hmix%Q = value
    case (1);  hmix%mu_h = value
    case (2);  hmix%tanb_h = value
    case (3);  hmix%v = value
    case (4);  hmix%MAsq = value
    case default
       ok = .false.
    end select
  end subroutine hmix_set_value

  subroutine hmix_get_value (hmix, i, ok, value)
    type(slha_hmix), intent(in) :: hmix
    integer, intent(in) :: i
    logical, intent(out) :: ok
    real(kind=default), intent(out) :: value
    ok = .true.
    select case (i)
    case (0);  value = hmix%Q
    case (1);  value = hmix%mu_h
    case (2);  value = hmix%tanb_h
    case (3);  value = hmix%v
    case (4);  value = hmix%MAsq
    case default
       value = 0
       ok = .false.
    end select
  end subroutine hmix_get_value

  function nmssmrun_comment (i) result (string)
    integer, intent(in) :: i
    character(len=SLHA_COMMENT_LEN) :: string
    select case (i)
    case (0);  string = "NMSSM Higgs sector parameters"
    case (1);  string = "Trilinear S-H1-H2 coupling"
    case (2);  string = "Trilinear S coupling"
    case (3);  string = "Soft trilinear S-H1-H2 coupling"
    case (4);  string = "Soft trilinear S coupling"
    case (5);  string = "Singlet vev (scaled by lambda)"
    case default;  string = ""
    end select
  end function nmssmrun_comment

  subroutine nmssmrun_set_value (nmssmrun, i, ok, value)
    type(slha_nmssmrun), intent(inout) :: nmssmrun
    integer, intent(in) :: i
    logical, intent(out) :: ok
    real(kind=default), intent(in) :: value
    ok = .true.
    select case (i)
    case (0);  nmssmrun%ls = value
    case (1);  nmssmrun%ls = value
    case (2);  nmssmrun%ks = value
    case (3);  nmssmrun%a_ls = value
    case (4);  nmssmrun%a_ks = value
    case (5);  nmssmrun%nmu = value
    case default
       ok = .false.
    end select
  end subroutine nmssmrun_set_value

  subroutine nmssmrun_get_value (nmssmrun, i, ok, value)
    type(slha_nmssmrun), intent(in) :: nmssmrun
    integer, intent(in) :: i
    logical, intent(out) :: ok
    real(kind=default), intent(out) :: value
    ok = .true.
    select case (i)
    case (0);  value = nmssmrun%Q 
    case (1);  value = nmssmrun%ls 
    case (2);  value = nmssmrun%ks
    case (3);  value = nmssmrun%a_ls
    case (4);  value = nmssmrun%a_ks
    case (5);  value = nmssmrun%nmu
    case default
       value = 0
       ok = .false.
    end select
  end subroutine nmssmrun_get_value

  subroutine create_mix (mix, lbound, ubound)
    type(slha_mix), intent(inout) :: mix
    integer, intent(in) :: lbound, ubound
    mix%Q = 0
    allocate (mix%c(lbound:ubound,lbound:ubound))
  end subroutine create_mix

  subroutine destroy_mix (mix)
    type(slha_mix), intent(inout) :: mix
    deallocate (mix%c)
  end subroutine destroy_mix

  subroutine mix_set_value (mix, value, i, j, ok)
    type(slha_mix), intent(inout) :: mix
    real(kind=default), intent(in) :: value
    integer, intent(in) :: i
    integer, intent(in), optional :: j
    logical, intent(out), optional :: ok
    if (present (j)) then
       if (lbound(mix%c,1)<=i .and. i<=ubound(mix%c,1)) then
          if (lbound(mix%c,2)<=j .and. j<=ubound(mix%c,2)) then
             mix%c(i,j) = value
             if (present (ok))  ok = .true.
          else
             if (present (ok))  ok = .false.
          end if
       else
          if (present (ok))  ok = .false.
       end if
    else
       select case (i)
       case (0)
          mix%Q = value
          if (present (ok))  ok = .true.
       case default
          if (present (ok))  ok = .false.
       end select
    end if
  end subroutine mix_set_value

  subroutine mix_get_value (mix, value, i, j, ok)
    type(slha_mix), intent(in) :: mix
    real(kind=default), intent(out) :: value
    integer, intent(in) :: i
    integer, intent(in), optional ::  j
    logical, intent(out), optional :: ok
    if (present (j)) then
       if (lbound(mix%c,1)<=i .and. i<=ubound(mix%c,1)) then
          if (lbound(mix%c,2)<=j .and. j<=ubound(mix%c,2)) then
             value = mix%c(i,j)
             if (present (ok))  ok = .true.
          else
             value = 0
             if (present (ok))  ok = .false.
          end if
       else
          value = 0
          if (present (ok))  ok = .false.
       end if
    else
       select case (i)
       case (0)
          value = mix%Q
          if (present (ok))  ok = .true.
       case default
          value = 0
          if (present (ok))  ok = .true.
       end select
    end if
  end subroutine mix_get_value

  subroutine rewrite_block_mix (b)
    type(slha_block), pointer :: b
    integer :: i, j
    integer :: ival
    real(kind=default) :: rval
    logical :: ok
    character(len=BUFFER_SIZE) :: string
    call destroy (b%contents)
    call create (b%contents)
    call get_value (b, 0, rval, ival, ok)
    if (ok) then
       call append (b%contents, trim (header_with_value (b, rval)))
    else
       call append (b%contents, trim (header_name (b)))
    end if
    do i = lbound(b%mix%c,1), ubound (b%mix%c,1)
       do j = lbound(b%mix%c,2), ubound (b%mix%c,2)
          string = string_value (b, i, j, ok=ok)
          if (ok)  call append (b%contents, trim (string))
       end do
    end do
  end subroutine rewrite_block_mix

  subroutine handle_info_block (b, prefix, ignore_errors)
    type(slha_block), pointer :: b
    character(len=*), intent(in) :: prefix
    logical, intent(in) :: ignore_errors
    type(line_t), pointer :: line
    integer :: ival
    real(kind=default) :: rval
    character(len=BUFFER_SIZE) :: string
    integer :: iostat
    logical :: ok
    character(len=BUFFER_SIZE) :: value
    integer :: i, s
    logical :: error_found
    error_found = .false.
    line => first_line (b%contents)
    call read_Q_value (line, rval, ok)
    call set_value (b, 0, rval, ival, ok)
    READLINE: do
       line => next (line)
       if (.not.associated(line))  exit READLINE
       select case (line_type (line))
       case (IS_DATA)
          string = remove_comment (line_string (line))
          ival = 0;  rval = 0
          read (string, *, iostat=iostat)  i, value
          if (iostat == 0) then
             if (0 <= i .and. i < 10) then
                s = scan (string, achar (iachar("0") + i)) + 1
                string = adjustl (string(s:))
             end if
             select case (i)
             case (1);  call slha_message (prefix//" name:    "//trim(string))
             case (2);  call slha_message (prefix//" version: "//trim(string))
             case (3);  call slha_message (prefix//" warning: "//trim(string))
             case (4);  call slha_message (prefix//" error:   "//trim(string))
                error_found = .true.
             end select
           else
              call slha_print_line (line)
              call slha_block_syntax_error (b%block_type)
           end if
        case (IS_EMPTY)
        case default
           exit READLINE
        end select
     end do READLINE
    if (error_found) then
       if (ignore_errors) then
          call slha_error (prefix //" signaled error(s)")
       else
          call slha_fatal (prefix //" signaled error(s)")
       end if
    end if
  end subroutine handle_info_block

  subroutine read_decay (b, line)
    type(slha_block), pointer :: b
    type(line_t), pointer :: line
    type(slha_decay), pointer :: decay
    integer :: code
    real(kind=default) :: value
    character(len=BUFFER_SIZE) :: string
    integer :: iostat
    logical :: ok
    decay => b%decay
    if (.not.associated (decay)) &
         & call slha_bug ('Decay block is not allocated as such')
    string = remove_comment (line_string (line))
    read (string(7:), *, iostat=iostat) code, value
    if (iostat == 0) then
       call set_value (b, code, value, 0, ok)
       if (.not.ok) then
          write (msg_buffer, '(A)')  line_string (line)
          call msg_message
          call slha_warning ("Unknown particle code in DECAY line")
       end if
    else
       call slha_print_line (line)
       call slha_error ('Syntax error in DECAY line')
    end if
  end subroutine read_decay

  subroutine rewrite_block_decay (b)
    type(slha_block), pointer :: b
    call destroy (b%contents)
    call create (b%contents)
include "slha_prt.inc"
  contains
    subroutine handle (code)
      integer, intent(in) :: code
      logical :: ok
      character(len=BUFFER_SIZE) :: string
      real(kind=default) :: rval
      integer :: ival
      call get_value (b, code, rval, ival, ok)
      if (ok) then
         string = header_decay (b, code, rval)
         call append (b%contents, trim (string))
      end if
    end subroutine handle
  end subroutine rewrite_block_decay

  subroutine decay_set_value (decay, code, ok, value)
    type(slha_decay), intent(inout) :: decay
    integer, intent(in) :: code
    logical, intent(out) :: ok
    real(kind=default), intent(in) :: value
    ok = .true.
    select case (abs(code))
include "slha_set_decay.inc"
    case default
       ok = .false.
    end select
  end subroutine decay_set_value

  subroutine decay_get_value (decay, code, ok, value)
    type(slha_decay), intent(in) :: decay
    integer, intent(in) :: code
    logical, intent(out) :: ok
    real(kind=default), intent(out) :: value
    ok = .true.
    select case (abs(code))
include "slha_get_decay.inc"
    case default
       value = 0
       ok = .false.
    end select
  end subroutine decay_get_value

  function contains_whizard_input (slha)
    type(slha_data), intent(in) :: slha
    logical :: contains_whizard_input
    contains_whizard_input = block_exists (slha, "WHIZARD_INPUT")
  end function contains_whizard_input

  subroutine write_whizard_input_name (name, slha)
    character(len=*), intent(in) :: name
    type(slha_data), intent(in) :: slha
    integer :: u
    u = free_unit ()
    open (u, file=name, action="write", status="replace")
    call write_whizard_input_unit (u, slha)
    close (u)
  end subroutine write_whizard_input_name

  subroutine write_whizard_input_unit (u, slha)
    integer, intent(in) :: u
    type(slha_data), intent(in) :: slha
    if (contains_whizard_input (slha)) then
       call write (u, line_list_nth (slha%whizard_input%contents, 2))
    end if
  end subroutine write_whizard_input_unit

  subroutine slha_write_process &
       & (unit, code_in, code_out, integral, error, chi2)
    integer, intent(in) :: unit
    integer, intent(in), dimension(:) :: code_in, code_out
    real(kind=default), intent(in) :: integral, error, chi2
    integer :: i
1   format('#',1x,A)
    write (unit, slha_header_format ()) "PROCESS", &
         & "Result of cross section calculation"
2   format(1x,I4,2x,I9,9x,'#',1x,A,1x,A)
    write (unit, 2)  1, size (code_in), &
         & "Number of incoming particles"
    do i = 1, size (code_in)
       write (unit, 2)  2, code_in(i), &
            & "  Incoming particle:", particle_name (code_in(i))
    end do
    write (unit, 2) 11, size (code_out), &
         & "Number of outgoing particles"
    do i = 1, size (code_out)
       write (unit, 2) 12, code_out(i), &
            & "  Outgoing particle:", particle_name (code_out(i))
    end do
3   format(1x,I4,2x,1P,E16.8,2x,'#',1x,A)
    write (unit, 3) 21, integral, "Cross section in fb"
    write (unit, 3) 22, error,    "Cross section error in fb"
    write (unit, 3) 23, chi2,     "Chi-squared value"
    write (unit, 1) 
  end subroutine slha_write_process

  subroutine slha_write_whizard_info (unit)
    integer, intent(in) :: unit
1   format ('#',1x,A)
    write (unit, slha_header_format ()) "CSINFO", &
         & "Cross section calculator information"
2   format (3x, I2, 4x, A, 1x, '#', 1x, A)
    write (unit, 2) 1, "WHIZARD   ", "Calculator name"
    write (unit, 2) 2, "1.95      ", "Calculator version"
    call msg_listing (WARNING, unit, " 3 ")
    call msg_listing (ERROR, unit, " 4 ")
    write (unit, 1)
  end subroutine slha_write_whizard_info


end module slha_interface
