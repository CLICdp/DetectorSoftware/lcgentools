! $Id: parameters.SMZprime.omega.f90,v 1.1 2004/05/04 14:37:17 reuter Exp $
!
! Copyright (C) 2000-2008 by Wolfgang Kilian <wolfgang.kilian@desy.de>,
! Thorsten Ohl <ohl@physik.uni-wuerzburg.de>, and Juergen Reuter 
! <juergen.reuter@desy.de>
!
! WHIZARD is free software; you can redistribute it and/or modify it
! under the terms of the GNU General Public License as published by
! the Free Software Foundation; either version 2, or (at your option)
! any later version.
!
! WHIZARD is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

module omega_parameters_whizard
  use omega_kinds !NODEP!
  use omega_constants !NODEP!
  use parameters
  implicit none
  private
  public :: import_from_whizard
  real(kind=omega_prec), dimension(45), save, public :: mass = 0, width = 0
  real(kind=omega_prec), parameter, public :: GeV = 1.0_omega_prec
  real(kind=omega_prec), parameter, public :: MeV = GeV / 1000
  real(kind=omega_prec), parameter, public :: keV = MeV / 1000
  real(kind=omega_prec), parameter, public :: TeV = GeV * 1000
  real(kind=omega_prec), save, public :: &
       alpha = 1.0_omega_prec / 137.0359895_omega_prec
  complex(kind=omega_prec), save, private :: vev
  complex(kind=omega_prec), save, public :: &
       qlep = 0, qup = 0, qdwn = 0, gcc = 0, qw = 0, &
       gzww = 0, gwww = 0, ghww = 0, ghhww = 0, ghzz = 0, ghhzz = 0, &
       ghbb = 0, ghtt = 0, ghcc = 0, ghtautau = 0, gh3 = 0, gh4 = 0, &
       iqw = 0, igzww = 0, igwww = 0, &
       gw4 = 0, gzzww = 0, gazww = 0, gaaww = 0, &
       gs = 0, igs = 0
  complex(kind=omega_prec), dimension(2), save, public :: &
       gnclep = 0, gncneu = 0, gncup = 0, gncdwn = 0
  complex(kind=omega_prec), dimension(2), save, public :: &
      gnchup = 0, gnchdwn = 0, gnchneu = 0, gnchlep = 0
contains
  subroutine import_from_whizard (par)
    type(parameter_set), intent(in) :: par
    real(kind=omega_prec) :: e, g, qelep, qeup, qedwn, sin2thw, sinthw, costhw
    real(kind=omega_prec) :: sinpsi, cospsi, atpsi
    mass(1:34) = 0
    width(1:34) = 0
    mass(3) = par%ms
    mass(4) = par%mc
    mass(5) = par%mb
    mass(6) = par%mtop
    width(6) = par%wtop
    mass(11) = par%me
    mass(13) = par%mmu
    mass(15) = par%mtau
    mass(23) = par%mZ
    width(23) = par%wZ
    mass(24) = par%mW
    width(24) = par%wW
    mass(25) = par%mH
    width(25) = par%wH
    mass(32) = par%mZH
    width(32) = par%wZH
    vev = 2 * par%mW * par%sw / par%ee 
    e = par%ee
    sinthw = par%sw
    sin2thw = sinthw**2
    costhw = par%cw
    qelep = - 1.0_omega_prec
    qeup = 2.0_omega_prec / 3.0_omega_prec
    qedwn = - 1.0_omega_prec / 3.0_omega_prec
    g = e / sinthw
    gcc = - g / 2 / sqrt (2.0_double)
    gncneu(1) = - g / 2 / costhw * ( + 0.5_double)
    gnclep(1) = - g / 2 / costhw * ( - 0.5_double - 2 * qelep * sin2thw)
    gncup(1)  = - g / 2 / costhw * ( + 0.5_double - 2 * qeup  * sin2thw)
    gncdwn(1) = - g / 2 / costhw * ( - 0.5_double - 2 * qedwn * sin2thw)
    gncneu(2) = - g / 2 / costhw * ( + 0.5_double)
    gnclep(2) = - g / 2 / costhw * ( - 0.5_double)
    gncup(2)  = - g / 2 / costhw * ( + 0.5_double)
    gncdwn(2) = - g / 2 / costhw * ( - 0.5_double)
    qlep = - e * qelep
    qup = - e * qeup
    qdwn = - e * qedwn
    qw = e
    iqw = (0,1)*qw
    gzww = g * costhw
    igzww = (0,1)*gzww
    gwww = g
    igwww = (0,1)*gwww
    ghww = mass(24) * g
    ghhww = g**2 / 2.0_omega_prec
    ghzz = mass(23) * g / costhw
    ghhzz = g**2 / 2.0_omega_prec / costhw**2
    ghtt = - mass(6) / vev
    ghbb = - mass(5) / vev
    ghcc = - mass(4) / vev
    ghtautau = - mass(15) / vev
    gh3 = - 3 * mass(25)**2 / vev
    gh4 = - 3 * mass(25)**2 / vev**2
    gw4 = gwww**2
    gzzww = gzww**2
    gazww = gzww * qw
    gaaww = qw**2
    !!! Color flow basis, divide by sqrt(2)
    gs = sqrt(2.0_omega_prec*PI*par%alphas)
    igs = (0.0_omega_prec, 1.0_omega_prec) * gs    
!!! Additional parameters to the Standard Model
!!! There are two independent possibilities to set the couplings of the heavy
!!! Z boson: multiples of the Z couplings and and explicit V and A coupling
    gnchlep(1) = par%v_lep * gnclep(1) + par%glepv
    gnchneu(1) = par%v_neu * gncneu(1) + par%gneuv
    gnchup(1)  = par%v_up  * gncup(1)  + par%gupv
    gnchdwn(1) = par%v_dwn * gncdwn(1) + par%gdwnv
    gnchlep(2) = par%a_lep * gnclep(2) + par%glepa
    gnchneu(2) = par%a_neu * gncneu(2) + par%gneua
    gnchup(2)  = par%a_up  * gncup(2)  + par%gupa
    gnchdwn(2) = par%a_dwn * gncdwn(2) + par%gdwna
  end subroutine import_from_whizard
end module omega_parameters_whizard
