! $Id: parameters.NMSSM.omega.f90,v 1.1 2005/10/25 09:21:48 reuter Exp $
!
! Copyright (C) 2000 by Thorsten Ohl <ohl@hep.tu-darmstadt.de>
! and others
!
! O'Mega is free software; you can redistribute it and/or modify it
! under the terms of the GNU General Public License as published by
! the Free Software Foundation; either version 2, or (at your option
! any later version.
!
! O'Mega is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
module omega_parameters_whizard
  use omega_kinds !NODEP!
  use omega_constants !NODEP!
  use parameters
  implicit none
  private
  public :: import_from_whizard 
  real(kind=omega_prec), dimension(73), save, public :: mass = 0, width = 0
  real(kind=omega_prec), parameter, public :: GeV = 1.0_omega_prec
  real(kind=omega_prec), parameter, public :: MeV = GeV / 1000
  real(kind=omega_prec), parameter, public :: keV = MeV / 1000
  real(kind=omega_prec), parameter, public :: TeV = GeV * 1000
  real(kind=omega_prec), save, public :: &
       alpha = 1.0_omega_prec / 137.0359895_omega_prec, &
       sin2thw = 0.23124_omega_prec
  integer, save, public :: &
       sign1 = +1, sign2 = +1, sign3 = +1, sign4 = +1, sign5 = +1
  real(kind=omega_prec), save, public :: &
       sigch1 = +1, sigch2 = +1 
  complex(kind=omega_prec), save, private :: vev
  complex(kind=omega_prec), save, public :: imago
  real(kind=omega_prec), public, save :: sind = 0._omega_prec, & 
       cosd = 1._omega_prec, sinckm12 = 0.223_omega_prec, &
       sinckm13 = 0.004_omega_prec, sinckm23 = 0.04_omega_prec, &
       tana = 30._omega_prec, tanb = 30._omega_prec, as = 0._omega_prec
  real(kind=omega_prec), public, save :: cos2am2b, sin2am2b, sinamb, & 
       sinapb, cosamb, cosapb, cos4be, sin4be, sin4al, sin2al, sin2be, cos2al, &
       cos2be, cosbe, sinbe, cosal, sinal, costhw, sinthw
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  integer, parameter :: dimh0 = 3, dimA0 = 2, dimNeu = 5 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!
!!!Higgs Mixing
!!!!!!!!!!!!
  real(kind=omega_prec), dimension(dimh0,dimh0), public, save :: mix_h0 = 0
  real(kind=omega_prec), dimension(2,3), public, save :: mix_A0 = 0  
!!!!!!!!!!!!!!!!
!!!!!Neutralino-Mixing
!!!!!!!!!!!!!!
  complex(kind=omega_prec), dimension(5,5), public, save :: mix_neu = 0
  integer :: neu1, neu2
!!!!!!!!!!!!!
!!!Chargino Mixing
!!!!!!!!!!!!!
  complex(kind=omega_prec), dimension(2,2), public, save :: mix_charU = 0, mix_charV = 0
!!!!!!!!!!!!
!!!CKM-MAtrix
!!!!!!!!!!!!!!
  complex(kind=omega_prec), dimension(3,3), public, save :: vckm = 0
  real(kind=omega_prec), public, save :: q_lep, q_up, q_down 
  complex(kind=omega_prec), public, save :: gcc, qchar, qdwn, qup, qlep, & 
       gz, g, e, gs
  complex(kind=omega_prec), save, public :: xia = 1, xi0 = 1, xipm = 1
  complex(kind=omega_prec), dimension(2), public, save :: gncdwn
  complex(kind=omega_prec), dimension(2), public, save :: gncup
  complex(kind=omega_prec), dimension(2), public, save :: gnclep
  complex(kind=omega_prec), dimension(2), public, save :: gncneu

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!Couplings!!!
!! the Lorenz structure is encoded in the model3.ml file in term of
!! S,P,SP,SLR,VLR,.. (rather instructive!). If the type is one of 
!! SP,SLR,VLR,VA the targets.ml file creates an exrta 2-dim 
!! structure which appears as !first! rank of the correspnding 
!! array in this file. 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!chargino fermion sfermion (slr,gen,char,sfm)
  complex(kind=omega_prec), dimension(2,3,2,1), public,  save :: &
       g_yuk_char_lsn, g_yuk_char_lsn_c 
  complex(kind=omega_prec), dimension(2,3,2,2), public,  save :: &
       g_yuk_char_nsl, g_yuk_char_nsl_c 
  complex(kind=omega_prec), dimension(2,3,3,2,2), public,  save :: &
       g_yuk_char_dsu, g_yuk_char_dsu_c, g_yuk_char_usd, g_yuk_char_usd_c  
!!!charged higgs to SLeptons (gauge basis)
 complex(kind=omega_prec), dimension(3),public, save :: & 
      g_hp_slLsnL, g_hp_slRsnL, g_hp_slLsnL_c, g_hp_slRsnL_c
!!!charged higgs to SLeptons (mass basis)
 complex(kind=omega_prec), dimension(3),public, save :: &
      g_hp_sl1sn1, g_hp_sl2sn1, g_hp_sl1sn1_c, g_hp_sl2sn1_c
!!!charged higgs to SQuarks  !!!(gauge basis)
 complex(kind=omega_prec), dimension(3,3),public, save :: & 
      g_hp_suLsdL, g_hp_suRsdL, g_hp_suLsdR, g_hp_suRsdR
!!!charged higgs to SQuarks (mass basis)
 complex(kind=omega_prec), dimension(3,3),public, save :: &
      g_hp_su1sd1, g_hp_su2sd1, g_hp_su1sd2, g_hp_su2sd2, &
      g_hp_su1sd1_c, g_hp_su2sd1_c, g_hp_su1sd2_c, g_hp_su2sd2_c
!!!Matrix structure coupling neutral Higgs to SQuarks dim(shiggs, gen) final mass eigenstate coupling (rep gh1su1su2_3 ...)
  complex(kind=omega_prec), dimension(dimh0,3), public, save :: &
       g_h0_su1su1, g_h0_su2su2, g_h0_su1su2, g_h0_su2su1, &
       g_h0_sd1sd1, g_h0_sd2sd2, g_h0_sd1sd2, g_h0_sd2sd1, &
       g_h0_sl1sl1, g_h0_sl2sl2, g_h0_sl1sl2, g_h0_sl2sl1, & 
       g_h0_sn1sn1
  complex(kind=omega_prec), dimension(2,3), public, save :: &
       g_A0_su1su1, g_A0_su2su2, g_A0_su1su2, g_A0_su2su1, &
       g_A0_sd1sd1, g_A0_sd2sd2, g_A0_sd1sd2, g_A0_sd2sd1, &
       g_A0_sl1sl1, g_A0_sl2sl2, g_A0_sl1sl2, g_A0_sl2sl1, &  
       g_A0_sn1sn1, g_A0_sn2sn2, g_A0_sn1sn2, g_A0_sn2sn1
!!! Matrix structure coupling neutral Higgs to SQuarks 
!!! dim(shiggs, gen) gauge eigenstate coupling 
!!! (rep g_h1222susu ...) to be multiplied w/ mix_su
   complex(kind=omega_prec), dimension(dimh0,3), public, save :: &
        g_h0_suLsuL, g_h0_suRsuR, g_h0_suLsuR, g_h0_suRsuL, &
        g_h0_sdLsdL, g_h0_sdRsdR, g_h0_sdLsdR, g_h0_sdRsdL, &
        g_h0_slLslL, g_h0_slRslR, g_h0_slLslR, g_h0_slRslL, & 
        g_h0_snLsnL 
   complex(kind=omega_prec), dimension(2,3), public, save :: &
        g_A0_suLsuR, g_A0_suRsuL, &
        g_A0_sdLsdR, g_A0_sdRsdL, &
        g_A0_slLslR, g_A0_slRslL 
  integer :: shiggs, phiggs
!!!Matrix structure coupling neutral Higgs to fermions dim (s/phiggs, gen)
  complex(kind=omega_prec), dimension(dimh0,3), public, save :: &
       g_yuk_h0_uu, g_yuk_h0_dd, g_yuk_h0_ll   
  complex(kind=omega_prec), dimension(2,3), public, save :: &
       g_yuk_A0_uu, g_yuk_A0_dd, g_yuk_A0_ll   
  integer ::gen, gen1, gen2
!!!Charged Higgs to Quarks (SLR, gen1,gen2)
  complex(kind=omega_prec), dimension(2,3,3), public, save :: g_yuk_hp_ud, g_yuk_hm_du
!!!Charged Higgs to Leptons (gen)
  complex(kind=omega_prec), dimension(3), public, save :: g_yuk_hp_ln  
!!!3- VErtex Higgs Gauge
!!!Z/photon to charged Higgses
  complex(kind=omega_prec), public, save ::  g_Ahmhp, g_Zhmhp
!!!Z to (shiggs,phiggs)
  complex(kind=omega_prec), dimension(dimh0,2), public, save :: g_Zh0A0 
!!!W to h+ , (shiggs)
  complex(kind=omega_prec), dimension(dimh0), public, save :: g_Whph0
!!!W to h+ , (phiggs)
  complex(kind=omega_prec), dimension(2), public, save :: g_WhpA0
!!!ZZ to (shiggs)
  complex(kind=omega_prec), dimension(dimh0), public, save :: g_ZZh0
!!!WW to (shiggs)
  complex(kind=omega_prec), dimension(dimh0), public, save :: g_WWh0
!!!4- Vertex Higgs Gauge
!!!WW (shiggs1, shiggs2) , ZZ (shiggs1, shiggs2)  
  complex(kind=omega_prec), dimension(dimh0,dimh0), public, save :: &
       g_WWh0h0 , g_ZZh0h0
!!!WZ hp (shiggs) , WA hp (shiggs)  
  complex(kind=omega_prec), dimension(dimh0), public, save :: &
       g_ZWhph0 , g_AWhph0
!!!WW (phiggs1, phiggs2) , ZZ (phiggs1, phiggs2)  
  complex(kind=omega_prec), dimension(2,2), public, save :: &
       g_WWA0A0 , g_ZZA0A0
!!!WZ hp (phiggs) , WA hp (phiggs)  
  complex(kind=omega_prec), dimension(2), public, save :: g_ZWhpA0 , g_AWhpA0
  complex(kind=omega_prec), public, save :: g_ZZhphm, g_ZAhphm, & 
       g_AAhphm, g_WWhphm   
!!!Triple Higgs couplings
  complex(kind=omega_prec), dimension(dimh0,dimh0,dimh0) ,public, save :: g_h0h0h0
  complex(kind=omega_prec), dimension(dimh0,2,2) ,public, save :: g_h0A0A0
  complex(kind=omega_prec), dimension(dimh0) ,public, save :: g_h0hphm
  integer :: shiggs1, shiggs2, shiggs3, phiggs1, phiggs2
!!!Neutral Higgs to Neutralinos (SLR, neu1 , neu2, s/phiggs)
  complex(kind=omega_prec), dimension(2,5,5,dimh0) ,public, save :: g_neuneuh0, g_neuneuh0_1  
  complex(kind=omega_prec), dimension(2,5,5,2) ,public, save :: g_neuneuA0, g_neuneuA0_1
!!!Neutral Higgs to Charginos (SLR, char1 , char2, s/phiggs)
  complex(kind=omega_prec), dimension(2,2,2,dimh0) ,public, save :: g_chchh0  
  complex(kind=omega_prec), dimension(2,2,2,2) ,public, save :: g_chchA0
  integer :: ch1, ch2
!!!Chargino, charged Higgs, Neutralino (SLR, neu, char)
  complex(kind=omega_prec), dimension(2,5,2) ,public, save :: &
       g_neuhmchar, g_neuhmchar_c
!!! Neutralino - Fermion -Sfermion
!!!!!(SLR,gen,neu,sfm)
  complex(kind=omega_prec), dimension(2,3,5,2) ,public, save :: &
       g_yuk_neu_lsl, g_yuk_neu_usu, g_yuk_neu_dsd, &
       g_yuk_neu_lsl_c, g_yuk_neu_usu_c, g_yuk_neu_dsd_c, &
       g_yuk_neu_nsn, g_yuk_neu_nsn_c
  !  complex(kind=omega_prec), dimension(3,5,1) ,public, save :: &
  ! g_yuk_neu_nsn, g_yuk_neu_nsn_c 
!!! Gluino - Quark -SQuark
!!!!!(SLR,gen,sfm)
  complex(kind=omega_prec), dimension(2,3,2) ,public, save :: &
       g_yuk_gluino_usu, g_yuk_gluino_dsd, g_yuk_gluino_usu_c, g_yuk_gluino_dsd_c
!!! ZZ SFermions  (gen,sfm1,sfm2)
  complex(kind=omega_prec), dimension(3,2,2) ,public, save :: &
       g_zz_slsl, g_zz_snsn, g_zz_sdsd, g_zz_susu 
!!! GaGa SFermions  
  complex(kind=omega_prec), public, save :: &
       g_AA_slsl, g_AA_snsn, g_AA_sdsd, g_AA_susu 
!!! WW SFermions  (gen,sfm1,sfm2)
  complex(kind=omega_prec), dimension(3,2,2) ,public, save :: &
       g_ww_slsl, g_ww_snsn, g_ww_sdsd, g_ww_susu 
!!! ZGa SFermions  (gen,sfm1,sfm2)
  complex(kind=omega_prec), dimension(3,2,2) ,public, save :: &
       g_zA_slsl, g_zA_sdsd, g_zA_susu 
!!! W Ga SLeptons  (gen,sfm1)
  complex(kind=omega_prec), dimension(3,2) ,public, save :: &
       g_wA_slsn, g_wA_slsn_c 
!!! W Z SLeptons  (gen,sfm1)
  complex(kind=omega_prec), dimension(3,2) ,public, save :: &
       g_wz_slsn, g_wz_slsn_c 
!!! W Ga Squarks  (gen1,gen2,sfm1,sfm2)
  complex(kind=omega_prec), dimension(3,3,2,2) ,public, save :: &
       g_wA_susd, g_wA_susd_c 
!!! W Z Squarks  (gen1,gen2,sfm1,sfm2)
  complex(kind=omega_prec), dimension(3,3,2,2) ,public, save :: &
       g_wz_susd, g_wz_susd_c 
!!! Gluon W Squarks  (gen1,gen2,sfm1,sfm2)
  complex(kind=omega_prec), dimension(3,3,2,2) ,public, save :: &
       g_gw_susd, g_gw_susd_c 
!!! Gluon Z Squarks  (gen1,sfm1,sfm2)
  complex(kind=omega_prec), dimension(3,2,2) ,public, save :: &
       g_gz_susu, g_gz_sdsd
!!! Gluon A Squarks 
  complex(kind=omega_prec), public, save :: g_gA_sqsq
!!! Gluon Gluon Squarks 
  complex(kind=omega_prec), public, save :: g_gg_sqsq

!!!SFermion mixing Matrix Style
  complex(kind=omega_prec), dimension(3,2,2), public, save :: &
    mix_sd, mix_su, mix_sl 
  
!!!!!W -slepton -sneutrino
  complex(kind=omega_prec), dimension(3,2), public, save :: g_wslsn, g_wslsn_c
!!!!!W -sup -sdown
  complex(kind=omega_prec), dimension(3,3,2,2), public, save :: g_wsusd, g_wsusd_c
!!!!!!!!!!!!
!!!Z to SFermions
!!!!!!!!!!!!
  complex(kind=omega_prec), dimension(3,1,1), public, save :: g_zsnsn
  complex(kind=omega_prec), dimension(3,2,2), public, save :: g_zslsl
  complex(kind=omega_prec), dimension(3,2,2), public, save :: g_zsusu
  complex(kind=omega_prec), dimension(3,2,2), public, save :: g_zsdsd
!!!NMSSM-parameters
  complex(kind=omega_prec), public, save ::  mu, lambda, &         
       A_lambda, k, A_k, r
  complex(kind=omega_prec), dimension(3), public, save :: al, au, ad
  complex(kind=omega_prec), public, save :: eta1, eta2, eta3, eta4, eta5
  
  complex(kind=omega_prec), public, save :: eidelta, cosckm23, cosckm13, &
       cosckm12,gpzww, gppww, gzzww, gw4, igwww, igzww, iqw, igs, &
       gssq
!!!Charged Current to quarks
  complex(kind=omega_prec), dimension(3,3), public, save :: g_ccq, g_ccq_c 
!!!Neutralino, Chargino, W+/-
  complex(kind=omega_prec), dimension(2,2,5), public, save :: g_cwn, g_nwc
!!!Neutral current to Neutralino
  complex(kind=omega_prec), dimension(2,5,5), public, save :: g_zneuneu
!!!Neutral current to Chargino
  complex(kind=omega_prec), dimension(2,2,2), public, save :: g_zchch
  integer :: i,j,sfm1,sfm2, rdummy
  complex(kind=omega_prec) :: sina, cosa

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!! go: setup input parameters
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
contains
  subroutine import_from_whizard (par)
   type(parameter_set), intent(in) :: par    
   real(kind=omega_prec) :: e, g, sinthw, costhw, qelep, qeup, qedwn, v
    mass(1:73) = 0
    width(1:73) = 0
    mass(3) = par%ms
    mass(4) = par%mc
    mass(5) = par%mb
    mass(6) = par%mtop
    width(6) = par%wtop
    mass(11) = par%me
    mass(13) = par%mmu
    mass(15) = par%mtau
    mass(23) = par%mZ
    width(23) = par%wZ
    mass(24) = par%mW
    width(24) = par%wW
    mass(25) = par%mh0_1
    width(25) = par%wh0_1
    mass(26) =  xi0 * mass(23)
    width(26) =  0
    mass(27) =  xipm * mass(24)
    width(27) =  0
    mass(35) = par%mh0_2
    width(35) = par%wh0_2
    mass(36) = par%mA0_1
    width(36) = par%wA0_1
    mass(37) = par%mHpm
    width(37) = par%wHpm
    mass(41) = par%msd1
    width(41) = par%wsd1
    mass(42) = par%msu1
    width(42) = par%wsu1
    mass(43) = par%mss1
    width(43) = par%wss1
    mass(44) = par%msc1
    width(44) = par%wsc1
    mass(45) = par%msb1
    width(45) = par%wsb1
    mass(46) = par%mstop1
    width(46) = par%wstop1
    mass(47) = par%msd2
    width(47) = par%wsd2
    mass(48) = par%msu2
    width(48) = par%wsu2
    mass(49) = par%mss2
    width(49) = par%wss2
    mass(50) = par%msc2
    width(50) = par%wsc2
    mass(51) = par%msb2
    width(51) = par%wsb2
    mass(52) = par%mstop2
    width(52) = par%wstop2
    mass(53) = par%mse1
    width(53) = par%wse1
    mass(54) = par%msne
    width(54) = par%wsne
    mass(55) = par%msmu1
    width(55) = par%wsmu1
    mass(56) = par%msnmu
    width(56) = par%wsnmu
    mass(57) = par%mstau1
    width(57) = par%wstau1
    mass(58) = par%msntau
    width(58) = par%wsntau
    mass(59) = par%mse2
    width(59) = par%wse2
    mass(61) = par%msmu2
    width(61) = par%wsmu2
    mass(63) = par%mstau2
    width(63) = par%wstau2
    mass(64) = par%mgg
    width(64) = par%wgg
    mass(65) = par%mneu1   
    width(65) = par%wneu1
    mass(66) = par%mneu2   
    width(66) = par%wneu2
    mass(67) = par%mneu3   
    width(67) = par%wneu3
    mass(68) = par%mneu4   
    width(68) = par%wneu4
    mass(69) = par%mneu5   
    width(69) = par%wneu5
    mass(70) = par%mch1   
    width(70) = par%wch1
    mass(71) = par%mch2   
    width(71) = par%wch2
    mass(72) = par%mA0_2
    width(72) = par%wA0_2
    mass(73) = par%mh0_3
    width(73) = par%wh0_3
    sigch1   = par%sigc1 
    sigch2   = par%sigc2
    sign1    = par%sig1
    sign2    = par%sig2
    sign3    = par%sig3
    sign4    = par%sig4
    sign5    = par%sig5
    vckm(1,1)  = 1
    vckm(1,2)  = 0
    vckm(1,3)  = 0
    vckm(2,1)  = 0
    vckm(2,2)  = 1
    vckm(2,3)  = 0
    vckm(3,1)  = 0
    vckm(3,2)  = 0
    vckm(3,3)  = 1
    v = 2 * par%mW * par%sw / par%ee
    e = par%ee
    as = par%alphas
    tanb = par%tanb_h
    !!! Higgs Mixing
    sina = sin(par%al_h)
    cosa = cos(par%al_h)

    mix_h0(1,1) = par%mixh0_11
    mix_h0(1,2) = par%mixh0_12
    mix_h0(1,3) = par%mixh0_13
    mix_h0(2,1) = par%mixh0_21
    mix_h0(2,2) = par%mixh0_22
    mix_h0(2,3) = par%mixh0_23
    mix_h0(3,1) = par%mixh0_31
    mix_h0(3,2) = par%mixh0_32
    mix_h0(3,3) = par%mixh0_33

    mix_A0 = 0
    mix_A0(1,1) = par%mixa0_11
    mix_A0(1,2) = par%mixa0_12
    mix_A0(1,3) = par%mixa0_13
    mix_A0(2,1) = par%mixa0_21
    mix_A0(2,2) = par%mixa0_22
    mix_A0(2,3) = par%mixa0_23
    select case (sign1)
       case (1)
          eta1 = (1.0_omega_prec,0.0_omega_prec)
       case (-1)
          eta1 = (0.0_omega_prec,1.0_omega_prec)
       case default 
          print *, 'sign1', sign1
          stop "parameters_NMSSM: No definite sign neutralino1"
    end select
    select case (sign2)
       case (1)
          eta2 = (1.0_omega_prec,0.0_omega_prec)
       case (-1)
          eta2 = (0.0_omega_prec,1.0_omega_prec)
       case default 
          print *, 'sign2', sign2
          stop "parameters_NMSSM: No definite sign neutralino2"
    end select
    select case (sign3)
       case (1)
          eta3 = (1.0_omega_prec,0.0_omega_prec)
       case (-1)
          eta3 = (0.0_omega_prec,1.0_omega_prec)
       case default 
          print *, 'sign3', sign3
          stop "parameters_NMSSM: No definite sign neutralino3"
    end select
    select case (sign4)
       case (1)
          eta4 = (1.0_omega_prec,0.0_omega_prec)
       case (-1)
          eta4 = (0.0_omega_prec,1.0_omega_prec)
       case default 
          print *, 'sign4', sign4
          stop "parameters_NMSSM: No definite sign neutralino4"
    end select
    select case (sign5)
       case (1)
          eta5 = (1.0_omega_prec,0.0_omega_prec)
       case (-1)
          eta5 = (0.0_omega_prec,1.0_omega_prec)
       case default 
          print *, 'sign5', sign5
          stop "parameters_NMSSM: No definite sign neutralino5"
    end select
    sinthw = par%sw
    sin2thw = sinthw**2
    costhw = par%cw
    qelep = - 1.0_omega_prec
    qeup = 2.0_omega_prec / 3.0_omega_prec
    qedwn = - 1.0_omega_prec / 3.0_omega_prec
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!!  MSSM Limit of the NMSSM, setting the superpotential terms
    !!!  (and corresponding A terms) equal to the mu term, and
    !!!  shrinking the Higgs mixing matrices to the MSSM case
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!!  Further rename the input Block in the SLHA-file from NMIX to NMNMIX 
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!! lambda=0
    !!! k=0
    !!! A_k=0
    !!! A_lambda=0
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!! mix_A0 = 0
    !!! mix_A0(1,1) = sinbe
    !!! mix_A0(1,2) = cosbe
    !!! !!!
    !!! mix_h0 = 0
    !!! mix_h0(1,1) = -sina
    !!! mix_h0(1,2) = cosa
    !!! mix_h0(2,2) = sina
    !!! mix_h0(2,1) = cosa
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    call setup_parameters1
    call setup_parameters2
    call setup_parameters3
    call setup_parameters4
    call setup_parameters5
    call setup_parameters6
    call setup_parameters7
    call setup_parameters8
    call setup_parameters9
contains
 subroutine setup_parameters1 ()
    imago = cmplx (0.0_omega_prec, 1.0_omega_prec, kind=omega_prec)
    ! e = sqrt ((4.0_omega_prec * PI * alpha)) *** This is predefined! ***
    g = (e / sinthw)
    gz = (g / costhw)
    !!! Color flow basis, divide by sqrt(2)
    gs = sqrt(2.0_omega_prec*PI*par%alphas)
    igs = (imago * gs)
    vev = ((2.0_omega_prec * mass(24)) / g)
    q_lep  = (- 1.0_omega_prec) 
    q_up   = (2.0_omega_prec / 3.0_omega_prec)
    q_down = (- 1.0_omega_prec / 3.0_omega_prec)    
    qlep = - e * qelep   !!! This is the negative particle charge !!! 
    qup = - e * qeup     !!! This is the negative particle charge !!! 
    qdwn = - e * qedwn   !!! This is the negative particle charge !!! 
    qchar = ( - e)       !!! This is the negative particle charge !!! 
    gcc = (g / (2.0_omega_prec * sqrt (2.0_omega_prec)))
    gssq = (gs / sqrt (2.0_omega_prec))
    iqw = imago * e
    igzww = imago * g * costhw
    gw4 = (g**2)
    gzzww = ((g**2) * (costhw**2))
    gppww = (e**2)
    gpzww = (e * g * costhw) 
!    sinal = par%mixh0_11
!    cosal = par%mixh0_12
    sinbe = (tanb / sqrt ((1.0_omega_prec + (tanb**2))))
    cosbe = (1.0_omega_prec / sqrt ((1.0_omega_prec + (tanb**2))))
    eidelta = (cosd + (imago * sind))
    cos2be = ((cosbe**2) - (sinbe**2))
    cos2al = ((cosal**2) - (sinal**2))
    sin2be = (2.0_omega_prec * cosbe * sinbe)
    sin2al = (2.0_omega_prec * cosal * sinal)
    sin4al = (2.0_omega_prec * cos2al * sin2al)
    sin4be = (2.0_omega_prec * cos2be * sin2be)
    cos4be = ((cos2be**2) - (sin2be**2))
    cosapb = ((cosal * cosbe) - (sinal * sinbe))
    cosamb = ((cosal * cosbe) + (sinal * sinbe))
    sinapb = ((cosal * sinbe) + (sinal * cosbe))
    sinamb = ((sinal * cosbe) - (sinbe * cosal))
    sin2am2b = (2.0_omega_prec * sinamb * cosamb)
    cos2am2b = ((cosamb**2) - (sinamb**2))
!!!!!!!!!!!!!!!!!!!!!
!!!!!!!Neutralino Mixing
!!!!!!!!!!!!!!!!!!!!!
    mix_neu(1,1) = eta1 * par%mixn_11
    mix_neu(1,2) = eta1 * par%mixn_12
    mix_neu(1,3) = eta1 * par%mixn_13
    mix_neu(1,4) = eta1 * par%mixn_14
    mix_neu(1,5) = eta1 * par%mixn_15
    mix_neu(2,1) = eta2 * par%mixn_21
    mix_neu(2,2) = eta2 * par%mixn_22
    mix_neu(2,3) = eta2 * par%mixn_23
    mix_neu(2,4) = eta2 * par%mixn_24
    mix_neu(2,5) = eta2 * par%mixn_25
    mix_neu(3,1) = eta3 * par%mixn_31
    mix_neu(3,2) = eta3 * par%mixn_32
    mix_neu(3,3) = eta3 * par%mixn_33
    mix_neu(3,4) = eta3 * par%mixn_34
    mix_neu(3,5) = eta3 * par%mixn_35
    mix_neu(4,1) = eta4 * par%mixn_41
    mix_neu(4,2) = eta4 * par%mixn_42
    mix_neu(4,3) = eta4 * par%mixn_43
    mix_neu(4,4) = eta4 * par%mixn_44
    mix_neu(4,5) = eta4 * par%mixn_45
    mix_neu(5,1) = eta5 * par%mixn_51
    mix_neu(5,2) = eta5 * par%mixn_52
    mix_neu(5,3) = eta5 * par%mixn_53
    mix_neu(5,4) = eta5 * par%mixn_54
    mix_neu(5,5) = eta5 * par%mixn_55
    !!! Checked by JR !!! 
    mix_charU(1,1) = par%mu_11                !!! Rotat. matrix containing phi_R
    mix_charU(1,2) = par%mu_12                !!! Rotat. matrix containing phi_R
    mix_charU(2,1) = par%mu_21                !!! Rotat. matrix containing phi_R
    mix_charU(2,2) = par%mu_22                !!! Rotat. matrix containing phi_R
    mix_charV(1,1) = sigch1 * par%mv_11       !!! Rotat. matrix containing phi_L
    mix_charV(1,2) = sigch1 * par%mv_12       !!! Rotat. matrix containing phi_L
    mix_charV(2,1) = sigch2 * par%mv_21       !!! Rotat. matrix containing phi_L
    mix_charV(2,2) = sigch2 * par%mv_22       !!! Rotat. matrix containing phi_L
    al(1) = 0
    au(1) = 0
    ad(1) = 0
    al(2) = 0
    au(2) = 0
    ad(2) = 0
    !!! SLHA has a different sign for the trilinear scalar parameters
    al(3) = par%Ae
    au(3) = par%Au
    ad(3) = par%Ad
!!!NMSSM parameters
    mu = par%nmu
    lambda = par%ls   
    A_lambda = par%a_ls  
    k = par%ks
    A_k = par%a_ks
!!! ensuring that r==0 if lambda=0
    if (lambda == 0) then
    r = 0.0_omega_prec
    else
    r = mu/(vev * lambda)
    end if
!!! SFermion mixing matrices (FB)
    mix_sl = 0.0_omega_prec
    mix_su = 0.0_omega_prec
    mix_sd = 0.0_omega_prec
    
    do i = 1,2
       mix_sl(1:2,i,i) = 1.0_omega_prec
       mix_su(1:2,i,i) = 1.0_omega_prec
       mix_sd(1:2,i,i) = 1.0_omega_prec
    end do
        
    mix_sl(3,1,1) = par%ml_11
    mix_sl(3,1,2) = par%ml_12
    mix_sl(3,2,1) = par%ml_21
    mix_sl(3,2,2) = par%ml_22
    mix_su(3,1,1) = par%mt_11
    mix_su(3,1,2) = par%mt_12
    mix_su(3,2,1) = par%mt_21
    mix_su(3,2,2) = par%mt_22
    mix_sd(3,1,1) = par%mb_11
    mix_sd(3,1,2) = par%mb_12
    mix_sd(3,2,1) = par%mb_21
    mix_sd(3,2,2) = par%mb_22 
 end subroutine setup_parameters1
 subroutine setup_parameters2 ()
!!!!!!!!!!!
!!!Neutral Current to Charged Higgs
!!!!!!!!!!!   
   g_Zhmhp = ((gz / 2.0_omega_prec) * (1.0_omega_prec -  &
        (2.0_omega_prec * sin2thw)))
   g_Ahmhp = e
   
!!!!!!!!!!!!!!!
!!!Charged Current to quarks
!!!!!!!!!!!!!!!!!
   do i = 1,3
      do j = 1,3
         g_ccq(i,j) = (gcc * vckm(i,j))
         g_ccq_c(i,j) = (gcc * conjg (vckm(i,j)))
      end do
   end do
!!!!!!!!!!!!!!!
!!!Charged Current to SQuarks
!!!!!!!!!!!!!!!!!
   do gen1 = 1,3
      do gen2 = 1,3
         do sfm1 = 1,2
            do sfm2 = 1,2
               g_wsusd(gen1,gen2,sfm1,sfm2) = &
                    ( - (gcc * 2.0_omega_prec * vckm(gen1,gen2) * &
                    conjg(mix_su(gen1,sfm1,1)) * mix_sd(gen2,sfm2,1)))
               
               g_wsusd_c(gen1,gen2,sfm1,sfm2) = conjg(g_wsusd(gen1,gen2,sfm1,sfm2))
            end do
         end do
      end do
   end do
 end subroutine setup_parameters2
 subroutine setup_parameters3 ()
!!!!!!!!!!!!!!!
!!!Neutral current to Sneutrinos
!!!!!!!!!!!!!!!!     
   g_zsnsn(1:3,1,1)  = (gz / 2.0_omega_prec)
!!!!!!!!!!!!!!!
!!!Neutral current to SLeptons
!!!!!!!!!!!!!!!!     
   
   do gen = 1,3
      do sfm1 = 1,2
         do sfm2 = 1,2
            g_zslsl(gen,sfm1,sfm2) = &
                 ((gz / 2.0_omega_prec) * ( mod(sfm1 + sfm2 + 1,2) *  &      
                 (2.0_omega_prec) * sin2thw  - (mix_sl(gen,sfm2,1) *  &
                 conjg (mix_sl(gen,sfm1,1)))))
            
            g_zsusu(gen,sfm1,sfm2) = &
                 ((gz / 2.0_omega_prec) * ((mix_su(gen,sfm1,1) * & 
                 conjg (mix_su(gen,sfm2,1))) - mod(sfm1 + sfm2 + 1,2) * &
                 ((4.0_omega_prec / 3.0_omega_prec) * sin2thw) ))
            
            g_zsdsd(gen,sfm1,sfm2) = & 
                 ((gz / 2.0_omega_prec) * (( mod(sfm1 + sfm2 + 1,2) * &
                 (2.0_omega_prec / 3.0_omega_prec) * sin2thw) - & 
                 (mix_sd(gen,sfm2,1) *  conjg (mix_sd(gen,sfm1,1)))))
          end do
       end do
    end do
    
!!!!!!!!!!!!!!!!!!!!!!!!!
!!!W to slepton sneutrino 
!!!!!!!!!!!!!!!!!!!!!!!!
    do gen = 1,3
       do i = 1,2
          g_wslsn(gen,i) = (gcc * 2.0_omega_prec * mix_sl(gen,i,1))
          g_wslsn_c(gen,i) = conjg(g_wslsn(gen,i))
       end do
    end do
    
!!!!!!!!!!!!!!!!!!!!!!
!!!Neutral current to Neutralinos
!!!!!!!!!!!!!!!!!!!!!!
    g_zneuneu=0
    do neu1 = 1,dimNeu
       do neu2 = 1,dimNeu
          g_zneuneu(1,neu1,neu2) = &
               (gz/4.0_omega_prec) * ( mix_neu(neu1,4) * &
               conjg (mix_neu(neu2,4)) - mix_neu(neu1,3) * &
               conjg (mix_neu(neu2,3)))
          g_zneuneu(2,neu1,neu2) = -conjg(g_zneuneu(1,neu1,neu2))
       end do
    end do
!!!!!!!!!!!!!!!!!!!!!!
!!!Neutral current to Charginos
!!!!!!!!!!!!!!!!!!!!!!
 do ch2 = 1,2
    do ch1 = 1,ch2
       g_zchch(1,ch1,ch2) =  &
            (gz*((((1.0_omega_prec - (2.0_omega_prec * sin2thw)) / 4.0_omega_prec) * &
            ((mix_charV(ch1,2) * conjg (mix_charV(ch2,2))) + &
            (conjg (mix_charU(ch1,2)) * mix_charU(ch2,2)))) + &
            (((costhw**2) / 2.0_omega_prec) * ( (mix_charV(ch1,1) * &
            conjg (mix_charV(ch2,1))) + ( conjg (mix_charU(ch1,1)) * &
            mix_charU(ch2,1))))))
       
       g_zchch(2,ch1,ch2) =  &
            (gz*((((1.0_omega_prec - (2.0_omega_prec * sin2thw)) / 4.0_omega_prec) * &
            ((mix_charV(ch1,2) * conjg (mix_charV(ch2,2))) - &
            (conjg (mix_charU(ch1,2)) * mix_charU(ch2,2)))) + &
            (((costhw**2) / 2.0_omega_prec) * ( (mix_charV(ch1,1) * &
            conjg (mix_charV(ch2,1))) - ( conjg (mix_charU(ch1,1)) * &
            mix_charU(ch2,1))))))
    end do
 end do
 g_zchch(1,2,1) = conjg(g_zchch(1,1,2))
 g_zchch(2,2,1) = conjg(g_zchch(2,1,2))
!!!!!!!!!!!!!!!!!!!!!!
!!! Z Z to SFermions 
!!!!!!!!!!!!!!!!!!!!!!
 do gen = 1,3
    do sfm1 = 1,2
       do sfm2 = 1,sfm1
          g_zz_slsl(gen,sfm1,sfm2) = &
               (((gz**2) / 2.0_omega_prec) * ((1.0_omega_prec -  &
               (4.0_omega_prec * sin2thw)) * mix_sl(gen,sfm1,1) * &
               conjg (mix_sl(gen,sfm2,1)) + &
               mod(sfm1+sfm2+1,2) * ((sin2thw**2) * 4.0_omega_prec)))
          g_zz_susu(gen,sfm1,sfm2) = &
               (((gz**2) / 2.0_omega_prec) * ((1.0_omega_prec - (sin2thw *  &
               (8.0_omega_prec / 3.0_omega_prec))) * mix_su(gen,sfm1,1) *  &
               conjg (mix_su(gen,sfm2,1)) + &
               mod(sfm1+sfm2+1,2) * ((sin2thw**2) * (2.0 * q_up)**2)))
          g_zz_sdsd(gen,sfm1,sfm2) = &
               (((gz**2) / 2.0_omega_prec) * ((1.0_omega_prec - (sin2thw *  &
               (4.0_omega_prec / 3.0_omega_prec))) * mix_sd(gen,sfm1,1) *  &
               conjg (mix_sd(gen,sfm2,1)) + &
               mod(sfm1+sfm2+1,2) * ((sin2thw**2) *  (2.0 * q_down)**2)))
       end do
    end do
 end do
 g_zz_slsl(:,1,2) = conjg(g_zz_slsl(:,2,1))
 g_zz_susu(:,1,2) = conjg(g_zz_susu(:,2,1))
 g_zz_sdsd(:,1,2) = conjg(g_zz_sdsd(:,2,1))     
 g_zz_snsn(1:3,1,1) = ((gz**2) / 2.0_omega_prec)
 
!!!!!!!!!!!!!!!
!!! Photon photon SFerm2
!!!!!!!!!!!!!!!
    g_AA_slsl = (2.0_omega_prec * (e**2))
    g_AA_susu = ((8.0_omega_prec / 9.0_omega_prec) * (e**2))
    g_AA_sdsd = ((2.0_omega_prec / 9.0_omega_prec) * (e**2))

!!!!!!!!!!!!!!!
!!!W W Sfermion
!!!!!!!!!!!!!!!
 do gen = 1,3
    do sfm1 = 1,2
       do sfm2 = 1, sfm1
          g_ww_slsl(gen,sfm1,sfm2) = &
               (((g**2) / 2.0_omega_prec) * mix_sl(gen,sfm1,1) * &
               conjg (mix_sl(gen,sfm2,1)))
          g_ww_sdsd(gen,sfm1,sfm2) = &
               (((g**2) / 2.0_omega_prec) * mix_sd(gen,sfm1,1) * &
               conjg (mix_sd(gen,sfm2,1)))
          g_ww_susu(gen,sfm1,sfm2) = &
               (((g**2) / 2.0_omega_prec) * mix_su(gen,sfm1,1) * &
               conjg (mix_su(gen,sfm2,1))) 
       end do
    end do
 end do
 g_ww_slsl(:,1,2) = conjg(g_ww_slsl(:,2,1))
 g_ww_susu(:,1,2) = conjg(g_ww_susu(:,2,1))
 g_ww_sdsd(:,1,2) = conjg(g_ww_sdsd(:,2,1))
 g_ww_snsn(1:3,1,1) = ((g**2) / 2.0_omega_prec) 
!!!!!!!!!!!!!!!
!!!Z Photon Sfermions
!!!!!!!!!!!!!!!
 do gen = 1,3
    do sfm1 = 1,2
       do sfm2 = 1,2
          g_zA_slsl(gen,sfm1,sfm2) = &
               (e * gz * ((mix_sl(gen,sfm1,1) *  conjg (mix_sl(gen,sfm2,1))) - &          
               (mod(sfm1+sfm2+1,2) * sin2thw * 2.0_omega_prec )))       
          g_zA_sdsd(gen,sfm1,sfm2) = &
               (e * gz * (1.0_omega_prec / 3.0_omega_prec) * &
               ((mix_sd(gen,sfm1,1) *  conjg (mix_sd(gen,sfm2,1))) - &          
               (mod(sfm1+sfm2+1,2) * sin2thw * (2.0_omega_prec / 3.0_omega_prec))))
          g_zA_susu(gen,sfm1,sfm2) = &
               (e * gz * (2.0_omega_prec / 3.0_omega_prec) * &
               ((mix_su(gen,sfm1,1) * conjg (mix_su(gen,sfm2,1))) - &
               (mod(sfm1+sfm2+1,2) * sin2thw * (4.0_omega_prec / 3.0_omega_prec))))
       end do
    end do
 end do
!!!!!!!!!!!!!!!
!!!W Photon SLeptons
!!!!!!!!!!!!!!!
 do gen = 1,3  
    do sfm1 = 1,2
       g_wA_slsn(gen,sfm1) = &
            ( - (e * 2.0_omega_prec * gcc * mix_sl(gen,sfm1,1)))          
    end do
 end do
 g_wA_slsn_c = conjg(g_wA_slsn)
!!!!!!!!!!!!!!!
!!!W Z SLeptons
!!!!!!!!!!!!!!!
 do gen = 1,3  
    do sfm1 = 1,2
       g_wz_slsn(gen,sfm1) = &
            gcc * gz * 2.0_omega_prec * sin2thw * mix_sl(gen,sfm1,1)          
    end do
 end do
 g_wz_slsn_c = conjg(g_wz_slsn)
!!!!!!!!!!!!!!!
!!!W Photon SQuarks
!!!!!!!!!!!!!!!
 do gen1 = 1,3
    do gen2 = 1,3
       do sfm1 = 1,2
          do sfm2 = 1,2
             g_wA_susd(gen1,gen2,sfm1,sfm2) = &
                  ((gcc * e * (2.0_omega_prec / 3.0_omega_prec) * vckm(gen1,gen2) *  &
                  conjg (mix_su(gen1,sfm1,1)) * mix_sd(gen2,sfm2,1)))
          end do
       end do
    end do
 end do
 g_wA_susd_c = conjg(g_wA_susd)
!!!!!!!!!!!!!!!
!!!W Z SQuarks
!!!!!!!!!!!!!!!
 do gen1 = 1,3
    do gen2 = 1,3
       do sfm1 = 1,2
          do sfm2 = 1,2
             g_wz_susd(gen1,gen2,sfm1,sfm2) = &
                  ( - (gcc * gz * (2.0_omega_prec / 3.0_omega_prec) * &
                  sin2thw * vckm(gen1,gen2) *  &
                  conjg (mix_su(gen1,sfm1,1)) * mix_sd(gen2,sfm2,1)))
          end do
       end do
    end do
 end do
 g_wz_susd_c = conjg(g_wz_susd)
!!!!!!!!!!!!!!!
!!!Gluon W SQuarks
!!!!!!!!!!!!!!!
 do gen1 = 1,3
    do gen2 = 1,3
       do sfm1 = 1,2
          do sfm2 = 1,2
             g_gw_susd(gen1,gen2,sfm1,sfm2) = &
                  (g * gs * sqrt (2.0_omega_prec) * vckm(gen1,gen2) *  &
                  conjg (mix_su(gen1,sfm1,1)) * mix_sd(gen2,sfm2,1))
          end do
       end do
    end do
 end do
 g_gw_susd_c = conjg(g_gw_susd)
!!!!!!!!!!!!!!!
!!!Gluon Z SQuarks
!!!!!!!!!!!!!!!
 do gen = 1,3
    do sfm1 = 1,2
       do sfm2 = 1,2
          g_gz_susu(gen,sfm1,sfm2) = &
               (gz * gs * (((1.0_omega_prec / 2.0_omega_prec) *  &
               (mix_su(gen,sfm1,1) * conjg (mix_su(gen,sfm2,1)))) - &
               mod(sfm1+sfm2+1,2)* (sin2thw *(q_up))))
          g_gz_sdsd(gen,sfm1,sfm2) = &
               (-(gz * gs * (((1.0_omega_prec / 2.0_omega_prec) *  &
               (mix_sd(gen,sfm1,1) * conjg (mix_sd(gen,sfm2,1)))) + &
               mod(sfm1+sfm2+1,2)* (sin2thw *(q_down)))))
       end do
    end do
 end do
!!!!!!!!!!!!!!!
!!!Glu Photon Squarks
!!!!!!!!!!!!!!!!
    g_gA_sqsq = 2.0_omega_prec * e * gs / 3.0_omega_prec
!!!!!!!!!!!!!!!
!!!Glu Glu Squarks
!!!!!!!!!!!!!!!!
    g_gg_sqsq = (gssq**2)    
!!!!!!!!!!!
!!!W to Chargino-Neuralino
!!!!!!!!!!!!!
 do ch1 = 1,2
    do neu1 = 1,dimNeu
       g_cwn(1,ch1,neu1) = gcc*((conjg (mix_neu(neu1,2)) * &
            mix_charV(ch1,1) * sqrt (2.0)) - & 
            (conjg (mix_neu(neu1,4)) * mix_charV(ch1,2)))  
       g_cwn(2,ch1,neu1) = gcc*((mix_neu(neu1,2) * &
            conjg (mix_charU(ch1,1)) * sqrt (2.0)) + &
            (mix_neu(neu1,3) * conjg (mix_charU(ch1,2))))
       g_nwc(1,ch1,neu1) = conjg(g_cwn(1,ch1,neu1))
       g_nwc(2,ch1,neu1) = conjg(g_cwn(2,ch1,neu1))
    end do
 end do
end subroutine setup_parameters3
subroutine setup_parameters4 ()
!!!!!!!!!!!!
!!!!Scalar Higgs coupling to chiral SQuarks,
!!!!SLeptons, and SNeutrinos a la Franke Fraas
!!!!!!!!!!!!
!!!!!!!second sfermion is cc in fr
!!!FB check 07/24/09
 do shiggs = 1,dimh0
    do gen = 1,3
       g_h0_suLsuL(shiggs, gen) = (-g * (mass(2*gen) **2) / ( mass(24) * &
            sinbe ) * mix_h0(shiggs,2) + (g / 2.0_omega_prec ) * ( &
            mass(23) / costhw) * (1.0_omega_prec - 2.0_omega_prec * &
            q_up * (sinthw ** 2)) * (mix_h0(shiggs,2) * sinbe - &
            mix_h0(shiggs,1) * cosbe))
       g_h0_suRsuR(shiggs, gen) = (-g * (mass(2*gen) **2) / ( mass(24) &
            *sinbe ) * mix_h0(shiggs,2) + g * mass(24) * q_up * &
            ((sinthw / costhw)** 2) * (mix_h0(shiggs,2) * sinbe - &
            mix_h0(shiggs,1) * cosbe))
       g_h0_suLsuR(shiggs, gen) = - (g * mass(2*gen)  / (2.0_omega_prec * &
            mass(24) * sinbe )) &
            * (- lambda * (vev / sqrt (2.0_omega_prec) )* cosbe * &
            mix_h0(shiggs,3)  - mu * mix_h0(shiggs,1) + au(gen) * &
            mix_h0(shiggs,2))
       g_h0_suRsuL(shiggs, gen) = conjg (g_h0_suLsuR(shiggs, gen))

       g_h0_suLsuL(shiggs, gen) = (-g * (mass(2*gen) **2) / ( mass(24) * &
            sinbe ) * mix_h0(shiggs,2) + (g / 2.0_omega_prec ) * ( &
            mass(23) / costhw) * (1.0_omega_prec - 2.0_omega_prec * &
            q_up * (sinthw ** 2)) * (mix_h0(shiggs,2) * sinbe - &
            mix_h0(shiggs,1) * cosbe))

       g_h0_sdLsdL(shiggs, gen) = (-g * (mass(2*gen-1) **2) / ( mass(24) &
            * cosbe ) * mix_h0(shiggs,1) - (g / 2.0_omega_prec ) * ( &
            mass(23) / costhw) * (1.0_omega_prec + 2.0_omega_prec * &
            q_down * (sinthw ** 2)) * (mix_h0(shiggs,2) * sinbe - &
            mix_h0(shiggs,1) * cosbe) )
       g_h0_sdRsdR(shiggs, gen) = (-g * (mass(2*gen-1) **2) / ( mass(24) &
            * cosbe ) * mix_h0(shiggs,1) + g * mass(24) * q_down * &
            ((sinthw / costhw)** 2) * (mix_h0(shiggs,2) * sinbe - &
            mix_h0(shiggs,1) * cosbe))

       g_h0_sdLsdR(shiggs, gen) =  - (g * mass(2*gen-1) / (2.0_omega_prec &
            * mass(24) * cosbe )) * &
            (- lambda * (vev/sqrt(2.0_omega_prec))*  sinbe * &
             mix_h0(shiggs,3) - mu * mix_h0(shiggs,2) + ad(gen) * &
             mix_h0(shiggs,1) )
       g_h0_sdRsdL(shiggs, gen) = conjg (g_h0_sdLsdR(shiggs, gen))

       g_h0_slLslL(shiggs, gen) = (-g * (mass(2*gen+9) **2) / ( mass(24) &
            * cosbe ) * mix_h0(shiggs,1) - (g / 2.0_omega_prec ) * ( &
            mass(23) / costhw) * (1.0_omega_prec + 2.0_omega_prec * &
            q_lep * (sinthw ** 2)) * (mix_h0(shiggs,2) * sinbe - &
            mix_h0(shiggs,1) * cosbe))
       g_h0_slRslR(shiggs, gen) = (-g * (mass(2*gen+9) **2) / ( mass(24) &
            * cosbe ) * mix_h0(shiggs,1) + g * mass(24) * q_lep * &
            ((sinthw / costhw)** 2) * (mix_h0(shiggs,2) * sinbe - &
            mix_h0(shiggs,1) * cosbe))
       g_h0_slLslR(shiggs, gen) = (-g * mass(2*gen+9) / (2.0_omega_prec &
            * mass(24) * cosbe ) * ( - lambda * ( vev/sqrt(2.0)) * sinbe * &
             mix_h0(shiggs,3) - mu * mix_h0(shiggs,2) + al(gen) * &
             mix_h0(shiggs,1) ))
       g_h0_slRslL(shiggs, gen) = conjg (g_h0_slLslR(shiggs, gen))

    end do
 end do

 do shiggs = 1,3
    g_h0_snLsnL(shiggs,:) = &
         (gz * mass(23) * (1.0_omega_prec / 2.0_omega_prec) * &
         (mix_h0(shiggs,2) * sinbe - mix_h0(shiggs,1) * cosbe ))
 end do

!!!!!!!!!!!!
!!!!Axial Higgs coupling to chiral SQuarks a la Franke Fraas
!!!!!!!!!!!!
!!!FB check 07/24/09
 do phiggs = 1,dimA0
    do gen = 1,3
       g_A0_suLsuR(phiggs, gen) = (- imago * g * mass(2*gen)  / &
            (2.0_omega_prec * mass(24) * sinbe )) * &
            ( lambda * ( vev / sqrt (2.0_omega_prec)) * &
            cosbe * mix_A0(phiggs,3) + mu * mix_A0(phiggs,1) + &
            au(gen) * mix_A0(phiggs,2) )
       g_A0_suRsuL(phiggs, gen) = conjg (g_A0_suLsuR(phiggs, gen))

       g_A0_sdLsdR(phiggs, gen) = (- imago * g * mass(2*gen-1)) / &
            (2.0_omega_prec * mass(24) * cosbe ) * ( lambda *  (vev/sqrt(2.0)) * &
            sinbe * mix_A0(phiggs,3) + mu * mix_A0(phiggs,2) + ad(gen) &
            * mix_A0(phiggs,1) )
       g_A0_sdRsdL(phiggs, gen) = conjg (g_A0_sdLsdR(phiggs, gen))

       g_A0_slLslR(phiggs, gen) = (- imago * g * mass(2*gen+9)) / &
            (2.0_omega_prec * mass(24) * cosbe ) * ( lambda * ( vev/sqrt(2.0)) * &
            sinbe * mix_A0(phiggs,3) + mu * mix_A0(phiggs,2) + &
            al(gen) * mix_A0(phiggs,1) )
       g_A0_slRslL(phiggs, gen) = conjg (g_A0_slLslR(phiggs, gen))

    end do
 end do
!!!!!!!!!!!!!!!!!!
!!!!Charged Higgs SLepton Sneutrino (L/R)
!!!!!!!!!!!!!!!!!!
 do gen = 1,3 
   g_hp_slLsnL(gen) = &
      ((g / (sqrt (2.0_omega_prec) * mass(24))) * (( &
      (mass(9 + 2*gen)**2) * tanb) - ((mass(24)**2) * sin2be)))
   g_hp_slRsnL(gen) = &
      (sqrt (2.0_omega_prec) * ((g * mass(9 + 2*gen) * & 
      ((conjg (al(gen)) * sinbe) + (mu * cosbe))) /  &
      (2.0_omega_prec * mass(24) * cosbe)))
 end do

 
end subroutine setup_parameters4
subroutine setup_parameters5 ()
!!!!!!!!!!!!!!!!!!!
!!!Charged Higgs to squarks (gauge) (gensu,gensd)
!!!!!!!!!!!!!!!!!!!   
  do gen1 = 1,3
     do gen2 = 1,3
        g_hp_suLsdL(gen1,gen2) = vckm(gen1,gen2) * &
            (g**2 * sinbe * cosbe * vev / sqrt(2.0_omega_prec)) &
           *( - 1.0_omega_prec  &
              + mass(2*gen2-1)**2 / (2.0_omega_prec*mass(24)**2 *cosbe**2) &
              + mass( 2*gen1 )**2 / (2.0_omega_prec*mass(24)**2 *sinbe**2) &
            )
        g_hp_suRsdR(gen1,gen2) = vckm(gen1,gen2) * &
            (g**2*vev*mass(2*gen2-1)*mass(2*gen1)/(mass(24)**2*sqrt(8.0))) &
           *(sinbe/cosbe + cosbe/sinbe)
        g_hp_suLsdR(gen1,gen2) = vckm(gen1,gen2) * &
            (g * mass(2*gen2-1) / (sqrt(2.0_omega_prec) * mass(24) )) &
           *(ad(gen2) * sinbe / cosbe + mu )
        g_hp_suRsdL(gen1,gen2) = vckm(gen1,gen2) * &
            (g * mass(2*gen1) / (sqrt(2.0_omega_prec) * mass(24) )) &
           *(au(gen1) * cosbe / sinbe + mu)
     end do
  end do

end subroutine setup_parameters5
!!!!!!!!!!!!
!!!gauge => masseigenstates
!!!!!!!!!!!!
 subroutine setup_parameters6 ()
!!!!!!!!!!!!
!!!Sfermions to Scalar Higgs 
!!!!!!!!!!!!
   do shiggs = 1,dimh0
      do gen = 1,3
!!!SUp
         g_h0_su1su1(shiggs,gen) = (conjg (mix_su(gen,1,1)) * &
              mix_su(gen,1,1) * g_h0_suLsuL(shiggs,gen) + &
              conjg(mix_su(gen,1,2)) * mix_su(gen,1,2) * &
              g_h0_suRsuR(shiggs,gen) + conjg (mix_su(gen,1,1)) * &
              mix_su(gen,1,2) * g_h0_suLsuR(shiggs,gen) + &
              conjg(mix_su(gen,1,2)) * mix_su(gen,1,1) * &
              g_h0_suRsuL(shiggs,gen))

         g_h0_su2su2(shiggs,gen) = (conjg (mix_su(gen,2,1)) * &
              mix_su(gen,2,1) * g_h0_suLsuL(shiggs,gen) + &
              conjg(mix_su(gen,2,2)) * mix_su(gen,2,2) * &
              g_h0_suRsuR(shiggs,gen) + conjg (mix_su(gen,2,1)) * &
              mix_su(gen,2,2) * g_h0_suLsuR(shiggs,gen) + &
              conjg(mix_su(gen,2,2)) * mix_su(gen,2,1) * &
              g_h0_suRsuL(shiggs,gen))

         g_h0_su1su2(shiggs,gen) = (conjg (mix_su(gen,1,1)) * &
              mix_su(gen,2,1) * g_h0_suLsuL(shiggs,gen) + &
              conjg(mix_su(gen,1,2)) * mix_su(gen,2,2) * &
              g_h0_suRsuR(shiggs,gen) + conjg (mix_su(gen,1,1)) * &
              mix_su(gen,2,2) * g_h0_suLsuR(shiggs,gen) + &
              conjg(mix_su(gen,1,2)) * mix_su(gen,2,1) * &
              g_h0_suRsuL(shiggs,gen))
         
         g_h0_su2su1(shiggs,gen) = conjg (g_h0_su1su2(shiggs,gen)) 
!!!SDown
         g_h0_sd1sd1(shiggs,gen) = (conjg(mix_sd(gen,1,1)) * &
              mix_sd(gen,1,1) * g_h0_sdLsdL(shiggs,gen) + &
              conjg(mix_sd(gen,1,2)) * mix_sd(gen,1,2) * &
              g_h0_sdRsdR(shiggs,gen) + conjg(mix_sd(gen,1,1)) * &
              mix_sd(gen,1,2) * g_h0_sdLsdR(shiggs,gen) + &
              conjg(mix_sd(gen,1,2)) * mix_sd(gen,1,1) * &
              g_h0_sdRsdL(shiggs,gen))

         g_h0_sd2sd2(shiggs,gen) = (conjg(mix_sd(gen,2,1)) * &
              mix_sd(gen,2,1) * g_h0_sdLsdL(shiggs,gen) + &
              conjg(mix_sd(gen,2,2)) * mix_sd(gen,2,2) * &
              g_h0_sdRsdR(shiggs,gen) + conjg (mix_sd(gen,2,1)) * &
              mix_sd(gen,2,2) * g_h0_sdLsdR(shiggs,gen) + &
              conjg(mix_sd(gen,2,2)) * mix_sd(gen,2,1) * &
              g_h0_sdRsdL(shiggs,gen))

         g_h0_sd1sd2(shiggs,gen) = (conjg(mix_sd(gen,1,1)) * &
              mix_sd(gen,2,1) * g_h0_sdLsdL(shiggs,gen) + &
              conjg(mix_sd(gen,1,2)) * mix_sd(gen,2,2) * &
              g_h0_sdRsdR(shiggs,gen) + conjg(mix_sd(gen,1,1)) *&
              mix_sd(gen,2,2) * g_h0_sdLsdR(shiggs,gen) + &
              conjg(mix_sd(gen,1,2)) * mix_sd(gen,2,1) * &
              g_h0_sdRsdL(shiggs,gen))

         g_h0_sd2sd1(shiggs,gen) = conjg(g_h0_sd1sd2(shiggs,gen)) 
!!!SLep
        g_h0_sl1sl1(shiggs,gen) = (conjg (mix_sl(gen,1,1)) * &
             mix_sl(gen,1,1) * g_h0_slLslL(shiggs,gen) + &
             conjg(mix_sl(gen,1,2)) * mix_sl(gen,1,2) * &
             g_h0_slRslR(shiggs,gen) + conjg (mix_sl(gen,1,1)) * &
             mix_sl(gen,1,2) * g_h0_slLslR(shiggs,gen) + &
             conjg(mix_sl(gen,1,2)) * mix_sl(gen,1,1) * &
             g_h0_slRslL(shiggs,gen))
	
        g_h0_sl2sl2(shiggs,gen) = (conjg (mix_sl(gen,2,1)) * &
             mix_sl(gen,2,1) * g_h0_slLslL(shiggs,gen) + &
             conjg(mix_sl(gen,2,2)) * mix_sl(gen,2,2) * &
             g_h0_slRslR(shiggs,gen) + conjg (mix_sl(gen,2,1)) * &
             mix_sl(gen,2,2) * g_h0_slLslR(shiggs,gen) + &
             conjg(mix_sl(gen,2,2)) * mix_sl(gen,2,1) * &
             g_h0_slRslL(shiggs,gen))
	
	g_h0_sl1sl2(shiggs,gen) = (conjg (mix_sl(gen,1,1)) * &
             mix_sl(gen,2,1) * g_h0_slLslL(shiggs,gen) + &
             conjg(mix_sl(gen,1,2)) * mix_sl(gen,2,2) * &
             g_h0_slRslR(shiggs,gen) + conjg (mix_sl(gen,1,1)) * &
             mix_sl(gen,2,2) * g_h0_slLslR(shiggs,gen) + &
             conjg(mix_sl(gen,1,2)) * mix_sl(gen,2,1) * &
             g_h0_slRslL(shiggs,gen))

        g_h0_sl2sl1(shiggs,gen) = conjg (g_h0_sl1sl2(shiggs,gen)) 
!!!SNeutrino
        g_h0_sn1sn1(shiggs,gen) =  g_h0_snLsnL(shiggs,gen)       
        
     end do
  end do
  
!!!!!!!!!!!!
!!!Sfermions to Axial Higgs 
!!!!!!!!!!!! 
  do phiggs = 1,dimA0
     do gen = 1,3  
!!!SUp
        g_A0_su1su1(phiggs, gen) = &
             (conjg (mix_su(gen,1,2)) * mix_su(gen,1,1) * g_A0_suLsuR(phiggs, gen) + &
             conjg (mix_su(gen,1,1)) * mix_su(gen,1,2) * g_A0_suRsuL(phiggs, gen))
        
        g_A0_su2su2(phiggs, gen) = &
             (conjg (mix_su(gen,2,2)) * mix_su(gen,2,1) * g_A0_suLsuR(phiggs, gen) + &
             conjg (mix_su(gen,2,1)) * mix_su(gen,2,2) * g_A0_suRsuL(phiggs, gen))
        
        g_A0_su1su2(phiggs, gen) = &
             (conjg (mix_su(gen,1,1)) * mix_su(gen,2,2) * g_A0_suLsuR(phiggs, gen) + &
             conjg (mix_su(gen,2,1)) * mix_su(gen,1,2) * g_A0_suRsuL(phiggs, gen))
        
        g_A0_su2su1(phiggs, gen) = conjg (g_A0_su1su2(phiggs, gen)) 
!!!SDown
        g_A0_sd1sd1(phiggs, gen) = &
             (conjg (mix_sd(gen,1,1)) * mix_sd(gen,2,1) * g_A0_sdLsdR(phiggs, gen) + &
             conjg (mix_sd(gen,2,1)) * mix_sd(gen,1,1) * g_A0_sdRsdL(phiggs, gen))
        
        g_A0_sd2sd2(phiggs, gen) = &
             (conjg (mix_sd(gen,1,2)) * mix_sd(gen,2,2) * g_A0_sdLsdR(phiggs, gen) + &
             conjg (mix_sd(gen,2,2)) * mix_sd(gen,1,2) * g_A0_sdRsdL(phiggs, gen))
        
        g_A0_sd1sd2(phiggs, gen) = &
             (conjg (mix_sd(gen,1,1)) * mix_sd(gen,2,2) * g_A0_sdLsdR(phiggs, gen) + &
             conjg (mix_sd(gen,2,1)) * mix_sd(gen,1,2) * g_A0_sdRsdL(phiggs, gen))
        
        g_A0_sd2sd1(phiggs, gen) = conjg (g_A0_sd1sd2(phiggs, gen)) 
!!!SLep
        g_A0_sl1sl1(phiggs, gen) = &
             (conjg (mix_sl(gen,1,1)) * mix_sl(gen,2,1) * g_A0_slLslR(phiggs, gen) + &
             conjg (mix_sl(gen,2,1)) * mix_sl(gen,1,1) * g_A0_slRslL(phiggs, gen))
        
        g_A0_sl2sl2(phiggs, gen) = &
             (conjg (mix_sl(gen,1,2)) * mix_sl(gen,2,2) * g_A0_slLslR(phiggs, gen) + &
             conjg (mix_sl(gen,2,2)) * mix_sl(gen,1,2) * g_A0_slRslL(phiggs, gen))

        g_A0_sl1sl2(phiggs, gen) = &
             (conjg (mix_sl(gen,1,1)) * mix_sl(gen,2,2) * g_A0_slLslR(phiggs, gen) + &
             conjg (mix_sl(gen,2,1)) * mix_sl(gen,1,2) * g_A0_slRslL(phiggs, gen))
        
        g_A0_sl2sl1(phiggs, gen) = conjg (g_A0_sl1sl2(phiggs, gen)) 
        
     end do
  end do
!!!!!!!!!!!!!!!!!
!!!Charged Higgs to SQuarks
!!!!!!!!!!!!!!!!!
  do gen1 = 1,3
     do gen2 = 1,3
        g_hp_su1sd1(gen1,gen2) = ((conjg (mix_su(gen1,1,1)) * &
             mix_sd(gen2,1,1) * g_hp_suLsdL(gen1,gen2)) + &
             ( conjg(mix_su(gen1,1,2)) * mix_sd(gen2,1,2) * &
             g_hp_suRsdR(gen1,gen2)) + ( conjg (mix_su(gen1,1,1)) * &
             mix_sd(gen2,1,2) * g_hp_suLsdR(gen1,gen2)) + &
             ( conjg(mix_su(gen1,1,2)) * mix_sd(gen2,1,1) *&
             g_hp_suRsdL(gen1,gen2)))
        g_hp_su1sd2(gen1,gen2) = ((conjg (mix_su(gen1,1,1)) * &
             mix_sd(gen2,2,1) * g_hp_suLsdL(gen1,gen2)) + &
             ( conjg(mix_su(gen1,1,2)) * mix_sd(gen2,2,2) *&
             g_hp_suRsdR(gen1,gen2)) + ( conjg (mix_su(gen1,1,1)) * &
             mix_sd(gen2,2,2) * g_hp_suLsdR(gen1,gen2)) + &
             ( conjg(mix_su(gen1,1,2)) * mix_sd(gen2,2,1) * &
             g_hp_suRsdL(gen1,gen2)))
        g_hp_su2sd1(gen1,gen2) = ((conjg (mix_su(gen1,2,1)) * &
             mix_sd(gen2,1,1) * g_hp_suLsdL(gen1,gen2)) + &
             ( conjg(mix_su(gen1,2,2)) * mix_sd(gen2,1,2) * &
             g_hp_suRsdR(gen1,gen2)) + ( conjg (mix_su(gen1,2,1)) * &
             mix_sd(gen2,1,2) * g_hp_suLsdR(gen1,gen2)) + &
             ( conjg(mix_su(gen1,2,2)) * mix_sd(gen2,1,1) * &
             g_hp_suRsdL(gen1,gen2)))
        g_hp_su2sd2(gen1,gen2) = ((conjg (mix_su(gen1,2,1)) * &
             mix_sd(gen2,2,1) * g_hp_suLsdL(gen1,gen2)) + &
             ( conjg(mix_su(gen1,2,2)) * mix_sd(gen2,2,2) * &
             g_hp_suRsdR(gen1,gen2)) + ( conjg (mix_su(gen1,2,1)) * &
             mix_sd(gen2,2,2) * g_hp_suLsdR(gen1,gen2)) + &
             ( conjg(mix_su(gen1,2,2)) * mix_sd(gen2,2,1) * &
             g_hp_suRsdL(gen1,gen2)))
     end do
  end do
  g_hp_su1sd1_c = conjg(g_hp_su1sd1)
  g_hp_su1sd2_c = conjg(g_hp_su1sd2)
  g_hp_su2sd1_c = conjg(g_hp_su2sd1)
  g_hp_su2sd2_c = conjg(g_hp_su2sd2)
  
!!!!!!!!!!!!!!!!!!!!!!!
!!!Scalar Higgs to fermions
!!!!!!!!!!!!!!!!!!!!!!!
  
  do shiggs = 1,dimh0
     do gen = 1,3
        g_yuk_h0_uu(shiggs, gen) = (-g * mass(2 * gen) / &
             ( 2.0_omega_prec * mass(24) * sinbe ) * mix_h0(shiggs,2)) 
        
        g_yuk_h0_ll(shiggs, gen) = (-g * mass(9 + 2*gen) / &
             ( 2.0_omega_prec * mass(24) * cosbe ) * mix_h0(shiggs,1)) 
        
        g_yuk_h0_dd(shiggs, gen) = (-g * mass(2*gen -1) / &
             ( 2.0_omega_prec * mass(24) * cosbe ) * mix_h0(shiggs,1))    
     end do
  end do
!!!!!!!!!!!!!!!!!!!!!!!
!!!Axial Higgs to fermions
!!!!!!!!!!!!!!!!!!!!!!!
  do phiggs = 1,dimA0
     do gen = 1,3
        g_yuk_A0_uu(phiggs, gen) = ( imago * g * mass(2 * gen) / &
             ( 2.0_omega_prec * mass(24) * sinbe ) * mix_A0(phiggs,2)) 
        
        g_yuk_A0_ll(phiggs, gen) = ( imago * g * mass(9 + 2*gen) / &
             ( 2.0_omega_prec * mass(24) * cosbe ) * mix_A0(phiggs,1) )
        
        g_yuk_A0_dd(phiggs, gen) = ( imago * g * mass(2*gen -1) / &
             ( 2.0_omega_prec * mass(24) * cosbe ) * mix_A0(phiggs,1) )  
     end do
  end do
  
!!!!!!!!!!!!!!!!!!!!!!!!
!!!!Charged Higgs Slepton Sneutrino (mass basis)
!!!!!!!!!!!!!!!!!!!!!!!!
  g_hp_sl1sn1(1:3) = ((conjg (mix_sl(3,1,1)) * g_hp_slLsnL(1:3)) + ( &
       conjg (mix_sl(3,1,2)) * g_hp_slRsnL(1:3)))
  g_hp_sl2sn1(1:3) = ((conjg (mix_sl(3,2,1)) * g_hp_slLsnL(1:3)) + ( &
       conjg (mix_sl(3,2,2)) * g_hp_slRsnL(1:3)))
  g_hp_sl1sn1_c = conjg(g_hp_sl1sn1)
  g_hp_sl2sn1_c = conjg(g_hp_sl2sn1)
  

!!!!!!!!!!!!!!!!!!!!!!!!
!!! Z-shiggs-phiggs
!!!!!!!!!!!!!!!!!!!!!!!!

  do shiggs = 1,dimh0
     do phiggs =1,dimA0
        g_Zh0A0(shiggs,phiggs) = ( - imago * gz / (2.0_omega_prec ) * &
             ( mix_h0(shiggs,1) * mix_A0(phiggs,1) - &
             mix_h0(shiggs,2) * mix_A0(phiggs,2) )  ) 
     end do
  end do
  

!!!!!!!!!!!!!!!!!!!!!!!!
!!! W-h+-phiggs
!!!!!!!!!!!!!!!!!!!!!!!!
  do phiggs =1,dimA0   
     g_WhpA0(phiggs) = ( -imago * g / (2.0_omega_prec ) * &
          (sinbe * mix_A0(phiggs,1)  + cosbe * mix_A0(phiggs,2) ) )
  end do
  
!!!!!!!!!!!!!!!!!!!!!!!!
!!! Z-Z-shiggs, W-W-shiggs, W-h+-shiggs
!!!!!!!!!!!!!!!!!!!!!!!!
  do shiggs = 1,dimh0   
     g_ZZh0(shiggs) = (  g * mass(23) / costhw * &
        (cosbe * mix_h0(shiggs,1)  + sinbe * mix_h0(shiggs,2) ) )
     g_WWh0(shiggs) = (  g * mass(24)  * &
        (cosbe * mix_h0(shiggs,1)  + sinbe * mix_h0(shiggs,2) ) )
     g_Whph0(shiggs) = ( - g / (2.0_omega_prec ) * &
        (sinbe * mix_h0(shiggs,1)  - cosbe * mix_h0(shiggs,2) )) 
  end do
  
!!!!!!!!!!!!!!!!!!!
!!!WW-h0h0, ZZ-h0h0, ZW-hp-h0, AW-hp-h0
!!!!!!!!!!!!!!!!!!!!
  do shiggs1 = 1,dimh0
     do shiggs2 = 1,dimh0
        g_WWh0h0(shiggs1,shiggs2) = ( g**2 / (2.0_omega_prec) *(mix_h0(shiggs1,1) * & 
             mix_h0(shiggs2,1) + mix_h0(shiggs1,2) * mix_h0(shiggs2,2))) 
        g_ZZh0h0(shiggs1,shiggs2) = ( g**2 / (2.0_omega_prec * costhw**2) *(mix_h0(shiggs1,1) &
             * mix_h0(shiggs2,1) + mix_h0(shiggs1,2) * mix_h0(shiggs2,2))) 
     end do
     g_ZWhph0(shiggs1) = ( g**2 * sinthw**2 / (2.0_omega_prec * costhw) *(mix_h0(shiggs1,1) &
          * sinbe - mix_h0(shiggs1,2) * cosbe))
     g_AWhph0(shiggs1) = ( - e * g  / (2.0_omega_prec) *(mix_h0(shiggs1,1) * sinbe - & 
          mix_h0(shiggs1,2) * cosbe ))
  end do
!!!!!!!!!!!!!!!!!!!!!!!!
!!!WW-A0A0, ZZ-A0A0, ZW-hp-A0, AW-hp-A0
!!!!!!!!!!!!!!!!!!!!!!!!
  do phiggs1 = 1,dimA0
     do phiggs2 = 1,dimA0
        g_WWA0A0(phiggs1,phiggs2) = ( g**2 / (2.0_omega_prec) *(mix_A0(phiggs1,1) * &
             mix_A0(phiggs2,1) + mix_A0(phiggs1,2) * mix_A0(phiggs2,2))) 
        g_ZZA0A0(phiggs1,phiggs2) = ( g**2 / (2.0_omega_prec * costhw**2) *(mix_A0(phiggs1,1) &
             * mix_A0(phiggs2,1) + mix_A0(phiggs1,2) * mix_A0(phiggs2,2))) 
     end do
     g_ZWhpA0(phiggs1) = (imago * g**2 * sinthw**2 / (2.0_omega_prec * costhw) * & 
          (mix_A0(phiggs1,1) * sinbe + mix_A0(phiggs1,2) * cosbe))
     g_AWhpA0(phiggs1) = ( - imago * e * g  / (2.0_omega_prec) *(mix_A0(phiggs1,1) * &
          sinbe + mix_A0(phiggs1,2) * cosbe))
  end do
!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!4-vertex: NEutral Gauge bosons to charged Higgses
!!!!!!!!!!!!!!!!!!!!!!!!!!  
    g_ZZhphm = (((gz**2) / 2.0_omega_prec) * &
            (((2.0_omega_prec * (costhw**2)) - 1.0_omega_prec)**2))
    g_AAhphm = (2.0_omega_prec * (e**2))
    g_ZAhphm = (e * gz * ((2.0_omega_prec * (costhw**2)) - &    
            1.0_omega_prec))
    g_WWhphm = ((g**2) / 2.0_omega_prec)
!!!!!!!!!!!!!!!!!!!!!!!!
!!!h0h0h0
!!!!!!!!!!!!!!!!!!!!!!!!
 do shiggs1 = 1,dimh0
    do shiggs2 = 1,dimh0
       do shiggs3 = 1,dimh0
          !!! checked by fb with FeynRules 8/31/09
          g_h0h0h0(shiggs1,shiggs2,shiggs3) = (&
                -(3.0_omega_prec / 4.0_omega_prec) * (gz**2) *&
                ( cosbe * vev * mix_h0(shiggs1,1) * mix_h0(shiggs2,1) * &
                mix_h0(shiggs3,1) + sinbe * vev * mix_h0(shiggs1,2) * &
                mix_h0(shiggs2,2) * mix_h0(shiggs3,2))  + &
                 (gz**2 / (4.0_omega_prec ) - ( lambda**2)) *&
               (  cosbe * vev * &
               (  mix_h0(shiggs1,1) * mix_h0(shiggs2,2) * mix_h0(shiggs3,2)&
               +  mix_h0(shiggs1,2) * mix_h0(shiggs2,1) * mix_h0(shiggs3,2)&
               +  mix_h0(shiggs1,2) * mix_h0(shiggs2,2) * mix_h0(shiggs3,1) )&
                + sinbe * vev * &
               (  mix_h0(shiggs1,1) * mix_h0(shiggs2,1) * mix_h0(shiggs3,2)&
               +  mix_h0(shiggs1,1) * mix_h0(shiggs2,2) * mix_h0(shiggs3,1)&
               +  mix_h0(shiggs1,2) * mix_h0(shiggs2,1) * mix_h0(shiggs3,1) ) )&
                + vev * ( k * lambda * sinbe - lambda**2 * cosbe) *&
               (  mix_h0(shiggs1,1) * mix_h0(shiggs2,3) * mix_h0(shiggs3,3)&
               +  mix_h0(shiggs1,3) * mix_h0(shiggs2,1) * mix_h0(shiggs3,3)&
               +  mix_h0(shiggs1,3) * mix_h0(shiggs2,3) * mix_h0(shiggs3,1)  )&
                + vev * ( k * lambda * cosbe - lambda**2 * sinbe) *&
               (  mix_h0(shiggs1,2) * mix_h0(shiggs2,3) * mix_h0(shiggs3,3)&
               +  mix_h0(shiggs1,3) * mix_h0(shiggs2,2) * mix_h0(shiggs3,3)&
               +  mix_h0(shiggs1,3) * mix_h0(shiggs2,3) * mix_h0(shiggs3,2)  )&
                - (lambda * mu * sqrt(2.0_omega_prec) )* &
               (  mix_h0(shiggs1,1) * mix_h0(shiggs2,1) * mix_h0(shiggs3,3)&
               +  mix_h0(shiggs1,1) * mix_h0(shiggs2,3) * mix_h0(shiggs3,1)&
               +  mix_h0(shiggs1,3) * mix_h0(shiggs2,1) * mix_h0(shiggs3,1)&
               +  mix_h0(shiggs1,2) * mix_h0(shiggs2,2) * mix_h0(shiggs3,3)&
               +  mix_h0(shiggs1,2) * mix_h0(shiggs2,3) * mix_h0(shiggs3,2)&
               +  mix_h0(shiggs1,3) * mix_h0(shiggs2,2) * mix_h0(shiggs3,2)  )&
               + ( lambda * A_lambda / sqrt (2.0_omega_prec) &
                + sqrt(2.0_omega_prec) * mu * k ) * &
               (  mix_h0(shiggs1,1) * mix_h0(shiggs2,2) * mix_h0(shiggs3,3)&
               +  mix_h0(shiggs1,1) * mix_h0(shiggs2,3) * mix_h0(shiggs3,2)&
               +  mix_h0(shiggs1,3) * mix_h0(shiggs2,1) * mix_h0(shiggs3,2)&
               +  mix_h0(shiggs1,2) * mix_h0(shiggs2,1) * mix_h0(shiggs3,3)&
               +  mix_h0(shiggs1,2) * mix_h0(shiggs2,3) * mix_h0(shiggs3,1)&
               +  mix_h0(shiggs1,3) * mix_h0(shiggs2,2) * mix_h0(shiggs3,1)  )&
                - ( k * A_k  * sqrt (2.0_omega_prec)  + &
                 6.0_omega_prec * sqrt (2.0_omega_prec) * k**2 * mu / lambda ) * &
                 mix_h0(shiggs1,3) * mix_h0(shiggs2,3) * mix_h0(shiggs3,3)  )
       end do
    end do
 end do
!!!!!!!!!!!!!!!!!!!!!!!!
!!!h0A0A0
!!!!!!!!!!!!!!!!!!!!!!!!
  do shiggs1 = 1,dimh0
     do phiggs1 = 1,dimA0
        do phiggs2 = 1,dimA0
           g_h0A0A0(shiggs1,phiggs1,phiggs2) = &
                 (-1.0_omega_prec / 4.0_omega_prec) * gz**2 * vev  *&
                 ( cosbe * mix_h0(shiggs1,1) * mix_A0(phiggs1,1) * mix_A0(phiggs2,1)&
                 + sinbe * mix_h0(shiggs1,2) * mix_A0(phiggs1,2) * mix_A0(phiggs2,2)  )&
                !!!                                                                                                             
                 + (gz**2 / 4.0_omega_prec - lambda**2 ) * vev * &
                 ( cosbe * mix_h0(shiggs1,1) * mix_A0(phiggs1,2) * mix_A0(phiggs2,2)&
                 + sinbe * mix_h0(shiggs1,2) * mix_A0(phiggs1,1) * mix_A0(phiggs2,1)  )&
                !!!                                                                                                             
                 - (  k * lambda * cosbe + lambda**2 * sinbe) * vev * &
                  mix_h0(shiggs1,2) * mix_A0(phiggs1,3) * mix_A0(phiggs2,3)&
                 - (  k * lambda * sinbe + lambda**2 * cosbe) * vev * &
                  mix_h0(shiggs1,1) * mix_A0(phiggs1,3) * mix_A0(phiggs2,3)&
                !!!                                                                                                             
                 - lambda * mu * sqrt (2.0_omega_prec) *  mix_h0(shiggs1,3) * &
                          ( mix_A0(phiggs1,1) * mix_A0(phiggs2,1)  &
                                      + mix_A0(phiggs1,2) * mix_A0(phiggs2,2)  ) &
                !!!                                                                                                             
                 - sqrt(2.0_omega_prec)* ( 2.0_omega_prec * k**2 * mu / lambda - k * A_k )&
                 * mix_h0(shiggs1,3) * mix_A0(phiggs1,3) * mix_A0(phiggs2,3)  &
                !!!                                                                                                             
                 +  lambda * k  * vev *  mix_h0(shiggs1,3) * &
                  ( cosbe * ( mix_A0(phiggs1,2) * mix_A0(phiggs2,3) &
                            + mix_A0(phiggs1,3) * mix_A0(phiggs2,2) ) &
                  + sinbe * ( mix_A0(phiggs1,1) * mix_A0(phiggs2,3) &
                            + mix_A0(phiggs1,3) * mix_A0(phiggs2,1) ) ) &
                !!!                                                                                                             
                 + ( k * mu * sqrt(2.0_omega_prec)&
                  - lambda * A_lambda / sqrt(2.0_omega_prec)  )*&
                  (mix_h0(shiggs1,1) * (  mix_A0(phiggs1,2) * mix_A0(phiggs2,3) &
                                        + mix_A0(phiggs1,3) * mix_A0(phiggs2,2) ) &
                 + mix_h0(shiggs1,2) * (  mix_A0(phiggs1,1) * mix_A0(phiggs2,3) &
                                        + mix_A0(phiggs1,3) * mix_A0(phiggs2,1) ) ) &
                !!!                                                                                                             
                 - (k * mu * sqrt(2.0_omega_prec) &
                 + lambda * A_lambda / sqrt(2.0_omega_prec)  )*&
                   mix_h0(shiggs1,3) * (  mix_A0(phiggs1,1) * mix_A0(phiggs2,2) &
                 + mix_A0(phiggs1,2) * mix_A0(phiggs2,1) )
        end do
     end do
  end do
!!!!!!!!!!!!!!!!!!!!!!!!
!!!h0h+h-
!!!!!!!!!!!!!!!!!!!!!!!!
  do shiggs1 = 1,dimh0
     g_h0hphm(shiggs1) = &
           - g * mass(24) * (mix_h0(shiggs1,1) * cosbe + mix_h0(shiggs1,2) * sinbe ) &
           -(g * mass(23) / (2.0_omega_prec * costhw) ) * (mix_h0(shiggs1,2) * sinbe &
           - mix_h0(shiggs1,1) * cosbe) * cos2be &
           + ( lambda**2 / 2.0_omega_prec * vev ) * &
             (mix_h0(shiggs1,2) * cosbe + mix_h0(shiggs1,1) * sinbe) * sin2be  &
           - ( lambda / sqrt(2.0_omega_prec) ) * mix_h0(shiggs1,3) * &
          ( (2.0_omega_prec * k * mu / lambda  + A_lambda ) * sin2be + 2.0 * mu )
  end do
!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!Scalar Higgs to Neutralinos
!!!!!!!!!!!!!!!!!!!!!!!!!!
  do neu1 = 1,dimNeu
     do neu2 = 1,dimNeu
        do shiggs1 = 1,dimh0
	   !! corrected convention w.r.t. Franke/Fraas
           g_neuneuh0_1(2,neu1,neu2,shiggs1) = &
                 (1.0_omega_prec / sqrt (2.0_omega_prec))* ( &
                - g / sqrt (2.0_omega_prec) * mix_neu(neu1,2) * &
                 ( mix_neu(neu2,3) * mix_h0(shiggs1,1)  &
                 - mix_neu(neu2,4) * mix_h0(shiggs1,2)) &
                + g * ( sinthw/costhw ) / sqrt (2.0_omega_prec) * mix_neu(neu1,1) * &
                 ( mix_neu(neu2,3) * mix_h0(shiggs1,1)  &
                 - mix_neu(neu2,4) * mix_h0(shiggs1,2)) &
                + lambda * mix_h0(shiggs1,3) * mix_neu(neu1,3) * mix_neu(neu2,4) &
		+ lambda * mix_h0(shiggs1,1) * mix_neu(neu1,4) * mix_neu(neu2,5) &
                + lambda * mix_h0(shiggs1,2) * mix_neu(neu1,3) * mix_neu(neu2,5) &
                - k * mix_h0(shiggs1,3) * mix_neu(neu1,5) * mix_neu(neu2,5) &
		)
        end do
     end do
  end do

  do neu1 = 1,dimNeu
     do neu2 = 1,dimNeu
        do shiggs1 = 1,dimh0
           g_neuneuh0(2,neu1,neu2,shiggs1) = &
                0.5_omega_prec * ( g_neuneuh0_1(2,neu1,neu2,shiggs1) &
                                 + g_neuneuh0_1(2,neu2,neu1,shiggs1))
	end do
     end do
  end do

  g_neuneuh0(1,:,:,:) = conjg (g_neuneuh0(2,:,:,:) )

!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!Axial Higgs to Neutralinos
!!!!!!!!!!!!!!!!!!!!!!!!!!
  do neu1 = 1,dimNeu
     do neu2 = 1,dimNeu
        do phiggs1 = 1,dimA0
           !! corrected convention w.r.t. Franke/Fraas                
           g_neuneuA0_1(1,neu1,neu2,phiggs1) = &
                 (imago / sqrt (2.0_omega_prec))* ( &
                - g / sqrt (2.0_omega_prec) * mix_neu(neu1,2) * &
                 ( mix_neu(neu2,3) * mix_A0(phiggs1,1)  &
                 - mix_neu(neu2,4) * mix_A0(phiggs1,2)) &
                + g * ( sinthw/costhw ) / sqrt (2.0_omega_prec) * mix_neu(neu1,1) * &
                 ( mix_neu(neu2,3) * mix_A0(phiggs1,1)  &
                 - mix_neu(neu2,4) * mix_A0(phiggs1,2)) &
                - lambda * mix_A0(phiggs1,3) * mix_neu(neu1,3) * mix_neu(neu2,4) &
                - lambda * mix_A0(phiggs1,1) * mix_neu(neu1,4) * mix_neu(neu2,5) &
                - lambda * mix_A0(phiggs1,2) * mix_neu(neu1,3) * mix_neu(neu2,5) &
                + k * mix_A0(phiggs1,3) * mix_neu(neu1,5) * mix_neu(neu2,5) &
                )
        end do
     end do
  end do

  do neu1 = 1,dimNeu
     do neu2 = 1,dimNeu
        do phiggs1 = 1,dimA0
           g_neuneuA0(1,neu1,neu2,phiggs1) = &
                0.5_omega_prec * conjg( g_neuneuA0_1(1,neu1,neu2,phiggs1) &
                                      + g_neuneuA0_1(1,neu2,neu1,phiggs1))
        end do
     end do
  end do

  g_neuneuA0(2,:,:,:) = conjg (g_neuneuA0(1,:,:,:) )

!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!Scalar Higgs to Charginos
!!!!!!!!!!!!!!!!!!!!!!!!!!
  do ch1 = 1,2
     do ch2 = 1,2
        do shiggs1 = 1,dimh0
           g_chchh0(1,ch1,ch2,shiggs1) =  - gcc * &
                (  mix_h0(shiggs1,1) * mix_charU(ch1,2) * mix_charV(ch2,1) &
                + mix_h0(shiggs1,2) * mix_charU(ch1,1) * mix_charV(ch2,2) ) &
                - (lambda / ( sqrt (8.0_omega_prec ))) * mix_h0(shiggs1,3) * &
                mix_charU(ch1,2) * mix_charV(ch2,2)
        end do
     end do
  end do

  do ch1 = 1,2
     do ch2 = 1,2
        do shiggs1 = 1,dimh0
           g_chchh0(2,ch1,ch2,shiggs1) = conjg(g_chchh0(1,ch2,ch1,shiggs1))
        end do
     end do
  end do
!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!Axial Higgs to Charginos
!!!!!!!!!!!!!!!!!!!!!!!!!!
  do ch1 = 1,2
     do ch2 = 1,2
        do phiggs1 = 1,dimA0
           g_chchA0(1,ch1,ch2,phiggs1) = imago *( gcc * &
                (mix_A0(phiggs1,1) * mix_charU(ch1,2) * mix_charV(ch2,1) &
                + mix_A0(phiggs1,2) * mix_charU(ch1,1) * mix_charV(ch2,2))  &
                -(lambda / sqrt(8.0_omega_prec)) * &
                mix_A0(phiggs1,3) * mix_charU(ch1,2) * mix_charV(ch2,2) )
        end do
     end do
  end do

  do ch1 = 1,2
     do ch2 = 1,2
        do phiggs1 = 1,dimA0
           g_chchA0(2,ch1,ch2,phiggs1) = conjg( g_chchA0(1,ch2,ch1,phiggs1))
        end do
     end do
  end do
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!Neutralino, H+/- , Chargino
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  do ch1 = 1,2
   do neu1 = 1,dimNeu

   g_neuhmchar(1,neu1,ch1) = (1.0_omega_prec / 2.0_omega_prec) * ( &
     g * cosbe * (( conjg(mix_neu(neu1,4)) ) * conjg(mix_charV(ch1,1)) + &
     ( 1.0_omega_prec / sqrt(2.0_omega_prec) ) * (( sinthw / costhw ) *&
     conjg( mix_neu(neu1,1)) + &
     conjg(mix_neu(neu1,2)) ) * conjg(mix_charV(ch1,2) )) + &
     lambda * sinbe * conjg(mix_neu(neu1,5)) * conjg(mix_charV(ch1,2))) 

   g_neuhmchar(2,neu1,ch1) = (1.0_omega_prec / 2.0_omega_prec) * ( &
     g * sinbe * (( mix_neu(neu1,3) ) * mix_charU(ch1,1) - &
     ( 1.0_omega_prec / sqrt(2.0_omega_prec) ) * (( sinthw / costhw ) * &
     mix_neu(neu1,1) + &
     mix_neu(neu1,2) ) * mix_charU(ch1,2) ) + &
     conjg(lambda) * cosbe * mix_neu(neu1,5) * mix_charU(ch1,2)) 

   end do
  end do
   g_neuhmchar_c(1,:,:) = conjg(g_neuhmchar(2,:,:))
   g_neuhmchar_c(2,:,:) = conjg(g_neuhmchar(1,:,:))

 end subroutine setup_parameters6
 subroutine setup_parameters7 ()
!!!!!!!!!!!!!!!!
!!!Neutralino Neutrino Sneutrino
!!!!!!!!!!!!!!!!  
   do gen = 1,3
      do neu1 = 1,dimNeu
         g_yuk_neu_nsn(2,gen,neu1,1) = &
              (- g / (2.0_omega_prec * sqrt(2.0_omega_prec))) * &
              ((mix_neu(neu1,2) - &
              (sinthw / costhw) * mix_neu(neu1,1)) )   
      end do
   end do
   g_yuk_neu_nsn_c(1,:,:,:) = conjg(g_yuk_neu_nsn(2,:,:,:))
   
!!!!!!!!!!!!!!!!
!!!Neutralino Lepton SLepton
!!!!!!!!!!!!!!!!
   do gen = 1,3
      do neu1 = 1,dimNeu
         do sfm1 = 1,2
              g_yuk_neu_lsl(1,gen,neu1,sfm1) = &
                   ( - (gcc * ((2.0_omega_prec * ( - q_lep) * conjg (mix_neu(neu1,1) * &
                   (sinthw / costhw) * mix_sl(gen,sfm1,2)) + ((conjg (mix_neu(neu1,3)) * &
                   mass(9 + 2*gen) * mix_sl(gen,sfm1,1)) / (mass(24) * cosbe))))))
              g_yuk_neu_lsl(2,gen,neu1,sfm1) = &
                   (gcc * ((1.0_omega_prec * (mix_neu(neu1,2) + (1.0_omega_prec *  &
                   (sinthw / costhw) * mix_neu(neu1,1))) * mix_sl(gen,sfm1,1)) - ( &     
                   (mix_neu(neu1,3) * mass(9+2*gen) * mix_sl(gen,sfm1,2)) / (mass(24) * & 
                   cosbe))))
           end do
        end do
     end do
     g_yuk_neu_lsl_c(2,:,:,:) = conjg(g_yuk_neu_lsl(1,:,:,:))
     g_yuk_neu_lsl_c(1,:,:,:) = conjg(g_yuk_neu_lsl(2,:,:,:))     
!!!!!!!!!!!!!!!!
!!!Neutralino Up SUp
!!!!!!!!!!!!!!!!
     do gen = 1,3
        do neu1 = 1,dimNeu
           do sfm1 = 1,2
              g_yuk_neu_usu(1,gen,neu1,sfm1) = &
                   ( - (gcc * ((2.0_omega_prec * ( - q_up) * conjg (mix_neu(neu1,1) * &
                   (sinthw / costhw) * mix_su(gen,sfm1,2)) + ((conjg (mix_neu(neu1,4)) *&
                   mass(2*gen) * mix_su(gen,sfm1,1)) / (mass(24) * sinbe))))))
              
              g_yuk_neu_usu(2,gen,neu1,sfm1) = &
                   (gcc * ((-1.0_omega_prec * (mix_neu(neu1,2) + &          
                   ((1.0_omega_prec/3.0_omega_prec )*  &
                   (sinthw / costhw) * mix_neu(neu1,1))) * mix_su(gen,sfm1,1)) - ( &
                   (mix_neu(neu1,4) * mass(2*gen) * mix_su(gen,sfm1,2)) / (mass(24) * & 
                   sinbe))))
           end do
        end do
     end do
     g_yuk_neu_usu_c(2,:,:,:) = conjg(g_yuk_neu_usu(1,:,:,:))
     g_yuk_neu_usu_c(1,:,:,:) = conjg(g_yuk_neu_usu(2,:,:,:))
     
!!!!!!!!!!!!!!!!
!!!Neutralino Down SDown
!!!!!!!!!!!!!!!!
     do gen = 1,3
        do neu1 = 1,dimNeu
           do sfm1 = 1,2
              g_yuk_neu_dsd(1,gen,neu1,sfm1) = &
                   ( - (gcc * ((2.0_omega_prec * ( - q_down) * conjg (mix_neu(neu1,1) * &
                   (sinthw / costhw) * mix_sd(gen,sfm1,2)) + ((conjg (mix_neu(neu1,3)) * &
                   mass(2*gen-1) * mix_sd(gen,sfm1,1)) / (mass(24) * cosbe))))))
              g_yuk_neu_dsd(2,gen,neu1,sfm1) = &
                   (gcc * ((1.0_omega_prec * (mix_neu(neu1,2) + (- &
                   (1.0_omega_prec / 3.0_omega_prec ) *  &
                   (sinthw / costhw) * mix_neu(neu1,1))) * mix_sd(gen,sfm1,1)) - ( &
                   (mix_neu(neu1,3) * mass(2*gen -1) * mix_sd(gen,sfm1,2)) / (mass(24) * & 
                   cosbe))))
           end do
        end do
     end do
     g_yuk_neu_dsd_c(2,:,:,:) = conjg(g_yuk_neu_dsd(1,:,:,:))
     g_yuk_neu_dsd_c(1,:,:,:) = conjg(g_yuk_neu_dsd(2,:,:,:))
     
     gncneu(1) = ((gz / 2.0_omega_prec) * ( &
          (2.0_omega_prec * 0.0_omega_prec * sin2thw) -  &
          (1.0_omega_prec / 2.0_omega_prec)))
     gncneu(2) = ((( - gz) / 2.0_omega_prec) *  &
          (1.0_omega_prec / 2.0_omega_prec))
     gnclep(1) = ((gz / 2.0_omega_prec) * ( &
          (2.0_omega_prec * (-1.0_omega_prec) * sin2thw) - ( -  &
          (1.0_omega_prec / 2.0_omega_prec))))
     gnclep(2) = ((( - gz) / 2.0_omega_prec) * ( -  &
          (1.0_omega_prec / 2.0_omega_prec)))
     gncup(1) = ((gz / 2.0_omega_prec) * ((2.0_omega_prec *  &
          (2.0_omega_prec / 3.0_omega_prec) * sin2thw) -  &
          (1.0_omega_prec / 2.0_omega_prec)))
     gncup(2) = ((( - gz) / 2.0_omega_prec) * (1.0_omega_prec / 2.0_omega_prec))
     gncdwn(1) = ((gz / 2.0_omega_prec) * ((2.0_omega_prec *  &
          ((-1.0_omega_prec) / 3.0_omega_prec) * sin2thw) - ( -  &
      (1.0_omega_prec / 2.0_omega_prec))))
     gncdwn(2) = ((( - gz) / 2.0_omega_prec) * ( -  &
          (1.0_omega_prec / 2.0_omega_prec)))
!!!!!!!!!!!!!!!!
!!!!!!Charged Higgs to Quarks
!!!!!!!!!!!!!!!!
     do gen1 = 1,3
        do gen2 = 1,3
           g_yuk_hp_ud(1,gen1,gen2) = ((gcc / mass(24)) * vckm(gen1,gen2) * (mass(2*gen1) / tanb))
           g_yuk_hp_ud(2,gen1,gen2) = ((gcc / mass(24)) * vckm(gen1,gen2) * tanb * mass(2*gen2-1))
   
           g_yuk_hm_du(1,gen1,gen2) = conjg(g_yuk_hp_ud(2,gen1,gen2))  
           g_yuk_hm_du(2,gen1,gen2) = conjg(g_yuk_hp_ud(1,gen1,gen2))
        end do
     end do
!!!!!!!!!!!!!!!!
!!!!!!Charged Higgs to Leptons
!!!!!!!!!!!!!!!!
     do gen = 1,3
        g_yuk_hp_ln(gen) = ((gcc / mass(24))  * (mass(9 + 2*(gen)) * tanb))
     end do
   end subroutine setup_parameters7
   subroutine setup_parameters8 ()
!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!Chargino Lepton Sneutrino
!!!!!!!!!!!!!!!!!!!!!!!!!!!
   do gen = 1,3
      do ch1 = 1,2
         g_yuk_char_lsn_c(2,gen,ch1,1) = &
              ( - ((g * mix_charV(ch1,1)) / 2.0_omega_prec))
         g_yuk_char_lsn_c(1,gen,ch1,1)=  &
              ((gcc * mass(9 + 2*gen) * conjg (mix_charU(ch1,2))) / &
              (mass(24)  * cosbe))
      end do
   end do
   g_yuk_char_lsn(2,:,:,1) = conjg(g_yuk_char_lsn_c(1,:,:,1))
   g_yuk_char_lsn(1,:,:,1) = conjg(g_yuk_char_lsn_c(2,:,:,1))
!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!Chargino Neutrino Slepton
!!!!!!!!!!!!!!!!!!!!!!!!!!!
   g_yuk_char_nsl_c(1,:,:,:) = 0 
   
   do gen = 1,3
      do ch1 = 1,2
         do sfm1 = 1,2
            g_yuk_char_nsl_c(2,gen,ch1,sfm1) = &  
                 ((((( - g) / 2.0_omega_prec) * mix_charU(ch1,1)) * &
                 conjg (mix_sl(gen,sfm1,1))) +&
                 (((gcc * mass(9+2*gen) * mix_charU(ch1,2)) / &
                 (mass(24) * cosbe)) * conjg (mix_sl(gen,sfm1,2))))
         end do
      end do
   end do
   g_yuk_char_nsl(2,:,:,:) = conjg(g_yuk_char_nsl_c(1,:,:,:))
   g_yuk_char_nsl(1,:,:,:) = conjg(g_yuk_char_nsl_c(2,:,:,:))   
!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!Chargino Down SUp (slr,sgen,fgen,char,sfm)
!!!!!!!!!!!!!!!!!!!!!!!!!!!
   do gen1 = 1,3
      do gen2 = 1,3
         do ch1 = 1,2
            do sfm1 = 1,2
               g_yuk_char_dsu(1,gen1,gen2,ch1,sfm1) = &  
                    (vckm(gen1,gen2) * gcc * (((conjg (mix_charV(ch1,2)) * &
                    mass(2*gen2) * conjg (mix_su(gen1,sfm1,2))) / &
                    (mass(24) * sinbe)) - (conjg (mix_charV(ch1,1)) *  &
                    sqrt (2.0_omega_prec) * conjg (mix_su(gen1,sfm1,1)))))
               g_yuk_char_dsu(2,gen1,gen2,ch1,sfm1) = &  
                    ((vckm(gen1,gen2) * gcc * mix_charU(ch1,2) * mass(2*gen2-1) * &
                    conjg (mix_su(gen1,sfm1,1))) / (mass(24) * cosbe))
            end do
         end do
      end do
   end do
   g_yuk_char_dsu_c(2,:,:,:,:) = conjg(g_yuk_char_dsu(1,:,:,:,:))
   g_yuk_char_dsu_c(1,:,:,:,:) = conjg(g_yuk_char_dsu(2,:,:,:,:))
!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!Chargino Up SDown (slr,sgen,fgen,char,sfm)
!!!!!!!!!!!!!!!!!!!!!!!!!!!
   do gen1 = 1,3
      do gen2 = 1,3
         do ch1 = 1,2
            do sfm1 = 1,2
               g_yuk_char_usd(1,gen1,gen2,ch1,sfm1) = &  
                    ((vckm(gen2,gen1) * gcc * mix_charV(ch1,2) * mass(2*gen2) *  &
                    conjg (mix_sd(gen1,sfm1,1))) / (mass(24) * sinbe))
               g_yuk_char_usd(2,gen1,gen2,ch1,sfm1) = &  
                    (vckm(gen2,gen1) * gcc * (((conjg (mix_charU(ch1,2)) * &
                    mass(2*gen1-1) * conjg (mix_sd(gen1,sfm1,2))) / &
                    (mass(24) * cosbe)) - (conjg (mix_charU(ch1,1)) *  &
                    sqrt (2.0_omega_prec) * conjg (mix_sd(gen1,sfm1,1)))))
            end do
         end do
      end do
   end do
   g_yuk_char_usd_c(2,:,:,:,:) = conjg(g_yuk_char_usd(1,:,:,:,:))
   g_yuk_char_usd_c(1,:,:,:,:) = conjg(g_yuk_char_usd(2,:,:,:,:))   
 end subroutine setup_parameters8
 subroutine setup_parameters9 ()
!!!!!!!!!!!!!!!
!!!!!!Gluino_Quark_SQuark
!!!!!!!!!!!!!!!
   do gen = 1,3
      do sfm1 = 1,2
         g_yuk_gluino_usu(1,gen,sfm1) = &
              ( - (mix_su(gen,sfm1,2) * (gs / sqrt (2.0_omega_prec))))
         g_yuk_gluino_usu(2,gen,sfm1) = & 
              (mix_su(gen,sfm1,1) * (gs / sqrt (2.0_omega_prec)))    
         
         g_yuk_gluino_dsd(1,gen,sfm1) = &
              ( - (mix_sd(gen,sfm1,2) * (gs / sqrt (2.0_omega_prec))))
         g_yuk_gluino_dsd(2,gen,sfm1) = & 
              (mix_sd(gen,sfm1,1) * (gs / sqrt (2.0_omega_prec)))    
      end do
   end do
   g_yuk_gluino_usu_c(1,:,:) =  g_yuk_gluino_usu(2,:,:) 
   g_yuk_gluino_usu_c(2,:,:) =  g_yuk_gluino_usu(1,:,:) 
   
   g_yuk_gluino_dsd_c(1,:,:) =  g_yuk_gluino_dsd(2,:,:) 
   g_yuk_gluino_dsd_c(2,:,:) =  g_yuk_gluino_dsd(1,:,:) 
 end subroutine setup_parameters9
  end subroutine import_from_whizard
end module omega_parameters_whizard
