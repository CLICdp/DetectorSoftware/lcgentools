! $Id: parameters.QED.omega.f90,v 1.1 2004/03/11 04:21:17 kilian Exp $
!
! Copyright (C) 2000 by Thorsten Ohl <ohl@hep.tu-darmstadt.de>
! and others
!
! O'Mega is free software; you can redistribute it and/or modify it
! under the terms of the GNU General Public License as published by 
! the Free Software Foundation; either version 2, or (at your option)
! any later version.
!
! O'Mega is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
module omega_parameters_whizard
  use omega_kinds !NODEP!
  use omega_parameters !NODEP!
  use parameters
  implicit none
  private
  public :: import_from_whizard
contains
  subroutine import_from_whizard (par)
    type(parameter_set), intent(in) :: par
    real(kind=omega_prec) :: e, qelep
    mass(1:27) = 0
    width(1:27) = 0
    mass(11) = par%me
    mass(13) = par%mmu
    mass(15) = par%mtau
    e = par%ee
    qelep = - 1
    qlep = - e * qelep
  end subroutine import_from_whizard
end module omega_parameters_whizard
