! $Id: parameters.SM_ac.omega.f90,v 1.5 2005/06/02 12:52:00 kilian Exp $
!
! Copyright (C) 2000 by Thorsten Ohl <ohl@hep.tu-darmstadt.de>
! and others
!
! O'Mega is free software; you can redistribute it and/or modify it
! under the terms of the GNU General Public License as published by 
! the Free Software Foundation; either version 2, or (at your option)
! any later version.
!
! O'Mega is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
module omega_parameters_whizard
  use omega_kinds !NODEP!
  use omega_constants !NODEP!
  use omega_parameters !NODEP!
  use parameters
  implicit none
  private
  public :: import_from_whizard
contains
  subroutine import_from_whizard (par)
    type(parameter_set), intent(in) :: par
    real(kind=omega_prec) :: e, qelep, qeup, qedwn
    mass(1:27) = 0
    width(1:27) = 0
    mass(3) = par%ms
    mass(4) = par%mc
    mass(5) = par%mb
    mass(6) = par%mtop
    width(6) = par%wtop
    mass(11) = par%me
    mass(13) = par%mmu
    mass(15) = par%mtau
    mass(23) = par%mZ
    width(23) = par%wZ
    mass(24) = par%mW
    width(24) = par%wW
    mass(25) = par%mH
    width(25) = par%wH
    mass(26) =  xi0 * mass(23)
    width(26) =  0
    mass(27) =  xipm * mass(24)
    width(27) =  0
    mkm(1) = par%mkm_s
    mkm(2) = par%mkm_p
    mkm(3) = par%mkm_r
    mkm(4) = par%mkm_f
    mkm(5) = par%mkm_t
    gkm(1) = par%gkm_s
    gkm(2) = par%gkm_p
    gkm(3) = par%gkm_r
    gkm(4) = par%gkm_f
    gkm(5) = par%gkm_t
    wkm(1) = par%wkm_s
    wkm(2) = par%wkm_p
    wkm(3) = par%wkm_r
    wkm(4) = par%wkm_f
    wkm(5) = par%wkm_t
    vev = 2 * par%mW * par%sw / par%ee 
    e = par%ee
    sinthw = par%sw
    sin2thw = sinthw**2
    costhw = par%cw
    qelep = - 1
    qeup = 2.0_omega_prec / 3.0_omega_prec
    qedwn = - 1.0_omega_prec / 3.0_omega_prec
    g = e / sinthw
    gcc = - g / 2 / sqrt (2.0_double)
    gncneu(1) = - g / 2 / costhw * ( + 0.5_double)
    gnclep(1) = - g / 2 / costhw * ( - 0.5_double - 2 * qelep * sin2thw)
    gncup(1)  = - g / 2 / costhw * ( + 0.5_double - 2 * qeup  * sin2thw)
    gncdwn(1) = - g / 2 / costhw * ( - 0.5_double - 2 * qedwn * sin2thw)
    gncneu(2) = - g / 2 / costhw * ( + 0.5_double)
    gnclep(2) = - g / 2 / costhw * ( - 0.5_double)
    gncup(2)  = - g / 2 / costhw * ( + 0.5_double)
    gncdwn(2) = - g / 2 / costhw * ( - 0.5_double)
    qlep = - e * qelep
    qup = - e * qeup
    qdwn = - e * qedwn
    qw = e
    iqw = (0,1)*qw
    gzww = g * costhw
    igzww = (0,1)*gzww
    gwww = g
    igwww = (0,1)*gwww
    gw4 = gwww**2
    gzzww = gzww**2
    gazww = gzww * qw
    gaaww = qw**2
!     ghww = mass(24) * g * par%fhvv
!     ghhww = (0,1) * g / Sqrt(2._omega_prec) * Sqrt(par%fhhvv)
!     ghzz = mass(23) * g / costhw * par%fhvv
!     ghhzz = (0,1) * g / costhw / Sqrt(2._omega_prec) * Sqrt(par%fhhvv)
    ghww = mass(24) * g
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!! This is for the old SM3:
    !!! ghhww = (0,1) * g / Sqrt(2.0_omega_prec)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ghhww = g**2 / 2.0_omega_prec
    ghzz = mass(23) * g / costhw
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!! This is for the old SM3:
    !!! ghhzz = (0,1) * g / costhw / Sqrt(2.0_omega_prec)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ghhzz = g**2 / 2.0_omega_prec / costhw**2
    ghtt = - mass(6) / vev
    ghbb = - mass(5) / vev
    ghcc = - mass(4) / vev
    ghtautau = - mass(15) / vev
    ghmm = - mass(13) / vev
!     gh3 = - 3 * par%mH**2 / vev * par%fhhh
!     gh4 = par%mH / vev * Sqrt(par%fhhhh)
    gh3 = - 3 * par%mH**2 / vev
    !!! gh4 = mass(25) / vev !!! Old SM3
    gh4 = - 3 * mass(25)**2 / vev**2
    a4 = par%a4
    a5 = par%a5
    a6 = par%a6
    a7 = par%a7
    a10 = par%a10
    lam_reg = par%mreg
    fudge_higgs = par%fhig
    fudge_km = par%fkm
    w_res = par%wres
    ig1a = iqw * par%g1a
    ig1z = igzww * par%g1z
    ig1pkpg4a = iqw   * (par%g1a + par%ka + par%g4a) / 2
    ig1pkpg4z = igzww * (par%g1z + par%kz + par%g4z) / 2
    ig1pkmg4a = iqw   * (par%g1a + par%ka - par%g4a) / 2
    ig1pkmg4z = igzww * (par%g1z + par%kz - par%g4z) / 2
    ig1mkpg4a = iqw   * (par%g1a - par%ka + par%g4a) / 2
    ig1mkpg4z = igzww * (par%g1z - par%kz + par%g4z) / 2
    ig1mkmg4a = iqw   * (par%g1a - par%ka - par%g4a) / 2
    ig1mkmg4z = igzww * (par%g1z - par%kz - par%g4z) / 2
    ila = iqw   * par%la / (mass(24)*mass(24))
    ilz = igzww * par%lz / (mass(24)*mass(24))
    rg5a = qw   * par%g5a
    rg5z = gzww * par%g5z
    ik5a = iqw   * par%k5a
    ik5z = igzww * par%k5z
    il5a = iqw   * par%l5a / (mass(24)*mass(24))
    il5z = igzww * par%l5z / (mass(24)*mass(24))
    alww0 = g**4 * (a4 + 2 * a5)
    alww2 = g**4 * 2 * a4
    alzw1 = g**4 / costhw**2 * (a4 + a6)
    alzw0 = g**4 / costhw**2 * 2 * (a5 + a7)
    alzz = g**4 / costhw**4 * 2 * (a4 + a5 + (a6+a7+a10)*2)  
    ialww0 = g**2 * sqrt (-cmplx(a4 + 2 * a5, kind=omega_prec))
    ialww2 = g**2 * sqrt (-cmplx(2 * a4, kind=omega_prec))
    ialzw1 = g**2 / costhw * sqrt (-cmplx(a4 + a6, kind=omega_prec))
    ialzw0 = g**2 / costhw &
         & * sqrt (-cmplx(2 * (a5 + a7), kind=omega_prec))
    ialzz  = g**2 / (costhw*costhw) &
         & * sqrt (-cmplx(2 * (a4 + a5 + (a6+a7+a10)*2), &
         &                kind=omega_prec))
    !!! Color flow basis, divide by sqrt(2)
    gs = sqrt(2.0_omega_prec*PI*par%alphas)
    igs = (0.0_omega_prec, 1.0_omega_prec) * gs    
  end subroutine import_from_whizard
end module omega_parameters_whizard

