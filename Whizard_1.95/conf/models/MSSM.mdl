########################################################################
# MSSM with unit CKM matrix
#     (Madgraph + CompHEP: SM)

# Independent parameters
parameter GF     1.16639E-5   # Fermi constant   
parameter mZ     91.1882      # Z-boson mass      
parameter mW     80.419       # W-boson mass      
parameter wZ     2.443        # Z-boson width
parameter wW     2.049        # W-boson width

parameter me     0.000511     # electron mass
parameter mmu    0.1057       # muon mass
parameter mtau   1.777        # tau-lepton mass
parameter mc     1.25         # c-quark mass
parameter ms     0.12         # s-quark mass
parameter mb     4.2          # b-quark mass
parameter mtop   174          # t-quark mass
parameter wtop   1.523        # t-quark width

parameter alphas 0.1178       # Strong coupling constant (Z point)

# SUSY: These parameters are not used in actual calculations
parameter mtype  1            # MSSM embedding model
parameter m_zero 100          # unified scalar mass (SUGRA/AMSB)
parameter m_half 250          # unified gaugino mass (SUGRA)
parameter A0     -100         # unified trilinear coupling (SUGRA)
parameter tanb   10           # tan(beta) = v2/v1 (MSSM input)
parameter sgn_mu 1            # signum(mu)
parameter Lambda 40000        # SUSY breaking scale (GMSB)
parameter M_mes  80000        # messenger scale (GMSB)
parameter N_5    3            # messenger index (GMSB)
parameter c_grav 1            # gravitino mass multiplier (GMSB)
parameter m_grav 60000        # gravitino mass (AMSB)

# The following parameters may all be used
# They can be derived from the above, but this is not done by WHIZARD
parameter Ae     0            # Ae soft breaking parameter
parameter Au     0            # Au soft breaking parameter
parameter Ad     0            # Ad soft breaking parameter

parameter mh     115          # light Higgs mass   
parameter wh     3.228E-3     # light Higgs width     
parameter mHH    1000         # heavy Higgs mass   
parameter mHA    1000         # axial Higgs mass
parameter mHpm   1000         # charged Higgs mass            
parameter wHH    0            # heavy Higgs width   
parameter wHpm   0            # charged Higgs width 
parameter wHA    0            # axial Higgs width 
parameter al_h   0            # Higgs mixing angle alpha
parameter mu_h   1000         # Higgs mu parameter
parameter tanb_h 10           # Higgs mixing angle tan(beta)

parameter msu1   1000         # u-squark mass
parameter msd1   1000	      # d-squark mass
parameter msc1   1000	      # c-squark mass
parameter mss1   1000	      # s-squark mass
parameter msb1   1000	      # b-squark mass
parameter mstop1 1000	      # t-squark mass
parameter msu2   1000	      # u-squark mass
parameter msd2   1000	      # d-squark mass
parameter msc2   1000	      # c-squark mass
parameter mss2   1000	      # s-squark mass
parameter msb2   1000	      # b-squark mass
parameter mstop2 1000	      # t-squark mass
parameter mse1   1000	      # selectron1 mass
parameter msne   1000	      # electron-sneutrino mass
parameter msmu1  1000	      # smuon1 mass
parameter msnmu  1000	      # muon-sneutrino mass
parameter mstau1 1000	      # stau1 mass    
parameter msntau 1000	      # tau-sneutrino mass
parameter mse2   1000	      # selectron2 mass
parameter msmu2  1000	      # smuon2 mass
parameter mstau2 1000	      # stau2 mass   

parameter mgg    1000	      # gluino mass

parameter mch1   1000	      # chargino1 mass
parameter mch2   1000	      # chargino2 mass
parameter sigc1  1            # signum chargino1
parameter sigc2  1            # signum chargino2
parameter mneu1  1000	      # neutralino1 mass
parameter mneu2  1000	      # neutralino2 mass
parameter mneu3  1000	      # neutralino3 mass
parameter mneu4  1000	      # neutralino4 mass
parameter sig1   1            # signum neutralino1
parameter sig2   1            # signum neutralino2
parameter sig3   1            # signum neutralino3
parameter sig4   1            # signum neutralino4

parameter wsu1   0	      # u-squark width         
parameter wsd1   0	      # d-squark width         
parameter wsc1   0	      # c-squark width         
parameter wss1   0	      # s-squark width         
parameter wsb1   0	      # b-squark width         
parameter wstop1 0	      # t-squark width         
parameter wsu2   0	      # u-squark width         
parameter wsd2   0	      # d-squark width         
parameter wsc2   0	      # c-squark width         
parameter wss2   0	      # s-squark width         
parameter wsb2   0	      # b-squark width         
parameter wstop2 0	      # t-squark width         
parameter wse1   0	      # selectron1 width
parameter wsne   0	      # electron-sneutrino width
parameter wsmu1  0	      # smuon1 width
parameter wsnmu  0	      # muon-sneutrino width
parameter wstau1 0	      # stau1 width    
parameter wsntau 0	      # tau-sneutrino width
parameter wse2   0	      # selectron2 width
parameter wsmu2  0	      # smuon2 width
parameter wstau2 0	      # stau2 width   
parameter wgg    0	      # gluino width
parameter wch1   0	      # chargino1 width
parameter wch2   0	      # chargino2 width
parameter wneu1  0	      # neutralino1 width
parameter wneu2  0	      # neutralino2 width
parameter wneu3  0	      # neutralino3 width
parameter wneu4  0	      # neutralino4 width

parameter mt_11	 1	      # stop mixing matrix	
parameter mt_12	 0	      # stop mixing matrix	
parameter mt_21	 0	      # stop mixing matrix	
parameter mt_22	 1	      # stop mixing matrix	
parameter mb_11	 1	      # sbottom mixing matrix	
parameter mb_12	 0	      # sbottom mixing matrix	
parameter mb_21	 0	      # sbottom mixing matrix	
parameter mb_22	 1	      # sbottom mixing matrix	
parameter ml_11	 1	      # stau mixing matrix	
parameter ml_12	 0	      # stau mixing matrix	
parameter ml_21	 0	      # stau mixing matrix	
parameter ml_22	 1	      # stau mixing matrix	

parameter mn_11  1            # neutralino mixing matrix
parameter mn_12  0            # neutralino mixing matrix
parameter mn_13  0            # neutralino mixing matrix
parameter mn_14  0            # neutralino mixing matrix
parameter mn_21  0            # neutralino mixing matrix
parameter mn_22  1            # neutralino mixing matrix
parameter mn_23  0            # neutralino mixing matrix
parameter mn_24  0            # neutralino mixing matrix
parameter mn_31  0            # neutralino mixing matrix
parameter mn_32  0            # neutralino mixing matrix
parameter mn_33  1            # neutralino mixing matrix
parameter mn_34  0            # neutralino mixing matrix
parameter mn_41  0            # neutralino mixing matrix
parameter mn_42  0            # neutralino mixing matrix
parameter mn_43  0            # neutralino mixing matrix
parameter mn_44  1            # neutralino mixing matrix
parameter mu_11  1            # chargino mixing matrix
parameter mu_12  0            # chargino mixing matrix
parameter mu_21  0            # chargino mixing matrix
parameter mu_22  1            # chargino mixing matrix
parameter mv_11  1            # chargino mixing matrix
parameter mv_12  0            # chargino mixing matrix
parameter mv_21  0            # chargino mixing matrix
parameter mv_22  1            # chargino mixing matrix
	

# Dependent parameters
derived v     1/Sqrt(Sqrt2*GF)              # v (Higgs vev)
derived cw    mW/mZ                            # cos(theta-W)
derived sw    Sqrt(1-cw**2)                    # sin(theta-W)
derived ee    2*sw*mW*Sqrt(Sqrt2*GF)           # em-coupling (GF scheme)
derived ez    ee/(sw*cw)
derived ey    ee*(sw/cw)
derived aQED  ee**2/(4*3.1415926536)           # alpha_QED
derived aQEDi 1/aQED                           # alpha_QED inverse
derived sw2   sw*sw                            # sin^2(theta-W)

########################################################################
# Particle content

# The quarks
particle D_QUARK 1  parton
  spin 1/2,  charge -1/3,  isospin -1/2, color 3
  name d, down
  anti chep:D, omega:dbar, mad:d~, tex:"\bar{d}"
particle U_QUARK 2  parton
  spin 1/2,  charge  2/3,  isospin 1/2,  color 3
  name u, up
  anti chep:U, omega:ubar, mad:u~, tex:"\bar{u}"
particle S_QUARK 3  like D_QUARK
  name s, strange
  anti chep:S, omega:sbar, mad:s~, tex:"\bar{s}"
  mass ms
particle C_QUARK 4  like U_QUARK
  name c, charm
  anti chep:C, omega:cbar, mad:c~, tex:"\bar{c}"
  mass mc
particle B_QUARK 5  like D_QUARK
  name b, bottom
  anti chep:B, omega:bbar, mad:b~, tex:"\bar{b}"
  mass mb
particle T_QUARK 6  like U_QUARK
  name t, top
  anti chep:T, omega:tbar, mad:t~, tex:"\bar{t}"
  mass mtop,  width wtop

# The leptons
particle E_LEPTON 11
  spin 1/2,  isospin -1/2,  charge  -1,
  name e, mad:omega:e-, chep:e1, electron, tex:e^-
  anti    mad:omega:e+, chep:E1, positron, tex:e^+
  mass me
particle E_NEUTRINO 12 left
  spin 1/2,  isospin 1/2
  name nu_e, omega:nue,    chep:n1, mad:ve,  e-neutrino, tex:\nu_e
  anti       omega:nuebar, chep:N1, mad:ve~,             tex:\bar\nu_e
particle MU_LEPTON 13  like E_LEPTON
  name mu, mad:omega:mu-, chep:e2, muon, tex:\mu^-
  anti     mad:omega:mu+, chep:E2,       tex:\mu^+
  mass mmu
particle MU_NEUTRINO 14  like E_NEUTRINO
  name nu_mu, omega:numu,    chep:n2, mad:vm,  mu-neutrino, tex:\nu_\mu
  anti        omega:numubar, chep:N2, mad:vm~,              tex:\bar\nu_\mu
particle TAU_LEPTON 15  like E_LEPTON
  name tau, omega:tau-, chep:e3, mad:ta-, tauon, tex:\tau^-
  anti      omega:tau+, chep:E3, mad:ta+,        tex:\tau^+
  mass mtau
particle TAU_NEUTRINO 16  like E_NEUTRINO
  name nu_tau, omega:nutau,    chep:n3, mad:vt,  tau_neutrino, tex:\nu_\tau
  anti         omega:nutaubar, chep:N3, mad:vt~,               tex:\bar\nu_\tau

# The vector bosons
particle GLUON 21  parton gauge
  spin 1,  color 8
  name g, omega:gl, chep:G, gluon
particle PHOTON 22  gauge
  spin 1
  name gamma, omega:chep:mad:A, photon, tex:\gamma
particle Z_BOSON 23  gauge
  spin 1
  name Z
  mass mZ,  width wZ
particle W_BOSON 24  gauge
  spin 1,  charge 1
  name W+, tex:W^+
  anti omega:chep:mad:W-, tex:W^-
  mass mW,  width wW

# The Higgses
particle LIGHT_HIGGS 25
  spin 0
  name h, h0, Higgs
  mass mH,  width wH
particle HEAVY_HIGGS 35
  spin 0
  name HH, HH0, omega:H, H0, tex:H^0
  mass mHH,  width wHH
particle AXIAL_HIGGS 36
  spin 0
  name HA, HA0, omega:A0, tex:A^0
  mass mHA,  width wHA
particle CHARGED_HIGGS 37
  spin 0,  charge +1
  name H+, Hp, tex:H^+
  anti omega:chep:mad:H-, Hm, tex:H^-
  mass mHpm,  width wHpm


# The squarks
# Left-handed
particle D_SQUARK1 1000001  like D_QUARK
  spin 0
  name sd1,      tex:"\tilde{d}_L"
  anti chep:SD1, omega:sd1c, sd1~,   tex:"\tilde{\bar d}_L"
  mass msd1,  width wsd1
particle U_SQUARK1 1000002  like U_QUARK
  spin 0
  name su1,      tex:"\tilde{u}_L"
  anti chep:SU1, omega:su1c, su1~, tex:"\tilde{\bar u}_L"
  mass msu1,  width wsu1
particle S_SQUARK1 1000003  like D_SQUARK1
  name ss1,      tex:"\tilde{s}_L"
  anti chep:SS1, omega:ss1c, ss1~,      tex:"\tilde{\bar s}_L"
  mass mss1,  width wss1
particle C_SQUARK1 1000004  like U_SQUARK1
  name sc1,      tex:"\tilde{c}_L"
  anti chep:SC1, omega:sc1c, sc1~,    tex:"\tilde{\bar c}_L"
  mass msc1,  width wsc1
particle B_SQUARK1 1000005  like D_SQUARK1
  name sb1,      tex:"\tilde{b}_1"
  anti chep:SB1, omega:sb1c, sb1~,     tex:"\tilde{\bar b}_1"
  mass msb1,  width wsb1
particle T_SQUARK1 1000006  like U_SQUARK1
  name st1,      tex:"\tilde{t}_1"
  anti chep:ST1, omega:st1c, st1~,  tex:"\tilde{\bar t}_1"
  mass mstop1,  width wstop1

# Right-handed
particle D_SQUARK2 2000001  like D_QUARK
  spin 0
  name sd2,      tex:"\tilde{d}_R"
  anti chep:SD2, omega:sd2c, sd2~,   tex:"\tilde{\bar d}_R"
  mass msd2,  width wsd2
particle U_SQUARK2 2000002  like U_QUARK
  spin 0
  name su2,      tex:"\tilde{u}_R"
  anti chep:SU2, omega:su2c, su2~, tex:"\tilde{\bar u}_R"
  mass msu2,  width wsu2
particle S_SQUARK2 2000003  like D_SQUARK2
  name ss2,      tex:"\tilde{s}_R"
  anti chep:SS2, omega:ss2c, ss2~,      tex:"\tilde{\bar s}_R"
  mass mss2,  width wss2
particle C_SQUARK2 2000004  like U_SQUARK2
  name sc2,      tex:"\tilde{c}_R"
  anti chep:SC2, omega:sc2c, sc2~,    tex:"\tilde{\bar c}_R"
  mass msc2,  width wsc2
particle B_SQUARK2 2000005  like D_SQUARK2
  name sb2,      tex:"\tilde{b}_2"
  anti chep:SB2, omega:sb2c, sb2~,     tex:"\tilde{\bar b}_2"
  mass msb2,  width wsb2
particle T_SQUARK2 2000006  like U_SQUARK2
  name st2,      tex:"\tilde{t}_2"
  anti chep:ST2, omega:st2c, st2~,  tex:"\tilde{\bar t}_2"
  mass mstop2,  width wstop2

# The sleptons
# Left-handed
particle E_SLEPTON1 1000011
  spin 0,  charge  -1
  name se1, omega:se1-, chep:se11, tex:"\tilde{e}_1^-"
  anti      omega:se1+, chep:SE11, tex:"\tilde{e}_1^+"
  mass mse1,  width wse1
particle E_SNEUTRINO1 1000012
  spin 0
  name snu_e1, omega:snue,  chep:sn11, sve,  tex:\tilde\nu_e
  anti         omega:snue*, chep:SN11, sve~, tex:\tilde\bar\nu_e
  mass msne,  width wsne
particle MU_SLEPTON1 1000013  like E_SLEPTON1
  name smu1, omega:smu1-, chep:se21, tex:\tilde\mu_1^-
  anti       omega:smu1+, chep:SE21, tex:\tilde\mu_1^+
  mass msmu1,  width wsmu1
particle MU_SNEUTRINO1 1000014  like E_SNEUTRINO1
  name snu_mu1, omega:snumu,  chep:sn21, svm,  tex:\tilde\nu_\mu
  anti          omega:snumu*, chep:SN21, svm~, tex:\tilde\bar\nu_\mu
  mass msnmu,  width wsnmu
particle TAU_SLEPTON1 1000015  like E_SLEPTON1
  name stau1, omega:stau1-, chep:se31, tex:\tilde\tau_1^-
  anti        omega:stau1+, chep:SE31, tex:\tilde\tau_1^+
  mass mstau1,  width wstau1
particle TAU_SNEUTRINO1 1000016  like E_SNEUTRINO1
  name snu_tau, omega:snutau,  chep:sn31, svt,  tex:\tilde\nu_\tau
  anti          omega:snutau*, chep:SN31, svt~, tex:\tilde\bar\nu_\tau
  mass msntau,  width wsntau

# Right-handed
particle E_SLEPTON2 2000011
  spin 0,  charge  -1
  name se2, omega:se2-, chep:se12, tex:"\tilde{e}_2^-"
  anti      omega:se2+, chep:SE12, tex:"\tilde{e}_2^+"
  mass mse2,  width wse2
particle MU_SLEPTON2 2000013  like E_SLEPTON2
  name smu2, omega:smu2-, chep:se22, tex:\tilde\mu_2^-
  anti       omega:smu2+, chep:SE22, tex:\tilde\mu_2^+
  mass msmu2,  width wsmu2
particle TAU_SLEPTON2 2000015  like E_SLEPTON2
  name stau2, omega:stau2-, chep:se32, tex:\tilde\tau_2^-
  anti        omega:stau2+, chep:SE32, tex:\tilde\tau_2^+
  mass mstau2,  width wstau2


# The gauginos
particle GLUINO 1000021  like GLUON
  spin 1/2
  name gg, omega:sgl, chep:GG, gluino, tex:"\tilde{g}"
  mass mgg,  width wgg

particle CHARGINO1 1000024
  spin 1/2,  charge 1
  name omega:chep:ch1+, CH1+, tex:\tilde\chi_1^+
  anti omega:chep:ch1-, CH1-, tex:\tilde\chi_1^-
  mass mch1,  width wch1
particle CHARGINO2 1000037  like CHARGINO1
  name omega:chep:ch2+, CH2+, tex:\tilde\chi_2^+
  anti omega:chep:ch2-, CH2-, tex:\tilde\chi_2^-
  mass mch2,  width wch2

particle NEUTRALINO1 1000022
  spin 1/2
  name neu1, NEU1, tex:\tilde\chi_1^0
  mass mneu1,  width wneu1
particle NEUTRALINO2 1000023  like NEUTRALINO1
  name neu2, NEU2, tex:\tilde\chi_2^0
  mass mneu2,  width wneu2
particle NEUTRALINO3 1000025  like NEUTRALINO1
  name neu3, NEU3, tex:\tilde\chi_3^0
  mass mneu3,  width wneu3
particle NEUTRALINO4 1000035  like NEUTRALINO1
  name neu4, NEU4, tex:\tilde\chi_4^0
  mass mneu4,  width wneu4

########################################################################
# Vertices of the MSSM
# CompHEP/Madgraph vertices: SM only (Feynman gauge for comphep)
# In graphs with identical structure, the first vertex is kept for phase space,
# therefore, lighter particles come before heavier ones.

! QED
vertex D d A 
      chep: { (-1/3)*ee 
            | G(m3) }
                        mad: { -ee/3   | QED }
vertex U u A 
      chep: { (2/3)*ee 
            | G(m3) }
                        mad: { ee*2/3  | QED }
vertex S s A 
      chep: { (-1/3)*ee 
            | G(m3) }
                        mad: { -ee/3   | QED }
vertex C c A 
      chep: { (2/3)*ee 
            | G(m3) }
                        mad: { ee*2/3  | QED }
vertex B b A 
      chep: { (-1/3)*ee 
            | G(m3) }
                        mad: { -ee/3   | QED }
vertex T t A 
      chep: { (2/3)*ee 
            | G(m3) }
                        mad: { ee*2/3  | QED }

vertex E1 e1 A 
      chep: { -ee 
            | G(m3) }
                        mad: { ee | QED }
vertex E2 e2 A 
      chep: { -ee 
            | G(m3) }
                        mad: { ee | QED }
vertex E3 e3 A 
      chep: { -ee 
            | G(m3) }
                        mad: { ee | QED }

! QED/SUSY
vertex sd1 SD1 A
vertex su1 SU1 A
vertex ss1 SS1 A
vertex sc1 SC1 A
vertex sb1 SB1 A
vertex st1 ST1 A
vertex se11 SE11 A
vertex se21 SE21 A
vertex se31 SE31 A
vertex sd2 SD2 A
vertex su2 SU2 A
vertex ss2 SS2 A
vertex sc2 SC2 A
vertex sb2 SB2 A
vertex st2 ST2 A
vertex se12 SE12 A
vertex se22 SE22 A
vertex se32 SE32 A

# QCD
vertex G G G 
      chep: { gg 
            | m1.m2*(p1-p2).m3+m2.m3*(p2-p3).m1+m3.m1*(p3-p1).m2 }
                        mad: { gg | QCD }
vertex G G G G
                        mad: { gg | QCD }
vertex G G G.t 
      chep: { gg/Sqrt2 
            | m1.M3*m2.m3-m1.m3*m2.M3 }
vertex G.C G.c G 
      chep: { -gg 
            | p1.m3 }
! SUSY QCD
vertex g gg gg

! QCD fermion-gluon
vertex D d G 
      chep: { gg 
            | G(m3) }
                        mad: { -gg | QCD }
vertex U u G 
      chep: { gg 
            | G(m3) }
                        mad: { -gg | QCD }
vertex S s G 
      chep: { gg 
            | G(m3) }
                        mad: { -gg | QCD }
vertex C c G 
      chep: { gg 
            | G(m3) }
                        mad: { -gg | QCD }
vertex B b G 
      chep: { gg 
            | G(m3) }
                        mad: { -gg | QCD }
vertex T t G 
      chep: { gg 
            | G(m3) }
                        mad: { -gg | QCD }

! SUSY sfermion-gluon
vertex sd1 SD1 g
vertex sd2 SD2 g
vertex su1 SU1 g
vertex su2 SU2 g
vertex ss1 SS1 g
vertex ss2 SS2 g
vertex sc1 SC1 g
vertex sc2 SC2 g
vertex sb1 SB1 g
vertex sb2 SB2 g
vertex st1 ST1 g
vertex st2 ST2 g
! SUSY gluino
vertex d SD1 gg
vertex d SD2 gg
vertex u SU1 gg
vertex u SU2 gg
vertex s SS1 gg
vertex s SS2 gg
vertex c SC1 gg
vertex c SC2 gg
vertex b SB1 gg
vertex b SB2 gg
vertex t ST1 gg
vertex t ST2 gg
vertex sd1 D gg
vertex sd2 D gg
vertex su1 U gg
vertex su2 U gg
vertex ss1 S gg
vertex ss2 S gg
vertex sc1 C gg
vertex sc2 C gg
vertex sb1 B gg
vertex sb2 B gg
vertex st1 T gg
vertex st2 T gg


# Neutral currents
vertex D d Z 
      chep: { -ee/(12*sw*cw) 
            | +3*G(m3)*(1-G5)-4*(sw^ 2)*G(m3) }
                        mad: { (/ -ez*(-1+2*sw2/3)/2,  -ey/3 /) | QFD } 
vertex U u Z 
      chep: { -ee/(12*sw*cw) 
            | -3*G(m3)*(1-G5)+8*(sw^ 2)*G(m3) }
                        mad: { (/ -ez*( 1-4*sw2/3)/2, ey*2/3 /) | QFD } 
vertex S s Z 
      chep: { -ee/(12*sw*cw) 
            | +3*G(m3)*(1-G5)-4*(sw^ 2)*G(m3) }
                        mad: { (/ -ez*(-1+2*sw2/3)/2,  -ey/3 /) | QFD } 
vertex C c Z 
      chep: { -ee/(12*sw*cw) 
            | -3*G(m3)*(1-G5)+8*(sw^ 2)*G(m3) }
                        mad: { (/ -ez*( 1-4*sw2/3)/2, ey*2/3 /) | QFD } 
vertex B b Z 
      chep: { -ee/(12*sw*cw) 
            | +3*G(m3)*(1-G5)-4*(sw^ 2)*G(m3) }
                        mad: { (/ -ez*(-1+2*sw2/3)/2,  -ey/3 /) | QFD } 
vertex T t Z 
      chep: { -ee/(12*sw*cw) 
            | -3*G(m3)*(1-G5)+8*(sw^ 2)*G(m3) }
                        mad: { (/ -ez*( 1-4*sw2/3)/2, ey*2/3 /) | QFD } 

vertex E1 e1 Z 
      chep: { -ee/(4*sw*cw) 
            | G(m3)*(1-G5)-4*(sw^ 2)*G(m3) }
                        mad: { (/ -ez*(-1+2*sw2)/2, -ey /) | QFD } 
vertex E2 e2 Z 
      chep: { -ee/(4*sw*cw) 
            | G(m3)*(1-G5)-4*(sw^ 2)*G(m3) }
                        mad: { (/ -ez*(-1+2*sw2)/2, -ey /) | QFD } 
vertex E3 e3 Z 
      chep: { -ee/(4*sw*cw) 
            | G(m3)*(1-G5)-4*(sw^ 2)*G(m3) }
                        mad: { (/ -ez*(-1+2*sw2)/2, -ey /) | QFD } 
vertex N1 n1 Z 
      chep: { ee/(4*sw*cw) 
            | G(m3)*(1-G5) }
                        mad: { (/ -ez/2, zero /) | QFD } 
vertex N2 n2 Z 
      chep: { ee/(4*sw*cw) 
            | G(m3)*(1-G5) }
                        mad: { (/ -ez/2, zero /) | QFD } 
vertex N3 n3 Z 
      chep: { ee/(4*sw*cw) 
            | G(m3)*(1-G5) }
                        mad: { (/ -ez/2, zero /) | QFD } 

! Neutral currents/SUSY
vertex sd1 SD1 Z
vertex su1 SU1 Z
vertex ss1 SS1 Z
vertex sc1 SC1 Z
vertex sb1 SB1 Z
vertex st1 ST1 Z
vertex se11 SE11 Z
vertex se21 SE21 Z
vertex se31 SE31 Z
vertex sn11 SN11 Z
vertex sn21 SN21 Z
vertex sn31 SN31 Z
vertex sd2 SD2 Z
vertex su2 SU2 Z
vertex ss2 SS2 Z
vertex sc2 SC2 Z
vertex sb2 SB2 Z
vertex st2 ST2 Z
vertex se12 SE12 Z
vertex se22 SE22 Z
vertex se32 SE32 Z
! 3rd gen mixing
vertex sb1 SB2 Z
vertex sb2 SB1 Z
vertex st1 ST2 Z
vertex st2 ST1 Z
vertex se31 SE32 Z
vertex se32 SE31 Z


# Charged currents
vertex U d W+ 
      chep: { ee/(2*Sqrt2*sw) 
            | G(m3)*(1-G5) }
                        mad: { (/ -ee/sqrt(2*sw2), zero /) | QFD }
vertex C s W+ 
      chep: { ee/(2*Sqrt2*sw) 
            | G(m3)*(1-G5) }
                        mad: { (/ -ee/sqrt(2*sw2), zero /) | QFD }
vertex T b W+ 
      chep: { ee/(2*Sqrt2*sw) 
            | G(m3)*(1-G5) }
                        mad: { (/ -ee/sqrt(2*sw2), zero /) | QFD }
vertex D u W- 
      chep: { ee/(2*Sqrt2*sw) 
            | G(m3)*(1-G5) }
                        mad: { (/ -ee/sqrt(2*sw2), zero /) | QFD }
vertex S c W- 
      chep: { ee/(2*Sqrt2*sw) 
            | G(m3)*(1-G5) }
                        mad: { (/ -ee/sqrt(2*sw2), zero /) | QFD }
vertex B t W- 
      chep: { ee/(2*Sqrt2*sw) 
            | G(m3)*(1-G5) }
                        mad: { (/ -ee/sqrt(2*sw2), zero /) | QFD }

vertex N1 e1 W+ 
      chep: { ee/(2*Sqrt2*sw) 
            | G(m3)*(1-G5) }
                        mad: { (/ -ee/sqrt(2*sw2), zero /) | QFD }
vertex N2 e2 W+ 
      chep: { ee/(2*Sqrt2*sw) 
            | G(m3)*(1-G5) }
                        mad: { (/ -ee/sqrt(2*sw2), zero /) | QFD }
vertex N3 e3 W+ 
      chep: { ee/(2*Sqrt2*sw) 
            | G(m3)*(1-G5) }
                        mad: { (/ -ee/sqrt(2*sw2), zero /) | QFD }
vertex E1 n1 W- 
      chep: { ee/(2*Sqrt2*sw) 
            | G(m3)*(1-G5) }
                        mad: { (/ -ee/sqrt(2*sw2), zero /) | QFD }
vertex E2 n2 W- 
      chep: { ee/(2*Sqrt2*sw) 
            | G(m3)*(1-G5) }
                        mad: { (/ -ee/sqrt(2*sw2), zero /) | QFD }
vertex E3 n3 W- 
      chep: { ee/(2*Sqrt2*sw) 
            | G(m3)*(1-G5) }
                        mad: { (/ -ee/sqrt(2*sw2), zero /) | QFD }

! SUSY
vertex su1 SD1 W-
vertex sc1 SS1 W-
vertex st1 SB1 W-
vertex sn11 SE11 W-
vertex sn21 SE21 W-
vertex sn31 SE31 W-
vertex sd1 SU1 W+
vertex ss1 SC1 W+
vertex sb1 ST1 W+
vertex se11 SN11 W+
vertex se21 SN21 W+
vertex se31 SN31 W+

! 3rd gen mixing
vertex st1 SB2 W-
vertex st2 SB1 W-
vertex st2 SB2 W-
vertex sn31 SE32 W-
vertex sb1 ST2 W+
vertex sb2 ST1 W+
vertex sb2 ST2 W+
vertex se32 SN31 W+ 

# Yukawa (neutral)
vertex S s h 
      chep: { -ee*ms/(2*mW*sw) }
                        mad: { -ms/v   | QFD }
vertex C c h 
      chep: { -ee*mc/(2*mW*sw) }
                        mad: { -mc/v   | QFD }
vertex B b h 
      chep: { -ee*mb/(2*mW*sw) }
                        mad: { -mb/v   | QFD }
vertex T t h 
      chep: { -ee*mtop/(2*mW*sw) }
                        mad: { -mtop/v | QFD }
vertex E2 e2 h 
      chep: { -ee*mmu/(2*mW*sw) }
                        mad: { -mmu/v  | QFD }
vertex E3 e3 h 
      chep: { -ee*mtau/(2*mW*sw) }
                        mad: { -mtau/v | QFD }

vertex S s Z.f 
      chep: { i*ee*ms/(2*mW*sw) 
            | G5 }
vertex C c Z.f 
      chep: { -i*ee*mc/(2*mW*sw) 
            | G5 }
vertex B b Z.f 
      chep: { i*ee*mb/(2*mW*sw) 
            | G5 }
vertex T t Z.f 
      chep: { -i*ee*mtop/(2*mW*sw) 
            | G5 }
vertex E2 e2 Z.f 
      chep: { i*ee*mmu/(2*mW*sw) 
            | G5 }
vertex E3 e3 Z.f 
      chep: { i*ee*mtau/(2*mW*sw) 
            | G5 }

# Yukawa (charged)
vertex C s W+.f 
      chep: { -i*ee/(2*Sqrt2*mW*sw) 
            | ms*(1+G5)-mc*(1-G5) }
vertex T b W+.f 
      chep: { -i*ee/(2*Sqrt2*mW*sw) 
            | mb*(1+G5)-mtop*(1-G5) }
vertex S c W-.f 
      chep: { -i*ee/(2*Sqrt2*mW*sw) 
            | mc*(1+G5)-ms*(1-G5) }
vertex B t W-.f 
      chep: { -i*ee/(2*Sqrt2*mW*sw) 
            | mtop*(1+G5)-mb*(1-G5) }

vertex E2 n2 W-.f 
      chep: { -i*ee*mmu/(2*Sqrt2*mW*sw) 
            | -(1-G5) }
vertex E3 n3 W-.f 
      chep: { -i*ee*mtau/(2*Sqrt2*mW*sw) 
            | -(1-G5) }
vertex N2 e2 W+.f 
      chep: { -i*ee*mmu/(2*Sqrt2*mW*sw) 
            | (1+G5) }
vertex N3 e3 W+.f 
      chep: { -i*ee*mtau/(2*Sqrt2*mW*sw) 
            | (1+G5) }

! SUSY Higgses
vertex e3 E3 HH
vertex b B HH
vertex t T HH
vertex e3 E3 HA
vertex b B HA
vertex t T HA
vertex e3 N3 H+
vertex E3 n3 H-
vertex b T H+
vertex B t H-

! SUSY-Yukawa
vertex se31 SE31 h
vertex se31 SE32 h
vertex se32 SE31 h
vertex se32 SE32 h
vertex sb1 SB1 h
vertex sb1 SB2 h
vertex sb2 SB1 h
vertex sb2 SB2 h
vertex st1 ST1 h
vertex st1 ST2 h
vertex st2 ST1 h
vertex st2 ST2 h
vertex se31 SE31 HH
vertex se31 SE32 HH
vertex se32 SE31 HH
vertex se32 SE32 HH
vertex sb1 SB1 HH
vertex sb1 SB2 HH
vertex sb2 SB1 HH
vertex sb2 SB2 HH
vertex st1 ST1 HH
vertex st1 ST2 HH
vertex st2 ST1 HH
vertex st2 ST2 HH
vertex se31 SE31 HA
vertex se31 SE32 HA
vertex se32 SE31 HA
vertex se32 SE32 HA
vertex sb1 SB1 HA
vertex sb1 SB2 HA
vertex sb2 SB1 HA
vertex sb2 SB2 HA
vertex st1 ST1 HA
vertex st1 ST2 HA
vertex st2 ST1 HA
vertex st2 ST2 HA
vertex se31 SN31 H+
vertex se32 SN31 H+
vertex sb1 ST1 H+
vertex sb1 ST2 H+
vertex sb2 ST1 H+
vertex sb2 ST2 H+
vertex SE31 sn31 H-
vertex SE32 sn31 H-
vertex SB1 st1 H-
vertex SB1 st2 H-
vertex SB2 st1 H-
vertex SB2 st2 H-

# Vector-boson self-interactions
vertex W+ W- A
      chep: { -ee 
            | m1.m2*(p1-p2).m3+m2.m3*(p2-p3).m1+m3.m1*(p3-p1).m2 }
                        mad: { ee       | QFD }
vertex W+ W- Z
      chep: { -ee*cw/sw 
            | m1.m2*(p1-p2).m3+m2.m3*(p2-p3).m1+m3.m1*(p3-p1).m2 }
                        mad: { ee*cw/sw | QFD }

vertex W+ W- Z Z
      chep: { -(ee*cw/sw)**2 
            | 2*m1.m2*m3.m4-m1.m3*m2.m4-m1.m4*m2.m3 }
                        mad: { (ee*cw/sw)**2 | QFD }
vertex W+ W+ W- W-
      chep: { (ee/sw)**2 
            | 2*m1.m2*m3.m4-m1.m3*m2.m4-m1.m4*m2.m3 }
                        mad: { (ee/sw)**2    | QFD }
vertex W+ W- A Z
      chep: { -ee**2*cw/sw 
            | 2*m1.m2*m3.m4-m1.m3*m2.m4-m1.m4*m2.m3 }
                        mad: { ee**2*cw/sw   | QFD }
vertex W+ W- A A
      chep: { -ee**2 
            | 2*m1.m2*m3.m4-m1.m3*m2.m4-m1.m4*m2.m3 }
                        mad: { ee**2         | QFD }

vertex W-.C Z.c W+ 
      chep: { ee*cw/sw 
            | p1.m3 }
vertex W+.C Z.c W- 
      chep: { -ee*cw/sw 
            | p1.m3 }
vertex Z.C W-.c W+ 
      chep: { -ee*cw/sw 
            | p1.m3 }
vertex Z.C W+.c W- 
      chep: { ee*cw/sw 
            | p1.m3 }
vertex W-.C W+.c Z 
      chep: { -ee*cw/sw 
            | p1.m3 }
vertex W+.C W-.c Z 
      chep: { ee*cw/sw 
            | p1.m3 }
vertex W-.C W+.c A 
      chep: { -ee 
            | p1.m3 }
vertex W+.C W-.c A 
      chep: { ee 
            | p1.m3 }

# Higgs - vector boson
vertex h W+ W-
      chep: { ee*mW/sw 
            | m2.m3 }
                        mad: { (ee/sw)**2*v/2    | QFD }
vertex h Z Z
      chep: { ee/(sw*cw**2)*mW 
            | m2.m3 }
                        mad: { (ee/sw/cw)**2*v/2 | QFD }
vertex h h W+ W-
      chep: {  (1/2)*(ee/sw)**2 
            | m3.m4 }
                        mad: { (ee/sw)**2/2      | QFD }
vertex h h Z Z
      chep: {  (1/2)*(ee/(sw*cw))**2 
            | m3.m4 }
                        mad: { (ee/sw/cw)**2/2   | QFD }

vertex h Z.f Z 
      chep: { i*ee/(2*cw*sw) 
            | (p2-p1).m3 }
vertex h W-.f W+ 
      chep: { i*ee/(2*sw) 
            | (p2-p1).m3 }
vertex h W+.f W- 
      chep: { i*ee/(2*sw) 
            | (p2-p1).m3 }
vertex Z.f W+.f W- 
      chep: { ee/(2*sw) 
            | -(p2-p1).m3 }
vertex Z.f W-.f W+ 
      chep: { ee/(2*sw) 
            | (p2-p1).m3 }
vertex W-.f W+.f Z 
      chep: { ee/(2*cw*sw) 
            | (1-2*sw^ 2)* (p2-p1).m3 }
vertex W-.f W+.f A 
      chep: { ee 
            | (p2-p1).m3 }

vertex W-.f W+ A 
      chep: { -i*ee*mW 
            | m2.m3 }
vertex W+.f W- A 
      chep: { -i*ee*mW 
            | -m2.m3 }
vertex W-.f W+ Z 
      chep: { -i*ee*mW*sw/cw 
            | -m2.m3 }
vertex W+.f W- Z 
      chep: { -i*ee*mW*sw/cw 
            | m2.m3 }

vertex W-.f W+.f A A
      chep: { 2*ee**2 
            | m3.m4 }
vertex W-.f W+.f Z Z
      chep: { (ee/(cw*sw))**2/2 
            | (1-2*sw^ 2)^ 2*m3.m4 }
vertex W-.f W+.f W- W+
      chep: { ee**2/(2*sw*sw) 
            | m3.m4 }
vertex W-.f W+.f Z A
      chep: { ee**2/(sw*cw) 
            | (1-2*sw^ 2)*m3.m4 }
vertex Z.f Z.f Z Z
      chep: { (ee/(sw*cw))**2/2 
            | m3.m4 }
vertex Z.f Z.f W- W+
      chep: { ee**2/(2*sw*sw) 
            | m3.m4 }
vertex W-.f Z.f W+ A
      chep: { -ee**2/(2*sw) 
            | m3.m4 }
vertex W+.f Z.f W- A
      chep: { -ee**2/(2*sw) 
            | m3.m4 }
vertex W-.f Z.f W+ Z
      chep: { ee**2/(2*cw) 
            | m3.m4 }
vertex W+.f Z.f W- Z
      chep: { ee**2/(2*cw) 
            | m3.m4 }
vertex W-.f h W+ A
      chep: { -i*ee**2/(2*sw) 
            | m3.m4 }
vertex W+.f h W- A
      chep: { i*ee**2/(2*sw) 
            | m3.m4 }
vertex W-.f h W+ Z
      chep: { i*ee**2/(2*cw) 
            | m3.m4 }
vertex W+.f h W- Z
      chep: { -i*ee**2/(2*cw) 
            | m3.m4 }

vertex Z.C Z.c h 
      chep: { -ee*mW/(2*sw*cw*cw) }

vertex W-.C W+.c h 
      chep: { -ee*mW/(2*sw) }

vertex W+.C W-.c h 
      chep: { -ee*mW/(2*sw) }

vertex W-.C W+.c Z.f 
      chep: { i*ee*mW/(2*sw) }

vertex W+.C W-.c Z.f 
      chep: { -i*ee*mW/(2*sw) }

vertex W-.C Z.c W+.f 
      chep: { -i*ee*mW/(2*cw*sw) 
            | 1-2*sw^ 2 }
vertex W+.C Z.c W-.f 
      chep: { i*ee*mW/(2*cw*sw) 
            | 1-2*sw^ 2 }
vertex Z.C W-.c W+.f 
      chep: { i*ee*mW/(2*cw*sw) }

vertex Z.C W+.c W-.f 
      chep: { -i*ee*mW/(2*cw*sw) }

vertex W-.C A.c W+ 
      chep: { ee 
            | p1.m3 }
vertex W+.C A.c W- 
      chep: { -ee 
            | p1.m3 }
vertex A.C W-.c W+ 
      chep: { -ee 
            | p1.m3 }
vertex A.C W+.c W- 
      chep: { ee 
            | p1.m3 }
vertex W-.C A.c W+.f 
      chep: { -i*ee*mW }

vertex W+.C A.c W-.f 
      chep: { i*ee*mW }

# Higgs self-interactions
vertex h h h
      chep: { -(3/2)*ee*mh**2/(mW*sw) }
                        mad: { -3*mh**2/v        | QFD }

vertex W+.f W-.f h 
      chep: { -ee*mh**2/(2*mW*sw) }

vertex Z.f Z.f h 
      chep: { -ee*mh**2/(2*mW*sw) }


vertex h h h h
      chep: { (-3/4)*(ee*mh/(mW*sw))**2 }
# mad: not implemented

vertex Z.f Z.f Z.f Z.f
      chep: { -3*(ee*mh/(2*mW*sw))**2 }

vertex Z.f Z.f W-.f W+.f
      chep: { -(ee*mh/(2*mW*sw))**2 }

vertex W-.f W-.f W+.f W+.f
      chep: { - (ee*mh/(mW*sw))**2/2 }

vertex Z.f Z.f h h
      chep: { -(ee*mh/(2*mW*sw))**2 }

vertex W+.f W-.f h h
      chep: { -(ee*mh/(2*mW*sw))**2 }

! SUSY Higgses
vertex W+ W- HH
vertex Z Z HH
vertex A H+ H-
vertex Z Z HH
vertex Z h HA
vertex Z HH HA
vertex Z H+ H-
vertex W+ H- h
vertex W- H+ h
vertex W+ H- HH
vertex W- H+ HH
vertex W+ H- HA
vertex W- H+ HA
vertex h h HH
vertex h HH HH
vertex HH HH HH
vertex h HA HA
vertex HH HA HA
vertex h H+ H-
vertex HH H+ H-

! Charginos
vertex ch1+ ch1- A
vertex ch2+ ch2- A
vertex ch1+ ch1- Z
vertex ch1+ ch2- Z
vertex ch2+ ch1- Z
vertex ch2+ ch2- Z

! Neutralinos
vertex neu1 neu1 Z
vertex neu1 neu2 Z
vertex neu1 neu3 Z
vertex neu1 neu4 Z
vertex neu2 neu2 Z
vertex neu2 neu3 Z
vertex neu2 neu4 Z
vertex neu3 neu3 Z
vertex neu3 neu4 Z
vertex neu4 neu4 Z

! Charginos + neutralinos
vertex ch1+ neu1 W-
vertex ch2+ neu1 W-
vertex ch1+ neu2 W-
vertex ch2+ neu2 W-
vertex ch1+ neu3 W-
vertex ch2+ neu3 W-
vertex ch1+ neu4 W-
vertex ch2+ neu4 W-
vertex ch1- neu1 W+
vertex ch2- neu1 W+
vertex ch1- neu2 W+
vertex ch2- neu2 W+
vertex ch1- neu3 W+
vertex ch2- neu3 W+
vertex ch1- neu4 W+
vertex ch2- neu4 W+

! Charginos + Higgs
vertex ch1+ ch1- h
vertex ch1+ ch2- h
vertex ch2+ ch1- h
vertex ch2+ ch2- h
vertex ch1+ ch1- HH
vertex ch1+ ch2- HH
vertex ch2+ ch1- HH
vertex ch2+ ch2- HH
vertex ch1+ ch1- HA
vertex ch1+ ch2- HA
vertex ch2+ ch1- HA
vertex ch2+ ch2- HA

! Neutralinos + Higgs
vertex neu1 neu1 h
vertex neu1 neu2 h
vertex neu1 neu3 h
vertex neu1 neu4 h
vertex neu2 neu2 h
vertex neu2 neu3 h
vertex neu2 neu4 h
vertex neu3 neu3 h
vertex neu3 neu4 h
vertex neu4 neu4 h
vertex neu1 neu1 HH
vertex neu1 neu2 HH
vertex neu1 neu3 HH
vertex neu1 neu4 HH
vertex neu2 neu2 HH
vertex neu2 neu3 HH
vertex neu2 neu4 HH
vertex neu3 neu3 HH
vertex neu3 neu4 HH
vertex neu4 neu4 HH
vertex neu1 neu1 HA
vertex neu1 neu2 HA
vertex neu1 neu3 HA
vertex neu1 neu4 HA
vertex neu2 neu2 HA
vertex neu2 neu3 HA
vertex neu2 neu4 HA
vertex neu3 neu3 HA
vertex neu3 neu4 HA
vertex neu4 neu4 HA

! Charginos + neutralinos + Higgs
vertex ch1+ neu1 H-
vertex ch2+ neu1 H-
vertex ch1+ neu2 H-
vertex ch2+ neu2 H-
vertex ch1+ neu3 H-
vertex ch2+ neu3 H-
vertex ch1+ neu4 H-
vertex ch2+ neu4 H-
vertex ch1- neu1 H+
vertex ch2- neu1 H+
vertex ch1- neu1 H+
vertex ch2- neu2 H+
vertex ch1- neu3 H+
vertex ch2- neu3 H+
vertex ch1- neu4 H+
vertex ch2- neu4 H+

! Lepton-slepton-neutralino
vertex e1 SE11 neu1
vertex e1 SE11 neu2
vertex e1 SE11 neu3
vertex e1 SE11 neu4
vertex e1 SE12 neu1
vertex e1 SE12 neu2
vertex e1 SE12 neu3
vertex e1 SE12 neu4
vertex E1 se11 neu1
vertex E1 se11 neu2
vertex E1 se11 neu3
vertex E1 se11 neu4
vertex E1 se12 neu1
vertex E1 se12 neu2
vertex E1 se12 neu3
vertex E1 se12 neu4
vertex e2 SE21 neu1
vertex e2 SE21 neu2
vertex e2 SE21 neu3
vertex e2 SE21 neu4
vertex E2 se21 neu1
vertex E2 se21 neu2
vertex E2 se21 neu3
vertex E2 se21 neu4
vertex e2 SE22 neu1
vertex e2 SE22 neu2
vertex e2 SE22 neu3
vertex e2 SE22 neu4
vertex E2 se22 neu1
vertex E2 se22 neu2
vertex E2 se22 neu3
vertex E2 se22 neu4
vertex e3 SE31 neu1
vertex e3 SE31 neu2
vertex e3 SE31 neu3
vertex e3 SE31 neu4
vertex E3 se31 neu1
vertex E3 se31 neu2
vertex E3 se31 neu3
vertex E3 se31 neu4
vertex e3 SE32 neu1
vertex e3 SE32 neu2
vertex e3 SE32 neu3
vertex e3 SE32 neu4
vertex E3 se32 neu1
vertex E3 se32 neu2
vertex E3 se32 neu3
vertex E3 se32 neu4
! Neutrino-sneutrino-neutralino
vertex n1 SN11 neu1
vertex n1 SN11 neu2
vertex n1 SN11 neu3
vertex n1 SN11 neu4
vertex N1 sn11 neu1
vertex N1 sn11 neu2
vertex N1 sn11 neu3
vertex N1 sn11 neu4
vertex n2 SN21 neu1
vertex n2 SN21 neu2
vertex n2 SN21 neu3
vertex n2 SN21 neu4
vertex N2 sn21 neu1
vertex N2 sn21 neu2
vertex N2 sn21 neu3
vertex N2 sn21 neu4
vertex n3 SN31 neu1
vertex n3 SN31 neu2
vertex n3 SN31 neu3
vertex n3 SN31 neu4
vertex N3 sn31 neu1
vertex N3 sn31 neu2
vertex N3 sn31 neu3
vertex N3 sn31 neu4
! Quark-squark-neutralino
vertex d SD1 neu1
vertex d SD1 neu2
vertex d SD1 neu3
vertex d SD1 neu4
vertex d SD2 neu1
vertex d SD2 neu2
vertex d SD2 neu3
vertex d SD2 neu4
vertex D sd1 neu1
vertex D sd1 neu2
vertex D sd1 neu3
vertex D sd1 neu4
vertex D sd2 neu1
vertex D sd2 neu2
vertex D sd2 neu3
vertex D sd2 neu4
vertex u SU1 neu1
vertex u SU1 neu2
vertex u SU1 neu3
vertex u SU1 neu4
vertex u SU2 neu1
vertex u SU2 neu2
vertex u SU2 neu3
vertex u SU2 neu4
vertex U su1 neu1
vertex U su1 neu2
vertex U su1 neu3
vertex U su1 neu4
vertex U su2 neu1
vertex U su2 neu2
vertex U su2 neu3
vertex U su2 neu4
vertex s SS1 neu1
vertex s SS1 neu2
vertex s SS1 neu3
vertex s SS1 neu4
vertex s SS2 neu1
vertex s SS2 neu2
vertex s SS2 neu3
vertex s SS2 neu4
vertex S ss1 neu1
vertex S ss1 neu2
vertex S ss1 neu3
vertex S ss1 neu4
vertex S ss2 neu1
vertex S ss2 neu2
vertex S ss2 neu3
vertex S ss2 neu4
vertex c SC1 neu1
vertex c SC1 neu2
vertex c SC1 neu3
vertex c SC1 neu4
vertex c SC2 neu1
vertex c SC2 neu2
vertex c SC2 neu3
vertex c SC2 neu4
vertex C sc1 neu1
vertex C sc1 neu2
vertex C sc1 neu3
vertex C sc1 neu4
vertex C sc2 neu1
vertex C sc2 neu2
vertex C sc2 neu3
vertex C sc2 neu4
vertex b SB1 neu1
vertex b SB1 neu2
vertex b SB1 neu3
vertex b SB1 neu4
vertex b SB2 neu1
vertex b SB2 neu2
vertex b SB2 neu3
vertex b SB2 neu4
vertex B sb1 neu1
vertex B sb1 neu2
vertex B sb1 neu3
vertex B sb1 neu4
vertex B sb2 neu1
vertex B sb2 neu2
vertex B sb2 neu3
vertex B sb2 neu4
vertex t ST1 neu1
vertex t ST1 neu2
vertex t ST1 neu3
vertex t ST1 neu4
vertex t ST2 neu1
vertex t ST2 neu2
vertex t ST2 neu3
vertex t ST2 neu4
vertex T st1 neu1
vertex T st1 neu2
vertex T st1 neu3
vertex T st1 neu4
vertex T st2 neu1
vertex T st2 neu2
vertex T st2 neu3
vertex T st2 neu4

! Lepton-sneutrino-chargino
vertex e1 SN11 ch1+
vertex e1 SN11 ch2+
vertex E1 sn11 ch1-
vertex E1 sn11 ch2-
vertex e2 SN21 ch1+
vertex e2 SN21 ch2+
vertex E2 sn21 ch1-
vertex E2 sn21 ch2-
vertex e3 SN31 ch1+
vertex e3 SN31 ch2+
vertex E3 sn31 ch1-
vertex E3 sn31 ch2-
! Slepton-neutrino-chargino
vertex se11 N1 ch1+
vertex se11 N1 ch2+
vertex se12 N1 ch1+
vertex se12 N1 ch2+
vertex SE11 n1 ch1-
vertex SE11 n1 ch2-
vertex SE12 n1 ch1-
vertex SE12 n1 ch2-
vertex se21 N2 ch1+
vertex se21 N2 ch2+
vertex se22 N2 ch1+
vertex se22 N2 ch2+
vertex SE21 n2 ch1-
vertex SE21 n2 ch2-
vertex SE22 n2 ch1-
vertex SE22 n2 ch2-
vertex se31 N3 ch1+
vertex se31 N3 ch2+
vertex se32 N3 ch1+
vertex se32 N3 ch2+
vertex SE31 n3 ch1-
vertex SE31 n3 ch2-
vertex SE32 n3 ch1-
vertex SE32 n3 ch2-
! Quark-squark-chargino [unit CKM matrix!]
vertex d SU1 ch1+
vertex d SU1 ch2+
vertex d SU2 ch1+
vertex d SU2 ch2+
vertex D su1 ch1-
vertex D su1 ch2-
vertex D su2 ch1-
vertex D su2 ch2-
vertex sd1 U ch1+
vertex sd1 U ch2+
vertex sd2 U ch1+
vertex sd2 U ch2+
vertex SD1 u ch1-
vertex SD1 u ch2-
vertex SD2 u ch1-
vertex SD2 u ch2-
vertex s SC1 ch1+
vertex s SC1 ch2+
vertex s SC2 ch1+
vertex s SC2 ch2+
vertex S sc1 ch1-
vertex S sc1 ch2-
vertex S sc2 ch1-
vertex S sc2 ch2-
vertex ss1 C ch1+
vertex ss1 C ch2+
vertex ss2 C ch1+
vertex ss2 C ch2+
vertex SS1 c ch1-
vertex SS1 c ch2-
vertex SS2 c ch1-
vertex SS2 c ch2-
vertex b ST1 ch1+
vertex b ST1 ch2+
vertex b ST2 ch1+
vertex b ST2 ch2+
vertex B st1 ch1-
vertex B st1 ch2-
vertex B st2 ch1-
vertex B st2 ch2-
vertex sb1 T ch1+
vertex sb1 T ch2+
vertex sb2 T ch1+
vertex sb2 T ch2+
vertex SB1 t ch1-
vertex SB1 t ch2-
vertex SB2 t ch1-
vertex SB2 t ch2-
