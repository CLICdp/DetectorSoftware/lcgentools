! $Id: parameters.UED.omega.f90,v 1.1 2006/06/16 13:31:48 kilian Exp $
!
! Copyright (C) 2000-2008 by Wolfgang Kilian <wolfgang.kilian@desy.de>,
! Thorsten Ohl <ohl@physik.uni-wuerzburg.de>, and Juergen Reuter 
! <juergen.reuter@desy.de>
!
! WHIZARD is free software; you can redistribute it and/or modify it
! under the terms of the GNU General Public License as published by
! the Free Software Foundation; either version 2, or (at your option)
! any later version.
!
! WHIZARD is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.  

module omega_parameters_whizard
  use omega_kinds !NODEP!
  use omega_constants !NODEP!
  use parameters
  implicit none
  private
  real(kind=omega_prec), dimension(39), save, public :: mass = 0, width = 0
  complex(kind=omega_prec), dimension(2), save, public :: &
       gnclep = 0, gncneu = 0, gncup = 0, gncdwn = 0
  real(kind=omega_prec), save, public :: &
       alpha = 1.0_omega_prec / 137.0359895_omega_prec, &
       sin2thw = 0.23124_omega_prec
  complex(kind=omega_prec), save, public :: &
       qlep = 0, qup = 0, qdwn = 0, gcc = 0, qw = 0, &
       gzww = 0, gwww = 0, ghww = 0, ghhww = 0, ghzz = 0, ghhzz = 0, &
       ghbb = 0, ghtt = 0, ghcc = 0, ghtautau = 0, gh3 = 0, gh4 = 0, &
       ghgaga = 0, ghgaz = 0, &
       iqw = 0, igzww = 0, igwww = 0, &
       gw4 = 0, gzzww = 0, gazww = 0, gaaww = 0, &
       gs = 0, igs = 0
  real(kind=omega_prec), save, public :: xia = 1, xi0 = 1, xipm = 1
  complex(kind=omega_prec), save, public :: ggrav = 0
  complex(kind=omega_prec), save, public :: &
       ghwhw = 0, ghwhwh = 0, ghahah = 0, ghzhz = 0, ghzhah = 0, &
       ghahz = 0, ghaa = 0, ghgg = 0
  integer, parameter, public :: &
       n0 = 5, nloop = 2 
  real(kind=omega_prec), parameter :: &
       acc = 1.e-12_omega_prec
  real(kind=omega_prec), parameter :: &
       asmz = 0.118_omega_prec
  complex(kind=omega_prec), save, public :: iqwk
  type(parameter_set) :: par
  public :: import_from_whizard
contains
  function faux (x) result (y)
    real(kind=omega_prec) :: x
    complex(kind=omega_prec) :: y
    if (1 <= x) then
       y = asin(sqrt(1/x))**2
    else
       y = - 1/4.0_omega_prec * (log((1 + sqrt(1 - x))/ &
            (1 - sqrt(1 - x))) - cmplx (0.0_omega_prec, PI))**2
    end if
  end function faux

  function fonehalf (x) result (y)
    real(kind=omega_prec), intent(in) :: x
    complex(kind=omega_prec) :: y
    if (x==0) then
       y = 0
    else
       y = - 2.0_omega_prec * x * (1 + (1 - x) * faux(x))
    end if
  end function fonehalf

  function fone (x) result  (y)
    real(kind=omega_prec), intent(in) :: x
    complex(kind=omega_prec) :: y
    if (x==0) then
       y = 2.0_omega_prec
    else
       y = 2.0_omega_prec + 3.0_omega_prec * x + &
            3.0_omega_prec * x * (2.0_omega_prec - x) * &
            faux(x)
    end if
  end function fone

  function gaux (x) result (y)
    real(kind=omega_prec), intent(in) :: x
    complex(kind=omega_prec) :: y
    if (1 <= x) then
       y = sqrt(x - 1) * asin(sqrt(1/x))
    else
       y = sqrt(1 - x) * (log((1 + sqrt(1 - x)) / &
            (1 - sqrt(1 - x))) - cmplx (0.0_omega_prec, PI)) / 2
    end if
  end function gaux

  function i1 (a,b) result (y)
    real(kind=omega_prec), intent(in) :: a,b
    complex(kind=omega_prec) :: y
    y = a*b/2.0_omega_prec/(a-b) + a**2 * b**2/2.0_omega_prec/(a-b)**2 * &
         (faux(a) - faux(b)) + &
         a**2 * b/(a-b)**2 * (gaux(a) - gaux(b))
  end function i1

  function i2 (a,b) result (y) 
    real(kind=omega_prec), intent(in) :: a,b
    complex(kind=omega_prec) :: y
    y = - a * b / 2.0_omega_prec / (a-b) * (faux(a) - faux(b)) 
  end function i2

  function b0 (nf) result (bnull)
    integer, intent(in) :: nf
    real(kind=omega_prec) :: bnull
    bnull = 33.0_omega_prec - 2.0_omega_prec * nf
  end function b0

  function b1 (nf) result (bone)
    integer, intent(in) :: nf
    real(kind=omega_prec) :: bone
    bone = 6.0_omega_prec * (153.0_omega_prec - 19.0_omega_prec * nf)/b0(nf)**2
  end function b1

  function aa (nf) result (aaa)
    integer, intent(in) :: nf
    real(kind=omega_prec) :: aaa
    aaa = 12.0_omega_prec * PI / b0(nf)
  end function aa

  function bb (nf) result (bbb)
    integer, intent(in) :: nf
    real(kind=omega_prec) :: bbb
    bbb = b1(nf) / aa(nf)
  end function bb

  subroutine import_from_whizard (par)
    type(parameter_set), intent(in) :: par
    real(kind=omega_prec) :: e, g, gp, sinthw, costhw, tanthw, e_em
    real(kind=omega_prec) :: qelep, qeup, qedwn, v
    real(kind=omega_prec) :: ttop, tbot, tc, ttau, tw
    real(kind=omega_prec) :: ltop, lbot, lc, ltau, lw
    !!! This corresponds to 1/alpha = 137.03598949333
    real(kind=omega_prec), parameter :: &
         alpha = 1.0_omega_prec/137.03598949333_omega_prec
    real(kind=omega_prec), parameter :: &
         asmz = 0.118_omega_prec
    e_em = sqrt(4.0_omega_prec * PI * alpha)
    mass(1:39) = 0
    width(1:39) = 0
    mass(3) = par%ms
    mass(4) = par%mc
    mass(5) = par%mb
    mass(6) = par%mtop
    width(6) = par%wtop
    mass(11) = par%me
    mass(13) = par%mmu
    mass(15) = par%mtau
    mass(23) = par%mZ
    width(23) = par%wZ
    mass(24) = par%mW
    width(24) = par%wW
    mass(25) = par%mH
    width(25) = par%wH
    mass(26) =  xi0 * mass(23)
    width(26) =  0
    mass(27) =  xipm * mass(24)
    width(27) =  0
    mass(39) = par%mgg
    width(39) = par%wgg
    ttop = 4.0_omega_prec * mass(6)**2 / mass(25)**2
    tbot = 4.0_omega_prec * mass(5)**2 / mass(25)**2
    tc   = 4.0_omega_prec * mass(4)**2 / mass(25)**2
    ttau = 4.0_omega_prec * mass(15)**2 / mass(25)**2
    tw   = 4.0_omega_prec * mass(24)**2 / mass(25)**2  
    ltop = 4.0_omega_prec * mass(6)**2 / mass(23)**2
    lbot = 4.0_omega_prec * mass(5)**2 / mass(23)**2  
    lc   = 4.0_omega_prec * mass(4)**2 / mass(23)**2
    ltau = 4.0_omega_prec * mass(15)**2 / mass(23)**2
    lw   = 4.0_omega_prec * mass(24)**2 / mass(23)**2
    v = 2 * mass(24) * par%sw / par%ee 
    e = par%ee
    sinthw = par%sw
    sin2thw = sinthw**2
    costhw = par%cw
    tanthw = sinthw/costhw
    qelep = - 1
    qeup = 2.0_omega_prec / 3.0_omega_prec
    qedwn = - 1.0_omega_prec / 3.0_omega_prec
    g = e / sinthw
    gp = e / costhw
    gcc = - g / 2 / sqrt (2.0_omega_prec)
    gncneu(1) = - g / 2 / costhw * ( + 0.5_omega_prec)
    gnclep(1) = - g / 2 / costhw * ( - 0.5_omega_prec - 2 * qelep * sin2thw)
    gncup(1)  = - g / 2 / costhw * ( + 0.5_omega_prec - 2 * qeup  * sin2thw)
    gncdwn(1) = - g / 2 / costhw * ( - 0.5_omega_prec - 2 * qedwn * sin2thw)
    gncneu(2) = - g / 2 / costhw * ( + 0.5_omega_prec)
    gnclep(2) = - g / 2 / costhw * ( - 0.5_omega_prec)
    gncup(2)  = - g / 2 / costhw * ( + 0.5_omega_prec)
    gncdwn(2) = - g / 2 / costhw * ( - 0.5_omega_prec)
    qlep = - e * qelep
    qup = - e * qeup
    qdwn = - e * qedwn
    qw = e
    iqw = (0,1)*qw
    gzww = g * costhw
    igzww = (0,1)*gzww
    gwww = g
    igwww = (0,1)*gwww
    gw4 = gwww**2
    gzzww = gzww**2
    gazww = gzww * qw
    gaaww = qw**2
    ghww = mass(24) * g
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!! This is for the old SM3:
    !!! ghhww = (0,1) * g / Sqrt(2.0_omega_prec)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ghhww = g**2 / 2.0_omega_prec
    ghzz = mass(23) * g / costhw
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!! This is for the old SM3:
    !!! ghhzz = (0,1) * g / costhw / Sqrt(2.0_omega_prec)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ghhzz = g**2 / 2.0_omega_prec / costhw**2
    ghtt = - mass(6) / v
    ghbb = - mass(5) / v
    ghcc = - mass(4) / v
    ghtautau = - mass(15) / v
    gh3 = - 3 * mass(25)**2 / v
    !!! gh4 = mass(25) / v !!! Old SM3
    gh4 = - 3 * mass(25)**2 / v**2
    !!! Color flow basis, divide by sqrt(2)
    gs = sqrt(2.0_omega_prec*PI*par%alphas)
    igs = (0.0_omega_prec, 1.0_omega_prec) * gs    
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!! Higgs anomaly couplings
    !!! SM LO loop factor (top,bottom,W)
    ghgaga = alpha / v / 2.0_omega_prec / PI * &
         Abs(( 4.0_omega_prec * (fonehalf(ttop) + fonehalf(tc)) &
         + fonehalf(tbot)) / 3.0_omega_prec + fonehalf(ttau) + fone(tw)) &
         * sqrt(par%khgaga)
    !!! asymptotic limit:
    !!! ghgaga = (par%ee)**2 / v / &
    !!!      9.0_omega_prec / pi**2
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!! SM LO loop factor (only top and W)
    ghgaz = e * e_em / 8.0_omega_prec / PI**2 / v * Abs( &
          ( - 2.0_omega_prec + &
          16.0_omega_prec/3.0_omega_prec * sin2thw) * &
          (i1(ttop,ltop) - i2(ttop,ltop)) / costhw & 
          + ( - 1.0_omega_prec + &
          4.0_omega_prec/3.0_omega_prec * sin2thw) & 
          * (i1(tbot,lbot) - i2(tbot,lbot)) / costhw &
           - costhw * ( 4.0_omega_prec * (3.0_omega_prec - tanthw**2) * &
           i2(tw,lw) + ((1 + 2.0_omega_prec/tw) * tanthw**2 - ( &
           5.0_omega_prec + 2.0_omega_prec/tw)) * i1(tw,lw)) &
          )/sinthw * sqrt(par%khgaz)
    !!! SM LO order loop factor with 
    !!! N(N)LO K factor = 2.1 (only top)
    !!! Limit of infinite top quark mass:
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ghgg = par%alphas / v / 4.0_omega_prec / PI * &
         Abs(fonehalf(ttop) + fonehalf(tbot) + fonehalf(tc)) * &
         sqrt(par%khgg)
    !!! ghgg   = par%alphas / 3.0_omega_prec &
    !!!      / v / pi * 2.1_omega_prec
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!!!!!   GRAVITATIONAL COUPLING      !!!!!!!!!!!!!
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ggrav = par%ggrav
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!!!!!   SPECIAL UED COUPLINGS       !!!!!!!!!!!!!
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    iqwk = g*gp / sqrt(g**2 + gp**2)
  end subroutine import_from_whizard
end module omega_parameters_whizard
