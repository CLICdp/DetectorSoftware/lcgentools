! $Id: parameters.Littlest_Eta.omega.f90,v 1.1 2006/06/16 13:31:48 kilian Exp $
!
! Copyright (C) 2000-2008 by Wolfgang Kilian <wolfgang.kilian@desy.de>,
! Thorsten Ohl <ohl@physik.uni-wuerzburg.de>, and Juergen Reuter 
! <juergen.reuter@desy.de>
!
! WHIZARD is free software; you can redistribute it and/or modify it
! under the terms of the GNU General Public License as published by
! the Free Software Foundation; either version 2, or (at your option)
! any later version.
!
! WHIZARD is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.  
  
module omega_parameters_whizard
  use omega_kinds !NODEP!
  use omega_constants !NODEP!
  use parameters
  implicit none
  private
  real(kind=omega_prec), dimension(45), save, public :: mass = 0, width = 0
  real(kind=omega_prec), parameter, public :: GeV = 1.0_omega_prec
  real(kind=omega_prec), parameter, public :: MeV = GeV / 1000
  real(kind=omega_prec), parameter, public :: keV = MeV / 1000
  real(kind=omega_prec), parameter, public :: TeV = GeV * 1000
  real(kind=omega_prec), save, public :: &
       alpha = 1.0_omega_prec / 137.0359895_omega_prec, &
       sin2thw = 0.23124_omega_prec
  complex(kind=omega_prec), save, private :: vev
  complex(kind=omega_prec), save, public :: &
       qlep = 0, qup = 0, qdwn = 0, gcc = 0, qw = 0, &
       gzww = 0, gwww = 0, ghww = 0, ghhww = 0, ghzz = 0, ghhzz = 0, &
       ghbb = 0, ghtt = 0, ghcc = 0, ghtautau = 0, gh3 = 0, gh4 = 0, &
       ghgaga = 0, ghgaz = 0, &
       iqw = 0, igzww = 0, igwww = 0, &
       gw4 = 0, gzzww = 0, gazww = 0, gaaww = 0, &
       gs = 0, igs = 0
  complex(kind=omega_prec), save, public :: &
       sinckm12 = 0, sinckm13 = 0, sinckm23 = 0, &
       cosckm12 = 0, cosckm13 = 0, cosckm23 = 0
  complex(kind=omega_prec), save, public :: &
       vckm_11 = 0, vckm_12 = 0, vckm_13 = 0, vckm_21 = 0, &
       vckm_22 = 0, vckm_23 = 0, vckm_31 = 0, vckm_32 = 0, vckm_33 = 0
  complex(kind=omega_prec), save, public :: &
       gccq11 = 0, gccq12 = 0, gccq13 = 0, gccq21 = 0, &
       gccq22 = 0, gccq23 = 0, gccq31 = 0, gccq32 = 0, gccq33 = 0       
  real(kind=omega_prec), save, public :: &
       g1a = 1, g1z = 1, kappaa = 1, kappaz = 1, lambdaa = 0, lambdaz = 0, &
       g4a = 0, g4z = 0, g5a = 0, g5z = 0, &
       kappa5a = 0, kappa5z = 0, lambda5a = 0, lambda5z = 0, &
       alpha4 = 0, alpha5 = 0, tau4 = 0, tau5 = 0
  real(kind=omega_prec), save, public :: xia = 1, xi0 = 1, xipm = 1
  complex(kind=omega_prec), dimension(2), save, public :: &
       gnclep = 0, gncneu = 0, gncup = 0, gncdwn = 0
  complex(kind=omega_prec), save, public :: &
       fudge_o1 = 1, fudge_o2 = 1, fudge_o3 = 1, fudge_o4 = 1
  complex(kind=omega_prec), save, public :: &
       ghmumu = 0
  complex(kind=omega_prec), save, public :: &
       gh0ww = 0, gh0zz = 0, &
       gh0tt = 0, gh0bb = 0, gh0cc = 0, gh0tautau = 0, gh0mumu = 0, &
       iga0tt = 0, iga0bb = 0, iga0cc = 0, iga0tautau = 0, iga0mumu = 0, &
       gahh = 0, gzhh = 0, igzha = 0, igzh0a = 0
  complex(kind=omega_prec), dimension(2), save, public :: &
       ghptb = 0, ghpcs = 0, ghptaunu = 0, ghpmunu = 0
  !!! Additional Littlest Higgs parameters 
  complex(kind=omega_prec), save, public :: &
       ghwhw = 0, ghwhwh = 0, ghahah = 0, ghzhz = 0, ghzhah = 0, &
       ghahz = 0, ghaa = 0, ghgg = 0, geaa = 0, geaz = 0, gegg = 0, &
       gebb = 0
  complex(kind=omega_prec), save, public :: & 
       gpsiww = 0, gpsiwhw = 0, gpsizz = 0, gpsizhzh = 0, & 
       gpsizhz = 0, gpsizah = 0, gpsizhah = 0, gpsiahah = 0, & 
       gpsizw = 0, gpsizwh = 0, gpsiahw = 0, gpsiahwh = 0, &
       gpsizhw = 0, gpsizhwh = 0, gpsippww = 0, gpsippwhw = 0, &
       gpsippwhwh = 0, gpsihw = 0, gpsihwh = 0, gpsi0w = 0, & 
       gpsi0wh = 0, gpsi1w = 0, gpsi1wh = 0, gpsippw = 0, &
       gpsippwh = 0
  complex(kind=omega_prec), save, public :: & 
       gpsihah = 0, gpsi0ah = 0, gahpsip = 0, &
       gpsi1hz = 0, gpsi1hzh = 0, gpsi01z = 0, gpsi01zh = 0, &
       gzpsip = 0, gzpsipp = 0, gzhpsipp = 0
  complex(kind=omega_prec), save, public :: &
       ghhaa = 0, ghhwhw = 0, ghhzhz = 0, ghhahz = 0, ghhzhah = 0, &
       ghpsi0ww = 0, ghpsi0whw = 0, ghpsi0zz = 0, ghpsi0zhzh = 0, & 
       ghpsi0zhz = 0, ghpsi0ahah = 0, ghpsi0zah = 0, ghpsi0zhah = 0
  complex(kind=omega_prec), save, public :: &
      ghpsipwa = 0, ghpsipwha = 0, ghpsipwz = 0, ghpsiwhz = 0, &
      ghpsipwah = 0, ghpsipwhah = 0, ghpsipwzh = 0, ghpsipwhzh = 0, & 
      ghpsippww = 0, ghpsippwhwh = 0, ghpsippwhw = 0, gpsi00zh = 0, &
      gpsi00ah = 0, gpsi00zhah = 0, gpsi0pwa = 0, gpsi0pwha = 0, & 
      gpsi0pwz = 0, gpsi0pwhz = 0, gpsi0pwah = 0, gpsi0pwhah = 0, & 
      gpsi0pwzh = 0, gpsi0pwhzh = 0, gpsi0ppww = 0, gpsi0ppwhwh = 0, &
      gpsi0ppwhw = 0, i_gpsi0pwa = 0, i_gpsi0pwha = 0, & 
      i_gpsi0pwz = 0, i_gpsi0pwhz = 0, i_gpsi0pwah = 0, i_gpsi0pwhah = 0, & 
      i_gpsi0pwzh = 0, i_gpsi0pwhzh = 0, i_gpsi0ppww = 0, i_gpsi0ppwhwh = 0, &
      i_gpsi0ppwhw = 0
  complex(kind=omega_prec), save, public :: & 
      gpsippzz = 0, gpsippzhzh = 0, gpsippaz = 0, gpsippaah = 0, & 
      gpsippzah = 0, gpsippwa = 0, gpsippwha = 0, gpsippwz = 0, &
      gpsippwhz = 0, gpsippwah = 0, gpsippwhah = 0, gpsippwzh = 0, &
      gpsippwhzh = 0, gpsicczz = 0, gpsiccaz = 0, gpsiccaah = 0, &
      gpsicczzh = 0, gpsiccazh = 0, gpsicczah = 0
  complex(kind=omega_prec), save, public :: &
      igahww = 0, igzhww = 0, igzwhw = 0, igahwhwh = 0, igzhwhwh = 0, &
      igahwhw = 0
  complex(kind=omega_prec), save, public :: &
      gwh4 = 0, gwhwhww = 0, gwhwww = 0, gwh3w = 0, gwwaah = 0, &
      gwwazh = 0, gwwzzh = 0, gwwzah = 0, gwhwhaah = 0, gwhwhazh = 0, &
      gwhwhzzh = 0, gwhwhzah = 0, gwwzhah = 0, gwhwhzhah = 0, &
      gwhwzz = 0, gwhwaz = 0, gwhwaah = 0, gwhwzah = 0, gwhwzhzh = 0, &
      gwhwzhah = 0, gwhwazh = 0, gwhwzzh = 0
  complex(kind=omega_prec), save, public :: &
      qzup = 0, gcch = 0, gcctop = 0, gccw = 0, gccwh = 0, &
      gnch = 0, gztht = 0, gzhtht = 0, gah = 0
  complex(kind=omega_prec), dimension(2), save, public :: &
      gnchup = 0, gnchdwn = 0, gnchneu = 0, gnchlep = 0, gahtt = 0, &
      gahthth = 0, ghtht = 0, gpsipq2 = 0, gpsipq3 = 0, &
      ghhtht = 0
  complex(kind=omega_prec), save, public :: &
      gahtht = 0, ghthth = 0, &
      gpsi0tt = 0, gpsi0bb = 0, gpsi0cc = 0, gpsi0tautau = 0, &
      gpsipl3 = 0, gpsi0tth = 0, gpsi1tth = 0, gpsipbth = 0, &
      ghhtt = 0, ghhthth = 0
  integer, parameter, public :: &
       n0 = 5, nloop = 2 
  real(kind=omega_prec), parameter :: &
       acc = 1.e-12_omega_prec
  real(kind=omega_prec), parameter :: &
       asmz = 0.118_omega_prec
  type(parameter_set) :: par
  public :: import_from_whizard
contains
  function faux (x) result (y)
    real(kind=omega_prec) :: x
    complex(kind=omega_prec) :: y
    if (1 <= x) then
       y = asin(sqrt(1/x))**2
    else
       y = - 1/4.0_omega_prec * (log((1 + sqrt(1 - x))/ &
            (1 - sqrt(1 - x))) - cmplx (0.0_omega_prec, PI))**2
    end if
  end function faux

  function fonehalf (x) result (y)
    real(kind=omega_prec), intent(in) :: x
    complex(kind=omega_prec) :: y
    if (x==0) then
       y = 0
    else
       y = - 2.0_omega_prec * x * (1 + (1 - x) * faux(x))
    end if
  end function fonehalf

  function fone (x) result  (y)
    real(kind=omega_prec), intent(in) :: x
    complex(kind=omega_prec) :: y
    if (x==0) then
       y = 2.0_omega_prec
    else
       y = 2.0_omega_prec + 3.0_omega_prec * x + &
            3.0_omega_prec * x * (2.0_omega_prec - x) * &
            faux(x)
    end if
  end function fone

  function gaux (x) result (y)
    real(kind=omega_prec), intent(in) :: x
    complex(kind=omega_prec) :: y
    if (1 <= x) then
       y = sqrt(x - 1) * asin(sqrt(1/x))
    else
       y = sqrt(1 - x) * (log((1 + sqrt(1 - x)) / &
            (1 - sqrt(1 - x))) - cmplx (0.0_omega_prec, PI)) / 2
    end if
  end function gaux

  function i1 (a,b) result (y)
    real(kind=omega_prec), intent(in) :: a,b
    complex(kind=omega_prec) :: y
    y = a*b/2.0_omega_prec/(a-b) + a**2 * b**2/2.0_omega_prec/(a-b)**2 * &
         (faux(a) - faux(b)) + &
         a**2 * b/(a-b)**2 * (gaux(a) - gaux(b))
  end function i1

  function i2 (a,b) result (y) 
    real(kind=omega_prec), intent(in) :: a,b
    complex(kind=omega_prec) :: y
    y = - a * b / 2.0_omega_prec / (a-b) * (faux(a) - faux(b)) 
  end function i2

  function b0 (nf) result (bnull)
    integer, intent(in) :: nf
    real(kind=omega_prec) :: bnull
    bnull = 33.0_omega_prec - 2.0_omega_prec * nf
  end function b0

  function b1 (nf) result (bone)
    integer, intent(in) :: nf
    real(kind=omega_prec) :: bone
    bone = 6.0_omega_prec * (153.0_omega_prec - 19.0_omega_prec * nf)/b0(nf)**2
  end function b1

  function aa (nf) result (aaa)
    integer, intent(in) :: nf
    real(kind=omega_prec) :: aaa
    aaa = 12.0_omega_prec * PI / b0(nf)
  end function aa

  function bb (nf) result (bbb)
    integer, intent(in) :: nf
    real(kind=omega_prec) :: bbb
    bbb = b1(nf) / aa(nf)
  end function bb

  subroutine import_from_whizard (par)
    type(parameter_set), intent(in) :: par
    complex(kind=omega_prec), parameter :: &
         imago = (0.0_omega_prec, 1.0_omega_prec)
    real(kind=omega_prec) :: e, g, gp, sinthw, costhw, tanthw, e_em
    real(kind=omega_prec) :: qelep, qeup, qedwn, v, vevp
    real(kind=omega_prec) :: ttop, tbot, tc, ttau, tw
    real(kind=omega_prec) :: ltop, lbot, lc, ltau, lw
    real(kind=omega_prec) :: sint, sintp, sin2t, sin2tp, &
         cost, costp, cos2t, cos2tp
    real(kind=omega_prec) :: spsip, cpsip, spsi1, cpsi1, spsi0, cpsi0
    real(kind=omega_prec) :: t_fac, tp_fac, ttp_fac, c4s4
    real(kind=omega_prec) :: xzbp, xzwp, xh, xlam
    real(kind=omega_prec) :: f, lam1, lam2
    real(kind=omega_prec) :: ye, yu, gah
    !!! This corresponds to 1/alpha = 137.03598949333
    real(kind=omega_prec), parameter :: &
         alpha = 1.0_omega_prec/137.03598949333_omega_prec
    real(kind=omega_prec), parameter :: &
         asmz = 0.118_omega_prec
    real(kind=omega_prec), parameter :: & 
         one = 1.0_omega_prec, two = 2.0_omega_prec, three = 3.0_omega_prec, &
         four = 4.0_omega_prec, five = 5.0_omega_prec
    e_em = sqrt(four * PI * alpha)
    mass(1:38) = 0
    width(1:38) = 0
    mass(3) = par%ms
    mass(4) = par%mc
    mass(5) = par%mb
    mass(6) = par%mtop
    width(6) = par%wtop
    mass(8) = par%mtoph
    width(8) = par%wtoph
    mass(11) = par%me
    mass(13) = par%mmu
    mass(15) = par%mtau
    mass(23) = par%mZ
    width(23) = par%wZ
    mass(24) = par%mW
    width(24) = par%wW
    mass(25) = par%mH
    width(25) = par%wH
    mass(26) =  xi0 * mass(23)
    width(26) =  0
    mass(27) =  xipm * mass(24)
    width(27) =  0
    mass(32) = par%mAH
    width(32) = par%wAH
    mass(33) = par%mZH
    width(33) = par%wZH
    mass(34) = par%mWH
    width(34) = par%wWH
    mass(35) = par%mpsi0
    width(35) = par%wpsi0
    mass(36) = par%mpsi1
    width(36) = par%wpsi1
    mass(37) = par%mpsip
    width(37) = par%wpsip
    mass(38) = par%mpsipp
    width(38) = par%wpsipp
    !!! This choice is responsible for anomaly cancellation:
    ye = - two / five
    yu = three / five
    ttop = four * mass(6)**2 / mass(25)**2
    tbot = four * mass(5)**2 / mass(25)**2
    tc   = four * mass(4)**2 / mass(25)**2
    ttau = four * mass(15)**2 / mass(25)**2
    tw   = four * mass(24)**2 / mass(25)**2  
    ltop = four * mass(6)**2 / mass(23)**2
    lbot = four * mass(5)**2 / mass(23)**2  
    lc   = four * mass(4)**2 / mass(23)**2
    ltau = four * mass(15)**2 / mass(23)**2
    lw   = four * mass(24)**2 / mass(23)**2
    v = 2 * mass(24) * par%sw / par%ee 
    vevp = par%vp
    f = par%f
    e = par%ee
    sinthw = par%sw
    sin2thw = sinthw**2
    costhw = par%cw
    tanthw = sinthw/costhw
    qelep = - one
    qeup = two / three
    qedwn = - one / three
    g = e / sinthw
    gp = e / costhw
    !!! In principle, we should allow here for deviations from 
    !!! the SM values 
    gcc = - g / 2 / sqrt (2.0_double)
    gncneu(1) = - g / two / costhw * ( + 0.5_double)
    gnclep(1) = - g / two / costhw * ( - 0.5_double - 2 * qelep * sin2thw)
    gncup(1)  = - g / two / costhw * ( + 0.5_double - 2 * qeup  * sin2thw)
    gncdwn(1) = - g / two / costhw * ( - 0.5_double - 2 * qedwn * sin2thw)
    gncneu(2) = - g / two / costhw * ( + 0.5_double)
    gnclep(2) = - g / two / costhw * ( - 0.5_double)
    gncup(2)  = - g / two / costhw * ( + 0.5_double)
    gncdwn(2) = - g / two / costhw * ( - 0.5_double)
    qlep = - e * qelep
    qup = - e * qeup
    qdwn = - e * qedwn
    qw = e
    iqw = imago*qw
    gzww = g * costhw
    igzww = imago*gzww
    gwww = g
    igwww = imago*gwww
    gw4 = gwww**2
    gzzww = gzww**2
    gazww = gzww * qw
    gaaww = qw**2
    ghww = mass(24) * g * (one - v**2/three/f**2 + v**2/f**2 * &
         (cos2t - sin2t)**2/two - spsi0**2/two - two * sqrt(two) * &
         spsi0 * vevp/v)
    !!! SM value !!! ghww = mass(24) * g    
    ghhww = g**2 / two
    ghzz = mass(23) * g / costhw * (one - v**2/three/f**2 - spsi0**2/two &
         + four*sqrt(two)*vevp/v  - v**2/f**2/two * ((cos2t - sin2t)**2 &
         + five * (cos2tp - sin2tp)**2))
    !!! SM value !!! ghzz = mass(23) * g / costhw
    ghhzz = g**2 / two / costhw**2
    !!! ghtt = - mass(6) / v !!! Will be corrected below
    ghbb = - mass(5) / v
    ghcc = - mass(4) / v
    ghtautau = - mass(15) / v
    gh3 = - 3 * mass(25)**2 / v
    !!! gh4 = mass(25) / v !!! Old SM3
    gh4 = - 3 * mass(25)**2 / v**2
    !!! Color flow basis, divide by sqrt(2)
    gs = sqrt(2.0_omega_prec*PI*par%alphas)
    igs = (0.0_omega_prec, 1.0_omega_prec) * gs    
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!! Higgs anomaly couplings
    !!! SM LO loop factor (top,bottom,W)
    ghgaga = alpha / v / two / PI * &
         Abs(( four * (fonehalf(ttop) + fonehalf(tc)) &
         + fonehalf(tbot)) / 3.0_omega_prec + fonehalf(ttau) + fone(tw)) &
         * sqrt(par%khgaga)
    !!! asymptotic limit:
    !!! ghgaga = (par%ee)**2 / v / &
    !!!      9.0_omega_prec / pi**2
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!! SM LO loop factor (only top and W)
    ghgaz = e * e_em / 8.0_omega_prec / PI**2 / v * Abs( &
          ( - two + &
          16.0_omega_prec/3.0_omega_prec * sin2thw) * &
          (i1(ttop,ltop) - i2(ttop,ltop)) / costhw & 
          + ( - 1.0_omega_prec + &
          four/3.0_omega_prec * sin2thw) & 
          * (i1(tbot,lbot) - i2(tbot,lbot)) / costhw &
           - costhw * ( four * (3.0_omega_prec - tanthw**2) * &
           i2(tw,lw) + ((1 + two/tw) * tanthw**2 - ( &
           five + two/tw)) * i1(tw,lw)) &
          )/sinthw * sqrt(par%khgaz)
    !!! SM LO order loop factor with 
    !!! N(N)LO K factor = 2.1 (only top)
    !!! Limit of infinite top quark mass:
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!! We use par%gg because of sqrt(2) above
    ghgg = par%alphas / v / 4.0_omega_prec / PI * &
         Abs(fonehalf(ttop) + fonehalf(tbot) + fonehalf(tc)) * &
         sqrt(par%khgg)
    !!! ghgg   = par%alphas / 3.0_omega_prec &
    !!!      / v / pi * 2.1_omega_prec
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!! Littlest Higgs special values
    sint = par%st
    sintp = par%stp
    sin2t = sint**2
    sin2tp = sintp**2
    cos2t = 1.0_omega_prec - sin2t
    cos2tp =  1.0_omega_prec - sin2tp
    cost = sqrt(cos2t)
    costp = sqrt(cos2tp)
    !!!
    lam1 = par%lam1
    lam2 = (mass(8)/f)**2 * (mass(6)/v) / lam1    
    xlam = lam1**2 / (lam1**2 + lam2**2)
    !!!
    xh = five*g*gp * sint*cost*sintp*costp * (cos2t*sin2tp + &
         sin2t*cos2tp) / (five*g**2*sin2tp*cos2tp - &
         gp**2*sin2t*cos2t) / two
    xzbp = - five/two*sintp*costp*(cos2tp-sin2tp)/sinthw
    xzwp = -sint*cost*(cos2t-sin2t)/two/costhw
    !!!
    spsi1 = sqrt(8.0_omega_prec)*vevp/sqrt(v**2+8.0_omega_prec*vevp**2)
    cpsi1 = v/sqrt(v**2+8.0_omega_prec*vevp**2)
    spsip = two*vevp/sqrt(v**2+four*vevp**2)
    cpsip = v/sqrt(v**2+four*vevp**2)
    spsi0 = sqrt(8.0_omega_prec)*vevp/v
    !!!
    t_fac = (cos2t - sin2t) / two / sint / cost
    tp_fac = (cos2tp - sin2tp) / two / sintp / costp
    ttp_fac = (cos2t * sin2tp + sin2t * cos2tp) / sint / sintp &
         / cost / costp
    c4s4 = (cos2t**2 + sin2t**2) / two / cos2t / sin2t
    ghwhwh = - g**2 * v / two
    ghahah = - gp**2 * v / two
    ghwhw = t_fac * ghwhwh
    ghzhz = ghwhw / costhw
    ghzhah = - g * gp * v * ttp_fac / four
    ghahz = - g * gp * tp_fac * v / costhw / two
    !!!
    gpsiww = - g**2 * (spsi0*v - sqrt(8.0_omega_prec)*vevp)/two
    gpsiwhw = - gpsiww * t_fac
    gpsizz = - g**2 * (spsi0*v - four*sqrt(two)*vevp) / two / costhw**2
    gpsizhzh = g**2 * (spsi0*v + sqrt(two)*vevp*t_fac) / two
    gpsizhz = - gpsizz * t_fac * costhw
    gpsizah = - gpsizz * tp_fac * costhw * gp / g
    gpsizhah = g*gp/four/sint/cost/sintp/costp * (v*spsi0 * &
         (cos2t*sin2tp + sin2t*cos2tp) + sqrt(8.0_omega_prec) * vevp * &
         (cos2t - sin2t) * (cos2tp - sin2tp))  
    gpsiahah = gp**2 * (v*spsi0 + tp_fac * sqrt(two) * vevp) / two
    !!!
    gpsizw = - g**2 / costhw * vevp 
    gpsizwh = - gpsizw * t_fac
    gpsiahw = - g*gp * tp_fac * (v*spsip - four*vevp) / two
    gpsiahwh = - g*gp * vevp * (cos2t*cos2tp + sin2t*sin2tp) / two / &
         sint / cost / sintp / costp 
    gpsizhw = g**2 * vevp * t_fac
    gpsizhwh = - g**2 * vevp * c4s4
    !!!
    gpsippww = two * g**2 * vevp
    gpsippwhw = - gpsippww * t_fac
    gpsippwhwh = gpsippww * c4s4
    !!!
    gpsihw = - g/two * (sqrt(two) * spsi0 - spsip)
    gpsihwh = - gpsihw * t_fac
    gpsi0w = - g/sqrt(two)
    gpsi0wh = - gpsi0w * t_fac
    gpsi1w = imago * gpsi0w
    gpsi1wh = imago * gpsi0wh
    gpsippw = - g
    gpsippwh = g * t_fac
    !!!
    gpsihah = imago / two * gp * tp_fac * (spsi1 - two * spsi0)
    gpsi0ah = - imago * gp * tp_fac
    gahpsip = gp * tp_fac
    gpsi1hz = - imago * g / 2 / costhw * (spsi1 - two * spsi0)
    gpsi1hzh = imago * g / two * t_fac * (spsi1 - two * spsi0)
    gpsi01z = imago * g / costhw
    gpsi01zh = - imago * g * t_fac
    gzpsip = g / costhw * sin2thw
    gzpsipp = - g / costhw * (one - two * sin2thw)
    gzhpsipp = g * t_fac
    !!!
    ghhaa = - gp**2 / two
    ghhwhw = - g**2 / two * t_fac 
    ghhzhz = - g**2 / two / costhw * t_fac 
    ghhahz = - g*gp / 2 / costhw * tp_fac 
    ghhzhah = - g*gp / four * ttp_fac
    !!!
    ghpsi0ww = g**2 / two * spsi0
    ghpsi0whw =  - g**2 / two * spsi0 * t_fac
    ghpsi0zz = three * g**2 * spsi0 / two / costhw**2
    ghpsi0zhzh = g**2 / two * (one + t_fac**2) * spsi0 
    ghpsi0zhz =  - three * g**2 * t_fac / two / costhw * spsi0
    ghpsi0ahah = gp**2 / two * (one + tp_fac**2) * spsi0
    ghpsi0zah =  - three * g*gp * spsi0 * tp_fac / two / costhw
    ghpsi0zhah = g*gp * spsi0 / four * (ttp_fac + two**3 * t_fac * tp_fac)
    !!!
    ghpsipwa = - e*g * (spsip - sqrt(two) * spsi0) / two 
    ghpsipwha = - ghpsipwa * t_fac
    ghpsipwz = g**2 / costhw / two * (spsip * sin2thw - sqrt(two) * spsi0 * &
         (one + sin2thw))
    ghpsiwhz = - ghpsipwz * t_fac
    ghpsipwah = - g*gp * (spsip - two*sqrt(two)*spsi0) * tp_fac / two
    ghpsipwhah = - g*gp * (ttp_fac*spsip + & 
         four*sqrt(two)*t_fac*tp_fac*spsi0) / four
    ghpsipwzh = g**2 * t_fac * spsi0 / two 
    ghpsipwhzh = - g**2 * c4s4 / two * spsi0
    ghpsippww = sqrt(two) * g**2 * spsi0
    ghpsippwhwh = sqrt(two) * g**2 * c4s4 * spsi0
    ghpsippwhw = - sqrt(two) * g**2 * t_fac * spsi0
    gpsi00zh =  two * g**2 * t_fac**2
    gpsi00ah = two * gp**2 * tp_fac**2
    gpsi00zhah = two * g*gp * t_fac * tp_fac
    !!!
    gpsi0pwa = - e * g / sqrt(two)
    gpsi0pwha = - gpsi0pwa * t_fac 
    gpsi0pwz = - g**2 * (one + sin2thw) / costhw / sqrt(two)
    gpsi0pwhz = - gpsi0pwz * t_fac
    gpsi0pwah = sqrt(two) * g * gp * tp_fac 
    gpsi0pwhah = - gpsi0pwah * t_fac
    gpsi0pwzh = g**2 * t_fac / sqrt(two)
    gpsi0pwhzh = - g**2 * c4s4 / sqrt(two)
    gpsi0ppww = sqrt(two) * g**2
    gpsi0ppwhwh = gpsi0ppww * c4s4
    gpsi0ppwhw = - gpsi0ppww * t_fac
    i_gpsi0pwa = imago * gpsi0pwa
    i_gpsi0pwha = imago * gpsi0pwha
    i_gpsi0pwz = imago * gpsi0pwz
    i_gpsi0pwhz = imago * gpsi0pwhz
    i_gpsi0pwah = imago * gpsi0pwah
    i_gpsi0pwhah = imago * gpsi0pwhah
    i_gpsi0pwzh = imago * gpsi0pwzh
    i_gpsi0pwhzh = imago * gpsi0pwhzh
    i_gpsi0ppww = imago * gpsi0ppww
    i_gpsi0ppwhwh = imago * gpsi0ppwhwh
    i_gpsi0ppwhw = imago * gpsi0ppwhw
    !!!
    gpsippzz = two * g**2 / costhw**2 * sin2thw**2
    gpsippzhzh = - two * g**2 / four / sin2t / cos2t
    gpsippaz = - two * e * g / costhw * sin2thw
    gpsippaah = - two * e * gp * tp_fac
    gpsippzah = two * g * gp * tp_fac * sin2thw / costhw 
    !!!
    gpsippwa = three * e * g
    gpsippwha = - gpsippwa * t_fac
    gpsippwz = g**2 * (one - three * sin2thw) / costhw
    gpsippwhz = - gpsippwz * t_fac
    gpsippwah = two * g * gp * tp_fac
    gpsippwhah = - gpsippwah * t_fac
    gpsippwzh = - g**2 * t_fac / sqrt(two)
    gpsippwhzh = g**2 * c4s4 / sqrt(two)
    !!!
    gpsicczz = g**2 * (one - two * sin2thw)**2 / costhw**2 
    gpsiccaz = four * e * g * (one - two * sin2thw) / costhw
    gpsiccaah = - four * e * gp * tp_fac
    gpsicczzh = two * g**2 * t_fac * (one - two * sin2thw) / costhw
    gpsiccazh = four * e * g * t_fac
    gpsicczah = - two * g * gp * tp_fac * (one - two * sin2thw) / costhw
    !!! heavy triple gauge couplings
    igahww = - imago * g * costhw * xzbp * v**2 / f**2
    igzhww = - imago * g * (costhw*xzwp + sint*cost*(cos2t-sin2t)) * v**2/f**2
    igzwhw = imago * g * xzwp * v**2 / f**2
    igahwhwh = - imago * g * (two*t_fac*xh + costhw*xzbp) * v**2 / f**2
    igzhwhwh = - imago * g * two * t_fac
    igahwhw = imago * g * xh * v**2 / f**2
    !!!
    gwh4 = g**2 * (cos2t**3 + sin2t**3)/sin2t/cos2t 
    gwhwhww = g**2 / four
    gwhwww =  g**2 * sint * cost * (cos2t - sin2t) * v**2 / four / f**2
    gwh3w = - g**2 * t_fac
    !!!
    gwwaah = - g**2*sinthw*costhw*xzbp*v**2/f**2
    gwwazh = - g**2*sinthw*costhw*xzwp*v**2/f**2 + g**2*sinthw*sint*cost* &
         (cos2t-sin2t)/two * v**2/f**2
    gwwzzh = - g**2 * (costhw**2 - sin2thw) * xzwp * v**2/f**2
    gwwzah = - g**2 * costhw**2 * xzbp * v**2/f**2
    gwhwhaah = gwwaah  - g**2*sinthw*two*t_fac*xh*v**2/f**2 
    gwhwhazh = - g**2*sinthw*two*t_fac
    gwhwhzzh = - g**2*costhw*two*t_fac
    gwhwhzah = gwwzah - g**2*costhw*xh*two*t_fac*v**2/f**2
    gwwzhah = g**2 * xh * v**2/f**2
    gwhwhzhah = gwh4*xh*v**2/f**2 + g**2*costhw*xzbp**two*t_fac*v**2/f**2
    gwhwzz = g**2*two*costhw*xzwp*v**2/f**2
    gwhwaz = g**2*sinthw*xzwp*v**2/two/f**2 
    gwhwaah = g**2*sinthw*xh*v**2/f**2 
    gwhwzah = g**2*costhw*xh*v**2/f**2 
    gwhwzhzh = - g**2 * two * t_fac
    gwhwzhah = - g**2*v**2/f**2 * (xh*two*t_fac + costhw*xzbp)
    gwhwazh = g**2 * sinthw 
    gwhwzzh = g**2 * costhw 
    !!! 
    qzup = g / costhw * qeup * sin2thw
    gcch = - gcc * cost / sint
    gcctop = gcc * (one - (v * xlam / f)**2 / two)
    gccw = gcc * v / f * xlam
    gccwh = - gccw * cost / sint
    gnch = g * cost / four / sint
    gztht = - g * xlam * v / four / costhw / f
    gzhtht = g * xlam * (v/f) * cost / sint
    !!! Here we  give the formulae for the masses as a function of other
    !!! input parameter in the case that they do not want to be given by
    !!! the user
    ! mass(32) = sqrt((f*gp/sintp/costp)**2/20_omega_prec - (gp*v/two)**2 &
    !            + (g*v/two/sint/cost)**2 * xh)
    ! mass(33) = sqrt((f*g/two/sint/cost)**2 - (g*v/two)**2 - &
    !            (gp*v/two/sintp/costp)**2 * xh)
    ! mass(34) = sqrt((f*g/two/sint/cost)**2 - (g*v/two)**2)
    gah = gp / two / sintp / costp
    gnchup(1) = gah * (two*yu + 17.0_omega_prec/15. - five/6. * cos2tp)
    gnchup(2) = - gah * (one/five - cos2tp/two)
    gnchdwn(1) = gah * (two*yu + 11.0_omega_prec/15. + one/6. * cos2tp)
    gnchdwn(2) = gah * (one/five - cos2tp/two)    
    gnchneu(1) = gah * (ye - four/five + one/two * cos2tp)
    gnchneu(2) = - gah * (- ye + four/five - cos2tp/two)    
    gnchlep(1) = gah * (two*ye - 9.0_omega_prec/five + three/two * cos2tp)
    gnchlep(2) = gah * (one/five - cos2tp/two)    
    gahtht = gah * mass(6) / v / five
    gahtt(1) = gnchup(1) - gah * xlam / five
    gahtt(2) = gnchup(2) + gah * xlam / five
    gahthth(1) = gah * (two * yu + 14.0_omega_prec/15. - four/three * &
         cos2tp + xlam / five)
    gahthth(2) = - gah * xlam / five
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!! Littlest Higgs Yukawa couplings
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ghtt = - mass(6) / v * ( one + (v/f)**2 * xlam * (one + xlam))
    ghthth = - xlam * (one + xlam) * mass(8) * v / f**2
    ghtht(1) = - mass(6) / two / f * (one + xlam)
    ghtht(2) = - mass(8) * xlam / f
    gpsi0tt = - mass(6)/sqrt(two)/v * (v/f - sqrt(two) * spsi0)
    gpsi0bb = - mass(5)/sqrt(two)/v * (v/f - sqrt(two) * spsi0)
    gpsi0cc = - mass(4)/sqrt(two)/v * (v/f - sqrt(two) * spsi0)
    gpsi0tautau = -mass(15)/sqrt(two)/v * (v/f - sqrt(two) * spsi0)
    gpsi0tt = imago * mass(6)/sqrt(two)/v * (v/f - sqrt(two) * spsi1)
    gpsi0bb = - imago * mass(5)/sqrt(two)/v * (v/f - sqrt(two) * spsi1)
    gpsi0cc = imago * mass(4)/sqrt(two)/v * (v/f - sqrt(two) * spsi1)
    gpsi0tautau = - imago * mass(15)/sqrt(two)/v * (v/f - sqrt(two) * spsi1)
    gpsipq2(1) = - mass(6)/sqrt(two)/v * (v/f - two * spsip) 
    gpsipq2(2) = - mass(5)/sqrt(two)/v * (v/f - two * spsip) 
    gpsipq3(1) = - mass(4)/sqrt(two)/v * (v/f - two * spsip) 
    gpsipq3(2) = - mass(3)/sqrt(two)/v * (v/f - two * spsip) 
    gpsipl3 = - mass(15)/two/sqrt(two)/v * (v/f - two * spsip)
    gpsi0tth = - mass(6)/two/sqrt(two)/v * (v/f - sqrt(two) * spsi0)
    gpsi1tth = imago * gpsi0tth
    gpsipbth = - mass(6)/two/sqrt(two)/v * (v/f - two * spsip) * lam1/lam2
    !!!
    ghhtt = two*mass(6)/f**2 * (one - two*f*vevp/v**2 - xlam/two)
    ghhthth = - lam1**2/mass(8)
    ghhtht(1) = ghhtt * lam1 / lam2
    ghhtht(2) = - mass(6) / v / f
  end subroutine import_from_whizard
end module omega_parameters_whizard
