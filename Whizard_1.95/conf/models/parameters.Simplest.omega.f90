! $Id: parameters.Simplest.omega.f90,v 1.1 2006/06/16 13:31:48 kilian Exp $
!
! Copyright (C) 2000-2008 by Wolfgang Kilian <wolfgang.kilian@desy.de>,
! Thorsten Ohl <ohl@physik.uni-wuerzburg.de>, and Juergen Reuter 
! <juergen.reuter@desy.de>
!
! WHIZARD is free software; you can redistribute it and/or modify it
! under the terms of the GNU General Public License as published by
! the Free Software Foundation; either version 2, or (at your option)
! any later version.
!
! WHIZARD is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.  

module omega_parameters_whizard
  use omega_kinds !NODEP!
  use omega_constants !NODEP!
  use parameters
  implicit none
  private
  real(kind=omega_prec), dimension(45), save, public :: mass = 0, width = 0
  real(kind=omega_prec), parameter, public :: GeV = 1.0_omega_prec
  real(kind=omega_prec), parameter, public :: MeV = GeV / 1000
  real(kind=omega_prec), parameter, public :: keV = MeV / 1000
  real(kind=omega_prec), parameter, public :: TeV = GeV * 1000
  real(kind=omega_prec), save, public :: &
       alpha = 1.0_omega_prec / 137.0359895_omega_prec, &
       sin2thw = 0.23124_omega_prec
  complex(kind=omega_prec), save, private :: vev
  complex(kind=omega_prec), save, public :: &
       qlep = 0, qup = 0, qdwn = 0, gcc = 0, qw = 0, &
       gzww = 0, gwww = 0, ghww = 0, ghhww = 0, ghzz = 0, ghhzz = 0, &
       ghbb = 0, ghtt = 0, ghcc = 0, ghtautau = 0, gh3 = 0, gh4 = 0, &
       ghgaga = 0, ghgaz = 0, &
       iqw = 0, igzww = 0, igwww = 0, &
       gw4 = 0, gzzww = 0, gazww = 0, gaaww = 0, &
       gs = 0, igs = 0
  complex(kind=omega_prec), save, public :: &
       sinckm12 = 0, sinckm13 = 0, sinckm23 = 0, &
       cosckm12 = 0, cosckm13 = 0, cosckm23 = 0
  complex(kind=omega_prec), save, public :: &
       vckm_11 = 0, vckm_12 = 0, vckm_13 = 0, vckm_21 = 0, &
       vckm_22 = 0, vckm_23 = 0, vckm_31 = 0, vckm_32 = 0, vckm_33 = 0
  complex(kind=omega_prec), save, public :: &
       gccq11 = 0, gccq12 = 0, gccq13 = 0, gccq21 = 0, &
       gccq22 = 0, gccq23 = 0, gccq31 = 0, gccq32 = 0, gccq33 = 0       
  real(kind=omega_prec), save, public :: &
       g1a = 1, g1z = 1, kappaa = 1, kappaz = 1, lambdaa = 0, lambdaz = 0, &
       g4a = 0, g4z = 0, g5a = 0, g5z = 0, &
       kappa5a = 0, kappa5z = 0, lambda5a = 0, lambda5z = 0, &
       alpha4 = 0, alpha5 = 0, tau4 = 0, tau5 = 0
  real(kind=omega_prec), save, public :: xia = 1, xi0 = 1, xipm = 1
  complex(kind=omega_prec), dimension(2), save, public :: &
       gnclep = 0, gncneu = 0, gncup = 0, gncdwn = 0
  complex(kind=omega_prec), save, public :: &
       fudge_o1 = 1, fudge_o2 = 1, fudge_o3 = 1, fudge_o4 = 1
  complex(kind=omega_prec), save, public :: &
       ghmumu = 0
  complex(kind=omega_prec), save, public :: &
       ghaa = 0, ghgg = 0, geaa = 0, geaz = 0, gegg = 0
  complex(kind=omega_prec), save, public :: &
       gh0ww = 0, gh0zz = 0, &
       gh0tt = 0, gh0bb = 0, gh0cc = 0, gh0tautau = 0, gh0mumu = 0, &
       iga0tt = 0, iga0bb = 0, iga0cc = 0, iga0tautau = 0, iga0mumu = 0, &
       gahh = 0, gzhh = 0, igzha = 0, igzh0a = 0
  complex(kind=omega_prec), dimension(2), save, public :: &
       ghptb = 0, ghpcs = 0, ghptaunu = 0, ghpmunu = 0
  !!! Specific simple group parameters
  complex(kind=omega_prec), save, public :: &
       gncx = 0, gncxt = 0, gncy = 0, gncyt = 0
  complex(kind=omega_prec), dimension(2), save, public :: &
       gnchlep = 0, gnchneu = 0, gnchup = 0, gnchdwn = 0, &
       gnchtop = 0, gnchbot = 0, gnchn = 0, &
       gnchu = 0, gnchd = 0
  complex(kind=omega_prec), save, public :: &
       ghhzzh = 0, iqzh = 0, igz1 = 0, igz2 = 0, igz3 = 0, & 
       igz4 = 0, igz5 = 0, igz6 = 0, i_gcc = 0, gnch = 0
  complex(kind=omega_prec), save, public :: &
        ghtht = 0, ghhthth = 0, gncht = 0, ghqhq = 0, getht = 0
  complex(kind=omega_prec), save, public :: &
        gzeh = 0, gzheh = 0, gebb = 0, gett = 0
  complex(kind=omega_prec), save, public :: & 
       ghyhvv = 0, ghyhww = 0
  integer, parameter, public :: &
       n0 = 5, nloop = 2 
  real(kind=omega_prec), parameter :: &
       acc = 1.e-12_omega_prec
  real(kind=omega_prec), parameter :: &
       asmz = 0.118_omega_prec
  type(parameter_set) :: par
  public :: import_from_whizard
contains
  function faux (x) result (y)
    real(kind=omega_prec) :: x
    complex(kind=omega_prec) :: y
    if (1 <= x) then
       y = asin(sqrt(1/x))**2
    else
       y = - 1/4.0_omega_prec * (log((1 + sqrt(1 - x))/ &
            (1 - sqrt(1 - x))) - cmplx (0.0_omega_prec, PI))**2
    end if
  end function faux

  function fonehalf (x) result (y)
    real(kind=omega_prec), intent(in) :: x
    complex(kind=omega_prec) :: y
    if (x==0) then
       y = 0
    else
       y = - 2.0_omega_prec * x * (1 + (1 - x) * faux(x))
    end if
  end function fonehalf

  function fonehalf_var (x) result (y) 
    real(kind=omega_prec), intent(in) :: x
    complex(kind=omega_prec) :: y
    if (x==0) then
       y = 0
    else 
       y = - 2.0_omega_prec * x * faux(x)
    end if
  end function fonehalf_var

  function fone (x) result  (y)
    real(kind=omega_prec), intent(in) :: x
    complex(kind=omega_prec) :: y
    if (x==0) then
       y = 2.0_omega_prec
    else
       y = 2.0_omega_prec + 3.0_omega_prec * x + &
            3.0_omega_prec * x * (2.0_omega_prec - x) * &
            faux(x)
    end if
  end function fone

  function gaux (x) result (y)
    real(kind=omega_prec), intent(in) :: x
    complex(kind=omega_prec) :: y
    if (1 <= x) then
       y = sqrt(x - 1) * asin(sqrt(1/x))
    else
       y = sqrt(1 - x) * (log((1 + sqrt(1 - x)) / &
            (1 - sqrt(1 - x))) - cmplx (0.0_omega_prec, PI)) / 2
    end if
  end function gaux

  function i1 (a,b) result (y)
    real(kind=omega_prec), intent(in) :: a,b
    complex(kind=omega_prec) :: y
    y = a*b/2.0_omega_prec/(a-b) + a**2 * b**2/2.0_omega_prec/(a-b)**2 * &
         (faux(a) - faux(b)) + &
         a**2 * b/(a-b)**2 * (gaux(a) - gaux(b))
  end function i1

  function i2 (a,b) result (y) 
    real(kind=omega_prec), intent(in) :: a,b
    complex(kind=omega_prec) :: y
    y = - a * b / 2.0_omega_prec / (a-b) * (faux(a) - faux(b)) 
  end function i2

  function b0 (nf) result (bnull)
    integer, intent(in) :: nf
    real(kind=omega_prec) :: bnull
    bnull = 33.0_omega_prec - 2.0_omega_prec * nf
  end function b0

  function b1 (nf) result (bone)
    integer, intent(in) :: nf
    real(kind=omega_prec) :: bone
    bone = 6.0_omega_prec * (153.0_omega_prec - 19.0_omega_prec * nf)/b0(nf)**2
  end function b1

  function aa (nf) result (aaa)
    integer, intent(in) :: nf
    real(kind=omega_prec) :: aaa
    aaa = 12.0_omega_prec * PI / b0(nf)
  end function aa

  function bb (nf) result (bbb)
    integer, intent(in) :: nf
    real(kind=omega_prec) :: bbb
    bbb = b1(nf) / aa(nf)
  end function bb

  subroutine import_from_whizard (par)
    type(parameter_set), intent(in) :: par
    real(kind=omega_prec) :: e, g, sinthw, costhw, tanthw, e_em
    real(kind=omega_prec) :: qelep, qeup, qedwn, v
    real(kind=omega_prec) :: ttop, tbot, tc, ttau, tw
    real(kind=omega_prec) :: tqh1, tqh2, tqh3
    real(kind=omega_prec) :: ltop, lbot, lc, ltau, lw
    !!! This corresponds to 1/alpha = 137.03598949333
    real(kind=omega_prec), parameter :: &
         alpha = 1.0_omega_prec/137.03598949333_omega_prec
    real(kind=omega_prec), parameter :: &
         asmz = 0.118_omega_prec
    real(kind=omega_prec), parameter :: & 
         one = 1.0_omega_prec, two = 2.0_omega_prec, three = 3.0_omega_prec, &
         four = 4.0_omega_prec, five = 5.0_omega_prec
    complex(kind=omega_prec), parameter :: &
         imago = (0.0_omega_prec, 1.0_omega_prec)
    real(kind=omega_prec) :: f, d_nu, d_top, tanb, sinb, cosb, &
         xlam, xlamp, gnzh
    real(kind=omega_prec) :: n1, n2, n3, gethth
    e_em = sqrt(4.0_omega_prec * PI * alpha)
    mass(1:45) = 0
    width(1:45) = 0
    mass(3) = par%ms
    mass(4) = par%mc
    mass(5) = par%mb
    mass(6) = par%mtop
    width(6) = par%wtop
    mass(11) = par%me
    mass(13) = par%mmu
    mass(15) = par%mtau
    mass(23) = par%mZ
    width(23) = par%wZ
    mass(24) = par%mW
    width(24) = par%wW
    mass(25) = par%mH
    width(25) = par%wH
    mass(36) = par%meta
    width(36) = par%weta
    mass(26) =  xi0 * mass(23)
    width(26) =  0
    mass(27) =  xipm * mass(24)
    width(27) =  0
    mass(32) = par%mzh
    width(32) = par%wzh
    mass(33) = par%mxh
    width(33) = par%wxh
    mass(34) = par%mwh
    width(34) = par%wwh
    mass(38) = par%myh
    width(38) = par%wyh
    mass(40) = par%mdh
    width(40) = par%wdh
    mass(41) = par%mnh1
    width(41) = par%wnh1
    mass(42) = par%msh
    width(42) = par%wsh
    mass(43) = par%mnh2
    width(43) = par%wnh2
    mass(44) = par%mtoph
    width(44) = par%wtoph
    mass(45) = par%mnh3
    width(45) = par%wnh3
    f = par%f
    tanb = par%tanb
    sinb = (tanb / sqrt ((1.0_omega_prec + (tanb**2))))
    cosb = (1.0_omega_prec / sqrt ((1.0_omega_prec + (tanb**2))))
    ttop = 4.0_omega_prec * mass(6)**2 / mass(25)**2
    tbot = 4.0_omega_prec * mass(5)**2 / mass(25)**2
    tc   = 4.0_omega_prec * mass(4)**2 / mass(25)**2
    ttau = 4.0_omega_prec * mass(15)**2 / mass(25)**2
    tw   = 4.0_omega_prec * mass(24)**2 / mass(25)**2  
    ltop = 4.0_omega_prec * mass(6)**2 / mass(23)**2
    lbot = 4.0_omega_prec * mass(5)**2 / mass(23)**2  
    lc   = 4.0_omega_prec * mass(4)**2 / mass(23)**2
    ltau = 4.0_omega_prec * mass(15)**2 / mass(23)**2
    lw   = 4.0_omega_prec * mass(24)**2 / mass(23)**2
    tqh1 = 4.0_omega_prec * mass(40)**2 / mass(25)**2
    tqh2 = 4.0_omega_prec * mass(42)**2 / mass(25)**2
    tqh3 = 4.0_omega_prec * mass(44)**2 / mass(25)**2
    v = 2 * mass(24) * par%sw / par%ee 
    e = par%ee
    sinthw = par%sw
    sin2thw = sinthw**2
    costhw = par%cw
    tanthw = sinthw/costhw
    qelep = - 1
    qeup = 2.0_omega_prec / 3.0_omega_prec
    qedwn = - 1.0_omega_prec / 3.0_omega_prec
    g = e / sinthw
    gcc = - g / 2 / sqrt (2.0_double)
    i_gcc = imago * gcc
    n1 = sinb**2 - cosb**2
    n2 = tanb - 1.0_omega_prec/tanb
    n3 = 4.0_omega_prec * mass(6)**2/v**2 * (sinb**4 + cosb**4)
    gncneu(1) = - g / 2 / costhw * ( + 0.5_double)
    gnclep(1) = - g / 2 / costhw * ( - 0.5_double - 2 * qelep * sin2thw)
    gncup(1)  = - g / 2 / costhw * ( + 0.5_double - 2 * qeup  * sin2thw)
    gncdwn(1) = - g / 2 / costhw * ( - 0.5_double - 2 * qedwn * sin2thw)
    gncneu(2) = - g / 2 / costhw * ( + 0.5_double)
    gnclep(2) = - g / 2 / costhw * ( - 0.5_double)
    gncup(2)  = - g / 2 / costhw * ( + 0.5_double)
    gncdwn(2) = - g / 2 / costhw * ( - 0.5_double)
    qlep = - e * qelep
    qup = - e * qeup
    qdwn = - e * qedwn
    qw = e
    iqw = imago*qw
    gzww = g * costhw
    igzww = imago*gzww
    gwww = g
    igwww = imago*gwww
    gw4 = gwww**2
    gzzww = gzww**2
    gazww = gzww * qw
    gaaww = qw**2
    ghww = mass(24) * g
    ghhww = g**2 / 2.0_omega_prec
    ghzz = mass(23) * g / costhw
    ghhzz = g**2 / 2.0_omega_prec / costhw**2
    ghtt = - mass(6) / v
    ghbb = - mass(5) / v
    ghcc = - mass(4) / v
    ghtautau = - mass(15) / v
    gh3 = - 3 * mass(25)**2 / v
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!! Specific Simplest Little Higgs parameters
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!    
    !!! We are always considering the case that
    !!! lambda1/lambda2 = f2/f1 minimizing the mtoph !
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!! Yukawa couplings
    !!!
    ghtht = (cosb**2 - sinb**2) * ghtt / two
    ghhthth = ghtt**2 * (one + (cosb - sinb)**2) 
    ghqhq = - mass(40)/two/sqrt(two)/f/tanb
    gett = n2*mass(6)/sqrt(two)/F - mass(6)**2*n1/v/mass(44)
    getht = ghtt / two
    gebb =  - imago * mass(5) * (tanb - one/tanb) / f / sqrt(two)
    gethth = n1*mass(6)/v + n1*n3*v*mass(6)/two/mass(44)**2
    gh4 = - 3 * mass(25)**2 / v**2
    !!! Color flow basis, divide by sqrt(2)
    gs = sqrt(2.0_omega_prec*PI*par%alphas)
    igs = (0.0_omega_prec, 1.0_omega_prec) * gs    
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!! Higgs anomaly couplings
    !!! SM LO loop factor (top,bottom,W)
    ghgaga = alpha / v / 2.0_omega_prec / PI * &
         Abs(( 4.0_omega_prec * &
         (fonehalf(ttop) + fonehalf(tc) + fonehalf(tqh3)) &
         + fonehalf(tbot) + fonehalf(tqh1) + fonehalf(tqh2)) &
         / 3.0_omega_prec + fonehalf(ttau) + fone(tw)) &
         * sqrt(par%khgaga)
    !!! asymptotic limit:
    !!! ghgaga = (par%ee)**2 / v / &
    !!!      9.0_omega_prec / pi**2
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!! SM LO loop factor (only top and W)
    ghgaz = e * e_em / 8.0_omega_prec / PI**2 / v * Abs( &
          ( - 2.0_omega_prec + &
          16.0_omega_prec/3.0_omega_prec * sin2thw) * &
          (i1(ttop,ltop) - i2(ttop,ltop)) / costhw & 
          + ( - 1.0_omega_prec + &
          4.0_omega_prec/3.0_omega_prec * sin2thw) & 
          * (i1(tbot,lbot) - i2(tbot,lbot)) / costhw &
           - costhw * ( 4.0_omega_prec * (3.0_omega_prec - tanthw**2) * &
           i2(tw,lw) + ((1 + 2.0_omega_prec/tw) * tanthw**2 - ( &
           5.0_omega_prec + 2.0_omega_prec/tw)) * i1(tw,lw)) &
          )/sinthw * sqrt(par%khgaz)
    !!! SM LO order loop factor with 
    !!! N(N)LO K factor = 2.1 (only top)
    !!! Limit of infinite top quark mass:
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!! We use par%alphas because of sqrt(2) above
    ghgg = par%alphas / v / 4.0_omega_prec / PI * &
         Abs(fonehalf(ttop) + fonehalf(tbot) + fonehalf(tc)) &
         * sqrt(par%khgg)
    gegg = par%alphas / 4.0_omega_prec / PI * &
         Abs( fonehalf_var(four*mass(6)**2/mass(36)**2) * gett/mass(6) &
         + fonehalf_var(four*mass(44)**2/mass(36)**2) * gethth/mass(44)  &
         + fonehalf_var(four*mass(40)**2/mass(36)**2) / tanb/F/sqrt(two) &
         + fonehalf_var(four*mass(42)**2/mass(36)**2) / tanb/F/sqrt(two))
!!! ghgg   = par%alphas / 3.0_omega_prec &
    !!!      / v / pi * 2.1_omega_prec
    xlam = mass(6) / v * (cosb**2 - sinb**2)
    xlamp = mass(6)**2 / v**2 * (one + (cosb**2 - sinb**2)**2)
    d_nu = - v / sqrt(two) / f / tanb 
    d_top = xlam * v / mass(44)
    gnzh = g / costhw / sqrt(three - four * sin2thw) / two
    !!!
    gncx = gcc * d_nu
    gncxt = - gcc * d_top        
    gncy = imago * gncx
    gncyt = imago * gncxt
    gnchneu(1) = gnzh * ( one/two + qelep * sin2thw)
    gnchlep(1) = gnchneu(1)
    gnchup(1)  = gnzh * ( - one/two + qeup * sin2thw)
    gnchdwn(1) = gnchup(1)
    gnchtop(1) = gnzh * ( one/two + qedwn * sin2thw)
    gnchbot(1) = gnchtop(1)
    gnchneu(2) = 0
    gnchlep(2) = gnzh * qelep * sin2thw
    gnchup(2)  = gnzh * qeup * sin2thw
    gnchdwn(2) = gnzh * qedwn * sin2thw
    gnchtop(2) = gnchup(2)
    gnchbot(2) =  gnchdwn(2)
    gnch = g / four / costhw * d_nu
    gncht = g / four / costhw * d_top
    !!!
    gnchn(1) = - gnzh * ( - one + sin2thw)
    gnchu(1) = - gnzh * ( - one + five / three * sin2thw)
    gnchd(1) = - gnzh * ( - one + five / three * sin2thw)
    gnchn(2) = 0
    gnchu(2) = - gnzh * qeup * sin2thw
    gnchd(2) = - gnzh * qedwn * sin2thw
    !!! 
    ghhzzh = g**2 * (one - sin2thw/costhw**2) / four / costhw / &
         sqrt(three - sin2thw/costhw**2)
    !!!
    iqzh = g * (one - two * sin2thw) / two / costhw
    igz1 = g / four / costhw
    igz2 = g / four * sqrt( one - sin2thw / three / costhw**2) * sqrt(three)
    igz3 = imago * g / two
    igz4 = g / two
    igz5 = imago * igz2
    igz6 = - imago * g * (one - sin2thw/costhw**2) * sqrt(three - sin2thw / &
         costhw**2) * v**2 / 8.0_omega_prec / f**2
    !!! 
    gzeh = mass(23) / sqrt(two) / f * (tanb - one/tanb)
    gzheh = gzeh * costhw * sqrt(one - three * tanthw**2)
  end subroutine import_from_whizard
end module omega_parameters_whizard
