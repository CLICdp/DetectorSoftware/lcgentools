! $Id: parameters.QCD.omega.f90,v 1.1 2005/05/19 16:00:08 kilian Exp $
!
! Copyright (C) 2000 by Thorsten Ohl <ohl@hep.tu-darmstadt.de>
! and others
!
! O'Mega is free software; you can redistribute it and/or modify it
! under the terms of the GNU General Public License as published by 
! the Free Software Foundation; either version 2, or (at your option)
! any later version.
!
! O'Mega is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
module omega_parameters_whizard
  use omega_kinds !NODEP!
  use omega_constants !NODEP!
  use omega_parameters !NODEP!
  use parameters
  implicit none
  private
  public :: import_from_whizard
contains
  subroutine import_from_whizard (par)
    type(parameter_set), intent(in) :: par
    mass = 0
    width = 0
    mass(3) = par%ms
    mass(4) = par%mc
    mass(5) = par%mb
    mass(6) = par%mtop
    width(6) = par%wtop
    !!! Color flow basis, divide by sqrt(2)
    gs = sqrt(2.0_omega_prec*PI*par%alphas)
    ! and 3g coupling carries a factor i
    igs = (0.0_omega_prec, 1.0_omega_prec) * gs    
  end subroutine import_from_whizard
end module omega_parameters_whizard
