# Process file for 2->2 MSSM comparison

model MSSM

#########################################################################
# bb channels
#########################################################################
# Squark pairs
bbbb11	b,B	sb1,sb1c	omega	c
bbbb22	b,B	sb2,sb2c	omega	c
bbbb12	b,B	sb1,sb2c	omega	c
bbtt11	b,B	st1,st1c	omega	c
bbtt22	b,B	st2,st2c	omega	c
bbtt12	b,B	st1,st2c	omega	c
#########################################################################
# Gaugino pairs
bbnn11	b,B	neu1,neu1	omega
bbnn12	b,B	neu1,neu2	omega
bbnn13	b,B	neu1,neu3	omega
bbnn14	b,B	neu1,neu4	omega
bbnn22	b,B	neu2,neu2	omega
bbnn23	b,B	neu2,neu3	omega
bbnn24	b,B	neu2,neu4	omega
bbnn33	b,B	neu3,neu3	omega
bbnn34	b,B	neu3,neu4	omega
bbnn44	b,B	neu4,neu4	omega
#
bbcc11	b,B	ch1+,ch1-	omega
bbcc22	b,B	ch2+,ch2-	omega
bbcc12	b,B	ch1+,ch2-	omega
#########################################################################
# Higgs pairs
bbh1	b,B	h,h		omega
bbh2	b,B	h,HH		omega
bbh12	b,B	HH,HH		omega
bbaa	b,B	HA,HA		omega
bbzh1	b,B	Z,h		omega
bbzh2 	b,B	Z,HH    	omega
bbza	b,B	Z,HA		omega
bbah1 	b,B	HA,h  	  	omega
bbah2 	b,B	HA,HH   	omega
bbhpm 	b,B	H+,H-   	omega
#########################################################################

