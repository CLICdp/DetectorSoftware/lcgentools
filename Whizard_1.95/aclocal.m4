dnl Macros for configuring WHIZARD subdirectories and subpackages
dnl All macros begin with 'WK'

dnl Horizontal line for readability:
AC_DEFUN(WK_HLINE,
[AC_MSG_NOTICE([--------------------------------------------------------------])])

dnl Message at the beginning of a configure section
AC_DEFUN(WK_CONFIGURE_SECTION,
[WK_HLINE()
AC_MSG_NOTICE([--- ]$1[ ---])
AC_MSG_NOTICE([])
])


dnl Define a variable and export it
dnl WK_WHIZARD_SET(variable, value)
AC_DEFUN(WK_WHIZARD_SET,
[$1=$2
AC_SUBST($1)])

dnl Add a list of names to the list of output resp. executable files
dnl WK_WHIZARD_OUTFILES(subdir, files)
AC_DEFUN(WK_WHIZARD_OUTFILES,
[for file in $2; do
  OUTFILES="$OUTFILES $1/$file";
done])

dnl WK_WHIZARD_EXECUTABLES(subdir, files)
AC_DEFUN(WK_WHIZARD_EXECUTABLES,
[for file in $2; do
  OUTFILES="$OUTFILES $1/$file";
  EXECUTABLES="$EXECUTABLES $1/$file"
done])


dnl This adds a package which resides in a WHIZARD subdirectory.
dnl The `variable' is inserted into AC_SUBST; it will refer to 
enl the package subdirectory in Makefiles.  The package
dnl has a subdirectory XXX-src and optionally a Makefile (or similar).
dnl WK_WHIZARD_PACKAGE(variable, identifier [,Makefiles [,Executables]])
AC_DEFUN(WK_WHIZARD_PACKAGE,
[WK_WHIZARD_SET($1,$2-src)
ifelse($#, 3, 
[WK_WHIZARD_OUTFILES($$1, $3)])
ifelse($#, 4, 
[WK_WHIZARD_OUTFILES($$1, $3)
WK_WHIZARD_EXECUTABLES($$1, $4)])
])

dnl This is similar, but no '-src' is appended for the directory name
dnl WK_WHIZARD_SUBDIR(variable, identifier [,Makefiles [,Executables]])
AC_DEFUN(WK_WHIZARD_SUBDIR,
[WK_WHIZARD_SET($1,$2)
ifelse($#, 3,
[WK_WHIZARD_OUTFILES($$1, $3)])
ifelse($#, 4, 
[WK_WHIZARD_OUTFILES($$1, $3)
WK_WHIZARD_EXECUTABLES($$1, $4)])
])


dnl This adds a matrix element generating package.
dnl There must be at least one Makefile.
dnl In addition to the preceding macro, it adds two scripts
dnl init.XXX and make-proc.XXX where XXX is the package identifier.
dnl WK_WHIZARD_ME_GENERATOR(variable, identifier, Makefile)
AC_DEFUN(WK_WHIZARD_ME_GENERATOR,
[WK_WHIZARD_PACKAGE($1,$2,$3, init.$2 make-proc.$2)])


dnl This is like WK_WHIZARD_PACKAGE, but it calls AC_ARG_ENABLE in addition.
dnl If the package `id' is disabled or the 'file' is not found,
dnl the variable `var' is set to "no".
dnl WK_WHIZARD_PACKAGE_ENABLE(var, id, help, file [,Makefiles [,Executables]])
AC_DEFUN(WK_WHIZARD_PACKAGE_ENABLE,
[AC_ARG_ENABLE($2,[$3])
if test "$enable_$2" = "no"; then
AC_MSG_CHECKING([for $2-src/$4])
WK_WHIZARD_SET($1, no)
AC_MSG_RESULT([(disabled)])
else
AC_CHECK_FILE($2-src/$4, enable_$2=yes, enable_$2=no)
if test "$enable_$2" = "no"; then
WK_WHIZARD_SET($1, no)
else
ifelse($#, 4, [WK_WHIZARD_PACKAGE($1, $2)])
ifelse($#, 5, [WK_WHIZARD_PACKAGE($1, $2, $5)])
ifelse($#, 6, [WK_WHIZARD_PACKAGE($1, $2, $5, $6)])
fi
fi
])

dnl The same, but disabled by default.
dnl If the package `id' is disabled or the 'file' is not found,
dnl the variable `var' is set to "no".
dnl WK_WHIZARD_PACKAGE_DISABLE(var, id, help, file [,Makefiles [,Executables]])
AC_DEFUN(WK_WHIZARD_PACKAGE_DISABLE,
[AC_ARG_ENABLE($2,[$3])
if test "$enable_$2" = "yes"; then
AC_CHECK_FILE($2-src/$4, enable_$2=yes, enable_$2=no)
if test "$enable_$2" = "no"; then
WK_WHIZARD_SET($1, no)
else
ifelse($#, 4, [WK_WHIZARD_PACKAGE($1, $2)])
ifelse($#, 5, [WK_WHIZARD_PACKAGE($1, $2, $5)])
ifelse($#, 6, [WK_WHIZARD_PACKAGE($1, $2, $5, $6)])
fi
else
enable_$2="no"
AC_MSG_CHECKING([for $2-src/$4])
WK_WHIZARD_SET($1, no)
AC_MSG_RESULT([(disabled)])
fi
])

dnl The same for a matrix element generator
dnl There must be at least one Makefile.
dnl WK_WHIZARD_ME_GENERATOR_ENABLE(var, id, help, file, Makefiles [,Executables])
AC_DEFUN(WK_WHIZARD_ME_GENERATOR_ENABLE,
[WK_WHIZARD_PACKAGE_ENABLE($1, $2, [$3], $4, $5, init.$2 make-proc.$2 $5)])

dnl The same, disabled by default:
dnl WK_WHIZARD_ME_GENERATOR_DISABLE(var, id, help, file, Makefiles [,Executables])
AC_DEFUN(WK_WHIZARD_ME_GENERATOR_DISABLE,
[WK_WHIZARD_PACKAGE_DISABLE($1, $2, [$3], $4, $5, init.$2 make-proc.$2 $5)])

dnl --------------------------------------------------------------------
dnl
dnl THO_FILENAME_CASE_CONVERSION
dnl
dnl   Define two variables LOWERCASE and UPPERCASE for /bin/sh filters
dnl   that convert strings to lower and upper case, respectively
dnl
AC_DEFUN([THO_FILENAME_CASE_CONVERSION],
[AC_SUBST([LOWERCASE])
AC_SUBST([UPPERCASE])
AC_PATH_PROGS(TR,tr)
AC_PATH_PROGS(SED,sed)
AC_MSG_CHECKING([for case conversion])
if test -n "$TR"; then
  LOWERCASE="$TR A-Z a-z"
  UPPERCASE="$TR a-z A-Z"
  THO_FILENAME_CASE_CONVERSION_TEST
fi
if test -n "$UPPERCASE" && test -n "$LOWERCASE"; then
  AC_MSG_RESULT([$TR works])
else
  LOWERCASE="$SED y/ABCDEFGHIJKLMNOPQRSTUVWXYZ/abcdefghijklmnopqrstuvwxyz/"
  UPPERCASE="$SED y/abcdefghijklmnopqrstuvwxyz/ABCDEFGHIJKLMNOPQRSTUVWXYZ/"
  THO_FILENAME_CASE_CONVERSION_TEST
  if test -n "$UPPERCASE" && test -n "$LOWERCASE"; then
    AC_MSG_RESULT([$SED works])
  fi
fi])
dnl
AC_DEFUN([THO_FILENAME_CASE_CONVERSION_TEST],
[if test "`echo fOo | $LOWERCASE`" != "foo"; then
  LOWERCASE=""
fi
if test "`echo fOo | $UPPERCASE`" != "FOO"; then
  UPPERCASE=""
fi])

dnl ------------------------------------------------------------------------
dnl Fortran compiler tests

dnl
dnl --------------------------------------------------------------------
dnl
dnl THO_PROG_F
dnl THO_PROG_FC
dnl
dnl   These allow to set F and F95 to overwrite the defaults
dnl   (assuming that the names contain no spaces!)
dnl
AC_DEFUN([THO_PROG_F],[AC_PATH_PROGS(F,$F F)])
AC_DEFUN([THO_PROG_FC],[AC_PATH_PROGS(FC,$FC $F95 $F90 lf95 f95 f90 ifort ifc fort)])

dnl This looks for a Fortran 95 compiler.
dnl The standard Fortran compiler test is AC_PROG_FC.
dnl We use this, but require the .f90 extension for free-form code.
dnl At the end FC, FCFLAGS and FCFLAGS_f90 are set, if successful.

dnl
dnl --------------------------------------------------------------------
dnl
dnl THO_FORTRAN_TEST_EXTENSION(VARIABLE, COMPILER, EXTENSION)
dnl
AC_DEFUN([THO_FORTRAN_TEST_EXTENSION],
[AC_SUBST([$1])
if test -n "$2"; then
   THO_COMPILE_FORTRAN90([$1], [$2], [$3], [], [$3], [])
fi])
dnl
dnl --------------------------------------------------------------------
dnl
dnl THO_FORTRAN_FIND_EXTENSION(VARIABLE, COMPILER, EXTENSIONS)
dnl
AC_DEFUN([THO_FORTRAN_FIND_EXTENSION],
[AC_SUBST([$1])
for ext in $3; do
   AC_MSG_CHECKING([whether '$2' accepts .$ext])
   THO_FORTRAN_TEST_EXTENSION([$1], [$2], [$ext])
   if test -n "[$]$1"; then
      AC_MSG_RESULT([yes])
      break
   else
      AC_MSG_RESULT([no])
   fi
done])
dnl
dnl --------------------------------------------------------------------
dnl
dnl THO_FORTRAN_TEST_PURE(VARIABLE, COMPILER, EXTENSION)
dnl
AC_DEFUN([THO_FORTRAN_TEST_PURE],
[AC_SUBST([$1])
if test -n "$2"; then
   AC_MSG_CHECKING([whether '$2' accepts PURE functions])
   THO_COMPILE_FORTRAN90([$1], [$2], [$3],
       [module conftest_module
          implicit none
          private :: f
	contains
	  pure function f (x) result (fx)
	    real, intent(in) :: x
	    real :: fx
	    fx = x
	  end function f
	end module conftest_module
       ], [$3], [])
  if test "[$]$1" = "$3"; then
    $1=yes
  else
    $1=no
  fi
  AC_MSG_RESULT([[$]$1])
fi])
dnl
dnl --------------------------------------------------------------------
dnl
dnl THO_FORTRAN_TEST_OPTION(VARIABLE, COMPILER, EXTENSION, OPTION)
dnl
dnl   Test whether the COMPILER accepts the OPTION (using EXTENSION
dnl   for the test source).  If so, the VARIABLE will be set to OPTION.
dnl
AC_DEFUN([THO_FORTRAN_TEST_OPTION],
[if test -n "$2"; then
   THO_COMPILE_FORTRAN90([$1], [$2 $4], [$3], [], [$4], [])
fi])
dnl
dnl --------------------------------------------------------------------
dnl
dnl THO_FORTRAN_FIND_OPTION(VARIABLE, COMPILER, EXTENSION, OPTIONS)
dnl
dnl   Append the first accepted option from OPTIONS to VARIABLE.
dnl
AC_DEFUN([THO_FORTRAN_FIND_OPTION],
[AC_SUBST([$1])
for option in $4; do
   AC_MSG_CHECKING([whether '$2' accepts $option])
   THO_FORTRAN_TEST_OPTION([tmp_$1], [$2], [$3], [$option])
   if test -n "[$]tmp_$1"; then
      $1="[$]$1 [$]tmp_$1"
      AC_MSG_RESULT([yes])
      break
   else
      AC_MSG_RESULT([no])
   fi
done])
dnl
dnl --------------------------------------------------------------------
dnl
dnl THO_FORTRAN_FILTER_OPTIONS(VARIABLE, COMPILER, EXTENSION, OPTIONS)
dnl
dnl   Append all accepted options from OPTIONS to VARIABLE.
dnl
AC_DEFUN([THO_FORTRAN_FILTER_OPTIONS],
[AC_SUBST([$1])
for option in $4; do
   AC_MSG_CHECKING([whether '$2' accepts $option])
   THO_FORTRAN_TEST_OPTION([tmp_$1], [$2], [$3], [$option])
   if test -n "[$]tmp_$1"; then
      $1="[$]$1 [$]tmp_$1"
      AC_MSG_RESULT([yes])
   else
      AC_MSG_RESULT([no])
   fi
done])
dnl
dnl --------------------------------------------------------------------
dnl
dnl THO_FORTRAN_TEST_QUADRUPLE(VARIABLE, COMPILER, EXTENSION)
dnl
AC_DEFUN([THO_FORTRAN_TEST_QUADRUPLE],
[AC_SUBST([$1])
if test -n "$2"; then
   AC_MSG_CHECKING([whether '$2' accepts quadruple precision])
   THO_COMPILE_FORTRAN90([$1], [$2], [$3],
       [module conftest_module
          integer, parameter :: d=selected_real_kind(precision(1.)+1, range(1.)+1)
          integer, parameter :: q=selected_real_kind(precision(1._d)+1, range(1._d))
          real(kind=q) :: x
          complex(kind=q) :: z
	end module conftest_module
       ], [$3], [])
  if test "[$]$1" = "$3"; then
    $1=yes
  else
    $1=no
  fi
  AC_MSG_RESULT([[$]$1])
fi])
dnl
dnl --------------------------------------------------------------------
dnl
dnl THO_FORTRAN_VENDOR(VARIABLE, COMPILER)
dnl
dnl AC_DEFUN([THO_FORTRAN_VENDOR],
dnl [AC_SUBST([$1])
dnl if test -n "$2"; then
dnl    $1=unknown
dnl    AC_MSG_CHECKING([vendor of '$2'])
dnl    $2 -V >conftest.out 2>&1
dnl    if grep -q 'NAGWare' conftest.out; then
dnl       $1=NAG
dnl    elif grep -q 'Intel(R)' conftest.out; then
dnl       $1=Intel
dnl       if grep -q 'Version 11\.' conftest.out; then
dnl          FC_IFC_VERSION=11	          
dnl       if grep -q 'Version 10\.' conftest.out; then
dnl          FC_IFC_VERSION=10	          
dnl       elif grep -q 'Version 9\.' conftest.out; then
dnl          FC_IFC_VERSION=9
dnl       elif grep -q 'Version 8\.' conftest.out; then
dnl          FC_IFC_VERSION=8
dnl       elif grep -q 'Version 7\.1' conftest.out; then
dnl          FC_IFC_VERSION=7
dnl          FC_IFC_MINOR_VERSION=1
dnl       elif grep -q 'Version 7\.0' conftest.out; then
dnl          FC_IFC_VERSION=7
dnl          FC_IFC_MINOR_VERSION=0
dnl       elif grep -q 'Version 7\.' conftest.out; then
dnl          FC_IFC_VERSION=7
dnl       elif grep -q 'Version 6\.' conftest.out; then
dnl          FC_IFC_VERSION=6
dnl       elif grep -q 'Version 5\.' conftest.out; then
dnl          FC_IFC_VERSION=5
dnl       else
dnl          AC_MSG_WARN([version of Intel Fortran compiler not recognized, dnl
dnl continuing at your own peril ...])
dnl          FC_IFC_VERSION=0
dnl       fi
dnl    else
dnl      $2 -version >conftest.out 2>&1
dnl      if grep -q 'Compaq' conftest.out; then
dnl        $1=Compaq
dnl      else
dnl        $2 --version >conftest.out 2>&1
dnl        if grep -q 'Lahey' conftest.out; then
dnl     	 $1=Lahey
dnl        else
dnl      	 $1=UNKNOWN
dnl        fi
dnl      fi
dnl    fi
dnl    AC_MSG_RESULT([[$]$1])
dnl    rm -f conftest.out
dnl fi])
dnl
dnl --------------------------------------------------------------------
dnl
dnl THO_FORTRAN90_MODULE_FILE(NAME, EXTENSION, COMPILER, EXTENSION)
dnl
AC_DEFUN([THO_FORTRAN90_MODULE_FILE],
[AC_SUBST([$1])
AC_SUBST([$2])
AC_MSG_CHECKING([for Fortran90 module file naming convention])
THO_COMPILE_FORTRAN90([tho_result], [$3], [$4],
  [module module_NAME
     implicit none
     integer, parameter, public :: forty_two = 42
   end module module_NAME], [ok], [], [KEEP])
if test -n "$tho_result"; then
  $1=unknown
  $2=unknown
  for name in module_NAME module_name MODULE_NAME conftest; do
    for ext in m mod M MOD d D; do
      if test -f "$name.$ext"; then
        $1="$name"
        $2="$ext"
        break 2
      fi
    done
  done
  AC_MSG_RESULT([name: [$]$1, extension: .[$]$2 ])
else
  $1=""
  $2=""
  AC_MSG_RESULT([compiler failed])
fi
rm -f conftest* CONFTEST* module_name* module_NAME* MODULE_NAME*])
dnl
dnl --------------------------------------------------------------------
dnl
dnl THO_COMPILE_FORTRAN90(VARIABLE, COMPILER, EXTENSION, MODULE,
dnl                       VALUE_SUCCESS, VALUE_FAILURE, KEEP)
dnl
AC_DEFUN([THO_COMPILE_FORTRAN90],
[cat >conftest.$3 <<__END__
$4
program conftest
  print *, 42
end program conftest
__END__
$2 -o conftest conftest.$3 >/dev/null 2>&1
./conftest >conftest.out 2>/dev/null
if test 42 = "`sed 's/ //g' conftest.out`"; then
  $1="$5"
else
  $1="$6"
fi
if test -z "$7"; then
  rm -f conftest* CONFTEST*
fi])



AC_DEFUN(WK_PROG_FC_F95,
[AC_FC_SRCEXT(f90)
AC_PROG_FC(,[Fortran 95])
])


dnl Determine vendor and version string.
AC_DEFUN(WK_FC_GET_VENDOR,
[AC_CACHE_CHECK([the compiler ID],
[wk_cv_prog_f90_version_string],
[$FC -version >conftest.log 2>&1
$FC -V >>conftest.log 2>&1
$FC --version >>conftest.log 2>&1

wk_grep_f90_NAG=`grep NAG conftest.log | head -1`
wk_grep_f90_Compaq=`grep Compaq conftest.log | head -1`
wk_grep_f90_Digital=`grep DIGITAL conftest.log | head -1`
wk_grep_f90_SGI=`grep MIPS conftest.log | head -1`
wk_grep_f90_Intel=`grep 'Intel(R)' conftest.log | head -1`
wk_grep_f90_Sun=`grep 'Sun' conftest.log | head -1`
wk_grep_f90_Lahey=`grep 'Lahey' conftest.log | head -1`
wk_grep_f90_PGI=`grep 'pgf' conftest.log | head -1`
wk_grep_f90_G95=`grep -i 'g95' conftest.log | grep -i 'gcc' | head -1`
wk_grep_f90_GFORTRAN=`grep -i 'GNU Fortran' conftest.log | head -1`
 
if test -n "$wk_grep_f90_NAG"; then
  wk_cv_prog_f90_type="NAG"
  wk_cv_prog_f90_version_string=$wk_grep_f90_NAG
  wk_cv_prog_f90_version=[`echo $wk_cv_prog_f90_version_string | sed -e 's/.* Release \([0-9][0-9]*\.[0-9][0-9]*.*$\)/\1/'`]
  wk_cv_prog_f90_major_version=[`echo $wk_cv_prog_f90_version | sed -e 's/\([0-9][0-9]*\)\..*/\1/'`]
elif test -n "$wk_grep_f90_Compaq"; then
  wk_cv_prog_f90_type="Compaq"
  wk_cv_prog_f90_version_string=$wk_grep_f90_Compaq
elif test -n "$wk_grep_f90_Digital"; then
  wk_cv_prog_f90_type="DEC"
  wk_cv_prog_f90_version_string=$wk_grep_f90_Digital
elif test -n "$wk_grep_f90_SGI"; then
  wk_cv_prog_f90_type="SGI"
  wk_cv_prog_f90_version_string=$wk_grep_f90_SGI
elif test -n "$wk_grep_f90_Intel"; then
  wk_cv_prog_f90_type="Intel"
  wk_cv_prog_f90_version_string=$wk_grep_f90_Intel
  wk_cv_prog_f90_version=[`echo $wk_cv_prog_f90_version_string | sed -e 's/.* Version \([0-9][0-9]*\.[0-9][0-9]*\) .*/\1/'`]
  wk_cv_prog_f90_major_version=[`echo $wk_cv_prog_f90_version | sed -e 's/\([0-9][0-9]*\)\..*/\1/'`]
elif test -n "$wk_grep_f90_Sun"; then
  wk_cv_prog_f90_type="Sun"
  wk_cv_prog_f90_version_string=$wk_grep_f90_Sun
  wk_cv_prog_f90_version=[`echo $wk_cv_prog_f90_version_string | sed -e 's/.* Fortran 95 \([0-9][0-9]*\.[0-9][0-9]*\) .*/\1/'`]
  wk_cv_prog_f90_major_version=[`echo $wk_cv_prog_f90_version | sed -e 's/\([0-9][0-9]*\)\..*/\1/'`]
elif test -n "$wk_grep_f90_Lahey"; then
  wk_cv_prog_f90_type="Lahey"
  wk_cv_prog_f90_version_string=$wk_grep_f90_Lahey
elif test -n "$wk_grep_f90_PGI"; then
  wk_cv_prog_f90_type="PGI"
  wk_cv_prog_f90_version_string=$wk_grep_f90_PGI
elif test -n "$wk_grep_f90_G95"; then
  wk_cv_prog_f90_type="G95"
  wk_cv_prog_f90_version_string=$wk_grep_f90_G95
elif test -n "$wk_grep_f90_GFORTRAN"; then
  wk_cv_prog_f90_type="GNU"
  wk_cv_prog_f90_version_string=$wk_grep_f90_GFORTRAN
  wk_cv_prog_f90_version=[`echo $wk_cv_prog_f90_version_string | sed -e 's/.*\([0-9][0-9]*\.[0-9][0-9]*\.[0-9][0-9]*\).*/\1/'`]
  wk_cv_prog_f90_major_version=[`echo $wk_cv_prog_f90_version | sed -e 's/\([0-9][0-9]*\)\..*/\1/'`]
else
  wk_cv_prog_f90_type="unknown"
  wk_cv_prog_f90_version_string="unknown"
fi

rm -f conftest.log

])  dnl end AC_CACHE_CHECK


dnl Vendor-specific variables:
AC_CACHE_CHECK([the compiler vendor], [wk_cv_prog_f90_type])

if test -n "$wk_cv_prog_f90_version"; then
  AC_CACHE_CHECK([the compiler version], [wk_cv_prog_f90_version])
else
  wk_cv_prog_f90_version=$wk_cv_prog_f90_version_string
fi

if test -n "$wk_cv_prog_f90_major_version"; then
  AC_CACHE_CHECK([the compiler major version], [wk_cv_prog_f90_major_version])
else
  wk_cv_prog_f90_major_version=$wk_cv_prog_f90_version
fi

FC_VERSION_STRING=$wk_cv_prog_f90_version_string
FC_VENDOR=$wk_cv_prog_f90_type
FC_VERSION=$wk_cv_prog_f90_version
FC_MAJOR_VERSION=$wk_cv_prog_f90_major_version
AC_SUBST(FC_VERSION_STRING)
AC_SUBST(FC_VENDOR)
AC_SUBST(FC_VERSION)
AC_SUBST(FC_MAJOR_VERSION)

dnl Module names: (all compilers apparently have converged to '.mod')
dnl The perl scripts need a quoted version of this
FC_MODNAME='$(1:.o=.mod)'
FC_MODNAME_Q='\$(1:.o=.mod)'
AC_SUBST(FC_MODNAME)
AC_SUBST(FC_MODNAME_Q)

])  dnl end AC_DEFUN


dnl Check for basic F95 features
AC_DEFUN(WK_FC_CHECK_F95,
[AC_CACHE_CHECK([whether this compiler supports Fortran 95 features],
[wk_cv_prog_f95_standard],
[AC_LANG(Fortran)
AC_COMPILE_IFELSE([
program conftest
   integer, dimension(2) :: ii
   integer :: i
   type :: foo
      integer, pointer :: bar => null ()
   end type foo
   forall (i = 1:2)  ii(i) = i
  contains
   elemental function f(x)
     real, intent(in) :: x
     real :: f
     f = x
   end function f
 end program conftest
],
wk_cv_prog_f95_standard=yes,
[AC_MSG_RESULT(no)
AC_MSG_FAILURE([Compiler does not support the Fortran 95 standard.])
])
FC_SUPPORTS_F95=$wk_cv_prog_f95_standard
AC_SUBST(FC_SUPPORTS_F95)
])
])
 

dnl Check for the TR15581 extensions (allocatable subobjects)
AC_DEFUN(WK_FC_CHECK_TR15581,
[AC_CACHE_CHECK([whether this compiler supports allocatable subobjects],
[wk_cv_prog_f95_allocatable],
[AC_LANG(Fortran)
AC_COMPILE_IFELSE([
program conftest
  type :: foo
     integer, dimension(:), allocatable :: bar
  end type foo
end program conftest
],
wk_cv_prog_f95_allocatable=yes,
wk_cv_prog_f95_allocatable=no)
FC_SUPPORTS_ALLOCATABLE=$wk_cv_prog_f95_allocatable
AC_SUBST(FC_SUPPORTS_ALLOCATABLE)
])
])

dnl Check for the C bindings extensions of Fortran 2003
AC_DEFUN(WK_FC_CHECK_C_BINDING,
[AC_CACHE_CHECK([whether this compiler supports C binding (Fortran 2003)],
[wk_cv_prog_f95_c_binding],
[AC_LANG(Fortran)
AC_COMPILE_IFELSE([
program conftest
  use iso_c_binding
  type, bind(c) :: t
     integer(c_int) :: i
     real(c_float) :: x
  end type t
end program conftest
],
wk_cv_prog_f95_c_binding=yes,
wk_cv_prog_f95_c_binding=no)
FC_SUPPORTS_ALLOCATABLE=$wk_cv_prog_f95_c_binding
AC_SUBST(FC_SUPPORTS_C_BINDING)
])
])


dnl Check for the command line interface of Fortran 2003
dnl We actually have to link in order to check availability
AC_DEFUN(WK_FC_CHECK_CMDLINE,
[AC_CACHE_CHECK([whether this compiler interfaces the command line (F2003)],
[wk_cv_prog_f95_cmdline],
[AC_LANG(Fortran)
AC_LINK_IFELSE([
program conftest
  call get_command_argument (command_argument_count ())
end program conftest
],
wk_cv_prog_f95_cmdline=yes,
wk_cv_prog_f95_cmdline=no)
FC_SUPPORTS_CMDLINE=$wk_cv_prog_f95_cmdline
AC_SUBST(FC_SUPPORTS_CMDLINE)
])
])


dnl Check for quadruple precision support (real and complex!)
dnl If quadruple is actually required but not supported, report failure.
AC_DEFUN(WK_FC_CHECK_QUADRUPLE,
[AC_CACHE_CHECK([whether this compiler permits quadruple real and complex],
[wk_cv_prog_f95_quadruple],
[AC_LANG(Fortran)
AC_COMPILE_IFELSE([
program conftest
   integer, parameter :: d=selected_real_kind(precision(1.)+1, range(1.)+1)
   integer, parameter :: q=selected_real_kind(precision(1._d)+1, range(1._d))
   real(kind=q) :: x
   complex(kind=q) :: z
end program conftest
], 
wk_cv_prog_f95_quadruple=yes,
if test "$FC_PRECISION" = "quadruple"; then
[AC_MSG_RESULT(no)
AC_MSG_FAILURE([Quadruple precision requested but not supported])]
else
wk_cv_prog_f95_quadruple=no
fi)
])
FC_SUPPORTS_QUADRUPLE=$wk_cv_prog_f95_quadruple
AC_SUBST(FC_SUPPORTS_QUADRUPLE)
if test "$FC_SUPPORTS_QUADRUPLE" = "yes"; then
  FC_QUAD_OR_SINGLE="quadruple"
else
  FC_QUAD_OR_SINGLE="single"
fi
AC_SUBST(FC_QUAD_OR_SINGLE)
])


dnl Check for profiling support
AC_DEFUN(WK_FC_CHECK_PROFILING,
[AC_CACHE_CHECK([whether this compiler supports profiling via -pg],
[wk_cv_prog_fc_profiling],
[AC_LANG(Fortran)
fcflags_tmp=$FCFLAGS
FCFLAGS="-pg $FCFLAGS"
rm -f gmon.out
AC_RUN_IFELSE([
program conftest
end program conftest
],
if test -f gmon.out; then
  wk_cv_prog_fc_profiling=yes
else
  wk_cv_prog_fc_profiling=no
fi,
wk_cv_prog_fc_profiling=no,
wk_cv_prog_fc_profiling="maybe [cross-compiling]")
rm -f gmon.out
FCFLAGS=$fcflags_tmp
FC_SUPPORTS_PROFILING=$wk_cv_prog_fc_profiling
AC_SUBST(FC_SUPPORTS_PROFILING)
])
])


dnl ------------------------------------------------------------------------
dnl Determine runtime libraries for additional Fortran compilers
dnl Since the results would interfere with the settings for the
dnl default Fortran compiler, do this by separate configure scripts
dnl in a subdirectory.

dnl WK_FC_LIBRARY_LDFLAGS(compiler-list)
dnl Take a list of Fortran compilers and determine the runtime libraries
dnl needed for them.  The result is concatenated to make up FCLIBS
AC_DEFUN(WK_FC_LIBRARY_LDFLAGS,
[if test -n "$1"; then
AC_MSG_CHECKING([for extra Fortran runtime libraries])
AC_MSG_RESULT(($1))
for comp in $1; do
  cd config.scripts
  sh configure.flibs FC=$comp
  wk_fclibs=`cat configure.flibs.out`
  FCLIBS="$FCLIBS $wk_fclibs"
  cd ..
done
fi
])



dnl ------------------------------------------------------------------------
dnl Fortran library tests

dnl Extension of AC_PATH_PROG: Search for libraries for which the name
dnl is not exactly known (because it may have the version number in it)
dnl Set output variables $var, $var_DIR, $var_LIB accordingly
dnl WHIZARD_PATH_LIB(var, id, name-list, path)
AC_DEFUN(WHIZARD_PATH_LIB,
[AC_CACHE_CHECK(for $3, wk_cv_path_$1,
[case "$$1" in
  /*)
  wk_cv_path_$1="$$1" # User-supplied path
  ;;
  *)
  IFS="${IFS= 	}"; ac_save_ifs="$IFS"; IFS=":"
  ac_dummy=$4
  for ac_dir in $ac_dummy; do
    test -z "$ac_dir" && ac_dir=.
    unset wk_cv_path_$1
    ac_pwd=`pwd`
    if test -d "$ac_dir"; then
      cd $ac_dir
      for ac_word in $3; do
        test -f "$ac_word" && wk_cv_path_$1="$ac_dir/$ac_word"
      done
      cd $ac_pwd
    fi
    if test -n "$wk_cv_path_$1"; then
      break
    fi
  done
  IFS="$ac_save_ifs"
  if test -z "$wk_cv_path_$1"; then
    wk_cv_path_$1="no"
  fi
  ;;
esac
])
$1=$wk_cv_path_$1
if test "$$1" != "no"; then
$1_DIR=`echo $$1 | sed -e 's|/lib$2.*\.a$||'`
$1_LIB=`echo $$1 | sed -e 's|^.*/lib\($2.*\)\.a$|\1|'`
fi
AC_SUBST($1)
AC_SUBST($1_DIR)
AC_SUBST($1_LIB)
])


dnl Library search with enable option, enabled by default
dnl WHIZARD_LIB_ENABLE(var, id, help, name-list, path)
AC_DEFUN(WHIZARD_LIB_ENABLE,
[AC_ARG_ENABLE($2,[$3])
if test "$enable_$2" = "no"; then
AC_MSG_CHECKING(for $4)
$1="no"
AC_MSG_RESULT([(disabled)])
else
WHIZARD_PATH_LIB($1, $2, $4, $5)
fi
])


dnl Library search with enable option, disabled by default
dnl WHIZARD_LIB_DISABLE(var, id, help, name-list, path)
AC_DEFUN(WHIZARD_LIB_DISABLE,
[AC_ARG_ENABLE($2,[$3])
if test "$enable_$2" = "yes"; then
WHIZARD_PATH_LIB($1, $2, $4, $5)
else
AC_MSG_CHECKING(for $4)
$1="no"
AC_MSG_RESULT([(disabled)])
fi
])

dnl Library search depending on another feature being enabled
dnl WHIZARD_LIB_IF_ENABLED(var, id, enable_var, name-list, path)
AC_DEFUN(WHIZARD_LIB_IF_ENABLED,
[if test "$3" != "no"; then
WHIZARD_PATH_LIB($1, $2, $4, $5)
fi
])


dnl Check whether the Fortran compiler can link to a library.
dnl We try to call a subroutine that is defined by the library.
dnl If successful, prepend the library(s) in question to LIBS.
dnl WHIZARD_CHECK_LIB_FC(lib, lib-basename, lib-dir, fun, extra-libs)
AC_DEFUN(WHIZARD_CHECK_LIB_FC,
[if test "$$1" != "no"; then
wk_otherlibs="-L$3 $5"
AC_LANG(Fortran)
AC_CHECK_LIB($2,$4,
[LIBS="-l$2 $wk_otherlibs $LIBS"],
[$1="no"
AC_MSG_WARN([*** Linking library $2 failed (see config.log for details) ***])],
$wk_otherlibs)
fi
])

dnl ------------------------------------------------------------------------
dnl C compiler tests (CompHEP only)

AC_DEFUN(WK_PROG_COMPHEP_CC_WORKS,
[AC_CACHE_CHECK([whether this compiler works], 
wk_cv_prog_COMPHEP_CC_works,
[CC=$COMPHEP_PATH/$COMPHEP_CC
unset CFLAGS
unset CPPFLAGS
AC_LANG(C)
AC_COMPILE_IFELSE([AC_LANG_PROGRAM(,[[;]])],
wk_cv_prog_COMPHEP_CC_works=yes,
[AC_MSG_RESULT(no)
AC_MSG_FAILURE([Comphep C compiler failed])])
])
])


dnl ------------------------------------------------------------------------
dnl Further tools

dnl Test for GNU tar:
dnl
dnl WK_PROG_GNU_TAR()
AC_DEFUN(WK_PROG_GNU_TAR,
[AC_CACHE_CHECK([for GNU tar], [wk_cv_prog_GTAR],
[wk_cv_prog_GTAR=$GTAR
test -z "$wk_cv_prog_GTAR" && wk_cv_prog_GTAR=tar
if test -n "`$wk_cv_prog_GTAR --version 2>/dev/null | grep GNU`"; then
  :
elif test -n "`gtar --version 2>/dev/null | grep GNU`"; then
  wk_cv_prog_GTAR=gtar
else
  wk_cv_prog_GTAR=no
fi
])
GTAR=$wk_cv_prog_GTAR
])

dnl Test for GNU make:
dnl
dnl WK_PROG_GNU_MAKE()
AC_DEFUN(WK_PROG_GNU_MAKE,
[AC_CACHE_CHECK([for GNU make], [wk_cv_prog_GMAKE],
[wk_cv_prog_GMAKE="$GMAKE"  # User-supplied command
test -z "$wk_cv_prog_GMAKE" && wk_cv_prog_GMAKE="make"
if test -n "`$wk_cv_prog_GMAKE --version 2>/dev/null | grep GNU`"; then
  :
elif test -n "`gmake --version 2>/dev/null | grep GNU`"; then
  wk_cv_prog_GMAKE=gmake
else
  wk_cv_prog_GMAKE=no
fi
])
GMAKE=$wk_cv_prog_GMAKE
])


dnl Test whether Metapost works and can dump a .mem file
dnl
dnl WK_PROG_MPOST_WORKS()
AC_DEFUN(WK_PROG_MPOST_WORKS,
[AC_CACHE_CHECK([whether Metapost ($MPOST) works], [wk_cv_prog_mpost_works],
[touch conftest.mp
echo dump | $MPOST --ini conftest >/dev/null 2>&1
if test -f conftest.mem; then
  wk_cv_prog_mpost_works="yes"
else
  wk_cv_prog_mpost_works="no"
fi
rm -f conftest.mp conftest.mem conftest.log

if test $wk_cv_prog_mpost_works = "no"; then
  AC_MSG_ERROR([installation or configuration problem: METAPOST cannot create .mem files.])
fi
dnl AC_MSG_RESULT($wk_cv_prog_mpost_works)
])])


dnl
dnl --------------------------------------------------------------------
dnl --------------------------------------------------------------------
dnl
dnl THO_PATH_PROGS(NAME, PROGS, ARGS)
dnl
dnl   Make sure that a program can be run
dnl   (I had a problem with ocamlc.opt being compiled but
dnl   dumping core on an Alpha)
dnl
AC_DEFUN([THO_PATH_PROGS],
[for name in $2; do
   AC_PATH_PROGS($1,[$]name)
   if test -n "[$]$1"; then
     [$]$1 $3 >/dev/null 2>&1 && break
     unset $1
     unset ac_cv_path_$1
   fi
 done])
dnl
dnl --------------------------------------------------------------------
dnl --------------------------------------------------------------------
dnl
AC_DEFUN([THO_OCAML_BASE],
[AC_PATH_PROGS(OCAML,ocaml)
THO_PATH_PROGS(OCAMLC,ocamlc.opt ocamlc,</dev/null)
THO_PATH_PROGS(OCAMLOPT,ocamlopt.opt ocamlopt,</dev/null)
dnl THO_PATH_PROGS(OCAMLLEX,ocamllex.opt ocamllex,</dev/null)
AC_PATH_PROGS(OCAMLLEX,ocamllex)
AC_PATH_PROGS(OCAMLYACC,ocamlyacc)
AC_PATH_PROGS(OCAMLMKTOP,ocamlmktop)
AC_PATH_PROGS(OCAMLCP,ocamlcp)
AC_PATH_PROGS(OCAMLDEP,ocamldep)])
dnl
dnl --------------------------------------------------------------------
dnl
AC_DEFUN([THO_OCAML_LIBDIR],
[AC_REQUIRE([AC_PROG_AWK])
AC_REQUIRE([THO_OCAML_BASE])
AC_SUBST(OCAML_LIBDIR)
if test -n "$OCAMLC"; then
  AC_MSG_CHECKING([for OCaml library directory])
  AC_CACHE_VAL([tho_cv_ocaml_libdir],
    [tho_cv_ocaml_libdir="`$OCAMLC -v | $AWK 'NR==2 {print [$]4}'`"
    if test -f $tho_cv_ocaml_libdir/stdlib.cma; then
      :
    elif test -f /usr/local/lib/ocaml/stdlib.cma; then
      tho_cv_ocaml_libdir=/usr/local/lib/ocaml
    else
      tho_cv_ocaml_libdir=""
    fi])
  OCAML_LIBDIR="$tho_cv_ocaml_libdir"
  if test -n "$OCAML_LIBDIR"; then
    AC_MSG_RESULT([$OCAML_LIBDIR])
  else
    AC_MSG_RESULT([not found])
  fi
fi])
dnl
dnl --------------------------------------------------------------------
dnl
dnl NB: strip the "+n (date)" for CVS versions
dnl
AC_DEFUN([THO_OCAML_VERSION],
[AC_REQUIRE([AC_PROG_AWK])
AC_REQUIRE([THO_OCAML_BASE])
AC_SUBST(OCAML_VERSION)
if test -n "$OCAMLC"; then
  AC_MSG_CHECKING([for OCaml version])
  AC_CACHE_VAL([tho_cv_ocaml_version],
    [tho_cv_ocaml_version="`$OCAMLC -v | \
      $AWK 'NR==1 && [$]5 ~ /version/ {
        changequote(<<,>>)dnl
          split (<<$>>6, version, "[.+]+");
          printf ("%d%02d%03d", version[1], version[2], version[3])}'`"
        changequote([,])])
  OCAML_VERSION=$tho_cv_ocaml_version
  if test -n "$OCAML_VERSION"; then
    AC_MSG_RESULT([$OCAML_VERSION])
  else
    AC_MSG_RESULT([not found])
    OCAML_VERSION="0"
  fi
fi])
dnl
dnl --------------------------------------------------------------------
dnl
AC_DEFUN([THO_OCAML_REQUIRE_VERSION],
[AC_REQUIRE([THO_OCAML_VERSION])
AC_MSG_CHECKING([for OCaml version $1])
if test "$OCAML_VERSION" -ge "$1"; then
   AC_MSG_RESULT([ok])
else
   AC_MSG_ERROR([found version $OCAML_VERSION])
fi])
dnl
dnl --------------------------------------------------------------------
dnl
AC_DEFUN([THO_OCAML_LABLGTK],
[AC_REQUIRE([THO_OCAML_BASE])
AC_SUBST(LABLGTKDIR)
LABLGTKDIR=$OCAML_LIBDIR
AC_MSG_CHECKING([for OCaml/GTK+ toolkit directory])
if test -f $LABLGTKDIR/lablgtk.cma; then
  AC_MSG_RESULT([$LABLGTKDIR])
else
  LABLGTKDIR=$OCAML_LIBDIR/lablgtk
  if test -f $LABLGTKDIR/lablgtk.cma; then
    AC_MSG_RESULT([$LABLGTKDIR])
  else
    AC_MSG_RESULT([not found])
  fi
fi])
dnl
dnl --------------------------------------------------------------------
dnl --------------------------------------------------------------------
dnl
AC_DEFUN([THO_OCAMLWEB],[AC_PATH_PROGS(OCAMLWEB,ocamlweb)])
dnl
dnl --------------------------------------------------------------------
dnl
AC_DEFUN([THO_OCAMLWEB_VERSION],
[AC_REQUIRE([AC_PROG_AWK])
AC_REQUIRE([THO_OCAMLWEB])
AC_SUBST(OCAMLWEB_VERSION)
if test -n "$OCAMLWEB"; then
  AC_MSG_CHECKING([for ocamlweb version])
  AC_CACHE_VAL([tho_cv_ocamlweb_version],
    [tho_cv_ocamlweb_version="`$OCAMLWEB --version 2>&1 |
      $AWK 'NR==1 && [$]4 ~ /version/ {
	changequote(<<,>>)dnl
	  split (<<$>>5, version, "[.+]+");
	  printf ("%d%02d%03d", version[1], version[2], version[3])}'`"
	changequote([,])])
  OCAMLWEB_VERSION=$tho_cv_ocamlweb_version
  if test -n "$OCAMLWEB_VERSION"; then
    AC_MSG_RESULT([$OCAMLWEB_VERSION])
  else
    AC_MSG_RESULT([not found])
    OCAMLWEB_VERSION="0"
  fi
fi])
dnl
dnl --------------------------------------------------------------------
dnl
AC_DEFUN([THO_OCAMLWEB_REQUIRE_VERSION],
[AC_REQUIRE([THO_OCAMLWEB_VERSION])
AC_MSG_CHECKING([for ocamlweb version $1])
if test "$OCAMLWEB_VERSION" -ge "$1"; then
   AC_MSG_RESULT([ok])
else
   AC_MSG_RESULT([too old])
   OCAMLWEB=false
fi])
dnl
dnl --------------------------------------------------------------------

