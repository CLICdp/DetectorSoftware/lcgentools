subroutine shower_spectator_electron
  use kinds, only: double 
  use pyjets_common
  implicit none
  !
  integer :: i
  integer :: ip1
  integer :: ip2

  !...Call PYHEPC to convert input from HEPEVT to PYJETS common.
  call pyhepc(2)
  ip1=0
  ip2=0
  loop_i: do i=1,n
     check_for_fermion: if(k(i,1).eq.1.and.  &
          &     (abs(k(i,2)).eq.11.or.abs(k(i,2)).eq.12).and.abs(p(i,1)).lt.epsilon(0.d0).and.abs(p(i,2)).lt.epsilon(0.d0)) then
        check_ip1: if(ip1.eq.0) then
           ip1=i
        else check_ip1
           check_k_prod: if(k(ip1,2)*k(i,2).lt.0) then
              ip2=i
              exit loop_i
           end if check_k_prod
        end if check_ip1
        !
     end if check_for_fermion
     !
  end do loop_i



  check_ip12: if(ip1.gt.0.and.ip2.gt.0.and.ip2-ip1.le.2) then

     call pyshow(ip1,-(ip2-ip1+1),sqrt(max(epsilon(0.d0),(p(ip1,4)+p(ip2,4))**2  &
          &   -(p(ip1,1)+p(ip2,1))**2-(p(ip1,2)+p(ip2,2))**2-(p(ip1,3)+p(ip2,3))**2)))

     !...Call PYHEPC to convert output from PYJETS to HEPEVT common.
     call pyhepc(1)
  end if check_ip12

  return
end subroutine shower_spectator_electron

