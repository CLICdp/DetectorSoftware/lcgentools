subroutine ilc_fragment_init(seed,pythia_parameters,sqrts)
  use kinds, only: double 
  use a6f_users
  use pydat1_common
  use pydat2_common
  use pydat3_common
  use pydatr_common
  use pyint1_common
  use pypars_common
  use pyjets_common
  use jaki_include_common
  use total_cross_mod
  use diagnostics, only: msg_level
  implicit none
  integer, intent(in)                         :: seed
  character(len=1000), intent(in)             :: pythia_parameters
  real(kind=double), intent(in)               :: sqrts


  integer                                     :: i0
  integer                                     :: isemi

  real(kind=double)                           :: sqrts_mstp171

  integer ::  recl_inquire
  logical,save :: tauolaini=.false.

  external pydata
  external pystrf
  external pyprep


  pmas(6,1)=174.d0
  pmas(6,2)=1.523d0


  mrpy(1)=seed
  mrpy(2)=0



  check_pythia_parameters: if (pythia_parameters /= ' ') then 
     i0=1
     print *, " before loop_pythia i0,pythia_parameters(i0:)= ",i0,pythia_parameters(i0:)
     loop_pythia: do
        isemi=scan(pythia_parameters(i0:),';')
        check_isemi: if(isemi.eq.0) then
           print *, " i0,pythia_parameters(i0:)= ",i0,pythia_parameters(i0:)
           call pygive(pythia_parameters(i0:))
           exit loop_pythia
        else check_isemi
           print *, " i0,isemi,pythia_parameters(i0:i0+isemi-2)= ", i0,isemi,pythia_parameters(i0:i0+isemi-2)
           call pygive(pythia_parameters(i0:i0+isemi-2))
           i0=i0+isemi
        end if check_isemi
     end do loop_pythia
  end if check_pythia_parameters

  check_mstp171: if(mstp(171).eq.1) then
     sqrts_mstp171=max(sqrt_shat_total_cross_min,0.01*sqrts,1.2*parp(2))
     p(1:2,:)=0.d0
!     p(1:2,3)=0.5d0*sqrts
     p(1:2,3)=0.5d0*sqrts_mstp171
     p(2,3)=-p(2,3)
!     call pyinit('3MOM', 'gamma', 'gamma', sqrts)
     call pyinit('3MOM', 'gamma', 'gamma', sqrts_mstp171)
  else check_mstp171
     call pyinit('NONE', ' ', ' ', 0.d0)
  end if check_mstp171

  !   brat(210:226)=0.d0
  !   brat(214)=1.d0
  !   brat(210)=0.d0 ! h --> d dbar
  !   brat(212)=0.0005443004d0 ! h --> s sbar
  !   brat(213)=0.0332701310d0 ! h --> c cbar
  !   brat(214)=0.7314012270d0 ! h --> b bbar
  !   brat(219)=0.0002545201d0 ! h --> mu+ mu-
  !   brat(220)=0.0733422656d0 ! h --> tau+ tau-
  !   brat(222)=0.0695911752d0 ! h --> gluon gluon
  !   brat(223)=0.0020645968d0 ! h --> gamma gamma
  !   brat(224)=0.0006817862d0 ! h --> gamma Z0
  !   brat(225)=0.0081519021d0 ! h --> Z0 Z0
  !   brat(226)=0.0806980953d0 ! h --> W+ W-

  !   mdme(224,1)=0  ! h --> gamma Z0
  !   mdme(225,1)=0  ! h --> Z0 Z0
  !   mdme(226,1)=0  ! h --> W+ W-

  vint(44)=1.d0

  if ( msg_level > 2 ) call pylist(12)


  print "(' seed=',i12)", seed
  call create(mcu,(/.false.,.false./),(/0.d0,0.d0/),(/0,0/),sqrts)

  if (  mstj(28) > 0 ) then
    if ( .not. tauolaini  ) then
      CALL TAUOLA(-1,1)
      tauolaini=.true.
    end if
    jak1=mstp(198)
    jak2=mstp(199)
    print *,"INIT TAUOLA user fragment init jak1,jak2= ",jak1,jak2
  endif
  return 

end subroutine ilc_fragment_init
