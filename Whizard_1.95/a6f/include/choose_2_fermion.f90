  subroutine choose_2_fermion(fermion_mask)
  use kinds, only: double 
  use hepevt_include_common
  use pydat2_common
  implicit none
  logical, dimension(8), intent(out) :: fermion_mask
!
  real(kind=double), dimension(5) :: p156
  real(kind=double), dimension(5) :: p278
  real(kind=double), dimension(5) :: p356
  real(kind=double), dimension(5) :: p478
  real(kind=double), parameter :: sigmax=3.d0
  real(kind=double)               :: ch12
  real(kind=double)               :: ch34
  integer :: i
  integer :: nfermion
  integer, dimension(8) :: ifermion
  nfermion=0
  fermion_mask(1:2)=.true.
  fermion_mask(3:8)=.false.
  loop_i: do i=1,nhep
     check_for_fermion: if((isthep(i).eq.1.and.jmohep(1,i).eq.0).and.(  &
          &     (abs(idhep(i)).ge.1.and.abs(idhep(i)).le.5)  &
          & .or.(abs(idhep(i)).ge.11.and.abs(idhep(i)).le.16))) then
!
        nfermion=nfermion+1
        check_nfermion: if(nfermion.le.8) then
           ifermion(nfermion)=i
        else check_nfermion
           print "(' from subroutine choose_2_fermion  pt 1 unexpected nfermion=',i12/' program will now stop')", nfermion
           stop
        end if check_nfermion
!
     end if check_for_fermion
!
  end do loop_i

  check_nfermion8: if(nfermion.ne.8) then
  print "(' from subroutine choose_2_fermion  pt 2 unexpected nfermion=',i12/' program will now stop')", nfermion
  stop
  end if check_nfermion8
  
  if(idhep(ifermion(1)).ne.5) return
  if(idhep(ifermion(2)).ne.-5) return
  if(idhep(ifermion(3)).ne.5) return
  if(idhep(ifermion(4)).ne.-5) return
  if(.not.( &
   &     (idhep(ifermion(5)).eq.2.and.idhep(ifermion(6)).eq.-1)  &
   & .or.(idhep(ifermion(5)).eq.4.and.idhep(ifermion(6)).eq.-3)  &
   & .or.(idhep(ifermion(5)).eq.12.and.idhep(ifermion(6)).eq.-11)  &
   & .or.(idhep(ifermion(5)).eq.14.and.idhep(ifermion(6)).eq.-13)  &
   & .or.(idhep(ifermion(5)).eq.16.and.idhep(ifermion(6)).eq.-15))) return
  if(.not.( &
   &     (idhep(ifermion(7)).eq.1.and.idhep(ifermion(8)).eq.-2)  &
   & .or.(idhep(ifermion(7)).eq.3.and.idhep(ifermion(8)).eq.-4)  &
   & .or.(idhep(ifermion(7)).eq.11.and.idhep(ifermion(8)).eq.-12)  &
   & .or.(idhep(ifermion(7)).eq.13.and.idhep(ifermion(8)).eq.-14)  &
   & .or.(idhep(ifermion(7)).eq.15.and.idhep(ifermion(8)).eq.-16))) return
 
  p156(1:4)=phep(1:4,ifermion(1))+phep(1:4,ifermion(5))+phep(1:4,ifermion(6))
  p278(1:4)=phep(1:4,ifermion(2))+phep(1:4,ifermion(7))+phep(1:4,ifermion(8))
  p156(5)=max(0.d0,sqrt(p156(4)**2-sum(p156(1:3)**2)))
  p278(5)=max(0.d0,sqrt(p278(4)**2-sum(p278(1:3)**2)))

  p356(1:4)=phep(1:4,ifermion(3))+phep(1:4,ifermion(5))+phep(1:4,ifermion(6))
  p478(1:4)=phep(1:4,ifermion(4))+phep(1:4,ifermion(7))+phep(1:4,ifermion(8))
  p356(5)=max(0.d0,sqrt(p356(4)**2-sum(p356(1:3)**2)))
  p478(5)=max(0.d0,sqrt(p478(4)**2-sum(p478(1:3)**2)))

  ch12=(p156(5)-pmas(6,1))**2+(p278(5)-pmas(6,1))**2
  ch34=(p356(5)-pmas(6,1))**2+(p478(5)-pmas(6,1))**2

  if(ch12.gt.ch34) return

  fermion_mask(1:2)=.false.
  fermion_mask(3:4)=.true.
  fermion_mask(5:8)=.false.

  return
  end subroutine choose_2_fermion

