module ilc_calc_nfermion_mod
  use kinds, only: double
  use hepevt_include_common
  implicit none

  private

  public :: ilc_calc_nfermion
  public :: ilc_restore_documentation_particles

  type :: hepevt_copy
     integer :: nhep
     integer, dimension(nmxhep) :: isthep, idhep
     integer, dimension(2, nmxhep) :: jmohep, jdahep
     real(kind=double), dimension(5, nmxhep) :: phep
     real(kind=double), dimension(4, nmxhep) :: vhep
  end type hepevt_copy


  type(hepevt_copy) :: hepevt_input


  integer, dimension(nmxhep) :: jmo_hep_out
  integer, dimension(-16:16) :: charge_quark
  integer, dimension(2,5) :: i_wplus
  integer, dimension(2,9) :: ifc_wplus
  integer, dimension(:), pointer :: ifermion
  integer, dimension(:), pointer :: ifn
  logical, dimension(:), pointer :: ffn
  integer :: m1
  integer :: m2
  integer :: m3
  integer :: nhep_out
  integer :: nfermion
  integer :: ncall=0


contains

  subroutine ilc_calc_nfermion(nfermion_out)
    integer, intent(out) :: nfermion_out
    integer :: i
    integer :: j
    logical :: f_ww
    logical :: f_zz
    logical :: fc_zz
    logical :: fc_ww
    logical :: f_bbww
    logical :: f_zww
    logical :: f_zzz
    logical :: fc_zzz
    logical :: fc_zww
    logical :: fzc_zww
    logical :: fzcwc_zww


    ncall=ncall+1
    if(ncall.eq.1) call calc_nfermion_ini


    hepevt_input%nhep=nhep
    hepevt_input%isthep(1:nhep)=isthep(1:nhep)
    hepevt_input%idhep(1:nhep)=idhep(1:nhep)
    hepevt_input%jmohep(:,1:nhep)=jmohep(:,1:nhep)
    hepevt_input%jdahep(:,1:nhep)=jdahep(:,1:nhep)
    hepevt_input%phep(:,1:nhep)=phep(:,1:nhep)
    hepevt_input%vhep(:,1:nhep)=vhep(:,1:nhep)


    jmohep(:,1:nhep)=0
    nfermion=0
    nhep_out=0
    allocate (ifermion(nhep))
    loop_i: do i=1,nhep
       check_isthep: if(isthep(i).eq.1) then
          check_idhep: if(  &
               &     (abs(idhep(i)).ge.1.and.abs(idhep(i)).le.5)  &
               & .or.(abs(idhep(i)).ge.11.and.abs(idhep(i)).le.16)) then
             nhep_out=nhep_out+1
             nfermion=nfermion+1
             ifermion(nfermion)=nhep_out
             isthep(i)=1
             jmohep(1,i)=i
             jdahep(:,i)=0
             hepevt_input%isthep(i)=2
             call copy_hepevt_entry(i,nhep_out)
          else check_idhep
             nhep_out=nhep_out+1
             jmohep(1,i)=i
             jdahep(:,i)=0
             hepevt_input%jdahep(:,i)=hepevt_input%nhep+nhep_out
             hepevt_input%isthep(i)=2
             call copy_hepevt_entry(i,nhep_out)
          end if check_idhep
       end if check_isthep
    end do loop_i

    nhep=nhep_out
    nfermion_out=nfermion

    f_ww=.false.
    f_zz=.false.
    fc_zz=.false.
    fc_ww=.false.
    f_bbww=.false.
    f_zww=.false.
    f_zzz=.false.
    fc_zzz=.false.
    fc_zww=.false.
    fzc_zww=.false.
    fzcwc_zww=.false.

    check_nfermion: if(nfermion.eq.2.or.nfermion.eq.4.or.nfermion.eq.6) then
       allocate (ifn(nfermion))
       allocate (ffn(nfermion))
       test_nfermion: select case(nfermion)
       case(2) test_nfermion
          check_id_2: if(idhep(ifermion(1)).gt.0) then
             ifn(1)=1
             ifn(2)=2
          else check_id_2
             ifn(1)=2
             ifn(2)=1
          end if check_id_2
       case(4) test_nfermion

          call try_ww(i_wplus,f_ww)
          check_f_ww: if(.not.f_ww) then
             call try_zz(f_zz,.false.)
             check_f_zz: if(.not.f_zz) then
                call try_ww(ifc_wplus,fc_ww)
                check_fc_ww: if(.not.fc_ww) then
                   call try_zz(fc_zz,.true.)
                   check_fc_zz: if(.not.fc_ww) then
                      print *, " from subroutine ilc_calc_nfermion, f_ww,f_zz,fc_ww,fc_zz=", f_ww,f_zz,fc_ww,fc_zz
                      print *, " program will now stop"
                      call pyhepc(2)
                      call heplst(1)
                      call pylist(2)
                      stop
                   end if check_fc_zz
                end if check_fc_ww
             end if check_f_zz
          end if check_f_ww

       case(6) test_nfermion

          call try_bbww(i_wplus,f_bbww)
          check_f_bbww: if(.not.f_bbww) then
             call try_zww(i_wplus,f_zww,.false.)
             check_f_zww: if(.not.f_zww) then
                call try_zzz(f_zzz,.false.)
                check_f_zzz: if(.not.f_zzz) then
                   call try_zww(ifc_wplus,fc_zww,.false.)
                   check_fc_zww: if(.not.fc_zww) then
                      call try_zww(i_wplus,fzc_zww,.true.)
                      check_fzc_zww: if(.not.fzc_zww) then
                         call try_zzz(fc_zzz,.true.)
                         check_fc_zzz: if(.not.fc_zzz) then
                            call try_zww(ifc_wplus,fzcwc_zww,.true.)
                            check_fzcwc_zww: if(.not.fzcwc_zww) then
                               print *, " from subroutine ilc_calc_nfermion, f_bbww,f_zww,f_zzz,fc_zww,fzc_zww,fc_zzz,fzcwc_zww=", f_bbww,f_zww,f_zzz,fc_zww,fzc_zww,fc_zzz,fzcwc_zww
                               print *, " program will now stop"
                               call pyhepc(2)
                               call heplst(1)
                               call pylist(2)
                               stop
                            end if check_fzcwc_zww
                         end if check_fc_zzz
                      end if check_fzc_zww
                   end if check_fc_zww
                end if check_f_zzz
             end if check_f_zww
          end if check_f_bbww

       end select test_nfermion
       call order_ifn
       deallocate (ffn)
       deallocate (ifn)
    end if check_nfermion

    loop_jmo_hep: do i=1,nhep
       jmo_hep_out(i)=jmohep(1,i)
       jmohep(:,i)=0
    end do loop_jmo_hep

    deallocate(ifermion)
  end subroutine ilc_calc_nfermion


  subroutine calc_nfermion_ini

    integer :: i

    i_wplus(1,1)=12
    i_wplus(2,1)=-11
    i_wplus(1,2)=14
    i_wplus(2,2)=-13
    i_wplus(1,3)=16
    i_wplus(2,3)=-15
    i_wplus(1,4)=2
    i_wplus(2,4)=-1
    i_wplus(1,5)=4
    i_wplus(2,5)=-3

    ifc_wplus(:,1:5)=i_wplus
    ifc_wplus(1,6)=2
    ifc_wplus(2,6)=-3
    ifc_wplus(1,7)=2
    ifc_wplus(2,7)=-5
    ifc_wplus(1,8)=4
    ifc_wplus(2,8)=-1
    ifc_wplus(1,9)=4
    ifc_wplus(2,9)=-5

    charge_quark=1000
    charge_quark(1)=-1
    charge_quark(2)=2
    charge_quark(3)=-1
    charge_quark(4)=2
    charge_quark(5)=-1
    charge_quark(6)=2
    charge_quark(11)=3
    charge_quark(12)=4
    charge_quark(13)=5
    charge_quark(14)=6
    charge_quark(15)=7
    charge_quark(16)=8
    loop_charge_quark: do i=1,6
       charge_quark(-i)=-charge_quark(i)
       charge_quark(-(10+i))=-charge_quark(10+i)
    end do loop_charge_quark

  end subroutine calc_nfermion_ini

  subroutine try_zww(i_wgen,f_match,f_fcnc)
    integer, dimension(:,:), intent(in) :: i_wgen
    logical, intent(out) :: f_match
    logical, intent(in) :: f_fcnc
    logical :: f_wboson
    logical :: f_zboson
    integer :: k3a
    f_match=.false.
    loop_i_6_wplus: do m1=1,5
       loop_j_6_wplus: do m2=m1+1,6
          check_f_fcnc: if(f_fcnc) then
             f_zboson=charge_quark(idhep(ifermion(m1))).eq.-charge_quark(idhep(ifermion(m2)))
          else check_f_fcnc
             f_zboson=idhep(ifermion(m1)).eq.-idhep(ifermion(m2))
          end if check_f_fcnc
          check_m1m2_wplus: if(f_zboson) then
             ffn=.true.
             ffn(m1)=.false.
             ffn(m2)=.false.
             loop_m3_6: do m3=1,6
                if(m3.eq.m1.or.m3.eq.m2) cycle loop_m3_6
                k3a=minval(abs(idhep(ifermion(m3))-i_wgen))
                check_k3a: if(k3a.eq.0) then
                   call check_wboson(i_wgen,f_wboson)
                   check_fk3a: if(f_wboson) then
                      f_match=.true.
                      exit loop_i_6_wplus
                   else check_fk3a
                      cycle loop_m3_6
                   end if check_fk3a
                else check_k3a
                   cycle loop_m3_6
                end if check_k3a
             end do loop_m3_6
          end if check_m1m2_wplus
       end do loop_j_6_wplus
    end do loop_i_6_wplus
  end subroutine try_zww

  subroutine try_zzz(f_match,f_fcnc)
    logical, intent(out) :: f_match
    logical, intent(in) :: f_fcnc
    logical :: f_zboson
    f_match=.false.
    loop_i_6_wplus: do m1=1,5
       loop_j_6_wplus: do m2=m1+1,6
          check_m1m2_wplus: if(idhep(ifermion(m1)).eq.-idhep(ifermion(m2))) then
             ffn=.true.
             ffn(m1)=.false.
             ffn(m2)=.false.
             loop_m3_6: do m3=1,3
                if(m3.eq.m1.or.m3.eq.m2) cycle loop_m3_6
                call check_zboson(f_zboson,f_fcnc)
                check_fk3a: if(f_zboson) then
                   f_match=.true.
                   exit loop_i_6_wplus
                else check_fk3a
                   cycle loop_j_6_wplus
                end if check_fk3a
             end do loop_m3_6
          end if check_m1m2_wplus
       end do loop_j_6_wplus
    end do loop_i_6_wplus
  end subroutine try_zzz

  subroutine check_wboson(i_wgen,f_match)
    integer, dimension(:,:), intent(in) :: i_wgen
    logical, intent(out) :: f_match

    integer :: k4
    integer, dimension(2) :: nn3
    integer, dimension(1) :: n4
    integer, dimension(1) :: n5
    integer, dimension(1) :: n6
    logical, dimension(:,:), pointer :: f_wgen
    integer :: ioff

    allocate(f_wgen(2,size(i_wgen,dim=2)))
    f_wgen=.true.

    loop_f_wgen: do
       nn3=minloc(abs(idhep(ifermion(m3))-i_wgen),mask=f_wgen)
       ffn(m3)=.false.
       k4=minval(abs(idhep(ifermion(1:nfermion))-i_wgen(mod(nn3(1),2)+1,nn3(2))),mask=ffn)
       if(k4.eq.0) exit loop_f_wgen
       f_wgen(:,nn3(2))=.false.
       if(count(idhep(ifermion(m3)).eq.i_wgen.and.f_wgen).eq.0) exit loop_f_wgen
    end do loop_f_wgen

    deallocate(f_wgen)

    check_k4: if(k4.eq.0) then
       f_match=.true.
       n4=minloc(abs(idhep(ifermion(1:nfermion))-i_wgen(mod(nn3(1),2)+1,nn3(2))),mask=ffn)
       ffn(n4(1))=.false.
       check_nfermion_wboson: if(nfermion.eq.6) then
          check_id_m1m2_sign: if(idhep(ifermion(m1)).gt.0) then
             ifn(1)=m1
             ifn(2)=m2
          else check_id_m1m2_sign
             ifn(1)=m2
             ifn(2)=m1
          end if check_id_m1m2_sign
       end if check_nfermion_wboson
       ioff=nfermion-6
       check_m3_n4_sign: if(idhep(ifermion(m3)).gt.0) then
          ifn(3+ioff)=m3
          ifn(4+ioff)=n4(1)
       else check_m3_n4_sign
          ifn(3+ioff)=n4(1)
          ifn(4+ioff)=m3
       end if check_m3_n4_sign
       n5=maxloc(idhep(ifermion(1:nfermion)),mask=ffn)
       ffn(n5(1))=.false.
       ifn(5+ioff)=n5(1)
       n6=minloc(idhep(ifermion(1:nfermion)),mask=ffn)
       ffn(n6(1))=.false.
       ifn(6+ioff)=n6(1)
    else check_k4
       f_match=.false.
    end if check_k4


  end subroutine check_wboson

  subroutine check_zboson(f_match,f_fcnc)
    logical, intent(out) :: f_match
    logical, intent(in) :: f_fcnc

    integer :: k4
    integer, dimension(1) :: n4
    integer, dimension(1) :: n5
    integer, dimension(1) :: n6
    integer :: ioff

    ffn(m3)=.false.
    check_f_fcnc_k4: if(f_fcnc) then
       k4=minval(abs(charge_quark(idhep(ifermion(1:nfermion)))+charge_quark(idhep(ifermion(m3)))),mask=ffn)
    else check_f_fcnc_k4
       k4=minval(abs(idhep(ifermion(1:nfermion))+idhep(ifermion(m3))),mask=ffn)
    end if check_f_fcnc_k4
    check_k4: if(k4.eq.0) then
       f_match=.true.
       check_f_fcnc_n4: if(f_fcnc) then
          n4=minloc(abs(charge_quark(idhep(ifermion(1:nfermion)))+charge_quark(idhep(ifermion(m3)))),mask=ffn)
       else check_f_fcnc_n4
          n4=minloc(abs(idhep(ifermion(1:nfermion))+idhep(ifermion(m3))),mask=ffn)
       end if check_f_fcnc_n4
       ffn(n4(1))=.false.
       check_nfermion_zboson: if(nfermion.eq.6) then
          check_id_m1m2_sign: if(idhep(ifermion(m1)).gt.0) then
             ifn(1)=m1
             ifn(2)=m2
          else check_id_m1m2_sign
             ifn(1)=m2
             ifn(2)=m1
          end if check_id_m1m2_sign
       end if check_nfermion_zboson
       ioff=nfermion-6
       check_m3_n4_sign: if(idhep(ifermion(m3)).gt.0) then
          ifn(3+ioff)=m3
          ifn(4+ioff)=n4(1)
       else check_m3_n4_sign
          ifn(3+ioff)=n4(1)
          ifn(4+ioff)=m3
       end if check_m3_n4_sign
       n5=maxloc(idhep(ifermion(1:nfermion)),mask=ffn)
       ffn(n5(1))=.false.
       n6=minloc(idhep(ifermion(1:nfermion)),mask=ffn)
       ifn(5+ioff)=n5(1)
       ifn(6+ioff)=n6(1)
    else check_k4
       f_match=.false.
    end if check_k4

  end subroutine check_zboson

  subroutine order_ifn
    integer i
    integer if1
    integer if2
    integer, dimension(1) :: i1


    loop_ifn: do i=1,nfermion-1

       check_if1: if(ifn(i).ne.i) then

          if1=ifermion(ifn(i))
          if2=ifermion(i)

          call swap_ifn(if1,if2)

          i1=minloc(abs(ifn-i))
          ifn(i1(1))=ifn(i)
          ifn(i)=i

       end if check_if1


    end do loop_ifn
  end subroutine order_ifn


  subroutine swap_ifn(if1,if2)
    integer, intent(in) :: if1
    integer, intent(in) :: if2
    integer :: isthep_s, idhep_s
    integer, dimension(2) :: jmohep_s, jdahep_s
    real(kind=double), dimension(5) :: phep_s
    real(kind=double), dimension(4) :: vhep_s

    isthep_s=isthep(if2)
    idhep_s=idhep(if2)
    jmohep_s=jmohep(:,if2)
    jdahep_s=jdahep(:,if2)
    phep_s=phep(:,if2)
    vhep_s=vhep(:,if2)

    isthep(if2)=isthep(if1)
    idhep(if2)=idhep(if1)
    jmohep(:,if2)=jmohep(:,if1)
    jdahep(:,if2)=jdahep(:,if1)
    phep(:,if2)=phep(:,if1)
    vhep(:,if2)=vhep(:,if1)

    isthep(if1)=isthep_s
    idhep(if1)=idhep_s
    jmohep(:,if1)=jmohep_s
    jdahep(:,if1)=jdahep_s
    phep(:,if1)=phep_s
    vhep(:,if1)=vhep_s


  end subroutine swap_ifn

  subroutine try_ww(i_wgen,f_match)
    integer, dimension(:,:), intent(in) :: i_wgen
    logical, intent(out) :: f_match
    logical :: f_wboson
    integer :: k3a
    f_match=.false.
    loop_m3_4_wplus: do m3=1,3
       ffn=.true.
       k3a=minval(abs(idhep(ifermion(m3))-i_wgen))
       check_k3a: if(k3a.eq.0) then
          call check_wboson(i_wgen,f_wboson)
          check_fk3a: if(f_wboson) then
             f_match=.true.
             exit loop_m3_4_wplus
          end if check_fk3a
       end if check_k3a
    end do loop_m3_4_wplus
  end subroutine try_ww

  subroutine try_zz(f_match,f_fcnc)
    logical, intent(out) :: f_match
    logical, intent(in) :: f_fcnc
    logical :: f_zboson
    f_match=.false.
    ffn=.true.
    m3=1
    call check_zboson(f_zboson,f_fcnc)
    check_fk3a: if(f_zboson) then
       f_match=.true.
    end if check_fk3a
  end subroutine try_zz

  subroutine copy_hepevt_entry(i,j)
    integer, intent(in) :: i
    integer, intent(in) :: j

    isthep(j)=isthep(i)
    idhep(j)=idhep(i)
    jmohep(:,j)=jmohep(:,i)
    jdahep(:,j)=jdahep(:,i)
    phep(:,j)=phep(:,i)
    vhep(:,j)=vhep(:,i)

  end subroutine copy_hepevt_entry

  subroutine shift_down_hepevt_hepev4(ishift_down,nshift_down)
    integer, intent(in) :: ishift_down
    integer, intent(in) :: nshift_down
    integer :: i
    integer :: i_in
    integer :: i_out
    loop_i: do i=1,nhep-ishift_down+1
       i_in=nhep-i+1
       i_out=i_in+nshift_down
       isthep(i_out)=isthep(i_in)
       idhep(i_out)=idhep(i_in)
       jmohep(:,i_out)=jmohep(:,i_in)
       jdahep(:,i_out)=jdahep(:,i_in)
       phep(:,i_out)=phep(:,i_in)
       vhep(:,i_out)=vhep(:,i_in)
       spinlh(:,i_out)=spinlh(:,i_in)
       icolorflowlh(:,i_out)=icolorflowlh(:,i_in)
       if(jmohep(1,i_out).gt.0) jmohep(1,i_out)=jmohep(1,i_out)+nshift_down
       if(jmohep(2,i_out).gt.0) jmohep(2,i_out)=jmohep(2,i_out)+nshift_down
       if(jdahep(1,i_out).gt.0) jdahep(1,i_out)=jdahep(1,i_out)+nshift_down
       if(jdahep(2,i_out).gt.0) jdahep(2,i_out)=jdahep(2,i_out)+nshift_down
    enddo loop_i

    nhep=nhep+nshift_down

  end subroutine shift_down_hepevt_hepev4

  subroutine  ilc_restore_documentation_particles

    integer :: i_doc
    integer :: n_doc
    integer :: i
    integer :: k

    call shift_down_hepevt_hepev4(1,hepevt_input%nhep)

    loop_i: do i=1,hepevt_input%nhep
       isthep(i)=hepevt_input%isthep(i)
       idhep(i)=hepevt_input%idhep(i)
       jmohep(:,i)=hepevt_input%jmohep(:,i)
       jdahep(:,i)=hepevt_input%jdahep(:,i)
       phep(:,i)=hepevt_input%phep(:,i)
       vhep(:,i)=hepevt_input%vhep(:,i)
       spinlh(:,i)=0.
       icolorflowlh(:,i)=0
    end do loop_i

    loop_nhep_out: do i=1,nhep_out
       jmohep(1,hepevt_input%nhep+i)=jmo_hep_out(i)
       jmohep(2,hepevt_input%nhep+i)=0
       if ( jdahep(1,jmo_hep_out(i)) == 0 ) then
          jdahep(1,jmo_hep_out(i))=hepevt_input%nhep+i
          jdahep(2,jmo_hep_out(i))=hepevt_input%nhep+i
       else
          jdahep(2,jmo_hep_out(i))=hepevt_input%nhep+i
       endif
    end do loop_nhep_out




  end subroutine ilc_restore_documentation_particles


  subroutine try_bbww(i_wgen,f_match)
    integer, dimension(:,:), intent(in) :: i_wgen
    logical, intent(out) :: f_match
    logical :: f_wboson
    logical :: f_zboson
    integer :: k3a
    f_match=.false.
    loop_i_6_wplus: do m1=1,5
       check_bquark: if(abs(idhep(ifermion(m1))).eq.5) then
          loop_j_6_wplus: do m2=m1+1,6
             f_zboson=idhep(ifermion(m1)).eq.-idhep(ifermion(m2))
             check_m1m2_wplus: if(f_zboson) then
                ffn=.true.
                ffn(m1)=.false.
                ffn(m2)=.false.
                loop_m3_6: do m3=1,6
                   if(m3.eq.m1.or.m3.eq.m2) cycle loop_m3_6
                   k3a=minval(abs(idhep(ifermion(m3))-i_wgen))
                   check_k3a: if(k3a.eq.0) then
                      call check_wboson(i_wgen,f_wboson)
                      check_fk3a: if(f_wboson) then
                         f_match=.true.
                         exit loop_i_6_wplus
                      else check_fk3a
                         cycle loop_m3_6
                      end if check_fk3a
                   else check_k3a
                      cycle loop_m3_6
                   end if check_k3a
                end do loop_m3_6
             end if check_m1m2_wplus
          end do loop_j_6_wplus
       end if check_bquark
    end do loop_i_6_wplus
  end subroutine try_bbww

end module ilc_calc_nfermion_mod


















