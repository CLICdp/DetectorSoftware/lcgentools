module epa_pt_mod
  use kinds, only: double
  use pyr_inter
  use a6f_users, only: mcu
  use hepevt_include_common
  implicit none

  private

  public :: epa_pt_1
  public :: epa_pt_2

  real(kind=double), parameter :: pt_max=1.d-8
  real(kind=double), parameter :: pi=3.1415926536d0
  real(kind=double), parameter :: xme=0.510998902d-3   !  units of xme are GeV
  integer, parameter :: n_q2_iter_max=100



contains

  subroutine epa_pt_1

    integer :: i
    integer :: ip
    integer :: jp3
    integer :: ncount=0

    real(kind=double) :: pt3
    real(kind=double) :: ct3
    real(kind=double) :: phi3
    real(kind=double) :: q_max
    real(kind=double), dimension(6) :: p5
    real(kind=double), dimension(6) :: p5_not_e
    real(kind=double), dimension(6) :: p3p5
    real(kind=double), dimension(6) :: ptot
    real(kind=double), dimension(6) :: pmiss
    real(kind=double), dimension(:,:), pointer :: pp

    logical, dimension(:), pointer :: f_p5
    logical, dimension(:), pointer :: f_p3p5

    ncount=ncount+1
    check_epa_on: if(mcu%epa_on(1)) then
       ip=1
    else check_epa_on
       ip=2
    end if check_epa_on

    check_nhep: if(nhep.gt.0) then
       allocate (f_p3p5(nhep))
       allocate (f_p5(nhep))
       jp3=0
       if(ncount.le.10) call heplst(1)
       loop_nhep_0: do i=1,nhep
          check_ncount_1: if(ncount.le.10) then
             print "(' i,idhep(i),isthep(i),pt_max,phep(1:2,i)=',3i6,3d20.12)", &
                  & i,idhep(i),isthep(i),pt_max,phep(1:2,i)
          end if check_ncount_1
          check_idhep_etc: if(idhep(i).eq.mcu%epa_prt_in(ip).and.abs(phep(1,i)).lt.pt_max & 
     &     .and.abs(phep(2,i)).lt.pt_max.and.isthep(i).eq.1.and.jmohep(1,i).eq.0.and.jp3.eq.0) then
             jp3=i
             f_p3p5(i)=.true.
             f_p5(i)=.false.
          elseif((abs(idhep(i)).eq.11.or.abs(idhep(i)).eq.22).and.abs(phep(1,i)).lt.pt_max & 
     &     .and.abs(phep(2,i)).lt.pt_max.and.isthep(i).eq.1.and.jmohep(1,i).eq.0) then check_idhep_etc
             f_p3p5(i)=.false.
             f_p5(i)=.false.
          elseif(isthep(i).eq.1) then check_idhep_etc
             f_p3p5(i)=.true.
             f_p5(i)=.true.
          else check_idhep_etc
             f_p3p5(i)=.false.
             f_p5(i)=.false.
          end if check_idhep_etc

       end do loop_nhep_0

       check_jp3_etc: if(jp3.eq.0.or.count(f_p5).lt.1) then
          print "(' from subroutine epa_pt_1 in epa_pt_mod.f90')"
          print "(' inconsistent jp3, count(f_p5)=',2i12)", jp3,count(f_p5)
          loop_nhep_err1: do i=1,nhep
             print "(' i,idhep(i),isthep(i),pt_max,phep(1:2,i)=',3i6,3d20.12)", &
                  & i,idhep(i),isthep(i),pt_max,phep(1:2,i)
          end do loop_nhep_err1
          print "(' program will stop after call to heplst(1)')"
          call heplst(1)
          stop
       end if check_jp3_etc

       allocate (pp(6,nhep))
       pp(:,1:nhep)=phep(:,1:nhep)
       pp(6,1:nhep)=sqrt(sum(pp(1:3,1:nhep)**2,dim=1))
       p5_not_e(1:4)=sum(phep(1:4,1:nhep),dim=2,mask=spread(f_p5.and.abs(idhep(1:nhep)).ne.11,dim=1,ncopies=4))
       p5_not_e(5)=sqrt(max(0.d0,p5_not_e(4)**2-sum(p5_not_e(1:3)**2)))
       p5_not_e(6)=sqrt(sum(p5_not_e(1:3)**2))
       p5(1:4)=sum(phep(1:4,1:nhep),dim=2,mask=spread(f_p5,dim=1,ncopies=4))
       p5(5)=sqrt(max(0.d0,p5(4)**2-sum(p5(1:3)**2)))
       p5(6)=sqrt(sum(p5(1:3)**2))
       p3p5(1:4)=phep(1:4,jp3)+p5(1:4)
       p3p5(5)=sqrt(max(0.d0,p3p5(4)**2-sum(p3p5(1:3)**2)))
       p3p5(6)=sqrt(sum(p3p5(1:3)**2))
       if(ncount.le.10) print "(' p5_not_e(1:6)=',6d20.12)", p5_not_e
       check_p3p5_pt: if(abs(p3p5(1)).gt.pt_max.or.abs(p3p5(2)).gt.pt_max) then
          print "(' from subroutine epa_pt_1 in epa_pt_mod.f90, ncount=',i12)", ncount
          print "(' bad p3p5=',6d20.12)", p3p5
          print *, " f_p5=", f_p5
          print "(' jp3,phep(1:4,jp3)=',i9,4d20.12)", jp3,phep(1:4,jp3)
          ptot(1:4)=sum(phep(1:4,1:nhep),dim=2,mask=spread(isthep(1:nhep).eq.1,dim=1,ncopies=4))
          ptot(5)=sqrt(max(0.d0,ptot(4)**2-sum(ptot(1:3)**2)))
          ptot(6)=sqrt(sum(ptot(1:3)**2))
          pmiss(1:3)=-ptot(1:3)
          pmiss(4)=mcu%sqrts-ptot(4)
          pmiss(5)=sqrt(max(0.d0,pmiss(4)**2-sum(pmiss(1:3)**2)))
          pmiss(6)=sqrt(sum(pmiss(1:3)**2))
          print "(' ptot(1:6)=',6d20.12)", ptot
          print "(' pmiss(1:6)=',6d20.12)", pmiss
          print "(' p5(1:6)=',6d20.12)", p5
          print "(' p5_not_e(1:6)=',6d20.12)", p5_not_e
          print "(' program will stop after call to heplst(1)')"
          call heplst(1)
          stop
       end if check_p3p5_pt
!      print *, " before zboost      p3p5=", p3p5
!      print *, " before zboost pp(:,jp3)=", pp(:,jp3)
       call zboost_to_arg3(pp,nhep,p3p5)
!      print *, "  after zboost pp(:,jp3)=", pp(:,jp3)
       call calc_pt(min(pp(6,jp3),mcu%epa_q_max(ip)),pt3)
       ct3=sign(sqrt(pp(6,jp3)**2-pt3**2)/pp(6,jp3),pp(3,jp3))
       phi3=2.d0*pi*pyr(0)
!      print *, " ct3,f_p3p5=", ct3,f_p3p5
       call rotate(pp,jp3,f_p3p5,ct3,phi3)
       call inverse_zboost_to_arg3(pp,nhep,p3p5)
       call replace_phep(pp,f_p3p5)
       deallocate(pp)
       deallocate(f_p5)
       deallocate(f_p3p5)
    end if check_nhep
    return

  end subroutine epa_pt_1




  subroutine epa_pt_2

    integer :: i
    integer :: jp3
    integer :: jp4
    integer :: jp6
    integer :: iz
    integer :: jz
    integer :: iz6
    integer :: jz6
    integer, dimension(2) :: npz
    integer :: ncount=0


    real(kind=double), parameter :: eps_tol=1.d-7
    real(kind=double), parameter :: e3_e4_tol=1.d-4

    real(kind=double) :: chz34
    real(kind=double) :: chz34_test
    real(kind=double) :: pt3
    real(kind=double) :: ct3
    real(kind=double) :: st3
    real(kind=double) :: phi3
    real(kind=double) :: pt4
    real(kind=double) :: ct4
    real(kind=double) :: st4
    real(kind=double) :: phi4
    real(kind=double) :: ct6
    real(kind=double) :: st6
    real(kind=double) :: phi6
    real(kind=double) :: e3_0
    real(kind=double) :: e4_0
    real(kind=double) :: pz6_2
    real(kind=double) :: econ_test
    real(kind=double) :: zeroin
    real(kind=double), dimension(2,2) :: pz3
    real(kind=double), dimension(2,2) :: pz4
    real(kind=double), dimension(2) :: pz6
    real(kind=double), dimension(6) :: p6_new
    real(kind=double), dimension(6) :: p3p4p6
    real(kind=double), dimension(:,:), pointer :: pp

    logical, dimension(:), pointer :: f_p3
    logical, dimension(:), pointer :: f_p4
    logical, dimension(:), pointer :: f_p6
    logical, dimension(:), pointer :: f_p3p4p6


    ncount=ncount+1
    check_nhep: if(nhep.gt.0) then
       allocate (f_p3p4p6(nhep))
       allocate (f_p3(nhep))
       allocate (f_p4(nhep))
       allocate (f_p6(nhep))
       jp3=0
       jp4=0
       if(ncount.le.10) call heplst(1)
       loop_nhep_0: do i=1,nhep
          check_idhep_etc: if(idhep(i).eq.mcu%epa_prt_in(1).and.abs(phep(1,i)).lt.pt_max & 
     &     .and.abs(phep(2,i)).lt.pt_max.and.isthep(i).eq.1.and.jmohep(1,i).eq.0.and.jp3.eq.0) then
!            print *, " point 1 i=", i
             jp3=i
             f_p3(i)=.true.
             f_p4(i)=.false.
             f_p6(i)=.false.
          elseif(idhep(i).eq.mcu%epa_prt_in(2).and.abs(phep(1,i)).lt.pt_max & 
     &     .and.abs(phep(2,i)).lt.pt_max.and.isthep(i).eq.1.and.jmohep(1,i).eq.0.and.jp4.eq.0) then check_idhep_etc
!            print *, " point 2 i=", i
             jp4=i
             f_p3(i)=.false.
             f_p4(i)=.true.
             f_p6(i)=.false.
          elseif((abs(idhep(i)).eq.11.or.abs(idhep(i)).eq.22).and.abs(phep(1,i)).lt.pt_max & 
     &     .and.abs(phep(2,i)).lt.pt_max.and.isthep(i).eq.1.and.jmohep(1,i).eq.0) then check_idhep_etc
!            print *, " point 3 i=", i
             f_p3(i)=.false.
             f_p4(i)=.false.
             f_p6(i)=.false.
          elseif(isthep(i).eq.1) then check_idhep_etc
!            print *, " point 4 i=", i
             f_p3(i)=.false.
             f_p4(i)=.false.
             f_p6(i)=.true.
          else check_idhep_etc
!            print *, " point 5 i=", i
             f_p3(i)=.false.
             f_p4(i)=.false.
             f_p6(i)=.false.
          end if check_idhep_etc

       end do loop_nhep_0

       check_jp3_etc: if(jp3.eq.0.or.jp4.eq.0.or.count(f_p6).lt.1) then
          print "(' from subroutine epa_pt_2 in epa_pt_mod.f90')"
          print "(' inconsistent jp3, jp4, count(f_p6)=',3i12)", jp3,jp4,count(f_p6)
          print "(' program will stop after call to heplst(1)')"
          print *, " phep(1:2,2)=", phep(1:2,2)
          print *, " phep(1:2,4)=", phep(1:2,4)
          print *, " f_p6=", f_p6
          print *, " epa_prt_in=", mcu%epa_prt_in
          print *, " pt_max=", pt_max
          print *, " idhep(2)=", idhep(2)
          print *, " isthep(2)=", isthep(2)
          print *, " jmohep(1,2)=", jmohep(1,2)
          call heplst(1)
          stop
       end if check_jp3_etc
       f_p3p4p6=f_p3.or.f_p4.or.f_p6
       jp6=nhep+1
       allocate (pp(6,nhep+1))
       pp(1:5,1:nhep)=phep(:,1:nhep)
       pp(6,1:nhep)=sqrt(sum(pp(1:3,1:nhep)**2,dim=1))
       pp(1:4,jp6)=sum(phep(1:4,1:nhep),dim=2,mask=spread(f_p6,dim=1,ncopies=4))
       pp(5,jp6)=sqrt(max(0.d0,pp(4,jp6)**2-sum(pp(1:3,jp6)**2)))
       pp(6,jp6)=sqrt(sum(pp(1:3,jp6)**2))
       p3p4p6(1:4)=phep(1:4,jp3)+phep(1:4,jp4)+pp(1:4,jp6)
       p3p4p6(5)=sqrt(max(0.d0,p3p4p6(4)**2-sum(p3p4p6(1:3)**2)))
       p3p4p6(6)=sqrt(sum(p3p4p6(1:3)**2))
       check_p3p4p6_pt: if(abs(p3p4p6(1)).gt.pt_max.or.abs(p3p4p6(2)).gt.pt_max) then
          print "(' from subroutine epa_pt_2 in epa_pt_mod.f90')"
          print "(' bad p3p4p6=',6d14.6)", p3p4p6
          print "(' program will stop after call to heplst(1)')"
          call heplst(1)
          stop
       end if check_p3p4p6_pt
!      print *, " f_p3=", f_p3
!      print *, " f_p4=", f_p4
!      print *, " f_p6=", f_p6
!      print *, " f_p3p4p6=", f_p3p4p6
!      print *, " p3p4p6=", p3p4p6
!      print *, " before zboost pp(:,nhep+1)=", pp(:,nhep+1)
       call zboost_to_arg3(pp,nhep+1,p3p4p6)
!    print *, " after zboost pp(:,nhep+1)=", pp(:,nhep+1)
       p6_new=pp(:,jp6)
       e3_0=pp(4,jp3)
       e4_0=pp(4,jp4)
       loop_q2_iter: do i=1,n_q2_iter_max
          call calc_pt(min(pp(6,jp3),mcu%epa_q_max(1)),pt3)
          call calc_pt(min(pp(6,jp4),mcu%epa_q_max(2)),pt4)
          phi3=2.d0*pi*pyr(0)
          phi4=2.d0*pi*pyr(0)
          pp(1,jp3)=pt3*cos(phi3)
          pp(2,jp3)=pt3*sin(phi3)
          pp(1,jp4)=pt4*cos(phi4)
          pp(2,jp4)=pt4*sin(phi4)
          p6_new(1)=-(pp(1,jp3)+pp(1,jp4))
          p6_new(2)=-(pp(2,jp3)+pp(2,jp4))
          pz6_2=p6_new(6)**2-p6_new(1)**2-p6_new(2)**2
!    print *, "i,pz6_2=", i,pz6_2
          if(pz6_2.lt.0.d0) cycle loop_q2_iter

          pz6(1)=sqrt(pz6_2)
          pz6(2)=-sqrt(pz6_2)
          call calc_pz34(pp(:,jp3),pp(:,jp4),pz6(1),npz(1),pz3(:,1),pz4(:,1))
          call calc_pz34(pp(:,jp3),pp(:,jp4),pz6(2),npz(2),pz3(:,2),pz4(:,2))
          jz6=0
          chz34=huge(0.d0)
!    print *, " pp(3,jp3),pp(3,jp4)=", pp(3,jp3),pp(3,jp4)
          loop_iz6: do iz6=1,2
             loop_iz: do iz=1,npz(iz6)
!    print *, " pz3(iz,iz6),pz4(iz,iz6)=", pz3(iz,iz6),pz4(iz,iz6)
                if(pz3(iz,iz6)*pp(3,jp3).lt.0.d0.or.pz4(iz,iz6)*pp(3,jp4).lt.0.d0) cycle loop_iz
                chz34_test=(pz3(iz,iz6)-pp(3,jp3))**2+(pz4(iz,iz6)-pp(3,jp4))**2
                check_chz: if(chz34_test.lt.chz34) then
                   chz34=chz34_test
                   jz=iz
                   jz6=iz6
                end if check_chz
             end do loop_iz
          end do loop_iz6
!
!    print *, "i,jz6=", i,jz6
          if(jz6.eq.0) cycle loop_q2_iter
          p6_new(3)=pz6(jz6)
          pp(3,jp3)=pz3(jz,jz6)
          pp(3,jp4)=pz4(jz,jz6)
          pp(4,jp3)=sqrt(pp(5,jp3)**2+sum(pp(1:3,jp3)**2))
          pp(4,jp4)=sqrt(pp(5,jp4)**2+sum(pp(1:3,jp4)**2))
          pp(6,jp3)=sqrt(sum(pp(1:3,jp3)**2))
          pp(6,jp4)=sqrt(sum(pp(1:3,jp4)**2))
!    print *, " before exiting loop_q2_iter pp(:,jp3)=", pp(:,jp3) 
!    print *, " before exiting loop_q2_iter pp(:,jp4)=", pp(:,jp4) 
          exit loop_q2_iter

       end do loop_q2_iter
       check_e3_e4: if(abs(pp(4,jp3)+pp(4,jp4)-e3_0-e4_0).gt.e3_e4_tol) then
          print "(' from subroutine epa_pt_2 in epa_pt_mod.f90')"
          print "(' problem with energy for jp3 and jp4')"
          print "(' pp(4,jp3),pp(4,jp4),e3_0,e4_0=',4d14.6)", pp(4,jp3),pp(4,jp4),e3_0,e4_0
          print "(' program will stop after call to heplst(1)')"
          call heplst(1)
          stop
       end if check_e3_e4
       phi6=atan2(p6_new(2),p6_new(1))
       ct6=p6_new(3)/p6_new(6)
!    print *, " before rotate   p6_new=", p6_new
       call rotate(pp,jp6,f_p6,ct6,phi6)
!    print *, " after rotate pp(:,jp6)=", pp(:,jp6)
       call inverse_zboost_to_arg3(pp,nhep+1,p3p4p6)
       call replace_phep(pp,f_p3p4p6)
       deallocate(pp)
       deallocate(f_p3)
       deallocate(f_p4)
       deallocate(f_p6)
       deallocate(f_p3p4p6)
    end if check_nhep
    return

  end subroutine epa_pt_2


  subroutine zboost_to_arg3(pp,npp,pcm)
    real(kind=double), dimension(6,*), intent(inout) :: pp
    integer, intent(in) :: npp
    real(kind=double), dimension(6), intent(in) :: pcm

    real(kind=double), dimension(:,:), pointer :: qq
    real(kind=double) :: gam
    real(kind=double) :: betgam
    

    allocate (qq(6,npp))
    qq(:,1:npp)=pp(:,1:npp)

    gam=pcm(4)/pcm(5)
    betgam=sign(sqrt(gam**2-1.d0),-pcm(3))
    pp(4,1:npp)=gam*qq(4,1:npp)+betgam*qq(3,1:npp)
    pp(3,1:npp)=betgam*qq(4,1:npp)+gam*qq(3,1:npp)
    pp(6,1:npp)=sqrt(sum(pp(1:3,1:npp)**2,dim=1))
    deallocate(qq)
    return

  end subroutine zboost_to_arg3



  subroutine inverse_zboost_to_arg3(pp,npp,pcm)
    real(kind=double), dimension(6,*), intent(inout) :: pp
    integer, intent(in) :: npp
    real(kind=double), dimension(6), intent(in) :: pcm

    real(kind=double), dimension(:,:), pointer :: qq
    real(kind=double) :: gam
    real(kind=double) :: betgam
    

    allocate (qq(6,npp))
    qq(:,1:npp)=pp(:,1:npp)

    gam=pcm(4)/pcm(5)
    betgam=sign(sqrt(gam**2-1.d0),pcm(3))
    pp(4,1:npp)=gam*qq(4,1:npp)+betgam*qq(3,1:npp)
    pp(3,1:npp)=betgam*qq(4,1:npp)+gam*qq(3,1:npp)
    deallocate(qq)
    return

  end subroutine inverse_zboost_to_arg3

  subroutine calc_pt(q_max,pt)
    real(kind=double), intent(in) :: q_max
    real(kind=double), intent(out) :: pt


    real(kind=double), parameter :: q_min=0.d0
    
    pt=sqrt((q_min**2+xme**2)*((q_max**2+xme**2)/(q_min**2+xme**2))**pyr(0)-xme**2)

!    print *, " q_max,pt=", q_max,pt

    return

  end subroutine calc_pt

  subroutine rotate(pp,jp1,f_p3,ct3,phi3)
    real(kind=double), dimension(6,*), intent(inout) :: pp
    integer, intent(in) :: jp1
    logical, dimension(*), intent(in) :: f_p3
    real(kind=double), intent(in) :: ct3
    real(kind=double), intent(in) :: phi3


    real(kind=double), dimension(3,3) :: rot_1
    real(kind=double), dimension(3,3) :: rot_2
    real(kind=double), dimension(3,3) :: rot
    real(kind=double) :: ct1
    real(kind=double) :: st1
    real(kind=double) :: phi1
    real(kind=double) :: cph1
    real(kind=double) :: sph1
    real(kind=double) :: st3
    real(kind=double) :: cph3
    real(kind=double) :: sph3

    integer :: i
    
    ct1=pp(3,jp1)/pp(6,jp1)
    st1=sqrt(1.d0-ct1**2)
    check_atan2_arg: if(abs(pp(1,jp1)).lt.epsilon(0.d0).and.abs(pp(2,jp1)).lt.epsilon(0.d0)) then
       phi1=0.d0
    else checK_atan2_arg
       phi1=atan2(pp(2,jp1),pp(1,jp1))
    end if check_atan2_arg
    cph1=cos(phi1)
    sph1=sin(phi1)
    st3=sqrt(1.d0-ct3**2)
    cph3=cos(phi3)
    sph3=sin(phi3)

! rot_1 rotates pp(:,jp1) to +z direction

    rot_1(1,1)=ct1*cph1
    rot_1(1,2)=ct1*sph1
    rot_1(1,3)=-st1
    rot_1(2,1)=-sph1
    rot_1(2,2)=cph1
    rot_1(2,3)=0.d0
    rot_1(3,1)=st1*cph1
    rot_1(3,2)=st1*sph1
    rot_1(3,3)=ct1

! rot_2 rotates +z direction to ct3,phi3

    rot_2(1,1)=ct3*cph3
    rot_2(2,1)=ct3*sph3
    rot_2(3,1)=-st3
    rot_2(1,2)=-sph3
    rot_2(2,2)=cph3
    rot_2(3,2)=0.d0
    rot_2(1,3)=st3*cph3
    rot_2(2,3)=st3*sph3
    rot_2(3,3)=ct3

    rot=matmul(rot_2,rot_1)

    loop_nhep: do i=1,nhep
       check_f_p3: if(f_p3(i)) then
          pp(1:3,i)=matmul(rot,pp(1:3,i))
       end if check_f_p3
    end do loop_nhep

    return

  end subroutine rotate


  subroutine replace_phep(pp,f_p3)
    real(kind=double), dimension(6,*), intent(in) :: pp
    logical, dimension(*), intent(in) :: f_p3



    integer :: i
    
    loop_nhep: do i=1,nhep
       check_f_p3: if(f_p3(i)) then
          phep(:,i)=pp(1:5,i)
       end if check_f_p3
    end do loop_nhep

    return

  end subroutine replace_phep


  subroutine calc_pz34(p3,p4,pz6,npz,pz3,pz4)
    real(kind=double), dimension(6), intent(in) :: p3
    real(kind=double), dimension(6), intent(in) :: p4
    real(kind=double), intent(in) :: pz6
    integer, intent(out) :: npz
    real(kind=double), dimension(2), intent(out) :: pz3
    real(kind=double), dimension(2), intent(out) :: pz4
    
    real(kind=double) :: rr
    real(kind=double) :: bb
    real(kind=double) :: cc
    real(kind=double) :: gg
    real(kind=double) :: ww
    real(kind=double) :: au
    real(kind=double) :: bu
    real(kind=double) :: cu
    real(kind=double), dimension(2), parameter :: bs=(/1.d0,-1.d0/)
    real(kind=double) :: b2_4ac

!    print *, "on entry to calc_pz34 p3=", p3
!    print *, "on entry to calc_pz34 p4=", p4
!    print *, "on entry to calc_pz34 pz6=", pz6

    rr=p3(1)**2+p3(2)**2+p3(5)**2
    bb=2.d0*pz6
    cc=p4(1)**2+p4(2)**2+pz6**2+p4(5)**2
    gg=-(p3(4)+p4(4))
    ww=cc-gg**2-rr
    au=bb**2-4.d0*gg**2
    bu=2.d0*bb*ww
    cu=ww**2-4.d0*gg**2*rr
    b2_4ac=bu**2-4.d0*au*cu
    check_b2_4ac: if(b2_4ac.ge.0.) then
       npz=2
       pz3=(-bu+bs*sqrt(b2_4ac))/2.d0/au
       pz4=-(pz6+pz3)
    else check_b2_4ac
       npz=0
    end if check_b2_4ac


    return

  end subroutine calc_pz34


end module epa_pt_mod

















