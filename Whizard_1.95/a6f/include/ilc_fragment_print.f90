subroutine ilc_fragment_print(ncount,whizard_integral)
  use kinds, only: double, i64
  use total_cross_mod
  use pyint1_common
  use pydat1_common
  use pypars_common
  use pyint5_common
  use pymssm_common
  implicit none
  integer(i64), intent(in) :: ncount
  real(kind=double), intent(in)               :: whizard_integral

  !
  print *, " ilc_fragment_print ncount=", ncount


  check_mstp171: if(mstp(171).eq.1) then
     print *, " ntries_total_cross= ", ntries_total_cross
     print *, " ncall_aa_pipi= ", ncall_aa_pipi
     print *, " ncall_force_pyevnt= ", ncall_force_pyevnt
     check_parp2: if(imss(0).eq.1.and.parp(2).lt.sqrt_shat_total_cross_min) then
        print *, " use peskin cross, whizard_integral=", whizard_integral
        print *, " use peskin cross, <aa cross-sec>(fb)=", sum_total_cross/dble(ntries_total_cross)*1.e12
        print *, " use peskin cross, total cross-section(fb)=", whizard_integral*sum_total_cross/dble(ntries_total_cross)*1.e12
      else check_parp2
        if(mint(121).gt.1) call pysave(5,0)
        print *, " use pythia cross, whizard_integral=", whizard_integral
        print *, " use pythia cross, <aa cross-sec>(fb)=", xsec(0,3)*1.e12
        print *, " use pythia cross, total cross-section(fb)=", whizard_integral*xsec(0,3)*1.e12
     end if check_parp2
     print *, " parp(2),sqrt_shat_total_cross_min,xsec(0,3)= ", parp(2),sqrt_shat_total_cross_min,xsec(0,3)
     print *, " xsec(11,3),xsec(12,3),xsec(13,3),xsec(28,3),xsec(53,3),xsec(68,3)= ", xsec(11,3),xsec(12,3),xsec(13,3),xsec(28,3),xsec(53,3),xsec(68,3)
     call pystat(1)
  else check_mstp171

     print *, " whizard_integral=", whizard_integral

  end if check_mstp171

  check_pytaud: if(mstj(28).eq.2) then
     print *,"CLOSE TAUOLA"
     CALL TAUOLA(1,1)
  end if check_pytaud

  return
end subroutine ilc_fragment_print
