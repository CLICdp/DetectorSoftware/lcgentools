module a6f_states

  use kinds, only: default, double !NODEP!
  use limits, only: CUT_TEX_STRLEN, MAX_CHANNELS, WHIZARD_ROOT
  use file_utils, only: free_unit !NODEP!

  implicit none
  private

  public :: create
  public :: read


  integer, parameter  :: PYGIVE_LEN = 100

  type, public :: a6f_input
     character(len=CUT_TEX_STRLEN) :: stdhep_file
     character(len=CUT_TEX_STRLEN) :: stdhep_output
     integer :: seed
     integer :: n_events
     integer :: i_sqrts
     logical :: output_events
     character(len=PYGIVE_LEN) :: pythia_parameters
     logical, dimension(2)           :: epa_on
     real(kind=double), dimension(2) :: epa_q_max
     integer, dimension(2)           :: epa_prt_in
  end type a6f_input

  type, public :: a6f_state
     character(len=CUT_TEX_STRLEN) :: stdhep_file
     character(len=CUT_TEX_STRLEN) :: stdhep_output
     integer :: seed
     integer :: n_events
     integer :: i_sqrts
     logical :: output_events
     character(len=PYGIVE_LEN) :: pythia_parameters
     integer :: n_events_std
     logical, dimension(2)           :: epa_on
     real(kind=double), dimension(2) :: epa_q_max
     integer, dimension(2)           :: epa_prt_in
  end type a6f_state

  type(a6f_state), public :: mcs

  interface create
     module procedure a6f_input_create
  end interface
  interface read
     module procedure a6f_input_read_unit
     module procedure a6f_input_read_name
  end interface
  
  interface create
     module procedure a6f_state_create
  end interface

contains

  subroutine a6f_input_create(mci)
    type(a6f_input), intent(out) :: mci
    mci%stdhep_file = "[undefined]"
    mci%stdhep_output = "a6f.stdhep"
    call system_clock(mci%seed)
    mci%output_events=.false.
    mci%pythia_parameters=' '
    mci%n_events = 0
    mci%i_sqrts = 500
    mci%epa_on = .false.
    mci%epa_q_max = 0.5d0
    mci%epa_prt_in(1) = 11
    mci%epa_prt_in(2) = -11
  end subroutine a6f_input_create

  subroutine a6f_input_read_unit(u, mci)
    integer, intent(in) :: u
    type(a6f_input), intent(inout) :: mci
    character(len=CUT_TEX_STRLEN) :: stdhep_file
    character(len=CUT_TEX_STRLEN) :: stdhep_output
    integer :: seed
    integer :: n_events
    integer :: i_sqrts
    logical :: output_events
    character(len=PYGIVE_LEN) :: pythia_parameters
     logical, dimension(2)           :: epa_on
     real(kind=double), dimension(2) :: epa_q_max
     integer, dimension(2)           :: epa_prt_in
    namelist /process_input/ stdhep_file
    namelist /process_input/ stdhep_output
    namelist /process_input/ seed
    namelist /process_input/ n_events
    namelist /process_input/ i_sqrts
    namelist /process_input/ output_events
    namelist /process_input/ pythia_parameters
    namelist /process_input/ epa_on
    namelist /process_input/ epa_q_max
    namelist /process_input/ epa_prt_in
    stdhep_file = mci%stdhep_file
    stdhep_output = mci%stdhep_output
    seed = mci%seed
    n_events = mci%n_events
    i_sqrts = mci%i_sqrts
    output_events = mci%output_events
    pythia_parameters = mci%pythia_parameters
    epa_on=mci%epa_on
    epa_q_max=mci%epa_q_max
    epa_prt_in=mci%epa_prt_in
    read(u, nml=process_input)
    mci%stdhep_file = stdhep_file
    mci%stdhep_output = stdhep_output
    mci%seed = seed
    mci%n_events = n_events
    mci%i_sqrts = i_sqrts
    mci%output_events = output_events
    mci%pythia_parameters = pythia_parameters
    mci%epa_on = epa_on
    mci%epa_q_max = epa_q_max
    mci%epa_prt_in = epa_prt_in
  end subroutine a6f_input_read_unit


  subroutine a6f_input_read_name(name, mci)
    character(len=*) :: name
    type(a6f_input), intent(inout) :: mci
    integer :: u
    print *, "! Reading process data from file ", trim(name)
    u = free_unit()
    open(u, file=name, action='read', status='old')
    call a6f_input_read_unit(u, mci)
    close(u)
  end subroutine a6f_input_read_name


  subroutine a6f_state_create(mcs, mci)
    type(a6f_state), intent(out) :: mcs
    type(a6f_input), intent(in) :: mci
    mcs%stdhep_file = mci%stdhep_file
    mcs%stdhep_output = mci%stdhep_output
    mcs%seed = mci%seed
    mcs%n_events = mci%n_events
    mcs%i_sqrts = mci%i_sqrts
    mcs%output_events = mci%output_events
    mcs%pythia_parameters = mci%pythia_parameters
    mcs%n_events_std = 0
    mcs%epa_on = mci%epa_on
    mcs%epa_q_max = mci%epa_q_max
    mcs%epa_prt_in = mci%epa_prt_in
    print *, "! stdhep_file= ", trim(mcs%stdhep_file)
    print *, "! stdhep_output= ", trim(mcs%stdhep_output)
    print *, "! seed= ", mcs%seed
    print *, "! n_events= ", mcs%n_events
    print *, "! i_sqrts= ", mcs%i_sqrts
    print *, "! output_events= ", mcs%output_events
    print *, "! pythia_parameters= ", mcs%pythia_parameters
    print *, "! epa_on= ", mcs%epa_on
    print *, "! epa_q_max= ", mcs%epa_q_max
    print *, "! epa_prt_in= ", mcs%epa_prt_in
  end subroutine a6f_state_create



end module a6f_states
