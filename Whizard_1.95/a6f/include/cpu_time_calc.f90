      subroutine cpu_time_calc(r_time)
      use kinds, only: double 
      use cpu_time_mod
      implicit none
!
!  r_time = estimated elapsed cpu time in seconds since cpu_time_init was called
!
      real(kind=double), intent(out)         :: r_time
!
      real(kind=double)                      :: r_rate
      real(kind=double)                      :: r_delta
!
!
!
      call system_clock(i_count_cpu,i_count_rate_cpu,i_count_max_cpu)
!
      check_count: if(i_count_cpu.gt.i_count_init_cpu) then
      i_delta_cpu=i_count_cpu-i_count_init_cpu
      else check_count
      i_delta_cpu=i_count_cpu+i_count_max_cpu-i_count_init_cpu
      end if check_count
! 
      r_rate=i_count_rate_cpu
      r_delta=i_delta_cpu
      r_time=r_delta/r_rate
!
!
!     print 4002, r_time
! 4002 format(/' Elapsed time:',f10.1,' sec.'/)
!
      return
      end subroutine cpu_time_calc
