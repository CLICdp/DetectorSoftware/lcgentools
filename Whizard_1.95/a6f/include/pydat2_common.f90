module pydat2_common

  use kinds, only: double 

  implicit none

  integer, dimension(500,4) :: kchg
  real(kind=double), dimension(500,4) :: pmas
  real(kind=double), dimension(2000) :: parf
  real(kind=double), dimension(4,4) :: vckm
  common/pydat2/kchg,pmas,parf,vckm


end module pydat2_common




