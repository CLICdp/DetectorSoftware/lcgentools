module ilc_tauola_mod
  use kinds, only: single,double 
  use hepevt_include_common
  use hepeup_include_common
  use taupos_include_common
  use momdec_include_common
  use pyr_inter
  use pycomp_inter
  use pyjets_common
  use diagnostics, only: msg_level
  implicit none

  private

  public :: ilc_tauola_ini
  public :: ilc_tauola_pytaud
  public :: ilc_tauola_end
  public :: tauspin_pyjets


  type, public :: whizard_mother
     integer                   :: k_orig_index
     integer                   :: k_after_fsr_index
     real(kind=double)         :: pol
  end type whizard_mother

  type, public :: neutral_mother
     integer                   :: k_index
     real(kind=double)         :: tau_minus_pol
     real(kind=double)         :: tau_plus_pol
  end type neutral_mother

  type(neutral_mother), dimension(:), pointer :: nma
  type(whizard_mother), dimension(:), pointer :: wma

  integer :: n_nma
  integer :: n_wma

  integer :: n_ini_call=0
! integer, parameter :: n_ini_call0=4360
  integer, parameter :: n_ini_call0=30971

  integer  :: jtau
  integer  :: jorig
  integer  :: jforig
  integer  :: nproducts

  integer, parameter                         :: n_pyjets_max=4000
  integer                                    :: id_dexay
  integer, parameter                         :: nprint_max=20

  real(kind=double), dimension(5)            :: p_dexay
  real(kind=double)                          :: spin_dexay
  real(kind=double), dimension(n_pyjets_max) :: tauspin_pyjets

  real(kind=single), dimension(4)  :: pol

  real(kind=double), parameter            :: a_tau=0.15
  real(kind=double), parameter            :: prob_tau_left_z=(a_tau+1.)/2.


contains

  subroutine ilc_tauola_ini


    integer :: i
    integer :: j
    real(kind=double)  :: sumdiff


    n_ini_call=n_ini_call+1

    tauspin_pyjets=0.
    allocate(wma(nhep))
    n_wma=0
    loop_i: do i=1,nhep
       check_id_etc: if(abs(idhep(i)).eq.15.and.isthep(i).eq.1) then
          loop_j: do j=1,nup
             check_consist: if(idup(j).eq.idhep(i)) then
                sumdiff=sum((pup(1:3,j)-phep(1:3,i))**2)
                check_sumdiff: if(sumdiff.lt.1.d-6) then
                   n_wma=n_wma+1
                   wma(n_wma)%k_orig_index=i
                   wma(n_wma)%pol=spinup(j)
                   wma(n_wma)%k_after_fsr_index=0
                   exit loop_j
                end if check_sumdiff
             end if check_consist
          end do loop_j
       end if check_id_etc
    end do loop_i



    return

  end subroutine ilc_tauola_ini

  subroutine ilc_tauola_pytaud(itau,iorig,kforig,ndecay)
    integer, intent(in)  :: itau
    integer, intent(in)  :: iorig
    integer, intent(in)  :: kforig
    integer, intent(out) :: ndecay


    integer  :: iakf

    jtau=itau
    jorig=iorig
    jforig=kforig
    loop_mothers: do
       iakf=abs(jforig)
       if(iakf.eq.23.or.iakf.eq.24.or.iakf.eq.25.or.iakf.eq.35.or.iakf.eq.36.or.iakf.eq.37.or.jorig.eq.0) exit loop_mothers
       jorig=k(jorig,3)
       if(jorig.gt.0) jforig=k(jorig,2)
    end do loop_mothers

    check_n_ini_pytaud: if((n_ini_call.le.nprint_max.or.n_ini_call.eq.n_ini_call0).and.msg_level>2) then 
       print *, " pytaud itau,orig,forig,n_ini= ", itau,jorig,jforig,n_ini_call
       call pylist(2)
    end if check_n_ini_pytaud

    check_jforig: if(jforig.eq.23.or.jforig.eq.25.or.jforig.eq.35.or.jforig.eq.36) then
       call neutral_mother_decay
    else if(abs(jforig).eq.24) then check_jforig
       call w_boson_decay
    else if(abs(jforig).eq.37) then check_jforig
       call charged_higgs_decay
    else if(jorig.eq.0.and.(jforig.eq.0.or.abs(jforig).eq.15.or.abs(jforig).eq.16)) then check_jforig
       call whizard_decay
    else check_jforig
       print *, " from ilc_tauola_pytaud unexpected itau,iorig,kforig,jorig,jforig = ", itau,iorig,kforig,jorig,jforig
       print *, " program will stop after call pylist(2)"
       call pylist(2)
       stop
    end if check_jforig

    ndecay=nproducts

    return

  end subroutine ilc_tauola_pytaud

  subroutine neutral_mother_decay

    integer :: j_nma
    integer, dimension(1) :: i_loc
    real(kind=double)  :: prob_tau_left
    
    check_n_ini_nmd: if((n_ini_call.le.nprint_max.or.n_ini_call.eq.n_ini_call0).and.msg_level>2)  then
       print *, " entry to neutral_mother_decay jtau,jorig,jforig= ", jtau,jorig,jforig
       call pylist(2)
    end if check_n_ini_nmd

    if(.not.associated(nma)) allocate(nma(n))
    n_nma=0

    check_nma_exist: if(count(nma(1:n_nma)%k_index.eq.jorig).gt.0) then

       i_loc=maxloc(nma(1:n_nma)%tau_minus_pol,mask=nma(1:n_nma)%k_index.eq.jorig)
       j_nma=i_loc(1)


    else check_nma_exist

       n_nma=n_nma+1
       j_nma=n_nma
       nma(j_nma)%k_index=jorig

       check_jforig_prob: if(jforig.eq.23) then
          prob_tau_left=prob_tau_left_z
       else check_jforig_prob
          prob_tau_left=0.5
       end if check_jforig_prob

       check_pyr: if(pyr(0).lt.prob_tau_left) then
          nma(j_nma)%tau_minus_pol=-1.
       else check_pyr
          nma(j_nma)%tau_minus_pol=1.
       end if check_pyr
       check_jforig: if(jforig.eq.23) then
          nma(j_nma)%tau_plus_pol=-nma(j_nma)%tau_minus_pol
       else check_jforig
          nma(j_nma)%tau_plus_pol=nma(j_nma)%tau_minus_pol
       end if check_jforig

    end if check_nma_exist

    id_dexay=k(jtau,2)
    p_dexay=p(jtau,1:5)
    if (msg_level>2 ) then
      if(n_ini_call.le.nprint_max.or.n_ini_call.eq.n_ini_call0) print *, " jtau,id_dexay= ", jtau,id_dexay
      if(n_ini_call.le.nprint_max.or.n_ini_call.eq.n_ini_call0) print *, " p_dexay(1:4)=", p_dexay(1:4)
    endif
    check_id: if(id_dexay.gt.0) then
       spin_dexay=nma(j_nma)%tau_minus_pol
    else check_id
       spin_dexay=nma(j_nma)%tau_plus_pol
    end if check_id

    call do_dexay

    return

  end subroutine neutral_mother_decay

  subroutine w_boson_decay

    id_dexay=k(jtau,2)
    p_dexay=p(jtau,1:5)
    check_w_sign: if(jforig.eq.24) then
       spin_dexay=1.
    else check_w_sign
       spin_dexay=-1.
    end if check_w_sign


    call do_dexay

    return

  end subroutine w_boson_decay

  subroutine charged_higgs_decay

    id_dexay=k(jtau,2)
    p_dexay=p(jtau,1:5)
    check_w_sign: if(jforig.eq.37) then
       spin_dexay=-1.
    else check_w_sign
       spin_dexay=1.
    end if check_w_sign


    call do_dexay

    return

  end subroutine charged_higgs_decay

  subroutine whizard_decay

    integer :: i
    integer :: ida

    integer :: j_wma
    integer, dimension(1) :: i_loc

    check_n_ini_wd: if((n_ini_call.le.nprint_max.or.n_ini_call.eq.n_ini_call0).and.msg_level>2) then
       print *, " entry to whizard_decay jtau,jorig,jforig= ", jtau,jorig,jforig
       call pylist(2)
    end if check_n_ini_wd



    check_k_after_fsr_set: if(count(wma(1:n_wma)%k_after_fsr_index.eq.0).eq.n_wma) then

       loop_n_wma: do i=1,n_wma
          ida=wma(i)%k_orig_index
          loop_daughters: do
             check_k4: if(k(ida,1).eq.1.and.k(ida,4).eq.0) then                     
                wma(i)%k_after_fsr_index=ida
                exit loop_daughters
             else check_k4
                ida=mod(k(ida,4),10000)
             end if check_k4
          end do loop_daughters

          if((n_ini_call.le.nprint_max.or.n_ini_call.eq.n_ini_call0).and.msg_level>2) then
             print *, " i,wma%k_orig,k_after_fsr,pol= ", i,wma(i)%k_orig_index,wma(i)%k_after_fsr_index,wma(i)%pol
          endif

       end do loop_n_wma


    end if check_k_after_fsr_set






    check_wma_exist: if(count(wma(1:n_wma)%k_after_fsr_index.eq.jtau).gt.0) then

       i_loc=maxloc(wma(1:n_wma)%pol,mask=wma(1:n_wma)%k_after_fsr_index.eq.jtau)
       j_wma=i_loc(1)


    elseif((k(jtau,2).eq.15.and.k(jtau+1,2).eq.-16).or.(k(jtau,2).eq.-15.and.k(jtau-1,2).eq.16)) then check_wma_exist

       call w_boson_decay
       return

    else check_wma_exist


       print *, " from ilc_tauola_mod::whizard_decay, can't find matching k_after_fsr_index"
       print *, " jtau,jorig,jforig= ", jtau,jorig,jforig
       print *, " n_ini_call= ", n_ini_call
       print *, " program will stop after calling pylist(2) "
       call pylist(2)
       stop

    end if check_wma_exist


    id_dexay=k(jtau,2)
    p_dexay=p(jtau,1:5)
    spin_dexay=wma(j_wma)%pol


    call do_dexay

    return

  end subroutine whizard_decay


  subroutine do_dexay

    integer   :: i

    tauspin_pyjets(jtau)=spin_dexay

    nhep=2

    isthep(1)=1
    idhep(1)=id_dexay
    jmohep(:,1)=0
    jdahep(:,1)=0
    phep(:,1)=p_dexay

    isthep(2)=1
    idhep(2)=-id_dexay
    jmohep(:,2)=0
    jdahep(:,2)=0
    phep(1:3,2)=-phep(1:3,1)
    phep(4:5,2)=phep(4:5,1)



    check_tau_sign: if(idhep(1).lt.0) then
       np1=1
       np2=2
       pol=0.
       pol(3)=-spin_dexay
       p1=phep(1:4,1)
       p2=phep(1:4,2)
       q1=p1+p2
!       print *, " antiparticle decay q1= ", q1
!       print *, " antiparticle decay p1= ", p1
!       print *, " antiparticle decay p2= ", p2
       call dexay(1,pol)
    else check_tau_sign
       np2=1
       np1=2
       pol=0.
       pol(3)=spin_dexay
       p2=phep(1:4,1)
       p1=phep(1:4,2)
       q1=p1+p2
!       print *, " particle decay q1= ", q1
!       print *, " particle decay p1= ", p1
!       print *, " particle decay p2= ", p2
       call dexay(2,pol)
    end if check_tau_sign


    nproducts=0
    if((n_ini_call.le.nprint_max.or.n_ini_call.eq.n_ini_call0) .and.msg_level>2) then
      print *, " do_dexay jtau,jorig,jforig,nhep= ", jtau,jorig,jforig,nhep
!      call heplst(1)
    endif
    loop_products: do i=3,nhep
       nproducts=nproducts+1
       p(n+nproducts,:)=phep(:,i)
       if ( isthep(i) .eq. 1 ) then
          k(n+nproducts,1)=1
          k(n+nproducts,4)=0
          k(n+nproducts,5)=0
       else
          k(n+nproducts,1)=11
          k(n+nproducts,4)=jdahep(1,i)+n-2
          k(n+nproducts,5)=jdahep(2,i)+n-2
       endif
       k(n+nproducts,2)=idhep(i)
       if ( abs(idhep(jmohep(1,i))) .ne. 15 ) then
          k(n+nproducts,3)=jmohep(1,i)+n-2
       else
          k(n+nproducts,3)=jtau
       endif
    end do loop_products

    return

  end subroutine do_dexay
  subroutine ilc_tauola_end

    if(associated(nma)) deallocate (nma)
    deallocate (wma)


    return

  end subroutine ilc_tauola_end


end module ilc_tauola_mod

















