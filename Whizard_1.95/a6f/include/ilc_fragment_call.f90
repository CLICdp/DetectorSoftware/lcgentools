
subroutine ilc_fragment_call(ncount)
  use kinds, only: double, i64
  use hepevt_include_common, only: nevhep, nhep, idhep, nhep_original, hepev4_fill, hepev4_update
  use hepeup_include_common
  use a6f_users
  use epa_pt_mod
  use setp1p2_mod
  use total_cross_mod
  use ilc_calc_nfermion_mod
  use ilc_tauola_mod
  use pydat1_common
  use pypars_common
  use pyjets_common
  use pyint1_common
  use pyint5_common
  use pydat3_common
  use pymssm_common
  use diagnostics, only: msg_level
  implicit none
  integer(i64), intent(inout) :: ncount

  !


  real(kind=double) :: atotsq=1.d0
  real(kind=double) :: a1sq=1.d0
  real(kind=double) :: a2sq=0.d0
  real(kind=double) :: p12=1.d0
  real(kind=double) :: p13=0.d0
  real(kind=double) :: p21=0.d0
  real(kind=double) :: p23=0.d0
  real(kind=double) :: p31=0.d0
  real(kind=double) :: p32=0.d0
  real(kind=double)            :: ptop
  real(kind=double), dimension(2,5) :: psave
  logical, dimension(8)        :: fermion_mask
  logical, dimension(4)        :: radiating
  logical                      :: f_ep_conserved
  integer :: nevhep_save
  integer :: nfermion
  integer :: itau=1  ! decay taus
  integer, parameter :: icom=0  ! input/output info stored in HEPEVT
  integer, parameter :: istrat=0
  integer, parameter :: nprint_max=20
  !  integer, parameter :: nprint_max=9000
  integer            :: i
!  integer, parameter :: ncount0=4360
!     set this to an event-number to get it printed at
!     various stages:
  integer, parameter :: ncount0=-1
  integer,save :: print_freq=1000
  integer,save :: nprinted = 0

  call setmuontaumass


  check_ncount_0: if((ncount.le.nprint_max.or.ncount.eq.ncount0.or.mod(ncount,print_freq).eq.0).and.msg_level>0) then
     print "(' on entry to user_fragment call;   ncount=',i12)", ncount
     call pyhepc(2)
     call heplst(1)
     loop_nup: do i=1,nup
        print 3001, i,idup(i),spinup(i),icolup(:,i)
        print 4001, i,pup(:,i)
3001    format(' i,idup,spinup,icolup(1:2)= ',2i12,f8.2,2i12)
4001    format(' i,pup= ',i12,5d14.6)
     end do loop_nup
  end if check_ncount_0

  !  check_mstj28: if(mstj(28).eq.2) then
  !     itau=0
  !  else check_mstj28
  !     itau=1
  !  end if check_mstj28



  ! call hide_beamstr_elec
  check_not_proton: if(idhep(1).ne.2212) then

     check_mstp171: if(mstp(171).eq.1) then
        call setp1p2
        check_parp2: if(imss(0).eq.1.and.parp(2).lt.sqrt_shat_total_cross_min) then
           psave=p(1:2,:)
           call total_cross_pyevnt
           check_msti61_total_cross: if(msti61_total_cross.eq.1) then
              ncount=ncount-1
              if(ncount.le.nprint_max.and.msg_level>0) print *, " total_cross_pyevnt msti(61),ncount,p(1,3),p(2,3),vint(1),ngen(0,1:3)=", msti(61),ncount,p(1,3),p(2,3),vint(1),ngen(0,1:3)
           else check_msti61_total_cross
              check_ncount_print: if(ncount.le.nprint_max.and.msg_level>0) then 
                 print *, "ncount,sqrt_shat_total_cross,sig_total_cross=", ncount,sqrt_shat_total_cross,sig_total_cross
                 print *, " p(1,1:3)= ", p(1,1:3),  " p(2,1:3)= ", p(2,1:3)
              end if check_ncount_print
              check_sqrts_hat: if(sqrt_shat_total_cross.gt.sqrt_shat_total_cross_min) then
                 loop_force_pyevnt: do
                    ncall_force_pyevnt=ncall_force_pyevnt+1
                    p(1:2,:)=psave
                    call pyevnt
                    check_msti61: if(msti(61).eq.1) then
                       if(ncount.le.nprint_max.and.msg_level>0) print *, " force_pyevnt msti(61),ncount,p(1,3),p(2,3),vint(1),ngen(0,1:3)=", msti(61),ncount,p(1,3),p(2,3),vint(1),ngen(0,1:3)
                    else
                       !...Call PYHEPC to convert output from PYJETS to HEPEVT common.
                       if(ncount.le.nprint_max.and.msg_level>0) print *, " force_pyevnt success ncount,p(1,3),p(2,3),vint(1),ngen(0,1:3)=", ncount,p(1,3),p(2,3),vint(1),ngen(0,1:3)
                       call pyhepc(1)
                       call add_spectator_electron_gamma
                       exit loop_force_pyevnt
                    end if check_msti61
                 end do loop_force_pyevnt
              else check_sqrts_hat
                 call aa_pipi
                 if(ncount.le.nprint_max.and.msg_level>0) print *, " aa_pipi success ncount,p(1,3),p(2,3),vint(1),ngen(0,1:3)=", ncount,p(1,3),p(2,3),vint(1),ngen(0,1:3)
                 !...Call PYHEPC to convert input from HEPEVT to PYJETS common.
                 call pyhepc(2)
                 call pyexec
                 !...Call PYHEPC to convert output from PYJETS to HEPEVT common.
                 call pyhepc(1)
              end if check_sqrts_hat
              !       call shower_spectator_electron
           end if check_msti61_total_cross
        else check_parp2
           ntries_total_cross=ntries_total_cross+1
           call pyevnt
           msti61_total_cross=msti(61)
           check_msti61_high_parp2: if(msti(61).eq.1) then
              ncount=ncount-1
              if(ncount.le.nprint_max.and.msg_level>0) print *, " check_parp2 msti(61),ncount,p(1,3),p(2,3),ngen(0,1:3)=", msti(61),ncount,p(1,3),p(2,3),ngen(0,1:3)
           else check_msti61_high_parp2
              !...Call PYHEPC to convert output from PYJETS to HEPEVT common.
              if(ncount.le.nprint_max.and.msg_level>0) print *, " check_parp2 success ncount,p(1,3),p(2,3),ngen(0,1:3)=", ncount,p(1,3),p(2,3),ngen(0,1:3)
              call pyhepc(1)
              call add_spectator_electron_gamma
           end if check_msti61_high_parp2

        end if check_parp2
        call deallocate_spectator
     else check_mstp171

        msti61_total_cross=0

        call decay_resonances(ncount)

        call ilc_calc_nfermion(nfermion)
        check_shower_enable: if(mstp(71).le.0.or.mstj(41).le.0) then
           nfermion=0
        end if check_shower_enable
        !
        ! call check_ep_conservation(f_ep_conserved)
        ! check_epa_pt: if(mcu%epa_pt.and.f_ep_conserved) then
        !  check_epa_count: if(count(mcu%epa_on).eq.1) then 
        !     call epa_pt_1
        !  elseif(count(mcu%epa_on).eq.2) then check_epa_count
        !     call epa_pt_2
        !  end if check_epa_count
        ! end if check_epa_pt


        nhep_original=nhep

        if ( mstj(28) .gt. 0 ) call ilc_tauola_ini

        call hepev4_fill





        check_ncount: if((ncount.le.nprint_max.or.ncount.eq.ncount0).and.msg_level>0) then
           print "(' after ilc_calc_nfermion: mstj(105),nfermion,ncount=',3i12)", mstj(105),nfermion,ncount
           call pyhepc(2)
           call heplst(1)
           call pylist(2)
        end if check_ncount
        !
        ! call disable_radiating_electrons(radiating)
        !
        nevhep_save=nevhep
        test_nfermion: select case(nfermion)
        case(2) test_nfermion
           call py2frm(mcu%i_qed_rad,itau,icom)
        case(4) test_nfermion
           call calc_a1sq_a2sq(atotsq,a1sq,a2sq)
           call py4frm(atotsq,a1sq,a2sq,istrat,mcu%i_qed_rad,itau,icom)
        case(5) test_nfermion
           print "(' from subroutine ilc_fragment_call unexpected nfermion=',i12)", nfermion
           print "(' program will now stop')"
           stop
        case(6) test_nfermion
           !...Call PYHEPC to convert input from HEPEVT to PYJETS common.
           call pyhepc(2)
           call calc_ptop(ptop)
           if((ncount.le.nprint_max.or.ncount.eq.ncount0).and.msg_level>0) print "(' case 6 ptop=',d14.6)", ptop
           call calc_p12_p32(p12,p13,p21,p23,p31,p32)
           call py6frm(p12,p13,p21,p23,p31,p32,ptop,mcu%i_qed_rad,itau,icom)         
        case(7) test_nfermion
           print "(' from subroutine ilc_fragment_call unexpected nfermion=',i12)", nfermion
           print "(' program will now stop')"
           stop
        case(8) test_nfermion
           call choose_2_fermion(fermion_mask) ! set fermion_mask=.T. for the ferm-antiferm pair which is not part of ttbar
           if((ncount.le.nprint_max.or.ncount.eq.ncount0).and.msg_level>0) print "(' from user_evt: fermion_mask=',8l3)", fermion_mask
           !...Call PYHEPC to convert input from HEPEVT to PYJETS common.
           call pyhepc(2)
           call enable_fermion(fermion_mask,8)  ! disable fermions from the ttbar
           !   if(ncount.le.nprint_max) call heplst(1)
           call py2frm(mcu%i_qed_rad,itau,1)
           !   if(ncount.le.nprint_max) call heplst(1)
           !   if(ncount.le.nprint_max) call pylist(1)
           fermion_mask=.not.fermion_mask
           call enable_invert_pyjets   ! enable fermions from the ttbar and disable the others; maintain pyjets record
           call calc_ptop(ptop)
           if((ncount.le.nprint_max.or.ncount.eq.ncount0).and.msg_level>0) print "(' case 8 ncount,ptop=',i10,d14.6)", ncount,ptop
           !   if(ncount.le.nprint_max) call heplst(1)
           call calc_p12_p32(p12,p13,p21,p23,p31,p32)
           check_ncount_8: if((ncount.le.nprint_max.or.ncount.eq.ncount0).and.msg_level>0) then
              print "(' before py6frm inside case(8) test_nfermion,ncount= ',i12)", ncount
              call heplst(1)
              call pylist(2)
           end if check_ncount_8
           call py6frm(p12,p13,p21,p23,p31,p32,ptop,mcu%i_qed_rad,itau,1)         
           !   if(ncount.le.nprint_max) call pylist(1)
           !   if(ncount.le.nprint_max) call heplst(1)
           call enable_all_pyjets ! enable everything in pyjets record
           !...Call PYHEPC to convert output from PYJETS to HEPEVT common
           call pyhepc(1)
           !   if(ncount.le.nprint_max) call heplst(1)
        case default test_nfermion

           !...Call PYHEPC to convert input from HEPEVT to PYJETS common.
           call pyhepc(2)
           call pyexec
           !...Call PYHEPC to convert output from PYJETS to HEPEVT common.
           call pyhepc(1)
        end select test_nfermion
        nevhep=nevhep_save

        call hepev4_update(tauspin_pyjets)

        if ( mstj(28) .gt. 0 ) call ilc_tauola_end

     end if check_mstp171


     call ilc_restore_documentation_particles

     check_ncount_2: if((ncount.le.nprint_max.or.ncount.eq.ncount0.or.mod(ncount,print_freq).eq.0).and.msti61_total_cross.eq.0.and.msg_level>0) then
        if ( ncount > nprint_max ) then
           nprinted = nprinted+1
           if ( nprinted > 20 ) then
             print_freq=print_freq*5
             nprinted = 0
           endif
        endif
        print "(' after fragmentation and decay: nfermion,ncount=',2i12)", nfermion,ncount
        call pyhepc(2)
        call heplst(2)
     end if check_ncount_2
  end if check_not_proton
  if ( mstu(23) /= 0 .or. mstu(24) /= 0 .or. mstu(28) /= 0 ) then
    print *, ' Pythia error : ', mstu(23),mstu(24),mstu(28),'. ncount = ',ncount
    print *, ' Error counters reset and event skipped '
    print *, ' event listing '
    call pyhepc(2)
    call pylist(3)
    mstu(23) = 0 ; mstu(24) = 0 ; mstu(28) = 0
    nevhep=-nevhep ; ncount = ncount - 1 
  endif
  return
end subroutine ilc_fragment_call
