  subroutine hide_beamstr_elec
  use kinds, only: double 
  use hepevt_include_common
  implicit none
!
  integer :: i

  ialhep(1:nhep)=0
  nhide=0
  loop_i: do i=1,nhep
!    if(abs(idhep(i)).eq.11) print *, "i,idhep,count(...)=",  &
!         &   i,idhep(i),count(isthep(1:nhep).eq.1.and.idhep(1:nhep).eq.22.and.abs(phep(1,1:nhep)).lt.1.d-10.and. &
!         &     abs(phep(2,1:nhep)).lt.1.d-10.and.phep(3,1:nhep)*phep(3,i).gt.0.d0)
     check_for_fermion: if(isthep(i).eq.1.and.  &
          &     abs(idhep(i)).eq.11.and.abs(phep(1,i)).lt.1.d-10.and.abs(phep(2,i)).lt.1.d-10.and. &
          &     count(isthep(1:nhep).eq.1.and.idhep(1:nhep).eq.22.and.abs(phep(1,1:nhep)).lt.1.d-10.and. &
          &     abs(phep(2,1:nhep)).lt.1.d-10.and.phep(3,1:nhep)*phep(3,i).gt.0.d0).eq.0) then
        ialhep(i)=1
        idhep(i)=sign(12,idhep(i))
        nhide=nhide+1
        p3hide(nhide)=phep(3,i)
!       print *, " i,ialhep(i)=", i,ialhep(i)
!
     end if check_for_fermion
!
  end do loop_i


  return
  end subroutine hide_beamstr_elec

