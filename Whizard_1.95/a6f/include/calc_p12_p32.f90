subroutine calc_p12_p32(p12,p13,p21,p23,p31,p32)
  use kinds, only: double 
  use hepevt_include_common
  use pyjets_common
  use pydat1_common
  use pydat2_common
  implicit none
  save
  real(kind=double), intent(out) :: p12
  real(kind=double), intent(out) :: p13
  real(kind=double), intent(out) :: p21
  real(kind=double), intent(out) :: p23
  real(kind=double), intent(out) :: p31
  real(kind=double), intent(out) :: p32
  !
  real(kind=double), dimension(3) :: asq
  real(kind=double), dimension(5,3) :: pij
  real(kind=double)               :: stw
  real(kind=double)               :: ctw
  real(kind=double)               :: gwl
  real(kind=double)               :: gwr
  real(kind=double)               :: gw2
  real(kind=double)               :: xmw
  real(kind=double)               :: gmw
  real(kind=double)               :: xmz
  real(kind=double)               :: gmz
  real(kind=double), dimension(:), pointer :: ggl
  real(kind=double), dimension(:), pointer :: ggr
  real(kind=double), dimension(:), pointer :: gg2
  real(kind=double), dimension(:), pointer :: gzl
  real(kind=double), dimension(:), pointer :: gzr
  real(kind=double), dimension(:), pointer :: gz2
  integer :: i
  integer :: j
  integer :: jd
  integer :: kl
  integer :: kk
  integer :: nfermion
  integer :: ncall=0
  integer :: nqqqq=0
  integer :: pycomp
  integer, dimension(6) :: ifermion
  integer, parameter :: nprint_max=20
  integer, parameter :: nw_comb=18
  integer, parameter :: nz_comb=11
  integer, dimension(:,:), pointer :: iw_comb
  integer, dimension(:,:), pointer :: iz_comb
  integer, dimension(6,6)          :: im
  !
  ncall=ncall+1
  check_ncall: if(ncall.eq.1) then
     allocate (ggl(nz_comb))
     allocate (ggr(nz_comb))
     allocate (gg2(nz_comb))
     allocate (gzl(nz_comb))
     allocate (gzr(nz_comb))
     allocate (gz2(nz_comb))
     allocate (iw_comb(2,nw_comb))
     allocate (iz_comb(2,nz_comb))
     stw=sqrt(paru(102))
     ctw=sqrt(1.d0-paru(102))
     xmw=pmas(pycomp(24),1)
     gmw=pmas(pycomp(24),2)
     xmz=pmas(pycomp(23),1)
     gmz=pmas(pycomp(23),2)
     gwl=1.d0/2.d0/sqrt(2.d0)
     gwr=0.d0
     ggl(1)=(stw/2.d0)*(-1.d0/3.d0)
     ggr(1)=ggl(1)
     ggl(2)=(stw/2.d0)*(2.d0/3.d0)
     ggr(2)=ggl(2)
     ggl(3)=ggl(1)
     ggr(3)=ggr(1)
     ggl(4)=ggl(2)
     ggr(4)=ggr(2)
     ggl(5)=ggl(1)
     ggr(5)=ggr(1)
     ggl(6)=(stw/2.d0)*(-1.d0)
     ggr(6)=ggl(6)
     ggl(7)=0.d0
     ggr(7)=ggl(7)
     ggl(8)=ggl(6)
     ggr(8)=ggr(6)
     ggl(9)=ggl(7)
     ggr(9)=ggr(7)
     ggl(10)=ggl(6)
     ggr(10)=ggr(6)
     ggl(11)=ggl(7)
     ggr(11)=ggr(7)
     gzl(1)=0.5d0/ctw*(-0.5d0-(-1.d0/3.d0)*stw**2)
     gzr(1)=0.5d0/ctw*(-(-1.d0/3.d0)*stw**2)
     gzl(2)=0.5d0/ctw*(0.5d0-(2.d0/3.d0)*stw**2)
     gzr(2)=0.5d0/ctw*(-(2.d0/3.d0)*stw**2)
     gzl(3)=gzl(1)
     gzr(3)=gzr(1)
     gzl(4)=gzl(2)
     gzr(4)=gzr(2)
     gzl(5)=gzl(1)
     gzr(5)=gzr(1)
     gzl(6)=0.5d0/ctw*(-0.5d0-(-1.d0)*stw**2)
     gzr(6)=0.5d0/ctw*(-(-1.d0)*stw**2)
     gzl(7)=0.5d0/ctw*(0.5d0-(0.d0)*stw**2)
     gzr(7)=0.5d0/ctw*(-(0.d0)*stw**2)
     gzl(8)=gzl(6)
     gzr(8)=gzr(6)
     gzl(9)=gzl(7)
     gzr(9)=gzr(7)
     gzl(10)=gzl(6)
     gzr(10)=gzr(6)
     gzl(11)=gzl(7)
     gzr(11)=gzr(7)
     gw2=gwl**2+gwr**2
     gg2=ggl**2+ggr**2
     gz2=gzl**2+gzr**2
     print "(' xmw,gmw,xmz,gmz=',4d14.6)", xmw,gmw,xmz,gmz
     print "(' stw**2=',d14.6)", stw**2
     print "(' gwl,gwr=',2d14.6)", gwl,gwr
     print "(' ggl(1),ggr(1)=',2d14.6)", ggl(1),ggr(1)
     print "(' ggl(2),ggr(2)=',2d14.6)", ggl(2),ggr(2)
     print "(' gzl(1),gzr(1)=',2d14.6)", gzl(1),gzr(1)
     print "(' gzl(2),gzr(2)=',2d14.6)", gzl(2),gzr(2)
     print "(' ggl(6),ggr(6)=',2d14.6)", ggl(6),ggr(6)
     print "(' ggl(7),ggr(7)=',2d14.6)", ggl(7),ggr(7)
     print "(' gzl(6),gzr(6)=',2d14.6)", gzl(6),gzr(6)
     print "(' gzl(7),gzr(7)=',2d14.6)", gzl(7),gzr(7)
     im(1,1)=1
     im(2,1)=2
     im(3,1)=3
     im(4,1)=4
     im(5,1)=5
     im(6,1)=6
     im(1,2)=1
     im(2,2)=2
     im(3,2)=3
     im(4,2)=6 
     im(5,2)=5
     im(6,2)=4
     im(1,3)=1
     im(2,3)=4
     im(3,3)=3
     im(4,3)=2
     im(5,3)=5
     im(6,3)=6
     im(1,4)=1
     im(2,4)=4
     im(3,4)=3
     im(4,4)=6
     im(5,4)=5
     im(6,4)=2
     im(1,5)=1
     im(2,5)=6
     im(3,5)=3
     im(4,5)=2
     im(5,5)=5
     im(6,5)=4
     im(1,6)=1
     im(2,6)=6
     im(3,6)=3
     im(4,6)=4
     im(5,6)=5
     im(6,6)=2
     iw_comb(1,1)=2
     iw_comb(2,1)=-1
     iw_comb(1,2)=4
     iw_comb(2,2)=-3
     iw_comb(1,3)=1
     iw_comb(2,3)=-2
     iw_comb(1,4)=3
     iw_comb(2,4)=-4
     iw_comb(1,5)=11
     iw_comb(2,5)=-12
     iw_comb(1,6)=13
     iw_comb(2,6)=-14
     iw_comb(1,7)=15
     iw_comb(2,7)=-16
     iw_comb(1,8)=12
     iw_comb(2,8)=-11
     iw_comb(1,9)=14
     iw_comb(2,9)=-13
     iw_comb(1,10)=16
     iw_comb(2,10)=-15
     iw_comb(1,11)=2
     iw_comb(2,11)=-3
     iw_comb(1,12)=2
     iw_comb(2,12)=-5
     iw_comb(1,13)=4
     iw_comb(2,13)=-1
     iw_comb(1,14)=4
     iw_comb(2,14)=-5
     iw_comb(1,15)=3
     iw_comb(2,15)=-2
     iw_comb(1,16)=5
     iw_comb(2,16)=-2
     iw_comb(1,17)=1
     iw_comb(2,17)=-4
     iw_comb(1,18)=5
     iw_comb(2,18)=-4
     iz_comb(1,1)=1
     iz_comb(2,1)=-1
     iz_comb(1,2)=2
     iz_comb(2,2)=-2
     iz_comb(1,3)=3
     iz_comb(2,3)=-3
     iz_comb(1,4)=4
     iz_comb(2,4)=-4
     iz_comb(1,5)=5
     iz_comb(2,5)=-5
     iz_comb(1,6)=11
     iz_comb(2,6)=-11
     iz_comb(1,7)=12
     iz_comb(2,7)=-12
     iz_comb(1,8)=13
     iz_comb(2,8)=-13
     iz_comb(1,9)=14
     iz_comb(2,9)=-14
     iz_comb(1,10)=15
     iz_comb(2,10)=-15
     iz_comb(1,11)=16
     iz_comb(2,11)=-16
  end if check_ncall
  !
  nfermion=0
  loop_i: do i=1,n
     check_for_fermion: if((k(i,1).ge.1.and.k(i,1).le.3.and.k(i,3).eq.0).and.(  &
          &     (abs(k(i,2)).ge.1.and.abs(k(i,2)).le.5)  &
          & .or.(abs(k(i,2)).ge.11.and.abs(k(i,2)).le.16))) then
        !
        nfermion=nfermion+1
        check_nfermion: if(nfermion.le.6) then
           ifermion(nfermion)=i
        else check_nfermion
           print "(' from subroutine calc_p12_p32  pt 1 unexpected nfermion=',i12/' program will now stop after calling heplst(1) and pylist(3)')", nfermion
           call heplst(1)
           call pylist(3)
           stop
        end if check_nfermion
        !
     end if check_for_fermion
     !
  end do loop_i

  check_nfermion8: if(nfermion.ne.6) then
     print "(' from subroutine calc_p12_p32  pt 2 unexpected nfermion=',i12/' program will now stop after calling heplst(1) and pylist(3)')", nfermion
     call heplst(1)
     call pylist(3)
     stop
  end if check_nfermion8


  nqqqq=nqqqq+1
  if(nqqqq.le.nprint_max) print "(' nqqqq,k(ifermion,2)=',i10,5x,6i6)", nqqqq,k(ifermion,2)

  check_whizard_color: if(count(icolorflowlh(:,ifermion).ne.0).ge.2) then
     check_choice: if(icolorflowlh(1,ifermion(1)).eq.icolorflowlh(2,ifermion(2)).and.icolorflowlh(1,ifermion(3)).eq.icolorflowlh(2,ifermion(4)).and.icolorflowlh(1,ifermion(5)).eq.icolorflowlh(2,ifermion(6))) then
           p12=1.
           p13=0.
           p21=0.
           p23=0.
           p31=0.
           p32=0.
     elseif(icolorflowlh(1,ifermion(1)).eq.icolorflowlh(2,ifermion(2)).and.icolorflowlh(1,ifermion(3)).eq.icolorflowlh(2,ifermion(6)).and.icolorflowlh(1,ifermion(5)).eq.icolorflowlh(2,ifermion(4))) then check_choice
           p12=0.
           p13=1.
           p21=0.
           p23=0.
           p31=0.
           p32=0.
     elseif(icolorflowlh(1,ifermion(1)).eq.icolorflowlh(2,ifermion(4)).and.icolorflowlh(1,ifermion(3)).eq.icolorflowlh(2,ifermion(2)).and.icolorflowlh(1,ifermion(5)).eq.icolorflowlh(2,ifermion(6))) then check_choice
           p12=0.
           p13=0.
           p21=1.
           p23=0.
           p31=0.
           p32=0.
     elseif(icolorflowlh(1,ifermion(1)).eq.icolorflowlh(2,ifermion(4)).and.icolorflowlh(1,ifermion(3)).eq.icolorflowlh(2,ifermion(6)).and.icolorflowlh(1,ifermion(5)).eq.icolorflowlh(2,ifermion(2))) then check_choice
           p12=0.
           p13=0.
           p21=0.
           p23=1.
           p31=0.
           p32=0.
     elseif(icolorflowlh(1,ifermion(1)).eq.icolorflowlh(2,ifermion(6)).and.icolorflowlh(1,ifermion(3)).eq.icolorflowlh(2,ifermion(2)).and.icolorflowlh(1,ifermion(5)).eq.icolorflowlh(2,ifermion(4))) then check_choice
           p12=0.
           p13=0.
           p21=0.
           p23=0.
           p31=1.
           p32=0.
     elseif(icolorflowlh(1,ifermion(1)).eq.icolorflowlh(2,ifermion(6)).and.icolorflowlh(1,ifermion(3)).eq.icolorflowlh(2,ifermion(4)).and.icolorflowlh(1,ifermion(5)).eq.icolorflowlh(2,ifermion(2))) then check_choice
           p12=0.
           p13=0.
           p21=0.
           p23=0.
           p31=0.
           p32=1.
     else check_choice
        print *, " from subroutine calc_p12_p32  invalid icolorflow "
        print *, "(' ifermion=',4i8)", ifermion
        print *, "(' idhep(ifermion)=',4i8)", idhep(ifermion)
        print *, "(' icolorflowlh(1,ifermion)=',4i8)", icolorflowlh(1,ifermion)
        print *, "(' icolorflowlh(2,ifermion)=',4i8)", icolorflowlh(2,ifermion)
        call heplst(1)
        call pylist(2)
        print *, " program will now stop"
        stop
     end if check_choice
  else check_whizard_color
     loop_i_perm: do i=1,6

        pij(1:4,1)=p(ifermion(im(1,i)),1:4)+p(ifermion(im(2,i)),1:4)
        pij(1:4,2)=p(ifermion(im(3,i)),1:4)+p(ifermion(im(4,i)),1:4)
        pij(1:4,3)=p(ifermion(im(5,i)),1:4)+p(ifermion(im(6,i)),1:4)
        loop_jd: do jd=1,3
           check_neg_sqrt: if(pij(4,jd)**2-sum(pij(1:3,jd)**2).lt.0.d0) then
              print "(' from subroutine calc_p12_p32  neg sqrt problem  jd=',i12,' pij(1:4,jd)=',4d14.6)", jd, pij(1:4,jd)
              print "(' im1,im2=',2i12,' ifermion(im1),ifermion(im2)=',2i12)", im(2*jd-1,i), im(2*jd,i), ifermion(im(2*jd-1,i)), ifermion(im(2*jd,i))
              print "(' ifermion=',6i4)", ifermion
              print "(' p(ifermion(im1),1:4)=',4d14.6)", p(ifermion(im(2*jd-1,i)),1:4)
              print "(' p(ifermion(im2),1:4)=',4d14.6)", p(ifermion(im(2*jd,i)),1:4)
              call heplst(2)
              exit loop_jd
           end if check_neg_sqrt
        end do loop_jd
        pij(5,:)=sqrt(max(0.d0,pij(4,:)**2-sum(pij(1:3,:)**2,dim=1)))
        if(nqqqq.le.nprint_max) print "(' i=',i6,' pij(5,:)=',3d14.6)", i,pij(5,:)

        loop_k: do kl=1,3
           asq(kl)=0.d0
           kk=2*kl-1

           loop_j_w: do j=1,nw_comb
              check_match_w: if(k(ifermion(im(kk,i)),2).eq.iw_comb(1,j).and.k(ifermion(im(kk+1,i)),2).eq.iw_comb(2,j)) then
                 check_quark_w: if(abs(k(ifermion(im(kk,i)),2)).le.5) then
                    asq(kl)=gw2/((pij(5,kl)**2-xmw**2)**2+gmw**2*xmw**2)
                 else check_quark_w
                    asq(kl)=1.d0
                 end if check_quark_w
                 if(nqqqq.le.nprint_max) print "(' found w vertex, kl,asq(kl)=',i6,d14.6)", kl,asq(kl)
                 cycle loop_k
              end if check_match_w
           end do loop_j_w

           loop_j_z: do j=1,nz_comb
              check_match_z: if(k(ifermion(im(kk,i)),2).eq.iz_comb(1,j).and.k(ifermion(im(kk+1,i)),2).eq.iz_comb(2,j)) then
                 check_quark_z: if(abs(k(ifermion(im(kk,i)),2)).le.5) then
                    asq(kl)=gg2(j)/pij(5,kl)**4+gz2(j)/((pij(5,kl)**2-xmz**2)**2+gmz**2*xmz**2)
                 else check_quark_z
                    asq(kl)=1.d0
                 end if check_quark_z
                 if(nqqqq.le.nprint_max) print "(' found z vertex, kl,asq(kl)=',i6,d14.6)", kl,asq(kl)
                 cycle loop_k
              end if check_match_z
           end do loop_j_z

        end do loop_k

        test_i_perm: select case(i)
        case(1) test_i_perm
           p12=asq(1)*asq(2)*asq(3)
           if(nqqqq.le.nprint_max) print "(' i,p12,asq(1),asq(2),asq(3)=',i6,4d14.6)", i,p12,asq(1),asq(2),asq(3)
        case(2) test_i_perm
           p13=asq(1)*asq(2)*asq(3)
           if(nqqqq.le.nprint_max) print "(' i,p13,asq(1),asq(2),asq(3)=',i6,4d14.6)", i,p13,asq(1),asq(2),asq(3)
        case(3) test_i_perm
           p21=asq(1)*asq(2)*asq(3)
           if(nqqqq.le.nprint_max) print "(' i,p21,asq(1),asq(2),asq(3)=',i6,4d14.6)", i,p21,asq(1),asq(2),asq(3)
        case(4) test_i_perm
           p23=asq(1)*asq(2)*asq(3)
           if(nqqqq.le.nprint_max) print "(' i,p23,asq(1),asq(2),asq(3)=',i6,4d14.6)", i,p23,asq(1),asq(2),asq(3)
        case(5) test_i_perm
           p31=asq(1)*asq(2)*asq(3)
           if(nqqqq.le.nprint_max) print "(' i,p31,asq(1),asq(2),asq(3)=',i6,4d14.6)", i,p31,asq(1),asq(2),asq(3)
        case(6) test_i_perm
           p32=asq(1)*asq(2)*asq(3)
           if(nqqqq.le.nprint_max) print "(' i,p32,asq(1),asq(2),asq(3)=',i6,4d14.6)", i,p32,asq(1),asq(2),asq(3)
        end select test_i_perm

     end do loop_i_perm
  end if check_whizard_color

  if(nqqqq.le.nprint_max) print "(' p12,p13,p21,p23,p31,p32=',6d14.6)", p12,p13,p21,p23,p31,p32

  return
end subroutine calc_p12_p32

