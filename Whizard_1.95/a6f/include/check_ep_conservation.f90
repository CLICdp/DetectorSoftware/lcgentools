  subroutine check_ep_conservation(f_ok)
  use kinds, only: double 
  use hepevt_include_common
  use a6f_users, only: mcu
  implicit none
  logical, intent(out) :: f_ok

!


  real(kind=double), parameter :: e_tol=1.d-8
  real(kind=double), dimension(5) :: ptot
  real(kind=double), dimension(5) :: p1
  real(kind=double), dimension(5) :: p2
  real(kind=double), dimension(3,3) :: rotm
  real(kind=double), dimension(3,3) :: testu
  real(kind=double) :: r3
  real(kind=double) :: r0
  real(kind=double) :: a1_2
  real(kind=double) :: eta
  real(kind=double) :: ct2
  real(kind=double) :: st2
  real(kind=double) :: ph2
  real(kind=double) :: cph2
  real(kind=double) :: sph2
  logical, dimension(:), pointer :: f_nc
  integer :: i
  integer :: i1
  integer :: i2
  integer :: ncount
  integer, dimension(1) :: ia
   

  ncount=ncount+1

  ptot(1:4)=sum(phep(1:4,1:nhep),dim=2,mask=spread(isthep(1:nhep).eq.1,dim=1,ncopies=4))

  f_ok=.true.

  check_e_tol: if(abs(ptot(1)).gt.e_tol.or.abs(ptot(2)).gt.e_tol.or.abs(ptot(3)).gt.e_tol &
       &  .or.abs(ptot(4)-mcu%sqrts).gt.e_tol) then
     allocate (f_nc(nhep))
     f_nc=((abs(idhep(1:nhep)).ge.11.and.abs(idhep(1:nhep)).le.16).or.idhep(1:nhep).eq.22).and.isthep(1:nhep).eq.1
     check_pt_tol: if(abs(ptot(1)).gt.e_tol.or.abs(ptot(2)).gt.e_tol) then
        ia=maxloc(sum(phep(1:2,1:nhep)**2,dim=1),mask=f_nc)
        i1=ia(1)
     else check_pt_tol
        ia=maxloc(phep(4,1:nhep),mask=f_nc)
        i1=ia(1)
     end if check_pt_tol
     check_i1_valid: if(i1.ge.1.and.i1.le.nhep) then
        f_nc(i1)=.false.
        check_pt1: if(abs(phep(1,i1)).gt.e_tol.or.abs(phep(2,i1)).gt.e_tol) then
           ia=maxloc(phep(4,1:nhep),mask=phep(5,1:nhep).lt.e_tol.and.f_nc.and.phep(3,1:nhep)*phep(3,i1).lt.0.d0)
        else check_pt1
           ia=maxloc(phep(4,1:nhep),mask=abs(phep(1,1:nhep)).lt.e_tol.and.abs(phep(2,1:nhep)).lt.e_tol  & 
                & .and.phep(5,1:nhep).lt.e_tol.and.f_nc.and.phep(3,1:nhep)*phep(3,i1).lt.0.d0)
        end if check_pt1
        i2=ia(1)
     else check_i1_valid
        i2=0
     end if check_i1_valid
     check_i2_valid: if(i2.lt.1.or.i2.gt.nhep) then
        !       print "(' from subroutine check_ep_conservation, ncount=',i12)", ncount
        !       print "(' invalid i1,i2=',2i12)", i1,i2
        !       print "(' program will stop after calling heplst and pylist')"
        !       call heplst(1)
        !       call pylist(2)
        !       stop
        f_ok=.false.
     else check_i2_valid

        check_pt2: if(abs(phep(1,i2)).gt.e_tol.or.abs(phep(2,i2)).gt.e_tol) then
           ct2=phep(3,i2)/phep(4,i2)
           st2=sqrt(1.d0-ct2**2)
           ph2=atan2(phep(2,i2),phep(1,i2))
           cph2=cos(ph2)
           sph2=sin(ph2)
        else check_pt2
           ct2=1.d0
           st2=0.d0
           cph2=1.d0
           sph2=0.d0
        end if check_pt2

        ! rotate phep(:,i1), phep(:,i2), ptot so phep(:,i2) is pointing in +z direction

        rotm(1,1)=ct2*cph2
        rotm(1,2)=ct2*sph2
        rotm(1,3)=-st2
        rotm(2,1)=-sph2
        rotm(2,2)=cph2
        rotm(2,3)=0.d0
        rotm(3,1)=st2*cph2
        rotm(3,2)=st2*sph2
        rotm(3,3)=ct2

        phep(1:3,i1)=matmul(rotm,phep(1:3,i1))
        phep(1:3,i2)=matmul(rotm,phep(1:3,i2))
        ptot(1:3)=matmul(rotm,ptot(1:3))

        p1(1:2)=phep(1:2,i1)-ptot(1:2)
        p1(5)=phep(5,i1)
        a1_2=p1(1)**2+p1(2)**2+p1(5)**2

        eta=sign(1.d0,phep(3,i2))
        r3=-(ptot(3)-phep(3,i1)-phep(3,i2))
        r0=mcu%sqrts-(ptot(4)-phep(4,i1)-phep(4,i2))
        p2(4)=(r0**2-r3**2-a1_2)/2.d0/(r0-eta*r3)
        p2(3)=eta*p2(4)
        p1(4)=r0-p2(4)
        p1(3)=r3-p2(3)
        p2(1:2)=0.d0
        p2(5)=0.d0
        phep(:,i1)=p1
        phep(:,i2)=p2

        ! rotate phep(:,i1), phep(:,i2) back to lab orientation

        phep(1:3,i1)=matmul(transpose(rotm),phep(1:3,i1))
        phep(1:3,i2)=matmul(transpose(rotm),phep(1:3,i2))


        check_e1_e2: if(p1(4).lt.0.d0.or.p2(4).lt.0.d0) then
           !       print "(' from subroutine check_ep_conservation, ncount=',i12)", ncount
           !       print "(' invalid e1,e2=',2d20.12)", p1(4),p2(4)
           !       print "(' program will stop after calling heplst and pylist')"
           !       call heplst(1)
           !       call pylist(2)
           !       stop
           f_ok=.false.
        end if check_e1_e2

     end if check_i2_valid

     deallocate (f_nc)

  end if check_e_tol

  return
end subroutine check_ep_conservation
