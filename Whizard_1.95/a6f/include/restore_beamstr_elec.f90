subroutine restore_beamstr_elec
  use kinds, only: double 
  use hepevt_include_common
  implicit none
  !
  integer :: i
  integer :: j
  integer :: in
  integer :: jn
  integer :: idmother
  integer :: nrestore

  nrestore=0

  loop_i: do i=1,nhep
!    if(abs(idhep(i)).eq.12) print *, " from restore beamstr_elec i,idhep(i),isthep(i),phep(1,i),phep(2,i)=", i,idhep(i),isthep(i),phep(1,i),phep(2,i)
     check_for_fermion: if(abs(idhep(i)).eq.12.and.abs(phep(1,i)).lt.1.d-10.and.abs(phep(2,i)).lt.1.d-10) then
        loop_hide: do j=1,nhide
!          print *, "i,j,phep(3,i),p3hide(j)=", i,j,phep(3,i),p3hide(j)
           check_p3: if(phep(3,i)*p3hide(j).gt.0.d0) then
              idhep(i)=sign(11,idhep(i))
!             print *, " found restore i,j=", i,j
              if(isthep(i).eq.1) nrestore=nrestore+1
           end if check_p3
        end do loop_hide
     end if check_for_fermion
     !
     !
  end do loop_i

  check_nrestore: if(nrestore.ne.nhide) then
     print *, " from subroutine restore_beamstr_elec"
     print *, " nrestore,nhide=", nrestore,nhide
     print *, " p3hide=", p3hide(1:nhide)
     print *, " will call heplst(1) and stop"
     call heplst(1)
     stop
  end if check_nrestore

  return
end subroutine restore_beamstr_elec

