  subroutine enable_fermion(fermion_mask,nf0)
  use pyjets_common
  implicit none
  logical, dimension(8), intent(in)  :: fermion_mask
  integer, intent(in)                :: nf0
!
  integer :: i
  integer :: nfermion

  check_nf0: if(nf0.gt.8) then
  print "(' from subroutine enable_fermion unexpected nf0=',i12/' program will now stop')", nf0
  stop
  end if check_nf0
  
  nfermion=0
  loop_i: do i=1,n
  check_ist_id: if(k(i,1).ge.1.and.k(i,1).le.3.and.k(i,3).eq.0.and. &
      &    ((abs(k(i,2)).ge.1.and.abs(k(i,2)).le.5)  &
      & .or.(abs(k(i,2)).ge.11.and.abs(k(i,2)).le.16))) then
        nfermion=nfermion+1
!       print "(' enable_fermion  nfermion,i=',2i6)", nfermion,i
!
  check_nfermion: if(nfermion.le.nf0) then
!
  if(.not.fermion_mask(nfermion)) k(i,1)=-k(i,1)
!
  else check_nfermion
  print "(' from subroutine enable_fermion  pt 1 unexpected nfermion,nf0=',2i12/' program will now stop')", nfermion,nf0
  stop
  end if check_nfermion
!
  end if check_ist_id
!
  end do loop_i

  check_nfermion_nf0: if(nfermion.ne.nf0) then
  print "(' from subroutine enable_fermion  pt 2 unexpected nfermion,nf0=',2i12/' program will now stop')", nfermion,nf0
  stop
  end if check_nfermion_nf0
  

  return
  end subroutine enable_fermion

