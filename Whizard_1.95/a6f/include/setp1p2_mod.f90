module setp1p2_mod
  use kinds, only: double
  use a6f_users, only: mcu
  use hepevt_include_common
  use pyjets_common
  implicit none

  private

  public :: setmuontaumass
  public :: setp1p2
  public :: add_spectator_electron_gamma
  public :: deallocate_spectator

  real(kind=double), parameter               :: xmmu_=0.10566d0
  real(kind=double), parameter               :: xmtau_=1.77684d0
  real(kind=double), dimension(:,:), pointer :: phep_spectator
  real(kind=double), dimension(:,:), pointer :: vhep_spectator
  integer, dimension(:), pointer :: isthep_spectator
  integer, dimension(:), pointer :: ialhep_spectator
  integer, dimension(:), pointer :: idhep_spectator
  integer, dimension(:,:), pointer :: jmohep_spectator
  integer, dimension(:,:), pointer :: jdahep_spectator
  integer :: nhep_spectator
  logical :: f_alloc_phep=.false.



contains

  subroutine setp1p2

    integer :: i
    integer :: nhep_ist1
  real(kind=double), dimension(4) :: p3p4

 
  p(1:2,1:2)=0.d0
  p(1,3)=0.5d0*mcu%sqrts
  p(2,3)=-p(1,3)
  nhep_ist1=0
  p3p4=0.
  loop_nhep_ini: do i=1,nhep
     check_ist_ini: if(isthep(i).eq.1) then
        nhep_ist1=nhep_ist1+1
        p3p4=p3p4+phep(1:4,i)
     end if check_ist_ini
  end do loop_nhep_ini
!  p3p4=sum(phep(1:4,1:nhep),dim=2)
    check_nhep: if(nhep_ist1.ge.4.and.nhep_ist1.le.6) then
       allocate(phep_spectator(5,nhep_ist1-2))
       allocate(vhep_spectator(5,nhep_ist1-2))
       allocate(isthep_spectator(nhep_ist1-2))
       allocate(ialhep_spectator(nhep_ist1-2))
       allocate(idhep_spectator(nhep_ist1-2))
       allocate(jmohep_spectator(2,nhep_ist1-2))
       allocate(jdahep_spectator(2,nhep_ist1-2))
       f_alloc_phep=.true.
       nhep_spectator=0
       loop_nhep_0: do i=1,nhep
          check_not_d: if(isthep(i).eq.1.and.(abs(idhep(i)).eq.11.or.abs(idhep(i)).eq.12.or.abs(idhep(i)).eq.22)) then
             nhep_spectator=nhep_spectator+1
             p3p4=p3p4-phep(1:4,i)
             check_nhep_spectator: if(nhep_spectator.le.nhep-2) then
                phep_spectator(:,nhep_spectator)=phep(:,i)
                vhep_spectator(:,nhep_spectator)=vhep(:,i)
                isthep_spectator(nhep_spectator)=isthep(i)
                ialhep_spectator(nhep_spectator)=ialhep(i)
                idhep_spectator(nhep_spectator)=idhep(i)
                jmohep_spectator(:,nhep_spectator)=jmohep(:,i)
                jdahep_spectator(:,nhep_spectator)=jdahep(:,i)
                check_pz: if(phep(3,i).gt.0.d0) then
                   p(1,3)=p(1,3)-phep(3,i)
                else check_pz
                   p(2,3)=p(2,3)-phep(3,i)
                end if check_pz
             else check_nhep_spectator
                print *, " from setp1p2 inconsistent nhep_spectator,nhep=", nhep_spectator,nhep
                print *, " program will now stop"
                stop
             end if check_nhep_spectator
          end if check_not_d
 
       end do loop_nhep_0
       p(1,3)=0.5*(p3p4(4)+p3p4(3))
       p(2,3)=-0.5*(p3p4(4)-p3p4(3))


    end if check_nhep
    return

  end subroutine setp1p2




  subroutine add_spectator_electron_gamma

    phep(:,nhep+1:nhep+nhep_spectator)=phep_spectator
    vhep(:,nhep+1:nhep+nhep_spectator)=vhep_spectator
    isthep(nhep+1:nhep+nhep_spectator)=isthep_spectator
    ialhep(nhep+1:nhep+nhep_spectator)=ialhep_spectator
    idhep(nhep+1:nhep+nhep_spectator)=idhep_spectator
    jmohep(:,nhep+1:nhep+nhep_spectator)=jmohep_spectator
    jdahep(:,nhep+1:nhep+nhep_spectator)=jdahep_spectator
    nhep=nhep+nhep_spectator


    return

  end subroutine add_spectator_electron_gamma

  subroutine deallocate_spectator
    check_f_alloc: if(f_alloc_phep) then
       deallocate(phep_spectator)
       deallocate(vhep_spectator)
       deallocate(isthep_spectator)
       deallocate(ialhep_spectator)
       deallocate(idhep_spectator)
       deallocate(jmohep_spectator)
       deallocate(jdahep_spectator)
       f_alloc_phep=.false.
    end if check_f_alloc

    return

  end subroutine deallocate_spectator


  subroutine setmuontaumass

    integer :: i
    loop_nhep_0: do i=1,nhep
       check_muontau: if(abs(idhep(i)).eq.13.and.abs(phep(5,i)-xmmu_).gt.0.05*xmmu_) then
          phep(5,i)=xmmu_
          phep(4,i)=sqrt(sum(phep(1:3,i)**2)+xmmu_**2)
       elseif(abs(idhep(i)).eq.15.and.abs(phep(5,i)-xmtau_).gt.0.05*xmtau_) then check_muontau
          phep(5,i)=xmtau_
          phep(4,i)=sqrt(sum(phep(1:3,i)**2)+xmtau_**2)
       end if check_muontau
    end do loop_nhep_0
  end subroutine setmuontaumass
end module setp1p2_mod

















