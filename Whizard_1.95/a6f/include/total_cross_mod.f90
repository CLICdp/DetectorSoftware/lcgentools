module total_cross_mod
  use kinds, only: double, i64
  use pyr_inter
  use pycomp_inter
  use pyjets_common
  use pydat2_common
  use pyint1_common
  use pyint7_common
  use hepevt_include_common
  implicit none

  private

  public :: total_cross_pyevnt
  public :: aa_pipi

!  real(kind=double), public, parameter :: sqrt_shat_total_cross_min=2.0d0
  real(kind=double), public, parameter :: sqrt_shat_total_cross_min=10.d0
  real(kind=double), public :: sum_total_cross=0.d0
  real(kind=double), public :: sqrt_shat_total_cross
  real(kind=double), public :: sig_total_cross
  real(kind=double) :: xmpip
  real(kind=double) :: xmrhop
  real(kind=double) :: srt_rhorho
  real(kind=double) :: srt_pirho
  real(kind=double), parameter :: xmeps=0.01d0
  real(kind=double), parameter :: pi=3.1415926536d0

  integer(i64), public :: ntries_total_cross=0
  integer, public :: msti61_total_cross
  integer, parameter :: kf_pip=211
  integer, parameter :: kf_rhop=213
  integer(i64), public  :: ncall_aa_pipi=0
  integer(i64), public  :: ncall_force_pyevnt=0



contains

  subroutine total_cross_pyevnt

    real(kind=double), parameter :: cross_max=6.1d-4   ! max cross-section in mb

    ntries_total_cross=ntries_total_cross+1
    call a6f_pyevnt
    sqrt_shat_total_cross=vint(1)
    sig_total_cross=sigt(0,0,0)
    sum_total_cross=sum_total_cross+sig_total_cross
    if(ntries_total_cross.le.20) print *, "ntries_total_cross,sqrt_shat_total_cross,sig_total_cross=", ntries_total_cross,sqrt_shat_total_cross,sig_total_cross
    check_pyr: if(sig_total_cross/cross_max.gt.pyr(0)) then
       msti61_total_cross=0
    else check_pyr
       msti61_total_cross=1
    end if check_pyr

    return

  end subroutine total_cross_pyevnt

  subroutine aa_pipi

    real(kind=double) :: cost
    real(kind=double) :: sint
    real(kind=double) :: phi
    real(kind=double) :: pabs
    real(kind=double) :: eqq
    real(kind=double) :: pzqq
    real(kind=double) :: xmqq
    real(kind=double) :: gam
    real(kind=double) :: bet
    real(kind=double) :: gambet
    real(kind=double), dimension(5) :: pq
    real(kind=double), dimension(5) :: pqb
    integer :: i
    integer :: iq
    integer :: iqb

    ncall_aa_pipi=ncall_aa_pipi+1
    check_ncall_aa: if(ncall_aa_pipi.eq.1) then
       xmpip=pmas(pycomp(kf_pip),1)
       xmrhop=pmas(pycomp(kf_rhop),1)
       srt_rhorho=2.d0*xmrhop+xmeps
       srt_pirho=xmpip+xmrhop+xmeps
    end if check_ncall_aa

    iq=0
    iqb=0
       loop_nhep_0: do i=1,nhep
          check_d: if(abs(idhep(i)).ne.11.and.abs(idhep(i)).ne.22) then
             check_iq_iqb: if(iq.eq.0.and.iqb.eq.0) then
                iq=i
             else check_iq_iqb
                iqb=i
                exit loop_nhep_0
             end if check_iq_iqb
          end if check_d
       end do loop_nhep_0
       check_cons_iq: if(iq.eq.0.or.iqb.eq.0) then
          print *, " from aa_pipi in total_cross_mod, invalid iq,iqb=", iq,iqb
          print *, " program will now stop"
          stop
       end if check_cons_iq

       eqq=phep(4,iq)+phep(4,iqb)
       pzqq=phep(3,iq)+phep(3,iqb)
       xmqq=sqrt(max(0.d0,eqq**2-pzqq**2))
       gam=eqq/xmqq
       bet=pzqq/eqq
       gambet=gam*bet

       if(ncall_aa_pipi.le.20) print *, " ncall_aa_pipi,sqrt_shat_total_cross,xmqq=", ncall_aa_pipi,sqrt_shat_total_cross,xmqq

       check_sqrt_shat: if(xmqq.gt.srt_rhorho) then
          idhep(iq)=kf_rhop
          idhep(iqb)=-kf_rhop
          pq(5)=xmrhop
          pqb(5)=xmrhop
       elseif(xmqq.gt.srt_pirho) then check_sqrt_shat
          check_charge_pi_rho: if(pyr(0).gt.0.5d0) then
             idhep(iq)=kf_pip
             idhep(iqb)=-kf_rhop
             pq(5)=xmpip
             pqb(5)=xmrhop
          else check_charge_pi_rho
             idhep(iq)=kf_rhop
             idhep(iqb)=-kf_pip
             pq(5)=xmrhop
             pqb(5)=xmpip
          end if check_charge_pi_rho
       else check_sqrt_shat
          idhep(iq)=kf_pip
          idhep(iqb)=-kf_pip
          pq(5)=xmpip
          pqb(5)=xmpip
       end if check_sqrt_shat

       cost=-1.d0+2.d0*pyr(0)
       sint=sqrt(max(0.d0,1.d0-cost**2))
       phi=2.d0*pi*pyr(0)
       pq(4)=(xmqq**2-pqb(5)**2+pq(5)**2)/2.d0/xmqq
       pqb(4)=xmqq-pq(4)
       pabs=sqrt(max(0.d0,pq(4)**2-pq(5)**2))
       pq(1)=pabs*sint*cos(phi)
       pq(2)=pabs*sint*sin(phi)
       pq(3)=pabs*cost
       pqb(1:3)=-pq(1:3)
       isthep(iq)=1
       isthep(iqb)=1
       jmohep(:,iq)=0
       jdahep(:,iqb)=0

       phep(5,iq)=pq(5)
       phep(4,iq)=gam*pq(4)+gambet*pq(3)
       phep(3,iq)=gambet*pq(4)+gam*pq(3)
       phep(1:2,iq)=pq(1:2)

       phep(5,iqb)=pqb(5)
       phep(4,iqb)=gam*pqb(4)+gambet*pqb(3)
       phep(3,iqb)=gambet*pqb(4)+gam*pqb(3)
       phep(1:2,iqb)=pqb(1:2)

    return

  end subroutine aa_pipi


end module total_cross_mod

















