  subroutine calc_ptop(ptop)
  use kinds, only: double 
  use pyjets_common
  use pydat2_common
  implicit none
  real(kind=double), intent(out) :: ptop
!
  real(kind=double), dimension(5) :: p134
  real(kind=double), dimension(5) :: p256
! real(kind=double), parameter :: sigmax=10.d0
  real(kind=double), parameter :: sigmax=20.d0
! real(kind=double), parameter :: sigmax=3.d9
  real(kind=double)            :: x134
  real(kind=double)            :: x256
  real(kind=double)            :: h134
  real(kind=double)            :: h256
  integer :: i
  integer :: nfermion
  integer, dimension(6) :: ifermion
  nfermion=0
  ptop=0.d0
!  call pylist(3)
  loop_i: do i=1,n
     check_for_fermion: if((k(i,1).ge.1.and.k(i,1).le.3.and.k(i,3).eq.0).and.(  &
          &     (abs(k(i,2)).ge.1.and.abs(k(i,2)).le.5)  &
          & .or.(abs(k(i,2)).ge.11.and.abs(k(i,2)).le.16))) then
!
        nfermion=nfermion+1
        check_nfermion: if(nfermion.le.6) then
           ifermion(nfermion)=i
        else check_nfermion
           return
        end if check_nfermion
!
     end if check_for_fermion
!
  end do loop_i
!  print "(' from calc_ptop  nfermion=',i10)", nfermion
  if(nfermion.ne.6) return
  if(k(ifermion(1),2).ne.5) return
  if(k(ifermion(2),2).ne.-5) return
  if(.not.( &
   &     (k(ifermion(3),2).eq.2.and.k(ifermion(4),2).eq.-1)  &
   & .or.(k(ifermion(3),2).eq.4.and.k(ifermion(4),2).eq.-3)  &
   & .or.(k(ifermion(3),2).eq.2.and.k(ifermion(4),2).eq.-3)  &
   & .or.(k(ifermion(3),2).eq.2.and.k(ifermion(4),2).eq.-5)  &
   & .or.(k(ifermion(3),2).eq.4.and.k(ifermion(4),2).eq.-1)  &
   & .or.(k(ifermion(3),2).eq.4.and.k(ifermion(4),2).eq.-5)  &
   & .or.(k(ifermion(3),2).eq.12.and.k(ifermion(4),2).eq.-11)  &
   & .or.(k(ifermion(3),2).eq.14.and.k(ifermion(4),2).eq.-13)  &
   & .or.(k(ifermion(3),2).eq.16.and.k(ifermion(4),2).eq.-15))) return
  if(.not.( &
   &     (k(ifermion(5),2).eq.1.and.k(ifermion(6),2).eq.-2)  &
   & .or.(k(ifermion(5),2).eq.3.and.k(ifermion(6),2).eq.-4)  &
   & .or.(k(ifermion(5),2).eq.3.and.k(ifermion(6),2).eq.-2)  &
   & .or.(k(ifermion(5),2).eq.5.and.k(ifermion(6),2).eq.-2)  &
   & .or.(k(ifermion(5),2).eq.1.and.k(ifermion(6),2).eq.-4)  &
   & .or.(k(ifermion(5),2).eq.5.and.k(ifermion(6),2).eq.-4)  &
   & .or.(k(ifermion(5),2).eq.11.and.k(ifermion(6),2).eq.-12)  &
   & .or.(k(ifermion(5),2).eq.13.and.k(ifermion(6),2).eq.-14)  &
   & .or.(k(ifermion(5),2).eq.15.and.k(ifermion(6),2).eq.-16))) return
 
  p134(1:4)=p(ifermion(1),1:4)+p(ifermion(3),1:4)+p(ifermion(4),1:4)
  p256(1:4)=p(ifermion(2),1:4)+p(ifermion(5),1:4)+p(ifermion(6),1:4)
  p134(5)=max(0.d0,sqrt(p134(4)**2-sum(p134(1:3)**2)))
  p256(5)=max(0.d0,sqrt(p256(4)**2-sum(p256(1:3)**2)))

  x134=p134(5)-pmas(6,1)
  x256=p256(5)-pmas(6,1)

! h134=exp(-x134**2/sigmax/pmas(6,2)**2)
! h256=exp(-x256**2/sigmax/pmas(6,2)**2)
! ptop=0.5d0*(h134+h256)
  ptop=exp(-(x134**2+x256**2)/sigmax/pmas(6,2)**2)

  return
  end subroutine calc_ptop

