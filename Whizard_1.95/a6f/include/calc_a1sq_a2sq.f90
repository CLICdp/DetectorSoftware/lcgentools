subroutine calc_a1sq_a2sq(atotsq,a1sq,a2sq)
  use kinds, only: double 
  use hepevt_include_common
  use pydat1_common
  use pydat2_common
  implicit none
  save
  real(kind=double), intent(out) :: atotsq
  real(kind=double), intent(out) :: a1sq
  real(kind=double), intent(out) :: a2sq
  !
  real(kind=double), dimension(2) :: asq
  real(kind=double), dimension(5,2) :: p12
  real(kind=double)               :: stw
  real(kind=double)               :: ctw
  real(kind=double)               :: gwl
  real(kind=double)               :: gwr
  real(kind=double)               :: gw2
  real(kind=double)               :: xmw
  real(kind=double)               :: gmw
  real(kind=double)               :: xmz
  real(kind=double)               :: gmz
  real(kind=double), dimension(:), pointer :: ggl
  real(kind=double), dimension(:), pointer :: ggr
  real(kind=double), dimension(:), pointer :: gg2
  real(kind=double), dimension(:), pointer :: gzl
  real(kind=double), dimension(:), pointer :: gzr
  real(kind=double), dimension(:), pointer :: gz2
  integer :: i
  integer :: j
  integer :: k
  integer :: kk
  integer :: nfermion
  integer :: ncall=0
  integer :: nqqqq=0
  integer :: pycomp
  integer, dimension(4) :: ifermion
  integer, parameter :: nprint_max=20
  integer, parameter :: nw_comb=12
  integer, parameter :: nz_comb=5
  integer, dimension(:,:), pointer :: iw_comb
  integer, dimension(:,:), pointer :: iz_comb
  integer, dimension(4,2)          :: im
  !
  ncall=ncall+1
  check_ncall: if(ncall.eq.1) then
     allocate (ggl(nz_comb))
     allocate (ggr(nz_comb))
     allocate (gg2(nz_comb))
     allocate (gzl(nz_comb))
     allocate (gzr(nz_comb))
     allocate (gz2(nz_comb))
     allocate (iw_comb(2,nw_comb))
     allocate (iz_comb(2,nz_comb))
     stw=sqrt(paru(102))
     ctw=sqrt(1.d0-paru(102))
     xmw=pmas(pycomp(24),1)
     gmw=pmas(pycomp(24),2)
     xmz=pmas(pycomp(23),1)
     gmz=pmas(pycomp(23),2)
     gwl=1.d0/2.d0/sqrt(2.d0)
     gwr=0.d0
     ggl(1)=(stw/2.d0)*(-1.d0/3.d0)
     ggr(1)=ggl(1)
     ggl(2)=(stw/2.d0)*(2.d0/3.d0)
     ggr(2)=ggl(2)
     ggl(3)=ggl(1)
     ggr(3)=ggr(1)
     ggl(4)=ggl(2)
     ggr(4)=ggr(2)
     ggl(5)=ggl(1)
     ggr(5)=ggr(1)
     gzl(1)=0.5d0/ctw*(-0.5d0-(-1.d0/3.d0)*stw**2)
     gzr(1)=0.5d0/ctw*(-(-1.d0/3.d0)*stw**2)
     gzl(2)=0.5d0/ctw*(0.5d0-(2.d0/3.d0)*stw**2)
     gzr(2)=0.5d0/ctw*(-(2.d0/3.d0)*stw**2)
     gzl(3)=gzl(1)
     gzr(3)=gzr(1)
     gzl(4)=gzl(2)
     gzr(4)=gzr(2)
     gzl(5)=gzl(1)
     gzr(5)=gzr(1)
     gw2=gwl**2+gwr**2
     gg2=ggl**2+ggr**2
     gz2=gzl**2+gzr**2
     print "(' xmw,gmw,xmz,gmz=',4d14.6)", xmw,gmw,xmz,gmz
     print "(' stw**2=',d14.6)", stw**2
     print "(' gwl,gwr=',2d14.6)", gwl,gwr
     print "(' ggl(1),ggr(1)=',2d14.6)", ggl(1),ggr(1)
     print "(' ggl(2),ggr(2)=',2d14.6)", ggl(2),ggr(2)
     print "(' gzl(1),gzr(1)=',2d14.6)", gzl(1),gzr(1)
     print "(' gzl(2),gzr(2)=',2d14.6)", gzl(2),gzr(2)
     im(1,1)=1
     im(2,1)=2
     im(3,1)=3
     im(4,1)=4
     im(1,2)=1
     im(2,2)=4
     im(3,2)=3
     im(4,2)=2 
     iw_comb(1,1)=2
     iw_comb(2,1)=-1
     iw_comb(1,2)=4
     iw_comb(2,2)=-3
     iw_comb(1,3)=1
     iw_comb(2,3)=-2
     iw_comb(1,4)=3
     iw_comb(2,4)=-4
     iw_comb(1,5)=2
     iw_comb(2,5)=-3
     iw_comb(1,6)=2
     iw_comb(2,6)=-5
     iw_comb(1,7)=4
     iw_comb(2,7)=-1
     iw_comb(1,8)=4
     iw_comb(2,8)=-5
     iw_comb(1,9)=3
     iw_comb(2,9)=-2
     iw_comb(1,10)=5
     iw_comb(2,10)=-2
     iw_comb(1,11)=1
     iw_comb(2,11)=-4
     iw_comb(1,12)=5
     iw_comb(2,12)=-4
     iz_comb(1,1)=1
     iz_comb(2,1)=-1
     iz_comb(1,2)=2
     iz_comb(2,2)=-2
     iz_comb(1,3)=3
     iz_comb(2,3)=-3
     iz_comb(1,4)=4
     iz_comb(2,4)=-4
     iz_comb(1,5)=5
     iz_comb(2,5)=-5
  end if check_ncall
  !
  nfermion=0
  loop_i: do i=1,nhep
     check_for_fermion: if((isthep(i).eq.1.and.jmohep(1,i).eq.0).and.(  &
          &     (abs(idhep(i)).ge.1.and.abs(idhep(i)).le.5)  &
          & .or.(abs(idhep(i)).ge.11.and.abs(idhep(i)).le.16))) then
        !
        nfermion=nfermion+1
        check_nfermion: if(nfermion.le.4) then
           ifermion(nfermion)=i
        else check_nfermion
           print "(' from subroutine calc_a1sq_a2sq  pt 1 unexpected nfermion=',i12/' program will now stop')", nfermion
           call pyhepc(2)
           call heplst(1)
           call pylist(2)
           stop
        end if check_nfermion
        !
     end if check_for_fermion
     !
  end do loop_i

  check_nfermion8: if(nfermion.ne.4) then
     print "(' from subroutine calc_a1sq_a2sq  pt 2 unexpected nfermion=',i12/' program will now stop')", nfermion
     call pyhepc(2)
     call heplst(1)
     call pylist(2)
     stop
  end if check_nfermion8

  check_nquark: if(count(abs(idhep(ifermion)).ge.1.and.abs(idhep(ifermion)).le.5).eq.4) then

     nqqqq=nqqqq+1
     if(nqqqq.le.nprint_max) print "(' idhep(ifermion)=',4i6,' count(icolorflowlh(:,ifermion).ne.0)=',i8)", idhep(ifermion),count(icolorflowlh(:,ifermion).ne.0)
     check_whizard_color: if(count(icolorflowlh(:,ifermion).ne.0).eq.4) then
     check_choice: if(icolorflowlh(1,ifermion(1)).eq.icolorflowlh(2,ifermion(2)).and.icolorflowlh(1,ifermion(3)).eq.icolorflowlh(2,ifermion(4))) then
     a1sq=1.d0
     a2sq=0.d0
     elseif(icolorflowlh(1,ifermion(1)).eq.icolorflowlh(2,ifermion(4)).and.icolorflowlh(1,ifermion(3)).eq.icolorflowlh(2,ifermion(2))) then check_choice
     a1sq=0.d0
     a2sq=1.d0
     else check_choice
        print *, " from subroutine calc_a1sq_a2sq  invalid icolorflow "
        print *, "(' ifermion=',4i8)", ifermion
        print *, "(' idhep(ifermion)=',4i8)", idhep(ifermion)
        print *, "(' icolorflowlh(1,ifermion)=',4i8)", icolorflowlh(1,ifermion)
        print *, "(' icolorflowlh(2,ifermion)=',4i8)", icolorflowlh(2,ifermion)
        print *, " program will now stop"
        stop
     end if check_choice
     else check_whizard_color

        loop_i_perm: do i=1,2

           p12(1:4,1)=phep(1:4,ifermion(im(1,i)))+phep(1:4,ifermion(im(2,i)))
           p12(1:4,2)=phep(1:4,ifermion(im(3,i)))+phep(1:4,ifermion(im(4,i)))
           p12(5,:)=max(0.d0,sqrt(p12(4,:)**2-sum(p12(1:3,:)**2,dim=1)))
           if(nqqqq.le.nprint_max) print "(' i=',i6,' p12(5,:)=',2d14.6)", i,p12(5,:)

           loop_k: do k=1,2
              asq(k)=0.d0
              kk=2*k-1

              loop_j_w: do j=1,nw_comb
                 check_match_w: if(idhep(ifermion(im(kk,i))).eq.iw_comb(1,j).and.idhep(ifermion(im(kk+1,i))).eq.iw_comb(2,j)) then
                    asq(k)=gw2/((p12(5,k)**2-xmw**2)**2+gmw**2*xmw**2)
                    if(nqqqq.le.nprint_max) print "(' found w vertex, k,asq(k)=',i6,d14.6)", k,asq(k)
                    cycle loop_k
                 end if check_match_w
              end do loop_j_w

              loop_j_z: do j=1,nz_comb
                 check_match_z: if(idhep(ifermion(im(kk,i))).eq.iz_comb(1,j).and.idhep(ifermion(im(kk+1,i))).eq.iz_comb(2,j)) then
                    asq(k)=gg2(j)/p12(5,k)**4+gz2(j)/((p12(5,k)**2-xmz**2)**2+gmz**2*xmz**2)
                    if(nqqqq.le.nprint_max) print "(' found z vertex, k,asq(k)=',i6,d14.6)", k,asq(k)
                    cycle loop_k
                 end if check_match_z
              end do loop_j_z

           end do loop_k

           check_i_perm: if(i.eq.1) then
              a1sq=asq(1)*asq(2)
              if(nqqqq.le.nprint_max) print "(' i,a1sq,asq(1),asq(2)=',i6,4d14.6)", i,a1sq,asq
           else check_i_perm
              a2sq=asq(1)*asq(2)
              if(nqqqq.le.nprint_max) print "(' i,a2sq,asq(1),asq(2)=',i6,3d14.6)", i,a2sq,asq
           end if check_i_perm

        end do loop_i_perm

     end if check_whizard_color

     atotsq=a1sq+a2sq

     check_atotsq: if(atotsq.le.0.d0) then
        print "( ' from subroutine calc_a1sq_a2sq   atotsq=',d14.6)", atotsq
        print "(' program will now stop')"
        call pyhepc(2)
        call heplst(1)
        call pylist(2)
        stop
     end if check_atotsq

     if(nqqqq.le.nprint_max) print "(' atotsq,a1sq,a2sq=',3d14.6)", atotsq,a1sq,a2sq

  else check_nquark
     atotsq=1.d0
     a1sq=1.d0
     a2sq=0.d0
  end if check_nquark



  return
end subroutine calc_a1sq_a2sq

