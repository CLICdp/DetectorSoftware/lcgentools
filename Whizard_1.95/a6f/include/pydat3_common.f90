module pydat3_common

  use kinds, only: double 

  implicit none

  integer, dimension(500,3) :: mdcy
  integer, dimension(8000,2) :: mdme
  real(kind=double), dimension(8000) :: brat
  integer, dimension(8000,5) :: kfdp
  common/pydat3/mdcy,mdme,brat,kfdp


end module pydat3_common




