module pyint5_common

  use kinds, only: double 

  implicit none

  integer :: ngenpd
  integer, dimension(0:500,3) :: ngen
  real(kind=double), dimension(0:500,3) :: xsec
  common/pyint5/ngenpd,ngen,xsec


end module pyint5_common




