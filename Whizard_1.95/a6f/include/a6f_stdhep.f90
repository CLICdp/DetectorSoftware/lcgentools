module a6f_stdhep

  implicit none
  private
 
  public :: stdhep_write_init, stdhep_write, stdhep_write_end
  integer :: istr_write, lok_write

  public :: stdhep_read_init, stdhep_read, stdhep_read_end
  integer :: ilbl_read, istr_read, lok_read

contains

  subroutine stdhep_write_init (file, title, nevt)
    character(len=*), intent(in) :: file, title
    integer, intent(in) :: nevt
    external stdxwinit, stdxwrt
    call stdxwopen (file, title, nevt, istr_write, lok_write)
    call stdxwrt (100, istr_write, lok_write)
  end subroutine stdhep_write_init

  subroutine stdhep_write(lok)
    integer, intent(out) :: lok
    external stdxwrt
    call stdxwrt (1, istr_write, lok)
  end subroutine stdhep_write

  subroutine stdhep_write_end
    external stdxwrt,stdxend
!   call stdxwrt (200, istr_write, lok_write)
    call stdxend (istr_write)
  end subroutine stdhep_write_end




  subroutine stdhep_read_init (file, nevt)
    character(len=*), intent(in) :: file
    integer, intent(out) :: nevt
    external stdxrinit, stdxrd
    call stdxrinit (file, nevt, istr_read, lok_read)
    call stdxrd (ilbl_read, istr_read, lok_read)
    nevt=nevt-1
  end subroutine stdhep_read_init

  subroutine stdhep_read(lok)
    integer, intent(out) :: lok
    external stdxrd
    call stdxrd (ilbl_read, istr_read, lok)
  end subroutine stdhep_read

  subroutine stdhep_read_end
    external stdxend
    call stdxend (istr_read)
  end subroutine stdhep_read_end

end module a6f_stdhep
