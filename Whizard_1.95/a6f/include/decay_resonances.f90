subroutine decay_resonances(ncount)
  use kinds, only: double, i64
  use hepevt_include_common
  use pypars_common
  use pydat1_common
  use pydat3_common
  use pyjets_common
  use diagnostics, only: msg_level
  implicit none
  integer(i64), intent(in) :: ncount

  integer :: idhep_s
  real(kind=double), dimension(5) :: phep_s
  real(kind=double), dimension(4) :: vhep_s


  integer :: mstp71_s
  integer :: mstj1_s
  integer :: i
  integer :: j
  integer :: jj
  integer :: kk
  integer :: kk0
  integer :: n_s
  integer :: nhep_s
  integer :: naction=0
  integer :: ndelete
  integer :: idc
  integer :: kc
  integer :: nfermion
  integer :: if1
  !  integer, parameter :: ncount0=4360
  integer, parameter :: ncount0=30971
  integer, parameter :: nprint_max=20
  logical f_swap

  if(ncount.eq.ncount0.and.msg_level>2) call heplst(1)


  loop_i: do i=1,nhep
     check_idhep: if(abs(idhep(i)).eq.6.or.(abs(idhep(i)).ge.1000000.and.abs(idhep(i)).lt.3000000).and.isthep(i).eq.1.and.mod(abs(idhep(i)),1000000).ge.1.and.mod(abs(idhep(i)),1000000).le.6) then

        naction=naction+1

        check_naction_1: if(naction.eq.1) then
           kc=6
           loop_idc_t: do idc=mdcy(kc,2),mdcy(kc,2)+mdcy(kc,3)-1
              if(.not.(mdme(idc,1).eq.1.and.kfdp(idc,3).eq.0.and.( &
                   &  (abs(kfdp(idc,1)).eq.24.and.abs(kfdp(idc,2)).eq.5).or.(abs(kfdp(idc,1)).eq.5.and.abs(kfdp(idc,2)).eq.24)))) mdme(idc,1)=0
           end do loop_idc_t
           !             kc=24
           !             loop_idc_w: do idc=mdcy(kc,2),mdcy(kc,2)+mdcy(kc,3)-1
           !                if(.not.(mdme(idc,1).eq.1.and.kfdp(idc,3).eq.0.and.( &
           !                     &       (abs(kfdp(idc,1)).eq.1.and.abs(kfdp(idc,2)).eq.2).or.(abs(kfdp(idc,1)).eq.2.and.abs(kfdp(idc,2)).eq.1)   &
           !                      &  .or.(abs(kfdp(idc,1)).eq.3.and.abs(kfdp(idc,2)).eq.4).or.(abs(kfdp(idc,1)).eq.4.and.abs(kfdp(idc,2)).eq.3)   &
           !                      &  .or.(abs(kfdp(idc,1)).eq.11.and.abs(kfdp(idc,2)).eq.12).or.(abs(kfdp(idc,1)).eq.12.and.abs(kfdp(idc,2)).eq.11)   &
           !                      &  .or.(abs(kfdp(idc,1)).eq.13.and.abs(kfdp(idc,2)).eq.14).or.(abs(kfdp(idc,1)).eq.14.and.abs(kfdp(idc,2)).eq.13)   &
           !                      &  .or.(abs(kfdp(idc,1)).eq.15.and.abs(kfdp(idc,2)).eq.16).or.(abs(kfdp(idc,1)).eq.16.and.abs(kfdp(idc,2)).eq.15)   &
           !                      & ))) mdme(idc,1)=0
           !             end do loop_idc_w
        end if check_naction_1


        mstp71_s=mstp(71)
        mstj1_s=mstj(1)
        mstp(71)=0
        mstj(1)=0

        check_naction: if(naction.le.5) then
           print "(' from decay resonances before action taken about to call heplst(1)   naction=',i12)", naction
           call heplst(1)
        end if check_naction


        !...Call PYHEPC to convert input from HEPEVT to PYJETS common.
        call pyhepc(2)

        !           call pylist(3)

        !           print "(' before loop_j nhep,n=',2i10)", nhep,n


        n_s=n
        loop_j: do j=1,n_s
           check_k2: if(abs(k(j,2)).eq.6.and.k(j,1).ge.1.and.k(j,1).le.3) then
              call pyresd(j)
           elseif(abs(k(j,2)).gt.1000000 .and.abs(k(j,2)).lt.3000000.and.mod(abs(k(j,2)),1000000).ge.1.and.mod(abs(k(j,2)),1000000).le.6 ) then check_k2
              call pydecy(j)
           end if check_k2
        end do loop_j

        !           print "(' after loop_j nhep,n=',2i10)", nhep,n
        !...Call PYHEPC to convert output from PYJETS to HEPEVT common.
        call pyhepc(1)
        if((ncount.le.nprint_max.or.ncount.eq.ncount0).and.msg_level>2) call heplst(1)

        !           print "(' after pyhepc nhep,n=',2i10)", nhep,n

        nhep_s=nhep
        ndelete=0
        !        loop_j_again: do j=1,nhep_s
        !           check_doc: if(isthep(j).ne.1) then
        !              ndelete=ndelete+1
        !              nhep=nhep-1
        !           elseif(ndelete.gt.0) then
        !              ialhep(j-ndelete)=ialhep(j)
        !              phep(:,j-ndelete)=phep(:,j)
        !              vhep(:,j-ndelete)=vhep(:,j)
        !              idhep(j-ndelete)=idhep(j)
        !              isthep(j-ndelete)=isthep(j)
        !              jmohep(:,j-ndelete)=0
        !              jdahep(:,j-ndelete)=0
        !           end if check_doc
        !        end do loop_j_again
        !
        !        !           print "(' after loop_j_again nhep,n=',2i10)", nhep,n
        !
        !
        loop_swap: do
           f_swap=.false.
           nfermion=0
           loop_j_order: do j=1,nhep
              check_abs_fermion: if(isthep(j).eq.1.and.((abs(idhep(j)).ge.1.and.abs(idhep(j)).le.5).or.(abs(idhep(j)).ge.11.and.abs(idhep(j)).le.16))) then
                 nfermion=nfermion+1
                 check_fermion_anti: if((mod(nfermion,2).eq.1.and.idhep(j).lt.0).or.(mod(nfermion,2).eq.0.and.idhep(j).gt.0)) then
                    check_swap: if(f_swap) then                          
                       phep_s=phep(:,if1)
                       vhep_s=vhep(:,if1)
                       idhep_s=idhep(if1)
                       phep(:,if1)=phep(:,j)
                       vhep(:,if1)=vhep(:,j)
                       idhep(if1)=idhep(j)
                       phep(:,j)=phep_s
                       vhep(:,j)=vhep_s
                       idhep(j)=idhep_s
                       cycle loop_swap
                    else check_swap
                       f_swap=.true.
                       if1=j
                    end if check_swap
                 end if check_fermion_anti
              end if check_abs_fermion
           end do loop_j_order
           check_swap_consistent: if(f_swap) then
              print "(' from subroutine decay_resonances inconsistent value for f_swap=T')"
              print "(' program will now stop')"
              stop
           end if check_swap_consistent
           exit loop_swap
        end do loop_swap

        if((ncount.le.nprint_max.or.ncount.eq.ncount0).and.msg_level>2) call heplst(1)
        loop_j_yet_again: do j=2,nhep
           check_antib: if(isthep(j).eq.1.and.idhep(j).eq.-5.and.jmohep(1,j).gt.0.and.jmohep(1,j).le.nhep) then
              check_antitop_mother: if(idhep(jmohep(1,j)).eq.-6) then
                 loop_jj: do jj=1,j-1
                    check_for_b: if(isthep(jj).eq.1.and.idhep(jj).eq.5.and.jmohep(1,jj).gt.0.and.jmohep(1,jj).le.nhep) then
                       check_top_mother: if(idhep(jmohep(1,jj)).eq.6) then
                          kk0=0
                          loop_kk_b: do kk=jj+1,nhep
                             check_istkk: if(isthep(kk).eq.1) then
                                kk0=kk
                                exit loop_kk_b
                             end if check_istkk
                          end do loop_kk_b
                          check_kk0_after: if(kk0.gt.0) then
                             phep_s=phep(:,kk0)
                             vhep_s=vhep(:,kk0)
                             idhep_s=idhep(kk0)
                             phep(:,kk0)=phep(:,j)
                             vhep(:,kk0)=vhep(:,j)
                             idhep(kk0)=idhep(j)
                             phep(:,j)=phep_s
                             vhep(:,j)=vhep_s
                             idhep(j)=idhep_s
                             if((ncount.le.nprint_max.or.ncount.eq.ncount0).and.msg_level>2) call heplst(1)
                             exit loop_j_yet_again
                          end if check_kk0_after
                       end if check_top_mother
                    end if check_for_b
                 end do loop_jj
              end if check_antitop_mother
           end if check_antib
        end do loop_j_yet_again

        if((ncount.le.nprint_max.or.ncount.eq.ncount0).and.msg_level>2) call heplst(1)

        check_naction_again: if(naction.le.5) then
           print "(' from decay resonances after action taken about to call heplst(1)   naction=',i12)", naction
           call heplst(1)
        end if check_naction_again


        mstp(71)=mstp71_s
        mstj(1)=mstj1_s

        exit loop_i


     end if check_idhep
  end do loop_i
  return
end subroutine decay_resonances

