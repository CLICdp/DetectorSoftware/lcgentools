subroutine calc_nfermion(nfermion)
  use kinds, only: double !NODEP!
  use hepevt_include_common
  implicit none
  integer, intent(out) :: nfermion
  integer :: isthep_s, idhep_s, ialhep_s
  integer, dimension(2) :: jmohep_s, jdahep_s
  real(kind=double), dimension(5) :: phep_s
  real(kind=double), dimension(4) :: vhep_s
  integer :: i
  integer, dimension(:), pointer :: ifermion
  integer :: if1
  integer :: if2
  integer :: ifm
  integer :: ifn
  integer :: ifs1
  integer :: ifs2
  logical f_swap

  nfermion=0
  f_swap=.false.
  allocate (ifermion(nhep))
  loop_i: do i=1,nhep
     check_jmo: if(jmohep(1,i).eq.0) then
        check_idhep: if(  &
             &     (abs(idhep(i)).ge.1.and.abs(idhep(i)).le.5)  &
             & .or.(abs(idhep(i)).ge.11.and.abs(idhep(i)).le.16)) then
           check_isthep: if(isthep(i).eq.1.or.isthep(i).eq.2) then
              nfermion=nfermion+1
              ifermion(nfermion)=i
              isthep(i)=1
              jdahep(:,i)=0
           else check_isthep
              isthep(i)=0  !  zero incoming fermion documentation lines 
           end if check_isthep
        else check_idhep
           check_ist_other: if((isthep(i).eq.1.or.isthep(i).eq.2).and.(idhep(i).eq.21.or.idhep(i).eq.22.or.idhep(i).eq.25)) then 
              isthep(i)=1
              jdahep(:,i)=0
           else check_ist_other
              isthep(i)=0
           end if check_ist_other
        end if check_idhep
     else check_jmo
        isthep(i:nhep)=0
        nhep=i-1
        exit loop_i
     end if check_jmo
  end do loop_i

  check_nfermion: if(nfermion.ge.4) then
        ifn=ifermion(nfermion)
        if2=ifermion(2)
     check_idh: if(idhep(if2).gt.0) then
        isthep_s=isthep(ifn)
        ialhep_s=ialhep(ifn)
        idhep_s=idhep(ifn)
        jmohep_s=jmohep(:,ifn)
        jdahep_s=jdahep(:,ifn)
        phep_s=phep(:,ifn)
        vhep_s=vhep(:,ifn)

        loop_i_shift: do i=1,nfermion-2
           ifs1=ifermion(nfermion-i)
           ifs2=ifermion(nfermion-i+1)
           isthep(ifs2)=isthep(ifs1)
           ialhep(ifs2)=ialhep(ifs1)
           idhep(ifs2)=idhep(ifs1)
           jmohep(:,ifs2)=jmohep(:,ifs1)
           jdahep(:,ifs2)=jdahep(:,ifs1)
           phep(:,ifs2)=phep(:,ifs1)
           vhep(:,ifs2)=vhep(:,ifs1)
        end do loop_i_shift

        isthep(if2)=isthep_s
        ialhep(if2)=ialhep_s
        idhep(if2)=idhep_s
        jmohep(:,if2)=jmohep_s
        jdahep(:,if2)=jdahep_s
        phep(:,if2)=phep_s
        vhep(:,if2)=vhep_s

     elseif(idhep(ifn).eq.11) then check_idh

        isthep_s=isthep(ifn)
        ialhep_s=ialhep(ifn)
        idhep_s=idhep(ifn)
        jmohep_s=jmohep(:,ifn)
        jdahep_s=jdahep(:,ifn)
        phep_s=phep(:,ifn)
        vhep_s=vhep(:,ifn)

        ifm=ifermion(nfermion-1)
        isthep(ifn)=isthep(ifm)
        ialhep(ifn)=ialhep(ifm)
        idhep(ifn)=idhep(ifm)
        jmohep(:,ifn)=jmohep(:,ifm)
        jdahep(:,ifn)=jdahep(:,ifm)
        phep(:,ifn)=phep(:,ifm)
        vhep(:,ifn)=vhep(:,ifm)

        isthep(ifm)=isthep_s
        ialhep(ifm)=ialhep_s
        idhep(ifm)=idhep_s
        jmohep(:,ifm)=jmohep_s
        jdahep(:,ifm)=jdahep_s
        phep(:,ifm)=phep_s
        vhep(:,ifm)=vhep_s

     end if check_idh
  end if check_nfermion

  check_nfermion_2: if(nfermion.ge.2) then
        if1=ifermion(1)
        if2=ifermion(2)
     check_sign_2: if(idhep(if1).lt.0) then

        isthep_s=isthep(if2)
        ialhep_s=ialhep(if2)
        idhep_s=idhep(if2)
        jmohep_s=jmohep(:,if2)
        jdahep_s=jdahep(:,if2)
        phep_s=phep(:,if2)
        vhep_s=vhep(:,if2)
        
        isthep(if2)=isthep(if1)
        ialhep(if2)=ialhep(if1)
        idhep(if2)=idhep(if1)
        jmohep(:,if2)=jmohep(:,if1)
        jdahep(:,if2)=jdahep(:,if1)
        phep(:,if2)=phep(:,if1)
        vhep(:,if2)=vhep(:,if1)

        isthep(if1)=isthep_s
        ialhep(if1)=ialhep_s
        idhep(if1)=idhep_s
        jmohep(:,if1)=jmohep_s
        jdahep(:,if1)=jdahep_s
        phep(:,if1)=phep_s
        vhep(:,if1)=vhep_s

     end if check_sign_2
  end if check_nfermion_2

  deallocate(ifermion)
  return
end subroutine calc_nfermion






