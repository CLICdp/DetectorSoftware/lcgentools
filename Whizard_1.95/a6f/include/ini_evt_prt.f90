module ini_evt_prt

  use kinds, only: double, default
  use a6f_states
  use a6f_stdhep !NODEP!
  use hepevt_include_common

  implicit none
  private

  public :: a6f_ini
  public :: a6f_evt
  public :: a6f_prt

  type(a6f_input) :: mci

  real(kind=default) :: mbyte_output
  real(kind=default), parameter :: mbyte_max=2038._default
  logical :: bad_file
  integer :: n_write_total


contains

  subroutine a6f_ini

!
    call cpu_time_init
    call create(mci)
    call read('a6f.in', mci)
    call create(mcs, mci)
    call stdhep_read_init(mcs%stdhep_file,mcs%n_events_std)
    print "(' n_events,n_events_std=',2i12/)", mcs%n_events,mcs%n_events_std
    check_events: if(mcs%n_events_std.le.0) then 
     mcs%n_events_std=1000000000
     bad_file=.true.
    else check_events
     bad_file=.false.
    end if check_events
    call user_ini
    if(mcs%output_events) call stdhep_write_init(mcs%stdhep_output,"Shower and frag",min(mcs%n_events,mcs%n_events_std))
    n_write_total=0
    mbyte_output=0
 
  end subroutine a6f_ini


  subroutine a6f_evt

   integer :: i
   integer :: lok
   
   loop_i: do i=1,min(mcs%n_events,mcs%n_events_std)
   call stdhep_read(lok)
   check_lok_read: if(lok.ne.0) then
   print "(' from subroutine a6f_evt stdhep_read error i,lok=',2i12)", i,lok
   exit loop_i
   end if check_lok_read
   if(bad_file) print "(' from a6f_evt i=',i12)", i
   call user_evt(i)
   check_output: if(mcs%output_events.and.nhep.gt.0) then
   call stdhep_write(lok)
   n_write_total=n_write_total+1
   mbyte_output=mbyte_output+(dble(nhep)*96._default+156._default)/1048576._default
   check_lok_write: if(lok.ne.0) then
   print "(' from subroutine a6f_evt stdhep_write error i,lok=',2i12)", i,lok
   exit loop_i
   else check_lok_write
      check_mbyte: if(mbyte_output.gt.mbyte_max) then
         print "(' from subroutine a6f_evt stdhep_write event loop terminating after ',i10,' events and ',f20.11,' Mbytes output'/)", n_write_total, mbyte_output
         exit loop_i
      end if check_mbyte
   end if check_lok_write
   end if check_output
   end do loop_i
 
  end subroutine a6f_evt


  subroutine a6f_prt

  real(kind=double)                      :: r_time

! call stdhep_read_end
  call user_prt
  if(mcs%output_events) call stdhep_write_end
 
  print "(' n_write_total=',i12)", n_write_total

  call cpu_time_calc(r_time)

  print "(/' Elapsed time:',f10.1,' sec.'/)", r_time
  end subroutine a6f_prt


end module ini_evt_prt
