module a6f_users

  use kinds, only: default, double !NODEP!
  use pydat1_common
  use pypars_common

  implicit none
  private

  public :: create


  type, public :: a6f_user
     integer                         :: i_qed_rad
     logical                         :: epa_pt
     logical, dimension(2)           :: epa_on
     real(kind=double), dimension(2) :: epa_q_max
     integer, dimension(2)           :: epa_prt_in
     real(kind=double)               :: sqrts
  end type a6f_user

  type(a6f_user), public :: mcu

  
  interface create
     module procedure a6f_user_create
  end interface

contains


  subroutine a6f_user_create(mcu,epa_on,epa_q_max,epa_prt_in,sqrts)
    type(a6f_user), intent(out) :: mcu
    logical, dimension(2), intent(in)           :: epa_on
    real(kind=double), dimension(2), intent(in) :: epa_q_max
    integer, dimension(2), intent(in)           :: epa_prt_in
    real(kind=double), intent(in)               :: sqrts
    check_qed: if(mstj(41).ge.2) then
       mcu%i_qed_rad=1
    else check_qed
       mcu%i_qed_rad=0
    end if check_qed
    mcu%sqrts=sqrts
    mcu%epa_on=epa_on
    mcu%epa_q_max=epa_q_max
    mcu%epa_prt_in=epa_prt_in
    mcu%epa_pt=mstp(13).eq.1.and.count(epa_on).gt.0
  end subroutine a6f_user_create



end module a6f_users
