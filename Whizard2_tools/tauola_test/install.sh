#!/bin/bash

# Setup environment parameters

# working directories
if [ ! -f .topdir ] ; then
  echo `pwd` > .topdir
fi

. env.sh 

# working directories
topdir=${TAUOLA_TEST_TOP}
builddir=${topdir}/build
whizard_version=2.3.0_alpha
installdir=${topdir}/${whizard_version}
tmpdir=${topdir}/tmp
patchdir=${topdir}/patches/${whizard_version}
targz=whizard-2.3.0_alpha-20160407.tar.gz
targz=whizard-2.3.0_alpha-20160512.tar.gz
whizardbuild=whizard-2.3.0

##################################
# Start building Whizard2
##################################

# Download if not exist
if [ ! -e ${tmpdir}/${targz} ] ; then 
 ( 
   mkdir -p ${tmpdir}
   cd ${tmpdir} 
   wget http://whizard.hepforge.org/versions/unofficial/${targz}
 )
fi  

mkdir -p ${builddir}
cd ${builddir}

echo "tar zxf ${targz} at ${builddir}"
tar zxf ${tmpdir}/${targz}

cd ${whizardbuild}

# Apply patch
# echo "Applying patches" 
# ( cd src/transforms && patch transforms.nw < ${patchdir}/transforms.nw.patch )
# ( cd src/events && patch events.nw < ${patchdir}/events.nw.patch )
# ( cd src/shower && patch shower.nw <  ${patchdir}/shower.nw.patch ) 
# ( cd pythia6 && patch pythia.f < ${patchdir}/pythia.f.patch )
# ( cd tauola && patch tauola.f < ${patchdir}/tauola.f.patch )
# ( cd tauola && patch tauola_photos_ini.f < ${patchdir}/tauola_photos_ini.f.patch )
# ( cd tauola && mv ilc_tauola_mod2.f90 ilc_tauola_mod2.f90-orig && cp -p ${patchdir}/ilc_tauola_mod2.f90 . )

# Configure whizard2
./configure  --prefix=${installdir}

# Start build library
make -j 12 

# Install
make -j 12 install


