#  A simple sample to read e2e2h stdhep file and plot costh and energy 
#  visible energ yand recoil mass of muon
#
#  Default input file name is mysample.stdhep
#  Output files are
#   myplot.root
#   myplot.png
#
#  Usage:
#   Edit setup.sh for your environment
#   $ . setup.sh 
#   $ python myplot.py
#
#  Author:: Akiya Miyamoto, 25-November-2013
#
# from pyLCIO.io import StdHepReader
# import pyLCIO
from pyLCIO.io import LcioReader, StdHepReader
import ROOT
ROOT.gROOT.SetBatch()
import sys
import os.path as path
import os
import math
import commands

import MyPlots

cs5mrad=math.cos(0.005)
cs5mrad=100.0


# =============================================================================
# AnaData analize pi in tau decay from H
# Plots angular colleration between pi+ and pi-
#
def AnaData_ZH(anatype, filenames, ecm, maxread=10000 ) :
#
# Check file time stamp and return, if root file is newer.
  if os.path.exists(anatype+'.root') : 
    stroot=os.stat(anatype+'.root').st_mtime
    stlcio=os.stat(filenames[0]).st_mtime
    if stroot > stlcio :
      return


  print "Analizing "+str(filenames)
  nfiles=len(filenames)
  reader = StdHepReader.StdHepReader()
  for i in range(0, nfiles) :
    reader.addFile(filenames[i])

  rfile=ROOT.TFile(anatype+".root","recreate")
#   nt = ROOT.TNtuple("nt","NT","angp:angm:acol:phi:spip:spimi:edif")
  nt = ROOT.TNtuple("nt","NT","acol:acolh:phi:edif:angm:angp")

  nout=0
  nfill=0

  for idx, event in enumerate(reader): 
     if idx >= maxread :
       break
     nout = nout + 1
     if nout%1000 == 1 :
       print 'Reading '+str(nout)+'-th event.'

     p_h0=ROOT.TLorentzVector(0.0, 0.0, 0.0, 0.0)
     p_taup=ROOT.TLorentzVector(0.0, 0.0, 0.0, 0.0)
     p_taum=ROOT.TLorentzVector(0.0, 0.0, 0.0, 0.0)
     p_pip=ROOT.TLorentzVector(0.0, 0.0, 0.0, 0.0)
     p_pim=ROOT.TLorentzVector(0.0, 0.0, 0.0, 0.0)
     p_nutaub=ROOT.TLorentzVector(0.0, 0.0, 0.0, 0.0)
     p_nutau=ROOT.TLorentzVector(0.0, 0.0, 0.0, 0.0)

     mcParticles = event.getCollection("MCParticle")
     psum=ROOT.TLorentzVector(0.0, 0.0, 0.0, 0.0)
     taum_pim_nutau_decay = 0
     taup_pip_nutaubar_decay = 0
     spinp=-10.0
     spinm=-10.0
     for p in mcParticles:
       pdg=p.getPDG()
       apdg = abs(pdg)
       v = MyPlots.convertToTLorentzVector(p)
       istat=p.getGeneratorStatus()
       daughters = p.getDaughters()
       ndaughters = len(daughters)


       if ( pdg == -15 and ndaughters == 2 ) :
#           print "pdg="+str(pdg)+" ndau="+str(ndaughters)+" istat="+str(istat)
          if ( daughters[0].getPDG() == -16 and daughters[1].getPDG() == 211 ) :
             taup_pip_nutaubar_decay=1
             p_pip=MyPlots.convertToTLorentzVector(daughters[1])
             p_nutaub=MyPlots.convertToTLorentzVector(daughters[0])
          if ( daughters[0].getPDG() == 211 and daughters[1].getPDG() == -16 ) :
             taup_pip_nutaubar_decay=1
             p_pip=MyPlots.convertToTLorentzVector(daughters[0])
             p_nutaub=MyPlots.convertToTLorentzVector(daughters[1])
          p_taup=p_pip+p_nutaub
          spinv=p.getSpin()
          spinp=spinv[2]

       elif ( pdg == 15 and ndaughters == 2 ) :
          if ( daughters[0].getPDG() == 16 and daughters[1].getPDG() == -211 ) :
             taum_pim_nutau_decay=1
             p_pim=MyPlots.convertToTLorentzVector(daughters[1])
             p_nutau=MyPlots.convertToTLorentzVector(daughters[0])
          if ( daughters[0].getPDG() == -211 and daughters[1].getPDG() == 16 ) :
             taum_pim_nutau_decay=1
             p_pim=MyPlots.convertToTLorentzVector(daughters[0])
             p_nutau=MyPlots.convertToTLorentzVector(daughters[1])
          p_taum=p_pim+p_nutau 
          spinv=p.getSpin()
          spinm=spinv[2]

# ===================== End of loop on particle list

     if nout < 10 :
#        p_h0.Print()
       print "taum_pim_nutau_decay="+str(taum_pim_nutau_decay)+"  taup_pip_nutaubar_decay="+str(taup_pip_nutaubar_decay) 
       print "p_pip="+str(p_pip.Px())+" "+str(p_pip.Py())+" "+str(p_pip.Pz())+" "+str(p_pip.E())
       print "p_pim="+str(p_pim.Px())+" "+str(p_pim.Py())+" "+str(p_pim.Pz())+" "+str(p_pim.E())


     if taum_pim_nutau_decay == 1 and taup_pip_nutaubar_decay == 1 :

       p_h0=p_taup+p_taum 
       boostv=(ROOT.TLorentzVector(-p_h0.Px(), -p_h0.Py(), -p_h0.Pz(), p_h0.E())).BoostVector()
       p_taup.Boost(boostv)
       p_taum.Boost(boostv)

       p_pip.Boost(boostv)
       p_pim.Boost(boostv)

       vtaup0=p_taup.Vect()
       vtaum0=p_taum.Vect()
       vpip0=p_pip.Vect()
       vpim0=p_pim.Vect()
       angp=(vtaup0.Dot(vpip0)/(vtaup0.Mag()*vpip0.Mag()))
       angm=(vtaum0.Dot(vpim0)/(vtaum0.Mag()*vpim0.Mag()))

       edif=p_pip.E()-p_pim.E()
       vpip0=p_pip.Vect()
       vpim0=p_pim.Vect()
       acolh=math.acos(vpip0.Dot(vpim0)/(vpip0.Mag() * vpim0.Mag() ))

       btaup=ROOT.TLorentzVector(-p_taup.Px(), -p_taup.Py(), -p_taup.Pz(), p_taup.E()).BoostVector()
       btaum=ROOT.TLorentzVector(-p_taum.Px(), -p_taum.Py(), -p_taum.Pz(), p_taum.E()).BoostVector()
       p_pip.Boost(btaup)
       p_pim.Boost(btaum)
#      angp=math.acos((p_pip.Vect()).CosTheta())
#      angm=math.acos((p_pim.Vect()).CosTheta())

       vpip=p_pip.Vect()
       vpim=p_pim.Vect()
    
       acol=vpip.Dot(vpim)/(vpip.Mag() * vpim.Mag() )
       

# Calculate phi between pi+ and pi-,
# in a coordinate where tau+ momentum is Z axis
       v_taup=p_taup.Vect()
       vz=v_taup.Unit()
       v_pim=p_pim.Vect()
       vy=(vz.Cross(v_pim)).Unit()
       vx=vz.Cross(vy)

       vxy_m=ROOT.TVector2(v_pim*vx, v_pim*vy)
       v_pip=p_pip.Vect()
       vxy_p=ROOT.TVector2(v_pip*vx, v_pip*vy)
       phi_angle=math.atan2( (vxy_m.X() * vxy_p.Y() - vxy_m.Y() * vxy_p.X() ) , ( vxy_m.X() * vxy_p.X() + vxy_m.Y() * vxy_m.Y() ) )

       if nout < 10 :
         print "tau+ and tau- 4 vector"
         p_taup.Print()
         p_taum.Print()
         print "pi+ and pi- 4 vector"
         p_pip.Print()
         p_pim.Print()
         print "spinp="+str(spinp)+"  spinm="+str(spinm)
#        print ' angp='+str(angp)+'  angm='+str(angm)+'  phi='+str(phi_angle)

       nout=nout+1

#        nt.Fill(angp, angm, acol, phi_angle, spinp, spinm, edif)
       nt.Fill(acol, acolh, phi_angle, edif, angp, angm)
       nfill = nfill + 1 

# === end of event loop

  rfile.Write()

  return nfill

# === end of AnaData


# =============================================================================
# =============================================================================
# AnaData_W analize pi in tau decay from W
# Plots angular colleration between pi+ and pi-
def AnaData_W(anatype, filenames, ecm, maxread=10000 ) :
# =============================================================================
# =============================================================================
# Check file time stamp and return, if root file is newer.
  if os.path.exists(anatype+'.root') : 
    stroot=os.stat(anatype+'.root').st_mtime
    stlcio=os.stat(filenames[0]).st_mtime
    if stroot > stlcio : 
      return

  nfiles=len(filenames)
  reader = StdHepReader.StdHepReader()
  for i in range(0, nfiles) :
    reader.addFile(filenames[i])
  
  rfile=ROOT.TFile(anatype+".root","recreate")
  nt = ROOT.TNtuple("nt","NT","cstp:cspip:cspiptau:ppip:cstm:cspim:cspimtau:ppim:acol")

  nout=0 
  for idx, event in enumerate(reader):
     if idx >= maxread :
       break
     nout += 1
     if nout%1000 == 1 : 
       print 'Reading '+str(nout)+'-th event.'

     pnames=["W-","W+","tau+","tau-","pi+","pi-","nu_tau", "nu_tau_bar", "psum"]
     pv={}
     for i in pnames :
       pv[i]=ROOT.TLorentzVector(0.0, 0.0, 0.0, 0.0)

     nnu=0
     nnubar=0
     npim=0
     npip=0

     taup_pip_nutaubar_decay=0
     taum_pim_nutau_decay=0

     mcParticles = event.getCollection("MCParticle")
     for p in mcParticles:
       pdg=p.getPDG()
       apdg = abs(pdg)
       v = MyPlots.convertToTLorentzVector(p)
       istat=p.getGeneratorStatus()
       daughters=p.getDaughters()

       if ( pdg == -15 ) :
          pv["tau+"]=v
#          print "find tau+:"+str(len(daughters))+" "+str(daughters[1].getPDG())+" "+str(daughters[2].getPDG())          
          if len(daughters) == 2 :
            if ( daughters[0].getPDG() == -16 and daughters[1].getPDG() == 211 ) :
               pv["nu_tau_bar"]=MyPlots.convertToTLorentzVector(daughters[0])
               pv["pi+"]=MyPlots.convertToTLorentzVector(daughters[1])
               taup_pip_nutaubar_decay=1
            if ( daughters[0].getPDG() == 211 and daughters[1].getPDG() == -16 ) :
               pv["nu_tau_bar"]=MyPlots.convertToTLorentzVector(daughters[1])
               pv["pi+"]=MyPlots.convertToTLorentzVector(daughters[0])
               taup_pip_nutaubar_decay=1

       elif ( pdg == 15 ) :
          pv["tau-"]=v 
          if len(daughters) == 2 :
            if ( daughters[0].getPDG() == 16 and daughters[1].getPDG() == -211 ) :
               pv["nu_tau"]=MyPlots.convertToTLorentzVector(daughters[0])
               pv["pi-"]=MyPlots.convertToTLorentzVector(daughters[1])
               taum_pim_nutau_decay=1
            if ( daughters[0].getPDG() == -211 and daughters[1].getPDG() == 16 ) :
               pv["nu_tau"]=MyPlots.convertToTLorentzVector(daughters[1])
               pv["pi-"]=MyPlots.convertToTLorentzVector(daughters[0])
               taum_pim_nutau_decay=1

# ===================== End of loop on particle list

     cs_taup=-10
     cs_pip=-10
     cs_pip_in_taup=-10
     ppip=pv["pi+"].Vect().Mag()
     if pv["tau+"].Mag() > 0.0 and taup_pip_nutaubar_decay==1 :
       p_wp=pv["tau+"]+pv["nu_tau"]
       boost_wp=(ROOT.TLorentzVector(-p_wp.Px(), -p_wp.Py(), -p_wp.Pz(), p_wp.E())).BoostVector()
       pv["tau+"].Boost(boost_wp)
       cs_taup=pv["tau+"].Vect().CosTheta()
       pv["pi+"].Boost(boost_wp)
       cs_pip=pv["pi+"].Vect().CosTheta()
       boost_taup=(ROOT.TLorentzVector(-pv["tau+"].Px(), -pv["tau+"].Py(), -pv["tau+"].Pz(), pv["tau+"].E())).BoostVector()
       pv["pi+"].Boost(boost_taup)
       cs_pip_in_taup=pv["pi+"].Vect().CosTheta()

     cs_taum=-10
     cs_pim=-10
     cs_pim_in_taum=-10
     ppim=pv["pi-"].Vect().Mag() 
     if pv["tau-"].Vect().Mag() > 0.0 and taum_pim_nutau_decay == 1 :
       p_wm=pv["tau-"]+pv["nu_tau_bar"]
       boost_wm=(ROOT.TLorentzVector(-p_wm.Px(), -p_wm.Py(), -p_wm.Pz(), p_wm.E())).BoostVector()
       pv["tau-"].Boost(boost_wm)
       cs_taum=pv["tau-"].Vect().CosTheta()
       pv["pi-"].Boost(boost_wm)
       cs_pim=pv["pi-"].Vect().CosTheta()
       boost_taum=(ROOT.TLorentzVector(-pv["tau-"].Px(), -pv["tau-"].Py(), -pv["tau-"].Pz(), pv["tau-"].E())).BoostVector()
       pv["pi-"].Boost(boost_taum)
       cs_pim_in_taum=pv["pi-"].Vect().CosTheta()


     acol=-10
     if ppip > 10 and ppim > 10 and ( taup_pip_nutaubar_decay == 1 and taum_pim_nutau_decay == 1 ) :
       vpip=pv["pi+"].Vect()
       vpim=pv["pi-"].Vect()
       acol=vpip.Dot(vpim)/(vpip.Mag() * vpim.Mag() )

#    nt = ROOT.TNtuple("nt","NT","cstp:cspip:cspiptau:ppip:cstm:cspim:cspimtau:ppim:acol")
     if taup_pip_nutaubar_decay == 1 or taum_pim_nutau_decay == 1 :
       nt.Fill(cs_taup, cs_pip, cs_pip_in_taup, ppip, cs_taum, cs_pim, cs_pim_in_taum, ppim, acol)

# === end of event loop

  rfile.Write()

# === end of AnaData_W


# ====================================================================================
def Do_CompPlot_ZH(ecm, msg1, figdir, maxread=100000 ): 

#  notes=["Black: #nu#nu Z","Red : #nu#nu H","Green: #nu#nuZ(DBD)","Blue: #nu#nuH(single decay mode0)",
#         "Cyan: #nu#nuH(single decay mode2)"," ??? : Kawada sample" ]
#  notes=["1(black): #nu#nu Z","2(red): #nu#nu H","3(green): #nu#nuZ(DBD)","4(blue): Kawada Sample",
#         "5(cyan): #nu#nuH(single decay mode2)" ]
  notes=["1(black): #nu#nu Z","2(red): #nu#nu H","3(green): #nu#nuZ(DBD)",
         "4(blue): #nu#nuH(single decay mode0 )", 
         "5(cyan): #nu#nuH(single decay mode2)"]

  outdir=figdir
  mp = MyPlots.MyPlots()
  mp.SetNotes(notes)
  mp.text_size=0.025
  mp.text_dy=0.03

  title=" #tau^{#pm}#rightarrow #pi^{#pm}#nu @ "+ecm+" GeV"
  mp.AddFile(outdir+"n2n2e3e3-"+ecm+".root",1,"same", maxread=maxread)
  mp.AddFile(outdir+"n2n2h_e3e3-"+ecm+".root",2,"same", maxread=maxread)
#  mp.AddFile(outdir+"dbd-n2n2e3e3-"+ecm+".root",3,"same", maxread=20*int(maxread))
#  mp.AddFile(outdir+"n2n2h_e3e3_flag0-"+ecm+".root",4,"same", maxread=maxread)
#   mp.AddFile(outdir+"skawada.root",4,"same",maxread=20*int(maxread))
#  mp.AddFile(outdir+"n2n2h_e3e3_flag2-"+ecm+".root",7,"same", maxread=maxread)

  nbx=50
  ymax=20000/nbx
  nch_opts={"title":"Acoliniarity of #pi^{+}#pi^{-} in Higgs rest frame :"+title, \
	  "nbx":nbx,"xmin":0.0,"xmax":3.20,"nby":50,"ymin":0,"ymax":ymax,"xlabel":"cos#theta_{#pi^{+}#pi^{-}}"}
  mp.Plot("acolh", nch_opts, outdir+ecm+"-acolh" )

  nch_opts={"title":"Acoliniarity of #pi^{+}#pi^{-} in Higgs rest frame :"+title, \
	  "nbx":nbx,"xmin":0.0,"xmax":3.20,"nby":50,"ymin":1,"ymax":ymax,"xlabel":"#theta_{#pi^{+}#pi^{-}}"}
  mp.Plot("acolh", nch_opts, outdir+ecm+"-logacolh", logscale=1 )

  nch_opts={"title":"cos#theta_{#pi^{+}#pi^{-}} :"+title, \
	  "nbx":nbx,"xmin":-1.0,"xmax":1.0,"nby":50,"ymin":0,"ymax":ymax,"xlabel":"#theta_{#pi^{+}#pi^{-}}"}
  mp.Plot("acol", nch_opts, outdir+ecm+"-acol" )

  nch_opts={"title":"E(#pi^{+})-E(#pi^{-}) :"+title, \
	  "nbx":nbx,"xmin":-100.0,"xmax":100.0,"nby":50,"ymin":0,"ymax":ymax,"xlabel":"E(#pi^{+})-E(#pi^{-}) (GeV)"}
  mp.Plot("edif", nch_opts, outdir+ecm+"-edif" )

  ymax=25000/nbx
  nch_opts={"title":"#phi_{#pi^{+}#pi^{-}} :"+title, \
	  "nbx":nbx,"xmin":-3.2,"xmax":3.2,"nby":50,"ymin":0,"ymax":ymax,"xlabel":"#phi_{#pi^{+}#pi^{-}}"}
  mp.text_xpos_left=0.45
  mp.Plot("phi", nch_opts, outdir+ecm+"-phi")

  ans=commands.getoutput("cd "+outdir+" && pdftk "+ecm+"-*.pdf cat output "+ecm+"all.pdf")
  print ans

# ====================================================================================
def Do_CompPlot_W(ecm, msg1, figdir, maxread=10000): 

  outdir=figdir
  mp = MyPlots.MyPlots()
  mp.AddFile(outdir+"munutaunu-notauola-"+ecm+".root",1,"same",maxread=maxread)
  mp.AddFile(outdir+"munutaunu-"+ecm+".root",2,"same",maxread=maxread)
  mp.AddFile(outdir+"4f_ww_l-"+ecm+".root",3,"same",maxread=50*int(maxread))
  notes=["Black: w/o tauola","Red: w/  tauola","Green: DBD "]
  mp.SetNotes(notes)

  title="  4f_#mu#nu#tau#nu @@ "+ecm+" GeV" 
  nch_opts={"title":"cos#theta(#pi^{+}) :"+title, \
	  "nbx":50,"xmin":-1.1,"xmax":1.1,"nby":50,"ymin":0,"ymax":350.0,"xlabel":"cos#theta(#pi^{+})"}
  mp.Plot("cspiptau", nch_opts, outdir+ecm+"-cspiptau" )

  nch_opts={"title":"cos#theta(#pi^{-}) :"+title, \
	  "nbx":50,"xmin":-1.1,"xmax":1.1,"nby":50,"ymin":0,"ymax":350.0,"xlabel":"cos#theta(#pi^{-})"}
  mp.Plot("cspimtau", nch_opts, outdir+ecm+"-cspimtau" )

  ans=commands.getoutput("cd "+outdir+" && pdftk "+ecm+"-*.pdf cat output "+ecm+"all.pdf")
  print ans

# ====================================================================================
def Do_CompPlot_W2(ecm):

  files={"1":{"file":"2fww/2f_ww_l-"+ecm+".root","color":1,"drawopt":"same",  "xsect":129148.58}, \
         "2":{"file":"2fww/munutaunu-"+ecm+".root","color":2,"drawopt":"same","xsect":130008.17} }

  nch_opts={"title":"cos#theta(#pi^{+}) : 2f_ww_l @@ "+ecm+" GeV", \
          "nbx":50,"xmin":-1.1,"xmax":1.1,"nby":50,"ymin":0,"ymax":250.0,"xlabel":"cos#theta(#pi^{+})", \
          "tx1":0.2,"ty1":50.0,"tx2":0.2,"ty2":20.0, "tl1":"Black: w/o tauola","tl2":"Red : w/ tauola"}
  Comp_Plot(files, "cspiptau", nch_opts, "wfig/"+ecm+"-cspiptau" )

  nch_opts={"title":"cos#theta(#pi^{-}) : 4f_#mu#nu#tau#nu @@ "+ecm+" GeV", \
          "nbx":50,"xmin":-1.1,"xmax":1.1,"nby":50,"ymin":0,"ymax":250.0,"xlabel":"cos#theta(#pi^{-})", \
          "tx1":0.2,"ty1":50.0,"tx2":0.2,"ty2":20.0, "tl1":"Black: w/o tauola","tl2":"Red : w/ tauola"}
  Comp_Plot(files, "cspimtau", nch_opts, "wfig/"+ecm+"-cspimtau" )

  ans=commands.getoutput("cd wfig && /home/ilc/miyamoto/bin/pdftk "+ecm+"-*.pdf cat output "+ecm+"all.pdf")
  print ans



# =====================================
#  Main program
# =====================================

target="all"
target="zhtau"
# target="ww"

datadir="../examples/jobs/"

if target == "all" or target == "zhtau" :
  figdir="zhtau/"
  doanal="yes"
  if doanal == "yes" :
#     ans=commands.getoutput("rm -rf "+figdir+" && mkdir "+figdir)
#     print ans
    br_taupinu=0.1119875
    nread=1000000 
    maxread=int(float(nread)*br_taupinu*br_taupinu)
    print ' nread='+str(nread)+' maxread='+str(maxread)

    n_zh=AnaData_ZH (figdir+"n2n2h_e3e3-250", [datadir+"E250.Pn2n2h_h2tautau_flag1/E250.Pn2n2h_h2tautau.Gwhizard2-beam.eL.pR.ev4.hep"], 250, maxread=maxread)

    n_zh_flag0=AnaData_ZH (figdir+"n2n2h_e3e3_flag0-250", [datadir+"E250.Pn2n2h_h2tautau_flag0/E250.Pn2n2h_h2tautau.Gwhizard2-beam.eL.pR.ev4.hep"], 250, maxread=maxread)
    n_zh_flag2=AnaData_ZH (figdir+"n2n2h_e3e3_flag2-250", [datadir+"E250.Pn2n2h_h2tautau_flag2/E250.Pn2n2h_h2tautau.Gwhizard2-beam.eL.pR.ev4.hep"], 250, maxread=maxread)
    n_nntt=AnaData_ZH (figdir+"n2n2e3e3-250", [datadir+"E250.P4f_n2n2e3e3/E250.P4f_n2n2e3e3.Gwhizard2-beam.eL.pR.ev4.hep"], 250, maxread=maxread)

    filepref="E250-TDR_ws.P4f_zz_nunutautau.Gwhizard-1_95.eL.pR"
    dir="/home/ilc/miyamoto/work/prod-whizard/4f/prod/jobs/250-TDR_ws/4f/E250-TDR_ws.P4f_zz_nunutautau.Gwhizard-1_95.eL.pR/"
    dbdfiles=[]
    for i in range(1,11) :
      dbdfiles.append(dir+filepref+".00"+str(i)+".stdhep")
#    ndbd=AnaData_ZH(figdir+"dbd-n2n2e3e3-250",dbdfiles,250,maxread=nread)

    kawadafiles=[
                 "/home/ilc/skawada/work/CPVHgen/out/250_kawada/eR.pL/u/u.stdhep",
                 "/home/ilc/skawada/work/CPVHgen/out/250_kawada/eR.pL/d/d.stdhep",
                 "/home/ilc/skawada/work/CPVHgen/out/250_kawada/eR.pL/s/s.stdhep",
                 "/home/ilc/skawada/work/CPVHgen/out/250_kawada/eL.pR/u/u.stdhep",
                 "/home/ilc/skawada/work/CPVHgen/out/250_kawada/eL.pR/d/d.stdhep",
                 "/home/ilc/skawada/work/CPVHgen/out/250_kawada/eL.pR/s/s.stdhep",
                 "/home/ilc/skawada/work/CPVHgen/out/250_kawada/eL.pR/n2/n2.stdhep",
                 "/home/ilc/skawada/work/CPVHgen/out/250_kawada/eL.pR/n1/n1.stdhep",
                 "/home/ilc/skawada/work/CPVHgen/out/250_kawada/eL.pR/n3/n3.stdhep",
                 "/home/ilc/skawada/work/CPVHgen/out/250_kawada/eL.pR/e1/e1.stdhep",
                 "/home/ilc/skawada/work/CPVHgen/out/250_kawada/eL.pR/e2/e2.stdhep",
                 "/home/ilc/skawada/work/CPVHgen/out/250_kawada/eR.pL/e1/e1.stdhep",
                 "/home/ilc/skawada/work/CPVHgen/out/250_kawada/eR.pL/e2/e2.stdhep",
                 "/home/ilc/skawada/work/CPVHgen/out/250_kawada/eR.pL/n2/n2.stdhep",
                 "/home/ilc/skawada/work/CPVHgen/out/250_kawada/eR.pL/n1/n1.stdhep",
                 "/home/ilc/skawada/work/CPVHgen/out/250_kawada/eR.pL/n3/n3.stdhep"
                 ]
#    nkawada=AnaData_ZH(figdir+"skawada",kawadafiles,250,100000000)
#    print "nkawada="+str(nkawada)

  Do_CompPlot_ZH("250","",figdir)

# ======================================================================
if target == "all" or target == "ww" :
  maxread=8000
  figdir="wwfig/"
  br_taupinu=0.1119875
  nread=int(float(maxread)/br_taupinu)*10
#  ans=commands.getoutput("rm -rf "+figdir+" && mkdir "+figdir)
#  print ans
  AnaData_W (figdir+"munutaunu-250", [datadir+"E250.P4f_munutaunu/E250.P4f_munutaunu.Gwhizard2-beam.eL.pR.ev4.hep"], 250, maxread=maxread)
  AnaData_W (figdir+"munutaunu-notauola-250", [datadir+"E250.P4f_munutaunu_notauola/E250.P4f_munutaunu.Gwhizard2-beam.eL.pR.notauola.ev4.hep"], 250, maxread=maxread)
  filepref="E250-TDR_ws.P4f_ww_l.Gwhizard-1_95.eL.pR.I106581"
  dir="/hsm/ilc/grid/storm/prod/ilc/mc-dbd/generated/250-TDR_ws/4f/"
  dbdfiles=[]
  for i in range(1,10) :
    dbdfiles.append(dir+filepref+".00"+str(i)+".stdhep")
  AnaData_W (figdir+"4f_ww_l-250", dbdfiles , 250, maxread=nread )

  Do_CompPlot_W("250", "", figdir, maxread=1000000)

