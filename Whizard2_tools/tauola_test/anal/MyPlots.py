#  Utilities for analysis and plots
#
#  30-March-2016  Akiya Miyamoto
#
import ROOT
# ROOT.gROOT.SetBatch()
import sys
import os.path as path
import math
import commands

# =====================================================
def convertToTLorentzVector(particle):
    p = particle.getMomentum()
    E = particle.getEnergy()
    return ROOT.TLorentzVector(p[0], p[1], p[2], E)

# =====================================================
class MyPlots:
  '''Draw Histogram from ROOT NTuple '''
    
 
  def __init__(self):
    self.text_xpos_left=0.15
    self.text_xpos_right=0.55
    self.text_size=0.035
    self.text_ypos=0.87
    self.text_dy=0.05
    self.files=[]
    self.notes=[]
    self.printformat=["png","C","pdf"]

# =============================================================================
  def AddFile(self, rootfile, color, drawopt, maxread=100000000) :
    afile={"file":rootfile, "color":color, "drawopt":drawopt, "maxread":maxread}
    self.files.append(afile)
    print rootfile+" will be ploted with color="+str(color)+" draw option="+drawopt

# ====================================================================
  def SetNotes(self, notes) :
    self.notes=notes

# ====================================================================
  def Plot(self, var, opts, figname, ntname="nt", cut="", logscale=0, autoscale="on") :
    c1 = ROOT.TCanvas("c1"+var)

    hf = ROOT.TH2F("hf"+var,opts["title"], opts["nbx"], opts["xmin"], opts["xmax"], \
                    opts["nby"], opts["ymin"], opts["ymax"]) 
    hf.SetMaximum(opts["ymax"])
    hf.SetStats(0)
    if logscale == 1 :
      c1.SetLogy(1)
    c1.SetGridx(1)
    c1.SetGridy(1)

    hf.GetXaxis().SetTitle(opts["xlabel"])
    hf.Draw()

#
# Find maximum entries and bin number by drawing ntuples
#
    hfmax=opts["ymax"]
    maxent=0
    maxbin=0
    nentmin=100000000    
    nentmin_max=10000
    for i in range(0, len(self.files)):
      finfo=self.files[i]
      finfo["rf"]=ROOT.TFile(finfo["file"])
      finfo["nt"]=finfo["rf"].Get(ntname)
      finfo["nt"].SetLineColor(finfo["color"])
      finfo["nt"].SetMarkerColor(finfo["color"])
      finfo["nt"].Draw(var, cut, finfo["drawopt"],finfo["maxread"])
      finfo["hist"]=finfo["nt"].GetHistogram()
      finfo["maxent"]=finfo["hist"].GetBinContent(finfo["hist"].GetMaximumBin())
      nent=finfo["hist"].GetEntries()
      print "Number of entries is "+str(nent)
      if nent < nentmin : 
        nentmin=nent

    ymax=0
    for i in range(0, len(self.files)) :
      finfo=self.files[i]
      nplot=finfo["hist"].GetEntries()
      thisent=int(float(finfo["maxent"])*float(nentmin)/float(nplot)) 
#      thisent=finfo["maxent"]
      if ( thisent >  ymax ) : 
         ymax = thisent
         maxbin=finfo["hist"].GetMaximumBin()

    print "ymax="+str(ymax)+" maxbin="+str(maxbin)

    txpos=self.text_xpos_left
    if autoscale == "on" : 
      hfmax=(int(1.1*float(ymax)/10.0)+1)*10
      print "hfmax is "+str(hfmax)
      if maxbin < int(0.5*float(opts["nbx"])) :
        txpos=self.text_xpos_right

#
# Rescale vertical maximum and plot histogram
#
    hf2 = ROOT.TH2F("hf2"+var,opts["title"], opts["nbx"], opts["xmin"], opts["xmax"], \
                    opts["nby"], opts["ymin"], hfmax)
    c1.Clear()
    hf2.SetStats(0)
    hf2.GetXaxis().SetTitle(opts["xlabel"])
    hf2.Draw()
    for i in range(0, len(self.files) ) :
      finfo=self.files[i]
      finfo["nt"].Draw(var, cut, finfo["drawopt"], int(nentmin))

#      finfo["nt"].Draw(var, cut, finfo["drawopt"], finfo["maxread"])

     
# 
# Draw notes on the plot
#
    notes=self.notes
    textsize=self.text_size
    lnotes=len(notes)
    ypos=self.text_ypos
    dy=self.text_dy
    txs={}
    for l in range(0, lnotes) :
      ypos=ypos-dy
      if l == len(self.files) :
        ypos=ypos-dy
      txs[str(l)]=ROOT.TLatex(txpos, ypos, notes[l])
      tl1=txs[str(l)]
      tl1.SetNDC(True)
      if l < len(self.files) :
        finfo=self.files[l]
        tl1.SetTextColor(finfo["color"])
      tl1.SetTextSize(textsize)
      tl1.Draw("same")

    for i in range(0, len(self.printformat)) :
      c1.Print(figname+"."+self.printformat[i])


# =============== End of Plot =========================================


