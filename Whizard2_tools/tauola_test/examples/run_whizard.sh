#!/bin/bash
# A script run Whizard jobs
# 

newpath=`echo $PATH | sed -e "s|/usr/local/texlive/p2011|/group/ilc/soft/utils64/texlive/p2011|"`
export PATH=${newpath}


if [ "x${TAUOLA_TEST_TOP}" == "x" ] ; then 
  echo "Run this script after surce env.sh "
  exit -1
fi

sindir=${TAUOLA_TEST_TOP}/examples/sindarin
aliases="${sindir}/aliases.sin"
pythia6_parameters="${sindir}/pythia6-parameters.sin"
beam_events_file="/group/ilc/soft/gcc481/GP/beamdata/lumiee250/lumiee250-run1.dat"
beam_events_file="/group/ilc/soft/gcc481/GP/beamdata/lumiee250-hs/lumiee250-run1to20.dat"
n_events=100000
n_events=10000

# tauola/photos parameters
jak1=3            # JAK1 (0) decay mode of tau+
jak2=3            # %%JAK2%% ! JAK2 (0) decay mode of tau-
itdkrc=1          # %%ITDKRC%%  ! ITDKRC(1)  switch of radiative corrections in decay
ifphot=1          # %%IFPHOT%%  ! IFPHOT(1) photos switch
spincorrelation=0 # ! Correlate(1)/not correlate(0)/only long. pol(2) transverse spin of tau+ tau- from Higgs


# ####################################
# Whizard run of one process
# ####################################
run_whizard() 
{
  proc=$1
  jobdir=jobs/${proc}
  if [ "x$2" != "x" ] ; then 
    spincorrelation=${2}
    echo "Spin correlation flag of this job is ${spincorrelation}"
    jobdir=${jobdir}_flag${spincorrelation}
  fi

  echo "Whizard directory is ${jobdir}"
  rm -rf ${jobdir}
  mkdir -p ${jobdir}
  ( 
    ecm=`echo ${proc} | cut -d"." -f1 | sed -e "s/E//" `
    beam_file=`echo ${beam_events_file} | sed -e "s|250|${ecm}|g"`

    cd ${jobdir}
    sed -e "s|%%ALIASES%%|${aliases}|g" \
	-e "s|%%PYTHIA6_PARAMETERS%%|${pythia6_parameters}|g" \
        -e "s|%%BEAM_EVENTS_FILE%%|${beam_file}|g" \
        -e "s|%%N_EVENTS%%|${n_events}|g" \
        ${sindir}/${proc}.sin > ${proc}.sin
    sed -e "s|%%JAK1%%|${jak1}|g" \
        -e "s|%%JAK2%%|${jak2}|g" \
        -e "s|%%ITDKRC%%|${itdkrc}|g" \
        -e "s|%%IFPHOT%%|${ifphot}|g" \
        -e "s|%%SPINCORRELATION%%|${spincorrelation}|g" \
        ${sindir}/tauola.input.temp > tauola.input

      /usr/bin/time whizard -r ${proc}.sin > run_whizard.log 2>&1  
#       /usr/bin/time whizard -r ${proc}.sin 2>&1 | tee run_whizard.log   
#       /usr/bin/time whizard ${proc}.sin > run_whizard.log 2>&1  
  )
}

# #######################################
# Loop over many processes and Run many runs 
# #######################################
#
# run_whizard E250.P2f_tautau

while read f ; do 
  if [ ${f:0:1} != "#" ] ; then 
    echo "Running whizard for $f "
    run_whizard $f 
  fi
done <<EOF
E250.Pn2n2h_h2tautau 1
# E250.Pn2n2h_h2tautau 0
# E250.Pn2n2h_h2tautau 2 
# E250.P4f_n2n2e3e3
# E250.P4f_munutaunu
# E250.P4f_munutaunu_notauola

## E500.P2f_tautau.eR.pL
## E500.P2f_tautau
##E250.Pn2n2h_h2tautau  
#E250.P2f_tautau
#E500.P2f_tautau
#E500.P2f_tautau.eR.pL
#E250.Pe2e2h_h2ww2taunu
#E250.Pe2e2h_h2zz2tautau
EOF

