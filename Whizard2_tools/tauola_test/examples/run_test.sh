#!/bin/bash
# A script run Whizard jobs
# 

newpath=`echo $PATH | sed -e "s|/usr/local/texlive/p2011|/group/ilc/soft/utils64/texlive/p2011|"`
export PATH=${newpath}


if [ "x${TAUOLA_TEST_TOP}" == "x" ] ; then 
  echo "Run this script after surce env.sh "
  exit -1
fi

sindir=${TAUOLA_TEST_TOP}/examples/sindarin
aliases="${sindir}/aliases.sin"
pythia6_parameters="${sindir}/pythia6-parameters.sin"
beam_events_file="/group/ilc/soft/gcc481/GP/beamdata/lumiee250/lumiee250-run1.dat"
beam_events_file="/group/ilc/soft/gcc481/GP/beamdata/lumiee500-hs/lumiee500-run1to20.dat"
beam_events_file="/group/ilc/soft/gcc481/GP/beamdata/lumiee250-hs/lumiee250-run1to20.dat"
n_events=10


# ####################################
# Whizard run of one process
# ####################################
run_whizard() 
{
  proc=$1
  jobdir=jobs-test/${proc}
  rm -rf ${jobdir}
  mkdir -p ${jobdir}
  ( 
    cd ${jobdir}
    sed -e "s|%%ALIASES%%|${aliases}|g" \
	-e "s|%%PYTHIA6_PARAMETERS%%|${pythia6_parameters}|g" \
        -e "s|%%BEAM_EVENTS_FILE%%|${beam_events_file}|g" \
        -e "s|%%N_EVENTS%%|${n_events}|g" \
        -e "s|sample_format = stdhep_ev4|sample_format = stdhep_ev4, long, short|" \
        ${sindir}/${proc}.sin > ${proc}.sin
#     if [ "x$2" == "xhiggs" ] ; then 
#       cp ${sindir}/tauola.input.higgs.only-pinu  tauola.input
#     else 
#       cp ${sindir}/tauola.input.only-pinu tauola.input
#     fi
#          /usr/bin/time whizard -r --debug2 tauola ${proc}.sin 
           /usr/bin/time whizard -r ${proc}.sin 
#           /usr/bin/time whizard -r --debug2 shower ${proc}.sin 
#        /usr/bin/time whizard -r --debug2 shower ${proc}.sin 
  )
}

# #######################################
# Loop over many processes and Run many runs 
# #######################################
#
# run_whizard E250.P2f_tautau

while read f ; do 
  if [ ${f:0:1} != "#" ] ; then
    echo "Running whizard for $f "
    run_whizard $f
  fi
done <<EOF
# E500.P2f_tautau
E250.Pn2n2h_h2tautau
# E250.Pn2n2h_h2tautau higgs
# E250.P4f_munutaunu
# E250.Pe2e2h_h2ww2taunu
EOF

# E250.Pe2e2h_h2zz2tautau
# E250.P4f_n2n2e3e3
# E250.Pe2e2h_h2zz2tautau

# E250.Pn2n2h_h2tautau
# E250.Pn2n2h_h2tautau-nofsr
# E250.Pnnh_h2tautau
# E250.P2f_tautau-nopol
# E250.P2f_tautau
# E250.P4f_munutaunu
# E250.Pe2e2h_h2tautau
# E250.Pnnh_h2tautau-nopol
# EOF

