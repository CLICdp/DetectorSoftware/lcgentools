#!/bin/bash

#
export PATH=/group/ilc/soft/gcc481/Python/2.7.10/bin:$PATH
export LD_LIBRARY_PATH=/group/ilc/soft/gcc481/Python/2.7.10/lib:$LD_LIBRARY_PATH
# Setup gcc481/gfortran environment
export PATH=/group/ilc/soft/gcc481/gcc/bin:$PATH
export LD_LIBRARY_PATH=/group/ilc/soft/gcc481/gcc/lib64:/group/ilc/soft/gcc481/gcc/lib:${LD_LIBRARY_PATH}

#

export TAUOLA_TEST_TOP=`pwd`
whizard2_version=2.3.0_alpha
whizard2_dir=${TAUOLA_TEST_TOP}

# Setting external libraries
automake_path=/group/ilc/soft/gcc481/automake/1.15
autoconf_path=/group/ilc/soft/gcc481/autoconf/2.69
libtool_path=/group/ilc/soft/gcc481/libtool/2.4.6
# noweb_path=/group/ilc/soft/gcc481/noweb/2.11b
export PATH=${automake_path}/bin:${autoconf_path}/bin:${libtool_path}/bin:${noweb_path}/bin:${PATH}
export LD_LIBRARY_PATH=${libtool_path}/lib:${LD_LIBRARY_PATH}
# 
export FASTJET_DIR=/group/ilc/soft/gcc481/FastJet/3.1.2
export OCAML_DIR=/group/ilc/soft/gcc481/ocaml/3.12.1
export HEPMC_DIR=/group/ilc/soft/gcc481/HepMC/2.06.09
export LCIO_DIR=/group/ilc/soft/ilcsoft/x86_64_gcc481/v01-17-08/lcio/v02-06
export PATH=${whizard2_dir}/${whizard2_version}/bin:${OCAML_DIR}/bin:${PATH}
export LD_LIBRARY_PATH=${whizard2_dir}/${whizard2_version}/lib:${LCIO_DIR}/lib:${OCAML_DIR}/lib:${LD_LIBRARY_PATH}

