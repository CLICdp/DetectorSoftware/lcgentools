!
!  ILC Tauola interface
!  Revised for Whizard2-Pythia6 based on the ilc_tauola_mod.f90 
!  for Whizard1 developped by Timothy Barklow (SLAC)
!
!    Akiya Miyamoto, 21 January 2016
!

module ilc_tauola_mod2

  use hepevt_include_common
  use hepeup_include_common

  implicit none

  !!! THIS COMMON BLOCK IS USED FOR COMMUNICATION WITH TAUOLA
  common/taupos/np1,np2
  integer                      ::  np1
  integer                      ::  np2

!  use momdec_include_common
  !!! THIS COMMON BLOCK IS USED FOR COMMUNICATION WITH TAUOLA
  COMMON / MOMDEC / Q1,Q2,P1,P2,P3,P4
  double precision Q1(4),Q2(4),P1(4),P2(4),P3(4),P4(4)

  interface
     function pyr(i_dum)
       implicit none
       double precision :: pyr
       integer, intent(in) :: i_dum
     end function pyr
  end interface

  integer :: n
  integer :: npad
  integer, dimension(4000,5) :: k
  double precision, dimension(4000,5) :: p
  double precision, dimension(4000,5) :: v
  common/pyjets/n,npad,k,p,v

  private

  double precision, external :: wthiggs


  public :: ilc_tauola_pytaud
  public :: tauspin_pyjets
  public :: ilc_tauola_print_helicity_info_mod
  public :: ilc_tauola_get_helicity_mod

  integer, save, public :: photo_switch_ifphot
  integer, save, public :: gCorrelateTransverseSpin   ! 1(0) to correlate (not to correlate ) transvese spin of tau+ and tau- in Higgs decay
                                                      ! 2 for internal debugging

  integer :: n_ini_call=0
! integer, parameter :: n_ini_call0=4360
  integer, parameter :: n_ini_call0=30971

  integer  :: jtau, jtau2
  integer  :: jorig
  integer  :: jforig
  integer  :: nproducts

  integer, parameter                         :: n_pyjets_max=4000
  integer                                    :: id_dexay
  integer, parameter                         :: nprint_max=20

  integer  :: max_dump=0
  integer  :: nsub_call=0
  save nsub_call
  save max_dump

  double precision, dimension(5)            :: p_dexay
  double precision                          :: spin_dexay
  double precision, dimension(n_pyjets_max) :: tauspin_pyjets

  double precision, dimension(4) :: pol
  logical :: gIsHiggsDecay

  double precision, parameter            :: a_tau=0.15
  double precision, parameter            :: prob_tau_left_z=(a_tau+1.)/2.

  type, public :: pyjets_spin
    integer           :: index_to_hepeup  ! =-1, if no matching entry in hepeup
    double precision  :: helicity   ! copy of SPINUP
    integer           :: pid        ! particle ID
    integer           :: id_orig    ! pid of parent
    integer           :: index_orig ! index of parent
    integer           :: n_daughter ! number of daughter
    integer, dimension(10) :: index_daughter  ! index of daughter particles
  end type pyjets_spin
  type(pyjets_spin), dimension(n_pyjets_max) :: pyjets_spin_data
  integer :: last_event_number = -999 
  save pyjets_spin_data

  integer, dimension(200) :: mstp
  double precision, dimension(200) :: parp
  integer, dimension(200) :: msti
  double precision, dimension(200) :: pari
  common/pypars/mstp,parp,msti,pari

contains

  subroutine fill_pyjets_spin_data
    integer :: ip
    integer :: hepeup_index
    logical :: is_document_line
    integer :: iorig
    integer :: idau1, idau2, n_doc_lines

!
! Set helicity information of document lines at the first call of this event
!

    do ip=1, n_pyjets_max
      pyjets_spin_data(ip)%index_to_hepeup=-100
      pyjets_spin_data(ip)%helicity=100
    end do

    ip = 1
    hepeup_index=0
    is_document_line=.true.
    
    do while ( k(ip,1) .eq. 21 )
      pyjets_spin_data(ip)%pid     = k(ip,2)
      pyjets_spin_data(ip)%id_orig = k(ip,3)
      pyjets_spin_data(ip)%index_orig = ip
      pyjets_spin_data(ip)%n_daughter = 0
      iorig=k(ip,3)
      if( iorig .eq. 0 ) then 
        hepeup_index=hepeup_index+1
        pyjets_spin_data(ip)%index_to_hepeup=hepeup_index
        pyjets_spin_data(ip)%helicity = spinup(hepeup_index)
      else
        pyjets_spin_data(ip)%index_to_hepeup=-1
        pyjets_spin_data(ip)%helicity = 0
        pyjets_spin_data(iorig)%n_daughter = pyjets_spin_data(iorig)%n_daughter+1
        pyjets_spin_data(iorig)%index_daughter(pyjets_spin_data(iorig)%n_daughter)=ip
      end if
!       print *,' ip=',ip,' iorig=',iorig,' pid=',k(ip,2),' spin=',pyjets_spin_data(ip)%helicity
      if ( abs(k(ip,2)) .eq. 15 .and. pyjets_spin_data(ip)%helicity.eq. 9 ) then 
        if ( nsub_call .lt. min(5, 2*max_dump  )) then 
          print *,'##########################################################################'
          print *,'WARNING from sub. fill_pyjets_spin_data in ilc_tauola_mod2.f90.'
          print *,'tau helicity information is not set, though polarized tau decay was requested.'
          print *,'Most likely, the sindarin file does not include "polarized e3,E3" and "?polarized_events=true" '
          print *,'Number of call=',nsub_call,' ip=',ip,' iorig=',iorig,' pid=',k(ip,2),' spin=',pyjets_spin_data(ip)%helicity
          print *,'##########################################################################'
        endif
      endif

      ip=ip+1
    end do
    n_doc_lines=ip-1

    do ip = 1, n_doc_lines
      if ( pyjets_spin_data(ip)%n_daughter .eq. 2 ) then 
! if h0 decays to tau pairs
        if ( pyjets_spin_data(ip)%pid .eq. 25 .or. pyjets_spin_data(ip)%pid .eq. 35 .or.  pyjets_spin_data(ip)%pid .eq. 36 ) then 
          idau1=pyjets_spin_data(ip)%index_daughter(1)
          idau2=pyjets_spin_data(ip)%index_daughter(2)
          if( abs(pyjets_spin_data(idau1)%pid).eq.15 .and. (pyjets_spin_data(idau1)%pid+pyjets_spin_data(idau2)%pid).eq.0 ) then 
            if( pyr(0) .lt. 0.5 ) then 
              pyjets_spin_data(idau1)%helicity=1
              pyjets_spin_data(idau2)%helicity=-1
            else   
              pyjets_spin_data(idau1)%helicity=-1
              pyjets_spin_data(idau2)%helicity=1
            end if
          end if

! if Z0 decays to tau pairs      
        else if ( pyjets_spin_data(ip)%pid .eq. 23 ) then
          idau1=pyjets_spin_data(ip)%index_daughter(1)
          idau2=pyjets_spin_data(ip)%index_daughter(2)
          if( abs(pyjets_spin_data(idau1)%pid).eq.15 .and. (pyjets_spin_data(idau1)%pid+pyjets_spin_data(idau2)%pid).eq.0 ) then 
! prob_tau_left_z = probability of tau- to be left-handed.
            if ( ((pyr(0) - prob_tau_left_z ) * pyjets_spin_data(idau1)%pid ) .gt. 0.0 ) then 
              pyjets_spin_data(idau1)%helicity=+1
              pyjets_spin_data(idau2)%helicity=-1
            else 
              pyjets_spin_data(idau1)%helicity=-1
              pyjets_spin_data(idau2)%helicity=+1
            end if
          end if

! If W+(24)/H+(37)  decays to tau+(-15) and neu_tau
        else if ( pyjets_spin_data(ip)%pid .eq. 24 .or. pyjets_spin_data(ip)%pid .eq. 37 )  then 
          idau1=pyjets_spin_data(ip)%index_daughter(1)
          idau2=pyjets_spin_data(ip)%index_daughter(2)
          if ( pyjets_spin_data(idau1)%pid .eq. -15 ) then 
               pyjets_spin_data(idau1)%helicity=1
          else if ( pyjets_spin_data(idau2)%pid .eq. -15 ) then 
               pyjets_spin_data(idau2)%helicity=1
          endif

! If W-(-24)/H-(-37) decays to tau-(+15) and neu_tau_bar
        else if ( pyjets_spin_data(ip)%pid .eq. -24 .or. pyjets_spin_data(ip)%pid .eq. -37 ) then
          idau1=pyjets_spin_data(ip)%index_daughter(1)
          idau2=pyjets_spin_data(ip)%index_daughter(2)
          if ( pyjets_spin_data(idau1)%pid .eq. 15 ) then
             pyjets_spin_data(idau1)%helicity=-1
          else if ( pyjets_spin_data(idau2)%pid .eq. 15 ) then
             pyjets_spin_data(idau2)%helicity=-1
          endif
        end if 
      end if
    end do  
!
    return
  end subroutine fill_pyjets_spin_data


! ========================================================================================================
!  Main interface to tauola.
!  Called by PYTAUD through handle_pytaud and calls TAUOLA 
! ========================================================================================================
!
  subroutine ilc_tauola_pytaud(itau,iorig,kforig,ndecay)
    !!! Line number in /JETSET/ where the tau is stored
    integer, intent(in)  :: itau
    !!! Line number where the mother is stored. =0 if the mother is not stored
    integer, intent(in)  :: iorig
    !!! Flavour code of the mother. 0 unknown. H0(25), W+-(+-24),
    !!!   gamma*/Z=23, H+-(+-37)
    integer, intent(in)  :: kforig
    !!! Number of decay products to be given by user routine.
    integer, intent(out) :: ndecay

    integer :: i
    integer :: ip
    integer :: inext
    integer :: itau1, ktau, idau1, idau2

    
    gIsHiggsDecay = .false.
    if ( kforig .eq. 25 .or. kforig.eq.35 .or. kforig.eq. 36 ) then 
      gIsHiggsDecay=.true.
    endif

    !!! JRR: Tau decays are very sensitive to numerical noise, momenta
    !!!      should be, in principle, strictly on the z axis
    if (abs (p(itau,1)) < 1.d-13)  p(itau,1) = 0
    if (abs (p(itau,2)) < 1.d-13)  p(itau,2) = 0

! MSTI(4) ; number of documentation lines
! MSTI(5) ; number of events generated
    if ( last_event_number .ne. msti(5) ) then 
      call fill_pyjets_spin_data
      last_event_number=MSTI(5)   
      jtau2=-1000
      nsub_call=nsub_call+1
    end if

    
    if ( nsub_call .lt. max_dump ) then 
      print *,''
      print *,'############### ilc_tauola_pytaud was called. ### ncall = ',nsub_call, ' ###########' 
      print *,' itau=',itau, ' iorig=',iorig,' kforig=',kforig
      call pylist(2)
      print *,'############### end of PYLIST  #################################################'
    endif

    jtau=itau
    jorig=iorig
    jforig=kforig

! If tau origin is not known ( tau is generated by parton generator ), look parton information,
! which are stored as status code=21
    if ( iorig .eq. 0 ) then 
      ip=itau
      do while ( k(ip,3) .ne. 0 .or. ip .le. 0 ) 
        inext = k(ip,3)
        ip = inext
      end do
      id_dexay=k(jtau,2)
      p_dexay=p(jtau,1:5)
      pyjets_spin_data(itau)%helicity=pyjets_spin_data(ip)%helicity
      spin_dexay = pyjets_spin_data(ip)%helicity
      if ( id_dexay .gt. 0 ) then ! flip the sign of spin in advance, because it is flipped later
        spin_dexay = -spin_dexay 
      end if

! 
! If tau origin is known ( iorig .ne. 0 ), 
! spin should be decided  by a call to fill_pyjeys_spin_data
!
    else
      id_dexay=k(jtau,2)
      p_dexay=p(jtau,1:5)

!### h0 or z0 or w case
      if ( gIsHiggsDecay .or. kforig.eq.23 .or. abs(kforig).eq.24 ) then 
        ip = k(itau,3)
        if( k(ip,1).eq.21 .and. abs(k(ip,2)).eq.15 ) then 
          pyjets_spin_data(itau)%helicity = pyjets_spin_data(ip)%helicity
        endif
        spin_dexay=pyjets_spin_data(itau)%helicity

! Fill momentum of second tau if kforig .eq. 25 ( Higgs )
        if( gIsHiggsDecay .and. gCorrelateTransverseSpin .ne. 0 ) then

          idau1=pyjets_spin_data(iorig)%index_daughter(1)
          idau2=pyjets_spin_data(iorig)%index_daughter(2)
          if ( idau1 .eq. 0 ) then ! Parent Higgs is not in the documentation line ( K(,1) != 21 )
            idau1 = k(iorig,4)     ! Get pointer to daughter directly from JETSET
            idau2 = k(iorig,5)
          endif
          if ( idau1 .ne. itau ) then
            print *,'Fatal error in  ilc_tauola_pytaud '
            print *,' idau1(',idau1,') and itau(',itau,') is not equal.'
            print *,' Something is wrong in parent-daughter relation.'
            stop
          endif
          jtau2=idau2
          pyjets_spin_data(jtau)%helicity = 0   ! reset tau spin information because it is decided internally
          pyjets_spin_data(jtau2)%helicity = 0
        endif

!### Unknow decay mother
      else 
        print *,'In ilc_tau_decay : Unknown decay modther of tau, id=',kforig,' tau is 50% right or 50% left handed.'
        if ( pyr(0) .lt. 0.5 ) then 
          spin_dexay=-1
        else
          spin_dexay=1
        end if
        pyjets_spin_data(itau)%helicity=spin_dexay
      end if  

    end if

    call do_dexay( kforig )

    ndecay=nproducts

    

    return

  end subroutine ilc_tauola_pytaud

! ========================================================================================================
  subroutine do_dexay( kforig )
!
!   Main routine to call Tauola.  Three type of tau decay, (A) Higgs to tau+tau-, 
!   (B) single tau+ decay, (C) single tau- decay, are treated separately. 
!
! ========================================================================================================

    integer, intent(in) :: kforig

    integer   :: i
    logical   :: ifpseudo, is_swapped
    double precision, dimension(4) :: pol1, pol2
    integer   :: im, idx1, idx2, idsign
    double precision  :: rrr(1), wt
    double precision  :: hh1(4), hh2(4)
    integer :: ion(3), np

    tauspin_pyjets(jtau)=spin_dexay

    nhep=3

    isthep(2)=1
    idhep(2)=id_dexay
    jmohep(:,2)=0
    jdahep(:,2)=0
    phep(:,2)=p_dexay

    isthep(3)=1
    idhep(3)=-id_dexay
    jmohep(:,3)=0
    jdahep(:,3)=0
    phep(1:3,3)=-phep(1:3,2)
    phep(4:5,3)=phep(4:5,2)

! NOTE (Akiya Miyamoto, 25-March-2016)
!  Higgs(h0, H0, A) to tau+tau- decay is handled here
!  in order to implement a transverse spin correlation. 
!  For this algorithm to work, photon emmision from tau
!  before decay should be turned off. Since photon emmision 
!  from tau is handled by PYTHIA, photon emmision from ALL tau 
!  should be turned off.  It is done by setting MSTJ(39)=15.
!  Instead, PHOTOS is called after tau decay and generate
!  photonss. 
!  
! ****************************************************************
! (A) Higgs to tau+ tau- decay .
! ****************************************************************
    if( gIsHiggsDecay .and. gCorrelateTransverseSpin .ne. 0 ) then 
      is_swapped=.false.
!
!  tau+ momentum should have positive Pz
!  tau- momentum should have negative Pz
!
! 
      if( idhep(2) .lt. 0 ) then ! First tau is tau+
        np1 = 2
        np2 = 3
        phep(:,2) = p_dexay
        phep(1:3,3) = -phep(1:3,2) 
        phep(4:5,3) = phep(4:5,2)
      else
        idhep(3)= id_dexay
        idhep(2)=-id_dexay
        np1 = 2
        np2 = 3
        phep(:,2) = p_dexay
        phep(1:3,3) = -phep(1:3,2) 
        phep(4:5,3) = phep(4:5,2)
        is_swapped=.true.
      endif

      isthep(1)=11
      idhep(1)=kforig
      jmohep(:,1)=0
      jdahep(1,1)=2
      jdahep(2,1)=3
      phep(:,1)=phep(:,2)+phep(:,3)
      phep(5,1)=sqrt(phep(4,1)**2-phep(1,1)**2-phep(2,1)**2-phep(3,1)**2)
      jmohep(:,2)=1
      jmohep(:,3)=1

      p1=phep(1:4,NP1)  ! tau+ momentum
      p2=phep(1:4,NP2)  ! tau- momentum 
      
      q1 = p1+p2
      im = 1  

      
! Does SPINHIGGS in tauface_jetset.f    
      ifpseudo = .false.
      if( kforig.eq.36 ) then 
        ifpseudo = .true.
      endif

!
! TODO (Akiya Miyamoto, 25-march-2016)
!   In the following code, tau helicity (polarization vector) 
!   information is not stored in pyjets_spin_data(jtau)%helicity 
!   and /HEPEV4/, because tau polarization vector is determined 
!   here in order to have a transverse spin correlation between
!   tau+ and tau-, but the decided polarization vectors are not 
!   calculated here.  It would be possible to calculate them 
!   from the polarimetric vectors, hh1 and hh2,  after
!   the end of the loop, between "10 continue" and "goto 10".  
!   

      if ( gCorrelateTransverseSpin .eq. 2 ) then 
             
        pol1=0
        pol2=0
        if( pyr(0) .gt. 0.5 ) then         
          pol1(3)=1
          pol2(3)=-1
        else
          pol1(3)=-1
          pol2(3)=1
        endif
        call dexay(1, pol1)
        call dexay(2, pol2) 

      else 
!
! Decide polarimetric vector to have a spin correlation
!
10      continue
          call ranmar(rrr, 1)
          call dekay(1, hh1)  ! TAU+ decay
          call dekay(2, hh2)  ! TAU- decay
          wt = wthiggs(ifpseudo, hh1, hh2)
       if( rrr(1) .gt. wt ) goto 10
!
        ion(1)=0
        ion(2)=0
        ion(3)=0
        call dekay(11, hh1)
        call taupi0(0, 1, ion )
        call dekay(12, hh2)
        call taupi0(0, 2, ion )

      endif 

      if( photo_switch_ifphot .eq. 1 ) call photos(im)

! ********************************************************
! (B) Single Tau+ decay
! ********************************************************
    else if(idhep(2).lt.0) then ! np1 is tau+
       np1=2
       np2=3
       pol=0.
       pol(3)=-spin_dexay
       p1=phep(1:4,2)
       p2=phep(1:4,3)
       q1=p1+p2
!       if ( nsub_call .lt. max_dump ) then 
!         print *,' tau+ decay with pol(3)=',pol(3)
!         print *, " pol(3)=",pol(3)
!         print *, " antiparticle decay q1= ", q1
!         print *, " antiparticle decay p1= ", p1
!         print *, " antiparticle decay p2= ", p2
!       endif 

       call dexay(1,pol)  ! tau+ decay by tauola

       if( photo_switch_ifphot .eq. 1 ) call photos(np1)

! ********************************************************
! (C) Single Tau- decay
! ********************************************************
    else  
       np2=2  
       np1=3 
       pol=0.
       pol(3)=-spin_dexay
       p2=phep(1:4,2) 
       p1=phep(1:4,3)  
       q1=p1+p2
!       if ( nsub_call .lt. max_dump ) then 
!         print *,'tau- decay with pol(3)=',pol(3)
!         print *, " pol(3)=",pol(3)
!         print *, " particle decay q1= ", q1
!         print *, " particle decay p1= ", p1
!         print *, " particle decay p2= ", p2
!       endif

       call dexay(2,pol)  ! tau- decay by tauola

       if( photo_switch_ifphot .eq. 1 ) call photos(np2)

    end if

! **********************************************************
! Now copies /HEPEVT/ to /PYJETS/
! Higgs tau pair decay and single tau decay are treated 
! separately.
! **********************************************************

    nproducts=0
    np=nproducts
!
! =========================================================
! Higgs to tau pair decay case.
! =========================================================
!
    if( gIsHiggsDecay .and. gCorrelateTransverseSpin.ne.0 .and. jtau2 .gt. 0 ) then 
      if ( is_swapped ) then ! invert all momentum
        do i=2, nhep
          phep(1:3,i) = - phep(1:3,i)
        enddo

        do i=4, nhep
          if( jmohep(1,i) .eq. 2 ) then
             jmohep(1,i)=3
             jmohep(2,i)=3
          else if ( jmohep(1,i) .eq. 3 ) then
             jmohep(1,i)=2
             jmohep(2,i)=2
          endif
        enddo

      endif

!
! Overwrite tau+ and tau- data in /PYJETS/, because tau+tau- momentum could be changed
! due to photon emmision in Higgs --> tau+ tau- system.  Their momentum should 
! be boosted and rotate back to the lab frame in the calling routine, PYDCAY.
!
      if( is_swapped ) then 
        p(jtau,:) = phep(:,3)
        k(jtau,4) = jdahep(1,3)-3+n
        k(jtau,5) = jdahep(2,3)-3+n
        p(jtau2,:) = phep(:,2)
        k(jtau2,4) = jdahep(1,2)-3+n
        k(jtau2,5) = jdahep(2,2)-3+n
      else
        p(jtau,:) = phep(:,2)
        k(jtau,4) = jdahep(1,2)-3+n
        k(jtau,5) = jdahep(2,2)-3+n
        p(jtau2,:) = phep(:,3)
        k(jtau2,4) = jdahep(1,3)-3+n
        k(jtau2,5) = jdahep(2,3)-3+n
      endif      
      k(jtau,  1) = 11
      k(jtau2, 1) = 11
      k(jtau,  3) = jorig
      k(jtau2, 3) = jorig

! TODO : Akiya Miyamoto, 12-April-2016
!   Reset daughter pointer of Higgs, because Higgs daughters increase when photons are emitted.
!   This may not work well if additional particles exist after second tau. 
!   
      k(jorig,4) = jtau
      k(jorig,5) = jtau2  ! jtau2 > jtau allways
      if( jdahep(2,1)-jdahep(1,1)+1 .gt. 2 ) then
        if ( n .gt. jtau2 ) then 
          print *,'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'
          print *,'% %Warning in tau decay (ilc-tauola-mod2@do_dexay) '
          print *,'% It is necessary to update the index of Higgs daughter in order to include photons produced by PHOTOS.'
          print *,'% But not able to do so because position after tau pair is ocuppied.'
          print *,'% Run continues without modifying the 2nd daughter pointer.'
          print *,'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'
        else
          k(jorig,5) = jdahep(2,1)-3+n
        endif
      endif      

!
! Now, fill the information of tau daughters to /PYJETS/
!
      nproducts = 0
      loop_products_higgs: do i=4,nhep
       nproducts=nproducts+1
       p(n+nproducts,:)=phep(:,i)
       k(n+nproducts,2)=idhep(i)
       k(n+nproducts,3)=jmohep(1,i)-3+n
       if ( isthep(i) .eq. 1 ) then
          k(n+nproducts,1)=1
          k(n+nproducts,4)=0
          k(n+nproducts,5)=0
       else
          k(n+nproducts,1)=11
          k(n+nproducts,4)=jdahep(1,i)-3+n
          k(n+nproducts,5)=jdahep(2,i)-3+n
       endif
    end do loop_products_higgs


!
! ***************************************************************
! Single tau decay case. 
! This case, parent tau daghter momentum is not over-wtitten
! ***************************************************************
!
    else
      loop_products_nohiggs: do i=4,nhep


         nproducts=nproducts+1
         p(n+nproducts,:)=phep(:,i)
         if ( isthep(i) .eq. 1 ) then
            k(n+nproducts,1)=1
            k(n+nproducts,4)=0
            k(n+nproducts,5)=0
         else
            k(n+nproducts,1)=11
            k(n+nproducts,4)=jdahep(1,i)-3+n
            k(n+nproducts,5)=jdahep(2,i)-3+n
         endif
         k(n+nproducts,2)=idhep(i)
         if ( abs(idhep(jmohep(1,i))) .ne. 15 ) then
           k(n+nproducts,3)=jmohep(1,i)-3+n
         else
            k(n+nproducts,3)=jtau
         endif
      end do loop_products_nohiggs
      k(jtau,4) = jdahep(1,2)-3+n
      k(jtau,5) = jdahep(2,2)-3+n

    endif

! *********************************
! All done.  Now return to caller
! *********************************

    if ( nsub_call .lt. max_dump ) then 
      print *,'%%% ilc_tauola_mod2.   PYLIST at the end of do_dexay ...'
      n=n+nproducts
      call pylist(2)
      n=n-nproducts
    endif 

    return

  end subroutine do_dexay

! ==========================================================
  subroutine ilc_tauola_print_helicity_info_mod
    integer :: ip

  end subroutine ilc_tauola_print_helicity_info_mod

! ==========================================================
  function ilc_tauola_get_helicity_mod (ip) result (the_helicity)
    implicit none
    integer, intent(in)  :: ip
    integer           :: the_helicity
    
    integer, dimension(200) :: mstu
    double precision, dimension(200) :: paru
    integer, dimension(200) :: mstj
    double precision, dimension(200) :: parj
    common/pydat1/mstu, paru, mstj, parj
    save/pydat1/

    if ( MSTJ(28) .NE. 2 ) then 
       the_helicity=0
    else 
      if ( ip .le. 0 .or. ip .gt. n ) then 
        the_helicity = 0
      else 
        the_helicity = int(pyjets_spin_data(ip)%helicity)
      end if
    endif 

  end function ilc_tauola_get_helicity_mod

end module ilc_tauola_mod2

! ============================================================================
subroutine ilc_tauola_init_call
!
! Tauola initialization.  Should be called once at the begining of a job.
! TAUOLA and PHOTOS parameters are read in from a file, tauola.input, if 
! it exists in the current directory, or read in from a file defined by 
! the environment parameter, TAUOLA_PARAMETER_FILE.  If they do not exist, 
! default parameters defined here are used. 
!
! Contents of taupla.input should be
!  jak1   ! (0) decay mode of first tau
!  jak2   ! (0) decay mode of second tau
!  itdkrc ! (1) switch of radiative corrections in decay
!  ifphot ! (1) PHOTOS switch
!  gCorrelateTransverseSpin ! (1) Correlate(1)/not correlate(0) transverse spin of tau+ tau- from Higgs
!  xmtau  ! (1.777 ) tau mass
!  xmh    ! (125.0) Higgs mass
!  mixing_angle_in_degree ! (90.0) Scalor and Psuedo_Scalar mixing angle
! 
! 
! ============================================================================

  use ilc_tauola_mod2, only : photo_switch_ifphot, gCorrelateTransverseSpin
  use hepeup_include_common

  implicit none

  INTEGER JAK1, JAK2, JAKP, JAKM, KTOM
  COMMON /JAKI/ JAK1, JAK2, JAKP, JAKM, KTOM

  !C...PYTHIA commonblock: only used to prxvidnteger, dimension(200) :: mstp
  integer, dimension(200) :: mstp
  double precision, dimension(200) :: parp
  integer, dimension(200) :: msti
  double precision, dimension(200) :: pari
  common /pypars/ mstp, parp, msti, pari
  save /pypars/

  integer, dimension(200) :: mstu
  double precision, dimension(200) :: paru
  integer, dimension(200) :: mstj
  double precision, dimension(200) :: parj
  common/pydat1/mstu, paru, mstj, parj
  save/pydat1/

  character*1024 tauola_parameter_file
  integer inuni
  double precision higgs_cp_angle

  integer :: itdkrc, ifphot
  double precision  :: xmtau, xmh, mixing_angle_in_degree
  double precision  :: pi, psi, betah
  double precision  csc, ssc
  common /pseudocoup/  csc, ssc
  save /pseudocoup/

  integer ion(3), iTransSpinCorr 
  double precision :: pol
  double precision :: pol1x(4)  

! define default TAUOLA parameters
!
   jak1  =  0     ! decay mode first tau
   jak2  =  0     ! decay mode second tau
   itdkrc=  1     ! switch of radiative corrections in decay
   ifphot=  1     ! PHOTOS switch
   xmtau = 1.777 ! tau mass
   xmh   = 125.0 ! higgs mass
   mixing_angle_in_degree = 90.0  ! Scalor and Pseudo_Scalar mixing angle
   pol = 1.0    ! tau polarization switch. must be 1 or 0
   gCorrelateTransverseSpin = 0 ! (1/0) Correlate/not correlate transverse spin of tau+ tau- from Higgs
                                ! 2 for internal test ( does longitudinal correlation only. )   
!
  call getenv("TAUOLA_PARAMETER_FILE",tauola_parameter_file)
  if ( lnblnk(tauola_parameter_file) .eq. 0 ) then 
    tauola_parameter_file="tauola.input"
  else 
    print *,'TAUOLA_PARAMETER_FILE is',tauola_parameter_file(1:lnblnk(tauola_parameter_file))
  end if


  inuni=28
  open(inuni,file=tauola_parameter_file,status='old',err=200)
  print *,'Tauola parameters are obtained from ',tauola_parameter_file(1:lnblnk(tauola_parameter_file))
  read(inuni,*) jak1 ! (0) decay mode of first tau
  read(inuni,*) jak2 ! (0) decay mode of second tau
  read(inuni,*) itdkrc ! (1) switch of radiative corrections in decay
  read(inuni,*) ifphot ! (1) PHOTOS switch
  read(inuni,*) gCorrelateTransverseSpin ! (1/0) Correlate/not correlate transverse spin of tau+ tau- from Higgs 
  read(inuni,*) xmtau  ! (1.777 ) tau mass
  read(inuni,*) xmh    ! (125.0) Higgs mass
  read(inuni,*) mixing_angle_in_degree ! (90.0) Scalor and Psuedo_Scalar mixing angle  
  close(inuni)
200 continue

!
   pi = ACOS(-1.0D0)
   psi=mixing_angle_in_degree/180.0*pi
   betah = sqrt(1.0d0 - 4.0*xmtau**2/xmh**2)
   csc = cos(psi)*betah
   ssc = sin(psi)
   photo_switch_ifphot = ifphot

   if ( gCorrelateTransverseSpin .ne. 0 ) then
     if ( mstj(39) .ne. 15 ) then
       print *,'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'
       print *,'%% Warning %% in ilc_tauola_init_call ...'
       print *,'%% Transverse spin correlation is reuested in Higgs to tau pair decay'
       print *,'%% But MSTJ(39) is not 15. in order to disable photon emission from taus'
       print *,'%% by PYTHIA for TAUOLA.MSTJ(39) is forced to be 15.'
       print *,'%% MSTJ(15) and CorrelateTransverseSpin should be 0 to allow photon '
       print *,'%% emission from taus by pythia. Only longitudinal pol. correlation in '
       print *,'%% Higgs decay should be seen.' 
       print *,'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'
       mstj(39) = 15
!     else
!       print *,'MSTJ(39)=15 is confirmed in ilc_tauola_init_call'
     endif
   endif



!   if( ifphot .eq. 1 ) 
   call phoini ! calls phoini anyway
   call inietc( jak1, jak2, itdkrc, ifphot)
   call inimas
   call iniphx(0.01d0)
   call initdk
!
! de-activation of pi0 and eta decays: (1) means on, (0) off
   ion(1) = 0
   ion(2) = 0
   ion(3) = 0  ! ilc_tauola_mod2 does not call taupi0
   call taupi0(-1, 1, ion)
   call dekay(-1, pol1x)
   write(6,7001) pol, psi, ion(1), ion(2), ion(3)
 7001 FORMAT(///1X,15(5H*****) &
     /,' *',     25X,'*****TAUOLA UNIVERSAL INTERFACE: ******',9X,1H*, &
     /,' *',     25X,'*****VERSION 1.21, September 2005******',9X,1H*, &
     /,' *',     25X,'**AUTHORS: P. Golonka, B. Kersevan, ***',9X,1H*, &
     /,' *',     25X,'**T. Pierzchala, E. Richter-Was, ******',9X,1H*, &
     /,' *',     25X,'****** Z. Was, M. Worek ***************',9X,1H*, &
     /,' *',     25X,'**USEFUL DISCUSSIONS, IN PARTICULAR ***',9X,1H*, &
     /,' *',     25X,'*WITH C. Biscarat and S. Slabospitzky**',9X,1H*, &
     /,' *',     25X,'****** are warmly acknowledged ********',9X,1H*, &
     /,' *',     25X,'                                       ',9X,1H*, &
     /,' *',     25X,'********** INITIALIZATION  ************',9X,1H*, &
     /,' *',F20.5,5X,'tau polarization switch must be 1 or 0 ',9X,1H*, &
     /,' *',F20.5,5X,'Higs scalar/pseudo mix CERN-TH/2003-166',9X,1H*, &
     /,' *',I10, 15X,'PI0 decay switch must be 1 or 0        ',9X,1H*, &
     /,' *',I10, 15X,'ETA decay switch must be 1 or 0        ',9X,1H*, &
     /,' *',I10, 15X,'K0S decay switch must be 1 or 0        ',9X,1H*, &
     /,1X,15(5H*****)/)


   print *,'Tau decay modes were set immediately after tauola initialization'
   print *,'Tau decay modes: tau+(jak1)=',jak1,' tau-(jak2)=',jak2
   print *,'  JAK=0 : All decay mode'
   print *,'  JAK=1 ELECTRON MODE'
   print *,'  JAK=2 MUON MODE'
   print *,'  JAK=3 PION MODE'
   print *,'  JAK=4 RHO  MODE'
   print *,'  JAK=5 A1   MODE'
   print *,'  JAK=6 K    MODE'
   print *,'  JAK=7 K*   MODE'
   print *,'  JAK=8-13 npi modes'
   print *,'  JAK=14-19 KKpi & Kpipi modes'
   print *,'  JAK=20-21 eta pi pi; gamma pi pi modes'
!
   print *,'  '
   print *,' ITDKRC=',ITDKRC,'  ( Radiative corrections in decay ON(1), Off(0) )'
   print *,' IFPHOT=',IFPHOT,'  ( PHOTOS ON(1), OFF(0) ) '
   print *,' TransSpinCorr=', gCorrelateTransverseSpin, ' Correlate(1)/not correlate(0) transverse spin of tau+ tau- from Higgs'
   print *,' xmtau=',xmtau,'  ( tau mass ) '
   print *,' xmh=',xmh, '  ( Higgs mass )' 
   print *,' mixing andle=',mixing_angle_in_degree,'  ( Mixing angle of Scalor and Pseudo Scalar in degree )'   

  return 
end subroutine ilc_tauola_init_call

! ============================================================================
subroutine ilc_tauola_print_helicity_info
! ============================================================================
  use ilc_tauola_mod2
  call ilc_tauola_print_helicity_info_mod
  return
end subroutine ilc_tauola_print_helicity_info

! ============================================================================
subroutine ilc_tauola_get_helicity (ip, the_helicity)
! ============================================================================
  use ilc_tauola_mod2
  implicit none
  integer, intent(in)  :: ip
  integer, intent(out) :: the_helicity
  the_helicity=ilc_tauola_get_helicity_mod(ip)
  if ( abs(the_helicity) .gt. 1 ) then 
    print *,'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'
    print *,'Warning : stored helicity information is wrong. ',the_helicity,' ip=',ip
    print *,'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'
  endif

end subroutine ilc_tauola_get_helicity

! ============================================================================
subroutine handle_pytaud(itau,iorig,kforig,ndecay)
! ============================================================================
  use ilc_tauola_mod2
  implicit none
  integer, intent(in)  :: itau
  integer, intent(in)  :: iorig
  integer, intent(in)  :: kforig
  integer, intent(out) :: ndecay
  call ilc_tauola_pytaud(itau,iorig,kforig,ndecay)
end subroutine handle_pytaud

!*********************************************************************
!...PYTAUD
!...Routine to handle the decay of a polarized tau lepton.
!...Input:
!...ITAU is the position where the decaying tau is stored in /PYJETS/.
!...IORIG is the position where the mother of the tau is stored;
!...     is 0 when the mother is not stored.
!...KFORIG is the flavour of the mother of the tau;
!...     is 0 when the mother is not known.
!...Note that IORIG=0 does not necessarily imply KFORIG=0;
!...     e.g. in B hadron semileptonic decays the W  propagator
!...     is not explicitly stored but the W code is still unambiguous.
!...Output:
!...NDECAY is the number of decay products in the current tau decay.
!...These decay products should be added to the /PYJETS/ common block,
!...in positions N+1 through N+NDECAY. For each product I you must
!...give the flavour codes K(I,2) and the five-momenta P(I,1), P(I,2),
!...P(I,3), P(I,4) and P(I,5). The rest will be stored automatically.
subroutine pytaud (itau,iorig,kforig,ndecay)
  use ilc_tauola_mod2
  implicit none
  integer itau,iorig,kforig
  integer ndecay
  !print *,"###############################################"
  !print *,"###### tauola pytaud was called ###############"
  !print *," itau,iorig,kforig=",itau,iorig,kforig
  !print *,"###############################################"

  call ilc_tauola_pytaud(itau,iorig,kforig,ndecay)

end subroutine pytaud

