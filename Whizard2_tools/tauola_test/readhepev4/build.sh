#!/bin/bash

. /group/ilc/soft/gcc481/gcc481.setup

STDHEP_DIR=/group/ilc/soft/gcc481/stdhep/stdhep-5-06-01
MCFIO_DIR=${STDHEP_DIR}


FFLAGS=" -fno-second-underscore  -fd-lines-as-comments -fPIC "
gfortran ${FFLAGS} -I${STDHEP_DIR}/src/inc -I${STDHEP_DIR}/mcfio/src listStdHep.F \
         -L${STDHEP_DIR}/lib -lstdhep -lFmcfio -lstdhepC \
         -o listStdHep.exe
