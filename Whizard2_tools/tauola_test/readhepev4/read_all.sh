#!/bin/bash 

datadir=../examples/jobs

maxrecord=100
for d in ${datadir}/E* ; do
  fread=`/bin/ls ${d}/*.ev4.hep`
  logpref=`basename ${d} | sed -e "s/hep.ev4//"`
  logfile=${logpref}.log
  echo "Input file=${fread}" > ${logfile}
  ls -l ${fread} >> ${logfile}
  ln -sf ${fread} ${logpref}.stdhep
  echo -e "${logpref}.stdhep \n ${maxrecord}" | ./listStdHep.exe >> ${logfile} 2>&1 
  rm -f ${logpref}.stdhep 
done
