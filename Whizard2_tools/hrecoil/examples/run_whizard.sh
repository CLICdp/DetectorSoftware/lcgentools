#!/bin/bash
# A script run Whizard jobs
# 



newpath=`echo $PATH | sed -e "s|/usr/local/texlive/p2011|/group/ilc/soft/utils64/texlive/p2011|"`
export PATH=${newpath}

sindir=`pwd`/sindarin
aliases="${sindir}/aliases.sin"
pythia6_parameters="${sindir}/pythia6-parameters.sin"
beam_events_file="/group/ilc/soft/gcc481/GP/beamdata/lumiee250/lumiee250-run1.dat"
beam_events_file="/group/ilc/soft/gcc481/GP/beamdata/lumiee250-hs/lumiee250-run1to20.dat"
# n_events=100000
n_events=20000

# ####################################
# Whizard run of one process
# ####################################
run_whizard() 
{
  proc=$1
  jobdir=jobs/${proc}
  rm -rf ${jobdir}
  mkdir -p ${jobdir}
  ( 
    cd ${jobdir}
    sed -e "s|%%ALIASES%%|${aliases}|g" \
	-e "s|%%PYTHIA6_PARAMETERS%%|${pythia6_parameters}|g" \
        -e "s|%%BEAM_EVENTS_FILE%%|${beam_events_file}|g" \
        -e "s|%%N_EVENTS%%|${n_events}|g" \
        ${sindir}/${proc}.sin > ${proc}.sin
      /usr/bin/time whizard -r ${proc}.sin > run_whizard.log 2>&1  
  )
}

# #######################################
# Loop over many processes and Run many runs 
# #######################################
#

while read f ; do 
  echo "Running whizard for $f "
  run_whizard $f 
done <<EOF
E250.Pe2e2h
EOF
