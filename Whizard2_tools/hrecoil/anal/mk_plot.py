
from RecoilTools import *
import commands

pdftk_path="pdftk"  # Modify path to pdftk

# =======================================================
# Create ROOT files. 
# =======================================================

ecm=250

dir="/hsm/ilc/grid/storm/prod/ilc/mc-dbd/generated/250-TDR_ws/higgs/"
filename=dir+"E250-TDR_ws.Pe2e2h.Gwhizard-1_95.eL.pR.I106479.001.stdhep"
reader=StdHepReader.StdHepReader(filename)
Anal_e2e2h(reader, "whizard1.95.root",ecm,verbosity=2)

'''
dir="/home/ilc/miyamoto/work/151008-whizard2/higgs/result-e2e2h-beam-hs/"
filename=dir+"E250.Pe2e2h.Gwhizard2-beam.Wpythia.eL.pR.slcio"
reader=LcioReader.LcioReader(filename)
Anal_e2e2h(reader, "whizard2.2.8.root",ecm,verbosity=2)
'''

dir="../examples/jobs/E250.Pe2e2h/"
filename=dir+"E250.Pe2e2h.Gwhizard2_2_8.eL.pR.ev4.hep"
reader=StdHepReader.StdHepReader(filename)
Anal_e2e2h(reader, "whizard2.2.8.root",ecm,verbosity=2)

# ======================================================
# Create comparison plots
# 
figdir="figs/"
ans=commands.getoutput("rm -rf "+figdir+" && mkdir "+figdir)

samples={"plot1":{"file":"whizard1.95.root","color":4, "ypos":800.0,"msg":"Blue: Whizard1.95(DBD,I106479)","nevt":10000,"xsec":17.143214,"opt":"histsame"},
         "plot2":{"file":"whizard2.2.8.root","color":6,"ypos":650.0,"msg":"Purple: Whizard2.2.8","nevt":10000,"xsec":17.211131,"opt":"Esame"}}
intlum=600
intlum=1000
samples["plot1"]["nevt"]=int(samples["plot1"]["xsec"]*float(intlum))
samples["plot2"]["nevt"]=int(samples["plot2"]["xsec"]*float(intlum))
opts={}
opts["htitle"]='#mu#muh recoil mass'+' at {:d} GeV, eL.eR'.format(ecm)
opts["xbins"]=50  #  Histogram X # of bins
opts["xmin"]=123.0  #  Histogram X min
opts["xmax"]=133.0  #  Histogram X max
opts["ymax"]=1800.0  # Histogram Ymax
opts["tx"]=127.0    # Legend X position
opts["msg1"]=str(intlum)+" fb^{-1}"
opts["ymsg"]=950.0 

Plot_e2e2h_recoil(samples, figdir+"hrecoil_linear", opts)

opts["xbins"]=125  #  Histogram X # of bins
opts["xmax"]=173.0
opts["ymax"]=5000.0
opts["tx"]=140.0    # Legend X position
opts["ymsg"]=1200.0 
samples["plot1"]["ypos"]=700.0 
samples["plot2"]["ypos"]=400.0

Plot_e2e2h_recoil(samples, figdir+"hrecoil_log", opts, logscale=1) # logscale=1 for log scale

# =========================================================================
# Create a PDF file, merging all
# =========================================================================
ans=commands.getoutput("cd "+figdir+" && "+pdftk_path+" hrecoil_*.pdf cat output hrecoil_all.pdf")
print ans
print "hrecoil_all.pdf was created."

 
