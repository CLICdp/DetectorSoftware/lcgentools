# ==========================================================================
#  A simple sample to read e2e2h stdhep file and plot costh and energy 
#  visible energy and recoil mass of muon
#
# (Ussage)
#    reader = StdHepReader.StdHepReader()
#    reader.addFile(%fileName%) 
#    Anal_e2e2h(reader, %rootFileName%, %ecm%, 
#              maxread=%maxread%, verbosity=%verb% )
#  where
#     %fileName% : StdHep file name
#     %rootFileName% : Root file name where Ntuple is written
#     %ecm% : Nominal CM Energy (GeV)
#     maxread=%maxread% : Optional parameter. -1(default) read up to EOF
#     verbosity=%verb%  : verbosity. 0(default) print out nothing. 
#                         1,2,3 : print event number in every 10000, 1000, 100 events
#  Author: Akiya Miyamoto, 23-March-2015
# ==========================================================================

from pyLCIO.io import LcioReader, StdHepReader
import ROOT
# ROOT.gROOT.SetBatch()
import sys
import os.path as path


# =======================================================================
# convertToTLorentzVector(particle)
# (Function)
# convert MCParticle to TLorentzVector
# =======================================================================
def convertToTLorentzVector(particle):

    p = particle.getMomentum()
    E = particle.getEnergy()
    return ROOT.TLorentzVector(p[0], p[1], p[2], E)

# ===================================================================
# Anal_e2e2h( reader, rootFileName, maxread=-1)
#   reader : LcioReader or StdHepReader
#   rootfile : Root file name to write Ntuple
#   ecm  : Nominal CM energy (GeV)
# ===================================================================
def Anal_e2e2h(reader, rootFileName, ecm, maxread=-1, verbosity=0) : 

  rfile=ROOT.TFile(rootFileName,"recreate")
  nt = ROOT.TNtuple("nt","NT","evis:mvis:mm")

  nlast=0
  for idx, event in enumerate(reader): 
    if maxread > 0 and idx >= maxread : 
      nlast=idx-1
      break
    if idx%100 == 0 and verbosity == 3 :
      print "Reading ev."+str(idx+1)
    if idx%1000 == 0 and verbosity == 2 :
      print "Reading ev."+str(idx+1)
    if idx%10000 == 0 and verbosity == 1 :
      print "Reading ev."+str(idx+1)
    nlast=idx
    muplus  = int(0)
    muminus = int(0)

    mcParticles = event.getCollection("MCParticle")
    psum=ROOT.TLorentzVector(0.0, 0.0, 0.0, 0.0)
    for p in mcParticles:
      pdg = abs(p.getPDG())
      v = convertToTLorentzVector(p)
      istat=p.getGeneratorStatus()
#     if ( istat == 1 ) :
      if ( pdg == 13 ) : 
        if ( p.getPDG() == 13 and muminus == 0 ) :
          psum += v
          muminus = 1
        if ( p.getPDG() == -13 and muplus == 0 ) :
          psum += v
          muplus = 1

    pini = ROOT.TLorentzVector(0.0, 0.0, 0.0, ecm)
    pmis = pini - psum
    missmass = pmis.M()
    evis = psum.E()
    mvis = psum.M()
    nt.Fill(evis, mvis, missmass )  

  if verbosity > 0 : 
    print "Last event was ev."+str(nlast+1)
  rfile.Write()


# ============================================================
# 
def Plot_e2e2h_recoil(samples, figname, opts, logscale=0) :
  c1=ROOT.TCanvas()
  ymin=0
  if logscale == 1 : 
    ymin=1
  hf=ROOT.TH2F("hf",opts["htitle"],opts["xbins"],opts["xmin"],opts["xmax"],50,ymin,opts["ymax"])
  hf.SetMaximum(opts["ymax"])
  hf.SetStats(0)
  c1.SetGridx(1)
  c1.SetGridy(1)
  if logscale == 1 :
    c1.SetLogy(1)
  hf.GetXaxis().SetTitle("Recoil mass (GeV/c^2)")
  hf.GetYaxis().SetTitle("# Events/bin")
  hf.Draw()
  hdata={}
  for type in samples.keys() :
#   print type
    hdata[type]={}
    hdata[type]["tfile"]=ROOT.TFile(samples[type]["file"])
    nt=hdata[type]["tfile"].Get("nt")
    nent=nt.GetEntries()
    nt.SetLineColor(samples[type]["color"])
    hdata[type]["nt"]=nt
    print type+" ntuple contains "+str(nent)+", requested events is "+str(samples[type]["nevt"])
    if nent < samples[type]["nevt"] : 
      print "Input file for "+type+" does not contain enough events."

    nt.Draw("mm","",samples[type]["opt"],samples[type]["nevt"])
 
    hdata[type]["text"]=ROOT.TLatex(opts["tx"],samples[type]["ypos"],samples[type]["msg"])
    hdata[type]["text"].SetTextColor(samples[type]["color"])
    hdata[type]["text"].Draw("same")

  msg1=ROOT.TLatex(opts["tx"], opts["ymsg"], opts["msg1"])
  msg1.Draw("same")

  c1.Print(figname+".png")
  c1.Print(figname+".C")
  c1.Print(figname+".pdf")

