#!/bin/bash
# A script run Whizard jobs
# 



newpath=`echo $PATH | sed -e "s|/usr/local/texlive/p2011|/group/ilc/soft/utils64/texlive/p2011|"`
export PATH=${newpath}

sindir=`pwd`/sindarin
aliases="${sindir}/aliases.sin"
pythia6_parameters="${sindir}/pythia6-parameters.sin"
beam_events_file_temp="/group/ilc/soft/gcc481/GP/beamdata/lumiee%ECM%-hs/lumiee%ECM%-run1to20.dat"
# n_events=100000
n_events=10000

# ####################################
# Whizard run of one process
# ####################################
run_whizard() 
{
  proc=$1
  jobdir=jobs/${proc}
  rm -rf ${jobdir}
  mkdir -p ${jobdir}
  ecm=`echo ${proc} | cut -d"." -f1 | sed -e "s/E//" `
  beam_events_file=`echo ${beam_events_file_temp} | sed -e "s/%ECM%/${ecm}/g" `
  echo "beam_events_file is ${beam_events_file}"
  ( 
    cd ${jobdir}
    sed -e "s|%%ALIASES%%|${aliases}|g" \
	-e "s|%%PYTHIA6_PARAMETERS%%|${pythia6_parameters}|g" \
        -e "s|%%BEAM_EVENTS_FILE%%|${beam_events_file}|g" \
        -e "s|%%N_EVENTS%%|${n_events}|g" \
        ${sindir}/${proc}.sin > ${proc}.sin
      /usr/bin/time whizard -r ${proc}.sin > run_whizard.log 2>&1  
  )
}

# #######################################
# Loop over many processes and Run many runs 
# #######################################
#

while read f ; do 
  echo "Running whizard for $f "
  run_whizard $f 
done <<EOF
E250.P2f_z_h.eL.pR  
E250.P4f_ww_h.eL.pR  
E500.P2f_z_h.eL.pR  
E500.P4f_ww_h.eL.pR
EOF
