# =============================================================================
# 
#  Analize 2f data at 250 GeV and 500 GeV and create pictures
#  comparing DBD samples and Whizard2 samples.
#
#  Author: Akiya Miyamoto, 30-March-2016
#
# =============================================================================
from pyLCIO.io import LcioReader, StdHepReader
import ROOT
ROOT.gROOT.SetBatch()
import sys
import os.path as path
import math
import commands

import MyPlots

# cs5mrad=math.cos(0.005)
cs5mrad=100.0

# =============================================================================
def AnaData(anatype, filename, ecm, maxread=10000 ) :
  print "Analizing "+filename

#   maxread=10000

  fullpath=filename
  reader =  StdHepReader.StdHepReader(fullpath)

  rfile=ROOT.TFile(anatype+".root","recreate")
  nt = ROOT.TNtuple("nt","NT","nch:ng:nnh:evis:pt:ech:eg:enh")
  ntg = ROOT.TNtuple("ntg","NTGamma","e:cs")

  nout=0

  for idx, event in enumerate(reader): 
    if idx >= maxread : 
      break
    nch=int(0)
    ng=int(0)
    nnh=int(0)
    evis=0
    ech=0
    eg=0
    enh=0
  
    mcParticles = event.getCollection("MCParticle")
    psum=ROOT.TLorentzVector(0.0, 0.0, 0.0, 0.0)
    for p in mcParticles:
      pdg=p.getPDG()
      apdg = abs(pdg)
      v = MyPlots.convertToTLorentzVector(p)
      istat=p.getGeneratorStatus()
      csv=v.Vect().CosTheta()

      if ( istat == 1 and apdg != 12 and apdg != 14 and apdg != 16 and abs(csv) < cs5mrad) :
        psum = psum + v
        ene = p.getEnergy()
        chg = p.getCharge()
        if ( chg < -0.5 or chg > 0.5 ) :
          nch = nch+1
          ech = ech + ene
        elif ( apdg == 22 ) :
          ng = ng + 1
          eg = eg + ene
          cs = v.Vect().CosTheta()
          ntg.Fill(ene, cs)

        else :
          nnh = nnh + 1
          enh = enh + ene


    evis = psum.E()
    pt = psum.Pt()
    nt.Fill(float(nch), float(ng), float(nnh), evis, pt, ech, eg, enh )

# === end of event loop

  rfile.Write()

# === end of AnaData


# =====================================================
# Create histogram to compare distribution
# =====================================================
def Comp_Plot(files, var, opts, figname, ntname="nt") :
  onp={}
  onp["c1"+var]=ROOT.TCanvas("c1"+var)
  onp["hf"+var]=ROOT.TH2F("hf"+var,opts["title"], opts["nbx"], opts["xmin"], opts["xmax"], \
                    opts["nby"], opts["ymin"], opts["ymax"]) 
  c1=onp["c1"+var]
  hf=onp["hf"+var]
  hf.SetStats(0)
  c1.SetGridx(1)
  c1.SetGridy(1)

  
  rf1=ROOT.TFile(files["1"]["file"])
  nt1=rf1.Get("nt")
  nt1.SetLineColor(files["1"]["color"])
  rf2=ROOT.TFile(files["2"]["file"])
  nt2=rf2.Get("nt")
  nt2.SetLineColor(files["2"]["color"])
  
  hf.Draw()
  nt1.Draw(var,"",files["1"]["drawopt"])
  hnt1=nt1.GetHistogram()
  hnt1max=hnt1.GetBinContent(hnt1.GetMaximumBin())
  nt2.Draw(var,"",files["2"]["drawopt"])
  hnt2=nt2.GetHistogram()
  hnt2max=hnt2.GetBinContent(hnt2.GetMaximumBin())
  hfmax=hnt1max
  if hfmax < hnt2max : 
    hfmax=hnt2max
  hfmax=(int(1.1*hfmax/10)+1)*10
  maxbin=hnt1.GetMaximumBin()
  txpos=0.15
  if maxbin < int(0.5*float(opts["nbx"])) : 
    txpos=0.55

  onp["hf2"+var]=ROOT.TH2F("hf2"+var,opts["title"], opts["nbx"], opts["xmin"], opts["xmax"], \
                    opts["nby"], opts["ymin"], hfmax) 
  hf2=onp["hf2"+var]
  
  c1.Clear()
  hf2.SetStats(0)
  hf2.GetXaxis().SetTitle(opts["xlabel"])
  hf2.Draw()
  nt1.Draw(var,"",files["1"]["drawopt"])
  nt2.Draw(var,"",files["2"]["drawopt"])

  notes=opts["notes"]
  textsize=0.035
  lnotes=len(notes)
  ypos=0.87
  dy=0.05
  txs={}
  for l in range(0, lnotes) :
    ypos=ypos-dy
    if l == 2 :
      ypos=ypos-dy
    txs[str(l)]=ROOT.TLatex(txpos, ypos, notes[l])
    tl1=txs[str(l)]
    tl1.SetNDC(True)
    if l < 2 :
      tl1.SetTextColor(files[str(l+1)]["color"])
    tl1.SetTextSize(textsize)
    tl1.Draw("same")

  c1.Print(figname+".png")
  c1.Print(figname+".C")
  c1.Print(figname+".pdf")

# Do_AnaData()
# ====================================================================================
def Do_CompPlot(ecm, msg1, figdir): 

  outdir=figdir

  myplots = MyPlots.MyPlots()
  myplots.AddFile("dbd-4f-"+ecm+".root",1,"same")
  myplots.AddFile("w2-4f-"+ecm+".root",2,"errsame")

  notes=["Hist: DBD"+msg1, "Cross: Whizard2 (M>10GeV)", "ISR_recoil on","No acceptance cut"]
  myplots.SetNotes(notes)

  title="4f_ww_h @ {0} GeV,".format(ecm)+" #alpha_{s}=0"

  nch_opts={"title":"# Charged: "+title, \
	  "nbx":50,"xmin":0.0,"xmax":100.0,"nby":50,"ymin":0,"ymax":1200.0,"xlabel":"# Charged tracks"}
  myplots.Plot("nch", nch_opts, outdir+ecm+"-nch" )

  ng_opts={"title":"# Gamma: "+title, \
	  "nbx":50,"xmin":0.0,"xmax":100.0,"nby":50,"ymin":0,"ymax":1000.0,"xlabel":"# gammas"}
  myplots.Plot("ng", ng_opts, outdir+ecm+"-ng")

  nnh_opts={"title":"# Neutral hadrons: "+title, \
	  "nbx":20,"xmin":0.0,"xmax":20.0,"nby":50,"ymin":0,"ymax":3000.0,"xlabel":"# neutral hadrons"}
  myplots.Plot("nnh", nnh_opts, outdir+ecm+"-nnh")

  scale=float(ecm)/250.0
  evis_opts={"title":"Visible energy: "+title, \
	  "nbx":50,"xmin":150*scale,"xmax":300*scale,"nby":50,"ymin":0,"ymax":7000.0,"xlabel":"Visible Energy (GeV)"}
  myplots.Plot("evis", evis_opts, outdir+ecm+"-evis")

  ech_opts={"title":"Charged track energy: "+title, \
	  "nbx":50,"xmin":0.0,"xmax":300.0*scale,"nby":50,"ymin":0,"ymax":1000.0,"xlabel":"Charged track Energy (GeV)"}
  myplots.Plot("ech", ech_opts, outdir+ecm+"-ech")

  eg_opts={"title":"Gamma energy: "+title, \
	  "nbx":50,"xmin":0.0,"xmax":300.0*scale,"nby":50,"ymin":0,"ymax":2000.0,"xlabel":"Gamma Energy (GeV)"}
  myplots.Plot("eg", eg_opts, outdir+ecm+"-eg")

  enh_opts={"title":"Neutral hadron energy: "+title, \
	  "nbx":50,"xmin":0.0,"xmax":150.0*scale,"nby":50,"ymin":0,"ymax":2000.0,"xlabel":"Neutral hadron Energy (GeV)"}
  myplots.Plot("enh", enh_opts, outdir+ecm+"-enh")

  ans=commands.getoutput("cd "+outdir+" && pdftk "+ecm+"-*.pdf cat output "+ecm+"all.pdf")
  print ans

# =====================================
# Main part for analysis and plots
# =====================================

figdir="4f-figure/"
ans=commands.getoutput("rm -rf "+figdir+" && mkdir "+figdir)

# =====================================================

AnaData ("dbd-4f-250", "/hsm/ilc/grid/storm/prod/ilc/mc-dbd/generated/250-TDR_ws/4f/E250-TDR_ws.P4f_ww_h.Gwhizard-1_95.eL.pR.I106551.001.stdhep", 250)
AnaData ("w2-4f-250", "../examples/jobs/E250.P4f_ww_h.eL.pR/E250.P4f_ww_h.Gwhizard2.eL.pR.ev4.hep", 250)

AnaData ("dbd-4f-500", "/hsm/ilc/grid/storm/prod/ilc/mc-dbd/generated/500-TDR_ws/4f/E500-TDR_ws.P4f_ww_h.Gwhizard-1_95.eL.pR.I250006.001.stdhep", 500)
AnaData ("w2-4f-500", "../examples/jobs/E500.P4f_ww_h.eL.pR/E500.P4f_ww_h.Gwhizard2.eL.pR.ev4.hep", 500)



# ====================================================

Do_CompPlot("250","DBD(I106551)",figdir)
Do_CompPlot("500","DBD(I250006)",figdir)

