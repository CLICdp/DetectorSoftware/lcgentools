#  Analize 2f data at 250 GeV and 500 GeV and create pictures 
#  comparing DBD samples and Whizard2 samples. 
#
#  Author: Akiya Miyamoto, 30-March-2016
# 
#
from pyLCIO.io import LcioReader, StdHepReader
import ROOT
ROOT.gROOT.SetBatch()
import sys
import os.path as path
import math
import commands

import MyPlots

cs5mrad=math.cos(0.005)
cs5mrad=100.0  # Set values less than 1.0 to cut very forward particles.

# =============================================================================
def AnaData(anatype, filename, ecm, maxread=10000 ) :
  print "Analizing "+filename

#   maxread=10000

  fullpath=filename
  reader =  StdHepReader.StdHepReader(fullpath)

  rfile=ROOT.TFile(anatype+".root","recreate")
  nt = ROOT.TNtuple("nt","NT","nch:ng:nnh:evis:pt:ech:eg:enh")
  ntg = ROOT.TNtuple("ntg","NTGamma","e:cs")

  nout=0

  for idx, event in enumerate(reader): 
    if idx >= maxread : 
      break
    nch=int(0)
    ng=int(0)
    nnh=int(0)
    evis=0
    ech=0
    eg=0
    enh=0
  
    mcParticles = event.getCollection("MCParticle")
    psum=ROOT.TLorentzVector(0.0, 0.0, 0.0, 0.0)
    for p in mcParticles:
      pdg=p.getPDG()
      apdg = abs(pdg)
      v = MyPlots.convertToTLorentzVector(p)
      istat=p.getGeneratorStatus()
      csv=v.Vect().CosTheta()

      if ( istat == 1 and apdg != 12 and apdg != 14 and apdg != 16 and abs(csv) < cs5mrad) :
        psum = psum + v
        ene = p.getEnergy()
        chg = p.getCharge()
        if ( chg < -0.5 or chg > 0.5 ) :
          nch = nch+1
          ech = ech + ene
        elif ( apdg == 22 ) :
          ng = ng + 1
          eg = eg + ene
          cs = v.Vect().CosTheta()
          ntg.Fill(ene, cs)

        else :
          nnh = nnh + 1
          enh = enh + ene


    evis = psum.E()
    pt = psum.Pt()
    nt.Fill(float(nch), float(ng), float(nnh), evis, pt, ech, eg, enh )

# === end of event loop

  rfile.Write()

# === end of AnaData

# ====================================================================================
def Do_CompPlot(ecm, msg1, figdir): 


  notes=["Hist: DBD"+msg1, "Cross: Whizard2 (M>10GeV)", "ISR_recoil on","No acceptance cut"]
  outdir=figdir
  myplots = MyPlots.MyPlots()   # For 1D plots
  my2dplots = MyPlots.MyPlots() # For 2D plots

  myplots.AddFile("dbd-2f-"+ecm+".root",1,"same")
  myplots.AddFile("w2-2f-"+ecm+".root",2,"errsame")

  my2dplots.AddFile("dbd-2f-"+ecm+".root",1,"same")
  my2dplots.AddFile("w2-2f-"+ecm+".root",2,"same")
  
  myplots.SetNotes(notes)
  my2dplots.SetNotes(notes)

  title="2f_z_h @ {0} GeV,".format(ecm)+" #alpha_{s}=0"

  nch_opts={"title":"# Charged: "+title, "xlabel":"# Charged tracks", \
       "nbx":50,"xmin":0.0,"xmax":100.0,"nby":50,"ymin":0,"ymax":1200.0}
  myplots.Plot("nch", nch_opts, outdir+ecm+"-nch" )

  ng_opts={"title":"# Gamma: "+title, \
	  "nbx":50,"xmin":0.0,"xmax":100.0,"nby":50,"ymin":0,"ymax":1000.0,"xlabel":"# gammas"}
  myplots.Plot("ng", ng_opts, outdir+ecm+"-ng")

  nnh_opts={"title":"# Neutral hadrons: "+title, \
	  "nbx":20,"xmin":0.0,"xmax":20.0,"nby":50,"ymin":0,"ymax":3000.0,"xlabel":"# neutral hadrons"}
  myplots.Plot("nnh", nnh_opts, outdir+ecm+"-nnh")

  evismax=300.0/250.0*float(ecm)
  evis_opts={"title":"Visible energy: "+title, \
	  "nbx":50,"xmin":0.0,"xmax":evismax,"nby":50,"ymin":0,"ymax":5000.0,"xlabel":"Visible Energy (GeV)" }
  myplots.Plot("evis", evis_opts, outdir+ecm+"-evis")

  enemax=300.0/250.0*float(ecm)
  ech_opts={"title":"Charged track total energy: "+title, \
	  "nbx":50,"xmin":0.0,"xmax":enemax,"nby":50,"ymin":0,"ymax":1000.0,"xlabel":"Charged track Energy (GeV)" }
  myplots.Plot("ech", ech_opts, outdir+ecm+"-ech")

  eg_opts={"title":"Gamma total energy: "+title, \
	  "nbx":50,"xmin":0.0,"xmax":enemax,"nby":50,"ymin":0,"ymax":2000.0,"xlabel":"Gamma Energy (GeV)"}
  myplots.Plot("eg", eg_opts, outdir+ecm+"-eg")

  enh_opts={"title":"Neutral hadron total energy: "+title, \
	  "nbx":50,"xmin":0.0,"xmax":enemax/2,"nby":50,"ymin":0,"ymax":4000.0,"xlabel":"Neutral hadron Energy (GeV)"}
  myplots.Plot("enh", enh_opts, outdir+ecm+"-enh")

  gamcs_opts={"title":"Angular distribution of #gamma : "+title, \
          "nbx":50,"xmin":-1.0,"xmax":1.0,"nby":50,"ymin":0,"ymax":4000.0,"xlabel":"cos#theta_{#gamma} (GeV)"}
  myplots.Plot( "cs", gamcs_opts, outdir+ecm+"-csg",ntname="ntg")

  egcut=float(ecm)/5.0
  gamcscut_opts={"title":"Angular distribution of #gamma(E_{#gamma}>"+str(egcut)+" GeV):"+title, \
          "nbx":50,"xmin":-1.0,"xmax":1.0,"nby":50,"ymin":1,"ymax":4000.0,"xlabel":"cos#theta_{#gamma} (GeV)"}
  myplots.Plot("cs", gamcscut_opts, outdir+ecm+"-csgcut",ntname="ntg",cut="e > "+str(egcut), logscale=1)

  halfecm=150.0/250.0*float(ecm)
  gamene_opts={"title":"#gamma energy: "+title, \
          "nbx":50,"xmin":0.0,"xmax":halfecm,"nby":50,"ymin":1,"ymax":4000.0,"xlabel":"E_{#gamma} (GeV)"}
  myplots.Plot("e", gamene_opts, outdir+ecm+"-gamene",ntname="ntg",logscale=1)

  halfecm=150.0/250.0*float(ecm)
  gamenecs_opts={"title":"#gamma energy vs cos: "+title, "nbx":100,"xmin":-1.0,"xmax":1.0,"nby":100,"ymin":0,"ymax":halfecm,"xlabel":"E_{#gamma} (GeV)"}
  my2dplots.text_xpos_left=0.35
  my2dplots.text_xpos_right=0.35
  my2dplots.text_ypos=0.6
  my2dplots.printformat=["png"]
  my2dplots.Plot("e:cs", gamenecs_opts, outdir+ecm+"-gamenecs",ntname="ntg", autoscale="off")

  ans=commands.getoutput("cd "+outdir+" && convert "+ecm+"-gamenecs.png "+ecm+"-gamenecs.pdf")
  ans=commands.getoutput("cd "+outdir+" && pdftk "+ecm+"-*.pdf cat output "+ecm+"all.pdf")
  print ans

# =====================================
# Main part
# =====================================

# Analize data and root ntuple files
AnaData ("w2-2f-250", "../examples/jobs/E250.P2f_z_h.eL.pR/E250.P2f_z_h.Gwhizard2.eL.pR.ev4.hep", 250)
AnaData ("dbd-2f-250", "/hsm/ilc/grid/storm/prod/ilc/mc-dbd/generated/250-TDR_ws/2f/E250-TDR_ws.P2f_z_h.Gwhizard-1_95.eL.pR.I106607.001.stdhep", 250)

AnaData ("w2-2f-500", "../examples/jobs/E500.P2f_z_h.eL.pR/E500.P2f_z_h.Gwhizard2.eL.pR.ev4.hep", 500)
AnaData ("dbd-2f-500", "/hsm/ilc/grid/storm/prod/ilc/mc-dbd/generated/500-TDR_ws/2f/E500-TDR_ws.P2f_z_h.Gwhizard-1_95.eL.pR.I250114.001.stdhep", 500)


#  Draw figures from root ntuple file.
figdir="2f-figure/"
ans=commands.getoutput("rm -rf "+figdir+" && mkdir "+figdir)

Do_CompPlot("250","DBD(I106607)", figdir)
Do_CompPlot("500","DBD(I250114)", figdir)




