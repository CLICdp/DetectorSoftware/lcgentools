#######
# plot_recoilMass
# Usage: Generate the mumuh events in the Circe1 and Circe2 directories
# download the DBD files from 
# /ilc/prod/ilc/mc-dbd/generated/250-TDR_ws/higgs/E250-TDR_ws.Pe2e2h.Gwhizard-1_95.eL.pR.I106479.001.stdhep
# /ilc/prod/ilc/mc-dbd/generated/250-TDR_ws/higgs/E250-TDR_ws.Pe2e2h.Gwhizard-1_95.eR.pL.I106480.001.stdhep
#######
import plotSampleComparison
import ROOT
import sys
from pyLCIO.io import StdHepReader
from pyLCIO import IOIMPL

def convertToTLorentzVector(particle):
    p = particle.getMomentum()
    E = particle.getEnergy()
    return ROOT.TLorentzVector(p[0], p[1], p[2], E)

def recoilMassHist(filename, sqrts):
    """ 
    Looks for two muons in the event. 
    Takes the first two, assuming they are from the Z.
    Computes the recoil mass 
    """
    hist = ROOT.TH1D("recoilMass", "Recoil Mass", 100,115,150)
    if filename.endswith("stdhep"):
        reader = StdHepReader.StdHepReader()
    elif filename.endswith("lcio"):
        reader = IOIMPL.LCFactory.getInstance().createLCReader()
    reader.open(filename)
    for event in reader:
        muonList = []
        for particle in event.getCollection("MCParticle"):
            if abs(particle.getPDG()) != 13:
                continue
            muonList.append(convertToTLorentzVector(particle))
        Z = (muonList[0]+muonList[1])
        energy = Z.E()
        mass = Z.M()
        recoil = sqrt(sqrts*sqrts+mass*mass - 2*energy*sqrts)
        hist.Fill(recoil)
    reader.close()
    return hist

c = ROOT.TCanvas()
h = recoilMassHist("E250-TDR_ws.Pe2e2h.Gwhizard-1_95.eL.pR.I106479.001.stdhep", 250)
h.SetName("Whizard1")
h.SetTitle("Whizard1")
h.Scale(1./h.GetEntries())
h.Draw()
hists = []
hists.append(h)
for filename in "Circe1", "Circe2":
    h = recoilMassHist("SindarinFiles/%s/E250-TDR_ws.Pmumuh.Gwhizard-2_11.eL.pR/eemumuh.slcio" % filename, 250)
    h.SetName(filename)
    h.SetTitle(filename)
    h.Scale(1/h.GetEntries())
    h.Draw("same")
    hists.append(h)
c.SaveAs("recoilMass.root")
