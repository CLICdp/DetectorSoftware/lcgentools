#!/usr/bin/env python

# argvs = sys.argv 
# argc = len(argvs)
# print argvs
# print argc

import sys
import math
import commands
import random

#
argvs = sys.argv
argc = len(argvs)

# print argvs
# print argc

if ( argc != 3 ) : 
  print "Usage: python mklumidata.py beam-energy bunch-number"
  print "beam-energy : 250 or 500 "
  print "bunch number: 1 to max bunch number"
  quit()

bmenergy=argvs[1]
runno = argvs[2]

# print bmenergy
# print runno
datadir="/afs/desy.de/group/flc/pool/analysis/public/pairs/hartin/Waisty_opt_Jan2012"

if ( bmenergy == "500" ) : 
  targz = datadir+"/500GeV/Waisty_opt_Jan2012_500GeV_run"+str(runno)+"_waisty_250.tgz"
  input = "temp/lumi_Waisty_opt_Jan2012_500GeV_run"+str(runno)+".dat"
elif ( bmenergy == "1000" ) : 
  targz = datadir+"/1000GeV/Waisty_opt_Jan2012_1000GeV_B1b_run"+str(runno)+"_waisty_190.tgz"
  input = "temp/lumi_Waisty_opt_Jan2012_1000GeV_B1b_run"+str(runno)+".dat"
elif ( bmenergy == "350" ) : 
  targz = datadir+"/350GeV/Waisty_opt_Jan2012_350GeV_run"+str(runno)+"_waisty_250.tgz"
  input = "temp/lumi_Waisty_opt_Jan2012_350GeV_run"+str(runno)+".dat"
elif ( bmenergy == "250" ) : 
  targz = datadir+"/250GeV/Waisty_opt_Jan2012_250GeV_run"+str(runno)+"_waisty_250.tgz"
  input = "temp/lumi_Waisty_opt_Jan2012_250GeV_run"+str(runno)+".dat"
else :
  print "Error: unkown beam energy "+str(bmenergy)+" was specified."
  quit()

print "creating lumidata for "+str(bmenergy)+" GeV  run"+str(runno)

enomi = float(bmenergy)/2.0
lumitype="lumiee"
ans=commands.getoutput("mkdir -p temp && cd temp && tar zxf "+targz)
# print ans
ans=commands.getoutput("mkdir -p "+lumitype+str(bmenergy))
# print ans
wl=int(commands.getoutput("cat "+input+" | wc -l" ))
# print wl
e1=[0]*wl
e2=[0]*wl

frd=open(input,"r")
fwt=open(lumitype+str(bmenergy)+"/"+lumitype+str(bmenergy)+"-run"+str(runno)+".dat","w")
ir=0
for line in frd:
#  print line
  val=line.split()
  e1[ir]=float(val[0])/enomi
  e2[ir]=float(val[1])/enomi
  ir=ir+1
  
asize=wl
for no in range(0,wl) :
  apnt=int(random.random()*float(asize))
  if apnt > asize - 1 :
    apnt = asize - 1
  fwt.write(str(e1[apnt])+" "+str(e2[apnt])+"\n")
  e1[apnt]=e1[asize-1]
  e2[apnt]=e2[asize-1]
  asize=asize-1  

frd.close()
fwt.close()

ans=commands.getoutput("rm -r temp")
