# WHIZARD configuration file

# The selected model (O'Mega)
model	SM

# Processes for test8
# (Methods: chep=CompHEP, mad=MadGraph, omega=O'Mega)
# (Options: s=selected diagrams, number=QCD order [Madgraph])
#           f=fudged width [O'Mega]
#
# Tag    	In      Out     	Method	Option
#=====================================================
#########################################################################
#

############################################################
# 2-fermion production with 1 higgs boson
#
# 

n1n1h_o    	e1,E1	n1,N1,H                     	omega	f
n2n2h_o    	e1,E1	n2,N2,H                     	omega	f
n3n3h_o    	e1,E1	n3,N3,H                     	omega	f
uuh_o    	e1,E1	u,U,H                    	omega	f
cch_o    	e1,E1	c,C,H                     	omega	f
e1e1h_o  	e1,E1	e1,E1,H                    	omega	f
e2e2h_o  	e1,E1	e2,E2,H                    	omega	f
e3e3h_o  	e1,E1	e3,E3,H                    	omega	f
ddh_o    	e1,E1	d,D,H                     	omega	f
ssh_o    	e1,E1	s,S,H                     	omega	f
bbh_o    	e1,E1	b,B,H                     	omega	f
