# WHIZARD configuration file


# The selected model (O'Mega)
model	SM

# Processes 0-2-4-fermion
# (Methods: chep=CompHEP, mad=MadGraph, omega=O'Mega)
# (Options: s=selected diagrams, number=QCD order [Madgraph])
#           f=fudged width [O'Mega]
#
# Tag    	In      Out     	Method	Option
#=====================================================
#########################################################################

#
# 4-fermion production
# type \gamma\gamma or e^- \gamma or \gamma e^+ --> ...
#

aa_e1e1_o  	A,A	e1,E1                    	omega	f
aa_e2e2_o  	A,A	e2,E2                    	omega	f
aa_e3e3_o  	A,A	e3,E3                    	omega	f
aa_uu_o  	A,A	u,U                     	omega	f
aa_dd_o  	A,A	d,D                     	omega	f
aa_ss_o  	A,A	s,S                     	omega	f
aa_cc_o  	A,A	c,C                     	omega	f
aa_bb_o  	A,A	b,B                     	omega	f

e1a_n1e1n1_o  	e1,A	n1,e1,N1                 	omega	f
e1a_n1e2n2_o  	e1,A	n1,e2,N2                 	omega	f
e1a_n1e3n3_o  	e1,A	n1,e3,N3                 	omega	f
e1a_n1du_o  	e1,A	n1,d,U                  	omega	f
e1a_n1sc_o  	e1,A	n1,s,C                  	omega	f

e1a_e1n2n2_o  	e1,A	e1,n2,N2                 	omega	f
e1a_e1n3n3_o  	e1,A	e1,n3,N3                 	omega	f
e1a_e1uu_o  	e1,A	e1,u,U                  	omega	f
e1a_e1cc_o  	e1,A	e1,c,C                  	omega	f
e1a_e1e1e1_o  	e1,A	e1,e1,E1                 	omega	f
e1a_e1e2e2_o  	e1,A	e1,e2,E2                 	omega	f
e1a_e1e3e3_o  	e1,A	e1,e3,E3                 	omega	f
e1a_e1dd_o  	e1,A	e1,d,D                  	omega	f
e1a_e1ss_o  	e1,A	e1,s,S                  	omega	f
e1a_e1bb_o  	e1,A	e1,b,B                  	omega	f

ae1_n1n1e1_o  	A,E1	N1,n1,E1                 	omega	f
ae1_n1n2e2_o  	A,E1	N1,n2,E2                 	omega	f
ae1_n1n3e3_o  	A,E1	N1,n3,E3                 	omega	f
ae1_n1ud_o  	A,E1	N1,u,D                  	omega	f
ae1_n1cs_o  	A,E1	N1,c,S                  	omega	f

ae1_e1n2n2_o  	A,E1	E1,n2,N2                 	omega	f
ae1_e1n3n3_o  	A,E1	E1,n3,N3                 	omega	f
ae1_e1uu_o  	A,E1	E1,u,U                  	omega	f
ae1_e1cc_o  	A,E1	E1,c,C                  	omega	f
ae1_e1e1e1_o  	A,E1	E1,e1,E1                 	omega	f
ae1_e1e2e2_o  	A,E1	E1,e2,E2                 	omega	f
ae1_e1e3e3_o  	A,E1	E1,e3,E3                 	omega	f
ae1_e1dd_o  	A,E1	E1,d,D                  	omega	f
ae1_e1ss_o  	A,E1	E1,s,S                  	omega	f
ae1_e1bb_o  	A,E1	E1,b,B                  	omega	f


