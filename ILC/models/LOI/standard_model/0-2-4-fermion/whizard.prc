# WHIZARD configuration file


# The selected model (O'Mega)
model	SM

# Processes 0-2-4-fermion
# (Methods: chep=CompHEP, mad=MadGraph, omega=O'Mega)
# (Options: s=selected diagrams, number=QCD order [Madgraph])
#           f=fudged width [O'Mega]
#
# Tag    	In      Out     	Method	Option
#=====================================================
#########################################################################

#
############################################################
# 0-fermion production with photons
aa_o		e1,E1	A,A	    	omega	f
aaa_o		e1,E1	A,A,A	    	omega	f
aaaa_o		e1,E1	A,A,A,A	    	omega	f
aaaaa_o		e1,E1	A,A,A,A,A	omega	f

#

#
############################################################
# 1-fermion production with photon
e1a_e1a_o  	e1,A	e1,A                     	omega	f
ae1_e1a_o  	A,E1	E1,A                     	omega	f

#
############################################################
# 2-neutrino production with photons
#
# 

n1n1a_o    	e1,E1	n1,N1,A                     	omega	f
n2n2a_o    	e1,E1	n2,N2,A                     	omega	f
n3n3a_o    	e1,E1	n3,N3,A                     	omega	f

n1n1aa_o    	e1,E1	n1,N1,A,A                     	omega	f
n2n2aa_o    	e1,E1	n2,N2,A,A                     	omega	f
n3n3aa_o    	e1,E1	n3,N3,A,A                     	omega	f

n1n1aaa_o    	e1,E1	n1,N1,A,A,A                   	omega	f
n2n2aaa_o    	e1,E1	n2,N2,A,A,A                   	omega	f
n3n3aaa_o    	e1,E1	n3,N3,A,A,A                    	omega	f

#
############################################################
# 2-fermion production
#
# 

uu_o    	e1,E1	u,U                     	omega	f
cc_o    	e1,E1	c,C                     	omega	f
e1e1_o  	e1,E1	e1,E1                    	omega	f
e2e2_o  	e1,E1	e2,E2                    	omega	f
e3e3_o  	e1,E1	e3,E3                    	omega	f
dd_o    	e1,E1	d,D                     	omega	f
ss_o    	e1,E1	s,S                     	omega	f
bb_o    	e1,E1	b,B                     	omega	f

#
############################################################
# 4-neutrino production with a photon
#

n1n1n1n1a_o	e1,E1	n1,N1,n1,N1,A               	omega	f
n1n1n2n2a_o	e1,E1	n1,N1,n2,N2,A               	omega	f
n1n1n3n3a_o	e1,E1	n1,N1,n3,N3,A               	omega	f
n2n2n2n2a_o	e1,E1	n2,N2,n2,N2,A               	omega	f
n2n2n3n3a_o	e1,E1	n2,N2,n3,N3,A               	omega	f
n3n3n3n3a_o	e1,E1	n3,N3,n3,N3,A               	omega	f


#
############################################################
# 4-fermion production
# Compatible with W+W-  type u_j \bar{d}_j d_k \bar{u}_k

n1e1e1n1_o	e1,E1	n1,E1,e1,N1             	omega	f
n1e1e2n2_o	e1,E1	n1,E1,e2,N2             	omega	f
n1e1e3n3_o	e1,E1	n1,E1,e3,N3             	omega	f
n1e1du_o	e1,E1	n1,E1,d,U                	omega	f
n1e1sc_o	e1,E1	n1,E1,s,C               	omega	f

n2e2e1n1_o	e1,E1	n2,E2,e1,N1             	omega	f
n2e2e2n2_o	e1,E1	n2,E2,e2,N2             	omega	f
n2e2e3n3_o	e1,E1	n2,E2,e3,N3             	omega	f
n2e2du_o	e1,E1	n2,E2,d,U               	omega	f
n2e2sc_o	e1,E1	n2,E2,s,C               	omega	f

n3e3e1n1_o	e1,E1	n3,E3,e1,N1             	omega	f
n3e3e2n2_o	e1,E1	n3,E3,e2,N2             	omega	f
n3e3e3n3_o	e1,E1	n3,E3,e3,N3             	omega	f
n3e3du_o	e1,E1	n3,E3,d,U               	omega	f
n3e3sc_o	e1,E1	n3,E3,s,C               	omega	f

ude1n1_o	e1,E1	u,D,e1,N1               	omega	f
ude2n2_o	e1,E1	u,D,e2,N2               	omega	f
ude3n3_o	e1,E1	u,D,e3,N3               	omega	f
uddu_o  	e1,E1	u,D,d,U                 	omega	f
udsc_o	        e1,E1	u,D,s,C                  	omega	f

cse1n1_o	e1,E1	c,S,e1,N1                       omega	f
cse2n2_o	e1,E1	c,S,e2,N2                	omega	f
cse3n3_o	e1,E1	c,S,e3,N3               	omega	f
csdu_o	        e1,E1	c,S,d,U                 	omega	f
cssc_o	        e1,E1	c,S,s,C                  	omega	f


# 4-fermion production
# Compatible with ZZ  type u_j \bar{u}_j u_k \bar{u}_k
#

n1n1uu_o	e1,E1	n1,N1,u,U               	omega	f
n1n1cc_o	e1,E1	n1,N1,c,C               	omega	f
n2n2uu_o	e1,E1	n2,N2,u,U               	omega	f
n2n2cc_o	e1,E1	n2,N2,c,C               	omega	f
n3n3uu_o	e1,E1	n3,N3,u,U               	omega	f
n3n3cc_o	e1,E1	n3,N3,c,C               	omega	f
uuuu_o  	e1,E1	u,U,u,U                 	omega	f
uucc_o  	e1,E1	u,U,c,C                 	omega	f
cccc_o  	e1,E1	c,C,c,C                 	omega	f


# 4-fermion production
# Compatible with ZZ  type u_j \bar{u}_j d_k \bar{d}_k
#

n1n1e2e2_o	e1,E1	n1,N1,e2,E2             	omega	f
n1n1e3e3_o	e1,E1	n1,N1,e3,E3             	omega	f
n1n1dd_o	e1,E1	n1,N1,d,D               	omega	f
n1n1ss_o	e1,E1	n1,N1,s,S               	omega	f
n1n1bb_o	e1,E1	n1,N1,b,B               	omega	f

n2n2e1e1_o	e1,E1	n2,N2,e1,E1             	omega	f
n2n2e3e3_o	e1,E1	n2,N2,e3,E3             	omega	f
n2n2dd_o	e1,E1	n2,N2,d,D               	omega	f
n2n2ss_o	e1,E1	n2,N2,s,S               	omega	f
n2n2bb_o	e1,E1	n2,N2,b,B               	omega	f

n3n3e1e1_o	e1,E1	n3,N3,e1,E1             	omega	f
n3n3e2e2_o	e1,E1	n3,N3,e2,E2             	omega	f
n3n3dd_o	e1,E1	n3,N3,d,D               	omega	f
n3n3ss_o	e1,E1	n3,N3,s,S               	omega	f
n3n3bb_o	e1,E1	n3,N3,b,B               	omega	f

uue1e1_o	e1,E1	u,U,e1,E1               	omega	f
uue2e2_o	e1,E1	u,U,e2,E2               	omega	f
uue3e3_o	e1,E1	u,U,e3,E3               	omega	f
uuss_o  	e1,E1	u,U,s,S                 	omega	f
uubb_o  	e1,E1	u,U,b,B                 	omega	f

cce1e1_o	e1,E1	c,C,e1,E1               	omega	f
cce2e2_o	e1,E1	c,C,e2,E2               	omega	f
cce3e3_o	e1,E1	c,C,e3,E3               	omega	f
ccdd_o  	e1,E1	c,C,d,D                 	omega	f
ccbb_o  	e1,E1	c,C,b,B                 	omega	f


# 4-fermion production
# Compatible with ZZ  type d_j \bar{d}_j d_k \bar{d}_k
#

e1e1e1e1_o	e1,E1	e1,E1,e1,E1             	omega	f
e1e1e2e2_o	e1,E1	e1,E1,e2,E2             	omega	f
e1e1e3e3_o	e1,E1	e1,E1,e3,E3             	omega	f
e1e1dd_o	e1,E1	e1,E1,d,D               	omega	f
e1e1ss_o	e1,E1	e1,E1,s,S               	omega	f
e1e1bb_o	e1,E1	e1,E1,b,B               	omega	f

e2e2e2e2_o	e1,E1	e2,E2,e2,E2             	omega	f
e2e2e3e3_o	e1,E1	e2,E2,e3,E3             	omega	f
e2e2dd_o	e1,E1	e2,E2,d,D               	omega	f
e2e2ss_o	e1,E1	e2,E2,s,S               	omega	f
e2e2bb_o	e1,E1	e2,E2,b,B               	omega	f

e3e3e3e3_o	e1,E1	e3,E3,e3,E3             	omega	f
e3e3dd_o	e1,E1	e3,E3,d,D               	omega	f
e3e3ss_o	e1,E1	e3,E3,s,S               	omega	f
e3e3bb_o	e1,E1	e3,E3,b,B               	omega	f

dddd_o  	e1,E1	d,D,d,D                  	omega	f
ddss_o  	e1,E1	d,D,s,S                 	omega	f
ddbb_o  	e1,E1	d,D,b,B                 	omega	f

ssss_o  	e1,E1	s,S,s,S                 	omega	f
ssbb_o  	e1,E1	s,S,b,B                 	omega	f

bbbb_o  	e1,E1	b,B,b,B                 	omega	f

#
# 4-fermion production
# type \gamma\gamma or e^- \gamma or \gamma e^+ --> ...
#

aa_e1e1_o  	A,A	e1,E1                    	omega	f
aa_e2e2_o  	A,A	e2,E2                    	omega	f
aa_e3e3_o  	A,A	e3,E3                    	omega	f
aa_uu_o  	A,A	u,U                     	omega	f
aa_dd_o  	A,A	d,D                     	omega	f
aa_ss_o  	A,A	s,S                     	omega	f
aa_cc_o  	A,A	c,C                     	omega	f
aa_bb_o  	A,A	b,B                     	omega	f

e1a_n1e1n1_o  	e1,A	n1,e1,N1                 	omega	f
e1a_n1e2n2_o  	e1,A	n1,e2,N2                 	omega	f
e1a_n1e3n3_o  	e1,A	n1,e3,N3                 	omega	f
e1a_n1du_o  	e1,A	n1,d,U                  	omega	f
e1a_n1sc_o  	e1,A	n1,s,C                  	omega	f

e1a_e1n2n2_o  	e1,A	e1,n2,N2                 	omega	f
e1a_e1n3n3_o  	e1,A	e1,n3,N3                 	omega	f
e1a_e1uu_o  	e1,A	e1,u,U                  	omega	f
e1a_e1cc_o  	e1,A	e1,c,C                  	omega	f
e1a_e1e1e1_o  	e1,A	e1,e1,E1                 	omega	f
e1a_e1e2e2_o  	e1,A	e1,e2,E2                 	omega	f
e1a_e1e3e3_o  	e1,A	e1,e3,E3                 	omega	f
e1a_e1dd_o  	e1,A	e1,d,D                  	omega	f
e1a_e1ss_o  	e1,A	e1,s,S                  	omega	f
e1a_e1bb_o  	e1,A	e1,b,B                  	omega	f

ae1_n1n1e1_o  	A,E1	N1,n1,E1                 	omega	f
ae1_n1n2e2_o  	A,E1	N1,n2,E2                 	omega	f
ae1_n1n3e3_o  	A,E1	N1,n3,E3                 	omega	f
ae1_n1ud_o  	A,E1	N1,u,D                  	omega	f
ae1_n1cs_o  	A,E1	N1,c,S                  	omega	f

ae1_e1n2n2_o  	A,E1	E1,n2,N2                 	omega	f
ae1_e1n3n3_o  	A,E1	E1,n3,N3                 	omega	f
ae1_e1uu_o  	A,E1	E1,u,U                  	omega	f
ae1_e1cc_o  	A,E1	E1,c,C                  	omega	f
ae1_e1e1e1_o  	A,E1	E1,e1,E1                 	omega	f
ae1_e1e2e2_o  	A,E1	E1,e2,E2                 	omega	f
ae1_e1e3e3_o  	A,E1	E1,e3,E3                 	omega	f
ae1_e1dd_o  	A,E1	E1,d,D                  	omega	f
ae1_e1ss_o  	A,E1	E1,s,S                  	omega	f
ae1_e1bb_o  	A,E1	E1,b,B                  	omega	f


