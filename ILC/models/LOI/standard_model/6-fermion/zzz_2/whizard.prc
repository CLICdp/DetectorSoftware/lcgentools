# WHIZARD configuration file

# The selected model (O'Mega)
model	SM

# Processes for 6-fermion/zzz
# (Methods: chep=CompHEP, mad=MadGraph, omega=O'Mega)
# (Options: s=selected diagrams, number=QCD order [Madgraph])
#           f=fudged width [O'Mega]
#
# Tag    	In      Out     	Method	Option
#=====================================================
#########################################################################
# 6-fermion production
# Compatible with ZZZ  type u_i \bar{u}_i d_j \bar{d}_j d_k \bar{d}_k
#

n1n1e2e2e2e2_o	e1,E1	n1,N1,e2,E2,e2,E2             	omega	f
n1n1e2e2e3e3_o	e1,E1	n1,N1,e2,E2,e3,E3             	omega	f
n1n1e2e2dd_o	e1,E1	n1,N1,e2,E2,d,D             	omega	f
n1n1e2e2ss_o	e1,E1	n1,N1,e2,E2,s,S             	omega	f
n1n1e2e2bb_o	e1,E1	n1,N1,e2,E2,b,B             	omega	f
n1n1e3e3e3e3_o	e1,E1	n1,N1,e3,E3,e3,E3             	omega	f
n1n1e3e3dd_o	e1,E1	n1,N1,e3,E3,d,D             	omega	f
n1n1e3e3ss_o	e1,E1	n1,N1,e3,E3,s,S             	omega	f
n1n1e3e3bb_o	e1,E1	n1,N1,e3,E3,b,B             	omega	f
n1n1dddd_o	e1,E1	n1,N1,d,D,d,D             	omega	f
n1n1ddss_o	e1,E1	n1,N1,d,D,s,S             	omega	f
n1n1ddbb_o	e1,E1	n1,N1,d,D,b,B             	omega	f
n1n1ssss_o	e1,E1	n1,N1,s,S,s,S             	omega	f
n1n1ssbb_o	e1,E1	n1,N1,s,S,b,B             	omega	f
n1n1bbbb_o	e1,E1	n1,N1,b,B,b,B             	omega	f

n2n2e1e1e1e1_o	e1,E1	n2,N2,e1,E1,e1,E1             	omega	f
n2n2e1e1e3e3_o	e1,E1	n2,N2,e1,E1,e3,E3             	omega	f
n2n2e1e1dd_o	e1,E1	n2,N2,e1,E1,d,D             	omega	f
n2n2e1e1ss_o	e1,E1	n2,N2,e1,E1,s,S             	omega	f
n2n2e1e1bb_o	e1,E1	n2,N2,e1,E1,b,B             	omega	f
n2n2e3e3e3e3_o	e1,E1	n2,N2,e3,E3,e3,E3             	omega	f
n2n2e3e3dd_o	e1,E1	n2,N2,e3,E3,d,D             	omega	f
n2n2e3e3ss_o	e1,E1	n2,N2,e3,E3,s,S             	omega	f
n2n2e3e3bb_o	e1,E1	n2,N2,e3,E3,b,B             	omega	f
n2n2dddd_o	e1,E1	n2,N2,d,D,d,D             	omega	f
n2n2ddss_o	e1,E1	n2,N2,d,D,s,S             	omega	f
n2n2ddbb_o	e1,E1	n2,N2,d,D,b,B             	omega	f
n2n2ssss_o	e1,E1	n2,N2,s,S,s,S             	omega	f
n2n2ssbb_o	e1,E1	n2,N2,s,S,b,B             	omega	f
n2n2bbbb_o	e1,E1	n2,N2,b,B,b,B             	omega	f

n3n3e1e1e1e1_o	e1,E1	n3,N3,e1,E1,e1,E1             	omega	f
n3n3e1e1e2e2_o	e1,E1	n3,N3,e1,E1,e2,E2             	omega	f
n3n3e1e1dd_o	e1,E1	n3,N3,e1,E1,d,D             	omega	f
n3n3e1e1ss_o	e1,E1	n3,N3,e1,E1,s,S             	omega	f
n3n3e1e1bb_o	e1,E1	n3,N3,e1,E1,b,B             	omega	f
n3n3e2e2e2e2_o	e1,E1	n3,N3,e2,E2,e2,E2             	omega	f
n3n3e2e2dd_o	e1,E1	n3,N3,e2,E2,d,D             	omega	f
n3n3e2e2ss_o	e1,E1	n3,N3,e2,E2,s,S             	omega	f
n3n3e2e2bb_o	e1,E1	n3,N3,e2,E2,b,B             	omega	f
n3n3dddd_o	e1,E1	n3,N3,d,D,d,D             	omega	f
n3n3ddss_o	e1,E1	n3,N3,d,D,s,S             	omega	f
n3n3ddbb_o	e1,E1	n3,N3,d,D,b,B             	omega	f
n3n3ssss_o	e1,E1	n3,N3,s,S,s,S             	omega	f
n3n3ssbb_o	e1,E1	n3,N3,s,S,b,B             	omega	f
n3n3bbbb_o	e1,E1	n3,N3,b,B,b,B             	omega	f

uue1e1e1e1_o	e1,E1	u,U,e1,E1,e1,E1             	omega	f
uue1e1e2e2_o	e1,E1	u,U,e1,E1,e2,E2             	omega	f
uue1e1e3e3_o	e1,E1	u,U,e1,E1,e3,E3             	omega	f
uue1e1ss_o	e1,E1	u,U,e1,E1,s,S             	omega	f
uue1e1bb_o	e1,E1	u,U,e1,E1,b,B             	omega	f
uue2e2e2e2_o	e1,E1	u,U,e2,E2,e2,E2             	omega	f
uue2e2e3e3_o	e1,E1	u,U,e2,E2,e3,E3             	omega	f
uue2e2ss_o	e1,E1	u,U,e2,E2,s,S             	omega	f
uue2e2bb_o	e1,E1	u,U,e2,E2,b,B             	omega	f
uue3e3e3e3_o	e1,E1	u,U,e3,E3,e3,E3             	omega	f
uue3e3ss_o	e1,E1	u,U,e3,E3,s,S             	omega	f
uue3e3bb_o	e1,E1	u,U,e3,E3,b,B             	omega	f
uussss_o	e1,E1	u,U,s,S,s,S             	omega	f
uussbb_o	e1,E1	u,U,s,S,b,B             	omega	f
uubbbb_o	e1,E1	u,U,b,B,b,B             	omega	f

cce1e1e1e1_o	e1,E1	c,C,e1,E1,e1,E1             	omega	f
cce1e1e2e2_o	e1,E1	c,C,e1,E1,e2,E2             	omega	f
cce1e1e3e3_o	e1,E1	c,C,e1,E1,e3,E3             	omega	f
cce1e1dd_o	e1,E1	c,C,e1,E1,d,D             	omega	f
cce1e1bb_o	e1,E1	c,C,e1,E1,b,B             	omega	f
cce2e2e2e2_o	e1,E1	c,C,e2,E2,e2,E2             	omega	f
cce2e2e3e3_o	e1,E1	c,C,e2,E2,e3,E3             	omega	f
cce2e2dd_o	e1,E1	c,C,e2,E2,d,D             	omega	f
cce2e2bb_o	e1,E1	c,C,e2,E2,b,B             	omega	f
cce3e3e3e3_o	e1,E1	c,C,e3,E3,e3,E3             	omega	f
cce3e3dd_o	e1,E1	c,C,e3,E3,d,D             	omega	f
cce3e3bb_o	e1,E1	c,C,e3,E3,b,B             	omega	f
ccdddd_o	e1,E1	c,C,d,D,d,D             	omega	f
ccddbb_o	e1,E1	c,C,d,D,b,B             	omega	f
ccbbbb_o	e1,E1	c,C,b,B,b,B             	omega	f


# 6-fermion production
# Compatible with ZZZ  type d_i \bar{d}_i d_j \bar{d}_j d_k \bar{d}_k
#

e1e1e1e1e1e1_o	e1,E1	e1,E1,e1,E1,e1,E1             	omega	f
e1e1e1e1e2e2_o	e1,E1	e1,E1,e1,E1,e2,E2             	omega	f
e1e1e1e1e3e3_o	e1,E1	e1,E1,e1,E1,e3,E3             	omega	f
e1e1e1e1dd_o	e1,E1	e1,E1,e1,E1,d,D             	omega	f
e1e1e1e1ss_o	e1,E1	e1,E1,e1,E1,s,S             	omega	f
e1e1e1e1bb_o	e1,E1	e1,E1,e1,E1,b,B             	omega	f
e1e1e2e2e2e2_o	e1,E1	e1,E1,e2,E2,e2,E2             	omega	f
e1e1e2e2e3e3_o	e1,E1	e1,E1,e2,E2,e3,E3             	omega	f
e1e1e2e2dd_o	e1,E1	e1,E1,e2,E2,d,D             	omega	f
e1e1e2e2ss_o	e1,E1	e1,E1,e2,E2,s,S             	omega	f
e1e1e2e2bb_o	e1,E1	e1,E1,e2,E2,b,B             	omega	f
e1e1e3e3e3e3_o	e1,E1	e1,E1,e3,E3,e3,E3             	omega	f
e1e1e3e3dd_o	e1,E1	e1,E1,e3,E3,d,D             	omega	f
e1e1e3e3ss_o	e1,E1	e1,E1,e3,E3,s,S             	omega	f
e1e1e3e3bb_o	e1,E1	e1,E1,e3,E3,b,B             	omega	f
e1e1dddd_o	e1,E1	e1,E1,d,D,d,D             	omega	f
e1e1ddss_o	e1,E1	e1,E1,d,D,s,S             	omega	f
e1e1ddbb_o	e1,E1	e1,E1,d,D,b,B             	omega	f
e1e1ssss_o	e1,E1	e1,E1,s,S,s,S             	omega	f
e1e1ssbb_o	e1,E1	e1,E1,s,S,b,B             	omega	f
e1e1bbbb_o	e1,E1	e1,E1,b,B,b,B             	omega	f

e2e2e2e2e2e2_o	e1,E1	e2,E2,e2,E2,e2,E2             	omega	f
e2e2e2e2e3e3_o	e1,E1	e2,E2,e2,E2,e3,E3             	omega	f
e2e2e2e2dd_o	e1,E1	e2,E2,e2,E2,d,D             	omega	f
e2e2e2e2ss_o	e1,E1	e2,E2,e2,E2,s,S             	omega	f
e2e2e2e2bb_o	e1,E1	e2,E2,e2,E2,b,B             	omega	f
e2e2e3e3e3e3_o	e1,E1	e2,E2,e3,E3,e3,E3             	omega	f
e2e2e3e3dd_o	e1,E1	e2,E2,e3,E3,d,D             	omega	f
e2e2e3e3ss_o	e1,E1	e2,E2,e3,E3,s,S             	omega	f
e2e2e3e3bb_o	e1,E1	e2,E2,e3,E3,b,B             	omega	f
e2e2dddd_o	e1,E1	e2,E2,d,D,d,D             	omega	f
e2e2ddss_o	e1,E1	e2,E2,d,D,s,S             	omega	f
e2e2ddbb_o	e1,E1	e2,E2,d,D,b,B             	omega	f
e2e2ssss_o	e1,E1	e2,E2,s,S,s,S             	omega	f
e2e2ssbb_o	e1,E1	e2,E2,s,S,b,B             	omega	f
e2e2bbbb_o	e1,E1	e2,E2,b,B,b,B             	omega	f

e3e3e3e3e3e3_o	e1,E1	e3,E3,e3,E3,e3,E3             	omega	f
e3e3e3e3dd_o	e1,E1	e3,E3,e3,E3,d,D             	omega	f
e3e3e3e3ss_o	e1,E1	e3,E3,e3,E3,s,S             	omega	f
e3e3e3e3bb_o	e1,E1	e3,E3,e3,E3,b,B             	omega	f
e3e3dddd_o	e1,E1	e3,E3,d,D,d,D             	omega	f
e3e3ddss_o	e1,E1	e3,E3,d,D,s,S             	omega	f
e3e3ddbb_o	e1,E1	e3,E3,d,D,b,B             	omega	f
e3e3ssss_o	e1,E1	e3,E3,s,S,s,S             	omega	f
e3e3ssbb_o	e1,E1	e3,E3,s,S,b,B             	omega	f
e3e3bbbb_o	e1,E1	e3,E3,b,B,b,B             	omega	f

dddddd_o	e1,E1	d,D,d,D,d,D             	omega	f
ddddss_o	e1,E1	d,D,d,D,s,S             	omega	f
ddddbb_o	e1,E1	d,D,d,D,b,B             	omega	f
ddssss_o	e1,E1	d,D,s,S,s,S             	omega	f
ddssbb_o	e1,E1	d,D,s,S,b,B             	omega	f
ddbbbb_o	e1,E1	d,D,b,B,b,B             	omega	f

ssssss_o	e1,E1	s,S,s,S,s,S             	omega	f
ssssbb_o	e1,E1	s,S,s,S,b,B             	omega	f
ssbbbb_o	e1,E1	s,S,b,B,b,B             	omega	f

bbbbbb_o	e1,E1	b,B,b,B,b,B             	omega	f



