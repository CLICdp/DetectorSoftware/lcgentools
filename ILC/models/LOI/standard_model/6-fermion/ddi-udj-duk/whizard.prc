# WHIZARD configuration file

# The selected model (O'Mega)
model	SM

# Processes for 6-fermion/ddi-udj-duk
# (Methods: chep=CompHEP, mad=MadGraph, omega=O'Mega)
# (Options: s=selected diagrams, number=QCD order [Madgraph])
#           f=fudged width [O'Mega]
#
# Tag    	In      Out     	Method	Option
#=====================================================
#########################################################################
#
# 6-fermion production
# Compatible with ZW+W-  type d_i \bar{d}_i u_j \bar{d}_j d_k \bar{u}_k

e1e1n1e1e1n1_o	e1,E1	e1,E1,n1,E1,e1,N1             	omega	f
e1e1n1e1e2n2_o	e1,E1	e1,E1,n1,E1,e2,N2             	omega	f
e1e1n1e1e3n3_o	e1,E1	e1,E1,n1,E1,e3,N3             	omega	f
e1e1n1e1du_o	e1,E1	e1,E1,n1,E1,d,U             	omega	f
e1e1n1e1sc_o	e1,E1	e1,E1,n1,E1,s,C             	omega	f

e1e1n2e2e1n1_o	e1,E1	e1,E1,n2,E2,e1,N1             	omega	f
e1e1n2e2e2n2_o	e1,E1	e1,E1,n2,E2,e2,N2             	omega	f
e1e1n2e2e3n3_o	e1,E1	e1,E1,n2,E2,e3,N3             	omega	f
e1e1n2e2du_o	e1,E1	e1,E1,n2,E2,d,U             	omega	f
e1e1n2e2sc_o	e1,E1	e1,E1,n2,E2,s,C             	omega	f

e1e1n3e3e1n1_o	e1,E1	e1,E1,n3,E3,e1,N1             	omega	f
e1e1n3e3e2n2_o	e1,E1	e1,E1,n3,E3,e2,N2             	omega	f
e1e1n3e3e3n3_o	e1,E1	e1,E1,n3,E3,e3,N3             	omega	f
e1e1n3e3du_o	e1,E1	e1,E1,n3,E3,d,U             	omega	f
e1e1n3e3sc_o	e1,E1	e1,E1,n3,E3,s,C             	omega	f

e1e1ude1n1_o	e1,E1	e1,E1,u,D,e1,N1             	omega	f
e1e1ude2n2_o	e1,E1	e1,E1,u,D,e2,N2             	omega	f
e1e1ude3n3_o	e1,E1	e1,E1,u,D,e3,N3             	omega	f
e1e1uddu_o	e1,E1	e1,E1,u,D,d,U             	omega	f
e1e1udsc_o	e1,E1	e1,E1,u,D,s,C             	omega	f

e1e1cse1n1_o	e1,E1	e1,E1,c,S,e1,N1             	omega	f
e1e1cse2n2_o	e1,E1	e1,E1,c,S,e2,N2             	omega	f
e1e1cse3n3_o	e1,E1	e1,E1,c,S,e3,N3             	omega	f
e1e1csdu_o	e1,E1	e1,E1,c,S,d,U             	omega	f
e1e1cssc_o	e1,E1	e1,E1,c,S,s,C             	omega	f

e2e2n1e1e1n1_o	e1,E1	e2,E2,n1,E1,e1,N1             	omega	f
e2e2n1e1e2n2_o	e1,E1	e2,E2,n1,E1,e2,N2             	omega	f
e2e2n1e1e3n3_o	e1,E1	e2,E2,n1,E1,e3,N3             	omega	f
e2e2n1e1du_o	e1,E1	e2,E2,n1,E1,d,U             	omega	f
e2e2n1e1sc_o	e1,E1	e2,E2,n1,E1,s,C             	omega	f

e2e2n2e2e1n1_o	e1,E1	e2,E2,n2,E2,e1,N1             	omega	f
e2e2n2e2e2n2_o	e1,E1	e2,E2,n2,E2,e2,N2             	omega	f
e2e2n2e2e3n3_o	e1,E1	e2,E2,n2,E2,e3,N3             	omega	f
e2e2n2e2du_o	e1,E1	e2,E2,n2,E2,d,U             	omega	f
e2e2n2e2sc_o	e1,E1	e2,E2,n2,E2,s,C             	omega	f

e2e2n3e3e1n1_o	e1,E1	e2,E2,n3,E3,e1,N1             	omega	f
e2e2n3e3e2n2_o	e1,E1	e2,E2,n3,E3,e2,N2             	omega	f
e2e2n3e3e3n3_o	e1,E1	e2,E2,n3,E3,e3,N3             	omega	f
e2e2n3e3du_o	e1,E1	e2,E2,n3,E3,d,U             	omega	f
e2e2n3e3sc_o	e1,E1	e2,E2,n3,E3,s,C             	omega	f

e2e2ude1n1_o	e1,E1	e2,E2,u,D,e1,N1             	omega	f
e2e2ude2n2_o	e1,E1	e2,E2,u,D,e2,N2             	omega	f
e2e2ude3n3_o	e1,E1	e2,E2,u,D,e3,N3             	omega	f
e2e2uddu_o	e1,E1	e2,E2,u,D,d,U             	omega	f
e2e2udsc_o	e1,E1	e2,E2,u,D,s,C             	omega	f

e2e2cse1n1_o	e1,E1	e2,E2,c,S,e1,N1             	omega	f
e2e2cse2n2_o	e1,E1	e2,E2,c,S,e2,N2             	omega	f
e2e2cse3n3_o	e1,E1	e2,E2,c,S,e3,N3             	omega	f
e2e2csdu_o	e1,E1	e2,E2,c,S,d,U             	omega	f
e2e2cssc_o	e1,E1	e2,E2,c,S,s,C             	omega	f

e3e3n1e1e1n1_o	e1,E1	e3,E3,n1,E1,e1,N1             	omega	f
e3e3n1e1e2n2_o	e1,E1	e3,E3,n1,E1,e2,N2             	omega	f
e3e3n1e1e3n3_o	e1,E1	e3,E3,n1,E1,e3,N3             	omega	f
e3e3n1e1du_o	e1,E1	e3,E3,n1,E1,d,U             	omega	f
e3e3n1e1sc_o	e1,E1	e3,E3,n1,E1,s,C             	omega	f

e3e3n2e2e1n1_o	e1,E1	e3,E3,n2,E2,e1,N1             	omega	f
e3e3n2e2e2n2_o	e1,E1	e3,E3,n2,E2,e2,N2             	omega	f
e3e3n2e2e3n3_o	e1,E1	e3,E3,n2,E2,e3,N3             	omega	f
e3e3n2e2du_o	e1,E1	e3,E3,n2,E2,d,U             	omega	f
e3e3n2e2sc_o	e1,E1	e3,E3,n2,E2,s,C             	omega	f

e3e3n3e3e1n1_o	e1,E1	e3,E3,n3,E3,e1,N1             	omega	f
e3e3n3e3e2n2_o	e1,E1	e3,E3,n3,E3,e2,N2             	omega	f
e3e3n3e3e3n3_o	e1,E1	e3,E3,n3,E3,e3,N3             	omega	f
e3e3n3e3du_o	e1,E1	e3,E3,n3,E3,d,U             	omega	f
e3e3n3e3sc_o	e1,E1	e3,E3,n3,E3,s,C             	omega	f

e3e3ude1n1_o	e1,E1	e3,E3,u,D,e1,N1             	omega	f
e3e3ude2n2_o	e1,E1	e3,E3,u,D,e2,N2             	omega	f
e3e3ude3n3_o	e1,E1	e3,E3,u,D,e3,N3             	omega	f
e3e3uddu_o	e1,E1	e3,E3,u,D,d,U             	omega	f
e3e3udsc_o	e1,E1	e3,E3,u,D,s,C             	omega	f

e3e3cse1n1_o	e1,E1	e3,E3,c,S,e1,N1             	omega	f
e3e3cse2n2_o	e1,E1	e3,E3,c,S,e2,N2             	omega	f
e3e3cse3n3_o	e1,E1	e3,E3,c,S,e3,N3             	omega	f
e3e3csdu_o	e1,E1	e3,E3,c,S,d,U             	omega	f
e3e3cssc_o	e1,E1	e3,E3,c,S,s,C             	omega	f

ddn1e1e1n1_o	e1,E1	d,D,n1,E1,e1,N1             	omega	f
ddn1e1e2n2_o	e1,E1	d,D,n1,E1,e2,N2             	omega	f
ddn1e1e3n3_o	e1,E1	d,D,n1,E1,e3,N3             	omega	f
ddn1e1du_o	e1,E1	d,D,n1,E1,d,U             	omega	f
ddn1e1sc_o	e1,E1	d,D,n1,E1,s,C             	omega	f

ddn2e2e1n1_o	e1,E1	d,D,n2,E2,e1,N1             	omega	f
ddn2e2e2n2_o	e1,E1	d,D,n2,E2,e2,N2             	omega	f
ddn2e2e3n3_o	e1,E1	d,D,n2,E2,e3,N3             	omega	f
ddn2e2du_o	e1,E1	d,D,n2,E2,d,U             	omega	f
ddn2e2sc_o	e1,E1	d,D,n2,E2,s,C             	omega	f

ddn3e3e1n1_o	e1,E1	d,D,n3,E3,e1,N1             	omega	f
ddn3e3e2n2_o	e1,E1	d,D,n3,E3,e2,N2             	omega	f
ddn3e3e3n3_o	e1,E1	d,D,n3,E3,e3,N3             	omega	f
ddn3e3du_o	e1,E1	d,D,n3,E3,d,U             	omega	f
ddn3e3sc_o	e1,E1	d,D,n3,E3,s,C             	omega	f

ddude1n1_o	e1,E1	d,D,u,D,e1,N1             	omega	f
ddude2n2_o	e1,E1	d,D,u,D,e2,N2             	omega	f
ddude3n3_o	e1,E1	d,D,u,D,e3,N3             	omega	f
dduddu_o	e1,E1	d,D,u,D,d,U             	omega	f
ddudsc_o	e1,E1	d,D,u,D,s,C             	omega	f

ddcse1n1_o	e1,E1	d,D,c,S,e1,N1             	omega	f
ddcse2n2_o	e1,E1	d,D,c,S,e2,N2             	omega	f
ddcse3n3_o	e1,E1	d,D,c,S,e3,N3             	omega	f
ddcsdu_o	e1,E1	d,D,c,S,d,U             	omega	f
ddcssc_o	e1,E1	d,D,c,S,s,C             	omega	f

ssn1e1e1n1_o	e1,E1	s,S,n1,E1,e1,N1             	omega	f
ssn1e1e2n2_o	e1,E1	s,S,n1,E1,e2,N2             	omega	f
ssn1e1e3n3_o	e1,E1	s,S,n1,E1,e3,N3             	omega	f
ssn1e1du_o	e1,E1	s,S,n1,E1,d,U             	omega	f
ssn1e1sc_o	e1,E1	s,S,n1,E1,s,C             	omega	f

ssn2e2e1n1_o	e1,E1	s,S,n2,E2,e1,N1             	omega	f
ssn2e2e2n2_o	e1,E1	s,S,n2,E2,e2,N2             	omega	f
ssn2e2e3n3_o	e1,E1	s,S,n2,E2,e3,N3             	omega	f
ssn2e2du_o	e1,E1	s,S,n2,E2,d,U             	omega	f
ssn2e2sc_o	e1,E1	s,S,n2,E2,s,C             	omega	f

ssn3e3e1n1_o	e1,E1	s,S,n3,E3,e1,N1             	omega	f
ssn3e3e2n2_o	e1,E1	s,S,n3,E3,e2,N2             	omega	f
ssn3e3e3n3_o	e1,E1	s,S,n3,E3,e3,N3             	omega	f
ssn3e3du_o	e1,E1	s,S,n3,E3,d,U             	omega	f
ssn3e3sc_o	e1,E1	s,S,n3,E3,s,C             	omega	f

ssude1n1_o	e1,E1	s,S,u,D,e1,N1             	omega	f
ssude2n2_o	e1,E1	s,S,u,D,e2,N2             	omega	f
ssude3n3_o	e1,E1	s,S,u,D,e3,N3             	omega	f
ssuddu_o	e1,E1	s,S,u,D,d,U             	omega	f
ssudsc_o	e1,E1	s,S,u,D,s,C             	omega	f

sscse1n1_o	e1,E1	s,S,c,S,e1,N1             	omega	f
sscse2n2_o	e1,E1	s,S,c,S,e2,N2             	omega	f
sscse3n3_o	e1,E1	s,S,c,S,e3,N3             	omega	f
sscsdu_o	e1,E1	s,S,c,S,d,U             	omega	f
sscssc_o	e1,E1	s,S,c,S,s,C             	omega	f

bbn1e1e1n1_o	e1,E1	b,B,n1,E1,e1,N1             	omega	f
bbn1e1e2n2_o	e1,E1	b,B,n1,E1,e2,N2             	omega	f
bbn1e1e3n3_o	e1,E1	b,B,n1,E1,e3,N3             	omega	f
bbn1e1du_o	e1,E1	b,B,n1,E1,d,U             	omega	f
bbn1e1sc_o	e1,E1	b,B,n1,E1,s,C             	omega	f

bbn2e2e1n1_o	e1,E1	b,B,n2,E2,e1,N1             	omega	f
bbn2e2e2n2_o	e1,E1	b,B,n2,E2,e2,N2             	omega	f
bbn2e2e3n3_o	e1,E1	b,B,n2,E2,e3,N3             	omega	f
bbn2e2du_o	e1,E1	b,B,n2,E2,d,U             	omega	f
bbn2e2sc_o	e1,E1	b,B,n2,E2,s,C             	omega	f

bbn3e3e1n1_o	e1,E1	b,B,n3,E3,e1,N1             	omega	f
bbn3e3e2n2_o	e1,E1	b,B,n3,E3,e2,N2             	omega	f
bbn3e3e3n3_o	e1,E1	b,B,n3,E3,e3,N3             	omega	f
bbn3e3du_o	e1,E1	b,B,n3,E3,d,U             	omega	f
bbn3e3sc_o	e1,E1	b,B,n3,E3,s,C             	omega	f

bbude1n1_o	e1,E1	b,B,u,D,e1,N1             	omega	f
bbude2n2_o	e1,E1	b,B,u,D,e2,N2             	omega	f
bbude3n3_o	e1,E1	b,B,u,D,e3,N3             	omega	f
bbuddu_o	e1,E1	b,B,u,D,d,U             	omega	f
bbudsc_o	e1,E1	b,B,u,D,s,C             	omega	f

bbcse1n1_o	e1,E1	b,B,c,S,e1,N1             	omega	f
bbcse2n2_o	e1,E1	b,B,c,S,e2,N2             	omega	f
bbcse3n3_o	e1,E1	b,B,c,S,e3,N3             	omega	f
bbcsdu_o	e1,E1	b,B,c,S,d,U             	omega	f
bbcssc_o	e1,E1	b,B,c,S,s,C             	omega	f

