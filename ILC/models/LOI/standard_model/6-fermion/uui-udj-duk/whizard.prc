# WHIZARD configuration file

# The selected model (O'Mega)
model	SM

# Processes for 6-fermion/uui-udj-duk
# (Methods: chep=CompHEP, mad=MadGraph, omega=O'Mega)
# (Options: s=selected diagrams, number=QCD order [Madgraph])
#           f=fudged width [O'Mega]
#
# Tag    	In      Out     	Method	Option
#=====================================================
#########################################################################
#
############################################################
# 6-fermion production
# Compatible with ZW+W-  type u_i \bar{u}_i u_j \bar{d}_j d_k \bar{u}_k

n1n1n1e1e1n1_o	e1,E1	n1,N1,n1,E1,e1,N1             	omega	f
n1n1n1e1e2n2_o	e1,E1	n1,N1,n1,E1,e2,N2             	omega	f
n1n1n1e1e3n3_o	e1,E1	n1,N1,n1,E1,e3,N3             	omega	f
n1n1n1e1du_o	e1,E1	n1,N1,n1,E1,d,U             	omega	f
n1n1n1e1sc_o	e1,E1	n1,N1,n1,E1,s,C             	omega	f

n1n1n2e2e1n1_o	e1,E1	n1,N1,n2,E2,e1,N1             	omega	f
n1n1n2e2e2n2_o	e1,E1	n1,N1,n2,E2,e2,N2             	omega	f
n1n1n2e2e3n3_o	e1,E1	n1,N1,n2,E2,e3,N3             	omega	f
n1n1n2e2du_o	e1,E1	n1,N1,n2,E2,d,U             	omega	f
n1n1n2e2sc_o	e1,E1	n1,N1,n2,E2,s,C             	omega	f

n1n1n3e3e1n1_o	e1,E1	n1,N1,n3,E3,e1,N1             	omega	f
n1n1n3e3e2n2_o	e1,E1	n1,N1,n3,E3,e2,N2             	omega	f
n1n1n3e3e3n3_o	e1,E1	n1,N1,n3,E3,e3,N3             	omega	f
n1n1n3e3du_o	e1,E1	n1,N1,n3,E3,d,U             	omega	f
n1n1n3e3sc_o	e1,E1	n1,N1,n3,E3,s,C             	omega	f

n1n1ude1n1_o	e1,E1	n1,N1,u,D,e1,N1             	omega	f
n1n1ude2n2_o	e1,E1	n1,N1,u,D,e2,N2             	omega	f
n1n1ude3n3_o	e1,E1	n1,N1,u,D,e3,N3             	omega	f
n1n1uddu_o	e1,E1	n1,N1,u,D,d,U             	omega	f
n1n1udsc_o	e1,E1	n1,N1,u,D,s,C             	omega	f

n1n1cse1n1_o	e1,E1	n1,N1,c,S,e1,N1             	omega	f
n1n1cse2n2_o	e1,E1	n1,N1,c,S,e2,N2             	omega	f
n1n1cse3n3_o	e1,E1	n1,N1,c,S,e3,N3             	omega	f
n1n1csdu_o	e1,E1	n1,N1,c,S,d,U             	omega	f
n1n1cssc_o	e1,E1	n1,N1,c,S,s,C             	omega	f

n2n2n1e1e1n1_o	e1,E1	n2,N2,n1,E1,e1,N1             	omega	f
n2n2n1e1e2n2_o	e1,E1	n2,N2,n1,E1,e2,N2             	omega	f
n2n2n1e1e3n3_o	e1,E1	n2,N2,n1,E1,e3,N3             	omega	f
n2n2n1e1du_o	e1,E1	n2,N2,n1,E1,d,U             	omega	f
n2n2n1e1sc_o	e1,E1	n2,N2,n1,E1,s,C             	omega	f

n2n2n2e2e1n1_o	e1,E1	n2,N2,n2,E2,e1,N1             	omega	f
n2n2n2e2e2n2_o	e1,E1	n2,N2,n2,E2,e2,N2             	omega	f
n2n2n2e2e3n3_o	e1,E1	n2,N2,n2,E2,e3,N3             	omega	f
n2n2n2e2du_o	e1,E1	n2,N2,n2,E2,d,U             	omega	f
n2n2n2e2sc_o	e1,E1	n2,N2,n2,E2,s,C             	omega	f

n2n2n3e3e1n1_o	e1,E1	n2,N2,n3,E3,e1,N1             	omega	f
n2n2n3e3e2n2_o	e1,E1	n2,N2,n3,E3,e2,N2             	omega	f
n2n2n3e3e3n3_o	e1,E1	n2,N2,n3,E3,e3,N3             	omega	f
n2n2n3e3du_o	e1,E1	n2,N2,n3,E3,d,U             	omega	f
n2n2n3e3sc_o	e1,E1	n2,N2,n3,E3,s,C             	omega	f

n2n2ude1n1_o	e1,E1	n2,N2,u,D,e1,N1             	omega	f
n2n2ude2n2_o	e1,E1	n2,N2,u,D,e2,N2             	omega	f
n2n2ude3n3_o	e1,E1	n2,N2,u,D,e3,N3             	omega	f
n2n2uddu_o	e1,E1	n2,N2,u,D,d,U             	omega	f
n2n2udsc_o	e1,E1	n2,N2,u,D,s,C             	omega	f

n2n2cse1n1_o	e1,E1	n2,N2,c,S,e1,N1             	omega	f
n2n2cse2n2_o	e1,E1	n2,N2,c,S,e2,N2             	omega	f
n2n2cse3n3_o	e1,E1	n2,N2,c,S,e3,N3             	omega	f
n2n2csdu_o	e1,E1	n2,N2,c,S,d,U             	omega	f
n2n2cssc_o	e1,E1	n2,N2,c,S,s,C             	omega	f

n3n3n1e1e1n1_o	e1,E1	n3,N3,n1,E1,e1,N1             	omega	f
n3n3n1e1e2n2_o	e1,E1	n3,N3,n1,E1,e2,N2             	omega	f
n3n3n1e1e3n3_o	e1,E1	n3,N3,n1,E1,e3,N3             	omega	f
n3n3n1e1du_o	e1,E1	n3,N3,n1,E1,d,U             	omega	f
n3n3n1e1sc_o	e1,E1	n3,N3,n1,E1,s,C             	omega	f

n3n3n2e2e1n1_o	e1,E1	n3,N3,n2,E2,e1,N1             	omega	f
n3n3n2e2e2n2_o	e1,E1	n3,N3,n2,E2,e2,N2             	omega	f
n3n3n2e2e3n3_o	e1,E1	n3,N3,n2,E2,e3,N3             	omega	f
n3n3n2e2du_o	e1,E1	n3,N3,n2,E2,d,U             	omega	f
n3n3n2e2sc_o	e1,E1	n3,N3,n2,E2,s,C             	omega	f

n3n3n3e3e1n1_o	e1,E1	n3,N3,n3,E3,e1,N1             	omega	f
n3n3n3e3e2n2_o	e1,E1	n3,N3,n3,E3,e2,N2             	omega	f
n3n3n3e3e3n3_o	e1,E1	n3,N3,n3,E3,e3,N3             	omega	f
n3n3n3e3du_o	e1,E1	n3,N3,n3,E3,d,U             	omega	f
n3n3n3e3sc_o	e1,E1	n3,N3,n3,E3,s,C             	omega	f

n3n3ude1n1_o	e1,E1	n3,N3,u,D,e1,N1             	omega	f
n3n3ude2n2_o	e1,E1	n3,N3,u,D,e2,N2             	omega	f
n3n3ude3n3_o	e1,E1	n3,N3,u,D,e3,N3             	omega	f
n3n3uddu_o	e1,E1	n3,N3,u,D,d,U             	omega	f
n3n3udsc_o	e1,E1	n3,N3,u,D,s,C             	omega	f

n3n3cse1n1_o	e1,E1	n3,N3,c,S,e1,N1             	omega	f
n3n3cse2n2_o	e1,E1	n3,N3,c,S,e2,N2             	omega	f
n3n3cse3n3_o	e1,E1	n3,N3,c,S,e3,N3             	omega	f
n3n3csdu_o	e1,E1	n3,N3,c,S,d,U             	omega	f
n3n3cssc_o	e1,E1	n3,N3,c,S,s,C             	omega	f

uun1e1e1n1_o	e1,E1	u,U,n1,E1,e1,N1             	omega	f
uun1e1e2n2_o	e1,E1	u,U,n1,E1,e2,N2             	omega	f
uun1e1e3n3_o	e1,E1	u,U,n1,E1,e3,N3             	omega	f
uun1e1du_o	e1,E1	u,U,n1,E1,d,U             	omega	f
uun1e1sc_o	e1,E1	u,U,n1,E1,s,C             	omega	f

uun2e2e1n1_o	e1,E1	u,U,n2,E2,e1,N1             	omega	f
uun2e2e2n2_o	e1,E1	u,U,n2,E2,e2,N2             	omega	f
uun2e2e3n3_o	e1,E1	u,U,n2,E2,e3,N3             	omega	f
uun2e2du_o	e1,E1	u,U,n2,E2,d,U             	omega	f
uun2e2sc_o	e1,E1	u,U,n2,E2,s,C             	omega	f

uun3e3e1n1_o	e1,E1	u,U,n3,E3,e1,N1             	omega	f
uun3e3e2n2_o	e1,E1	u,U,n3,E3,e2,N2             	omega	f
uun3e3e3n3_o	e1,E1	u,U,n3,E3,e3,N3             	omega	f
uun3e3du_o	e1,E1	u,U,n3,E3,d,U             	omega	f
uun3e3sc_o	e1,E1	u,U,n3,E3,s,C             	omega	f

uuude1n1_o	e1,E1	u,U,u,D,e1,N1             	omega	f
uuude2n2_o	e1,E1	u,U,u,D,e2,N2             	omega	f
uuude3n3_o	e1,E1	u,U,u,D,e3,N3             	omega	f
uuuddu_o	e1,E1	u,U,u,D,d,U             	omega	f
uuudsc_o	e1,E1	u,U,u,D,s,C             	omega	f

uucse1n1_o	e1,E1	u,U,c,S,e1,N1             	omega	f
uucse2n2_o	e1,E1	u,U,c,S,e2,N2             	omega	f
uucse3n3_o	e1,E1	u,U,c,S,e3,N3             	omega	f
uucsdu_o	e1,E1	u,U,c,S,d,U             	omega	f
uucssc_o	e1,E1	u,U,c,S,s,C             	omega	f

ccn1e1e1n1_o	e1,E1	c,C,n1,E1,e1,N1             	omega	f
ccn1e1e2n2_o	e1,E1	c,C,n1,E1,e2,N2             	omega	f
ccn1e1e3n3_o	e1,E1	c,C,n1,E1,e3,N3             	omega	f
ccn1e1du_o	e1,E1	c,C,n1,E1,d,U             	omega	f
ccn1e1sc_o	e1,E1	c,C,n1,E1,s,C             	omega	f

ccn2e2e1n1_o	e1,E1	c,C,n2,E2,e1,N1             	omega	f
ccn2e2e2n2_o	e1,E1	c,C,n2,E2,e2,N2             	omega	f
ccn2e2e3n3_o	e1,E1	c,C,n2,E2,e3,N3             	omega	f
ccn2e2du_o	e1,E1	c,C,n2,E2,d,U             	omega	f
ccn2e2sc_o	e1,E1	c,C,n2,E2,s,C             	omega	f

ccn3e3e1n1_o	e1,E1	c,C,n3,E3,e1,N1             	omega	f
ccn3e3e2n2_o	e1,E1	c,C,n3,E3,e2,N2             	omega	f
ccn3e3e3n3_o	e1,E1	c,C,n3,E3,e3,N3             	omega	f
ccn3e3du_o	e1,E1	c,C,n3,E3,d,U             	omega	f
ccn3e3sc_o	e1,E1	c,C,n3,E3,s,C             	omega	f

ccude1n1_o	e1,E1	c,C,u,D,e1,N1             	omega	f
ccude2n2_o	e1,E1	c,C,u,D,e2,N2             	omega	f
ccude3n3_o	e1,E1	c,C,u,D,e3,N3             	omega	f
ccuddu_o	e1,E1	c,C,u,D,d,U             	omega	f
ccudsc_o	e1,E1	c,C,u,D,s,C             	omega	f

cccse1n1_o	e1,E1	c,C,c,S,e1,N1             	omega	f
cccse2n2_o	e1,E1	c,C,c,S,e2,N2             	omega	f
cccse3n3_o	e1,E1	c,C,c,S,e3,N3             	omega	f
cccsdu_o	e1,E1	c,C,c,S,d,U             	omega	f
cccssc_o	e1,E1	c,C,c,S,s,C             	omega	f

