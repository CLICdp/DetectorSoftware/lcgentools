# WHIZARD configuration file

# The selected model (O'Mega)
model	SM

# Processes for 6-fermion/zzz
# (Methods: chep=CompHEP, mad=MadGraph, omega=O'Mega)
# (Options: s=selected diagrams, number=QCD order [Madgraph])
#           f=fudged width [O'Mega]
#
# Tag    	In      Out     	Method	Option
#=====================================================
#########################################################################
# 6-fermion production
# Compatible with ZZZ  type u_i \bar{u}_i u_j \bar{u}_j u_k \bar{u}_k
#

n1n1n1n1uu_o	e1,E1	n1,N1,n1,N1,u,U             	omega	f
n1n1n1n1cc_o	e1,E1	n1,N1,n1,N1,c,C             	omega	f
n1n1n2n2uu_o	e1,E1	n1,N1,n2,N2,u,U             	omega	f
n1n1n2n2cc_o	e1,E1	n1,N1,n2,N2,c,C             	omega	f
n1n1n3n3uu_o	e1,E1	n1,N1,n3,N3,u,U             	omega	f
n1n1n3n3cc_o	e1,E1	n1,N1,n3,N3,c,C             	omega	f
n1n1uuuu_o	e1,E1	n1,N1,u,U,u,U             	omega	f
n1n1uucc_o	e1,E1	n1,N1,u,U,c,C             	omega	f
n1n1cccc_o	e1,E1	n1,N1,c,C,c,C             	omega	f

n2n2n2n2uu_o	e1,E1	n2,N2,n2,N2,u,U             	omega	f
n2n2n2n2cc_o	e1,E1	n2,N2,n2,N2,c,C             	omega	f
n2n2n3n3uu_o	e1,E1	n2,N2,n3,N3,u,U             	omega	f
n2n2n3n3cc_o	e1,E1	n2,N2,n3,N3,c,C             	omega	f
n2n2uuuu_o	e1,E1	n2,N2,u,U,u,U             	omega	f
n2n2uucc_o	e1,E1	n2,N2,u,U,c,C             	omega	f
n2n2cccc_o	e1,E1	n2,N2,c,C,c,C             	omega	f

n3n3n3n3uu_o	e1,E1	n3,N3,n3,N3,u,U             	omega	f
n3n3n3n3cc_o	e1,E1	n3,N3,n3,N3,c,C             	omega	f
n3n3uuuu_o	e1,E1	n3,N3,u,U,u,U             	omega	f
n3n3uucc_o	e1,E1	n3,N3,u,U,c,C             	omega	f
n3n3cccc_o	e1,E1	n3,N3,c,C,c,C             	omega	f

uuuuuu_o	e1,E1	u,U,u,U,u,U             	omega	f
uuuucc_o	e1,E1	u,U,u,U,c,C             	omega	f
uucccc_o	e1,E1	u,U,c,C,c,C             	omega	f

cccccc_o	e1,E1	c,C,c,C,c,C             	omega	f

# 6-fermion production
# Compatible with ZZZ  type u_i \bar{u}_i u_j \bar{u}_j d_k \bar{d}_k
#

n1n1n1n1e2e2_o	e1,E1	n1,N1,n1,N1,e2,E2             	omega	f
n1n1n1n1e3e3_o	e1,E1	n1,N1,n1,N1,e3,E3             	omega	f
n1n1n1n1dd_o	e1,E1	n1,N1,n1,N1,d,D             	omega	f
n1n1n1n1ss_o	e1,E1	n1,N1,n1,N1,s,S             	omega	f
n1n1n1n1bb_o	e1,E1	n1,N1,n1,N1,b,B             	omega	f

n1n1n2n2e3e3_o	e1,E1	n1,N1,n2,N2,e3,E3             	omega	f
n1n1n2n2dd_o	e1,E1	n1,N1,n2,N2,d,D             	omega	f
n1n1n2n2ss_o	e1,E1	n1,N1,n2,N2,s,S             	omega	f
n1n1n2n2bb_o	e1,E1	n1,N1,n2,N2,b,B             	omega	f

n1n1n3n3e2e2_o	e1,E1	n1,N1,n3,N3,e2,E2             	omega	f
n1n1n3n3dd_o	e1,E1	n1,N1,n3,N3,d,D             	omega	f
n1n1n3n3ss_o	e1,E1	n1,N1,n3,N3,s,S             	omega	f
n1n1n3n3bb_o	e1,E1	n1,N1,n3,N3,b,B             	omega	f

n1n1uue2e2_o	e1,E1	n1,N1,u,U,e2,E2             	omega	f
n1n1uue3e3_o	e1,E1	n1,N1,u,U,e3,E3             	omega	f
n1n1uuss_o	e1,E1	n1,N1,u,U,s,S             	omega	f
n1n1uubb_o	e1,E1	n1,N1,u,U,b,B             	omega	f

n1n1cce2e2_o	e1,E1	n1,N1,c,C,e2,E2             	omega	f
n1n1cce3e3_o	e1,E1	n1,N1,c,C,e3,E3             	omega	f
n1n1ccdd_o	e1,E1	n1,N1,c,C,d,D             	omega	f
n1n1ccbb_o	e1,E1	n1,N1,c,C,b,B             	omega	f

n2n2n2n2e1e1_o	e1,E1	n2,N2,n2,N2,e1,E1             	omega	f
n2n2n2n2e3e3_o	e1,E1	n2,N2,n2,N2,e3,E3             	omega	f
n2n2n2n2dd_o	e1,E1	n2,N2,n2,N2,d,D             	omega	f
n2n2n2n2ss_o	e1,E1	n2,N2,n2,N2,s,S             	omega	f
n2n2n2n2bb_o	e1,E1	n2,N2,n2,N2,b,B             	omega	f

n2n2n3n3e1e1_o	e1,E1	n2,N2,n3,N3,e1,E1             	omega	f
n2n2n3n3dd_o	e1,E1	n2,N2,n3,N3,d,D             	omega	f
n2n2n3n3ss_o	e1,E1	n2,N2,n3,N3,s,S             	omega	f
n2n2n3n3bb_o	e1,E1	n2,N2,n3,N3,b,B             	omega	f

n2n2uue1e1_o	e1,E1	n2,N2,u,U,e1,E1             	omega	f
n2n2uue3e3_o	e1,E1	n2,N2,u,U,e3,E3             	omega	f
n2n2uuss_o	e1,E1	n2,N2,u,U,s,S             	omega	f
n2n2uubb_o	e1,E1	n2,N2,u,U,b,B             	omega	f

n2n2cce1e1_o	e1,E1	n2,N2,c,C,e1,E1             	omega	f
n2n2cce3e3_o	e1,E1	n2,N2,c,C,e3,E3             	omega	f
n2n2ccdd_o	e1,E1	n2,N2,c,C,d,D             	omega	f
n2n2ccbb_o	e1,E1	n2,N2,c,C,b,B             	omega	f

n3n3n3n3e1e1_o	e1,E1	n3,N3,n3,N3,e1,E1             	omega	f
n3n3n3n3e2e2_o	e1,E1	n3,N3,n3,N3,e2,E2             	omega	f
n3n3n3n3dd_o	e1,E1	n3,N3,n3,N3,d,D             	omega	f
n3n3n3n3ss_o	e1,E1	n3,N3,n3,N3,s,S             	omega	f
n3n3n3n3bb_o	e1,E1	n3,N3,n3,N3,b,B             	omega	f

n3n3uue1e1_o	e1,E1	n3,N3,u,U,e1,E1             	omega	f
n3n3uue2e2_o	e1,E1	n3,N3,u,U,e2,E2             	omega	f
n3n3uuss_o	e1,E1	n3,N3,u,U,s,S             	omega	f
n3n3uubb_o	e1,E1	n3,N3,u,U,b,B             	omega	f

n3n3cce1e1_o	e1,E1	n3,N3,c,C,e1,E1             	omega	f
n3n3cce2e2_o	e1,E1	n3,N3,c,C,e2,E2             	omega	f
n3n3ccdd_o	e1,E1	n3,N3,c,C,d,D             	omega	f
n3n3ccbb_o	e1,E1	n3,N3,c,C,b,B             	omega	f

uuuue1e1_o	e1,E1	u,U,u,U,e1,E1             	omega	f
uuuue2e2_o	e1,E1	u,U,u,U,e2,E2             	omega	f
uuuue3e3_o	e1,E1	u,U,u,U,e3,E3             	omega	f
uuuuss_o	e1,E1	u,U,u,U,s,S             	omega	f
uuuubb_o	e1,E1	u,U,u,U,b,B             	omega	f

uucce1e1_o	e1,E1	u,U,c,C,e1,E1             	omega	f
uucce2e2_o	e1,E1	u,U,c,C,e2,E2             	omega	f
uucce3e3_o	e1,E1	u,U,c,C,e3,E3             	omega	f
uuccbb_o	e1,E1	u,U,c,C,b,B             	omega	f

cccce1e1_o	e1,E1	c,C,c,C,e1,E1             	omega	f
cccce2e2_o	e1,E1	c,C,c,C,e2,E2             	omega	f
cccce3e3_o	e1,E1	c,C,c,C,e3,E3             	omega	f
ccccdd_o	e1,E1	c,C,c,C,d,D             	omega	f
ccccbb_o	e1,E1	c,C,c,C,b,B             	omega	f

