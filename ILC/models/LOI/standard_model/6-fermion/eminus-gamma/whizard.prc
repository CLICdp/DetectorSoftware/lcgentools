# WHIZARD configuration file

# The selected model (O'Mega)
model	SM

# Processes for 6-fermion/eminus-gamma
# (Methods: chep=CompHEP, mad=MadGraph, omega=O'Mega)
# (Options: s=selected diagrams, number=QCD order [Madgraph])
#           f=fudged width [O'Mega]
#
# Tag    	In      Out     	Method	Option
#=====================================================
#########################################################################
#
#
# 6-fermion production
# Compatible with e^- \gamma --> \nu_e ZW  type \nu_e u_j \bar{u}_j d_k \bar{u}_k
#

e1a_n1n1n1e1n1_o	e1,A	n1,n1,N1,e1,N1             	omega	f
e1a_n1n1n1e2n2_o	e1,A	n1,n1,N1,e2,N2             	omega	f
e1a_n1n1n1e3n3_o	e1,A	n1,n1,N1,e3,N3             	omega	f
e1a_n1n1n1du_o  	e1,A	n1,n1,N1,d,U             	omega	f
e1a_n1n1n1sc_o  	e1,A	n1,n1,N1,s,C             	omega	f

e1a_n1n2n2e1n1_o	e1,A	n1,n2,N2,e1,N1             	omega	f
e1a_n1n2n2e2n2_o	e1,A	n1,n2,N2,e2,N2             	omega	f
e1a_n1n2n2e3n3_o	e1,A	n1,n2,N2,e3,N3             	omega	f
e1a_n1n2n2du_o  	e1,A	n1,n2,N2,d,U             	omega	f
e1a_n1n2n2sc_o  	e1,A	n1,n2,N2,s,C             	omega	f

e1a_n1n3n3e1n1_o	e1,A	n1,n3,N3,e1,N1             	omega	f
e1a_n1n3n3e2n2_o	e1,A	n1,n3,N3,e2,N2             	omega	f
e1a_n1n3n3e3n3_o	e1,A	n1,n3,N3,e3,N3             	omega	f
e1a_n1n3n3du_o  	e1,A	n1,n3,N3,d,U             	omega	f
e1a_n1n3n3sc_o  	e1,A	n1,n3,N3,s,C             	omega	f

e1a_n1uue1n1_o  	e1,A	n1,u,U,e1,N1             	omega	f
e1a_n1uue2n2_o  	e1,A	n1,u,U,e2,N2             	omega	f
e1a_n1uue3n3_o  	e1,A	n1,u,U,e3,N3             	omega	f
e1a_n1uu1du_o    	e1,A	n1,u,U,d,U              	omega	f
e1a_n1uusc_o    	e1,A	n1,u,U,s,C              	omega	f

e1a_n1cce1n1_o  	e1,A	n1,c,C,e1,N1             	omega	f
e1a_n1cce2n2_o  	e1,A	n1,c,C,e2,N2             	omega	f
e1a_n1cce3n3_o  	e1,A	n1,c,C,e3,N3             	omega	f
e1a_n1ccdu_o    	e1,A	n1,c,C,d,U              	omega	f
e1a_n1ccsc_o    	e1,A	n1,c,C,s,C              	omega	f

#
# 6-fermion production
# Compatible with e^- \gamma --> \nu_e ZW  type \nu_e d_j \bar{d}_j d_k \bar{u}_k
#

e1a_n1e1e1e1n1_o	e1,A	n1,e1,E1,e1,N1             	omega	f
e1a_n1e1e1e2n2_o	e1,A	n1,e1,E1,e2,N2             	omega	f
e1a_n1e1e1e3n3_o	e1,A	n1,e1,E1,e3,N3             	omega	f
e1a_n1e1e1du_o  	e1,A	n1,e1,E1,d,U             	omega	f
e1a_n1e1e1sc_o  	e1,A	n1,e1,E1,s,C             	omega	f

e1a_n1e2e2e1n1_o	e1,A	n1,e2,E2,e1,N1             	omega	f
e1a_n1e2e2e2n2_o	e1,A	n1,e2,E2,e2,N2             	omega	f
e1a_n1e2e2e3n3_o	e1,A	n1,e2,E2,e3,N3             	omega	f
e1a_n1e2e2du_o  	e1,A	n1,e2,E2,d,U             	omega	f
e1a_n1e2e2sc_o  	e1,A	n1,e2,E2,s,C             	omega	f

e1a_n1e3e3e1n1_o	e1,A	n1,e3,E3,e1,N1             	omega	f
e1a_n1e3e3e2n2_o	e1,A	n1,e3,E3,e2,N2             	omega	f
e1a_n1e3e3e3n3_o	e1,A	n1,e3,E3,e3,N3             	omega	f
e1a_n1e3e3du_o  	e1,A	n1,e3,E3,d,U             	omega	f
e1a_n1e3e3sc_o  	e1,A	n1,e3,E3,s,C             	omega	f

e1a_n1dde1n1_o  	e1,A	n1,d,D,e1,N1             	omega	f
e1a_n1dde2n2_o  	e1,A	n1,d,D,e2,N2             	omega	f
e1a_n1dde3n3_o  	e1,A	n1,d,D,e3,N3             	omega	f
e1a_n1dd1du_o    	e1,A	n1,d,D,d,U              	omega	f
e1a_n1ddsc_o    	e1,A	n1,d,D,s,C              	omega	f

e1a_n1sse1n1_o  	e1,A	n1,s,S,e1,N1             	omega	f
e1a_n1sse2n2_o  	e1,A	n1,s,S,e2,N2             	omega	f
e1a_n1sse3n3_o  	e1,A	n1,s,S,e3,N3             	omega	f
e1a_n1ssdu_o    	e1,A	n1,s,S,d,U              	omega	f
e1a_n1sssc_o    	e1,A	n1,s,S,s,C              	omega	f

e1a_n1bbe1n1_o  	e1,A	n1,b,B,e1,N1             	omega	f
e1a_n1bbe2n2_o  	e1,A	n1,b,B,e2,N2             	omega	f
e1a_n1bbe3n3_o  	e1,A	n1,b,B,e3,N3             	omega	f
e1a_n1bbdu_o    	e1,A	n1,b,B,d,U              	omega	f
e1a_n1bbsc_o    	e1,A	n1,b,B,s,C              	omega	f




# 6-fermion production
# Compatible with e^- \gamma --> e^- WW  type e^- u_j \bar{d}_j d_k \bar{u}_k
#

e1a_e1n2e2e1n1_o	e1,A	e1,n2,E2,e1,N1             	omega	f
e1a_e1n2e2e2n2_o	e1,A	e1,n2,E2,e2,N2             	omega	f
e1a_e1n2e2e3n3_o	e1,A	e1,n2,E2,e3,N3             	omega	f
e1a_e1n2e2du_o  	e1,A	e1,n2,E2,d,U               	omega	f
e1a_e1n2e2sc_o  	e1,A	e1,n2,E2,s,C               	omega	f

e1a_e1n3e3e1n1_o	e1,A	e1,n3,E3,e1,N1             	omega	f
e1a_e1n3e3e2n2_o	e1,A	e1,n3,E3,e2,N2             	omega	f
e1a_e1n3e3e3n3_o	e1,A	e1,n3,E3,e3,N3             	omega	f
e1a_e1n3e3du_o  	e1,A	e1,n3,E3,d,U               	omega	f
e1a_e1n3e3sc_o  	e1,A	e1,n3,E3,s,C               	omega	f

e1a_e1ude1n1_o  	e1,A	e1,u,D,e1,N1               	omega	f
e1a_e1ude2n2_o  	e1,A	e1,u,D,e2,N2               	omega	f
e1a_e1ude3n3_o  	e1,A	e1,u,D,e3,N3               	omega	f
e1a_e1uddu_o    	e1,A	e1,u,D,d,U                 	omega	f
e1a_e1udsc_o	        e1,A	e1,u,D,s,C                  	omega	f

e1a_e1cse1n1_o  	e1,A	e1,c,S,e1,N1                    omega	f
e1a_e1cse2n2_o  	e1,A	e1,c,S,e2,N2                	omega	f
e1a_e1cse3n3_o  	e1,A	e1,c,S,e3,N3               	omega	f
e1a_e1csdu_o	        e1,A	e1,c,S,d,U                 	omega	f
e1a_e1cssc_o	        e1,A	e1,c,S,s,C                  	omega	f


# 6-fermion production
# Compatible with  e^- \gamma --> e^- ZZ  type e^- u_j \bar{u}_j u_k \bar{u}_k
#

e1a_e1n2n2n2n2_o  	e1,A	e1,n2,N2,n2,N2               	omega	f
e1a_e1n2n2n3n3_o  	e1,A	e1,n2,N2,n3,N3               	omega	f
e1a_e1n2n2uu_o  	e1,A	e1,n2,N2,u,U               	omega	f
e1a_e1n2n2cc_o  	e1,A	e1,n2,N2,c,C               	omega	f
e1a_e1n3n3n3n3_o  	e1,A	e1,n3,N3,n3,N3               	omega	f
e1a_e1n3n3uu_o  	e1,A	e1,n3,N3,u,U               	omega	f
e1a_e1n3n3cc_o  	e1,A	e1,n3,N3,c,C               	omega	f
e1a_e1uuuu_o    	e1,A	e1,u,U,u,U                 	omega	f
e1a_e1uucc_o    	e1,A	e1,u,U,c,C                 	omega	f
e1a_e1cccc_o    	e1,A	e1,c,C,c,C                 	omega	f


# 6-fermion production
# Compatible with e^- \gamma --> e^-  ZZ  type e^- u_j \bar{u}_j d_k \bar{d}_k
#

e1a_e1n2n2e1e1_o	e1,A	e1,n2,N2,e1,E1             	omega	f
e1a_e1n2n2e3e3_o	e1,A	e1,n2,N2,e3,E3             	omega	f
e1a_e1n2n2dd_o  	e1,A	e1,n2,N2,d,D               	omega	f
e1a_e1n2n2ss_o  	e1,A	e1,n2,N2,s,S               	omega	f
e1a_e1n2n2bb_o  	e1,A	e1,n2,N2,b,B               	omega	f

e1a_e1n3n3e1e1_o	e1,A	e1,n3,N3,e1,E1             	omega	f
e1a_e1n3n3e2e2_o	e1,A	e1,n3,N3,e2,E2             	omega	f
e1a_e1n3n3dd_o  	e1,A	e1,n3,N3,d,D               	omega	f
e1a_e1n3n3ss_o  	e1,A	e1,n3,N3,s,S               	omega	f
e1a_e1n3n3bb_o  	e1,A	e1,n3,N3,b,B               	omega	f

e1a_e1uue1e1_o   	e1,A	e1,u,U,e1,E1               	omega	f
e1a_e1uue2e2_o  	e1,A	e1,u,U,e2,E2               	omega	f
e1a_e1uue3e3_o  	e1,A	e1,u,U,e3,E3               	omega	f
e1a_e1uuss_o    	e1,A	e1,u,U,s,S                 	omega	f
e1a_e1uubb_o    	e1,A	e1,u,U,b,B                 	omega	f

e1a_e1cce1e1_o  	e1,A	e1,c,C,e1,E1               	omega	f
e1a_e1cce2e2_o  	e1,A	e1,c,C,e2,E2               	omega	f
e1a_e1cce3e3_o  	e1,A	e1,c,C,e3,E3               	omega	f
e1a_e1ccdd_o    	e1,A	e1,c,C,d,D                 	omega	f
e1a_e1ccbb_o    	e1,A	e1,c,C,b,B                 	omega	f


# 6-fermion production
# Compatible with e^- \gamma --> e^-  ZZ  type e^- d_j \bar{d}_j d_k \bar{d}_k
#

e1a_e1e1e1e1e1_o	e1,A	e1,e1,E1,e1,E1             	omega	f
e1a_e1e1e1e2e2_o	e1,A	e1,e1,E1,e2,E2             	omega	f
e1a_e1e1e1e3e3_o	e1,A	e1,e1,E1,e3,E3             	omega	f
e1a_e1e1e1dd_o  	e1,A	e1,e1,E1,d,D               	omega	f
e1a_e1e1e1ss_o  	e1,A	e1,e1,E1,s,S               	omega	f
e1a_e1e1e1bb_o  	e1,A	e1,e1,E1,b,B               	omega	f

e1a_e1e2e2e2e2_o	e1,A	e1,e2,E2,e2,E2             	omega	f
e1a_e1e2e2e3e3_o	e1,A	e1,e2,E2,e3,E3             	omega	f
e1a_e1e2e2dd_o  	e1,A	e1,e2,E2,d,D               	omega	f
e1a_e1e2e2ss_o  	e1,A	e1,e2,E2,s,S               	omega	f
e1a_e1e2e2bb_o  	e1,A	e1,e2,E2,b,B               	omega	f

e1a_e1e3e3e3e3_o	e1,A	e1,e3,E3,e3,E3             	omega	f
e1a_e1e3e3dd_o  	e1,A	e1,e3,E3,d,D               	omega	f
e1a_e1e3e3ss_o  	e1,A	e1,e3,E3,s,S               	omega	f
e1a_e1e3e3bb_o  	e1,A	e1,e3,E3,b,B               	omega	f 

e1a_e1dddd_o    	e1,A	e1,d,D,d,D                  	omega	f
e1a_e1ddss_o    	e1,A	e1,d,D,s,S                 	omega	f
e1a_e1ddbb_o    	e1,A	e1,d,D,b,B                 	omega	f

e1a_e1ssss_o    	e1,A	e1,s,S,s,S                 	omega	f
e1a_e1ssbb_o    	e1,A	e1,s,S,b,B                 	omega	f

e1a_e1bbbb_o    	e1,A	e1,b,B,b,B                 	omega	f



