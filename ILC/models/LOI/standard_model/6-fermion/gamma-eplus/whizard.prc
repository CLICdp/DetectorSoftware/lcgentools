# WHIZARD configuration file

# The selected model (O'Mega)
model	SM

# Processes for 6-fermion/gamma-eplus
# (Methods: chep=CompHEP, mad=MadGraph, omega=O'Mega)
# (Options: s=selected diagrams, number=QCD order [Madgraph])
#           f=fudged width [O'Mega]
#
# Tag    	In      Out     	Method	Option
#=====================================================
#########################################################################
#
#
# 6-fermion production
# Compatible with \gamma e^+ --> \bar{\nu_e} WZ  type \bar{\nu_e} u_j \bar{d}_j u_k \bar{u}_k
#

ae1_n1n1e1n1n1_o	A,E1	N1,n1,E1,n1,N1             	omega	f
ae1_n1n2e2n1n1_o	A,E1	N1,n2,E2,n1,N1             	omega	f
ae1_n1n3e3n1n1_o	A,E1	N1,n3,E3,n1,N1             	omega	f
ae1_n1udn1n1_o  	A,E1	N1,u,D,n1,N1             	omega	f
ae1_n1csn1n1_o  	A,E1	N1,c,S,n1,N1             	omega	f

ae1_n1n1e1n2n2_o	A,E1	N1,n1,E1,n2,N2             	omega	f
ae1_n1n2e2n2n2_o	A,E1	N1,n2,E2,n2,N2             	omega	f
ae1_n1n3e3n2n2_o	A,E1	N1,n3,E3,n2,N2             	omega	f
ae1_n1udn2n2_o  	A,E1	N1,u,D,n2,N2             	omega	f
ae1_n1csn2n2_o  	A,E1	N1,c,S,n2,N2             	omega	f

ae1_n1n1e1n3n3_o	A,E1	N1,n1,E1,n3,N3             	omega	f
ae1_n1n2e2n3n3_o	A,E1	N1,n2,E2,n3,N3             	omega	f
ae1_n1n3e3n3n3_o	A,E1	N1,n3,E3,n3,N3             	omega	f
ae1_n1udn3n3_o  	A,E1	N1,u,D,n3,N3             	omega	f
ae1_n1csn3n3_o  	A,E1	N1,c,S,n3,N3             	omega	f

ae1_n1n1e1uu_o  	A,E1	N1,n1,E1,u,U             	omega	f
ae1_n1n2e2uu_o  	A,E1	N1,n2,E2,u,U             	omega	f
ae1_n1n3e3uu_o  	A,E1	N1,n3,E3,u,U             	omega	f
ae1_n1uduu_o    	A,E1	N1,u,D,u,U              	omega	f
ae1_n1csuu_o    	A,E1	N1,c,S,u,U              	omega	f

ae1_n1n1e1cc_o  	A,E1	N1,n1,E1,c,C             	omega	f
ae1_n1n2e2cc_o  	A,E1	N1,n2,E2,c,C             	omega	f
ae1_n1n3e3cc_o  	A,E1	N1,n3,E3,c,C            	omega	f
ae1_n1udcc_o    	A,E1	N1,u,D,c,C              	omega	f
ae1_n1cscc_o    	A,E1	N1,c,S,c,C              	omega	f

#
# 6-fermion production
# Compatible with \gamma e^+ --> \bar{\nu_e} WZ  type \bar{\nu_e} u_j \bar{d}_j d_k \bar{d}_k
#



ae1_n1n1e1e1e1_o	A,E1	N1,n1,E1,e1,E1             	omega	f
ae1_n1n2e2e1e1_o	A,E1	N1,n2,E2,e1,E1             	omega	f
ae1_n1n3e3e1e1_o	A,E1	N1,n3,E3,e1,E1             	omega	f
ae1_n1ude1e1_o  	A,E1	N1,u,D,e1,E1             	omega	f
ae1_n1cse1e1_o  	A,E1	N1,c,S,e1,E1             	omega	f

ae1_n1n1e1e2e2_o	A,E1	N1,n1,E1,e2,E2             	omega	f
ae1_n1n2e2e2e2_o	A,E1	N1,n2,E2,e2,E2             	omega	f
ae1_n1n3e3e2e2_o	A,E1	N1,n3,E3,e2,E2             	omega	f
ae1_n1ude2e2_o  	A,E1	N1,u,D,e2,E2             	omega	f
ae1_n1cse2e2_o  	A,E1	N1,c,S,e2,E2             	omega	f

ae1_n1n1e1e3e3_o	A,E1	N1,n1,E1,e3,E3             	omega	f
ae1_n1n2e2e3e3_o	A,E1	N1,n2,E2,e3,E3             	omega	f
ae1_n1n3e3e3e3_o	A,E1	N1,n3,E3,e3,E3             	omega	f
ae1_n1ude3e3_o  	A,E1	N1,u,D,e3,E3             	omega	f
ae1_n1cse3e3_o  	A,E1	N1,c,S,e3,E3             	omega	f

ae1_n1n1e1dd_o  	A,E1	N1,n1,E1,d,D             	omega	f
ae1_n1n2e2dd_o  	A,E1	N1,n2,E2,d,D             	omega	f
ae1_n1n3e3dd_o  	A,E1	N1,n3,E3,d,D             	omega	f
ae1_n1uddd_o    	A,E1	N1,u,D,d,D              	omega	f
ae1_n1csdd_o    	A,E1	N1,c,S,d,D              	omega	f

ae1_n1n1e1ss_o  	A,E1	N1,n1,E1,s,S             	omega	f
ae1_n1n2e2ss_o  	A,E1	N1,n2,E2,s,S             	omega	f
ae1_n1n3e3ss_o  	A,E1	N1,n3,E3,s,S            	omega	f
ae1_n1udss_o    	A,E1	N1,u,D,s,S              	omega	f
ae1_n1csss_o    	A,E1	N1,c,S,s,S              	omega	f

ae1_n1n1e1bb_o  	A,E1	N1,n1,E1,b,B             	omega	f
ae1_n1n2e2bb_o  	A,E1	N1,n2,E2,b,B             	omega	f
ae1_n1n3e3bb_o  	A,E1	N1,n3,E3,b,B            	omega	f
ae1_n1udbb_o    	A,E1	N1,u,D,b,B              	omega	f
ae1_n1csbb_o    	A,E1	N1,c,S,b,B              	omega	f



# 6-fermion production
# Compatible with \gamma e^+ --> e^+ WW  type e^+ u_j \bar{d}_j d_k \bar{u}_k
#

ae1_e1n2e2e1n1_o	A,E1	E1,n2,E2,e1,N1             	omega	f
ae1_e1n2e2e2n2_o	A,E1	E1,n2,E2,e2,N2             	omega	f
ae1_e1n2e2e3n3_o	A,E1	E1,n2,E2,e3,N3             	omega	f
ae1_e1n2e2du_o  	A,E1	E1,n2,E2,d,U               	omega	f
ae1_e1n2e2sc_o  	A,E1	E1,n2,E2,s,C               	omega	f

ae1_e1n3e3e1n1_o	A,E1	E1,n3,E3,e1,N1             	omega	f
ae1_e1n3e3e2n2_o	A,E1	E1,n3,E3,e2,N2             	omega	f
ae1_e1n3e3e3n3_o	A,E1	E1,n3,E3,e3,N3             	omega	f
ae1_e1n3e3du_o  	A,E1	E1,n3,E3,d,U               	omega	f
ae1_e1n3e3sc_o  	A,E1	E1,n3,E3,s,C               	omega	f

ae1_e1ude1n1_o  	A,E1	E1,u,D,e1,N1               	omega	f
ae1_e1ude2n2_o  	A,E1	E1,u,D,e2,N2               	omega	f
ae1_e1ude3n3_o  	A,E1	E1,u,D,e3,N3               	omega	f
ae1_e1uddu_o    	A,E1	E1,u,D,d,U                 	omega	f
ae1_e1udsc_o	        A,E1	E1,u,D,s,C                  	omega	f

ae1_e1cse1n1_o  	A,E1	E1,c,S,e1,N1                    omega	f
ae1_e1cse2n2_o  	A,E1	E1,c,S,e2,N2                	omega	f
ae1_e1cse3n3_o  	A,E1	E1,c,S,e3,N3               	omega	f
ae1_e1csdu_o	        A,E1	E1,c,S,d,U                 	omega	f
ae1_e1cssc_o	        A,E1	E1,c,S,s,C                  	omega	f


# 6-fermion production
# Compatible with  \gamma e^+ --> e^+ ZZ  type e^+ u_j \bar{u}_j u_k \bar{u}_k
#

ae1_e1n2n2n2n2_o  	A,E1	E1,n2,N2,n2,N2               	omega	f
ae1_e1n2n2n3n3_o  	A,E1	E1,n2,N2,n3,N3               	omega	f
ae1_e1n2n2uu_o  	A,E1	E1,n2,N2,u,U               	omega	f
ae1_e1n2n2cc_o  	A,E1	E1,n2,N2,c,C               	omega	f
ae1_e1n3n3n3n3_o  	A,E1	E1,n3,N3,n3,N3               	omega	f
ae1_e1n3n3uu_o  	A,E1	E1,n3,N3,u,U               	omega	f
ae1_e1n3n3cc_o  	A,E1	E1,n3,N3,c,C               	omega	f
ae1_e1uuuu_o    	A,E1	E1,u,U,u,U                 	omega	f
ae1_e1uucc_o    	A,E1	E1,u,U,c,C                 	omega	f
ae1_e1cccc_o    	A,E1	E1,c,C,c,C                 	omega	f


# 6-fermion production
# Compatible with \gamma e^+ --> e^+  ZZ  type e^+ u_j \bar{u}_j d_k \bar{d}_k
#

ae1_e1n2n2e1e1_o	A,E1	E1,n2,N2,e1,E1             	omega	f
ae1_e1n2n2e3e3_o	A,E1	E1,n2,N2,e3,E3             	omega	f
ae1_e1n2n2dd_o  	A,E1	E1,n2,N2,d,D               	omega	f
ae1_e1n2n2ss_o  	A,E1	E1,n2,N2,s,S               	omega	f
ae1_e1n2n2bb_o  	A,E1	E1,n2,N2,b,B               	omega	f

ae1_e1n3n3e1e1_o	A,E1	E1,n3,N3,e1,E1             	omega	f
ae1_e1n3n3e2e2_o	A,E1	E1,n3,N3,e2,E2             	omega	f
ae1_e1n3n3dd_o  	A,E1	E1,n3,N3,d,D               	omega	f
ae1_e1n3n3ss_o  	A,E1	E1,n3,N3,s,S               	omega	f
ae1_e1n3n3bb_o  	A,E1	E1,n3,N3,b,B               	omega	f

ae1_e1uue1e1_o   	A,E1	E1,u,U,e1,E1               	omega	f
ae1_e1uue2e2_o  	A,E1	E1,u,U,e2,E2               	omega	f
ae1_e1uue3e3_o  	A,E1	E1,u,U,e3,E3               	omega	f
ae1_e1uuss_o    	A,E1	E1,u,U,s,S                 	omega	f
ae1_e1uubb_o    	A,E1	E1,u,U,b,B                 	omega	f

ae1_e1cce1e1_o  	A,E1	E1,c,C,e1,E1               	omega	f
ae1_e1cce2e2_o  	A,E1	E1,c,C,e2,E2               	omega	f
ae1_e1cce3e3_o  	A,E1	E1,c,C,e3,E3               	omega	f
ae1_e1ccdd_o    	A,E1	E1,c,C,d,D                 	omega	f
ae1_e1ccbb_o    	A,E1	E1,c,C,b,B                 	omega	f


# 6-fermion production
# Compatible with \gamma e^+ --> e^+  ZZ  type e^+ d_j \bar{d}_j d_k \bar{d}_k
#

ae1_e1e1e1e1e1_o	A,E1	E1,e1,E1,e1,E1             	omega	f
ae1_e1e1e1e2e2_o	A,E1	E1,e1,E1,e2,E2             	omega	f
ae1_e1e1e1e3e3_o	A,E1	E1,e1,E1,e3,E3             	omega	f
ae1_e1e1e1dd_o  	A,E1	E1,e1,E1,d,D               	omega	f
ae1_e1e1e1ss_o  	A,E1	E1,e1,E1,s,S               	omega	f
ae1_e1e1e1bb_o  	A,E1	E1,e1,E1,b,B               	omega	f

ae1_e1e2e2e2e2_o	A,E1	E1,e2,E2,e2,E2             	omega	f
ae1_e1e2e2e3e3_o	A,E1	E1,e2,E2,e3,E3             	omega	f
ae1_e1e2e2dd_o  	A,E1	E1,e2,E2,d,D               	omega	f
ae1_e1e2e2ss_o  	A,E1	E1,e2,E2,s,S               	omega	f
ae1_e1e2e2bb_o  	A,E1	E1,e2,E2,b,B               	omega	f

ae1_e1e3e3e3e3_o	A,E1	E1,e3,E3,e3,E3             	omega	f
ae1_e1e3e3dd_o  	A,E1	E1,e3,E3,d,D               	omega	f
ae1_e1e3e3ss_o  	A,E1	E1,e3,E3,s,S               	omega	f
ae1_e1e3e3bb_o  	A,E1	E1,e3,E3,b,B               	omega	f 

ae1_e1dddd_o    	A,E1	E1,d,D,d,D                  	omega	f
ae1_e1ddss_o    	A,E1	E1,d,D,s,S                 	omega	f
ae1_e1ddbb_o    	A,E1	E1,d,D,b,B                 	omega	f

ae1_e1ssss_o    	A,E1	E1,s,S,s,S                 	omega	f
ae1_e1ssbb_o    	A,E1	E1,s,S,b,B                 	omega	f

ae1_e1bbbb_o    	A,E1	E1,b,B,b,B                 	omega	f


#
