# WHIZARD configuration file

# The selected model (O'Mega)
model	SM

# Processes for 6-fermion/gamma-gamma
# (Methods: chep=CompHEP, mad=MadGraph, omega=O'Mega)
# (Options: s=selected diagrams, number=QCD order [Madgraph])
#           f=fudged width [O'Mega]
#
# Tag    	In      Out     	Method	Option
#=====================================================
#########################################################################
#
# 6-fermion production
# Compatible with \gamma\gamma --> W+W-  type u_j \bar{d}_j d_k \bar{u}_k
#

aa_n1e1e1n1_o	A,A	n1,E1,e1,N1             	omega	f
aa_n1e1e2n2_o	A,A	n1,E1,e2,N2             	omega	f
aa_n1e1e3n3_o	A,A	n1,E1,e3,N3             	omega	f
aa_n1e1du_o	A,A	n1,E1,d,U                	omega	f
aa_n1e1sc_o	A,A	n1,E1,s,C               	omega	f

aa_n2e2e1n1_o	A,A	n2,E2,e1,N1             	omega	f
aa_n2e2e2n2_o	A,A	n2,E2,e2,N2             	omega	f
aa_n2e2e3n3_o	A,A	n2,E2,e3,N3             	omega	f
aa_n2e2du_o	A,A	n2,E2,d,U               	omega	f
aa_n2e2sc_o	A,A	n2,E2,s,C               	omega	f

aa_n3e3e1n1_o	A,A	n3,E3,e1,N1             	omega	f
aa_n3e3e2n2_o	A,A	n3,E3,e2,N2             	omega	f
aa_n3e3e3n3_o	A,A	n3,E3,e3,N3             	omega	f
aa_n3e3du_o	A,A	n3,E3,d,U               	omega	f
aa_n3e3sc_o	A,A	n3,E3,s,C               	omega	f

aa_ude1n1_o	A,A	u,D,e1,N1               	omega	f
aa_ude2n2_o	A,A	u,D,e2,N2               	omega	f
aa_ude3n3_o	A,A	u,D,e3,N3               	omega	f
aa_uddu_o  	A,A	u,D,d,U                 	omega	f
aa_udsc_o	A,A	u,D,s,C                  	omega	f

aa_cse1n1_o	A,A	c,S,e1,N1                       omega	f
aa_cse2n2_o	A,A	c,S,e2,N2                	omega	f
aa_cse3n3_o	A,A	c,S,e3,N3               	omega	f
aa_csdu_o	A,A	c,S,d,U                 	omega	f
aa_cssc_o	A,A	c,S,s,C                  	omega	f


# 6-fermion production
# Compatible with \gamma\gamma --> ZZ  type u_j \bar{u}_j u_k \bar{u}_k
#

aa_n1n1uu_o	A,A	n1,N1,u,U               	omega	f
aa_n1n1cc_o	A,A	n1,N1,c,C               	omega	f
aa_n2n2uu_o	A,A	n2,N2,u,U               	omega	f
aa_n2n2cc_o	A,A	n2,N2,c,C               	omega	f
aa_n3n3uu_o	A,A	n3,N3,u,U               	omega	f
aa_n3n3cc_o	A,A	n3,N3,c,C               	omega	f
aa_uuuu_o  	A,A	u,U,u,U                 	omega	f
aa_uucc_o  	A,A	u,U,c,C                 	omega	f
aa_cccc_o  	A,A	c,C,c,C                 	omega	f


# 6-fermion production
# Compatible with \gamma\gamma --> ZZ  type u_j \bar{u}_j d_k \bar{d}_k
#

aa_n1n1e2e2_o	A,A	n1,N1,e2,E2             	omega	f
aa_n1n1e3e3_o	A,A	n1,N1,e3,E3             	omega	f
aa_n1n1dd_o	A,A	n1,N1,d,D               	omega	f
aa_n1n1ss_o	A,A	n1,N1,s,S               	omega	f
aa_n1n1bb_o	A,A	n1,N1,b,B               	omega	f
aa_n2n2e1e1_o	A,A	n2,N2,e1,E1             	omega	f
aa_n2n2e3e3_o	A,A	n2,N2,e3,E3             	omega	f
aa_n2n2dd_o	A,A	n2,N2,d,D               	omega	f
aa_n2n2ss_o	A,A	n2,N2,s,S               	omega	f
aa_n2n2bb_o	A,A	n2,N2,b,B               	omega	f

aa_n3n3e1e1_o	A,A	n3,N3,e1,E1             	omega	f
aa_n3n3e2e2_o	A,A	n3,N3,e2,E2             	omega	f
aa_n3n3dd_o	A,A	n3,N3,d,D               	omega	f
aa_n3n3ss_o	A,A	n3,N3,s,S               	omega	f
aa_n3n3bb_o	A,A	n3,N3,b,B               	omega	f

aa_uue1e1_o	A,A	u,U,e1,E1               	omega	f
aa_uue2e2_o	A,A	u,U,e2,E2               	omega	f
aa_uue3e3_o	A,A	u,U,e3,E3               	omega	f
aa_uuss_o  	A,A	u,U,s,S                 	omega	f
aa_uubb_o  	A,A	u,U,b,B                 	omega	f

aa_cce1e1_o	A,A	c,C,e1,E1               	omega	f
aa_cce2e2_o	A,A	c,C,e2,E2               	omega	f
aa_cce3e3_o	A,A	c,C,e3,E3               	omega	f
aa_ccdd_o  	A,A	c,C,d,D                 	omega	f
aa_ccbb_o  	A,A	c,C,b,B                 	omega	f


# 6-fermion production
# Compatible with \gamma\gamma --> ZZ  type d_j \bar{d}_j d_k \bar{d}_k
#

aa_e1e1e1e1_o	A,A	e1,E1,e1,E1             	omega	f
aa_e1e1e2e2_o	A,A	e1,E1,e2,E2             	omega	f
aa_e1e1e3e3_o	A,A	e1,E1,e3,E3             	omega	f
aa_e1e1dd_o	A,A	e1,E1,d,D               	omega	f
aa_e1e1ss_o	A,A	e1,E1,s,S               	omega	f
aa_e1e1bb_o	A,A	e1,E1,b,B               	omega	f

aa_e2e2e2e2_o	A,A	e2,E2,e2,E2             	omega	f
aa_e2e2e3e3_o	A,A	e2,E2,e3,E3             	omega	f
aa_e2e2dd_o	A,A	e2,E2,d,D               	omega	f
aa_e2e2ss_o	A,A	e2,E2,s,S               	omega	f
aa_e2e2bb_o	A,A	e2,E2,b,B               	omega	f

aa_e3e3e3e3_o	A,A	e3,E3,e3,E3             	omega	f
aa_e3e3dd_o	A,A	e3,E3,d,D               	omega	f
aa_e3e3ss_o	A,A	e3,E3,s,S               	omega	f
aa_e3e3bb_o	A,A	e3,E3,b,B               	omega	f

aa_dddd_o  	A,A	d,D,d,D                  	omega	f
aa_ddss_o  	A,A	d,D,s,S                 	omega	f
aa_ddbb_o  	A,A	d,D,b,B                 	omega	f

aa_ssss_o  	A,A	s,S,s,S                 	omega	f
aa_ssbb_o  	A,A	s,S,b,B                 	omega	f

aa_bbbb_o  	A,A	b,B,b,B                 	omega	f

