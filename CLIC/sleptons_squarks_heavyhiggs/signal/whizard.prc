# WHIZARD configuration file

# The selected model
model   MSSM

# Processes
#  Methods: chep=CompHEP, mad=MadGraph, omega=O'Mega, test=trivial)
#  Options: s      selected diagrams (CompHEP/MadGraph)
#           r      restricted intermediate state (O'Mega)
#           c      apply exact color algebra (O'Mega)
#           n:XXX  coupling order (MadGraph)
#           w:XXX  width scheme (O'Mega)
#           p      transfer polarization (test)
#           u      unit matrix element (test)
#
# Tag                  In      Out             Method  Option
#


# sleptons.
se1se1_e1e1           e1,E1 neu1,E1,neu1,e1 omega r:3+4~se2 && 5+6~se2 || 4+5~se2 && 3+6~se2
smu1smu1_e2e2         e1,E1 neu1,E2,neu1,e2 omega r:3+4~smu2 && 5+6~smu2 || 4+5~smu2 && 3+6~smu2
stau1stau1_e3e3       e1,E1 neu1,E3,neu1,e3 omega r:3+4~stau1 && 5+6~stau1  || 4+5~stau1 && 3+6~stau1
se2se2_e1e1           e1,E1 neu1,E1,neu1,e1 omega r:3+4~se1 && 5+6~se1 || 4+5~se1 && 3+6~se1
smu2smu2_e2e2         e1,E1 neu1,E2,neu1,e2 omega r:3+4~smu1 && 5+6~smu1 || 4+5~smu1 && 3+6~smu1
stau2stau2_e3e3       e1,E1 neu1,E3,neu1,e3 omega r:3+4~stau2 && 5+6~stau2  || 4+5~stau2 && 3+6~stau2
stau1stau2_e3e3       e1,E1 neu1,E3,neu1,e3 omega r:3+4~stau1 && 5+6~stau2  || 4+5~stau1 && 3+6~stau2 || 3+4~stau2 && 5+6~stau1  || 4+5~stau2 && 3+6~stau1
#se1se2_e1e1           e1,E1 neu1,E1,neu1,e1 omega r:3+4~se1 && 5+6~se2 || 4+5~se1 && 3+6~se2 || 3+4~se2 && 5+6~se1 || 4+5~se2 && 3+6~se1
se1se2_e1e1           e1,E1 se1+,se2- omega 
se2se1_e1e1           e1,E1 se2+,se1- omega 
su2su2_e1e1           e1,E1 u,neu1,ubar,neu1 omega r:3+4~su2 && 5+6~su2c || 3+6~su2 && 4+5~su2c
sc2sc2_e1e1           e1,E1 c,neu1,cbar,neu1 omega r:3+4~sc2 && 5+6~sc2c || 3+6~sc2 && 4+5~sc2c
sd2sd2_e1e1           e1,E1 d,neu1,dbar,neu1 omega r:3+4~sd2 && 5+6~sd2c || 3+6~sd2 && 4+5~sd2c
ss2ss2_e1e1           e1,E1 s,neu1,sbar,neu1 omega r:3+4~ss2 && 5+6~ss2c || 3+6~ss2 && 4+5~ss2c

hh_e1e1               e1,E1 H+,H- omega
ha_e1e1               e1,E1 H,A0  omega
thh_e1e1               e1,E1 H+,H- test
tha_e1e1               e1,E1 H,A0  test


# remaining : sleptons in cascades, few permil br:s, and binos 3-body
# decays (up 5%, but only leptons in the f.s.


