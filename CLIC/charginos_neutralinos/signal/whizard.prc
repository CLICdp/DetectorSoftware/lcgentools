# WHIZARD configuration file

# The selected model
model   MSSM


# Processes
#  Methods: chep=CompHEP, mad=MadGraph, omega=O'Mega, test=trivial)
#  Options: s      selected diagrams (CompHEP/MadGraph)
#           r      restricted intermediate state (O'Mega)
#           c      apply exact color algebra (O'Mega)
#           n:XXX  coupling order (MadGraph)
#           w:XXX  width scheme (O'Mega)
#           p      transfer polarization (test)
#           u      unit matrix element (test)
#
# Tag                  In      Out             Method  Option
#

##Charginos
neu1neu2_e1e1  e1,E1 neu1,neu2  omega
neu1neu3_e1e1  e1,E1 neu1,neu3  omega
neu1neu4_e1e1  e1,E1 neu1,neu4  omega
neu2neu2_e1e1  e1,E1 neu2,neu2  omega
neu2neu3_e1e1  e1,E1 neu2,neu3  omega
neu2neu4_e1e1  e1,E1 neu2,neu4  omega
neu3neu4_e1e1  e1,E1 neu3,neu4  omega

ch1ch1_e1e1    e1,E1 ch1+,ch1-   omega
ch1ch2_e1e1    e1,E1 ch1+,ch2-   omega
ch2ch2_e1e1    e1,E1 ch2+,ch2-   omega
