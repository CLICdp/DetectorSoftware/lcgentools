// ***********************************************************************************
//
// HepSplit
//  A small program to split a large stdhep file to many small files
//  useful to run simulation jobs at many GRID sites.
// (Command format)
//     hepsplit [options]
//  Options
//     --infile [infile]    : (D=input.stdhep) Input stdhep file name
//     --outprefix [ofile]  : (D=output )    Prefix of output file. File names will be
//                  output_NNN.stdhep, where NNN=[nseq_from]~999
//     --nseq_from [N]      : (D=0) First sequence number of output file.
//     --maxread   [N]      : (D=0) Number of read recors, <= 0 to the end
//     --nw_per_file [N]    : (D=300) Number of events in each output file.
//     --write_begin_rec : (D=1) Output begin record in each file or not. (0,1)=(No, Yes)
//     --skip_nevents [N]  : (D=0) Skip N records before start writting.
//
// (How to build) 
//   Use GNUmakefile in the ssame directory of this soruce code.
//
// (Author)
//    Akiya Miyamoto, KEK, 5 July 2011.
//
//***************************************************************************************

#include <iostream>
#include <string>
#include <sstream>
#include <iomanip>
#include <stdlib.h>

#include "stdlun.h"
#include "stdhd.h"

extern "C" {
  extern void mcfio_init_();
  extern void stdxropen_(const char *fname, int *nentries, 
                        int *istream,int *iok, int lfname);
  extern void stdxrinit_(const char *fname, int *nentries, 
                        int *istream,int *iok, int lfname);
  extern void stdxrd_(int *iflag, int *istream, int *iok);
  extern void stdxend_(int *istream);


  extern void stdxwinit_(const char *filename, const char *title, 
                         int *ntries, int *istream, int *iok,
                         int lfilename, int ltitle);
  extern void stdxwopen_(const char *filename, const char *title, 
                         int *ntries, int *istream, int *iok,
                         int lfilename, int ltitle);
  extern void stdxwrt_(int *ilbl, int *istream, int *lok);
  extern void stdflpyxsec_(int *ntries);
  extern void stdxend_(int *istream);
  extern void heplst_(int *mlst);
};

// =================================================================================
class HepSplit
{
  std::string  fInfile;
  std::string  fOutfilePrefix;
  int  fMaxRead;
  int  fNWperFile;
  int  fWriteBeginRecord;

  int  fNFile;
  int  fNSeqFrom;
  int  fNReadTotal;
  int  fNWrite;
  int  fNWriteTotal;
  int  fInStream;
  int  fOutStream;
  int  fNEventsSkip;
  int  fNEventsRead;

public:
  HepSplit();
  int  Arguments(int argc, char* argv[]);
  void PrintHelp();
  int  Run();

};

// =====================================================
HepSplit::HepSplit()
{
  heplun_.lnhwrt=0;
  heplun_.lnhrd=23;
  heplun_.lnhout=6;
  heplun_.lnhdcy=0;
  heplun_.lnhpdf=0;
  heplun_.lnhdmp=0;
  mcfio_init_();

  fInfile=std::string("input.stdhep");
  fOutfilePrefix=std::string("output");
  fMaxRead=0;
  fNWperFile=300;
  fWriteBeginRecord=1;

  fNFile=0;
  fNSeqFrom=0;
  fNReadTotal=0;
  fNWrite=0;
  fNWriteTotal=0;
  fInStream=0;
  fOutStream=0;
  fNEventsRead=0;
  fNEventsSkip=0;
}

//======================================================
int HepSplit::Arguments(int argc, char *argv[])
{
//  std::cout << " # of arguments=" << argc << std::endl;
  int ip=1;
  while( ip < argc ) {
    std::string a(argv[ip]);
    if( a == std::string("--infile") ) {
      fInfile=std::string(argv[ip+1]);
      ip++;
    }
    else if ( a == std::string("--outpref") ) {
      fOutfilePrefix=std::string(argv[ip+1]);
      ip++;
    }
    else if ( a == std::string("--maxread") ) {
      fMaxRead=atoi(argv[ip+1]);
      ip++;
    }
    else if ( a == std::string("--nw_per_file")) {
      fNWperFile=atoi(argv[ip+1]);
      ip++;
    }
    else if ( a == std::string("--write_begin_rec")) {
      fWriteBeginRecord=atoi(argv[ip+1]);
      ip++;
    }
    else if ( a == std::string("--skip_nevents")) {
      fNEventsSkip=atoi(argv[ip+1]);
      ip++;
    }
    else if ( a == std::string("--nseq_from")) {
      fNSeqFrom=atoi(argv[ip+1]);
      ip++;
    }
    else if ( a == std::string("--help") ) {
      PrintHelp();
      return -1;
    }
    else {
      std::cout << "Invarid arguments: last arguments is " << argv[ip] << std::endl;
      PrintHelp();
      return -1 ;
    }
    ip++;
  }
  std::cout << "==== hepsplit input parameters ==================================" << std::endl;
  std::cout << "  Input file             : " << fInfile << std::endl;
  std::cout << "  Output file prefix     : " << fOutfilePrefix << std::endl;
  std::cout << "  Output file sequence number from : " << fNSeqFrom << std::endl;
  std::cout << "  Max readin events (0=to EOF)  : " << fMaxRead << std::endl;
  std::cout << "  Number of events per file     : " << fNWperFile << std::endl;
  std::cout << "  Number of events to skip      : " << fNEventsSkip << std::endl;
  std::cout << "  Output Begin Run Record to each file ? (0,1)=(No,Yes) : " << fWriteBeginRecord << std::endl;
  return 0;
}

// ==========================================================================
void HepSplit::PrintHelp()
{  
  std::cout << "HepSplit" << std::endl
            << "  Split an input stdhep file to more than one files. " << std::endl
            << "  Command format " << std::endl
            << "     hepsplit [options] " << std::endl
            << "  Options " << std::endl
            << "     --infile [infile]    : (D=input.stdhep) Input stdhep file name  " << std::endl
            << "     --outpref [ofile]    : (D=output )    Prefix of output file. File names will be " <<std::endl
            << "                  output_NNN.stdhep, where NNN=[nseq_from]~999" << std::endl
            << "     --nseq_from [N]      : (D=0) File sequence number (NNN) of first output file" << std::endl
            << "     --maxread   [N]      : (D=0) Number of read recors, <= 0 to the end " << std::endl
            << "     --nw_per_file [N]    : (D=300) Number of events in each output file." << std::endl
            << "     --skip_nevents[N]    : (D=0) Number of events to skip before writting events." << std::endl
            << "     --write_begin_rec    : (D=1) Output begin record in each file or not. (0,1)=(No, Yes)" << std::endl;

}

//======================================================
int main(int argc, char* argv[])
{

  HepSplit hs;

  int iret=hs.Arguments(argc, argv);
  if( iret < 0 ) { return iret; }

  return hs.Run();

}

//======================================================
int HepSplit::Run()
{
  
  int ntries=fMaxRead;
  int iok=0;
  stdxrinit_(fInfile.c_str(), &ntries, &fInStream, &iok, fInfile.size());
  if( iok != 0 ) {
    std::cout << "Fatal error : failed to initialize read file." << fInfile << std::endl;
    return 1;
  }

  int flag=0;
  std::string outfile;
  int lastFileNumber=-1;
  while ( iok == 0 && ( fMaxRead ==0 || fNReadTotal < fMaxRead )) {
    stdxrd_(&flag, &fInStream, &iok);
    if( iok != 0 ) { //When reaching the end of the input file
//      std::cout << "Fatal error: read error. iok=" << iok << " Maybe end of file?"<<std::endl;
      std::cout << "Rread iok=" << iok << " Maybe end of file?"<<std::endl;
      std::cout << " Record written = " << fNWrite << "  (Number of written event records.)"<< std::endl;
      if ( &fOutStream != 0 ) { stdxend_(&fOutStream); }  //Close the output file
      
      std::cout << "==== hepsplit run statistics ==================================" << std::endl;
      std::cout << "  Number of read events : " << fNEventsRead << std::endl;
      std::cout << "  Number of skipped events : " << fNEventsSkip << std::endl;
      std::cout << "  Total number of written events : " << fNWriteTotal << std::endl;
      std::cout << "  File name of the last file : " << outfile << std::endl;
      std::cout << "  Number of files written : " << fNFile << std::endl;
      std::cout << "  File number of the last file : " << lastFileNumber << std::endl;
      std::cout << "================================== hepsplit completed. =======" << std::endl;
      return 2;
    }
    fNReadTotal++;

    if( flag == 200 ) {  // Got end-run record. 
      std::cout << "Got end-run record. Close file after writting end-run record."<<std::endl;
      std::cout << " Record written = " << fNWrite << "  (Number of written event records.)"<< std::endl;
      stdxwrt_(&flag, &fOutStream, &iok);  //write the current event

      if ( &fInStream != 0 ) { stdxend_(&fInStream); } //Close input file
      if ( fOutStream != 0 ) { stdxend_(&fOutStream); }//Close the last file in case of round number (unlikely to happen)

      std::cout << "==== hepsplit run statistics ==================================" << std::endl;
      std::cout << "  Number of read events : " << fNEventsRead << std::endl;
      std::cout << "  Number of skipped events : " << fNEventsSkip << std::endl;
      std::cout << "  Total number of written events : " << fNWriteTotal << std::endl;
      std::cout << "  File name of the last file : " << outfile << std::endl;
      std::cout << "  Number of files written : " << fNFile << std::endl;
      std::cout << "  File number of the last file : " << lastFileNumber << std::endl;
      std::cout << "================================== hepsplit completed. =======" << std::endl;
      return 2; 
    }
    if( fNWrite == fNWperFile || fOutStream == 0 ) { //We have enough events in this file, we need another, 
      //or no file yet
      if( fOutStream != 0 ) { // If we already have a file, close it
        std::cout << " Record written = " << fNWrite << "  (Number of written event records.)" << std::endl;
        stdxend_(&fOutStream);
        fOutStream=0;
      }
      fNFile++;
      std::stringstream cbuff;
      cbuff << "_" << std::setw(3) << std::setfill('0') << std::right << (fNSeqFrom+fNFile-1) << ".stdhep" << std::ends;
      lastFileNumber=fNSeqFrom+fNFile-1;
      outfile=fOutfilePrefix+cbuff.str();
      std::string  outtitle=std::string(stdhd1_.title);
      /// Now open a new file
      stdxwopen_(outfile.c_str(), outtitle.c_str(),&ntries, &fOutStream, &iok, outfile.size(), outtitle.size());
      if( iok != 0 ) {
        std::cout << "Fatal error: Unable to open output file " << outfile << std::endl;
        return 3;
      }
      std::cout << "Open output file " << outfile << std::endl;
      if( fWriteBeginRecord != 0 ) { //If we need to write the run record, do it "by hand"
        int thisflag=100; //This is the value to use for run header
        std::cout << "Write header title: " << std::string(stdhd1_.title) << std::endl;
        stdxwrt_(&thisflag, &fOutStream, &iok);
        if( iok != 0 ) {
          std::cout << "Fatal error: Failed to write begin run record to " << outfile << std::endl;
          return 4;
        }
      }
      fNWrite=0;//Reset the number of events written: none yet, only the run record maybe.
      //continue;  //we should not continue as the current event should still be written below.
    }
    int outok=0;
    if( flag!=100 ) { // Avoid write two Begin Run Record, written above when file is opened
      fNEventsRead++;
      if ( fNEventsRead > fNEventsSkip ) {
        stdxwrt_(&flag, &fOutStream, &outok);  //write the current event
        if ( outok != 0 ){
          std::cout << "Fatal error: Failed to write record to current file" << std::endl;
          return 5;
        }
        fNWrite++;  //one event was written
        fNWriteTotal++;  //one more event was writen in total
      }
    } 
  }
  stdxend_(&fInStream);//Close input file
  if( fOutStream != 0 ) { stdxend_(&fOutStream); }//Close the last file in case of round number (unlikely to happen)
   
  std::cout << " Exceeded maximum number of read " << std::endl;
  std::cout << " Requested MAXREAD = " << fMaxRead << " Read " << fNReadTotal << " record." << std::endl;
  std::cout << " Record written = " << fNWrite << "  (Number of written event records.)" << std::endl;

  std::cout << "==== hepsplit run statistics ==================================" << std::endl;
  std::cout << "  Number of read events : " << fNEventsRead << std::endl;
  std::cout << "  Number of skipped events : " << fNEventsSkip << std::endl;
  std::cout << "  Total number of written events : " << fNWriteTotal << std::endl;
  std::cout << "  File name of the last file : " << outfile << std::endl;
  std::cout << "  Number of files written : " << fNFile << std::endl;
  std::cout << "  File number of the last file : " << lastFileNumber << std::endl;
  std::cout << "================================== hepsplit completed. =======" << std::endl;

  return 0;
}  
