#!/bin/bash 
# Example to run HepSplit program
#

# #########################################################################
# 1. Setup gcc/gfortran environment
# #########################################################################
. /cvmfs/sft.cern.ch/lcg/external/gcc/4.8.1/x86_64-slc6-gcc48-opt/setup.sh /cvmfs/sft.cern.ch/lcg/external 

# #########################################################################
# 2. Download the original stdhep file, if not exist.
# #########################################################################
if [ !-e E500-TDR_ws.P4f_sze_l.Gwhizard-1_95.eR.pR.I250035.003.stdhep ] ; then 
  dirac-dms-get-file /ilc/prod/ilc/mc-dbd/generated/500-TDR_ws/4f/E500-TDR_ws.P4f_sze_l.Gwhizard-1_95.eR.pR.I250035.003.stdhep
fi

# ##########################################################################
# 3. Run HepSplit and create splitted hepstd files.
# ##########################################################################
../hepsplit \
  --infile E500-TDR_ws.P4f_sze_l.Gwhizard-1_95.eR.pR.I250035.003.stdhep \
  --outpref E500-TDR_ws.P4f_sze_l.Gwhizard-1_95.eR.pR.I250035.003 \
  --nw_per_file 1000 \
  --nseq_from 17 \
  --skip_nevents 7680 \
  --maxread 10000 \
  2>&1 | tee  hepsplit-250035.003.log  

# make a list of splitted stdhep file.
# /bin/ls E500-TDR_ws.P4f_sze_l.Gwhizard-1_95.eR.pR.I250035.003*.stdhep > stdhep.list
