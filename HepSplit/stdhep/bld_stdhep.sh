#!/bin/bash

# build stdhep program using source files in downloads directory

which gcc
which gfortran

INSTALL_DEST=`pwd`

res=${INSTALL_DEST}/downloads
vers=5-06-01
vers2=`echo ${vers} | sed -e "s/-/\./g"`

# tar -xzf ${res}/stdhep-${vers}.tar.gz
# cvs -d :pserver:anonymous@cdcvs.fnal.gov:/cvs/pat_read_only co -d stdhep-${vers} stdhep

(
cd stdhep-${vers}
patch -p0 < ${res}/misc/stdhep-stdhep_arch.patch
patch -p0 < ${res}/misc/stdhep-stdhep_mcfio.patch
cd src/stdhep
patch -p0 < ${res}/misc/stdhep-stdxwinit.patch
cd ../..
export STDHEP_DIR=`pwd`

make all
)
ln -s stdhep-${vers} ${vers2}


