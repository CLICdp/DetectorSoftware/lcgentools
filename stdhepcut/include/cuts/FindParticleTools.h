#ifndef FINDPARTICLETOOLS
#define FINDPARTICLETOOLS

#include <EVENT/MCParticle.h>
#include <EVENT/LCCollection.h>
#include <list>

class FindParticleTools
{
 public:
  static std::list<EVENT::MCParticle *> FindTaus( EVENT::LCCollection *col );
  static std::list<EVENT::MCParticle *> FindQuarks( EVENT::LCCollection *col );
  static std::list<EVENT::MCParticle *> FindLeptons( EVENT::LCCollection *lcc , int pdg);

 protected:
  static bool LoopDaughtersForTau(EVENT::MCParticle *particle);
  static EVENT::MCParticle* LoopDaughtersForLastQuark(EVENT::MCParticle *particle,int pdg);

};

#endif
