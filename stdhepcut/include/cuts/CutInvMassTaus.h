/*
 * CutInvMassTaus.h
 *
 *  Created on: 22.02.2012
 *      Author: Astrid Muennich
 */

#ifndef CUTINVMASSTAUS_H_
#define CUTINVMASSTAUS_H_

#include "Cut.h"

/**
 * This is an example on how to create an additional cut
 * First: We have to inherit from Cut
 * 2nd,3rd ...: See cpp file
 */
class CutInvMassTaus: public Cut
{
public:
	CutInvMassTaus();
	virtual ~CutInvMassTaus();

	virtual bool evaluate(EVENT::LCCollection* lcc);

	virtual std::ostream& toString(std::ostream& out);

};

#endif /* CUTINVMASSTAUS_H_ */
