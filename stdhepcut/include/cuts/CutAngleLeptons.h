/*
 * CutAngleLeptons.h
 *
 *  Created on: 22.02.2012
 *      Author: Astrid Muennich
 */

#ifndef CUTANGLELEPTONS_H_
#define CUTANGLELEPTONS_H_

#include "Cut.h"

/**
 * This is an example on how to create an additional cut
 * First: We have to inherit from Cut
 * 2nd,3rd ...: See cpp file
 */
class CutAngleLeptons: public Cut
{
public:
	CutAngleLeptons();
	virtual ~CutAngleLeptons();

	virtual bool evaluate(EVENT::LCCollection* lcc);

	virtual std::ostream& toString(std::ostream& out);

};

#endif /* CUTANGLELEPTONS_H_ */
