/*
 * CutDiParticle.h
 *
 *  Created on: Mar 11, 2013
 *      Author: Christian Grefe, CERN
 */

#ifndef CUTDIPARTICLE_H_
#define CUTDIPARTICLE_H_

#include "Cut.h"

class CutDiParticle: public Cut {
public:
	CutDiParticle();
	virtual ~CutDiParticle();

	virtual bool evaluate(EVENT::LCCollection* lcc);

	virtual std::ostream& toString(std::ostream& out);
};

#endif /* CUTDIPARTICLE_H_ */
