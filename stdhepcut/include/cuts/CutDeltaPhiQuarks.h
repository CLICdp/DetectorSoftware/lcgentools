/*
 * CutDeltaPhiQuarks.h
 *
 *  Created on: 22.02.2012
 *      Author: Astrid Muennich
 */

#ifndef CUTDELTAPHIQUARKS_H_
#define CUTDELTAPHIQUARKS_H_

#include "Cut.h"

/**
 * This is an example on how to create an additional cut
 * First: We have to inherit from Cut
 * 2nd,3rd ...: See cpp file
 */
class CutDeltaPhiQuarks: public Cut
{
public:
	CutDeltaPhiQuarks();
	virtual ~CutDeltaPhiQuarks();

	virtual bool evaluate(EVENT::LCCollection* lcc);

	virtual std::ostream& toString(std::ostream& out);

};

#endif /* CUTDELTAPHIQUARKS_H_ */
