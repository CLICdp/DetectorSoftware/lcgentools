/*
 * CutInvMassQuarks.h
 *
 *  Created on: 22.02.2012
 *      Author: Astrid Muennich
 */

#ifndef CUTINVMASSQUARKS_H_
#define CUTINVMASSQUARKS_H_

#include "Cut.h"

/**
 * This is an example on how to create an additional cut
 * First: We have to inherit from Cut
 * 2nd,3rd ...: See cpp file
 */
class CutInvMassQuarks: public Cut
{
public:
	CutInvMassQuarks();
	virtual ~CutInvMassQuarks();

	virtual bool evaluate(EVENT::LCCollection* lcc);

	virtual std::ostream& toString(std::ostream& out);

};

#endif /* CUTINVMASSQUARKS_H_ */
