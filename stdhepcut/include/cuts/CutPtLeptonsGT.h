/**
 *  @file   stdhepcut/include/cuts/CutPtLeptonsGT.h
 * 
 *  @brief  Header file for the cut pt leptons gt class.
 */
#ifndef CUTPTLEPTONSGT_H_
#define CUTPTLEPTONSGT_H_

#include "Cut.h"

/**
 *  @brief  CutPtLeptonsGT class
 */
class CutPtLeptonsGT: public Cut
{
public:
    CutPtLeptonsGT();
    virtual ~CutPtLeptonsGT();
    virtual bool evaluate(EVENT::LCCollection* lcc);
    virtual std::ostream& toString(std::ostream& out);
};

#endif /* CUTPTLEPTONSGT_H_ */
