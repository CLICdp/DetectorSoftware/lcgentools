/*
 * CutDeltaPhiTaus.h
 *
 *  Created on: 22.02.2012
 *      Author: Astrid Muennich
 */

#ifndef CUTDELTAPHITAUS_H_
#define CUTDELTAPHITAUS_H_

#include "Cut.h"

/**
 * This is an example on how to create an additional cut
 * First: We have to inherit from Cut
 * 2nd,3rd ...: See cpp file
 */
class CutDeltaPhiTaus: public Cut
{
public:
	CutDeltaPhiTaus();
	virtual ~CutDeltaPhiTaus();

	virtual bool evaluate(EVENT::LCCollection* lcc);

	virtual std::ostream& toString(std::ostream& out);

};

#endif /* CUTDELTAPHITAUS_H_ */
