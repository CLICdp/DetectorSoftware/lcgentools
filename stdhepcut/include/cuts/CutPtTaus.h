/*
 * CutPtTaus.h
 *
 *  Created on: 22.02.2012
 *      Author: Astrid Muennich
 */

#ifndef CUTPTTAUS_H_
#define CUTPTTAUS_H_

#include "Cut.h"

/**
 * This is an example on how to create an additional cut
 * First: We have to inherit from Cut
 * 2nd,3rd ...: See cpp file
 */
class CutPtTaus: public Cut
{
public:
	CutPtTaus();
	virtual ~CutPtTaus();

	virtual bool evaluate(EVENT::LCCollection* lcc);

	virtual std::ostream& toString(std::ostream& out);

};

#endif /* CUTPTTAUS_H_ */
