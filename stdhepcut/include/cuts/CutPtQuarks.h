/*
 * CutPtQuarks.h
 *
 *  Created on: 22.02.2012
 *      Author: Astrid Muennich
 */

#ifndef CUTPTQUARKS_H_
#define CUTPTQUARKS_H_

#include "Cut.h"

/**
 * This is an example on how to create an additional cut
 * First: We have to inherit from Cut
 * 2nd,3rd ...: See cpp file
 */
class CutPtQuarks: public Cut
{
public:
	CutPtQuarks();
	virtual ~CutPtQuarks();

	virtual bool evaluate(EVENT::LCCollection* lcc);

	virtual std::ostream& toString(std::ostream& out);
	int _event;
};

#endif /* CUTPTQUARKS_H_ */
