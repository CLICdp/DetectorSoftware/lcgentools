/**
 *  @file   stdhepcut/include/cuts/CutAngleLeptonsGT.h
 * 
 *  @brief  Header file for the cut angle leptons gt class.
 */
#ifndef CUTANGLELEPTONSGT_H_
#define CUTANGLELEPTONSGT_H_

#include "Cut.h"

/**
 *  @brief  CutAngleLeptonsGT class
 */
class CutAngleLeptonsGT: public Cut
{
public:
    CutAngleLeptonsGT();
    virtual ~CutAngleLeptonsGT();
    virtual bool evaluate(EVENT::LCCollection* lcc);
    virtual std::ostream& toString(std::ostream& out);
};

#endif /* CUTANGLELEPTONSGT_H_ */
