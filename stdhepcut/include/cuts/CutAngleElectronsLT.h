/*
 *  CutAngleElectronsLT.h
 *  Created on: 07.02.2013
 *      Author: Strahinja Lukic
 *
 * Adapted from:
 * CutAngleLeptons.h
 *
 *  Created on: 22.02.2012
 *      Author: Astrid Muennich
 */

#ifndef CUTANGLEELECTRONS_H_
#define CUTANGLEELECTRONS_H_

#include "Cut.h"

/**
 * This is an example on how to create an additional cut
 * First: We have to inherit from Cut
 * 2nd,3rd ...: See cpp file
 */
class CutAngleElectronsLT: public Cut
{
public:
	CutAngleElectronsLT();
	virtual ~CutAngleElectronsLT();

	virtual bool evaluate(EVENT::LCCollection* lcc);

	virtual std::ostream& toString(std::ostream& out);

};

#endif /* CUTANGLEELECTRONS_H_ */
