/*
 * CutAngleTaus.h
 *
 *  Created on: 22.02.2012
 *      Author: Astrid Muennich
 */

#ifndef CUTANGLETAUS_H_
#define CUTANGLETAUS_H_

#include "Cut.h"

/**
 * This is an example on how to create an additional cut
 * First: We have to inherit from Cut
 * 2nd,3rd ...: See cpp file
 */
class CutAngleTaus: public Cut
{
public:
	CutAngleTaus();
	virtual ~CutAngleTaus();

	virtual bool evaluate(EVENT::LCCollection* lcc);

	virtual std::ostream& toString(std::ostream& out);

};

#endif /* CUTANGLETAUS_H_ */
