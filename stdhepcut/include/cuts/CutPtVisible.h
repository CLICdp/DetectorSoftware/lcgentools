/*
 * CutPtVisible.h
 *
 *  Created on: 29.04.2011
 *      Author: weuste
 */

#ifndef CUTPTVISIBLE_H_
#define CUTPTVISIBLE_H_

#include "Cut.h"

/**
 * This is an example on how to create an additional cut
 * First: We have to inherit from Cut
 * 2nd,3rd ...: See cpp file
 */
class CutPtVisible: public Cut
{
public:
	CutPtVisible();
	virtual ~CutPtVisible();

	virtual bool evaluate(EVENT::LCCollection* lcc);

	virtual std::ostream& toString(std::ostream& out);
};

#endif /* CUTPTVISIBLE_H_ */
