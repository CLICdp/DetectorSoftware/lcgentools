/*
 * CutAngleQuarks.h
 *
 *  Created on: 22.02.2012
 *      Author: Astrid Muennich
 */

#ifndef CUTANGLEQUARKS_H_
#define CUTANGLEQUARKS_H_

#include "Cut.h"

/**
 * This is an example on how to create an additional cut
 * First: We have to inherit from Cut
 * 2nd,3rd ...: See cpp file
 */
class CutAngleQuarks: public Cut
{
public:
	CutAngleQuarks();
	virtual ~CutAngleQuarks();

	virtual bool evaluate(EVENT::LCCollection* lcc);

	virtual std::ostream& toString(std::ostream& out);

};

#endif /* CUTANGLEQUARKS_H_ */
