/*
 * CutThetaQuarks.h
 *
 *  Created on: 22.02.2012
 *      Author: Astrid Muennich
 */

#ifndef CUTTHETAQUARKS_H_
#define CUTTHETAQUARKS_H_

#include "Cut.h"

/**
 * This is an example on how to create an additional cut
 * First: We have to inherit from Cut
 * 2nd,3rd ...: See cpp file
 */
class CutThetaQuarks: public Cut
{
public:
	CutThetaQuarks();
	virtual ~CutThetaQuarks();

	virtual bool evaluate(EVENT::LCCollection* lcc);

	virtual std::ostream& toString(std::ostream& out);

};

#endif /* CUTTHETAQUARKS_H_ */
