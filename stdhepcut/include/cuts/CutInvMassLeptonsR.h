/**
 *  @file   stdhepcut/include/cuts/CutInvMassLeptonsR.h
 * 
 *  @brief  Header file for the cut inv mass leptons r class.
 */
#ifndef CUTINVMASSLEPTONSR_H_
#define CUTINVMASSLEPTONSR_H_

#include "Cut.h"

/**
 *  @brief  CutInvMassLeptonsR class
 */
class CutInvMassLeptonsR: public Cut
{
public:
    CutInvMassLeptonsR();
    virtual ~CutInvMassLeptonsR();
    virtual bool evaluate(EVENT::LCCollection* lcc);
    virtual std::ostream& toString(std::ostream& out);
};

#endif /* CUTINVMASSLEPTONSR_H_ */
