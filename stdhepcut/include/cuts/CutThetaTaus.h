/*
 * CutThetaTaus.h
 *
 *  Created on: 22.02.2012
 *      Author: Astrid Muennich
 */

#ifndef CUTTHETATAUS_H_
#define CUTTHETATAUS_H_

#include "Cut.h"

/**
 * This is an example on how to create an additional cut
 * First: We have to inherit from Cut
 * 2nd,3rd ...: See cpp file
 */
class CutThetaTaus: public Cut
{
public:
	CutThetaTaus();
	virtual ~CutThetaTaus();

	virtual bool evaluate(EVENT::LCCollection* lcc);

	virtual std::ostream& toString(std::ostream& out);

 private:
	unsigned _event;
};

#endif /* CUTTHETATAUS_H_ */
