/*
 * StdhepCut.h
 *
 *  Created on: 29.04.2011
 *      Author: weuste
 */

#ifndef STDHEPCUT_H_
#define STDHEPCUT_H_

#include "Cut.h"
#include "StdhepWriter.h"
#include "UTIL/LCStdHepRdr.h"
#include "EVENT/LCCollection.h"

#include <list>

using namespace std;

class StdhepCut
{
public:
	StdhepCut();
	virtual ~StdhepCut();

	void printParams();

	void addInputFile(const char* filename)	{ _inFileList.push_back(filename); }
	void setCutFile(const char* filename)	{ _cutFile = filename; }
	void setOutFile(const char* filename)	{ _outFile = filename; }
	void setMaxEvents(int processMaxEvents) { _processMaxEvents = processMaxEvents; }
	void setMaxEventsDebug(int processMaxEvents) { _processMaxEventsDebug = processMaxEvents; }

	void run();

protected:
	const char*			_cutFile;
	const char* 		_outFile;
	list< const char* >	_inFileList;
	int					_processMaxEvents;
  int         _processMaxEventsDebug;

	list< Cut* >		_cutList;
	StdhepWriter*		_writer;
	UTIL::LCStdHepRdr*	_reader;

	int		_statsEventsTotal;
	int		_statsEventsAfterCut;
	int		_statsEventsWritten;

	void initCuts();

	void processFiles();
	void processEvent(EVENT::LCCollection* mcpCollection);
};

#endif /* STDHEPCUT_H_ */
