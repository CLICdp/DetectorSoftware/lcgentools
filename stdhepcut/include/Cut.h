/*
 * Cut.h
 *
 *  Created on: 29.04.2011
 *      Author: weuste
 */

#ifndef CUT_H_
#define CUT_H_

#include <EVENT/LCCollection.h>
#include <string>
#include <list>
#include <iostream>
#include <vector>

class Cut
{
public:
	static Cut* getCut(std::string name);
	static void printAvailableCuts();

	Cut(std::string name, unsigned nParams);
	virtual ~Cut();

	virtual bool evaluate(EVENT::LCCollection* lcc) = 0;

	virtual std::ostream & toString(std::ostream &out);


	std::string getName() { return _name; }
	void setParams(std::vector<double> params);

protected:
	std::string	_name;

	std::vector<double>	_params;
	unsigned			_nParams;

	static std::list< Cut* >*	_availableCuts;

};

std::ostream & operator<<(std::ostream &out, Cut &c);
std::ostream & operator<<(std::ostream &out, Cut *c);

#endif /* CUT_H_ */
