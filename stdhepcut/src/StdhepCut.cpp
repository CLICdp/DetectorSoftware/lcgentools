/*
 * StdhepCut.cpp
 *
 *  Created on: 29.04.2011
 *      Author: weuste
 */

#include "StdhepCut.h"

#include "EVENT/LCCollection.h"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <vector>
#include <cstdlib>

using namespace EVENT;
using namespace UTIL;
using namespace std;

StdhepCut::StdhepCut()
{
	_cutFile = "Cut.txt";
	_outFile = "out.stdhep";
	_processMaxEvents = -1;
  _processMaxEventsDebug = -1;
  
	_statsEventsAfterCut = 0;
	_statsEventsTotal = 0;
  _statsEventsWritten = 0;
  
	_reader = NULL;
	_writer = NULL;
}

StdhepCut::~StdhepCut()
{
	if (_reader)
	{
		delete _reader;
		_reader = NULL;
	}

	if (_writer)
	{
		delete _writer;
		_writer = NULL;
	}
}

void StdhepCut::printParams()
{
	cout << "Parameters:" << endl;
	cout << "Cut File: " << _cutFile << endl;
	cout << "Out File: " << _outFile << endl;
	cout << "In File(s):" << endl;
	for (list< const char* >::iterator it = _inFileList.begin(); it != _inFileList.end(); it++)
		cout << " " << *it << endl;
}

void StdhepCut::run()
{
	// read the cut file
	this->initCuts();

	// create the output file
	_writer = new StdhepWriter(_outFile);

	// process the input files
	this->processFiles();

	// write some stats
	cout << "Finished" << endl;
	cout << "Events passing cuts: " << _statsEventsAfterCut << endl;
	cout << "Events kept: " << _statsEventsWritten << endl;
	cout << "Events cutted : " << _statsEventsTotal - _statsEventsAfterCut << endl;
	cout << "Events total  : " << _statsEventsTotal << endl;

	// clean up
	_writer->close();

	if (_reader)
	{
		delete _reader;
		_reader = NULL;
	}

	delete _writer;
	_writer = NULL;
}


void StdhepCut::initCuts()
{
	cout << "Reading Cuts from " << _cutFile << endl;

	ifstream file(_cutFile);

	if (!file.is_open())
	{
		cout << "Could not open cut file" << endl;
		exit(-10);
	}

	string str;
	while( getline( file, str ) )
	{
		// remove everything after "#"
		int pos = str.find('#');
		str = str.substr(0, pos);

		// split the string
		vector < string > splitted;
		stringstream iss(str);
	    do
	    {
	        string sub;
	        iss >> sub;
	        if (sub.size() > 0)
	        	splitted.push_back(sub);
	    } while (iss);

	    // skip over emtpy lines
	    if (splitted.size() <= 1)
	    	continue;

	    // search for a cut with that names
	    Cut* c = Cut::getCut(splitted[0]);

	    if (!c)
	    {
	    	cout << "ERROR: Unknown cut with name '" << splitted[0] << "'" <<endl;
	    	exit(-12);
	    }

	    // check on how many parameters we have
	    vector <double> parameters;
	    for (unsigned i=1; i<splitted.size(); i++)
	    	parameters.push_back(atof(splitted[i].c_str()));

	    // set the cut's parameters
	    c->setParams(parameters);

	    // save the cut
	    _cutList.push_back(c);

	    cout << c << endl;
	}
}

void StdhepCut::processFiles()
{
	// loop over all input files
	for (list<const char*>::iterator itInFile = _inFileList.begin(); itInFile != _inFileList.end(); itInFile++)
	{
		// (re)create the reader for this file
		if (_reader)
			delete _reader;

		cout << "Opening input file " << *itInFile << endl;
		_reader = new UTIL::LCStdHepRdr(*itInFile);

		// loop over all events
		EVENT::LCCollection* lcc = _reader->readEvent();

		while (lcc != NULL)
		{
      //std::cout<<"lcc "<<lcc<<std::endl;
      
			processEvent(lcc);

      delete lcc;
      lcc=NULL;
      
			// break if we have enough events 
			if (_processMaxEventsDebug != -1 && _processMaxEventsDebug <= _statsEventsAfterCut)
      {
				return;
      }
      
			lcc = _reader->readEvent();
		}
	}
}

void StdhepCut::processEvent(EVENT::LCCollection* mcpCollection)
{
	_statsEventsTotal++;
  //std::cout<<"before"<<std::endl;
  
	// loop over all cuts and let them evaluate. Once a single cut returns false, we will drop this event
	for (list< Cut* >::iterator itCut = _cutList.begin(); itCut != _cutList.end(); itCut++)
	{
    //std::cout<<*itCut<<std::endl;
    
		if (!(*itCut)->evaluate(mcpCollection))
    {
			return;
    }
  }
  

	// only events passing all cuts are here. Write them into the stdhep file
	_statsEventsAfterCut++;
  
  if (_processMaxEvents!=-1){
    if ( _statsEventsAfterCut<=_processMaxEvents){
      _statsEventsWritten ++;
      //std::cout<<"write"<<std::endl;      
      _writer->writeEvent(mcpCollection);
      //std::cout<<"done"<<std::endl;      
    }
  }
  else {
    _statsEventsWritten ++;
    //std::cout<<"write"<<std::endl;      
    _writer->writeEvent(mcpCollection);
    //  std::cout<<"done"<<std::endl;      
  }
  
}
