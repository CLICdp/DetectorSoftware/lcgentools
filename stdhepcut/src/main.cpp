/*
 * main.cpp
 *
 *  Created on: 29.04.2011
 *      Author: weuste
 */


#include <EVENT/LCCollection.h>
#include <UTIL/LCStdHepRdr.h>
#include <UTIL/LCTOOLS.h>

#include "StdhepCut.h"
#include "Cut.h"

#include <cstdlib>
#include <iostream>


using namespace std;


int main(int argc, char **argv)
{

	cout << "StdHep generator level cutting tool version 0.1" << endl;
	cout << "created by Lars Weuste - MPP" << endl << endl;

	StdhepCut stdhepCut;

	// check options

	if (argc < 2)
	{
		cout << "Please see " << argv[0] << " -h for help" << endl;
		return 0;
	}

	for (int i = 1; i < argc; ++i)
	{
		if (argv[i][0] == '-')
		{
			if (argv[i][1] == 'h')
			{
				cout << "Usage: " << argv[0] << "[-l] [-m <maxEvents>] [-d <maxEvents>] -o <output.stdhep> -c <cut.txt> <input.stdhep> [<moreInput.stdhep>]" << endl;
				cout << "Will write events from <input.stdhep> that pass the cuts provided in <cut.txt> to <output.stdhep>" << endl;
				cout << "-l                will list all available cuts" << endl;
				cout << "-m <maxEvents>    stop writing after <maxEvents> passed the cut" << endl;
				cout << "-d <maxEvents>    stop processing after <maxEvents> passed the cut (debug)" << endl;
				cout << endl;
				return 0;
			}
			else if (argv[i][1] == 'l')
			{
				Cut::printAvailableCuts();
				return 0;
			}
			else if (argv[i][1] == 'm')
			{
				i++;
				if (i >= argc)
				{
					cout << "Missing parameter after maxEvents -m" << endl;
					return -1;
				}
				stdhepCut.setMaxEvents(atoi(argv[i]));
			}
			else if (argv[i][1] == 'd')
			{
				i++;
				if (i >= argc)
				{
					cout << "Missing parameter after maxEventsForDebug -d" << endl;
					return -1;
				}
				stdhepCut.setMaxEventsDebug(atoi(argv[i]));
			}
			else if (argv[i][1] == 'o')
			{
				i++;
				if (i >= argc)
				{
					cout << "Missing parameter after outputfile -o" << endl;
					return -1;
				}
				stdhepCut.setOutFile(argv[i]);
			}
			else if (argv[i][1] == 'c')
			{
				i++;
				if (i >= argc)
				{
					cout << "Missing parameter after cutfile -c" << endl;
					return -2;
				}
				stdhepCut.setCutFile(argv[i]);
			}
			else
			{
				cout << "Unknown option" << endl;
				return -3;
			}
		}
		else
			stdhepCut.addInputFile(argv[i]);
	}

	stdhepCut.printParams();

	stdhepCut.run();

	return 0;
}
