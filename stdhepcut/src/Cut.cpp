/*
 * Cut.cpp
 *
 *  Created on: 29.04.2011
 *      Author: weuste
 */

#include "Cut.h"
#include <cstdlib>

std::list< Cut* >*	Cut::_availableCuts = NULL;

using namespace std;

Cut::Cut(std::string name, unsigned nParams)  : _name(name), _nParams(nParams)
{
	if (!Cut::_availableCuts)
		Cut::_availableCuts = new std::list< Cut* >;
	Cut::_availableCuts->push_back(this);

}

Cut::~Cut()
{

}

void Cut::printAvailableCuts()
{
	cout << "Available cuts(no. parameters):" << endl;
	for (list< Cut* >::iterator it = Cut::_availableCuts->begin(); it != Cut::_availableCuts->end(); it++)
	{
		cout << " " << (*it)->_name << "(" << (*it)->_nParams << ")" << endl;
	}
	cout << endl;
}

Cut* Cut::getCut(std::string name)
{
	for (list< Cut* >::iterator it = Cut::_availableCuts->begin(); it != Cut::_availableCuts->end(); it++)
	{
		if ((*it)->_name == name)
			return *it;
	}

	return NULL;
}

void Cut::setParams(std::vector<double> params)
{
	if (params.size() != _nParams)
	{
		cout << "ERROR: The cut " << _name << " needs " << _nParams << " parameters (cut values), but we got " << params.size() << endl;
		exit(-20);
	}
	_params = params;
}

std::ostream & Cut::toString(std::ostream &out)
{
	out << _name;
	for (std::vector<double>::iterator it = _params.begin(); it != _params.end(); it++)
		out << " " << *it;
	return out;
}

std::ostream & operator<<(std::ostream &out, Cut &c)
{
	return c.toString(out);
}

std::ostream & operator<<(std::ostream &out, Cut *c)
{
	return c->toString(out);
}
