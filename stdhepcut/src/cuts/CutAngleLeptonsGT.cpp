/**
 *  @file   stdhepcut/src/cuts/CutAngleLeptonsGTGT.cpp
 * 
 *  @brief  Implementation of the cut angle leptons gt class.
 */
#include "cuts/CutAngleLeptonsGT.h"
#include "cuts/FindParticleTools.h"

#include <CLHEP/Vector/LorentzVector.h>

#include <cmath>
#include <cfloat>

// we have to create one instance of this cut. This gets then automatically available for the program
CutAngleLeptonsGT aCutAngleLeptonsGT;

// Cut on the polar angle of either of two leptons, applying minimum angle constraint in degrees.
// cut parameter _params[0] is used to specify which lepton by giving the pdg: 11, 13 or 15
// The name is set to leptonAngle_GT
// The number of parameters (1 pdg code, 1 threshold) is set to 2.
CutAngleLeptonsGT::CutAngleLeptonsGT() : Cut("leptonAngle_GT", 2)
{
    // TODO Auto-generated constructor stub
}

CutAngleLeptonsGT::~CutAngleLeptonsGT()
{
    // TODO Auto-generated destructor stub
}

// This has to be overwritten by the new cut class
// Here we decide whether this event passes this cut or not
// we have access to the threshold as it is in the cuts.txt file via _params
bool CutAngleLeptonsGT::evaluate(EVENT::LCCollection* lcc)
{
    //find all final leptons
    std::list<EVENT::MCParticle*> myleptons = FindParticleTools::FindLeptons(lcc, (int)_params[0]);

    //check that there are only two leptons, otherwise something is wrong
    if (myleptons.size() != 2)
    {
        std::cout << "WARNING: Wrong number of leptons found: "<< myleptons.size() << std::endl;
        std::cout << "Cut: leptons " << _params[0] << ", theta > " << _params[1] << " not applied." << std::endl;
        return true;
    }

    // loop over all final leptons
    for (std::list<EVENT::MCParticle*>::const_iterator iter = myleptons.begin(); iter != myleptons.end(); ++iter)
    {
        EVENT::MCParticle* mcp = *iter;
        CLHEP::HepLorentzVector lepton(0,0,0,0);
        lepton.setPx(mcp->getMomentum()[0]);
        lepton.setPy(mcp->getMomentum()[1]);
        lepton.setPz(mcp->getMomentum()[2]);
        lepton.setE(mcp->getEnergy());

        if (lepton.theta() * 180 / M_PI < _params[1])
            return false;
    }

    return true;
}

std::ostream& CutAngleLeptonsGT::toString(std::ostream& out)
{
    return out << " leptons " << _params[0] << "theta > " <<_params[1];
}


