/**
 *  @file   stdhepcut/src/cuts/CutInvMassLeptonsR.cpp
 * 
 *  @brief  Implementation of the cut inv mass leptons r class.
 */
#include "cuts/CutInvMassLeptonsR.h"
#include "cuts/FindParticleTools.h"

#include <CLHEP/Vector/LorentzVector.h>

#include <cmath>
#include <cfloat>

// we have to create one instance of this cut. This gets then automatically available for the program
CutInvMassLeptonsR aCutInvMassLeptonsR;

// Cut on the invariant mass of two leptons, allowing only a specified range in GeV to pass.
// cut parameter _params[0] is used to specify which lepton by giving the pdg: 11, 13 or 15
// The name is set to leptonInvMass_R
// The number of parameters (1 pdg code, 1 low value, 1 high value) is set to 3.
CutInvMassLeptonsR::CutInvMassLeptonsR() : Cut("leptonInvMass_R", 3)
{
    // TODO Auto-generated constructor stub
}

CutInvMassLeptonsR::~CutInvMassLeptonsR()
{
    // TODO Auto-generated destructor stub
}

// This has to be overwritten by the new cut class
// Here we decide whether this event passes this cut or not
// we have access to the threshold as it is in the cuts.txt file via _params
bool CutInvMassLeptonsR::evaluate(EVENT::LCCollection* lcc)
{
    //find all final leptons
    std::list<EVENT::MCParticle*> myleptons= FindParticleTools::FindLeptons(lcc, (int)_params[0]);

    //check that there are only two leptons, otherwise something is wrong
    if (myleptons.size() != 2)
    {
        std::cout << "WARNING: Wrong number of leptons found: "<< myleptons.size() << std::endl;
        std::cout << "Cut: leptons " << _params[0] << ", low mass " << _params[1] << " , high mass " << _params[2] << ", not applied." << std::endl;
        return true;
    }

    CLHEP::HepLorentzVector lepton1(0,0,0,0);
    CLHEP::HepLorentzVector lepton2(0,0,0,0);
    std::list<EVENT::MCParticle*>::const_iterator iter = myleptons.begin();
    EVENT::MCParticle* mcp1 = *iter;
    iter++;
    EVENT::MCParticle* mcp2 = *iter;

    lepton1.setPx(mcp1->getMomentum()[0]);
    lepton1.setPy(mcp1->getMomentum()[1]);
    lepton1.setPz(mcp1->getMomentum()[2]);
    lepton1.setE(mcp1->getEnergy());

    lepton2.setPx(mcp2->getMomentum()[0]);
    lepton2.setPy(mcp2->getMomentum()[1]);
    lepton2.setPz(mcp2->getMomentum()[2]);
    lepton2.setE(mcp2->getEnergy());

    const double Minv((lepton1 + lepton2).invariantMass());

    if ((Minv < _params[1]) || (Minv > _params[2]))
    {
        return false;
    }
    else
    {
        return true;
    }
}

std::ostream& CutInvMassLeptonsR::toString(std::ostream& out)
{
    return out << " leptons " << _params[0] << ", " << _params[1] << " < inv. Mass < "<<_params[2];
}


