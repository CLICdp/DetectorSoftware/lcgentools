/*
 * CutAngleLeptons.cpp
 *
 *  Created on: 22.02.2012
 *      Author: Astrid Muennich
 */

#include "cuts/CutAngleLeptons.h"
#include "cuts/FindParticleTools.h"
#include <CLHEP/Vector/LorentzVector.h>
#include <cmath>
#include <cfloat>

// we have to create one instance of this cut. This gets then automatically available for the program
CutAngleLeptons aCutAngleLeptons;

// This cut is intended to cut on the polar angle of leptons in processes like ee->eeqq
// cut parameter _params[0] is used to specify which lepton by giving the pdg: 11,13 or 15
// The name is set to tauPt_GT
// The number of parameters (1 threshold) is set to 1.
CutAngleLeptons::CutAngleLeptons() : Cut("leptonAngle_LT", 3)
{
	// TODO Auto-generated constructor stub

}

CutAngleLeptons::~CutAngleLeptons()
{
	// TODO Auto-generated destructor stub
}

// This has to be overwritten by the new cut class
// Here we decide whether this event passes this cut or not
// we have access to the threshold as it is in the cuts.txt file via _params
bool CutAngleLeptons::evaluate(EVENT::LCCollection* lcc)
{
  //find all final taus
  std::list<EVENT::MCParticle*> myleptons= FindParticleTools::FindLeptons( lcc ,(int)_params[0]);
   
  bool theta_OK=true;
  // loop over all final leptons
  // check if one is in angular region with specific energy
  for (std::list<EVENT::MCParticle*>::iterator iter = myleptons.begin(); iter != myleptons.end(); ++iter)
    {
      EVENT::MCParticle* mcp = *iter;
      if(mcp->getEnergy()>_params[1])
	{
	  CLHEP::HepLorentzVector tau(0,0,0,0);
	  tau.setPx(mcp->getMomentum ( ) [0]);
	  tau.setPy(mcp->getMomentum ( ) [1]);
	  tau.setPz(mcp->getMomentum ( ) [2]);
	  tau.setE(mcp->getEnergy   ( ));
	  double angle=tau.theta()*180/M_PI;
	  if(angle>90)
	    angle=180-angle;
	  if(angle>_params[2])
	    {
	      theta_OK= false;
	      break;
	    }
	}
    }
  
  return theta_OK;
}

std::ostream& CutAngleLeptons::toString(std::ostream& out)
{
  return out << "Lepton: "<<_params[0]<<", theta >"<<_params[2] <<", Energy<  "<<_params[1];
}


