/*
 * CutThetaQuarks.cpp
 *
 *  Created on: 22.02.2012
 *      Author: Astrid Muennich
 */

#include "cuts/CutThetaQuarks.h"
#include "cuts/FindParticleTools.h"
#include <CLHEP/Vector/LorentzVector.h>
#include <cmath>

// we have to create one instance of this cut. This gets then automatically available for the program
CutThetaQuarks aCutThetaQuarks;

// This cut is intended to cut on the [polar angle  of the quarks within a certain range in deg
// The name is set to quarkTheta_GT
// The number of parameters (1 threshold) is set to 1.
CutThetaQuarks::CutThetaQuarks() : Cut("quarkTheta_R", 2)
{
	// TODO Auto-generated constructor stub

}

CutThetaQuarks::~CutThetaQuarks()
{
	// TODO Auto-generated destructor stub
}

// This has to be overwritten by the new cut class
// Here we decide whether this event passes this cut or not
// we have access to the threshold as it is in the cuts.txt file via _params
bool CutThetaQuarks::evaluate(EVENT::LCCollection* lcc)
{
  //find all final quarks
  std::list<EVENT::MCParticle*> myquarks = FindParticleTools::FindQuarks( lcc );

  //check that there are only two quarks, otherwise something is wrong
   if(myquarks.size()!=2)
    {
      std::cout<<"WARNING: Wrong number of quarks found: "<<myquarks.size()<<std::endl;
      std::cout<<"Cut "<< _params[0] << " < theta < "<<_params[1]<<" not applied"<<std::endl;
      return true;
    }


  // loop over all final quarks
  bool theta_OK=true;
  for (std::list<EVENT::MCParticle*>::iterator iter = myquarks.begin(); iter != myquarks.end(); ++iter)
    {
      EVENT::MCParticle* mcp = *iter;
      CLHEP::HepLorentzVector quark(0,0,0,0);
      quark.setPx(mcp->getMomentum ( ) [0]);
      quark.setPy(mcp->getMomentum ( ) [1]);
      quark.setPz(mcp->getMomentum ( ) [2]);
      quark.setE(mcp->getEnergy   ( ));
      if(quark.theta()*180/M_PI<_params[0] || quark.theta()*180/M_PI>_params[1])
	{
	  theta_OK=false;
	  break;
	}
    }
  
  return theta_OK;
}

std::ostream& CutThetaQuarks::toString(std::ostream& out)
{
  return out << _params[0] << " < theta < "<<_params[1];
}


