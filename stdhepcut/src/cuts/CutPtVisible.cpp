/*
 * CutPtVisible.cpp
 *
 *  Created on: 29.04.2011
 *      Author: weuste
 */

#include "cuts/CutPtVisible.h"

#include <EVENT/MCParticle.h>

#include <cmath>

// we have to create one instance of this cut. This gets then automatically available for the program
CutPtVisible aCutPtVisible;

// This cut is intended to cut on the visible p_t greater than a certain threshold
// The name is set to visiblePt_GT
// The number of parameters (1 threshold) is set to 1.
CutPtVisible::CutPtVisible() : Cut("visiblePt_GT", 1)
{
	// TODO Auto-generated constructor stub

}

CutPtVisible::~CutPtVisible()
{
	// TODO Auto-generated destructor stub
}

// This has to be overwritten by the new cut class
// Here we decide whether this event passes this cut or not
// we have access to the threshold as it is in the cuts.txt file via _params
bool CutPtVisible::evaluate(EVENT::LCCollection* lcc)
{
	// calculate the pt of all visible particles
	double px = 0;
	double py = 0;

	// loop over all particles
	for (int i = 0; i < lcc->getNumberOfElements(); ++i)
	{
		// get the particle
		EVENT::MCParticle* mcp = (EVENT::MCParticle*)lcc->getElementAt(i);

		// only visible particles: nothing that has a daughter (i.e. is decayed)
		if (mcp->getDaughters().size() > 0)
			continue;

		// nothing with generator status other than 1 (i.e. decayed)
		if (mcp->getGeneratorStatus() != 1)
			continue;

		// no SUSY particles and no neutrinos
		int pdg = (int)fabs(mcp->getPDG());
		if (pdg == 12 || pdg == 14 || pdg == 16 || pdg > 100000)
			continue;

		const double* mom = mcp->getMomentum();
		px += mom[0];
		py += mom[1];
	}

	double pt = sqrt((px*px) + (py*py));

	return _params[0] < pt;
}

std::ostream& CutPtVisible::toString(std::ostream& out)
{
	return out << _params[0] << " < pt";
}
