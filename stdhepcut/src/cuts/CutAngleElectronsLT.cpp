/*
 *  CutAngleElectronsLT.cpp
 *  Created on: 07.02.2013
 *      Author: Strahinja Lukic
 *
 * Adapted from:
 * CutAngleLeptons.cpp
 *  Created on: 22.02.2012
 *      Author: Astrid Muennich
 *
 */

#include "cuts/CutAngleElectronsLT.h"
#include "cuts/FindParticleTools.h"
#include <CLHEP/Vector/LorentzVector.h>
#include <cmath>
#include <cfloat>

// we have to create one instance of this cut. This gets then automatically available for the program
CutAngleElectronsLT aCutAngleElectronsLT;

// This cut is intended to cut on the polar angle of leptons in processes like ee->eeqq
// cut parameter _params[0] is used to specify which lepton by giving the pdg: 11,13 or 15
// The name is set to electronAngle_LT
// The number of parameters (1 threshold) is set to 1.
CutAngleElectronsLT::CutAngleElectronsLT() : Cut("electronAngle_LT", 1)
{
	// TODO Auto-generated constructor stub

}

CutAngleElectronsLT::~CutAngleElectronsLT()
{
	// TODO Auto-generated destructor stub
}

// This has to be overwritten by the new cut class
// Here we decide whether this event passes this cut or not
// we have access to the threshold as it is in the cuts.txt file via _params
bool CutAngleElectronsLT::evaluate(EVENT::LCCollection* lcc)
{
  //find all final electrons
  std::list<EVENT::MCParticle*> myleptons= FindParticleTools::FindLeptons( lcc , 11);
   
  bool theta_OK=true;
  // loop over all final leptons
  // check if one is in angular region with specific energy
  for (std::list<EVENT::MCParticle*>::iterator iter = myleptons.begin(); iter != myleptons.end(); ++iter)
    {
      EVENT::MCParticle* mcp = *iter;
      // The cut checks only final electrons
      if(mcp->getGeneratorStatus() == 1)
      {
		  CLHEP::HepLorentzVector electron(0,0,0,0);
		  electron.setPx(mcp->getMomentum ( ) [0]);
		  electron.setPy(mcp->getMomentum ( ) [1]);
		  electron.setPz(mcp->getMomentum ( ) [2]);
		  electron.setE(mcp->getEnergy   ( ));
		  double angle=electron.theta()*180/M_PI;
		  if(angle>90)
			angle=180-angle;
		  if(angle>_params[0])
			{
			  theta_OK= false;
			  break;
			}
      }
    }
  
  return theta_OK;
}

std::ostream& CutAngleElectronsLT::toString(std::ostream& out)
{
  return out << "theta < "<<_params[0];
}


