#include "cuts/FindParticleTools.h"
#include <cstdlib>
#include <cmath>


std::list<EVENT::MCParticle *> FindParticleTools::FindTaus( EVENT::LCCollection *lcc )
{
  std::list<EVENT::MCParticle *> alltaus;
  // loop over all particles
  for (int i = 0; i < lcc->getNumberOfElements(); ++i)
    {
      // get the particle
      EVENT::MCParticle* mcp = (EVENT::MCParticle*)lcc->getElementAt(i);
      //find all final taus 
      //mcp->getDaughters().size()>1 is necessary due to bug in LCStdHepRdr which sometimes
      //looses daughter relations. Thus look only at real 2 body decay taus, others should be intermediate anyway
      if(std::abs(mcp->getPDG())==15 && mcp->getDaughters().size()>1)
	{
	  //check if tau has a tau as daughter, then not final tau
	  bool hasTau=LoopDaughtersForTau(mcp);
	  if(hasTau==false)
	    alltaus.push_back(mcp);
	}
    }
  return alltaus;
}

std::list<EVENT::MCParticle *> FindParticleTools::FindQuarks( EVENT::LCCollection *lcc )
{
  std::list<EVENT::MCParticle *> firstquarks;
  std::list<EVENT::MCParticle *> lastquarks;
  // loop over all particles to determine first quark pair
  for (int i = 0; i < lcc->getNumberOfElements(); ++i)
    {
      // get the particle
      EVENT::MCParticle* mcp = (EVENT::MCParticle*)lcc->getElementAt(i);
      if(std::abs(mcp->getPDG())<6 && (std::abs(mcp->getParents()[0]->getPDG())==11 ||
				       std::abs(mcp->getParents()[0]->getPDG())==22) 
	 && mcp->getParents()[0]->getGeneratorStatus()!=1)
	firstquarks.push_back(mcp);
    }
  
  // loop over the initial quarks
  for (std::list<EVENT::MCParticle*>::iterator iter = firstquarks.begin(); iter != firstquarks.end(); ++iter)
    {
      EVENT::MCParticle* mcp = *iter;
      EVENT::MCParticle* lq= LoopDaughtersForLastQuark(mcp,std::abs(mcp->getPDG()));
      lastquarks.push_back(lq);
    }
  return lastquarks;
}


bool FindParticleTools::LoopDaughtersForTau(EVENT::MCParticle *particle)
{
  for(unsigned int d=0;d<particle->getDaughters().size();d++)
    { 
      EVENT::MCParticle *daughter=particle->getDaughters()[d];
      if(std::abs(daughter->getPDG())==15)
	return true;
      
      if ( LoopDaughtersForTau(daughter) )
	return true;
    }
  return false;
}

EVENT::MCParticle* FindParticleTools::LoopDaughtersForLastQuark(EVENT::MCParticle *particle, int pdg)
{
  // check the first generation for daughters already hadronising
  for(unsigned int d=0;d<particle->getDaughters().size();d++)
    { 
      EVENT::MCParticle *daughter=particle->getDaughters()[d];
      //stop if hadronisation starts
      if(std::abs(daughter->getPDG())==21 || std::abs(daughter->getPDG())==94 || std::abs(daughter->getPDG())==92 )
	return particle;
    }
  //now check the relevant quarks recursively
  for(unsigned int d=0;d<particle->getDaughters().size();d++)
    { 
      EVENT::MCParticle *daughter=particle->getDaughters()[d];

      if(std::abs(daughter->getPDG())==pdg)
   	return LoopDaughtersForLastQuark(daughter,pdg) ;
    }
  return particle;
}


std::list<EVENT::MCParticle *> FindParticleTools::FindLeptons( EVENT::LCCollection *lcc , int pdg)
{
  std::list<EVENT::MCParticle *> alleptons;
  // std::cout<<"FindParticleTools::FindLeptons"<<std::endl;
  // loop over all particles
  for (int i = 0; i < lcc->getNumberOfElements(); ++i)
    {
      // get the particle
      EVENT::MCParticle* mcp = (EVENT::MCParticle*)lcc->getElementAt(i);
      //find all final leptons (electron or muon)
      if(std::abs(mcp->getPDG())==pdg && mcp->getGeneratorStatus()==1)
	{
	  //assumption of parents not allowed as it can't be replicated in analysis
	  //EVENT::MCParticle *parent=mcp->getParents()[0];
	  //  if(std::abs(parent->getPDG())==pdg || std::abs(parent->getPDG())==94 || std::abs(parent->getPDG())==92) 
	    {
	      // std::cout<<"Selected "<<pdg<<" "<<mcp->getEnergy()<<" "<<mcp->getParents()[0]->getPDG()<<" "<<parent->getGeneratorStatus()<<std::endl;
	      alleptons.push_back(mcp);
	      //  std::cout<<"Writing it"<<std::endl;
	    }
	}
    }
  return alleptons;
}
