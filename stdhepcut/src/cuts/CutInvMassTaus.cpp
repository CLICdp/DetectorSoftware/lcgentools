/*
 * CutInvMassTaus.cpp
 *
 *  Created on: 22.02.2012
 *      Author: Astrid Muennich
 */

#include "cuts/CutInvMassTaus.h"
#include "cuts/FindParticleTools.h"
#include <CLHEP/Vector/LorentzVector.h>
#include <cmath>

// we have to create one instance of this cut. This gets then automatically available for the program
CutInvMassTaus aCutInvMassTaus;

// This cut is intended to cut on the invariant mass of the two tau leptons within a certain range in GeV
// The name is set to tauInvMass_GT
// The number of parameters (1 threshold) is set to 1.
CutInvMassTaus::CutInvMassTaus() : Cut("tauInvMass_R", 2)
{
	// TODO Auto-generated constructor stub

}

CutInvMassTaus::~CutInvMassTaus()
{
	// TODO Auto-generated destructor stub
}

// This has to be overwritten by the new cut class
// Here we decide whether this event passes this cut or not
// we have access to the threshold as it is in the cuts.txt file via _params
bool CutInvMassTaus::evaluate(EVENT::LCCollection* lcc)
{
  //find all final taus
  std::list<EVENT::MCParticle*> mytaus = FindParticleTools::FindTaus( lcc );

  //check that there are only two taus, otherwise something is wrong
  if(mytaus.size()!=2)
    {
      std::cout<<"WARNING: Wrong number of taus found: "<<mytaus.size()<<std::endl;
      std::cout<<"Cut pt>"<< _params[0] <<" not applied"<<std::endl;
      return true;
    }


  CLHEP::HepLorentzVector tau1(0,0,0,0);
  CLHEP::HepLorentzVector tau2(0,0,0,0);
  std::list<EVENT::MCParticle*>::iterator iter = mytaus.begin(); 
  EVENT::MCParticle* mcp1 = *iter;
  iter++;
  EVENT::MCParticle* mcp2 = *iter;

  tau1.setPx(mcp1->getMomentum ( ) [0]);
  tau1.setPy(mcp1->getMomentum ( ) [1]);
  tau1.setPz(mcp1->getMomentum ( ) [2]);
  tau1.setE(mcp1->getEnergy   ( ));
  tau2.setPx(mcp2->getMomentum ( ) [0]);
  tau2.setPy(mcp2->getMomentum ( ) [1]);
  tau2.setPz(mcp2->getMomentum ( ) [2]);
  tau2.setE(mcp2->getEnergy   ( ));
  
  double Minv=(tau1+tau2).invariantMass();  
  if(Minv <_params[0] || Minv>_params[1])
    return false;
  else
    return true;
}

std::ostream& CutInvMassTaus::toString(std::ostream& out)
{
  return out << _params[0] << " < inv. Mass < "<<_params[1];
}


