/**
 *  @file   stdhepcut/src/cuts/CutPtLeptonsGT.cpp
 * 
 *  @brief  Implementation of the cut inv mass leptons r class.
 */
#include "cuts/CutPtLeptonsGT.h"
#include "cuts/FindParticleTools.h"

#include <CLHEP/Vector/LorentzVector.h>

#include <cmath>
#include <cfloat>

// we have to create one instance of this cut. This gets then automatically available for the program
CutPtLeptonsGT aCutPtLeptonsGT;

// Cut on the pT of two leptons, obtained via vector sum of the lepton momenta.
// cut parameter _params[0] is used to specify which lepton by giving the pdg: 11, 13 or 15
// The name is set to leptonInvMass_R
// The number of parameters (1 pdg code, 1 threshold) is set to 2.
CutPtLeptonsGT::CutPtLeptonsGT() : Cut("leptonPt_GT", 2)
{
    // TODO Auto-generated constructor stub
}

CutPtLeptonsGT::~CutPtLeptonsGT()
{
    // TODO Auto-generated destructor stub
}

// This has to be overwritten by the new cut class
// Here we decide whether this event passes this cut or not
// we have access to the threshold as it is in the cuts.txt file via _params
bool CutPtLeptonsGT::evaluate(EVENT::LCCollection* lcc)
{
    //find all final leptons
    std::list<EVENT::MCParticle*> myleptons= FindParticleTools::FindLeptons(lcc, (int)_params[0]);

    //check that there are only two leptons, otherwise something is wrong
    if (myleptons.size() != 2)
    {
        std::cout<<"WARNING: Wrong number of leptons found: "<<myleptons.size()<<std::endl;
        std::cout << "Cut: leptons " << _params[0] << ", pT > " << _params[1] << " not applied" << std::endl;
        return true;
    }

    std::list<EVENT::MCParticle*>::const_iterator iter = myleptons.begin(); 
    EVENT::MCParticle* mcp1 = *iter;
    iter++;
    EVENT::MCParticle* mcp2 = *iter;

    const double mom[3] = {mcp1->getMomentum()[0] + mcp2->getMomentum()[0],
        mcp1->getMomentum()[1] + mcp2->getMomentum()[1],
        mcp1->getMomentum()[2] + mcp2->getMomentum()[2]};
    const double pt(std::sqrt((mom[0] * mom[0]) + (mom[1] * mom[1])));

    return _params[1] < pt;
}

std::ostream& CutPtLeptonsGT::toString(std::ostream& out)
{
    return out << " pdg code " << _params[0]  << "pt > "<< _params[1] ;
}


