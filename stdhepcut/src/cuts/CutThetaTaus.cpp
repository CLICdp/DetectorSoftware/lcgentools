/*
 * CutThetaTaus.cpp
 *
 *  Created on: 22.02.2012
 *      Author: Astrid Muennich
 */

#include "cuts/CutThetaTaus.h"
#include "cuts/FindParticleTools.h"
#include <CLHEP/Vector/LorentzVector.h>
#include <cmath>

// we have to create one instance of this cut. This gets then automatically available for the program
CutThetaTaus aCutThetaTaus;

// This cut is intended to cut on the polar angle of tau leptons within a certain range in degrees
// The name is set to tauTheta_GT
// The number of parameters (1 threshold) is set to 1.
CutThetaTaus::CutThetaTaus() : Cut("tauTheta_R", 2), _event(0)
{
	// TODO Auto-generated constructor stub
  
}

CutThetaTaus::~CutThetaTaus()
{
	// TODO Auto-generated destructor stub
}

// This has to be overwritten by the new cut class
// Here we decide whether this event passes this cut or not
// we have access to the threshold as it is in the cuts.txt file via _params
bool CutThetaTaus::evaluate(EVENT::LCCollection* lcc)
{
//  std::cout << "Event "<< _event++ << std::endl;

  //find all final taus
  std::list<EVENT::MCParticle*> mytaus = FindParticleTools::FindTaus( lcc );

  //check that there are only two taus, otherwise something is wrong
  if(mytaus.size()!=2)
     {
      std::cout<<"WARNING: Wrong number of taus found: "<<mytaus.size()<<std::endl;
      std::cout<<"Cut "<< _params[0] << " < theta < "<<_params[1]<<" not applied"<<std::endl;
      return true;
    }


  // loop over all final taus
  bool theta_OK=true;
  for (std::list<EVENT::MCParticle*>::iterator iter = mytaus.begin(); iter != mytaus.end(); ++iter)
    {
      EVENT::MCParticle* mcp = *iter;
      CLHEP::HepLorentzVector tau(0,0,0,0);
      tau.setPx(mcp->getMomentum ( ) [0]);
      tau.setPy(mcp->getMomentum ( ) [1]);
      tau.setPz(mcp->getMomentum ( ) [2]);
      tau.setE(mcp->getEnergy   ( ));
      if(tau.theta()*180/M_PI<_params[0] || tau.theta()*180/M_PI>_params[1])
	{
	  theta_OK=false;
	  break;
	}
    }
  
  return theta_OK;
}

std::ostream& CutThetaTaus::toString(std::ostream& out)
{
  return out << _params[0] << " < theta < "<<_params[1];
}


