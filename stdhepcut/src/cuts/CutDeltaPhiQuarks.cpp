/*
 * CutDeltaPhiQuarks.cpp
 *
 *  Created on: 22.02.2012
 *      Author: Astrid Muennich
 */

#include "cuts/CutDeltaPhiQuarks.h"
#include "cuts/FindParticleTools.h"
#include <CLHEP/Vector/LorentzVector.h>
#include <cmath>

// we have to create one instance of this cut. This gets then automatically available for the program
CutDeltaPhiQuarks aCutDeltaPhiQuarks;

// This cut is intended to cut on the DeltaPhi of quarks within a certain range in deg
// The name is set to quarkDeltaPhi_GT
// The number of parameters (1 threshold) is set to 1.
CutDeltaPhiQuarks::CutDeltaPhiQuarks() : Cut("quarkDeltaPhi_R", 2)
{
	// TODO Auto-generated constructor stub

}

CutDeltaPhiQuarks::~CutDeltaPhiQuarks()
{
	// TODO Auto-generated destructor stub
}

// This has to be overwritten by the new cut class
// Here we decide whether this event passes this cut or not
// we have access to the threshold as it is in the cuts.txt file via _params
bool CutDeltaPhiQuarks::evaluate(EVENT::LCCollection* lcc)
{
  //find all final quarks
  std::list<EVENT::MCParticle*> myquarks = FindParticleTools::FindQuarks( lcc );

  //check that there are only two quarks, otherwise something is wrong
  if(myquarks.size()!=2)
    {
      std::cout<<"WARNING: Wrong number of quarks found: "<<myquarks.size()<<std::endl;
      std::cout<<"Cut "<< _params[0] << " < DeltaPhi < "<<_params[1]<<" not applied"<<std::endl;
      return true;
    }

  // loop over all final quarks
  CLHEP::HepLorentzVector quark1(0,0,0,0);
  CLHEP::HepLorentzVector quark2(0,0,0,0);
  std::list<EVENT::MCParticle*>::iterator iter = myquarks.begin(); 
  EVENT::MCParticle* mcp1 = *iter;
  iter++;
  EVENT::MCParticle* mcp2 = *iter;

  quark1.setPx(mcp1->getMomentum ( ) [0]);
  quark1.setPy(mcp1->getMomentum ( ) [1]);
  quark1.setPz(mcp1->getMomentum ( ) [2]);
  quark1.setE(mcp1->getEnergy   ( ));
  quark2.setPx(mcp2->getMomentum ( ) [0]);
  quark2.setPy(mcp2->getMomentum ( ) [1]);
  quark2.setPz(mcp2->getMomentum ( ) [2]);
  quark2.setE(mcp2->getEnergy   ( ));

  double Accoplan = fabs(quark1.phi() - quark2.phi())*180/M_PI;
  if (Accoplan > 180) 
    Accoplan = -Accoplan+360;
  //if not in range
  if(Accoplan <_params[0] || Accoplan>_params[1])
    return false;
  else
    return true;
}

std::ostream& CutDeltaPhiQuarks::toString(std::ostream& out)
{
  return out << _params[0] << " < DeltaPhi < "<<_params[1];
}


