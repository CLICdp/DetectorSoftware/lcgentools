/*
 * CutPtTaus.cpp
 *
 *  Created on: 22.02.2012
 *      Author: Astrid Muennich
 */

#include "cuts/CutPtTaus.h"
#include "cuts/FindParticleTools.h"

#include <cmath>
#include <cfloat>

// we have to create one instance of this cut. This gets then automatically available for the program
CutPtTaus aCutPtTaus;

// This cut is intended to cut on the p_t of tau leptons greater than a certain threshold in GeV
// The name is set to tauPt_GT
// The number of parameters (1 threshold) is set to 1.
CutPtTaus::CutPtTaus() : Cut("tauPt_GT", 1)
{
	// TODO Auto-generated constructor stub

}

CutPtTaus::~CutPtTaus()
{
	// TODO Auto-generated destructor stub
}

// This has to be overwritten by the new cut class
// Here we decide whether this event passes this cut or not
// we have access to the threshold as it is in the cuts.txt file via _params
bool CutPtTaus::evaluate(EVENT::LCCollection* lcc)
{
  //find all final taus
  std::list<EVENT::MCParticle*> mytaus= FindParticleTools::FindTaus( lcc );
 
  //check that there are only two taus, otherwise something is wrong
  if(mytaus.size()!=2)
    {
      std::cout<<"WARNING: Wrong number of taus found: "<<mytaus.size()<<std::endl;
      std::cout<<"Cut pt>"<< _params[0] <<" not applied"<<std::endl;
      return true;
    }

  //smallest pt of any tau in event
  double pt=DBL_MAX;
  // loop over all final taus
  for (std::list<EVENT::MCParticle*>::iterator iter = mytaus.begin(); iter != mytaus.end(); ++iter)
    {
      EVENT::MCParticle* mcp = *iter;
      const double* mom = mcp->getMomentum();
      double pt_here = sqrt((mom[0]*mom[0]) + (mom[1]*mom[1]));
      if(pt_here< pt)
	pt=pt_here;
    }
  
  return _params[0] < pt;
}

std::ostream& CutPtTaus::toString(std::ostream& out)
{
	return out << "pt > "<< _params[0] ;
}


