/*
 * CutDiParticle.cpp
 *
 *  Created on: Mar 11, 2013
 *      Author: Christian Grefe, CERN
 */

#include "cuts/CutDiParticle.h"
#include "cuts/FindParticleTools.h"

#include "CLHEP/Vector/LorentzVector.h"
#include "EVENT/MCParticle.h"

#include <cmath>

using CLHEP::HepLorentzVector;
using EVENT::MCParticle;
using std::list;
using std::pow;
using std::sqrt;

CutDiParticle aCutDiParticle;

/*
 * Cut on several variables of a two particle system.
 *
 * Parameter 0 defines the PDGID of both particles (independent of charge, set positive only)
 * Parameters 1 and 2 define the accepted polar angle range in the interval 0 - 90 degrees for each particle
 * Parameters 3 and 4 define the accepted transverse momentum range for each particle
 * Parameters 5 and 6 define the accepted energy range for each particle
 * Parameters 7 and 8 define the accepted invariant mass range of the two particle system
 *
 * At least two particles of the desired PDGID have to pass the single particle selections on the polar angle,
 * transverse momentum and energy. Only if at least one of the possible two particle systems pass the invariant
 * mass requirements the event will be kept.
 */
CutDiParticle::CutDiParticle() :
		Cut("diParticle", 9) {

}

CutDiParticle::~CutDiParticle() {

}

bool CutDiParticle::evaluate(EVENT::LCCollection* lcc) {
	int pdgid = (int) _params[0];
	double minTheta = _params[1];
	double maxTheta = _params[2];
	double minPt = _params[3];
	double maxPt = _params[4];
	double minEnergy = _params[5];
	double maxEnergy = _params[6];
	double minInvMass = _params[7];
	double maxInvMass = _params[8];

	// find all final state particles with matching (absolute) PDGID
	list<MCParticle*> particles = FindParticleTools::FindLeptons(lcc, pdgid);
	list<HepLorentzVector> passedParticles;
	for (list<MCParticle*>::const_iterator itParticle = particles.begin(); itParticle != particles.end();
			++itParticle) {
		MCParticle* particle = *itParticle;

		// check particle energy
		double energy = particle->getEnergy();
		if (energy > maxEnergy or energy < minEnergy) {
			continue;
		}

		// check transverse momentum
		const double* p = particle->getMomentum();
		double pT = sqrt(pow(p[0], 2) + pow(p[1], 2));
		if (pT > maxPt or pT < minPt) {
			continue;
		}

		// check polar angle projected into interval [0,90]
		HepLorentzVector v(p[0], p[1], p[2], energy);
		double theta = 90. - 180. * std::fabs(v.theta() / M_PI - 0.5);
		if (theta > maxTheta or theta < minTheta) {
			continue;
		}

		// this particle passed all cuts, add it to the list
		passedParticles.push_back(v);
	}

	list<HepLorentzVector>::const_iterator itParticle = passedParticles.begin();
	list<HepLorentzVector>::const_iterator itOtherParticle;
	for (; itParticle != passedParticles.end(); ++itParticle) {
		for (itOtherParticle = itParticle; itOtherParticle != passedParticles.end(); ++itOtherParticle) {
			if (itParticle == itOtherParticle) {
				continue;
			}
			HepLorentzVector diParticle(*itParticle);
			diParticle += *itOtherParticle;
			double invMass = diParticle.invariantMass();
			if (invMass >= minInvMass and invMass <= maxInvMass) {
				// It is sufficient to find at least one combination
				return true;
			}
		}
	}

	return false;
}

std::ostream& CutDiParticle::toString(std::ostream& out) {
	return out << "Particle ID: " << (int) _params[0] << ", " << _params[1] << " deg < theta < " << _params[2]
			<< " deg, " << _params[3] << " GeV < pT < " << _params[4] << " GeV, " << _params[5] << " GeV < energy < "
			<< _params[6] << " GeV, " << _params[7] << " GeV < mass < " << _params[8] << " GeV, " << std::endl;
}
