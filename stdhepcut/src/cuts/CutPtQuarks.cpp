/*
 * CutPtQuarks.cpp
 *
 *  Created on: 22.02.2012
 *      Author: Astrid Muennich
 */

#include "cuts/CutPtQuarks.h"
#include "cuts/FindParticleTools.h"
#include <CLHEP/Vector/LorentzVector.h>
#include <cmath>
#include <cfloat>

// we have to create one instance of this cut. This gets then automatically available for the program
CutPtQuarks aCutPtQuarks;

// This cut is intended to cut on the p_t of quarks greater than a certain threshold in GeV
// The name is set to quarkPt_GT
// The number of parameters (1 threshold) is set to 1.
CutPtQuarks::CutPtQuarks() : Cut("quarkPt_GT", 1), _event(0)
{
	// TODO Auto-generated constructor stub

}

CutPtQuarks::~CutPtQuarks()
{
	// TODO Auto-generated destructor stub
}

// This has to be overwritten by the new cut class
// Here we decide whether this event passes this cut or not
// we have access to the threshold as it is in the cuts.txt file via _params
bool CutPtQuarks::evaluate(EVENT::LCCollection* lcc)
{
  //   std::cout << "Event "<< _event++ << std::endl;
   //find all final quarks
  std::list<EVENT::MCParticle*> myquarks = FindParticleTools::FindQuarks( lcc );

  //check that there are only two quarks, otherwise something is wrong
  if(myquarks.size()!=2)
    {
      std::cout<<"WARNING: Wrong number of quarks found: "<<myquarks.size()<<std::endl;
      std::cout<<"Cut pt>"<< _params[0] <<" not applied"<<std::endl;
      return true;
    }

  //smallest pt of any quark in event
  double pt=DBL_MAX;
  // loop over all final quarks
  for (std::list<EVENT::MCParticle*>::iterator iter = myquarks.begin(); iter != myquarks.end(); ++iter)
    {
      EVENT::MCParticle* mcp = *iter;
      const double* mom = mcp->getMomentum();
      double pt_here = sqrt((mom[0]*mom[0]) + (mom[1]*mom[1]));
      if(pt_here< pt)
	pt=pt_here;
    }
  
  return _params[0] < pt;
}

std::ostream& CutPtQuarks::toString(std::ostream& out)
{
	return out << "pt > "<< _params[0] ;
}

