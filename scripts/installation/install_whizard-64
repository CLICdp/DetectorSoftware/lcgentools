#!/bin/sh
FILES_FOR_WHIZARD_64=/afs/desy.de/user/b/berggren/public/files-for-whizard-64
define_cernlib(){
  remove_motif(){
    # removes the compilation of binaries needing the motif
    # libraries (Xm etc.). Only a limited set of cernlib executables
    # will be built, but all the object libraries will be OK.
    patch -p0 <<-"EOF"
	--- 2005/src/packlib/kuip/Imakefile      1997-11-28 09:26:56.000000000 -0800
	+++ 2005/src/packlib/kuip/Imakefile      2010-05-16 21:24:20.000000000 -0700
	@@ -4,12 +4,6 @@
	
	 LIBDIRS= code_kuip
	
	-#ifndef CERNLIB_WINNT
	-LIBDIRS := $(LIBDIRS) code_motif
	-#else
	-LIBDIRS := $(LIBDIRS) code_windows
	-#endif
	-
	 SUBDIRS= $(LIBDIRS) programs examples kuip
	
	 TopOfPackage(kuip)
	--- 2005/src/pawlib/paw/Imakefile        2000-06-27 08:27:03.000000000 -0700
	+++ 2005/src/pawlib/paw/Imakefile        2010-05-16 21:34:08.000000000 -0700
	@@ -9,19 +9,6 @@
	
	 LIBDIRS= code cpaw cdf ntuple mlpfit
	
	-#ifndef CERNLIB_WINNT
	-LIBDIRS :=$(LIBDIRS) $(MOTIF_DIRS) $(PAWPP_DIRS)
	-#endif
	-
	-#if defined(CERNLIB_UNIX) && !defined(CERNLIB_WINNT)
	-MotifDependantMakeVar(PAWPP_DIRS,xbae)
	-LIBDIRS := $(LIBDIRS) $(PAWPP_DIRS)
	-#endif
	-
	-#if defined(CERNLIB_VAXVMS)
	-MotifDependantMakeVar(PAWPP_DIRS,xbaevms)
	-LIBDIRS := $(LIBDIRS) $(PAWPP_DIRS)
	-#endif
	
	 SUBDIRS= $(LIBDIRS) programs piafs hbpiaf stagerd paw

EOF
  }
  local here
  here=`pwd`
  if [[ "$1" ==  "install" ]] 
  then
    mkdir cernlib_64
  fi
  cd cernlib_64/
  if [[ "$1" ==  "install" ]] 
  then
    wget http://www-zeuthen.desy.de/linear_collider/cernlib/new/cernlib-2005-all-new.tgz
    wget http://www-zeuthen.desy.de/linear_collider/cernlib/new/cernlib.2005.corr.2009.06.13.tgz
    tar -xzf cernlib-2005-all-new.tgz
    mv cernlib.2005.corr.tgz cernlib.2005.corr.tgz-old
    ln -s cernlib.2005.corr.2009.06.13.tgz cernlib.2005.corr.tgz
    alias mkdirhier=mkdir
    # to use gfortran44 on SLC5 to compile at CERN:
    # for whizard to compile with gfortran, 4.4 is needed, and
    # there seems to be some subtle compatibility issues with
    # different versions of gfortran.
    # It seems that if whizard is compiled with ifort (not available
    # on the NAF), of if cernlib is compiled with g77 (not available on SLC5), 
    # whizard with gfortran4.4 it works. 
    mkdir bin
    cd bin
    ln -s `which gfortran44` gfortran
    ln -s `which gcc44` gcc
    cd ..
  fi
  cd bin
  export PATH=`pwd`:$PATH
  cd ..
  if [[ "$1" ==  "install" ]] 
  then
    export FC=gfortran
    if ! locate libXm.a >> /dev/null
    then
      csplit Install_cernlib 65
      mv xx01 Install_cernlib_p2
      mv xx00 Install_cernlib_p1
      bash Install_cernlib_p1
      remove_motif
      bash ./Install_cernlib_p2
    else
      ./Install_cernlib
    fi
    ln -s 2005 pro
  fi
  . cernlib_env
  cd $here
}
install_pythia(){
  patch_pythia(){
    local here
    here=`pwd`
    cd $1
    #--- replace upinit.f by a (true) dummy:
    mv upinit.f upinit.f-orig
    cat<<-"EOF" > upinit.f
	 
	C*********************************************************************
	 
	C...UPINIT
	C...Dummy routine, to be replaced by a user implementing external
	C...processes. Is supposed to fill the HEPRUP commonblock with info
	C...on incoming beams and allowed processes.
	
	
	      SUBROUTINE UPINIT
	      END
EOF
    # patch pydecy:
    patch -p0 <<-"EOF"
	--- pydecy.f	2010-06-07 15:07:08.000000000 +0200
	+++ /afs/naf.desy.de/user/b/berggren/new/pydecy.f	2010-06-25 17:25:03.000000000 +0200
	@@ -134,10 +134,10 @@
	         IF(KFORIG.NE.0.OR.MSTJ(28).EQ.2) THEN
	           CALL PYTAUD(ITAU,IORIG,KFORIG,NDECAY)
	           DO 200 II=NSAV+1,NSAV+NDECAY
	-            K(II,1)=1
	-            K(II,3)=IP
	-            K(II,4)=0
	-            K(II,5)=0
	+C            K(II,1)=1
	+C            K(II,3)=IP
	+C            K(II,4)=0
	+C            K(II,5)=0
	   200     CONTINUE
	           N=NSAV+NDECAY
	         ENDIF
	@@ -814,7 +814,14 @@
	       IF(K(IP,1).LE.10) K(IP,1)=11
	       IF(MMIX.EQ.1.AND.MSTJ(26).EQ.2.AND.K(IP,1).EQ.11) K(IP,1)=12
	       K(IP,4)=NSAV+1
	+      if (mstj(28) .ge. 1 ) then
	+         do iii=nsav+1,n
	+            if ( k(iii,3) .eq. ip ) then
	+               k(ip,5)=iii
	+            endif
	+         enddo
	+      else
	       K(IP,5)=N
	- 
	+      endif
	       RETURN
	       END
EOF
     cd $here 
  }
  local here
  here=`pwd`
  # Add latest pythia:
  # ( three different version of the version ...)
  ver=$1
#echo $ver
#echo $ver | sed -e s/\./_/
  ver2=$(echo $ver | sed -e s/\\./_/ | tr -d ".")
#echo $ver2
  ver3=$(echo $ver2 | tr -d "_")
#echo $ver3
  wget http://home.fnal.gov/~rhatcher/build_pythia6.sh.txt
  mv build_pythia6.sh.txt build_pythia6.sh
  bash build_pythia6.sh $ver gfortran
  cd v${ver2}/src
  patch_pythia `pwd`
  make
  cd ../lib/
  mv liblund.a ../../cernlib_64/2005/lib/libpythia${ver3}.a
  cd $here
}
define_ocaml(){
  local here
  here=`pwd`
  #
  # now, Ocaml:
  mayor=$2
  minor=$3
  if [[ "$1" ==  "install" ]] 
  then
    wget http://caml.inria.fr/pub/distrib/ocaml-${mayor}/ocaml-${mayor}.${minor}.tar.gz
    tar -xzf ocaml-${mayor}.${minor}.tar.gz
  fi
  cd ocaml-${mayor}.${minor}
  if [[ "$1" ==  "install" ]] 
  then
    ./configure -prefix `pwd`
    # ( sometimes the make gets a 'Text file busy' error, this is why
    #   it is re-run.  boot/ocamlrun gives an error, but makes the
    #   file available in the the following step)
    make world.opt > make.log 
    sleep 1
    boot/ocamlrun
    boot/ocamlrun
    boot/ocamlrun
    sleep 1
    make world.opt > make2.log
    sleep 1 
    boot/ocamlrun
    boot/ocamlrun
    boot/ocamlrun
    sleep 1
    make install  > makeinstall.log 
    sleep 1
    boot/ocamlrun
    boot/ocamlrun
    boot/ocamlrun
    sleep 1
     make install  > makeinstall2.log
  fi
  #  (if you already have ocaml, make sure these are defined correctly)
  export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:`pwd`/lib
  export PATH=$PATH:`pwd`/bin
  cd $here
}
define_stdhep(){
  local here
  here=`pwd`
  #
  # Now, stdhep:
  #
  vers=$2
  if [[ "$1" ==  "install" ]] 
  then
    wget http://cepa.fnal.gov/dist/stdhep/stdhep-${vers}.tar.gz
    tar -xzf stdhep-${vers}.tar.gz
  fi
  cd stdhep-${vers}
  if [[ "$1" ==  "install" ]] 
  then
    export STDHEP_DIR=`pwd`
    gmake all
  fi
  # (if you already have stdhep-64 make sure this is set. Whizard
  # and stdhep itself have different opinions on what this should
  # point to)
  export STDHEP_DIR=`pwd`/lib
  cd $here
}
define_tauola(){
  local here
  here=`pwd`
  ln -s `which gfortran44` gfortran
  ln -s `which gcc44` gcc
  #
  #  Now, our TAUOLA :
  #
  if [[ "$1" ==  "install" ]] 
  then
    tar -xzf $2 
  fi
  cd tauola_desy/TAUOLA/
  if [[ "$1" ==  "install" ]] 
  then
    export WHZ=`pwd`/../..
    #
    #  (make sure make.inc points to the right architecture)
    #
    # Make photos. 4kD-all to get the consitant shape of /HEPEVT/
    cd photos-F/
    make 4kD-all
    cd ..
    # make the rest. There will be an error, but it doesn't
    # matter, it happens after the libraries have been made...
    bash make_tauola_script
    cd tauola
    make
    mv glib.a libtauola.a
    cd ..
    cd photos
    mv glib.a libphotos.a
    cd ..
  fi
  export TAUOLALIB=`pwd`/tauola
  export PHOTOSLIB=`pwd`/photos
  cd $here
}
define_whizard(){
  export F95=`which gfortran44`
  # (for gfortran, needed for our user.f90 and for a6f. Means that there is
  # no restriction on line-length. The f95 standard defines max line-length
  # to 132, which apearently ifort and lf95 ingnores)
  export FCFLAGS="-ffree-line-length-0"
  export F77=$F95

}
make_our_whizard(){
  install_default_whizard(){
    #
    # Finally: whizard
    #
    local here
    here=`pwd`
    vers=$1
    if [ -d whizard-${vers} ] 
    then
      cd whizard-${vers}
      svn update
    else
      svn co svn+ssh://svn.cern.ch/reps/lcgentools/trunk/Whizard_${vers} whizard-${vers}
    fi
    #wget http://www.hepforge.org/archive/whizard/whizard-${vers}.tgz
    #mkdir whizard-${vers}
    #cd whizard-${vers}
    #tar -xzf ../whizard-${vers}.tgz    #
    cd $here
  }
  install_lumi_files(){
    #
    #  the lumi-files
    #
    local here
    here=`pwd`
    tar -xzf $1
    mkdir /tmp/lumi_linker_directory/
    cd /tmp/lumi_linker_directory/
    ln -s ${here}/energy_spread/* .
    cd $here
  }
  install_a6f(){
    patch_a6f(){

    # patches:
    #
    #   ilc_tauola_mod: jorig -> jtau bug-fix, transmit
    #   full decay-chain back to pythia
    #
    patch -p0 <<-"EOF"
	--- ilc_tauola_mod.f90      2010-06-03 17:27:25.000000000 +0200
	+++ new/ilc_tauola_mod.f90      2010-06-08 17:38:23.000000000 +0200
	@@ -263,7 +263,11 @@
	
	     check_wma_exist: if(count(wma(1:n_wma)%k_after_fsr_index.eq.jtau).gt.0) then
	
	-       i_loc=maxloc(wma(1:n_wma)%pol,mask=wma(1:n_wma)%k_after_fsr_index.eq.jorig)
	+       if ( jtau .ne. 0 ) then
	+          i_loc=maxloc(wma(1:n_wma)%pol,mask=wma(1:n_wma)%k_after_fsr_index.eq.jtau)
	+       else
	+          i_loc=maxloc(wma(1:n_wma)%pol)
	+       endif
	        j_wma=i_loc(1)
	
	
	@@ -344,11 +348,25 @@
	 !    print *, " do_dexay jtau,jorig,jforig,nhep= ", jtau,jorig,jforig,nhep
	     call heplst(1)
	     loop_products: do i=3,nhep
	-       check_isthep: if(isthep(i).eq.1) then
	+!       check_isthep: if(isthep(i).eq.1) then
	           nproducts=nproducts+1
	           p(n+nproducts,:)=phep(:,i)
	+          if ( isthep(i) .eq. 1 ) then
	+             k(n+nproducts,1)=1
	+             k(n+nproducts,4)=0
	+             k(n+nproducts,5)=0
	+          else
	+             k(n+nproducts,1)=11
	+             k(n+nproducts,4)=jdahep(1,i)+n-2
	+             k(n+nproducts,5)=jdahep(2,i)+n-2
	+          endif
	           k(n+nproducts,2)=idhep(i)
	-       end if check_isthep
	+          if ( abs(idhep(jmohep(1,i))) .ne. 15 ) then
	+              k(n+nproducts,3)=jmohep(1,i)+n-2
	+          else
	+              k(n+nproducts,3)=jtau
	+          endif
	+!       end if check_isthep
	     end do loop_products
	
	     return
EOF
    #
    # ilc_fragment_call.f90: move call to ilc_tauola_ini
    #
    patch -p0 <<-"EOF"
	--- ilc_fragment_call.f90   2010-06-23 12:33:59.000000000 +0200
	+++ new/ilc_fragment_call.f90   2010-06-23 12:34:34.000000000 +0200
	@@ -44,7 +44,6 @@
	   call setmuonmass
	
	
	-  if(mstj(28).gt.0) call ilc_tauola_ini
	
	   check_ncount_0: if(ncount.le.nprint_max.or.ncount.eq.14.or.ncount.eq.47.or.ncount.eq.24975) then
	      print "(' on entry to user_fragment call;   ncount=',i12)", ncount
	@@ -130,6 +129,7 @@
	      else check_shower_enable
	         nfermion=0
	      end if check_shower_enable
	+     if(mstj(28).gt.0) call ilc_tauola_ini
	      !
	      ! call check_ep_conservation(f_ep_conserved)
	      ! check_epa_pt: if(mcu%epa_pt.and.f_ep_conserved) then
EOF
    #
    #   Makefile: ifort -> gfortran (with  -ffree-line-length-0),
    #   remove force_trace_.o
    #
    patch -p0 <<-"EOF"
	--- Makefile	2010-06-25 16:22:53.000000000 +0200
	+++ /afs/naf.desy.de/user/b/berggren/scratch/Makefile-a6f-w-tauola-31may	2010-05-31 23:27:14.000000000 +0200
	@@ -3,9 +3,9 @@
	 #
	 #
	 SHELL         = /bin/csh
	-F77           = gfortran -c
	-FC            = ifort -c -I. -I$(WH195)/whizard-src -I$(WH195)/kinds-src 
	-FFLAGS        = 
	+F77           = gfortran -c -ffree-line-length-0
	+FC            = gfortran -c -ffree-line-length-0 -I. -I$(WH195)/whizard-src -I$(WH195)/kinds-src 
	+#FFLAGS        = -ffree-line-length-0
	 #
	 LIBINC      = $(A6F)/lib/libinclude.a
	 #
	@@ -40,13 +40,13 @@
	 $(LIBINC)(a6f_pyxtot.o)\
	 $(LIBINC)(hide_beamstr_elec.o) $(LIBINC)(restore_beamstr_elec.o)\
	 $(LIBINC)(shower_spectator_electron.o)\
	-$(LIBINC)(force_trace_.o)\
	 $(LIBINC)(ini_evt_prt.o)  $(LIBINC)(a6f_driver.o)  $(LIBINC)(user_fragment_init.o)  $(LIBINC)(user_fragment_call.o) $(LIBINC)(user_fragment_print.o)\
	 $(LIBINC)(handle_pytaud.o)\
	 $(LIBINC)(ilc_fragment_init.o)  $(LIBINC)(ilc_fragment_call.o) $(LIBINC)(ilc_fragment_print.o)
	 #$(LIBINC)(pyrand.o) $(LIBINC)(pyklim.o)\
	 #$(LIBINC)(pyevnt.o)\
	 #$(LIBINC)(pyxtot.o)\
	+#$(LIBINC)(force_trace_.o)\
	 
	 
	 all:	$(OBJS)
EOF

    }
    local here
    here=`pwd`
    cd whizard-${1}
    cd a6f/include
    #patch_a6f
    cd $here
  }
  install_our_user(){
    local here
    here=`pwd`
    cd whizard-$1
    cd whizard-src/
    cp $2 user.f90
    cd $here
  }
  compile_whizard(){
    local here
    here=`pwd`
    cd whizard-$1
    #
    #  and configure: g2c because stdhep is compiled by g77, pytaud.o explicitly
    #  since otherwise the dummy version in pythia will be loaded. I prefere
    #  explicit paths rather than -L -l combinations, which gives less control
    # 
    ./configure \
     LIBS=" /usr/lib/gcc/x86_64-redhat-linux/3.4.6/libg2c.a" USERLIBS="$(pwd)/a6f/include/pytaud.o $(pwd)/a6f/lib/libinclude.a $TAUOLALIB/libtauola.a $PHOTOSLIB/libphotos.a"
    #
    #   make our stuff. the kinds and limits modules must exist before 
    #
    cd kinds-src
    make install
    cd ..
    cd whizard-src
    $F95 $FCFLAGS  -I../include -c limits.f90
    cd ..
    make -C a6f/include/  A6F=.. WHIZ=../.. WH195=../.. SHELL=/bin/bash
    #  
    #
    make test
    export LUMI_LINKER='/tmp/lumi_linker_directory/lumi_linker_000'
    export PHOTONS_B1='/tmp/lumi_linker_directory/photons_beam1_linker_000'
    export PHOTONS_B2='/tmp/lumi_linker_directory/photons_beam2_linker_000'
    export EBEAM='/tmp/lumi_linker_directory/ebeam_in_linker_000'
    export PBEAM='/tmp/lumi_linker_directory/pbeam_in_linker_000'    
    cd $here
  } 
  build_whizard_models(){
    local here
    here=`pwd`
    cd whizard-$1
    cd conf
    mv  whizard.prc  whizard.prc-orig
    cp $2 .
    cp $3 .
    cp $4 .
    ln -s models/NMSSM.mdl .
    cp whizard.prc-msugra_1 whizard.prc
    cd ..
    make prg install
    cd results
    cp $5 whizard.in
    cp ../conf/sps1ap.in LesHouches.msugra_1.in
    cd $here
  }
  here=`pwd`
  whizvers=$1
  install_default_whizard ${whizvers}
  install_lumi_files ${FILES_FOR_WHIZARD_64}/beam_spectra.tar.gz
  install_a6f ${whizvers}
  #install_our_user  ${whizvers}  ${FILES_FOR_WHIZARD_64}/user.f90-for-whiz${vers}
  compile_whizard ${whizvers}
  build_whizard_models ${whizvers} \
          ${FILES_FOR_WHIZARD_64}/whizard.prc-msugra_1  \
          ${FILES_FOR_WHIZARD_64}/whizard.prc.NMSSM \
          ${FILES_FOR_WHIZARD_64}/whizard.prc-point5 \
          ${FILES_FOR_WHIZARD_64}/whizard.in-msugra-1
  cd $here
}
# If you are brave, remove the "#---" and run it all.
# Otherwise, cut-n-paste to the bash command-line and check
# each step....
#--- define_cernlib install
#--- install_pythia 6.4.22
#--- define_ocaml install 3.11 2
#--- define_stdhep install 5-06-01
#--- define_tauola install ${FILES_FOR_WHIZARD_64}/tim-tauola_desy_tidy.tar.gz
#--- define_whizard
#--- make_our_whizard 1.95
# test it:
#--- cd whizard-1.95/results
#---  ./whizard --process_input 'process_id = "stau2stau2_e3e3"'  --simulation_input 'write_events_file = "stau2stau2_e3e3"' 2>/dev/null
# Size of all: 1.4 GB (220 MB in whizard-1.95/results, ie. the test,
# 100 MB in tar-balls that can be removed)

##OR Setup the env, and don't recomplile all but whizard
#define_cernlib setup
#install_pythia 6.4.22
#define_ocaml setup 3.11 2
#define_stdhep setup 5-06-01
#define_tauola setup ${FILES_FOR_WHIZARD_64}/tim-tauola_desy_tidy.tar.gz
#define_whizard
#make_our_whizard 1.95
