#!/bin/bash

# #################################################################
# run-demolc.sh 
# 
# (Description)
#   run BHWIDE program.  
#   Parameters from CMSEne to KeyPia are for BHWIDE. 
#   If CMSEne < 0, default parameters hard coded in the program (demolc.F)
#   will be used.  Otherwise, parameters given below are used.
#
# ##################################################################

execfiles=../bin/demolc.exe
hbook="demolc-bhwide.hbook"
stdhep="demolc-bhwide.stdhep"
lumidata="lumiee500-run1to20.dat"
rm -fv ${hbook}
rm -fv ${stdhep} 


${execfiles}  <<EOF
500.0  ! CMSEne(500.0)  2*Ebeam [GeV] or 0. to use default parameters
7.0    ! ThMinp(7.0)    511.d-6/cmsene *57.3   ! Detector range ThetaMin [deg] for positrons
173.0  ! ThMaxp(180.-thminp)    ! Detector range ThetaMax [deg] for positrons
200.0  ! EnMinp(200)    !Energy minimum [GeV] for detected positrons
7.0    ! ThMine(7.0)    ! 1.2/370.*57.3    ! Detector range ThetaMin [deg] for electrons
173.0  ! ThMaxe(180.-thmine)    ! Detector range ThetaMax [deg] for electrons
200.0  ! EnMine(200.0)  ! Energy minimum [GeV] for detected electrons
180.0  ! Acolli(180.0)  ! Maximum acollinearity [deg] of final e+e-
1E-4   ! epsCMS(1d-4)   ! Infrared cut on photon energy
0  ! KeyWgt(0) ! 0 for un-weighted event, WT=1 events for detector simulation. Try both options for detector simulation, both should be same
1  ! KeyRnd(1) ! 1 for RANMAR random numbers  ( select rundom number generator )
1  ! KeyEWC(0) ! 0 for QED corrections only, 1 Total O(alpha) ElectroWeak corr. included
1  ! KeyLib(1) ! 1 for ElectroWeak corrections from BABAMC (obsolete!),  2 for ElectroWeak corrections from ALIBABA
2  ! KeyMod(2) ! 1 for Hard bremsstr. matrix element from MODEL1, 2 for Hard bremsstr. matrix alement from MODEL2
1  ! KeyPia(1) ! Vacuum polarization option (0/1/2/3)
100000 ! NevTot(100000) ! Number of events to be generated
313    ! ProcessID stored in the stdhep file
${lumidata}
${hbook}
${stdhep}
EOF

