#!/bin/bash 

# ############################################################
# install.sh 
#
# A script to install bhwide for LC in the current directory.
# See README file about how to use this script.
#
# #############################################################

work_dir=`pwd`
bhwide_src=${work_dir}/misc/bhwide.tar
bhwide_bin_dir=${work_dir}/bin

if [ ! -f ${bhwide_src} ] ; then
  echo "bhwide source code (bhwide.tar) was not found in misc/"
  echo "Download bhwide.tar from /afs/cern.ch/eng/clic/software/x86_64-slc5-gcc41/BHWide"
  echo "and place it in misc/ directory."
  exit -1
fi

# 
# 
mkdir -p ${work_dir}/build ${work_dir}/bin
( 
  cd ${work_dir}/build
  tar xf ${bhwide_src}
  ( 
    cd bhwide
    mv -v environment.sh environment.sh.orig
    cp -v ${work_dir}/environment.sh .
    patch lib/glibk.f < ${work_dir}/src/glibk.f.patch 
    . environment.sh 
    mkdir demolc
    ( cd demolc 
      ln -sf ${work_dir}/src/demolc/Makefile .
      ln -sf ${work_dir}/src/demolc/circe.f .
      ln -sf ${work_dir}/src/demolc/cwn.f .
      ln -sf ${work_dir}/src/demolc/demolc.F .
    )
    ( 
      oldhome=${HOME}
      export HOME=${work_dir}/build/bhwide
      cd demolc
      make
      cp -v demolc.exe ${work_dir}/bin
      export HOME=${oldhome}      

      echo "The installation of demolc.exe hsa completed."
      echo "run example will be found in examples directory."
    )
  )
)
 
