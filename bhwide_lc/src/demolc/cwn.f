      Program CWN
*
      Common /Pawc/ H(100000)
      Parameter (Nmax = 100)
      Common /CWN2/  N,X(4, Nmax),Y(4, Nmax),Z(4, Nmax)
*.___________________________________________
*
      Call Hlimit(100000)
*
      Call Hropen(1,'CWN','cwn.hbook','N',1024,Istat)
      If (Istat.Ne.0) Then
         Print*, 'Error in opening file ...'
         Stop
      Endif
*
      Call Hbnt(1,'CWN',' ')
      Call Hbname(1,'Block1',N,'N[0,100], X(4,N), Y(4,N), Z(4,N)')
*
      Do N=1,Nmax
         Do I=1,N
            Do k = 1,4
               X(k, N) = N*N + k
               Y(k, N) = N*N*N + k
               Z(k, N) = SIN(X(k, N))+k
            EndDo
         EndDo
         Call HFNT(1)
      EndDo
*
      Call Hrout(0,Icycle,' ')
      Call Hrend('CWN')
*
      end
