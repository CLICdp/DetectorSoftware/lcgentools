# 
# Define environment parameters to meet your environment
#

softdir=/group/ilc/soft/gcc481
export PATH=${softdir}/gcc/bin:$PATH
export LD_LIBRARY_PATH=${softdir}/gcc/lib64:${softdir}/gcc/lib:${LD_LIBRARY_PATH}
export CERN=${softdir}/cernlib/
export CERN_LEVEL=2005
export STDHEP_DIR=${softdir}/stdhep/5.06.01

# export LD_LIBRARY_PATH=/afs/cern.ch/sw/lcg/contrib/gcc/4.3.2/x86_64-slc5-gcc43-opt/lib64:$LD_LIBRARY_PATH
# export PATH=/afs/cern.ch/sw/lcg/contrib/gcc/4.3.2/x86_64-slc5-gcc43-opt/bin:$PATH
# export CERN=/afs/cern.ch/eng/clic/software/x86_64-slc5-gcc41/CERNLIBS
# export CERN_LEVEL=2006-gfortran43
# export ROOTSYS=/afs/cern.ch/sw/lcg/app/releases/ROOT/5.28.00/x86_64-slc5-gcc43-opt/root

export CERN_ROOT=${CERN}/${CERN_LEVEL}
export CVSCOSRC="$CERN_ROOT/src"
export PATH="$CERN_ROOT/bin:$PATH"
export LD_LIBRARY_PATH="$CERN_ROOT/lib:$LD_LIBRARY_PATH"

# export PATH="$ROOTSYS/bin:$PATH"
# export ROOTSYS=${softdir}/root/5.34.30
# export LD_LIBRARY_PATH="$ROOTSYS/lib:$LD_LIBRARY_PATH"
