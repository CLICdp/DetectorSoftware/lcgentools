package org.lcsim.stdhepcut;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.lcsim.stdhepcut.util.ClassFinder;

import hep.lcio.event.MCParticle;

/**
 * Base class for cuts used by the StdhepCut program. Based on C++ version by
 * Lars Weuste.
 * 
 * @author Christian Grefe (CERN)
 */
public class Cut implements Comparable<Cut>, Cloneable {

	protected static List<Cut> _availableCuts;
	protected String _name;
	protected List<Double> _parameters;
	protected List<String> _parameterDescriptions;
	protected int _nParameters;
	protected String _description;

	/*
	 * Constructor used by all derived cuts the initialize their parameters. The
	 * name is used to look up the cut from the list of available cuts. The list
	 * of parameter descriptions also defines the number of parameters for this
	 * cut.
	 */
	protected Cut(String name, List<String> parameterDescriptions) {
		_name = name;
		_nParameters = parameterDescriptions.size();
		_parameterDescriptions = parameterDescriptions;
		_parameters = new ArrayList<Double>(_nParameters);
		_description = "No description available";
	}

	/*
	 * Constructor used by all derived cuts the initialize their parameters. The
	 * name is used to look up the cut from the list of available cuts. The list
	 * of parameter descriptions also defines the number of parameters for this
	 * cut.
	 */
	protected Cut(String name, String[] parameterDescriptions) {
		this(name, Arrays.asList(parameterDescriptions));
	}

	/**
	 * Returns the name of the cut and all parameters with their values.
	 */
	public String toString() {
		String str = _name + ":";
		for (int index = 0; index < _nParameters; index++) {
			str += " " + _parameterDescriptions.get(index) + " = " + _parameters.get(index) + ",";
		}
		return str;
	}

	/**
	 * @return The name of this cut
	 */
	public String getName() {
		return _name;
	}

	/**
	 * @return The number of parameters of this cut
	 */
	public int getNParameters() {
		return _nParameters;
	}

	/**
	 * @return The description of this cut
	 */
	public String getDescription() {
		return _description;
	}

	/**
	 * @param index
	 *            The parameter index
	 * @return The description of the given parameter
	 */
	public String getParameterDescription(int index) {
		return _parameterDescriptions.get(index);
	}

	/**
	 * Sets all parameter values for this cut. The length of the list of values
	 * has to be exactly the number of parameters of this cut.
	 * 
	 * @param params
	 *            The list of parameter values
	 */
	public void setParams(List<Double> params) {
		if (params.size() != _nParameters) {
			System.err.println("ERROR: The cut " + _name + " needs " + _nParameters
					+ " parameters (cut values), but we got " + params.size());
			System.exit(100);
		} else {
			_parameters = params;
		}
	}

	/**
	 * Check if this MCParticle collection passes this cut. This is the method
	 * that has to be implemented by all derived cuts.
	 * 
	 * @param mcParticles
	 *            The MCParticle collection
	 * @return Input collection passes this cut
	 */
	public boolean evaluate(List<MCParticle> mcParticles) {
		return false;
	}

	/**
	 * Compare two cuts by their name. Needed to sort a list of cuts.
	 */
	@Override
	public int compareTo(Cut otherCut) {
		return getName().compareTo(otherCut.getName());
	}

	public static Cut getCut(String name) {
		for (Cut cut : _availableCuts) {
			if (cut.getName().equals(name)) {
				Cut myCut = null;
				try {
					myCut = (Cut) cut.clone();
				} catch (CloneNotSupportedException e) {
					throw new RuntimeException(e.getMessage());
				}
				return myCut;
			}
		}
		return null;
	}

	/**
	 * Print all available cuts together with their description.
	 */
	public static void printAvailableCuts() {
		System.out.println("Available cuts:");
		for (Cut cut : _availableCuts) {
			System.out.println("  " + cut.getName() + ": " + cut.getDescription());
			String str = "      Parameters(" + cut.getNParameters() + "):";
			for (int index = 0; index < cut.getNParameters(); index++) {
				str += " " + cut.getParameterDescription(index) + ",";
			}
			System.out.println(str.substring(0, str.length() - 1));
			System.out.println(" ");
		}
	}

	/**
	 * Find all cuts and fill the list of available cuts.
	 */
	static {
		String basepath = "org.lcsim.stdhepcut.cuts";
		_availableCuts = new ArrayList<Cut>();
		try {
			for (String className : ClassFinder.getClassNamesFromPackage(basepath)) {
				Object cut = null;
				try {
					cut = Class.forName(basepath + "." + className).newInstance();
				} catch (Exception e) {
					//e.printStackTrace();
				}
				if (cut != null) {
					if (cut instanceof Cut) {
						_availableCuts.add((Cut) cut);
					}
				}
			}
		} catch (IOException e) {
			//e.printStackTrace();
		}
		Collections.sort(_availableCuts);
	}
}
