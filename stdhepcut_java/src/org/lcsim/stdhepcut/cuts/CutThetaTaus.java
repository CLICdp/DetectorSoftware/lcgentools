package org.lcsim.stdhepcut.cuts;

import java.util.List;

import org.lcsim.stdhepcut.Cut;
import org.lcsim.stdhepcut.util.ParticleFinder;
import org.lcsim.stdhepcut.util.ThreeVector;

import hep.lcio.event.MCParticle;

/**
 * @author Christian Grefe (CERN)
 */
public class CutThetaTaus extends Cut {
	
	public CutThetaTaus() {
		super("tauTheta_R", new String[]{"minTheta", "maxTheta"});
		_description = "Selects events with exactly two taus and requires each to have a polar angle within the given range."; 
	}
	
	public boolean evaluate(List<MCParticle> mcParticles) {
		double minTheta = _parameters.get(0);
		double maxTheta = _parameters.get(1);
		List<MCParticle> particles = ParticleFinder.findTaus(mcParticles);
		
		if (particles.size() != 2) {
			System.err.println("WARNING: Wrong number of taus found: " + particles.size());
			System.err.println(toString() + " not applied.");
	        return true;
		}
		
		for (MCParticle particle : particles) {
			ThreeVector p = new ThreeVector(particle.getMomentum());
			double theta = p.theta() * 180. / Math.PI;;
			if (theta < minTheta || theta > maxTheta) {
				return false;
			}
		}
		return true;
	}

}
