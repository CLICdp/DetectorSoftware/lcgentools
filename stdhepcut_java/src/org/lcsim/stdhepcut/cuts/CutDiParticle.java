package org.lcsim.stdhepcut.cuts;

import java.util.ArrayList;
import java.util.List;

import org.lcsim.stdhepcut.Cut;
import org.lcsim.stdhepcut.util.LorentzVector;
import org.lcsim.stdhepcut.util.ParticleFinder;

import hep.lcio.event.MCParticle;

/**
 * @author Christian Grefe (CERN)
 */
public class CutDiParticle extends Cut {

	public CutDiParticle() {
		super("diParticle", new String[] { "PDGID", "minTheta", "maxTheta", "minPt", "maxPt", "minEnergy", "maxEnergy",
				"minInvMass", "minInvMass", "diParticleMinPt", "diParticleMaxPt" });
		_description = "At least two particles of the desired PDGID have to pass the single particle selections on the"
				+ " polar angle, transverse momentum and energy. Only if at least one of the possible two particle systems"
				+ " passes the invariant mass and the transverse momentum requirements the event will be kept.";
	}

	@Override
	public boolean evaluate(List<MCParticle> mcParticles) {
		int pdgid = _parameters.get(0).intValue();
		double minTheta = _parameters.get(1);
		double maxTheta = _parameters.get(2);
		double minPt = _parameters.get(3);
		double maxPt = _parameters.get(4);
		double minEnergy = _parameters.get(5);
		double maxEnergy = _parameters.get(6);
		double minInvMass = _parameters.get(7);
		double maxInvMass = _parameters.get(8);
		double diParticleMinPt = _parameters.get(9);
		double diParticleMaxPt = _parameters.get(10);

		List<MCParticle> particles = ParticleFinder.findFinalStateParticles(mcParticles, pdgid);
		List<LorentzVector> passedParticles = new ArrayList<LorentzVector>();
		for (MCParticle particle : particles) {
			// check particle energy
			LorentzVector v = new LorentzVector(particle.getEnergy(), particle.getMomentum());
			if (v.E() > maxEnergy || v.E() < minEnergy) {
				continue;
			}
			// check transverse momentum
			if (v.pT() > maxPt || v.pT() < minPt) {
				continue;
			}
			// check polar angle projected into interval [0,90]
			double theta = 90. - 180. * Math.abs(v.theta() / Math.PI - 0.5);
			if (theta > maxTheta || theta < minTheta) {
				continue;
			}

			passedParticles.add(v);
		}

		for (LorentzVector particle : passedParticles) {
			for (LorentzVector otherParticle : passedParticles) {
				if (particle == otherParticle) {
					continue;
				}
				LorentzVector diParticle = new LorentzVector(particle);
				diParticle.add(otherParticle);
				if (diParticle.mass() >= minInvMass && diParticle.mass() <= maxInvMass && diParticle.pT() >= diParticleMinPt && diParticle.pT() <= diParticleMaxPt) {
					return true;
				}
			}
		}

		return false;
	}
}
