package org.lcsim.stdhepcut.cuts;

import java.util.List;

import org.lcsim.stdhepcut.Cut;
import org.lcsim.stdhepcut.util.ParticleFinder;
import org.lcsim.stdhepcut.util.ThreeVector;

import hep.lcio.event.MCParticle;

/**
 * @author Christian Grefe (CERN)
 */
public class CutAngleQuarks extends Cut {

	public CutAngleQuarks() {
		super("quarkAngle_GT", new String[]{"minTheta"});
		_description = 	"Selects events with a minimum angle between two quarks.";
	}
	
	@Override
	public boolean evaluate(List<MCParticle> mcParticles) {
		List<MCParticle> quarks = ParticleFinder.findQuarks(mcParticles);
		if (quarks.size() != 2) {
			System.err.println("WARNING: Wrong number of quarks found: " + quarks.size());
			System.err.println(toString() + " not applied.");
	        return true;
		}
		
		ThreeVector p1 = new ThreeVector(quarks.get(0).getMomentum());
		ThreeVector p2 = new ThreeVector(quarks.get(1).getMomentum());
		double angle = p1.angle(p2);
		return angle > _parameters.get(0);
	}
}
