package org.lcsim.stdhepcut.cuts;

import java.util.List;

import org.lcsim.stdhepcut.Cut;
import org.lcsim.stdhepcut.util.ParticleFinder;
import org.lcsim.stdhepcut.util.ThreeVector;

import hep.lcio.event.MCParticle;

/**
 * @author Christian Grefe (CERN)
 */
public class CutAngleLeptons extends Cut {

	public CutAngleLeptons() {
		super("leptonAngle_LT", new String[]{"PDGID", "minEnergy", "maxTheta"});
		_description = "Selects events with no particles of the given PDG ID and with an energy larger than the minimum energy and a polar angle larger than the minimum angle."; 
	}
	
	@Override
	public boolean evaluate(List<MCParticle> mcParticles) {
		List<MCParticle> particles = ParticleFinder.findFinalStateParticles(mcParticles, _parameters.get(0).intValue());
		for (MCParticle mcp : particles) {
			if (mcp.getEnergy() > _parameters.get(1)); {
				ThreeVector p = new ThreeVector(mcp.getMomentum());
				double theta = p.theta() * 180. / Math.PI;
				if (theta > 90) {
					theta = 180. - theta;
				}
				if (theta > _parameters.get(2)) {
					return false;
				}
			}
		}
		return true;
	}
	
}
