package org.lcsim.stdhepcut.cuts;

import java.util.List;

import org.lcsim.stdhepcut.Cut;
import org.lcsim.stdhepcut.util.ParticleFinder;
import org.lcsim.stdhepcut.util.ThreeVector;

import hep.lcio.event.MCParticle;

/**
 * @author Christian Grefe (CERN)
 */
public class CutPtQuarks extends Cut {
	
	public CutPtQuarks() {
		super("quarkPt_GT", new String[]{"minPt"});
		_description = "Selects events with exactly two quarks and requires each to have a transverse momentum larger than the minimum pT."; 
	}
	
	public boolean evaluate(List<MCParticle> mcParticles) {
		double minPt = _parameters.get(0);
		List<MCParticle> particles = ParticleFinder.findQuarks(mcParticles);
		
		if (particles.size() != 2) {
			System.err.println("WARNING: Wrong number of quarks found: " + particles.size());
			System.err.println(toString() + " not applied.");
	        return true;
		}
		
		ThreeVector p1 = new ThreeVector(particles.get(0).getMomentum());
		ThreeVector p2 = new ThreeVector(particles.get(1).getMomentum());
		return minPt < p1.pT() && minPt < p2.pT();
	}

}
