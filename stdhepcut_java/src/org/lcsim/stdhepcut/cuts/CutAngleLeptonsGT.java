package org.lcsim.stdhepcut.cuts;

import java.util.List;

import org.lcsim.stdhepcut.Cut;
import org.lcsim.stdhepcut.util.ParticleFinder;
import org.lcsim.stdhepcut.util.ThreeVector;

import hep.lcio.event.MCParticle;

/**
 * @author Christian Grefe (CERN)
 */
public class CutAngleLeptonsGT extends Cut {

	public CutAngleLeptonsGT() {
		super("leptonAngle_GT", new String[]{"PDGID", "minTheta"});
		_description = "Selects events with exactly two particles of the given PDG ID and a polar angle larger than the minimum angle."; 
	}
	
	@Override
	public boolean evaluate(List<MCParticle> mcParticles) {
		int pdgid = _parameters.get(0).intValue();
		double minTheta = _parameters.get(1);
		List<MCParticle> particles = ParticleFinder.findFinalStateParticles(mcParticles, pdgid);
		
		if (particles.size() != 2) {
			System.err.println("WARNING: Wrong number of leptons found: " + particles.size());
			System.err.println(toString() + " not applied.");
	        return true;
		}
		
		for (MCParticle particle : particles) {
			ThreeVector p = new ThreeVector(particle.getMomentum());
			double theta = 90. - 180. * Math.abs(p.theta() / Math.PI - 0.5);
			if (theta < minTheta) {
				return false;
			}
		}
		return true;
	}
}
