package org.lcsim.stdhepcut.cuts;

import hep.lcio.event.MCParticle;

import java.util.List;

import org.lcsim.stdhepcut.Cut;
import org.lcsim.stdhepcut.util.LorentzVector;
import org.lcsim.stdhepcut.util.ParticleFinder;


/**
 * @author Christian Grefe (CERN)
 */
public class CutInvMassQuarks extends Cut {

	public CutInvMassQuarks() {
		super("quarkInvMass_R", new String[]{"minInvMass", "maxInvMass"});
		_description = "Selects events with exactly two quarks within the given invariant mass range.";
	}
	
	@Override
	public boolean evaluate(List<MCParticle> mcParticles) {
		double minInvMass = _parameters.get(0);
		double maxInvMass = _parameters.get(1);
		List<MCParticle> particles = ParticleFinder.findQuarks(mcParticles);
		if (particles.size() != 2) {
			System.err.println("WARNING: Wrong number of quarks found: " + particles.size());
			System.err.println(toString() + " not applied.");
	        return true;
		}
		
		LorentzVector v1 = new LorentzVector(particles.get(0).getEnergy(), particles.get(0).getMomentum());
		LorentzVector v2 = new LorentzVector(particles.get(1).getEnergy(), particles.get(1).getMomentum());
		v1.add(v2);
		double invMass = v1.mass();
		return invMass >= minInvMass && invMass <= maxInvMass;
	}
}
