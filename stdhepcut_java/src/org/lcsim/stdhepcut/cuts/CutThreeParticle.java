package org.lcsim.stdhepcut.cuts;

import java.util.ArrayList;
import java.util.List;

import org.lcsim.stdhepcut.Cut;
import org.lcsim.stdhepcut.util.LorentzVector;
import org.lcsim.stdhepcut.util.ParticleFinder;

import hep.lcio.event.MCParticle;

/**
 * @author Christian Grefe, Eva Sicking (CERN)
 */
public class CutThreeParticle extends Cut {

    public CutThreeParticle() {
	super("threeParticle", new String[] { "minTheta", "maxTheta", 
					      "minPt", "maxPt", 
					      "minEnergy", "maxEnergy",
					      "minInvMass", "minInvMass" });
	_description = "At least three particles (one photon and two charged leptons or two quarks) have to pass "
	    + "the single particle selections on the polar angle, the transverse momentum, and the energy. "
	    + "Only if at least one of the possible three particle systems passes the invariant mass requirements, "
	    + "the event will be kept.";
    }

    @Override
	public boolean evaluate(List<MCParticle> mcParticles) {
	double minInvMass = _parameters.get(6);
	double maxInvMass = _parameters.get(7);


	//find photons
	List<MCParticle> photons = ParticleFinder.findFinalStateParticles(mcParticles, 22);
	List<LorentzVector> passedPhotons = new ArrayList<LorentzVector>();
	for (MCParticle photon : photons) {

	    if(!acceptParticle(photon))continue;
	    // photon passed cut, add it to list
	    LorentzVector v = new LorentzVector(photon.getEnergy(), photon.getMomentum());
	    passedPhotons.add(v);
	}
	//reject event if there is less than one accepted photon
	if(passedPhotons.size()<1) return false;


	//find charged leptons
	List<LorentzVector> passedChargedLeptons = new ArrayList<LorentzVector>();

	for(int pdg=11; pdg<=15; pdg+=2){

	    List<MCParticle> chargedLeptons  = new ArrayList<MCParticle>();
	    if(pdg!=15)chargedLeptons = ParticleFinder.findFinalStateParticles(mcParticles, pdg);
	    else if(pdg==15)chargedLeptons = ParticleFinder.findTaus(mcParticles);
	    
	    for (MCParticle chargedLepton : chargedLeptons) {
		
		if(!acceptParticle(chargedLepton))continue;
		// charged lepton passed cut, add it to list
		LorentzVector v = new LorentzVector(chargedLepton.getEnergy(), chargedLepton.getMomentum());
		passedChargedLeptons.add(v);
	    }
	}

	
	//find quarks
	List<MCParticle> quarks = ParticleFinder.findQuarks(mcParticles);
	List<LorentzVector> passedQuarks = new ArrayList<LorentzVector>();
	for (MCParticle quark : quarks) {

	    if(!acceptParticle(quark))continue;
	    // quark passed cut, add it to list
	    LorentzVector v = new LorentzVector(quark.getEnergy(), quark.getMomentum());
	    passedQuarks.add(v);
	}
	//reject event if there are less than two Z daughter candidates
	if(passedChargedLeptons.size()<2 && passedQuarks.size()<2) return false;
	

	//build higgs candidates
	List<LorentzVector> zDaughters = new ArrayList<LorentzVector>();

	for(int type=0;type<2;type++){
	    //type=0 : charged leptons
	    //type=1 : quarks
	    
	    if(type==0) zDaughters = passedChargedLeptons;
	    else zDaughters = passedQuarks;

	    for (LorentzVector zDaughter1 : zDaughters) {
		for (LorentzVector zDaughter2 : zDaughters) {
		    if (zDaughter1 == zDaughter2) {
			continue;
		    }
		    for (LorentzVector photon : passedPhotons) {
			
			LorentzVector threeParticle = new LorentzVector(zDaughter1);
			threeParticle.add(zDaughter2);
			threeParticle.add(photon);
			
			if (threeParticle.mass() >= minInvMass && threeParticle.mass() <= maxInvMass) {
			    //already one Higgs candidate is enough
			    return true;
			}
		    }
		}
	    }
	}
	
	return false;
    }

    public boolean acceptParticle(MCParticle particle) {
	double minTheta  = _parameters.get(0);
	double maxTheta  = _parameters.get(1);
	double minPt     = _parameters.get(2);
	double maxPt     = _parameters.get(3);
	double minEnergy = _parameters.get(4);
	double maxEnergy = _parameters.get(5);

	LorentzVector v = new LorentzVector(particle.getEnergy(), particle.getMomentum());

	// check polar angle projected into interval [0,90]
	double theta = 90. - 180. * Math.abs(v.theta() / Math.PI - 0.5);
	if (theta > maxTheta || theta < minTheta) {
	    return false;
	}
	
	// check transverse momentum
	if (v.pT() > maxPt || v.pT() < minPt) {
	    return false;
	}

	// check particle energy
	if (v.E() > maxEnergy || v.E() < minEnergy) {
	    return false;
	}

	return true;
    }
    
}
