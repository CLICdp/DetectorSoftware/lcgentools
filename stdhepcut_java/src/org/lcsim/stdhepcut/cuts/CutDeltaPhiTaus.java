package org.lcsim.stdhepcut.cuts;

import java.util.List;

import org.lcsim.stdhepcut.Cut;
import org.lcsim.stdhepcut.util.ParticleFinder;
import org.lcsim.stdhepcut.util.ThreeVector;

import hep.lcio.event.MCParticle;

/**
 * @author Christian Grefe (CERN)
 */
public class CutDeltaPhiTaus extends Cut {

	public CutDeltaPhiTaus() {
		super("tauDeltaPhi_R", new String[] {"minPhi", "maxPhi"});
		_description = "Selects events with two taus in the given accoplanarity range.";
	}
	
	@Override
	public boolean evaluate(List<MCParticle> mcParticles) {
		List<MCParticle> taus = ParticleFinder.findTaus(mcParticles);
		if (taus.size() != 2) {
			System.err.println("WARNING: Wrong number of taus found: " + taus.size());
			System.err.println(toString() + " not applied.");
	        return true;
		}
		
		ThreeVector p1 = new ThreeVector(taus.get(0).getMomentum());
		ThreeVector p2 = new ThreeVector(taus.get(1).getMomentum());
		double accoplanarity = Math.abs(p1.phi() - p2.phi()) * 180. / Math.PI;
		if (accoplanarity > 180.) {
			accoplanarity = - accoplanarity + 360.;
		}
		return accoplanarity >= _parameters.get(0) && accoplanarity <= _parameters.get(1);
	}
	
}
