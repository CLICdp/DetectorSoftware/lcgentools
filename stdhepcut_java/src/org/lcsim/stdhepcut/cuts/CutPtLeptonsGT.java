package org.lcsim.stdhepcut.cuts;

import java.util.List;

import org.lcsim.stdhepcut.Cut;
import org.lcsim.stdhepcut.util.ParticleFinder;
import org.lcsim.stdhepcut.util.ThreeVector;

import hep.lcio.event.MCParticle;

/**
 * @author Christian Grefe (CERN)
 */
public class CutPtLeptonsGT extends Cut {
	
	public CutPtLeptonsGT() {
		super("leptonPt_GT", new String[]{"PDGID", "minPt"});
		_description = "Selects events with exactly two particles of the given PDG ID and a combined transverse momentum larger than the minimum pT."; 
	}
	
	public boolean evaluate(List<MCParticle> mcParticles) {
		int pdgid = _parameters.get(0).intValue();
		double minPt = _parameters.get(1);
		List<MCParticle> particles = ParticleFinder.findFinalStateParticles(mcParticles, pdgid);
		
		if (particles.size() != 2) {
			System.err.println("WARNING: Wrong number of particles found: " + particles.size());
			System.err.println(toString() + " not applied.");
	        return true;
		}
		
		ThreeVector p1 = new ThreeVector(particles.get(0).getMomentum());
		ThreeVector p2 = new ThreeVector(particles.get(1).getMomentum());
		p1.add(p2);
		return minPt < p1.pT();
	}

}
