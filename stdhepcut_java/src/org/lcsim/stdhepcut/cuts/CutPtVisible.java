package org.lcsim.stdhepcut.cuts;

import java.util.List;

import org.lcsim.stdhepcut.Cut;
import org.lcsim.stdhepcut.util.ThreeVector;

import hep.lcio.event.MCParticle;

/**
 * @author Christian Grefe (CERN)
 */
public class CutPtVisible extends Cut {
	
	public CutPtVisible() {
		super("visiblePt_GT", new String[]{"minPt"});
		_description = "Selects events with a total visible transverse momentum larger than the minimum pT."; 
	}
	
	public boolean evaluate(List<MCParticle> mcParticles) {
		// calculate the pt of all visible particles
		ThreeVector momentum = new ThreeVector();
		
		// loop over all particles
		for (MCParticle mcp : mcParticles) {
			// only visible particles: nothing that has a daughter (i.e. is decayed)
			if (mcp.getDaughters().size() > 0) {
				continue;
			}
			
			// nothing with generator status other than 1 (i.e. decayed)
			if (mcp.getGeneratorStatus() != 1) {
				continue;
			}
			
			// no SUSY particles and no neutrinos
			int pdg = Math.abs(mcp.getPDG());
			if (pdg == 12 || pdg == 14 || pdg == 16 || pdg > 100000) {
				continue;
			}
			
			momentum.add(new ThreeVector(mcp.getMomentum()));
		}
		return _parameters.get(0) < momentum.pT();
	}

}
