package org.lcsim.stdhepcut;

import hep.io.stdhep.StdhepEvent;
import hep.io.stdhep.StdhepReader;
import hep.io.stdhep.StdhepRecord;
import hep.io.stdhep.StdhepWriter;
import hep.lcio.event.LCCollection;
import hep.lcio.event.MCParticle;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.lcsim.stdhepcut.util.StdhepConverter;

/**
 * Main class applying cuts to stdhep input files. Based on C++ version by Lars
 * Weuste.
 * 
 * @author Christian Grefe, CERN
 */
public class StdhepCut {

	// ------------------------------//
	// class members
	// ------------------------------//

	protected String _cutFile;
	protected String _outputFile;
	protected List<String> _inputFileList;
	protected int _processMaxEvents;
	protected int _processMaxEventsDebug;
	protected List<Cut> _cutList;
	protected int _statsEventsTotal;
	protected int _statsEventsAfterCut;
	protected int _statsEventsWritten;
	protected StdhepReader _reader;
	protected StdhepWriter _writer;

	// protected StdhepConverter _converter;

	// ------------------------------//
	// constructors
	// ------------------------------//

	/**
	 * Default constructor
	 */
	public StdhepCut() {
		_cutFile = "Cut.txt";
		_outputFile = "out.stdhep";
		_processMaxEvents = -1;
		_processMaxEventsDebug = -1;

		_statsEventsAfterCut = 0;
		_statsEventsTotal = 0;
		_statsEventsWritten = 0;

		_inputFileList = new ArrayList<String>();
		_cutList = new ArrayList<Cut>();

		// _converter = new StdhepConverter();
	}

	// ------------------------------//
	// public methods
	// ------------------------------//

	/*
	 * Print the names of the input and output files.
	 */
	public void printParams() {
		System.out.println("Parameters:");
		System.out.println("  Cut File: " + _cutFile);
		System.out.println("  Out File: " + _outputFile);
		String str = "  In File(s):";
		for (String fileName : _inputFileList) {
			str += " " + fileName + ",";
		}
		System.out.println(str.substring(0, str.length() - 1));
	}

	/*
	 * Add an input stdhep file
	 */
	public void addInputFile(String fileName) {
		_inputFileList.add(fileName);
	}

	/*
	 * Set the input cut file name
	 */
	public void setCutFile(String fileName) {
		_cutFile = fileName;
	}

	/*
	 * Set the output stdhep file name.
	 */
	public void setOutFile(String fileName) {
		_outputFile = fileName;
	}

	/**
	 * Limit the number of processed events. Default is -1 which will process
	 * all events.
	 * 
	 * @param nEvents
	 *            The maximum number of events read from the input files(s)
	 */
	public void setMaxEvents(int nEvents) {
		_processMaxEvents = nEvents;
	}

	/**
	 * Limit the number of written events.
	 * 
	 * @param nEvents
	 *            The maximum number of events passing the cuts
	 */
	public void setMaxEventsDebug(int nEvents) {
		_processMaxEventsDebug = nEvents;
	}

	/**
	 * Main method to execute the StdhepCut.
	 */
	public void run() {
		initCuts();

		try {
			_writer = new StdhepWriter(_outputFile, "", "", 10);
		} catch (IOException e) {
			System.err.println(e.getMessage());
			System.exit(1);
		}

		processFiles();

		System.out.println("Finished:");
		System.out.println("  Events passing cuts: " + _statsEventsAfterCut);
		System.out.println("  Events kept: " + _statsEventsWritten);
		System.out.println("  Events cut: " + (_statsEventsTotal - _statsEventsAfterCut));
		System.out.println("  Events total: " + _statsEventsTotal);

		if (_reader != null) {
			try {
				_reader.close();
			} catch (IOException e) {
				System.err.println(e.getMessage());
				System.exit(50);
			}
		}

		if (_writer != null) {
			try {
				_writer.close();
			} catch (IOException e) {
				System.err.println(e.getMessage());
				System.exit(51);
			}
		}
	}

	// ------------------------------//
	// protected methods
	// ------------------------------//

	/**
	 * Read the cut definition input file and register the cuts.
	 */
	protected void initCuts() {
		System.out.println("Reading Cuts from " + _cutFile + ":");
		BufferedReader cutReader = null;
		try {
			cutReader = new BufferedReader(new FileReader(_cutFile));
		} catch (FileNotFoundException e) {
			System.err.println(e.getMessage());
			System.exit(30);
		}
		while (true) {
			String line;
			try {
				line = cutReader.readLine();
			} catch (IOException e) {
				break;
			}
			if (line == null) {
				break;
			}
			String[] splitLine = line.split(" ");
			if (splitLine.length < 1) {
				continue;
			}
			if (splitLine[0].equals("") || splitLine[0].equals("#")) {
				continue;
			}
			Cut cut = Cut.getCut(splitLine[0]);
			if (cut == null) {
				System.err.println("ERROR: Unknown cut with name '" + splitLine[0] + "'");
				System.exit(31);
			}
			List<Double> parameters = new ArrayList<Double>();
			for (String word : Arrays.copyOfRange(splitLine, 1, splitLine.length)) {
				parameters.add(Double.parseDouble(word));
			}
			cut.setParams(parameters);
			_cutList.add(cut);
			System.out.println("  " + cut.toString());
		}
		try {
			cutReader.close();
		} catch (IOException e) {
			System.err.println(e.getMessage());
			System.exit(32);
		}
	}

	/**
	 * Loop over all input stdhep files and write all events that pass the
	 * registered cuts to the output file.
	 */
	protected void processFiles() {
		for (String inputFile : _inputFileList) {
			if (_reader != null) {
				try {
					_reader.close();
				} catch (IOException e) {
					System.err.println(e.getMessage());
					System.exit(40);
				}
			}

			System.out.println("Opening input file: " + inputFile);
			try {
				_reader = new StdhepReader(inputFile);
			} catch (IOException e) {
				System.err.println(e.getMessage());
				System.exit(41);
			}

			while (true) {
				StdhepRecord record;
				try {
					record = _reader.nextRecord();
				} catch (IOException e) {
					break;
				}
				boolean EventPass = true;
				if (record instanceof StdhepEvent) {
					LCCollection mcParticleCollection = StdhepConverter.convert((StdhepEvent) record);
					EventPass = processEvent(mcParticleCollection);
				}
				if (EventPass) {
				        _statsEventsAfterCut++;
                                        if (_processMaxEvents != -1){
					    if (_statsEventsAfterCut <= _processMaxEvents){
						try {
                                                    _statsEventsWritten++;
						    _writer.writeRecord(record);
                                                } catch (IOException e) {
						    System.err.println(e.getMessage());
						    System.exit(42);
						}
					    }
					} 
					else {
					    try {
						_statsEventsWritten++;
						_writer.writeRecord(record);
					    } catch (IOException e) {
						System.err.println(e.getMessage());
						System.exit(42);
					    }
					}
					
				}
				if (_processMaxEventsDebug != -1 && _statsEventsAfterCut > _processMaxEventsDebug) {
					return;
				}
			}
		}
	}

	/**
	 * Evaluates all registered cuts for the given MCParticle collection.
	 * 
	 * @param mcParticles
	 *            The input MCParticle list
	 * @return Event passes all cuts
	 */
	protected boolean processEvent(LCCollection mcParticles) {
		_statsEventsTotal++;
		for (Cut cut : _cutList) {
			if (!cut.evaluate((List<MCParticle>) mcParticles)) {
				return false;
			}
		}
		
		return true;
	}

	// ------------------------------//
	// static methods
	// ------------------------------//

	/**
	 * Helper method to print the program usage
	 */
	public static void usage() {
		System.out.println("Usage: java StedhepCut.java [-l] [-m <maxEvents>] [-d <maxEvents>] -o <output.stdhep>"
				+ " -c <cut.txt> <input.stdhep> [<moreInput.stdhep>]");
		System.out.println("Will write events from <input.stdhep> that pass the cuts provided in <cut.txt>"
				+ " to <output.stdhep>");
		System.out.println("-l                will list all available cuts");
		System.out.println("-m <maxEvents>    stop writing after <maxEvents> passed the cut");
		System.out.println("-d <maxEvents>    stop processing after <maxEvents> passed the cut (debug)");
	}

	/**
	 * Helper method to parse the command line arguments and set up an
	 * StdhepCut.
	 * 
	 * @param stdhepCut
	 *            The StdhepCut to set up
	 * @param args
	 *            Command line parameters
	 */
	public static void parseArguments(StdhepCut stdhepCut, List<String> args) {
		System.out.println("#---------------------------------------------#");
		System.out.println("#  StdHep generator level cutting tool        #");
		System.out.println("#  created by Christian Grefe - CERN          #");
		System.out.println("#  based on C++ version by Lars Weuste - MPP  #");
		System.out.println("#---------------------------------------------#");
		Iterator<String> argIt = args.iterator();
		while (argIt.hasNext()) {
			String arg = argIt.next();
			if (arg.startsWith("-")) {
				if (arg.equals("-h")) {
					usage();
					System.exit(0);
				}
				if (arg.equals("-l")) {
					Cut.printAvailableCuts();
					System.exit(0);
				}
				if (argIt.hasNext()) {
					String val = argIt.next();
					if (arg.equals("-m")) {
						stdhepCut.setMaxEvents(Integer.parseInt(val));
					} else if (arg.equals("-d")) {
						stdhepCut.setMaxEventsDebug(Integer.parseInt(val));
					} else if (arg.equals("-o")) {
						stdhepCut.setOutFile(val);
					} else if (arg.equals("-c")) {
						stdhepCut.setCutFile(val);
					} else {
						System.err.println("Unknown flag " + arg);
						System.exit(1);
					}
				} else {
					System.err.println("Missing parameter after " + arg);
					System.exit(1);
				}
			} else {
				stdhepCut.addInputFile(arg);
			}
		}
	}

	/**
	 * The executable.
	 * 
	 * @param args
	 *            Command line parameters
	 */
	public static void main(String[] args) {
		StdhepCut stdhepCut = new StdhepCut();

		if (args.length < 1) {
			usage();
			System.exit(1);
		}

		parseArguments(stdhepCut, Arrays.asList(args));

		stdhepCut.printParams();
		stdhepCut.run();

		System.exit(0);
	}

}
