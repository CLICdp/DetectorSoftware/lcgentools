package org.lcsim.stdhepcut.util;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * Helper class to find classes using reflection.
 * @author Christian Grefe (CERN)
 */
public class ClassFinder {

	public static List<String> getClassNamesFromPackage(String packageName) throws IOException {
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		URL packageURL;
		ArrayList<String> names = new ArrayList<String>();

		packageName = packageName.replace(".", "/");
		packageURL = classLoader.getResource(packageName);

		if (packageURL.getProtocol().equals("jar")) {
			String jarFileName;
			JarFile jarFile;
			Enumeration<JarEntry> jarEntries;
			String entryName;

			// build jar file name, then loop through zipped entries
			jarFileName = URLDecoder.decode(packageURL.getFile(), "UTF-8");
			jarFileName = jarFileName.substring(5, jarFileName.indexOf("!"));
			jarFile = new JarFile(jarFileName);
			jarEntries = jarFile.entries();
			while (jarEntries.hasMoreElements()) {
				entryName = jarEntries.nextElement().getName();
				if (entryName.startsWith(packageName) && entryName.length() > packageName.length() + 5) {
					entryName = entryName.substring(packageName.length(), entryName.lastIndexOf('.'));
					if (entryName.startsWith("/")) {
						entryName = entryName.substring(1);
					}
					names.add(entryName);
				}
			}
			jarFile.close();

		// loop through files in classpath
		} else {
			File folder = new File(packageURL.getFile());
			File[] content = folder.listFiles();
			String entryName;
			for (File actual : content) {
				entryName = actual.getName();
				entryName = entryName.substring(0, entryName.lastIndexOf('.'));
				names.add(entryName);
			}
		}
		return names;
	}
}
