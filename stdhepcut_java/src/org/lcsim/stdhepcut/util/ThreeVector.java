package org.lcsim.stdhepcut.util;

import hep.physics.vec.BasicHep3Vector;
import hep.physics.vec.Hep3Vector;

/**
 * Extension of the Hep3Vector. Adds typical vector methods.
 * @author Christian Grefe (CERN)
 */
public class ThreeVector extends BasicHep3Vector {

	private static final long serialVersionUID = -1902903263477336425L;

	public ThreeVector() {
		super();
	}

	public ThreeVector(double[] v) {
		super(v);
	}

	public ThreeVector(float[] v) {
		super(v);
	}

	public ThreeVector(double x, double y, double z) {
		super(x, y, z);
	}

	public ThreeVector(Hep3Vector v) {
		super(v.v());
	}

	@Override
	public double magnitudeSquared() {
		return dot(this);
	}

	@Override
	public double magnitude() {
		return Math.sqrt(magnitudeSquared());
	}

	public void add(Hep3Vector v) {
		setV(x() + v.x(), y() + v.y(), z() + v.z());
	}

	public void sub(Hep3Vector v) {
		setV(x() - v.x(), y() - v.y(), z() - v.z());
	}

	public void mult(double s) {
		setV(s * x(), s * y(), s * z());
	}

	public double dot(Hep3Vector v) {
		return x() * v.x() + y() * v.y() + z() * v.z();
	}

	public ThreeVector cross(Hep3Vector v) {
		double u1 = y() * v.z() - z() * v.y();
		double u2 = z() * v.x() - x() * v.z();
		double u3 = x() * v.y() - y() * v.x();
		return new ThreeVector(u1, u2, u3);
	}
	
	/**
	 * @param v
	 * @return Angle between this vector and the given vector.
	 */
	public double angle(Hep3Vector v) {
		return Math.acos(dot(v)/(magnitude()*v.magnitude()));
	}

	public double p() {
		return magnitude();
	}

	/**
	 * @return Transverse momentum
	 */
	public double pT() {
		return Math.sqrt(x() * x() + y() * y());
	}

	/**
	 * @return Azimuthal angle
	 */
	public double phi() {
		return Math.atan2(y(), x());
	}

	/**
	 * @return Polar angle
	 */
	public double theta() {
		return Math.acos(z() / p());
	}

	@Override
	public String toString() {
		return "ThreeVector: x = " + x() + ", y = " + y() + ", z = " + z();
	}

}
