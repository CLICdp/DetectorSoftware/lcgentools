package org.lcsim.stdhepcut.util;

import hep.physics.vec.BasicHepLorentzVector;
import hep.physics.vec.Hep3Vector;
import hep.physics.vec.HepLorentzVector;
import hep.physics.vec.VecOp;

/**
 * Extension of the HepLorentzVector. Adds typical vector methods.
 * @author Christian Grefe (CERN)
 */
public class LorentzVector extends BasicHepLorentzVector {

	private static final long serialVersionUID = -6823824188655436767L;
	
	public LorentzVector() {
		super();
	}
	
	public LorentzVector(double energy, Hep3Vector v) {
		super(energy, v.v());
	}
	
	public LorentzVector(HepLorentzVector v) {
		super(v.t(), v.v3());
	}
	
	public LorentzVector(double energy, double px, double py, double pz) {
		super(energy, px, py, pz);
	}
	
	public LorentzVector(double energy, double[] v) {
		super(energy, v);
	}
	
	public LorentzVector(double energy, float[] v) {
		super(energy, v);
	}
	
	public double E() {
		return t();
	}
	
	public double px() {
		return v3().x();
	}
	
	public double py() {
		return v3().y();
	}

	public double pz() {
		return v3().z();
	}
	
	public double p() {
		return v3().magnitude();
	}
	
	/**
	 * @return Transverse momentum
	 */
	public double pT() {
		return Math.sqrt(px() * px() + py() * py());
	}
	
	/**
	 * @return Azimuthal angle
	 */
	public double phi() {
		return Math.atan2(py(), px());
	}

	/**
	 * @return Polar angle
	 */
	public double theta() {
		return Math.acos(pz() / p());
	}
	
	/**
	 * @return Invariant mass
	 */
	public double mass() {
		return magnitude();
	}
	
	/**
	 * @param v
	 * @return Angle between this vector and the given vector.
	 */
	public double angle(Hep3Vector v) {
		return Math.acos(VecOp.dot(this.v3(), v) / (this.v3().magnitude() * v.magnitude()));
	}
	
	/**
	 * @param v
	 * @return Angle between this vector and the given vector.
	 */
	public double angle(HepLorentzVector v) {
		return Math.acos(VecOp.dot(this.v3(), v.v3()) / (this.v3().magnitude() * v.v3().magnitude()));
	}
	
	public void add(HepLorentzVector v) {
		HepLorentzVector w = VecOp.add(this, v);
		setV3(w.t(), w.v3());
	}
	
	public void sub(HepLorentzVector v) {
		HepLorentzVector w = VecOp.sub(this, v);
		setV3(w.t(), w.v3());
	}
	
	public void mult(double s) {
		setV3(s*E(), s*px(), s*py(), s*pz());
	}
	
	public double dot(HepLorentzVector v) {
		return VecOp.dot(this, v);
	}
	
	public void boost(Hep3Vector boostVector) {
		HepLorentzVector boosted = VecOp.boost(this, boostVector);
		setV3(boosted.t(), boosted.v3());
	}
	
	public void boost(HepLorentzVector refFourVector) {
		HepLorentzVector boosted = VecOp.boost(this, refFourVector);
		setV3(boosted.t(), boosted.v3());
	}
	
	@Override
	public String toString() {
		return "LorentzVector: E = " + E() + ", px = " + px() + ", py = " + py() + " pz = " + pz() + ", mass = " + mass();
	}
	
}
