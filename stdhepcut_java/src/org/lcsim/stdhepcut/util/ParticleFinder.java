package org.lcsim.stdhepcut.util;

import hep.lcio.event.MCParticle;

import java.util.ArrayList;
import java.util.List;

/**
 * Helper class to find specific sets of MCParticles from an input collection.
 * 
 * @author Christian Grefe (CERN)
 */
public class ParticleFinder {

	/**
	 * Returns a list of all final state MCParticles with the given PDG ID in
	 * the input collection. Particles and anti-particles are kept.
	 * 
	 * @param mcParticles
	 *            The collection of MCParticles
	 * @param pdgid
	 *            The PDG ID of the MCParticles to keep
	 * @return The list of matching MCParticles
	 */
	public static List<MCParticle> findFinalStateParticles(List<MCParticle> mcParticles, int pdgid) {
		List<MCParticle> particles = new ArrayList<MCParticle>();
		for (MCParticle mcp : mcParticles) {
			if (Math.abs(mcp.getPDG()) == pdgid && mcp.getGeneratorStatus() == 1) {
				particles.add(mcp);
			}
		}
		return particles;
	}

	/**
	 * Returns a list of all taus in the input collection. Since taus can decay
	 * into themselves before the actual decay, only the last taus are returned.
	 * 
	 * @param mcParticles
	 *            The collection of MCParticles
	 * @return The list of taus
	 */
	public static List<MCParticle> findTaus(List<MCParticle> mcParticles) {
		List<MCParticle> taus = new ArrayList<MCParticle>();
		for (MCParticle mcp : mcParticles) {
			if (Math.abs(mcp.getPDG()) == 15 && mcp.getDaughters().size() > 1) {
				boolean hasTau = loopDaughtersForTau(mcp);
				if (!hasTau) {
					taus.add(mcp);
				}
			}
		}
		return taus;
	}

	/**
	 * Returns a list of all quarks in the input collection. Since quarks can
	 * decay into themselves before the actual decay, only the last taus are
	 * returned.
	 * 
	 * @param mcParticles
	 *            The collection of MCParticles
	 * @return The list of quarks before hadronization
	 */
	public static List<MCParticle> findQuarks(List<MCParticle> mcParticles) {
		// Start by finding the initial quarks
		List<MCParticle> firstQuarks = new ArrayList<MCParticle>();
		for (MCParticle mcp : mcParticles) {
			if (mcp.getParents().size() < 1) {
				continue;
			}
			MCParticle parent = (MCParticle) mcp.getParents().get(0);
			int parentPDG = Math.abs(parent.getPDG());
			// Initial quarks have a parent different from themselves
			if (Math.abs(mcp.getPDG()) < 7 && (parentPDG == 11 || parentPDG == 22) && parent.getGeneratorStatus() != 1) {
				firstQuarks.add(mcp);
			}
		}
		// Find the quarks right before fragmentation
		List<MCParticle> lastQuarks = new ArrayList<MCParticle>();
		for (MCParticle mcp : firstQuarks) {
			lastQuarks.add(loopDaughtersForLastQuark(mcp, Math.abs(mcp.getPDG())));
		}
		return lastQuarks;
	}

	/**
	 * Checks if the given particle has a tau daughter.
	 * 
	 * @param particle
	 *            The particle to check
	 * @return Contains tau daughter
	 */
	@SuppressWarnings("unchecked")
	protected static boolean loopDaughtersForTau(MCParticle particle) {
		for (MCParticle daughter : (List<MCParticle>) particle.getDaughters()) {
			if (Math.abs(daughter.getPDG()) == 15) {
				return true;
			}
			if (loopDaughtersForTau(daughter)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Finds the last quark in the decay chain before the hadronization.
	 * 
	 * @param particle
	 *            The input quark
	 * @param pdgid
	 *            The PDG ID of the input quark
	 * @return The quark that hadronizes
	 */
	@SuppressWarnings("unchecked")
	protected static MCParticle loopDaughtersForLastQuark(MCParticle particle, int pdgid) {
		for (MCParticle daughter : (List<MCParticle>) particle.getDaughters()) {
			int daughterPDG = Math.abs(daughter.getPDG());
			// Check if this is the last quark before hadronization
			if (daughterPDG == 21 || daughterPDG == 92 || daughterPDG == 94) {
				return particle;
			}
			if (daughterPDG == pdgid) {
				return loopDaughtersForLastQuark(daughter, pdgid);
			}
		}
		return particle;
	}

}
